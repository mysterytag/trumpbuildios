﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/<Map>c__AnonStorey10E`2<System.Int32,System.Object>
struct U3CMapU3Ec__AnonStorey10E_2_t2813723001;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"

// System.Void CellReactiveApi/<Map>c__AnonStorey10E`2<System.Int32,System.Object>::.ctor()
extern "C"  void U3CMapU3Ec__AnonStorey10E_2__ctor_m771292267_gshared (U3CMapU3Ec__AnonStorey10E_2_t2813723001 * __this, const MethodInfo* method);
#define U3CMapU3Ec__AnonStorey10E_2__ctor_m771292267(__this, method) ((  void (*) (U3CMapU3Ec__AnonStorey10E_2_t2813723001 *, const MethodInfo*))U3CMapU3Ec__AnonStorey10E_2__ctor_m771292267_gshared)(__this, method)
// System.IDisposable CellReactiveApi/<Map>c__AnonStorey10E`2<System.Int32,System.Object>::<>m__180(System.Action`1<T2>,Priority)
extern "C"  Il2CppObject * U3CMapU3Ec__AnonStorey10E_2_U3CU3Em__180_m1992129854_gshared (U3CMapU3Ec__AnonStorey10E_2_t2813723001 * __this, Action_1_t985559125 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define U3CMapU3Ec__AnonStorey10E_2_U3CU3Em__180_m1992129854(__this, ___reaction0, ___p1, method) ((  Il2CppObject * (*) (U3CMapU3Ec__AnonStorey10E_2_t2813723001 *, Action_1_t985559125 *, int32_t, const MethodInfo*))U3CMapU3Ec__AnonStorey10E_2_U3CU3Em__180_m1992129854_gshared)(__this, ___reaction0, ___p1, method)
// T2 CellReactiveApi/<Map>c__AnonStorey10E`2<System.Int32,System.Object>::<>m__181()
extern "C"  Il2CppObject * U3CMapU3Ec__AnonStorey10E_2_U3CU3Em__181_m1165071513_gshared (U3CMapU3Ec__AnonStorey10E_2_t2813723001 * __this, const MethodInfo* method);
#define U3CMapU3Ec__AnonStorey10E_2_U3CU3Em__181_m1165071513(__this, method) ((  Il2CppObject * (*) (U3CMapU3Ec__AnonStorey10E_2_t2813723001 *, const MethodInfo*))U3CMapU3Ec__AnonStorey10E_2_U3CU3Em__181_m1165071513_gshared)(__this, method)
