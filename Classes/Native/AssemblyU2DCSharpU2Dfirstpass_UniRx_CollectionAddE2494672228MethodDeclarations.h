﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionAddE2416521987MethodDeclarations.h"

// System.Void UniRx.CollectionAddEvent`1<SettingsButton>::.ctor(System.Int32,T)
#define CollectionAddEvent_1__ctor_m2631571755(__this, ___index0, ___value1, method) ((  void (*) (CollectionAddEvent_1_t2494672228 *, int32_t, SettingsButton_t915256661 *, const MethodInfo*))CollectionAddEvent_1__ctor_m4121910756_gshared)(__this, ___index0, ___value1, method)
// System.Int32 UniRx.CollectionAddEvent`1<SettingsButton>::get_Index()
#define CollectionAddEvent_1_get_Index_m537635155(__this, method) ((  int32_t (*) (CollectionAddEvent_1_t2494672228 *, const MethodInfo*))CollectionAddEvent_1_get_Index_m1649808760_gshared)(__this, method)
// System.Void UniRx.CollectionAddEvent`1<SettingsButton>::set_Index(System.Int32)
#define CollectionAddEvent_1_set_Index_m2009830118(__this, ___value0, method) ((  void (*) (CollectionAddEvent_1_t2494672228 *, int32_t, const MethodInfo*))CollectionAddEvent_1_set_Index_m4001500511_gshared)(__this, ___value0, method)
// T UniRx.CollectionAddEvent`1<SettingsButton>::get_Value()
#define CollectionAddEvent_1_get_Value_m3262911369(__this, method) ((  SettingsButton_t915256661 * (*) (CollectionAddEvent_1_t2494672228 *, const MethodInfo*))CollectionAddEvent_1_get_Value_m1882365472_gshared)(__this, method)
// System.Void UniRx.CollectionAddEvent`1<SettingsButton>::set_Value(T)
#define CollectionAddEvent_1_set_Value_m2489496042(__this, ___value0, method) ((  void (*) (CollectionAddEvent_1_t2494672228 *, SettingsButton_t915256661 *, const MethodInfo*))CollectionAddEvent_1_set_Value_m1389365521_gshared)(__this, ___value0, method)
// System.String UniRx.CollectionAddEvent`1<SettingsButton>::ToString()
#define CollectionAddEvent_1_ToString_m2212624593(__this, method) ((  String_t* (*) (CollectionAddEvent_1_t2494672228 *, const MethodInfo*))CollectionAddEvent_1_ToString_m3034630034_gshared)(__this, method)
// System.Int32 UniRx.CollectionAddEvent`1<SettingsButton>::GetHashCode()
#define CollectionAddEvent_1_GetHashCode_m265187323(__this, method) ((  int32_t (*) (CollectionAddEvent_1_t2494672228 *, const MethodInfo*))CollectionAddEvent_1_GetHashCode_m3912132320_gshared)(__this, method)
// System.Boolean UniRx.CollectionAddEvent`1<SettingsButton>::Equals(UniRx.CollectionAddEvent`1<T>)
#define CollectionAddEvent_1_Equals_m1521280175(__this, ___other0, method) ((  bool (*) (CollectionAddEvent_1_t2494672228 *, CollectionAddEvent_1_t2494672228 , const MethodInfo*))CollectionAddEvent_1_Equals_m2095755040_gshared)(__this, ___other0, method)
