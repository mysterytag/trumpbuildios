﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ModestTree.MiscExtensions/<Yield>c__Iterator3D`1<System.Object>
struct U3CYieldU3Ec__Iterator3D_1_t3174792911;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2320212868;

#include "codegen/il2cpp-codegen.h"

// System.Void ModestTree.MiscExtensions/<Yield>c__Iterator3D`1<System.Object>::.ctor()
extern "C"  void U3CYieldU3Ec__Iterator3D_1__ctor_m2090070712_gshared (U3CYieldU3Ec__Iterator3D_1_t3174792911 * __this, const MethodInfo* method);
#define U3CYieldU3Ec__Iterator3D_1__ctor_m2090070712(__this, method) ((  void (*) (U3CYieldU3Ec__Iterator3D_1_t3174792911 *, const MethodInfo*))U3CYieldU3Ec__Iterator3D_1__ctor_m2090070712_gshared)(__this, method)
// T ModestTree.MiscExtensions/<Yield>c__Iterator3D`1<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Il2CppObject * U3CYieldU3Ec__Iterator3D_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m3185341375_gshared (U3CYieldU3Ec__Iterator3D_1_t3174792911 * __this, const MethodInfo* method);
#define U3CYieldU3Ec__Iterator3D_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m3185341375(__this, method) ((  Il2CppObject * (*) (U3CYieldU3Ec__Iterator3D_1_t3174792911 *, const MethodInfo*))U3CYieldU3Ec__Iterator3D_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m3185341375_gshared)(__this, method)
// System.Object ModestTree.MiscExtensions/<Yield>c__Iterator3D`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CYieldU3Ec__Iterator3D_1_System_Collections_IEnumerator_get_Current_m24006446_gshared (U3CYieldU3Ec__Iterator3D_1_t3174792911 * __this, const MethodInfo* method);
#define U3CYieldU3Ec__Iterator3D_1_System_Collections_IEnumerator_get_Current_m24006446(__this, method) ((  Il2CppObject * (*) (U3CYieldU3Ec__Iterator3D_1_t3174792911 *, const MethodInfo*))U3CYieldU3Ec__Iterator3D_1_System_Collections_IEnumerator_get_Current_m24006446_gshared)(__this, method)
// System.Collections.IEnumerator ModestTree.MiscExtensions/<Yield>c__Iterator3D`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CYieldU3Ec__Iterator3D_1_System_Collections_IEnumerable_GetEnumerator_m2463013289_gshared (U3CYieldU3Ec__Iterator3D_1_t3174792911 * __this, const MethodInfo* method);
#define U3CYieldU3Ec__Iterator3D_1_System_Collections_IEnumerable_GetEnumerator_m2463013289(__this, method) ((  Il2CppObject * (*) (U3CYieldU3Ec__Iterator3D_1_t3174792911 *, const MethodInfo*))U3CYieldU3Ec__Iterator3D_1_System_Collections_IEnumerable_GetEnumerator_m2463013289_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> ModestTree.MiscExtensions/<Yield>c__Iterator3D`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* U3CYieldU3Ec__Iterator3D_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2861739714_gshared (U3CYieldU3Ec__Iterator3D_1_t3174792911 * __this, const MethodInfo* method);
#define U3CYieldU3Ec__Iterator3D_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2861739714(__this, method) ((  Il2CppObject* (*) (U3CYieldU3Ec__Iterator3D_1_t3174792911 *, const MethodInfo*))U3CYieldU3Ec__Iterator3D_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2861739714_gshared)(__this, method)
// System.Boolean ModestTree.MiscExtensions/<Yield>c__Iterator3D`1<System.Object>::MoveNext()
extern "C"  bool U3CYieldU3Ec__Iterator3D_1_MoveNext_m1241276476_gshared (U3CYieldU3Ec__Iterator3D_1_t3174792911 * __this, const MethodInfo* method);
#define U3CYieldU3Ec__Iterator3D_1_MoveNext_m1241276476(__this, method) ((  bool (*) (U3CYieldU3Ec__Iterator3D_1_t3174792911 *, const MethodInfo*))U3CYieldU3Ec__Iterator3D_1_MoveNext_m1241276476_gshared)(__this, method)
// System.Void ModestTree.MiscExtensions/<Yield>c__Iterator3D`1<System.Object>::Dispose()
extern "C"  void U3CYieldU3Ec__Iterator3D_1_Dispose_m2706510069_gshared (U3CYieldU3Ec__Iterator3D_1_t3174792911 * __this, const MethodInfo* method);
#define U3CYieldU3Ec__Iterator3D_1_Dispose_m2706510069(__this, method) ((  void (*) (U3CYieldU3Ec__Iterator3D_1_t3174792911 *, const MethodInfo*))U3CYieldU3Ec__Iterator3D_1_Dispose_m2706510069_gshared)(__this, method)
// System.Void ModestTree.MiscExtensions/<Yield>c__Iterator3D`1<System.Object>::Reset()
extern "C"  void U3CYieldU3Ec__Iterator3D_1_Reset_m4031470949_gshared (U3CYieldU3Ec__Iterator3D_1_t3174792911 * __this, const MethodInfo* method);
#define U3CYieldU3Ec__Iterator3D_1_Reset_m4031470949(__this, method) ((  void (*) (U3CYieldU3Ec__Iterator3D_1_t3174792911 *, const MethodInfo*))U3CYieldU3Ec__Iterator3D_1_Reset_m4031470949_gshared)(__this, method)
