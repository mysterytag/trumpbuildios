﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetUserCombinedInfoRequestCallback
struct GetUserCombinedInfoRequestCallback_t3104634176;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetUserCombinedInfoRequest
struct GetUserCombinedInfoRequest_t3431761899;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetUserComb3431761899.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetUserCombinedInfoRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetUserCombinedInfoRequestCallback__ctor_m1043843631 (GetUserCombinedInfoRequestCallback_t3104634176 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetUserCombinedInfoRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetUserCombinedInfoRequest,System.Object)
extern "C"  void GetUserCombinedInfoRequestCallback_Invoke_m1201037855 (GetUserCombinedInfoRequestCallback_t3104634176 * __this, String_t* ___urlPath0, int32_t ___callId1, GetUserCombinedInfoRequest_t3431761899 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetUserCombinedInfoRequestCallback_t3104634176(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetUserCombinedInfoRequest_t3431761899 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetUserCombinedInfoRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetUserCombinedInfoRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetUserCombinedInfoRequestCallback_BeginInvoke_m1944150886 (GetUserCombinedInfoRequestCallback_t3104634176 * __this, String_t* ___urlPath0, int32_t ___callId1, GetUserCombinedInfoRequest_t3431761899 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetUserCombinedInfoRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetUserCombinedInfoRequestCallback_EndInvoke_m3064835007 (GetUserCombinedInfoRequestCallback_t3104634176 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
