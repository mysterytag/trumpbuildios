﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator4`1<System.Object>
struct U3CToEnumerableU3Ec__Iterator4_1_t4039804212;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2320212868;

#include "codegen/il2cpp-codegen.h"

// System.Void GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator4`1<System.Object>::.ctor()
extern "C"  void U3CToEnumerableU3Ec__Iterator4_1__ctor_m2253352140_gshared (U3CToEnumerableU3Ec__Iterator4_1_t4039804212 * __this, const MethodInfo* method);
#define U3CToEnumerableU3Ec__Iterator4_1__ctor_m2253352140(__this, method) ((  void (*) (U3CToEnumerableU3Ec__Iterator4_1_t4039804212 *, const MethodInfo*))U3CToEnumerableU3Ec__Iterator4_1__ctor_m2253352140_gshared)(__this, method)
// T GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator4`1<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Il2CppObject * U3CToEnumerableU3Ec__Iterator4_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1583042899_gshared (U3CToEnumerableU3Ec__Iterator4_1_t4039804212 * __this, const MethodInfo* method);
#define U3CToEnumerableU3Ec__Iterator4_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1583042899(__this, method) ((  Il2CppObject * (*) (U3CToEnumerableU3Ec__Iterator4_1_t4039804212 *, const MethodInfo*))U3CToEnumerableU3Ec__Iterator4_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1583042899_gshared)(__this, method)
// System.Object GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator4`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CToEnumerableU3Ec__Iterator4_1_System_Collections_IEnumerator_get_Current_m949637914_gshared (U3CToEnumerableU3Ec__Iterator4_1_t4039804212 * __this, const MethodInfo* method);
#define U3CToEnumerableU3Ec__Iterator4_1_System_Collections_IEnumerator_get_Current_m949637914(__this, method) ((  Il2CppObject * (*) (U3CToEnumerableU3Ec__Iterator4_1_t4039804212 *, const MethodInfo*))U3CToEnumerableU3Ec__Iterator4_1_System_Collections_IEnumerator_get_Current_m949637914_gshared)(__this, method)
// System.Collections.IEnumerator GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator4`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CToEnumerableU3Ec__Iterator4_1_System_Collections_IEnumerable_GetEnumerator_m1545923861_gshared (U3CToEnumerableU3Ec__Iterator4_1_t4039804212 * __this, const MethodInfo* method);
#define U3CToEnumerableU3Ec__Iterator4_1_System_Collections_IEnumerable_GetEnumerator_m1545923861(__this, method) ((  Il2CppObject * (*) (U3CToEnumerableU3Ec__Iterator4_1_t4039804212 *, const MethodInfo*))U3CToEnumerableU3Ec__Iterator4_1_System_Collections_IEnumerable_GetEnumerator_m1545923861_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator4`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* U3CToEnumerableU3Ec__Iterator4_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4203936214_gshared (U3CToEnumerableU3Ec__Iterator4_1_t4039804212 * __this, const MethodInfo* method);
#define U3CToEnumerableU3Ec__Iterator4_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4203936214(__this, method) ((  Il2CppObject* (*) (U3CToEnumerableU3Ec__Iterator4_1_t4039804212 *, const MethodInfo*))U3CToEnumerableU3Ec__Iterator4_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4203936214_gshared)(__this, method)
// System.Boolean GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator4`1<System.Object>::MoveNext()
extern "C"  bool U3CToEnumerableU3Ec__Iterator4_1_MoveNext_m3624260520_gshared (U3CToEnumerableU3Ec__Iterator4_1_t4039804212 * __this, const MethodInfo* method);
#define U3CToEnumerableU3Ec__Iterator4_1_MoveNext_m3624260520(__this, method) ((  bool (*) (U3CToEnumerableU3Ec__Iterator4_1_t4039804212 *, const MethodInfo*))U3CToEnumerableU3Ec__Iterator4_1_MoveNext_m3624260520_gshared)(__this, method)
// System.Void GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator4`1<System.Object>::Dispose()
extern "C"  void U3CToEnumerableU3Ec__Iterator4_1_Dispose_m706172425_gshared (U3CToEnumerableU3Ec__Iterator4_1_t4039804212 * __this, const MethodInfo* method);
#define U3CToEnumerableU3Ec__Iterator4_1_Dispose_m706172425(__this, method) ((  void (*) (U3CToEnumerableU3Ec__Iterator4_1_t4039804212 *, const MethodInfo*))U3CToEnumerableU3Ec__Iterator4_1_Dispose_m706172425_gshared)(__this, method)
// System.Void GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator4`1<System.Object>::Reset()
extern "C"  void U3CToEnumerableU3Ec__Iterator4_1_Reset_m4194752377_gshared (U3CToEnumerableU3Ec__Iterator4_1_t4039804212 * __this, const MethodInfo* method);
#define U3CToEnumerableU3Ec__Iterator4_1_Reset_m4194752377(__this, method) ((  void (*) (U3CToEnumerableU3Ec__Iterator4_1_t4039804212 *, const MethodInfo*))U3CToEnumerableU3Ec__Iterator4_1_Reset_m4194752377_gshared)(__this, method)
