﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/CancelTradeRequestCallback
struct CancelTradeRequestCallback_t3608790954;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.CancelTradeRequest
struct CancelTradeRequest_t685728853;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_CancelTradeR685728853.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/CancelTradeRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void CancelTradeRequestCallback__ctor_m3051839385 (CancelTradeRequestCallback_t3608790954 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/CancelTradeRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.CancelTradeRequest,System.Object)
extern "C"  void CancelTradeRequestCallback_Invoke_m173463903 (CancelTradeRequestCallback_t3608790954 * __this, String_t* ___urlPath0, int32_t ___callId1, CancelTradeRequest_t685728853 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_CancelTradeRequestCallback_t3608790954(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, CancelTradeRequest_t685728853 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/CancelTradeRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.CancelTradeRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * CancelTradeRequestCallback_BeginInvoke_m807333178 (CancelTradeRequestCallback_t3608790954 * __this, String_t* ___urlPath0, int32_t ___callId1, CancelTradeRequest_t685728853 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/CancelTradeRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void CancelTradeRequestCallback_EndInvoke_m2206500393 (CancelTradeRequestCallback_t3608790954 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
