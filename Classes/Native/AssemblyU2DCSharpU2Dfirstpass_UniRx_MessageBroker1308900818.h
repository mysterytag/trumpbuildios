﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IMessageBroker
struct IMessageBroker_t3057826857;
// System.Collections.Generic.Dictionary`2<System.Type,System.Object>
struct Dictionary_2_t2949654135;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.MessageBroker
struct  MessageBroker_t1308900818  : public Il2CppObject
{
public:
	// System.Boolean UniRx.MessageBroker::isDisposed
	bool ___isDisposed_1;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Object> UniRx.MessageBroker::notifiers
	Dictionary_2_t2949654135 * ___notifiers_2;

public:
	inline static int32_t get_offset_of_isDisposed_1() { return static_cast<int32_t>(offsetof(MessageBroker_t1308900818, ___isDisposed_1)); }
	inline bool get_isDisposed_1() const { return ___isDisposed_1; }
	inline bool* get_address_of_isDisposed_1() { return &___isDisposed_1; }
	inline void set_isDisposed_1(bool value)
	{
		___isDisposed_1 = value;
	}

	inline static int32_t get_offset_of_notifiers_2() { return static_cast<int32_t>(offsetof(MessageBroker_t1308900818, ___notifiers_2)); }
	inline Dictionary_2_t2949654135 * get_notifiers_2() const { return ___notifiers_2; }
	inline Dictionary_2_t2949654135 ** get_address_of_notifiers_2() { return &___notifiers_2; }
	inline void set_notifiers_2(Dictionary_2_t2949654135 * value)
	{
		___notifiers_2 = value;
		Il2CppCodeGenWriteBarrier(&___notifiers_2, value);
	}
};

struct MessageBroker_t1308900818_StaticFields
{
public:
	// UniRx.IMessageBroker UniRx.MessageBroker::Default
	Il2CppObject * ___Default_0;

public:
	inline static int32_t get_offset_of_Default_0() { return static_cast<int32_t>(offsetof(MessageBroker_t1308900818_StaticFields, ___Default_0)); }
	inline Il2CppObject * get_Default_0() const { return ___Default_0; }
	inline Il2CppObject ** get_address_of_Default_0() { return &___Default_0; }
	inline void set_Default_0(Il2CppObject * value)
	{
		___Default_0 = value;
		Il2CppCodeGenWriteBarrier(&___Default_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
