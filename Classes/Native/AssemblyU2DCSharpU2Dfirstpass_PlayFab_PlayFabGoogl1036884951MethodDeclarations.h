﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabGoogleCloudMessaging/GCMMessageReceived
struct GCMMessageReceived_t1036884951;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabGoogleCloudMessaging/GCMMessageReceived::.ctor(System.Object,System.IntPtr)
extern "C"  void GCMMessageReceived__ctor_m2329524089 (GCMMessageReceived_t1036884951 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabGoogleCloudMessaging/GCMMessageReceived::Invoke(System.String)
extern "C"  void GCMMessageReceived_Invoke_m4197313743 (GCMMessageReceived_t1036884951 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GCMMessageReceived_t1036884951(Il2CppObject* delegate, String_t* ___message0);
// System.IAsyncResult PlayFab.PlayFabGoogleCloudMessaging/GCMMessageReceived::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GCMMessageReceived_BeginInvoke_m3003929940 (GCMMessageReceived_t1036884951 * __this, String_t* ___message0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabGoogleCloudMessaging/GCMMessageReceived::EndInvoke(System.IAsyncResult)
extern "C"  void GCMMessageReceived_EndInvoke_m1478348297 (GCMMessageReceived_t1036884951 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
