﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.InternalUtil.EmptyObserver`1<TableButtons/StatkaPoTableViev>
struct EmptyObserver_1_t2917748807;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.InternalUtil.EmptyObserver`1<TableButtons/StatkaPoTableViev>
struct  EmptyObserver_1_t2917748807  : public Il2CppObject
{
public:

public:
};

struct EmptyObserver_1_t2917748807_StaticFields
{
public:
	// UniRx.InternalUtil.EmptyObserver`1<T> UniRx.InternalUtil.EmptyObserver`1::Instance
	EmptyObserver_1_t2917748807 * ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(EmptyObserver_1_t2917748807_StaticFields, ___Instance_0)); }
	inline EmptyObserver_1_t2917748807 * get_Instance_0() const { return ___Instance_0; }
	inline EmptyObserver_1_t2917748807 ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(EmptyObserver_1_t2917748807 * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier(&___Instance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
