﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.WebProxy
struct WebProxy_t3145103108;
// System.String
struct String_t;
// System.Uri
struct Uri_t2776692961;
// System.String[]
struct StringU5BU5D_t2956870243;
// System.Net.ICredentials
struct ICredentials_t2307785309;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2995724695;
// System.Collections.ArrayList
struct ArrayList_t2121638921;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "System_System_Uri2776692961.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2995724695.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCont986364934.h"

// System.Void System.Net.WebProxy::.ctor()
extern "C"  void WebProxy__ctor_m2333292256 (WebProxy_t3145103108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebProxy::.ctor(System.String)
extern "C"  void WebProxy__ctor_m2361824290 (WebProxy_t3145103108 * __this, String_t* ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebProxy::.ctor(System.Uri)
extern "C"  void WebProxy__ctor_m3249294995 (WebProxy_t3145103108 * __this, Uri_t2776692961 * ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebProxy::.ctor(System.String,System.Boolean)
extern "C"  void WebProxy__ctor_m3814635291 (WebProxy_t3145103108 * __this, String_t* ___address0, bool ___bypassOnLocal1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebProxy::.ctor(System.String,System.Int32)
extern "C"  void WebProxy__ctor_m203542517 (WebProxy_t3145103108 * __this, String_t* ___host0, int32_t ___port1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebProxy::.ctor(System.Uri,System.Boolean)
extern "C"  void WebProxy__ctor_m879799434 (WebProxy_t3145103108 * __this, Uri_t2776692961 * ___address0, bool ___bypassOnLocal1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebProxy::.ctor(System.String,System.Boolean,System.String[])
extern "C"  void WebProxy__ctor_m768340341 (WebProxy_t3145103108 * __this, String_t* ___address0, bool ___bypassOnLocal1, StringU5BU5D_t2956870243* ___bypassList2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebProxy::.ctor(System.Uri,System.Boolean,System.String[])
extern "C"  void WebProxy__ctor_m3355125988 (WebProxy_t3145103108 * __this, Uri_t2776692961 * ___address0, bool ___bypassOnLocal1, StringU5BU5D_t2956870243* ___bypassList2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebProxy::.ctor(System.String,System.Boolean,System.String[],System.Net.ICredentials)
extern "C"  void WebProxy__ctor_m3733302398 (WebProxy_t3145103108 * __this, String_t* ___address0, bool ___bypassOnLocal1, StringU5BU5D_t2956870243* ___bypassList2, Il2CppObject * ___credentials3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebProxy::.ctor(System.Uri,System.Boolean,System.String[],System.Net.ICredentials)
extern "C"  void WebProxy__ctor_m1295075053 (WebProxy_t3145103108 * __this, Uri_t2776692961 * ___address0, bool ___bypassOnLocal1, StringU5BU5D_t2956870243* ___bypassList2, Il2CppObject * ___credentials3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebProxy::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void WebProxy__ctor_m3926845473 (WebProxy_t3145103108 * __this, SerializationInfo_t2995724695 * ___serializationInfo0, StreamingContext_t986364934  ___streamingContext1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebProxy::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void WebProxy_System_Runtime_Serialization_ISerializable_GetObjectData_m1269108139 (WebProxy_t3145103108 * __this, SerializationInfo_t2995724695 * ___serializationInfo0, StreamingContext_t986364934  ___streamingContext1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri System.Net.WebProxy::get_Address()
extern "C"  Uri_t2776692961 * WebProxy_get_Address_m3934846299 (WebProxy_t3145103108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebProxy::set_Address(System.Uri)
extern "C"  void WebProxy_set_Address_m125708040 (WebProxy_t3145103108 * __this, Uri_t2776692961 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList System.Net.WebProxy::get_BypassArrayList()
extern "C"  ArrayList_t2121638921 * WebProxy_get_BypassArrayList_m3025434820 (WebProxy_t3145103108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.Net.WebProxy::get_BypassList()
extern "C"  StringU5BU5D_t2956870243* WebProxy_get_BypassList_m1934073074 (WebProxy_t3145103108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebProxy::set_BypassList(System.String[])
extern "C"  void WebProxy_set_BypassList_m642702137 (WebProxy_t3145103108 * __this, StringU5BU5D_t2956870243* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.WebProxy::get_BypassProxyOnLocal()
extern "C"  bool WebProxy_get_BypassProxyOnLocal_m2749156893 (WebProxy_t3145103108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebProxy::set_BypassProxyOnLocal(System.Boolean)
extern "C"  void WebProxy_set_BypassProxyOnLocal_m1093149342 (WebProxy_t3145103108 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.ICredentials System.Net.WebProxy::get_Credentials()
extern "C"  Il2CppObject * WebProxy_get_Credentials_m3075169793 (WebProxy_t3145103108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebProxy::set_Credentials(System.Net.ICredentials)
extern "C"  void WebProxy_set_Credentials_m1460592146 (WebProxy_t3145103108 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.WebProxy::get_UseDefaultCredentials()
extern "C"  bool WebProxy_get_UseDefaultCredentials_m2666418573 (WebProxy_t3145103108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebProxy::set_UseDefaultCredentials(System.Boolean)
extern "C"  void WebProxy_set_UseDefaultCredentials_m174746170 (WebProxy_t3145103108 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebProxy System.Net.WebProxy::GetDefaultProxy()
extern "C"  WebProxy_t3145103108 * WebProxy_GetDefaultProxy_m4035654666 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri System.Net.WebProxy::GetProxy(System.Uri)
extern "C"  Uri_t2776692961 * WebProxy_GetProxy_m1799832125 (WebProxy_t3145103108 * __this, Uri_t2776692961 * ___destination0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.WebProxy::IsBypassed(System.Uri)
extern "C"  bool WebProxy_IsBypassed_m2441560530 (WebProxy_t3145103108 * __this, Uri_t2776692961 * ___host0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebProxy::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void WebProxy_GetObjectData_m2646244990 (WebProxy_t3145103108 * __this, SerializationInfo_t2995724695 * ___serializationInfo0, StreamingContext_t986364934  ___streamingContext1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebProxy::CheckBypassList()
extern "C"  void WebProxy_CheckBypassList_m2380971276 (WebProxy_t3145103108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri System.Net.WebProxy::ToUri(System.String)
extern "C"  Uri_t2776692961 * WebProxy_ToUri_m1948809377 (Il2CppObject * __this /* static, unused */, String_t* ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
