﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey160
struct U3CRegisterDonateButtonsU3Ec__AnonStorey160_t2690654991;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey160::.ctor()
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey160__ctor_m2552583455 (U3CRegisterDonateButtonsU3Ec__AnonStorey160_t2690654991 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey160::<>m__25F(UniRx.Unit)
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey160_U3CU3Em__25F_m1948022145 (U3CRegisterDonateButtonsU3Ec__AnonStorey160_t2690654991 * __this, Unit_t2558286038  ___unit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey160::<>m__260(System.Boolean)
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey160_U3CU3Em__260_m2217299229 (U3CRegisterDonateButtonsU3Ec__AnonStorey160_t2690654991 * __this, bool ___connecGroup0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
