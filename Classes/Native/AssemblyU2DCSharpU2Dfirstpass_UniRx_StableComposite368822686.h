﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.IDisposable modreq(System.Runtime.CompilerServices.IsVolatile)
struct IDisposable_t1628921374;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_StableComposit3433635614.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.StableCompositeDisposable/Quaternary
struct  Quaternary_t368822686  : public StableCompositeDisposable_t3433635614
{
public:
	// System.Int32 UniRx.StableCompositeDisposable/Quaternary::disposedCallCount
	int32_t ___disposedCallCount_0;
	// System.IDisposable modreq(System.Runtime.CompilerServices.IsVolatile) UniRx.StableCompositeDisposable/Quaternary::_disposable1
	Il2CppObject * ____disposable1_1;
	// System.IDisposable modreq(System.Runtime.CompilerServices.IsVolatile) UniRx.StableCompositeDisposable/Quaternary::_disposable2
	Il2CppObject * ____disposable2_2;
	// System.IDisposable modreq(System.Runtime.CompilerServices.IsVolatile) UniRx.StableCompositeDisposable/Quaternary::_disposable3
	Il2CppObject * ____disposable3_3;
	// System.IDisposable modreq(System.Runtime.CompilerServices.IsVolatile) UniRx.StableCompositeDisposable/Quaternary::_disposable4
	Il2CppObject * ____disposable4_4;

public:
	inline static int32_t get_offset_of_disposedCallCount_0() { return static_cast<int32_t>(offsetof(Quaternary_t368822686, ___disposedCallCount_0)); }
	inline int32_t get_disposedCallCount_0() const { return ___disposedCallCount_0; }
	inline int32_t* get_address_of_disposedCallCount_0() { return &___disposedCallCount_0; }
	inline void set_disposedCallCount_0(int32_t value)
	{
		___disposedCallCount_0 = value;
	}

	inline static int32_t get_offset_of__disposable1_1() { return static_cast<int32_t>(offsetof(Quaternary_t368822686, ____disposable1_1)); }
	inline Il2CppObject * get__disposable1_1() const { return ____disposable1_1; }
	inline Il2CppObject ** get_address_of__disposable1_1() { return &____disposable1_1; }
	inline void set__disposable1_1(Il2CppObject * value)
	{
		____disposable1_1 = value;
		Il2CppCodeGenWriteBarrier(&____disposable1_1, value);
	}

	inline static int32_t get_offset_of__disposable2_2() { return static_cast<int32_t>(offsetof(Quaternary_t368822686, ____disposable2_2)); }
	inline Il2CppObject * get__disposable2_2() const { return ____disposable2_2; }
	inline Il2CppObject ** get_address_of__disposable2_2() { return &____disposable2_2; }
	inline void set__disposable2_2(Il2CppObject * value)
	{
		____disposable2_2 = value;
		Il2CppCodeGenWriteBarrier(&____disposable2_2, value);
	}

	inline static int32_t get_offset_of__disposable3_3() { return static_cast<int32_t>(offsetof(Quaternary_t368822686, ____disposable3_3)); }
	inline Il2CppObject * get__disposable3_3() const { return ____disposable3_3; }
	inline Il2CppObject ** get_address_of__disposable3_3() { return &____disposable3_3; }
	inline void set__disposable3_3(Il2CppObject * value)
	{
		____disposable3_3 = value;
		Il2CppCodeGenWriteBarrier(&____disposable3_3, value);
	}

	inline static int32_t get_offset_of__disposable4_4() { return static_cast<int32_t>(offsetof(Quaternary_t368822686, ____disposable4_4)); }
	inline Il2CppObject * get__disposable4_4() const { return ____disposable4_4; }
	inline Il2CppObject ** get_address_of__disposable4_4() { return &____disposable4_4; }
	inline void set__disposable4_4(Il2CppObject * value)
	{
		____disposable4_4 = value;
		Il2CppCodeGenWriteBarrier(&____disposable4_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
