﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.UUnit.StructNumFieldTest::.cctor()
extern "C"  void StructNumFieldTest__cctor_m2040586781 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct StructNumFieldTest_t2313733701;
struct StructNumFieldTest_t2313733701_marshaled_pinvoke;

extern "C" void StructNumFieldTest_t2313733701_marshal_pinvoke(const StructNumFieldTest_t2313733701& unmarshaled, StructNumFieldTest_t2313733701_marshaled_pinvoke& marshaled);
extern "C" void StructNumFieldTest_t2313733701_marshal_pinvoke_back(const StructNumFieldTest_t2313733701_marshaled_pinvoke& marshaled, StructNumFieldTest_t2313733701& unmarshaled);
extern "C" void StructNumFieldTest_t2313733701_marshal_pinvoke_cleanup(StructNumFieldTest_t2313733701_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct StructNumFieldTest_t2313733701;
struct StructNumFieldTest_t2313733701_marshaled_com;

extern "C" void StructNumFieldTest_t2313733701_marshal_com(const StructNumFieldTest_t2313733701& unmarshaled, StructNumFieldTest_t2313733701_marshaled_com& marshaled);
extern "C" void StructNumFieldTest_t2313733701_marshal_com_back(const StructNumFieldTest_t2313733701_marshaled_com& marshaled, StructNumFieldTest_t2313733701& unmarshaled);
extern "C" void StructNumFieldTest_t2313733701_marshal_com_cleanup(StructNumFieldTest_t2313733701_marshaled_com& marshaled);
