﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_EventArgs516466188.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Examples.Sample09_EventHandling/MyEventArgs
struct  MyEventArgs_t3100194347  : public EventArgs_t516466188
{
public:
	// System.Int32 UniRx.Examples.Sample09_EventHandling/MyEventArgs::<MyProperty>k__BackingField
	int32_t ___U3CMyPropertyU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CMyPropertyU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MyEventArgs_t3100194347, ___U3CMyPropertyU3Ek__BackingField_1)); }
	inline int32_t get_U3CMyPropertyU3Ek__BackingField_1() const { return ___U3CMyPropertyU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CMyPropertyU3Ek__BackingField_1() { return &___U3CMyPropertyU3Ek__BackingField_1; }
	inline void set_U3CMyPropertyU3Ek__BackingField_1(int32_t value)
	{
		___U3CMyPropertyU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
