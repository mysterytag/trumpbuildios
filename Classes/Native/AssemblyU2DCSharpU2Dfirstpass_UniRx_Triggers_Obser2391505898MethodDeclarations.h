﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Triggers.ObservableVisibleTrigger
struct ObservableVisibleTrigger_t2391505898;
// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Triggers.ObservableVisibleTrigger::.ctor()
extern "C"  void ObservableVisibleTrigger__ctor_m702503315 (ObservableVisibleTrigger_t2391505898 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableVisibleTrigger::OnBecameInvisible()
extern "C"  void ObservableVisibleTrigger_OnBecameInvisible_m2155984902 (ObservableVisibleTrigger_t2391505898 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Triggers.ObservableVisibleTrigger::OnBecameInvisibleAsObservable()
extern "C"  Il2CppObject* ObservableVisibleTrigger_OnBecameInvisibleAsObservable_m840859203 (ObservableVisibleTrigger_t2391505898 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableVisibleTrigger::OnBecameVisible()
extern "C"  void ObservableVisibleTrigger_OnBecameVisible_m3915354507 (ObservableVisibleTrigger_t2391505898 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Triggers.ObservableVisibleTrigger::OnBecameVisibleAsObservable()
extern "C"  Il2CppObject* ObservableVisibleTrigger_OnBecameVisibleAsObservable_m1510967880 (ObservableVisibleTrigger_t2391505898 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableVisibleTrigger::RaiseOnCompletedOnDestroy()
extern "C"  void ObservableVisibleTrigger_RaiseOnCompletedOnDestroy_m195209548 (ObservableVisibleTrigger_t2391505898 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
