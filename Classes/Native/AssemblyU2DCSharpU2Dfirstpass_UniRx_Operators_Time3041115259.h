﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.TimeIntervalObservable`1<System.Object>
struct TimeIntervalObservable_1_t4159975444;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1058727271.h"
#include "mscorlib_System_DateTimeOffset3712260035.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.TimeIntervalObservable`1/TimeInterval<System.Object>
struct  TimeInterval_t3041115259  : public OperatorObserverBase_2_t1058727271
{
public:
	// UniRx.Operators.TimeIntervalObservable`1<T> UniRx.Operators.TimeIntervalObservable`1/TimeInterval::parent
	TimeIntervalObservable_1_t4159975444 * ___parent_2;
	// System.DateTimeOffset UniRx.Operators.TimeIntervalObservable`1/TimeInterval::lastTime
	DateTimeOffset_t3712260035  ___lastTime_3;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(TimeInterval_t3041115259, ___parent_2)); }
	inline TimeIntervalObservable_1_t4159975444 * get_parent_2() const { return ___parent_2; }
	inline TimeIntervalObservable_1_t4159975444 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(TimeIntervalObservable_1_t4159975444 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_lastTime_3() { return static_cast<int32_t>(offsetof(TimeInterval_t3041115259, ___lastTime_3)); }
	inline DateTimeOffset_t3712260035  get_lastTime_3() const { return ___lastTime_3; }
	inline DateTimeOffset_t3712260035 * get_address_of_lastTime_3() { return &___lastTime_3; }
	inline void set_lastTime_3(DateTimeOffset_t3712260035  value)
	{
		___lastTime_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
