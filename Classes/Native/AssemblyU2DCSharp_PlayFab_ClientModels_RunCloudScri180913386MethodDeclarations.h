﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.RunCloudScriptRequest
struct RunCloudScriptRequest_t180913386;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Object837106420.h"

// System.Void PlayFab.ClientModels.RunCloudScriptRequest::.ctor()
extern "C"  void RunCloudScriptRequest__ctor_m2570744191 (RunCloudScriptRequest_t180913386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.RunCloudScriptRequest::get_ActionId()
extern "C"  String_t* RunCloudScriptRequest_get_ActionId_m873644674 (RunCloudScriptRequest_t180913386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.RunCloudScriptRequest::set_ActionId(System.String)
extern "C"  void RunCloudScriptRequest_set_ActionId_m2157676623 (RunCloudScriptRequest_t180913386 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayFab.ClientModels.RunCloudScriptRequest::get_Params()
extern "C"  Il2CppObject * RunCloudScriptRequest_get_Params_m2338083177 (RunCloudScriptRequest_t180913386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.RunCloudScriptRequest::set_Params(System.Object)
extern "C"  void RunCloudScriptRequest_set_Params_m3136186988 (RunCloudScriptRequest_t180913386 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.RunCloudScriptRequest::get_ParamsEncoded()
extern "C"  String_t* RunCloudScriptRequest_get_ParamsEncoded_m3729537497 (RunCloudScriptRequest_t180913386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.RunCloudScriptRequest::set_ParamsEncoded(System.String)
extern "C"  void RunCloudScriptRequest_set_ParamsEncoded_m3057837722 (RunCloudScriptRequest_t180913386 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
