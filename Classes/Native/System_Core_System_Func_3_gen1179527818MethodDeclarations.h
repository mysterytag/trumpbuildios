﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_3_gen881161140MethodDeclarations.h"

// System.Void System.Func`3<UniRx.Unit,System.Int32,System.Collections.Generic.IEnumerable`1<UniRx.Unit>>::.ctor(System.Object,System.IntPtr)
#define Func_3__ctor_m3973114745(__this, ___object0, ___method1, method) ((  void (*) (Func_3_t1179527818 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_3__ctor_m1267307914_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`3<UniRx.Unit,System.Int32,System.Collections.Generic.IEnumerable`1<UniRx.Unit>>::Invoke(T1,T2)
#define Func_3_Invoke_m1393333812(__this, ___arg10, ___arg21, method) ((  Il2CppObject* (*) (Func_3_t1179527818 *, Unit_t2558286038 , int32_t, const MethodInfo*))Func_3_Invoke_m2765784959_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Func`3<UniRx.Unit,System.Int32,System.Collections.Generic.IEnumerable`1<UniRx.Unit>>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Func_3_BeginInvoke_m869745849(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Func_3_t1179527818 *, Unit_t2558286038 , int32_t, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_3_BeginInvoke_m1838859904_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// TResult System.Func`3<UniRx.Unit,System.Int32,System.Collections.Generic.IEnumerable`1<UniRx.Unit>>::EndInvoke(System.IAsyncResult)
#define Func_3_EndInvoke_m1194438439(__this, ___result0, method) ((  Il2CppObject* (*) (Func_3_t1179527818 *, Il2CppObject *, const MethodInfo*))Func_3_EndInvoke_m362880316_gshared)(__this, ___result0, method)
