﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.DeferObservable`1/Defer<System.Object>
struct Defer_t3471526139;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.DeferObservable`1/Defer<System.Object>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Defer__ctor_m1777074501_gshared (Defer_t3471526139 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define Defer__ctor_m1777074501(__this, ___observer0, ___cancel1, method) ((  void (*) (Defer_t3471526139 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Defer__ctor_m1777074501_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.DeferObservable`1/Defer<System.Object>::OnNext(T)
extern "C"  void Defer_OnNext_m3829588781_gshared (Defer_t3471526139 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Defer_OnNext_m3829588781(__this, ___value0, method) ((  void (*) (Defer_t3471526139 *, Il2CppObject *, const MethodInfo*))Defer_OnNext_m3829588781_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.DeferObservable`1/Defer<System.Object>::OnError(System.Exception)
extern "C"  void Defer_OnError_m3644654652_gshared (Defer_t3471526139 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Defer_OnError_m3644654652(__this, ___error0, method) ((  void (*) (Defer_t3471526139 *, Exception_t1967233988 *, const MethodInfo*))Defer_OnError_m3644654652_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.DeferObservable`1/Defer<System.Object>::OnCompleted()
extern "C"  void Defer_OnCompleted_m3382915471_gshared (Defer_t3471526139 * __this, const MethodInfo* method);
#define Defer_OnCompleted_m3382915471(__this, method) ((  void (*) (Defer_t3471526139 *, const MethodInfo*))Defer_OnCompleted_m3382915471_gshared)(__this, method)
