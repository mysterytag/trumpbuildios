﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observer/Subscribe_`1<UniRx.Tuple`2<System.Object,System.Object>>
struct Subscribe__1_t3114194070;
// System.Action`1<System.Exception>
struct Action_1_t2115686693;
// System.Action
struct Action_t437523947;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen369261819.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Observer/Subscribe_`1<UniRx.Tuple`2<System.Object,System.Object>>::.ctor(System.Action`1<System.Exception>,System.Action)
extern "C"  void Subscribe__1__ctor_m4154005147_gshared (Subscribe__1_t3114194070 * __this, Action_1_t2115686693 * ___onError0, Action_t437523947 * ___onCompleted1, const MethodInfo* method);
#define Subscribe__1__ctor_m4154005147(__this, ___onError0, ___onCompleted1, method) ((  void (*) (Subscribe__1_t3114194070 *, Action_1_t2115686693 *, Action_t437523947 *, const MethodInfo*))Subscribe__1__ctor_m4154005147_gshared)(__this, ___onError0, ___onCompleted1, method)
// System.Void UniRx.Observer/Subscribe_`1<UniRx.Tuple`2<System.Object,System.Object>>::OnNext(T)
extern "C"  void Subscribe__1_OnNext_m3798907166_gshared (Subscribe__1_t3114194070 * __this, Tuple_2_t369261819  ___value0, const MethodInfo* method);
#define Subscribe__1_OnNext_m3798907166(__this, ___value0, method) ((  void (*) (Subscribe__1_t3114194070 *, Tuple_2_t369261819 , const MethodInfo*))Subscribe__1_OnNext_m3798907166_gshared)(__this, ___value0, method)
// System.Void UniRx.Observer/Subscribe_`1<UniRx.Tuple`2<System.Object,System.Object>>::OnError(System.Exception)
extern "C"  void Subscribe__1_OnError_m1728632877_gshared (Subscribe__1_t3114194070 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Subscribe__1_OnError_m1728632877(__this, ___error0, method) ((  void (*) (Subscribe__1_t3114194070 *, Exception_t1967233988 *, const MethodInfo*))Subscribe__1_OnError_m1728632877_gshared)(__this, ___error0, method)
// System.Void UniRx.Observer/Subscribe_`1<UniRx.Tuple`2<System.Object,System.Object>>::OnCompleted()
extern "C"  void Subscribe__1_OnCompleted_m2166400768_gshared (Subscribe__1_t3114194070 * __this, const MethodInfo* method);
#define Subscribe__1_OnCompleted_m2166400768(__this, method) ((  void (*) (Subscribe__1_t3114194070 *, const MethodInfo*))Subscribe__1_OnCompleted_m2166400768_gshared)(__this, method)
