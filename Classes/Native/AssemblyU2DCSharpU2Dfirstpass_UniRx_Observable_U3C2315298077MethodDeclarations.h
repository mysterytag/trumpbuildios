﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/<ToAsync>c__AnonStorey3A/<ToAsync>c__AnonStorey3B
struct U3CToAsyncU3Ec__AnonStorey3B_t2315298077;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Observable/<ToAsync>c__AnonStorey3A/<ToAsync>c__AnonStorey3B::.ctor()
extern "C"  void U3CToAsyncU3Ec__AnonStorey3B__ctor_m2410514707 (U3CToAsyncU3Ec__AnonStorey3B_t2315298077 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Observable/<ToAsync>c__AnonStorey3A/<ToAsync>c__AnonStorey3B::<>m__63()
extern "C"  void U3CToAsyncU3Ec__AnonStorey3B_U3CU3Em__63_m3825452697 (U3CToAsyncU3Ec__AnonStorey3B_t2315298077 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
