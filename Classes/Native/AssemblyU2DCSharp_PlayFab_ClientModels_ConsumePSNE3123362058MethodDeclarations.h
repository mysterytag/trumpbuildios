﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.ConsumePSNEntitlementsRequest
struct ConsumePSNEntitlementsRequest_t3123362058;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.ConsumePSNEntitlementsRequest::.ctor()
extern "C"  void ConsumePSNEntitlementsRequest__ctor_m1083042655 (ConsumePSNEntitlementsRequest_t3123362058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.ConsumePSNEntitlementsRequest::get_CatalogVersion()
extern "C"  String_t* ConsumePSNEntitlementsRequest_get_CatalogVersion_m4193214128 (ConsumePSNEntitlementsRequest_t3123362058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ConsumePSNEntitlementsRequest::set_CatalogVersion(System.String)
extern "C"  void ConsumePSNEntitlementsRequest_set_CatalogVersion_m3400780961 (ConsumePSNEntitlementsRequest_t3123362058 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlayFab.ClientModels.ConsumePSNEntitlementsRequest::get_ServiceLabel()
extern "C"  int32_t ConsumePSNEntitlementsRequest_get_ServiceLabel_m1902926107 (ConsumePSNEntitlementsRequest_t3123362058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ConsumePSNEntitlementsRequest::set_ServiceLabel(System.Int32)
extern "C"  void ConsumePSNEntitlementsRequest_set_ServiceLabel_m4197564114 (ConsumePSNEntitlementsRequest_t3123362058 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
