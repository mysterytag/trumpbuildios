﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1<GameAnalyticsSDK.Settings/HelpTypes>
struct Comparer_1_t3746015880;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Collections.Generic.Comparer`1<GameAnalyticsSDK.Settings/HelpTypes>::.ctor()
extern "C"  void Comparer_1__ctor_m3273347385_gshared (Comparer_1_t3746015880 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m3273347385(__this, method) ((  void (*) (Comparer_1_t3746015880 *, const MethodInfo*))Comparer_1__ctor_m3273347385_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<GameAnalyticsSDK.Settings/HelpTypes>::.cctor()
extern "C"  void Comparer_1__cctor_m2207424916_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m2207424916(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m2207424916_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<GameAnalyticsSDK.Settings/HelpTypes>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m131685478_gshared (Comparer_1_t3746015880 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m131685478(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparer_1_t3746015880 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m131685478_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<GameAnalyticsSDK.Settings/HelpTypes>::get_Default()
extern "C"  Comparer_1_t3746015880 * Comparer_1_get_Default_m4113319805_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m4113319805(__this /* static, unused */, method) ((  Comparer_1_t3746015880 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m4113319805_gshared)(__this /* static, unused */, method)
