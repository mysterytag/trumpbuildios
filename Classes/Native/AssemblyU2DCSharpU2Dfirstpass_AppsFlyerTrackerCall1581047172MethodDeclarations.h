﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AppsFlyerTrackerCallbacks
struct AppsFlyerTrackerCallbacks_t1581047172;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void AppsFlyerTrackerCallbacks::.ctor()
extern "C"  void AppsFlyerTrackerCallbacks__ctor_m1729839155 (AppsFlyerTrackerCallbacks_t1581047172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppsFlyerTrackerCallbacks::Start()
extern "C"  void AppsFlyerTrackerCallbacks_Start_m676976947 (AppsFlyerTrackerCallbacks_t1581047172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppsFlyerTrackerCallbacks::Update()
extern "C"  void AppsFlyerTrackerCallbacks_Update_m3812268346 (AppsFlyerTrackerCallbacks_t1581047172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppsFlyerTrackerCallbacks::didReceiveConversionData(System.String)
extern "C"  void AppsFlyerTrackerCallbacks_didReceiveConversionData_m1726608173 (AppsFlyerTrackerCallbacks_t1581047172 * __this, String_t* ___conversionData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppsFlyerTrackerCallbacks::didReceiveConversionDataWithError(System.String)
extern "C"  void AppsFlyerTrackerCallbacks_didReceiveConversionDataWithError_m2493028499 (AppsFlyerTrackerCallbacks_t1581047172 * __this, String_t* ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppsFlyerTrackerCallbacks::didFinishValidateReceipt(System.String)
extern "C"  void AppsFlyerTrackerCallbacks_didFinishValidateReceipt_m2188660257 (AppsFlyerTrackerCallbacks_t1581047172 * __this, String_t* ___validateResult0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppsFlyerTrackerCallbacks::didFinishValidateReceiptWithError(System.String)
extern "C"  void AppsFlyerTrackerCallbacks_didFinishValidateReceiptWithError_m2081640479 (AppsFlyerTrackerCallbacks_t1581047172 * __this, String_t* ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
