﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetPublisherDataResult
struct GetPublisherDataResult_t2154202237;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.GetPublisherDataResult::.ctor()
extern "C"  void GetPublisherDataResult__ctor_m1150730464 (GetPublisherDataResult_t2154202237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PlayFab.ClientModels.GetPublisherDataResult::get_Data()
extern "C"  Dictionary_2_t2606186806 * GetPublisherDataResult_get_Data_m3083559785 (GetPublisherDataResult_t2154202237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetPublisherDataResult::set_Data(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void GetPublisherDataResult_set_Data_m750927932 (GetPublisherDataResult_t2154202237 * __this, Dictionary_2_t2606186806 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
