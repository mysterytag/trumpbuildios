﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Notification`1/OnErrorNotification<System.Object>
struct OnErrorNotification_t36474669;
// System.Exception
struct Exception_t1967233988;
// System.Object
struct Il2CppObject;
// UniRx.Notification`1<System.Object>
struct Notification_1_t38356375;
// System.String
struct String_t;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// System.Action`1<System.Exception>
struct Action_1_t2115686693;
// System.Action
struct Action_t437523947;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_NotificationKi3324123761.h"
#include "System_Core_System_Action437523947.h"

// System.Void UniRx.Notification`1/OnErrorNotification<System.Object>::.ctor(System.Exception)
extern "C"  void OnErrorNotification__ctor_m1734167588_gshared (OnErrorNotification_t36474669 * __this, Exception_t1967233988 * ___exception0, const MethodInfo* method);
#define OnErrorNotification__ctor_m1734167588(__this, ___exception0, method) ((  void (*) (OnErrorNotification_t36474669 *, Exception_t1967233988 *, const MethodInfo*))OnErrorNotification__ctor_m1734167588_gshared)(__this, ___exception0, method)
// T UniRx.Notification`1/OnErrorNotification<System.Object>::get_Value()
extern "C"  Il2CppObject * OnErrorNotification_get_Value_m3306744921_gshared (OnErrorNotification_t36474669 * __this, const MethodInfo* method);
#define OnErrorNotification_get_Value_m3306744921(__this, method) ((  Il2CppObject * (*) (OnErrorNotification_t36474669 *, const MethodInfo*))OnErrorNotification_get_Value_m3306744921_gshared)(__this, method)
// System.Exception UniRx.Notification`1/OnErrorNotification<System.Object>::get_Exception()
extern "C"  Exception_t1967233988 * OnErrorNotification_get_Exception_m3413657613_gshared (OnErrorNotification_t36474669 * __this, const MethodInfo* method);
#define OnErrorNotification_get_Exception_m3413657613(__this, method) ((  Exception_t1967233988 * (*) (OnErrorNotification_t36474669 *, const MethodInfo*))OnErrorNotification_get_Exception_m3413657613_gshared)(__this, method)
// System.Boolean UniRx.Notification`1/OnErrorNotification<System.Object>::get_HasValue()
extern "C"  bool OnErrorNotification_get_HasValue_m3730319604_gshared (OnErrorNotification_t36474669 * __this, const MethodInfo* method);
#define OnErrorNotification_get_HasValue_m3730319604(__this, method) ((  bool (*) (OnErrorNotification_t36474669 *, const MethodInfo*))OnErrorNotification_get_HasValue_m3730319604_gshared)(__this, method)
// UniRx.NotificationKind UniRx.Notification`1/OnErrorNotification<System.Object>::get_Kind()
extern "C"  int32_t OnErrorNotification_get_Kind_m3558801887_gshared (OnErrorNotification_t36474669 * __this, const MethodInfo* method);
#define OnErrorNotification_get_Kind_m3558801887(__this, method) ((  int32_t (*) (OnErrorNotification_t36474669 *, const MethodInfo*))OnErrorNotification_get_Kind_m3558801887_gshared)(__this, method)
// System.Int32 UniRx.Notification`1/OnErrorNotification<System.Object>::GetHashCode()
extern "C"  int32_t OnErrorNotification_GetHashCode_m734850713_gshared (OnErrorNotification_t36474669 * __this, const MethodInfo* method);
#define OnErrorNotification_GetHashCode_m734850713(__this, method) ((  int32_t (*) (OnErrorNotification_t36474669 *, const MethodInfo*))OnErrorNotification_GetHashCode_m734850713_gshared)(__this, method)
// System.Boolean UniRx.Notification`1/OnErrorNotification<System.Object>::Equals(UniRx.Notification`1<T>)
extern "C"  bool OnErrorNotification_Equals_m2626472589_gshared (OnErrorNotification_t36474669 * __this, Notification_1_t38356375 * ___other0, const MethodInfo* method);
#define OnErrorNotification_Equals_m2626472589(__this, ___other0, method) ((  bool (*) (OnErrorNotification_t36474669 *, Notification_1_t38356375 *, const MethodInfo*))OnErrorNotification_Equals_m2626472589_gshared)(__this, ___other0, method)
// System.String UniRx.Notification`1/OnErrorNotification<System.Object>::ToString()
extern "C"  String_t* OnErrorNotification_ToString_m2686246201_gshared (OnErrorNotification_t36474669 * __this, const MethodInfo* method);
#define OnErrorNotification_ToString_m2686246201(__this, method) ((  String_t* (*) (OnErrorNotification_t36474669 *, const MethodInfo*))OnErrorNotification_ToString_m2686246201_gshared)(__this, method)
// System.Void UniRx.Notification`1/OnErrorNotification<System.Object>::Accept(UniRx.IObserver`1<T>)
extern "C"  void OnErrorNotification_Accept_m2048116170_gshared (OnErrorNotification_t36474669 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define OnErrorNotification_Accept_m2048116170(__this, ___observer0, method) ((  void (*) (OnErrorNotification_t36474669 *, Il2CppObject*, const MethodInfo*))OnErrorNotification_Accept_m2048116170_gshared)(__this, ___observer0, method)
// System.Void UniRx.Notification`1/OnErrorNotification<System.Object>::Accept(System.Action`1<T>,System.Action`1<System.Exception>,System.Action)
extern "C"  void OnErrorNotification_Accept_m2008980579_gshared (OnErrorNotification_t36474669 * __this, Action_1_t985559125 * ___onNext0, Action_1_t2115686693 * ___onError1, Action_t437523947 * ___onCompleted2, const MethodInfo* method);
#define OnErrorNotification_Accept_m2008980579(__this, ___onNext0, ___onError1, ___onCompleted2, method) ((  void (*) (OnErrorNotification_t36474669 *, Action_1_t985559125 *, Action_1_t2115686693 *, Action_t437523947 *, const MethodInfo*))OnErrorNotification_Accept_m2008980579_gshared)(__this, ___onNext0, ___onError1, ___onCompleted2, method)
