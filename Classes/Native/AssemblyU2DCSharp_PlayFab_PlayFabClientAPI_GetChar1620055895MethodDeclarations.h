﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetCharacterDataRequestCallback
struct GetCharacterDataRequestCallback_t1620055895;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetCharacterDataRequest
struct GetCharacterDataRequest_t572698882;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetCharacter572698882.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetCharacterDataRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetCharacterDataRequestCallback__ctor_m1190593638 (GetCharacterDataRequestCallback_t1620055895 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetCharacterDataRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetCharacterDataRequest,System.Object)
extern "C"  void GetCharacterDataRequestCallback_Invoke_m2031921707 (GetCharacterDataRequestCallback_t1620055895 * __this, String_t* ___urlPath0, int32_t ___callId1, GetCharacterDataRequest_t572698882 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetCharacterDataRequestCallback_t1620055895(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetCharacterDataRequest_t572698882 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetCharacterDataRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetCharacterDataRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetCharacterDataRequestCallback_BeginInvoke_m6879806 (GetCharacterDataRequestCallback_t1620055895 * __this, String_t* ___urlPath0, int32_t ___callId1, GetCharacterDataRequest_t572698882 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetCharacterDataRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetCharacterDataRequestCallback_EndInvoke_m2929236342 (GetCharacterDataRequestCallback_t1620055895 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
