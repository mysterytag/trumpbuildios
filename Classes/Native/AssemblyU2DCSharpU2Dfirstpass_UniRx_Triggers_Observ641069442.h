﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Subject`1<UnityEngine.EventSystems.BaseEventData>
struct Subject_1_t1190171050;
// UniRx.Subject`1<UnityEngine.EventSystems.AxisEventData>
struct Subject_1_t2490931930;
// UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData>
struct Subject_1_t848168958;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Triggers_Obser1883237895.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableEventTrigger
struct  ObservableEventTrigger_t641069442  : public ObservableTriggerBase_t1883237895
{
public:
	// UniRx.Subject`1<UnityEngine.EventSystems.BaseEventData> UniRx.Triggers.ObservableEventTrigger::onDeselect
	Subject_1_t1190171050 * ___onDeselect_8;
	// UniRx.Subject`1<UnityEngine.EventSystems.AxisEventData> UniRx.Triggers.ObservableEventTrigger::onMove
	Subject_1_t2490931930 * ___onMove_9;
	// UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableEventTrigger::onPointerDown
	Subject_1_t848168958 * ___onPointerDown_10;
	// UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableEventTrigger::onPointerEnter
	Subject_1_t848168958 * ___onPointerEnter_11;
	// UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableEventTrigger::onPointerExit
	Subject_1_t848168958 * ___onPointerExit_12;
	// UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableEventTrigger::onPointerUp
	Subject_1_t848168958 * ___onPointerUp_13;
	// UniRx.Subject`1<UnityEngine.EventSystems.BaseEventData> UniRx.Triggers.ObservableEventTrigger::onSelect
	Subject_1_t1190171050 * ___onSelect_14;
	// UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableEventTrigger::onPointerClick
	Subject_1_t848168958 * ___onPointerClick_15;
	// UniRx.Subject`1<UnityEngine.EventSystems.BaseEventData> UniRx.Triggers.ObservableEventTrigger::onSubmit
	Subject_1_t1190171050 * ___onSubmit_16;
	// UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableEventTrigger::onDrag
	Subject_1_t848168958 * ___onDrag_17;
	// UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableEventTrigger::onBeginDrag
	Subject_1_t848168958 * ___onBeginDrag_18;
	// UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableEventTrigger::onEndDrag
	Subject_1_t848168958 * ___onEndDrag_19;
	// UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableEventTrigger::onDrop
	Subject_1_t848168958 * ___onDrop_20;
	// UniRx.Subject`1<UnityEngine.EventSystems.BaseEventData> UniRx.Triggers.ObservableEventTrigger::onUpdateSelected
	Subject_1_t1190171050 * ___onUpdateSelected_21;
	// UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableEventTrigger::onInitializePotentialDrag
	Subject_1_t848168958 * ___onInitializePotentialDrag_22;
	// UniRx.Subject`1<UnityEngine.EventSystems.BaseEventData> UniRx.Triggers.ObservableEventTrigger::onCancel
	Subject_1_t1190171050 * ___onCancel_23;
	// UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableEventTrigger::onScroll
	Subject_1_t848168958 * ___onScroll_24;

public:
	inline static int32_t get_offset_of_onDeselect_8() { return static_cast<int32_t>(offsetof(ObservableEventTrigger_t641069442, ___onDeselect_8)); }
	inline Subject_1_t1190171050 * get_onDeselect_8() const { return ___onDeselect_8; }
	inline Subject_1_t1190171050 ** get_address_of_onDeselect_8() { return &___onDeselect_8; }
	inline void set_onDeselect_8(Subject_1_t1190171050 * value)
	{
		___onDeselect_8 = value;
		Il2CppCodeGenWriteBarrier(&___onDeselect_8, value);
	}

	inline static int32_t get_offset_of_onMove_9() { return static_cast<int32_t>(offsetof(ObservableEventTrigger_t641069442, ___onMove_9)); }
	inline Subject_1_t2490931930 * get_onMove_9() const { return ___onMove_9; }
	inline Subject_1_t2490931930 ** get_address_of_onMove_9() { return &___onMove_9; }
	inline void set_onMove_9(Subject_1_t2490931930 * value)
	{
		___onMove_9 = value;
		Il2CppCodeGenWriteBarrier(&___onMove_9, value);
	}

	inline static int32_t get_offset_of_onPointerDown_10() { return static_cast<int32_t>(offsetof(ObservableEventTrigger_t641069442, ___onPointerDown_10)); }
	inline Subject_1_t848168958 * get_onPointerDown_10() const { return ___onPointerDown_10; }
	inline Subject_1_t848168958 ** get_address_of_onPointerDown_10() { return &___onPointerDown_10; }
	inline void set_onPointerDown_10(Subject_1_t848168958 * value)
	{
		___onPointerDown_10 = value;
		Il2CppCodeGenWriteBarrier(&___onPointerDown_10, value);
	}

	inline static int32_t get_offset_of_onPointerEnter_11() { return static_cast<int32_t>(offsetof(ObservableEventTrigger_t641069442, ___onPointerEnter_11)); }
	inline Subject_1_t848168958 * get_onPointerEnter_11() const { return ___onPointerEnter_11; }
	inline Subject_1_t848168958 ** get_address_of_onPointerEnter_11() { return &___onPointerEnter_11; }
	inline void set_onPointerEnter_11(Subject_1_t848168958 * value)
	{
		___onPointerEnter_11 = value;
		Il2CppCodeGenWriteBarrier(&___onPointerEnter_11, value);
	}

	inline static int32_t get_offset_of_onPointerExit_12() { return static_cast<int32_t>(offsetof(ObservableEventTrigger_t641069442, ___onPointerExit_12)); }
	inline Subject_1_t848168958 * get_onPointerExit_12() const { return ___onPointerExit_12; }
	inline Subject_1_t848168958 ** get_address_of_onPointerExit_12() { return &___onPointerExit_12; }
	inline void set_onPointerExit_12(Subject_1_t848168958 * value)
	{
		___onPointerExit_12 = value;
		Il2CppCodeGenWriteBarrier(&___onPointerExit_12, value);
	}

	inline static int32_t get_offset_of_onPointerUp_13() { return static_cast<int32_t>(offsetof(ObservableEventTrigger_t641069442, ___onPointerUp_13)); }
	inline Subject_1_t848168958 * get_onPointerUp_13() const { return ___onPointerUp_13; }
	inline Subject_1_t848168958 ** get_address_of_onPointerUp_13() { return &___onPointerUp_13; }
	inline void set_onPointerUp_13(Subject_1_t848168958 * value)
	{
		___onPointerUp_13 = value;
		Il2CppCodeGenWriteBarrier(&___onPointerUp_13, value);
	}

	inline static int32_t get_offset_of_onSelect_14() { return static_cast<int32_t>(offsetof(ObservableEventTrigger_t641069442, ___onSelect_14)); }
	inline Subject_1_t1190171050 * get_onSelect_14() const { return ___onSelect_14; }
	inline Subject_1_t1190171050 ** get_address_of_onSelect_14() { return &___onSelect_14; }
	inline void set_onSelect_14(Subject_1_t1190171050 * value)
	{
		___onSelect_14 = value;
		Il2CppCodeGenWriteBarrier(&___onSelect_14, value);
	}

	inline static int32_t get_offset_of_onPointerClick_15() { return static_cast<int32_t>(offsetof(ObservableEventTrigger_t641069442, ___onPointerClick_15)); }
	inline Subject_1_t848168958 * get_onPointerClick_15() const { return ___onPointerClick_15; }
	inline Subject_1_t848168958 ** get_address_of_onPointerClick_15() { return &___onPointerClick_15; }
	inline void set_onPointerClick_15(Subject_1_t848168958 * value)
	{
		___onPointerClick_15 = value;
		Il2CppCodeGenWriteBarrier(&___onPointerClick_15, value);
	}

	inline static int32_t get_offset_of_onSubmit_16() { return static_cast<int32_t>(offsetof(ObservableEventTrigger_t641069442, ___onSubmit_16)); }
	inline Subject_1_t1190171050 * get_onSubmit_16() const { return ___onSubmit_16; }
	inline Subject_1_t1190171050 ** get_address_of_onSubmit_16() { return &___onSubmit_16; }
	inline void set_onSubmit_16(Subject_1_t1190171050 * value)
	{
		___onSubmit_16 = value;
		Il2CppCodeGenWriteBarrier(&___onSubmit_16, value);
	}

	inline static int32_t get_offset_of_onDrag_17() { return static_cast<int32_t>(offsetof(ObservableEventTrigger_t641069442, ___onDrag_17)); }
	inline Subject_1_t848168958 * get_onDrag_17() const { return ___onDrag_17; }
	inline Subject_1_t848168958 ** get_address_of_onDrag_17() { return &___onDrag_17; }
	inline void set_onDrag_17(Subject_1_t848168958 * value)
	{
		___onDrag_17 = value;
		Il2CppCodeGenWriteBarrier(&___onDrag_17, value);
	}

	inline static int32_t get_offset_of_onBeginDrag_18() { return static_cast<int32_t>(offsetof(ObservableEventTrigger_t641069442, ___onBeginDrag_18)); }
	inline Subject_1_t848168958 * get_onBeginDrag_18() const { return ___onBeginDrag_18; }
	inline Subject_1_t848168958 ** get_address_of_onBeginDrag_18() { return &___onBeginDrag_18; }
	inline void set_onBeginDrag_18(Subject_1_t848168958 * value)
	{
		___onBeginDrag_18 = value;
		Il2CppCodeGenWriteBarrier(&___onBeginDrag_18, value);
	}

	inline static int32_t get_offset_of_onEndDrag_19() { return static_cast<int32_t>(offsetof(ObservableEventTrigger_t641069442, ___onEndDrag_19)); }
	inline Subject_1_t848168958 * get_onEndDrag_19() const { return ___onEndDrag_19; }
	inline Subject_1_t848168958 ** get_address_of_onEndDrag_19() { return &___onEndDrag_19; }
	inline void set_onEndDrag_19(Subject_1_t848168958 * value)
	{
		___onEndDrag_19 = value;
		Il2CppCodeGenWriteBarrier(&___onEndDrag_19, value);
	}

	inline static int32_t get_offset_of_onDrop_20() { return static_cast<int32_t>(offsetof(ObservableEventTrigger_t641069442, ___onDrop_20)); }
	inline Subject_1_t848168958 * get_onDrop_20() const { return ___onDrop_20; }
	inline Subject_1_t848168958 ** get_address_of_onDrop_20() { return &___onDrop_20; }
	inline void set_onDrop_20(Subject_1_t848168958 * value)
	{
		___onDrop_20 = value;
		Il2CppCodeGenWriteBarrier(&___onDrop_20, value);
	}

	inline static int32_t get_offset_of_onUpdateSelected_21() { return static_cast<int32_t>(offsetof(ObservableEventTrigger_t641069442, ___onUpdateSelected_21)); }
	inline Subject_1_t1190171050 * get_onUpdateSelected_21() const { return ___onUpdateSelected_21; }
	inline Subject_1_t1190171050 ** get_address_of_onUpdateSelected_21() { return &___onUpdateSelected_21; }
	inline void set_onUpdateSelected_21(Subject_1_t1190171050 * value)
	{
		___onUpdateSelected_21 = value;
		Il2CppCodeGenWriteBarrier(&___onUpdateSelected_21, value);
	}

	inline static int32_t get_offset_of_onInitializePotentialDrag_22() { return static_cast<int32_t>(offsetof(ObservableEventTrigger_t641069442, ___onInitializePotentialDrag_22)); }
	inline Subject_1_t848168958 * get_onInitializePotentialDrag_22() const { return ___onInitializePotentialDrag_22; }
	inline Subject_1_t848168958 ** get_address_of_onInitializePotentialDrag_22() { return &___onInitializePotentialDrag_22; }
	inline void set_onInitializePotentialDrag_22(Subject_1_t848168958 * value)
	{
		___onInitializePotentialDrag_22 = value;
		Il2CppCodeGenWriteBarrier(&___onInitializePotentialDrag_22, value);
	}

	inline static int32_t get_offset_of_onCancel_23() { return static_cast<int32_t>(offsetof(ObservableEventTrigger_t641069442, ___onCancel_23)); }
	inline Subject_1_t1190171050 * get_onCancel_23() const { return ___onCancel_23; }
	inline Subject_1_t1190171050 ** get_address_of_onCancel_23() { return &___onCancel_23; }
	inline void set_onCancel_23(Subject_1_t1190171050 * value)
	{
		___onCancel_23 = value;
		Il2CppCodeGenWriteBarrier(&___onCancel_23, value);
	}

	inline static int32_t get_offset_of_onScroll_24() { return static_cast<int32_t>(offsetof(ObservableEventTrigger_t641069442, ___onScroll_24)); }
	inline Subject_1_t848168958 * get_onScroll_24() const { return ___onScroll_24; }
	inline Subject_1_t848168958 ** get_address_of_onScroll_24() { return &___onScroll_24; }
	inline void set_onScroll_24(Subject_1_t848168958 * value)
	{
		___onScroll_24 = value;
		Il2CppCodeGenWriteBarrier(&___onScroll_24, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
