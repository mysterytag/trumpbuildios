﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<System.Double>
struct U3CSubscribeU3Ec__AnonStorey5B_t132523343;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<System.Double>::.ctor()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B__ctor_m598786264_gshared (U3CSubscribeU3Ec__AnonStorey5B_t132523343 * __this, const MethodInfo* method);
#define U3CSubscribeU3Ec__AnonStorey5B__ctor_m598786264(__this, method) ((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t132523343 *, const MethodInfo*))U3CSubscribeU3Ec__AnonStorey5B__ctor_m598786264_gshared)(__this, method)
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<System.Double>::<>m__73()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m2216203645_gshared (U3CSubscribeU3Ec__AnonStorey5B_t132523343 * __this, const MethodInfo* method);
#define U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m2216203645(__this, method) ((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t132523343 *, const MethodInfo*))U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m2216203645_gshared)(__this, method)
