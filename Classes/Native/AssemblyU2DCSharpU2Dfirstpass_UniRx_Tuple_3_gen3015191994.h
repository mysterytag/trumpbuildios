﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.ICancelable
struct ICancelable_t4109686575;
// System.Action`1<UniRx.Unit>
struct Action_1_t2706738743;

#include "mscorlib_System_ValueType4014882752.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Tuple`3<UniRx.ICancelable,UniRx.Unit,System.Action`1<UniRx.Unit>>
struct  Tuple_3_t3015191994 
{
public:
	// T1 UniRx.Tuple`3::item1
	Il2CppObject * ___item1_0;
	// T2 UniRx.Tuple`3::item2
	Unit_t2558286038  ___item2_1;
	// T3 UniRx.Tuple`3::item3
	Action_1_t2706738743 * ___item3_2;

public:
	inline static int32_t get_offset_of_item1_0() { return static_cast<int32_t>(offsetof(Tuple_3_t3015191994, ___item1_0)); }
	inline Il2CppObject * get_item1_0() const { return ___item1_0; }
	inline Il2CppObject ** get_address_of_item1_0() { return &___item1_0; }
	inline void set_item1_0(Il2CppObject * value)
	{
		___item1_0 = value;
		Il2CppCodeGenWriteBarrier(&___item1_0, value);
	}

	inline static int32_t get_offset_of_item2_1() { return static_cast<int32_t>(offsetof(Tuple_3_t3015191994, ___item2_1)); }
	inline Unit_t2558286038  get_item2_1() const { return ___item2_1; }
	inline Unit_t2558286038 * get_address_of_item2_1() { return &___item2_1; }
	inline void set_item2_1(Unit_t2558286038  value)
	{
		___item2_1 = value;
	}

	inline static int32_t get_offset_of_item3_2() { return static_cast<int32_t>(offsetof(Tuple_3_t3015191994, ___item3_2)); }
	inline Action_1_t2706738743 * get_item3_2() const { return ___item3_2; }
	inline Action_1_t2706738743 ** get_address_of_item3_2() { return &___item3_2; }
	inline void set_item3_2(Action_1_t2706738743 * value)
	{
		___item3_2 = value;
		Il2CppCodeGenWriteBarrier(&___item3_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
