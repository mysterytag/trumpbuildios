﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef3541566221.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"

// System.Void System.Runtime.InteropServices.HandleRef::.ctor(System.Object,System.IntPtr)
extern "C"  void HandleRef__ctor_m3670859955 (HandleRef_t3541566221 * __this, Il2CppObject * ___wrapper0, IntPtr_t ___handle1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.HandleRef::get_Handle()
extern "C"  IntPtr_t HandleRef_get_Handle_m2336437435 (HandleRef_t3541566221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.HandleRef::ToIntPtr(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t HandleRef_ToIntPtr_m4006420033 (Il2CppObject * __this /* static, unused */, HandleRef_t3541566221  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct HandleRef_t3541566221;
struct HandleRef_t3541566221_marshaled_com;

extern "C" void HandleRef_t3541566221_marshal_com(const HandleRef_t3541566221& unmarshaled, HandleRef_t3541566221_marshaled_com& marshaled);
extern "C" void HandleRef_t3541566221_marshal_com_back(const HandleRef_t3541566221_marshaled_com& marshaled, HandleRef_t3541566221& unmarshaled);
extern "C" void HandleRef_t3541566221_marshal_com_cleanup(HandleRef_t3541566221_marshaled_com& marshaled);
