﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectManyObservable`3<System.Double,System.Double,System.Double>
struct SelectManyObservable_3_t44588665;
// UniRx.IObservable`1<System.Double>
struct IObservable_1_t293314978;
// System.Func`2<System.Double,UniRx.IObservable`1<System.Double>>
struct Func_2_t2628749132;
// System.Func`3<System.Double,System.Double,System.Double>
struct Func_3_t1933728903;
// System.Func`3<System.Double,System.Int32,UniRx.IObservable`1<System.Double>>
struct Func_3_t2454229394;
// System.Func`5<System.Double,System.Int32,System.Double,System.Int32,System.Double>
struct Func_5_t2889627055;
// System.Func`2<System.Double,System.Collections.Generic.IEnumerable`1<System.Double>>
struct Func_2_t1447137828;
// System.Func`3<System.Double,System.Int32,System.Collections.Generic.IEnumerable`1<System.Double>>
struct Func_3_t1272618090;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Double>
struct IObserver_1_t2746515517;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.SelectManyObservable`3<System.Double,System.Double,System.Double>::.ctor(UniRx.IObservable`1<TSource>,System.Func`2<TSource,UniRx.IObservable`1<TCollection>>,System.Func`3<TSource,TCollection,TResult>)
extern "C"  void SelectManyObservable_3__ctor_m3030330384_gshared (SelectManyObservable_3_t44588665 * __this, Il2CppObject* ___source0, Func_2_t2628749132 * ___collectionSelector1, Func_3_t1933728903 * ___resultSelector2, const MethodInfo* method);
#define SelectManyObservable_3__ctor_m3030330384(__this, ___source0, ___collectionSelector1, ___resultSelector2, method) ((  void (*) (SelectManyObservable_3_t44588665 *, Il2CppObject*, Func_2_t2628749132 *, Func_3_t1933728903 *, const MethodInfo*))SelectManyObservable_3__ctor_m3030330384_gshared)(__this, ___source0, ___collectionSelector1, ___resultSelector2, method)
// System.Void UniRx.Operators.SelectManyObservable`3<System.Double,System.Double,System.Double>::.ctor(UniRx.IObservable`1<TSource>,System.Func`3<TSource,System.Int32,UniRx.IObservable`1<TCollection>>,System.Func`5<TSource,System.Int32,TCollection,System.Int32,TResult>)
extern "C"  void SelectManyObservable_3__ctor_m3694731364_gshared (SelectManyObservable_3_t44588665 * __this, Il2CppObject* ___source0, Func_3_t2454229394 * ___collectionSelector1, Func_5_t2889627055 * ___resultSelector2, const MethodInfo* method);
#define SelectManyObservable_3__ctor_m3694731364(__this, ___source0, ___collectionSelector1, ___resultSelector2, method) ((  void (*) (SelectManyObservable_3_t44588665 *, Il2CppObject*, Func_3_t2454229394 *, Func_5_t2889627055 *, const MethodInfo*))SelectManyObservable_3__ctor_m3694731364_gshared)(__this, ___source0, ___collectionSelector1, ___resultSelector2, method)
// System.Void UniRx.Operators.SelectManyObservable`3<System.Double,System.Double,System.Double>::.ctor(UniRx.IObservable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TCollection>>,System.Func`3<TSource,TCollection,TResult>)
extern "C"  void SelectManyObservable_3__ctor_m3822843770_gshared (SelectManyObservable_3_t44588665 * __this, Il2CppObject* ___source0, Func_2_t1447137828 * ___collectionSelector1, Func_3_t1933728903 * ___resultSelector2, const MethodInfo* method);
#define SelectManyObservable_3__ctor_m3822843770(__this, ___source0, ___collectionSelector1, ___resultSelector2, method) ((  void (*) (SelectManyObservable_3_t44588665 *, Il2CppObject*, Func_2_t1447137828 *, Func_3_t1933728903 *, const MethodInfo*))SelectManyObservable_3__ctor_m3822843770_gshared)(__this, ___source0, ___collectionSelector1, ___resultSelector2, method)
// System.Void UniRx.Operators.SelectManyObservable`3<System.Double,System.Double,System.Double>::.ctor(UniRx.IObservable`1<TSource>,System.Func`3<TSource,System.Int32,System.Collections.Generic.IEnumerable`1<TCollection>>,System.Func`5<TSource,System.Int32,TCollection,System.Int32,TResult>)
extern "C"  void SelectManyObservable_3__ctor_m4288629010_gshared (SelectManyObservable_3_t44588665 * __this, Il2CppObject* ___source0, Func_3_t1272618090 * ___collectionSelector1, Func_5_t2889627055 * ___resultSelector2, const MethodInfo* method);
#define SelectManyObservable_3__ctor_m4288629010(__this, ___source0, ___collectionSelector1, ___resultSelector2, method) ((  void (*) (SelectManyObservable_3_t44588665 *, Il2CppObject*, Func_3_t1272618090 *, Func_5_t2889627055 *, const MethodInfo*))SelectManyObservable_3__ctor_m4288629010_gshared)(__this, ___source0, ___collectionSelector1, ___resultSelector2, method)
// System.IDisposable UniRx.Operators.SelectManyObservable`3<System.Double,System.Double,System.Double>::SubscribeCore(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  Il2CppObject * SelectManyObservable_3_SubscribeCore_m1821950702_gshared (SelectManyObservable_3_t44588665 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define SelectManyObservable_3_SubscribeCore_m1821950702(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (SelectManyObservable_3_t44588665 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SelectManyObservable_3_SubscribeCore_m1821950702_gshared)(__this, ___observer0, ___cancel1, method)
