﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.CombineLatestObservable`3/CombineLatest/LeftObserver<System.Boolean,System.Boolean,System.Object>
struct LeftObserver_t4047867234;
// UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Boolean,System.Object>
struct CombineLatest_t4145300795;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/LeftObserver<System.Boolean,System.Boolean,System.Object>::.ctor(UniRx.Operators.CombineLatestObservable`3/CombineLatest<TLeft,TRight,TResult>)
extern "C"  void LeftObserver__ctor_m1047300263_gshared (LeftObserver_t4047867234 * __this, CombineLatest_t4145300795 * ___parent0, const MethodInfo* method);
#define LeftObserver__ctor_m1047300263(__this, ___parent0, method) ((  void (*) (LeftObserver_t4047867234 *, CombineLatest_t4145300795 *, const MethodInfo*))LeftObserver__ctor_m1047300263_gshared)(__this, ___parent0, method)
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/LeftObserver<System.Boolean,System.Boolean,System.Object>::OnNext(TLeft)
extern "C"  void LeftObserver_OnNext_m3935101292_gshared (LeftObserver_t4047867234 * __this, bool ___value0, const MethodInfo* method);
#define LeftObserver_OnNext_m3935101292(__this, ___value0, method) ((  void (*) (LeftObserver_t4047867234 *, bool, const MethodInfo*))LeftObserver_OnNext_m3935101292_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/LeftObserver<System.Boolean,System.Boolean,System.Object>::OnError(System.Exception)
extern "C"  void LeftObserver_OnError_m3441466050_gshared (LeftObserver_t4047867234 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define LeftObserver_OnError_m3441466050(__this, ___error0, method) ((  void (*) (LeftObserver_t4047867234 *, Exception_t1967233988 *, const MethodInfo*))LeftObserver_OnError_m3441466050_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/LeftObserver<System.Boolean,System.Boolean,System.Object>::OnCompleted()
extern "C"  void LeftObserver_OnCompleted_m1031790869_gshared (LeftObserver_t4047867234 * __this, const MethodInfo* method);
#define LeftObserver_OnCompleted_m1031790869(__this, method) ((  void (*) (LeftObserver_t4047867234 *, const MethodInfo*))LeftObserver_OnCompleted_m1031790869_gshared)(__this, method)
