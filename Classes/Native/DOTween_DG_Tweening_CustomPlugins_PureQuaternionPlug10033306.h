﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// DG.Tweening.CustomPlugins.PureQuaternionPlugin
struct PureQuaternionPlugin_t10033306;

#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_2861657316.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.CustomPlugins.PureQuaternionPlugin
struct  PureQuaternionPlugin_t10033306  : public ABSTweenPlugin_3_t2861657316
{
public:

public:
};

struct PureQuaternionPlugin_t10033306_StaticFields
{
public:
	// DG.Tweening.CustomPlugins.PureQuaternionPlugin DG.Tweening.CustomPlugins.PureQuaternionPlugin::_plug
	PureQuaternionPlugin_t10033306 * ____plug_0;

public:
	inline static int32_t get_offset_of__plug_0() { return static_cast<int32_t>(offsetof(PureQuaternionPlugin_t10033306_StaticFields, ____plug_0)); }
	inline PureQuaternionPlugin_t10033306 * get__plug_0() const { return ____plug_0; }
	inline PureQuaternionPlugin_t10033306 ** get_address_of__plug_0() { return &____plug_0; }
	inline void set__plug_0(PureQuaternionPlugin_t10033306 * value)
	{
		____plug_0 = value;
		Il2CppCodeGenWriteBarrier(&____plug_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
