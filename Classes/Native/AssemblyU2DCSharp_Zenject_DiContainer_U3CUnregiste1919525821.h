﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Zenject.ProviderBase
struct ProviderBase_t1627494391;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DiContainer/<UnregisterProvider>c__AnonStorey186
struct  U3CUnregisterProviderU3Ec__AnonStorey186_t1919525821  : public Il2CppObject
{
public:
	// Zenject.ProviderBase Zenject.DiContainer/<UnregisterProvider>c__AnonStorey186::provider
	ProviderBase_t1627494391 * ___provider_0;

public:
	inline static int32_t get_offset_of_provider_0() { return static_cast<int32_t>(offsetof(U3CUnregisterProviderU3Ec__AnonStorey186_t1919525821, ___provider_0)); }
	inline ProviderBase_t1627494391 * get_provider_0() const { return ___provider_0; }
	inline ProviderBase_t1627494391 ** get_address_of_provider_0() { return &___provider_0; }
	inline void set_provider_0(ProviderBase_t1627494391 * value)
	{
		___provider_0 = value;
		Il2CppCodeGenWriteBarrier(&___provider_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
