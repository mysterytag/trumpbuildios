﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Func`2<System.Int64,UnityEngine.Vector2>
struct Func_2_t1270693722;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Func`2<System.Int64,UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m31193364_gshared (Func_2_t1270693722 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Func_2__ctor_m31193364(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t1270693722 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m31193364_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<System.Int64,UnityEngine.Vector2>::Invoke(T)
extern "C"  Vector2_t3525329788  Func_2_Invoke_m282123490_gshared (Func_2_t1270693722 * __this, int64_t ___arg10, const MethodInfo* method);
#define Func_2_Invoke_m282123490(__this, ___arg10, method) ((  Vector2_t3525329788  (*) (Func_2_t1270693722 *, int64_t, const MethodInfo*))Func_2_Invoke_m282123490_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<System.Int64,UnityEngine.Vector2>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_2_BeginInvoke_m1292894997_gshared (Func_2_t1270693722 * __this, int64_t ___arg10, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method);
#define Func_2_BeginInvoke_m1292894997(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t1270693722 *, int64_t, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m1292894997_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<System.Int64,UnityEngine.Vector2>::EndInvoke(System.IAsyncResult)
extern "C"  Vector2_t3525329788  Func_2_EndInvoke_m2121508914_gshared (Func_2_t1270693722 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Func_2_EndInvoke_m2121508914(__this, ___result0, method) ((  Vector2_t3525329788  (*) (Func_2_t1270693722 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m2121508914_gshared)(__this, ___result0, method)
