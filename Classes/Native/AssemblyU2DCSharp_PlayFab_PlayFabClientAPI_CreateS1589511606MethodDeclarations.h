﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/CreateSharedGroupRequestCallback
struct CreateSharedGroupRequestCallback_t1589511606;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.CreateSharedGroupRequest
struct CreateSharedGroupRequest_t972582497;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_CreateShared972582497.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/CreateSharedGroupRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void CreateSharedGroupRequestCallback__ctor_m531194789 (CreateSharedGroupRequestCallback_t1589511606 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/CreateSharedGroupRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.CreateSharedGroupRequest,System.Object)
extern "C"  void CreateSharedGroupRequestCallback_Invoke_m116638239 (CreateSharedGroupRequestCallback_t1589511606 * __this, String_t* ___urlPath0, int32_t ___callId1, CreateSharedGroupRequest_t972582497 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_CreateSharedGroupRequestCallback_t1589511606(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, CreateSharedGroupRequest_t972582497 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/CreateSharedGroupRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.CreateSharedGroupRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * CreateSharedGroupRequestCallback_BeginInvoke_m7537106 (CreateSharedGroupRequestCallback_t1589511606 * __this, String_t* ___urlPath0, int32_t ___callId1, CreateSharedGroupRequest_t972582497 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/CreateSharedGroupRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void CreateSharedGroupRequestCallback_EndInvoke_m1638035509 (CreateSharedGroupRequestCallback_t1589511606 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
