﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.MergeObservable`1/MergeOuterObserver<System.Object>
struct MergeOuterObserver_t1613081930;
// UniRx.Operators.MergeObservable`1<System.Object>
struct MergeObservable_1_t3637992042;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.MergeObservable`1/MergeOuterObserver<System.Object>::.ctor(UniRx.Operators.MergeObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void MergeOuterObserver__ctor_m3701700961_gshared (MergeOuterObserver_t1613081930 * __this, MergeObservable_1_t3637992042 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define MergeOuterObserver__ctor_m3701700961(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (MergeOuterObserver_t1613081930 *, MergeObservable_1_t3637992042 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))MergeOuterObserver__ctor_m3701700961_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.MergeObservable`1/MergeOuterObserver<System.Object>::Run()
extern "C"  Il2CppObject * MergeOuterObserver_Run_m1261133362_gshared (MergeOuterObserver_t1613081930 * __this, const MethodInfo* method);
#define MergeOuterObserver_Run_m1261133362(__this, method) ((  Il2CppObject * (*) (MergeOuterObserver_t1613081930 *, const MethodInfo*))MergeOuterObserver_Run_m1261133362_gshared)(__this, method)
// System.Void UniRx.Operators.MergeObservable`1/MergeOuterObserver<System.Object>::OnNext(UniRx.IObservable`1<T>)
extern "C"  void MergeOuterObserver_OnNext_m960514567_gshared (MergeOuterObserver_t1613081930 * __this, Il2CppObject* ___value0, const MethodInfo* method);
#define MergeOuterObserver_OnNext_m960514567(__this, ___value0, method) ((  void (*) (MergeOuterObserver_t1613081930 *, Il2CppObject*, const MethodInfo*))MergeOuterObserver_OnNext_m960514567_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.MergeObservable`1/MergeOuterObserver<System.Object>::OnError(System.Exception)
extern "C"  void MergeOuterObserver_OnError_m563103557_gshared (MergeOuterObserver_t1613081930 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define MergeOuterObserver_OnError_m563103557(__this, ___error0, method) ((  void (*) (MergeOuterObserver_t1613081930 *, Exception_t1967233988 *, const MethodInfo*))MergeOuterObserver_OnError_m563103557_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.MergeObservable`1/MergeOuterObserver<System.Object>::OnCompleted()
extern "C"  void MergeOuterObserver_OnCompleted_m3234069016_gshared (MergeOuterObserver_t1613081930 * __this, const MethodInfo* method);
#define MergeOuterObserver_OnCompleted_m3234069016(__this, method) ((  void (*) (MergeOuterObserver_t1613081930 *, const MethodInfo*))MergeOuterObserver_OnCompleted_m3234069016_gshared)(__this, method)
