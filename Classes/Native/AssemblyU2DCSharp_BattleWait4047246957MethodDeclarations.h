﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BattleWait
struct BattleWait_t4047246957;

#include "codegen/il2cpp-codegen.h"

// System.Void BattleWait::.ctor(System.Single)
extern "C"  void BattleWait__ctor_m1358779293 (BattleWait_t4047246957 * __this, float ___timeToWait0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BattleWait::get_finished()
extern "C"  bool BattleWait_get_finished_m2932388805 (BattleWait_t4047246957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BattleWait::Tick(System.Single)
extern "C"  void BattleWait_Tick_m1673087864 (BattleWait_t4047246957 * __this, float ___dt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BattleWait::Dispose()
extern "C"  void BattleWait_Dispose_m2371181963 (BattleWait_t4047246957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
