﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.FirstObservable`1/First_<UniRx.Unit>
struct First__t1485551954;
// UniRx.Operators.FirstObservable`1<UniRx.Unit>
struct FirstObservable_1_t285873156;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.FirstObservable`1/First_<UniRx.Unit>::.ctor(UniRx.Operators.FirstObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void First___ctor_m1383866883_gshared (First__t1485551954 * __this, FirstObservable_1_t285873156 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define First___ctor_m1383866883(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (First__t1485551954 *, FirstObservable_1_t285873156 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))First___ctor_m1383866883_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.FirstObservable`1/First_<UniRx.Unit>::OnNext(T)
extern "C"  void First__OnNext_m3791894240_gshared (First__t1485551954 * __this, Unit_t2558286038  ___value0, const MethodInfo* method);
#define First__OnNext_m3791894240(__this, ___value0, method) ((  void (*) (First__t1485551954 *, Unit_t2558286038 , const MethodInfo*))First__OnNext_m3791894240_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.FirstObservable`1/First_<UniRx.Unit>::OnError(System.Exception)
extern "C"  void First__OnError_m3216904687_gshared (First__t1485551954 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define First__OnError_m3216904687(__this, ___error0, method) ((  void (*) (First__t1485551954 *, Exception_t1967233988 *, const MethodInfo*))First__OnError_m3216904687_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.FirstObservable`1/First_<UniRx.Unit>::OnCompleted()
extern "C"  void First__OnCompleted_m2392650690_gshared (First__t1485551954 * __this, const MethodInfo* method);
#define First__OnCompleted_m2392650690(__this, method) ((  void (*) (First__t1485551954 *, const MethodInfo*))First__OnCompleted_m2392650690_gshared)(__this, method)
