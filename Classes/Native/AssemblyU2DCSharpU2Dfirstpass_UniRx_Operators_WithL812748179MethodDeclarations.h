﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.WithLatestFromObservable`3/WithLatestFrom/RightObserver<System.Object,System.Object,System.Object>
struct RightObserver_t812748181;
// UniRx.Operators.WithLatestFromObservable`3/WithLatestFrom<System.Object,System.Object,System.Object>
struct WithLatestFrom_t2869896360;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.WithLatestFromObservable`3/WithLatestFrom/RightObserver<System.Object,System.Object,System.Object>::.ctor(UniRx.Operators.WithLatestFromObservable`3/WithLatestFrom<TLeft,TRight,TResult>,System.IDisposable)
extern "C"  void RightObserver__ctor_m2144139916_gshared (RightObserver_t812748181 * __this, WithLatestFrom_t2869896360 * ___parent0, Il2CppObject * ___subscription1, const MethodInfo* method);
#define RightObserver__ctor_m2144139916(__this, ___parent0, ___subscription1, method) ((  void (*) (RightObserver_t812748181 *, WithLatestFrom_t2869896360 *, Il2CppObject *, const MethodInfo*))RightObserver__ctor_m2144139916_gshared)(__this, ___parent0, ___subscription1, method)
// System.Void UniRx.Operators.WithLatestFromObservable`3/WithLatestFrom/RightObserver<System.Object,System.Object,System.Object>::OnNext(TRight)
extern "C"  void RightObserver_OnNext_m66342386_gshared (RightObserver_t812748181 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define RightObserver_OnNext_m66342386(__this, ___value0, method) ((  void (*) (RightObserver_t812748181 *, Il2CppObject *, const MethodInfo*))RightObserver_OnNext_m66342386_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.WithLatestFromObservable`3/WithLatestFrom/RightObserver<System.Object,System.Object,System.Object>::OnError(System.Exception)
extern "C"  void RightObserver_OnError_m3007273939_gshared (RightObserver_t812748181 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define RightObserver_OnError_m3007273939(__this, ___error0, method) ((  void (*) (RightObserver_t812748181 *, Exception_t1967233988 *, const MethodInfo*))RightObserver_OnError_m3007273939_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.WithLatestFromObservable`3/WithLatestFrom/RightObserver<System.Object,System.Object,System.Object>::OnCompleted()
extern "C"  void RightObserver_OnCompleted_m695399334_gshared (RightObserver_t812748181 * __this, const MethodInfo* method);
#define RightObserver_OnCompleted_m695399334(__this, method) ((  void (*) (RightObserver_t812748181 *, const MethodInfo*))RightObserver_OnCompleted_m695399334_gshared)(__this, method)
