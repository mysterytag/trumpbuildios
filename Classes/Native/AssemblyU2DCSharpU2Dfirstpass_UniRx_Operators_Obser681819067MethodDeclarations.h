﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ObserveOnObservable`1/ObserveOn/<OnError>c__AnonStorey62<System.Object>
struct U3COnErrorU3Ec__AnonStorey62_t681819067;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn/<OnError>c__AnonStorey62<System.Object>::.ctor()
extern "C"  void U3COnErrorU3Ec__AnonStorey62__ctor_m2294652856_gshared (U3COnErrorU3Ec__AnonStorey62_t681819067 * __this, const MethodInfo* method);
#define U3COnErrorU3Ec__AnonStorey62__ctor_m2294652856(__this, method) ((  void (*) (U3COnErrorU3Ec__AnonStorey62_t681819067 *, const MethodInfo*))U3COnErrorU3Ec__AnonStorey62__ctor_m2294652856_gshared)(__this, method)
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn/<OnError>c__AnonStorey62<System.Object>::<>m__7F()
extern "C"  void U3COnErrorU3Ec__AnonStorey62_U3CU3Em__7F_m4151411632_gshared (U3COnErrorU3Ec__AnonStorey62_t681819067 * __this, const MethodInfo* method);
#define U3COnErrorU3Ec__AnonStorey62_U3CU3Em__7F_m4151411632(__this, method) ((  void (*) (U3COnErrorU3Ec__AnonStorey62_t681819067 *, const MethodInfo*))U3COnErrorU3Ec__AnonStorey62_U3CU3Em__7F_m4151411632_gshared)(__this, method)
