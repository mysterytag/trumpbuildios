﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.BufferObservable`1<System.Object>
struct BufferObservable_1_t574294898;
// System.Collections.Generic.Queue`1<System.Collections.Generic.List`1<System.Object>>
struct Queue_1_t3342152929;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3354260463.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.BufferObservable`1/Buffer_<System.Object>
struct  Buffer__t1281669152  : public OperatorObserverBase_2_t3354260463
{
public:
	// UniRx.Operators.BufferObservable`1<T> UniRx.Operators.BufferObservable`1/Buffer_::parent
	BufferObservable_1_t574294898 * ___parent_2;
	// System.Collections.Generic.Queue`1<System.Collections.Generic.List`1<T>> UniRx.Operators.BufferObservable`1/Buffer_::q
	Queue_1_t3342152929 * ___q_3;
	// System.Int32 UniRx.Operators.BufferObservable`1/Buffer_::index
	int32_t ___index_4;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(Buffer__t1281669152, ___parent_2)); }
	inline BufferObservable_1_t574294898 * get_parent_2() const { return ___parent_2; }
	inline BufferObservable_1_t574294898 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(BufferObservable_1_t574294898 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_q_3() { return static_cast<int32_t>(offsetof(Buffer__t1281669152, ___q_3)); }
	inline Queue_1_t3342152929 * get_q_3() const { return ___q_3; }
	inline Queue_1_t3342152929 ** get_address_of_q_3() { return &___q_3; }
	inline void set_q_3(Queue_1_t3342152929 * value)
	{
		___q_3 = value;
		Il2CppCodeGenWriteBarrier(&___q_3, value);
	}

	inline static int32_t get_offset_of_index_4() { return static_cast<int32_t>(offsetof(Buffer__t1281669152, ___index_4)); }
	inline int32_t get_index_4() const { return ___index_4; }
	inline int32_t* get_address_of_index_4() { return &___index_4; }
	inline void set_index_4(int32_t value)
	{
		___index_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
