﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.OperatorObservableBase`1<UnityEngine.Color>
struct OperatorObservableBase_1_t652320731;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UnityEngine.Color>
struct IObserver_1_t3800174663;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.OperatorObservableBase`1<UnityEngine.Color>::.ctor(System.Boolean)
extern "C"  void OperatorObservableBase_1__ctor_m3365105728_gshared (OperatorObservableBase_1_t652320731 * __this, bool ___isRequiredSubscribeOnCurrentThread0, const MethodInfo* method);
#define OperatorObservableBase_1__ctor_m3365105728(__this, ___isRequiredSubscribeOnCurrentThread0, method) ((  void (*) (OperatorObservableBase_1_t652320731 *, bool, const MethodInfo*))OperatorObservableBase_1__ctor_m3365105728_gshared)(__this, ___isRequiredSubscribeOnCurrentThread0, method)
// System.Boolean UniRx.Operators.OperatorObservableBase`1<UnityEngine.Color>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m3379150818_gshared (OperatorObservableBase_1_t652320731 * __this, const MethodInfo* method);
#define OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m3379150818(__this, method) ((  bool (*) (OperatorObservableBase_1_t652320731 *, const MethodInfo*))OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m3379150818_gshared)(__this, method)
// System.IDisposable UniRx.Operators.OperatorObservableBase`1<UnityEngine.Color>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * OperatorObservableBase_1_Subscribe_m1120264414_gshared (OperatorObservableBase_1_t652320731 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define OperatorObservableBase_1_Subscribe_m1120264414(__this, ___observer0, method) ((  Il2CppObject * (*) (OperatorObservableBase_1_t652320731 *, Il2CppObject*, const MethodInfo*))OperatorObservableBase_1_Subscribe_m1120264414_gshared)(__this, ___observer0, method)
