﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnonymousCell`1<System.Double>
struct AnonymousCell_1_t4095970411;
// System.Func`3<System.Action`1<System.Double>,Priority,System.IDisposable>
struct Func_3_t892795874;
// System.Func`1<System.Double>
struct Func_1_t1677297861;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<System.Double>
struct Action_1_t682969319;
// UniRx.IObserver`1<System.Double>
struct IObserver_1_t2746515517;
// System.Action
struct Action_t437523947;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"
#include "System_Core_System_Action437523947.h"

// System.Void AnonymousCell`1<System.Double>::.ctor()
extern "C"  void AnonymousCell_1__ctor_m4270617137_gshared (AnonymousCell_1_t4095970411 * __this, const MethodInfo* method);
#define AnonymousCell_1__ctor_m4270617137(__this, method) ((  void (*) (AnonymousCell_1_t4095970411 *, const MethodInfo*))AnonymousCell_1__ctor_m4270617137_gshared)(__this, method)
// System.Void AnonymousCell`1<System.Double>::.ctor(System.Func`3<System.Action`1<T>,Priority,System.IDisposable>,System.Func`1<T>)
extern "C"  void AnonymousCell_1__ctor_m521802405_gshared (AnonymousCell_1_t4095970411 * __this, Func_3_t892795874 * ___subscribe0, Func_1_t1677297861 * ___current1, const MethodInfo* method);
#define AnonymousCell_1__ctor_m521802405(__this, ___subscribe0, ___current1, method) ((  void (*) (AnonymousCell_1_t4095970411 *, Func_3_t892795874 *, Func_1_t1677297861 *, const MethodInfo*))AnonymousCell_1__ctor_m521802405_gshared)(__this, ___subscribe0, ___current1, method)
// T AnonymousCell`1<System.Double>::get_value()
extern "C"  double AnonymousCell_1_get_value_m1409289432_gshared (AnonymousCell_1_t4095970411 * __this, const MethodInfo* method);
#define AnonymousCell_1_get_value_m1409289432(__this, method) ((  double (*) (AnonymousCell_1_t4095970411 *, const MethodInfo*))AnonymousCell_1_get_value_m1409289432_gshared)(__this, method)
// System.IDisposable AnonymousCell`1<System.Double>::ListenUpdates(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * AnonymousCell_1_ListenUpdates_m1966092355_gshared (AnonymousCell_1_t4095970411 * __this, Action_1_t682969319 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define AnonymousCell_1_ListenUpdates_m1966092355(__this, ___reaction0, ___p1, method) ((  Il2CppObject * (*) (AnonymousCell_1_t4095970411 *, Action_1_t682969319 *, int32_t, const MethodInfo*))AnonymousCell_1_ListenUpdates_m1966092355_gshared)(__this, ___reaction0, ___p1, method)
// System.IDisposable AnonymousCell`1<System.Double>::Bind(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * AnonymousCell_1_Bind_m3152967327_gshared (AnonymousCell_1_t4095970411 * __this, Action_1_t682969319 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define AnonymousCell_1_Bind_m3152967327(__this, ___reaction0, ___p1, method) ((  Il2CppObject * (*) (AnonymousCell_1_t4095970411 *, Action_1_t682969319 *, int32_t, const MethodInfo*))AnonymousCell_1_Bind_m3152967327_gshared)(__this, ___reaction0, ___p1, method)
// System.IDisposable AnonymousCell`1<System.Double>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * AnonymousCell_1_Subscribe_m3848338200_gshared (AnonymousCell_1_t4095970411 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define AnonymousCell_1_Subscribe_m3848338200(__this, ___observer0, method) ((  Il2CppObject * (*) (AnonymousCell_1_t4095970411 *, Il2CppObject*, const MethodInfo*))AnonymousCell_1_Subscribe_m3848338200_gshared)(__this, ___observer0, method)
// System.IDisposable AnonymousCell`1<System.Double>::Listen(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * AnonymousCell_1_Listen_m2937579157_gshared (AnonymousCell_1_t4095970411 * __this, Action_1_t682969319 * ___reaction0, int32_t ___priority1, const MethodInfo* method);
#define AnonymousCell_1_Listen_m2937579157(__this, ___reaction0, ___priority1, method) ((  Il2CppObject * (*) (AnonymousCell_1_t4095970411 *, Action_1_t682969319 *, int32_t, const MethodInfo*))AnonymousCell_1_Listen_m2937579157_gshared)(__this, ___reaction0, ___priority1, method)
// System.IDisposable AnonymousCell`1<System.Double>::Listen(System.Action,Priority)
extern "C"  Il2CppObject * AnonymousCell_1_Listen_m2745078130_gshared (AnonymousCell_1_t4095970411 * __this, Action_t437523947 * ___action0, int32_t ___priority1, const MethodInfo* method);
#define AnonymousCell_1_Listen_m2745078130(__this, ___action0, ___priority1, method) ((  Il2CppObject * (*) (AnonymousCell_1_t4095970411 *, Action_t437523947 *, int32_t, const MethodInfo*))AnonymousCell_1_Listen_m2745078130_gshared)(__this, ___action0, ___priority1, method)
// System.IDisposable AnonymousCell`1<System.Double>::OnChanged(System.Action,Priority)
extern "C"  Il2CppObject * AnonymousCell_1_OnChanged_m3721869654_gshared (AnonymousCell_1_t4095970411 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method);
#define AnonymousCell_1_OnChanged_m3721869654(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (AnonymousCell_1_t4095970411 *, Action_t437523947 *, int32_t, const MethodInfo*))AnonymousCell_1_OnChanged_m3721869654_gshared)(__this, ___action0, ___p1, method)
// System.Object AnonymousCell`1<System.Double>::get_valueObject()
extern "C"  Il2CppObject * AnonymousCell_1_get_valueObject_m3207412929_gshared (AnonymousCell_1_t4095970411 * __this, const MethodInfo* method);
#define AnonymousCell_1_get_valueObject_m3207412929(__this, method) ((  Il2CppObject * (*) (AnonymousCell_1_t4095970411 *, const MethodInfo*))AnonymousCell_1_get_valueObject_m3207412929_gshared)(__this, method)
