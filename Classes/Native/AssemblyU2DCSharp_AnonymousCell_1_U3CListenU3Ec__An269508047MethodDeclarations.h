﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnonymousCell`1/<Listen>c__AnonStoreyF9<System.Int32>
struct U3CListenU3Ec__AnonStoreyF9_t269508047;

#include "codegen/il2cpp-codegen.h"

// System.Void AnonymousCell`1/<Listen>c__AnonStoreyF9<System.Int32>::.ctor()
extern "C"  void U3CListenU3Ec__AnonStoreyF9__ctor_m4004770184_gshared (U3CListenU3Ec__AnonStoreyF9_t269508047 * __this, const MethodInfo* method);
#define U3CListenU3Ec__AnonStoreyF9__ctor_m4004770184(__this, method) ((  void (*) (U3CListenU3Ec__AnonStoreyF9_t269508047 *, const MethodInfo*))U3CListenU3Ec__AnonStoreyF9__ctor_m4004770184_gshared)(__this, method)
// System.Void AnonymousCell`1/<Listen>c__AnonStoreyF9<System.Int32>::<>m__16F(T)
extern "C"  void U3CListenU3Ec__AnonStoreyF9_U3CU3Em__16F_m366960492_gshared (U3CListenU3Ec__AnonStoreyF9_t269508047 * __this, int32_t ____0, const MethodInfo* method);
#define U3CListenU3Ec__AnonStoreyF9_U3CU3Em__16F_m366960492(__this, ____0, method) ((  void (*) (U3CListenU3Ec__AnonStoreyF9_t269508047 *, int32_t, const MethodInfo*))U3CListenU3Ec__AnonStoreyF9_U3CU3Em__16F_m366960492_gshared)(__this, ____0, method)
