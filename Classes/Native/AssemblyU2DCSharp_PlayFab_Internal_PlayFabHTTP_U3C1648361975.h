﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t58506160;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;
// UnityEngine.WWW
struct WWW_t1522972100;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.Internal.PlayFabHTTP/<MakeRequestViaUnity>c__Iterator6
struct  U3CMakeRequestViaUnityU3Ec__Iterator6_t1648361975  : public Il2CppObject
{
public:
	// PlayFab.CallRequestContainer PlayFab.Internal.PlayFabHTTP/<MakeRequestViaUnity>c__Iterator6::requestContainer
	CallRequestContainer_t432318609 * ___requestContainer_0;
	// System.String PlayFab.Internal.PlayFabHTTP/<MakeRequestViaUnity>c__Iterator6::<fullUrl>__0
	String_t* ___U3CfullUrlU3E__0_1;
	// System.Byte[] PlayFab.Internal.PlayFabHTTP/<MakeRequestViaUnity>c__Iterator6::<bData>__1
	ByteU5BU5D_t58506160* ___U3CbDataU3E__1_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> PlayFab.Internal.PlayFabHTTP/<MakeRequestViaUnity>c__Iterator6::<headers>__2
	Dictionary_2_t2606186806 * ___U3CheadersU3E__2_3;
	// UnityEngine.WWW PlayFab.Internal.PlayFabHTTP/<MakeRequestViaUnity>c__Iterator6::<www>__3
	WWW_t1522972100 * ___U3CwwwU3E__3_4;
	// System.Int32 PlayFab.Internal.PlayFabHTTP/<MakeRequestViaUnity>c__Iterator6::$PC
	int32_t ___U24PC_5;
	// System.Object PlayFab.Internal.PlayFabHTTP/<MakeRequestViaUnity>c__Iterator6::$current
	Il2CppObject * ___U24current_6;
	// PlayFab.CallRequestContainer PlayFab.Internal.PlayFabHTTP/<MakeRequestViaUnity>c__Iterator6::<$>requestContainer
	CallRequestContainer_t432318609 * ___U3CU24U3ErequestContainer_7;

public:
	inline static int32_t get_offset_of_requestContainer_0() { return static_cast<int32_t>(offsetof(U3CMakeRequestViaUnityU3Ec__Iterator6_t1648361975, ___requestContainer_0)); }
	inline CallRequestContainer_t432318609 * get_requestContainer_0() const { return ___requestContainer_0; }
	inline CallRequestContainer_t432318609 ** get_address_of_requestContainer_0() { return &___requestContainer_0; }
	inline void set_requestContainer_0(CallRequestContainer_t432318609 * value)
	{
		___requestContainer_0 = value;
		Il2CppCodeGenWriteBarrier(&___requestContainer_0, value);
	}

	inline static int32_t get_offset_of_U3CfullUrlU3E__0_1() { return static_cast<int32_t>(offsetof(U3CMakeRequestViaUnityU3Ec__Iterator6_t1648361975, ___U3CfullUrlU3E__0_1)); }
	inline String_t* get_U3CfullUrlU3E__0_1() const { return ___U3CfullUrlU3E__0_1; }
	inline String_t** get_address_of_U3CfullUrlU3E__0_1() { return &___U3CfullUrlU3E__0_1; }
	inline void set_U3CfullUrlU3E__0_1(String_t* value)
	{
		___U3CfullUrlU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CfullUrlU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U3CbDataU3E__1_2() { return static_cast<int32_t>(offsetof(U3CMakeRequestViaUnityU3Ec__Iterator6_t1648361975, ___U3CbDataU3E__1_2)); }
	inline ByteU5BU5D_t58506160* get_U3CbDataU3E__1_2() const { return ___U3CbDataU3E__1_2; }
	inline ByteU5BU5D_t58506160** get_address_of_U3CbDataU3E__1_2() { return &___U3CbDataU3E__1_2; }
	inline void set_U3CbDataU3E__1_2(ByteU5BU5D_t58506160* value)
	{
		___U3CbDataU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CbDataU3E__1_2, value);
	}

	inline static int32_t get_offset_of_U3CheadersU3E__2_3() { return static_cast<int32_t>(offsetof(U3CMakeRequestViaUnityU3Ec__Iterator6_t1648361975, ___U3CheadersU3E__2_3)); }
	inline Dictionary_2_t2606186806 * get_U3CheadersU3E__2_3() const { return ___U3CheadersU3E__2_3; }
	inline Dictionary_2_t2606186806 ** get_address_of_U3CheadersU3E__2_3() { return &___U3CheadersU3E__2_3; }
	inline void set_U3CheadersU3E__2_3(Dictionary_2_t2606186806 * value)
	{
		___U3CheadersU3E__2_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CheadersU3E__2_3, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__3_4() { return static_cast<int32_t>(offsetof(U3CMakeRequestViaUnityU3Ec__Iterator6_t1648361975, ___U3CwwwU3E__3_4)); }
	inline WWW_t1522972100 * get_U3CwwwU3E__3_4() const { return ___U3CwwwU3E__3_4; }
	inline WWW_t1522972100 ** get_address_of_U3CwwwU3E__3_4() { return &___U3CwwwU3E__3_4; }
	inline void set_U3CwwwU3E__3_4(WWW_t1522972100 * value)
	{
		___U3CwwwU3E__3_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__3_4, value);
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CMakeRequestViaUnityU3Ec__Iterator6_t1648361975, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CMakeRequestViaUnityU3Ec__Iterator6_t1648361975, ___U24current_6)); }
	inline Il2CppObject * get_U24current_6() const { return ___U24current_6; }
	inline Il2CppObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(Il2CppObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U3CU24U3ErequestContainer_7() { return static_cast<int32_t>(offsetof(U3CMakeRequestViaUnityU3Ec__Iterator6_t1648361975, ___U3CU24U3ErequestContainer_7)); }
	inline CallRequestContainer_t432318609 * get_U3CU24U3ErequestContainer_7() const { return ___U3CU24U3ErequestContainer_7; }
	inline CallRequestContainer_t432318609 ** get_address_of_U3CU24U3ErequestContainer_7() { return &___U3CU24U3ErequestContainer_7; }
	inline void set_U3CU24U3ErequestContainer_7(CallRequestContainer_t432318609 * value)
	{
		___U3CU24U3ErequestContainer_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3ErequestContainer_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
