﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<LinkPSNAccount>c__AnonStorey7C
struct U3CLinkPSNAccountU3Ec__AnonStorey7C_t2389557901;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<LinkPSNAccount>c__AnonStorey7C::.ctor()
extern "C"  void U3CLinkPSNAccountU3Ec__AnonStorey7C__ctor_m3387761606 (U3CLinkPSNAccountU3Ec__AnonStorey7C_t2389557901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<LinkPSNAccount>c__AnonStorey7C::<>m__69(PlayFab.CallRequestContainer)
extern "C"  void U3CLinkPSNAccountU3Ec__AnonStorey7C_U3CU3Em__69_m3822445479 (U3CLinkPSNAccountU3Ec__AnonStorey7C_t2389557901 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
