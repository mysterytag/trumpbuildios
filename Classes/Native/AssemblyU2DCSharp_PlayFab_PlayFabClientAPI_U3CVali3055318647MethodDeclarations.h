﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<ValidateIOSReceipt>c__AnonStoreyB3
struct U3CValidateIOSReceiptU3Ec__AnonStoreyB3_t3055318647;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<ValidateIOSReceipt>c__AnonStoreyB3::.ctor()
extern "C"  void U3CValidateIOSReceiptU3Ec__AnonStoreyB3__ctor_m2447253276 (U3CValidateIOSReceiptU3Ec__AnonStoreyB3_t3055318647 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<ValidateIOSReceipt>c__AnonStoreyB3::<>m__A0(PlayFab.CallRequestContainer)
extern "C"  void U3CValidateIOSReceiptU3Ec__AnonStoreyB3_U3CU3Em__A0_m2285148361 (U3CValidateIOSReceiptU3Ec__AnonStoreyB3_t3055318647 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
