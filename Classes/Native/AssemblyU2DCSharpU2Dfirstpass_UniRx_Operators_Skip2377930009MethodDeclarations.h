﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SkipObservable`1/Skip_<System.Object>
struct Skip__t2377930009;
// UniRx.Operators.SkipObservable`1<System.Object>
struct SkipObservable_1_t823380185;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SkipObservable`1/Skip_<System.Object>::.ctor(UniRx.Operators.SkipObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Skip___ctor_m2222128644_gshared (Skip__t2377930009 * __this, SkipObservable_1_t823380185 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Skip___ctor_m2222128644(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Skip__t2377930009 *, SkipObservable_1_t823380185 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Skip___ctor_m2222128644_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.SkipObservable`1/Skip_<System.Object>::Run()
extern "C"  Il2CppObject * Skip__Run_m2515609146_gshared (Skip__t2377930009 * __this, const MethodInfo* method);
#define Skip__Run_m2515609146(__this, method) ((  Il2CppObject * (*) (Skip__t2377930009 *, const MethodInfo*))Skip__Run_m2515609146_gshared)(__this, method)
// System.Void UniRx.Operators.SkipObservable`1/Skip_<System.Object>::Tick()
extern "C"  void Skip__Tick_m2960760075_gshared (Skip__t2377930009 * __this, const MethodInfo* method);
#define Skip__Tick_m2960760075(__this, method) ((  void (*) (Skip__t2377930009 *, const MethodInfo*))Skip__Tick_m2960760075_gshared)(__this, method)
// System.Void UniRx.Operators.SkipObservable`1/Skip_<System.Object>::OnNext(T)
extern "C"  void Skip__OnNext_m1573132542_gshared (Skip__t2377930009 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Skip__OnNext_m1573132542(__this, ___value0, method) ((  void (*) (Skip__t2377930009 *, Il2CppObject *, const MethodInfo*))Skip__OnNext_m1573132542_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SkipObservable`1/Skip_<System.Object>::OnError(System.Exception)
extern "C"  void Skip__OnError_m912422925_gshared (Skip__t2377930009 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Skip__OnError_m912422925(__this, ___error0, method) ((  void (*) (Skip__t2377930009 *, Exception_t1967233988 *, const MethodInfo*))Skip__OnError_m912422925_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SkipObservable`1/Skip_<System.Object>::OnCompleted()
extern "C"  void Skip__OnCompleted_m2432898784_gshared (Skip__t2377930009 * __this, const MethodInfo* method);
#define Skip__OnCompleted_m2432898784(__this, method) ((  void (*) (Skip__t2377930009 *, const MethodInfo*))Skip__OnCompleted_m2432898784_gshared)(__this, method)
