﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_FrameCountType167759182.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/<NextFrame>c__AnonStorey52
struct  U3CNextFrameU3Ec__AnonStorey52_t2543931332  : public Il2CppObject
{
public:
	// UniRx.FrameCountType UniRx.Observable/<NextFrame>c__AnonStorey52::frameCountType
	int32_t ___frameCountType_0;

public:
	inline static int32_t get_offset_of_frameCountType_0() { return static_cast<int32_t>(offsetof(U3CNextFrameU3Ec__AnonStorey52_t2543931332, ___frameCountType_0)); }
	inline int32_t get_frameCountType_0() const { return ___frameCountType_0; }
	inline int32_t* get_address_of_frameCountType_0() { return &___frameCountType_0; }
	inline void set_frameCountType_0(int32_t value)
	{
		___frameCountType_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
