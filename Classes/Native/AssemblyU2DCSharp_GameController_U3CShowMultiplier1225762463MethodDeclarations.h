﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameController/<ShowMultiplierAnimation>c__Iterator13
struct U3CShowMultiplierAnimationU3Ec__Iterator13_t1225762463;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GameController/<ShowMultiplierAnimation>c__Iterator13::.ctor()
extern "C"  void U3CShowMultiplierAnimationU3Ec__Iterator13__ctor_m1545792235 (U3CShowMultiplierAnimationU3Ec__Iterator13_t1225762463 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameController/<ShowMultiplierAnimation>c__Iterator13::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CShowMultiplierAnimationU3Ec__Iterator13_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1801147143 (U3CShowMultiplierAnimationU3Ec__Iterator13_t1225762463 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameController/<ShowMultiplierAnimation>c__Iterator13::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CShowMultiplierAnimationU3Ec__Iterator13_System_Collections_IEnumerator_get_Current_m579486875 (U3CShowMultiplierAnimationU3Ec__Iterator13_t1225762463 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameController/<ShowMultiplierAnimation>c__Iterator13::MoveNext()
extern "C"  bool U3CShowMultiplierAnimationU3Ec__Iterator13_MoveNext_m3703563817 (U3CShowMultiplierAnimationU3Ec__Iterator13_t1225762463 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController/<ShowMultiplierAnimation>c__Iterator13::Dispose()
extern "C"  void U3CShowMultiplierAnimationU3Ec__Iterator13_Dispose_m3640903784 (U3CShowMultiplierAnimationU3Ec__Iterator13_t1225762463 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController/<ShowMultiplierAnimation>c__Iterator13::Reset()
extern "C"  void U3CShowMultiplierAnimationU3Ec__Iterator13_Reset_m3487192472 (U3CShowMultiplierAnimationU3Ec__Iterator13_t1225762463 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
