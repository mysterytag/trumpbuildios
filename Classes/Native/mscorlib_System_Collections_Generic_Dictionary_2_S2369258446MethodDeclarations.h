﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>
struct ShimEnumerator_t2369258446;
// System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>
struct Dictionary_2_t938533758;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry130027246.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m945476012_gshared (ShimEnumerator_t2369258446 * __this, Dictionary_2_t938533758 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m945476012(__this, ___host0, method) ((  void (*) (ShimEnumerator_t2369258446 *, Dictionary_2_t938533758 *, const MethodInfo*))ShimEnumerator__ctor_m945476012_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m1414080313_gshared (ShimEnumerator_t2369258446 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m1414080313(__this, method) ((  bool (*) (ShimEnumerator_t2369258446 *, const MethodInfo*))ShimEnumerator_MoveNext_m1414080313_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::get_Entry()
extern "C"  DictionaryEntry_t130027246  ShimEnumerator_get_Entry_m2406006353_gshared (ShimEnumerator_t2369258446 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m2406006353(__this, method) ((  DictionaryEntry_t130027246  (*) (ShimEnumerator_t2369258446 *, const MethodInfo*))ShimEnumerator_get_Entry_m2406006353_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m2211748944_gshared (ShimEnumerator_t2369258446 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m2211748944(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t2369258446 *, const MethodInfo*))ShimEnumerator_get_Key_m2211748944_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m530360034_gshared (ShimEnumerator_t2369258446 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m530360034(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t2369258446 *, const MethodInfo*))ShimEnumerator_get_Value_m530360034_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m4294542570_gshared (ShimEnumerator_t2369258446 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m4294542570(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t2369258446 *, const MethodInfo*))ShimEnumerator_get_Current_m4294542570_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m580495870_gshared (ShimEnumerator_t2369258446 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m580495870(__this, method) ((  void (*) (ShimEnumerator_t2369258446 *, const MethodInfo*))ShimEnumerator_Reset_m580495870_gshared)(__this, method)
