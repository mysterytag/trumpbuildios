﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UnityEngine.Color>
struct U3CSubscribeU3Ec__AnonStorey5B_t1186182489;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UnityEngine.Color>::.ctor()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B__ctor_m1328540788_gshared (U3CSubscribeU3Ec__AnonStorey5B_t1186182489 * __this, const MethodInfo* method);
#define U3CSubscribeU3Ec__AnonStorey5B__ctor_m1328540788(__this, method) ((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t1186182489 *, const MethodInfo*))U3CSubscribeU3Ec__AnonStorey5B__ctor_m1328540788_gshared)(__this, method)
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UnityEngine.Color>::<>m__73()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m3430631961_gshared (U3CSubscribeU3Ec__AnonStorey5B_t1186182489 * __this, const MethodInfo* method);
#define U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m3430631961(__this, method) ((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t1186182489 *, const MethodInfo*))U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m3430631961_gshared)(__this, method)
