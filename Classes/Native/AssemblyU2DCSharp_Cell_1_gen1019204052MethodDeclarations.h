﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Cell`1<System.Object>
struct Cell_1_t1019204052;
// System.Object
struct Il2CppObject;
// ICell`1<System.Object>
struct ICell_1_t2388737397;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action
struct Action_t437523947;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// IStream`1<System.Object>
struct IStream_1_t1389797411;
// UnityEngine.UI.Text
struct Text_t3286458198;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "System_Core_System_Action437523947.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"
#include "UnityEngine_UI_UnityEngine_UI_Text3286458198.h"

// System.Void Cell`1<System.Object>::.ctor()
extern "C"  void Cell_1__ctor_m1701983586_gshared (Cell_1_t1019204052 * __this, const MethodInfo* method);
#define Cell_1__ctor_m1701983586(__this, method) ((  void (*) (Cell_1_t1019204052 *, const MethodInfo*))Cell_1__ctor_m1701983586_gshared)(__this, method)
// System.Void Cell`1<System.Object>::.ctor(T)
extern "C"  void Cell_1__ctor_m1221884988_gshared (Cell_1_t1019204052 * __this, Il2CppObject * ___initial0, const MethodInfo* method);
#define Cell_1__ctor_m1221884988(__this, ___initial0, method) ((  void (*) (Cell_1_t1019204052 *, Il2CppObject *, const MethodInfo*))Cell_1__ctor_m1221884988_gshared)(__this, ___initial0, method)
// System.Void Cell`1<System.Object>::.ctor(ICell`1<T>)
extern "C"  void Cell_1__ctor_m3977204632_gshared (Cell_1_t1019204052 * __this, Il2CppObject* ___other0, const MethodInfo* method);
#define Cell_1__ctor_m3977204632(__this, ___other0, method) ((  void (*) (Cell_1_t1019204052 *, Il2CppObject*, const MethodInfo*))Cell_1__ctor_m3977204632_gshared)(__this, ___other0, method)
// System.Void Cell`1<System.Object>::SetInputCell(ICell`1<T>)
extern "C"  void Cell_1_SetInputCell_m3952243682_gshared (Cell_1_t1019204052 * __this, Il2CppObject* ___cell0, const MethodInfo* method);
#define Cell_1_SetInputCell_m3952243682(__this, ___cell0, method) ((  void (*) (Cell_1_t1019204052 *, Il2CppObject*, const MethodInfo*))Cell_1_SetInputCell_m3952243682_gshared)(__this, ___cell0, method)
// T Cell`1<System.Object>::get_value()
extern "C"  Il2CppObject * Cell_1_get_value_m916594727_gshared (Cell_1_t1019204052 * __this, const MethodInfo* method);
#define Cell_1_get_value_m916594727(__this, method) ((  Il2CppObject * (*) (Cell_1_t1019204052 *, const MethodInfo*))Cell_1_get_value_m916594727_gshared)(__this, method)
// System.Void Cell`1<System.Object>::set_value(T)
extern "C"  void Cell_1_set_value_m570539626_gshared (Cell_1_t1019204052 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Cell_1_set_value_m570539626(__this, ___value0, method) ((  void (*) (Cell_1_t1019204052 *, Il2CppObject *, const MethodInfo*))Cell_1_set_value_m570539626_gshared)(__this, ___value0, method)
// System.Void Cell`1<System.Object>::Set(T)
extern "C"  void Cell_1_Set_m1115959164_gshared (Cell_1_t1019204052 * __this, Il2CppObject * ___val0, const MethodInfo* method);
#define Cell_1_Set_m1115959164(__this, ___val0, method) ((  void (*) (Cell_1_t1019204052 *, Il2CppObject *, const MethodInfo*))Cell_1_Set_m1115959164_gshared)(__this, ___val0, method)
// System.IDisposable Cell`1<System.Object>::OnChanged(System.Action,Priority)
extern "C"  Il2CppObject * Cell_1_OnChanged_m3781809805_gshared (Cell_1_t1019204052 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method);
#define Cell_1_OnChanged_m3781809805(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (Cell_1_t1019204052 *, Action_t437523947 *, int32_t, const MethodInfo*))Cell_1_OnChanged_m3781809805_gshared)(__this, ___action0, ___p1, method)
// System.Object Cell`1<System.Object>::get_valueObject()
extern "C"  Il2CppObject * Cell_1_get_valueObject_m2686689916_gshared (Cell_1_t1019204052 * __this, const MethodInfo* method);
#define Cell_1_get_valueObject_m2686689916(__this, method) ((  Il2CppObject * (*) (Cell_1_t1019204052 *, const MethodInfo*))Cell_1_get_valueObject_m2686689916_gshared)(__this, method)
// System.IDisposable Cell`1<System.Object>::ListenUpdates(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Cell_1_ListenUpdates_m3691348204_gshared (Cell_1_t1019204052 * __this, Action_1_t985559125 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define Cell_1_ListenUpdates_m3691348204(__this, ___reaction0, ___p1, method) ((  Il2CppObject * (*) (Cell_1_t1019204052 *, Action_1_t985559125 *, int32_t, const MethodInfo*))Cell_1_ListenUpdates_m3691348204_gshared)(__this, ___reaction0, ___p1, method)
// IStream`1<T> Cell`1<System.Object>::get_updates()
extern "C"  Il2CppObject* Cell_1_get_updates_m786418834_gshared (Cell_1_t1019204052 * __this, const MethodInfo* method);
#define Cell_1_get_updates_m786418834(__this, method) ((  Il2CppObject* (*) (Cell_1_t1019204052 *, const MethodInfo*))Cell_1_get_updates_m786418834_gshared)(__this, method)
// System.IDisposable Cell`1<System.Object>::Bind(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Cell_1_Bind_m3212907478_gshared (Cell_1_t1019204052 * __this, Action_1_t985559125 * ___action0, int32_t ___p1, const MethodInfo* method);
#define Cell_1_Bind_m3212907478(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (Cell_1_t1019204052 *, Action_1_t985559125 *, int32_t, const MethodInfo*))Cell_1_Bind_m3212907478_gshared)(__this, ___action0, ___p1, method)
// System.Void Cell`1<System.Object>::SetInputStream(IStream`1<T>)
extern "C"  void Cell_1_SetInputStream_m2944642014_gshared (Cell_1_t1019204052 * __this, Il2CppObject* ___stream0, const MethodInfo* method);
#define Cell_1_SetInputStream_m2944642014(__this, ___stream0, method) ((  void (*) (Cell_1_t1019204052 *, Il2CppObject*, const MethodInfo*))Cell_1_SetInputStream_m2944642014_gshared)(__this, ___stream0, method)
// System.Void Cell`1<System.Object>::ResetInputStream()
extern "C"  void Cell_1_ResetInputStream_m3069937277_gshared (Cell_1_t1019204052 * __this, const MethodInfo* method);
#define Cell_1_ResetInputStream_m3069937277(__this, method) ((  void (*) (Cell_1_t1019204052 *, const MethodInfo*))Cell_1_ResetInputStream_m3069937277_gshared)(__this, method)
// System.Void Cell`1<System.Object>::TransactionIterationFinished()
extern "C"  void Cell_1_TransactionIterationFinished_m1709986707_gshared (Cell_1_t1019204052 * __this, const MethodInfo* method);
#define Cell_1_TransactionIterationFinished_m1709986707(__this, method) ((  void (*) (Cell_1_t1019204052 *, const MethodInfo*))Cell_1_TransactionIterationFinished_m1709986707_gshared)(__this, method)
// System.Void Cell`1<System.Object>::Unpack(System.Int32)
extern "C"  void Cell_1_Unpack_m1047006821_gshared (Cell_1_t1019204052 * __this, int32_t ___p0, const MethodInfo* method);
#define Cell_1_Unpack_m1047006821(__this, ___p0, method) ((  void (*) (Cell_1_t1019204052 *, int32_t, const MethodInfo*))Cell_1_Unpack_m1047006821_gshared)(__this, ___p0, method)
// System.Boolean Cell`1<System.Object>::op_LessThanOrEqual(Cell`1<T>,UnityEngine.UI.Text)
extern "C"  bool Cell_1_op_LessThanOrEqual_m4088946964_gshared (Il2CppObject * __this /* static, unused */, Cell_1_t1019204052 * ___cell0, Text_t3286458198 * ___text1, const MethodInfo* method);
#define Cell_1_op_LessThanOrEqual_m4088946964(__this /* static, unused */, ___cell0, ___text1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Cell_1_t1019204052 *, Text_t3286458198 *, const MethodInfo*))Cell_1_op_LessThanOrEqual_m4088946964_gshared)(__this /* static, unused */, ___cell0, ___text1, method)
// System.Boolean Cell`1<System.Object>::op_GreaterThanOrEqual(Cell`1<T>,UnityEngine.UI.Text)
extern "C"  bool Cell_1_op_GreaterThanOrEqual_m38753523_gshared (Il2CppObject * __this /* static, unused */, Cell_1_t1019204052 * ___cell0, Text_t3286458198 * ___text1, const MethodInfo* method);
#define Cell_1_op_GreaterThanOrEqual_m38753523(__this /* static, unused */, ___cell0, ___text1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Cell_1_t1019204052 *, Text_t3286458198 *, const MethodInfo*))Cell_1_op_GreaterThanOrEqual_m38753523_gshared)(__this /* static, unused */, ___cell0, ___text1, method)
