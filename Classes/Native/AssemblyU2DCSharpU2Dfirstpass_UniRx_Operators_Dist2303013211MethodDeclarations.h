﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.DistinctUntilChangedObservable`1/DistinctUntilChanged<System.Object>
struct DistinctUntilChanged_t2303013211;
// UniRx.Operators.DistinctUntilChangedObservable`1<System.Object>
struct DistinctUntilChangedObservable_1_t3826078964;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.DistinctUntilChangedObservable`1/DistinctUntilChanged<System.Object>::.ctor(UniRx.Operators.DistinctUntilChangedObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void DistinctUntilChanged__ctor_m351372310_gshared (DistinctUntilChanged_t2303013211 * __this, DistinctUntilChangedObservable_1_t3826078964 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define DistinctUntilChanged__ctor_m351372310(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (DistinctUntilChanged_t2303013211 *, DistinctUntilChangedObservable_1_t3826078964 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))DistinctUntilChanged__ctor_m351372310_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.DistinctUntilChangedObservable`1/DistinctUntilChanged<System.Object>::OnNext(T)
extern "C"  void DistinctUntilChanged_OnNext_m1826660895_gshared (DistinctUntilChanged_t2303013211 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define DistinctUntilChanged_OnNext_m1826660895(__this, ___value0, method) ((  void (*) (DistinctUntilChanged_t2303013211 *, Il2CppObject *, const MethodInfo*))DistinctUntilChanged_OnNext_m1826660895_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.DistinctUntilChangedObservable`1/DistinctUntilChanged<System.Object>::OnError(System.Exception)
extern "C"  void DistinctUntilChanged_OnError_m3677527854_gshared (DistinctUntilChanged_t2303013211 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define DistinctUntilChanged_OnError_m3677527854(__this, ___error0, method) ((  void (*) (DistinctUntilChanged_t2303013211 *, Exception_t1967233988 *, const MethodInfo*))DistinctUntilChanged_OnError_m3677527854_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.DistinctUntilChangedObservable`1/DistinctUntilChanged<System.Object>::OnCompleted()
extern "C"  void DistinctUntilChanged_OnCompleted_m1048848257_gshared (DistinctUntilChanged_t2303013211 * __this, const MethodInfo* method);
#define DistinctUntilChanged_OnCompleted_m1048848257(__this, method) ((  void (*) (DistinctUntilChanged_t2303013211 *, const MethodInfo*))DistinctUntilChanged_OnCompleted_m1048848257_gshared)(__this, method)
