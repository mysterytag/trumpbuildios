﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23312956448MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,PlayFab.ClientModels.UserDataRecord>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2405632103(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1290068936 *, String_t*, UserDataRecord_t163839734 *, const MethodInfo*))KeyValuePair_2__ctor_m4168265535_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,PlayFab.ClientModels.UserDataRecord>::get_Key()
#define KeyValuePair_2_get_Key_m1409338145(__this, method) ((  String_t* (*) (KeyValuePair_2_t1290068936 *, const MethodInfo*))KeyValuePair_2_get_Key_m3256475977_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,PlayFab.ClientModels.UserDataRecord>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2196008290(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1290068936 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m1278074762_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,PlayFab.ClientModels.UserDataRecord>::get_Value()
#define KeyValuePair_2_get_Value_m1711035909(__this, method) ((  UserDataRecord_t163839734 * (*) (KeyValuePair_2_t1290068936 *, const MethodInfo*))KeyValuePair_2_get_Value_m3899079597_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,PlayFab.ClientModels.UserDataRecord>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1789280354(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1290068936 *, UserDataRecord_t163839734 *, const MethodInfo*))KeyValuePair_2_set_Value_m2954518154_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,PlayFab.ClientModels.UserDataRecord>::ToString()
#define KeyValuePair_2_ToString_m3622116518(__this, method) ((  String_t* (*) (KeyValuePair_2_t1290068936 *, const MethodInfo*))KeyValuePair_2_ToString_m1313859518_gshared)(__this, method)
