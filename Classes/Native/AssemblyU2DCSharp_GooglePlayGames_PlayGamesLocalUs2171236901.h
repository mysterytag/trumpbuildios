﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GooglePlayGames.PlayGamesPlatform
struct PlayGamesPlatform_t930834684;
// UnityEngine.WWW
struct WWW_t1522972100;
// UnityEngine.Texture2D
struct Texture2D_t2509538522;

#include "AssemblyU2DCSharp_GooglePlayGames_PlayGamesUserPro2081647341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.PlayGamesLocalUser
struct  PlayGamesLocalUser_t2171236901  : public PlayGamesUserProfile_t2081647341
{
public:
	// GooglePlayGames.PlayGamesPlatform GooglePlayGames.PlayGamesLocalUser::mPlatform
	PlayGamesPlatform_t930834684 * ___mPlatform_0;
	// UnityEngine.WWW GooglePlayGames.PlayGamesLocalUser::mAvatarUrl
	WWW_t1522972100 * ___mAvatarUrl_1;
	// UnityEngine.Texture2D GooglePlayGames.PlayGamesLocalUser::mImage
	Texture2D_t2509538522 * ___mImage_2;

public:
	inline static int32_t get_offset_of_mPlatform_0() { return static_cast<int32_t>(offsetof(PlayGamesLocalUser_t2171236901, ___mPlatform_0)); }
	inline PlayGamesPlatform_t930834684 * get_mPlatform_0() const { return ___mPlatform_0; }
	inline PlayGamesPlatform_t930834684 ** get_address_of_mPlatform_0() { return &___mPlatform_0; }
	inline void set_mPlatform_0(PlayGamesPlatform_t930834684 * value)
	{
		___mPlatform_0 = value;
		Il2CppCodeGenWriteBarrier(&___mPlatform_0, value);
	}

	inline static int32_t get_offset_of_mAvatarUrl_1() { return static_cast<int32_t>(offsetof(PlayGamesLocalUser_t2171236901, ___mAvatarUrl_1)); }
	inline WWW_t1522972100 * get_mAvatarUrl_1() const { return ___mAvatarUrl_1; }
	inline WWW_t1522972100 ** get_address_of_mAvatarUrl_1() { return &___mAvatarUrl_1; }
	inline void set_mAvatarUrl_1(WWW_t1522972100 * value)
	{
		___mAvatarUrl_1 = value;
		Il2CppCodeGenWriteBarrier(&___mAvatarUrl_1, value);
	}

	inline static int32_t get_offset_of_mImage_2() { return static_cast<int32_t>(offsetof(PlayGamesLocalUser_t2171236901, ___mImage_2)); }
	inline Texture2D_t2509538522 * get_mImage_2() const { return ___mImage_2; }
	inline Texture2D_t2509538522 ** get_address_of_mImage_2() { return &___mImage_2; }
	inline void set_mImage_2(Texture2D_t2509538522 * value)
	{
		___mImage_2 = value;
		Il2CppCodeGenWriteBarrier(&___mImage_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
