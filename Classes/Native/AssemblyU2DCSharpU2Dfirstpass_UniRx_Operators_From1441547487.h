﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4165376397.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.FromCoroutine`1/FromCoroutineObserver<UniRx.Unit>
struct  FromCoroutineObserver_t1441547487  : public OperatorObserverBase_2_t4165376397
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
