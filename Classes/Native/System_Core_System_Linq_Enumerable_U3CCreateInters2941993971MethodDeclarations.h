﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Enumerable/<CreateIntersectIterator>c__IteratorA`1<System.Object>
struct U3CCreateIntersectIteratorU3Ec__IteratorA_1_t2941993971;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2320212868;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Linq.Enumerable/<CreateIntersectIterator>c__IteratorA`1<System.Object>::.ctor()
extern "C"  void U3CCreateIntersectIteratorU3Ec__IteratorA_1__ctor_m2444888689_gshared (U3CCreateIntersectIteratorU3Ec__IteratorA_1_t2941993971 * __this, const MethodInfo* method);
#define U3CCreateIntersectIteratorU3Ec__IteratorA_1__ctor_m2444888689(__this, method) ((  void (*) (U3CCreateIntersectIteratorU3Ec__IteratorA_1_t2941993971 *, const MethodInfo*))U3CCreateIntersectIteratorU3Ec__IteratorA_1__ctor_m2444888689_gshared)(__this, method)
// TSource System.Linq.Enumerable/<CreateIntersectIterator>c__IteratorA`1<System.Object>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern "C"  Il2CppObject * U3CCreateIntersectIteratorU3Ec__IteratorA_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m2934410946_gshared (U3CCreateIntersectIteratorU3Ec__IteratorA_1_t2941993971 * __this, const MethodInfo* method);
#define U3CCreateIntersectIteratorU3Ec__IteratorA_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m2934410946(__this, method) ((  Il2CppObject * (*) (U3CCreateIntersectIteratorU3Ec__IteratorA_1_t2941993971 *, const MethodInfo*))U3CCreateIntersectIteratorU3Ec__IteratorA_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m2934410946_gshared)(__this, method)
// System.Object System.Linq.Enumerable/<CreateIntersectIterator>c__IteratorA`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateIntersectIteratorU3Ec__IteratorA_1_System_Collections_IEnumerator_get_Current_m3020026837_gshared (U3CCreateIntersectIteratorU3Ec__IteratorA_1_t2941993971 * __this, const MethodInfo* method);
#define U3CCreateIntersectIteratorU3Ec__IteratorA_1_System_Collections_IEnumerator_get_Current_m3020026837(__this, method) ((  Il2CppObject * (*) (U3CCreateIntersectIteratorU3Ec__IteratorA_1_t2941993971 *, const MethodInfo*))U3CCreateIntersectIteratorU3Ec__IteratorA_1_System_Collections_IEnumerator_get_Current_m3020026837_gshared)(__this, method)
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateIntersectIterator>c__IteratorA`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateIntersectIteratorU3Ec__IteratorA_1_System_Collections_IEnumerable_GetEnumerator_m2094035440_gshared (U3CCreateIntersectIteratorU3Ec__IteratorA_1_t2941993971 * __this, const MethodInfo* method);
#define U3CCreateIntersectIteratorU3Ec__IteratorA_1_System_Collections_IEnumerable_GetEnumerator_m2094035440(__this, method) ((  Il2CppObject * (*) (U3CCreateIntersectIteratorU3Ec__IteratorA_1_t2941993971 *, const MethodInfo*))U3CCreateIntersectIteratorU3Ec__IteratorA_1_System_Collections_IEnumerable_GetEnumerator_m2094035440_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateIntersectIterator>c__IteratorA`1<System.Object>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateIntersectIteratorU3Ec__IteratorA_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m2149049115_gshared (U3CCreateIntersectIteratorU3Ec__IteratorA_1_t2941993971 * __this, const MethodInfo* method);
#define U3CCreateIntersectIteratorU3Ec__IteratorA_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m2149049115(__this, method) ((  Il2CppObject* (*) (U3CCreateIntersectIteratorU3Ec__IteratorA_1_t2941993971 *, const MethodInfo*))U3CCreateIntersectIteratorU3Ec__IteratorA_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m2149049115_gshared)(__this, method)
// System.Boolean System.Linq.Enumerable/<CreateIntersectIterator>c__IteratorA`1<System.Object>::MoveNext()
extern "C"  bool U3CCreateIntersectIteratorU3Ec__IteratorA_1_MoveNext_m920239363_gshared (U3CCreateIntersectIteratorU3Ec__IteratorA_1_t2941993971 * __this, const MethodInfo* method);
#define U3CCreateIntersectIteratorU3Ec__IteratorA_1_MoveNext_m920239363(__this, method) ((  bool (*) (U3CCreateIntersectIteratorU3Ec__IteratorA_1_t2941993971 *, const MethodInfo*))U3CCreateIntersectIteratorU3Ec__IteratorA_1_MoveNext_m920239363_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateIntersectIterator>c__IteratorA`1<System.Object>::Dispose()
extern "C"  void U3CCreateIntersectIteratorU3Ec__IteratorA_1_Dispose_m89202286_gshared (U3CCreateIntersectIteratorU3Ec__IteratorA_1_t2941993971 * __this, const MethodInfo* method);
#define U3CCreateIntersectIteratorU3Ec__IteratorA_1_Dispose_m89202286(__this, method) ((  void (*) (U3CCreateIntersectIteratorU3Ec__IteratorA_1_t2941993971 *, const MethodInfo*))U3CCreateIntersectIteratorU3Ec__IteratorA_1_Dispose_m89202286_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateIntersectIterator>c__IteratorA`1<System.Object>::Reset()
extern "C"  void U3CCreateIntersectIteratorU3Ec__IteratorA_1_Reset_m91321630_gshared (U3CCreateIntersectIteratorU3Ec__IteratorA_1_t2941993971 * __this, const MethodInfo* method);
#define U3CCreateIntersectIteratorU3Ec__IteratorA_1_Reset_m91321630(__this, method) ((  void (*) (U3CCreateIntersectIteratorU3Ec__IteratorA_1_t2941993971 *, const MethodInfo*))U3CCreateIntersectIteratorU3Ec__IteratorA_1_Reset_m91321630_gshared)(__this, method)
