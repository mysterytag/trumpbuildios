﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Cell`1/<OnChanged>c__AnonStoreyF3<System.Double>
struct U3COnChangedU3Ec__AnonStoreyF3_t3910665378;

#include "codegen/il2cpp-codegen.h"

// System.Void Cell`1/<OnChanged>c__AnonStoreyF3<System.Double>::.ctor()
extern "C"  void U3COnChangedU3Ec__AnonStoreyF3__ctor_m1940134908_gshared (U3COnChangedU3Ec__AnonStoreyF3_t3910665378 * __this, const MethodInfo* method);
#define U3COnChangedU3Ec__AnonStoreyF3__ctor_m1940134908(__this, method) ((  void (*) (U3COnChangedU3Ec__AnonStoreyF3_t3910665378 *, const MethodInfo*))U3COnChangedU3Ec__AnonStoreyF3__ctor_m1940134908_gshared)(__this, method)
// System.Void Cell`1/<OnChanged>c__AnonStoreyF3<System.Double>::<>m__162(T)
extern "C"  void U3COnChangedU3Ec__AnonStoreyF3_U3CU3Em__162_m4177795188_gshared (U3COnChangedU3Ec__AnonStoreyF3_t3910665378 * __this, double ____0, const MethodInfo* method);
#define U3COnChangedU3Ec__AnonStoreyF3_U3CU3Em__162_m4177795188(__this, ____0, method) ((  void (*) (U3COnChangedU3Ec__AnonStoreyF3_t3910665378 *, double, const MethodInfo*))U3COnChangedU3Ec__AnonStoreyF3_U3CU3Em__162_m4177795188_gshared)(__this, ____0, method)
