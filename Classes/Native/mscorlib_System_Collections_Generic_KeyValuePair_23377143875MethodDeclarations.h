﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23312956448MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Zenject.PrefabSingletonId,Zenject.PrefabSingletonLazyCreator>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3501830838(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3377143875 *, PrefabSingletonId_t3643845047 *, PrefabSingletonLazyCreator_t3719004230 *, const MethodInfo*))KeyValuePair_2__ctor_m4168265535_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Zenject.PrefabSingletonId,Zenject.PrefabSingletonLazyCreator>::get_Key()
#define KeyValuePair_2_get_Key_m3657684082(__this, method) ((  PrefabSingletonId_t3643845047 * (*) (KeyValuePair_2_t3377143875 *, const MethodInfo*))KeyValuePair_2_get_Key_m3256475977_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Zenject.PrefabSingletonId,Zenject.PrefabSingletonLazyCreator>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m426921395(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3377143875 *, PrefabSingletonId_t3643845047 *, const MethodInfo*))KeyValuePair_2_set_Key_m1278074762_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<Zenject.PrefabSingletonId,Zenject.PrefabSingletonLazyCreator>::get_Value()
#define KeyValuePair_2_get_Value_m1691664050(__this, method) ((  PrefabSingletonLazyCreator_t3719004230 * (*) (KeyValuePair_2_t3377143875 *, const MethodInfo*))KeyValuePair_2_get_Value_m3899079597_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Zenject.PrefabSingletonId,Zenject.PrefabSingletonLazyCreator>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1270452275(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3377143875 *, PrefabSingletonLazyCreator_t3719004230 *, const MethodInfo*))KeyValuePair_2_set_Value_m2954518154_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<Zenject.PrefabSingletonId,Zenject.PrefabSingletonLazyCreator>::ToString()
#define KeyValuePair_2_ToString_m3729432655(__this, method) ((  String_t* (*) (KeyValuePair_2_t3377143875 *, const MethodInfo*))KeyValuePair_2_ToString_m1313859518_gshared)(__this, method)
