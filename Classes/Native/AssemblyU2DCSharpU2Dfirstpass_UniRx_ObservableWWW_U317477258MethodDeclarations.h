﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ObservableWWW/<GetAndGetBytes>c__AnonStorey7F
struct U3CGetAndGetBytesU3Ec__AnonStorey7F_t17477258;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// UniRx.IObserver`1<System.Byte[]>
struct IObserver_1_t2270505063;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560.h"

// System.Void UniRx.ObservableWWW/<GetAndGetBytes>c__AnonStorey7F::.ctor()
extern "C"  void U3CGetAndGetBytesU3Ec__AnonStorey7F__ctor_m4018533664 (U3CGetAndGetBytesU3Ec__AnonStorey7F_t17477258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UniRx.ObservableWWW/<GetAndGetBytes>c__AnonStorey7F::<>m__AB(UniRx.IObserver`1<System.Byte[]>,UniRx.CancellationToken)
extern "C"  Il2CppObject * U3CGetAndGetBytesU3Ec__AnonStorey7F_U3CU3Em__AB_m1487153967 (U3CGetAndGetBytesU3Ec__AnonStorey7F_t17477258 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellation1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
