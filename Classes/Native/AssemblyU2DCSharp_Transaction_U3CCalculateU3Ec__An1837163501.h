﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<ICell>
struct List_1_t866472516;
// System.Func`1<System.Object>
struct Func_1_t1979887667;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Transaction/<Calculate>c__AnonStorey149`1<System.Object>
struct  U3CCalculateU3Ec__AnonStorey149_1_t1837163501  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<ICell> Transaction/<Calculate>c__AnonStorey149`1::copyTouched
	List_1_t866472516 * ___copyTouched_0;
	// System.Func`1<T> Transaction/<Calculate>c__AnonStorey149`1::formula
	Func_1_t1979887667 * ___formula_1;

public:
	inline static int32_t get_offset_of_copyTouched_0() { return static_cast<int32_t>(offsetof(U3CCalculateU3Ec__AnonStorey149_1_t1837163501, ___copyTouched_0)); }
	inline List_1_t866472516 * get_copyTouched_0() const { return ___copyTouched_0; }
	inline List_1_t866472516 ** get_address_of_copyTouched_0() { return &___copyTouched_0; }
	inline void set_copyTouched_0(List_1_t866472516 * value)
	{
		___copyTouched_0 = value;
		Il2CppCodeGenWriteBarrier(&___copyTouched_0, value);
	}

	inline static int32_t get_offset_of_formula_1() { return static_cast<int32_t>(offsetof(U3CCalculateU3Ec__AnonStorey149_1_t1837163501, ___formula_1)); }
	inline Func_1_t1979887667 * get_formula_1() const { return ___formula_1; }
	inline Func_1_t1979887667 ** get_address_of_formula_1() { return &___formula_1; }
	inline void set_formula_1(Func_1_t1979887667 * value)
	{
		___formula_1 = value;
		Il2CppCodeGenWriteBarrier(&___formula_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
