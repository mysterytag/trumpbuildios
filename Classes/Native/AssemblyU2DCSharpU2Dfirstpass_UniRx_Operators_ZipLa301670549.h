﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.ZipLatestObservable`1/ZipLatest<System.Object>
struct ZipLatest_t2874059041;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.ZipLatestObservable`1/ZipLatest/CombineLatestObserver<System.Object>
struct  CombineLatestObserver_t301670550  : public Il2CppObject
{
public:
	// UniRx.Operators.ZipLatestObservable`1/ZipLatest<T> UniRx.Operators.ZipLatestObservable`1/ZipLatest/CombineLatestObserver::parent
	ZipLatest_t2874059041 * ___parent_0;
	// System.Int32 UniRx.Operators.ZipLatestObservable`1/ZipLatest/CombineLatestObserver::index
	int32_t ___index_1;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(CombineLatestObserver_t301670550, ___parent_0)); }
	inline ZipLatest_t2874059041 * get_parent_0() const { return ___parent_0; }
	inline ZipLatest_t2874059041 ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(ZipLatest_t2874059041 * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier(&___parent_0, value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(CombineLatestObserver_t301670550, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
