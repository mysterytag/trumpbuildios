﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SponsorPay.BrandEngageResponse
struct BrandEngageResponse_t3340618179;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void SponsorPay.BrandEngageResponse::.ctor()
extern "C"  void BrandEngageResponse__ctor_m196957010 (BrandEngageResponse_t3340618179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SponsorPay.BrandEngageResponse::get_error()
extern "C"  String_t* BrandEngageResponse_get_error_m629546796 (BrandEngageResponse_t3340618179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.BrandEngageResponse::set_error(System.String)
extern "C"  void BrandEngageResponse_set_error_m2006598823 (BrandEngageResponse_t3340618179 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SponsorPay.BrandEngageResponse::get_offersAvailable()
extern "C"  bool BrandEngageResponse_get_offersAvailable_m1453392357 (BrandEngageResponse_t3340618179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.BrandEngageResponse::set_offersAvailable(System.Boolean)
extern "C"  void BrandEngageResponse_set_offersAvailable_m1769749212 (BrandEngageResponse_t3340618179 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
