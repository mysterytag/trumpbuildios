﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ModestTree.Util.Tuple`3<System.Object,System.Object,System.Object>
struct Tuple_3_t1160054790;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void ModestTree.Util.Tuple`3<System.Object,System.Object,System.Object>::.ctor()
extern "C"  void Tuple_3__ctor_m3193714974_gshared (Tuple_3_t1160054790 * __this, const MethodInfo* method);
#define Tuple_3__ctor_m3193714974(__this, method) ((  void (*) (Tuple_3_t1160054790 *, const MethodInfo*))Tuple_3__ctor_m3193714974_gshared)(__this, method)
// System.Void ModestTree.Util.Tuple`3<System.Object,System.Object,System.Object>::.ctor(T1,T2,T3)
extern "C"  void Tuple_3__ctor_m2993656704_gshared (Tuple_3_t1160054790 * __this, Il2CppObject * ___first0, Il2CppObject * ___second1, Il2CppObject * ___third2, const MethodInfo* method);
#define Tuple_3__ctor_m2993656704(__this, ___first0, ___second1, ___third2, method) ((  void (*) (Tuple_3_t1160054790 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_3__ctor_m2993656704_gshared)(__this, ___first0, ___second1, ___third2, method)
// System.Boolean ModestTree.Util.Tuple`3<System.Object,System.Object,System.Object>::Equals(System.Object)
extern "C"  bool Tuple_3_Equals_m680266331_gshared (Tuple_3_t1160054790 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define Tuple_3_Equals_m680266331(__this, ___obj0, method) ((  bool (*) (Tuple_3_t1160054790 *, Il2CppObject *, const MethodInfo*))Tuple_3_Equals_m680266331_gshared)(__this, ___obj0, method)
// System.Boolean ModestTree.Util.Tuple`3<System.Object,System.Object,System.Object>::Equals(ModestTree.Util.Tuple`3<T1,T2,T3>)
extern "C"  bool Tuple_3_Equals_m707126330_gshared (Tuple_3_t1160054790 * __this, Tuple_3_t1160054790 * ___that0, const MethodInfo* method);
#define Tuple_3_Equals_m707126330(__this, ___that0, method) ((  bool (*) (Tuple_3_t1160054790 *, Tuple_3_t1160054790 *, const MethodInfo*))Tuple_3_Equals_m707126330_gshared)(__this, ___that0, method)
// System.Int32 ModestTree.Util.Tuple`3<System.Object,System.Object,System.Object>::GetHashCode()
extern "C"  int32_t Tuple_3_GetHashCode_m845495039_gshared (Tuple_3_t1160054790 * __this, const MethodInfo* method);
#define Tuple_3_GetHashCode_m845495039(__this, method) ((  int32_t (*) (Tuple_3_t1160054790 *, const MethodInfo*))Tuple_3_GetHashCode_m845495039_gshared)(__this, method)
