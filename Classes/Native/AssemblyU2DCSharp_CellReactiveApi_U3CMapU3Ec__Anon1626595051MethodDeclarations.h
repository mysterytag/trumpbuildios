﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/<Map>c__AnonStorey10E`2/<Map>c__AnonStorey10F`2<System.Int32,System.Int32>
struct U3CMapU3Ec__AnonStorey10F_2_t1626595051;

#include "codegen/il2cpp-codegen.h"

// System.Void CellReactiveApi/<Map>c__AnonStorey10E`2/<Map>c__AnonStorey10F`2<System.Int32,System.Int32>::.ctor()
extern "C"  void U3CMapU3Ec__AnonStorey10F_2__ctor_m4226048493_gshared (U3CMapU3Ec__AnonStorey10F_2_t1626595051 * __this, const MethodInfo* method);
#define U3CMapU3Ec__AnonStorey10F_2__ctor_m4226048493(__this, method) ((  void (*) (U3CMapU3Ec__AnonStorey10F_2_t1626595051 *, const MethodInfo*))U3CMapU3Ec__AnonStorey10F_2__ctor_m4226048493_gshared)(__this, method)
// System.Void CellReactiveApi/<Map>c__AnonStorey10E`2/<Map>c__AnonStorey10F`2<System.Int32,System.Int32>::<>m__197(T)
extern "C"  void U3CMapU3Ec__AnonStorey10F_2_U3CU3Em__197_m990546499_gshared (U3CMapU3Ec__AnonStorey10F_2_t1626595051 * __this, int32_t ___val0, const MethodInfo* method);
#define U3CMapU3Ec__AnonStorey10F_2_U3CU3Em__197_m990546499(__this, ___val0, method) ((  void (*) (U3CMapU3Ec__AnonStorey10F_2_t1626595051 *, int32_t, const MethodInfo*))U3CMapU3Ec__AnonStorey10F_2_U3CU3Em__197_m990546499_gshared)(__this, ___val0, method)
