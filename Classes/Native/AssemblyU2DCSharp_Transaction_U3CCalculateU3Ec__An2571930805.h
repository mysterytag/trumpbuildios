﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CellUtils.CellJoinDisposable`1<System.Object>
struct CellJoinDisposable_1_t3736493403;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// Transaction/<Calculate>c__AnonStorey149`1<System.Object>
struct U3CCalculateU3Ec__AnonStorey149_1_t1837163501;

#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Transaction/<Calculate>c__AnonStorey149`1/<Calculate>c__AnonStorey14A`1<System.Object>
struct  U3CCalculateU3Ec__AnonStorey14A_1_t2571930805  : public Il2CppObject
{
public:
	// Priority Transaction/<Calculate>c__AnonStorey149`1/<Calculate>c__AnonStorey14A`1::p
	int32_t ___p_0;
	// CellUtils.CellJoinDisposable`1<T> Transaction/<Calculate>c__AnonStorey149`1/<Calculate>c__AnonStorey14A`1::group
	CellJoinDisposable_1_t3736493403 * ___group_1;
	// System.Action`1<T> Transaction/<Calculate>c__AnonStorey149`1/<Calculate>c__AnonStorey14A`1::reaction
	Action_1_t985559125 * ___reaction_2;
	// Transaction/<Calculate>c__AnonStorey149`1<T> Transaction/<Calculate>c__AnonStorey149`1/<Calculate>c__AnonStorey14A`1::<>f__ref$329
	U3CCalculateU3Ec__AnonStorey149_1_t1837163501 * ___U3CU3Ef__refU24329_3;

public:
	inline static int32_t get_offset_of_p_0() { return static_cast<int32_t>(offsetof(U3CCalculateU3Ec__AnonStorey14A_1_t2571930805, ___p_0)); }
	inline int32_t get_p_0() const { return ___p_0; }
	inline int32_t* get_address_of_p_0() { return &___p_0; }
	inline void set_p_0(int32_t value)
	{
		___p_0 = value;
	}

	inline static int32_t get_offset_of_group_1() { return static_cast<int32_t>(offsetof(U3CCalculateU3Ec__AnonStorey14A_1_t2571930805, ___group_1)); }
	inline CellJoinDisposable_1_t3736493403 * get_group_1() const { return ___group_1; }
	inline CellJoinDisposable_1_t3736493403 ** get_address_of_group_1() { return &___group_1; }
	inline void set_group_1(CellJoinDisposable_1_t3736493403 * value)
	{
		___group_1 = value;
		Il2CppCodeGenWriteBarrier(&___group_1, value);
	}

	inline static int32_t get_offset_of_reaction_2() { return static_cast<int32_t>(offsetof(U3CCalculateU3Ec__AnonStorey14A_1_t2571930805, ___reaction_2)); }
	inline Action_1_t985559125 * get_reaction_2() const { return ___reaction_2; }
	inline Action_1_t985559125 ** get_address_of_reaction_2() { return &___reaction_2; }
	inline void set_reaction_2(Action_1_t985559125 * value)
	{
		___reaction_2 = value;
		Il2CppCodeGenWriteBarrier(&___reaction_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24329_3() { return static_cast<int32_t>(offsetof(U3CCalculateU3Ec__AnonStorey14A_1_t2571930805, ___U3CU3Ef__refU24329_3)); }
	inline U3CCalculateU3Ec__AnonStorey149_1_t1837163501 * get_U3CU3Ef__refU24329_3() const { return ___U3CU3Ef__refU24329_3; }
	inline U3CCalculateU3Ec__AnonStorey149_1_t1837163501 ** get_address_of_U3CU3Ef__refU24329_3() { return &___U3CU3Ef__refU24329_3; }
	inline void set_U3CU3Ef__refU24329_3(U3CCalculateU3Ec__AnonStorey149_1_t1837163501 * value)
	{
		___U3CU3Ef__refU24329_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24329_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
