﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t514686775;
// System.String
struct String_t;
// System.Collections.Generic.IEnumerator`1<UnityEngine.GameObject>
struct IEnumerator_1_t1200834254;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ZenUtil/<LoadSceneAdditiveWithContainer>c__Iterator4D
struct  U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D_t2977085640  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Zenject.ZenUtil/<LoadSceneAdditiveWithContainer>c__Iterator4D::<rootObjectsBeforeLoad>__0
	List_1_t514686775 * ___U3CrootObjectsBeforeLoadU3E__0_0;
	// System.String Zenject.ZenUtil/<LoadSceneAdditiveWithContainer>c__Iterator4D::levelName
	String_t* ___levelName_1;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Zenject.ZenUtil/<LoadSceneAdditiveWithContainer>c__Iterator4D::<rootObjectsAfterLoad>__1
	List_1_t514686775 * ___U3CrootObjectsAfterLoadU3E__1_2;
	// System.Collections.Generic.IEnumerator`1<UnityEngine.GameObject> Zenject.ZenUtil/<LoadSceneAdditiveWithContainer>c__Iterator4D::<$s_321>__2
	Il2CppObject* ___U3CU24s_321U3E__2_3;
	// UnityEngine.GameObject Zenject.ZenUtil/<LoadSceneAdditiveWithContainer>c__Iterator4D::<newObject>__3
	GameObject_t4012695102 * ___U3CnewObjectU3E__3_4;
	// Zenject.DiContainer Zenject.ZenUtil/<LoadSceneAdditiveWithContainer>c__Iterator4D::parentContainer
	DiContainer_t2383114449 * ___parentContainer_5;
	// System.Int32 Zenject.ZenUtil/<LoadSceneAdditiveWithContainer>c__Iterator4D::$PC
	int32_t ___U24PC_6;
	// System.Object Zenject.ZenUtil/<LoadSceneAdditiveWithContainer>c__Iterator4D::$current
	Il2CppObject * ___U24current_7;
	// System.String Zenject.ZenUtil/<LoadSceneAdditiveWithContainer>c__Iterator4D::<$>levelName
	String_t* ___U3CU24U3ElevelName_8;
	// Zenject.DiContainer Zenject.ZenUtil/<LoadSceneAdditiveWithContainer>c__Iterator4D::<$>parentContainer
	DiContainer_t2383114449 * ___U3CU24U3EparentContainer_9;

public:
	inline static int32_t get_offset_of_U3CrootObjectsBeforeLoadU3E__0_0() { return static_cast<int32_t>(offsetof(U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D_t2977085640, ___U3CrootObjectsBeforeLoadU3E__0_0)); }
	inline List_1_t514686775 * get_U3CrootObjectsBeforeLoadU3E__0_0() const { return ___U3CrootObjectsBeforeLoadU3E__0_0; }
	inline List_1_t514686775 ** get_address_of_U3CrootObjectsBeforeLoadU3E__0_0() { return &___U3CrootObjectsBeforeLoadU3E__0_0; }
	inline void set_U3CrootObjectsBeforeLoadU3E__0_0(List_1_t514686775 * value)
	{
		___U3CrootObjectsBeforeLoadU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrootObjectsBeforeLoadU3E__0_0, value);
	}

	inline static int32_t get_offset_of_levelName_1() { return static_cast<int32_t>(offsetof(U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D_t2977085640, ___levelName_1)); }
	inline String_t* get_levelName_1() const { return ___levelName_1; }
	inline String_t** get_address_of_levelName_1() { return &___levelName_1; }
	inline void set_levelName_1(String_t* value)
	{
		___levelName_1 = value;
		Il2CppCodeGenWriteBarrier(&___levelName_1, value);
	}

	inline static int32_t get_offset_of_U3CrootObjectsAfterLoadU3E__1_2() { return static_cast<int32_t>(offsetof(U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D_t2977085640, ___U3CrootObjectsAfterLoadU3E__1_2)); }
	inline List_1_t514686775 * get_U3CrootObjectsAfterLoadU3E__1_2() const { return ___U3CrootObjectsAfterLoadU3E__1_2; }
	inline List_1_t514686775 ** get_address_of_U3CrootObjectsAfterLoadU3E__1_2() { return &___U3CrootObjectsAfterLoadU3E__1_2; }
	inline void set_U3CrootObjectsAfterLoadU3E__1_2(List_1_t514686775 * value)
	{
		___U3CrootObjectsAfterLoadU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrootObjectsAfterLoadU3E__1_2, value);
	}

	inline static int32_t get_offset_of_U3CU24s_321U3E__2_3() { return static_cast<int32_t>(offsetof(U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D_t2977085640, ___U3CU24s_321U3E__2_3)); }
	inline Il2CppObject* get_U3CU24s_321U3E__2_3() const { return ___U3CU24s_321U3E__2_3; }
	inline Il2CppObject** get_address_of_U3CU24s_321U3E__2_3() { return &___U3CU24s_321U3E__2_3; }
	inline void set_U3CU24s_321U3E__2_3(Il2CppObject* value)
	{
		___U3CU24s_321U3E__2_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_321U3E__2_3, value);
	}

	inline static int32_t get_offset_of_U3CnewObjectU3E__3_4() { return static_cast<int32_t>(offsetof(U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D_t2977085640, ___U3CnewObjectU3E__3_4)); }
	inline GameObject_t4012695102 * get_U3CnewObjectU3E__3_4() const { return ___U3CnewObjectU3E__3_4; }
	inline GameObject_t4012695102 ** get_address_of_U3CnewObjectU3E__3_4() { return &___U3CnewObjectU3E__3_4; }
	inline void set_U3CnewObjectU3E__3_4(GameObject_t4012695102 * value)
	{
		___U3CnewObjectU3E__3_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CnewObjectU3E__3_4, value);
	}

	inline static int32_t get_offset_of_parentContainer_5() { return static_cast<int32_t>(offsetof(U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D_t2977085640, ___parentContainer_5)); }
	inline DiContainer_t2383114449 * get_parentContainer_5() const { return ___parentContainer_5; }
	inline DiContainer_t2383114449 ** get_address_of_parentContainer_5() { return &___parentContainer_5; }
	inline void set_parentContainer_5(DiContainer_t2383114449 * value)
	{
		___parentContainer_5 = value;
		Il2CppCodeGenWriteBarrier(&___parentContainer_5, value);
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D_t2977085640, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D_t2977085640, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U3CU24U3ElevelName_8() { return static_cast<int32_t>(offsetof(U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D_t2977085640, ___U3CU24U3ElevelName_8)); }
	inline String_t* get_U3CU24U3ElevelName_8() const { return ___U3CU24U3ElevelName_8; }
	inline String_t** get_address_of_U3CU24U3ElevelName_8() { return &___U3CU24U3ElevelName_8; }
	inline void set_U3CU24U3ElevelName_8(String_t* value)
	{
		___U3CU24U3ElevelName_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3ElevelName_8, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EparentContainer_9() { return static_cast<int32_t>(offsetof(U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D_t2977085640, ___U3CU24U3EparentContainer_9)); }
	inline DiContainer_t2383114449 * get_U3CU24U3EparentContainer_9() const { return ___U3CU24U3EparentContainer_9; }
	inline DiContainer_t2383114449 ** get_address_of_U3CU24U3EparentContainer_9() { return &___U3CU24U3EparentContainer_9; }
	inline void set_U3CU24U3EparentContainer_9(DiContainer_t2383114449 * value)
	{
		___U3CU24U3EparentContainer_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3EparentContainer_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
