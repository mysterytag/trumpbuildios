﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<LinkIOSDeviceID>c__AnonStorey7A
struct U3CLinkIOSDeviceIDU3Ec__AnonStorey7A_t3633880987;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<LinkIOSDeviceID>c__AnonStorey7A::.ctor()
extern "C"  void U3CLinkIOSDeviceIDU3Ec__AnonStorey7A__ctor_m1170431512 (U3CLinkIOSDeviceIDU3Ec__AnonStorey7A_t3633880987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<LinkIOSDeviceID>c__AnonStorey7A::<>m__67(PlayFab.CallRequestContainer)
extern "C"  void U3CLinkIOSDeviceIDU3Ec__AnonStorey7A_U3CU3Em__67_m3410571767 (U3CLinkIOSDeviceIDU3Ec__AnonStorey7A_t3633880987 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
