﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Examples.Sample09_EventHandling/<Start>c__AnonStorey31
struct U3CStartU3Ec__AnonStorey31_t1152509869;
// System.Object
struct Il2CppObject;
// UniRx.Examples.Sample09_EventHandling/MyEventArgs
struct MyEventArgs_t3100194347;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sampl3100194347.h"

// System.Void UniRx.Examples.Sample09_EventHandling/<Start>c__AnonStorey31::.ctor()
extern "C"  void U3CStartU3Ec__AnonStorey31__ctor_m2817058867 (U3CStartU3Ec__AnonStorey31_t1152509869 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample09_EventHandling/<Start>c__AnonStorey31::<>m__2F(System.Object,UniRx.Examples.Sample09_EventHandling/MyEventArgs)
extern "C"  void U3CStartU3Ec__AnonStorey31_U3CU3Em__2F_m2705133908 (U3CStartU3Ec__AnonStorey31_t1152509869 * __this, Il2CppObject * ___sender0, MyEventArgs_t3100194347 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
