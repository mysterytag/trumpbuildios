﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/Carrier`1<System.Object>
struct Carrier_1_t2723215674;

#include "codegen/il2cpp-codegen.h"

// System.Void CellReactiveApi/Carrier`1<System.Object>::.ctor()
extern "C"  void Carrier_1__ctor_m1719464920_gshared (Carrier_1_t2723215674 * __this, const MethodInfo* method);
#define Carrier_1__ctor_m1719464920(__this, method) ((  void (*) (Carrier_1_t2723215674 *, const MethodInfo*))Carrier_1__ctor_m1719464920_gshared)(__this, method)
