﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// <>__AnonType1`2<System.Object,System.Object>
struct U3CU3E__AnonType1_2_t3686769320;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void <>__AnonType1`2<System.Object,System.Object>::.ctor(<show>__T,<hidden>__T)
extern "C"  void U3CU3E__AnonType1_2__ctor_m1629335399_gshared (U3CU3E__AnonType1_2_t3686769320 * __this, Il2CppObject * ___show0, Il2CppObject * ___hidden1, const MethodInfo* method);
#define U3CU3E__AnonType1_2__ctor_m1629335399(__this, ___show0, ___hidden1, method) ((  void (*) (U3CU3E__AnonType1_2_t3686769320 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))U3CU3E__AnonType1_2__ctor_m1629335399_gshared)(__this, ___show0, ___hidden1, method)
// <show>__T <>__AnonType1`2<System.Object,System.Object>::get_show()
extern "C"  Il2CppObject * U3CU3E__AnonType1_2_get_show_m3552153506_gshared (U3CU3E__AnonType1_2_t3686769320 * __this, const MethodInfo* method);
#define U3CU3E__AnonType1_2_get_show_m3552153506(__this, method) ((  Il2CppObject * (*) (U3CU3E__AnonType1_2_t3686769320 *, const MethodInfo*))U3CU3E__AnonType1_2_get_show_m3552153506_gshared)(__this, method)
// <hidden>__T <>__AnonType1`2<System.Object,System.Object>::get_hidden()
extern "C"  Il2CppObject * U3CU3E__AnonType1_2_get_hidden_m4204487170_gshared (U3CU3E__AnonType1_2_t3686769320 * __this, const MethodInfo* method);
#define U3CU3E__AnonType1_2_get_hidden_m4204487170(__this, method) ((  Il2CppObject * (*) (U3CU3E__AnonType1_2_t3686769320 *, const MethodInfo*))U3CU3E__AnonType1_2_get_hidden_m4204487170_gshared)(__this, method)
// System.Boolean <>__AnonType1`2<System.Object,System.Object>::Equals(System.Object)
extern "C"  bool U3CU3E__AnonType1_2_Equals_m543332105_gshared (U3CU3E__AnonType1_2_t3686769320 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define U3CU3E__AnonType1_2_Equals_m543332105(__this, ___obj0, method) ((  bool (*) (U3CU3E__AnonType1_2_t3686769320 *, Il2CppObject *, const MethodInfo*))U3CU3E__AnonType1_2_Equals_m543332105_gshared)(__this, ___obj0, method)
// System.Int32 <>__AnonType1`2<System.Object,System.Object>::GetHashCode()
extern "C"  int32_t U3CU3E__AnonType1_2_GetHashCode_m2936555053_gshared (U3CU3E__AnonType1_2_t3686769320 * __this, const MethodInfo* method);
#define U3CU3E__AnonType1_2_GetHashCode_m2936555053(__this, method) ((  int32_t (*) (U3CU3E__AnonType1_2_t3686769320 *, const MethodInfo*))U3CU3E__AnonType1_2_GetHashCode_m2936555053_gshared)(__this, method)
// System.String <>__AnonType1`2<System.Object,System.Object>::ToString()
extern "C"  String_t* U3CU3E__AnonType1_2_ToString_m2447757191_gshared (U3CU3E__AnonType1_2_t3686769320 * __this, const MethodInfo* method);
#define U3CU3E__AnonType1_2_ToString_m2447757191(__this, method) ((  String_t* (*) (U3CU3E__AnonType1_2_t3686769320 *, const MethodInfo*))U3CU3E__AnonType1_2_ToString_m2447757191_gshared)(__this, method)
