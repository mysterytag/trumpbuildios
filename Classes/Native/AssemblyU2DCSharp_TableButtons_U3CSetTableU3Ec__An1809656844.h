﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// TableButtons
struct TableButtons_t1868573683;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TableButtons/<SetTable>c__AnonStorey15A
struct  U3CSetTableU3Ec__AnonStorey15A_t1809656844  : public Il2CppObject
{
public:
	// System.String TableButtons/<SetTable>c__AnonStorey15A::premiumSku
	String_t* ___premiumSku_0;
	// TableButtons TableButtons/<SetTable>c__AnonStorey15A::<>f__this
	TableButtons_t1868573683 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_premiumSku_0() { return static_cast<int32_t>(offsetof(U3CSetTableU3Ec__AnonStorey15A_t1809656844, ___premiumSku_0)); }
	inline String_t* get_premiumSku_0() const { return ___premiumSku_0; }
	inline String_t** get_address_of_premiumSku_0() { return &___premiumSku_0; }
	inline void set_premiumSku_0(String_t* value)
	{
		___premiumSku_0 = value;
		Il2CppCodeGenWriteBarrier(&___premiumSku_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CSetTableU3Ec__AnonStorey15A_t1809656844, ___U3CU3Ef__this_1)); }
	inline TableButtons_t1868573683 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline TableButtons_t1868573683 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(TableButtons_t1868573683 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
