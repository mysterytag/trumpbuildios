﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Reactor`1/Subscription<UniRx.CollectionAddEvent`1<System.Object>>
struct Subscription_t195768510;

#include "codegen/il2cpp-codegen.h"

// System.Void Reactor`1/Subscription<UniRx.CollectionAddEvent`1<System.Object>>::.ctor()
extern "C"  void Subscription__ctor_m2749810918_gshared (Subscription_t195768510 * __this, const MethodInfo* method);
#define Subscription__ctor_m2749810918(__this, method) ((  void (*) (Subscription_t195768510 *, const MethodInfo*))Subscription__ctor_m2749810918_gshared)(__this, method)
// System.Void Reactor`1/Subscription<UniRx.CollectionAddEvent`1<System.Object>>::Dispose()
extern "C"  void Subscription_Dispose_m1061688227_gshared (Subscription_t195768510 * __this, const MethodInfo* method);
#define Subscription_Dispose_m1061688227(__this, method) ((  void (*) (Subscription_t195768510 *, const MethodInfo*))Subscription_Dispose_m1061688227_gshared)(__this, method)
