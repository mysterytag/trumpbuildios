﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.ICancelable
struct ICancelable_t4109686575;

#include "mscorlib_System_ValueType4014882752.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.CancellationToken
struct  CancellationToken_t1439151560 
{
public:
	// UniRx.ICancelable UniRx.CancellationToken::source
	Il2CppObject * ___source_0;

public:
	inline static int32_t get_offset_of_source_0() { return static_cast<int32_t>(offsetof(CancellationToken_t1439151560, ___source_0)); }
	inline Il2CppObject * get_source_0() const { return ___source_0; }
	inline Il2CppObject ** get_address_of_source_0() { return &___source_0; }
	inline void set_source_0(Il2CppObject * value)
	{
		___source_0 = value;
		Il2CppCodeGenWriteBarrier(&___source_0, value);
	}
};

struct CancellationToken_t1439151560_StaticFields
{
public:
	// UniRx.CancellationToken UniRx.CancellationToken::Empty
	CancellationToken_t1439151560  ___Empty_1;

public:
	inline static int32_t get_offset_of_Empty_1() { return static_cast<int32_t>(offsetof(CancellationToken_t1439151560_StaticFields, ___Empty_1)); }
	inline CancellationToken_t1439151560  get_Empty_1() const { return ___Empty_1; }
	inline CancellationToken_t1439151560 * get_address_of_Empty_1() { return &___Empty_1; }
	inline void set_Empty_1(CancellationToken_t1439151560  value)
	{
		___Empty_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
