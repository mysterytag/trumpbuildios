﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<RefreshPSNAuthToken>c__AnonStoreyC2
struct U3CRefreshPSNAuthTokenU3Ec__AnonStoreyC2_t1119652189;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<RefreshPSNAuthToken>c__AnonStoreyC2::.ctor()
extern "C"  void U3CRefreshPSNAuthTokenU3Ec__AnonStoreyC2__ctor_m3633669782 (U3CRefreshPSNAuthTokenU3Ec__AnonStoreyC2_t1119652189 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<RefreshPSNAuthToken>c__AnonStoreyC2::<>m__AF(PlayFab.CallRequestContainer)
extern "C"  void U3CRefreshPSNAuthTokenU3Ec__AnonStoreyC2_U3CU3Em__AF_m3341173849 (U3CRefreshPSNAuthTokenU3Ec__AnonStoreyC2_t1119652189 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
