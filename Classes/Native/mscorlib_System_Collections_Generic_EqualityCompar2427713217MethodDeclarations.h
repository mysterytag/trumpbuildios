﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<PlayFab.UUnit.Region>
struct DefaultComparer_t2427713217;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PlayFab_UUnit_Region3089759486.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<PlayFab.UUnit.Region>::.ctor()
extern "C"  void DefaultComparer__ctor_m1159631248_gshared (DefaultComparer_t2427713217 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1159631248(__this, method) ((  void (*) (DefaultComparer_t2427713217 *, const MethodInfo*))DefaultComparer__ctor_m1159631248_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<PlayFab.UUnit.Region>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1902195043_gshared (DefaultComparer_t2427713217 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m1902195043(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t2427713217 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m1902195043_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<PlayFab.UUnit.Region>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2490180773_gshared (DefaultComparer_t2427713217 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m2490180773(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t2427713217 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m2490180773_gshared)(__this, ___x0, ___y1, method)
