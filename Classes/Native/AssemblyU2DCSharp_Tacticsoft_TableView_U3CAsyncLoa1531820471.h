﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// Tacticsoft.TableView
struct TableView_t692333993;

#include "mscorlib_System_Object837106420.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Range938821841.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tacticsoft.TableView/<AsyncLoad>c__Iterator20
struct  U3CAsyncLoadU3Ec__Iterator20_t1531820471  : public Il2CppObject
{
public:
	// System.Int32 Tacticsoft.TableView/<AsyncLoad>c__Iterator20::<i>__0
	int32_t ___U3CiU3E__0_0;
	// UnityEngine.SocialPlatforms.Range Tacticsoft.TableView/<AsyncLoad>c__Iterator20::visibleRows
	Range_t938821841  ___visibleRows_1;
	// System.Int32 Tacticsoft.TableView/<AsyncLoad>c__Iterator20::$PC
	int32_t ___U24PC_2;
	// System.Object Tacticsoft.TableView/<AsyncLoad>c__Iterator20::$current
	Il2CppObject * ___U24current_3;
	// UnityEngine.SocialPlatforms.Range Tacticsoft.TableView/<AsyncLoad>c__Iterator20::<$>visibleRows
	Range_t938821841  ___U3CU24U3EvisibleRows_4;
	// Tacticsoft.TableView Tacticsoft.TableView/<AsyncLoad>c__Iterator20::<>f__this
	TableView_t692333993 * ___U3CU3Ef__this_5;

public:
	inline static int32_t get_offset_of_U3CiU3E__0_0() { return static_cast<int32_t>(offsetof(U3CAsyncLoadU3Ec__Iterator20_t1531820471, ___U3CiU3E__0_0)); }
	inline int32_t get_U3CiU3E__0_0() const { return ___U3CiU3E__0_0; }
	inline int32_t* get_address_of_U3CiU3E__0_0() { return &___U3CiU3E__0_0; }
	inline void set_U3CiU3E__0_0(int32_t value)
	{
		___U3CiU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_visibleRows_1() { return static_cast<int32_t>(offsetof(U3CAsyncLoadU3Ec__Iterator20_t1531820471, ___visibleRows_1)); }
	inline Range_t938821841  get_visibleRows_1() const { return ___visibleRows_1; }
	inline Range_t938821841 * get_address_of_visibleRows_1() { return &___visibleRows_1; }
	inline void set_visibleRows_1(Range_t938821841  value)
	{
		___visibleRows_1 = value;
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CAsyncLoadU3Ec__Iterator20_t1531820471, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CAsyncLoadU3Ec__Iterator20_t1531820471, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EvisibleRows_4() { return static_cast<int32_t>(offsetof(U3CAsyncLoadU3Ec__Iterator20_t1531820471, ___U3CU24U3EvisibleRows_4)); }
	inline Range_t938821841  get_U3CU24U3EvisibleRows_4() const { return ___U3CU24U3EvisibleRows_4; }
	inline Range_t938821841 * get_address_of_U3CU24U3EvisibleRows_4() { return &___U3CU24U3EvisibleRows_4; }
	inline void set_U3CU24U3EvisibleRows_4(Range_t938821841  value)
	{
		___U3CU24U3EvisibleRows_4 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_5() { return static_cast<int32_t>(offsetof(U3CAsyncLoadU3Ec__Iterator20_t1531820471, ___U3CU3Ef__this_5)); }
	inline TableView_t692333993 * get_U3CU3Ef__this_5() const { return ___U3CU3Ef__this_5; }
	inline TableView_t692333993 ** get_address_of_U3CU3Ef__this_5() { return &___U3CU3Ef__this_5; }
	inline void set_U3CU3Ef__this_5(TableView_t692333993 * value)
	{
		___U3CU3Ef__this_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
