﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabGoogleCloudMessaging
struct PlayFabGoogleCloudMessaging_t2415988188;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFabNotificationP2037342920.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.PlayFabGoogleCloudMessaging::.ctor()
extern "C"  void PlayFabGoogleCloudMessaging__ctor_m737209847 (PlayFabGoogleCloudMessaging_t2415988188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabGoogleCloudMessaging::Init()
extern "C"  void PlayFabGoogleCloudMessaging_Init_m1360790909 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.PlayFabGoogleCloudMessaging::GetToken()
extern "C"  String_t* PlayFabGoogleCloudMessaging_GetToken_m1051161709 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.PlayFabGoogleCloudMessaging::GetPushCacheData()
extern "C"  String_t* PlayFabGoogleCloudMessaging_GetPushCacheData_m3210769798 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayFabNotificationPackage PlayFab.PlayFabGoogleCloudMessaging::GetPushCache()
extern "C"  PlayFabNotificationPackage_t2037342920  PlayFabGoogleCloudMessaging_GetPushCache_m817942018 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabGoogleCloudMessaging::RegistrationReady(System.Boolean)
extern "C"  void PlayFabGoogleCloudMessaging_RegistrationReady_m4275567158 (Il2CppObject * __this /* static, unused */, bool ___status0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabGoogleCloudMessaging::RegistrationComplete(System.String,System.String)
extern "C"  void PlayFabGoogleCloudMessaging_RegistrationComplete_m2031169375 (Il2CppObject * __this /* static, unused */, String_t* ___id0, String_t* ___error1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabGoogleCloudMessaging::MessageReceived(System.String)
extern "C"  void PlayFabGoogleCloudMessaging_MessageReceived_m3717748485 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
