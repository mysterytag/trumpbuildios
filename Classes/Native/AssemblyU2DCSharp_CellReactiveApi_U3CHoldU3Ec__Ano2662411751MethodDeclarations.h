﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/<Hold>c__AnonStoreyFF`1/<Hold>c__AnonStorey100`1<System.Object>
struct U3CHoldU3Ec__AnonStorey100_1_t2662411751;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void CellReactiveApi/<Hold>c__AnonStoreyFF`1/<Hold>c__AnonStorey100`1<System.Object>::.ctor()
extern "C"  void U3CHoldU3Ec__AnonStorey100_1__ctor_m3705864343_gshared (U3CHoldU3Ec__AnonStorey100_1_t2662411751 * __this, const MethodInfo* method);
#define U3CHoldU3Ec__AnonStorey100_1__ctor_m3705864343(__this, method) ((  void (*) (U3CHoldU3Ec__AnonStorey100_1_t2662411751 *, const MethodInfo*))U3CHoldU3Ec__AnonStorey100_1__ctor_m3705864343_gshared)(__this, method)
// System.Void CellReactiveApi/<Hold>c__AnonStoreyFF`1/<Hold>c__AnonStorey100`1<System.Object>::<>m__192(T)
extern "C"  void U3CHoldU3Ec__AnonStorey100_1_U3CU3Em__192_m685997586_gshared (U3CHoldU3Ec__AnonStorey100_1_t2662411751 * __this, Il2CppObject * ___val0, const MethodInfo* method);
#define U3CHoldU3Ec__AnonStorey100_1_U3CU3Em__192_m685997586(__this, ___val0, method) ((  void (*) (U3CHoldU3Ec__AnonStorey100_1_t2662411751 *, Il2CppObject *, const MethodInfo*))U3CHoldU3Ec__AnonStorey100_1_U3CU3Em__192_m685997586_gshared)(__this, ___val0, method)
