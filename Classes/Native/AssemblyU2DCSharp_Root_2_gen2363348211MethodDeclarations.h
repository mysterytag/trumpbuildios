﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Root`2<System.Object,System.Object>
struct Root_2_t2363348211;

#include "codegen/il2cpp-codegen.h"

// System.Void Root`2<System.Object,System.Object>::.ctor()
extern "C"  void Root_2__ctor_m3188506133_gshared (Root_2_t2363348211 * __this, const MethodInfo* method);
#define Root_2__ctor_m3188506133(__this, method) ((  void (*) (Root_2_t2363348211 *, const MethodInfo*))Root_2__ctor_m3188506133_gshared)(__this, method)
