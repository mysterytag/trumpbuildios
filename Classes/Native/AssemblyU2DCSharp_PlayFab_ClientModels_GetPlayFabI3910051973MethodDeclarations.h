﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetPlayFabIDsFromKongregateIDsResult
struct GetPlayFabIDsFromKongregateIDsResult_t3910051973;
// System.Collections.Generic.List`1<PlayFab.ClientModels.KongregatePlayFabIdPair>
struct List_1_t3782140534;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.GetPlayFabIDsFromKongregateIDsResult::.ctor()
extern "C"  void GetPlayFabIDsFromKongregateIDsResult__ctor_m1522504792 (GetPlayFabIDsFromKongregateIDsResult_t3910051973 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PlayFab.ClientModels.KongregatePlayFabIdPair> PlayFab.ClientModels.GetPlayFabIDsFromKongregateIDsResult::get_Data()
extern "C"  List_1_t3782140534 * GetPlayFabIDsFromKongregateIDsResult_get_Data_m7444893 (GetPlayFabIDsFromKongregateIDsResult_t3910051973 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetPlayFabIDsFromKongregateIDsResult::set_Data(System.Collections.Generic.List`1<PlayFab.ClientModels.KongregatePlayFabIdPair>)
extern "C"  void GetPlayFabIDsFromKongregateIDsResult_set_Data_m719029038 (GetPlayFabIDsFromKongregateIDsResult_t3910051973 * __this, List_1_t3782140534 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
