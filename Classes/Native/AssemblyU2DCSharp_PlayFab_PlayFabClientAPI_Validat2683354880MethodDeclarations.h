﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/ValidateAmazonIAPReceiptResponseCallback
struct ValidateAmazonIAPReceiptResponseCallback_t2683354880;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.ValidateAmazonReceiptRequest
struct ValidateAmazonReceiptRequest_t2422909633;
// PlayFab.ClientModels.ValidateAmazonReceiptResult
struct ValidateAmazonReceiptResult_t528948171;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ValidateAma2422909633.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ValidateAmaz528948171.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/ValidateAmazonIAPReceiptResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void ValidateAmazonIAPReceiptResponseCallback__ctor_m4009005039 (ValidateAmazonIAPReceiptResponseCallback_t2683354880 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/ValidateAmazonIAPReceiptResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.ValidateAmazonReceiptRequest,PlayFab.ClientModels.ValidateAmazonReceiptResult,PlayFab.PlayFabError,System.Object)
extern "C"  void ValidateAmazonIAPReceiptResponseCallback_Invoke_m2405318742 (ValidateAmazonIAPReceiptResponseCallback_t2683354880 * __this, String_t* ___urlPath0, int32_t ___callId1, ValidateAmazonReceiptRequest_t2422909633 * ___request2, ValidateAmazonReceiptResult_t528948171 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_ValidateAmazonIAPReceiptResponseCallback_t2683354880(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, ValidateAmazonReceiptRequest_t2422909633 * ___request2, ValidateAmazonReceiptResult_t528948171 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/ValidateAmazonIAPReceiptResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.ValidateAmazonReceiptRequest,PlayFab.ClientModels.ValidateAmazonReceiptResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ValidateAmazonIAPReceiptResponseCallback_BeginInvoke_m162250063 (ValidateAmazonIAPReceiptResponseCallback_t2683354880 * __this, String_t* ___urlPath0, int32_t ___callId1, ValidateAmazonReceiptRequest_t2422909633 * ___request2, ValidateAmazonReceiptResult_t528948171 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/ValidateAmazonIAPReceiptResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void ValidateAmazonIAPReceiptResponseCallback_EndInvoke_m2452836735 (ValidateAmazonIAPReceiptResponseCallback_t2683354880 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
