﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object[]
struct ObjectU5BU5D_t11523773;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ReflectionUtils
struct  ReflectionUtils_t266409575  : public Il2CppObject
{
public:

public:
};

struct ReflectionUtils_t266409575_StaticFields
{
public:
	// System.Object[] PlayFab.ReflectionUtils::EmptyObjects
	ObjectU5BU5D_t11523773* ___EmptyObjects_0;

public:
	inline static int32_t get_offset_of_EmptyObjects_0() { return static_cast<int32_t>(offsetof(ReflectionUtils_t266409575_StaticFields, ___EmptyObjects_0)); }
	inline ObjectU5BU5D_t11523773* get_EmptyObjects_0() const { return ___EmptyObjects_0; }
	inline ObjectU5BU5D_t11523773** get_address_of_EmptyObjects_0() { return &___EmptyObjects_0; }
	inline void set_EmptyObjects_0(ObjectU5BU5D_t11523773* value)
	{
		___EmptyObjects_0 = value;
		Il2CppCodeGenWriteBarrier(&___EmptyObjects_0, value);
	}
};

struct ReflectionUtils_t266409575_ThreadStaticFields
{
public:
	// System.Object[] PlayFab.ReflectionUtils::_1ObjArray
	ObjectU5BU5D_t11523773* ____1ObjArray_1;

public:
	inline static int32_t get_offset_of__1ObjArray_1() { return static_cast<int32_t>(offsetof(ReflectionUtils_t266409575_ThreadStaticFields, ____1ObjArray_1)); }
	inline ObjectU5BU5D_t11523773* get__1ObjArray_1() const { return ____1ObjArray_1; }
	inline ObjectU5BU5D_t11523773** get_address_of__1ObjArray_1() { return &____1ObjArray_1; }
	inline void set__1ObjArray_1(ObjectU5BU5D_t11523773* value)
	{
		____1ObjArray_1 = value;
		Il2CppCodeGenWriteBarrier(&____1ObjArray_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
