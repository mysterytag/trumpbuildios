﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GooglePlayGames.OurUtils.PlayGamesHelperObject
struct PlayGamesHelperObject_t378230303;
// System.Collections.Generic.List`1<System.Action>
struct List_1_t1234482916;
// System.Action`1<System.Boolean>
struct Action_1_t359458046;
// System.Action`1<System.Action>
struct Action_1_t585976652;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "mscorlib_System_Boolean211005341.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.OurUtils.PlayGamesHelperObject
struct  PlayGamesHelperObject_t378230303  : public MonoBehaviour_t3012272455
{
public:

public:
};

struct PlayGamesHelperObject_t378230303_StaticFields
{
public:
	// GooglePlayGames.OurUtils.PlayGamesHelperObject GooglePlayGames.OurUtils.PlayGamesHelperObject::instance
	PlayGamesHelperObject_t378230303 * ___instance_2;
	// System.Boolean GooglePlayGames.OurUtils.PlayGamesHelperObject::sIsDummy
	bool ___sIsDummy_3;
	// System.Collections.Generic.List`1<System.Action> GooglePlayGames.OurUtils.PlayGamesHelperObject::sQueue
	List_1_t1234482916 * ___sQueue_4;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) GooglePlayGames.OurUtils.PlayGamesHelperObject::sQueueEmpty
	bool ___sQueueEmpty_5;
	// System.Action`1<System.Boolean> GooglePlayGames.OurUtils.PlayGamesHelperObject::sPauseCallback
	Action_1_t359458046 * ___sPauseCallback_6;
	// System.Action`1<System.Boolean> GooglePlayGames.OurUtils.PlayGamesHelperObject::sFocusCallback
	Action_1_t359458046 * ___sFocusCallback_7;
	// System.Action`1<System.Action> GooglePlayGames.OurUtils.PlayGamesHelperObject::<>f__am$cache6
	Action_1_t585976652 * ___U3CU3Ef__amU24cache6_8;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(PlayGamesHelperObject_t378230303_StaticFields, ___instance_2)); }
	inline PlayGamesHelperObject_t378230303 * get_instance_2() const { return ___instance_2; }
	inline PlayGamesHelperObject_t378230303 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(PlayGamesHelperObject_t378230303 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}

	inline static int32_t get_offset_of_sIsDummy_3() { return static_cast<int32_t>(offsetof(PlayGamesHelperObject_t378230303_StaticFields, ___sIsDummy_3)); }
	inline bool get_sIsDummy_3() const { return ___sIsDummy_3; }
	inline bool* get_address_of_sIsDummy_3() { return &___sIsDummy_3; }
	inline void set_sIsDummy_3(bool value)
	{
		___sIsDummy_3 = value;
	}

	inline static int32_t get_offset_of_sQueue_4() { return static_cast<int32_t>(offsetof(PlayGamesHelperObject_t378230303_StaticFields, ___sQueue_4)); }
	inline List_1_t1234482916 * get_sQueue_4() const { return ___sQueue_4; }
	inline List_1_t1234482916 ** get_address_of_sQueue_4() { return &___sQueue_4; }
	inline void set_sQueue_4(List_1_t1234482916 * value)
	{
		___sQueue_4 = value;
		Il2CppCodeGenWriteBarrier(&___sQueue_4, value);
	}

	inline static int32_t get_offset_of_sQueueEmpty_5() { return static_cast<int32_t>(offsetof(PlayGamesHelperObject_t378230303_StaticFields, ___sQueueEmpty_5)); }
	inline bool get_sQueueEmpty_5() const { return ___sQueueEmpty_5; }
	inline bool* get_address_of_sQueueEmpty_5() { return &___sQueueEmpty_5; }
	inline void set_sQueueEmpty_5(bool value)
	{
		___sQueueEmpty_5 = value;
	}

	inline static int32_t get_offset_of_sPauseCallback_6() { return static_cast<int32_t>(offsetof(PlayGamesHelperObject_t378230303_StaticFields, ___sPauseCallback_6)); }
	inline Action_1_t359458046 * get_sPauseCallback_6() const { return ___sPauseCallback_6; }
	inline Action_1_t359458046 ** get_address_of_sPauseCallback_6() { return &___sPauseCallback_6; }
	inline void set_sPauseCallback_6(Action_1_t359458046 * value)
	{
		___sPauseCallback_6 = value;
		Il2CppCodeGenWriteBarrier(&___sPauseCallback_6, value);
	}

	inline static int32_t get_offset_of_sFocusCallback_7() { return static_cast<int32_t>(offsetof(PlayGamesHelperObject_t378230303_StaticFields, ___sFocusCallback_7)); }
	inline Action_1_t359458046 * get_sFocusCallback_7() const { return ___sFocusCallback_7; }
	inline Action_1_t359458046 ** get_address_of_sFocusCallback_7() { return &___sFocusCallback_7; }
	inline void set_sFocusCallback_7(Action_1_t359458046 * value)
	{
		___sFocusCallback_7 = value;
		Il2CppCodeGenWriteBarrier(&___sFocusCallback_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_8() { return static_cast<int32_t>(offsetof(PlayGamesHelperObject_t378230303_StaticFields, ___U3CU3Ef__amU24cache6_8)); }
	inline Action_1_t585976652 * get_U3CU3Ef__amU24cache6_8() const { return ___U3CU3Ef__amU24cache6_8; }
	inline Action_1_t585976652 ** get_address_of_U3CU3Ef__amU24cache6_8() { return &___U3CU3Ef__amU24cache6_8; }
	inline void set_U3CU3Ef__amU24cache6_8(Action_1_t585976652 * value)
	{
		___U3CU3Ef__amU24cache6_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
