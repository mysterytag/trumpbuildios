﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GuiController
struct GuiController_t1495593751;
// <>__AnonType0`4<System.Boolean,System.Boolean,System.Boolean,System.Boolean>
struct U3CU3E__AnonType0_4_t733143067;
// <>__AnonType1`2<System.Boolean,System.Boolean>
struct U3CU3E__AnonType1_2_t2559104484;
// <>__AnonType2`2<System.Boolean,System.Boolean>
struct U3CU3E__AnonType2_2_t3656635463;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"

// System.Void GuiController::.ctor()
extern "C"  void GuiController__ctor_m1058011188 (GuiController_t1495593751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GuiController::PostInject()
extern "C"  void GuiController_PostInject_m2530286945 (GuiController_t1495593751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GuiController::Initialize()
extern "C"  void GuiController_Initialize_m2438160096 (GuiController_t1495593751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// <>__AnonType0`4<System.Boolean,System.Boolean,System.Boolean,System.Boolean> GuiController::<Initialize>m__21(System.Boolean,System.Boolean,System.Boolean,System.Boolean)
extern "C"  U3CU3E__AnonType0_4_t733143067 * GuiController_U3CInitializeU3Em__21_m977944111 (Il2CppObject * __this /* static, unused */, bool ___show0, bool ___tutor1, bool ___bottom2, bool ___hidden3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GuiController::<Initialize>m__23(System.Boolean)
extern "C"  void GuiController_U3CInitializeU3Em__23_m3860484197 (Il2CppObject * __this /* static, unused */, bool ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// <>__AnonType1`2<System.Boolean,System.Boolean> GuiController::<Initialize>m__24(System.Boolean,System.Boolean)
extern "C"  U3CU3E__AnonType1_2_t2559104484 * GuiController_U3CInitializeU3Em__24_m2210892717 (Il2CppObject * __this /* static, unused */, bool ___show0, bool ___hidden1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color GuiController::<Initialize>m__27(System.Int32)
extern "C"  Color_t1588175760  GuiController_U3CInitializeU3Em__27_m2056766666 (Il2CppObject * __this /* static, unused */, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// <>__AnonType2`2<System.Boolean,System.Boolean> GuiController::<Initialize>m__2A(System.Boolean,System.Boolean)
extern "C"  U3CU3E__AnonType2_2_t3656635463 * GuiController_U3CInitializeU3Em__2A_m896144927 (Il2CppObject * __this /* static, unused */, bool ___hidden0, bool ___showTable1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
