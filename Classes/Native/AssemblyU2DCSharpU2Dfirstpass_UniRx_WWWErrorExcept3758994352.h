﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;
// UnityEngine.WWW
struct WWW_t1522972100;

#include "mscorlib_System_Exception1967233988.h"
#include "System_System_Net_HttpStatusCode2736938801.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.WWWErrorException
struct  WWWErrorException_t3758994352  : public Exception_t1967233988
{
public:
	// System.String UniRx.WWWErrorException::<RawErrorMessage>k__BackingField
	String_t* ___U3CRawErrorMessageU3Ek__BackingField_11;
	// System.Boolean UniRx.WWWErrorException::<HasResponse>k__BackingField
	bool ___U3CHasResponseU3Ek__BackingField_12;
	// System.String UniRx.WWWErrorException::<Text>k__BackingField
	String_t* ___U3CTextU3Ek__BackingField_13;
	// System.Net.HttpStatusCode UniRx.WWWErrorException::<StatusCode>k__BackingField
	int32_t ___U3CStatusCodeU3Ek__BackingField_14;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> UniRx.WWWErrorException::<ResponseHeaders>k__BackingField
	Dictionary_2_t2606186806 * ___U3CResponseHeadersU3Ek__BackingField_15;
	// UnityEngine.WWW UniRx.WWWErrorException::<WWW>k__BackingField
	WWW_t1522972100 * ___U3CWWWU3Ek__BackingField_16;

public:
	inline static int32_t get_offset_of_U3CRawErrorMessageU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(WWWErrorException_t3758994352, ___U3CRawErrorMessageU3Ek__BackingField_11)); }
	inline String_t* get_U3CRawErrorMessageU3Ek__BackingField_11() const { return ___U3CRawErrorMessageU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CRawErrorMessageU3Ek__BackingField_11() { return &___U3CRawErrorMessageU3Ek__BackingField_11; }
	inline void set_U3CRawErrorMessageU3Ek__BackingField_11(String_t* value)
	{
		___U3CRawErrorMessageU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CRawErrorMessageU3Ek__BackingField_11, value);
	}

	inline static int32_t get_offset_of_U3CHasResponseU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(WWWErrorException_t3758994352, ___U3CHasResponseU3Ek__BackingField_12)); }
	inline bool get_U3CHasResponseU3Ek__BackingField_12() const { return ___U3CHasResponseU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CHasResponseU3Ek__BackingField_12() { return &___U3CHasResponseU3Ek__BackingField_12; }
	inline void set_U3CHasResponseU3Ek__BackingField_12(bool value)
	{
		___U3CHasResponseU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CTextU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(WWWErrorException_t3758994352, ___U3CTextU3Ek__BackingField_13)); }
	inline String_t* get_U3CTextU3Ek__BackingField_13() const { return ___U3CTextU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CTextU3Ek__BackingField_13() { return &___U3CTextU3Ek__BackingField_13; }
	inline void set_U3CTextU3Ek__BackingField_13(String_t* value)
	{
		___U3CTextU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTextU3Ek__BackingField_13, value);
	}

	inline static int32_t get_offset_of_U3CStatusCodeU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(WWWErrorException_t3758994352, ___U3CStatusCodeU3Ek__BackingField_14)); }
	inline int32_t get_U3CStatusCodeU3Ek__BackingField_14() const { return ___U3CStatusCodeU3Ek__BackingField_14; }
	inline int32_t* get_address_of_U3CStatusCodeU3Ek__BackingField_14() { return &___U3CStatusCodeU3Ek__BackingField_14; }
	inline void set_U3CStatusCodeU3Ek__BackingField_14(int32_t value)
	{
		___U3CStatusCodeU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CResponseHeadersU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(WWWErrorException_t3758994352, ___U3CResponseHeadersU3Ek__BackingField_15)); }
	inline Dictionary_2_t2606186806 * get_U3CResponseHeadersU3Ek__BackingField_15() const { return ___U3CResponseHeadersU3Ek__BackingField_15; }
	inline Dictionary_2_t2606186806 ** get_address_of_U3CResponseHeadersU3Ek__BackingField_15() { return &___U3CResponseHeadersU3Ek__BackingField_15; }
	inline void set_U3CResponseHeadersU3Ek__BackingField_15(Dictionary_2_t2606186806 * value)
	{
		___U3CResponseHeadersU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CResponseHeadersU3Ek__BackingField_15, value);
	}

	inline static int32_t get_offset_of_U3CWWWU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(WWWErrorException_t3758994352, ___U3CWWWU3Ek__BackingField_16)); }
	inline WWW_t1522972100 * get_U3CWWWU3Ek__BackingField_16() const { return ___U3CWWWU3Ek__BackingField_16; }
	inline WWW_t1522972100 ** get_address_of_U3CWWWU3Ek__BackingField_16() { return &___U3CWWWU3Ek__BackingField_16; }
	inline void set_U3CWWWU3Ek__BackingField_16(WWW_t1522972100 * value)
	{
		___U3CWWWU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CWWWU3Ek__BackingField_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
