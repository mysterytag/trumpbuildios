﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ionic.Zlib.SharedUtils
struct SharedUtils_t453409945;
// System.IO.TextReader
struct TextReader_t1534522647;
// System.Byte[]
struct ByteU5BU5D_t58506160;
// System.String
struct String_t;
// System.Char[]
struct CharU5BU5D_t3416858730;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_TextReader1534522647.h"
#include "mscorlib_System_String968488902.h"

// System.Void Ionic.Zlib.SharedUtils::.ctor()
extern "C"  void SharedUtils__ctor_m3448887590 (SharedUtils_t453409945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.SharedUtils::URShift(System.Int32,System.Int32)
extern "C"  int32_t SharedUtils_URShift_m2362891147 (Il2CppObject * __this /* static, unused */, int32_t ___number0, int32_t ___bits1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.SharedUtils::ReadInput(System.IO.TextReader,System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t SharedUtils_ReadInput_m1989428788 (Il2CppObject * __this /* static, unused */, TextReader_t1534522647 * ___sourceTextReader0, ByteU5BU5D_t58506160* ___target1, int32_t ___start2, int32_t ___count3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Ionic.Zlib.SharedUtils::ToByteArray(System.String)
extern "C"  ByteU5BU5D_t58506160* SharedUtils_ToByteArray_m2190406962 (Il2CppObject * __this /* static, unused */, String_t* ___sourceString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char[] Ionic.Zlib.SharedUtils::ToCharArray(System.Byte[])
extern "C"  CharU5BU5D_t3416858730* SharedUtils_ToCharArray_m294713273 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t58506160* ___byteArray0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
