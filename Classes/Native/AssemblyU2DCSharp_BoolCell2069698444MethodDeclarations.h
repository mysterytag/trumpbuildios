﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BoolCell
struct BoolCell_t2069698444;

#include "codegen/il2cpp-codegen.h"

// System.Void BoolCell::.ctor()
extern "C"  void BoolCell__ctor_m924279951 (BoolCell_t2069698444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BoolCell::.ctor(System.Boolean)
extern "C"  void BoolCell__ctor_m1817146310 (BoolCell_t2069698444 * __this, bool ___initial0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
