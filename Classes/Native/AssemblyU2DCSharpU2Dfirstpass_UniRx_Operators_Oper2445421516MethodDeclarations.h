﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<System.Int32>
struct U3CSubscribeU3Ec__AnonStorey5B_t2445421516;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<System.Int32>::.ctor()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B__ctor_m1035516561_gshared (U3CSubscribeU3Ec__AnonStorey5B_t2445421516 * __this, const MethodInfo* method);
#define U3CSubscribeU3Ec__AnonStorey5B__ctor_m1035516561(__this, method) ((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t2445421516 *, const MethodInfo*))U3CSubscribeU3Ec__AnonStorey5B__ctor_m1035516561_gshared)(__this, method)
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<System.Int32>::<>m__73()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m1007224054_gshared (U3CSubscribeU3Ec__AnonStorey5B_t2445421516 * __this, const MethodInfo* method);
#define U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m1007224054(__this, method) ((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t2445421516 *, const MethodInfo*))U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m1007224054_gshared)(__this, method)
