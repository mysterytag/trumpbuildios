﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.WhereObservable`1/Where<System.Object>
struct Where_t2974463400;
// UniRx.Operators.WhereObservable`1<System.Object>
struct WhereObservable_1_t2035666369;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.WhereObservable`1/Where<System.Object>::.ctor(UniRx.Operators.WhereObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Where__ctor_m2773960177_gshared (Where_t2974463400 * __this, WhereObservable_1_t2035666369 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Where__ctor_m2773960177(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Where_t2974463400 *, WhereObservable_1_t2035666369 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Where__ctor_m2773960177_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.WhereObservable`1/Where<System.Object>::OnNext(T)
extern "C"  void Where_OnNext_m1539564119_gshared (Where_t2974463400 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Where_OnNext_m1539564119(__this, ___value0, method) ((  void (*) (Where_t2974463400 *, Il2CppObject *, const MethodInfo*))Where_OnNext_m1539564119_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.WhereObservable`1/Where<System.Object>::OnError(System.Exception)
extern "C"  void Where_OnError_m3233705830_gshared (Where_t2974463400 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Where_OnError_m3233705830(__this, ___error0, method) ((  void (*) (Where_t2974463400 *, Exception_t1967233988 *, const MethodInfo*))Where_OnError_m3233705830_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.WhereObservable`1/Where<System.Object>::OnCompleted()
extern "C"  void Where_OnCompleted_m2363263929_gshared (Where_t2974463400 * __this, const MethodInfo* method);
#define Where_OnCompleted_m2363263929(__this, method) ((  void (*) (Where_t2974463400 *, const MethodInfo*))Where_OnCompleted_m2363263929_gshared)(__this, method)
