﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ZipLatestObserver`1<System.Object>
struct ZipLatestObserver_1_t426653717;
// System.Object
struct Il2CppObject;
// UniRx.Operators.IZipLatestObservable
struct IZipLatestObservable_t130533167;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.ZipLatestObserver`1<System.Object>::.ctor(System.Object,UniRx.Operators.IZipLatestObservable,System.Int32)
extern "C"  void ZipLatestObserver_1__ctor_m3627449811_gshared (ZipLatestObserver_1_t426653717 * __this, Il2CppObject * ___gate0, Il2CppObject * ___parent1, int32_t ___index2, const MethodInfo* method);
#define ZipLatestObserver_1__ctor_m3627449811(__this, ___gate0, ___parent1, ___index2, method) ((  void (*) (ZipLatestObserver_1_t426653717 *, Il2CppObject *, Il2CppObject *, int32_t, const MethodInfo*))ZipLatestObserver_1__ctor_m3627449811_gshared)(__this, ___gate0, ___parent1, ___index2, method)
// T UniRx.Operators.ZipLatestObserver`1<System.Object>::get_Value()
extern "C"  Il2CppObject * ZipLatestObserver_1_get_Value_m3282529970_gshared (ZipLatestObserver_1_t426653717 * __this, const MethodInfo* method);
#define ZipLatestObserver_1_get_Value_m3282529970(__this, method) ((  Il2CppObject * (*) (ZipLatestObserver_1_t426653717 *, const MethodInfo*))ZipLatestObserver_1_get_Value_m3282529970_gshared)(__this, method)
// System.Void UniRx.Operators.ZipLatestObserver`1<System.Object>::OnNext(T)
extern "C"  void ZipLatestObserver_1_OnNext_m2246094483_gshared (ZipLatestObserver_1_t426653717 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ZipLatestObserver_1_OnNext_m2246094483(__this, ___value0, method) ((  void (*) (ZipLatestObserver_1_t426653717 *, Il2CppObject *, const MethodInfo*))ZipLatestObserver_1_OnNext_m2246094483_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.ZipLatestObserver`1<System.Object>::OnError(System.Exception)
extern "C"  void ZipLatestObserver_1_OnError_m2006139810_gshared (ZipLatestObserver_1_t426653717 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ZipLatestObserver_1_OnError_m2006139810(__this, ___error0, method) ((  void (*) (ZipLatestObserver_1_t426653717 *, Exception_t1967233988 *, const MethodInfo*))ZipLatestObserver_1_OnError_m2006139810_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.ZipLatestObserver`1<System.Object>::OnCompleted()
extern "C"  void ZipLatestObserver_1_OnCompleted_m2264979957_gshared (ZipLatestObserver_1_t426653717 * __this, const MethodInfo* method);
#define ZipLatestObserver_1_OnCompleted_m2264979957(__this, method) ((  void (*) (ZipLatestObserver_1_t426653717 *, const MethodInfo*))ZipLatestObserver_1_OnCompleted_m2264979957_gshared)(__this, method)
