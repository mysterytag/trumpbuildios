﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_Enumerat4014815677MethodDeclarations.h"

// System.Void System.Collections.Generic.Stack`1/Enumerator<SimpleJSON.JSONNode>::.ctor(System.Collections.Generic.Stack`1<T>)
#define Enumerator__ctor_m824776589(__this, ___t0, method) ((  void (*) (Enumerator_t3758331890 *, Stack_1_t3151028667 *, const MethodInfo*))Enumerator__ctor_m1003414509_gshared)(__this, ___t0, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<SimpleJSON.JSONNode>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2038998417(__this, method) ((  void (*) (Enumerator_t3758331890 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3054975473_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1/Enumerator<SimpleJSON.JSONNode>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3468132295(__this, method) ((  Il2CppObject * (*) (Enumerator_t3758331890 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2470832103_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<SimpleJSON.JSONNode>::Dispose()
#define Enumerator_Dispose_m2888025158(__this, method) ((  void (*) (Enumerator_t3758331890 *, const MethodInfo*))Enumerator_Dispose_m1634653158_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<SimpleJSON.JSONNode>::MoveNext()
#define Enumerator_MoveNext_m1453938773(__this, method) ((  bool (*) (Enumerator_t3758331890 *, const MethodInfo*))Enumerator_MoveNext_m3012756789_gshared)(__this, method)
// T System.Collections.Generic.Stack`1/Enumerator<SimpleJSON.JSONNode>::get_Current()
#define Enumerator_get_Current_m867911704(__this, method) ((  JSONNode_t580622632 * (*) (Enumerator_t3758331890 *, const MethodInfo*))Enumerator_get_Current_m2483819640_gshared)(__this, method)
