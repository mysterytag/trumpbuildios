﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Action
struct Action_t437523947;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4196218687.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.FinallyObservable`1<System.Object>
struct  FinallyObservable_1_t2652127741  : public OperatorObservableBase_1_t4196218687
{
public:
	// UniRx.IObservable`1<T> UniRx.Operators.FinallyObservable`1::source
	Il2CppObject* ___source_1;
	// System.Action UniRx.Operators.FinallyObservable`1::finallyAction
	Action_t437523947 * ___finallyAction_2;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(FinallyObservable_1_t2652127741, ___source_1)); }
	inline Il2CppObject* get_source_1() const { return ___source_1; }
	inline Il2CppObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(Il2CppObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier(&___source_1, value);
	}

	inline static int32_t get_offset_of_finallyAction_2() { return static_cast<int32_t>(offsetof(FinallyObservable_1_t2652127741, ___finallyAction_2)); }
	inline Action_t437523947 * get_finallyAction_2() const { return ___finallyAction_2; }
	inline Action_t437523947 ** get_address_of_finallyAction_2() { return &___finallyAction_2; }
	inline void set_finallyAction_2(Action_t437523947 * value)
	{
		___finallyAction_2 = value;
		Il2CppCodeGenWriteBarrier(&___finallyAction_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
