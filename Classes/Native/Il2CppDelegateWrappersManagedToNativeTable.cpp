﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"

extern "C" void pinvoke_delegate_wrapper_PrimalityTest_t397689819 ();
extern "C" void pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1233396096 ();
extern "C" void pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1233396097 ();
extern "C" void pinvoke_delegate_wrapper_AppDomainInitializer_t1372758802 ();
extern "C" void pinvoke_delegate_wrapper_Swapper_t4148259466 ();
extern "C" void pinvoke_delegate_wrapper_AssemblyLoadEventHandler_t3698454801 ();
extern "C" void pinvoke_delegate_wrapper_AsyncCallback_t1363551830 ();
extern "C" void pinvoke_delegate_wrapper_EventHandler_t247020293 ();
extern "C" void pinvoke_delegate_wrapper_ReadDelegate_t3831560539 ();
extern "C" void pinvoke_delegate_wrapper_WriteDelegate_t4066867812 ();
extern "C" void pinvoke_delegate_wrapper_AddEventAdapter_t836903542 ();
extern "C" void pinvoke_delegate_wrapper_MemberFilter_t1585748256 ();
extern "C" void pinvoke_delegate_wrapper_GetterAdapter_t943738788 ();
extern "C" void pinvoke_delegate_wrapper_TypeFilter_t2379296192 ();
extern "C" void pinvoke_delegate_wrapper_ResolveEventHandler_t2783314641 ();
extern "C" void pinvoke_delegate_wrapper_CrossContextDelegate_t2286976850 ();
extern "C" void pinvoke_delegate_wrapper_RenewalDelegate_t4057835773 ();
extern "C" void pinvoke_delegate_wrapper_HeaderHandler_t379822999 ();
extern "C" void pinvoke_delegate_wrapper_CallbackHandler_t3576143653 ();
extern "C" void pinvoke_delegate_wrapper_ParameterizedThreadStart_t3844818690 ();
extern "C" void pinvoke_delegate_wrapper_SendOrPostCallback_t692973875 ();
extern "C" void pinvoke_delegate_wrapper_ThreadStart_t2758142267 ();
extern "C" void pinvoke_delegate_wrapper_TimerCallback_t4291881837 ();
extern "C" void pinvoke_delegate_wrapper_WaitCallback_t827025885 ();
extern "C" void pinvoke_delegate_wrapper_WaitOrTimerCallback_t4215077909 ();
extern "C" void pinvoke_delegate_wrapper_UnhandledExceptionEventHandler_t4230172209 ();
extern "C" void pinvoke_delegate_wrapper_PrimalityTest_t397689820 ();
extern "C" void pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1233396098 ();
extern "C" void pinvoke_delegate_wrapper_CertificateSelectionCallback_t3257378130 ();
extern "C" void pinvoke_delegate_wrapper_CertificateValidationCallback_t3726148045 ();
extern "C" void pinvoke_delegate_wrapper_CertificateValidationCallback2_t1582269749 ();
extern "C" void pinvoke_delegate_wrapper_PrivateKeySelectionCallback_t4199006061 ();
extern "C" void pinvoke_delegate_wrapper_AsyncHandshakeDelegate_t751863712 ();
extern "C" void pinvoke_delegate_wrapper_AsyncCompletedEventHandler_t2512468712 ();
extern "C" void pinvoke_delegate_wrapper_ProcessWorkerEventHandler_t2811277981 ();
extern "C" void pinvoke_delegate_wrapper_CollectionChangeEventHandler_t609299751 ();
extern "C" void pinvoke_delegate_wrapper_DoWorkEventHandler_t2061310581 ();
extern "C" void pinvoke_delegate_wrapper_ListChangedEventHandler_t3535012143 ();
extern "C" void pinvoke_delegate_wrapper_ProgressChangedEventHandler_t1174806336 ();
extern "C" void pinvoke_delegate_wrapper_PropertyChangedEventHandler_t2898121368 ();
extern "C" void pinvoke_delegate_wrapper_RefreshEventHandler_t980072532 ();
extern "C" void pinvoke_delegate_wrapper_RunWorkerCompletedEventHandler_t4056931739 ();
extern "C" void pinvoke_delegate_wrapper_ReadMethod_t1307699767 ();
extern "C" void pinvoke_delegate_wrapper_UnmanagedReadOrWrite_t1833047712 ();
extern "C" void pinvoke_delegate_wrapper_WriteMethod_t2894535040 ();
extern "C" void pinvoke_delegate_wrapper_AuthenticationSchemeSelector_t1750022342 ();
extern "C" void pinvoke_delegate_wrapper_BindIPEndPoint_t3524008771 ();
extern "C" void pinvoke_delegate_wrapper_GetHostAddressesCallback_t999064489 ();
extern "C" void pinvoke_delegate_wrapper_GetHostByNameCallback_t2530953093 ();
extern "C" void pinvoke_delegate_wrapper_GetHostEntryIPCallback_t3762708128 ();
extern "C" void pinvoke_delegate_wrapper_GetHostEntryNameCallback_t2141571876 ();
extern "C" void pinvoke_delegate_wrapper_ResolveCallback_t1117196849 ();
extern "C" void pinvoke_delegate_wrapper_DownloadDataCompletedEventHandler_t2491706195 ();
extern "C" void pinvoke_delegate_wrapper_DownloadProgressChangedEventHandler_t3634764377 ();
extern "C" void pinvoke_delegate_wrapper_DownloadStringCompletedEventHandler_t4056081228 ();
extern "C" void pinvoke_delegate_wrapper_GetRequestStreamCallback_t745857726 ();
extern "C" void pinvoke_delegate_wrapper_GetResponseCallback_t786822908 ();
extern "C" void pinvoke_delegate_wrapper_ReadDelegate_t3831560540 ();
extern "C" void pinvoke_delegate_wrapper_WriteDelegate_t4066867813 ();
extern "C" void pinvoke_delegate_wrapper_HttpContinueDelegate_t835016574 ();
extern "C" void pinvoke_delegate_wrapper_OpenReadCompletedEventHandler_t2398543205 ();
extern "C" void pinvoke_delegate_wrapper_OpenWriteCompletedEventHandler_t371723280 ();
extern "C" void pinvoke_delegate_wrapper_LocalCertificateSelectionCallback_t2540829269 ();
extern "C" void pinvoke_delegate_wrapper_RemoteCertificateValidationCallback_t4087051103 ();
extern "C" void pinvoke_delegate_wrapper_SocketAsyncCall_t106740903 ();
extern "C" void pinvoke_delegate_wrapper_UploadDataCompletedEventHandler_t1421445114 ();
extern "C" void pinvoke_delegate_wrapper_UploadFileCompletedEventHandler_t95467880 ();
extern "C" void pinvoke_delegate_wrapper_UploadProgressChangedEventHandler_t1611049280 ();
extern "C" void pinvoke_delegate_wrapper_UploadStringCompletedEventHandler_t2032366131 ();
extern "C" void pinvoke_delegate_wrapper_UploadValuesCompletedEventHandler_t1802287554 ();
extern "C" void pinvoke_delegate_wrapper_MatchAppendEvaluator_t2378778064 ();
extern "C" void pinvoke_delegate_wrapper_CostDelegate_t3008899218 ();
extern "C" void pinvoke_delegate_wrapper_MatchEvaluator_t510977541 ();
extern "C" void pinvoke_delegate_wrapper_Action_t437523947 ();
extern "C" void pinvoke_delegate_wrapper_EaseFunction_t1285385724 ();
extern "C" void pinvoke_delegate_wrapper_TweenCallback_t3786476454 ();
extern "C" void pinvoke_delegate_wrapper_UnreferencedObjectEventHandler_t3729189727 ();
extern "C" void pinvoke_delegate_wrapper_XmlAttributeEventHandler_t1071444467 ();
extern "C" void pinvoke_delegate_wrapper_XmlElementEventHandler_t2494321811 ();
extern "C" void pinvoke_delegate_wrapper_XmlNodeEventHandler_t3820646951 ();
extern "C" void pinvoke_delegate_wrapper_XmlSerializationCollectionFixupCallback_t2598659068 ();
extern "C" void pinvoke_delegate_wrapper_XmlSerializationFixupCallback_t3787251386 ();
extern "C" void pinvoke_delegate_wrapper_XmlSerializationReadCallback_t3159962994 ();
extern "C" void pinvoke_delegate_wrapper_XmlSerializationWriteCallback_t4044254761 ();
extern "C" void pinvoke_delegate_wrapper_XmlNodeChangedEventHandler_t1687630483 ();
extern "C" void pinvoke_delegate_wrapper_CharGetter_t2496392225 ();
extern "C" void pinvoke_delegate_wrapper_UnityAdsDelegate_t2828518919 ();
extern "C" void pinvoke_delegate_wrapper_LogCallback_t3235662729 ();
extern "C" void pinvoke_delegate_wrapper_PCMReaderCallback_t749510018 ();
extern "C" void pinvoke_delegate_wrapper_PCMSetPositionCallback_t2977871350 ();
extern "C" void pinvoke_delegate_wrapper_AudioConfigurationChangeHandler_t1722466426 ();
extern "C" void pinvoke_delegate_wrapper_CameraCallback_t1908940458 ();
extern "C" void pinvoke_delegate_wrapper_WillRenderCanvases_t1153522766 ();
extern "C" void pinvoke_delegate_wrapper_StateChanged_t1076524291 ();
extern "C" void pinvoke_delegate_wrapper_DisplaysUpdatedDelegate_t1240057615 ();
extern "C" void pinvoke_delegate_wrapper_UnityAction_t909267611 ();
extern "C" void pinvoke_delegate_wrapper_FontTextureRebuildCallback_t401089076 ();
extern "C" void pinvoke_delegate_wrapper_WindowFunction_t999919624 ();
extern "C" void pinvoke_delegate_wrapper_SkinChangedDelegate_t914030844 ();
extern "C" void pinvoke_delegate_wrapper_BannerFailedToLoadDelegate_t2071786927 ();
extern "C" void pinvoke_delegate_wrapper_BannerWasClickedDelegate_t1325188367 ();
extern "C" void pinvoke_delegate_wrapper_BannerWasLoadedDelegate_t69910023 ();
extern "C" void pinvoke_delegate_wrapper_InterstitialWasLoadedDelegate_t2963828231 ();
extern "C" void pinvoke_delegate_wrapper_InterstitialWasViewedDelegate_t402653446 ();
extern "C" void pinvoke_delegate_wrapper_IteratorDelegate_t3855630387 ();
extern "C" void pinvoke_delegate_wrapper_ReapplyDrivenProperties_t3247703954 ();
extern "C" void pinvoke_delegate_wrapper_OnValidateInput_t3303221397 ();
extern "C" void pinvoke_delegate_wrapper_ErrorCallback_t582108558 ();
extern "C" void pinvoke_delegate_wrapper_GCMMessageReceived_t1036884951 ();
extern "C" void pinvoke_delegate_wrapper_GCMRegisterComplete_t1540081165 ();
extern "C" void pinvoke_delegate_wrapper_GCMRegisterReady_t3751777615 ();
extern "C" void pinvoke_delegate_wrapper_OnDLLLoaded_t1601980106 ();
extern "C" void pinvoke_delegate_wrapper_HideUnityDelegate_t2364798903 ();
extern "C" void pinvoke_delegate_wrapper_InitDelegate_t1475272884 ();
extern "C" void pinvoke_delegate_wrapper_InvitationReceivedDelegate_t716623425 ();
extern "C" void pinvoke_delegate_wrapper_MatchDelegate_t4292352560 ();
extern "C" void pinvoke_delegate_wrapper_ConflictCallback_t1218325666 ();
extern "C" void pinvoke_delegate_wrapper_OutStringMethod_t201278112 ();
extern "C" void pinvoke_delegate_wrapper_CompressFunc_t1605129574 ();
extern "C" void pinvoke_delegate_wrapper_ExporterFunc_t282942218 ();
extern "C" void pinvoke_delegate_wrapper_ImporterFunc_t3385868859 ();
extern "C" void pinvoke_delegate_wrapper_StateHandler_t1030384025 ();
extern "C" void pinvoke_delegate_wrapper_WrapperFactory_t3863042620 ();
extern "C" void pinvoke_delegate_wrapper_LocalyticsDidDismissInAppMessage_t1872466793 ();
extern "C" void pinvoke_delegate_wrapper_LocalyticsDidDisplayInAppMessage_t3613222305 ();
extern "C" void pinvoke_delegate_wrapper_LocalyticsSessionDidOpen_t115241990 ();
extern "C" void pinvoke_delegate_wrapper_LocalyticsSessionWillClose_t2690468515 ();
extern "C" void pinvoke_delegate_wrapper_LocalyticsSessionWillOpen_t2581002303 ();
extern "C" void pinvoke_delegate_wrapper_LocalyticsWillDismissInAppMessage_t2580912496 ();
extern "C" void pinvoke_delegate_wrapper_LocalyticsWillDisplayInAppMessage_t26700712 ();
extern "C" void pinvoke_delegate_wrapper_ReceiveAnalyticsDelegate_t2804656808 ();
extern "C" void pinvoke_delegate_wrapper_ReceiveMessagingDelegate_t3408128038 ();
extern "C" void pinvoke_delegate_wrapper_AcceptTradeRequestCallback_t3365960504 ();
extern "C" void pinvoke_delegate_wrapper_AcceptTradeResponseCallback_t371666146 ();
extern "C" void pinvoke_delegate_wrapper_AddFriendRequestCallback_t518991733 ();
extern "C" void pinvoke_delegate_wrapper_AddFriendResponseCallback_t2309947461 ();
extern "C" void pinvoke_delegate_wrapper_AddSharedGroupMembersRequestCallback_t3768324820 ();
extern "C" void pinvoke_delegate_wrapper_AddSharedGroupMembersResponseCallback_t4255025350 ();
extern "C" void pinvoke_delegate_wrapper_AddUsernamePasswordRequestCallback_t3276435810 ();
extern "C" void pinvoke_delegate_wrapper_AddUsernamePasswordResponseCallback_t1891367928 ();
extern "C" void pinvoke_delegate_wrapper_AddUserVirtualCurrencyRequestCallback_t2037651236 ();
extern "C" void pinvoke_delegate_wrapper_AddUserVirtualCurrencyResponseCallback_t2143751798 ();
extern "C" void pinvoke_delegate_wrapper_AndroidDevicePushNotificationRegistrationRequestCallback_t642279121 ();
extern "C" void pinvoke_delegate_wrapper_AndroidDevicePushNotificationRegistrationResponseCallback_t1836889193 ();
extern "C" void pinvoke_delegate_wrapper_AttributeInstallRequestCallback_t2957636213 ();
extern "C" void pinvoke_delegate_wrapper_AttributeInstallResponseCallback_t598515013 ();
extern "C" void pinvoke_delegate_wrapper_CancelTradeRequestCallback_t3608790954 ();
extern "C" void pinvoke_delegate_wrapper_CancelTradeResponseCallback_t3604442800 ();
extern "C" void pinvoke_delegate_wrapper_ConfirmPurchaseRequestCallback_t3314372083 ();
extern "C" void pinvoke_delegate_wrapper_ConfirmPurchaseResponseCallback_t3067392391 ();
extern "C" void pinvoke_delegate_wrapper_ConsumeItemRequestCallback_t2404276965 ();
extern "C" void pinvoke_delegate_wrapper_ConsumeItemResponseCallback_t624247509 ();
extern "C" void pinvoke_delegate_wrapper_ConsumePSNEntitlementsRequestCallback_t909350751 ();
extern "C" void pinvoke_delegate_wrapper_ConsumePSNEntitlementsResponseCallback_t1526175131 ();
extern "C" void pinvoke_delegate_wrapper_CreateSharedGroupRequestCallback_t1589511606 ();
extern "C" void pinvoke_delegate_wrapper_CreateSharedGroupResponseCallback_t1136325156 ();
extern "C" void pinvoke_delegate_wrapper_GetAccountInfoRequestCallback_t3830661583 ();
extern "C" void pinvoke_delegate_wrapper_GetAccountInfoResponseCallback_t1892497707 ();
extern "C" void pinvoke_delegate_wrapper_GetAllUsersCharactersRequestCallback_t2380868717 ();
extern "C" void pinvoke_delegate_wrapper_GetAllUsersCharactersResponseCallback_t4193559117 ();
extern "C" void pinvoke_delegate_wrapper_GetCatalogItemsRequestCallback_t822107319 ();
extern "C" void pinvoke_delegate_wrapper_GetCatalogItemsResponseCallback_t3116596035 ();
extern "C" void pinvoke_delegate_wrapper_GetCharacterDataRequestCallback_t1620055895 ();
extern "C" void pinvoke_delegate_wrapper_GetCharacterDataResponseCallback_t2083198115 ();
extern "C" void pinvoke_delegate_wrapper_GetCharacterInventoryRequestCallback_t3802500779 ();
extern "C" void pinvoke_delegate_wrapper_GetCharacterInventoryResponseCallback_t1019512783 ();
extern "C" void pinvoke_delegate_wrapper_GetCharacterLeaderboardRequestCallback_t2066086314 ();
extern "C" void pinvoke_delegate_wrapper_GetCharacterLeaderboardResponseCallback_t3025239216 ();
extern "C" void pinvoke_delegate_wrapper_GetCharacterReadOnlyDataRequestCallback_t2408693909 ();
extern "C" void pinvoke_delegate_wrapper_GetCharacterReadOnlyDataResponseCallback_t761172773 ();
extern "C" void pinvoke_delegate_wrapper_GetCharacterStatisticsRequestCallback_t3212398334 ();
extern "C" void pinvoke_delegate_wrapper_GetCharacterStatisticsResponseCallback_t4201173468 ();
extern "C" void pinvoke_delegate_wrapper_GetCloudScriptUrlRequestCallback_t3206540911 ();
extern "C" void pinvoke_delegate_wrapper_GetCloudScriptUrlResponseCallback_t4019593355 ();
extern "C" void pinvoke_delegate_wrapper_GetContentDownloadUrlRequestCallback_t1572459504 ();
extern "C" void pinvoke_delegate_wrapper_GetContentDownloadUrlResponseCallback_t607709994 ();
extern "C" void pinvoke_delegate_wrapper_GetCurrentGamesRequestCallback_t2884599030 ();
extern "C" void pinvoke_delegate_wrapper_GetCurrentGamesResponseCallback_t2629329636 ();
extern "C" void pinvoke_delegate_wrapper_GetFriendLeaderboardAroundCurrentUserRequestCallback_t1502039974 ();
extern "C" void pinvoke_delegate_wrapper_GetFriendLeaderboardAroundCurrentUserResponseCallback_t2719671860 ();
extern "C" void pinvoke_delegate_wrapper_GetFriendLeaderboardAroundPlayerRequestCallback_t56831965 ();
extern "C" void pinvoke_delegate_wrapper_GetFriendLeaderboardAroundPlayerResponseCallback_t867896541 ();
extern "C" void pinvoke_delegate_wrapper_GetFriendLeaderboardRequestCallback_t853114699 ();
extern "C" void pinvoke_delegate_wrapper_GetFriendLeaderboardResponseCallback_t4077824815 ();
extern "C" void pinvoke_delegate_wrapper_GetFriendsListRequestCallback_t2916847479 ();
extern "C" void pinvoke_delegate_wrapper_GetFriendsListResponseCallback_t3629031555 ();
extern "C" void pinvoke_delegate_wrapper_GetGameServerRegionsRequestCallback_t51967360 ();
extern "C" void pinvoke_delegate_wrapper_GetGameServerRegionsResponseCallback_t717093786 ();
extern "C" void pinvoke_delegate_wrapper_GetLeaderboardAroundCharacterRequestCallback_t1344227519 ();
extern "C" void pinvoke_delegate_wrapper_GetLeaderboardAroundCharacterResponseCallback_t2122453051 ();
extern "C" void pinvoke_delegate_wrapper_GetLeaderboardAroundCurrentUserRequestCallback_t3017392292 ();
extern "C" void pinvoke_delegate_wrapper_GetLeaderboardAroundCurrentUserResponseCallback_t2450953462 ();
extern "C" void pinvoke_delegate_wrapper_GetLeaderboardAroundPlayerRequestCallback_t3396442143 ();
extern "C" void pinvoke_delegate_wrapper_GetLeaderboardAroundPlayerResponseCallback_t1316596955 ();
extern "C" void pinvoke_delegate_wrapper_GetLeaderboardForUserCharactersRequestCallback_t2495424733 ();
extern "C" void pinvoke_delegate_wrapper_GetLeaderboardForUserCharactersResponseCallback_t3449828317 ();
extern "C" void pinvoke_delegate_wrapper_GetLeaderboardRequestCallback_t4230003341 ();
extern "C" void pinvoke_delegate_wrapper_GetLeaderboardResponseCallback_t1387190317 ();
extern "C" void pinvoke_delegate_wrapper_GetPhotonAuthenticationTokenRequestCallback_t1169441349 ();
extern "C" void pinvoke_delegate_wrapper_GetPhotonAuthenticationTokenResponseCallback_t999049077 ();
extern "C" void pinvoke_delegate_wrapper_GetPlayerStatisticsRequestCallback_t222839386 ();
extern "C" void pinvoke_delegate_wrapper_GetPlayerStatisticsResponseCallback_t1719159296 ();
extern "C" void pinvoke_delegate_wrapper_GetPlayerTradesRequestCallback_t658865902 ();
extern "C" void pinvoke_delegate_wrapper_GetPlayerTradesResponseCallback_t2351079404 ();
extern "C" void pinvoke_delegate_wrapper_GetPlayFabIDsFromFacebookIDsRequestCallback_t1008885767 ();
extern "C" void pinvoke_delegate_wrapper_GetPlayFabIDsFromFacebookIDsResponseCallback_t316793331 ();
extern "C" void pinvoke_delegate_wrapper_GetPlayFabIDsFromGameCenterIDsRequestCallback_t3026686120 ();
extern "C" void pinvoke_delegate_wrapper_GetPlayFabIDsFromGameCenterIDsResponseCallback_t2739062130 ();
extern "C" void pinvoke_delegate_wrapper_GetPlayFabIDsFromGoogleIDsRequestCallback_t4137431546 ();
extern "C" void pinvoke_delegate_wrapper_GetPlayFabIDsFromGoogleIDsResponseCallback_t2812431968 ();
extern "C" void pinvoke_delegate_wrapper_GetPlayFabIDsFromKongregateIDsRequestCallback_t1819281052 ();
extern "C" void pinvoke_delegate_wrapper_GetPlayFabIDsFromKongregateIDsResponseCallback_t3964210686 ();
extern "C" void pinvoke_delegate_wrapper_GetPlayFabIDsFromPSNAccountIDsRequestCallback_t2015930851 ();
extern "C" void pinvoke_delegate_wrapper_GetPlayFabIDsFromPSNAccountIDsResponseCallback_t1470419863 ();
extern "C" void pinvoke_delegate_wrapper_GetPlayFabIDsFromSteamIDsRequestCallback_t2305743079 ();
extern "C" void pinvoke_delegate_wrapper_GetPlayFabIDsFromSteamIDsResponseCallback_t1864664339 ();
extern "C" void pinvoke_delegate_wrapper_GetPublisherDataRequestCallback_t2716082596 ();
extern "C" void pinvoke_delegate_wrapper_GetPublisherDataResponseCallback_t1700287478 ();
extern "C" void pinvoke_delegate_wrapper_GetPurchaseRequestCallback_t1297378461 ();
extern "C" void pinvoke_delegate_wrapper_GetPurchaseResponseCallback_t670132253 ();
extern "C" void pinvoke_delegate_wrapper_GetSharedGroupDataRequestCallback_t273675334 ();
extern "C" void pinvoke_delegate_wrapper_GetSharedGroupDataResponseCallback_t3295073684 ();
extern "C" void pinvoke_delegate_wrapper_GetStoreItemsRequestCallback_t580285439 ();
extern "C" void pinvoke_delegate_wrapper_GetStoreItemsResponseCallback_t4210052347 ();
extern "C" void pinvoke_delegate_wrapper_GetTitleDataRequestCallback_t3277276872 ();
extern "C" void pinvoke_delegate_wrapper_GetTitleDataResponseCallback_t1917440850 ();
extern "C" void pinvoke_delegate_wrapper_GetTitleNewsRequestCallback_t2894451839 ();
extern "C" void pinvoke_delegate_wrapper_GetTitleNewsResponseCallback_t2934766715 ();
extern "C" void pinvoke_delegate_wrapper_GetTradeStatusRequestCallback_t88514164 ();
extern "C" void pinvoke_delegate_wrapper_GetTradeStatusResponseCallback_t1850044710 ();
extern "C" void pinvoke_delegate_wrapper_GetUserCombinedInfoRequestCallback_t3104634176 ();
extern "C" void pinvoke_delegate_wrapper_GetUserCombinedInfoResponseCallback_t860484570 ();
extern "C" void pinvoke_delegate_wrapper_GetUserDataRequestCallback_t2154083593 ();
extern "C" void pinvoke_delegate_wrapper_GetUserDataResponseCallback_t1458187569 ();
extern "C" void pinvoke_delegate_wrapper_GetUserInventoryRequestCallback_t2682892345 ();
extern "C" void pinvoke_delegate_wrapper_GetUserInventoryResponseCallback_t671389697 ();
extern "C" void pinvoke_delegate_wrapper_GetUserPublisherDataRequestCallback_t3367155727 ();
extern "C" void pinvoke_delegate_wrapper_GetUserPublisherDataResponseCallback_t408718059 ();
extern "C" void pinvoke_delegate_wrapper_GetUserPublisherReadOnlyDataRequestCallback_t205793613 ();
extern "C" void pinvoke_delegate_wrapper_GetUserPublisherReadOnlyDataResponseCallback_t1190740333 ();
extern "C" void pinvoke_delegate_wrapper_GetUserReadOnlyDataRequestCallback_t2869857351 ();
extern "C" void pinvoke_delegate_wrapper_GetUserReadOnlyDataResponseCallback_t2172337587 ();
extern "C" void pinvoke_delegate_wrapper_GetUserStatisticsRequestCallback_t2864275248 ();
extern "C" void pinvoke_delegate_wrapper_GetUserStatisticsResponseCallback_t1999292394 ();
extern "C" void pinvoke_delegate_wrapper_GrantCharacterToUserRequestCallback_t420782753 ();
extern "C" void pinvoke_delegate_wrapper_GrantCharacterToUserResponseCallback_t3560436377 ();
extern "C" void pinvoke_delegate_wrapper_LinkAndroidDeviceIDRequestCallback_t3969310990 ();
extern "C" void pinvoke_delegate_wrapper_LinkAndroidDeviceIDResponseCallback_t1895662028 ();
extern "C" void pinvoke_delegate_wrapper_LinkCustomIDRequestCallback_t2324547726 ();
extern "C" void pinvoke_delegate_wrapper_LinkCustomIDResponseCallback_t2447608396 ();
extern "C" void pinvoke_delegate_wrapper_LinkFacebookAccountRequestCallback_t3099507495 ();
extern "C" void pinvoke_delegate_wrapper_LinkFacebookAccountResponseCallback_t701557459 ();
extern "C" void pinvoke_delegate_wrapper_LinkGameCenterAccountRequestCallback_t1941561864 ();
extern "C" void pinvoke_delegate_wrapper_LinkGameCenterAccountResponseCallback_t3459948562 ();
extern "C" void pinvoke_delegate_wrapper_LinkGoogleAccountRequestCallback_t2572844250 ();
extern "C" void pinvoke_delegate_wrapper_LinkGoogleAccountResponseCallback_t1554866048 ();
extern "C" void pinvoke_delegate_wrapper_LinkIOSDeviceIDRequestCallback_t2284814000 ();
extern "C" void pinvoke_delegate_wrapper_LinkIOSDeviceIDResponseCallback_t1215862890 ();
extern "C" void pinvoke_delegate_wrapper_LinkKongregateRequestCallback_t2315286943 ();
extern "C" void pinvoke_delegate_wrapper_LinkKongregateResponseCallback_t2160524123 ();
extern "C" void pinvoke_delegate_wrapper_LinkPSNAccountRequestCallback_t3771265976 ();
extern "C" void pinvoke_delegate_wrapper_LinkPSNAccountResponseCallback_t51233890 ();
extern "C" void pinvoke_delegate_wrapper_LinkSteamAccountRequestCallback_t1545210621 ();
extern "C" void pinvoke_delegate_wrapper_LinkSteamAccountResponseCallback_t4057961917 ();
extern "C" void pinvoke_delegate_wrapper_LinkXboxAccountRequestCallback_t2932719604 ();
extern "C" void pinvoke_delegate_wrapper_LinkXboxAccountResponseCallback_t4121067430 ();
extern "C" void pinvoke_delegate_wrapper_LogEventRequestCallback_t1756126014 ();
extern "C" void pinvoke_delegate_wrapper_LogEventResponseCallback_t2006404508 ();
extern "C" void pinvoke_delegate_wrapper_LoginWithAndroidDeviceIDRequestCallback_t638411235 ();
extern "C" void pinvoke_delegate_wrapper_LoginWithAndroidDeviceIDResponseCallback_t1716984727 ();
extern "C" void pinvoke_delegate_wrapper_LoginWithCustomIDRequestCallback_t1450663257 ();
extern "C" void pinvoke_delegate_wrapper_LoginWithCustomIDResponseCallback_t1126993633 ();
extern "C" void pinvoke_delegate_wrapper_LoginWithEmailAddressRequestCallback_t1473970925 ();
extern "C" void pinvoke_delegate_wrapper_LoginWithEmailAddressResponseCallback_t1849531341 ();
extern "C" void pinvoke_delegate_wrapper_LoginWithFacebookRequestCallback_t3296410527 ();
extern "C" void pinvoke_delegate_wrapper_LoginWithFacebookResponseCallback_t2510584155 ();
extern "C" void pinvoke_delegate_wrapper_LoginWithGameCenterRequestCallback_t1923062046 ();
extern "C" void pinvoke_delegate_wrapper_LoginWithGameCenterResponseCallback_t2886454204 ();
extern "C" void pinvoke_delegate_wrapper_LoginWithGoogleAccountRequestCallback_t3199545071 ();
extern "C" void pinvoke_delegate_wrapper_LoginWithGoogleAccountResponseCallback_t3802722315 ();
extern "C" void pinvoke_delegate_wrapper_LoginWithIOSDeviceIDRequestCallback_t189379077 ();
extern "C" void pinvoke_delegate_wrapper_LoginWithIOSDeviceIDResponseCallback_t681889717 ();
extern "C" void pinvoke_delegate_wrapper_LoginWithKongregateRequestCallback_t30934954 ();
extern "C" void pinvoke_delegate_wrapper_LoginWithKongregateResponseCallback_t65089200 ();
extern "C" void pinvoke_delegate_wrapper_LoginWithPlayFabRequestCallback_t102383312 ();
extern "C" void pinvoke_delegate_wrapper_LoginWithPlayFabResponseCallback_t2279988298 ();
extern "C" void pinvoke_delegate_wrapper_LoginWithPSNRequestCallback_t1193662392 ();
extern "C" void pinvoke_delegate_wrapper_LoginWithPSNResponseCallback_t1749901410 ();
extern "C" void pinvoke_delegate_wrapper_LoginWithSteamRequestCallback_t986411731 ();
extern "C" void pinvoke_delegate_wrapper_LoginWithSteamResponseCallback_t3915065511 ();
extern "C" void pinvoke_delegate_wrapper_LoginWithXboxRequestCallback_t3337908466 ();
extern "C" void pinvoke_delegate_wrapper_LoginWithXboxResponseCallback_t3797020264 ();
extern "C" void pinvoke_delegate_wrapper_MatchmakeRequestCallback_t3988812385 ();
extern "C" void pinvoke_delegate_wrapper_MatchmakeResponseCallback_t2500205273 ();
extern "C" void pinvoke_delegate_wrapper_OpenTradeRequestCallback_t2658287482 ();
extern "C" void pinvoke_delegate_wrapper_OpenTradeResponseCallback_t4203606240 ();
extern "C" void pinvoke_delegate_wrapper_PayForPurchaseRequestCallback_t3168214098 ();
extern "C" void pinvoke_delegate_wrapper_PayForPurchaseResponseCallback_t2831462152 ();
extern "C" void pinvoke_delegate_wrapper_PurchaseItemRequestCallback_t647441792 ();
extern "C" void pinvoke_delegate_wrapper_PurchaseItemResponseCallback_t1996931994 ();
extern "C" void pinvoke_delegate_wrapper_RedeemCouponRequestCallback_t2811043474 ();
extern "C" void pinvoke_delegate_wrapper_RedeemCouponResponseCallback_t349107400 ();
extern "C" void pinvoke_delegate_wrapper_RefreshPSNAuthTokenRequestCallback_t2712704179 ();
extern "C" void pinvoke_delegate_wrapper_RefreshPSNAuthTokenResponseCallback_t1595556551 ();
extern "C" void pinvoke_delegate_wrapper_RegisterForIOSPushNotificationRequestCallback_t727986152 ();
extern "C" void pinvoke_delegate_wrapper_RegisterForIOSPushNotificationResponseCallback_t198839858 ();
extern "C" void pinvoke_delegate_wrapper_RegisterPlayFabUserRequestCallback_t2047186841 ();
extern "C" void pinvoke_delegate_wrapper_RegisterPlayFabUserResponseCallback_t2439355553 ();
extern "C" void pinvoke_delegate_wrapper_RemoveFriendRequestCallback_t2222935858 ();
extern "C" void pinvoke_delegate_wrapper_RemoveFriendResponseCallback_t3592607784 ();
extern "C" void pinvoke_delegate_wrapper_RemoveSharedGroupMembersRequestCallback_t1655799569 ();
extern "C" void pinvoke_delegate_wrapper_RemoveSharedGroupMembersResponseCallback_t3191252009 ();
extern "C" void pinvoke_delegate_wrapper_ReportPlayerRequestCallback_t1214398943 ();
extern "C" void pinvoke_delegate_wrapper_ReportPlayerResponseCallback_t2392734491 ();
extern "C" void pinvoke_delegate_wrapper_RestoreIOSPurchasesRequestCallback_t2601568225 ();
extern "C" void pinvoke_delegate_wrapper_RestoreIOSPurchasesResponseCallback_t2445309273 ();
extern "C" void pinvoke_delegate_wrapper_RunCloudScriptRequestCallback_t2578940735 ();
extern "C" void pinvoke_delegate_wrapper_RunCloudScriptResponseCallback_t1743857083 ();
extern "C" void pinvoke_delegate_wrapper_SendAccountRecoveryEmailRequestCallback_t2508225522 ();
extern "C" void pinvoke_delegate_wrapper_SendAccountRecoveryEmailResponseCallback_t3846652776 ();
extern "C" void pinvoke_delegate_wrapper_SetFriendTagsRequestCallback_t1608187131 ();
extern "C" void pinvoke_delegate_wrapper_SetFriendTagsResponseCallback_t1715266431 ();
extern "C" void pinvoke_delegate_wrapper_StartGameRequestCallback_t237042176 ();
extern "C" void pinvoke_delegate_wrapper_StartGameResponseCallback_t2159445786 ();
extern "C" void pinvoke_delegate_wrapper_StartPurchaseRequestCallback_t1079755281 ();
extern "C" void pinvoke_delegate_wrapper_StartPurchaseResponseCallback_t2513748265 ();
extern "C" void pinvoke_delegate_wrapper_SubtractUserVirtualCurrencyRequestCallback_t1466902807 ();
extern "C" void pinvoke_delegate_wrapper_SubtractUserVirtualCurrencyResponseCallback_t1630419683 ();
extern "C" void pinvoke_delegate_wrapper_UnlinkAndroidDeviceIDRequestCallback_t1521451367 ();
extern "C" void pinvoke_delegate_wrapper_UnlinkAndroidDeviceIDResponseCallback_t3321425043 ();
extern "C" void pinvoke_delegate_wrapper_UnlinkCustomIDRequestCallback_t3859278421 ();
extern "C" void pinvoke_delegate_wrapper_UnlinkCustomIDResponseCallback_t2779619685 ();
extern "C" void pinvoke_delegate_wrapper_UnlinkFacebookAccountRequestCallback_t651647872 ();
extern "C" void pinvoke_delegate_wrapper_UnlinkFacebookAccountResponseCallback_t2127320474 ();
extern "C" void pinvoke_delegate_wrapper_UnlinkGameCenterAccountRequestCallback_t3190542369 ();
extern "C" void pinvoke_delegate_wrapper_UnlinkGameCenterAccountResponseCallback_t3523638553 ();
extern "C" void pinvoke_delegate_wrapper_UnlinkGoogleAccountRequestCallback_t4255211379 ();
extern "C" void pinvoke_delegate_wrapper_UnlinkGoogleAccountResponseCallback_t2168639495 ();
extern "C" void pinvoke_delegate_wrapper_UnlinkIOSDeviceIDRequestCallback_t3520082825 ();
extern "C" void pinvoke_delegate_wrapper_UnlinkIOSDeviceIDResponseCallback_t854490801 ();
extern "C" void pinvoke_delegate_wrapper_UnlinkKongregateRequestCallback_t4017702310 ();
extern "C" void pinvoke_delegate_wrapper_UnlinkKongregateResponseCallback_t3395792948 ();
extern "C" void pinvoke_delegate_wrapper_UnlinkPSNAccountRequestCallback_t1178714047 ();
extern "C" void pinvoke_delegate_wrapper_UnlinkPSNAccountResponseCallback_t1286502715 ();
extern "C" void pinvoke_delegate_wrapper_UnlinkSteamAccountRequestCallback_t1183838532 ();
extern "C" void pinvoke_delegate_wrapper_UnlinkSteamAccountResponseCallback_t1445361750 ();
extern "C" void pinvoke_delegate_wrapper_UnlinkXboxAccountRequestCallback_t4167988429 ();
extern "C" void pinvoke_delegate_wrapper_UnlinkXboxAccountResponseCallback_t3759695341 ();
extern "C" void pinvoke_delegate_wrapper_UnlockContainerInstanceRequestCallback_t3270674530 ();
extern "C" void pinvoke_delegate_wrapper_UnlockContainerInstanceResponseCallback_t1712768248 ();
extern "C" void pinvoke_delegate_wrapper_UnlockContainerItemRequestCallback_t3188944804 ();
extern "C" void pinvoke_delegate_wrapper_UnlockContainerItemResponseCallback_t3474114038 ();
extern "C" void pinvoke_delegate_wrapper_UpdateCharacterDataRequestCallback_t2875943562 ();
extern "C" void pinvoke_delegate_wrapper_UpdateCharacterDataResponseCallback_t2361010128 ();
extern "C" void pinvoke_delegate_wrapper_UpdateCharacterStatisticsRequestCallback_t1764241393 ();
extern "C" void pinvoke_delegate_wrapper_UpdateCharacterStatisticsResponseCallback_t2257981257 ();
extern "C" void pinvoke_delegate_wrapper_UpdatePlayerStatisticsRequestCallback_t912211527 ();
extern "C" void pinvoke_delegate_wrapper_UpdatePlayerStatisticsResponseCallback_t1614859187 ();
extern "C" void pinvoke_delegate_wrapper_UpdateSharedGroupDataRequestCallback_t295913145 ();
extern "C" void pinvoke_delegate_wrapper_UpdateSharedGroupDataResponseCallback_t3984445825 ();
extern "C" void pinvoke_delegate_wrapper_UpdateUserDataRequestCallback_t567864822 ();
extern "C" void pinvoke_delegate_wrapper_UpdateUserDataResponseCallback_t3825013220 ();
extern "C" void pinvoke_delegate_wrapper_UpdateUserPublisherDataRequestCallback_t3262855618 ();
extern "C" void pinvoke_delegate_wrapper_UpdateUserPublisherDataResponseCallback_t1470381976 ();
extern "C" void pinvoke_delegate_wrapper_UpdateUserStatisticsRequestCallback_t3142087261 ();
extern "C" void pinvoke_delegate_wrapper_UpdateUserStatisticsResponseCallback_t2021530205 ();
extern "C" void pinvoke_delegate_wrapper_UpdateUserTitleDisplayNameRequestCallback_t567940939 ();
extern "C" void pinvoke_delegate_wrapper_UpdateUserTitleDisplayNameResponseCallback_t3827372847 ();
extern "C" void pinvoke_delegate_wrapper_ValidateAmazonIAPReceiptRequestCallback_t946679130 ();
extern "C" void pinvoke_delegate_wrapper_ValidateAmazonIAPReceiptResponseCallback_t2683354880 ();
extern "C" void pinvoke_delegate_wrapper_ValidateGooglePlayPurchaseRequestCallback_t2036451920 ();
extern "C" void pinvoke_delegate_wrapper_ValidateGooglePlayPurchaseResponseCallback_t2106573002 ();
extern "C" void pinvoke_delegate_wrapper_ValidateIOSReceiptRequestCallback_t3487373939 ();
extern "C" void pinvoke_delegate_wrapper_ValidateIOSReceiptResponseCallback_t4135482631 ();
extern "C" void pinvoke_delegate_wrapper_ConstructorDelegate_t4072949631 ();
extern "C" void pinvoke_delegate_wrapper_GetDelegate_t270123739 ();
extern "C" void pinvoke_delegate_wrapper_SetDelegate_t181543911 ();
extern "C" void pinvoke_delegate_wrapper_UUnitTestDelegate_t1722340944 ();
extern "C" void pinvoke_delegate_wrapper_BrandEngageRequestErrorReceivedHandler_t816797986 ();
extern "C" void pinvoke_delegate_wrapper_BrandEngageRequestResponseReceivedHandler_t263189403 ();
extern "C" void pinvoke_delegate_wrapper_BrandEngageResultHandler_t570975071 ();
extern "C" void pinvoke_delegate_wrapper_DeltaOfCoinsResponseReceivedHandler_t2871420975 ();
extern "C" void pinvoke_delegate_wrapper_ErrorHandler_t1258969084 ();
extern "C" void pinvoke_delegate_wrapper_InterstitialRequestErrorReceivedHandler_t2305716478 ();
extern "C" void pinvoke_delegate_wrapper_InterstitialRequestResponseReceivedHandler_t2506718783 ();
extern "C" void pinvoke_delegate_wrapper_InterstitialStatusCloseHandler_t3952291690 ();
extern "C" void pinvoke_delegate_wrapper_InterstitialStatusErrorHandler_t1250724186 ();
extern "C" void pinvoke_delegate_wrapper_NativeExceptionHandler_t1751116300 ();
extern "C" void pinvoke_delegate_wrapper_OfferWallResultHandler_t3220154273 ();
extern "C" void pinvoke_delegate_wrapper_SuccessfulCurrencyResponseReceivedHandler_t2285112823 ();
extern "C" void pinvoke_delegate_wrapper_BindingCondition_t1528286123 ();
extern const Il2CppMethodPointer g_DelegateWrappersManagedToNative[391] = 
{
	pinvoke_delegate_wrapper_PrimalityTest_t397689819,
	pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1233396096,
	pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1233396097,
	pinvoke_delegate_wrapper_AppDomainInitializer_t1372758802,
	pinvoke_delegate_wrapper_Swapper_t4148259466,
	pinvoke_delegate_wrapper_AssemblyLoadEventHandler_t3698454801,
	pinvoke_delegate_wrapper_AsyncCallback_t1363551830,
	pinvoke_delegate_wrapper_EventHandler_t247020293,
	pinvoke_delegate_wrapper_ReadDelegate_t3831560539,
	pinvoke_delegate_wrapper_WriteDelegate_t4066867812,
	pinvoke_delegate_wrapper_AddEventAdapter_t836903542,
	pinvoke_delegate_wrapper_MemberFilter_t1585748256,
	pinvoke_delegate_wrapper_GetterAdapter_t943738788,
	pinvoke_delegate_wrapper_TypeFilter_t2379296192,
	pinvoke_delegate_wrapper_ResolveEventHandler_t2783314641,
	pinvoke_delegate_wrapper_CrossContextDelegate_t2286976850,
	pinvoke_delegate_wrapper_RenewalDelegate_t4057835773,
	pinvoke_delegate_wrapper_HeaderHandler_t379822999,
	pinvoke_delegate_wrapper_CallbackHandler_t3576143653,
	pinvoke_delegate_wrapper_ParameterizedThreadStart_t3844818690,
	pinvoke_delegate_wrapper_SendOrPostCallback_t692973875,
	pinvoke_delegate_wrapper_ThreadStart_t2758142267,
	pinvoke_delegate_wrapper_TimerCallback_t4291881837,
	pinvoke_delegate_wrapper_WaitCallback_t827025885,
	pinvoke_delegate_wrapper_WaitOrTimerCallback_t4215077909,
	pinvoke_delegate_wrapper_UnhandledExceptionEventHandler_t4230172209,
	pinvoke_delegate_wrapper_PrimalityTest_t397689820,
	pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1233396098,
	pinvoke_delegate_wrapper_CertificateSelectionCallback_t3257378130,
	pinvoke_delegate_wrapper_CertificateValidationCallback_t3726148045,
	pinvoke_delegate_wrapper_CertificateValidationCallback2_t1582269749,
	pinvoke_delegate_wrapper_PrivateKeySelectionCallback_t4199006061,
	pinvoke_delegate_wrapper_AsyncHandshakeDelegate_t751863712,
	pinvoke_delegate_wrapper_AsyncCompletedEventHandler_t2512468712,
	pinvoke_delegate_wrapper_ProcessWorkerEventHandler_t2811277981,
	pinvoke_delegate_wrapper_CollectionChangeEventHandler_t609299751,
	pinvoke_delegate_wrapper_DoWorkEventHandler_t2061310581,
	pinvoke_delegate_wrapper_ListChangedEventHandler_t3535012143,
	pinvoke_delegate_wrapper_ProgressChangedEventHandler_t1174806336,
	pinvoke_delegate_wrapper_PropertyChangedEventHandler_t2898121368,
	pinvoke_delegate_wrapper_RefreshEventHandler_t980072532,
	pinvoke_delegate_wrapper_RunWorkerCompletedEventHandler_t4056931739,
	pinvoke_delegate_wrapper_ReadMethod_t1307699767,
	pinvoke_delegate_wrapper_UnmanagedReadOrWrite_t1833047712,
	pinvoke_delegate_wrapper_WriteMethod_t2894535040,
	pinvoke_delegate_wrapper_AuthenticationSchemeSelector_t1750022342,
	pinvoke_delegate_wrapper_BindIPEndPoint_t3524008771,
	pinvoke_delegate_wrapper_GetHostAddressesCallback_t999064489,
	pinvoke_delegate_wrapper_GetHostByNameCallback_t2530953093,
	pinvoke_delegate_wrapper_GetHostEntryIPCallback_t3762708128,
	pinvoke_delegate_wrapper_GetHostEntryNameCallback_t2141571876,
	pinvoke_delegate_wrapper_ResolveCallback_t1117196849,
	pinvoke_delegate_wrapper_DownloadDataCompletedEventHandler_t2491706195,
	pinvoke_delegate_wrapper_DownloadProgressChangedEventHandler_t3634764377,
	pinvoke_delegate_wrapper_DownloadStringCompletedEventHandler_t4056081228,
	pinvoke_delegate_wrapper_GetRequestStreamCallback_t745857726,
	pinvoke_delegate_wrapper_GetResponseCallback_t786822908,
	pinvoke_delegate_wrapper_ReadDelegate_t3831560540,
	pinvoke_delegate_wrapper_WriteDelegate_t4066867813,
	pinvoke_delegate_wrapper_HttpContinueDelegate_t835016574,
	pinvoke_delegate_wrapper_OpenReadCompletedEventHandler_t2398543205,
	pinvoke_delegate_wrapper_OpenWriteCompletedEventHandler_t371723280,
	pinvoke_delegate_wrapper_LocalCertificateSelectionCallback_t2540829269,
	pinvoke_delegate_wrapper_RemoteCertificateValidationCallback_t4087051103,
	pinvoke_delegate_wrapper_SocketAsyncCall_t106740903,
	pinvoke_delegate_wrapper_UploadDataCompletedEventHandler_t1421445114,
	pinvoke_delegate_wrapper_UploadFileCompletedEventHandler_t95467880,
	pinvoke_delegate_wrapper_UploadProgressChangedEventHandler_t1611049280,
	pinvoke_delegate_wrapper_UploadStringCompletedEventHandler_t2032366131,
	pinvoke_delegate_wrapper_UploadValuesCompletedEventHandler_t1802287554,
	pinvoke_delegate_wrapper_MatchAppendEvaluator_t2378778064,
	pinvoke_delegate_wrapper_CostDelegate_t3008899218,
	pinvoke_delegate_wrapper_MatchEvaluator_t510977541,
	pinvoke_delegate_wrapper_Action_t437523947,
	pinvoke_delegate_wrapper_EaseFunction_t1285385724,
	pinvoke_delegate_wrapper_TweenCallback_t3786476454,
	pinvoke_delegate_wrapper_UnreferencedObjectEventHandler_t3729189727,
	pinvoke_delegate_wrapper_XmlAttributeEventHandler_t1071444467,
	pinvoke_delegate_wrapper_XmlElementEventHandler_t2494321811,
	pinvoke_delegate_wrapper_XmlNodeEventHandler_t3820646951,
	pinvoke_delegate_wrapper_XmlSerializationCollectionFixupCallback_t2598659068,
	pinvoke_delegate_wrapper_XmlSerializationFixupCallback_t3787251386,
	pinvoke_delegate_wrapper_XmlSerializationReadCallback_t3159962994,
	pinvoke_delegate_wrapper_XmlSerializationWriteCallback_t4044254761,
	pinvoke_delegate_wrapper_XmlNodeChangedEventHandler_t1687630483,
	pinvoke_delegate_wrapper_CharGetter_t2496392225,
	pinvoke_delegate_wrapper_UnityAdsDelegate_t2828518919,
	pinvoke_delegate_wrapper_LogCallback_t3235662729,
	pinvoke_delegate_wrapper_PCMReaderCallback_t749510018,
	pinvoke_delegate_wrapper_PCMSetPositionCallback_t2977871350,
	pinvoke_delegate_wrapper_AudioConfigurationChangeHandler_t1722466426,
	pinvoke_delegate_wrapper_CameraCallback_t1908940458,
	pinvoke_delegate_wrapper_WillRenderCanvases_t1153522766,
	pinvoke_delegate_wrapper_StateChanged_t1076524291,
	pinvoke_delegate_wrapper_DisplaysUpdatedDelegate_t1240057615,
	pinvoke_delegate_wrapper_UnityAction_t909267611,
	pinvoke_delegate_wrapper_FontTextureRebuildCallback_t401089076,
	pinvoke_delegate_wrapper_WindowFunction_t999919624,
	pinvoke_delegate_wrapper_SkinChangedDelegate_t914030844,
	pinvoke_delegate_wrapper_BannerFailedToLoadDelegate_t2071786927,
	pinvoke_delegate_wrapper_BannerWasClickedDelegate_t1325188367,
	pinvoke_delegate_wrapper_BannerWasLoadedDelegate_t69910023,
	pinvoke_delegate_wrapper_InterstitialWasLoadedDelegate_t2963828231,
	pinvoke_delegate_wrapper_InterstitialWasViewedDelegate_t402653446,
	pinvoke_delegate_wrapper_IteratorDelegate_t3855630387,
	pinvoke_delegate_wrapper_ReapplyDrivenProperties_t3247703954,
	pinvoke_delegate_wrapper_OnValidateInput_t3303221397,
	pinvoke_delegate_wrapper_ErrorCallback_t582108558,
	pinvoke_delegate_wrapper_GCMMessageReceived_t1036884951,
	pinvoke_delegate_wrapper_GCMRegisterComplete_t1540081165,
	pinvoke_delegate_wrapper_GCMRegisterReady_t3751777615,
	pinvoke_delegate_wrapper_OnDLLLoaded_t1601980106,
	pinvoke_delegate_wrapper_HideUnityDelegate_t2364798903,
	pinvoke_delegate_wrapper_InitDelegate_t1475272884,
	pinvoke_delegate_wrapper_InvitationReceivedDelegate_t716623425,
	pinvoke_delegate_wrapper_MatchDelegate_t4292352560,
	pinvoke_delegate_wrapper_ConflictCallback_t1218325666,
	pinvoke_delegate_wrapper_OutStringMethod_t201278112,
	pinvoke_delegate_wrapper_CompressFunc_t1605129574,
	pinvoke_delegate_wrapper_ExporterFunc_t282942218,
	pinvoke_delegate_wrapper_ImporterFunc_t3385868859,
	pinvoke_delegate_wrapper_StateHandler_t1030384025,
	pinvoke_delegate_wrapper_WrapperFactory_t3863042620,
	pinvoke_delegate_wrapper_LocalyticsDidDismissInAppMessage_t1872466793,
	pinvoke_delegate_wrapper_LocalyticsDidDisplayInAppMessage_t3613222305,
	pinvoke_delegate_wrapper_LocalyticsSessionDidOpen_t115241990,
	pinvoke_delegate_wrapper_LocalyticsSessionWillClose_t2690468515,
	pinvoke_delegate_wrapper_LocalyticsSessionWillOpen_t2581002303,
	pinvoke_delegate_wrapper_LocalyticsWillDismissInAppMessage_t2580912496,
	pinvoke_delegate_wrapper_LocalyticsWillDisplayInAppMessage_t26700712,
	pinvoke_delegate_wrapper_ReceiveAnalyticsDelegate_t2804656808,
	pinvoke_delegate_wrapper_ReceiveMessagingDelegate_t3408128038,
	pinvoke_delegate_wrapper_AcceptTradeRequestCallback_t3365960504,
	pinvoke_delegate_wrapper_AcceptTradeResponseCallback_t371666146,
	pinvoke_delegate_wrapper_AddFriendRequestCallback_t518991733,
	pinvoke_delegate_wrapper_AddFriendResponseCallback_t2309947461,
	pinvoke_delegate_wrapper_AddSharedGroupMembersRequestCallback_t3768324820,
	pinvoke_delegate_wrapper_AddSharedGroupMembersResponseCallback_t4255025350,
	pinvoke_delegate_wrapper_AddUsernamePasswordRequestCallback_t3276435810,
	pinvoke_delegate_wrapper_AddUsernamePasswordResponseCallback_t1891367928,
	pinvoke_delegate_wrapper_AddUserVirtualCurrencyRequestCallback_t2037651236,
	pinvoke_delegate_wrapper_AddUserVirtualCurrencyResponseCallback_t2143751798,
	pinvoke_delegate_wrapper_AndroidDevicePushNotificationRegistrationRequestCallback_t642279121,
	pinvoke_delegate_wrapper_AndroidDevicePushNotificationRegistrationResponseCallback_t1836889193,
	pinvoke_delegate_wrapper_AttributeInstallRequestCallback_t2957636213,
	pinvoke_delegate_wrapper_AttributeInstallResponseCallback_t598515013,
	pinvoke_delegate_wrapper_CancelTradeRequestCallback_t3608790954,
	pinvoke_delegate_wrapper_CancelTradeResponseCallback_t3604442800,
	pinvoke_delegate_wrapper_ConfirmPurchaseRequestCallback_t3314372083,
	pinvoke_delegate_wrapper_ConfirmPurchaseResponseCallback_t3067392391,
	pinvoke_delegate_wrapper_ConsumeItemRequestCallback_t2404276965,
	pinvoke_delegate_wrapper_ConsumeItemResponseCallback_t624247509,
	pinvoke_delegate_wrapper_ConsumePSNEntitlementsRequestCallback_t909350751,
	pinvoke_delegate_wrapper_ConsumePSNEntitlementsResponseCallback_t1526175131,
	pinvoke_delegate_wrapper_CreateSharedGroupRequestCallback_t1589511606,
	pinvoke_delegate_wrapper_CreateSharedGroupResponseCallback_t1136325156,
	pinvoke_delegate_wrapper_GetAccountInfoRequestCallback_t3830661583,
	pinvoke_delegate_wrapper_GetAccountInfoResponseCallback_t1892497707,
	pinvoke_delegate_wrapper_GetAllUsersCharactersRequestCallback_t2380868717,
	pinvoke_delegate_wrapper_GetAllUsersCharactersResponseCallback_t4193559117,
	pinvoke_delegate_wrapper_GetCatalogItemsRequestCallback_t822107319,
	pinvoke_delegate_wrapper_GetCatalogItemsResponseCallback_t3116596035,
	pinvoke_delegate_wrapper_GetCharacterDataRequestCallback_t1620055895,
	pinvoke_delegate_wrapper_GetCharacterDataResponseCallback_t2083198115,
	pinvoke_delegate_wrapper_GetCharacterInventoryRequestCallback_t3802500779,
	pinvoke_delegate_wrapper_GetCharacterInventoryResponseCallback_t1019512783,
	pinvoke_delegate_wrapper_GetCharacterLeaderboardRequestCallback_t2066086314,
	pinvoke_delegate_wrapper_GetCharacterLeaderboardResponseCallback_t3025239216,
	pinvoke_delegate_wrapper_GetCharacterReadOnlyDataRequestCallback_t2408693909,
	pinvoke_delegate_wrapper_GetCharacterReadOnlyDataResponseCallback_t761172773,
	pinvoke_delegate_wrapper_GetCharacterStatisticsRequestCallback_t3212398334,
	pinvoke_delegate_wrapper_GetCharacterStatisticsResponseCallback_t4201173468,
	pinvoke_delegate_wrapper_GetCloudScriptUrlRequestCallback_t3206540911,
	pinvoke_delegate_wrapper_GetCloudScriptUrlResponseCallback_t4019593355,
	pinvoke_delegate_wrapper_GetContentDownloadUrlRequestCallback_t1572459504,
	pinvoke_delegate_wrapper_GetContentDownloadUrlResponseCallback_t607709994,
	pinvoke_delegate_wrapper_GetCurrentGamesRequestCallback_t2884599030,
	pinvoke_delegate_wrapper_GetCurrentGamesResponseCallback_t2629329636,
	pinvoke_delegate_wrapper_GetFriendLeaderboardAroundCurrentUserRequestCallback_t1502039974,
	pinvoke_delegate_wrapper_GetFriendLeaderboardAroundCurrentUserResponseCallback_t2719671860,
	pinvoke_delegate_wrapper_GetFriendLeaderboardAroundPlayerRequestCallback_t56831965,
	pinvoke_delegate_wrapper_GetFriendLeaderboardAroundPlayerResponseCallback_t867896541,
	pinvoke_delegate_wrapper_GetFriendLeaderboardRequestCallback_t853114699,
	pinvoke_delegate_wrapper_GetFriendLeaderboardResponseCallback_t4077824815,
	pinvoke_delegate_wrapper_GetFriendsListRequestCallback_t2916847479,
	pinvoke_delegate_wrapper_GetFriendsListResponseCallback_t3629031555,
	pinvoke_delegate_wrapper_GetGameServerRegionsRequestCallback_t51967360,
	pinvoke_delegate_wrapper_GetGameServerRegionsResponseCallback_t717093786,
	pinvoke_delegate_wrapper_GetLeaderboardAroundCharacterRequestCallback_t1344227519,
	pinvoke_delegate_wrapper_GetLeaderboardAroundCharacterResponseCallback_t2122453051,
	pinvoke_delegate_wrapper_GetLeaderboardAroundCurrentUserRequestCallback_t3017392292,
	pinvoke_delegate_wrapper_GetLeaderboardAroundCurrentUserResponseCallback_t2450953462,
	pinvoke_delegate_wrapper_GetLeaderboardAroundPlayerRequestCallback_t3396442143,
	pinvoke_delegate_wrapper_GetLeaderboardAroundPlayerResponseCallback_t1316596955,
	pinvoke_delegate_wrapper_GetLeaderboardForUserCharactersRequestCallback_t2495424733,
	pinvoke_delegate_wrapper_GetLeaderboardForUserCharactersResponseCallback_t3449828317,
	pinvoke_delegate_wrapper_GetLeaderboardRequestCallback_t4230003341,
	pinvoke_delegate_wrapper_GetLeaderboardResponseCallback_t1387190317,
	pinvoke_delegate_wrapper_GetPhotonAuthenticationTokenRequestCallback_t1169441349,
	pinvoke_delegate_wrapper_GetPhotonAuthenticationTokenResponseCallback_t999049077,
	pinvoke_delegate_wrapper_GetPlayerStatisticsRequestCallback_t222839386,
	pinvoke_delegate_wrapper_GetPlayerStatisticsResponseCallback_t1719159296,
	pinvoke_delegate_wrapper_GetPlayerTradesRequestCallback_t658865902,
	pinvoke_delegate_wrapper_GetPlayerTradesResponseCallback_t2351079404,
	pinvoke_delegate_wrapper_GetPlayFabIDsFromFacebookIDsRequestCallback_t1008885767,
	pinvoke_delegate_wrapper_GetPlayFabIDsFromFacebookIDsResponseCallback_t316793331,
	pinvoke_delegate_wrapper_GetPlayFabIDsFromGameCenterIDsRequestCallback_t3026686120,
	pinvoke_delegate_wrapper_GetPlayFabIDsFromGameCenterIDsResponseCallback_t2739062130,
	pinvoke_delegate_wrapper_GetPlayFabIDsFromGoogleIDsRequestCallback_t4137431546,
	pinvoke_delegate_wrapper_GetPlayFabIDsFromGoogleIDsResponseCallback_t2812431968,
	pinvoke_delegate_wrapper_GetPlayFabIDsFromKongregateIDsRequestCallback_t1819281052,
	pinvoke_delegate_wrapper_GetPlayFabIDsFromKongregateIDsResponseCallback_t3964210686,
	pinvoke_delegate_wrapper_GetPlayFabIDsFromPSNAccountIDsRequestCallback_t2015930851,
	pinvoke_delegate_wrapper_GetPlayFabIDsFromPSNAccountIDsResponseCallback_t1470419863,
	pinvoke_delegate_wrapper_GetPlayFabIDsFromSteamIDsRequestCallback_t2305743079,
	pinvoke_delegate_wrapper_GetPlayFabIDsFromSteamIDsResponseCallback_t1864664339,
	pinvoke_delegate_wrapper_GetPublisherDataRequestCallback_t2716082596,
	pinvoke_delegate_wrapper_GetPublisherDataResponseCallback_t1700287478,
	pinvoke_delegate_wrapper_GetPurchaseRequestCallback_t1297378461,
	pinvoke_delegate_wrapper_GetPurchaseResponseCallback_t670132253,
	pinvoke_delegate_wrapper_GetSharedGroupDataRequestCallback_t273675334,
	pinvoke_delegate_wrapper_GetSharedGroupDataResponseCallback_t3295073684,
	pinvoke_delegate_wrapper_GetStoreItemsRequestCallback_t580285439,
	pinvoke_delegate_wrapper_GetStoreItemsResponseCallback_t4210052347,
	pinvoke_delegate_wrapper_GetTitleDataRequestCallback_t3277276872,
	pinvoke_delegate_wrapper_GetTitleDataResponseCallback_t1917440850,
	pinvoke_delegate_wrapper_GetTitleNewsRequestCallback_t2894451839,
	pinvoke_delegate_wrapper_GetTitleNewsResponseCallback_t2934766715,
	pinvoke_delegate_wrapper_GetTradeStatusRequestCallback_t88514164,
	pinvoke_delegate_wrapper_GetTradeStatusResponseCallback_t1850044710,
	pinvoke_delegate_wrapper_GetUserCombinedInfoRequestCallback_t3104634176,
	pinvoke_delegate_wrapper_GetUserCombinedInfoResponseCallback_t860484570,
	pinvoke_delegate_wrapper_GetUserDataRequestCallback_t2154083593,
	pinvoke_delegate_wrapper_GetUserDataResponseCallback_t1458187569,
	pinvoke_delegate_wrapper_GetUserInventoryRequestCallback_t2682892345,
	pinvoke_delegate_wrapper_GetUserInventoryResponseCallback_t671389697,
	pinvoke_delegate_wrapper_GetUserPublisherDataRequestCallback_t3367155727,
	pinvoke_delegate_wrapper_GetUserPublisherDataResponseCallback_t408718059,
	pinvoke_delegate_wrapper_GetUserPublisherReadOnlyDataRequestCallback_t205793613,
	pinvoke_delegate_wrapper_GetUserPublisherReadOnlyDataResponseCallback_t1190740333,
	pinvoke_delegate_wrapper_GetUserReadOnlyDataRequestCallback_t2869857351,
	pinvoke_delegate_wrapper_GetUserReadOnlyDataResponseCallback_t2172337587,
	pinvoke_delegate_wrapper_GetUserStatisticsRequestCallback_t2864275248,
	pinvoke_delegate_wrapper_GetUserStatisticsResponseCallback_t1999292394,
	pinvoke_delegate_wrapper_GrantCharacterToUserRequestCallback_t420782753,
	pinvoke_delegate_wrapper_GrantCharacterToUserResponseCallback_t3560436377,
	pinvoke_delegate_wrapper_LinkAndroidDeviceIDRequestCallback_t3969310990,
	pinvoke_delegate_wrapper_LinkAndroidDeviceIDResponseCallback_t1895662028,
	pinvoke_delegate_wrapper_LinkCustomIDRequestCallback_t2324547726,
	pinvoke_delegate_wrapper_LinkCustomIDResponseCallback_t2447608396,
	pinvoke_delegate_wrapper_LinkFacebookAccountRequestCallback_t3099507495,
	pinvoke_delegate_wrapper_LinkFacebookAccountResponseCallback_t701557459,
	pinvoke_delegate_wrapper_LinkGameCenterAccountRequestCallback_t1941561864,
	pinvoke_delegate_wrapper_LinkGameCenterAccountResponseCallback_t3459948562,
	pinvoke_delegate_wrapper_LinkGoogleAccountRequestCallback_t2572844250,
	pinvoke_delegate_wrapper_LinkGoogleAccountResponseCallback_t1554866048,
	pinvoke_delegate_wrapper_LinkIOSDeviceIDRequestCallback_t2284814000,
	pinvoke_delegate_wrapper_LinkIOSDeviceIDResponseCallback_t1215862890,
	pinvoke_delegate_wrapper_LinkKongregateRequestCallback_t2315286943,
	pinvoke_delegate_wrapper_LinkKongregateResponseCallback_t2160524123,
	pinvoke_delegate_wrapper_LinkPSNAccountRequestCallback_t3771265976,
	pinvoke_delegate_wrapper_LinkPSNAccountResponseCallback_t51233890,
	pinvoke_delegate_wrapper_LinkSteamAccountRequestCallback_t1545210621,
	pinvoke_delegate_wrapper_LinkSteamAccountResponseCallback_t4057961917,
	pinvoke_delegate_wrapper_LinkXboxAccountRequestCallback_t2932719604,
	pinvoke_delegate_wrapper_LinkXboxAccountResponseCallback_t4121067430,
	pinvoke_delegate_wrapper_LogEventRequestCallback_t1756126014,
	pinvoke_delegate_wrapper_LogEventResponseCallback_t2006404508,
	pinvoke_delegate_wrapper_LoginWithAndroidDeviceIDRequestCallback_t638411235,
	pinvoke_delegate_wrapper_LoginWithAndroidDeviceIDResponseCallback_t1716984727,
	pinvoke_delegate_wrapper_LoginWithCustomIDRequestCallback_t1450663257,
	pinvoke_delegate_wrapper_LoginWithCustomIDResponseCallback_t1126993633,
	pinvoke_delegate_wrapper_LoginWithEmailAddressRequestCallback_t1473970925,
	pinvoke_delegate_wrapper_LoginWithEmailAddressResponseCallback_t1849531341,
	pinvoke_delegate_wrapper_LoginWithFacebookRequestCallback_t3296410527,
	pinvoke_delegate_wrapper_LoginWithFacebookResponseCallback_t2510584155,
	pinvoke_delegate_wrapper_LoginWithGameCenterRequestCallback_t1923062046,
	pinvoke_delegate_wrapper_LoginWithGameCenterResponseCallback_t2886454204,
	pinvoke_delegate_wrapper_LoginWithGoogleAccountRequestCallback_t3199545071,
	pinvoke_delegate_wrapper_LoginWithGoogleAccountResponseCallback_t3802722315,
	pinvoke_delegate_wrapper_LoginWithIOSDeviceIDRequestCallback_t189379077,
	pinvoke_delegate_wrapper_LoginWithIOSDeviceIDResponseCallback_t681889717,
	pinvoke_delegate_wrapper_LoginWithKongregateRequestCallback_t30934954,
	pinvoke_delegate_wrapper_LoginWithKongregateResponseCallback_t65089200,
	pinvoke_delegate_wrapper_LoginWithPlayFabRequestCallback_t102383312,
	pinvoke_delegate_wrapper_LoginWithPlayFabResponseCallback_t2279988298,
	pinvoke_delegate_wrapper_LoginWithPSNRequestCallback_t1193662392,
	pinvoke_delegate_wrapper_LoginWithPSNResponseCallback_t1749901410,
	pinvoke_delegate_wrapper_LoginWithSteamRequestCallback_t986411731,
	pinvoke_delegate_wrapper_LoginWithSteamResponseCallback_t3915065511,
	pinvoke_delegate_wrapper_LoginWithXboxRequestCallback_t3337908466,
	pinvoke_delegate_wrapper_LoginWithXboxResponseCallback_t3797020264,
	pinvoke_delegate_wrapper_MatchmakeRequestCallback_t3988812385,
	pinvoke_delegate_wrapper_MatchmakeResponseCallback_t2500205273,
	pinvoke_delegate_wrapper_OpenTradeRequestCallback_t2658287482,
	pinvoke_delegate_wrapper_OpenTradeResponseCallback_t4203606240,
	pinvoke_delegate_wrapper_PayForPurchaseRequestCallback_t3168214098,
	pinvoke_delegate_wrapper_PayForPurchaseResponseCallback_t2831462152,
	pinvoke_delegate_wrapper_PurchaseItemRequestCallback_t647441792,
	pinvoke_delegate_wrapper_PurchaseItemResponseCallback_t1996931994,
	pinvoke_delegate_wrapper_RedeemCouponRequestCallback_t2811043474,
	pinvoke_delegate_wrapper_RedeemCouponResponseCallback_t349107400,
	pinvoke_delegate_wrapper_RefreshPSNAuthTokenRequestCallback_t2712704179,
	pinvoke_delegate_wrapper_RefreshPSNAuthTokenResponseCallback_t1595556551,
	pinvoke_delegate_wrapper_RegisterForIOSPushNotificationRequestCallback_t727986152,
	pinvoke_delegate_wrapper_RegisterForIOSPushNotificationResponseCallback_t198839858,
	pinvoke_delegate_wrapper_RegisterPlayFabUserRequestCallback_t2047186841,
	pinvoke_delegate_wrapper_RegisterPlayFabUserResponseCallback_t2439355553,
	pinvoke_delegate_wrapper_RemoveFriendRequestCallback_t2222935858,
	pinvoke_delegate_wrapper_RemoveFriendResponseCallback_t3592607784,
	pinvoke_delegate_wrapper_RemoveSharedGroupMembersRequestCallback_t1655799569,
	pinvoke_delegate_wrapper_RemoveSharedGroupMembersResponseCallback_t3191252009,
	pinvoke_delegate_wrapper_ReportPlayerRequestCallback_t1214398943,
	pinvoke_delegate_wrapper_ReportPlayerResponseCallback_t2392734491,
	pinvoke_delegate_wrapper_RestoreIOSPurchasesRequestCallback_t2601568225,
	pinvoke_delegate_wrapper_RestoreIOSPurchasesResponseCallback_t2445309273,
	pinvoke_delegate_wrapper_RunCloudScriptRequestCallback_t2578940735,
	pinvoke_delegate_wrapper_RunCloudScriptResponseCallback_t1743857083,
	pinvoke_delegate_wrapper_SendAccountRecoveryEmailRequestCallback_t2508225522,
	pinvoke_delegate_wrapper_SendAccountRecoveryEmailResponseCallback_t3846652776,
	pinvoke_delegate_wrapper_SetFriendTagsRequestCallback_t1608187131,
	pinvoke_delegate_wrapper_SetFriendTagsResponseCallback_t1715266431,
	pinvoke_delegate_wrapper_StartGameRequestCallback_t237042176,
	pinvoke_delegate_wrapper_StartGameResponseCallback_t2159445786,
	pinvoke_delegate_wrapper_StartPurchaseRequestCallback_t1079755281,
	pinvoke_delegate_wrapper_StartPurchaseResponseCallback_t2513748265,
	pinvoke_delegate_wrapper_SubtractUserVirtualCurrencyRequestCallback_t1466902807,
	pinvoke_delegate_wrapper_SubtractUserVirtualCurrencyResponseCallback_t1630419683,
	pinvoke_delegate_wrapper_UnlinkAndroidDeviceIDRequestCallback_t1521451367,
	pinvoke_delegate_wrapper_UnlinkAndroidDeviceIDResponseCallback_t3321425043,
	pinvoke_delegate_wrapper_UnlinkCustomIDRequestCallback_t3859278421,
	pinvoke_delegate_wrapper_UnlinkCustomIDResponseCallback_t2779619685,
	pinvoke_delegate_wrapper_UnlinkFacebookAccountRequestCallback_t651647872,
	pinvoke_delegate_wrapper_UnlinkFacebookAccountResponseCallback_t2127320474,
	pinvoke_delegate_wrapper_UnlinkGameCenterAccountRequestCallback_t3190542369,
	pinvoke_delegate_wrapper_UnlinkGameCenterAccountResponseCallback_t3523638553,
	pinvoke_delegate_wrapper_UnlinkGoogleAccountRequestCallback_t4255211379,
	pinvoke_delegate_wrapper_UnlinkGoogleAccountResponseCallback_t2168639495,
	pinvoke_delegate_wrapper_UnlinkIOSDeviceIDRequestCallback_t3520082825,
	pinvoke_delegate_wrapper_UnlinkIOSDeviceIDResponseCallback_t854490801,
	pinvoke_delegate_wrapper_UnlinkKongregateRequestCallback_t4017702310,
	pinvoke_delegate_wrapper_UnlinkKongregateResponseCallback_t3395792948,
	pinvoke_delegate_wrapper_UnlinkPSNAccountRequestCallback_t1178714047,
	pinvoke_delegate_wrapper_UnlinkPSNAccountResponseCallback_t1286502715,
	pinvoke_delegate_wrapper_UnlinkSteamAccountRequestCallback_t1183838532,
	pinvoke_delegate_wrapper_UnlinkSteamAccountResponseCallback_t1445361750,
	pinvoke_delegate_wrapper_UnlinkXboxAccountRequestCallback_t4167988429,
	pinvoke_delegate_wrapper_UnlinkXboxAccountResponseCallback_t3759695341,
	pinvoke_delegate_wrapper_UnlockContainerInstanceRequestCallback_t3270674530,
	pinvoke_delegate_wrapper_UnlockContainerInstanceResponseCallback_t1712768248,
	pinvoke_delegate_wrapper_UnlockContainerItemRequestCallback_t3188944804,
	pinvoke_delegate_wrapper_UnlockContainerItemResponseCallback_t3474114038,
	pinvoke_delegate_wrapper_UpdateCharacterDataRequestCallback_t2875943562,
	pinvoke_delegate_wrapper_UpdateCharacterDataResponseCallback_t2361010128,
	pinvoke_delegate_wrapper_UpdateCharacterStatisticsRequestCallback_t1764241393,
	pinvoke_delegate_wrapper_UpdateCharacterStatisticsResponseCallback_t2257981257,
	pinvoke_delegate_wrapper_UpdatePlayerStatisticsRequestCallback_t912211527,
	pinvoke_delegate_wrapper_UpdatePlayerStatisticsResponseCallback_t1614859187,
	pinvoke_delegate_wrapper_UpdateSharedGroupDataRequestCallback_t295913145,
	pinvoke_delegate_wrapper_UpdateSharedGroupDataResponseCallback_t3984445825,
	pinvoke_delegate_wrapper_UpdateUserDataRequestCallback_t567864822,
	pinvoke_delegate_wrapper_UpdateUserDataResponseCallback_t3825013220,
	pinvoke_delegate_wrapper_UpdateUserPublisherDataRequestCallback_t3262855618,
	pinvoke_delegate_wrapper_UpdateUserPublisherDataResponseCallback_t1470381976,
	pinvoke_delegate_wrapper_UpdateUserStatisticsRequestCallback_t3142087261,
	pinvoke_delegate_wrapper_UpdateUserStatisticsResponseCallback_t2021530205,
	pinvoke_delegate_wrapper_UpdateUserTitleDisplayNameRequestCallback_t567940939,
	pinvoke_delegate_wrapper_UpdateUserTitleDisplayNameResponseCallback_t3827372847,
	pinvoke_delegate_wrapper_ValidateAmazonIAPReceiptRequestCallback_t946679130,
	pinvoke_delegate_wrapper_ValidateAmazonIAPReceiptResponseCallback_t2683354880,
	pinvoke_delegate_wrapper_ValidateGooglePlayPurchaseRequestCallback_t2036451920,
	pinvoke_delegate_wrapper_ValidateGooglePlayPurchaseResponseCallback_t2106573002,
	pinvoke_delegate_wrapper_ValidateIOSReceiptRequestCallback_t3487373939,
	pinvoke_delegate_wrapper_ValidateIOSReceiptResponseCallback_t4135482631,
	pinvoke_delegate_wrapper_ConstructorDelegate_t4072949631,
	pinvoke_delegate_wrapper_GetDelegate_t270123739,
	pinvoke_delegate_wrapper_SetDelegate_t181543911,
	pinvoke_delegate_wrapper_UUnitTestDelegate_t1722340944,
	pinvoke_delegate_wrapper_BrandEngageRequestErrorReceivedHandler_t816797986,
	pinvoke_delegate_wrapper_BrandEngageRequestResponseReceivedHandler_t263189403,
	pinvoke_delegate_wrapper_BrandEngageResultHandler_t570975071,
	pinvoke_delegate_wrapper_DeltaOfCoinsResponseReceivedHandler_t2871420975,
	pinvoke_delegate_wrapper_ErrorHandler_t1258969084,
	pinvoke_delegate_wrapper_InterstitialRequestErrorReceivedHandler_t2305716478,
	pinvoke_delegate_wrapper_InterstitialRequestResponseReceivedHandler_t2506718783,
	pinvoke_delegate_wrapper_InterstitialStatusCloseHandler_t3952291690,
	pinvoke_delegate_wrapper_InterstitialStatusErrorHandler_t1250724186,
	pinvoke_delegate_wrapper_NativeExceptionHandler_t1751116300,
	pinvoke_delegate_wrapper_OfferWallResultHandler_t3220154273,
	pinvoke_delegate_wrapper_SuccessfulCurrencyResponseReceivedHandler_t2285112823,
	pinvoke_delegate_wrapper_BindingCondition_t1528286123,
};
