﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen2135783352MethodDeclarations.h"

// System.Void System.Func`2<System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>,System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m1718635948(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t1776554224 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m3944524044_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>,System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>>::Invoke(T)
#define Func_2_Invoke_m1563831666(__this, ___arg10, method) ((  EventHandler_1_t3942771990 * (*) (Func_2_t1776554224 *, EventHandler_1_t3942771990 *, const MethodInfo*))Func_2_Invoke_m1924616534_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>,System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m4252354849(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t1776554224 *, EventHandler_1_t3942771990 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m4281703301_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>,System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m3367588258(__this, ___result0, method) ((  EventHandler_1_t3942771990 * (*) (Func_2_t1776554224 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m4118168638_gshared)(__this, ___result0, method)
