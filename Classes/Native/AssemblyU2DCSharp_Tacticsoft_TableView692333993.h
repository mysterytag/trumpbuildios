﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Tacticsoft.TableView/CellVisibilityChangeEvent
struct CellVisibilityChangeEvent_t4208228854;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// Tacticsoft.ITableViewDataSource
struct ITableViewDataSource_t1629295493;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t1048578170;
// UnityEngine.UI.LayoutElement
struct LayoutElement_t2655817307;
// System.Single[]
struct SingleU5BU5D_t1219431280;
// System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>
struct Dictionary_2_t1267230538;
// UnityEngine.RectTransform
struct RectTransform_t3317474837;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>>
struct Dictionary_2_t4154247205;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Range938821841.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tacticsoft.TableView
struct  TableView_t692333993  : public MonoBehaviour_t3012272455
{
public:
	// Tacticsoft.TableView/CellVisibilityChangeEvent Tacticsoft.TableView::onCellVisibilityChanged
	CellVisibilityChangeEvent_t4208228854 * ___onCellVisibilityChanged_2;
	// Zenject.DiContainer Tacticsoft.TableView::DiContainer
	DiContainer_t2383114449 * ___DiContainer_3;
	// System.Single Tacticsoft.TableView::padding
	float ___padding_4;
	// System.Single Tacticsoft.TableView::wider
	float ___wider_5;
	// Tacticsoft.ITableViewDataSource Tacticsoft.TableView::m_dataSource
	Il2CppObject * ___m_dataSource_6;
	// System.Boolean Tacticsoft.TableView::m_requiresReload
	bool ___m_requiresReload_7;
	// System.Single Tacticsoft.TableView::m_verticalLayoutGroup
	float ___m_verticalLayoutGroup_8;
	// UnityEngine.UI.ScrollRect Tacticsoft.TableView::m_scrollRect
	ScrollRect_t1048578170 * ___m_scrollRect_9;
	// UnityEngine.UI.LayoutElement Tacticsoft.TableView::m_topPadding
	LayoutElement_t2655817307 * ___m_topPadding_10;
	// UnityEngine.UI.LayoutElement Tacticsoft.TableView::m_bottomPadding
	LayoutElement_t2655817307 * ___m_bottomPadding_11;
	// System.Single[] Tacticsoft.TableView::m_rowHeights
	SingleU5BU5D_t1219431280* ___m_rowHeights_12;
	// System.Single[] Tacticsoft.TableView::m_cumulativeRowHeights
	SingleU5BU5D_t1219431280* ___m_cumulativeRowHeights_13;
	// System.Int32 Tacticsoft.TableView::m_cleanCumulativeIndex
	int32_t ___m_cleanCumulativeIndex_14;
	// System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell> Tacticsoft.TableView::m_visibleCells
	Dictionary_2_t1267230538 * ___m_visibleCells_15;
	// UnityEngine.SocialPlatforms.Range Tacticsoft.TableView::m_visibleRowRange
	Range_t938821841  ___m_visibleRowRange_16;
	// UnityEngine.RectTransform Tacticsoft.TableView::m_reusableCellContainer
	RectTransform_t3317474837 * ___m_reusableCellContainer_17;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>> Tacticsoft.TableView::m_reusableCells
	Dictionary_2_t4154247205 * ___m_reusableCells_18;
	// System.Single Tacticsoft.TableView::m_paddingY
	float ___m_paddingY_19;
	// System.Single Tacticsoft.TableView::m_scrollY
	float ___m_scrollY_20;
	// System.Boolean Tacticsoft.TableView::m_requiresRefresh
	bool ___m_requiresRefresh_21;
	// System.Single[] Tacticsoft.TableView::sectors
	SingleU5BU5D_t1219431280* ___sectors_22;
	// System.Boolean Tacticsoft.TableView::<isEmpty>k__BackingField
	bool ___U3CisEmptyU3Ek__BackingField_23;

public:
	inline static int32_t get_offset_of_onCellVisibilityChanged_2() { return static_cast<int32_t>(offsetof(TableView_t692333993, ___onCellVisibilityChanged_2)); }
	inline CellVisibilityChangeEvent_t4208228854 * get_onCellVisibilityChanged_2() const { return ___onCellVisibilityChanged_2; }
	inline CellVisibilityChangeEvent_t4208228854 ** get_address_of_onCellVisibilityChanged_2() { return &___onCellVisibilityChanged_2; }
	inline void set_onCellVisibilityChanged_2(CellVisibilityChangeEvent_t4208228854 * value)
	{
		___onCellVisibilityChanged_2 = value;
		Il2CppCodeGenWriteBarrier(&___onCellVisibilityChanged_2, value);
	}

	inline static int32_t get_offset_of_DiContainer_3() { return static_cast<int32_t>(offsetof(TableView_t692333993, ___DiContainer_3)); }
	inline DiContainer_t2383114449 * get_DiContainer_3() const { return ___DiContainer_3; }
	inline DiContainer_t2383114449 ** get_address_of_DiContainer_3() { return &___DiContainer_3; }
	inline void set_DiContainer_3(DiContainer_t2383114449 * value)
	{
		___DiContainer_3 = value;
		Il2CppCodeGenWriteBarrier(&___DiContainer_3, value);
	}

	inline static int32_t get_offset_of_padding_4() { return static_cast<int32_t>(offsetof(TableView_t692333993, ___padding_4)); }
	inline float get_padding_4() const { return ___padding_4; }
	inline float* get_address_of_padding_4() { return &___padding_4; }
	inline void set_padding_4(float value)
	{
		___padding_4 = value;
	}

	inline static int32_t get_offset_of_wider_5() { return static_cast<int32_t>(offsetof(TableView_t692333993, ___wider_5)); }
	inline float get_wider_5() const { return ___wider_5; }
	inline float* get_address_of_wider_5() { return &___wider_5; }
	inline void set_wider_5(float value)
	{
		___wider_5 = value;
	}

	inline static int32_t get_offset_of_m_dataSource_6() { return static_cast<int32_t>(offsetof(TableView_t692333993, ___m_dataSource_6)); }
	inline Il2CppObject * get_m_dataSource_6() const { return ___m_dataSource_6; }
	inline Il2CppObject ** get_address_of_m_dataSource_6() { return &___m_dataSource_6; }
	inline void set_m_dataSource_6(Il2CppObject * value)
	{
		___m_dataSource_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_dataSource_6, value);
	}

	inline static int32_t get_offset_of_m_requiresReload_7() { return static_cast<int32_t>(offsetof(TableView_t692333993, ___m_requiresReload_7)); }
	inline bool get_m_requiresReload_7() const { return ___m_requiresReload_7; }
	inline bool* get_address_of_m_requiresReload_7() { return &___m_requiresReload_7; }
	inline void set_m_requiresReload_7(bool value)
	{
		___m_requiresReload_7 = value;
	}

	inline static int32_t get_offset_of_m_verticalLayoutGroup_8() { return static_cast<int32_t>(offsetof(TableView_t692333993, ___m_verticalLayoutGroup_8)); }
	inline float get_m_verticalLayoutGroup_8() const { return ___m_verticalLayoutGroup_8; }
	inline float* get_address_of_m_verticalLayoutGroup_8() { return &___m_verticalLayoutGroup_8; }
	inline void set_m_verticalLayoutGroup_8(float value)
	{
		___m_verticalLayoutGroup_8 = value;
	}

	inline static int32_t get_offset_of_m_scrollRect_9() { return static_cast<int32_t>(offsetof(TableView_t692333993, ___m_scrollRect_9)); }
	inline ScrollRect_t1048578170 * get_m_scrollRect_9() const { return ___m_scrollRect_9; }
	inline ScrollRect_t1048578170 ** get_address_of_m_scrollRect_9() { return &___m_scrollRect_9; }
	inline void set_m_scrollRect_9(ScrollRect_t1048578170 * value)
	{
		___m_scrollRect_9 = value;
		Il2CppCodeGenWriteBarrier(&___m_scrollRect_9, value);
	}

	inline static int32_t get_offset_of_m_topPadding_10() { return static_cast<int32_t>(offsetof(TableView_t692333993, ___m_topPadding_10)); }
	inline LayoutElement_t2655817307 * get_m_topPadding_10() const { return ___m_topPadding_10; }
	inline LayoutElement_t2655817307 ** get_address_of_m_topPadding_10() { return &___m_topPadding_10; }
	inline void set_m_topPadding_10(LayoutElement_t2655817307 * value)
	{
		___m_topPadding_10 = value;
		Il2CppCodeGenWriteBarrier(&___m_topPadding_10, value);
	}

	inline static int32_t get_offset_of_m_bottomPadding_11() { return static_cast<int32_t>(offsetof(TableView_t692333993, ___m_bottomPadding_11)); }
	inline LayoutElement_t2655817307 * get_m_bottomPadding_11() const { return ___m_bottomPadding_11; }
	inline LayoutElement_t2655817307 ** get_address_of_m_bottomPadding_11() { return &___m_bottomPadding_11; }
	inline void set_m_bottomPadding_11(LayoutElement_t2655817307 * value)
	{
		___m_bottomPadding_11 = value;
		Il2CppCodeGenWriteBarrier(&___m_bottomPadding_11, value);
	}

	inline static int32_t get_offset_of_m_rowHeights_12() { return static_cast<int32_t>(offsetof(TableView_t692333993, ___m_rowHeights_12)); }
	inline SingleU5BU5D_t1219431280* get_m_rowHeights_12() const { return ___m_rowHeights_12; }
	inline SingleU5BU5D_t1219431280** get_address_of_m_rowHeights_12() { return &___m_rowHeights_12; }
	inline void set_m_rowHeights_12(SingleU5BU5D_t1219431280* value)
	{
		___m_rowHeights_12 = value;
		Il2CppCodeGenWriteBarrier(&___m_rowHeights_12, value);
	}

	inline static int32_t get_offset_of_m_cumulativeRowHeights_13() { return static_cast<int32_t>(offsetof(TableView_t692333993, ___m_cumulativeRowHeights_13)); }
	inline SingleU5BU5D_t1219431280* get_m_cumulativeRowHeights_13() const { return ___m_cumulativeRowHeights_13; }
	inline SingleU5BU5D_t1219431280** get_address_of_m_cumulativeRowHeights_13() { return &___m_cumulativeRowHeights_13; }
	inline void set_m_cumulativeRowHeights_13(SingleU5BU5D_t1219431280* value)
	{
		___m_cumulativeRowHeights_13 = value;
		Il2CppCodeGenWriteBarrier(&___m_cumulativeRowHeights_13, value);
	}

	inline static int32_t get_offset_of_m_cleanCumulativeIndex_14() { return static_cast<int32_t>(offsetof(TableView_t692333993, ___m_cleanCumulativeIndex_14)); }
	inline int32_t get_m_cleanCumulativeIndex_14() const { return ___m_cleanCumulativeIndex_14; }
	inline int32_t* get_address_of_m_cleanCumulativeIndex_14() { return &___m_cleanCumulativeIndex_14; }
	inline void set_m_cleanCumulativeIndex_14(int32_t value)
	{
		___m_cleanCumulativeIndex_14 = value;
	}

	inline static int32_t get_offset_of_m_visibleCells_15() { return static_cast<int32_t>(offsetof(TableView_t692333993, ___m_visibleCells_15)); }
	inline Dictionary_2_t1267230538 * get_m_visibleCells_15() const { return ___m_visibleCells_15; }
	inline Dictionary_2_t1267230538 ** get_address_of_m_visibleCells_15() { return &___m_visibleCells_15; }
	inline void set_m_visibleCells_15(Dictionary_2_t1267230538 * value)
	{
		___m_visibleCells_15 = value;
		Il2CppCodeGenWriteBarrier(&___m_visibleCells_15, value);
	}

	inline static int32_t get_offset_of_m_visibleRowRange_16() { return static_cast<int32_t>(offsetof(TableView_t692333993, ___m_visibleRowRange_16)); }
	inline Range_t938821841  get_m_visibleRowRange_16() const { return ___m_visibleRowRange_16; }
	inline Range_t938821841 * get_address_of_m_visibleRowRange_16() { return &___m_visibleRowRange_16; }
	inline void set_m_visibleRowRange_16(Range_t938821841  value)
	{
		___m_visibleRowRange_16 = value;
	}

	inline static int32_t get_offset_of_m_reusableCellContainer_17() { return static_cast<int32_t>(offsetof(TableView_t692333993, ___m_reusableCellContainer_17)); }
	inline RectTransform_t3317474837 * get_m_reusableCellContainer_17() const { return ___m_reusableCellContainer_17; }
	inline RectTransform_t3317474837 ** get_address_of_m_reusableCellContainer_17() { return &___m_reusableCellContainer_17; }
	inline void set_m_reusableCellContainer_17(RectTransform_t3317474837 * value)
	{
		___m_reusableCellContainer_17 = value;
		Il2CppCodeGenWriteBarrier(&___m_reusableCellContainer_17, value);
	}

	inline static int32_t get_offset_of_m_reusableCells_18() { return static_cast<int32_t>(offsetof(TableView_t692333993, ___m_reusableCells_18)); }
	inline Dictionary_2_t4154247205 * get_m_reusableCells_18() const { return ___m_reusableCells_18; }
	inline Dictionary_2_t4154247205 ** get_address_of_m_reusableCells_18() { return &___m_reusableCells_18; }
	inline void set_m_reusableCells_18(Dictionary_2_t4154247205 * value)
	{
		___m_reusableCells_18 = value;
		Il2CppCodeGenWriteBarrier(&___m_reusableCells_18, value);
	}

	inline static int32_t get_offset_of_m_paddingY_19() { return static_cast<int32_t>(offsetof(TableView_t692333993, ___m_paddingY_19)); }
	inline float get_m_paddingY_19() const { return ___m_paddingY_19; }
	inline float* get_address_of_m_paddingY_19() { return &___m_paddingY_19; }
	inline void set_m_paddingY_19(float value)
	{
		___m_paddingY_19 = value;
	}

	inline static int32_t get_offset_of_m_scrollY_20() { return static_cast<int32_t>(offsetof(TableView_t692333993, ___m_scrollY_20)); }
	inline float get_m_scrollY_20() const { return ___m_scrollY_20; }
	inline float* get_address_of_m_scrollY_20() { return &___m_scrollY_20; }
	inline void set_m_scrollY_20(float value)
	{
		___m_scrollY_20 = value;
	}

	inline static int32_t get_offset_of_m_requiresRefresh_21() { return static_cast<int32_t>(offsetof(TableView_t692333993, ___m_requiresRefresh_21)); }
	inline bool get_m_requiresRefresh_21() const { return ___m_requiresRefresh_21; }
	inline bool* get_address_of_m_requiresRefresh_21() { return &___m_requiresRefresh_21; }
	inline void set_m_requiresRefresh_21(bool value)
	{
		___m_requiresRefresh_21 = value;
	}

	inline static int32_t get_offset_of_sectors_22() { return static_cast<int32_t>(offsetof(TableView_t692333993, ___sectors_22)); }
	inline SingleU5BU5D_t1219431280* get_sectors_22() const { return ___sectors_22; }
	inline SingleU5BU5D_t1219431280** get_address_of_sectors_22() { return &___sectors_22; }
	inline void set_sectors_22(SingleU5BU5D_t1219431280* value)
	{
		___sectors_22 = value;
		Il2CppCodeGenWriteBarrier(&___sectors_22, value);
	}

	inline static int32_t get_offset_of_U3CisEmptyU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(TableView_t692333993, ___U3CisEmptyU3Ek__BackingField_23)); }
	inline bool get_U3CisEmptyU3Ek__BackingField_23() const { return ___U3CisEmptyU3Ek__BackingField_23; }
	inline bool* get_address_of_U3CisEmptyU3Ek__BackingField_23() { return &___U3CisEmptyU3Ek__BackingField_23; }
	inline void set_U3CisEmptyU3Ek__BackingField_23(bool value)
	{
		___U3CisEmptyU3Ek__BackingField_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
