﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BioProcessor`1<System.Object>
struct BioProcessor_1_t2501661084;
// System.IDisposable
struct IDisposable_t1628921374;
// ILiving
struct ILiving_t2639664210;
// System.Func`2<System.Object,System.Object>
struct Func_2_t2135783352;
// BioProcessor`1/Instruction<System.Object>
struct Instruction_t1241108407;
// System.Object
struct Il2CppObject;
// IStream`1<System.Object>
struct IStream_1_t1389797411;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void BioProcessor`1<System.Object>::.ctor()
extern "C"  void BioProcessor_1__ctor_m3935699562_gshared (BioProcessor_1_t2501661084 * __this, const MethodInfo* method);
#define BioProcessor_1__ctor_m3935699562(__this, method) ((  void (*) (BioProcessor_1_t2501661084 *, const MethodInfo*))BioProcessor_1__ctor_m3935699562_gshared)(__this, method)
// System.IDisposable BioProcessor`1<System.Object>::Affect(ILiving,System.Func`2<T,T>)
extern "C"  Il2CppObject * BioProcessor_1_Affect_m3514211993_gshared (BioProcessor_1_t2501661084 * __this, Il2CppObject * ___intruder0, Func_2_t2135783352 * ___processor1, const MethodInfo* method);
#define BioProcessor_1_Affect_m3514211993(__this, ___intruder0, ___processor1, method) ((  Il2CppObject * (*) (BioProcessor_1_t2501661084 *, Il2CppObject *, Func_2_t2135783352 *, const MethodInfo*))BioProcessor_1_Affect_m3514211993_gshared)(__this, ___intruder0, ___processor1, method)
// System.IDisposable BioProcessor`1<System.Object>::AffectUnsafe(System.Func`2<T,T>)
extern "C"  Il2CppObject * BioProcessor_1_AffectUnsafe_m2299920505_gshared (BioProcessor_1_t2501661084 * __this, Func_2_t2135783352 * ___processor0, const MethodInfo* method);
#define BioProcessor_1_AffectUnsafe_m2299920505(__this, ___processor0, method) ((  Il2CppObject * (*) (BioProcessor_1_t2501661084 *, Func_2_t2135783352 *, const MethodInfo*))BioProcessor_1_AffectUnsafe_m2299920505_gshared)(__this, ___processor0, method)
// System.Void BioProcessor`1<System.Object>::ClearInstruction(BioProcessor`1/Instruction<T>)
extern "C"  void BioProcessor_1_ClearInstruction_m2200475659_gshared (BioProcessor_1_t2501661084 * __this, Instruction_t1241108407 * ___intr0, const MethodInfo* method);
#define BioProcessor_1_ClearInstruction_m2200475659(__this, ___intr0, method) ((  void (*) (BioProcessor_1_t2501661084 *, Instruction_t1241108407 *, const MethodInfo*))BioProcessor_1_ClearInstruction_m2200475659_gshared)(__this, ___intr0, method)
// T BioProcessor`1<System.Object>::Process(T)
extern "C"  Il2CppObject * BioProcessor_1_Process_m2053800744_gshared (BioProcessor_1_t2501661084 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define BioProcessor_1_Process_m2053800744(__this, ___value0, method) ((  Il2CppObject * (*) (BioProcessor_1_t2501661084 *, Il2CppObject *, const MethodInfo*))BioProcessor_1_Process_m2053800744_gshared)(__this, ___value0, method)
// IStream`1<T> BioProcessor`1<System.Object>::get_output()
extern "C"  Il2CppObject* BioProcessor_1_get_output_m3847914643_gshared (BioProcessor_1_t2501661084 * __this, const MethodInfo* method);
#define BioProcessor_1_get_output_m3847914643(__this, method) ((  Il2CppObject* (*) (BioProcessor_1_t2501661084 *, const MethodInfo*))BioProcessor_1_get_output_m3847914643_gshared)(__this, method)
