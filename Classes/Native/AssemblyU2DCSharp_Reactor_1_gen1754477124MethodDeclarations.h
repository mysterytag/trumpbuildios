﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Reactor_1_gen1676326883MethodDeclarations.h"

// System.Void Reactor`1<UniRx.CollectionAddEvent`1<SettingsButton>>::.ctor()
#define Reactor_1__ctor_m1613036259(__this, method) ((  void (*) (Reactor_1_t1754477124 *, const MethodInfo*))Reactor_1__ctor_m3042049264_gshared)(__this, method)
// System.Void Reactor`1<UniRx.CollectionAddEvent`1<SettingsButton>>::React(T)
#define Reactor_1_React_m2296178590(__this, ___t0, method) ((  void (*) (Reactor_1_t1754477124 *, CollectionAddEvent_1_t2494672228 , const MethodInfo*))Reactor_1_React_m3645908785_gshared)(__this, ___t0, method)
// System.Void Reactor`1<UniRx.CollectionAddEvent`1<SettingsButton>>::TransactionUnpackReact(T,System.Int32)
#define Reactor_1_TransactionUnpackReact_m541143337(__this, ___t0, ___i1, method) ((  void (*) (Reactor_1_t1754477124 *, CollectionAddEvent_1_t2494672228 , int32_t, const MethodInfo*))Reactor_1_TransactionUnpackReact_m2959551868_gshared)(__this, ___t0, ___i1, method)
// System.Boolean Reactor`1<UniRx.CollectionAddEvent`1<SettingsButton>>::Empty()
#define Reactor_1_Empty_m2608673218(__this, method) ((  bool (*) (Reactor_1_t1754477124 *, const MethodInfo*))Reactor_1_Empty_m1349981671_gshared)(__this, method)
// System.IDisposable Reactor`1<UniRx.CollectionAddEvent`1<SettingsButton>>::AddReaction(System.Action`1<T>,Priority)
#define Reactor_1_AddReaction_m4289295236(__this, ___reaction0, ___priority1, method) ((  Il2CppObject * (*) (Reactor_1_t1754477124 *, Action_1_t2643124933 *, int32_t, const MethodInfo*))Reactor_1_AddReaction_m243157725_gshared)(__this, ___reaction0, ___priority1, method)
// System.Void Reactor`1<UniRx.CollectionAddEvent`1<SettingsButton>>::UpgradeToMultyPriority()
#define Reactor_1_UpgradeToMultyPriority_m1124416023(__this, method) ((  void (*) (Reactor_1_t1754477124 *, const MethodInfo*))Reactor_1_UpgradeToMultyPriority_m1822749610_gshared)(__this, method)
// System.Void Reactor`1<UniRx.CollectionAddEvent`1<SettingsButton>>::AddToMultipriority(System.Action`1<T>,Priority)
#define Reactor_1_AddToMultipriority_m3701568320(__this, ___reaction0, ___p1, method) ((  void (*) (Reactor_1_t1754477124 *, Action_1_t2643124933 *, int32_t, const MethodInfo*))Reactor_1_AddToMultipriority_m625386573_gshared)(__this, ___reaction0, ___p1, method)
// System.Void Reactor`1<UniRx.CollectionAddEvent`1<SettingsButton>>::RemoveReaction(System.Action`1<T>,Priority)
#define Reactor_1_RemoveReaction_m2417110068(__this, ___reaction0, ___p1, method) ((  void (*) (Reactor_1_t1754477124 *, Action_1_t2643124933 *, int32_t, const MethodInfo*))Reactor_1_RemoveReaction_m3790334913_gshared)(__this, ___reaction0, ___p1, method)
// System.Void Reactor`1<UniRx.CollectionAddEvent`1<SettingsButton>>::Clear()
#define Reactor_1_Clear_m3314136846(__this, method) ((  void (*) (Reactor_1_t1754477124 *, const MethodInfo*))Reactor_1_Clear_m448182555_gshared)(__this, method)
