﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Object>
struct OperatorObserverBase_2_t1929422447;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Object>::.ctor(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void OperatorObserverBase_2__ctor_m1945314260_gshared (OperatorObserverBase_2_t1929422447 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define OperatorObserverBase_2__ctor_m1945314260(__this, ___observer0, ___cancel1, method) ((  void (*) (OperatorObserverBase_2_t1929422447 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))OperatorObserverBase_2__ctor_m1945314260_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Object>::Dispose()
extern "C"  void OperatorObserverBase_2_Dispose_m1235051158_gshared (OperatorObserverBase_2_t1929422447 * __this, const MethodInfo* method);
#define OperatorObserverBase_2_Dispose_m1235051158(__this, method) ((  void (*) (OperatorObserverBase_2_t1929422447 *, const MethodInfo*))OperatorObserverBase_2_Dispose_m1235051158_gshared)(__this, method)
