﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.ZenUtil
struct ZenUtil_t3149524122;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Action`1<Zenject.DiContainer>
struct Action_1_t2531567154;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// Zenject.DiContainer
struct DiContainer_t2383114449;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449.h"

// System.Void Zenject.ZenUtil::.ctor()
extern "C"  void ZenUtil__ctor_m3462249477 (ZenUtil_t3149524122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.ZenUtil::IsNull(System.Object)
extern "C"  bool ZenUtil_IsNull_m3822553240 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.ZenUtil::LoadScene(System.String)
extern "C"  void ZenUtil_LoadScene_m2898507705 (Il2CppObject * __this /* static, unused */, String_t* ___levelName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.ZenUtil::LoadScene(System.String,System.Action`1<Zenject.DiContainer>)
extern "C"  void ZenUtil_LoadScene_m3251234358 (Il2CppObject * __this /* static, unused */, String_t* ___levelName0, Action_1_t2531567154 * ___preBindings1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.ZenUtil::LoadScene(System.String,System.Action`1<Zenject.DiContainer>,System.Action`1<Zenject.DiContainer>)
extern "C"  void ZenUtil_LoadScene_m3590255065 (Il2CppObject * __this /* static, unused */, String_t* ___levelName0, Action_1_t2531567154 * ___preBindings1, Action_1_t2531567154 * ___postBindings2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.ZenUtil::LoadSceneAdditive(System.String)
extern "C"  void ZenUtil_LoadSceneAdditive_m4086475341 (Il2CppObject * __this /* static, unused */, String_t* ___levelName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.ZenUtil::LoadSceneAdditive(System.String,System.Action`1<Zenject.DiContainer>)
extern "C"  void ZenUtil_LoadSceneAdditive_m2676801570 (Il2CppObject * __this /* static, unused */, String_t* ___levelName0, Action_1_t2531567154 * ___preBindings1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.ZenUtil::LoadSceneAdditive(System.String,System.Action`1<Zenject.DiContainer>,System.Action`1<Zenject.DiContainer>)
extern "C"  void ZenUtil_LoadSceneAdditive_m2288524141 (Il2CppObject * __this /* static, unused */, String_t* ___levelName0, Action_1_t2531567154 * ___preBindings1, Action_1_t2531567154 * ___postBindings2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.ZenUtil::LoadSceneInternal(System.String,System.Boolean,System.Action`1<Zenject.DiContainer>,System.Action`1<Zenject.DiContainer>)
extern "C"  void ZenUtil_LoadSceneInternal_m1988330721 (Il2CppObject * __this /* static, unused */, String_t* ___levelName0, bool ___isAdditive1, Action_1_t2531567154 * ___preBindings2, Action_1_t2531567154 * ___postBindings3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Zenject.ZenUtil::LoadSceneAdditiveWithContainer(System.String,Zenject.DiContainer)
extern "C"  Il2CppObject * ZenUtil_LoadSceneAdditiveWithContainer_m1312014993 (Il2CppObject * __this /* static, unused */, String_t* ___levelName0, DiContainer_t2383114449 * ___parentContainer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
