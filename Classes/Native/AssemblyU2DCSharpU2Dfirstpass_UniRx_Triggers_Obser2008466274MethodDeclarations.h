﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Triggers.ObservableDestroyTrigger
struct ObservableDestroyTrigger_t2008466274;
// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Triggers.ObservableDestroyTrigger::.ctor()
extern "C"  void ObservableDestroyTrigger__ctor_m1105262875 (ObservableDestroyTrigger_t2008466274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableDestroyTrigger::OnDestroy()
extern "C"  void ObservableDestroyTrigger_OnDestroy_m2550843540 (ObservableDestroyTrigger_t2008466274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Triggers.ObservableDestroyTrigger::OnDestroyAsObservable()
extern "C"  Il2CppObject* ObservableDestroyTrigger_OnDestroyAsObservable_m172061137 (ObservableDestroyTrigger_t2008466274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
