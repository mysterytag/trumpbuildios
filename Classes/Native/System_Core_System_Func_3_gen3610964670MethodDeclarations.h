﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_3_gen1892209229MethodDeclarations.h"

// System.Void System.Func`3<System.String,System.String,<>__AnonType0`2<System.String,System.String>>::.ctor(System.Object,System.IntPtr)
#define Func_3__ctor_m2612643231(__this, ___object0, ___method1, method) ((  void (*) (Func_3_t3610964670 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_3__ctor_m4028180665_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`3<System.String,System.String,<>__AnonType0`2<System.String,System.String>>::Invoke(T1,T2)
#define Func_3_Invoke_m1050432658(__this, ___arg10, ___arg21, method) ((  U3CU3E__AnonType0_2_t1370999997 * (*) (Func_3_t3610964670 *, String_t*, String_t*, const MethodInfo*))Func_3_Invoke_m4101486064_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Func`3<System.String,System.String,<>__AnonType0`2<System.String,System.String>>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Func_3_BeginInvoke_m1065799191(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Func_3_t3610964670 *, String_t*, String_t*, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_3_BeginInvoke_m69685489_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// TResult System.Func`3<System.String,System.String,<>__AnonType0`2<System.String,System.String>>::EndInvoke(System.IAsyncResult)
#define Func_3_EndInvoke_m2129049737(__this, ___result0, method) ((  U3CU3E__AnonType0_2_t1370999997 * (*) (Func_3_t3610964670 *, Il2CppObject *, const MethodInfo*))Func_3_EndInvoke_m1319796459_gshared)(__this, ___result0, method)
