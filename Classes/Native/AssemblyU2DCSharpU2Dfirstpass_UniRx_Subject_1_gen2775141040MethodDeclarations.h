﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1<System.Object>
struct Subject_1_t2775141040;
// System.Exception
struct Exception_t1967233988;
// System.Object
struct Il2CppObject;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.Subject`1<System.Object>::.ctor()
extern "C"  void Subject_1__ctor_m3752525464_gshared (Subject_1_t2775141040 * __this, const MethodInfo* method);
#define Subject_1__ctor_m3752525464(__this, method) ((  void (*) (Subject_1_t2775141040 *, const MethodInfo*))Subject_1__ctor_m3752525464_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<System.Object>::get_HasObservers()
extern "C"  bool Subject_1_get_HasObservers_m2673131508_gshared (Subject_1_t2775141040 * __this, const MethodInfo* method);
#define Subject_1_get_HasObservers_m2673131508(__this, method) ((  bool (*) (Subject_1_t2775141040 *, const MethodInfo*))Subject_1_get_HasObservers_m2673131508_gshared)(__this, method)
// System.Void UniRx.Subject`1<System.Object>::OnCompleted()
extern "C"  void Subject_1_OnCompleted_m1083367458_gshared (Subject_1_t2775141040 * __this, const MethodInfo* method);
#define Subject_1_OnCompleted_m1083367458(__this, method) ((  void (*) (Subject_1_t2775141040 *, const MethodInfo*))Subject_1_OnCompleted_m1083367458_gshared)(__this, method)
// System.Void UniRx.Subject`1<System.Object>::OnError(System.Exception)
extern "C"  void Subject_1_OnError_m1700032079_gshared (Subject_1_t2775141040 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Subject_1_OnError_m1700032079(__this, ___error0, method) ((  void (*) (Subject_1_t2775141040 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m1700032079_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<System.Object>::OnNext(T)
extern "C"  void Subject_1_OnNext_m1235145536_gshared (Subject_1_t2775141040 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Subject_1_OnNext_m1235145536(__this, ___value0, method) ((  void (*) (Subject_1_t2775141040 *, Il2CppObject *, const MethodInfo*))Subject_1_OnNext_m1235145536_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<System.Object>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * Subject_1_Subscribe_m2428743831_gshared (Subject_1_t2775141040 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define Subject_1_Subscribe_m2428743831(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t2775141040 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m2428743831_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<System.Object>::Dispose()
extern "C"  void Subject_1_Dispose_m2597692629_gshared (Subject_1_t2775141040 * __this, const MethodInfo* method);
#define Subject_1_Dispose_m2597692629(__this, method) ((  void (*) (Subject_1_t2775141040 *, const MethodInfo*))Subject_1_Dispose_m2597692629_gshared)(__this, method)
// System.Void UniRx.Subject`1<System.Object>::ThrowIfDisposed()
extern "C"  void Subject_1_ThrowIfDisposed_m956279134_gshared (Subject_1_t2775141040 * __this, const MethodInfo* method);
#define Subject_1_ThrowIfDisposed_m956279134(__this, method) ((  void (*) (Subject_1_t2775141040 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m956279134_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<System.Object>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool Subject_1_IsRequiredSubscribeOnCurrentThread_m3969161195_gshared (Subject_1_t2775141040 * __this, const MethodInfo* method);
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m3969161195(__this, method) ((  bool (*) (Subject_1_t2775141040 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m3969161195_gshared)(__this, method)
