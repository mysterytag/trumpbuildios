﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.UpdateCharacterStatisticsResult
struct UpdateCharacterStatisticsResult_t2994977680;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.UpdateCharacterStatisticsResult::.ctor()
extern "C"  void UpdateCharacterStatisticsResult__ctor_m2122125849 (UpdateCharacterStatisticsResult_t2994977680 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
