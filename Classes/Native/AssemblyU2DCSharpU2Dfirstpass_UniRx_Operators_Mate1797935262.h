﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.MaterializeObservable`1<System.Object>
struct MaterializeObservable_1_t1619437495;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera389018104.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.MaterializeObservable`1/Materialize<System.Object>
struct  Materialize_t1797935262  : public OperatorObserverBase_2_t389018104
{
public:
	// UniRx.Operators.MaterializeObservable`1<T> UniRx.Operators.MaterializeObservable`1/Materialize::parent
	MaterializeObservable_1_t1619437495 * ___parent_2;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(Materialize_t1797935262, ___parent_2)); }
	inline MaterializeObservable_1_t1619437495 * get_parent_2() const { return ___parent_2; }
	inline MaterializeObservable_1_t1619437495 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(MaterializeObservable_1_t1619437495 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
