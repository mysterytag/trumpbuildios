﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo4000251768MethodDeclarations.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SubstanceBase`2/Intrusion<System.Single,System.Func`1<System.Single>>>::.ctor(System.Collections.Generic.IList`1<T>)
#define ReadOnlyCollection_1__ctor_m1883635559(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t89855723 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1366664402_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SubstanceBase`2/Intrusion<System.Single,System.Func`1<System.Single>>>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3401459153(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t89855723 *, Intrusion_t1221677671 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2541166012_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SubstanceBase`2/Intrusion<System.Single,System.Func`1<System.Single>>>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m77742361(__this, method) ((  void (*) (ReadOnlyCollection_1_t89855723 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3473426062_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SubstanceBase`2/Intrusion<System.Single,System.Func`1<System.Single>>>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m860247928(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t89855723 *, int32_t, Intrusion_t1221677671 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3496388003_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SubstanceBase`2/Intrusion<System.Single,System.Func`1<System.Single>>>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2144574594(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t89855723 *, Intrusion_t1221677671 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m348744375_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SubstanceBase`2/Intrusion<System.Single,System.Func`1<System.Single>>>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3029068094(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t89855723 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1370240873_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<SubstanceBase`2/Intrusion<System.Single,System.Func`1<System.Single>>>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1110705282(__this, ___index0, method) ((  Intrusion_t1221677671 * (*) (ReadOnlyCollection_1_t89855723 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3534609325_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SubstanceBase`2/Intrusion<System.Single,System.Func`1<System.Single>>>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3874134607(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t89855723 *, int32_t, Intrusion_t1221677671 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3174042042_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SubstanceBase`2/Intrusion<System.Single,System.Func`1<System.Single>>>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24472269(__this, method) ((  bool (*) (ReadOnlyCollection_1_t89855723 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2459576056_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SubstanceBase`2/Intrusion<System.Single,System.Func`1<System.Single>>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m639441046(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t89855723 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1945557633_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<SubstanceBase`2/Intrusion<System.Single,System.Func`1<System.Single>>>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m74486673(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t89855723 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3330065468_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<SubstanceBase`2/Intrusion<System.Single,System.Func`1<System.Single>>>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m1950553728(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t89855723 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1628967861_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SubstanceBase`2/Intrusion<System.Single,System.Func`1<System.Single>>>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m836894628(__this, method) ((  void (*) (ReadOnlyCollection_1_t89855723 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m514207119_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SubstanceBase`2/Intrusion<System.Single,System.Func`1<System.Single>>>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m3004320204(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t89855723 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m736178103_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<SubstanceBase`2/Intrusion<System.Single,System.Func`1<System.Single>>>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2266238168(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t89855723 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3658311565_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SubstanceBase`2/Intrusion<System.Single,System.Func`1<System.Single>>>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m2707842563(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t89855723 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m2823806264_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SubstanceBase`2/Intrusion<System.Single,System.Func`1<System.Single>>>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m1091615301(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t89855723 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m2498539760_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SubstanceBase`2/Intrusion<System.Single,System.Func`1<System.Single>>>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1065691667(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t89855723 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1730676936_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SubstanceBase`2/Intrusion<System.Single,System.Func`1<System.Single>>>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3169659408(__this, method) ((  bool (*) (ReadOnlyCollection_1_t89855723 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1373829189_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<SubstanceBase`2/Intrusion<System.Single,System.Func`1<System.Single>>>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m4014043068(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t89855723 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m918746289_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SubstanceBase`2/Intrusion<System.Single,System.Func`1<System.Single>>>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1473790523(__this, method) ((  bool (*) (ReadOnlyCollection_1_t89855723 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m932754534_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SubstanceBase`2/Intrusion<System.Single,System.Func`1<System.Single>>>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3688139102(__this, method) ((  bool (*) (ReadOnlyCollection_1_t89855723 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2423760339_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<SubstanceBase`2/Intrusion<System.Single,System.Func`1<System.Single>>>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m1680368195(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t89855723 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m3512499704_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SubstanceBase`2/Intrusion<System.Single,System.Func`1<System.Single>>>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m100474138(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t89855723 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m4167408399_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<SubstanceBase`2/Intrusion<System.Single,System.Func`1<System.Single>>>::Contains(T)
#define ReadOnlyCollection_1_Contains_m217797575(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t89855723 *, Intrusion_t1221677671 *, const MethodInfo*))ReadOnlyCollection_1_Contains_m687553276_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<SubstanceBase`2/Intrusion<System.Single,System.Func`1<System.Single>>>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m798923777(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t89855723 *, IntrusionU5BU5D_t2110565406*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m475587820_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<SubstanceBase`2/Intrusion<System.Single,System.Func`1<System.Single>>>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m1814662954(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t89855723 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m809369055_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<SubstanceBase`2/Intrusion<System.Single,System.Func`1<System.Single>>>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m813235269(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t89855723 *, Intrusion_t1221677671 *, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m817393776_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<SubstanceBase`2/Intrusion<System.Single,System.Func`1<System.Single>>>::get_Count()
#define ReadOnlyCollection_1_get_Count_m3552764374(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t89855723 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m3681678091_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<SubstanceBase`2/Intrusion<System.Single,System.Func`1<System.Single>>>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m3765419842(__this, ___index0, method) ((  Intrusion_t1221677671 * (*) (ReadOnlyCollection_1_t89855723 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m2421641197_gshared)(__this, ___index0, method)
