﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t3824425150;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.DictionaryDisposable`2<System.Object,System.Object>
struct  DictionaryDisposable_2_t2367950693  : public Il2CppObject
{
public:
	// System.Boolean UniRx.DictionaryDisposable`2::isDisposed
	bool ___isDisposed_0;
	// System.Collections.Generic.Dictionary`2<TKey,TValue> UniRx.DictionaryDisposable`2::inner
	Dictionary_2_t3824425150 * ___inner_1;

public:
	inline static int32_t get_offset_of_isDisposed_0() { return static_cast<int32_t>(offsetof(DictionaryDisposable_2_t2367950693, ___isDisposed_0)); }
	inline bool get_isDisposed_0() const { return ___isDisposed_0; }
	inline bool* get_address_of_isDisposed_0() { return &___isDisposed_0; }
	inline void set_isDisposed_0(bool value)
	{
		___isDisposed_0 = value;
	}

	inline static int32_t get_offset_of_inner_1() { return static_cast<int32_t>(offsetof(DictionaryDisposable_2_t2367950693, ___inner_1)); }
	inline Dictionary_2_t3824425150 * get_inner_1() const { return ___inner_1; }
	inline Dictionary_2_t3824425150 ** get_address_of_inner_1() { return &___inner_1; }
	inline void set_inner_1(Dictionary_2_t3824425150 * value)
	{
		___inner_1 = value;
		Il2CppCodeGenWriteBarrier(&___inner_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
