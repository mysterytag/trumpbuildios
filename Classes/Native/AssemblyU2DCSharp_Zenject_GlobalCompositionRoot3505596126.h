﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Zenject.GlobalCompositionRoot
struct GlobalCompositionRoot_t3505596126;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// Zenject.IDependencyRoot
struct IDependencyRoot_t2368104171;
// System.Func`2<Zenject.GlobalInstallerConfig,System.Collections.Generic.IEnumerable`1<Zenject.MonoInstaller>>
struct Func_2_t3681249322;

#include "AssemblyU2DCSharp_Zenject_CompositionRoot1939928321.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.GlobalCompositionRoot
struct  GlobalCompositionRoot_t3505596126  : public CompositionRoot_t1939928321
{
public:
	// Zenject.DiContainer Zenject.GlobalCompositionRoot::_container
	DiContainer_t2383114449 * ____container_3;
	// Zenject.IDependencyRoot Zenject.GlobalCompositionRoot::_dependencyRoot
	Il2CppObject * ____dependencyRoot_4;
	// System.Boolean Zenject.GlobalCompositionRoot::_hasInitialized
	bool ____hasInitialized_5;

public:
	inline static int32_t get_offset_of__container_3() { return static_cast<int32_t>(offsetof(GlobalCompositionRoot_t3505596126, ____container_3)); }
	inline DiContainer_t2383114449 * get__container_3() const { return ____container_3; }
	inline DiContainer_t2383114449 ** get_address_of__container_3() { return &____container_3; }
	inline void set__container_3(DiContainer_t2383114449 * value)
	{
		____container_3 = value;
		Il2CppCodeGenWriteBarrier(&____container_3, value);
	}

	inline static int32_t get_offset_of__dependencyRoot_4() { return static_cast<int32_t>(offsetof(GlobalCompositionRoot_t3505596126, ____dependencyRoot_4)); }
	inline Il2CppObject * get__dependencyRoot_4() const { return ____dependencyRoot_4; }
	inline Il2CppObject ** get_address_of__dependencyRoot_4() { return &____dependencyRoot_4; }
	inline void set__dependencyRoot_4(Il2CppObject * value)
	{
		____dependencyRoot_4 = value;
		Il2CppCodeGenWriteBarrier(&____dependencyRoot_4, value);
	}

	inline static int32_t get_offset_of__hasInitialized_5() { return static_cast<int32_t>(offsetof(GlobalCompositionRoot_t3505596126, ____hasInitialized_5)); }
	inline bool get__hasInitialized_5() const { return ____hasInitialized_5; }
	inline bool* get_address_of__hasInitialized_5() { return &____hasInitialized_5; }
	inline void set__hasInitialized_5(bool value)
	{
		____hasInitialized_5 = value;
	}
};

struct GlobalCompositionRoot_t3505596126_StaticFields
{
public:
	// Zenject.GlobalCompositionRoot Zenject.GlobalCompositionRoot::_instance
	GlobalCompositionRoot_t3505596126 * ____instance_2;
	// System.Func`2<Zenject.GlobalInstallerConfig,System.Collections.Generic.IEnumerable`1<Zenject.MonoInstaller>> Zenject.GlobalCompositionRoot::<>f__am$cache4
	Func_2_t3681249322 * ___U3CU3Ef__amU24cache4_6;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(GlobalCompositionRoot_t3505596126_StaticFields, ____instance_2)); }
	inline GlobalCompositionRoot_t3505596126 * get__instance_2() const { return ____instance_2; }
	inline GlobalCompositionRoot_t3505596126 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(GlobalCompositionRoot_t3505596126 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier(&____instance_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_6() { return static_cast<int32_t>(offsetof(GlobalCompositionRoot_t3505596126_StaticFields, ___U3CU3Ef__amU24cache4_6)); }
	inline Func_2_t3681249322 * get_U3CU3Ef__amU24cache4_6() const { return ___U3CU3Ef__amU24cache4_6; }
	inline Func_2_t3681249322 ** get_address_of_U3CU3Ef__amU24cache4_6() { return &___U3CU3Ef__amU24cache4_6; }
	inline void set_U3CU3Ef__amU24cache4_6(Func_2_t3681249322 * value)
	{
		___U3CU3Ef__amU24cache4_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
