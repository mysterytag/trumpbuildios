﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetPlayFabIDsFromSteamIDsRequest
struct GetPlayFabIDsFromSteamIDsRequest_t241435282;
// System.Collections.Generic.List`1<System.UInt64>
struct List_1_t1782884390;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.GetPlayFabIDsFromSteamIDsRequest::.ctor()
extern "C"  void GetPlayFabIDsFromSteamIDsRequest__ctor_m3523419627 (GetPlayFabIDsFromSteamIDsRequest_t241435282 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.UInt64> PlayFab.ClientModels.GetPlayFabIDsFromSteamIDsRequest::get_SteamIDs()
extern "C"  List_1_t1782884390 * GetPlayFabIDsFromSteamIDsRequest_get_SteamIDs_m3835998034 (GetPlayFabIDsFromSteamIDsRequest_t241435282 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetPlayFabIDsFromSteamIDsRequest::set_SteamIDs(System.Collections.Generic.List`1<System.UInt64>)
extern "C"  void GetPlayFabIDsFromSteamIDsRequest_set_SteamIDs_m1144414521 (GetPlayFabIDsFromSteamIDsRequest_t241435282 * __this, List_1_t1782884390 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.GetPlayFabIDsFromSteamIDsRequest::get_SteamStringIDs()
extern "C"  List_1_t1765447871 * GetPlayFabIDsFromSteamIDsRequest_get_SteamStringIDs_m2616253192 (GetPlayFabIDsFromSteamIDsRequest_t241435282 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetPlayFabIDsFromSteamIDsRequest::set_SteamStringIDs(System.Collections.Generic.List`1<System.String>)
extern "C"  void GetPlayFabIDsFromSteamIDsRequest_set_SteamStringIDs_m1377890369 (GetPlayFabIDsFromSteamIDsRequest_t241435282 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
