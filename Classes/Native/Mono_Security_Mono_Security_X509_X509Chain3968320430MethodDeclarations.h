﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.X509.X509Chain
struct X509Chain_t3968320430;
// Mono.Security.X509.X509CertificateCollection
struct X509CertificateCollection_t3336811651;
// Mono.Security.X509.X509Certificate
struct X509Certificate_t273828613;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "Mono_Security_Mono_Security_X509_X509CertificateCo3336811650.h"
#include "Mono_Security_Mono_Security_X509_X509ChainStatusFl2524654829.h"
#include "Mono_Security_Mono_Security_X509_X509Certificate273828612.h"
#include "mscorlib_System_String968488902.h"

// System.Void Mono.Security.X509.X509Chain::.ctor()
extern "C"  void X509Chain__ctor_m4239706530 (X509Chain_t3968320430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509Chain::.ctor(Mono.Security.X509.X509CertificateCollection)
extern "C"  void X509Chain__ctor_m2818311608 (X509Chain_t3968320430 * __this, X509CertificateCollection_t3336811651 * ___chain0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509CertificateCollection Mono.Security.X509.X509Chain::get_Chain()
extern "C"  X509CertificateCollection_t3336811651 * X509Chain_get_Chain_m669388235 (X509Chain_t3968320430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Certificate Mono.Security.X509.X509Chain::get_Root()
extern "C"  X509Certificate_t273828613 * X509Chain_get_Root_m809849464 (X509Chain_t3968320430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509ChainStatusFlags Mono.Security.X509.X509Chain::get_Status()
extern "C"  int32_t X509Chain_get_Status_m4000836433 (X509Chain_t3968320430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509CertificateCollection Mono.Security.X509.X509Chain::get_TrustAnchors()
extern "C"  X509CertificateCollection_t3336811651 * X509Chain_get_TrustAnchors_m885238782 (X509Chain_t3968320430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509Chain::set_TrustAnchors(Mono.Security.X509.X509CertificateCollection)
extern "C"  void X509Chain_set_TrustAnchors_m1809030235 (X509Chain_t3968320430 * __this, X509CertificateCollection_t3336811651 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509Chain::LoadCertificate(Mono.Security.X509.X509Certificate)
extern "C"  void X509Chain_LoadCertificate_m2786681893 (X509Chain_t3968320430 * __this, X509Certificate_t273828613 * ___x5090, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509Chain::LoadCertificates(Mono.Security.X509.X509CertificateCollection)
extern "C"  void X509Chain_LoadCertificates_m3111163962 (X509Chain_t3968320430 * __this, X509CertificateCollection_t3336811651 * ___collection0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Certificate Mono.Security.X509.X509Chain::FindByIssuerName(System.String)
extern "C"  X509Certificate_t273828613 * X509Chain_FindByIssuerName_m1146941345 (X509Chain_t3968320430 * __this, String_t* ___issuerName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509Chain::Build(Mono.Security.X509.X509Certificate)
extern "C"  bool X509Chain_Build_m2539190260 (X509Chain_t3968320430 * __this, X509Certificate_t273828613 * ___leaf0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509Chain::Reset()
extern "C"  void X509Chain_Reset_m1886139471 (X509Chain_t3968320430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509Chain::IsValid(Mono.Security.X509.X509Certificate)
extern "C"  bool X509Chain_IsValid_m122831928 (X509Chain_t3968320430 * __this, X509Certificate_t273828613 * ___cert0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Certificate Mono.Security.X509.X509Chain::FindCertificateParent(Mono.Security.X509.X509Certificate)
extern "C"  X509Certificate_t273828613 * X509Chain_FindCertificateParent_m3659377521 (X509Chain_t3968320430 * __this, X509Certificate_t273828613 * ___child0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Certificate Mono.Security.X509.X509Chain::FindCertificateRoot(Mono.Security.X509.X509Certificate)
extern "C"  X509Certificate_t273828613 * X509Chain_FindCertificateRoot_m3426389033 (X509Chain_t3968320430 * __this, X509Certificate_t273828613 * ___potentialRoot0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509Chain::IsTrusted(Mono.Security.X509.X509Certificate)
extern "C"  bool X509Chain_IsTrusted_m622221811 (X509Chain_t3968320430 * __this, X509Certificate_t273828613 * ___potentialTrusted0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509Chain::IsParent(Mono.Security.X509.X509Certificate,Mono.Security.X509.X509Certificate)
extern "C"  bool X509Chain_IsParent_m3808002146 (X509Chain_t3968320430 * __this, X509Certificate_t273828613 * ___child0, X509Certificate_t273828613 * ___parent1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
