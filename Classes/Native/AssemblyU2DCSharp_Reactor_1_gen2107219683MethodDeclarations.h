﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Reactor`1<System.Int32>
struct Reactor_1_t2107219683;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<System.Int32>
struct Action_1_t2995867492;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"

// System.Void Reactor`1<System.Int32>::.ctor()
extern "C"  void Reactor_1__ctor_m2484461381_gshared (Reactor_1_t2107219683 * __this, const MethodInfo* method);
#define Reactor_1__ctor_m2484461381(__this, method) ((  void (*) (Reactor_1_t2107219683 *, const MethodInfo*))Reactor_1__ctor_m2484461381_gshared)(__this, method)
// System.Void Reactor`1<System.Int32>::React(T)
extern "C"  void Reactor_1_React_m3540553596_gshared (Reactor_1_t2107219683 * __this, int32_t ___t0, const MethodInfo* method);
#define Reactor_1_React_m3540553596(__this, ___t0, method) ((  void (*) (Reactor_1_t2107219683 *, int32_t, const MethodInfo*))Reactor_1_React_m3540553596_gshared)(__this, ___t0, method)
// System.Void Reactor`1<System.Int32>::TransactionUnpackReact(T,System.Int32)
extern "C"  void Reactor_1_TransactionUnpackReact_m2309767303_gshared (Reactor_1_t2107219683 * __this, int32_t ___t0, int32_t ___i1, const MethodInfo* method);
#define Reactor_1_TransactionUnpackReact_m2309767303(__this, ___t0, ___i1, method) ((  void (*) (Reactor_1_t2107219683 *, int32_t, int32_t, const MethodInfo*))Reactor_1_TransactionUnpackReact_m2309767303_gshared)(__this, ___t0, ___i1, method)
// System.Boolean Reactor`1<System.Int32>::Empty()
extern "C"  bool Reactor_1_Empty_m4190751012_gshared (Reactor_1_t2107219683 * __this, const MethodInfo* method);
#define Reactor_1_Empty_m4190751012(__this, method) ((  bool (*) (Reactor_1_t2107219683 *, const MethodInfo*))Reactor_1_Empty_m4190751012_gshared)(__this, method)
// System.IDisposable Reactor`1<System.Int32>::AddReaction(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Reactor_1_AddReaction_m3815700514_gshared (Reactor_1_t2107219683 * __this, Action_1_t2995867492 * ___reaction0, int32_t ___priority1, const MethodInfo* method);
#define Reactor_1_AddReaction_m3815700514(__this, ___reaction0, ___priority1, method) ((  Il2CppObject * (*) (Reactor_1_t2107219683 *, Action_1_t2995867492 *, int32_t, const MethodInfo*))Reactor_1_AddReaction_m3815700514_gshared)(__this, ___reaction0, ___priority1, method)
// System.Void Reactor`1<System.Int32>::UpgradeToMultyPriority()
extern "C"  void Reactor_1_UpgradeToMultyPriority_m2589025781_gshared (Reactor_1_t2107219683 * __this, const MethodInfo* method);
#define Reactor_1_UpgradeToMultyPriority_m2589025781(__this, method) ((  void (*) (Reactor_1_t2107219683 *, const MethodInfo*))Reactor_1_UpgradeToMultyPriority_m2589025781_gshared)(__this, method)
// System.Void Reactor`1<System.Int32>::AddToMultipriority(System.Action`1<T>,Priority)
extern "C"  void Reactor_1_AddToMultipriority_m3694054818_gshared (Reactor_1_t2107219683 * __this, Action_1_t2995867492 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define Reactor_1_AddToMultipriority_m3694054818(__this, ___reaction0, ___p1, method) ((  void (*) (Reactor_1_t2107219683 *, Action_1_t2995867492 *, int32_t, const MethodInfo*))Reactor_1_AddToMultipriority_m3694054818_gshared)(__this, ___reaction0, ___p1, method)
// System.Void Reactor`1<System.Int32>::RemoveReaction(System.Action`1<T>,Priority)
extern "C"  void Reactor_1_RemoveReaction_m2325743510_gshared (Reactor_1_t2107219683 * __this, Action_1_t2995867492 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define Reactor_1_RemoveReaction_m2325743510(__this, ___reaction0, ___p1, method) ((  void (*) (Reactor_1_t2107219683 *, Action_1_t2995867492 *, int32_t, const MethodInfo*))Reactor_1_RemoveReaction_m2325743510_gshared)(__this, ___reaction0, ___p1, method)
// System.Void Reactor`1<System.Int32>::Clear()
extern "C"  void Reactor_1_Clear_m4185561968_gshared (Reactor_1_t2107219683 * __this, const MethodInfo* method);
#define Reactor_1_Clear_m4185561968(__this, method) ((  void (*) (Reactor_1_t2107219683 *, const MethodInfo*))Reactor_1_Clear_m4185561968_gshared)(__this, method)
