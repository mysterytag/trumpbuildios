﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>
struct List_1_t2730771739;

#include "AssemblyU2DCSharp_PlayFab_Internal_PlayFabResultCom379512675.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.GetPlayerTradesResponse
struct  GetPlayerTradesResponse_t1092460183  : public PlayFabResultCommon_t379512675
{
public:
	// System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo> PlayFab.ClientModels.GetPlayerTradesResponse::<OpenedTrades>k__BackingField
	List_1_t2730771739 * ___U3COpenedTradesU3Ek__BackingField_2;
	// System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo> PlayFab.ClientModels.GetPlayerTradesResponse::<AcceptedTrades>k__BackingField
	List_1_t2730771739 * ___U3CAcceptedTradesU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3COpenedTradesU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GetPlayerTradesResponse_t1092460183, ___U3COpenedTradesU3Ek__BackingField_2)); }
	inline List_1_t2730771739 * get_U3COpenedTradesU3Ek__BackingField_2() const { return ___U3COpenedTradesU3Ek__BackingField_2; }
	inline List_1_t2730771739 ** get_address_of_U3COpenedTradesU3Ek__BackingField_2() { return &___U3COpenedTradesU3Ek__BackingField_2; }
	inline void set_U3COpenedTradesU3Ek__BackingField_2(List_1_t2730771739 * value)
	{
		___U3COpenedTradesU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3COpenedTradesU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CAcceptedTradesU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(GetPlayerTradesResponse_t1092460183, ___U3CAcceptedTradesU3Ek__BackingField_3)); }
	inline List_1_t2730771739 * get_U3CAcceptedTradesU3Ek__BackingField_3() const { return ___U3CAcceptedTradesU3Ek__BackingField_3; }
	inline List_1_t2730771739 ** get_address_of_U3CAcceptedTradesU3Ek__BackingField_3() { return &___U3CAcceptedTradesU3Ek__BackingField_3; }
	inline void set_U3CAcceptedTradesU3Ek__BackingField_3(List_1_t2730771739 * value)
	{
		___U3CAcceptedTradesU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAcceptedTradesU3Ek__BackingField_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
