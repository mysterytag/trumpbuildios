﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// UniRx.IScheduler
struct IScheduler_t2938318244;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4196218687.h"
#include "mscorlib_System_Nullable_1_gen3649900800.h"
#include "mscorlib_System_Nullable_1_gen2303330647.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.TimeoutObservable`1<System.Object>
struct  TimeoutObservable_1_t1895307051  : public OperatorObservableBase_1_t4196218687
{
public:
	// UniRx.IObservable`1<T> UniRx.Operators.TimeoutObservable`1::source
	Il2CppObject* ___source_1;
	// System.Nullable`1<System.TimeSpan> UniRx.Operators.TimeoutObservable`1::dueTime
	Nullable_1_t3649900800  ___dueTime_2;
	// System.Nullable`1<System.DateTimeOffset> UniRx.Operators.TimeoutObservable`1::dueTimeDT
	Nullable_1_t2303330647  ___dueTimeDT_3;
	// UniRx.IScheduler UniRx.Operators.TimeoutObservable`1::scheduler
	Il2CppObject * ___scheduler_4;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(TimeoutObservable_1_t1895307051, ___source_1)); }
	inline Il2CppObject* get_source_1() const { return ___source_1; }
	inline Il2CppObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(Il2CppObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier(&___source_1, value);
	}

	inline static int32_t get_offset_of_dueTime_2() { return static_cast<int32_t>(offsetof(TimeoutObservable_1_t1895307051, ___dueTime_2)); }
	inline Nullable_1_t3649900800  get_dueTime_2() const { return ___dueTime_2; }
	inline Nullable_1_t3649900800 * get_address_of_dueTime_2() { return &___dueTime_2; }
	inline void set_dueTime_2(Nullable_1_t3649900800  value)
	{
		___dueTime_2 = value;
	}

	inline static int32_t get_offset_of_dueTimeDT_3() { return static_cast<int32_t>(offsetof(TimeoutObservable_1_t1895307051, ___dueTimeDT_3)); }
	inline Nullable_1_t2303330647  get_dueTimeDT_3() const { return ___dueTimeDT_3; }
	inline Nullable_1_t2303330647 * get_address_of_dueTimeDT_3() { return &___dueTimeDT_3; }
	inline void set_dueTimeDT_3(Nullable_1_t2303330647  value)
	{
		___dueTimeDT_3 = value;
	}

	inline static int32_t get_offset_of_scheduler_4() { return static_cast<int32_t>(offsetof(TimeoutObservable_1_t1895307051, ___scheduler_4)); }
	inline Il2CppObject * get_scheduler_4() const { return ___scheduler_4; }
	inline Il2CppObject ** get_address_of_scheduler_4() { return &___scheduler_4; }
	inline void set_scheduler_4(Il2CppObject * value)
	{
		___scheduler_4 = value;
		Il2CppCodeGenWriteBarrier(&___scheduler_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
