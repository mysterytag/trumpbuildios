﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.BinderGeneric`1<System.Object>
struct BinderGeneric_1_t520705716;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.String
struct String_t;
// Zenject.SingletonProviderMap
struct SingletonProviderMap_t1557411893;
// Zenject.BindingConditionSetter
struct BindingConditionSetter_t259147722;
// System.Func`2<Zenject.InjectContext,System.Object>
struct Func_2_t2621245597;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_Zenject_SingletonProviderMap1557411893.h"

// System.Void Zenject.BinderGeneric`1<System.Object>::.ctor(Zenject.DiContainer,System.String,Zenject.SingletonProviderMap)
extern "C"  void BinderGeneric_1__ctor_m467851124_gshared (BinderGeneric_1_t520705716 * __this, DiContainer_t2383114449 * ___container0, String_t* ___identifier1, SingletonProviderMap_t1557411893 * ___singletonMap2, const MethodInfo* method);
#define BinderGeneric_1__ctor_m467851124(__this, ___container0, ___identifier1, ___singletonMap2, method) ((  void (*) (BinderGeneric_1_t520705716 *, DiContainer_t2383114449 *, String_t*, SingletonProviderMap_t1557411893 *, const MethodInfo*))BinderGeneric_1__ctor_m467851124_gshared)(__this, ___container0, ___identifier1, ___singletonMap2, method)
// Zenject.BindingConditionSetter Zenject.BinderGeneric`1<System.Object>::ToMethod(System.Func`2<Zenject.InjectContext,TContract>)
extern "C"  BindingConditionSetter_t259147722 * BinderGeneric_1_ToMethod_m4174284639_gshared (BinderGeneric_1_t520705716 * __this, Func_2_t2621245597 * ___method0, const MethodInfo* method);
#define BinderGeneric_1_ToMethod_m4174284639(__this, ___method0, method) ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t520705716 *, Func_2_t2621245597 *, const MethodInfo*))BinderGeneric_1_ToMethod_m4174284639_gshared)(__this, ___method0, method)
