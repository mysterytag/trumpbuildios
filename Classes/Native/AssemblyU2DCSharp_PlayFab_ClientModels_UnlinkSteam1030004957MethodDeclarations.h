﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.UnlinkSteamAccountResult
struct UnlinkSteamAccountResult_t1030004957;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.UnlinkSteamAccountResult::.ctor()
extern "C"  void UnlinkSteamAccountResult__ctor_m2537139712 (UnlinkSteamAccountResult_t1030004957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
