﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// System.IDisposable
struct IDisposable_t1628921374;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.RefCountDisposable
struct  RefCountDisposable_t3288429070  : public Il2CppObject
{
public:
	// System.Object UniRx.RefCountDisposable::_gate
	Il2CppObject * ____gate_0;
	// System.IDisposable UniRx.RefCountDisposable::_disposable
	Il2CppObject * ____disposable_1;
	// System.Boolean UniRx.RefCountDisposable::_isPrimaryDisposed
	bool ____isPrimaryDisposed_2;
	// System.Int32 UniRx.RefCountDisposable::_count
	int32_t ____count_3;

public:
	inline static int32_t get_offset_of__gate_0() { return static_cast<int32_t>(offsetof(RefCountDisposable_t3288429070, ____gate_0)); }
	inline Il2CppObject * get__gate_0() const { return ____gate_0; }
	inline Il2CppObject ** get_address_of__gate_0() { return &____gate_0; }
	inline void set__gate_0(Il2CppObject * value)
	{
		____gate_0 = value;
		Il2CppCodeGenWriteBarrier(&____gate_0, value);
	}

	inline static int32_t get_offset_of__disposable_1() { return static_cast<int32_t>(offsetof(RefCountDisposable_t3288429070, ____disposable_1)); }
	inline Il2CppObject * get__disposable_1() const { return ____disposable_1; }
	inline Il2CppObject ** get_address_of__disposable_1() { return &____disposable_1; }
	inline void set__disposable_1(Il2CppObject * value)
	{
		____disposable_1 = value;
		Il2CppCodeGenWriteBarrier(&____disposable_1, value);
	}

	inline static int32_t get_offset_of__isPrimaryDisposed_2() { return static_cast<int32_t>(offsetof(RefCountDisposable_t3288429070, ____isPrimaryDisposed_2)); }
	inline bool get__isPrimaryDisposed_2() const { return ____isPrimaryDisposed_2; }
	inline bool* get_address_of__isPrimaryDisposed_2() { return &____isPrimaryDisposed_2; }
	inline void set__isPrimaryDisposed_2(bool value)
	{
		____isPrimaryDisposed_2 = value;
	}

	inline static int32_t get_offset_of__count_3() { return static_cast<int32_t>(offsetof(RefCountDisposable_t3288429070, ____count_3)); }
	inline int32_t get__count_3() const { return ____count_3; }
	inline int32_t* get_address_of__count_3() { return &____count_3; }
	inline void set__count_3(int32_t value)
	{
		____count_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
