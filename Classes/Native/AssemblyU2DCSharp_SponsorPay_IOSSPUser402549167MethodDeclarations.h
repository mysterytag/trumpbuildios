﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SponsorPay.IOSSPUser
struct IOSSPUser_t402549167;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen1438485399.h"
#include "mscorlib_System_Nullable_1_gen2074414583.h"
#include "mscorlib_System_Nullable_1_gen3787560604.h"
#include "mscorlib_System_Nullable_1_gen3783893733.h"
#include "mscorlib_System_Nullable_1_gen2392475756.h"
#include "mscorlib_System_Nullable_1_gen4294542862.h"
#include "mscorlib_System_Nullable_1_gen2936825364.h"
#include "mscorlib_System_Nullable_1_gen3097043249.h"
#include "mscorlib_System_Nullable_1_gen3844246929.h"
#include "mscorlib_System_Nullable_1_gen1438485494.h"

// System.Void SponsorPay.IOSSPUser::.ctor()
extern "C"  void IOSSPUser__ctor_m1717265126 (IOSSPUser_t402549167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SponsorPay.IOSSPUser::_SPUser(System.String)
extern "C"  String_t* IOSSPUser__SPUser_m2360337402 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.IOSSPUser::_SPUserReset()
extern "C"  void IOSSPUser__SPUserReset_m3720998 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> SponsorPay.IOSSPUser::GetAge()
extern "C"  Nullable_1_t1438485399  IOSSPUser_GetAge_m1989687958 (IOSSPUser_t402549167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<SponsorPay.SPUserGender> SponsorPay.IOSSPUser::GetGender()
extern "C"  Nullable_1_t2074414583  IOSSPUser_GetGender_m119393790 (IOSSPUser_t402549167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<SponsorPay.SPUserSexualOrientation> SponsorPay.IOSSPUser::GetSexualOrientation()
extern "C"  Nullable_1_t3787560604  IOSSPUser_GetSexualOrientation_m4200840044 (IOSSPUser_t402549167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<SponsorPay.SPUserEthnicity> SponsorPay.IOSSPUser::GetEthnicity()
extern "C"  Nullable_1_t3783893733  IOSSPUser_GetEthnicity_m821524990 (IOSSPUser_t402549167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<SponsorPay.SPUserMaritalStatus> SponsorPay.IOSSPUser::GetMaritalStatus()
extern "C"  Nullable_1_t2392475756  IOSSPUser_GetMaritalStatus_m1226756236 (IOSSPUser_t402549167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> SponsorPay.IOSSPUser::GetNumberOfChildrens()
extern "C"  Nullable_1_t1438485399  IOSSPUser_GetNumberOfChildrens_m3583698187 (IOSSPUser_t402549167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> SponsorPay.IOSSPUser::GetAnnualHouseholdIncome()
extern "C"  Nullable_1_t1438485399  IOSSPUser_GetAnnualHouseholdIncome_m2754109472 (IOSSPUser_t402549167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<SponsorPay.SPUserEducation> SponsorPay.IOSSPUser::GetEducation()
extern "C"  Nullable_1_t4294542862  IOSSPUser_GetEducation_m1385198416 (IOSSPUser_t402549167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<SponsorPay.SPUserConnection> SponsorPay.IOSSPUser::GetConnection()
extern "C"  Nullable_1_t2936825364  IOSSPUser_GetConnection_m1093207838 (IOSSPUser_t402549167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> SponsorPay.IOSSPUser::GetIap()
extern "C"  Nullable_1_t3097043249  IOSSPUser_GetIap_m3914558857 (IOSSPUser_t402549167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Single> SponsorPay.IOSSPUser::GetIapAmount()
extern "C"  Nullable_1_t3844246929  IOSSPUser_GetIapAmount_m14904691 (IOSSPUser_t402549167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> SponsorPay.IOSSPUser::GetNumberOfSessions()
extern "C"  Nullable_1_t1438485399  IOSSPUser_GetNumberOfSessions_m405221608 (IOSSPUser_t402549167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int64> SponsorPay.IOSSPUser::GetPsTime()
extern "C"  Nullable_1_t1438485494  IOSSPUser_GetPsTime_m2553153436 (IOSSPUser_t402549167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int64> SponsorPay.IOSSPUser::GetLastSession()
extern "C"  Nullable_1_t1438485494  IOSSPUser_GetLastSession_m2277806038 (IOSSPUser_t402549167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.IOSSPUser::NativePut(System.String)
extern "C"  void IOSSPUser_NativePut_m2183366790 (IOSSPUser_t402549167 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.IOSSPUser::NativeReset()
extern "C"  void IOSSPUser_NativeReset_m3831402300 (IOSSPUser_t402549167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SponsorPay.IOSSPUser::GetJsonMessage(System.String)
extern "C"  String_t* IOSSPUser_GetJsonMessage_m861178744 (IOSSPUser_t402549167 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
