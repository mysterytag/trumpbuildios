﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4014815677MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<PlayFab.ClientModels.CatalogItem>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2304541441(__this, ___l0, method) ((  void (*) (Enumerator_t3015331205 *, List_1_t634580917 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PlayFab.ClientModels.CatalogItem>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m186315953(__this, method) ((  void (*) (Enumerator_t3015331205 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<PlayFab.ClientModels.CatalogItem>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2735959271(__this, method) ((  Il2CppObject * (*) (Enumerator_t3015331205 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PlayFab.ClientModels.CatalogItem>::Dispose()
#define Enumerator_Dispose_m3707052326(__this, method) ((  void (*) (Enumerator_t3015331205 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PlayFab.ClientModels.CatalogItem>::VerifyState()
#define Enumerator_VerifyState_m2529680735(__this, method) ((  void (*) (Enumerator_t3015331205 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<PlayFab.ClientModels.CatalogItem>::MoveNext()
#define Enumerator_MoveNext_m348835297(__this, method) ((  bool (*) (Enumerator_t3015331205 *, const MethodInfo*))Enumerator_MoveNext_m302222300_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<PlayFab.ClientModels.CatalogItem>::get_Current()
#define Enumerator_get_Current_m2026909432(__this, method) ((  CatalogItem_t4132589244 * (*) (Enumerator_t3015331205 *, const MethodInfo*))Enumerator_get_Current_m1113061044_gshared)(__this, method)
