﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1178132721.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21270642218.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,PlayFab.UUnit.Region>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3218474434_gshared (InternalEnumerator_1_t1178132721 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3218474434(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1178132721 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3218474434_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,PlayFab.UUnit.Region>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4069295262_gshared (InternalEnumerator_1_t1178132721 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4069295262(__this, method) ((  void (*) (InternalEnumerator_1_t1178132721 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4069295262_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,PlayFab.UUnit.Region>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3676182996_gshared (InternalEnumerator_1_t1178132721 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3676182996(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1178132721 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3676182996_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,PlayFab.UUnit.Region>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1760131609_gshared (InternalEnumerator_1_t1178132721 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1760131609(__this, method) ((  void (*) (InternalEnumerator_1_t1178132721 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1760131609_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,PlayFab.UUnit.Region>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3074880206_gshared (InternalEnumerator_1_t1178132721 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3074880206(__this, method) ((  bool (*) (InternalEnumerator_1_t1178132721 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3074880206_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,PlayFab.UUnit.Region>>::get_Current()
extern "C"  KeyValuePair_2_t1270642218  InternalEnumerator_1_get_Current_m70069803_gshared (InternalEnumerator_1_t1178132721 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m70069803(__this, method) ((  KeyValuePair_2_t1270642218  (*) (InternalEnumerator_1_t1178132721 *, const MethodInfo*))InternalEnumerator_1_get_Current_m70069803_gshared)(__this, method)
