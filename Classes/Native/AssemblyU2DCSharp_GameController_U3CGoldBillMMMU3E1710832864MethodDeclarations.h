﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameController/<GoldBillMMM>c__Iterator14
struct U3CGoldBillMMMU3Ec__Iterator14_t1710832864;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GameController/<GoldBillMMM>c__Iterator14::.ctor()
extern "C"  void U3CGoldBillMMMU3Ec__Iterator14__ctor_m1387958346 (U3CGoldBillMMMU3Ec__Iterator14_t1710832864 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameController/<GoldBillMMM>c__Iterator14::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGoldBillMMMU3Ec__Iterator14_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2446128008 (U3CGoldBillMMMU3Ec__Iterator14_t1710832864 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameController/<GoldBillMMM>c__Iterator14::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGoldBillMMMU3Ec__Iterator14_System_Collections_IEnumerator_get_Current_m2703689500 (U3CGoldBillMMMU3Ec__Iterator14_t1710832864 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameController/<GoldBillMMM>c__Iterator14::MoveNext()
extern "C"  bool U3CGoldBillMMMU3Ec__Iterator14_MoveNext_m361341546 (U3CGoldBillMMMU3Ec__Iterator14_t1710832864 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController/<GoldBillMMM>c__Iterator14::Dispose()
extern "C"  void U3CGoldBillMMMU3Ec__Iterator14_Dispose_m2286391815 (U3CGoldBillMMMU3Ec__Iterator14_t1710832864 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController/<GoldBillMMM>c__Iterator14::Reset()
extern "C"  void U3CGoldBillMMMU3Ec__Iterator14_Reset_m3329358583 (U3CGoldBillMMMU3Ec__Iterator14_t1710832864 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
