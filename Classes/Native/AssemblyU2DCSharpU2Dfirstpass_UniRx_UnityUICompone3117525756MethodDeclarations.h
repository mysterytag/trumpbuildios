﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.UnityUIComponentExtensions/<OnValueChangedAsObservable>c__AnonStoreyAA
struct U3COnValueChangedAsObservableU3Ec__AnonStoreyAA_t3117525756;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Single>
struct IObserver_1_t3170207924;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.UnityUIComponentExtensions/<OnValueChangedAsObservable>c__AnonStoreyAA::.ctor()
extern "C"  void U3COnValueChangedAsObservableU3Ec__AnonStoreyAA__ctor_m416697154 (U3COnValueChangedAsObservableU3Ec__AnonStoreyAA_t3117525756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable UniRx.UnityUIComponentExtensions/<OnValueChangedAsObservable>c__AnonStoreyAA::<>m__E8(UniRx.IObserver`1<System.Single>)
extern "C"  Il2CppObject * U3COnValueChangedAsObservableU3Ec__AnonStoreyAA_U3CU3Em__E8_m1468494408 (U3COnValueChangedAsObservableU3Ec__AnonStoreyAA_t3117525756 * __this, Il2CppObject* ___observer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
