﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Stream_1_gen3823212738MethodDeclarations.h"

// System.Void Stream`1<CollectionMoveEvent`1<DonateButton>>::.ctor()
#define Stream_1__ctor_m3246261471(__this, method) ((  void (*) (Stream_1_t2096841424 *, const MethodInfo*))Stream_1__ctor_m2034388992_gshared)(__this, method)
// System.Void Stream`1<CollectionMoveEvent`1<DonateButton>>::Send(T)
#define Stream_1_Send_m1775479025(__this, ___value0, method) ((  void (*) (Stream_1_t2096841424 *, CollectionMoveEvent_1_t3405702402 *, const MethodInfo*))Stream_1_Send_m563606546_gshared)(__this, ___value0, method)
// System.Void Stream`1<CollectionMoveEvent`1<DonateButton>>::SendInTransaction(T,System.Int32)
#define Stream_1_SendInTransaction_m4064878919(__this, ___value0, ___p1, method) ((  void (*) (Stream_1_t2096841424 *, CollectionMoveEvent_1_t3405702402 *, int32_t, const MethodInfo*))Stream_1_SendInTransaction_m3764966696_gshared)(__this, ___value0, ___p1, method)
// System.IDisposable Stream`1<CollectionMoveEvent`1<DonateButton>>::Listen(System.Action`1<T>,Priority)
#define Stream_1_Listen_m676735753(__this, ___action0, ___priority1, method) ((  Il2CppObject * (*) (Stream_1_t2096841424 *, Action_1_t3554155107 *, int32_t, const MethodInfo*))Stream_1_Listen_m889871082_gshared)(__this, ___action0, ___priority1, method)
// System.IDisposable Stream`1<CollectionMoveEvent`1<DonateButton>>::Listen(System.Action,Priority)
#define Stream_1_Listen_m1912362110(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (Stream_1_t2096841424 *, Action_t437523947 *, int32_t, const MethodInfo*))Stream_1_Listen_m2133851133_gshared)(__this, ___action0, ___p1, method)
// System.Void Stream`1<CollectionMoveEvent`1<DonateButton>>::Dispose()
#define Stream_1_Dispose_m1409299804(__this, method) ((  void (*) (Stream_1_t2096841424 *, const MethodInfo*))Stream_1_Dispose_m735984701_gshared)(__this, method)
// System.Void Stream`1<CollectionMoveEvent`1<DonateButton>>::SetInputStream(IStream`1<T>)
#define Stream_1_SetInputStream_m2844482689(__this, ___stream0, method) ((  void (*) (Stream_1_t2096841424 *, Il2CppObject*, const MethodInfo*))Stream_1_SetInputStream_m3106996224_gshared)(__this, ___stream0, method)
// System.Void Stream`1<CollectionMoveEvent`1<DonateButton>>::ClearInputStream()
#define Stream_1_ClearInputStream_m2340129442(__this, method) ((  void (*) (Stream_1_t2096841424 *, const MethodInfo*))Stream_1_ClearInputStream_m445690081_gshared)(__this, method)
// System.Void Stream`1<CollectionMoveEvent`1<DonateButton>>::TransactionIterationFinished()
#define Stream_1_TransactionIterationFinished_m4241123190(__this, method) ((  void (*) (Stream_1_t2096841424 *, const MethodInfo*))Stream_1_TransactionIterationFinished_m3113559861_gshared)(__this, method)
// System.Void Stream`1<CollectionMoveEvent`1<DonateButton>>::Unpack(System.Int32)
#define Stream_1_Unpack_m2878345736(__this, ___p0, method) ((  void (*) (Stream_1_t2096841424 *, int32_t, const MethodInfo*))Stream_1_Unpack_m3388253319_gshared)(__this, ___p0, method)
// System.Void Stream`1<CollectionMoveEvent`1<DonateButton>>::<SetInputStream>m__1BA(T)
#define Stream_1_U3CSetInputStreamU3Em__1BA_m2566760892(__this, ___val0, method) ((  void (*) (Stream_1_t2096841424 *, CollectionMoveEvent_1_t3405702402 *, const MethodInfo*))Stream_1_U3CSetInputStreamU3Em__1BA_m3579563677_gshared)(__this, ___val0, method)
