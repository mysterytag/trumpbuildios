﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PlayFab.UUnit.ObjNumFieldTest
struct ObjNumFieldTest_t1159807015;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.UUnit.ObjNumFieldTest
struct  ObjNumFieldTest_t1159807015  : public Il2CppObject
{
public:
	// System.SByte PlayFab.UUnit.ObjNumFieldTest::SbyteValue
	int8_t ___SbyteValue_0;
	// System.Byte PlayFab.UUnit.ObjNumFieldTest::ByteValue
	uint8_t ___ByteValue_1;
	// System.Int16 PlayFab.UUnit.ObjNumFieldTest::ShortValue
	int16_t ___ShortValue_2;
	// System.UInt16 PlayFab.UUnit.ObjNumFieldTest::UshortValue
	uint16_t ___UshortValue_3;
	// System.Int32 PlayFab.UUnit.ObjNumFieldTest::IntValue
	int32_t ___IntValue_4;
	// System.UInt32 PlayFab.UUnit.ObjNumFieldTest::UintValue
	uint32_t ___UintValue_5;
	// System.Int64 PlayFab.UUnit.ObjNumFieldTest::LongValue
	int64_t ___LongValue_6;
	// System.UInt64 PlayFab.UUnit.ObjNumFieldTest::UlongValue
	uint64_t ___UlongValue_7;
	// System.Single PlayFab.UUnit.ObjNumFieldTest::FloatValue
	float ___FloatValue_8;
	// System.Double PlayFab.UUnit.ObjNumFieldTest::DoubleValue
	double ___DoubleValue_9;

public:
	inline static int32_t get_offset_of_SbyteValue_0() { return static_cast<int32_t>(offsetof(ObjNumFieldTest_t1159807015, ___SbyteValue_0)); }
	inline int8_t get_SbyteValue_0() const { return ___SbyteValue_0; }
	inline int8_t* get_address_of_SbyteValue_0() { return &___SbyteValue_0; }
	inline void set_SbyteValue_0(int8_t value)
	{
		___SbyteValue_0 = value;
	}

	inline static int32_t get_offset_of_ByteValue_1() { return static_cast<int32_t>(offsetof(ObjNumFieldTest_t1159807015, ___ByteValue_1)); }
	inline uint8_t get_ByteValue_1() const { return ___ByteValue_1; }
	inline uint8_t* get_address_of_ByteValue_1() { return &___ByteValue_1; }
	inline void set_ByteValue_1(uint8_t value)
	{
		___ByteValue_1 = value;
	}

	inline static int32_t get_offset_of_ShortValue_2() { return static_cast<int32_t>(offsetof(ObjNumFieldTest_t1159807015, ___ShortValue_2)); }
	inline int16_t get_ShortValue_2() const { return ___ShortValue_2; }
	inline int16_t* get_address_of_ShortValue_2() { return &___ShortValue_2; }
	inline void set_ShortValue_2(int16_t value)
	{
		___ShortValue_2 = value;
	}

	inline static int32_t get_offset_of_UshortValue_3() { return static_cast<int32_t>(offsetof(ObjNumFieldTest_t1159807015, ___UshortValue_3)); }
	inline uint16_t get_UshortValue_3() const { return ___UshortValue_3; }
	inline uint16_t* get_address_of_UshortValue_3() { return &___UshortValue_3; }
	inline void set_UshortValue_3(uint16_t value)
	{
		___UshortValue_3 = value;
	}

	inline static int32_t get_offset_of_IntValue_4() { return static_cast<int32_t>(offsetof(ObjNumFieldTest_t1159807015, ___IntValue_4)); }
	inline int32_t get_IntValue_4() const { return ___IntValue_4; }
	inline int32_t* get_address_of_IntValue_4() { return &___IntValue_4; }
	inline void set_IntValue_4(int32_t value)
	{
		___IntValue_4 = value;
	}

	inline static int32_t get_offset_of_UintValue_5() { return static_cast<int32_t>(offsetof(ObjNumFieldTest_t1159807015, ___UintValue_5)); }
	inline uint32_t get_UintValue_5() const { return ___UintValue_5; }
	inline uint32_t* get_address_of_UintValue_5() { return &___UintValue_5; }
	inline void set_UintValue_5(uint32_t value)
	{
		___UintValue_5 = value;
	}

	inline static int32_t get_offset_of_LongValue_6() { return static_cast<int32_t>(offsetof(ObjNumFieldTest_t1159807015, ___LongValue_6)); }
	inline int64_t get_LongValue_6() const { return ___LongValue_6; }
	inline int64_t* get_address_of_LongValue_6() { return &___LongValue_6; }
	inline void set_LongValue_6(int64_t value)
	{
		___LongValue_6 = value;
	}

	inline static int32_t get_offset_of_UlongValue_7() { return static_cast<int32_t>(offsetof(ObjNumFieldTest_t1159807015, ___UlongValue_7)); }
	inline uint64_t get_UlongValue_7() const { return ___UlongValue_7; }
	inline uint64_t* get_address_of_UlongValue_7() { return &___UlongValue_7; }
	inline void set_UlongValue_7(uint64_t value)
	{
		___UlongValue_7 = value;
	}

	inline static int32_t get_offset_of_FloatValue_8() { return static_cast<int32_t>(offsetof(ObjNumFieldTest_t1159807015, ___FloatValue_8)); }
	inline float get_FloatValue_8() const { return ___FloatValue_8; }
	inline float* get_address_of_FloatValue_8() { return &___FloatValue_8; }
	inline void set_FloatValue_8(float value)
	{
		___FloatValue_8 = value;
	}

	inline static int32_t get_offset_of_DoubleValue_9() { return static_cast<int32_t>(offsetof(ObjNumFieldTest_t1159807015, ___DoubleValue_9)); }
	inline double get_DoubleValue_9() const { return ___DoubleValue_9; }
	inline double* get_address_of_DoubleValue_9() { return &___DoubleValue_9; }
	inline void set_DoubleValue_9(double value)
	{
		___DoubleValue_9 = value;
	}
};

struct ObjNumFieldTest_t1159807015_StaticFields
{
public:
	// PlayFab.UUnit.ObjNumFieldTest PlayFab.UUnit.ObjNumFieldTest::Max
	ObjNumFieldTest_t1159807015 * ___Max_10;
	// PlayFab.UUnit.ObjNumFieldTest PlayFab.UUnit.ObjNumFieldTest::Min
	ObjNumFieldTest_t1159807015 * ___Min_11;
	// PlayFab.UUnit.ObjNumFieldTest PlayFab.UUnit.ObjNumFieldTest::Zero
	ObjNumFieldTest_t1159807015 * ___Zero_12;

public:
	inline static int32_t get_offset_of_Max_10() { return static_cast<int32_t>(offsetof(ObjNumFieldTest_t1159807015_StaticFields, ___Max_10)); }
	inline ObjNumFieldTest_t1159807015 * get_Max_10() const { return ___Max_10; }
	inline ObjNumFieldTest_t1159807015 ** get_address_of_Max_10() { return &___Max_10; }
	inline void set_Max_10(ObjNumFieldTest_t1159807015 * value)
	{
		___Max_10 = value;
		Il2CppCodeGenWriteBarrier(&___Max_10, value);
	}

	inline static int32_t get_offset_of_Min_11() { return static_cast<int32_t>(offsetof(ObjNumFieldTest_t1159807015_StaticFields, ___Min_11)); }
	inline ObjNumFieldTest_t1159807015 * get_Min_11() const { return ___Min_11; }
	inline ObjNumFieldTest_t1159807015 ** get_address_of_Min_11() { return &___Min_11; }
	inline void set_Min_11(ObjNumFieldTest_t1159807015 * value)
	{
		___Min_11 = value;
		Il2CppCodeGenWriteBarrier(&___Min_11, value);
	}

	inline static int32_t get_offset_of_Zero_12() { return static_cast<int32_t>(offsetof(ObjNumFieldTest_t1159807015_StaticFields, ___Zero_12)); }
	inline ObjNumFieldTest_t1159807015 * get_Zero_12() const { return ___Zero_12; }
	inline ObjNumFieldTest_t1159807015 ** get_address_of_Zero_12() { return &___Zero_12; }
	inline void set_Zero_12(ObjNumFieldTest_t1159807015 * value)
	{
		___Zero_12 = value;
		Il2CppCodeGenWriteBarrier(&___Zero_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
