﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.DiContainer
struct DiContainer_t2383114449;
// UnityEngine.Transform
struct Transform_t284553113;
// System.Collections.Generic.IEnumerable`1<Zenject.IInstaller>
struct IEnumerable_1_t1032410536;
// Zenject.ProviderBase
struct ProviderBase_t1627494391;
// System.Collections.Generic.IEnumerable`1<Zenject.BindingId>
struct IEnumerable_1_t1542981321;
// System.Collections.Generic.IEnumerable`1<System.Type>
struct IEnumerable_1_t1356416995;
// Zenject.BinderUntyped
struct BinderUntyped_t2430239676;
// System.Type
struct Type_t;
// System.String
struct String_t;
// Zenject.BindScope
struct BindScope_t2945157996;
// Zenject.BindingId
struct BindingId_t2965794261;
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException>
struct IEnumerable_1_t4073207355;
// System.Type[]
struct TypeU5BU5D_t3431720054;
// Zenject.InjectContext
struct InjectContext_t3456483891;
// System.Collections.Generic.List`1<Zenject.ProviderBase>
struct List_1_t2424453360;
// System.Collections.Generic.IEnumerable`1<Zenject.ProviderBase>
struct IEnumerable_1_t204681451;
// System.Collections.IList
struct IList_t1612618265;
// System.Collections.Generic.List`1<System.Type>
struct List_1_t3576188904;
// Zenject.IInstaller
struct IInstaller_t2455223476;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.List`1<Zenject.TypeValuePair>
struct List_1_t1417891359;
// System.Collections.Generic.IEnumerable`1<Zenject.TypeValuePair>
struct IEnumerable_1_t3493086746;
// Zenject.ZenjectTypeInfo
struct ZenjectTypeInfo_t283213708;
// UnityEngine.Component
struct Component_t2126946602;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t3709260776;
// Zenject.TypeValuePair
struct TypeValuePair_t620932390;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Transform284553113.h"
#include "AssemblyU2DCSharp_Zenject_ProviderBase1627494391.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_Zenject_BindingId2965794261.h"
#include "AssemblyU2DCSharp_Zenject_InjectContext3456483891.h"
#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharp_Zenject_ZenjectTypeInfo283213708.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22091358871.h"
#include "AssemblyU2DCSharp_Zenject_TypeValuePair620932390.h"

// System.Void Zenject.DiContainer::.ctor(UnityEngine.Transform)
extern "C"  void DiContainer__ctor_m3219451343 (DiContainer_t2383114449 * __this, Transform_t284553113 * ___rootTransform0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.DiContainer::.ctor()
extern "C"  void DiContainer__ctor_m4122028590 (DiContainer_t2383114449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.DiContainer::get_CheckForCircularDependencies()
extern "C"  bool DiContainer_get_CheckForCircularDependencies_m2298890260 (DiContainer_t2383114449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Zenject.IInstaller> Zenject.DiContainer::get_InstalledInstallers()
extern "C"  Il2CppObject* DiContainer_get_InstalledInstallers_m1054194606 (DiContainer_t2383114449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Zenject.DiContainer::get_RootTransform()
extern "C"  Transform_t284553113 * DiContainer_get_RootTransform_m947931243 (DiContainer_t2383114449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.ProviderBase Zenject.DiContainer::get_FallbackProvider()
extern "C"  ProviderBase_t1627494391 * DiContainer_get_FallbackProvider_m3537626710 (DiContainer_t2383114449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.DiContainer::set_FallbackProvider(Zenject.ProviderBase)
extern "C"  void DiContainer_set_FallbackProvider_m203495429 (DiContainer_t2383114449 * __this, ProviderBase_t1627494391 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.DiContainer::get_AllowNullBindings()
extern "C"  bool DiContainer_get_AllowNullBindings_m3648988981 (DiContainer_t2383114449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.DiContainer::set_AllowNullBindings(System.Boolean)
extern "C"  void DiContainer_set_AllowNullBindings_m2759860292 (DiContainer_t2383114449 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Zenject.BindingId> Zenject.DiContainer::get_AllContracts()
extern "C"  Il2CppObject* DiContainer_get_AllContracts_m1345902226 (DiContainer_t2383114449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.Type> Zenject.DiContainer::get_AllConcreteTypes()
extern "C"  Il2CppObject* DiContainer_get_AllConcreteTypes_m1934566957 (DiContainer_t2383114449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.BinderUntyped Zenject.DiContainer::Bind(System.Type,System.String)
extern "C"  BinderUntyped_t2430239676 * DiContainer_Bind_m1978119117 (DiContainer_t2383114449 * __this, Type_t * ___contractType0, String_t* ___identifier1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.BindScope Zenject.DiContainer::CreateScope()
extern "C"  BindScope_t2945157996 * DiContainer_CreateScope_m2665286303 (DiContainer_t2383114449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.DiContainer::RegisterProvider(Zenject.ProviderBase,Zenject.BindingId)
extern "C"  void DiContainer_RegisterProvider_m2952257076 (DiContainer_t2383114449 * __this, ProviderBase_t1627494391 * ___provider0, BindingId_t2965794261 * ___bindingId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Zenject.DiContainer::UnregisterProvider(Zenject.ProviderBase)
extern "C"  int32_t DiContainer_UnregisterProvider_m1897958228 (DiContainer_t2383114449 * __this, ProviderBase_t1627494391 * ___provider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.DiContainer::ValidateValidatables(System.Type[])
extern "C"  Il2CppObject* DiContainer_ValidateValidatables_m2628416096 (DiContainer_t2383114449 * __this, TypeU5BU5D_t3431720054* ___ignoreTypes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.DiContainer::ValidateResolve(Zenject.InjectContext)
extern "C"  Il2CppObject* DiContainer_ValidateResolve_m3744280082 (DiContainer_t2383114449 * __this, InjectContext_t3456483891 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.DiContainer::ValidateObjectGraph(System.Type,System.Type[])
extern "C"  Il2CppObject* DiContainer_ValidateObjectGraph_m579741446 (DiContainer_t2383114449 * __this, Type_t * ___contractType0, TypeU5BU5D_t3431720054* ___extras1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.DiContainer::ValidateObjectGraph(System.Type,Zenject.InjectContext,System.Type[])
extern "C"  Il2CppObject* DiContainer_ValidateObjectGraph_m464997203 (DiContainer_t2383114449 * __this, Type_t * ___contractType0, InjectContext_t3456483891 * ___context1, TypeU5BU5D_t3431720054* ___extras2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Zenject.ProviderBase> Zenject.DiContainer::GetProviderMatches(Zenject.InjectContext)
extern "C"  List_1_t2424453360 * DiContainer_GetProviderMatches_m2040453285 (DiContainer_t2383114449 * __this, InjectContext_t3456483891 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Zenject.ProviderBase> Zenject.DiContainer::GetProviderMatchesInternal(Zenject.InjectContext)
extern "C"  Il2CppObject* DiContainer_GetProviderMatchesInternal_m2782337917 (DiContainer_t2383114449 * __this, InjectContext_t3456483891 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Zenject.ProviderBase> Zenject.DiContainer::GetProvidersForContract(Zenject.BindingId)
extern "C"  Il2CppObject* DiContainer_GetProvidersForContract_m858156269 (DiContainer_t2383114449 * __this, BindingId_t2965794261 * ___bindingId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.DiContainer::HasBinding(Zenject.InjectContext)
extern "C"  bool DiContainer_HasBinding_m2882674310 (DiContainer_t2383114449 * __this, InjectContext_t3456483891 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IList Zenject.DiContainer::ResolveAll(Zenject.InjectContext)
extern "C"  Il2CppObject * DiContainer_ResolveAll_m3470176054 (DiContainer_t2383114449 * __this, InjectContext_t3456483891 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Type> Zenject.DiContainer::ResolveTypeAll(Zenject.InjectContext)
extern "C"  List_1_t3576188904 * DiContainer_ResolveTypeAll_m1475306812 (DiContainer_t2383114449 * __this, InjectContext_t3456483891 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.DiContainer::Install(Zenject.IInstaller)
extern "C"  void DiContainer_Install_m2959618825 (DiContainer_t2383114449 * __this, Il2CppObject * ___installer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.DiContainer::InstallInstallerInternal(Zenject.IInstaller)
extern "C"  void DiContainer_InstallInstallerInternal_m4081360834 (DiContainer_t2383114449 * __this, Il2CppObject * ___installer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.DiContainer::Install(System.Type)
extern "C"  void DiContainer_Install_m2198099378 (DiContainer_t2383114449 * __this, Type_t * ___installerType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Zenject.DiContainer::Resolve(Zenject.InjectContext)
extern "C"  Il2CppObject * DiContainer_Resolve_m1328994886 (DiContainer_t2383114449 * __this, InjectContext_t3456483891 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.Type> Zenject.DiContainer::GetDependencyContracts(System.Type)
extern "C"  Il2CppObject* DiContainer_GetDependencyContracts_m3395218664 (DiContainer_t2383114449 * __this, Type_t * ___contract0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Zenject.DiContainer::InstantiateExplicit(System.Type,System.Collections.Generic.List`1<Zenject.TypeValuePair>,Zenject.InjectContext,System.String,System.Boolean)
extern "C"  Il2CppObject * DiContainer_InstantiateExplicit_m3996819186 (DiContainer_t2383114449 * __this, Type_t * ___concreteType0, List_1_t1417891359 * ___extraArgMap1, InjectContext_t3456483891 * ___currentContext2, String_t* ___concreteIdentifier3, bool ___autoInject4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Zenject.DiContainer::InstantiateInternal(System.Type,System.Collections.Generic.IEnumerable`1<Zenject.TypeValuePair>,Zenject.InjectContext,System.String,System.Boolean)
extern "C"  Il2CppObject * DiContainer_InstantiateInternal_m1853104588 (DiContainer_t2383114449 * __this, Type_t * ___concreteType0, Il2CppObject* ___extraArgs1, InjectContext_t3456483891 * ___currentContext2, String_t* ___concreteIdentifier3, bool ___autoInject4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.DiContainer::InjectExplicit(System.Object,System.Collections.Generic.IEnumerable`1<Zenject.TypeValuePair>,System.Boolean,Zenject.ZenjectTypeInfo,Zenject.InjectContext,System.String)
extern "C"  void DiContainer_InjectExplicit_m3957872714 (DiContainer_t2383114449 * __this, Il2CppObject * ___injectable0, Il2CppObject* ___extraArgs1, bool ___shouldUseAll2, ZenjectTypeInfo_t283213708 * ___typeInfo3, InjectContext_t3456483891 * ___context4, String_t* ___concreteIdentifier5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component Zenject.DiContainer::InstantiateComponent(System.Type,UnityEngine.GameObject,System.Object[])
extern "C"  Component_t2126946602 * DiContainer_InstantiateComponent_m1837712909 (DiContainer_t2383114449 * __this, Type_t * ___componentType0, GameObject_t4012695102 * ___gameObject1, ObjectU5BU5D_t11523773* ___extraArgMap2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Zenject.DiContainer::InstantiatePrefabResourceExplicit(System.String,System.Collections.Generic.IEnumerable`1<System.Object>,Zenject.InjectContext)
extern "C"  GameObject_t4012695102 * DiContainer_InstantiatePrefabResourceExplicit_m1346987425 (DiContainer_t2383114449 * __this, String_t* ___resourcePath0, Il2CppObject* ___extraArgMap1, InjectContext_t3456483891 * ___context2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Zenject.DiContainer::InstantiatePrefabExplicit(UnityEngine.GameObject,System.Collections.Generic.IEnumerable`1<System.Object>,Zenject.InjectContext)
extern "C"  GameObject_t4012695102 * DiContainer_InstantiatePrefabExplicit_m3878641707 (DiContainer_t2383114449 * __this, GameObject_t4012695102 * ___prefab0, Il2CppObject* ___extraArgMap1, InjectContext_t3456483891 * ___context2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Zenject.DiContainer::InstantiateGameObject(System.String)
extern "C"  GameObject_t4012695102 * DiContainer_InstantiateGameObject_m2868936294 (DiContainer_t2383114449 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Zenject.DiContainer::InstantiateComponentOnNewGameObjectExplicit(System.Type,System.String,System.Collections.Generic.List`1<Zenject.TypeValuePair>,Zenject.InjectContext)
extern "C"  Il2CppObject * DiContainer_InstantiateComponentOnNewGameObjectExplicit_m571899144 (DiContainer_t2383114449 * __this, Type_t * ___componentType0, String_t* ___name1, List_1_t1417891359 * ___extraArgMap2, InjectContext_t3456483891 * ___currentContext3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Zenject.DiContainer::InstantiatePrefabResourceForComponentExplicit(System.Type,System.String,System.Collections.Generic.List`1<Zenject.TypeValuePair>,Zenject.InjectContext)
extern "C"  Il2CppObject * DiContainer_InstantiatePrefabResourceForComponentExplicit_m3994334233 (DiContainer_t2383114449 * __this, Type_t * ___componentType0, String_t* ___resourcePath1, List_1_t1417891359 * ___extraArgs2, InjectContext_t3456483891 * ___currentContext3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Zenject.DiContainer::InstantiatePrefabForComponentExplicit(System.Type,UnityEngine.GameObject,System.Collections.Generic.List`1<Zenject.TypeValuePair>,Zenject.InjectContext)
extern "C"  Il2CppObject * DiContainer_InstantiatePrefabForComponentExplicit_m4212530413 (DiContainer_t2383114449 * __this, Type_t * ___componentType0, GameObject_t4012695102 * ___prefab1, List_1_t1417891359 * ___extraArgs2, InjectContext_t3456483891 * ___currentContext3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Zenject.DiContainer::Instantiate(System.Type,System.Object[])
extern "C"  Il2CppObject * DiContainer_Instantiate_m630963738 (DiContainer_t2383114449 * __this, Type_t * ___concreteType0, ObjectU5BU5D_t11523773* ___extraArgs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Zenject.DiContainer::InstantiateExplicit(System.Type,System.Collections.Generic.List`1<Zenject.TypeValuePair>)
extern "C"  Il2CppObject * DiContainer_InstantiateExplicit_m1723441122 (DiContainer_t2383114449 * __this, Type_t * ___concreteType0, List_1_t1417891359 * ___extraArgMap1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Zenject.DiContainer::InstantiatePrefab(UnityEngine.GameObject,System.Object[])
extern "C"  GameObject_t4012695102 * DiContainer_InstantiatePrefab_m461129907 (DiContainer_t2383114449 * __this, GameObject_t4012695102 * ___prefab0, ObjectU5BU5D_t11523773* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Zenject.DiContainer::InstantiatePrefabResource(System.String,System.Object[])
extern "C"  GameObject_t4012695102 * DiContainer_InstantiatePrefabResource_m2231306705 (DiContainer_t2383114449 * __this, String_t* ___resourcePath0, ObjectU5BU5D_t11523773* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Zenject.DiContainer::InstantiatePrefabForComponent(System.Type,UnityEngine.GameObject,System.Object[])
extern "C"  Il2CppObject * DiContainer_InstantiatePrefabForComponent_m31285748 (DiContainer_t2383114449 * __this, Type_t * ___concreteType0, GameObject_t4012695102 * ___prefab1, ObjectU5BU5D_t11523773* ___extraArgs2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Zenject.DiContainer::InstantiatePrefabForComponentExplicit(System.Type,UnityEngine.GameObject,System.Collections.Generic.List`1<Zenject.TypeValuePair>)
extern "C"  Il2CppObject * DiContainer_InstantiatePrefabForComponentExplicit_m265498656 (DiContainer_t2383114449 * __this, Type_t * ___concreteType0, GameObject_t4012695102 * ___prefab1, List_1_t1417891359 * ___extraArgMap2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Zenject.DiContainer::InstantiatePrefabResourceForComponent(System.Type,System.String,System.Object[])
extern "C"  Il2CppObject * DiContainer_InstantiatePrefabResourceForComponent_m1917107568 (DiContainer_t2383114449 * __this, Type_t * ___concreteType0, String_t* ___resourcePath1, ObjectU5BU5D_t11523773* ___extraArgs2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Zenject.DiContainer::InstantiatePrefabResourceForComponentExplicit(System.Type,System.String,System.Collections.Generic.List`1<Zenject.TypeValuePair>)
extern "C"  Il2CppObject * DiContainer_InstantiatePrefabResourceForComponentExplicit_m1778085964 (DiContainer_t2383114449 * __this, Type_t * ___concreteType0, String_t* ___resourcePath1, List_1_t1417891359 * ___extraArgMap2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Zenject.DiContainer::InstantiateComponentOnNewGameObject(System.Type,System.String,System.Object[])
extern "C"  Il2CppObject * DiContainer_InstantiateComponentOnNewGameObject_m3571227489 (DiContainer_t2383114449 * __this, Type_t * ___concreteType0, String_t* ___name1, ObjectU5BU5D_t11523773* ___extraArgs2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Zenject.DiContainer::InstantiateComponentOnNewGameObjectExplicit(System.Type,System.String,System.Collections.Generic.List`1<Zenject.TypeValuePair>)
extern "C"  Il2CppObject * DiContainer_InstantiateComponentOnNewGameObjectExplicit_m962762363 (DiContainer_t2383114449 * __this, Type_t * ___concreteType0, String_t* ___name1, List_1_t1417891359 * ___extraArgMap2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.DiContainer::InjectGameObject(UnityEngine.GameObject,System.Boolean,System.Boolean)
extern "C"  void DiContainer_InjectGameObject_m1983917936 (DiContainer_t2383114449 * __this, GameObject_t4012695102 * ___gameObject0, bool ___recursive1, bool ___includeInactive2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.DiContainer::InjectGameObject(UnityEngine.GameObject,System.Boolean)
extern "C"  void DiContainer_InjectGameObject_m3361741933 (DiContainer_t2383114449 * __this, GameObject_t4012695102 * ___gameObject0, bool ___recursive1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.DiContainer::InjectGameObject(UnityEngine.GameObject)
extern "C"  void DiContainer_InjectGameObject_m1673884944 (DiContainer_t2383114449 * __this, GameObject_t4012695102 * ___gameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.DiContainer::InjectGameObject(UnityEngine.GameObject,System.Boolean,System.Boolean,System.Collections.Generic.IEnumerable`1<System.Object>)
extern "C"  void DiContainer_InjectGameObject_m814375347 (DiContainer_t2383114449 * __this, GameObject_t4012695102 * ___gameObject0, bool ___recursive1, bool ___includeInactive2, Il2CppObject* ___extraArgs3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.DiContainer::InjectGameObject(UnityEngine.GameObject,System.Boolean,System.Boolean,System.Collections.Generic.IEnumerable`1<System.Object>,Zenject.InjectContext)
extern "C"  void DiContainer_InjectGameObject_m3244133952 (DiContainer_t2383114449 * __this, GameObject_t4012695102 * ___gameObject0, bool ___recursive1, bool ___includeInactive2, Il2CppObject* ___extraArgs3, InjectContext_t3456483891 * ___context4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.DiContainer::Inject(System.Object)
extern "C"  void DiContainer_Inject_m1445639053 (DiContainer_t2383114449 * __this, Il2CppObject * ___injectable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.DiContainer::Inject(System.Object,System.Collections.Generic.IEnumerable`1<System.Object>)
extern "C"  void DiContainer_Inject_m3772639952 (DiContainer_t2383114449 * __this, Il2CppObject * ___injectable0, Il2CppObject* ___additional1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.DiContainer::Inject(System.Object,System.Collections.Generic.IEnumerable`1<System.Object>,System.Boolean)
extern "C"  void DiContainer_Inject_m1343581869 (DiContainer_t2383114449 * __this, Il2CppObject * ___injectable0, Il2CppObject* ___additional1, bool ___shouldUseAll2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.DiContainer::Inject(System.Object,System.Collections.Generic.IEnumerable`1<System.Object>,System.Boolean,Zenject.InjectContext)
extern "C"  void DiContainer_Inject_m2655470522 (DiContainer_t2383114449 * __this, Il2CppObject * ___injectable0, Il2CppObject* ___additional1, bool ___shouldUseAll2, InjectContext_t3456483891 * ___context3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.DiContainer::Inject(System.Object,System.Collections.Generic.IEnumerable`1<System.Object>,System.Boolean,Zenject.InjectContext,Zenject.ZenjectTypeInfo)
extern "C"  void DiContainer_Inject_m146900942 (DiContainer_t2383114449 * __this, Il2CppObject * ___injectable0, Il2CppObject* ___additional1, bool ___shouldUseAll2, InjectContext_t3456483891 * ___context3, ZenjectTypeInfo_t283213708 * ___typeInfo4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.DiContainer::InjectExplicit(System.Object,System.Collections.Generic.List`1<Zenject.TypeValuePair>)
extern "C"  void DiContainer_InjectExplicit_m3722179555 (DiContainer_t2383114449 * __this, Il2CppObject * ___injectable0, List_1_t1417891359 * ___additional1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.DiContainer::InjectExplicit(System.Object,System.Collections.Generic.List`1<Zenject.TypeValuePair>,Zenject.InjectContext)
extern "C"  void DiContainer_InjectExplicit_m1587698800 (DiContainer_t2383114449 * __this, Il2CppObject * ___injectable0, List_1_t1417891359 * ___additional1, InjectContext_t3456483891 * ___context2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Type> Zenject.DiContainer::ResolveTypeAll(System.Type)
extern "C"  List_1_t3576188904 * DiContainer_ResolveTypeAll_m2536840802 (DiContainer_t2383114449 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Zenject.DiContainer::TryResolve(System.Type)
extern "C"  Il2CppObject * DiContainer_TryResolve_m1735736647 (DiContainer_t2383114449 * __this, Type_t * ___contractType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Zenject.DiContainer::TryResolve(System.Type,System.String)
extern "C"  Il2CppObject * DiContainer_TryResolve_m4216986947 (DiContainer_t2383114449 * __this, Type_t * ___contractType0, String_t* ___identifier1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Zenject.DiContainer::Resolve(System.Type)
extern "C"  Il2CppObject * DiContainer_Resolve_m1905227756 (DiContainer_t2383114449 * __this, Type_t * ___contractType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Zenject.DiContainer::Resolve(System.Type,System.String)
extern "C"  Il2CppObject * DiContainer_Resolve_m4034024232 (DiContainer_t2383114449 * __this, Type_t * ___contractType0, String_t* ___identifier1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IList Zenject.DiContainer::ResolveAll(System.Type)
extern "C"  Il2CppObject * DiContainer_ResolveAll_m3088562396 (DiContainer_t2383114449 * __this, Type_t * ___contractType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IList Zenject.DiContainer::ResolveAll(System.Type,System.String)
extern "C"  Il2CppObject * DiContainer_ResolveAll_m1094594584 (DiContainer_t2383114449 * __this, Type_t * ___contractType0, String_t* ___identifier1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IList Zenject.DiContainer::ResolveAll(System.Type,System.Boolean)
extern "C"  Il2CppObject * DiContainer_ResolveAll_m4078716705 (DiContainer_t2383114449 * __this, Type_t * ___contractType0, bool ___optional1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IList Zenject.DiContainer::ResolveAll(System.Type,System.String,System.Boolean)
extern "C"  Il2CppObject * DiContainer_ResolveAll_m3850956901 (DiContainer_t2383114449 * __this, Type_t * ___contractType0, String_t* ___identifier1, bool ___optional2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.BinderUntyped Zenject.DiContainer::Bind(System.Type)
extern "C"  BinderUntyped_t2430239676 * DiContainer_Bind_m1530178897 (DiContainer_t2383114449 * __this, Type_t * ___contractType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.DiContainer::BindAllInterfacesToSingle(System.Type)
extern "C"  void DiContainer_BindAllInterfacesToSingle_m4056933228 (DiContainer_t2383114449 * __this, Type_t * ___concreteType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.DiContainer::BindAllInterfacesToInstance(System.Type,System.Object)
extern "C"  void DiContainer_BindAllInterfacesToInstance_m1147920845 (DiContainer_t2383114449 * __this, Type_t * ___concreteType0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Zenject.ProviderBase> Zenject.DiContainer::<get_AllConcreteTypes>m__2BB(System.Collections.Generic.KeyValuePair`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>)
extern "C"  Il2CppObject* DiContainer_U3Cget_AllConcreteTypesU3Em__2BB_m2658446591 (Il2CppObject * __this /* static, unused */, KeyValuePair_2_t2091358871  ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Zenject.DiContainer::<get_AllConcreteTypes>m__2BC(System.Collections.Generic.KeyValuePair`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>,Zenject.ProviderBase)
extern "C"  Type_t * DiContainer_U3Cget_AllConcreteTypesU3Em__2BC_m1080099350 (Il2CppObject * __this /* static, unused */, KeyValuePair_2_t2091358871  ___x0, ProviderBase_t1627494391 * ___p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.DiContainer::<get_AllConcreteTypes>m__2BD(System.Type)
extern "C"  bool DiContainer_U3Cget_AllConcreteTypesU3Em__2BD_m805286602 (Il2CppObject * __this /* static, unused */, Type_t * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.DiContainer::<UnregisterProvider>m__2C0(System.Collections.Generic.KeyValuePair`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>)
extern "C"  bool DiContainer_U3CUnregisterProviderU3Em__2C0_m2034929328 (Il2CppObject * __this /* static, unused */, KeyValuePair_2_t2091358871  ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.BindingId Zenject.DiContainer::<UnregisterProvider>m__2C1(System.Collections.Generic.KeyValuePair`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>>)
extern "C"  BindingId_t2965794261 * DiContainer_U3CUnregisterProviderU3Em__2C1_m1132585235 (Il2CppObject * __this /* static, unused */, KeyValuePair_2_t2091358871  ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Zenject.DiContainer::<ResolveTypeAll>m__2C5(Zenject.ProviderBase)
extern "C"  Type_t * DiContainer_U3CResolveTypeAllU3Em__2C5_m3382267129 (Il2CppObject * __this /* static, unused */, ProviderBase_t1627494391 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.DiContainer::<ResolveTypeAll>m__2C6(System.Type)
extern "C"  bool DiContainer_U3CResolveTypeAllU3Em__2C6_m4215594744 (Il2CppObject * __this /* static, unused */, Type_t * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.DiContainer::<Resolve>m__2C9(Zenject.ProviderBase)
extern "C"  bool DiContainer_U3CResolveU3Em__2C9_m780668072 (Il2CppObject * __this /* static, unused */, ProviderBase_t1627494391 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Zenject.DiContainer::<InstantiateInternal>m__2CB(Zenject.TypeValuePair)
extern "C"  String_t* DiContainer_U3CInstantiateInternalU3Em__2CB_m30882970 (Il2CppObject * __this /* static, unused */, TypeValuePair_t620932390 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Zenject.DiContainer::<InjectExplicit>m__2CC(Zenject.TypeValuePair)
extern "C"  String_t* DiContainer_U3CInjectExplicitU3Em__2CC_m2299610027 (Il2CppObject * __this /* static, unused */, TypeValuePair_t620932390 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
