﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.Object>
struct Action_1_t985559125;
// System.Collections.ObjectModel.Collection`1<System.Object>
struct Collection_1_t2806094150;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZergRush.ReactiveCollectionExtensions/<ProcessEach>c__AnonStorey11C`1<System.Object>
struct  U3CProcessEachU3Ec__AnonStorey11C_1_t2501101944  : public Il2CppObject
{
public:
	// System.Action`1<T> ZergRush.ReactiveCollectionExtensions/<ProcessEach>c__AnonStorey11C`1::onRemove
	Action_1_t985559125 * ___onRemove_0;
	// System.Collections.ObjectModel.Collection`1<T> ZergRush.ReactiveCollectionExtensions/<ProcessEach>c__AnonStorey11C`1::collection
	Collection_1_t2806094150 * ___collection_1;
	// System.Action`1<T> ZergRush.ReactiveCollectionExtensions/<ProcessEach>c__AnonStorey11C`1::func
	Action_1_t985559125 * ___func_2;

public:
	inline static int32_t get_offset_of_onRemove_0() { return static_cast<int32_t>(offsetof(U3CProcessEachU3Ec__AnonStorey11C_1_t2501101944, ___onRemove_0)); }
	inline Action_1_t985559125 * get_onRemove_0() const { return ___onRemove_0; }
	inline Action_1_t985559125 ** get_address_of_onRemove_0() { return &___onRemove_0; }
	inline void set_onRemove_0(Action_1_t985559125 * value)
	{
		___onRemove_0 = value;
		Il2CppCodeGenWriteBarrier(&___onRemove_0, value);
	}

	inline static int32_t get_offset_of_collection_1() { return static_cast<int32_t>(offsetof(U3CProcessEachU3Ec__AnonStorey11C_1_t2501101944, ___collection_1)); }
	inline Collection_1_t2806094150 * get_collection_1() const { return ___collection_1; }
	inline Collection_1_t2806094150 ** get_address_of_collection_1() { return &___collection_1; }
	inline void set_collection_1(Collection_1_t2806094150 * value)
	{
		___collection_1 = value;
		Il2CppCodeGenWriteBarrier(&___collection_1, value);
	}

	inline static int32_t get_offset_of_func_2() { return static_cast<int32_t>(offsetof(U3CProcessEachU3Ec__AnonStorey11C_1_t2501101944, ___func_2)); }
	inline Action_1_t985559125 * get_func_2() const { return ___func_2; }
	inline Action_1_t985559125 ** get_address_of_func_2() { return &___func_2; }
	inline void set_func_2(Action_1_t985559125 * value)
	{
		___func_2 = value;
		Il2CppCodeGenWriteBarrier(&___func_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
