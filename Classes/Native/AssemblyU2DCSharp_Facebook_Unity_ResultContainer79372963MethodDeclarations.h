﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.ResultContainer
struct ResultContainer_t79372963;
// System.Collections.Generic.IDictionary`2<System.String,System.Object>
struct IDictionary_2_t3650470111;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void Facebook.Unity.ResultContainer::.ctor(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern "C"  void ResultContainer__ctor_m2691469252 (ResultContainer_t79372963 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.ResultContainer::.ctor(System.String)
extern "C"  void ResultContainer__ctor_m4178585898 (ResultContainer_t79372963 * __this, String_t* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.ResultContainer::get_RawResult()
extern "C"  String_t* ResultContainer_get_RawResult_m3018396207 (ResultContainer_t79372963 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.ResultContainer::set_RawResult(System.String)
extern "C"  void ResultContainer_set_RawResult_m2915204612 (ResultContainer_t79372963 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IDictionary`2<System.String,System.Object> Facebook.Unity.ResultContainer::get_ResultDictionary()
extern "C"  Il2CppObject* ResultContainer_get_ResultDictionary_m3778681285 (ResultContainer_t79372963 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.ResultContainer::set_ResultDictionary(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern "C"  void ResultContainer_set_ResultDictionary_m195032288 (ResultContainer_t79372963 * __this, Il2CppObject* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IDictionary`2<System.String,System.Object> Facebook.Unity.ResultContainer::GetWebFormattedResponseDictionary(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern "C"  Il2CppObject* ResultContainer_GetWebFormattedResponseDictionary_m869449838 (ResultContainer_t79372963 * __this, Il2CppObject* ___resultDictionary0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
