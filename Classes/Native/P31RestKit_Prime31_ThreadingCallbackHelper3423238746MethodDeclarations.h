﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Prime31.ThreadingCallbackHelper
struct ThreadingCallbackHelper_t3423238746;
// System.Action
struct Action_t437523947;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"

// System.Void Prime31.ThreadingCallbackHelper::.ctor()
extern "C"  void ThreadingCallbackHelper__ctor_m2363282603 (ThreadingCallbackHelper_t3423238746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.ThreadingCallbackHelper::addActionToQueue(System.Action)
extern "C"  void ThreadingCallbackHelper_addActionToQueue_m1029015557 (ThreadingCallbackHelper_t3423238746 * __this, Action_t437523947 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.ThreadingCallbackHelper::Update()
extern "C"  void ThreadingCallbackHelper_Update_m1974178754 (ThreadingCallbackHelper_t3423238746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Prime31.ThreadingCallbackHelper::disableIfEmpty()
extern "C"  void ThreadingCallbackHelper_disableIfEmpty_m3972502849 (ThreadingCallbackHelper_t3423238746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
