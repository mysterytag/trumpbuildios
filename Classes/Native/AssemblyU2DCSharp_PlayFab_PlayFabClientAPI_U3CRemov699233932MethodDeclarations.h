﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<RemoveFriend>c__AnonStoreyAF
struct U3CRemoveFriendU3Ec__AnonStoreyAF_t699233932;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<RemoveFriend>c__AnonStoreyAF::.ctor()
extern "C"  void U3CRemoveFriendU3Ec__AnonStoreyAF__ctor_m1553500839 (U3CRemoveFriendU3Ec__AnonStoreyAF_t699233932 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<RemoveFriend>c__AnonStoreyAF::<>m__9C(PlayFab.CallRequestContainer)
extern "C"  void U3CRemoveFriendU3Ec__AnonStoreyAF_U3CU3Em__9C_m1129012463 (U3CRemoveFriendU3Ec__AnonStoreyAF_t699233932 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
