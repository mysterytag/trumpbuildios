﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.RepeatObservable`1/<SubscribeCore>c__AnonStorey66<System.Object>
struct U3CSubscribeCoreU3Ec__AnonStorey66_t3102228927;
// System.Action
struct Action_t437523947;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"

// System.Void UniRx.Operators.RepeatObservable`1/<SubscribeCore>c__AnonStorey66<System.Object>::.ctor()
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey66__ctor_m1241725906_gshared (U3CSubscribeCoreU3Ec__AnonStorey66_t3102228927 * __this, const MethodInfo* method);
#define U3CSubscribeCoreU3Ec__AnonStorey66__ctor_m1241725906(__this, method) ((  void (*) (U3CSubscribeCoreU3Ec__AnonStorey66_t3102228927 *, const MethodInfo*))U3CSubscribeCoreU3Ec__AnonStorey66__ctor_m1241725906_gshared)(__this, method)
// System.Void UniRx.Operators.RepeatObservable`1/<SubscribeCore>c__AnonStorey66<System.Object>::<>m__83(System.Action)
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey66_U3CU3Em__83_m2350846695_gshared (U3CSubscribeCoreU3Ec__AnonStorey66_t3102228927 * __this, Action_t437523947 * ___self0, const MethodInfo* method);
#define U3CSubscribeCoreU3Ec__AnonStorey66_U3CU3Em__83_m2350846695(__this, ___self0, method) ((  void (*) (U3CSubscribeCoreU3Ec__AnonStorey66_t3102228927 *, Action_t437523947 *, const MethodInfo*))U3CSubscribeCoreU3Ec__AnonStorey66_U3CU3Em__83_m2350846695_gshared)(__this, ___self0, method)
