﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CollectionRemoveEvent`1<System.Object>
struct CollectionRemoveEvent_1_t898259258;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void CollectionRemoveEvent`1<System.Object>::.ctor(System.Int32,T)
extern "C"  void CollectionRemoveEvent_1__ctor_m1174003473_gshared (CollectionRemoveEvent_1_t898259258 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define CollectionRemoveEvent_1__ctor_m1174003473(__this, ___index0, ___value1, method) ((  void (*) (CollectionRemoveEvent_1_t898259258 *, int32_t, Il2CppObject *, const MethodInfo*))CollectionRemoveEvent_1__ctor_m1174003473_gshared)(__this, ___index0, ___value1, method)
// System.Int32 CollectionRemoveEvent`1<System.Object>::get_Index()
extern "C"  int32_t CollectionRemoveEvent_1_get_Index_m2777665697_gshared (CollectionRemoveEvent_1_t898259258 * __this, const MethodInfo* method);
#define CollectionRemoveEvent_1_get_Index_m2777665697(__this, method) ((  int32_t (*) (CollectionRemoveEvent_1_t898259258 *, const MethodInfo*))CollectionRemoveEvent_1_get_Index_m2777665697_gshared)(__this, method)
// System.Void CollectionRemoveEvent`1<System.Object>::set_Index(System.Int32)
extern "C"  void CollectionRemoveEvent_1_set_Index_m1446049612_gshared (CollectionRemoveEvent_1_t898259258 * __this, int32_t ___value0, const MethodInfo* method);
#define CollectionRemoveEvent_1_set_Index_m1446049612(__this, ___value0, method) ((  void (*) (CollectionRemoveEvent_1_t898259258 *, int32_t, const MethodInfo*))CollectionRemoveEvent_1_set_Index_m1446049612_gshared)(__this, ___value0, method)
// T CollectionRemoveEvent`1<System.Object>::get_Value()
extern "C"  Il2CppObject * CollectionRemoveEvent_1_get_Value_m445974639_gshared (CollectionRemoveEvent_1_t898259258 * __this, const MethodInfo* method);
#define CollectionRemoveEvent_1_get_Value_m445974639(__this, method) ((  Il2CppObject * (*) (CollectionRemoveEvent_1_t898259258 *, const MethodInfo*))CollectionRemoveEvent_1_get_Value_m445974639_gshared)(__this, method)
// System.Void CollectionRemoveEvent`1<System.Object>::set_Value(T)
extern "C"  void CollectionRemoveEvent_1_set_Value_m347083076_gshared (CollectionRemoveEvent_1_t898259258 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define CollectionRemoveEvent_1_set_Value_m347083076(__this, ___value0, method) ((  void (*) (CollectionRemoveEvent_1_t898259258 *, Il2CppObject *, const MethodInfo*))CollectionRemoveEvent_1_set_Value_m347083076_gshared)(__this, ___value0, method)
// System.String CollectionRemoveEvent`1<System.Object>::ToString()
extern "C"  String_t* CollectionRemoveEvent_1_ToString_m2292716331_gshared (CollectionRemoveEvent_1_t898259258 * __this, const MethodInfo* method);
#define CollectionRemoveEvent_1_ToString_m2292716331(__this, method) ((  String_t* (*) (CollectionRemoveEvent_1_t898259258 *, const MethodInfo*))CollectionRemoveEvent_1_ToString_m2292716331_gshared)(__this, method)
