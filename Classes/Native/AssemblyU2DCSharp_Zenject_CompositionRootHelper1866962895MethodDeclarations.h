﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.Collections.Generic.IEnumerable`1<Zenject.IInstaller>
struct IEnumerable_1_t1032410536;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449.h"

// System.Void Zenject.CompositionRootHelper::InstallSceneInstallers(Zenject.DiContainer,System.Collections.Generic.IEnumerable`1<Zenject.IInstaller>)
extern "C"  void CompositionRootHelper_InstallSceneInstallers_m1888110602 (Il2CppObject * __this /* static, unused */, DiContainer_t2383114449 * ___container0, Il2CppObject* ___installers1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
