﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<UIElemHideInfo>
struct List_1_t823457644;
// GameController
struct GameController_t2782302542;
// UniRx.BoolReactiveProperty
struct BoolReactiveProperty_t3047538250;
// UnityEngine.Coroutine
struct Coroutine_t2246592261;
struct Coroutine_t2246592261_marshaled_pinvoke;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIHidder
struct  UIHidder_t339629410  : public MonoBehaviour_t3012272455
{
public:
	// System.Collections.Generic.List`1<UIElemHideInfo> UIHidder::elemsToHide
	List_1_t823457644 * ___elemsToHide_2;
	// GameController UIHidder::gc
	GameController_t2782302542 * ___gc_3;
	// System.Int32 UIHidder::multiplier
	int32_t ___multiplier_5;
	// UnityEngine.Coroutine UIHidder::currentCoroutine
	Coroutine_t2246592261 * ___currentCoroutine_6;
	// UnityEngine.Coroutine UIHidder::moveCoroutine
	Coroutine_t2246592261 * ___moveCoroutine_7;
	// System.Single UIHidder::currVal
	float ___currVal_8;
	// System.Single UIHidder::animSecs
	float ___animSecs_9;

public:
	inline static int32_t get_offset_of_elemsToHide_2() { return static_cast<int32_t>(offsetof(UIHidder_t339629410, ___elemsToHide_2)); }
	inline List_1_t823457644 * get_elemsToHide_2() const { return ___elemsToHide_2; }
	inline List_1_t823457644 ** get_address_of_elemsToHide_2() { return &___elemsToHide_2; }
	inline void set_elemsToHide_2(List_1_t823457644 * value)
	{
		___elemsToHide_2 = value;
		Il2CppCodeGenWriteBarrier(&___elemsToHide_2, value);
	}

	inline static int32_t get_offset_of_gc_3() { return static_cast<int32_t>(offsetof(UIHidder_t339629410, ___gc_3)); }
	inline GameController_t2782302542 * get_gc_3() const { return ___gc_3; }
	inline GameController_t2782302542 ** get_address_of_gc_3() { return &___gc_3; }
	inline void set_gc_3(GameController_t2782302542 * value)
	{
		___gc_3 = value;
		Il2CppCodeGenWriteBarrier(&___gc_3, value);
	}

	inline static int32_t get_offset_of_multiplier_5() { return static_cast<int32_t>(offsetof(UIHidder_t339629410, ___multiplier_5)); }
	inline int32_t get_multiplier_5() const { return ___multiplier_5; }
	inline int32_t* get_address_of_multiplier_5() { return &___multiplier_5; }
	inline void set_multiplier_5(int32_t value)
	{
		___multiplier_5 = value;
	}

	inline static int32_t get_offset_of_currentCoroutine_6() { return static_cast<int32_t>(offsetof(UIHidder_t339629410, ___currentCoroutine_6)); }
	inline Coroutine_t2246592261 * get_currentCoroutine_6() const { return ___currentCoroutine_6; }
	inline Coroutine_t2246592261 ** get_address_of_currentCoroutine_6() { return &___currentCoroutine_6; }
	inline void set_currentCoroutine_6(Coroutine_t2246592261 * value)
	{
		___currentCoroutine_6 = value;
		Il2CppCodeGenWriteBarrier(&___currentCoroutine_6, value);
	}

	inline static int32_t get_offset_of_moveCoroutine_7() { return static_cast<int32_t>(offsetof(UIHidder_t339629410, ___moveCoroutine_7)); }
	inline Coroutine_t2246592261 * get_moveCoroutine_7() const { return ___moveCoroutine_7; }
	inline Coroutine_t2246592261 ** get_address_of_moveCoroutine_7() { return &___moveCoroutine_7; }
	inline void set_moveCoroutine_7(Coroutine_t2246592261 * value)
	{
		___moveCoroutine_7 = value;
		Il2CppCodeGenWriteBarrier(&___moveCoroutine_7, value);
	}

	inline static int32_t get_offset_of_currVal_8() { return static_cast<int32_t>(offsetof(UIHidder_t339629410, ___currVal_8)); }
	inline float get_currVal_8() const { return ___currVal_8; }
	inline float* get_address_of_currVal_8() { return &___currVal_8; }
	inline void set_currVal_8(float value)
	{
		___currVal_8 = value;
	}

	inline static int32_t get_offset_of_animSecs_9() { return static_cast<int32_t>(offsetof(UIHidder_t339629410, ___animSecs_9)); }
	inline float get_animSecs_9() const { return ___animSecs_9; }
	inline float* get_address_of_animSecs_9() { return &___animSecs_9; }
	inline void set_animSecs_9(float value)
	{
		___animSecs_9 = value;
	}
};

struct UIHidder_t339629410_StaticFields
{
public:
	// UniRx.BoolReactiveProperty UIHidder::hidden
	BoolReactiveProperty_t3047538250 * ___hidden_4;

public:
	inline static int32_t get_offset_of_hidden_4() { return static_cast<int32_t>(offsetof(UIHidder_t339629410_StaticFields, ___hidden_4)); }
	inline BoolReactiveProperty_t3047538250 * get_hidden_4() const { return ___hidden_4; }
	inline BoolReactiveProperty_t3047538250 ** get_address_of_hidden_4() { return &___hidden_4; }
	inline void set_hidden_4(BoolReactiveProperty_t3047538250 * value)
	{
		___hidden_4 = value;
		Il2CppCodeGenWriteBarrier(&___hidden_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
