﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Tuple`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,UniRx.Tuple`1<System.Object>>
struct Tuple_8_t121772413;
// System.Object
struct Il2CppObject;
// System.Collections.IComparer
struct IComparer_t2207526184;
// System.Collections.IEqualityComparer
struct IEqualityComparer_t1661793090;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_1_gen3656007660.h"

// System.Void UniRx.Tuple`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,UniRx.Tuple`1<System.Object>>::.ctor(T1,T2,T3,T4,T5,T6,T7,TRest)
extern "C"  void Tuple_8__ctor_m1593336462_gshared (Tuple_8_t121772413 * __this, Il2CppObject * ___item10, Il2CppObject * ___item21, Il2CppObject * ___item32, Il2CppObject * ___item43, Il2CppObject * ___item54, Il2CppObject * ___item65, Il2CppObject * ___item76, Tuple_1_t3656007660  ___rest7, const MethodInfo* method);
#define Tuple_8__ctor_m1593336462(__this, ___item10, ___item21, ___item32, ___item43, ___item54, ___item65, ___item76, ___rest7, method) ((  void (*) (Tuple_8_t121772413 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Tuple_1_t3656007660 , const MethodInfo*))Tuple_8__ctor_m1593336462_gshared)(__this, ___item10, ___item21, ___item32, ___item43, ___item54, ___item65, ___item76, ___rest7, method)
// System.Int32 UniRx.Tuple`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,UniRx.Tuple`1<System.Object>>::System.IComparable.CompareTo(System.Object)
extern "C"  int32_t Tuple_8_System_IComparable_CompareTo_m1907202394_gshared (Tuple_8_t121772413 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define Tuple_8_System_IComparable_CompareTo_m1907202394(__this, ___obj0, method) ((  int32_t (*) (Tuple_8_t121772413 *, Il2CppObject *, const MethodInfo*))Tuple_8_System_IComparable_CompareTo_m1907202394_gshared)(__this, ___obj0, method)
// System.Int32 UniRx.Tuple`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,UniRx.Tuple`1<System.Object>>::UniRx.IStructuralComparable.CompareTo(System.Object,System.Collections.IComparer)
extern "C"  int32_t Tuple_8_UniRx_IStructuralComparable_CompareTo_m2706161324_gshared (Tuple_8_t121772413 * __this, Il2CppObject * ___other0, Il2CppObject * ___comparer1, const MethodInfo* method);
#define Tuple_8_UniRx_IStructuralComparable_CompareTo_m2706161324(__this, ___other0, ___comparer1, method) ((  int32_t (*) (Tuple_8_t121772413 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_8_UniRx_IStructuralComparable_CompareTo_m2706161324_gshared)(__this, ___other0, ___comparer1, method)
// System.Boolean UniRx.Tuple`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,UniRx.Tuple`1<System.Object>>::UniRx.IStructuralEquatable.Equals(System.Object,System.Collections.IEqualityComparer)
extern "C"  bool Tuple_8_UniRx_IStructuralEquatable_Equals_m706690467_gshared (Tuple_8_t121772413 * __this, Il2CppObject * ___other0, Il2CppObject * ___comparer1, const MethodInfo* method);
#define Tuple_8_UniRx_IStructuralEquatable_Equals_m706690467(__this, ___other0, ___comparer1, method) ((  bool (*) (Tuple_8_t121772413 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_8_UniRx_IStructuralEquatable_Equals_m706690467_gshared)(__this, ___other0, ___comparer1, method)
// System.Int32 UniRx.Tuple`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,UniRx.Tuple`1<System.Object>>::UniRx.IStructuralEquatable.GetHashCode(System.Collections.IEqualityComparer)
extern "C"  int32_t Tuple_8_UniRx_IStructuralEquatable_GetHashCode_m482733367_gshared (Tuple_8_t121772413 * __this, Il2CppObject * ___comparer0, const MethodInfo* method);
#define Tuple_8_UniRx_IStructuralEquatable_GetHashCode_m482733367(__this, ___comparer0, method) ((  int32_t (*) (Tuple_8_t121772413 *, Il2CppObject *, const MethodInfo*))Tuple_8_UniRx_IStructuralEquatable_GetHashCode_m482733367_gshared)(__this, ___comparer0, method)
// System.String UniRx.Tuple`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,UniRx.Tuple`1<System.Object>>::UniRx.ITuple.ToString()
extern "C"  String_t* Tuple_8_UniRx_ITuple_ToString_m634988430_gshared (Tuple_8_t121772413 * __this, const MethodInfo* method);
#define Tuple_8_UniRx_ITuple_ToString_m634988430(__this, method) ((  String_t* (*) (Tuple_8_t121772413 *, const MethodInfo*))Tuple_8_UniRx_ITuple_ToString_m634988430_gshared)(__this, method)
// T1 UniRx.Tuple`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,UniRx.Tuple`1<System.Object>>::get_Item1()
extern "C"  Il2CppObject * Tuple_8_get_Item1_m1880476325_gshared (Tuple_8_t121772413 * __this, const MethodInfo* method);
#define Tuple_8_get_Item1_m1880476325(__this, method) ((  Il2CppObject * (*) (Tuple_8_t121772413 *, const MethodInfo*))Tuple_8_get_Item1_m1880476325_gshared)(__this, method)
// T2 UniRx.Tuple`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,UniRx.Tuple`1<System.Object>>::get_Item2()
extern "C"  Il2CppObject * Tuple_8_get_Item2_m223471045_gshared (Tuple_8_t121772413 * __this, const MethodInfo* method);
#define Tuple_8_get_Item2_m223471045(__this, method) ((  Il2CppObject * (*) (Tuple_8_t121772413 *, const MethodInfo*))Tuple_8_get_Item2_m223471045_gshared)(__this, method)
// T3 UniRx.Tuple`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,UniRx.Tuple`1<System.Object>>::get_Item3()
extern "C"  Il2CppObject * Tuple_8_get_Item3_m2861433061_gshared (Tuple_8_t121772413 * __this, const MethodInfo* method);
#define Tuple_8_get_Item3_m2861433061(__this, method) ((  Il2CppObject * (*) (Tuple_8_t121772413 *, const MethodInfo*))Tuple_8_get_Item3_m2861433061_gshared)(__this, method)
// T4 UniRx.Tuple`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,UniRx.Tuple`1<System.Object>>::get_Item4()
extern "C"  Il2CppObject * Tuple_8_get_Item4_m1204427781_gshared (Tuple_8_t121772413 * __this, const MethodInfo* method);
#define Tuple_8_get_Item4_m1204427781(__this, method) ((  Il2CppObject * (*) (Tuple_8_t121772413 *, const MethodInfo*))Tuple_8_get_Item4_m1204427781_gshared)(__this, method)
// T5 UniRx.Tuple`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,UniRx.Tuple`1<System.Object>>::get_Item5()
extern "C"  Il2CppObject * Tuple_8_get_Item5_m3842389797_gshared (Tuple_8_t121772413 * __this, const MethodInfo* method);
#define Tuple_8_get_Item5_m3842389797(__this, method) ((  Il2CppObject * (*) (Tuple_8_t121772413 *, const MethodInfo*))Tuple_8_get_Item5_m3842389797_gshared)(__this, method)
// T6 UniRx.Tuple`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,UniRx.Tuple`1<System.Object>>::get_Item6()
extern "C"  Il2CppObject * Tuple_8_get_Item6_m2185384517_gshared (Tuple_8_t121772413 * __this, const MethodInfo* method);
#define Tuple_8_get_Item6_m2185384517(__this, method) ((  Il2CppObject * (*) (Tuple_8_t121772413 *, const MethodInfo*))Tuple_8_get_Item6_m2185384517_gshared)(__this, method)
// T7 UniRx.Tuple`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,UniRx.Tuple`1<System.Object>>::get_Item7()
extern "C"  Il2CppObject * Tuple_8_get_Item7_m528379237_gshared (Tuple_8_t121772413 * __this, const MethodInfo* method);
#define Tuple_8_get_Item7_m528379237(__this, method) ((  Il2CppObject * (*) (Tuple_8_t121772413 *, const MethodInfo*))Tuple_8_get_Item7_m528379237_gshared)(__this, method)
// TRest UniRx.Tuple`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,UniRx.Tuple`1<System.Object>>::get_Rest()
extern "C"  Tuple_1_t3656007660  Tuple_8_get_Rest_m3857193452_gshared (Tuple_8_t121772413 * __this, const MethodInfo* method);
#define Tuple_8_get_Rest_m3857193452(__this, method) ((  Tuple_1_t3656007660  (*) (Tuple_8_t121772413 *, const MethodInfo*))Tuple_8_get_Rest_m3857193452_gshared)(__this, method)
// System.Boolean UniRx.Tuple`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,UniRx.Tuple`1<System.Object>>::Equals(System.Object)
extern "C"  bool Tuple_8_Equals_m144498775_gshared (Tuple_8_t121772413 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define Tuple_8_Equals_m144498775(__this, ___obj0, method) ((  bool (*) (Tuple_8_t121772413 *, Il2CppObject *, const MethodInfo*))Tuple_8_Equals_m144498775_gshared)(__this, ___obj0, method)
// System.Int32 UniRx.Tuple`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,UniRx.Tuple`1<System.Object>>::GetHashCode()
extern "C"  int32_t Tuple_8_GetHashCode_m1227691631_gshared (Tuple_8_t121772413 * __this, const MethodInfo* method);
#define Tuple_8_GetHashCode_m1227691631(__this, method) ((  int32_t (*) (Tuple_8_t121772413 *, const MethodInfo*))Tuple_8_GetHashCode_m1227691631_gshared)(__this, method)
// System.String UniRx.Tuple`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,UniRx.Tuple`1<System.Object>>::ToString()
extern "C"  String_t* Tuple_8_ToString_m237798627_gshared (Tuple_8_t121772413 * __this, const MethodInfo* method);
#define Tuple_8_ToString_m237798627(__this, method) ((  String_t* (*) (Tuple_8_t121772413 *, const MethodInfo*))Tuple_8_ToString_m237798627_gshared)(__this, method)
// System.Boolean UniRx.Tuple`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,UniRx.Tuple`1<System.Object>>::Equals(UniRx.Tuple`8<T1,T2,T3,T4,T5,T6,T7,TRest>)
extern "C"  bool Tuple_8_Equals_m567371111_gshared (Tuple_8_t121772413 * __this, Tuple_8_t121772413 * ___other0, const MethodInfo* method);
#define Tuple_8_Equals_m567371111(__this, ___other0, method) ((  bool (*) (Tuple_8_t121772413 *, Tuple_8_t121772413 *, const MethodInfo*))Tuple_8_Equals_m567371111_gshared)(__this, ___other0, method)
