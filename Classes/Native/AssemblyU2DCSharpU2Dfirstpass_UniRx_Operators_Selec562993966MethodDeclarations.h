﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectManyObservable`2<System.Object,System.Object>
struct SelectManyObservable_2_t562993966;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Func`2<System.Object,UniRx.IObservable`1<System.Object>>
struct Func_2_t1894581716;
// System.Func`3<System.Object,System.Int32,UniRx.IObservable`1<System.Object>>
struct Func_3_t3449466942;
// System.Func`2<System.Object,System.Collections.Generic.IEnumerable`1<System.Object>>
struct Func_2_t712970412;
// System.Func`3<System.Object,System.Int32,System.Collections.Generic.IEnumerable`1<System.Object>>
struct Func_3_t2267855638;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.SelectManyObservable`2<System.Object,System.Object>::.ctor(UniRx.IObservable`1<TSource>,System.Func`2<TSource,UniRx.IObservable`1<TResult>>)
extern "C"  void SelectManyObservable_2__ctor_m136900612_gshared (SelectManyObservable_2_t562993966 * __this, Il2CppObject* ___source0, Func_2_t1894581716 * ___selector1, const MethodInfo* method);
#define SelectManyObservable_2__ctor_m136900612(__this, ___source0, ___selector1, method) ((  void (*) (SelectManyObservable_2_t562993966 *, Il2CppObject*, Func_2_t1894581716 *, const MethodInfo*))SelectManyObservable_2__ctor_m136900612_gshared)(__this, ___source0, ___selector1, method)
// System.Void UniRx.Operators.SelectManyObservable`2<System.Object,System.Object>::.ctor(UniRx.IObservable`1<TSource>,System.Func`3<TSource,System.Int32,UniRx.IObservable`1<TResult>>)
extern "C"  void SelectManyObservable_2__ctor_m89255032_gshared (SelectManyObservable_2_t562993966 * __this, Il2CppObject* ___source0, Func_3_t3449466942 * ___selector1, const MethodInfo* method);
#define SelectManyObservable_2__ctor_m89255032(__this, ___source0, ___selector1, method) ((  void (*) (SelectManyObservable_2_t562993966 *, Il2CppObject*, Func_3_t3449466942 *, const MethodInfo*))SelectManyObservable_2__ctor_m89255032_gshared)(__this, ___source0, ___selector1, method)
// System.Void UniRx.Operators.SelectManyObservable`2<System.Object,System.Object>::.ctor(UniRx.IObservable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
extern "C"  void SelectManyObservable_2__ctor_m4000859532_gshared (SelectManyObservable_2_t562993966 * __this, Il2CppObject* ___source0, Func_2_t712970412 * ___selector1, const MethodInfo* method);
#define SelectManyObservable_2__ctor_m4000859532(__this, ___source0, ___selector1, method) ((  void (*) (SelectManyObservable_2_t562993966 *, Il2CppObject*, Func_2_t712970412 *, const MethodInfo*))SelectManyObservable_2__ctor_m4000859532_gshared)(__this, ___source0, ___selector1, method)
// System.Void UniRx.Operators.SelectManyObservable`2<System.Object,System.Object>::.ctor(UniRx.IObservable`1<TSource>,System.Func`3<TSource,System.Int32,System.Collections.Generic.IEnumerable`1<TResult>>)
extern "C"  void SelectManyObservable_2__ctor_m3533527448_gshared (SelectManyObservable_2_t562993966 * __this, Il2CppObject* ___source0, Func_3_t2267855638 * ___selector1, const MethodInfo* method);
#define SelectManyObservable_2__ctor_m3533527448(__this, ___source0, ___selector1, method) ((  void (*) (SelectManyObservable_2_t562993966 *, Il2CppObject*, Func_3_t2267855638 *, const MethodInfo*))SelectManyObservable_2__ctor_m3533527448_gshared)(__this, ___source0, ___selector1, method)
// System.IDisposable UniRx.Operators.SelectManyObservable`2<System.Object,System.Object>::SubscribeCore(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  Il2CppObject * SelectManyObservable_2_SubscribeCore_m3166260151_gshared (SelectManyObservable_2_t562993966 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define SelectManyObservable_2_SubscribeCore_m3166260151(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (SelectManyObservable_2_t562993966 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SelectManyObservable_2_SubscribeCore_m3166260151_gshared)(__this, ___observer0, ___cancel1, method)
