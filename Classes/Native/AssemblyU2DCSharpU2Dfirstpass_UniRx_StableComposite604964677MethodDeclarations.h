﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.StableCompositeDisposable/Trinary
struct Trinary_t604964677;
// System.IDisposable
struct IDisposable_t1628921374;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.StableCompositeDisposable/Trinary::.ctor(System.IDisposable,System.IDisposable,System.IDisposable)
extern "C"  void Trinary__ctor_m339876131 (Trinary_t604964677 * __this, Il2CppObject * ___disposable10, Il2CppObject * ___disposable21, Il2CppObject * ___disposable32, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.StableCompositeDisposable/Trinary::get_IsDisposed()
extern "C"  bool Trinary_get_IsDisposed_m2550072307 (Trinary_t604964677 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.StableCompositeDisposable/Trinary::Dispose()
extern "C"  void Trinary_Dispose_m2692035338 (Trinary_t604964677 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
