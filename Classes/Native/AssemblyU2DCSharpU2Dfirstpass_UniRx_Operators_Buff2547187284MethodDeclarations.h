﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.BufferObservable`2/Buffer/Buffer_<System.Object,System.Object>
struct Buffer__t2547187284;
// UniRx.Operators.BufferObservable`2/Buffer<System.Object,System.Object>
struct Buffer_t3527656087;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.BufferObservable`2/Buffer/Buffer_<System.Object,System.Object>::.ctor(UniRx.Operators.BufferObservable`2/Buffer<TSource,TWindowBoundary>)
extern "C"  void Buffer___ctor_m1018972210_gshared (Buffer__t2547187284 * __this, Buffer_t3527656087 * ___parent0, const MethodInfo* method);
#define Buffer___ctor_m1018972210(__this, ___parent0, method) ((  void (*) (Buffer__t2547187284 *, Buffer_t3527656087 *, const MethodInfo*))Buffer___ctor_m1018972210_gshared)(__this, ___parent0, method)
// System.Void UniRx.Operators.BufferObservable`2/Buffer/Buffer_<System.Object,System.Object>::OnNext(TWindowBoundary)
extern "C"  void Buffer__OnNext_m2817482694_gshared (Buffer__t2547187284 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Buffer__OnNext_m2817482694(__this, ___value0, method) ((  void (*) (Buffer__t2547187284 *, Il2CppObject *, const MethodInfo*))Buffer__OnNext_m2817482694_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.BufferObservable`2/Buffer/Buffer_<System.Object,System.Object>::OnError(System.Exception)
extern "C"  void Buffer__OnError_m1902719023_gshared (Buffer__t2547187284 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Buffer__OnError_m1902719023(__this, ___error0, method) ((  void (*) (Buffer__t2547187284 *, Exception_t1967233988 *, const MethodInfo*))Buffer__OnError_m1902719023_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.BufferObservable`2/Buffer/Buffer_<System.Object,System.Object>::OnCompleted()
extern "C"  void Buffer__OnCompleted_m2203283970_gshared (Buffer__t2547187284 * __this, const MethodInfo* method);
#define Buffer__OnCompleted_m2203283970(__this, method) ((  void (*) (Buffer__t2547187284 *, const MethodInfo*))Buffer__OnCompleted_m2203283970_gshared)(__this, method)
