﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.LinkCustomIDResult
struct LinkCustomIDResult_t297504339;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.LinkCustomIDResult::.ctor()
extern "C"  void LinkCustomIDResult__ctor_m66981066 (LinkCustomIDResult_t297504339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
