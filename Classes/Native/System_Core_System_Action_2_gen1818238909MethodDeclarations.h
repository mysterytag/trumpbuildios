﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_2_gen2210548610MethodDeclarations.h"

// System.Void System.Action`2<System.DateTime,ConnectionCollector>::.ctor(System.Object,System.IntPtr)
#define Action_2__ctor_m3590451133(__this, ___object0, ___method1, method) ((  void (*) (Action_2_t1818238909 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_2__ctor_m3283378366_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`2<System.DateTime,ConnectionCollector>::Invoke(T1,T2)
#define Action_2_Invoke_m3145059646(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t1818238909 *, DateTime_t339033936 , ConnectionCollector_t444796719 *, const MethodInfo*))Action_2_Invoke_m1100612781_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Action`2<System.DateTime,ConnectionCollector>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Action_2_BeginInvoke_m2386152029(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Action_2_t1818238909 *, DateTime_t339033936 , ConnectionCollector_t444796719 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Action_2_BeginInvoke_m1192547276_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// System.Void System.Action`2<System.DateTime,ConnectionCollector>::EndInvoke(System.IAsyncResult)
#define Action_2_EndInvoke_m2737843549(__this, ___result0, method) ((  void (*) (Action_2_t1818238909 *, Il2CppObject *, const MethodInfo*))Action_2_EndInvoke_m1013047758_gshared)(__this, ___result0, method)
