﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionRepl1373905935MethodDeclarations.h"

// System.Void UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::.ctor(System.Int32,T,T)
#define CollectionReplaceEvent_1__ctor_m2467496951(__this, ___index0, ___oldValue1, ___newValue2, method) ((  void (*) (CollectionReplaceEvent_1_t3193219059 *, int32_t, Tuple_2_t2188574943 , Tuple_2_t2188574943 , const MethodInfo*))CollectionReplaceEvent_1__ctor_m3073699305_gshared)(__this, ___index0, ___oldValue1, ___newValue2, method)
// System.Int32 UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::get_Index()
#define CollectionReplaceEvent_1_get_Index_m4247709427(__this, method) ((  int32_t (*) (CollectionReplaceEvent_1_t3193219059 *, const MethodInfo*))CollectionReplaceEvent_1_get_Index_m4036036665_gshared)(__this, method)
// System.Void UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::set_Index(System.Int32)
#define CollectionReplaceEvent_1_set_Index_m977635034(__this, ___value0, method) ((  void (*) (CollectionReplaceEvent_1_t3193219059 *, int32_t, const MethodInfo*))CollectionReplaceEvent_1_set_Index_m1583837388_gshared)(__this, ___value0, method)
// T UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::get_OldValue()
#define CollectionReplaceEvent_1_get_OldValue_m1800879810(__this, method) ((  Tuple_2_t2188574943  (*) (CollectionReplaceEvent_1_t3193219059 *, const MethodInfo*))CollectionReplaceEvent_1_get_OldValue_m2151637166_gshared)(__this, method)
// System.Void UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::set_OldValue(T)
#define CollectionReplaceEvent_1_set_OldValue_m3983659881(__this, ___value0, method) ((  void (*) (CollectionReplaceEvent_1_t3193219059 *, Tuple_2_t2188574943 , const MethodInfo*))CollectionReplaceEvent_1_set_OldValue_m2601373531_gshared)(__this, ___value0, method)
// T UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::get_NewValue()
#define CollectionReplaceEvent_1_get_NewValue_m518330665(__this, method) ((  Tuple_2_t2188574943  (*) (CollectionReplaceEvent_1_t3193219059 *, const MethodInfo*))CollectionReplaceEvent_1_get_NewValue_m869088021_gshared)(__this, method)
// System.Void UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::set_NewValue(T)
#define CollectionReplaceEvent_1_set_NewValue_m2879342050(__this, ___value0, method) ((  void (*) (CollectionReplaceEvent_1_t3193219059 *, Tuple_2_t2188574943 , const MethodInfo*))CollectionReplaceEvent_1_set_NewValue_m1497055700_gshared)(__this, ___value0, method)
// System.String UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::ToString()
#define CollectionReplaceEvent_1_ToString_m4027255927(__this, method) ((  String_t* (*) (CollectionReplaceEvent_1_t3193219059 *, const MethodInfo*))CollectionReplaceEvent_1_ToString_m4234752299_gshared)(__this, method)
// System.Int32 UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::GetHashCode()
#define CollectionReplaceEvent_1_GetHashCode_m823707035(__this, method) ((  int32_t (*) (CollectionReplaceEvent_1_t3193219059 *, const MethodInfo*))CollectionReplaceEvent_1_GetHashCode_m3564612961_gshared)(__this, method)
// System.Boolean UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::Equals(UniRx.CollectionReplaceEvent`1<T>)
#define CollectionReplaceEvent_1_Equals_m3773592936(__this, ___other0, method) ((  bool (*) (CollectionReplaceEvent_1_t3193219059 *, CollectionReplaceEvent_1_t3193219059 , const MethodInfo*))CollectionReplaceEvent_1_Equals_m2277216290_gshared)(__this, ___other0, method)
