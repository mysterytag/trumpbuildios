﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.LoginWithCustomIDRequest
struct LoginWithCustomIDRequest_t3362330884;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen3097043249.h"

// System.Void PlayFab.ClientModels.LoginWithCustomIDRequest::.ctor()
extern "C"  void LoginWithCustomIDRequest__ctor_m3572265401 (LoginWithCustomIDRequest_t3362330884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LoginWithCustomIDRequest::get_TitleId()
extern "C"  String_t* LoginWithCustomIDRequest_get_TitleId_m2735533348 (LoginWithCustomIDRequest_t3362330884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LoginWithCustomIDRequest::set_TitleId(System.String)
extern "C"  void LoginWithCustomIDRequest_set_TitleId_m3416292501 (LoginWithCustomIDRequest_t3362330884 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LoginWithCustomIDRequest::get_CustomId()
extern "C"  String_t* LoginWithCustomIDRequest_get_CustomId_m3409343773 (LoginWithCustomIDRequest_t3362330884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LoginWithCustomIDRequest::set_CustomId(System.String)
extern "C"  void LoginWithCustomIDRequest_set_CustomId_m4136163246 (LoginWithCustomIDRequest_t3362330884 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.LoginWithCustomIDRequest::get_CreateAccount()
extern "C"  Nullable_1_t3097043249  LoginWithCustomIDRequest_get_CreateAccount_m650424392 (LoginWithCustomIDRequest_t3362330884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LoginWithCustomIDRequest::set_CreateAccount(System.Nullable`1<System.Boolean>)
extern "C"  void LoginWithCustomIDRequest_set_CreateAccount_m3209891901 (LoginWithCustomIDRequest_t3362330884 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
