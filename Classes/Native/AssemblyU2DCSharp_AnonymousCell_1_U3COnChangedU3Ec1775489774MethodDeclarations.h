﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnonymousCell`1/<OnChanged>c__AnonStoreyFA<System.Object>
struct U3COnChangedU3Ec__AnonStoreyFA_t1775489774;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void AnonymousCell`1/<OnChanged>c__AnonStoreyFA<System.Object>::.ctor()
extern "C"  void U3COnChangedU3Ec__AnonStoreyFA__ctor_m2934523675_gshared (U3COnChangedU3Ec__AnonStoreyFA_t1775489774 * __this, const MethodInfo* method);
#define U3COnChangedU3Ec__AnonStoreyFA__ctor_m2934523675(__this, method) ((  void (*) (U3COnChangedU3Ec__AnonStoreyFA_t1775489774 *, const MethodInfo*))U3COnChangedU3Ec__AnonStoreyFA__ctor_m2934523675_gshared)(__this, method)
// System.Void AnonymousCell`1/<OnChanged>c__AnonStoreyFA<System.Object>::<>m__170(T)
extern "C"  void U3COnChangedU3Ec__AnonStoreyFA_U3CU3Em__170_m1769851606_gshared (U3COnChangedU3Ec__AnonStoreyFA_t1775489774 * __this, Il2CppObject * ____0, const MethodInfo* method);
#define U3COnChangedU3Ec__AnonStoreyFA_U3CU3Em__170_m1769851606(__this, ____0, method) ((  void (*) (U3COnChangedU3Ec__AnonStoreyFA_t1775489774 *, Il2CppObject *, const MethodInfo*))U3COnChangedU3Ec__AnonStoreyFA_U3CU3Em__170_m1769851606_gshared)(__this, ____0, method)
