﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen567907229MethodDeclarations.h"

// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.RectOffset>::.ctor(System.Object,System.IntPtr)
#define DOSetter_1__ctor_m2281334075(__this, ___object0, ___method1, method) ((  void (*) (DOSetter_1_t3124971693 *, Il2CppObject *, IntPtr_t, const MethodInfo*))DOSetter_1__ctor_m1482065619_gshared)(__this, ___object0, ___method1, method)
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.RectOffset>::Invoke(T)
#define DOSetter_1_Invoke_m1209469609(__this, ___pNewValue0, method) ((  void (*) (DOSetter_1_t3124971693 *, RectOffset_t3394170884 *, const MethodInfo*))DOSetter_1_Invoke_m2178813457_gshared)(__this, ___pNewValue0, method)
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<UnityEngine.RectOffset>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define DOSetter_1_BeginInvoke_m2722413310(__this, ___pNewValue0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (DOSetter_1_t3124971693 *, RectOffset_t3394170884 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))DOSetter_1_BeginInvoke_m2069320926_gshared)(__this, ___pNewValue0, ___callback1, ___object2, method)
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.RectOffset>::EndInvoke(System.IAsyncResult)
#define DOSetter_1_EndInvoke_m3266292939(__this, ___result0, method) ((  void (*) (DOSetter_1_t3124971693 *, Il2CppObject *, const MethodInfo*))DOSetter_1_EndInvoke_m1925995107_gshared)(__this, ___result0, method)
