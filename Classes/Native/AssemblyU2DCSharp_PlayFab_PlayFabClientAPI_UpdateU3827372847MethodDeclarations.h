﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/UpdateUserTitleDisplayNameResponseCallback
struct UpdateUserTitleDisplayNameResponseCallback_t3827372847;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.UpdateUserTitleDisplayNameRequest
struct UpdateUserTitleDisplayNameRequest_t2467685622;
// PlayFab.ClientModels.UpdateUserTitleDisplayNameResult
struct UpdateUserTitleDisplayNameResult_t807487222;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UpdateUserT2467685622.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UpdateUserTi807487222.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/UpdateUserTitleDisplayNameResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void UpdateUserTitleDisplayNameResponseCallback__ctor_m1551477918 (UpdateUserTitleDisplayNameResponseCallback_t3827372847 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UpdateUserTitleDisplayNameResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.UpdateUserTitleDisplayNameRequest,PlayFab.ClientModels.UpdateUserTitleDisplayNameResult,PlayFab.PlayFabError,System.Object)
extern "C"  void UpdateUserTitleDisplayNameResponseCallback_Invoke_m1209343563 (UpdateUserTitleDisplayNameResponseCallback_t3827372847 * __this, String_t* ___urlPath0, int32_t ___callId1, UpdateUserTitleDisplayNameRequest_t2467685622 * ___request2, UpdateUserTitleDisplayNameResult_t807487222 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_UpdateUserTitleDisplayNameResponseCallback_t3827372847(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, UpdateUserTitleDisplayNameRequest_t2467685622 * ___request2, UpdateUserTitleDisplayNameResult_t807487222 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/UpdateUserTitleDisplayNameResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.UpdateUserTitleDisplayNameRequest,PlayFab.ClientModels.UpdateUserTitleDisplayNameResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UpdateUserTitleDisplayNameResponseCallback_BeginInvoke_m83482936 (UpdateUserTitleDisplayNameResponseCallback_t3827372847 * __this, String_t* ___urlPath0, int32_t ___callId1, UpdateUserTitleDisplayNameRequest_t2467685622 * ___request2, UpdateUserTitleDisplayNameResult_t807487222 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UpdateUserTitleDisplayNameResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void UpdateUserTitleDisplayNameResponseCallback_EndInvoke_m4234681774 (UpdateUserTitleDisplayNameResponseCallback_t3827372847 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
