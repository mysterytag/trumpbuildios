﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.DistinctUntilChangedObservable`1<System.Boolean>
struct DistinctUntilChangedObservable_1_t3199977885;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Operat60103313.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.DistinctUntilChangedObservable`1/DistinctUntilChanged<System.Boolean>
struct  DistinctUntilChanged_t1676912132  : public OperatorObserverBase_2_t60103313
{
public:
	// UniRx.Operators.DistinctUntilChangedObservable`1<T> UniRx.Operators.DistinctUntilChangedObservable`1/DistinctUntilChanged::parent
	DistinctUntilChangedObservable_1_t3199977885 * ___parent_2;
	// System.Boolean UniRx.Operators.DistinctUntilChangedObservable`1/DistinctUntilChanged::isFirst
	bool ___isFirst_3;
	// T UniRx.Operators.DistinctUntilChangedObservable`1/DistinctUntilChanged::prevKey
	bool ___prevKey_4;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(DistinctUntilChanged_t1676912132, ___parent_2)); }
	inline DistinctUntilChangedObservable_1_t3199977885 * get_parent_2() const { return ___parent_2; }
	inline DistinctUntilChangedObservable_1_t3199977885 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(DistinctUntilChangedObservable_1_t3199977885 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_isFirst_3() { return static_cast<int32_t>(offsetof(DistinctUntilChanged_t1676912132, ___isFirst_3)); }
	inline bool get_isFirst_3() const { return ___isFirst_3; }
	inline bool* get_address_of_isFirst_3() { return &___isFirst_3; }
	inline void set_isFirst_3(bool value)
	{
		___isFirst_3 = value;
	}

	inline static int32_t get_offset_of_prevKey_4() { return static_cast<int32_t>(offsetof(DistinctUntilChanged_t1676912132, ___prevKey_4)); }
	inline bool get_prevKey_4() const { return ___prevKey_4; }
	inline bool* get_address_of_prevKey_4() { return &___prevKey_4; }
	inline void set_prevKey_4(bool value)
	{
		___prevKey_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
