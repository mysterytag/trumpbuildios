﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// UniRx.Subject`1<UniRx.Tuple`2<System.Object,System.Object>>
struct Subject_1_t2307296439;
// UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Object>>
struct IObserver_1_t2581260722;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Subject`1/Subscription<UniRx.Tuple`2<System.Object,System.Object>>
struct  Subscription_t2443475637  : public Il2CppObject
{
public:
	// System.Object UniRx.Subject`1/Subscription::gate
	Il2CppObject * ___gate_0;
	// UniRx.Subject`1<T> UniRx.Subject`1/Subscription::parent
	Subject_1_t2307296439 * ___parent_1;
	// UniRx.IObserver`1<T> UniRx.Subject`1/Subscription::unsubscribeTarget
	Il2CppObject* ___unsubscribeTarget_2;

public:
	inline static int32_t get_offset_of_gate_0() { return static_cast<int32_t>(offsetof(Subscription_t2443475637, ___gate_0)); }
	inline Il2CppObject * get_gate_0() const { return ___gate_0; }
	inline Il2CppObject ** get_address_of_gate_0() { return &___gate_0; }
	inline void set_gate_0(Il2CppObject * value)
	{
		___gate_0 = value;
		Il2CppCodeGenWriteBarrier(&___gate_0, value);
	}

	inline static int32_t get_offset_of_parent_1() { return static_cast<int32_t>(offsetof(Subscription_t2443475637, ___parent_1)); }
	inline Subject_1_t2307296439 * get_parent_1() const { return ___parent_1; }
	inline Subject_1_t2307296439 ** get_address_of_parent_1() { return &___parent_1; }
	inline void set_parent_1(Subject_1_t2307296439 * value)
	{
		___parent_1 = value;
		Il2CppCodeGenWriteBarrier(&___parent_1, value);
	}

	inline static int32_t get_offset_of_unsubscribeTarget_2() { return static_cast<int32_t>(offsetof(Subscription_t2443475637, ___unsubscribeTarget_2)); }
	inline Il2CppObject* get_unsubscribeTarget_2() const { return ___unsubscribeTarget_2; }
	inline Il2CppObject** get_address_of_unsubscribeTarget_2() { return &___unsubscribeTarget_2; }
	inline void set_unsubscribeTarget_2(Il2CppObject* value)
	{
		___unsubscribeTarget_2 = value;
		Il2CppCodeGenWriteBarrier(&___unsubscribeTarget_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
