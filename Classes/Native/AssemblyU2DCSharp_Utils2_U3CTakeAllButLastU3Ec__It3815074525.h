﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t3709260776;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2320212868;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils2/<TakeAllButLast>c__Iterator29`1<System.Object>
struct  U3CTakeAllButLastU3Ec__Iterator29_1_t3815074525  : public Il2CppObject
{
public:
	// System.Collections.Generic.IEnumerable`1<T> Utils2/<TakeAllButLast>c__Iterator29`1::source
	Il2CppObject* ___source_0;
	// System.Collections.Generic.IEnumerator`1<T> Utils2/<TakeAllButLast>c__Iterator29`1::<it>__0
	Il2CppObject* ___U3CitU3E__0_1;
	// System.Boolean Utils2/<TakeAllButLast>c__Iterator29`1::<hasRemainingItems>__1
	bool ___U3ChasRemainingItemsU3E__1_2;
	// System.Boolean Utils2/<TakeAllButLast>c__Iterator29`1::<isFirst>__2
	bool ___U3CisFirstU3E__2_3;
	// T Utils2/<TakeAllButLast>c__Iterator29`1::<item>__3
	Il2CppObject * ___U3CitemU3E__3_4;
	// System.Int32 Utils2/<TakeAllButLast>c__Iterator29`1::$PC
	int32_t ___U24PC_5;
	// T Utils2/<TakeAllButLast>c__Iterator29`1::$current
	Il2CppObject * ___U24current_6;
	// System.Collections.Generic.IEnumerable`1<T> Utils2/<TakeAllButLast>c__Iterator29`1::<$>source
	Il2CppObject* ___U3CU24U3Esource_7;

public:
	inline static int32_t get_offset_of_source_0() { return static_cast<int32_t>(offsetof(U3CTakeAllButLastU3Ec__Iterator29_1_t3815074525, ___source_0)); }
	inline Il2CppObject* get_source_0() const { return ___source_0; }
	inline Il2CppObject** get_address_of_source_0() { return &___source_0; }
	inline void set_source_0(Il2CppObject* value)
	{
		___source_0 = value;
		Il2CppCodeGenWriteBarrier(&___source_0, value);
	}

	inline static int32_t get_offset_of_U3CitU3E__0_1() { return static_cast<int32_t>(offsetof(U3CTakeAllButLastU3Ec__Iterator29_1_t3815074525, ___U3CitU3E__0_1)); }
	inline Il2CppObject* get_U3CitU3E__0_1() const { return ___U3CitU3E__0_1; }
	inline Il2CppObject** get_address_of_U3CitU3E__0_1() { return &___U3CitU3E__0_1; }
	inline void set_U3CitU3E__0_1(Il2CppObject* value)
	{
		___U3CitU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CitU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U3ChasRemainingItemsU3E__1_2() { return static_cast<int32_t>(offsetof(U3CTakeAllButLastU3Ec__Iterator29_1_t3815074525, ___U3ChasRemainingItemsU3E__1_2)); }
	inline bool get_U3ChasRemainingItemsU3E__1_2() const { return ___U3ChasRemainingItemsU3E__1_2; }
	inline bool* get_address_of_U3ChasRemainingItemsU3E__1_2() { return &___U3ChasRemainingItemsU3E__1_2; }
	inline void set_U3ChasRemainingItemsU3E__1_2(bool value)
	{
		___U3ChasRemainingItemsU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U3CisFirstU3E__2_3() { return static_cast<int32_t>(offsetof(U3CTakeAllButLastU3Ec__Iterator29_1_t3815074525, ___U3CisFirstU3E__2_3)); }
	inline bool get_U3CisFirstU3E__2_3() const { return ___U3CisFirstU3E__2_3; }
	inline bool* get_address_of_U3CisFirstU3E__2_3() { return &___U3CisFirstU3E__2_3; }
	inline void set_U3CisFirstU3E__2_3(bool value)
	{
		___U3CisFirstU3E__2_3 = value;
	}

	inline static int32_t get_offset_of_U3CitemU3E__3_4() { return static_cast<int32_t>(offsetof(U3CTakeAllButLastU3Ec__Iterator29_1_t3815074525, ___U3CitemU3E__3_4)); }
	inline Il2CppObject * get_U3CitemU3E__3_4() const { return ___U3CitemU3E__3_4; }
	inline Il2CppObject ** get_address_of_U3CitemU3E__3_4() { return &___U3CitemU3E__3_4; }
	inline void set_U3CitemU3E__3_4(Il2CppObject * value)
	{
		___U3CitemU3E__3_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CitemU3E__3_4, value);
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CTakeAllButLastU3Ec__Iterator29_1_t3815074525, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CTakeAllButLastU3Ec__Iterator29_1_t3815074525, ___U24current_6)); }
	inline Il2CppObject * get_U24current_6() const { return ___U24current_6; }
	inline Il2CppObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(Il2CppObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Esource_7() { return static_cast<int32_t>(offsetof(U3CTakeAllButLastU3Ec__Iterator29_1_t3815074525, ___U3CU24U3Esource_7)); }
	inline Il2CppObject* get_U3CU24U3Esource_7() const { return ___U3CU24U3Esource_7; }
	inline Il2CppObject** get_address_of_U3CU24U3Esource_7() { return &___U3CU24U3Esource_7; }
	inline void set_U3CU24U3Esource_7(Il2CppObject* value)
	{
		___U3CU24U3Esource_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Esource_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
