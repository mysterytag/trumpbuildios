﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler
struct IgnoreTimeScaleMainThreadScheduler_t3837140013;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Action
struct Action_t437523947;
// UniRx.ICancelable
struct ICancelable_t4109686575;
// System.Object
struct Il2CppObject;
// System.IDisposable
struct IDisposable_t1628921374;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_TimeSpan763862892.h"
#include "System_Core_System_Action437523947.h"
#include "mscorlib_System_DateTimeOffset3712260035.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler::.ctor()
extern "C"  void IgnoreTimeScaleMainThreadScheduler__ctor_m2406972566 (IgnoreTimeScaleMainThreadScheduler_t3837140013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler::DelayAction(System.TimeSpan,System.Action,UniRx.ICancelable)
extern "C"  Il2CppObject * IgnoreTimeScaleMainThreadScheduler_DelayAction_m3134211101 (IgnoreTimeScaleMainThreadScheduler_t3837140013 * __this, TimeSpan_t763862892  ___dueTime0, Action_t437523947 * ___action1, Il2CppObject * ___cancellation2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler::PeriodicAction(System.TimeSpan,System.Action,UniRx.ICancelable)
extern "C"  Il2CppObject * IgnoreTimeScaleMainThreadScheduler_PeriodicAction_m4084215339 (IgnoreTimeScaleMainThreadScheduler_t3837140013 * __this, TimeSpan_t763862892  ___period0, Action_t437523947 * ___action1, Il2CppObject * ___cancellation2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTimeOffset UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler::get_Now()
extern "C"  DateTimeOffset_t3712260035  IgnoreTimeScaleMainThreadScheduler_get_Now_m2297306907 (IgnoreTimeScaleMainThreadScheduler_t3837140013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler::Schedule(System.Object)
extern "C"  void IgnoreTimeScaleMainThreadScheduler_Schedule_m2016594287 (IgnoreTimeScaleMainThreadScheduler_t3837140013 * __this, Il2CppObject * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler::Schedule(System.Action)
extern "C"  Il2CppObject * IgnoreTimeScaleMainThreadScheduler_Schedule_m75685693 (IgnoreTimeScaleMainThreadScheduler_t3837140013 * __this, Action_t437523947 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler::Schedule(System.DateTimeOffset,System.Action)
extern "C"  Il2CppObject * IgnoreTimeScaleMainThreadScheduler_Schedule_m1288276060 (IgnoreTimeScaleMainThreadScheduler_t3837140013 * __this, DateTimeOffset_t3712260035  ___dueTime0, Action_t437523947 * ___action1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler::Schedule(System.TimeSpan,System.Action)
extern "C"  Il2CppObject * IgnoreTimeScaleMainThreadScheduler_Schedule_m3747037459 (IgnoreTimeScaleMainThreadScheduler_t3837140013 * __this, TimeSpan_t763862892  ___dueTime0, Action_t437523947 * ___action1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler::SchedulePeriodic(System.TimeSpan,System.Action)
extern "C"  Il2CppObject * IgnoreTimeScaleMainThreadScheduler_SchedulePeriodic_m2084470264 (IgnoreTimeScaleMainThreadScheduler_t3837140013 * __this, TimeSpan_t763862892  ___period0, Action_t437523947 * ___action1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
