﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.FromCoroutine`1/FromCoroutineObserver<System.Object>
struct FromCoroutineObserver_t4015335165;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.FromCoroutine`1/FromCoroutineObserver<System.Object>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void FromCoroutineObserver__ctor_m2161387994_gshared (FromCoroutineObserver_t4015335165 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define FromCoroutineObserver__ctor_m2161387994(__this, ___observer0, ___cancel1, method) ((  void (*) (FromCoroutineObserver_t4015335165 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))FromCoroutineObserver__ctor_m2161387994_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.FromCoroutine`1/FromCoroutineObserver<System.Object>::OnNext(T)
extern "C"  void FromCoroutineObserver_OnNext_m1155718520_gshared (FromCoroutineObserver_t4015335165 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define FromCoroutineObserver_OnNext_m1155718520(__this, ___value0, method) ((  void (*) (FromCoroutineObserver_t4015335165 *, Il2CppObject *, const MethodInfo*))FromCoroutineObserver_OnNext_m1155718520_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.FromCoroutine`1/FromCoroutineObserver<System.Object>::OnError(System.Exception)
extern "C"  void FromCoroutineObserver_OnError_m2691500167_gshared (FromCoroutineObserver_t4015335165 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define FromCoroutineObserver_OnError_m2691500167(__this, ___error0, method) ((  void (*) (FromCoroutineObserver_t4015335165 *, Exception_t1967233988 *, const MethodInfo*))FromCoroutineObserver_OnError_m2691500167_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.FromCoroutine`1/FromCoroutineObserver<System.Object>::OnCompleted()
extern "C"  void FromCoroutineObserver_OnCompleted_m2312572506_gshared (FromCoroutineObserver_t4015335165 * __this, const MethodInfo* method);
#define FromCoroutineObserver_OnCompleted_m2312572506(__this, method) ((  void (*) (FromCoroutineObserver_t4015335165 *, const MethodInfo*))FromCoroutineObserver_OnCompleted_m2312572506_gshared)(__this, method)
