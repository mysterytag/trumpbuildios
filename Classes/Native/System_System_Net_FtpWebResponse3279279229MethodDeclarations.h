﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.FtpWebResponse
struct FtpWebResponse_t3279279229;
// System.Net.FtpWebRequest
struct FtpWebRequest_t2824072935;
// System.Uri
struct Uri_t2776692961;
// System.String
struct String_t;
// System.Net.FtpStatus
struct FtpStatus_t955148094;
// System.Net.WebHeaderCollection
struct WebHeaderCollection_t1099177929;
// System.IO.Stream
struct Stream_t219029575;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_FtpWebRequest2824072935.h"
#include "System_System_Uri2776692961.h"
#include "mscorlib_System_String968488902.h"
#include "System_System_Net_FtpStatusCode1094090571.h"
#include "System_System_Net_FtpStatus955148094.h"
#include "mscorlib_System_DateTime339033936.h"
#include "mscorlib_System_IO_Stream219029575.h"

// System.Void System.Net.FtpWebResponse::.ctor(System.Net.FtpWebRequest,System.Uri,System.String,System.Boolean)
extern "C"  void FtpWebResponse__ctor_m3542190242 (FtpWebResponse_t3279279229 * __this, FtpWebRequest_t2824072935 * ___request0, Uri_t2776692961 * ___uri1, String_t* ___method2, bool ___keepAlive3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.FtpWebResponse::.ctor(System.Net.FtpWebRequest,System.Uri,System.String,System.Net.FtpStatusCode,System.String)
extern "C"  void FtpWebResponse__ctor_m2217738742 (FtpWebResponse_t3279279229 * __this, FtpWebRequest_t2824072935 * ___request0, Uri_t2776692961 * ___uri1, String_t* ___method2, int32_t ___statusCode3, String_t* ___statusDescription4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.FtpWebResponse::.ctor(System.Net.FtpWebRequest,System.Uri,System.String,System.Net.FtpStatus)
extern "C"  void FtpWebResponse__ctor_m287332007 (FtpWebResponse_t3279279229 * __this, FtpWebRequest_t2824072935 * ___request0, Uri_t2776692961 * ___uri1, String_t* ___method2, FtpStatus_t955148094 * ___status3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.FtpWebResponse::get_ContentLength()
extern "C"  int64_t FtpWebResponse_get_ContentLength_m2147509100 (FtpWebResponse_t3279279229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebHeaderCollection System.Net.FtpWebResponse::get_Headers()
extern "C"  WebHeaderCollection_t1099177929 * FtpWebResponse_get_Headers_m584940466 (FtpWebResponse_t3279279229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri System.Net.FtpWebResponse::get_ResponseUri()
extern "C"  Uri_t2776692961 * FtpWebResponse_get_ResponseUri_m3281149241 (FtpWebResponse_t3279279229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Net.FtpWebResponse::get_LastModified()
extern "C"  DateTime_t339033936  FtpWebResponse_get_LastModified_m3581311276 (FtpWebResponse_t3279279229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.FtpWebResponse::set_LastModified(System.DateTime)
extern "C"  void FtpWebResponse_set_LastModified_m1304868287 (FtpWebResponse_t3279279229 * __this, DateTime_t339033936  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.FtpWebResponse::get_BannerMessage()
extern "C"  String_t* FtpWebResponse_get_BannerMessage_m470841882 (FtpWebResponse_t3279279229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.FtpWebResponse::set_BannerMessage(System.String)
extern "C"  void FtpWebResponse_set_BannerMessage_m1222407647 (FtpWebResponse_t3279279229 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.FtpWebResponse::get_WelcomeMessage()
extern "C"  String_t* FtpWebResponse_get_WelcomeMessage_m537656616 (FtpWebResponse_t3279279229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.FtpWebResponse::set_WelcomeMessage(System.String)
extern "C"  void FtpWebResponse_set_WelcomeMessage_m1191478019 (FtpWebResponse_t3279279229 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.FtpWebResponse::get_ExitMessage()
extern "C"  String_t* FtpWebResponse_get_ExitMessage_m1705041896 (FtpWebResponse_t3279279229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.FtpWebResponse::set_ExitMessage(System.String)
extern "C"  void FtpWebResponse_set_ExitMessage_m262451345 (FtpWebResponse_t3279279229 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.FtpStatusCode System.Net.FtpWebResponse::get_StatusCode()
extern "C"  int32_t FtpWebResponse_get_StatusCode_m387981751 (FtpWebResponse_t3279279229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.FtpWebResponse::set_StatusCode(System.Net.FtpStatusCode)
extern "C"  void FtpWebResponse_set_StatusCode_m3350416872 (FtpWebResponse_t3279279229 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.FtpWebResponse::get_StatusDescription()
extern "C"  String_t* FtpWebResponse_get_StatusDescription_m144686281 (FtpWebResponse_t3279279229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.FtpWebResponse::set_StatusDescription(System.String)
extern "C"  void FtpWebResponse_set_StatusDescription_m3055358992 (FtpWebResponse_t3279279229 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.FtpWebResponse::Close()
extern "C"  void FtpWebResponse_Close_m3876778333 (FtpWebResponse_t3279279229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.Net.FtpWebResponse::GetResponseStream()
extern "C"  Stream_t219029575 * FtpWebResponse_GetResponseStream_m2153035058 (FtpWebResponse_t3279279229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.FtpWebResponse::set_Stream(System.IO.Stream)
extern "C"  void FtpWebResponse_set_Stream_m4206595185 (FtpWebResponse_t3279279229 * __this, Stream_t219029575 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.Net.FtpWebResponse::get_Stream()
extern "C"  Stream_t219029575 * FtpWebResponse_get_Stream_m2532366000 (FtpWebResponse_t3279279229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.FtpWebResponse::UpdateStatus(System.Net.FtpStatus)
extern "C"  void FtpWebResponse_UpdateStatus_m2179474228 (FtpWebResponse_t3279279229 * __this, FtpStatus_t955148094 * ___status0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.FtpWebResponse::CheckDisposed()
extern "C"  void FtpWebResponse_CheckDisposed_m2215497074 (FtpWebResponse_t3279279229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.FtpWebResponse::IsFinal()
extern "C"  bool FtpWebResponse_IsFinal_m2524807527 (FtpWebResponse_t3279279229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
