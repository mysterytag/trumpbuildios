﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.MultilineReactivePropertyAttribute
struct MultilineReactivePropertyAttribute_t2518291827;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.MultilineReactivePropertyAttribute::.ctor()
extern "C"  void MultilineReactivePropertyAttribute__ctor_m117944694 (MultilineReactivePropertyAttribute_t2518291827 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.MultilineReactivePropertyAttribute::.ctor(System.Int32)
extern "C"  void MultilineReactivePropertyAttribute__ctor_m1392043463 (MultilineReactivePropertyAttribute_t2518291827 * __this, int32_t ___lines0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UniRx.MultilineReactivePropertyAttribute::get_Lines()
extern "C"  int32_t MultilineReactivePropertyAttribute_get_Lines_m2975535156 (MultilineReactivePropertyAttribute_t2518291827 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.MultilineReactivePropertyAttribute::set_Lines(System.Int32)
extern "C"  void MultilineReactivePropertyAttribute_set_Lines_m1542825415 (MultilineReactivePropertyAttribute_t2518291827 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
