﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<GetCharacterData>c__AnonStoreyCD
struct U3CGetCharacterDataU3Ec__AnonStoreyCD_t2676766019;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<GetCharacterData>c__AnonStoreyCD::.ctor()
extern "C"  void U3CGetCharacterDataU3Ec__AnonStoreyCD__ctor_m4256059856 (U3CGetCharacterDataU3Ec__AnonStoreyCD_t2676766019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<GetCharacterData>c__AnonStoreyCD::<>m__BA(PlayFab.CallRequestContainer)
extern "C"  void U3CGetCharacterDataU3Ec__AnonStoreyCD_U3CU3Em__BA_m2121508013 (U3CGetCharacterDataU3Ec__AnonStoreyCD_t2676766019 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
