﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// UniRx.Observable/<OnErrorRetry>c__AnonStorey3D`2<System.Object,System.Object>
struct U3COnErrorRetryU3Ec__AnonStorey3D_2_t4266989836;

#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_TimeSpan763862892.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/<OnErrorRetry>c__AnonStorey3D`2/<OnErrorRetry>c__AnonStorey3E`2<System.Object,System.Object>
struct  U3COnErrorRetryU3Ec__AnonStorey3E_2_t1069553519  : public Il2CppObject
{
public:
	// System.Int32 UniRx.Observable/<OnErrorRetry>c__AnonStorey3D`2/<OnErrorRetry>c__AnonStorey3E`2::count
	int32_t ___count_0;
	// System.TimeSpan UniRx.Observable/<OnErrorRetry>c__AnonStorey3D`2/<OnErrorRetry>c__AnonStorey3E`2::dueTime
	TimeSpan_t763862892  ___dueTime_1;
	// UniRx.IObservable`1<TSource> UniRx.Observable/<OnErrorRetry>c__AnonStorey3D`2/<OnErrorRetry>c__AnonStorey3E`2::self
	Il2CppObject* ___self_2;
	// UniRx.Observable/<OnErrorRetry>c__AnonStorey3D`2<TSource,TException> UniRx.Observable/<OnErrorRetry>c__AnonStorey3D`2/<OnErrorRetry>c__AnonStorey3E`2::<>f__ref$61
	U3COnErrorRetryU3Ec__AnonStorey3D_2_t4266989836 * ___U3CU3Ef__refU2461_3;

public:
	inline static int32_t get_offset_of_count_0() { return static_cast<int32_t>(offsetof(U3COnErrorRetryU3Ec__AnonStorey3E_2_t1069553519, ___count_0)); }
	inline int32_t get_count_0() const { return ___count_0; }
	inline int32_t* get_address_of_count_0() { return &___count_0; }
	inline void set_count_0(int32_t value)
	{
		___count_0 = value;
	}

	inline static int32_t get_offset_of_dueTime_1() { return static_cast<int32_t>(offsetof(U3COnErrorRetryU3Ec__AnonStorey3E_2_t1069553519, ___dueTime_1)); }
	inline TimeSpan_t763862892  get_dueTime_1() const { return ___dueTime_1; }
	inline TimeSpan_t763862892 * get_address_of_dueTime_1() { return &___dueTime_1; }
	inline void set_dueTime_1(TimeSpan_t763862892  value)
	{
		___dueTime_1 = value;
	}

	inline static int32_t get_offset_of_self_2() { return static_cast<int32_t>(offsetof(U3COnErrorRetryU3Ec__AnonStorey3E_2_t1069553519, ___self_2)); }
	inline Il2CppObject* get_self_2() const { return ___self_2; }
	inline Il2CppObject** get_address_of_self_2() { return &___self_2; }
	inline void set_self_2(Il2CppObject* value)
	{
		___self_2 = value;
		Il2CppCodeGenWriteBarrier(&___self_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU2461_3() { return static_cast<int32_t>(offsetof(U3COnErrorRetryU3Ec__AnonStorey3E_2_t1069553519, ___U3CU3Ef__refU2461_3)); }
	inline U3COnErrorRetryU3Ec__AnonStorey3D_2_t4266989836 * get_U3CU3Ef__refU2461_3() const { return ___U3CU3Ef__refU2461_3; }
	inline U3COnErrorRetryU3Ec__AnonStorey3D_2_t4266989836 ** get_address_of_U3CU3Ef__refU2461_3() { return &___U3CU3Ef__refU2461_3; }
	inline void set_U3CU3Ef__refU2461_3(U3COnErrorRetryU3Ec__AnonStorey3D_2_t4266989836 * value)
	{
		___U3CU3Ef__refU2461_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU2461_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
