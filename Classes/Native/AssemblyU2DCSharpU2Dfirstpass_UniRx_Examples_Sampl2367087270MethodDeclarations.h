﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Examples.Sample05_ConvertFromCoroutine
struct Sample05_ConvertFromCoroutine_t2367087270;
// UniRx.IObservable`1<System.String>
struct IObservable_1_t727287266;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// UniRx.IObserver`1<System.String>
struct IObserver_1_t3180487805;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560.h"

// System.Void UniRx.Examples.Sample05_ConvertFromCoroutine::.ctor()
extern "C"  void Sample05_ConvertFromCoroutine__ctor_m989510065 (Sample05_ConvertFromCoroutine_t2367087270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.String> UniRx.Examples.Sample05_ConvertFromCoroutine::GetWWW(System.String)
extern "C"  Il2CppObject* Sample05_ConvertFromCoroutine_GetWWW_m44874254 (Il2CppObject * __this /* static, unused */, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UniRx.Examples.Sample05_ConvertFromCoroutine::GetWWWCore(System.String,UniRx.IObserver`1<System.String>,UniRx.CancellationToken)
extern "C"  Il2CppObject * Sample05_ConvertFromCoroutine_GetWWWCore_m3349104995 (Il2CppObject * __this /* static, unused */, String_t* ___url0, Il2CppObject* ___observer1, CancellationToken_t1439151560  ___cancellationToken2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
