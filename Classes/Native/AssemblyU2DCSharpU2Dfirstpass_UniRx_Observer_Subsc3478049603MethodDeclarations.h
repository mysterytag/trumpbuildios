﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observer/Subscribe`1<UniRx.Tuple`2<System.Object,System.Object>>
struct Subscribe_1_t3478049603;
// System.Action`1<UniRx.Tuple`2<System.Object,System.Object>>
struct Action_1_t517714524;
// System.Action`1<System.Exception>
struct Action_1_t2115686693;
// System.Action
struct Action_t437523947;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen369261819.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Observer/Subscribe`1<UniRx.Tuple`2<System.Object,System.Object>>::.ctor(System.Action`1<T>,System.Action`1<System.Exception>,System.Action)
extern "C"  void Subscribe_1__ctor_m894586150_gshared (Subscribe_1_t3478049603 * __this, Action_1_t517714524 * ___onNext0, Action_1_t2115686693 * ___onError1, Action_t437523947 * ___onCompleted2, const MethodInfo* method);
#define Subscribe_1__ctor_m894586150(__this, ___onNext0, ___onError1, ___onCompleted2, method) ((  void (*) (Subscribe_1_t3478049603 *, Action_1_t517714524 *, Action_1_t2115686693 *, Action_t437523947 *, const MethodInfo*))Subscribe_1__ctor_m894586150_gshared)(__this, ___onNext0, ___onError1, ___onCompleted2, method)
// System.Void UniRx.Observer/Subscribe`1<UniRx.Tuple`2<System.Object,System.Object>>::OnNext(T)
extern "C"  void Subscribe_1_OnNext_m1782031363_gshared (Subscribe_1_t3478049603 * __this, Tuple_2_t369261819  ___value0, const MethodInfo* method);
#define Subscribe_1_OnNext_m1782031363(__this, ___value0, method) ((  void (*) (Subscribe_1_t3478049603 *, Tuple_2_t369261819 , const MethodInfo*))Subscribe_1_OnNext_m1782031363_gshared)(__this, ___value0, method)
// System.Void UniRx.Observer/Subscribe`1<UniRx.Tuple`2<System.Object,System.Object>>::OnError(System.Exception)
extern "C"  void Subscribe_1_OnError_m3417397522_gshared (Subscribe_1_t3478049603 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Subscribe_1_OnError_m3417397522(__this, ___error0, method) ((  void (*) (Subscribe_1_t3478049603 *, Exception_t1967233988 *, const MethodInfo*))Subscribe_1_OnError_m3417397522_gshared)(__this, ___error0, method)
// System.Void UniRx.Observer/Subscribe`1<UniRx.Tuple`2<System.Object,System.Object>>::OnCompleted()
extern "C"  void Subscribe_1_OnCompleted_m3539965797_gshared (Subscribe_1_t3478049603 * __this, const MethodInfo* method);
#define Subscribe_1_OnCompleted_m3539965797(__this, method) ((  void (*) (Subscribe_1_t3478049603 *, const MethodInfo*))Subscribe_1_OnCompleted_m3539965797_gshared)(__this, method)
