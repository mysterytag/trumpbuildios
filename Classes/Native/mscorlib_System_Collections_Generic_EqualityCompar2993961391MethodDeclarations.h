﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UniRx.Tuple`1<System.Object>>
struct DefaultComparer_t2993961391;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_1_gen3656007660.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UniRx.Tuple`1<System.Object>>::.ctor()
extern "C"  void DefaultComparer__ctor_m2299308583_gshared (DefaultComparer_t2993961391 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2299308583(__this, method) ((  void (*) (DefaultComparer_t2993961391 *, const MethodInfo*))DefaultComparer__ctor_m2299308583_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UniRx.Tuple`1<System.Object>>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m4020694252_gshared (DefaultComparer_t2993961391 * __this, Tuple_1_t3656007660  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m4020694252(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t2993961391 *, Tuple_1_t3656007660 , const MethodInfo*))DefaultComparer_GetHashCode_m4020694252_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UniRx.Tuple`1<System.Object>>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m682686396_gshared (DefaultComparer_t2993961391 * __this, Tuple_1_t3656007660  ___x0, Tuple_1_t3656007660  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m682686396(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t2993961391 *, Tuple_1_t3656007660 , Tuple_1_t3656007660 , const MethodInfo*))DefaultComparer_Equals_m682686396_gshared)(__this, ___x0, ___y1, method)
