﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Scheduler/MainThreadScheduler/<PeriodicAction>c__Iterator1A
struct U3CPeriodicActionU3Ec__Iterator1A_t3918433488;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Scheduler/MainThreadScheduler/<PeriodicAction>c__Iterator1A::.ctor()
extern "C"  void U3CPeriodicActionU3Ec__Iterator1A__ctor_m4127415114 (U3CPeriodicActionU3Ec__Iterator1A_t3918433488 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UniRx.Scheduler/MainThreadScheduler/<PeriodicAction>c__Iterator1A::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPeriodicActionU3Ec__Iterator1A_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1651268232 (U3CPeriodicActionU3Ec__Iterator1A_t3918433488 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UniRx.Scheduler/MainThreadScheduler/<PeriodicAction>c__Iterator1A::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPeriodicActionU3Ec__Iterator1A_System_Collections_IEnumerator_get_Current_m1400139292 (U3CPeriodicActionU3Ec__Iterator1A_t3918433488 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.Scheduler/MainThreadScheduler/<PeriodicAction>c__Iterator1A::MoveNext()
extern "C"  bool U3CPeriodicActionU3Ec__Iterator1A_MoveNext_m3388095874 (U3CPeriodicActionU3Ec__Iterator1A_t3918433488 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Scheduler/MainThreadScheduler/<PeriodicAction>c__Iterator1A::Dispose()
extern "C"  void U3CPeriodicActionU3Ec__Iterator1A_Dispose_m2089393415 (U3CPeriodicActionU3Ec__Iterator1A_t3918433488 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Scheduler/MainThreadScheduler/<PeriodicAction>c__Iterator1A::Reset()
extern "C"  void U3CPeriodicActionU3Ec__Iterator1A_Reset_m1773848055 (U3CPeriodicActionU3Ec__Iterator1A_t3918433488 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
