﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.WebPermissionInfo
struct WebPermissionInfo_t538092251;
// System.String
struct String_t;
// System.Text.RegularExpressions.Regex
struct Regex_t3802381858;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_WebPermissionInfoType1047627189.h"
#include "mscorlib_System_String968488902.h"
#include "System_System_Text_RegularExpressions_Regex3802381858.h"

// System.Void System.Net.WebPermissionInfo::.ctor(System.Net.WebPermissionInfoType,System.String)
extern "C"  void WebPermissionInfo__ctor_m1018762044 (WebPermissionInfo_t538092251 * __this, int32_t ___type0, String_t* ___info1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebPermissionInfo::.ctor(System.Text.RegularExpressions.Regex)
extern "C"  void WebPermissionInfo__ctor_m3585943617 (WebPermissionInfo_t538092251 * __this, Regex_t3802381858 * ___regex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.WebPermissionInfo::get_Info()
extern "C"  String_t* WebPermissionInfo_get_Info_m68442659 (WebPermissionInfo_t538092251 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
