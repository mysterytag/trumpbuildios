﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnePF.OpenIAB
struct OpenIAB_t3048630868;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// System.String
struct String_t;
// OnePF.Options
struct Options_t3062372690;
// System.String[]
struct StringU5BU5D_t2956870243;
// OnePF.Purchase
struct Purchase_t160195573;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_Options3062372690.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_Purchase160195573.h"

// System.Void OnePF.OpenIAB::.ctor()
extern "C"  void OpenIAB__ctor_m575505577 (OpenIAB_t3048630868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.OpenIAB::.cctor()
extern "C"  void OpenIAB__cctor_m178707492 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject OnePF.OpenIAB::get_EventManager()
extern "C"  GameObject_t4012695102 * OpenIAB_get_EventManager_m4227360122 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.OpenIAB::mapSku(System.String,System.String,System.String)
extern "C"  void OpenIAB_mapSku_m368492606 (Il2CppObject * __this /* static, unused */, String_t* ___sku0, String_t* ___storeName1, String_t* ___storeSku2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.OpenIAB::init(OnePF.Options)
extern "C"  void OpenIAB_init_m2625521499 (Il2CppObject * __this /* static, unused */, Options_t3062372690 * ___options0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.OpenIAB::unbindService()
extern "C"  void OpenIAB_unbindService_m4148534182 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OnePF.OpenIAB::areSubscriptionsSupported()
extern "C"  bool OpenIAB_areSubscriptionsSupported_m953657423 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.OpenIAB::queryInventory()
extern "C"  void OpenIAB_queryInventory_m1258082127 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.OpenIAB::queryInventory(System.String[])
extern "C"  void OpenIAB_queryInventory_m811955825 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t2956870243* ___skus0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.OpenIAB::purchaseProduct(System.String,System.String)
extern "C"  void OpenIAB_purchaseProduct_m501900041 (Il2CppObject * __this /* static, unused */, String_t* ___sku0, String_t* ___developerPayload1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.OpenIAB::purchaseSubscription(System.String,System.String)
extern "C"  void OpenIAB_purchaseSubscription_m756822917 (Il2CppObject * __this /* static, unused */, String_t* ___sku0, String_t* ___developerPayload1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.OpenIAB::consumeProduct(OnePF.Purchase)
extern "C"  void OpenIAB_consumeProduct_m2513437947 (Il2CppObject * __this /* static, unused */, Purchase_t160195573 * ___purchase0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.OpenIAB::restoreTransactions()
extern "C"  void OpenIAB_restoreTransactions_m2405457322 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OnePF.OpenIAB::isDebugLog()
extern "C"  bool OpenIAB_isDebugLog_m2994064698 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.OpenIAB::enableDebugLogging(System.Boolean)
extern "C"  void OpenIAB_enableDebugLogging_m3667255489 (Il2CppObject * __this /* static, unused */, bool ___enabled0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnePF.OpenIAB::enableDebugLogging(System.Boolean,System.String)
extern "C"  void OpenIAB_enableDebugLogging_m4256310077 (Il2CppObject * __this /* static, unused */, bool ___enabled0, String_t* ___tag1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
