﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.CastObservable`2<System.Object,System.Object>
struct CastObservable_2_t4040585978;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.CastObservable`2<System.Object,System.Object>::.ctor(UniRx.IObservable`1<TSource>)
extern "C"  void CastObservable_2__ctor_m3763410834_gshared (CastObservable_2_t4040585978 * __this, Il2CppObject* ___source0, const MethodInfo* method);
#define CastObservable_2__ctor_m3763410834(__this, ___source0, method) ((  void (*) (CastObservable_2_t4040585978 *, Il2CppObject*, const MethodInfo*))CastObservable_2__ctor_m3763410834_gshared)(__this, ___source0, method)
// System.IDisposable UniRx.Operators.CastObservable`2<System.Object,System.Object>::SubscribeCore(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  Il2CppObject * CastObservable_2_SubscribeCore_m3071551731_gshared (CastObservable_2_t4040585978 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define CastObservable_2_SubscribeCore_m3071551731(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (CastObservable_2_t4040585978 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))CastObservable_2_SubscribeCore_m3071551731_gshared)(__this, ___observer0, ___cancel1, method)
