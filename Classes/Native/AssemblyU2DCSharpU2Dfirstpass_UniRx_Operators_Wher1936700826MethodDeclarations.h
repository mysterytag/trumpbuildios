﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.WhereObservable`1/<CombinePredicate>c__AnonStorey72<System.Boolean>
struct U3CCombinePredicateU3Ec__AnonStorey72_t1936700826;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.WhereObservable`1/<CombinePredicate>c__AnonStorey72<System.Boolean>::.ctor()
extern "C"  void U3CCombinePredicateU3Ec__AnonStorey72__ctor_m579441617_gshared (U3CCombinePredicateU3Ec__AnonStorey72_t1936700826 * __this, const MethodInfo* method);
#define U3CCombinePredicateU3Ec__AnonStorey72__ctor_m579441617(__this, method) ((  void (*) (U3CCombinePredicateU3Ec__AnonStorey72_t1936700826 *, const MethodInfo*))U3CCombinePredicateU3Ec__AnonStorey72__ctor_m579441617_gshared)(__this, method)
// System.Boolean UniRx.Operators.WhereObservable`1/<CombinePredicate>c__AnonStorey72<System.Boolean>::<>m__8F(T)
extern "C"  bool U3CCombinePredicateU3Ec__AnonStorey72_U3CU3Em__8F_m467298138_gshared (U3CCombinePredicateU3Ec__AnonStorey72_t1936700826 * __this, bool ___x0, const MethodInfo* method);
#define U3CCombinePredicateU3Ec__AnonStorey72_U3CU3Em__8F_m467298138(__this, ___x0, method) ((  bool (*) (U3CCombinePredicateU3Ec__AnonStorey72_t1936700826 *, bool, const MethodInfo*))U3CCombinePredicateU3Ec__AnonStorey72_U3CU3Em__8F_m467298138_gshared)(__this, ___x0, method)
