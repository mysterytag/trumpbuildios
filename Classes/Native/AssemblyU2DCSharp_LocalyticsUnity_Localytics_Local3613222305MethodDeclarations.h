﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LocalyticsUnity.Localytics/LocalyticsDidDisplayInAppMessage
struct LocalyticsDidDisplayInAppMessage_t3613222305;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void LocalyticsUnity.Localytics/LocalyticsDidDisplayInAppMessage::.ctor(System.Object,System.IntPtr)
extern "C"  void LocalyticsDidDisplayInAppMessage__ctor_m896525928 (LocalyticsDidDisplayInAppMessage_t3613222305 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics/LocalyticsDidDisplayInAppMessage::Invoke()
extern "C"  void LocalyticsDidDisplayInAppMessage_Invoke_m4043818178 (LocalyticsDidDisplayInAppMessage_t3613222305 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_LocalyticsDidDisplayInAppMessage_t3613222305(Il2CppObject* delegate);
// System.IAsyncResult LocalyticsUnity.Localytics/LocalyticsDidDisplayInAppMessage::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LocalyticsDidDisplayInAppMessage_BeginInvoke_m3805002377 (LocalyticsDidDisplayInAppMessage_t3613222305 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics/LocalyticsDidDisplayInAppMessage::EndInvoke(System.IAsyncResult)
extern "C"  void LocalyticsDidDisplayInAppMessage_EndInvoke_m3578186872 (LocalyticsDidDisplayInAppMessage_t3613222305 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
