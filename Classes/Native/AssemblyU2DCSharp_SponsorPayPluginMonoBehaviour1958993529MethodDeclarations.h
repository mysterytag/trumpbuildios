﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SponsorPayPluginMonoBehaviour
struct SponsorPayPluginMonoBehaviour_t1958993529;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void SponsorPayPluginMonoBehaviour::.ctor()
extern "C"  void SponsorPayPluginMonoBehaviour__ctor_m862448402 (SponsorPayPluginMonoBehaviour_t1958993529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPayPluginMonoBehaviour::Awake()
extern "C"  void SponsorPayPluginMonoBehaviour_Awake_m1100053621 (SponsorPayPluginMonoBehaviour_t1958993529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPayPluginMonoBehaviour::OnSPOfferWallResultFromSDK(System.String)
extern "C"  void SponsorPayPluginMonoBehaviour_OnSPOfferWallResultFromSDK_m2751369127 (SponsorPayPluginMonoBehaviour_t1958993529 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPayPluginMonoBehaviour::OnSPCurrencyDeltaOfCoinsMessageFromSDK(System.String)
extern "C"  void SponsorPayPluginMonoBehaviour_OnSPCurrencyDeltaOfCoinsMessageFromSDK_m2111010887 (SponsorPayPluginMonoBehaviour_t1958993529 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPayPluginMonoBehaviour::OnSPBrandEngageStatusMessageFromSDK(System.String)
extern "C"  void SponsorPayPluginMonoBehaviour_OnSPBrandEngageStatusMessageFromSDK_m286172587 (SponsorPayPluginMonoBehaviour_t1958993529 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPayPluginMonoBehaviour::OnSPBrandEngageResultFromSDK(System.String)
extern "C"  void SponsorPayPluginMonoBehaviour_OnSPBrandEngageResultFromSDK_m3237793641 (SponsorPayPluginMonoBehaviour_t1958993529 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPayPluginMonoBehaviour::OnSPInterstitialMessageFromSDK(System.String)
extern "C"  void SponsorPayPluginMonoBehaviour_OnSPInterstitialMessageFromSDK_m361365855 (SponsorPayPluginMonoBehaviour_t1958993529 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPayPluginMonoBehaviour::OnSPInterstitialResultFromSDK(System.String)
extern "C"  void SponsorPayPluginMonoBehaviour_OnSPInterstitialResultFromSDK_m2192132615 (SponsorPayPluginMonoBehaviour_t1958993529 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPayPluginMonoBehaviour::OnExceptionFromSDK(System.String)
extern "C"  void SponsorPayPluginMonoBehaviour_OnExceptionFromSDK_m2777768240 (SponsorPayPluginMonoBehaviour_t1958993529 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
