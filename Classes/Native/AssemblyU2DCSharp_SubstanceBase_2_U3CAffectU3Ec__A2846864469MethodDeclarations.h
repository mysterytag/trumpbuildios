﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SubstanceBase`2/<Affect>c__AnonStorey143<System.Single,System.Single>
struct U3CAffectU3Ec__AnonStorey143_t2846864469;

#include "codegen/il2cpp-codegen.h"

// System.Void SubstanceBase`2/<Affect>c__AnonStorey143<System.Single,System.Single>::.ctor()
extern "C"  void U3CAffectU3Ec__AnonStorey143__ctor_m2650718533_gshared (U3CAffectU3Ec__AnonStorey143_t2846864469 * __this, const MethodInfo* method);
#define U3CAffectU3Ec__AnonStorey143__ctor_m2650718533(__this, method) ((  void (*) (U3CAffectU3Ec__AnonStorey143_t2846864469 *, const MethodInfo*))U3CAffectU3Ec__AnonStorey143__ctor_m2650718533_gshared)(__this, method)
// System.Void SubstanceBase`2/<Affect>c__AnonStorey143<System.Single,System.Single>::<>m__1D9(InfT)
extern "C"  void U3CAffectU3Ec__AnonStorey143_U3CU3Em__1D9_m2168660199_gshared (U3CAffectU3Ec__AnonStorey143_t2846864469 * __this, float ___val0, const MethodInfo* method);
#define U3CAffectU3Ec__AnonStorey143_U3CU3Em__1D9_m2168660199(__this, ___val0, method) ((  void (*) (U3CAffectU3Ec__AnonStorey143_t2846864469 *, float, const MethodInfo*))U3CAffectU3Ec__AnonStorey143_U3CU3Em__1D9_m2168660199_gshared)(__this, ___val0, method)
