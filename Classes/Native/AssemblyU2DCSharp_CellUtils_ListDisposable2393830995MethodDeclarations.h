﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellUtils.ListDisposable
struct ListDisposable_t2393830995;
// System.IDisposable[]
struct IDisposableU5BU5D_t165183403;
// System.Collections.Generic.IEnumerable`1<System.IDisposable>
struct IEnumerable_1_t206108434;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Collections.Generic.IEnumerator`1<System.IDisposable>
struct IEnumerator_1_t3112027822;

#include "codegen/il2cpp-codegen.h"

// System.Void CellUtils.ListDisposable::.ctor()
extern "C"  void ListDisposable__ctor_m4289441534 (ListDisposable_t2393830995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CellUtils.ListDisposable::.ctor(System.Int32)
extern "C"  void ListDisposable__ctor_m2720323407 (ListDisposable_t2393830995 * __this, int32_t ___capacity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CellUtils.ListDisposable::.ctor(System.IDisposable[])
extern "C"  void ListDisposable__ctor_m1388779954 (ListDisposable_t2393830995 * __this, IDisposableU5BU5D_t165183403* ___disposables0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CellUtils.ListDisposable::.ctor(System.Collections.Generic.IEnumerable`1<System.IDisposable>)
extern "C"  void ListDisposable__ctor_m4214592275 (ListDisposable_t2393830995 * __this, Il2CppObject* ___disposables0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CellUtils.ListDisposable::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ListDisposable_System_Collections_IEnumerable_GetEnumerator_m1132931123 (ListDisposable_t2393830995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CellUtils.ListDisposable::SetArray(System.Collections.Generic.IEnumerable`1<System.IDisposable>)
extern "C"  void ListDisposable_SetArray_m1080170738 (ListDisposable_t2393830995 * __this, Il2CppObject* ___disposables0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CellUtils.ListDisposable::get_Count()
extern "C"  int32_t ListDisposable_get_Count_m3993007796 (ListDisposable_t2393830995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CellUtils.ListDisposable::Add(System.IDisposable)
extern "C"  void ListDisposable_Add_m3414112787 (ListDisposable_t2393830995 * __this, Il2CppObject * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CellUtils.ListDisposable::Remove(System.IDisposable)
extern "C"  bool ListDisposable_Remove_m495409940 (ListDisposable_t2393830995 * __this, Il2CppObject * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CellUtils.ListDisposable::Dispose()
extern "C"  void ListDisposable_Dispose_m3177960379 (ListDisposable_t2393830995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CellUtils.ListDisposable::Clear()
extern "C"  void ListDisposable_Clear_m1695574825 (ListDisposable_t2393830995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CellUtils.ListDisposable::Contains(System.IDisposable)
extern "C"  bool ListDisposable_Contains_m2266133231 (ListDisposable_t2393830995 * __this, Il2CppObject * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CellUtils.ListDisposable::CopyTo(System.IDisposable[],System.Int32)
extern "C"  void ListDisposable_CopyTo_m2068262989 (ListDisposable_t2393830995 * __this, IDisposableU5BU5D_t165183403* ___array0, int32_t ___arrayIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CellUtils.ListDisposable::get_IsReadOnly()
extern "C"  bool ListDisposable_get_IsReadOnly_m3162650351 (ListDisposable_t2393830995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.IDisposable> CellUtils.ListDisposable::GetEnumerator()
extern "C"  Il2CppObject* ListDisposable_GetEnumerator_m1231826112 (ListDisposable_t2393830995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CellUtils.ListDisposable::get_IsDisposed()
extern "C"  bool ListDisposable_get_IsDisposed_m2970467826 (ListDisposable_t2393830995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
