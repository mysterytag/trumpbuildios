﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TableButtons/<AddOpenIabButton>c__AnonStorey15C/<AddOpenIabButton>c__AnonStorey15D
struct U3CAddOpenIabButtonU3Ec__AnonStorey15D_t3212335978;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "mscorlib_System_String968488902.h"

// System.Void TableButtons/<AddOpenIabButton>c__AnonStorey15C/<AddOpenIabButton>c__AnonStorey15D::.ctor()
extern "C"  void U3CAddOpenIabButtonU3Ec__AnonStorey15D__ctor_m2126170215 (U3CAddOpenIabButtonU3Ec__AnonStorey15D_t3212335978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TableButtons/<AddOpenIabButton>c__AnonStorey15C/<AddOpenIabButton>c__AnonStorey15D::<>m__258(UniRx.Unit)
extern "C"  bool U3CAddOpenIabButtonU3Ec__AnonStorey15D_U3CU3Em__258_m2550843103 (U3CAddOpenIabButtonU3Ec__AnonStorey15D_t3212335978 * __this, Unit_t2558286038  ___unit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<AddOpenIabButton>c__AnonStorey15C/<AddOpenIabButton>c__AnonStorey15D::<>m__259(UniRx.Unit)
extern "C"  void U3CAddOpenIabButtonU3Ec__AnonStorey15D_U3CU3Em__259_m3002286764 (U3CAddOpenIabButtonU3Ec__AnonStorey15D_t3212335978 * __this, Unit_t2558286038  ____0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<AddOpenIabButton>c__AnonStorey15C/<AddOpenIabButton>c__AnonStorey15D::<>m__25A(System.String)
extern "C"  void U3CAddOpenIabButtonU3Ec__AnonStorey15D_U3CU3Em__25A_m1154890674 (U3CAddOpenIabButtonU3Ec__AnonStorey15D_t3212335978 * __this, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
