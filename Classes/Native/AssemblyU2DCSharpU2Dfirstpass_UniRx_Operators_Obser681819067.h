﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.SingleAssignmentDisposable
struct SingleAssignmentDisposable_t2336378823;
// System.Collections.Generic.LinkedListNode`1<System.IDisposable>
struct LinkedListNode_1_t1330911994;
// System.Exception
struct Exception_t1967233988;
// UniRx.Operators.ObserveOnObservable`1/ObserveOn<System.Object>
struct ObserveOn_t3253138108;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.ObserveOnObservable`1/ObserveOn/<OnError>c__AnonStorey62<System.Object>
struct  U3COnErrorU3Ec__AnonStorey62_t681819067  : public Il2CppObject
{
public:
	// UniRx.SingleAssignmentDisposable UniRx.Operators.ObserveOnObservable`1/ObserveOn/<OnError>c__AnonStorey62::self
	SingleAssignmentDisposable_t2336378823 * ___self_0;
	// System.Collections.Generic.LinkedListNode`1<System.IDisposable> UniRx.Operators.ObserveOnObservable`1/ObserveOn/<OnError>c__AnonStorey62::node
	LinkedListNode_1_t1330911994 * ___node_1;
	// System.Exception UniRx.Operators.ObserveOnObservable`1/ObserveOn/<OnError>c__AnonStorey62::error
	Exception_t1967233988 * ___error_2;
	// UniRx.Operators.ObserveOnObservable`1/ObserveOn<T> UniRx.Operators.ObserveOnObservable`1/ObserveOn/<OnError>c__AnonStorey62::<>f__this
	ObserveOn_t3253138108 * ___U3CU3Ef__this_3;

public:
	inline static int32_t get_offset_of_self_0() { return static_cast<int32_t>(offsetof(U3COnErrorU3Ec__AnonStorey62_t681819067, ___self_0)); }
	inline SingleAssignmentDisposable_t2336378823 * get_self_0() const { return ___self_0; }
	inline SingleAssignmentDisposable_t2336378823 ** get_address_of_self_0() { return &___self_0; }
	inline void set_self_0(SingleAssignmentDisposable_t2336378823 * value)
	{
		___self_0 = value;
		Il2CppCodeGenWriteBarrier(&___self_0, value);
	}

	inline static int32_t get_offset_of_node_1() { return static_cast<int32_t>(offsetof(U3COnErrorU3Ec__AnonStorey62_t681819067, ___node_1)); }
	inline LinkedListNode_1_t1330911994 * get_node_1() const { return ___node_1; }
	inline LinkedListNode_1_t1330911994 ** get_address_of_node_1() { return &___node_1; }
	inline void set_node_1(LinkedListNode_1_t1330911994 * value)
	{
		___node_1 = value;
		Il2CppCodeGenWriteBarrier(&___node_1, value);
	}

	inline static int32_t get_offset_of_error_2() { return static_cast<int32_t>(offsetof(U3COnErrorU3Ec__AnonStorey62_t681819067, ___error_2)); }
	inline Exception_t1967233988 * get_error_2() const { return ___error_2; }
	inline Exception_t1967233988 ** get_address_of_error_2() { return &___error_2; }
	inline void set_error_2(Exception_t1967233988 * value)
	{
		___error_2 = value;
		Il2CppCodeGenWriteBarrier(&___error_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_3() { return static_cast<int32_t>(offsetof(U3COnErrorU3Ec__AnonStorey62_t681819067, ___U3CU3Ef__this_3)); }
	inline ObserveOn_t3253138108 * get_U3CU3Ef__this_3() const { return ___U3CU3Ef__this_3; }
	inline ObserveOn_t3253138108 ** get_address_of_U3CU3Ef__this_3() { return &___U3CU3Ef__this_3; }
	inline void set_U3CU3Ef__this_3(ObserveOn_t3253138108 * value)
	{
		___U3CU3Ef__this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
