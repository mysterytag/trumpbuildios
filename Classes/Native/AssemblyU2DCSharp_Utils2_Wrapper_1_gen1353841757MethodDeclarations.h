﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Utils2/Wrapper`1<System.Object>
struct Wrapper_1_t1353841757;

#include "codegen/il2cpp-codegen.h"

// System.Void Utils2/Wrapper`1<System.Object>::.ctor()
extern "C"  void Wrapper_1__ctor_m3548568133_gshared (Wrapper_1_t1353841757 * __this, const MethodInfo* method);
#define Wrapper_1__ctor_m3548568133(__this, method) ((  void (*) (Wrapper_1_t1353841757 *, const MethodInfo*))Wrapper_1__ctor_m3548568133_gshared)(__this, method)
