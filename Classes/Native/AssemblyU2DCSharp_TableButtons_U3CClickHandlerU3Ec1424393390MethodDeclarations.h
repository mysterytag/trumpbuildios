﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TableButtons/<ClickHandler>c__AnonStorey159
struct U3CClickHandlerU3Ec__AnonStorey159_t1424393390;

#include "codegen/il2cpp-codegen.h"

// System.Void TableButtons/<ClickHandler>c__AnonStorey159::.ctor()
extern "C"  void U3CClickHandlerU3Ec__AnonStorey159__ctor_m4120899681 (U3CClickHandlerU3Ec__AnonStorey159_t1424393390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TableButtons/<ClickHandler>c__AnonStorey159::<>m__22C(System.Int32)
extern "C"  bool U3CClickHandlerU3Ec__AnonStorey159_U3CU3Em__22C_m2971363096 (U3CClickHandlerU3Ec__AnonStorey159_t1424393390 * __this, int32_t ___pack0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
