﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AchievControlIOS
struct AchievControlIOS_t3809172804;

#include "codegen/il2cpp-codegen.h"

// System.Void AchievControlIOS::.ctor()
extern "C"  void AchievControlIOS__ctor_m3544479191 (AchievControlIOS_t3809172804 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AchievControlIOS::Start()
extern "C"  void AchievControlIOS_Start_m2491616983 (AchievControlIOS_t3809172804 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AchievControlIOS::onProcessAuthent(System.Boolean)
extern "C"  void AchievControlIOS_onProcessAuthent_m3056705463 (AchievControlIOS_t3809172804 * __this, bool ___success0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AchievControlIOS::OnLider()
extern "C"  void AchievControlIOS_OnLider_m1486801802 (AchievControlIOS_t3809172804 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AchievControlIOS::OnAchiev()
extern "C"  void AchievControlIOS_OnAchiev_m237981984 (AchievControlIOS_t3809172804 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AchievControlIOS::Update()
extern "C"  void AchievControlIOS_Update_m4231534614 (AchievControlIOS_t3809172804 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
