﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;
// UniRx.IObservable`1<UniRx.Unit>[]
struct IObservable_1U5BU5D_t3461854471;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/<CombineSources>c__IteratorD`1<UniRx.Unit>
struct  U3CCombineSourcesU3Ec__IteratorD_1_t4292290176  : public Il2CppObject
{
public:
	// UniRx.IObservable`1<T> UniRx.Observable/<CombineSources>c__IteratorD`1::first
	Il2CppObject* ___first_0;
	// System.Int32 UniRx.Observable/<CombineSources>c__IteratorD`1::<i>__0
	int32_t ___U3CiU3E__0_1;
	// UniRx.IObservable`1<T>[] UniRx.Observable/<CombineSources>c__IteratorD`1::seconds
	IObservable_1U5BU5D_t3461854471* ___seconds_2;
	// System.Int32 UniRx.Observable/<CombineSources>c__IteratorD`1::$PC
	int32_t ___U24PC_3;
	// UniRx.IObservable`1<T> UniRx.Observable/<CombineSources>c__IteratorD`1::$current
	Il2CppObject* ___U24current_4;
	// UniRx.IObservable`1<T> UniRx.Observable/<CombineSources>c__IteratorD`1::<$>first
	Il2CppObject* ___U3CU24U3Efirst_5;
	// UniRx.IObservable`1<T>[] UniRx.Observable/<CombineSources>c__IteratorD`1::<$>seconds
	IObservable_1U5BU5D_t3461854471* ___U3CU24U3Eseconds_6;

public:
	inline static int32_t get_offset_of_first_0() { return static_cast<int32_t>(offsetof(U3CCombineSourcesU3Ec__IteratorD_1_t4292290176, ___first_0)); }
	inline Il2CppObject* get_first_0() const { return ___first_0; }
	inline Il2CppObject** get_address_of_first_0() { return &___first_0; }
	inline void set_first_0(Il2CppObject* value)
	{
		___first_0 = value;
		Il2CppCodeGenWriteBarrier(&___first_0, value);
	}

	inline static int32_t get_offset_of_U3CiU3E__0_1() { return static_cast<int32_t>(offsetof(U3CCombineSourcesU3Ec__IteratorD_1_t4292290176, ___U3CiU3E__0_1)); }
	inline int32_t get_U3CiU3E__0_1() const { return ___U3CiU3E__0_1; }
	inline int32_t* get_address_of_U3CiU3E__0_1() { return &___U3CiU3E__0_1; }
	inline void set_U3CiU3E__0_1(int32_t value)
	{
		___U3CiU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_seconds_2() { return static_cast<int32_t>(offsetof(U3CCombineSourcesU3Ec__IteratorD_1_t4292290176, ___seconds_2)); }
	inline IObservable_1U5BU5D_t3461854471* get_seconds_2() const { return ___seconds_2; }
	inline IObservable_1U5BU5D_t3461854471** get_address_of_seconds_2() { return &___seconds_2; }
	inline void set_seconds_2(IObservable_1U5BU5D_t3461854471* value)
	{
		___seconds_2 = value;
		Il2CppCodeGenWriteBarrier(&___seconds_2, value);
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CCombineSourcesU3Ec__IteratorD_1_t4292290176, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CCombineSourcesU3Ec__IteratorD_1_t4292290176, ___U24current_4)); }
	inline Il2CppObject* get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject* value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Efirst_5() { return static_cast<int32_t>(offsetof(U3CCombineSourcesU3Ec__IteratorD_1_t4292290176, ___U3CU24U3Efirst_5)); }
	inline Il2CppObject* get_U3CU24U3Efirst_5() const { return ___U3CU24U3Efirst_5; }
	inline Il2CppObject** get_address_of_U3CU24U3Efirst_5() { return &___U3CU24U3Efirst_5; }
	inline void set_U3CU24U3Efirst_5(Il2CppObject* value)
	{
		___U3CU24U3Efirst_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Efirst_5, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eseconds_6() { return static_cast<int32_t>(offsetof(U3CCombineSourcesU3Ec__IteratorD_1_t4292290176, ___U3CU24U3Eseconds_6)); }
	inline IObservable_1U5BU5D_t3461854471* get_U3CU24U3Eseconds_6() const { return ___U3CU24U3Eseconds_6; }
	inline IObservable_1U5BU5D_t3461854471** get_address_of_U3CU24U3Eseconds_6() { return &___U3CU24U3Eseconds_6; }
	inline void set_U3CU24U3Eseconds_6(IObservable_1U5BU5D_t3461854471* value)
	{
		___U3CU24U3Eseconds_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eseconds_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
