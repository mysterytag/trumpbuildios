﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen4294542862.h"
#include "AssemblyU2DCSharp_SponsorPay_SPUserEducation1408504954.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Nullable`1<SponsorPay.SPUserEducation>::.ctor(T)
extern "C"  void Nullable_1__ctor_m1980471517_gshared (Nullable_1_t4294542862 * __this, int32_t ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m1980471517(__this, ___value0, method) ((  void (*) (Nullable_1_t4294542862 *, int32_t, const MethodInfo*))Nullable_1__ctor_m1980471517_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<SponsorPay.SPUserEducation>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m12063873_gshared (Nullable_1_t4294542862 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m12063873(__this, method) ((  bool (*) (Nullable_1_t4294542862 *, const MethodInfo*))Nullable_1_get_HasValue_m12063873_gshared)(__this, method)
// T System.Nullable`1<SponsorPay.SPUserEducation>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m2234930694_gshared (Nullable_1_t4294542862 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m2234930694(__this, method) ((  int32_t (*) (Nullable_1_t4294542862 *, const MethodInfo*))Nullable_1_get_Value_m2234930694_gshared)(__this, method)
// System.Boolean System.Nullable`1<SponsorPay.SPUserEducation>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3975908052_gshared (Nullable_1_t4294542862 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m3975908052(__this, ___other0, method) ((  bool (*) (Nullable_1_t4294542862 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m3975908052_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<SponsorPay.SPUserEducation>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1322250731_gshared (Nullable_1_t4294542862 * __this, Nullable_1_t4294542862  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m1322250731(__this, ___other0, method) ((  bool (*) (Nullable_1_t4294542862 *, Nullable_1_t4294542862 , const MethodInfo*))Nullable_1_Equals_m1322250731_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<SponsorPay.SPUserEducation>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1359729324_gshared (Nullable_1_t4294542862 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m1359729324(__this, method) ((  int32_t (*) (Nullable_1_t4294542862 *, const MethodInfo*))Nullable_1_GetHashCode_m1359729324_gshared)(__this, method)
// T System.Nullable`1<SponsorPay.SPUserEducation>::GetValueOrDefault()
extern "C"  int32_t Nullable_1_GetValueOrDefault_m1806370913_gshared (Nullable_1_t4294542862 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m1806370913(__this, method) ((  int32_t (*) (Nullable_1_t4294542862 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m1806370913_gshared)(__this, method)
// System.String System.Nullable`1<SponsorPay.SPUserEducation>::ToString()
extern "C"  String_t* Nullable_1_ToString_m3206598764_gshared (Nullable_1_t4294542862 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m3206598764(__this, method) ((  String_t* (*) (Nullable_1_t4294542862 *, const MethodInfo*))Nullable_1_ToString_m3206598764_gshared)(__this, method)
// System.Nullable`1<T> System.Nullable`1<SponsorPay.SPUserEducation>::op_Implicit(T)
extern "C"  Nullable_1_t4294542862  Nullable_1_op_Implicit_m1189824294_gshared (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method);
#define Nullable_1_op_Implicit_m1189824294(__this /* static, unused */, ___value0, method) ((  Nullable_1_t4294542862  (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))Nullable_1_op_Implicit_m1189824294_gshared)(__this /* static, unused */, ___value0, method)
