﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3591453091MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1313905765(__this, ___dictionary0, method) ((  void (*) (Enumerator_t3921275146 *, Dictionary_2_t4154247205 *, const MethodInfo*))Enumerator__ctor_m3920831137_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1195788636(__this, method) ((  Il2CppObject * (*) (Enumerator_t3921275146 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3262087712_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m854482736(__this, method) ((  void (*) (Enumerator_t3921275146 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2959141748_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2942089017(__this, method) ((  DictionaryEntry_t130027246  (*) (Enumerator_t3921275146 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2279524093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2985824568(__this, method) ((  Il2CppObject * (*) (Enumerator_t3921275146 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1201448700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1387692490(__this, method) ((  Il2CppObject * (*) (Enumerator_t3921275146 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m294434446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>>::MoveNext()
#define Enumerator_MoveNext_m1211992348(__this, method) ((  bool (*) (Enumerator_t3921275146 *, const MethodInfo*))Enumerator_MoveNext_m217327200_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>>::get_Current()
#define Enumerator_get_Current_m2619517588(__this, method) ((  KeyValuePair_2_t3642778503  (*) (Enumerator_t3921275146 *, const MethodInfo*))Enumerator_get_Current_m4240003024_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m3838534377(__this, method) ((  String_t* (*) (Enumerator_t3921275146 *, const MethodInfo*))Enumerator_get_CurrentKey_m3062159917_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m80079949(__this, method) ((  LinkedList_1_t2516549301 * (*) (Enumerator_t3921275146 *, const MethodInfo*))Enumerator_get_CurrentValue_m592783249_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>>::Reset()
#define Enumerator_Reset_m1776709431(__this, method) ((  void (*) (Enumerator_t3921275146 *, const MethodInfo*))Enumerator_Reset_m3001375603_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>>::VerifyState()
#define Enumerator_VerifyState_m1218525696(__this, method) ((  void (*) (Enumerator_t3921275146 *, const MethodInfo*))Enumerator_VerifyState_m4290054460_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1221993832(__this, method) ((  void (*) (Enumerator_t3921275146 *, const MethodInfo*))Enumerator_VerifyCurrent_m2318603684_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>>::Dispose()
#define Enumerator_Dispose_m544208455(__this, method) ((  void (*) (Enumerator_t3921275146 *, const MethodInfo*))Enumerator_Dispose_m627360643_gshared)(__this, method)
