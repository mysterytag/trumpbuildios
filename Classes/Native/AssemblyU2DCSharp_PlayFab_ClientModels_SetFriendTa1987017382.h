﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.SetFriendTagsRequest
struct  SetFriendTagsRequest_t1987017382  : public Il2CppObject
{
public:
	// System.String PlayFab.ClientModels.SetFriendTagsRequest::<FriendPlayFabId>k__BackingField
	String_t* ___U3CFriendPlayFabIdU3Ek__BackingField_0;
	// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.SetFriendTagsRequest::<Tags>k__BackingField
	List_1_t1765447871 * ___U3CTagsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CFriendPlayFabIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SetFriendTagsRequest_t1987017382, ___U3CFriendPlayFabIdU3Ek__BackingField_0)); }
	inline String_t* get_U3CFriendPlayFabIdU3Ek__BackingField_0() const { return ___U3CFriendPlayFabIdU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CFriendPlayFabIdU3Ek__BackingField_0() { return &___U3CFriendPlayFabIdU3Ek__BackingField_0; }
	inline void set_U3CFriendPlayFabIdU3Ek__BackingField_0(String_t* value)
	{
		___U3CFriendPlayFabIdU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CFriendPlayFabIdU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CTagsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SetFriendTagsRequest_t1987017382, ___U3CTagsU3Ek__BackingField_1)); }
	inline List_1_t1765447871 * get_U3CTagsU3Ek__BackingField_1() const { return ___U3CTagsU3Ek__BackingField_1; }
	inline List_1_t1765447871 ** get_address_of_U3CTagsU3Ek__BackingField_1() { return &___U3CTagsU3Ek__BackingField_1; }
	inline void set_U3CTagsU3Ek__BackingField_1(List_1_t1765447871 * value)
	{
		___U3CTagsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTagsU3Ek__BackingField_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
