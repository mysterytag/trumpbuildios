﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DG.Tweening.Core.DOGetter`1<System.Double>
struct DOGetter_1_t3523644891;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void DG.Tweening.Core.DOGetter`1<System.Double>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m2027841653_gshared (DOGetter_1_t3523644891 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define DOGetter_1__ctor_m2027841653(__this, ___object0, ___method1, method) ((  void (*) (DOGetter_1_t3523644891 *, Il2CppObject *, IntPtr_t, const MethodInfo*))DOGetter_1__ctor_m2027841653_gshared)(__this, ___object0, ___method1, method)
// T DG.Tweening.Core.DOGetter`1<System.Double>::Invoke()
extern "C"  double DOGetter_1_Invoke_m3739392142_gshared (DOGetter_1_t3523644891 * __this, const MethodInfo* method);
#define DOGetter_1_Invoke_m3739392142(__this, method) ((  double (*) (DOGetter_1_t3523644891 *, const MethodInfo*))DOGetter_1_Invoke_m3739392142_gshared)(__this, method)
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<System.Double>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m3661121556_gshared (DOGetter_1_t3523644891 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method);
#define DOGetter_1_BeginInvoke_m3661121556(__this, ___callback0, ___object1, method) ((  Il2CppObject * (*) (DOGetter_1_t3523644891 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))DOGetter_1_BeginInvoke_m3661121556_gshared)(__this, ___callback0, ___object1, method)
// T DG.Tweening.Core.DOGetter`1<System.Double>::EndInvoke(System.IAsyncResult)
extern "C"  double DOGetter_1_EndInvoke_m2222423876_gshared (DOGetter_1_t3523644891 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define DOGetter_1_EndInvoke_m2222423876(__this, ___result0, method) ((  double (*) (DOGetter_1_t3523644891 *, Il2CppObject *, const MethodInfo*))DOGetter_1_EndInvoke_m2222423876_gshared)(__this, ___result0, method)
