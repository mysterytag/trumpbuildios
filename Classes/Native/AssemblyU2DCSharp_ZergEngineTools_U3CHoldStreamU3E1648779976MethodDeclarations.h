﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZergEngineTools/<HoldStream>c__AnonStorey150
struct U3CHoldStreamU3Ec__AnonStorey150_t1648779976;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action
struct Action_t437523947;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"

// System.Void ZergEngineTools/<HoldStream>c__AnonStorey150::.ctor()
extern "C"  void U3CHoldStreamU3Ec__AnonStorey150__ctor_m2030460189 (U3CHoldStreamU3Ec__AnonStorey150_t1648779976 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable ZergEngineTools/<HoldStream>c__AnonStorey150::<>m__1FD(System.Action,Priority)
extern "C"  Il2CppObject * U3CHoldStreamU3Ec__AnonStorey150_U3CU3Em__1FD_m1965807395 (U3CHoldStreamU3Ec__AnonStorey150_t1648779976 * __this, Action_t437523947 * ___reaction0, int32_t ___p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
