﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ObserveExtensions/<PublishPocoValueChanged>c__Iterator24`2<System.Object,System.Double>
struct U3CPublishPocoValueChangedU3Ec__Iterator24_2_t2868476001;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.ObserveExtensions/<PublishPocoValueChanged>c__Iterator24`2<System.Object,System.Double>::.ctor()
extern "C"  void U3CPublishPocoValueChangedU3Ec__Iterator24_2__ctor_m4291736542_gshared (U3CPublishPocoValueChangedU3Ec__Iterator24_2_t2868476001 * __this, const MethodInfo* method);
#define U3CPublishPocoValueChangedU3Ec__Iterator24_2__ctor_m4291736542(__this, method) ((  void (*) (U3CPublishPocoValueChangedU3Ec__Iterator24_2_t2868476001 *, const MethodInfo*))U3CPublishPocoValueChangedU3Ec__Iterator24_2__ctor_m4291736542_gshared)(__this, method)
// System.Object UniRx.ObserveExtensions/<PublishPocoValueChanged>c__Iterator24`2<System.Object,System.Double>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPublishPocoValueChangedU3Ec__Iterator24_2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m944527348_gshared (U3CPublishPocoValueChangedU3Ec__Iterator24_2_t2868476001 * __this, const MethodInfo* method);
#define U3CPublishPocoValueChangedU3Ec__Iterator24_2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m944527348(__this, method) ((  Il2CppObject * (*) (U3CPublishPocoValueChangedU3Ec__Iterator24_2_t2868476001 *, const MethodInfo*))U3CPublishPocoValueChangedU3Ec__Iterator24_2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m944527348_gshared)(__this, method)
// System.Object UniRx.ObserveExtensions/<PublishPocoValueChanged>c__Iterator24`2<System.Object,System.Double>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPublishPocoValueChangedU3Ec__Iterator24_2_System_Collections_IEnumerator_get_Current_m3136603016_gshared (U3CPublishPocoValueChangedU3Ec__Iterator24_2_t2868476001 * __this, const MethodInfo* method);
#define U3CPublishPocoValueChangedU3Ec__Iterator24_2_System_Collections_IEnumerator_get_Current_m3136603016(__this, method) ((  Il2CppObject * (*) (U3CPublishPocoValueChangedU3Ec__Iterator24_2_t2868476001 *, const MethodInfo*))U3CPublishPocoValueChangedU3Ec__Iterator24_2_System_Collections_IEnumerator_get_Current_m3136603016_gshared)(__this, method)
// System.Boolean UniRx.ObserveExtensions/<PublishPocoValueChanged>c__Iterator24`2<System.Object,System.Double>::MoveNext()
extern "C"  bool U3CPublishPocoValueChangedU3Ec__Iterator24_2_MoveNext_m786312814_gshared (U3CPublishPocoValueChangedU3Ec__Iterator24_2_t2868476001 * __this, const MethodInfo* method);
#define U3CPublishPocoValueChangedU3Ec__Iterator24_2_MoveNext_m786312814(__this, method) ((  bool (*) (U3CPublishPocoValueChangedU3Ec__Iterator24_2_t2868476001 *, const MethodInfo*))U3CPublishPocoValueChangedU3Ec__Iterator24_2_MoveNext_m786312814_gshared)(__this, method)
// System.Void UniRx.ObserveExtensions/<PublishPocoValueChanged>c__Iterator24`2<System.Object,System.Double>::Dispose()
extern "C"  void U3CPublishPocoValueChangedU3Ec__Iterator24_2_Dispose_m1088495771_gshared (U3CPublishPocoValueChangedU3Ec__Iterator24_2_t2868476001 * __this, const MethodInfo* method);
#define U3CPublishPocoValueChangedU3Ec__Iterator24_2_Dispose_m1088495771(__this, method) ((  void (*) (U3CPublishPocoValueChangedU3Ec__Iterator24_2_t2868476001 *, const MethodInfo*))U3CPublishPocoValueChangedU3Ec__Iterator24_2_Dispose_m1088495771_gshared)(__this, method)
// System.Void UniRx.ObserveExtensions/<PublishPocoValueChanged>c__Iterator24`2<System.Object,System.Double>::Reset()
extern "C"  void U3CPublishPocoValueChangedU3Ec__Iterator24_2_Reset_m1938169483_gshared (U3CPublishPocoValueChangedU3Ec__Iterator24_2_t2868476001 * __this, const MethodInfo* method);
#define U3CPublishPocoValueChangedU3Ec__Iterator24_2_Reset_m1938169483(__this, method) ((  void (*) (U3CPublishPocoValueChangedU3Ec__Iterator24_2_t2868476001 *, const MethodInfo*))U3CPublishPocoValueChangedU3Ec__Iterator24_2_Reset_m1938169483_gshared)(__this, method)
// System.Void UniRx.ObserveExtensions/<PublishPocoValueChanged>c__Iterator24`2<System.Object,System.Double>::<>__Finally0()
extern "C"  void U3CPublishPocoValueChangedU3Ec__Iterator24_2_U3CU3E__Finally0_m663584981_gshared (U3CPublishPocoValueChangedU3Ec__Iterator24_2_t2868476001 * __this, const MethodInfo* method);
#define U3CPublishPocoValueChangedU3Ec__Iterator24_2_U3CU3E__Finally0_m663584981(__this, method) ((  void (*) (U3CPublishPocoValueChangedU3Ec__Iterator24_2_t2868476001 *, const MethodInfo*))U3CPublishPocoValueChangedU3Ec__Iterator24_2_U3CU3E__Finally0_m663584981_gshared)(__this, method)
