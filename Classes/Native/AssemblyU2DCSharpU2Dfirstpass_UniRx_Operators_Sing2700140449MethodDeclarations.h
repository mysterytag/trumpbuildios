﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SingleObservable`1/Single<System.Object>
struct Single_t2700140449;
// UniRx.Operators.SingleObservable`1<System.Object>
struct SingleObservable_1_t427280954;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SingleObservable`1/Single<System.Object>::.ctor(UniRx.Operators.SingleObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Single__ctor_m1558334988_gshared (Single_t2700140449 * __this, SingleObservable_1_t427280954 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Single__ctor_m1558334988(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Single_t2700140449 *, SingleObservable_1_t427280954 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Single__ctor_m1558334988_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.SingleObservable`1/Single<System.Object>::OnNext(T)
extern "C"  void Single_OnNext_m3043659679_gshared (Single_t2700140449 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Single_OnNext_m3043659679(__this, ___value0, method) ((  void (*) (Single_t2700140449 *, Il2CppObject *, const MethodInfo*))Single_OnNext_m3043659679_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SingleObservable`1/Single<System.Object>::OnError(System.Exception)
extern "C"  void Single_OnError_m2301725870_gshared (Single_t2700140449 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Single_OnError_m2301725870(__this, ___error0, method) ((  void (*) (Single_t2700140449 *, Exception_t1967233988 *, const MethodInfo*))Single_OnError_m2301725870_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SingleObservable`1/Single<System.Object>::OnCompleted()
extern "C"  void Single_OnCompleted_m760960257_gshared (Single_t2700140449 * __this, const MethodInfo* method);
#define Single_OnCompleted_m760960257(__this, method) ((  void (*) (Single_t2700140449 *, const MethodInfo*))Single_OnCompleted_m760960257_gshared)(__this, method)
