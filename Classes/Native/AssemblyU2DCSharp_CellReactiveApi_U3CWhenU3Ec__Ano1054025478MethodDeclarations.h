﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/<When>c__AnonStorey10A`1/<When>c__AnonStorey10B`1<System.Object>
struct U3CWhenU3Ec__AnonStorey10B_1_t1054025478;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void CellReactiveApi/<When>c__AnonStorey10A`1/<When>c__AnonStorey10B`1<System.Object>::.ctor()
extern "C"  void U3CWhenU3Ec__AnonStorey10B_1__ctor_m3102183327_gshared (U3CWhenU3Ec__AnonStorey10B_1_t1054025478 * __this, const MethodInfo* method);
#define U3CWhenU3Ec__AnonStorey10B_1__ctor_m3102183327(__this, method) ((  void (*) (U3CWhenU3Ec__AnonStorey10B_1_t1054025478 *, const MethodInfo*))U3CWhenU3Ec__AnonStorey10B_1__ctor_m3102183327_gshared)(__this, method)
// System.Void CellReactiveApi/<When>c__AnonStorey10A`1/<When>c__AnonStorey10B`1<System.Object>::<>m__195(T)
extern "C"  void U3CWhenU3Ec__AnonStorey10B_1_U3CU3Em__195_m1115334199_gshared (U3CWhenU3Ec__AnonStorey10B_1_t1054025478 * __this, Il2CppObject * ___val0, const MethodInfo* method);
#define U3CWhenU3Ec__AnonStorey10B_1_U3CU3Em__195_m1115334199(__this, ___val0, method) ((  void (*) (U3CWhenU3Ec__AnonStorey10B_1_t1054025478 *, Il2CppObject *, const MethodInfo*))U3CWhenU3Ec__AnonStorey10B_1_U3CU3Em__195_m1115334199_gshared)(__this, ___val0, method)
