﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AdToApp.AndroidWrapper.ATABannerAdListener
struct ATABannerAdListener_t2854275270;

#include "codegen/il2cpp-codegen.h"

// System.Void AdToApp.AndroidWrapper.ATABannerAdListener::.ctor()
extern "C"  void ATABannerAdListener__ctor_m1543430333 (ATABannerAdListener_t2854275270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AndroidWrapper.ATABannerAdListener::onBannerLoad()
extern "C"  void ATABannerAdListener_onBannerLoad_m2673358840 (ATABannerAdListener_t2854275270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AndroidWrapper.ATABannerAdListener::onBannerFailedToLoad()
extern "C"  void ATABannerAdListener_onBannerFailedToLoad_m2596828336 (ATABannerAdListener_t2854275270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AndroidWrapper.ATABannerAdListener::onBannerClicked()
extern "C"  void ATABannerAdListener_onBannerClicked_m1448583735 (ATABannerAdListener_t2854275270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
