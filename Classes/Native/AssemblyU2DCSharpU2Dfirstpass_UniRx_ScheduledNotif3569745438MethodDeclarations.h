﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ScheduledNotifier`1/<Report>c__AnonStorey58<System.Single>
struct U3CReportU3Ec__AnonStorey58_t3569745438;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.ScheduledNotifier`1/<Report>c__AnonStorey58<System.Single>::.ctor()
extern "C"  void U3CReportU3Ec__AnonStorey58__ctor_m2014622677_gshared (U3CReportU3Ec__AnonStorey58_t3569745438 * __this, const MethodInfo* method);
#define U3CReportU3Ec__AnonStorey58__ctor_m2014622677(__this, method) ((  void (*) (U3CReportU3Ec__AnonStorey58_t3569745438 *, const MethodInfo*))U3CReportU3Ec__AnonStorey58__ctor_m2014622677_gshared)(__this, method)
// System.Void UniRx.ScheduledNotifier`1/<Report>c__AnonStorey58<System.Single>::<>m__6C()
extern "C"  void U3CReportU3Ec__AnonStorey58_U3CU3Em__6C_m1330349291_gshared (U3CReportU3Ec__AnonStorey58_t3569745438 * __this, const MethodInfo* method);
#define U3CReportU3Ec__AnonStorey58_U3CU3Em__6C_m1330349291(__this, method) ((  void (*) (U3CReportU3Ec__AnonStorey58_t3569745438 *, const MethodInfo*))U3CReportU3Ec__AnonStorey58_U3CU3Em__6C_m1330349291_gshared)(__this, method)
