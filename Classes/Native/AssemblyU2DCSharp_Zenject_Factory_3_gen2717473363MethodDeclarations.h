﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.Factory`3<System.Object,System.Object,System.Object>
struct Factory_3_t2717473363;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t3431720054;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void Zenject.Factory`3<System.Object,System.Object,System.Object>::.ctor()
extern "C"  void Factory_3__ctor_m1680892321_gshared (Factory_3_t2717473363 * __this, const MethodInfo* method);
#define Factory_3__ctor_m1680892321(__this, method) ((  void (*) (Factory_3_t2717473363 *, const MethodInfo*))Factory_3__ctor_m1680892321_gshared)(__this, method)
// System.Type Zenject.Factory`3<System.Object,System.Object,System.Object>::get_ConstructedType()
extern "C"  Type_t * Factory_3_get_ConstructedType_m1442327372_gshared (Factory_3_t2717473363 * __this, const MethodInfo* method);
#define Factory_3_get_ConstructedType_m1442327372(__this, method) ((  Type_t * (*) (Factory_3_t2717473363 *, const MethodInfo*))Factory_3_get_ConstructedType_m1442327372_gshared)(__this, method)
// System.Type[] Zenject.Factory`3<System.Object,System.Object,System.Object>::get_ProvidedTypes()
extern "C"  TypeU5BU5D_t3431720054* Factory_3_get_ProvidedTypes_m518722868_gshared (Factory_3_t2717473363 * __this, const MethodInfo* method);
#define Factory_3_get_ProvidedTypes_m518722868(__this, method) ((  TypeU5BU5D_t3431720054* (*) (Factory_3_t2717473363 *, const MethodInfo*))Factory_3_get_ProvidedTypes_m518722868_gshared)(__this, method)
// Zenject.DiContainer Zenject.Factory`3<System.Object,System.Object,System.Object>::get_Container()
extern "C"  DiContainer_t2383114449 * Factory_3_get_Container_m6330113_gshared (Factory_3_t2717473363 * __this, const MethodInfo* method);
#define Factory_3_get_Container_m6330113(__this, method) ((  DiContainer_t2383114449 * (*) (Factory_3_t2717473363 *, const MethodInfo*))Factory_3_get_Container_m6330113_gshared)(__this, method)
// TValue Zenject.Factory`3<System.Object,System.Object,System.Object>::Create(TParam1,TParam2)
extern "C"  Il2CppObject * Factory_3_Create_m3393227086_gshared (Factory_3_t2717473363 * __this, Il2CppObject * ___param10, Il2CppObject * ___param21, const MethodInfo* method);
#define Factory_3_Create_m3393227086(__this, ___param10, ___param21, method) ((  Il2CppObject * (*) (Factory_3_t2717473363 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Factory_3_Create_m3393227086_gshared)(__this, ___param10, ___param21, method)
