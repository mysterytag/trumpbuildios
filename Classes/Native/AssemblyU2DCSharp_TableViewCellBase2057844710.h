﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Image
struct Image_t3354615620;
// UniRx.Subject`1<UniRx.Unit>
struct Subject_1_t201353362;
// UniRx.Subject`1<TableViewCellBase>
struct Subject_1_t3995879330;
// System.Collections.Generic.List`1<UnityEngine.UI.Image>
struct List_1_t4151574589;
// OligarchParameters
struct OligarchParameters_t821144443;

#include "AssemblyU2DCSharp_Tacticsoft_TableViewCell776419755.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TableViewCellBase
struct  TableViewCellBase_t2057844710  : public TableViewCell_t776419755
{
public:
	// UnityEngine.UI.Image TableViewCellBase::Icon
	Image_t3354615620 * ___Icon_7;
	// UniRx.Subject`1<UniRx.Unit> TableViewCellBase::ClickSubject
	Subject_1_t201353362 * ___ClickSubject_8;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> TableViewCellBase::_images
	List_1_t4151574589 * ____images_10;
	// OligarchParameters TableViewCellBase::OligarchParameters
	OligarchParameters_t821144443 * ___OligarchParameters_11;
	// System.Boolean TableViewCellBase::Clickable
	bool ___Clickable_12;

public:
	inline static int32_t get_offset_of_Icon_7() { return static_cast<int32_t>(offsetof(TableViewCellBase_t2057844710, ___Icon_7)); }
	inline Image_t3354615620 * get_Icon_7() const { return ___Icon_7; }
	inline Image_t3354615620 ** get_address_of_Icon_7() { return &___Icon_7; }
	inline void set_Icon_7(Image_t3354615620 * value)
	{
		___Icon_7 = value;
		Il2CppCodeGenWriteBarrier(&___Icon_7, value);
	}

	inline static int32_t get_offset_of_ClickSubject_8() { return static_cast<int32_t>(offsetof(TableViewCellBase_t2057844710, ___ClickSubject_8)); }
	inline Subject_1_t201353362 * get_ClickSubject_8() const { return ___ClickSubject_8; }
	inline Subject_1_t201353362 ** get_address_of_ClickSubject_8() { return &___ClickSubject_8; }
	inline void set_ClickSubject_8(Subject_1_t201353362 * value)
	{
		___ClickSubject_8 = value;
		Il2CppCodeGenWriteBarrier(&___ClickSubject_8, value);
	}

	inline static int32_t get_offset_of__images_10() { return static_cast<int32_t>(offsetof(TableViewCellBase_t2057844710, ____images_10)); }
	inline List_1_t4151574589 * get__images_10() const { return ____images_10; }
	inline List_1_t4151574589 ** get_address_of__images_10() { return &____images_10; }
	inline void set__images_10(List_1_t4151574589 * value)
	{
		____images_10 = value;
		Il2CppCodeGenWriteBarrier(&____images_10, value);
	}

	inline static int32_t get_offset_of_OligarchParameters_11() { return static_cast<int32_t>(offsetof(TableViewCellBase_t2057844710, ___OligarchParameters_11)); }
	inline OligarchParameters_t821144443 * get_OligarchParameters_11() const { return ___OligarchParameters_11; }
	inline OligarchParameters_t821144443 ** get_address_of_OligarchParameters_11() { return &___OligarchParameters_11; }
	inline void set_OligarchParameters_11(OligarchParameters_t821144443 * value)
	{
		___OligarchParameters_11 = value;
		Il2CppCodeGenWriteBarrier(&___OligarchParameters_11, value);
	}

	inline static int32_t get_offset_of_Clickable_12() { return static_cast<int32_t>(offsetof(TableViewCellBase_t2057844710, ___Clickable_12)); }
	inline bool get_Clickable_12() const { return ___Clickable_12; }
	inline bool* get_address_of_Clickable_12() { return &___Clickable_12; }
	inline void set_Clickable_12(bool value)
	{
		___Clickable_12 = value;
	}
};

struct TableViewCellBase_t2057844710_StaticFields
{
public:
	// UniRx.Subject`1<TableViewCellBase> TableViewCellBase::fillCell
	Subject_1_t3995879330 * ___fillCell_9;

public:
	inline static int32_t get_offset_of_fillCell_9() { return static_cast<int32_t>(offsetof(TableViewCellBase_t2057844710_StaticFields, ___fillCell_9)); }
	inline Subject_1_t3995879330 * get_fillCell_9() const { return ___fillCell_9; }
	inline Subject_1_t3995879330 ** get_address_of_fillCell_9() { return &___fillCell_9; }
	inline void set_fillCell_9(Subject_1_t3995879330 * value)
	{
		___fillCell_9 = value;
		Il2CppCodeGenWriteBarrier(&___fillCell_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
