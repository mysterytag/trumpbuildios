﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.FromCoroutine`1/FromCoroutineObserver<System.Int64>
struct FromCoroutineObserver_t1730676331;
// UniRx.IObserver`1<System.Int64>
struct IObserver_1_t764446489;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.FromCoroutine`1/FromCoroutineObserver<System.Int64>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void FromCoroutineObserver__ctor_m343764228_gshared (FromCoroutineObserver_t1730676331 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define FromCoroutineObserver__ctor_m343764228(__this, ___observer0, ___cancel1, method) ((  void (*) (FromCoroutineObserver_t1730676331 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))FromCoroutineObserver__ctor_m343764228_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.FromCoroutine`1/FromCoroutineObserver<System.Int64>::OnNext(T)
extern "C"  void FromCoroutineObserver_OnNext_m934453262_gshared (FromCoroutineObserver_t1730676331 * __this, int64_t ___value0, const MethodInfo* method);
#define FromCoroutineObserver_OnNext_m934453262(__this, ___value0, method) ((  void (*) (FromCoroutineObserver_t1730676331 *, int64_t, const MethodInfo*))FromCoroutineObserver_OnNext_m934453262_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.FromCoroutine`1/FromCoroutineObserver<System.Int64>::OnError(System.Exception)
extern "C"  void FromCoroutineObserver_OnError_m318824221_gshared (FromCoroutineObserver_t1730676331 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define FromCoroutineObserver_OnError_m318824221(__this, ___error0, method) ((  void (*) (FromCoroutineObserver_t1730676331 *, Exception_t1967233988 *, const MethodInfo*))FromCoroutineObserver_OnError_m318824221_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.FromCoroutine`1/FromCoroutineObserver<System.Int64>::OnCompleted()
extern "C"  void FromCoroutineObserver_OnCompleted_m859280880_gshared (FromCoroutineObserver_t1730676331 * __this, const MethodInfo* method);
#define FromCoroutineObserver_OnCompleted_m859280880(__this, method) ((  void (*) (FromCoroutineObserver_t1730676331 *, const MethodInfo*))FromCoroutineObserver_OnCompleted_m859280880_gshared)(__this, method)
