﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.OpenReadCompletedEventArgs
struct OpenReadCompletedEventArgs_t223760182;
// System.IO.Stream
struct Stream_t219029575;
// System.Exception
struct Exception_t1967233988;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream219029575.h"
#include "mscorlib_System_Exception1967233988.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Net.OpenReadCompletedEventArgs::.ctor(System.IO.Stream,System.Exception,System.Boolean,System.Object)
extern "C"  void OpenReadCompletedEventArgs__ctor_m1321533210 (OpenReadCompletedEventArgs_t223760182 * __this, Stream_t219029575 * ___result0, Exception_t1967233988 * ___error1, bool ___cancelled2, Il2CppObject * ___userState3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.Net.OpenReadCompletedEventArgs::get_Result()
extern "C"  Stream_t219029575 * OpenReadCompletedEventArgs_get_Result_m3105401478 (OpenReadCompletedEventArgs_t223760182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
