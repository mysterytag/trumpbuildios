﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/StartGameResponseCallback
struct StartGameResponseCallback_t2159445786;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.StartGameRequest
struct StartGameRequest_t2370915499;
// PlayFab.ClientModels.StartGameResult
struct StartGameResult_t665818273;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_StartGameRe2370915499.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_StartGameRes665818273.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/StartGameResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void StartGameResponseCallback__ctor_m2264558249 (StartGameResponseCallback_t2159445786 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/StartGameResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.StartGameRequest,PlayFab.ClientModels.StartGameResult,PlayFab.PlayFabError,System.Object)
extern "C"  void StartGameResponseCallback_Invoke_m2690919740 (StartGameResponseCallback_t2159445786 * __this, String_t* ___urlPath0, int32_t ___callId1, StartGameRequest_t2370915499 * ___request2, StartGameResult_t665818273 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_StartGameResponseCallback_t2159445786(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, StartGameRequest_t2370915499 * ___request2, StartGameResult_t665818273 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/StartGameResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.StartGameRequest,PlayFab.ClientModels.StartGameResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * StartGameResponseCallback_BeginInvoke_m749080037 (StartGameResponseCallback_t2159445786 * __this, String_t* ___urlPath0, int32_t ___callId1, StartGameRequest_t2370915499 * ___request2, StartGameResult_t665818273 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/StartGameResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void StartGameResponseCallback_EndInvoke_m2037881145 (StartGameResponseCallback_t2159445786 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
