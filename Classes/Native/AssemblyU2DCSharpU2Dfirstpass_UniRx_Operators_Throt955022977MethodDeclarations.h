﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ThrottleFirstFrameObservable`1<System.Object>
struct ThrottleFirstFrameObservable_1_t955022977;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_FrameCountType167759182.h"

// System.Void UniRx.Operators.ThrottleFirstFrameObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Int32,UniRx.FrameCountType)
extern "C"  void ThrottleFirstFrameObservable_1__ctor_m3787531655_gshared (ThrottleFirstFrameObservable_1_t955022977 * __this, Il2CppObject* ___source0, int32_t ___frameCount1, int32_t ___frameCountType2, const MethodInfo* method);
#define ThrottleFirstFrameObservable_1__ctor_m3787531655(__this, ___source0, ___frameCount1, ___frameCountType2, method) ((  void (*) (ThrottleFirstFrameObservable_1_t955022977 *, Il2CppObject*, int32_t, int32_t, const MethodInfo*))ThrottleFirstFrameObservable_1__ctor_m3787531655_gshared)(__this, ___source0, ___frameCount1, ___frameCountType2, method)
// System.IDisposable UniRx.Operators.ThrottleFirstFrameObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * ThrottleFirstFrameObservable_1_SubscribeCore_m2769623739_gshared (ThrottleFirstFrameObservable_1_t955022977 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define ThrottleFirstFrameObservable_1_SubscribeCore_m2769623739(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (ThrottleFirstFrameObservable_1_t955022977 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))ThrottleFirstFrameObservable_1_SubscribeCore_m2769623739_gshared)(__this, ___observer0, ___cancel1, method)
