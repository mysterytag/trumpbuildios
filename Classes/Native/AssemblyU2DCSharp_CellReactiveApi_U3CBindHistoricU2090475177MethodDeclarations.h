﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/<BindHistoric>c__AnonStoreyFD`1<System.Object>
struct U3CBindHistoricU3Ec__AnonStoreyFD_1_t2090475177;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void CellReactiveApi/<BindHistoric>c__AnonStoreyFD`1<System.Object>::.ctor()
extern "C"  void U3CBindHistoricU3Ec__AnonStoreyFD_1__ctor_m766171871_gshared (U3CBindHistoricU3Ec__AnonStoreyFD_1_t2090475177 * __this, const MethodInfo* method);
#define U3CBindHistoricU3Ec__AnonStoreyFD_1__ctor_m766171871(__this, method) ((  void (*) (U3CBindHistoricU3Ec__AnonStoreyFD_1_t2090475177 *, const MethodInfo*))U3CBindHistoricU3Ec__AnonStoreyFD_1__ctor_m766171871_gshared)(__this, method)
// System.Void CellReactiveApi/<BindHistoric>c__AnonStoreyFD`1<System.Object>::<>m__173(T)
extern "C"  void U3CBindHistoricU3Ec__AnonStoreyFD_1_U3CU3Em__173_m3255384503_gshared (U3CBindHistoricU3Ec__AnonStoreyFD_1_t2090475177 * __this, Il2CppObject * ___val0, const MethodInfo* method);
#define U3CBindHistoricU3Ec__AnonStoreyFD_1_U3CU3Em__173_m3255384503(__this, ___val0, method) ((  void (*) (U3CBindHistoricU3Ec__AnonStoreyFD_1_t2090475177 *, Il2CppObject *, const MethodInfo*))U3CBindHistoricU3Ec__AnonStoreyFD_1_U3CU3Em__173_m3255384503_gshared)(__this, ___val0, method)
