﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// OnePF.SkuDetails
struct SkuDetails_t70130009;
// OnePF.Purchase
struct Purchase_t160195573;
// OnePF.JSON
struct JSON_t2649481148;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;
// SimpleJSON.JSONNode
struct JSONNode_t580622632;
// UniRx.IObserver`1<System.Single>
struct IObserver_1_t3170207924;
// UniRx.IObservable`1<System.String>
struct IObservable_1_t727287266;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// UniRx.IObserver`1<System.Int32>
struct IObserver_1_t764446394;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;
// UniRx.ISubject`1<System.Object>
struct ISubject_1_t353709935;
// UniRx.IObserver`1<System.Boolean>
struct IObserver_1_t2423004244;
// UniRx.IObserver`1<UniRx.CountChangedStatus>
struct IObserver_1_t1688259328;
// UniRx.IObserver`1<System.Int64>
struct IObserver_1_t764446489;
// UniRx.IObserver`1<System.Byte>
struct IObserver_1_t695725428;
// UniRx.IObserver`1<System.Double>
struct IObserver_1_t2746515517;
// UniRx.IObserver`1<UnityEngine.Vector2>
struct IObserver_1_t1442361395;
// UniRx.IObserver`1<UnityEngine.Vector3>
struct IObserver_1_t1442361396;
// UniRx.IObserver`1<UnityEngine.Vector4>
struct IObserver_1_t1442361397;
// UniRx.IObserver`1<UnityEngine.Color>
struct IObserver_1_t3800174663;
// UniRx.IObserver`1<UnityEngine.Rect>
struct IObserver_1_t3737427720;
// UniRx.IObserver`1<UnityEngine.Bounds>
struct IObserver_1_t1435546585;
// UniRx.IObserver`1<UnityEngine.Quaternion>
struct IObserver_1_t4103714882;
// UniRx.LazyTask
struct LazyTask_t1365889643;
// UniRx.MainThreadDispatcher
struct MainThreadDispatcher_t4027217788;
// UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>
struct IObserver_1_t296601793;
// UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Object>>
struct IObserver_1_t2581260722;
// UniRx.IObserver`1<UnityEngine.NetworkDisconnection>
struct IObserver_1_t2550759298;
// UniRx.IObserver`1<UnityEngine.NetworkConnectionError>
struct IObserver_1_t3235804896;
// UniRx.IObserver`1<UnityEngine.MasterServerEvent>
struct IObserver_1_t194839097;
// UniRx.IObserver`1<UnityEngine.NetworkMessageInfo>
struct IObserver_1_t491376491;
// UniRx.IObserver`1<UnityEngine.NetworkPlayer>
struct IObserver_1_t3493136275;
// UniRx.IObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>
struct IObserver_1_t23531890;
// UniRx.IPresenter
struct IPresenter_t2177085329;
// UniRx.IObserver`1<UniRx.CollectionAddEvent`1<System.Object>>
struct IObserver_1_t333553594;
// UniRx.IObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>
struct IObserver_1_t833465454;
// UniRx.IObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>
struct IObserver_1_t2454636627;
// UniRx.IObserver`1<UniRx.CollectionReplaceEvent`1<System.Object>>
struct IObserver_1_t4053749439;
// UniRx.IObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>
struct IObserver_1_t1695958670;
// UniRx.IObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>
struct IObserver_1_t2462979911;
// UniRx.IObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>
struct IObserver_1_t1239319898;
// UniRx.IObservable`1<System.Boolean>
struct IObservable_1_t4264771001;
// UniRx.IObserver`1<TableButtons/StatkaPoTableViev>
struct IObserver_1_t1425427424;
// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;
// UniRx.IStructuralEquatable
struct IStructuralEquatable_t339884642;
// UniRx.IStructuralComparable
struct IStructuralComparable_t1141307058;
// UniRx.ITuple
struct ITuple_t431777841;
// UniRx.IObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct IObserver_1_t4160676289;
// UniRx.IObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct IObserver_1_t365620853;
// UniRx.IObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct IObserver_1_t1986792026;
// UniRx.IObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct IObserver_1_t3585904838;
// UniRx.IObservable`1<UniRx.Tuple`2<System.String,System.String>>
struct IObservable_1_t3204789135;
// UniRx.IObservable`1<UniRx.Tuple`2<System.Object,System.Object>>
struct IObservable_1_t128060183;
// UniRx.InternalUtil.ImmutableList`1<System.Object>
struct ImmutableList_1_t1897310919;

#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_SkuDetails70130009.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_Purchase160195573.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_JSON2649481148.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SimpleJSON_JSONNode580622632.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_TimeInterval_1_708065542.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Timestamped_1_2519184273.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_P4248560733.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_P1029390262.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_LazyTask1365889643.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_MainThreadDisp4027217788.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen369261819.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen2188574943.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen3445990771.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionAddE2416521987.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I1897310919.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionAddE3054882909.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionAddE2250169008.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionAddE2494672228.h"

#pragma once
// OnePF.SkuDetails[]
struct SkuDetailsU5BU5D_t1013186852  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) SkuDetails_t70130009 * m_Items[1];

public:
	inline SkuDetails_t70130009 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline SkuDetails_t70130009 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, SkuDetails_t70130009 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// OnePF.Purchase[]
struct PurchaseU5BU5D_t1805421720  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Purchase_t160195573 * m_Items[1];

public:
	inline Purchase_t160195573 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Purchase_t160195573 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Purchase_t160195573 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// OnePF.JSON[]
struct JSONU5BU5D_t1777688021  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) JSON_t2649481148 * m_Items[1];

public:
	inline JSON_t2649481148 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline JSON_t2649481148 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, JSON_t2649481148 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// PlayFab.CallRequestContainer[]
struct CallRequestContainerU5BU5D_t4152489484  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) CallRequestContainer_t432318609 * m_Items[1];

public:
	inline CallRequestContainer_t432318609 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CallRequestContainer_t432318609 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CallRequestContainer_t432318609 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SimpleJSON.JSONNode[]
struct JSONNodeU5BU5D_t51860793  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) JSONNode_t580622632 * m_Items[1];

public:
	inline JSONNode_t580622632 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline JSONNode_t580622632 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, JSONNode_t580622632 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.IObserver`1<System.Single>[]
struct IObserver_1U5BU5D_t911596029  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.IObservable`1<System.String>[]
struct IObservable_1U5BU5D_t855805015  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.IObservable`1<System.Object>[]
struct IObservable_1U5BU5D_t2205425841  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.Unit[]
struct UnitU5BU5D_t1267952403  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Unit_t2558286038  m_Items[1];

public:
	inline Unit_t2558286038  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Unit_t2558286038 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Unit_t2558286038  value)
	{
		m_Items[index] = value;
	}
};
// UniRx.IObserver`1<System.Int32>[]
struct IObserver_1U5BU5D_t1502147871  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.IObserver`1<System.Object>[]
struct IObserver_1U5BU5D_t3998655818  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.TimeInterval`1<System.Object>[]
struct TimeInterval_1U5BU5D_t2124321827  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) TimeInterval_1_t708065542  m_Items[1];

public:
	inline TimeInterval_1_t708065542  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TimeInterval_1_t708065542 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TimeInterval_1_t708065542  value)
	{
		m_Items[index] = value;
	}
};
// UniRx.IObserver`1<UniRx.Unit>[]
struct IObserver_1U5BU5D_t960117152  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.ISubject`1<System.Object>[]
struct ISubject_1U5BU5D_t3884569078  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.Timestamped`1<System.Object>[]
struct Timestamped_1U5BU5D_t3409075980  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Timestamped_1_t2519184273  m_Items[1];

public:
	inline Timestamped_1_t2519184273  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Timestamped_1_t2519184273 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Timestamped_1_t2519184273  value)
	{
		m_Items[index] = value;
	}
};
// UniRx.InternalUtil.PriorityQueue`1/IndexedItem<System.Object>[]
struct IndexedItemU5BU5D_t3079746960  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) IndexedItem_t4248560733  m_Items[1];

public:
	inline IndexedItem_t4248560733  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline IndexedItem_t4248560733 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, IndexedItem_t4248560733  value)
	{
		m_Items[index] = value;
	}
};
// UniRx.InternalUtil.PriorityQueue`1/IndexedItem<UniRx.InternalUtil.ScheduledItem>[]
struct IndexedItemU5BU5D_t1528605875  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) IndexedItem_t1029390262  m_Items[1];

public:
	inline IndexedItem_t1029390262  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline IndexedItem_t1029390262 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, IndexedItem_t1029390262  value)
	{
		m_Items[index] = value;
	}
};
// UniRx.IObserver`1<System.Boolean>[]
struct IObserver_1U5BU5D_t3497092061  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.IObserver`1<UniRx.CountChangedStatus>[]
struct IObserver_1U5BU5D_t335784193  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.IObserver`1<System.Int64>[]
struct IObserver_1U5BU5D_t445342820  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.IObserver`1<System.Byte>[]
struct IObserver_1U5BU5D_t4045638205  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.IObserver`1<System.Double>[]
struct IObserver_1U5BU5D_t740445744  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.IObserver`1<UnityEngine.Vector2>[]
struct IObserver_1U5BU5D_t2433548706  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.IObserver`1<UnityEngine.Vector3>[]
struct IObserver_1U5BU5D_t2919736445  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.IObserver`1<UnityEngine.Vector4>[]
struct IObserver_1U5BU5D_t3405924184  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.IObserver`1<UnityEngine.Color>[]
struct IObserver_1U5BU5D_t3169245886  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.IObserver`1<UnityEngine.Rect>[]
struct IObserver_1U5BU5D_t749149145  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.IObserver`1<UnityEngine.Bounds>[]
struct IObserver_1U5BU5D_t579021988  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.IObserver`1<UnityEngine.Quaternion>[]
struct IObserver_1U5BU5D_t2928567415  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.LazyTask[]
struct LazyTaskU5BU5D_t3442259850  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) LazyTask_t1365889643 * m_Items[1];

public:
	inline LazyTask_t1365889643 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline LazyTask_t1365889643 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, LazyTask_t1365889643 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.MainThreadDispatcher[]
struct MainThreadDispatcherU5BU5D_t933801749  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) MainThreadDispatcher_t4027217788 * m_Items[1];

public:
	inline MainThreadDispatcher_t4027217788 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline MainThreadDispatcher_t4027217788 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, MainThreadDispatcher_t4027217788 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>[]
struct IObserver_1U5BU5D_t1093590812  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Object>>[]
struct IObserver_1U5BU5D_t3590098759  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.IObserver`1<UnityEngine.NetworkDisconnection>[]
struct IObserver_1U5BU5D_t3485751351  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.IObserver`1<UnityEngine.NetworkConnectionError>[]
struct IObserver_1U5BU5D_t1548317089  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.IObserver`1<UnityEngine.MasterServerEvent>[]
struct IObserver_1U5BU5D_t2992830660  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.IObserver`1<UnityEngine.NetworkMessageInfo>[]
struct IObserver_1U5BU5D_t98838154  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.IObserver`1<UnityEngine.NetworkPlayer>[]
struct IObserver_1U5BU5D_t632717506  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.IObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>[]
struct IObserver_1U5BU5D_t3985248391  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.IPresenter[]
struct IPresenterU5BU5D_t656077580  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.IObserver`1<UniRx.CollectionAddEvent`1<System.Object>>[]
struct IObserver_1U5BU5D_t482039839  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.IObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>[]
struct IObserver_1U5BU5D_t3525058075  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.IObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>[]
struct IObserver_1U5BU5D_t3107448066  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.IObserver`1<UniRx.CollectionReplaceEvent`1<System.Object>>[]
struct IObserver_1U5BU5D_t1302543206  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.IObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>[]
struct IObserver_1U5BU5D_t23082875  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.IObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>[]
struct IObserver_1U5BU5D_t2153699262  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.IObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>[]
struct IObserver_1U5BU5D_t329451519  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.IObservable`1<System.Boolean>[]
struct IObservable_1U5BU5D_t1703862084  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.IObserver`1<TableButtons/StatkaPoTableViev>[]
struct IObserver_1U5BU5D_t594978977  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.IObservable`1<UniRx.Unit>[]
struct IObservable_1U5BU5D_t3461854471  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.Tuple`2<System.Object,System.Object>[]
struct Tuple_2U5BU5D_t3897934010  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Tuple_2_t369261819  m_Items[1];

public:
	inline Tuple_2_t369261819  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Tuple_2_t369261819 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Tuple_2_t369261819  value)
	{
		m_Items[index] = value;
	}
};
// UniRx.IStructuralEquatable[]
struct IStructuralEquatableU5BU5D_t4182635991  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.IStructuralComparable[]
struct IStructuralComparableU5BU5D_t4226133575  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.ITuple[]
struct ITupleU5BU5D_t1509303276  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.IObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>[]
struct IObserver_1U5BU5D_t73482780  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.IObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>[]
struct IObserver_1U5BU5D_t3116501016  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.IObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>[]
struct IObserver_1U5BU5D_t2698891007  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.IObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>[]
struct IObserver_1U5BU5D_t893986147  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.Tuple`2<System.String,UnityEngine.Sprite>[]
struct Tuple_2U5BU5D_t2629098694  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Tuple_2_t2188574943  m_Items[1];

public:
	inline Tuple_2_t2188574943  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Tuple_2_t2188574943 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Tuple_2_t2188574943  value)
	{
		m_Items[index] = value;
	}
};
// UniRx.IObservable`1<UniRx.Tuple`2<System.String,System.String>>[]
struct IObservable_1U5BU5D_t2981730646  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.IObservable`1<UniRx.Tuple`2<System.Object,System.Object>>[]
struct IObservable_1U5BU5D_t1796868782  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.Tuple`2<System.String,System.String>[]
struct Tuple_2U5BU5D_t787828578  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Tuple_2_t3445990771  m_Items[1];

public:
	inline Tuple_2_t3445990771  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Tuple_2_t3445990771 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Tuple_2_t3445990771  value)
	{
		m_Items[index] = value;
	}
};
// UniRx.CollectionAddEvent`1<System.Object>[]
struct CollectionAddEvent_1U5BU5D_t789875090  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) CollectionAddEvent_1_t2416521987  m_Items[1];

public:
	inline CollectionAddEvent_1_t2416521987  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CollectionAddEvent_1_t2416521987 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CollectionAddEvent_1_t2416521987  value)
	{
		m_Items[index] = value;
	}
};
// UniRx.InternalUtil.ImmutableList`1<System.Object>[]
struct ImmutableList_1U5BU5D_t2697725502  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) ImmutableList_1_t1897310919 * m_Items[1];

public:
	inline ImmutableList_1_t1897310919 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ImmutableList_1_t1897310919 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ImmutableList_1_t1897310919 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UniRx.CollectionAddEvent`1<UpgradeButton>[]
struct CollectionAddEvent_1U5BU5D_t961943952  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) CollectionAddEvent_1_t3054882909  m_Items[1];

public:
	inline CollectionAddEvent_1_t3054882909  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CollectionAddEvent_1_t3054882909 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CollectionAddEvent_1_t3054882909  value)
	{
		m_Items[index] = value;
	}
};
// UniRx.CollectionAddEvent`1<DonateButton>[]
struct CollectionAddEvent_1U5BU5D_t313029777  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) CollectionAddEvent_1_t2250169008  m_Items[1];

public:
	inline CollectionAddEvent_1_t2250169008  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CollectionAddEvent_1_t2250169008 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CollectionAddEvent_1_t2250169008  value)
	{
		m_Items[index] = value;
	}
};
// UniRx.CollectionAddEvent`1<SettingsButton>[]
struct CollectionAddEvent_1U5BU5D_t3881868429  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) CollectionAddEvent_1_t2494672228  m_Items[1];

public:
	inline CollectionAddEvent_1_t2494672228  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CollectionAddEvent_1_t2494672228 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CollectionAddEvent_1_t2494672228  value)
	{
		m_Items[index] = value;
	}
};
