﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.UnityEqualityComparer/Vector2EqualityComparer
struct Vector2EqualityComparer_t1368622230;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"

// System.Void UniRx.UnityEqualityComparer/Vector2EqualityComparer::.ctor()
extern "C"  void Vector2EqualityComparer__ctor_m4041787660 (Vector2EqualityComparer_t1368622230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.UnityEqualityComparer/Vector2EqualityComparer::Equals(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  bool Vector2EqualityComparer_Equals_m3094482675 (Vector2EqualityComparer_t1368622230 * __this, Vector2_t3525329788  ___self0, Vector2_t3525329788  ___vector1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UniRx.UnityEqualityComparer/Vector2EqualityComparer::GetHashCode(UnityEngine.Vector2)
extern "C"  int32_t Vector2EqualityComparer_GetHashCode_m3863669481 (Vector2EqualityComparer_t1368622230 * __this, Vector2_t3525329788  ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
