﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetStoreItemsRequest
struct GetStoreItemsRequest_t3204724138;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.GetStoreItemsRequest::.ctor()
extern "C"  void GetStoreItemsRequest__ctor_m2862126291 (GetStoreItemsRequest_t3204724138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GetStoreItemsRequest::get_StoreId()
extern "C"  String_t* GetStoreItemsRequest_get_StoreId_m1961920583 (GetStoreItemsRequest_t3204724138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetStoreItemsRequest::set_StoreId(System.String)
extern "C"  void GetStoreItemsRequest_set_StoreId_m3574934482 (GetStoreItemsRequest_t3204724138 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GetStoreItemsRequest::get_CatalogVersion()
extern "C"  String_t* GetStoreItemsRequest_get_CatalogVersion_m3708874774 (GetStoreItemsRequest_t3204724138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetStoreItemsRequest::set_CatalogVersion(System.String)
extern "C"  void GetStoreItemsRequest_set_CatalogVersion_m3641232661 (GetStoreItemsRequest_t3204724138 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
