﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<UpdateSharedGroupData>c__AnonStoreyC0
struct U3CUpdateSharedGroupDataU3Ec__AnonStoreyC0_t1638635285;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<UpdateSharedGroupData>c__AnonStoreyC0::.ctor()
extern "C"  void U3CUpdateSharedGroupDataU3Ec__AnonStoreyC0__ctor_m2533217246 (U3CUpdateSharedGroupDataU3Ec__AnonStoreyC0_t1638635285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<UpdateSharedGroupData>c__AnonStoreyC0::<>m__AD(PlayFab.CallRequestContainer)
extern "C"  void U3CUpdateSharedGroupDataU3Ec__AnonStoreyC0_U3CU3Em__AD_m880325407 (U3CUpdateSharedGroupDataU3Ec__AnonStoreyC0_t1638635285 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
