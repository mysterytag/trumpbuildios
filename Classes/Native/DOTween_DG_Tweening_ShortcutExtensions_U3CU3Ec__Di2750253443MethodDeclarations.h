﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass72_0
struct U3CU3Ec__DisplayClass72_0_t2750253443;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"

// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass72_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass72_0__ctor_m2949033459 (U3CU3Ec__DisplayClass72_0_t2750253443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color DG.Tweening.ShortcutExtensions/<>c__DisplayClass72_0::<DOBlendableColor>b__0()
extern "C"  Color_t1588175760  U3CU3Ec__DisplayClass72_0_U3CDOBlendableColorU3Eb__0_m504380869 (U3CU3Ec__DisplayClass72_0_t2750253443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass72_0::<DOBlendableColor>b__1(UnityEngine.Color)
extern "C"  void U3CU3Ec__DisplayClass72_0_U3CDOBlendableColorU3Eb__1_m3975419911 (U3CU3Ec__DisplayClass72_0_t2750253443 * __this, Color_t1588175760  ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
