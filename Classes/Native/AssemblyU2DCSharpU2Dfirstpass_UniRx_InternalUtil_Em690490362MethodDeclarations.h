﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.EmptyObserver`1<UnityEngine.NetworkPlayer>
struct EmptyObserver_1_t690490362;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_NetworkPlayer1281137372.h"

// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.NetworkPlayer>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m3777574866_gshared (EmptyObserver_1_t690490362 * __this, const MethodInfo* method);
#define EmptyObserver_1__ctor_m3777574866(__this, method) ((  void (*) (EmptyObserver_1_t690490362 *, const MethodInfo*))EmptyObserver_1__ctor_m3777574866_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.NetworkPlayer>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m658607643_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EmptyObserver_1__cctor_m658607643(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EmptyObserver_1__cctor_m658607643_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.NetworkPlayer>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m3941320156_gshared (EmptyObserver_1_t690490362 * __this, const MethodInfo* method);
#define EmptyObserver_1_OnCompleted_m3941320156(__this, method) ((  void (*) (EmptyObserver_1_t690490362 *, const MethodInfo*))EmptyObserver_1_OnCompleted_m3941320156_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.NetworkPlayer>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m3417345289_gshared (EmptyObserver_1_t690490362 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define EmptyObserver_1_OnError_m3417345289(__this, ___error0, method) ((  void (*) (EmptyObserver_1_t690490362 *, Exception_t1967233988 *, const MethodInfo*))EmptyObserver_1_OnError_m3417345289_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.NetworkPlayer>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m3832784378_gshared (EmptyObserver_1_t690490362 * __this, NetworkPlayer_t1281137372  ___value0, const MethodInfo* method);
#define EmptyObserver_1_OnNext_m3832784378(__this, ___value0, method) ((  void (*) (EmptyObserver_1_t690490362 *, NetworkPlayer_t1281137372 , const MethodInfo*))EmptyObserver_1_OnNext_m3832784378_gshared)(__this, ___value0, method)
