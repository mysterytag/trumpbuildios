﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ReflectionUtils/GetDelegate
struct GetDelegate_t270123739;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.ReflectionUtils/GetDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void GetDelegate__ctor_m266313582 (GetDelegate_t270123739 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayFab.ReflectionUtils/GetDelegate::Invoke(System.Object)
extern "C"  Il2CppObject * GetDelegate_Invoke_m3977510337 (GetDelegate_t270123739 * __this, Il2CppObject * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" Il2CppObject * pinvoke_delegate_wrapper_GetDelegate_t270123739(Il2CppObject* delegate, Il2CppObject * ___source0);
// System.IAsyncResult PlayFab.ReflectionUtils/GetDelegate::BeginInvoke(System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetDelegate_BeginInvoke_m3279792117 (GetDelegate_t270123739 * __this, Il2CppObject * ___source0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayFab.ReflectionUtils/GetDelegate::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * GetDelegate_EndInvoke_m841681833 (GetDelegate_t270123739 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
