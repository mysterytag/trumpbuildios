﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_3_gen2453810542MethodDeclarations.h"

// System.Void System.Func`3<System.Single,System.String,<>__AnonType4`2<System.Single,System.String>>::.ctor(System.Object,System.IntPtr)
#define Func_3__ctor_m3483730141(__this, ___object0, ___method1, method) ((  void (*) (Func_3_t4290768246 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_3__ctor_m2316382416_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`3<System.Single,System.String,<>__AnonType4`2<System.Single,System.String>>::Invoke(T1,T2)
#define Func_3_Invoke_m2809531488(__this, ___arg10, ___arg21, method) ((  U3CU3E__AnonType4_2_t4023684950 * (*) (Func_3_t4290768246 *, float, String_t*, const MethodInfo*))Func_3_Invoke_m4291358073_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Func`3<System.Single,System.String,<>__AnonType4`2<System.Single,System.String>>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Func_3_BeginInvoke_m3271565285(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Func_3_t4290768246 *, float, String_t*, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_3_BeginInvoke_m1433176954_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// TResult System.Func`3<System.Single,System.String,<>__AnonType4`2<System.Single,System.String>>::EndInvoke(System.IAsyncResult)
#define Func_3_EndInvoke_m959829627(__this, ___result0, method) ((  U3CU3E__AnonType4_2_t4023684950 * (*) (Func_3_t4290768246 *, Il2CppObject *, const MethodInfo*))Func_3_EndInvoke_m3105892994_gshared)(__this, ___result0, method)
