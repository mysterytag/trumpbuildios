﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g54746374.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Double,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3126525293_gshared (KeyValuePair_2_t54746374 * __this, double ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m3126525293(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t54746374 *, double, Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m3126525293_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Double,System.Object>::get_Key()
extern "C"  double KeyValuePair_2_get_Key_m2666415963_gshared (KeyValuePair_2_t54746374 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m2666415963(__this, method) ((  double (*) (KeyValuePair_2_t54746374 *, const MethodInfo*))KeyValuePair_2_get_Key_m2666415963_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Double,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1029500060_gshared (KeyValuePair_2_t54746374 * __this, double ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m1029500060(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t54746374 *, double, const MethodInfo*))KeyValuePair_2_set_Key_m1029500060_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Double,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m3787089215_gshared (KeyValuePair_2_t54746374 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m3787089215(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t54746374 *, const MethodInfo*))KeyValuePair_2_get_Value_m3787089215_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Double,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m704156316_gshared (KeyValuePair_2_t54746374 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m704156316(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t54746374 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m704156316_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Double,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m201868268_gshared (KeyValuePair_2_t54746374 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m201868268(__this, method) ((  String_t* (*) (KeyValuePair_2_t54746374 *, const MethodInfo*))KeyValuePair_2_ToString_m201868268_gshared)(__this, method)
