﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int64[]
struct Int64U5BU5D_t753178071;
// UniRx.Operators.BufferObservable`2<System.Int64,System.Int64>
struct BufferObservable_2_t550437669;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.List`1<System.Int64>
struct List_1_t3644373851;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1811255927.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.BufferObservable`2/Buffer<System.Int64,System.Int64>
struct  Buffer_t1984651551  : public OperatorObserverBase_2_t1811255927
{
public:
	// UniRx.Operators.BufferObservable`2<TSource,TWindowBoundary> UniRx.Operators.BufferObservable`2/Buffer::parent
	BufferObservable_2_t550437669 * ___parent_3;
	// System.Object UniRx.Operators.BufferObservable`2/Buffer::gate
	Il2CppObject * ___gate_4;
	// System.Collections.Generic.List`1<TSource> UniRx.Operators.BufferObservable`2/Buffer::list
	List_1_t3644373851 * ___list_5;

public:
	inline static int32_t get_offset_of_parent_3() { return static_cast<int32_t>(offsetof(Buffer_t1984651551, ___parent_3)); }
	inline BufferObservable_2_t550437669 * get_parent_3() const { return ___parent_3; }
	inline BufferObservable_2_t550437669 ** get_address_of_parent_3() { return &___parent_3; }
	inline void set_parent_3(BufferObservable_2_t550437669 * value)
	{
		___parent_3 = value;
		Il2CppCodeGenWriteBarrier(&___parent_3, value);
	}

	inline static int32_t get_offset_of_gate_4() { return static_cast<int32_t>(offsetof(Buffer_t1984651551, ___gate_4)); }
	inline Il2CppObject * get_gate_4() const { return ___gate_4; }
	inline Il2CppObject ** get_address_of_gate_4() { return &___gate_4; }
	inline void set_gate_4(Il2CppObject * value)
	{
		___gate_4 = value;
		Il2CppCodeGenWriteBarrier(&___gate_4, value);
	}

	inline static int32_t get_offset_of_list_5() { return static_cast<int32_t>(offsetof(Buffer_t1984651551, ___list_5)); }
	inline List_1_t3644373851 * get_list_5() const { return ___list_5; }
	inline List_1_t3644373851 ** get_address_of_list_5() { return &___list_5; }
	inline void set_list_5(List_1_t3644373851 * value)
	{
		___list_5 = value;
		Il2CppCodeGenWriteBarrier(&___list_5, value);
	}
};

struct Buffer_t1984651551_StaticFields
{
public:
	// TSource[] UniRx.Operators.BufferObservable`2/Buffer::EmptyArray
	Int64U5BU5D_t753178071* ___EmptyArray_2;

public:
	inline static int32_t get_offset_of_EmptyArray_2() { return static_cast<int32_t>(offsetof(Buffer_t1984651551_StaticFields, ___EmptyArray_2)); }
	inline Int64U5BU5D_t753178071* get_EmptyArray_2() const { return ___EmptyArray_2; }
	inline Int64U5BU5D_t753178071** get_address_of_EmptyArray_2() { return &___EmptyArray_2; }
	inline void set_EmptyArray_2(Int64U5BU5D_t753178071* value)
	{
		___EmptyArray_2 = value;
		Il2CppCodeGenWriteBarrier(&___EmptyArray_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
