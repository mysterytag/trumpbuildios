﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StreamAPI/<FirstOnly>c__AnonStorey13A/<FirstOnly>c__AnonStorey13B
struct U3CFirstOnlyU3Ec__AnonStorey13B_t4032128535;

#include "codegen/il2cpp-codegen.h"

// System.Void StreamAPI/<FirstOnly>c__AnonStorey13A/<FirstOnly>c__AnonStorey13B::.ctor()
extern "C"  void U3CFirstOnlyU3Ec__AnonStorey13B__ctor_m564161186 (U3CFirstOnlyU3Ec__AnonStorey13B_t4032128535 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StreamAPI/<FirstOnly>c__AnonStorey13A/<FirstOnly>c__AnonStorey13B::<>m__1CF()
extern "C"  void U3CFirstOnlyU3Ec__AnonStorey13B_U3CU3Em__1CF_m3549220267 (U3CFirstOnlyU3Ec__AnonStorey13B_t4032128535 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
