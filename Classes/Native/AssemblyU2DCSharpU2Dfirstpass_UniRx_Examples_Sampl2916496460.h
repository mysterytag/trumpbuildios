﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<UniRx.Unit>
struct Action_1_t2706738743;
// System.Action
struct Action_t437523947;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Examples.Sample02_ObservableTriggers
struct  Sample02_ObservableTriggers_t2916496460  : public MonoBehaviour_t3012272455
{
public:

public:
};

struct Sample02_ObservableTriggers_t2916496460_StaticFields
{
public:
	// System.Action`1<UniRx.Unit> UniRx.Examples.Sample02_ObservableTriggers::<>f__am$cache0
	Action_1_t2706738743 * ___U3CU3Ef__amU24cache0_2;
	// System.Action UniRx.Examples.Sample02_ObservableTriggers::<>f__am$cache1
	Action_t437523947 * ___U3CU3Ef__amU24cache1_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_2() { return static_cast<int32_t>(offsetof(Sample02_ObservableTriggers_t2916496460_StaticFields, ___U3CU3Ef__amU24cache0_2)); }
	inline Action_1_t2706738743 * get_U3CU3Ef__amU24cache0_2() const { return ___U3CU3Ef__amU24cache0_2; }
	inline Action_1_t2706738743 ** get_address_of_U3CU3Ef__amU24cache0_2() { return &___U3CU3Ef__amU24cache0_2; }
	inline void set_U3CU3Ef__amU24cache0_2(Action_1_t2706738743 * value)
	{
		___U3CU3Ef__amU24cache0_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_3() { return static_cast<int32_t>(offsetof(Sample02_ObservableTriggers_t2916496460_StaticFields, ___U3CU3Ef__amU24cache1_3)); }
	inline Action_t437523947 * get_U3CU3Ef__amU24cache1_3() const { return ___U3CU3Ef__amU24cache1_3; }
	inline Action_t437523947 ** get_address_of_U3CU3Ef__amU24cache1_3() { return &___U3CU3Ef__amU24cache1_3; }
	inline void set_U3CU3Ef__amU24cache1_3(Action_t437523947 * value)
	{
		___U3CU3Ef__amU24cache1_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
