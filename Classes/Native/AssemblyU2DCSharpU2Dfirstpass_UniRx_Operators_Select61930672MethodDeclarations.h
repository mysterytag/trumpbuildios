﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectManyObservable`2/SelectManyEnumerableObserverWithIndex<System.Object,System.Object>
struct SelectManyEnumerableObserverWithIndex_t61930672;
// UniRx.Operators.SelectManyObservable`2<System.Object,System.Object>
struct SelectManyObservable_2_t562993966;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyEnumerableObserverWithIndex<System.Object,System.Object>::.ctor(UniRx.Operators.SelectManyObservable`2<TSource,TResult>,UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void SelectManyEnumerableObserverWithIndex__ctor_m370066306_gshared (SelectManyEnumerableObserverWithIndex_t61930672 * __this, SelectManyObservable_2_t562993966 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define SelectManyEnumerableObserverWithIndex__ctor_m370066306(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (SelectManyEnumerableObserverWithIndex_t61930672 *, SelectManyObservable_2_t562993966 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SelectManyEnumerableObserverWithIndex__ctor_m370066306_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.SelectManyObservable`2/SelectManyEnumerableObserverWithIndex<System.Object,System.Object>::Run()
extern "C"  Il2CppObject * SelectManyEnumerableObserverWithIndex_Run_m1698265980_gshared (SelectManyEnumerableObserverWithIndex_t61930672 * __this, const MethodInfo* method);
#define SelectManyEnumerableObserverWithIndex_Run_m1698265980(__this, method) ((  Il2CppObject * (*) (SelectManyEnumerableObserverWithIndex_t61930672 *, const MethodInfo*))SelectManyEnumerableObserverWithIndex_Run_m1698265980_gshared)(__this, method)
// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyEnumerableObserverWithIndex<System.Object,System.Object>::OnNext(TSource)
extern "C"  void SelectManyEnumerableObserverWithIndex_OnNext_m824687269_gshared (SelectManyEnumerableObserverWithIndex_t61930672 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define SelectManyEnumerableObserverWithIndex_OnNext_m824687269(__this, ___value0, method) ((  void (*) (SelectManyEnumerableObserverWithIndex_t61930672 *, Il2CppObject *, const MethodInfo*))SelectManyEnumerableObserverWithIndex_OnNext_m824687269_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyEnumerableObserverWithIndex<System.Object,System.Object>::OnError(System.Exception)
extern "C"  void SelectManyEnumerableObserverWithIndex_OnError_m1785147343_gshared (SelectManyEnumerableObserverWithIndex_t61930672 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define SelectManyEnumerableObserverWithIndex_OnError_m1785147343(__this, ___error0, method) ((  void (*) (SelectManyEnumerableObserverWithIndex_t61930672 *, Exception_t1967233988 *, const MethodInfo*))SelectManyEnumerableObserverWithIndex_OnError_m1785147343_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyEnumerableObserverWithIndex<System.Object,System.Object>::OnCompleted()
extern "C"  void SelectManyEnumerableObserverWithIndex_OnCompleted_m1184620962_gshared (SelectManyEnumerableObserverWithIndex_t61930672 * __this, const MethodInfo* method);
#define SelectManyEnumerableObserverWithIndex_OnCompleted_m1184620962(__this, method) ((  void (*) (SelectManyEnumerableObserverWithIndex_t61930672 *, const MethodInfo*))SelectManyEnumerableObserverWithIndex_OnCompleted_m1184620962_gshared)(__this, method)
