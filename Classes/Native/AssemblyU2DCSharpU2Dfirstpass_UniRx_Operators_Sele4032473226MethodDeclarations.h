﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectManyObservable`2/SelectManyOuterObserver/SelectMany<System.Object,UniRx.Unit>
struct SelectMany_t4032473226;
// UniRx.Operators.SelectManyObservable`2/SelectManyOuterObserver<System.Object,UniRx.Unit>
struct SelectManyOuterObserver_t1519697883;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyOuterObserver/SelectMany<System.Object,UniRx.Unit>::.ctor(UniRx.Operators.SelectManyObservable`2/SelectManyOuterObserver<TSource,TResult>,System.IDisposable)
extern "C"  void SelectMany__ctor_m177567365_gshared (SelectMany_t4032473226 * __this, SelectManyOuterObserver_t1519697883 * ___parent0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define SelectMany__ctor_m177567365(__this, ___parent0, ___cancel1, method) ((  void (*) (SelectMany_t4032473226 *, SelectManyOuterObserver_t1519697883 *, Il2CppObject *, const MethodInfo*))SelectMany__ctor_m177567365_gshared)(__this, ___parent0, ___cancel1, method)
// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyOuterObserver/SelectMany<System.Object,UniRx.Unit>::OnNext(TResult)
extern "C"  void SelectMany_OnNext_m2903916750_gshared (SelectMany_t4032473226 * __this, Unit_t2558286038  ___value0, const MethodInfo* method);
#define SelectMany_OnNext_m2903916750(__this, ___value0, method) ((  void (*) (SelectMany_t4032473226 *, Unit_t2558286038 , const MethodInfo*))SelectMany_OnNext_m2903916750_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyOuterObserver/SelectMany<System.Object,UniRx.Unit>::OnError(System.Exception)
extern "C"  void SelectMany_OnError_m998042618_gshared (SelectMany_t4032473226 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define SelectMany_OnError_m998042618(__this, ___error0, method) ((  void (*) (SelectMany_t4032473226 *, Exception_t1967233988 *, const MethodInfo*))SelectMany_OnError_m998042618_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyOuterObserver/SelectMany<System.Object,UniRx.Unit>::OnCompleted()
extern "C"  void SelectMany_OnCompleted_m1889683021_gshared (SelectMany_t4032473226 * __this, const MethodInfo* method);
#define SelectMany_OnCompleted_m1889683021(__this, method) ((  void (*) (SelectMany_t4032473226 *, const MethodInfo*))SelectMany_OnCompleted_m1889683021_gshared)(__this, method)
