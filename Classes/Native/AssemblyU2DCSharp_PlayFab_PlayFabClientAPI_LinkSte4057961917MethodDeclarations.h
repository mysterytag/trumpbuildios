﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/LinkSteamAccountResponseCallback
struct LinkSteamAccountResponseCallback_t4057961917;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.LinkSteamAccountRequest
struct LinkSteamAccountRequest_t3142953640;
// PlayFab.ClientModels.LinkSteamAccountResult
struct LinkSteamAccountResult_t3738764036;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkSteamAc3142953640.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkSteamAc3738764036.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/LinkSteamAccountResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void LinkSteamAccountResponseCallback__ctor_m1725545516 (LinkSteamAccountResponseCallback_t4057961917 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LinkSteamAccountResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.LinkSteamAccountRequest,PlayFab.ClientModels.LinkSteamAccountResult,PlayFab.PlayFabError,System.Object)
extern "C"  void LinkSteamAccountResponseCallback_Invoke_m266782361 (LinkSteamAccountResponseCallback_t4057961917 * __this, String_t* ___urlPath0, int32_t ___callId1, LinkSteamAccountRequest_t3142953640 * ___request2, LinkSteamAccountResult_t3738764036 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_LinkSteamAccountResponseCallback_t4057961917(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, LinkSteamAccountRequest_t3142953640 * ___request2, LinkSteamAccountResult_t3738764036 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/LinkSteamAccountResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.LinkSteamAccountRequest,PlayFab.ClientModels.LinkSteamAccountResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LinkSteamAccountResponseCallback_BeginInvoke_m796888454 (LinkSteamAccountResponseCallback_t4057961917 * __this, String_t* ___urlPath0, int32_t ___callId1, LinkSteamAccountRequest_t3142953640 * ___request2, LinkSteamAccountResult_t3738764036 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LinkSteamAccountResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void LinkSteamAccountResponseCallback_EndInvoke_m4218037308 (LinkSteamAccountResponseCallback_t4057961917 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
