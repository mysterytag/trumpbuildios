﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WindowManager/<unlockInput>c__Iterator2B
struct U3CunlockInputU3Ec__Iterator2B_t3265886029;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void WindowManager/<unlockInput>c__Iterator2B::.ctor()
extern "C"  void U3CunlockInputU3Ec__Iterator2B__ctor_m4138965596 (U3CunlockInputU3Ec__Iterator2B_t3265886029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object WindowManager/<unlockInput>c__Iterator2B::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CunlockInputU3Ec__Iterator2B_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3459873792 (U3CunlockInputU3Ec__Iterator2B_t3265886029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object WindowManager/<unlockInput>c__Iterator2B::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CunlockInputU3Ec__Iterator2B_System_Collections_IEnumerator_get_Current_m1709408148 (U3CunlockInputU3Ec__Iterator2B_t3265886029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WindowManager/<unlockInput>c__Iterator2B::MoveNext()
extern "C"  bool U3CunlockInputU3Ec__Iterator2B_MoveNext_m2667866624 (U3CunlockInputU3Ec__Iterator2B_t3265886029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WindowManager/<unlockInput>c__Iterator2B::Dispose()
extern "C"  void U3CunlockInputU3Ec__Iterator2B_Dispose_m304504729 (U3CunlockInputU3Ec__Iterator2B_t3265886029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WindowManager/<unlockInput>c__Iterator2B::Reset()
extern "C"  void U3CunlockInputU3Ec__Iterator2B_Reset_m1785398537 (U3CunlockInputU3Ec__Iterator2B_t3265886029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
