﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameAnalyticsSDK.GA_ContinuationManager/EditorCoroutine
struct EditorCoroutine_t1174651851;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Func`1<System.Boolean>
struct Func_1_t1353786588;
// System.Action
struct Action_t437523947;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"

// System.Void GameAnalyticsSDK.GA_ContinuationManager/EditorCoroutine::.ctor(System.Collections.IEnumerator,System.Func`1<System.Boolean>)
extern "C"  void EditorCoroutine__ctor_m2833431776 (EditorCoroutine_t1174651851 * __this, Il2CppObject * ___routine0, Func_1_t1353786588 * ___done1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GameAnalyticsSDK.GA_ContinuationManager/EditorCoroutine::get_Routine()
extern "C"  Il2CppObject * EditorCoroutine_get_Routine_m3095053623 (EditorCoroutine_t1174651851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_ContinuationManager/EditorCoroutine::set_Routine(System.Collections.IEnumerator)
extern "C"  void EditorCoroutine_set_Routine_m2573263558 (EditorCoroutine_t1174651851 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Func`1<System.Boolean> GameAnalyticsSDK.GA_ContinuationManager/EditorCoroutine::get_Done()
extern "C"  Func_1_t1353786588 * EditorCoroutine_get_Done_m700417619 (EditorCoroutine_t1174651851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_ContinuationManager/EditorCoroutine::set_Done(System.Func`1<System.Boolean>)
extern "C"  void EditorCoroutine_set_Done_m3263202936 (EditorCoroutine_t1174651851 * __this, Func_1_t1353786588 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action GameAnalyticsSDK.GA_ContinuationManager/EditorCoroutine::get_ContinueWith()
extern "C"  Action_t437523947 * EditorCoroutine_get_ContinueWith_m1875472006 (EditorCoroutine_t1174651851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_ContinuationManager/EditorCoroutine::set_ContinueWith(System.Action)
extern "C"  void EditorCoroutine_set_ContinueWith_m2908392741 (EditorCoroutine_t1174651851 * __this, Action_t437523947 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
