﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.FactoryNested`12<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct FactoryNested_12_t4045899478;
// Zenject.IFactory`11<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct IFactory_11_t2716224629;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void Zenject.FactoryNested`12<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(Zenject.IFactory`11<TParam1,TParam2,TParam3,TParam4,TParam5,TParam6,TParam7,TParam8,TParam9,TParam10,TConcrete>)
extern "C"  void FactoryNested_12__ctor_m3724480721_gshared (FactoryNested_12_t4045899478 * __this, Il2CppObject* ___concreteFactory0, const MethodInfo* method);
#define FactoryNested_12__ctor_m3724480721(__this, ___concreteFactory0, method) ((  void (*) (FactoryNested_12_t4045899478 *, Il2CppObject*, const MethodInfo*))FactoryNested_12__ctor_m3724480721_gshared)(__this, ___concreteFactory0, method)
// TContract Zenject.FactoryNested`12<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Create(TParam1,TParam2,TParam3,TParam4,TParam5,TParam6,TParam7,TParam8,TParam9,TParam10)
extern "C"  Il2CppObject * FactoryNested_12_Create_m172551287_gshared (FactoryNested_12_t4045899478 * __this, Il2CppObject * ___param10, Il2CppObject * ___param21, Il2CppObject * ___param32, Il2CppObject * ___param43, Il2CppObject * ___param54, Il2CppObject * ___param65, Il2CppObject * ___param76, Il2CppObject * ___param87, Il2CppObject * ___param98, Il2CppObject * ___param109, const MethodInfo* method);
#define FactoryNested_12_Create_m172551287(__this, ___param10, ___param21, ___param32, ___param43, ___param54, ___param65, ___param76, ___param87, ___param98, ___param109, method) ((  Il2CppObject * (*) (FactoryNested_12_t4045899478 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))FactoryNested_12_Create_m172551287_gshared)(__this, ___param10, ___param21, ___param32, ___param43, ___param54, ___param65, ___param76, ___param87, ___param98, ___param109, method)
