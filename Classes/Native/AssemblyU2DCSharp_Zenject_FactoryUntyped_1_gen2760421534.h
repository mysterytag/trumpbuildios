﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.Type
struct Type_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FactoryUntyped`1<System.Object>
struct  FactoryUntyped_1_t2760421534  : public Il2CppObject
{
public:
	// Zenject.DiContainer Zenject.FactoryUntyped`1::_container
	DiContainer_t2383114449 * ____container_0;
	// System.Type Zenject.FactoryUntyped`1::_concreteType
	Type_t * ____concreteType_1;

public:
	inline static int32_t get_offset_of__container_0() { return static_cast<int32_t>(offsetof(FactoryUntyped_1_t2760421534, ____container_0)); }
	inline DiContainer_t2383114449 * get__container_0() const { return ____container_0; }
	inline DiContainer_t2383114449 ** get_address_of__container_0() { return &____container_0; }
	inline void set__container_0(DiContainer_t2383114449 * value)
	{
		____container_0 = value;
		Il2CppCodeGenWriteBarrier(&____container_0, value);
	}

	inline static int32_t get_offset_of__concreteType_1() { return static_cast<int32_t>(offsetof(FactoryUntyped_1_t2760421534, ____concreteType_1)); }
	inline Type_t * get__concreteType_1() const { return ____concreteType_1; }
	inline Type_t ** get_address_of__concreteType_1() { return &____concreteType_1; }
	inline void set__concreteType_1(Type_t * value)
	{
		____concreteType_1 = value;
		Il2CppCodeGenWriteBarrier(&____concreteType_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
