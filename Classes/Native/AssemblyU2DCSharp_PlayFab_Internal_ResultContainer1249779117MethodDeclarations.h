﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PlayFab_Internal_ResultContainer1789381198MethodDeclarations.h"

// System.Void PlayFab.Internal.ResultContainer`1<PlayFab.ClientModels.LinkCustomIDResult>::.ctor()
#define ResultContainer_1__ctor_m3905065841(__this, method) ((  void (*) (ResultContainer_1_t1249779117 *, const MethodInfo*))ResultContainer_1__ctor_m944826126_gshared)(__this, method)
// System.Void PlayFab.Internal.ResultContainer`1<PlayFab.ClientModels.LinkCustomIDResult>::.cctor()
#define ResultContainer_1__cctor_m315860572(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ResultContainer_1__cctor_m3037709919_gshared)(__this /* static, unused */, method)
// PlayFab.Internal.ResultContainer`1<TResultType> PlayFab.Internal.ResultContainer`1<PlayFab.ClientModels.LinkCustomIDResult>::KillWarnings()
#define ResultContainer_1_KillWarnings_m4012379045(__this /* static, unused */, method) ((  ResultContainer_1_t1249779117 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ResultContainer_1_KillWarnings_m2638698408_gshared)(__this /* static, unused */, method)
// TResultType PlayFab.Internal.ResultContainer`1<PlayFab.ClientModels.LinkCustomIDResult>::HandleResults(PlayFab.CallRequestContainer,System.Delegate,PlayFab.ErrorCallback,System.Action`2<TResultType,PlayFab.CallRequestContainer>)
#define ResultContainer_1_HandleResults_m4183155947(__this /* static, unused */, ___callRequest0, ___resultCallback1, ___errorCallback2, ___resultAction3, method) ((  LinkCustomIDResult_t297504339 * (*) (Il2CppObject * /* static, unused */, CallRequestContainer_t432318609 *, Delegate_t3660574010 *, ErrorCallback_t582108558 *, Action_2_t3363124912 *, const MethodInfo*))ResultContainer_1_HandleResults_m887745544_gshared)(__this /* static, unused */, ___callRequest0, ___resultCallback1, ___errorCallback2, ___resultAction3, method)
