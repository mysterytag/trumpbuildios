﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PlayFab.ErrorCallback
struct ErrorCallback_t582108558;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.Internal.EventTest/EventInstanceListener
struct  EventInstanceListener_t4205177603  : public Il2CppObject
{
public:

public:
};

struct EventInstanceListener_t4205177603_StaticFields
{
public:
	// PlayFab.ErrorCallback PlayFab.Internal.EventTest/EventInstanceListener::<>f__am$cache0
	ErrorCallback_t582108558 * ___U3CU3Ef__amU24cache0_0;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(EventInstanceListener_t4205177603_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline ErrorCallback_t582108558 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline ErrorCallback_t582108558 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(ErrorCallback_t582108558 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
