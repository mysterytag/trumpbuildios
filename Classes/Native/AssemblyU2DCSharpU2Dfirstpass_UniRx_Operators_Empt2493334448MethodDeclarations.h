﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.EmptyObservable`1/Empty<System.Double>
struct Empty_t2493334448;
// UniRx.IObserver`1<System.Double>
struct IObserver_1_t2746515517;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.EmptyObservable`1/Empty<System.Double>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Empty__ctor_m1156529469_gshared (Empty_t2493334448 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define Empty__ctor_m1156529469(__this, ___observer0, ___cancel1, method) ((  void (*) (Empty_t2493334448 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Empty__ctor_m1156529469_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.EmptyObservable`1/Empty<System.Double>::OnNext(T)
extern "C"  void Empty_OnNext_m123612213_gshared (Empty_t2493334448 * __this, double ___value0, const MethodInfo* method);
#define Empty_OnNext_m123612213(__this, ___value0, method) ((  void (*) (Empty_t2493334448 *, double, const MethodInfo*))Empty_OnNext_m123612213_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.EmptyObservable`1/Empty<System.Double>::OnError(System.Exception)
extern "C"  void Empty_OnError_m1542798660_gshared (Empty_t2493334448 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Empty_OnError_m1542798660(__this, ___error0, method) ((  void (*) (Empty_t2493334448 *, Exception_t1967233988 *, const MethodInfo*))Empty_OnError_m1542798660_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.EmptyObservable`1/Empty<System.Double>::OnCompleted()
extern "C"  void Empty_OnCompleted_m3965892247_gshared (Empty_t2493334448 * __this, const MethodInfo* method);
#define Empty_OnCompleted_m3965892247(__this, method) ((  void (*) (Empty_t2493334448 *, const MethodInfo*))Empty_OnCompleted_m3965892247_gshared)(__this, method)
