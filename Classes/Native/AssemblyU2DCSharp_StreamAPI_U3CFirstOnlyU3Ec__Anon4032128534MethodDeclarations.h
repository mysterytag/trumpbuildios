﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StreamAPI/<FirstOnly>c__AnonStorey13A
struct U3CFirstOnlyU3Ec__AnonStorey13A_t4032128534;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action
struct Action_t437523947;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"

// System.Void StreamAPI/<FirstOnly>c__AnonStorey13A::.ctor()
extern "C"  void U3CFirstOnlyU3Ec__AnonStorey13A__ctor_m2192130730 (U3CFirstOnlyU3Ec__AnonStorey13A_t4032128534 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable StreamAPI/<FirstOnly>c__AnonStorey13A::<>m__1C9(System.Action,Priority)
extern "C"  Il2CppObject * U3CFirstOnlyU3Ec__AnonStorey13A_U3CU3Em__1C9_m3156197128 (U3CFirstOnlyU3Ec__AnonStorey13A_t4032128534 * __this, Action_t437523947 * ___reaction0, int32_t ___p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
