﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4014815677MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<OnePF.SkuDetails>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2396489034(__this, ___l0, method) ((  void (*) (Enumerator_t3247839266 *, List_1_t867088978 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<OnePF.SkuDetails>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3235375304(__this, method) ((  void (*) (Enumerator_t3247839266 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<OnePF.SkuDetails>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m962280894(__this, method) ((  Il2CppObject * (*) (Enumerator_t3247839266 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<OnePF.SkuDetails>::Dispose()
#define Enumerator_Dispose_m3804923311(__this, method) ((  void (*) (Enumerator_t3247839266 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<OnePF.SkuDetails>::VerifyState()
#define Enumerator_VerifyState_m852874600(__this, method) ((  void (*) (Enumerator_t3247839266 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<OnePF.SkuDetails>::MoveNext()
#define Enumerator_MoveNext_m2837293921(__this, method) ((  bool (*) (Enumerator_t3247839266 *, const MethodInfo*))Enumerator_MoveNext_m302222300_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<OnePF.SkuDetails>::get_Current()
#define Enumerator_get_Current_m3417642715(__this, method) ((  SkuDetails_t70130009 * (*) (Enumerator_t3247839266 *, const MethodInfo*))Enumerator_get_Current_m1113061044_gshared)(__this, method)
