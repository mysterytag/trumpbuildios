﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SponsorPay.AbstractResponse
struct  AbstractResponse_t2918001245  : public Il2CppObject
{
public:
	// System.Boolean SponsorPay.AbstractResponse::<success>k__BackingField
	bool ___U3CsuccessU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CsuccessU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AbstractResponse_t2918001245, ___U3CsuccessU3Ek__BackingField_0)); }
	inline bool get_U3CsuccessU3Ek__BackingField_0() const { return ___U3CsuccessU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CsuccessU3Ek__BackingField_0() { return &___U3CsuccessU3Ek__BackingField_0; }
	inline void set_U3CsuccessU3Ek__BackingField_0(bool value)
	{
		___U3CsuccessU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
