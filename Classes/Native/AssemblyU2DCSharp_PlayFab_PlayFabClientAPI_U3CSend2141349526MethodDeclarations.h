﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<SendAccountRecoveryEmail>c__AnonStorey7F
struct U3CSendAccountRecoveryEmailU3Ec__AnonStorey7F_t2141349526;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<SendAccountRecoveryEmail>c__AnonStorey7F::.ctor()
extern "C"  void U3CSendAccountRecoveryEmailU3Ec__AnonStorey7F__ctor_m724614621 (U3CSendAccountRecoveryEmailU3Ec__AnonStorey7F_t2141349526 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<SendAccountRecoveryEmail>c__AnonStorey7F::<>m__6C(PlayFab.CallRequestContainer)
extern "C"  void U3CSendAccountRecoveryEmailU3Ec__AnonStorey7F_U3CU3Em__6C_m2823933448 (U3CSendAccountRecoveryEmailU3Ec__AnonStorey7F_t2141349526 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
