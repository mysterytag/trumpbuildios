﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// IEmptyStream
struct IEmptyStream_t3684082468;
// IEmptyStream[]
struct IEmptyStreamU5BU5D_t1849841613;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StreamAPI/<MergeWith>c__AnonStorey138
struct  U3CMergeWithU3Ec__AnonStorey138_t3466452907  : public Il2CppObject
{
public:
	// IEmptyStream StreamAPI/<MergeWith>c__AnonStorey138::stream
	Il2CppObject * ___stream_0;
	// IEmptyStream[] StreamAPI/<MergeWith>c__AnonStorey138::others
	IEmptyStreamU5BU5D_t1849841613* ___others_1;

public:
	inline static int32_t get_offset_of_stream_0() { return static_cast<int32_t>(offsetof(U3CMergeWithU3Ec__AnonStorey138_t3466452907, ___stream_0)); }
	inline Il2CppObject * get_stream_0() const { return ___stream_0; }
	inline Il2CppObject ** get_address_of_stream_0() { return &___stream_0; }
	inline void set_stream_0(Il2CppObject * value)
	{
		___stream_0 = value;
		Il2CppCodeGenWriteBarrier(&___stream_0, value);
	}

	inline static int32_t get_offset_of_others_1() { return static_cast<int32_t>(offsetof(U3CMergeWithU3Ec__AnonStorey138_t3466452907, ___others_1)); }
	inline IEmptyStreamU5BU5D_t1849841613* get_others_1() const { return ___others_1; }
	inline IEmptyStreamU5BU5D_t1849841613** get_address_of_others_1() { return &___others_1; }
	inline void set_others_1(IEmptyStreamU5BU5D_t1849841613* value)
	{
		___others_1 = value;
		Il2CppCodeGenWriteBarrier(&___others_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
