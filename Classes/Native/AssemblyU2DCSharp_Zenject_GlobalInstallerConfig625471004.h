﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Zenject.MonoInstaller[]
struct MonoInstallerU5BU5D_t2181499551;

#include "UnityEngine_UnityEngine_ScriptableObject184905905.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.GlobalInstallerConfig
struct  GlobalInstallerConfig_t625471004  : public ScriptableObject_t184905905
{
public:
	// Zenject.MonoInstaller[] Zenject.GlobalInstallerConfig::Installers
	MonoInstallerU5BU5D_t2181499551* ___Installers_2;

public:
	inline static int32_t get_offset_of_Installers_2() { return static_cast<int32_t>(offsetof(GlobalInstallerConfig_t625471004, ___Installers_2)); }
	inline MonoInstallerU5BU5D_t2181499551* get_Installers_2() const { return ___Installers_2; }
	inline MonoInstallerU5BU5D_t2181499551** get_address_of_Installers_2() { return &___Installers_2; }
	inline void set_Installers_2(MonoInstallerU5BU5D_t2181499551* value)
	{
		___Installers_2 = value;
		Il2CppCodeGenWriteBarrier(&___Installers_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
