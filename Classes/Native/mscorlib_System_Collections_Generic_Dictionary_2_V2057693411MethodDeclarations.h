﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>
struct Dictionary_2_t2290665470;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2057693411.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<SponsorPay.SPLogLevel,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2929783405_gshared (Enumerator_t2057693413 * __this, Dictionary_2_t2290665470 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m2929783405(__this, ___host0, method) ((  void (*) (Enumerator_t2057693413 *, Dictionary_2_t2290665470 *, const MethodInfo*))Enumerator__ctor_m2929783405_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<SponsorPay.SPLogLevel,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3587164190_gshared (Enumerator_t2057693413 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3587164190(__this, method) ((  Il2CppObject * (*) (Enumerator_t2057693413 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3587164190_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<SponsorPay.SPLogLevel,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1401639976_gshared (Enumerator_t2057693413 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1401639976(__this, method) ((  void (*) (Enumerator_t2057693413 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1401639976_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<SponsorPay.SPLogLevel,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m2324782671_gshared (Enumerator_t2057693413 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2324782671(__this, method) ((  void (*) (Enumerator_t2057693413 *, const MethodInfo*))Enumerator_Dispose_m2324782671_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<SponsorPay.SPLogLevel,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1210929112_gshared (Enumerator_t2057693413 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1210929112(__this, method) ((  bool (*) (Enumerator_t2057693413 *, const MethodInfo*))Enumerator_MoveNext_m1210929112_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<SponsorPay.SPLogLevel,System.Int32>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m250608466_gshared (Enumerator_t2057693413 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m250608466(__this, method) ((  int32_t (*) (Enumerator_t2057693413 *, const MethodInfo*))Enumerator_get_Current_m250608466_gshared)(__this, method)
