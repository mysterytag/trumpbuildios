﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.CatchObservable`1<System.Object>
struct CatchObservable_1_t1624243829;
// System.Collections.Generic.IEnumerable`1<UniRx.IObservable`1<System.Object>>
struct IEnumerable_1_t3468059140;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.CatchObservable`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<UniRx.IObservable`1<T>>)
extern "C"  void CatchObservable_1__ctor_m727030327_gshared (CatchObservable_1_t1624243829 * __this, Il2CppObject* ___sources0, const MethodInfo* method);
#define CatchObservable_1__ctor_m727030327(__this, ___sources0, method) ((  void (*) (CatchObservable_1_t1624243829 *, Il2CppObject*, const MethodInfo*))CatchObservable_1__ctor_m727030327_gshared)(__this, ___sources0, method)
// System.IDisposable UniRx.Operators.CatchObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * CatchObservable_1_SubscribeCore_m3817746035_gshared (CatchObservable_1_t1624243829 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define CatchObservable_1_SubscribeCore_m3817746035(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (CatchObservable_1_t1624243829 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))CatchObservable_1_SubscribeCore_m3817746035_gshared)(__this, ___observer0, ___cancel1, method)
