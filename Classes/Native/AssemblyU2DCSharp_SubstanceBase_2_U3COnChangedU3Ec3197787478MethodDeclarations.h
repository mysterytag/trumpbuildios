﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SubstanceBase`2/<OnChanged>c__AnonStorey142<System.Double,System.Double>
struct U3COnChangedU3Ec__AnonStorey142_t3197787478;

#include "codegen/il2cpp-codegen.h"

// System.Void SubstanceBase`2/<OnChanged>c__AnonStorey142<System.Double,System.Double>::.ctor()
extern "C"  void U3COnChangedU3Ec__AnonStorey142__ctor_m3505358214_gshared (U3COnChangedU3Ec__AnonStorey142_t3197787478 * __this, const MethodInfo* method);
#define U3COnChangedU3Ec__AnonStorey142__ctor_m3505358214(__this, method) ((  void (*) (U3COnChangedU3Ec__AnonStorey142_t3197787478 *, const MethodInfo*))U3COnChangedU3Ec__AnonStorey142__ctor_m3505358214_gshared)(__this, method)
// System.Void SubstanceBase`2/<OnChanged>c__AnonStorey142<System.Double,System.Double>::<>m__1D8(T)
extern "C"  void U3COnChangedU3Ec__AnonStorey142_U3CU3Em__1D8_m2295574598_gshared (U3COnChangedU3Ec__AnonStorey142_t3197787478 * __this, double ____0, const MethodInfo* method);
#define U3COnChangedU3Ec__AnonStorey142_U3CU3Em__1D8_m2295574598(__this, ____0, method) ((  void (*) (U3COnChangedU3Ec__AnonStorey142_t3197787478 *, double, const MethodInfo*))U3COnChangedU3Ec__AnonStorey142_U3CU3Em__1D8_m2295574598_gshared)(__this, ____0, method)
