﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Action`2<System.DateTime,System.Object>
struct Action_2_t2210548610;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_DateTime339033936.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Action`2<System.DateTime,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m3283378366_gshared (Action_2_t2210548610 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Action_2__ctor_m3283378366(__this, ___object0, ___method1, method) ((  void (*) (Action_2_t2210548610 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_2__ctor_m3283378366_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`2<System.DateTime,System.Object>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m1100612781_gshared (Action_2_t2210548610 * __this, DateTime_t339033936  ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
#define Action_2_Invoke_m1100612781(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t2210548610 *, DateTime_t339033936 , Il2CppObject *, const MethodInfo*))Action_2_Invoke_m1100612781_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Action`2<System.DateTime,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_2_BeginInvoke_m1192547276_gshared (Action_2_t2210548610 * __this, DateTime_t339033936  ___arg10, Il2CppObject * ___arg21, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Action_2_BeginInvoke_m1192547276(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Action_2_t2210548610 *, DateTime_t339033936 , Il2CppObject *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Action_2_BeginInvoke_m1192547276_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// System.Void System.Action`2<System.DateTime,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m1013047758_gshared (Action_2_t2210548610 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Action_2_EndInvoke_m1013047758(__this, ___result0, method) ((  void (*) (Action_2_t2210548610 *, Il2CppObject *, const MethodInfo*))Action_2_EndInvoke_m1013047758_gshared)(__this, ___result0, method)
