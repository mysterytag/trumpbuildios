﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.ValidateIOSReceiptRequest
struct ValidateIOSReceiptRequest_t1279430686;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.ValidateIOSReceiptRequest::.ctor()
extern "C"  void ValidateIOSReceiptRequest__ctor_m2292417995 (ValidateIOSReceiptRequest_t1279430686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.ValidateIOSReceiptRequest::get_ReceiptData()
extern "C"  String_t* ValidateIOSReceiptRequest_get_ReceiptData_m3936489343 (ValidateIOSReceiptRequest_t1279430686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ValidateIOSReceiptRequest::set_ReceiptData(System.String)
extern "C"  void ValidateIOSReceiptRequest_set_ReceiptData_m1994381428 (ValidateIOSReceiptRequest_t1279430686 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.ValidateIOSReceiptRequest::get_CurrencyCode()
extern "C"  String_t* ValidateIOSReceiptRequest_get_CurrencyCode_m4184427939 (ValidateIOSReceiptRequest_t1279430686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ValidateIOSReceiptRequest::set_CurrencyCode(System.String)
extern "C"  void ValidateIOSReceiptRequest_set_CurrencyCode_m243810766 (ValidateIOSReceiptRequest_t1279430686 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlayFab.ClientModels.ValidateIOSReceiptRequest::get_PurchasePrice()
extern "C"  int32_t ValidateIOSReceiptRequest_get_PurchasePrice_m3412641274 (ValidateIOSReceiptRequest_t1279430686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ValidateIOSReceiptRequest::set_PurchasePrice(System.Int32)
extern "C"  void ValidateIOSReceiptRequest_set_PurchasePrice_m2019591973 (ValidateIOSReceiptRequest_t1279430686 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
