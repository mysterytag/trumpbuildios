﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LocalyticsUnity.Localytics/LocalyticsDidTagEvent
struct LocalyticsDidTagEvent_t1639420300;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void LocalyticsUnity.Localytics/LocalyticsDidTagEvent::.ctor(System.Object,System.IntPtr)
extern "C"  void LocalyticsDidTagEvent__ctor_m3437844163 (LocalyticsDidTagEvent_t1639420300 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics/LocalyticsDidTagEvent::Invoke(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>,System.Int64)
extern "C"  void LocalyticsDidTagEvent_Invoke_m3644200710 (LocalyticsDidTagEvent_t1639420300 * __this, String_t* ___eventName0, Dictionary_2_t2606186806 * ___attributes1, int64_t ___customerValueIncrease2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult LocalyticsUnity.Localytics/LocalyticsDidTagEvent::BeginInvoke(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>,System.Int64,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LocalyticsDidTagEvent_BeginInvoke_m2045473321 (LocalyticsDidTagEvent_t1639420300 * __this, String_t* ___eventName0, Dictionary_2_t2606186806 * ___attributes1, int64_t ___customerValueIncrease2, AsyncCallback_t1363551830 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics/LocalyticsDidTagEvent::EndInvoke(System.IAsyncResult)
extern "C"  void LocalyticsDidTagEvent_EndInvoke_m497328723 (LocalyticsDidTagEvent_t1639420300 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
