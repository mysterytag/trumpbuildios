﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_2_gen4105459918MethodDeclarations.h"

// System.Void System.Action`2<PlayFab.ClientModels.GetAccountInfoResult,PlayFab.CallRequestContainer>::.ctor(System.Object,System.IntPtr)
#define Action_2__ctor_m1466224121(__this, ___object0, ___method1, method) ((  void (*) (Action_2_t3549999029 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_2__ctor_m3253598474_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`2<PlayFab.ClientModels.GetAccountInfoResult,PlayFab.CallRequestContainer>::Invoke(T1,T2)
#define Action_2_Invoke_m2509596818(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t3549999029 *, GetAccountInfoResult_t3022616562 *, CallRequestContainer_t432318609 *, const MethodInfo*))Action_2_Invoke_m1325815329_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Action`2<PlayFab.ClientModels.GetAccountInfoResult,PlayFab.CallRequestContainer>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Action_2_BeginInvoke_m3071226937(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Action_2_t3549999029 *, GetAccountInfoResult_t3022616562 *, CallRequestContainer_t432318609 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Action_2_BeginInvoke_m3413657584_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// System.Void System.Action`2<PlayFab.ClientModels.GetAccountInfoResult,PlayFab.CallRequestContainer>::EndInvoke(System.IAsyncResult)
#define Action_2_EndInvoke_m1052255369(__this, ___result0, method) ((  void (*) (Action_2_t3549999029 *, Il2CppObject *, const MethodInfo*))Action_2_EndInvoke_m3926193450_gshared)(__this, ___result0, method)
