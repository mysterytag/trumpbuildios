﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.DoObservable`1<System.Object>
struct DoObservable_1_t911152645;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// System.Action`1<System.Exception>
struct Action_1_t2115686693;
// System.Action
struct Action_t437523947;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"

// System.Void UniRx.Operators.DoObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Action`1<T>,System.Action`1<System.Exception>,System.Action)
extern "C"  void DoObservable_1__ctor_m529972321_gshared (DoObservable_1_t911152645 * __this, Il2CppObject* ___source0, Action_1_t985559125 * ___onNext1, Action_1_t2115686693 * ___onError2, Action_t437523947 * ___onCompleted3, const MethodInfo* method);
#define DoObservable_1__ctor_m529972321(__this, ___source0, ___onNext1, ___onError2, ___onCompleted3, method) ((  void (*) (DoObservable_1_t911152645 *, Il2CppObject*, Action_1_t985559125 *, Action_1_t2115686693 *, Action_t437523947 *, const MethodInfo*))DoObservable_1__ctor_m529972321_gshared)(__this, ___source0, ___onNext1, ___onError2, ___onCompleted3, method)
// System.IDisposable UniRx.Operators.DoObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * DoObservable_1_SubscribeCore_m1332131607_gshared (DoObservable_1_t911152645 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define DoObservable_1_SubscribeCore_m1332131607(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (DoObservable_1_t911152645 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))DoObservable_1_SubscribeCore_m1332131607_gshared)(__this, ___observer0, ___cancel1, method)
