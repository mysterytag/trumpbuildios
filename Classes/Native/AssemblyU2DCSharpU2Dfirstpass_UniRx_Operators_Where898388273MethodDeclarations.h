﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.WhereObservable`1/Where_<System.Object>
struct Where__t898388273;
// UniRx.Operators.WhereObservable`1<System.Object>
struct WhereObservable_1_t2035666369;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.WhereObservable`1/Where_<System.Object>::.ctor(UniRx.Operators.WhereObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Where___ctor_m3323323968_gshared (Where__t898388273 * __this, WhereObservable_1_t2035666369 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Where___ctor_m3323323968(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Where__t898388273 *, WhereObservable_1_t2035666369 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Where___ctor_m3323323968_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.WhereObservable`1/Where_<System.Object>::OnNext(T)
extern "C"  void Where__OnNext_m1292943718_gshared (Where__t898388273 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Where__OnNext_m1292943718(__this, ___value0, method) ((  void (*) (Where__t898388273 *, Il2CppObject *, const MethodInfo*))Where__OnNext_m1292943718_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.WhereObservable`1/Where_<System.Object>::OnError(System.Exception)
extern "C"  void Where__OnError_m1281446005_gshared (Where__t898388273 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Where__OnError_m1281446005(__this, ___error0, method) ((  void (*) (Where__t898388273 *, Exception_t1967233988 *, const MethodInfo*))Where__OnError_m1281446005_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.WhereObservable`1/Where_<System.Object>::OnCompleted()
extern "C"  void Where__OnCompleted_m1064651592_gshared (Where__t898388273 * __this, const MethodInfo* method);
#define Where__OnCompleted_m1064651592(__this, method) ((  void (*) (Where__t898388273 *, const MethodInfo*))Where__OnCompleted_m1064651592_gshared)(__this, method)
