﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey5C
struct U3CGetSetMethodByReflectionU3Ec__AnonStorey5C_t2116090498;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void PlayFab.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey5C::.ctor()
extern "C"  void U3CGetSetMethodByReflectionU3Ec__AnonStorey5C__ctor_m3559349101 (U3CGetSetMethodByReflectionU3Ec__AnonStorey5C_t2116090498 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey5C::<>m__48(System.Object,System.Object)
extern "C"  void U3CGetSetMethodByReflectionU3Ec__AnonStorey5C_U3CU3Em__48_m314058920 (U3CGetSetMethodByReflectionU3Ec__AnonStorey5C_t2116090498 * __this, Il2CppObject * ___source0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
