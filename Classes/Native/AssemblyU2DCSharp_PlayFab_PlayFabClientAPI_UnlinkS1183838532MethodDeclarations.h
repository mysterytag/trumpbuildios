﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/UnlinkSteamAccountRequestCallback
struct UnlinkSteamAccountRequestCallback_t1183838532;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.UnlinkSteamAccountRequest
struct UnlinkSteamAccountRequest_t775800815;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkSteamA775800815.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/UnlinkSteamAccountRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void UnlinkSteamAccountRequestCallback__ctor_m832288723 (UnlinkSteamAccountRequestCallback_t1183838532 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UnlinkSteamAccountRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.UnlinkSteamAccountRequest,System.Object)
extern "C"  void UnlinkSteamAccountRequestCallback_Invoke_m1951595985 (UnlinkSteamAccountRequestCallback_t1183838532 * __this, String_t* ___urlPath0, int32_t ___callId1, UnlinkSteamAccountRequest_t775800815 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_UnlinkSteamAccountRequestCallback_t1183838532(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, UnlinkSteamAccountRequest_t775800815 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/UnlinkSteamAccountRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.UnlinkSteamAccountRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnlinkSteamAccountRequestCallback_BeginInvoke_m3968657758 (UnlinkSteamAccountRequestCallback_t1183838532 * __this, String_t* ___urlPath0, int32_t ___callId1, UnlinkSteamAccountRequest_t775800815 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UnlinkSteamAccountRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void UnlinkSteamAccountRequestCallback_EndInvoke_m3629255523 (UnlinkSteamAccountRequestCallback_t1183838532 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
