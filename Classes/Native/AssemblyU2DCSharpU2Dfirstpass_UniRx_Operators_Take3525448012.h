﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t1509682273;
// System.Func`3<System.Object,System.Int32,System.Boolean>
struct Func_3_t3064567499;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4196218687.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.TakeWhileObservable`1<System.Object>
struct  TakeWhileObservable_1_t3525448012  : public OperatorObservableBase_1_t4196218687
{
public:
	// UniRx.IObservable`1<T> UniRx.Operators.TakeWhileObservable`1::source
	Il2CppObject* ___source_1;
	// System.Func`2<T,System.Boolean> UniRx.Operators.TakeWhileObservable`1::predicate
	Func_2_t1509682273 * ___predicate_2;
	// System.Func`3<T,System.Int32,System.Boolean> UniRx.Operators.TakeWhileObservable`1::predicateWithIndex
	Func_3_t3064567499 * ___predicateWithIndex_3;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(TakeWhileObservable_1_t3525448012, ___source_1)); }
	inline Il2CppObject* get_source_1() const { return ___source_1; }
	inline Il2CppObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(Il2CppObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier(&___source_1, value);
	}

	inline static int32_t get_offset_of_predicate_2() { return static_cast<int32_t>(offsetof(TakeWhileObservable_1_t3525448012, ___predicate_2)); }
	inline Func_2_t1509682273 * get_predicate_2() const { return ___predicate_2; }
	inline Func_2_t1509682273 ** get_address_of_predicate_2() { return &___predicate_2; }
	inline void set_predicate_2(Func_2_t1509682273 * value)
	{
		___predicate_2 = value;
		Il2CppCodeGenWriteBarrier(&___predicate_2, value);
	}

	inline static int32_t get_offset_of_predicateWithIndex_3() { return static_cast<int32_t>(offsetof(TakeWhileObservable_1_t3525448012, ___predicateWithIndex_3)); }
	inline Func_3_t3064567499 * get_predicateWithIndex_3() const { return ___predicateWithIndex_3; }
	inline Func_3_t3064567499 ** get_address_of_predicateWithIndex_3() { return &___predicateWithIndex_3; }
	inline void set_predicateWithIndex_3(Func_3_t3064567499 * value)
	{
		___predicateWithIndex_3 = value;
		Il2CppCodeGenWriteBarrier(&___predicateWithIndex_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
