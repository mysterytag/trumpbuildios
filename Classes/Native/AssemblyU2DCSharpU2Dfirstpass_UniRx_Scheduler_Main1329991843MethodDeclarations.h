﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.Scheduler/MainThreadScheduler/QueuedAction`1<System.Object>::.cctor()
extern "C"  void QueuedAction_1__cctor_m1522584373_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define QueuedAction_1__cctor_m1522584373(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))QueuedAction_1__cctor_m1522584373_gshared)(__this /* static, unused */, method)
// System.Void UniRx.Scheduler/MainThreadScheduler/QueuedAction`1<System.Object>::Invoke(System.Object)
extern "C"  void QueuedAction_1_Invoke_m2692532624_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___state0, const MethodInfo* method);
#define QueuedAction_1_Invoke_m2692532624(__this /* static, unused */, ___state0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))QueuedAction_1_Invoke_m2692532624_gshared)(__this /* static, unused */, ___state0, method)
