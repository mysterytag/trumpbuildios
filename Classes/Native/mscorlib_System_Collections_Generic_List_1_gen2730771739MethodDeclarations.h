﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1634065389MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::.ctor()
#define List_1__ctor_m665346888(__this, method) ((  void (*) (List_1_t2730771739 *, const MethodInfo*))List_1__ctor_m348833073_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m2832944183(__this, ___collection0, method) ((  void (*) (List_1_t2730771739 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1160795371_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::.ctor(System.Int32)
#define List_1__ctor_m3264441497(__this, ___capacity0, method) ((  void (*) (List_1_t2730771739 *, int32_t, const MethodInfo*))List_1__ctor_m2892763214_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::.cctor()
#define List_1__cctor_m2963788133(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3826137881_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2203312210(__this, method) ((  Il2CppObject* (*) (List_1_t2730771739 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2808422246_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m54827772(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2730771739 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m4034025648_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3400713355(__this, method) ((  Il2CppObject * (*) (List_1_t2730771739 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1841330603_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m788860690(__this, ___item0, method) ((  int32_t (*) (List_1_t2730771739 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3794749222_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m3420428782(__this, ___item0, method) ((  bool (*) (List_1_t2730771739 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2659633254_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m2820893802(__this, ___item0, method) ((  int32_t (*) (List_1_t2730771739 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3431692926_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m4173626333(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2730771739 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2067529129_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m822225451(__this, ___item0, method) ((  void (*) (List_1_t2730771739 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1644145887_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2084468847(__this, method) ((  bool (*) (List_1_t2730771739 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1299706087_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m4184690350(__this, method) ((  bool (*) (List_1_t2730771739 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3867536694_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m1187482208(__this, method) ((  Il2CppObject * (*) (List_1_t2730771739 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m4244374434_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m677696477(__this, method) ((  bool (*) (List_1_t2730771739 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m432946261_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m2692627324(__this, method) ((  bool (*) (List_1_t2730771739 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2961826820_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m4069680103(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t2730771739 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3985478825_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m4264371316(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2730771739 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3234554688_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::Add(T)
#define List_1_Add_m11605047(__this, ___item0, method) ((  void (*) (List_1_t2730771739 *, TradeInfo_t1933812770 *, const MethodInfo*))List_1_Add_m642669291_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m300876018(__this, ___newCount0, method) ((  void (*) (List_1_t2730771739 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m4122600870_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m3100577776(__this, ___collection0, method) ((  void (*) (List_1_t2730771739 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2478449828_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m2361550000(__this, ___enumerable0, method) ((  void (*) (List_1_t2730771739 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1739422052_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m4183710695(__this, ___collection0, method) ((  void (*) (List_1_t2730771739 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2229151411_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::AsReadOnly()
#define List_1_AsReadOnly_m2581587134(__this, method) ((  ReadOnlyCollection_1_t801990822 * (*) (List_1_t2730771739 *, const MethodInfo*))List_1_AsReadOnly_m769820182_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::Clear()
#define List_1_Clear_m2366447475(__this, method) ((  void (*) (List_1_t2730771739 *, const MethodInfo*))List_1_Clear_m454602559_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::Contains(T)
#define List_1_Contains_m3083005541(__this, ___item0, method) ((  bool (*) (List_1_t2730771739 *, TradeInfo_t1933812770 *, const MethodInfo*))List_1_Contains_m4186092781_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m3805609063(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2730771739 *, TradeInfoU5BU5D_t1777265943*, int32_t, const MethodInfo*))List_1_CopyTo_m3988356635_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::Find(System.Predicate`1<T>)
#define List_1_Find_m1896185087(__this, ___match0, method) ((  TradeInfo_t1933812770 * (*) (List_1_t2730771739 *, Predicate_1_t2504776668 *, const MethodInfo*))List_1_Find_m3379773421_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m248850012(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t2504776668 *, const MethodInfo*))List_1_CheckMatch_m3390394152_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m416407865(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t2730771739 *, int32_t, int32_t, Predicate_1_t2504776668 *, const MethodInfo*))List_1_GetIndex_m4275988045_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m3815962320(__this, ___action0, method) ((  void (*) (List_1_t2730771739 *, Action_1_t2082265475 *, const MethodInfo*))List_1_ForEach_m2030348444_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::GetEnumerator()
#define List_1_GetEnumerator_m3065410850(__this, method) ((  Enumerator_t816554731  (*) (List_1_t2730771739 *, const MethodInfo*))List_1_GetEnumerator_m1820263692_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::IndexOf(T)
#define List_1_IndexOf_m540860787(__this, ___item0, method) ((  int32_t (*) (List_1_t2730771739 *, TradeInfo_t1933812770 *, const MethodInfo*))List_1_IndexOf_m1752303327_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m1073314750(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t2730771739 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3807054194_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m3551976247(__this, ___index0, method) ((  void (*) (List_1_t2730771739 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3734723819_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::Insert(System.Int32,T)
#define List_1_Insert_m1451557022(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2730771739 *, int32_t, TradeInfo_t1933812770 *, const MethodInfo*))List_1_Insert_m3427163986_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m3769575059(__this, ___collection0, method) ((  void (*) (List_1_t2730771739 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2905071175_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::Remove(T)
#define List_1_Remove_m4248437664(__this, ___item0, method) ((  bool (*) (List_1_t2730771739 *, TradeInfo_t1933812770 *, const MethodInfo*))List_1_Remove_m2747911208_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m877707510(__this, ___match0, method) ((  int32_t (*) (List_1_t2730771739 *, Predicate_1_t2504776668 *, const MethodInfo*))List_1_RemoveAll_m2933443938_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m3620377188(__this, ___index0, method) ((  void (*) (List_1_t2730771739 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1301016856_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::Reverse()
#define List_1_Reverse_m3781010824(__this, method) ((  void (*) (List_1_t2730771739 *, const MethodInfo*))List_1_Reverse_m449081940_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::Sort()
#define List_1_Sort_m537577242(__this, method) ((  void (*) (List_1_t2730771739 *, const MethodInfo*))List_1_Sort_m1168641486_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m4016948234(__this, ___comparer0, method) ((  void (*) (List_1_t2730771739 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m3726677974_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m3711640557(__this, ___comparison0, method) ((  void (*) (List_1_t2730771739 *, Comparison_1_t342520350 *, const MethodInfo*))List_1_Sort_m4192185249_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::ToArray()
#define List_1_ToArray_m740548705(__this, method) ((  TradeInfoU5BU5D_t1777265943* (*) (List_1_t2730771739 *, const MethodInfo*))List_1_ToArray_m1046025517_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::TrimExcess()
#define List_1_TrimExcess_m2955586355(__this, method) ((  void (*) (List_1_t2730771739 *, const MethodInfo*))List_1_TrimExcess_m2451380967_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::get_Capacity()
#define List_1_get_Capacity_m3948472355(__this, method) ((  int32_t (*) (List_1_t2730771739 *, const MethodInfo*))List_1_get_Capacity_m543520655_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m1806032132(__this, ___value0, method) ((  void (*) (List_1_t2730771739 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1332789688_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::get_Count()
#define List_1_get_Count_m3699090024(__this, method) ((  int32_t (*) (List_1_t2730771739 *, const MethodInfo*))List_1_get_Count_m2599103100_gshared)(__this, method)
// T System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::get_Item(System.Int32)
#define List_1_get_Item_m3899431498(__this, ___index0, method) ((  TradeInfo_t1933812770 * (*) (List_1_t2730771739 *, int32_t, const MethodInfo*))List_1_get_Item_m2771401372_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<PlayFab.ClientModels.TradeInfo>::set_Item(System.Int32,T)
#define List_1_set_Item_m891523573(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2730771739 *, int32_t, TradeInfo_t1933812770 *, const MethodInfo*))List_1_set_Item_m1074271145_gshared)(__this, ___index0, ___value1, method)
