﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.LazyTask/<WhenAllCore>c__Iterator1F
struct U3CWhenAllCoreU3Ec__Iterator1F_t3942062194;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.LazyTask/<WhenAllCore>c__Iterator1F::.ctor()
extern "C"  void U3CWhenAllCoreU3Ec__Iterator1F__ctor_m2052034791 (U3CWhenAllCoreU3Ec__Iterator1F_t3942062194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UniRx.LazyTask/<WhenAllCore>c__Iterator1F::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWhenAllCoreU3Ec__Iterator1F_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2963570187 (U3CWhenAllCoreU3Ec__Iterator1F_t3942062194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UniRx.LazyTask/<WhenAllCore>c__Iterator1F::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWhenAllCoreU3Ec__Iterator1F_System_Collections_IEnumerator_get_Current_m217184159 (U3CWhenAllCoreU3Ec__Iterator1F_t3942062194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.LazyTask/<WhenAllCore>c__Iterator1F::MoveNext()
extern "C"  bool U3CWhenAllCoreU3Ec__Iterator1F_MoveNext_m3635524293 (U3CWhenAllCoreU3Ec__Iterator1F_t3942062194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.LazyTask/<WhenAllCore>c__Iterator1F::Dispose()
extern "C"  void U3CWhenAllCoreU3Ec__Iterator1F_Dispose_m513728356 (U3CWhenAllCoreU3Ec__Iterator1F_t3942062194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.LazyTask/<WhenAllCore>c__Iterator1F::Reset()
extern "C"  void U3CWhenAllCoreU3Ec__Iterator1F_Reset_m3993435028 (U3CWhenAllCoreU3Ec__Iterator1F_t3942062194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
