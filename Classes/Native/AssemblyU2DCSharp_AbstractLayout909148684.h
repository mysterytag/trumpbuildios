﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.Single>
struct List_1_t1755167990;
// System.Collections.Generic.List`1<UnityEngine.UI.LayoutElement>
struct List_1_t3452776276;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Range938821841.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AbstractLayout
struct  AbstractLayout_t909148684  : public MonoBehaviour_t3012272455
{
public:
	// System.Single AbstractLayout::scrollOffset
	float ___scrollOffset_2;
	// System.Single AbstractLayout::preferredHeight
	float ___preferredHeight_3;
	// System.Single AbstractLayout::spacing
	float ___spacing_4;
	// System.Single AbstractLayout::topPadding
	float ___topPadding_5;
	// System.Single AbstractLayout::combinedHeight
	float ___combinedHeight_6;
	// System.Boolean AbstractLayout::cellExpanded
	bool ___cellExpanded_7;
	// System.Collections.Generic.List`1<System.Single> AbstractLayout::expandedOffsets
	List_1_t1755167990 * ___expandedOffsets_8;
	// System.Collections.Generic.List`1<UnityEngine.UI.LayoutElement> AbstractLayout::elements
	List_1_t3452776276 * ___elements_9;
	// UnityEngine.SocialPlatforms.Range AbstractLayout::range
	Range_t938821841  ___range_10;

public:
	inline static int32_t get_offset_of_scrollOffset_2() { return static_cast<int32_t>(offsetof(AbstractLayout_t909148684, ___scrollOffset_2)); }
	inline float get_scrollOffset_2() const { return ___scrollOffset_2; }
	inline float* get_address_of_scrollOffset_2() { return &___scrollOffset_2; }
	inline void set_scrollOffset_2(float value)
	{
		___scrollOffset_2 = value;
	}

	inline static int32_t get_offset_of_preferredHeight_3() { return static_cast<int32_t>(offsetof(AbstractLayout_t909148684, ___preferredHeight_3)); }
	inline float get_preferredHeight_3() const { return ___preferredHeight_3; }
	inline float* get_address_of_preferredHeight_3() { return &___preferredHeight_3; }
	inline void set_preferredHeight_3(float value)
	{
		___preferredHeight_3 = value;
	}

	inline static int32_t get_offset_of_spacing_4() { return static_cast<int32_t>(offsetof(AbstractLayout_t909148684, ___spacing_4)); }
	inline float get_spacing_4() const { return ___spacing_4; }
	inline float* get_address_of_spacing_4() { return &___spacing_4; }
	inline void set_spacing_4(float value)
	{
		___spacing_4 = value;
	}

	inline static int32_t get_offset_of_topPadding_5() { return static_cast<int32_t>(offsetof(AbstractLayout_t909148684, ___topPadding_5)); }
	inline float get_topPadding_5() const { return ___topPadding_5; }
	inline float* get_address_of_topPadding_5() { return &___topPadding_5; }
	inline void set_topPadding_5(float value)
	{
		___topPadding_5 = value;
	}

	inline static int32_t get_offset_of_combinedHeight_6() { return static_cast<int32_t>(offsetof(AbstractLayout_t909148684, ___combinedHeight_6)); }
	inline float get_combinedHeight_6() const { return ___combinedHeight_6; }
	inline float* get_address_of_combinedHeight_6() { return &___combinedHeight_6; }
	inline void set_combinedHeight_6(float value)
	{
		___combinedHeight_6 = value;
	}

	inline static int32_t get_offset_of_cellExpanded_7() { return static_cast<int32_t>(offsetof(AbstractLayout_t909148684, ___cellExpanded_7)); }
	inline bool get_cellExpanded_7() const { return ___cellExpanded_7; }
	inline bool* get_address_of_cellExpanded_7() { return &___cellExpanded_7; }
	inline void set_cellExpanded_7(bool value)
	{
		___cellExpanded_7 = value;
	}

	inline static int32_t get_offset_of_expandedOffsets_8() { return static_cast<int32_t>(offsetof(AbstractLayout_t909148684, ___expandedOffsets_8)); }
	inline List_1_t1755167990 * get_expandedOffsets_8() const { return ___expandedOffsets_8; }
	inline List_1_t1755167990 ** get_address_of_expandedOffsets_8() { return &___expandedOffsets_8; }
	inline void set_expandedOffsets_8(List_1_t1755167990 * value)
	{
		___expandedOffsets_8 = value;
		Il2CppCodeGenWriteBarrier(&___expandedOffsets_8, value);
	}

	inline static int32_t get_offset_of_elements_9() { return static_cast<int32_t>(offsetof(AbstractLayout_t909148684, ___elements_9)); }
	inline List_1_t3452776276 * get_elements_9() const { return ___elements_9; }
	inline List_1_t3452776276 ** get_address_of_elements_9() { return &___elements_9; }
	inline void set_elements_9(List_1_t3452776276 * value)
	{
		___elements_9 = value;
		Il2CppCodeGenWriteBarrier(&___elements_9, value);
	}

	inline static int32_t get_offset_of_range_10() { return static_cast<int32_t>(offsetof(AbstractLayout_t909148684, ___range_10)); }
	inline Range_t938821841  get_range_10() const { return ___range_10; }
	inline Range_t938821841 * get_address_of_range_10() { return &___range_10; }
	inline void set_range_10(Range_t938821841  value)
	{
		___range_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
