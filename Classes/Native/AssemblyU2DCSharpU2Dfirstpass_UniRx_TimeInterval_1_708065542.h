﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;

#include "mscorlib_System_ValueType4014882752.h"
#include "mscorlib_System_TimeSpan763862892.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.TimeInterval`1<System.Object>
struct  TimeInterval_1_t708065542 
{
public:
	// System.TimeSpan UniRx.TimeInterval`1::_interval
	TimeSpan_t763862892  ____interval_0;
	// T UniRx.TimeInterval`1::_value
	Il2CppObject * ____value_1;

public:
	inline static int32_t get_offset_of__interval_0() { return static_cast<int32_t>(offsetof(TimeInterval_1_t708065542, ____interval_0)); }
	inline TimeSpan_t763862892  get__interval_0() const { return ____interval_0; }
	inline TimeSpan_t763862892 * get_address_of__interval_0() { return &____interval_0; }
	inline void set__interval_0(TimeSpan_t763862892  value)
	{
		____interval_0 = value;
	}

	inline static int32_t get_offset_of__value_1() { return static_cast<int32_t>(offsetof(TimeInterval_1_t708065542, ____value_1)); }
	inline Il2CppObject * get__value_1() const { return ____value_1; }
	inline Il2CppObject ** get_address_of__value_1() { return &____value_1; }
	inline void set__value_1(Il2CppObject * value)
	{
		____value_1 = value;
		Il2CppCodeGenWriteBarrier(&____value_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
