﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Rect>
struct EmptyObserver_1_t934781807;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_Rect1525428817.h"

// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Rect>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m392656715_gshared (EmptyObserver_1_t934781807 * __this, const MethodInfo* method);
#define EmptyObserver_1__ctor_m392656715(__this, method) ((  void (*) (EmptyObserver_1_t934781807 *, const MethodInfo*))EmptyObserver_1__ctor_m392656715_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Rect>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m3100327362_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EmptyObserver_1__cctor_m3100327362(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EmptyObserver_1__cctor_m3100327362_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Rect>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m2752562837_gshared (EmptyObserver_1_t934781807 * __this, const MethodInfo* method);
#define EmptyObserver_1_OnCompleted_m2752562837(__this, method) ((  void (*) (EmptyObserver_1_t934781807 *, const MethodInfo*))EmptyObserver_1_OnCompleted_m2752562837_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Rect>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m1376075842_gshared (EmptyObserver_1_t934781807 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define EmptyObserver_1_OnError_m1376075842(__this, ___error0, method) ((  void (*) (EmptyObserver_1_t934781807 *, Exception_t1967233988 *, const MethodInfo*))EmptyObserver_1_OnError_m1376075842_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Rect>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m2216684339_gshared (EmptyObserver_1_t934781807 * __this, Rect_t1525428817  ___value0, const MethodInfo* method);
#define EmptyObserver_1_OnNext_m2216684339(__this, ___value0, method) ((  void (*) (EmptyObserver_1_t934781807 *, Rect_t1525428817 , const MethodInfo*))EmptyObserver_1_OnNext_m2216684339_gshared)(__this, ___value0, method)
