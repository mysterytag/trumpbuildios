﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Reactor`1/<TransactionUnpackReact>c__AnonStorey141<UniRx.Unit>
struct U3CTransactionUnpackReactU3Ec__AnonStorey141_t591262053;

#include "codegen/il2cpp-codegen.h"

// System.Void Reactor`1/<TransactionUnpackReact>c__AnonStorey141<UniRx.Unit>::.ctor()
extern "C"  void U3CTransactionUnpackReactU3Ec__AnonStorey141__ctor_m1621850015_gshared (U3CTransactionUnpackReactU3Ec__AnonStorey141_t591262053 * __this, const MethodInfo* method);
#define U3CTransactionUnpackReactU3Ec__AnonStorey141__ctor_m1621850015(__this, method) ((  void (*) (U3CTransactionUnpackReactU3Ec__AnonStorey141_t591262053 *, const MethodInfo*))U3CTransactionUnpackReactU3Ec__AnonStorey141__ctor_m1621850015_gshared)(__this, method)
// System.Void Reactor`1/<TransactionUnpackReact>c__AnonStorey141<UniRx.Unit>::<>m__1D6()
extern "C"  void U3CTransactionUnpackReactU3Ec__AnonStorey141_U3CU3Em__1D6_m982088669_gshared (U3CTransactionUnpackReactU3Ec__AnonStorey141_t591262053 * __this, const MethodInfo* method);
#define U3CTransactionUnpackReactU3Ec__AnonStorey141_U3CU3Em__1D6_m982088669(__this, method) ((  void (*) (U3CTransactionUnpackReactU3Ec__AnonStorey141_t591262053 *, const MethodInfo*))U3CTransactionUnpackReactU3Ec__AnonStorey141_U3CU3Em__1D6_m982088669_gshared)(__this, method)
