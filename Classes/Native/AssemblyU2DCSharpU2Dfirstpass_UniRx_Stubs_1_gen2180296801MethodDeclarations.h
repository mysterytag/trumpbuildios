﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.Stubs`1<System.Object>::.cctor()
extern "C"  void Stubs_1__cctor_m4180273948_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Stubs_1__cctor_m4180273948(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Stubs_1__cctor_m4180273948_gshared)(__this /* static, unused */, method)
// System.Void UniRx.Stubs`1<System.Object>::<Ignore>m__71(T)
extern "C"  void Stubs_1_U3CIgnoreU3Em__71_m3494437912_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___t0, const MethodInfo* method);
#define Stubs_1_U3CIgnoreU3Em__71_m3494437912(__this /* static, unused */, ___t0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Stubs_1_U3CIgnoreU3Em__71_m3494437912_gshared)(__this /* static, unused */, ___t0, method)
// T UniRx.Stubs`1<System.Object>::<Identity>m__72(T)
extern "C"  Il2CppObject * Stubs_1_U3CIdentityU3Em__72_m4025510410_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___t0, const MethodInfo* method);
#define Stubs_1_U3CIdentityU3Em__72_m4025510410(__this /* static, unused */, ___t0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Stubs_1_U3CIdentityU3Em__72_m4025510410_gshared)(__this /* static, unused */, ___t0, method)
