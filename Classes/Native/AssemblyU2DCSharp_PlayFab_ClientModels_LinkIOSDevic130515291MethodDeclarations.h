﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.LinkIOSDeviceIDRequest
struct LinkIOSDeviceIDRequest_t130515291;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.LinkIOSDeviceIDRequest::.ctor()
extern "C"  void LinkIOSDeviceIDRequest__ctor_m742481346 (LinkIOSDeviceIDRequest_t130515291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LinkIOSDeviceIDRequest::get_DeviceId()
extern "C"  String_t* LinkIOSDeviceIDRequest_get_DeviceId_m535183513 (LinkIOSDeviceIDRequest_t130515291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LinkIOSDeviceIDRequest::set_DeviceId(System.String)
extern "C"  void LinkIOSDeviceIDRequest_set_DeviceId_m447830258 (LinkIOSDeviceIDRequest_t130515291 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LinkIOSDeviceIDRequest::get_OS()
extern "C"  String_t* LinkIOSDeviceIDRequest_get_OS_m989193420 (LinkIOSDeviceIDRequest_t130515291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LinkIOSDeviceIDRequest::set_OS(System.String)
extern "C"  void LinkIOSDeviceIDRequest_set_OS_m1695760991 (LinkIOSDeviceIDRequest_t130515291 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LinkIOSDeviceIDRequest::get_DeviceModel()
extern "C"  String_t* LinkIOSDeviceIDRequest_get_DeviceModel_m360704909 (LinkIOSDeviceIDRequest_t130515291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LinkIOSDeviceIDRequest::set_DeviceModel(System.String)
extern "C"  void LinkIOSDeviceIDRequest_set_DeviceModel_m527069708 (LinkIOSDeviceIDRequest_t130515291 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
