﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t58506160;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGa1895597947.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_TimeSpan763862892.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_SavedGa2752299644.h"

// GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder::WithUpdatedDescription(System.String)
extern "C"  Builder_t1895597947  Builder_WithUpdatedDescription_m100494325 (Builder_t1895597947 * __this, String_t* ___description0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder::WithUpdatedPngCoverImage(System.Byte[])
extern "C"  Builder_t1895597947  Builder_WithUpdatedPngCoverImage_m409300395 (Builder_t1895597947 * __this, ByteU5BU5D_t58506160* ___newPngCoverImage0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder::WithUpdatedPlayedTime(System.TimeSpan)
extern "C"  Builder_t1895597947  Builder_WithUpdatedPlayedTime_m3971838059 (Builder_t1895597947 * __this, TimeSpan_t763862892  ___newPlayedTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate GooglePlayGames.BasicApi.SavedGame.SavedGameMetadataUpdate/Builder::Build()
extern "C"  SavedGameMetadataUpdate_t2752299644  Builder_Build_m643199134 (Builder_t1895597947 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
