﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Stream`1/<Listen>c__AnonStorey128<System.Object>
struct U3CListenU3Ec__AnonStorey128_t68466444;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void Stream`1/<Listen>c__AnonStorey128<System.Object>::.ctor()
extern "C"  void U3CListenU3Ec__AnonStorey128__ctor_m4125250920_gshared (U3CListenU3Ec__AnonStorey128_t68466444 * __this, const MethodInfo* method);
#define U3CListenU3Ec__AnonStorey128__ctor_m4125250920(__this, method) ((  void (*) (U3CListenU3Ec__AnonStorey128_t68466444 *, const MethodInfo*))U3CListenU3Ec__AnonStorey128__ctor_m4125250920_gshared)(__this, method)
// System.Void Stream`1/<Listen>c__AnonStorey128<System.Object>::<>m__1B9(T)
extern "C"  void U3CListenU3Ec__AnonStorey128_U3CU3Em__1B9_m1444676741_gshared (U3CListenU3Ec__AnonStorey128_t68466444 * __this, Il2CppObject * ____0, const MethodInfo* method);
#define U3CListenU3Ec__AnonStorey128_U3CU3Em__1B9_m1444676741(__this, ____0, method) ((  void (*) (U3CListenU3Ec__AnonStorey128_t68466444 *, Il2CppObject *, const MethodInfo*))U3CListenU3Ec__AnonStorey128_U3CU3Em__1B9_m1444676741_gshared)(__this, ____0, method)
