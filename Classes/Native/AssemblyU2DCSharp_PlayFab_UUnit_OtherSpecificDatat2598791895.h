﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t190145395;
// System.Collections.Generic.Dictionary`2<System.String,System.UInt32>
struct Dictionary_2_t2623623230;
// System.Collections.Generic.Dictionary`2<System.String,PlayFab.UUnit.Region>
struct Dictionary_2_t432490094;
// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.UUnit.OtherSpecificDatatypes
struct  OtherSpecificDatatypes_t2598791895  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.String> PlayFab.UUnit.OtherSpecificDatatypes::<StringDict>k__BackingField
	Dictionary_2_t2606186806 * ___U3CStringDictU3Ek__BackingField_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> PlayFab.UUnit.OtherSpecificDatatypes::<IntDict>k__BackingField
	Dictionary_2_t190145395 * ___U3CIntDictU3Ek__BackingField_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.UInt32> PlayFab.UUnit.OtherSpecificDatatypes::<UintDict>k__BackingField
	Dictionary_2_t2623623230 * ___U3CUintDictU3Ek__BackingField_2;
	// System.Collections.Generic.Dictionary`2<System.String,PlayFab.UUnit.Region> PlayFab.UUnit.OtherSpecificDatatypes::<EnumDict>k__BackingField
	Dictionary_2_t432490094 * ___U3CEnumDictU3Ek__BackingField_3;
	// System.String PlayFab.UUnit.OtherSpecificDatatypes::<TestString>k__BackingField
	String_t* ___U3CTestStringU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CStringDictU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(OtherSpecificDatatypes_t2598791895, ___U3CStringDictU3Ek__BackingField_0)); }
	inline Dictionary_2_t2606186806 * get_U3CStringDictU3Ek__BackingField_0() const { return ___U3CStringDictU3Ek__BackingField_0; }
	inline Dictionary_2_t2606186806 ** get_address_of_U3CStringDictU3Ek__BackingField_0() { return &___U3CStringDictU3Ek__BackingField_0; }
	inline void set_U3CStringDictU3Ek__BackingField_0(Dictionary_2_t2606186806 * value)
	{
		___U3CStringDictU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CStringDictU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CIntDictU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(OtherSpecificDatatypes_t2598791895, ___U3CIntDictU3Ek__BackingField_1)); }
	inline Dictionary_2_t190145395 * get_U3CIntDictU3Ek__BackingField_1() const { return ___U3CIntDictU3Ek__BackingField_1; }
	inline Dictionary_2_t190145395 ** get_address_of_U3CIntDictU3Ek__BackingField_1() { return &___U3CIntDictU3Ek__BackingField_1; }
	inline void set_U3CIntDictU3Ek__BackingField_1(Dictionary_2_t190145395 * value)
	{
		___U3CIntDictU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CIntDictU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CUintDictU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(OtherSpecificDatatypes_t2598791895, ___U3CUintDictU3Ek__BackingField_2)); }
	inline Dictionary_2_t2623623230 * get_U3CUintDictU3Ek__BackingField_2() const { return ___U3CUintDictU3Ek__BackingField_2; }
	inline Dictionary_2_t2623623230 ** get_address_of_U3CUintDictU3Ek__BackingField_2() { return &___U3CUintDictU3Ek__BackingField_2; }
	inline void set_U3CUintDictU3Ek__BackingField_2(Dictionary_2_t2623623230 * value)
	{
		___U3CUintDictU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CUintDictU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CEnumDictU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(OtherSpecificDatatypes_t2598791895, ___U3CEnumDictU3Ek__BackingField_3)); }
	inline Dictionary_2_t432490094 * get_U3CEnumDictU3Ek__BackingField_3() const { return ___U3CEnumDictU3Ek__BackingField_3; }
	inline Dictionary_2_t432490094 ** get_address_of_U3CEnumDictU3Ek__BackingField_3() { return &___U3CEnumDictU3Ek__BackingField_3; }
	inline void set_U3CEnumDictU3Ek__BackingField_3(Dictionary_2_t432490094 * value)
	{
		___U3CEnumDictU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CEnumDictU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CTestStringU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(OtherSpecificDatatypes_t2598791895, ___U3CTestStringU3Ek__BackingField_4)); }
	inline String_t* get_U3CTestStringU3Ek__BackingField_4() const { return ___U3CTestStringU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CTestStringU3Ek__BackingField_4() { return &___U3CTestStringU3Ek__BackingField_4; }
	inline void set_U3CTestStringU3Ek__BackingField_4(String_t* value)
	{
		___U3CTestStringU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTestStringU3Ek__BackingField_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
