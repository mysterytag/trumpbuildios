﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.KeyedFactory`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct KeyedFactory_6_t605647996;
// System.Type[]
struct TypeU5BU5D_t3431720054;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void Zenject.KeyedFactory`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C"  void KeyedFactory_6__ctor_m1887853450_gshared (KeyedFactory_6_t605647996 * __this, const MethodInfo* method);
#define KeyedFactory_6__ctor_m1887853450(__this, method) ((  void (*) (KeyedFactory_6_t605647996 *, const MethodInfo*))KeyedFactory_6__ctor_m1887853450_gshared)(__this, method)
// System.Type[] Zenject.KeyedFactory`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_ProvidedTypes()
extern "C"  TypeU5BU5D_t3431720054* KeyedFactory_6_get_ProvidedTypes_m1143220621_gshared (KeyedFactory_6_t605647996 * __this, const MethodInfo* method);
#define KeyedFactory_6_get_ProvidedTypes_m1143220621(__this, method) ((  TypeU5BU5D_t3431720054* (*) (KeyedFactory_6_t605647996 *, const MethodInfo*))KeyedFactory_6_get_ProvidedTypes_m1143220621_gshared)(__this, method)
// TBase Zenject.KeyedFactory`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Create(TKey,TParam1,TParam2,TParam3,TParam4)
extern "C"  Il2CppObject * KeyedFactory_6_Create_m2275708103_gshared (KeyedFactory_6_t605647996 * __this, Il2CppObject * ___key0, Il2CppObject * ___param11, Il2CppObject * ___param22, Il2CppObject * ___param33, Il2CppObject * ___param44, const MethodInfo* method);
#define KeyedFactory_6_Create_m2275708103(__this, ___key0, ___param11, ___param22, ___param33, ___param44, method) ((  Il2CppObject * (*) (KeyedFactory_6_t605647996 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))KeyedFactory_6_Create_m2275708103_gshared)(__this, ___key0, ___param11, ___param22, ___param33, ___param44, method)
