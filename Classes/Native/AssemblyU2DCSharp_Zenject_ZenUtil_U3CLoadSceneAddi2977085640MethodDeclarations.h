﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.ZenUtil/<LoadSceneAdditiveWithContainer>c__Iterator4D
struct U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D_t2977085640;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Zenject.ZenUtil/<LoadSceneAdditiveWithContainer>c__Iterator4D::.ctor()
extern "C"  void U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D__ctor_m582760364 (U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D_t2977085640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Zenject.ZenUtil/<LoadSceneAdditiveWithContainer>c__Iterator4D::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m296853990 (U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D_t2977085640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Zenject.ZenUtil/<LoadSceneAdditiveWithContainer>c__Iterator4D::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D_System_Collections_IEnumerator_get_Current_m4131402106 (U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D_t2977085640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.ZenUtil/<LoadSceneAdditiveWithContainer>c__Iterator4D::MoveNext()
extern "C"  bool U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D_MoveNext_m3967568328 (U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D_t2977085640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.ZenUtil/<LoadSceneAdditiveWithContainer>c__Iterator4D::Dispose()
extern "C"  void U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D_Dispose_m1585244393 (U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D_t2977085640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.ZenUtil/<LoadSceneAdditiveWithContainer>c__Iterator4D::Reset()
extern "C"  void U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D_Reset_m2524160601 (U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D_t2977085640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
