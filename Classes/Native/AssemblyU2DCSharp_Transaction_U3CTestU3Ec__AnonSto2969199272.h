﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Cell`1<System.Int32>
struct Cell_1_t3029512419;
// System.IDisposable
struct IDisposable_t1628921374;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Transaction/<Test>c__AnonStorey14B
struct  U3CTestU3Ec__AnonStorey14B_t2969199272  : public Il2CppObject
{
public:
	// Cell`1<System.Int32> Transaction/<Test>c__AnonStorey14B::c1
	Cell_1_t3029512419 * ___c1_0;
	// System.IDisposable Transaction/<Test>c__AnonStorey14B::dispLow2
	Il2CppObject * ___dispLow2_1;
	// System.IDisposable Transaction/<Test>c__AnonStorey14B::dispMid1
	Il2CppObject * ___dispMid1_2;
	// Cell`1<System.Int32> Transaction/<Test>c__AnonStorey14B::c2
	Cell_1_t3029512419 * ___c2_3;
	// System.IDisposable Transaction/<Test>c__AnonStorey14B::disp1
	Il2CppObject * ___disp1_4;

public:
	inline static int32_t get_offset_of_c1_0() { return static_cast<int32_t>(offsetof(U3CTestU3Ec__AnonStorey14B_t2969199272, ___c1_0)); }
	inline Cell_1_t3029512419 * get_c1_0() const { return ___c1_0; }
	inline Cell_1_t3029512419 ** get_address_of_c1_0() { return &___c1_0; }
	inline void set_c1_0(Cell_1_t3029512419 * value)
	{
		___c1_0 = value;
		Il2CppCodeGenWriteBarrier(&___c1_0, value);
	}

	inline static int32_t get_offset_of_dispLow2_1() { return static_cast<int32_t>(offsetof(U3CTestU3Ec__AnonStorey14B_t2969199272, ___dispLow2_1)); }
	inline Il2CppObject * get_dispLow2_1() const { return ___dispLow2_1; }
	inline Il2CppObject ** get_address_of_dispLow2_1() { return &___dispLow2_1; }
	inline void set_dispLow2_1(Il2CppObject * value)
	{
		___dispLow2_1 = value;
		Il2CppCodeGenWriteBarrier(&___dispLow2_1, value);
	}

	inline static int32_t get_offset_of_dispMid1_2() { return static_cast<int32_t>(offsetof(U3CTestU3Ec__AnonStorey14B_t2969199272, ___dispMid1_2)); }
	inline Il2CppObject * get_dispMid1_2() const { return ___dispMid1_2; }
	inline Il2CppObject ** get_address_of_dispMid1_2() { return &___dispMid1_2; }
	inline void set_dispMid1_2(Il2CppObject * value)
	{
		___dispMid1_2 = value;
		Il2CppCodeGenWriteBarrier(&___dispMid1_2, value);
	}

	inline static int32_t get_offset_of_c2_3() { return static_cast<int32_t>(offsetof(U3CTestU3Ec__AnonStorey14B_t2969199272, ___c2_3)); }
	inline Cell_1_t3029512419 * get_c2_3() const { return ___c2_3; }
	inline Cell_1_t3029512419 ** get_address_of_c2_3() { return &___c2_3; }
	inline void set_c2_3(Cell_1_t3029512419 * value)
	{
		___c2_3 = value;
		Il2CppCodeGenWriteBarrier(&___c2_3, value);
	}

	inline static int32_t get_offset_of_disp1_4() { return static_cast<int32_t>(offsetof(U3CTestU3Ec__AnonStorey14B_t2969199272, ___disp1_4)); }
	inline Il2CppObject * get_disp1_4() const { return ___disp1_4; }
	inline Il2CppObject ** get_address_of_disp1_4() { return &___disp1_4; }
	inline void set_disp1_4(Il2CppObject * value)
	{
		___disp1_4 = value;
		Il2CppCodeGenWriteBarrier(&___disp1_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
