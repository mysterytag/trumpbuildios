﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CoroMutex/<Lock>c__AnonStorey148
struct U3CLockU3Ec__AnonStorey148_t2495481637;

#include "codegen/il2cpp-codegen.h"

// System.Void CoroMutex/<Lock>c__AnonStorey148::.ctor()
extern "C"  void U3CLockU3Ec__AnonStorey148__ctor_m1983798845 (U3CLockU3Ec__AnonStorey148_t2495481637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CoroMutex/<Lock>c__AnonStorey148::<>m__1E7()
extern "C"  void U3CLockU3Ec__AnonStorey148_U3CU3Em__1E7_m3431800991 (U3CLockU3Ec__AnonStorey148_t2495481637 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
