﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.AmbObservable`1/AmbOuterObserver/Amb<System.Object>
struct  Amb_t634706591  : public Il2CppObject
{
public:
	// UniRx.IObserver`1<T> UniRx.Operators.AmbObservable`1/AmbOuterObserver/Amb::targetObserver
	Il2CppObject* ___targetObserver_0;
	// System.IDisposable UniRx.Operators.AmbObservable`1/AmbOuterObserver/Amb::targetDisposable
	Il2CppObject * ___targetDisposable_1;

public:
	inline static int32_t get_offset_of_targetObserver_0() { return static_cast<int32_t>(offsetof(Amb_t634706591, ___targetObserver_0)); }
	inline Il2CppObject* get_targetObserver_0() const { return ___targetObserver_0; }
	inline Il2CppObject** get_address_of_targetObserver_0() { return &___targetObserver_0; }
	inline void set_targetObserver_0(Il2CppObject* value)
	{
		___targetObserver_0 = value;
		Il2CppCodeGenWriteBarrier(&___targetObserver_0, value);
	}

	inline static int32_t get_offset_of_targetDisposable_1() { return static_cast<int32_t>(offsetof(Amb_t634706591, ___targetDisposable_1)); }
	inline Il2CppObject * get_targetDisposable_1() const { return ___targetDisposable_1; }
	inline Il2CppObject ** get_address_of_targetDisposable_1() { return &___targetDisposable_1; }
	inline void set_targetDisposable_1(Il2CppObject * value)
	{
		___targetDisposable_1 = value;
		Il2CppCodeGenWriteBarrier(&___targetDisposable_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
