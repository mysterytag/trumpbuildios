﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>
struct List_1_t1166220788;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3546971076.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen369261819.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UniRx.Tuple`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2905175419_gshared (Enumerator_t3546971076 * __this, List_1_t1166220788 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m2905175419(__this, ___l0, method) ((  void (*) (Enumerator_t3546971076 *, List_1_t1166220788 *, const MethodInfo*))Enumerator__ctor_m2905175419_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4249542135_gshared (Enumerator_t3546971076 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m4249542135(__this, method) ((  void (*) (Enumerator_t3546971076 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m4249542135_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UniRx.Tuple`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2120211309_gshared (Enumerator_t3546971076 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2120211309(__this, method) ((  Il2CppObject * (*) (Enumerator_t3546971076 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2120211309_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UniRx.Tuple`2<System.Object,System.Object>>::Dispose()
extern "C"  void Enumerator_Dispose_m2961363488_gshared (Enumerator_t3546971076 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2961363488(__this, method) ((  void (*) (Enumerator_t3546971076 *, const MethodInfo*))Enumerator_Dispose_m2961363488_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UniRx.Tuple`2<System.Object,System.Object>>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2579530073_gshared (Enumerator_t3546971076 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m2579530073(__this, method) ((  void (*) (Enumerator_t3546971076 *, const MethodInfo*))Enumerator_VerifyState_m2579530073_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UniRx.Tuple`2<System.Object,System.Object>>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m603586983_gshared (Enumerator_t3546971076 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m603586983(__this, method) ((  bool (*) (Enumerator_t3546971076 *, const MethodInfo*))Enumerator_MoveNext_m603586983_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UniRx.Tuple`2<System.Object,System.Object>>::get_Current()
extern "C"  Tuple_2_t369261819  Enumerator_get_Current_m4286840242_gshared (Enumerator_t3546971076 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m4286840242(__this, method) ((  Tuple_2_t369261819  (*) (Enumerator_t3546971076 *, const MethodInfo*))Enumerator_get_Current_m4286840242_gshared)(__this, method)
