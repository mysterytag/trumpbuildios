﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ThrowObservable`1<UniRx.Unit>
struct ThrowObservable_1_t1098488778;
// System.Exception
struct Exception_t1967233988;
// UniRx.IScheduler
struct IScheduler_t2938318244;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.ThrowObservable`1<UniRx.Unit>::.ctor(System.Exception,UniRx.IScheduler)
extern "C"  void ThrowObservable_1__ctor_m155364606_gshared (ThrowObservable_1_t1098488778 * __this, Exception_t1967233988 * ___error0, Il2CppObject * ___scheduler1, const MethodInfo* method);
#define ThrowObservable_1__ctor_m155364606(__this, ___error0, ___scheduler1, method) ((  void (*) (ThrowObservable_1_t1098488778 *, Exception_t1967233988 *, Il2CppObject *, const MethodInfo*))ThrowObservable_1__ctor_m155364606_gshared)(__this, ___error0, ___scheduler1, method)
// System.IDisposable UniRx.Operators.ThrowObservable`1<UniRx.Unit>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * ThrowObservable_1_SubscribeCore_m3461710106_gshared (ThrowObservable_1_t1098488778 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define ThrowObservable_1_SubscribeCore_m3461710106(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (ThrowObservable_1_t1098488778 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))ThrowObservable_1_SubscribeCore_m3461710106_gshared)(__this, ___observer0, ___cancel1, method)
