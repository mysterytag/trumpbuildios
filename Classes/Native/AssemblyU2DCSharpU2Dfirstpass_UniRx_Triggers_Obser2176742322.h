﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Subject`1<UnityEngine.EventSystems.BaseEventData>
struct Subject_1_t1190171050;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Triggers_Obser1883237895.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableUpdateSelectedTrigger
struct  ObservableUpdateSelectedTrigger_t2176742322  : public ObservableTriggerBase_t1883237895
{
public:
	// UniRx.Subject`1<UnityEngine.EventSystems.BaseEventData> UniRx.Triggers.ObservableUpdateSelectedTrigger::onUpdateSelected
	Subject_1_t1190171050 * ___onUpdateSelected_8;

public:
	inline static int32_t get_offset_of_onUpdateSelected_8() { return static_cast<int32_t>(offsetof(ObservableUpdateSelectedTrigger_t2176742322, ___onUpdateSelected_8)); }
	inline Subject_1_t1190171050 * get_onUpdateSelected_8() const { return ___onUpdateSelected_8; }
	inline Subject_1_t1190171050 ** get_address_of_onUpdateSelected_8() { return &___onUpdateSelected_8; }
	inline void set_onUpdateSelected_8(Subject_1_t1190171050 * value)
	{
		___onUpdateSelected_8 = value;
		Il2CppCodeGenWriteBarrier(&___onUpdateSelected_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
