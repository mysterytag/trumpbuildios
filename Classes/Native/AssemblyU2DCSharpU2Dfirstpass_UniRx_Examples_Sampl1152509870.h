﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Examples.Sample09_EventHandling
struct Sample09_EventHandling_t2113606094;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Examples.Sample09_EventHandling/<Start>c__AnonStorey32
struct  U3CStartU3Ec__AnonStorey32_t1152509870  : public Il2CppObject
{
public:
	// System.Int32 UniRx.Examples.Sample09_EventHandling/<Start>c__AnonStorey32::capture
	int32_t ___capture_0;
	// UniRx.Examples.Sample09_EventHandling UniRx.Examples.Sample09_EventHandling/<Start>c__AnonStorey32::<>f__this
	Sample09_EventHandling_t2113606094 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_capture_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__AnonStorey32_t1152509870, ___capture_0)); }
	inline int32_t get_capture_0() const { return ___capture_0; }
	inline int32_t* get_address_of_capture_0() { return &___capture_0; }
	inline void set_capture_0(int32_t value)
	{
		___capture_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__AnonStorey32_t1152509870, ___U3CU3Ef__this_1)); }
	inline Sample09_EventHandling_t2113606094 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline Sample09_EventHandling_t2113606094 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(Sample09_EventHandling_t2113606094 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
