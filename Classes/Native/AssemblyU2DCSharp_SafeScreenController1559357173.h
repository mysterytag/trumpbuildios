﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GameController
struct GameController_t2782302542;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t2223784725;
// UnityEngine.Transform
struct Transform_t284553113;
// UnityEngine.TextMesh
struct TextMesh_t583678247;
// SafeBar
struct SafeBar_t3522663718;
// GlobalStat
struct GlobalStat_t1134678199;
// OligarchParameters
struct OligarchParameters_t821144443;

#include "AssemblyU2DCSharp_SceneController1039782184.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SafeScreenController
struct  SafeScreenController_t1559357173  : public SceneController_t1039782184
{
public:
	// GameController SafeScreenController::gameController
	GameController_t2782302542 * ___gameController_2;
	// UnityEngine.SpriteRenderer SafeScreenController::backRenderer
	SpriteRenderer_t2223784725 * ___backRenderer_3;
	// UnityEngine.Transform SafeScreenController::content
	Transform_t284553113 * ___content_4;
	// UnityEngine.TextMesh SafeScreenController::moneyText
	TextMesh_t583678247 * ___moneyText_5;
	// UnityEngine.TextMesh SafeScreenController::incomeText
	TextMesh_t583678247 * ___incomeText_6;
	// SafeBar SafeScreenController::safeBar
	SafeBar_t3522663718 * ___safeBar_7;
	// UnityEngine.SpriteRenderer SafeScreenController::safeSprite
	SpriteRenderer_t2223784725 * ___safeSprite_8;
	// System.Single SafeScreenController::updateTime
	float ___updateTime_9;
	// System.Double SafeScreenController::incomePerSecond
	double ___incomePerSecond_10;
	// GameController SafeScreenController::GameController
	GameController_t2782302542 * ___GameController_11;
	// GlobalStat SafeScreenController::GlobalStat
	GlobalStat_t1134678199 * ___GlobalStat_12;
	// OligarchParameters SafeScreenController::OligarchParameters
	OligarchParameters_t821144443 * ___OligarchParameters_13;

public:
	inline static int32_t get_offset_of_gameController_2() { return static_cast<int32_t>(offsetof(SafeScreenController_t1559357173, ___gameController_2)); }
	inline GameController_t2782302542 * get_gameController_2() const { return ___gameController_2; }
	inline GameController_t2782302542 ** get_address_of_gameController_2() { return &___gameController_2; }
	inline void set_gameController_2(GameController_t2782302542 * value)
	{
		___gameController_2 = value;
		Il2CppCodeGenWriteBarrier(&___gameController_2, value);
	}

	inline static int32_t get_offset_of_backRenderer_3() { return static_cast<int32_t>(offsetof(SafeScreenController_t1559357173, ___backRenderer_3)); }
	inline SpriteRenderer_t2223784725 * get_backRenderer_3() const { return ___backRenderer_3; }
	inline SpriteRenderer_t2223784725 ** get_address_of_backRenderer_3() { return &___backRenderer_3; }
	inline void set_backRenderer_3(SpriteRenderer_t2223784725 * value)
	{
		___backRenderer_3 = value;
		Il2CppCodeGenWriteBarrier(&___backRenderer_3, value);
	}

	inline static int32_t get_offset_of_content_4() { return static_cast<int32_t>(offsetof(SafeScreenController_t1559357173, ___content_4)); }
	inline Transform_t284553113 * get_content_4() const { return ___content_4; }
	inline Transform_t284553113 ** get_address_of_content_4() { return &___content_4; }
	inline void set_content_4(Transform_t284553113 * value)
	{
		___content_4 = value;
		Il2CppCodeGenWriteBarrier(&___content_4, value);
	}

	inline static int32_t get_offset_of_moneyText_5() { return static_cast<int32_t>(offsetof(SafeScreenController_t1559357173, ___moneyText_5)); }
	inline TextMesh_t583678247 * get_moneyText_5() const { return ___moneyText_5; }
	inline TextMesh_t583678247 ** get_address_of_moneyText_5() { return &___moneyText_5; }
	inline void set_moneyText_5(TextMesh_t583678247 * value)
	{
		___moneyText_5 = value;
		Il2CppCodeGenWriteBarrier(&___moneyText_5, value);
	}

	inline static int32_t get_offset_of_incomeText_6() { return static_cast<int32_t>(offsetof(SafeScreenController_t1559357173, ___incomeText_6)); }
	inline TextMesh_t583678247 * get_incomeText_6() const { return ___incomeText_6; }
	inline TextMesh_t583678247 ** get_address_of_incomeText_6() { return &___incomeText_6; }
	inline void set_incomeText_6(TextMesh_t583678247 * value)
	{
		___incomeText_6 = value;
		Il2CppCodeGenWriteBarrier(&___incomeText_6, value);
	}

	inline static int32_t get_offset_of_safeBar_7() { return static_cast<int32_t>(offsetof(SafeScreenController_t1559357173, ___safeBar_7)); }
	inline SafeBar_t3522663718 * get_safeBar_7() const { return ___safeBar_7; }
	inline SafeBar_t3522663718 ** get_address_of_safeBar_7() { return &___safeBar_7; }
	inline void set_safeBar_7(SafeBar_t3522663718 * value)
	{
		___safeBar_7 = value;
		Il2CppCodeGenWriteBarrier(&___safeBar_7, value);
	}

	inline static int32_t get_offset_of_safeSprite_8() { return static_cast<int32_t>(offsetof(SafeScreenController_t1559357173, ___safeSprite_8)); }
	inline SpriteRenderer_t2223784725 * get_safeSprite_8() const { return ___safeSprite_8; }
	inline SpriteRenderer_t2223784725 ** get_address_of_safeSprite_8() { return &___safeSprite_8; }
	inline void set_safeSprite_8(SpriteRenderer_t2223784725 * value)
	{
		___safeSprite_8 = value;
		Il2CppCodeGenWriteBarrier(&___safeSprite_8, value);
	}

	inline static int32_t get_offset_of_updateTime_9() { return static_cast<int32_t>(offsetof(SafeScreenController_t1559357173, ___updateTime_9)); }
	inline float get_updateTime_9() const { return ___updateTime_9; }
	inline float* get_address_of_updateTime_9() { return &___updateTime_9; }
	inline void set_updateTime_9(float value)
	{
		___updateTime_9 = value;
	}

	inline static int32_t get_offset_of_incomePerSecond_10() { return static_cast<int32_t>(offsetof(SafeScreenController_t1559357173, ___incomePerSecond_10)); }
	inline double get_incomePerSecond_10() const { return ___incomePerSecond_10; }
	inline double* get_address_of_incomePerSecond_10() { return &___incomePerSecond_10; }
	inline void set_incomePerSecond_10(double value)
	{
		___incomePerSecond_10 = value;
	}

	inline static int32_t get_offset_of_GameController_11() { return static_cast<int32_t>(offsetof(SafeScreenController_t1559357173, ___GameController_11)); }
	inline GameController_t2782302542 * get_GameController_11() const { return ___GameController_11; }
	inline GameController_t2782302542 ** get_address_of_GameController_11() { return &___GameController_11; }
	inline void set_GameController_11(GameController_t2782302542 * value)
	{
		___GameController_11 = value;
		Il2CppCodeGenWriteBarrier(&___GameController_11, value);
	}

	inline static int32_t get_offset_of_GlobalStat_12() { return static_cast<int32_t>(offsetof(SafeScreenController_t1559357173, ___GlobalStat_12)); }
	inline GlobalStat_t1134678199 * get_GlobalStat_12() const { return ___GlobalStat_12; }
	inline GlobalStat_t1134678199 ** get_address_of_GlobalStat_12() { return &___GlobalStat_12; }
	inline void set_GlobalStat_12(GlobalStat_t1134678199 * value)
	{
		___GlobalStat_12 = value;
		Il2CppCodeGenWriteBarrier(&___GlobalStat_12, value);
	}

	inline static int32_t get_offset_of_OligarchParameters_13() { return static_cast<int32_t>(offsetof(SafeScreenController_t1559357173, ___OligarchParameters_13)); }
	inline OligarchParameters_t821144443 * get_OligarchParameters_13() const { return ___OligarchParameters_13; }
	inline OligarchParameters_t821144443 ** get_address_of_OligarchParameters_13() { return &___OligarchParameters_13; }
	inline void set_OligarchParameters_13(OligarchParameters_t821144443 * value)
	{
		___OligarchParameters_13 = value;
		Il2CppCodeGenWriteBarrier(&___OligarchParameters_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
