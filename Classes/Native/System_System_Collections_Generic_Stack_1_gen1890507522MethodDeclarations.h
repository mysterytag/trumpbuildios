﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_gen1588395187MethodDeclarations.h"

// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.KeyValuePair`2<System.IDisposable,System.Action`1<System.Object>>>::.ctor()
#define Stack_1__ctor_m1559106341(__this, method) ((  void (*) (Stack_1_t1890507522 *, const MethodInfo*))Stack_1__ctor_m3329604301_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.KeyValuePair`2<System.IDisposable,System.Action`1<System.Object>>>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define Stack_1__ctor_m2627023930(__this, ___collection0, method) ((  void (*) (Stack_1_t1890507522 *, Il2CppObject*, const MethodInfo*))Stack_1__ctor_m1217418130_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.Stack`1<System.Collections.Generic.KeyValuePair`2<System.IDisposable,System.Action`1<System.Object>>>::System.Collections.ICollection.get_IsSynchronized()
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m4076558003(__this, method) ((  bool (*) (Stack_1_t1890507522 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_IsSynchronized_m888626651_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<System.Collections.Generic.KeyValuePair`2<System.IDisposable,System.Action`1<System.Object>>>::System.Collections.ICollection.get_SyncRoot()
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m782347891(__this, method) ((  Il2CppObject * (*) (Stack_1_t1890507522 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m613022299_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.KeyValuePair`2<System.IDisposable,System.Action`1<System.Object>>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Stack_1_System_Collections_ICollection_CopyTo_m1696252351(__this, ___dest0, ___idx1, method) ((  void (*) (Stack_1_t1890507522 *, Il2CppArray *, int32_t, const MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m1931486999_gshared)(__this, ___dest0, ___idx1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<System.Collections.Generic.KeyValuePair`2<System.IDisposable,System.Action`1<System.Object>>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2218633527(__this, method) ((  Il2CppObject* (*) (Stack_1_t1890507522 *, const MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m50117087_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<System.Collections.Generic.KeyValuePair`2<System.IDisposable,System.Action`1<System.Object>>>::System.Collections.IEnumerable.GetEnumerator()
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m1868364302(__this, method) ((  Il2CppObject * (*) (Stack_1_t1890507522 *, const MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m1044611302_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.KeyValuePair`2<System.IDisposable,System.Action`1<System.Object>>>::Clear()
#define Stack_1_Clear_m3260206928(__this, method) ((  void (*) (Stack_1_t1890507522 *, const MethodInfo*))Stack_1_Clear_m735737592_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<System.Collections.Generic.KeyValuePair`2<System.IDisposable,System.Action`1<System.Object>>>::Contains(T)
#define Stack_1_Contains_m3824492394(__this, ___t0, method) ((  bool (*) (Stack_1_t1890507522 *, KeyValuePair_2_t3615068783 , const MethodInfo*))Stack_1_Contains_m799370898_gshared)(__this, ___t0, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.KeyValuePair`2<System.IDisposable,System.Action`1<System.Object>>>::Peek()
#define Stack_1_Peek_m772829017(__this, method) ((  KeyValuePair_2_t3615068783  (*) (Stack_1_t1890507522 *, const MethodInfo*))Stack_1_Peek_m1591094257_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.KeyValuePair`2<System.IDisposable,System.Action`1<System.Object>>>::Pop()
#define Stack_1_Pop_m1133615029(__this, method) ((  KeyValuePair_2_t3615068783  (*) (Stack_1_t1890507522 *, const MethodInfo*))Stack_1_Pop_m190179357_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.KeyValuePair`2<System.IDisposable,System.Action`1<System.Object>>>::Push(T)
#define Stack_1_Push_m2183583333(__this, ___t0, method) ((  void (*) (Stack_1_t1890507522 *, KeyValuePair_2_t3615068783 , const MethodInfo*))Stack_1_Push_m3954081293_gshared)(__this, ___t0, method)
// System.Int32 System.Collections.Generic.Stack`1<System.Collections.Generic.KeyValuePair`2<System.IDisposable,System.Action`1<System.Object>>>::get_Count()
#define Stack_1_get_Count_m2148349561(__this, method) ((  int32_t (*) (Stack_1_t1890507522 *, const MethodInfo*))Stack_1_get_Count_m3785819681_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<System.Collections.Generic.KeyValuePair`2<System.IDisposable,System.Action`1<System.Object>>>::GetEnumerator()
#define Stack_1_GetEnumerator_m2801173531(__this, method) ((  Enumerator_t2497810744  (*) (Stack_1_t1890507522 *, const MethodInfo*))Stack_1_GetEnumerator_m530402883_gshared)(__this, method)
