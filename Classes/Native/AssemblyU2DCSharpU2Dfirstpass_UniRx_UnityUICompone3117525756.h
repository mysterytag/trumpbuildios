﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Slider
struct Slider_t1468074762;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.UnityUIComponentExtensions/<OnValueChangedAsObservable>c__AnonStoreyAA
struct  U3COnValueChangedAsObservableU3Ec__AnonStoreyAA_t3117525756  : public Il2CppObject
{
public:
	// UnityEngine.UI.Slider UniRx.UnityUIComponentExtensions/<OnValueChangedAsObservable>c__AnonStoreyAA::slider
	Slider_t1468074762 * ___slider_0;

public:
	inline static int32_t get_offset_of_slider_0() { return static_cast<int32_t>(offsetof(U3COnValueChangedAsObservableU3Ec__AnonStoreyAA_t3117525756, ___slider_0)); }
	inline Slider_t1468074762 * get_slider_0() const { return ___slider_0; }
	inline Slider_t1468074762 ** get_address_of_slider_0() { return &___slider_0; }
	inline void set_slider_0(Slider_t1468074762 * value)
	{
		___slider_0 = value;
		Il2CppCodeGenWriteBarrier(&___slider_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
