﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.CollectionAddEvent`1<System.Object>>
struct ReadOnlyCollection_1_t1284700039;
// System.Collections.Generic.IList`1<UniRx.CollectionAddEvent`1<System.Object>>
struct IList_1_t288047005;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// UniRx.CollectionAddEvent`1<System.Object>[]
struct CollectionAddEvent_1U5BU5D_t789875090;
// System.Collections.Generic.IEnumerator`1<UniRx.CollectionAddEvent`1<System.Object>>
struct IEnumerator_1_t3899628435;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionAddE2416521987.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.CollectionAddEvent`1<System.Object>>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m65730228_gshared (ReadOnlyCollection_1_t1284700039 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m65730228(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t1284700039 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m65730228_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2303504030_gshared (ReadOnlyCollection_1_t1284700039 * __this, CollectionAddEvent_1_t2416521987  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2303504030(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t1284700039 *, CollectionAddEvent_1_t2416521987 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2303504030_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m400871916_gshared (ReadOnlyCollection_1_t1284700039 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m400871916(__this, method) ((  void (*) (ReadOnlyCollection_1_t1284700039 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m400871916_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1382941701_gshared (ReadOnlyCollection_1_t1284700039 * __this, int32_t ___index0, CollectionAddEvent_1_t2416521987  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1382941701(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t1284700039 *, int32_t, CollectionAddEvent_1_t2416521987 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1382941701_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2672766357_gshared (ReadOnlyCollection_1_t1284700039 * __this, CollectionAddEvent_1_t2416521987  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2672766357(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t1284700039 *, CollectionAddEvent_1_t2416521987 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2672766357_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3551761867_gshared (ReadOnlyCollection_1_t1284700039 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3551761867(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1284700039 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3551761867_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  CollectionAddEvent_1_t2416521987  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m716346767_gshared (ReadOnlyCollection_1_t1284700039 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m716346767(__this, ___index0, method) ((  CollectionAddEvent_1_t2416521987  (*) (ReadOnlyCollection_1_t1284700039 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m716346767_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3671676828_gshared (ReadOnlyCollection_1_t1284700039 * __this, int32_t ___index0, CollectionAddEvent_1_t2416521987  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3671676828(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1284700039 *, int32_t, CollectionAddEvent_1_t2416521987 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3671676828_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3518697050_gshared (ReadOnlyCollection_1_t1284700039 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3518697050(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1284700039 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3518697050_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3735185251_gshared (ReadOnlyCollection_1_t1284700039 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3735185251(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1284700039 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3735185251_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3748546718_gshared (ReadOnlyCollection_1_t1284700039 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3748546718(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1284700039 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3748546718_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m3555803027_gshared (ReadOnlyCollection_1_t1284700039 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m3555803027(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1284700039 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m3555803027_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m2486026865_gshared (ReadOnlyCollection_1_t1284700039 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m2486026865(__this, method) ((  void (*) (ReadOnlyCollection_1_t1284700039 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m2486026865_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m980912537_gshared (ReadOnlyCollection_1_t1284700039 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m980912537(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1284700039 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m980912537_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2727441515_gshared (ReadOnlyCollection_1_t1284700039 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2727441515(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1284700039 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2727441515_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m1731480342_gshared (ReadOnlyCollection_1_t1284700039 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m1731480342(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1284700039 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m1731480342_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m2136281682_gshared (ReadOnlyCollection_1_t1284700039 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m2136281682(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t1284700039 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m2136281682_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3385578406_gshared (ReadOnlyCollection_1_t1284700039 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3385578406(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1284700039 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3385578406_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3697851171_gshared (ReadOnlyCollection_1_t1284700039 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3697851171(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1284700039 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3697851171_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1733705871_gshared (ReadOnlyCollection_1_t1284700039 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1733705871(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1284700039 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1733705871_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m619225032_gshared (ReadOnlyCollection_1_t1284700039 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m619225032(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1284700039 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m619225032_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3383477809_gshared (ReadOnlyCollection_1_t1284700039 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3383477809(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1284700039 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3383477809_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m709642838_gshared (ReadOnlyCollection_1_t1284700039 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m709642838(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1284700039 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m709642838_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m2414217581_gshared (ReadOnlyCollection_1_t1284700039 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m2414217581(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1284700039 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m2414217581_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.CollectionAddEvent`1<System.Object>>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m2222175194_gshared (ReadOnlyCollection_1_t1284700039 * __this, CollectionAddEvent_1_t2416521987  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m2222175194(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1284700039 *, CollectionAddEvent_1_t2416521987 , const MethodInfo*))ReadOnlyCollection_1_Contains_m2222175194_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.CollectionAddEvent`1<System.Object>>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m1466727374_gshared (ReadOnlyCollection_1_t1284700039 * __this, CollectionAddEvent_1U5BU5D_t789875090* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m1466727374(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1284700039 *, CollectionAddEvent_1U5BU5D_t789875090*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m1466727374_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.CollectionAddEvent`1<System.Object>>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m2154267325_gshared (ReadOnlyCollection_1_t1284700039 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m2154267325(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t1284700039 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m2154267325_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.CollectionAddEvent`1<System.Object>>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m4014299346_gshared (ReadOnlyCollection_1_t1284700039 * __this, CollectionAddEvent_1_t2416521987  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m4014299346(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1284700039 *, CollectionAddEvent_1_t2416521987 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m4014299346_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.CollectionAddEvent`1<System.Object>>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m4001502953_gshared (ReadOnlyCollection_1_t1284700039 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m4001502953(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t1284700039 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m4001502953_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UniRx.CollectionAddEvent`1<System.Object>>::get_Item(System.Int32)
extern "C"  CollectionAddEvent_1_t2416521987  ReadOnlyCollection_1_get_Item_m3037483727_gshared (ReadOnlyCollection_1_t1284700039 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m3037483727(__this, ___index0, method) ((  CollectionAddEvent_1_t2416521987  (*) (ReadOnlyCollection_1_t1284700039 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m3037483727_gshared)(__this, ___index0, method)
