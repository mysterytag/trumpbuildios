﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.SkipWhileObservable`1<System.Object>
struct SkipWhileObservable_1_t2542074388;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.SkipWhileObservable`1/SkipWhile<System.Object>
struct  SkipWhile_t3761473147  : public OperatorObserverBase_2_t1187768149
{
public:
	// UniRx.Operators.SkipWhileObservable`1<T> UniRx.Operators.SkipWhileObservable`1/SkipWhile::parent
	SkipWhileObservable_1_t2542074388 * ___parent_2;
	// System.Boolean UniRx.Operators.SkipWhileObservable`1/SkipWhile::endSkip
	bool ___endSkip_3;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(SkipWhile_t3761473147, ___parent_2)); }
	inline SkipWhileObservable_1_t2542074388 * get_parent_2() const { return ___parent_2; }
	inline SkipWhileObservable_1_t2542074388 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(SkipWhileObservable_1_t2542074388 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_endSkip_3() { return static_cast<int32_t>(offsetof(SkipWhile_t3761473147, ___endSkip_3)); }
	inline bool get_endSkip_3() const { return ___endSkip_3; }
	inline bool* get_address_of_endSkip_3() { return &___endSkip_3; }
	inline void set_endSkip_3(bool value)
	{
		___endSkip_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
