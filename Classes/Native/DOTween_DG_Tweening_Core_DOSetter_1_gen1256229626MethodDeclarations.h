﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>
struct DOSetter_1_t1256229626;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "UnityEngine_UnityEngine_Rect1525428817.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m3557167822_gshared (DOSetter_1_t1256229626 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define DOSetter_1__ctor_m3557167822(__this, ___object0, ___method1, method) ((  void (*) (DOSetter_1_t1256229626 *, Il2CppObject *, IntPtr_t, const MethodInfo*))DOSetter_1__ctor_m3557167822_gshared)(__this, ___object0, ___method1, method)
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m969078198_gshared (DOSetter_1_t1256229626 * __this, Rect_t1525428817  ___pNewValue0, const MethodInfo* method);
#define DOSetter_1_Invoke_m969078198(__this, ___pNewValue0, method) ((  void (*) (DOSetter_1_t1256229626 *, Rect_t1525428817 , const MethodInfo*))DOSetter_1_Invoke_m969078198_gshared)(__this, ___pNewValue0, method)
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m3970304011_gshared (DOSetter_1_t1256229626 * __this, Rect_t1525428817  ___pNewValue0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method);
#define DOSetter_1_BeginInvoke_m3970304011(__this, ___pNewValue0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (DOSetter_1_t1256229626 *, Rect_t1525428817 , AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))DOSetter_1_BeginInvoke_m3970304011_gshared)(__this, ___pNewValue0, ___callback1, ___object2, method)
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m2969508830_gshared (DOSetter_1_t1256229626 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define DOSetter_1_EndInvoke_m2969508830(__this, ___result0, method) ((  void (*) (DOSetter_1_t1256229626 *, Il2CppObject *, const MethodInfo*))DOSetter_1_EndInvoke_m2969508830_gshared)(__this, ___result0, method)
