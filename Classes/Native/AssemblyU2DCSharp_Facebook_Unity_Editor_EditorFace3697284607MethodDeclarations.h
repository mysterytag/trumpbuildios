﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.Editor.EditorFacebookMockDialog
struct EditorFacebookMockDialog_t3697284607;
// Facebook.Unity.Utilities/Callback`1<Facebook.Unity.ResultContainer>
struct Callback_1_t1746511022;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void Facebook.Unity.Editor.EditorFacebookMockDialog::.ctor()
extern "C"  void EditorFacebookMockDialog__ctor_m3957943638 (EditorFacebookMockDialog_t3697284607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Facebook.Unity.Utilities/Callback`1<Facebook.Unity.ResultContainer> Facebook.Unity.Editor.EditorFacebookMockDialog::get_Callback()
extern "C"  Callback_1_t1746511022 * EditorFacebookMockDialog_get_Callback_m1063601588 (EditorFacebookMockDialog_t3697284607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorFacebookMockDialog::set_Callback(Facebook.Unity.Utilities/Callback`1<Facebook.Unity.ResultContainer>)
extern "C"  void EditorFacebookMockDialog_set_Callback_m2154233127 (EditorFacebookMockDialog_t3697284607 * __this, Callback_1_t1746511022 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.Editor.EditorFacebookMockDialog::get_CallbackID()
extern "C"  String_t* EditorFacebookMockDialog_get_CallbackID_m1158444410 (EditorFacebookMockDialog_t3697284607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorFacebookMockDialog::set_CallbackID(System.String)
extern "C"  void EditorFacebookMockDialog_set_CallbackID_m1554343703 (EditorFacebookMockDialog_t3697284607 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorFacebookMockDialog::Start()
extern "C"  void EditorFacebookMockDialog_Start_m2905081430 (EditorFacebookMockDialog_t3697284607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorFacebookMockDialog::OnGUI()
extern "C"  void EditorFacebookMockDialog_OnGUI_m3453342288 (EditorFacebookMockDialog_t3697284607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorFacebookMockDialog::SendCancelResult()
extern "C"  void EditorFacebookMockDialog_SendCancelResult_m4271168717 (EditorFacebookMockDialog_t3697284607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorFacebookMockDialog::SendErrorResult(System.String)
extern "C"  void EditorFacebookMockDialog_SendErrorResult_m600294289 (EditorFacebookMockDialog_t3697284607 * __this, String_t* ___errorMessage0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.Editor.EditorFacebookMockDialog::OnGUIDialog(System.Int32)
extern "C"  void EditorFacebookMockDialog_OnGUIDialog_m3103280073 (EditorFacebookMockDialog_t3697284607 * __this, int32_t ___windowId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
