﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.UUnit.UUnitAssertException
struct UUnitAssertException_t1600770266;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Object837106420.h"

// System.Void PlayFab.UUnit.UUnitAssertException::.ctor(System.String)
extern "C"  void UUnitAssertException__ctor_m386870343 (UUnitAssertException_t1600770266 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.UUnitAssertException::.ctor(System.Object,System.Object,System.String)
extern "C"  void UUnitAssertException__ctor_m161051107 (UUnitAssertException_t1600770266 * __this, Il2CppObject * ___expected0, Il2CppObject * ___received1, String_t* ___message2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.UUnitAssertException::.ctor(System.Object,System.Object)
extern "C"  void UUnitAssertException__ctor_m3529452007 (UUnitAssertException_t1600770266 * __this, Il2CppObject * ___expected0, Il2CppObject * ___received1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
