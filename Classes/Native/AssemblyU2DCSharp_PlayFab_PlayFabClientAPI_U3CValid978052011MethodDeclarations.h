﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<ValidateAmazonIAPReceipt>c__AnonStoreyD0
struct U3CValidateAmazonIAPReceiptU3Ec__AnonStoreyD0_t978052011;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<ValidateAmazonIAPReceipt>c__AnonStoreyD0::.ctor()
extern "C"  void U3CValidateAmazonIAPReceiptU3Ec__AnonStoreyD0__ctor_m1156196456 (U3CValidateAmazonIAPReceiptU3Ec__AnonStoreyD0_t978052011 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<ValidateAmazonIAPReceipt>c__AnonStoreyD0::<>m__BD(PlayFab.CallRequestContainer)
extern "C"  void U3CValidateAmazonIAPReceiptU3Ec__AnonStoreyD0_U3CU3Em__BD_m3972413960 (U3CValidateAmazonIAPReceiptU3Ec__AnonStoreyD0_t978052011 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
