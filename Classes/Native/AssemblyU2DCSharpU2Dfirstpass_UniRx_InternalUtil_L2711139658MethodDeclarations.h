﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.ListObserver`1<UniRx.CollectionAddEvent`1<System.Object>>
struct ListObserver_1_t2711139658;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UniRx.CollectionAddEvent`1<System.Object>>>
struct ImmutableList_1_t1393758093;
// System.Exception
struct Exception_t1967233988;
// UniRx.IObserver`1<UniRx.CollectionAddEvent`1<System.Object>>
struct IObserver_1_t333553594;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionAddE2416521987.h"

// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionAddEvent`1<System.Object>>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m3351957823_gshared (ListObserver_1_t2711139658 * __this, ImmutableList_1_t1393758093 * ___observers0, const MethodInfo* method);
#define ListObserver_1__ctor_m3351957823(__this, ___observers0, method) ((  void (*) (ListObserver_1_t2711139658 *, ImmutableList_1_t1393758093 *, const MethodInfo*))ListObserver_1__ctor_m3351957823_gshared)(__this, ___observers0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionAddEvent`1<System.Object>>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m3994638647_gshared (ListObserver_1_t2711139658 * __this, const MethodInfo* method);
#define ListObserver_1_OnCompleted_m3994638647(__this, method) ((  void (*) (ListObserver_1_t2711139658 *, const MethodInfo*))ListObserver_1_OnCompleted_m3994638647_gshared)(__this, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionAddEvent`1<System.Object>>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m2024599524_gshared (ListObserver_1_t2711139658 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ListObserver_1_OnError_m2024599524(__this, ___error0, method) ((  void (*) (ListObserver_1_t2711139658 *, Exception_t1967233988 *, const MethodInfo*))ListObserver_1_OnError_m2024599524_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionAddEvent`1<System.Object>>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m2249137877_gshared (ListObserver_1_t2711139658 * __this, CollectionAddEvent_1_t2416521987  ___value0, const MethodInfo* method);
#define ListObserver_1_OnNext_m2249137877(__this, ___value0, method) ((  void (*) (ListObserver_1_t2711139658 *, CollectionAddEvent_1_t2416521987 , const MethodInfo*))ListObserver_1_OnNext_m2249137877_gshared)(__this, ___value0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.CollectionAddEvent`1<System.Object>>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m1401659111_gshared (ListObserver_1_t2711139658 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Add_m1401659111(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t2711139658 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Add_m1401659111_gshared)(__this, ___observer0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.CollectionAddEvent`1<System.Object>>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m1587969796_gshared (ListObserver_1_t2711139658 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Remove_m1587969796(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t2711139658 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Remove_m1587969796_gshared)(__this, ___observer0, method)
