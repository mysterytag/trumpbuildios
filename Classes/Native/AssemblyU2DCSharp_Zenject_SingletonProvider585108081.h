﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Zenject.SingletonLazyCreator
struct SingletonLazyCreator_t2762284194;
// Zenject.DiContainer
struct DiContainer_t2383114449;

#include "AssemblyU2DCSharp_Zenject_ProviderBase1627494391.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SingletonProvider
struct  SingletonProvider_t585108081  : public ProviderBase_t1627494391
{
public:
	// Zenject.SingletonLazyCreator Zenject.SingletonProvider::_creator
	SingletonLazyCreator_t2762284194 * ____creator_2;
	// Zenject.DiContainer Zenject.SingletonProvider::_container
	DiContainer_t2383114449 * ____container_3;

public:
	inline static int32_t get_offset_of__creator_2() { return static_cast<int32_t>(offsetof(SingletonProvider_t585108081, ____creator_2)); }
	inline SingletonLazyCreator_t2762284194 * get__creator_2() const { return ____creator_2; }
	inline SingletonLazyCreator_t2762284194 ** get_address_of__creator_2() { return &____creator_2; }
	inline void set__creator_2(SingletonLazyCreator_t2762284194 * value)
	{
		____creator_2 = value;
		Il2CppCodeGenWriteBarrier(&____creator_2, value);
	}

	inline static int32_t get_offset_of__container_3() { return static_cast<int32_t>(offsetof(SingletonProvider_t585108081, ____container_3)); }
	inline DiContainer_t2383114449 * get__container_3() const { return ____container_3; }
	inline DiContainer_t2383114449 ** get_address_of__container_3() { return &____container_3; }
	inline void set__container_3(DiContainer_t2383114449 * value)
	{
		____container_3 = value;
		Il2CppCodeGenWriteBarrier(&____container_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
