﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// IInstantiationPoolBase
struct IInstantiationPoolBase_t2184937649;
// System.String
struct String_t;
// ConnectionCollector
struct ConnectionCollector_t444796719;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InstantiatableInPool
struct  InstantiatableInPool_t1882737686  : public MonoBehaviour_t3012272455
{
public:
	// IInstantiationPoolBase InstantiatableInPool::pool
	Il2CppObject * ___pool_2;
	// System.String InstantiatableInPool::__prefabName
	String_t* _____prefabName_3;
	// ConnectionCollector InstantiatableInPool::connections
	ConnectionCollector_t444796719 * ___connections_4;

public:
	inline static int32_t get_offset_of_pool_2() { return static_cast<int32_t>(offsetof(InstantiatableInPool_t1882737686, ___pool_2)); }
	inline Il2CppObject * get_pool_2() const { return ___pool_2; }
	inline Il2CppObject ** get_address_of_pool_2() { return &___pool_2; }
	inline void set_pool_2(Il2CppObject * value)
	{
		___pool_2 = value;
		Il2CppCodeGenWriteBarrier(&___pool_2, value);
	}

	inline static int32_t get_offset_of___prefabName_3() { return static_cast<int32_t>(offsetof(InstantiatableInPool_t1882737686, _____prefabName_3)); }
	inline String_t* get___prefabName_3() const { return _____prefabName_3; }
	inline String_t** get_address_of___prefabName_3() { return &_____prefabName_3; }
	inline void set___prefabName_3(String_t* value)
	{
		_____prefabName_3 = value;
		Il2CppCodeGenWriteBarrier(&_____prefabName_3, value);
	}

	inline static int32_t get_offset_of_connections_4() { return static_cast<int32_t>(offsetof(InstantiatableInPool_t1882737686, ___connections_4)); }
	inline ConnectionCollector_t444796719 * get_connections_4() const { return ___connections_4; }
	inline ConnectionCollector_t444796719 ** get_address_of_connections_4() { return &___connections_4; }
	inline void set_connections_4(ConnectionCollector_t444796719 * value)
	{
		___connections_4 = value;
		Il2CppCodeGenWriteBarrier(&___connections_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
