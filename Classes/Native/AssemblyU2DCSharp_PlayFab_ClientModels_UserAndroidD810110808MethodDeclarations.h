﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.UserAndroidDeviceInfo
struct UserAndroidDeviceInfo_t810110808;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.UserAndroidDeviceInfo::.ctor()
extern "C"  void UserAndroidDeviceInfo__ctor_m2391507665 (UserAndroidDeviceInfo_t810110808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.UserAndroidDeviceInfo::get_AndroidDeviceId()
extern "C"  String_t* UserAndroidDeviceInfo_get_AndroidDeviceId_m47877795 (UserAndroidDeviceInfo_t810110808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UserAndroidDeviceInfo::set_AndroidDeviceId(System.String)
extern "C"  void UserAndroidDeviceInfo_set_AndroidDeviceId_m423836112 (UserAndroidDeviceInfo_t810110808 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
