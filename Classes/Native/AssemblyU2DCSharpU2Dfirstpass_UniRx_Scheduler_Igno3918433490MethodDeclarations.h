﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler/<PeriodicAction>c__Iterator1C
struct U3CPeriodicActionU3Ec__Iterator1C_t3918433490;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler/<PeriodicAction>c__Iterator1C::.ctor()
extern "C"  void U3CPeriodicActionU3Ec__Iterator1C__ctor_m1242987955 (U3CPeriodicActionU3Ec__Iterator1C_t3918433490 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler/<PeriodicAction>c__Iterator1C::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPeriodicActionU3Ec__Iterator1C_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3950142729 (U3CPeriodicActionU3Ec__Iterator1C_t3918433490 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler/<PeriodicAction>c__Iterator1C::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPeriodicActionU3Ec__Iterator1C_System_Collections_IEnumerator_get_Current_m783915677 (U3CPeriodicActionU3Ec__Iterator1C_t3918433490 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler/<PeriodicAction>c__Iterator1C::MoveNext()
extern "C"  bool U3CPeriodicActionU3Ec__Iterator1C_MoveNext_m1761677873 (U3CPeriodicActionU3Ec__Iterator1C_t3918433490 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler/<PeriodicAction>c__Iterator1C::Dispose()
extern "C"  void U3CPeriodicActionU3Ec__Iterator1C_Dispose_m408799536 (U3CPeriodicActionU3Ec__Iterator1C_t3918433490 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler/<PeriodicAction>c__Iterator1C::Reset()
extern "C"  void U3CPeriodicActionU3Ec__Iterator1C_Reset_m3184388192 (U3CPeriodicActionU3Ec__Iterator1C_t3918433490 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
