﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey96`1<UnityEngine.Vector2>
struct U3CAsObservableU3Ec__AnonStorey96_1_t2923013161;
// UnityEngine.Events.UnityAction`1<UnityEngine.Vector2>
struct UnityAction_1_t3505791693;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey96`1<UnityEngine.Vector2>::.ctor()
extern "C"  void U3CAsObservableU3Ec__AnonStorey96_1__ctor_m334696447_gshared (U3CAsObservableU3Ec__AnonStorey96_1_t2923013161 * __this, const MethodInfo* method);
#define U3CAsObservableU3Ec__AnonStorey96_1__ctor_m334696447(__this, method) ((  void (*) (U3CAsObservableU3Ec__AnonStorey96_1_t2923013161 *, const MethodInfo*))U3CAsObservableU3Ec__AnonStorey96_1__ctor_m334696447_gshared)(__this, method)
// System.Void UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey96`1<UnityEngine.Vector2>::<>m__CA(UnityEngine.Events.UnityAction`1<T>)
extern "C"  void U3CAsObservableU3Ec__AnonStorey96_1_U3CU3Em__CA_m3341182980_gshared (U3CAsObservableU3Ec__AnonStorey96_1_t2923013161 * __this, UnityAction_1_t3505791693 * ___h0, const MethodInfo* method);
#define U3CAsObservableU3Ec__AnonStorey96_1_U3CU3Em__CA_m3341182980(__this, ___h0, method) ((  void (*) (U3CAsObservableU3Ec__AnonStorey96_1_t2923013161 *, UnityAction_1_t3505791693 *, const MethodInfo*))U3CAsObservableU3Ec__AnonStorey96_1_U3CU3Em__CA_m3341182980_gshared)(__this, ___h0, method)
// System.Void UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey96`1<UnityEngine.Vector2>::<>m__CB(UnityEngine.Events.UnityAction`1<T>)
extern "C"  void U3CAsObservableU3Ec__AnonStorey96_1_U3CU3Em__CB_m679019683_gshared (U3CAsObservableU3Ec__AnonStorey96_1_t2923013161 * __this, UnityAction_1_t3505791693 * ___h0, const MethodInfo* method);
#define U3CAsObservableU3Ec__AnonStorey96_1_U3CU3Em__CB_m679019683(__this, ___h0, method) ((  void (*) (U3CAsObservableU3Ec__AnonStorey96_1_t2923013161 *, UnityAction_1_t3505791693 *, const MethodInfo*))U3CAsObservableU3Ec__AnonStorey96_1_U3CU3Em__CB_m679019683_gshared)(__this, ___h0, method)
