﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>
struct List_1_t3213480956;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1299263948.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionAddE2416521987.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UniRx.CollectionAddEvent`1<System.Object>>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3061823779_gshared (Enumerator_t1299263948 * __this, List_1_t3213480956 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m3061823779(__this, ___l0, method) ((  void (*) (Enumerator_t1299263948 *, List_1_t3213480956 *, const MethodInfo*))Enumerator__ctor_m3061823779_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3878875471_gshared (Enumerator_t1299263948 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3878875471(__this, method) ((  void (*) (Enumerator_t1299263948 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3878875471_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UniRx.CollectionAddEvent`1<System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1383570619_gshared (Enumerator_t1299263948 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1383570619(__this, method) ((  Il2CppObject * (*) (Enumerator_t1299263948 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1383570619_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UniRx.CollectionAddEvent`1<System.Object>>::Dispose()
extern "C"  void Enumerator_Dispose_m314998728_gshared (Enumerator_t1299263948 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m314998728(__this, method) ((  void (*) (Enumerator_t1299263948 *, const MethodInfo*))Enumerator_Dispose_m314998728_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UniRx.CollectionAddEvent`1<System.Object>>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2980387585_gshared (Enumerator_t1299263948 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m2980387585(__this, method) ((  void (*) (Enumerator_t1299263948 *, const MethodInfo*))Enumerator_VerifyState_m2980387585_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UniRx.CollectionAddEvent`1<System.Object>>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m247813307_gshared (Enumerator_t1299263948 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m247813307(__this, method) ((  bool (*) (Enumerator_t1299263948 *, const MethodInfo*))Enumerator_MoveNext_m247813307_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UniRx.CollectionAddEvent`1<System.Object>>::get_Current()
extern "C"  CollectionAddEvent_1_t2416521987  Enumerator_get_Current_m924044920_gshared (Enumerator_t1299263948 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m924044920(__this, method) ((  CollectionAddEvent_1_t2416521987  (*) (Enumerator_t1299263948 *, const MethodInfo*))Enumerator_get_Current_m924044920_gshared)(__this, method)
