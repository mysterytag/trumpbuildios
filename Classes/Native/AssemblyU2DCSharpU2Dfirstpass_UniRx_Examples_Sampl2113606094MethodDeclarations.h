﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Examples.Sample09_EventHandling
struct Sample09_EventHandling_t2113606094;
// System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>
struct EventHandler_1_t3942771990;
// System.Action`1<System.Int32>
struct Action_1_t2995867492;
// UniRx.IObservable`1<System.Int32>
struct IObservable_1_t2606213151;
// System.Action`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>
struct Action_1_t3248647052;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Examples.Sample09_EventHandling::.ctor()
extern "C"  void Sample09_EventHandling__ctor_m3366617911 (Sample09_EventHandling_t2113606094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample09_EventHandling::add_FooBar(System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>)
extern "C"  void Sample09_EventHandling_add_FooBar_m1259845978 (Sample09_EventHandling_t2113606094 * __this, EventHandler_1_t3942771990 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample09_EventHandling::remove_FooBar(System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>)
extern "C"  void Sample09_EventHandling_remove_FooBar_m3925468735 (Sample09_EventHandling_t2113606094 * __this, EventHandler_1_t3942771990 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample09_EventHandling::add_FooFoo(System.Action`1<System.Int32>)
extern "C"  void Sample09_EventHandling_add_FooFoo_m912362574 (Sample09_EventHandling_t2113606094 * __this, Action_1_t2995867492 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample09_EventHandling::remove_FooFoo(System.Action`1<System.Int32>)
extern "C"  void Sample09_EventHandling_remove_FooFoo_m1379018889 (Sample09_EventHandling_t2113606094 * __this, Action_1_t2995867492 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.Int32> UniRx.Examples.Sample09_EventHandling::get_OnBarBar()
extern "C"  Il2CppObject* Sample09_EventHandling_get_OnBarBar_m2982259802 (Sample09_EventHandling_t2113606094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample09_EventHandling::Start()
extern "C"  void Sample09_EventHandling_Start_m2313755703 (Sample09_EventHandling_t2113606094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample09_EventHandling::OnDestroy()
extern "C"  void Sample09_EventHandling_OnDestroy_m747234480 (Sample09_EventHandling_t2113606094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs> UniRx.Examples.Sample09_EventHandling::<Start>m__24(System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>)
extern "C"  EventHandler_1_t3942771990 * Sample09_EventHandling_U3CStartU3Em__24_m1633895583 (Il2CppObject * __this /* static, unused */, EventHandler_1_t3942771990 * ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs> UniRx.Examples.Sample09_EventHandling::<Start>m__27(System.Action`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>)
extern "C"  EventHandler_1_t3942771990 * Sample09_EventHandling_U3CStartU3Em__27_m3241598920 (Il2CppObject * __this /* static, unused */, Action_1_t3248647052 * ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
