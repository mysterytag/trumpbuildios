﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.DisposedObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>
struct DisposedObserver_1_t3720405766;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_DictionaryAddEv250981008.h"

// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m15980265_gshared (DisposedObserver_1_t3720405766 * __this, const MethodInfo* method);
#define DisposedObserver_1__ctor_m15980265(__this, method) ((  void (*) (DisposedObserver_1_t3720405766 *, const MethodInfo*))DisposedObserver_1__ctor_m15980265_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m13292004_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define DisposedObserver_1__cctor_m13292004(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))DisposedObserver_1__cctor_m13292004_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::OnCompleted()
extern "C"  void DisposedObserver_1_OnCompleted_m1253210035_gshared (DisposedObserver_1_t3720405766 * __this, const MethodInfo* method);
#define DisposedObserver_1_OnCompleted_m1253210035(__this, method) ((  void (*) (DisposedObserver_1_t3720405766 *, const MethodInfo*))DisposedObserver_1_OnCompleted_m1253210035_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::OnError(System.Exception)
extern "C"  void DisposedObserver_1_OnError_m2583704672_gshared (DisposedObserver_1_t3720405766 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define DisposedObserver_1_OnError_m2583704672(__this, ___error0, method) ((  void (*) (DisposedObserver_1_t3720405766 *, Exception_t1967233988 *, const MethodInfo*))DisposedObserver_1_OnError_m2583704672_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::OnNext(T)
extern "C"  void DisposedObserver_1_OnNext_m1007868753_gshared (DisposedObserver_1_t3720405766 * __this, DictionaryAddEvent_2_t250981008  ___value0, const MethodInfo* method);
#define DisposedObserver_1_OnNext_m1007868753(__this, ___value0, method) ((  void (*) (DisposedObserver_1_t3720405766 *, DictionaryAddEvent_2_t250981008 , const MethodInfo*))DisposedObserver_1_OnNext_m1007868753_gshared)(__this, ___value0, method)
