﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.RemoveSharedGroupMembersResult
struct RemoveSharedGroupMembersResult_t3306088560;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.RemoveSharedGroupMembersResult::.ctor()
extern "C"  void RemoveSharedGroupMembersResult__ctor_m3669513293 (RemoveSharedGroupMembersResult_t3306088560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
