﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1451594948MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Zenject.PrefabSingletonId,Zenject.PrefabSingletonLazyCreator>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m1570765039(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t1515782375 *, Dictionary_2_t3888612577 *, const MethodInfo*))ValueCollection__ctor_m4177258586_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Zenject.PrefabSingletonId,Zenject.PrefabSingletonLazyCreator>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1444145507(__this, ___item0, method) ((  void (*) (ValueCollection_t1515782375 *, PrefabSingletonLazyCreator_t3719004230 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1528225944_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Zenject.PrefabSingletonId,Zenject.PrefabSingletonLazyCreator>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m258211756(__this, method) ((  void (*) (ValueCollection_t1515782375 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2827278689_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Zenject.PrefabSingletonId,Zenject.PrefabSingletonLazyCreator>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2932087783(__this, ___item0, method) ((  bool (*) (ValueCollection_t1515782375 *, PrefabSingletonLazyCreator_t3719004230 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2015709838_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Zenject.PrefabSingletonId,Zenject.PrefabSingletonLazyCreator>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m535888460(__this, ___item0, method) ((  bool (*) (ValueCollection_t1515782375 *, PrefabSingletonLazyCreator_t3719004230 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3578506931_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Zenject.PrefabSingletonId,Zenject.PrefabSingletonLazyCreator>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3614087212(__this, method) ((  Il2CppObject* (*) (ValueCollection_t1515782375 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m4041606511_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Zenject.PrefabSingletonId,Zenject.PrefabSingletonLazyCreator>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m2475686320(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1515782375 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1709700389_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<Zenject.PrefabSingletonId,Zenject.PrefabSingletonLazyCreator>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m123619135(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1515782375 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2843387488_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Zenject.PrefabSingletonId,Zenject.PrefabSingletonLazyCreator>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1207263642(__this, method) ((  bool (*) (ValueCollection_t1515782375 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m290885697_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Zenject.PrefabSingletonId,Zenject.PrefabSingletonLazyCreator>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m354270842(__this, method) ((  bool (*) (ValueCollection_t1515782375 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1930063777_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<Zenject.PrefabSingletonId,Zenject.PrefabSingletonLazyCreator>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m408420268(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1515782375 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1874108365_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Zenject.PrefabSingletonId,Zenject.PrefabSingletonLazyCreator>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m3401365878(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1515782375 *, PrefabSingletonLazyCreatorU5BU5D_t356452323*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1735386657_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Zenject.PrefabSingletonId,Zenject.PrefabSingletonLazyCreator>::GetEnumerator()
#define ValueCollection_GetEnumerator_m762569631(__this, method) ((  Enumerator_t3655640520  (*) (ValueCollection_t1515782375 *, const MethodInfo*))ValueCollection_GetEnumerator_m1204216004_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<Zenject.PrefabSingletonId,Zenject.PrefabSingletonLazyCreator>::get_Count()
#define ValueCollection_get_Count_m1948390708(__this, method) ((  int32_t (*) (ValueCollection_t1515782375 *, const MethodInfo*))ValueCollection_get_Count_m2709231847_gshared)(__this, method)
