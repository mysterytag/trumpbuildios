﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.MultipleAssignmentDisposable
struct MultipleAssignmentDisposable_t3090232719;
// System.IDisposable
struct IDisposable_t1628921374;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.MultipleAssignmentDisposable::.ctor()
extern "C"  void MultipleAssignmentDisposable__ctor_m4117172186 (MultipleAssignmentDisposable_t3090232719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.MultipleAssignmentDisposable::.cctor()
extern "C"  void MultipleAssignmentDisposable__cctor_m2596189971 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.MultipleAssignmentDisposable::get_IsDisposed()
extern "C"  bool MultipleAssignmentDisposable_get_IsDisposed_m3534532222 (MultipleAssignmentDisposable_t3090232719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable UniRx.MultipleAssignmentDisposable::get_Disposable()
extern "C"  Il2CppObject * MultipleAssignmentDisposable_get_Disposable_m1668944462 (MultipleAssignmentDisposable_t3090232719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.MultipleAssignmentDisposable::set_Disposable(System.IDisposable)
extern "C"  void MultipleAssignmentDisposable_set_Disposable_m2693870941 (MultipleAssignmentDisposable_t3090232719 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.MultipleAssignmentDisposable::Dispose()
extern "C"  void MultipleAssignmentDisposable_Dispose_m835874199 (MultipleAssignmentDisposable_t3090232719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
