﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.FactoryUntyped`2<System.Object,System.Object>
struct FactoryUntyped_2_t884050241;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.Object
struct Il2CppObject;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException>
struct IEnumerable_1_t4073207355;
// System.Type[]
struct TypeU5BU5D_t3431720054;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449.h"

// System.Void Zenject.FactoryUntyped`2<System.Object,System.Object>::.ctor()
extern "C"  void FactoryUntyped_2__ctor_m1787600921_gshared (FactoryUntyped_2_t884050241 * __this, const MethodInfo* method);
#define FactoryUntyped_2__ctor_m1787600921(__this, method) ((  void (*) (FactoryUntyped_2_t884050241 *, const MethodInfo*))FactoryUntyped_2__ctor_m1787600921_gshared)(__this, method)
// Zenject.DiContainer Zenject.FactoryUntyped`2<System.Object,System.Object>::get_Container()
extern "C"  DiContainer_t2383114449 * FactoryUntyped_2_get_Container_m1034149349_gshared (FactoryUntyped_2_t884050241 * __this, const MethodInfo* method);
#define FactoryUntyped_2_get_Container_m1034149349(__this, method) ((  DiContainer_t2383114449 * (*) (FactoryUntyped_2_t884050241 *, const MethodInfo*))FactoryUntyped_2_get_Container_m1034149349_gshared)(__this, method)
// System.Void Zenject.FactoryUntyped`2<System.Object,System.Object>::set_Container(Zenject.DiContainer)
extern "C"  void FactoryUntyped_2_set_Container_m1008159162_gshared (FactoryUntyped_2_t884050241 * __this, DiContainer_t2383114449 * ___value0, const MethodInfo* method);
#define FactoryUntyped_2_set_Container_m1008159162(__this, ___value0, method) ((  void (*) (FactoryUntyped_2_t884050241 *, DiContainer_t2383114449 *, const MethodInfo*))FactoryUntyped_2_set_Container_m1008159162_gshared)(__this, ___value0, method)
// TContract Zenject.FactoryUntyped`2<System.Object,System.Object>::Create(System.Object[])
extern "C"  Il2CppObject * FactoryUntyped_2_Create_m3733682712_gshared (FactoryUntyped_2_t884050241 * __this, ObjectU5BU5D_t11523773* ___constructorArgs0, const MethodInfo* method);
#define FactoryUntyped_2_Create_m3733682712(__this, ___constructorArgs0, method) ((  Il2CppObject * (*) (FactoryUntyped_2_t884050241 *, ObjectU5BU5D_t11523773*, const MethodInfo*))FactoryUntyped_2_Create_m3733682712_gshared)(__this, ___constructorArgs0, method)
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.FactoryUntyped`2<System.Object,System.Object>::Validate(System.Type[])
extern "C"  Il2CppObject* FactoryUntyped_2_Validate_m1558660213_gshared (FactoryUntyped_2_t884050241 * __this, TypeU5BU5D_t3431720054* ___extras0, const MethodInfo* method);
#define FactoryUntyped_2_Validate_m1558660213(__this, ___extras0, method) ((  Il2CppObject* (*) (FactoryUntyped_2_t884050241 *, TypeU5BU5D_t3431720054*, const MethodInfo*))FactoryUntyped_2_Validate_m1558660213_gshared)(__this, ___extras0, method)
