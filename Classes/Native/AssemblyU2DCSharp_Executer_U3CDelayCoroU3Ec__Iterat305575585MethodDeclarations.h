﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Executer/<DelayCoro>c__Iterator31
struct U3CDelayCoroU3Ec__Iterator31_t305575585;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Executer/<DelayCoro>c__Iterator31::.ctor()
extern "C"  void U3CDelayCoroU3Ec__Iterator31__ctor_m3469163032 (U3CDelayCoroU3Ec__Iterator31_t305575585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Executer/<DelayCoro>c__Iterator31::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDelayCoroU3Ec__Iterator31_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1299556730 (U3CDelayCoroU3Ec__Iterator31_t305575585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Executer/<DelayCoro>c__Iterator31::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDelayCoroU3Ec__Iterator31_System_Collections_IEnumerator_get_Current_m2869813518 (U3CDelayCoroU3Ec__Iterator31_t305575585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Executer/<DelayCoro>c__Iterator31::MoveNext()
extern "C"  bool U3CDelayCoroU3Ec__Iterator31_MoveNext_m3705309660 (U3CDelayCoroU3Ec__Iterator31_t305575585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Executer/<DelayCoro>c__Iterator31::Dispose()
extern "C"  void U3CDelayCoroU3Ec__Iterator31_Dispose_m869335125 (U3CDelayCoroU3Ec__Iterator31_t305575585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Executer/<DelayCoro>c__Iterator31::Reset()
extern "C"  void U3CDelayCoroU3Ec__Iterator31_Reset_m1115595973 (U3CDelayCoroU3Ec__Iterator31_t305575585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
