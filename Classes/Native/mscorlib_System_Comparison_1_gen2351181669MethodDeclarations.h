﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Comparison`1<LitJson.PropertyMetadata>
struct Comparison_1_t2351181669;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "AssemblyU2DCSharp_LitJson_PropertyMetadata3942474089.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Comparison`1<LitJson.PropertyMetadata>::.ctor(System.Object,System.IntPtr)
extern "C"  void Comparison_1__ctor_m3296549046_gshared (Comparison_1_t2351181669 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Comparison_1__ctor_m3296549046(__this, ___object0, ___method1, method) ((  void (*) (Comparison_1_t2351181669 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m3296549046_gshared)(__this, ___object0, ___method1, method)
// System.Int32 System.Comparison`1<LitJson.PropertyMetadata>::Invoke(T,T)
extern "C"  int32_t Comparison_1_Invoke_m4197955074_gshared (Comparison_1_t2351181669 * __this, PropertyMetadata_t3942474089  ___x0, PropertyMetadata_t3942474089  ___y1, const MethodInfo* method);
#define Comparison_1_Invoke_m4197955074(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t2351181669 *, PropertyMetadata_t3942474089 , PropertyMetadata_t3942474089 , const MethodInfo*))Comparison_1_Invoke_m4197955074_gshared)(__this, ___x0, ___y1, method)
// System.IAsyncResult System.Comparison`1<LitJson.PropertyMetadata>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Comparison_1_BeginInvoke_m1198085451_gshared (Comparison_1_t2351181669 * __this, PropertyMetadata_t3942474089  ___x0, PropertyMetadata_t3942474089  ___y1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Comparison_1_BeginInvoke_m1198085451(__this, ___x0, ___y1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Comparison_1_t2351181669 *, PropertyMetadata_t3942474089 , PropertyMetadata_t3942474089 , AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Comparison_1_BeginInvoke_m1198085451_gshared)(__this, ___x0, ___y1, ___callback2, ___object3, method)
// System.Int32 System.Comparison`1<LitJson.PropertyMetadata>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Comparison_1_EndInvoke_m574318378_gshared (Comparison_1_t2351181669 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Comparison_1_EndInvoke_m574318378(__this, ___result0, method) ((  int32_t (*) (Comparison_1_t2351181669 *, Il2CppObject *, const MethodInfo*))Comparison_1_EndInvoke_m574318378_gshared)(__this, ___result0, method)
