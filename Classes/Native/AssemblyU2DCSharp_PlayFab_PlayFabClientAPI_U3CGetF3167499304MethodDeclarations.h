﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<GetFriendLeaderboardAroundPlayer>c__AnonStorey8D
struct U3CGetFriendLeaderboardAroundPlayerU3Ec__AnonStorey8D_t3167499304;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<GetFriendLeaderboardAroundPlayer>c__AnonStorey8D::.ctor()
extern "C"  void U3CGetFriendLeaderboardAroundPlayerU3Ec__AnonStorey8D__ctor_m2301748363 (U3CGetFriendLeaderboardAroundPlayerU3Ec__AnonStorey8D_t3167499304 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<GetFriendLeaderboardAroundPlayer>c__AnonStorey8D::<>m__7A(PlayFab.CallRequestContainer)
extern "C"  void U3CGetFriendLeaderboardAroundPlayerU3Ec__AnonStorey8D_U3CU3Em__7A_m1893941139 (U3CGetFriendLeaderboardAroundPlayerU3Ec__AnonStorey8D_t3167499304 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
