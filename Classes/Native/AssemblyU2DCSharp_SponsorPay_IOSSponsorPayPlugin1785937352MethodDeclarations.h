﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SponsorPay.IOSSponsorPayPlugin
struct IOSSponsorPayPlugin_t1785937352;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Action`1<System.Int32>
struct Action_1_t2995867492;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_SponsorPay_SPLogLevel220722135.h"

// System.Void SponsorPay.IOSSponsorPayPlugin::.ctor(UnityEngine.GameObject)
extern "C"  void IOSSponsorPayPlugin__ctor_m646177957 (IOSSponsorPayPlugin_t1785937352 * __this, GameObject_t4012695102 * ___gameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.IOSSponsorPayPlugin::.cctor()
extern "C"  void IOSSponsorPayPlugin__cctor_m364696672 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.IOSSponsorPayPlugin::_SPSetCallbackGameObjectName(System.String)
extern "C"  void IOSSponsorPayPlugin__SPSetCallbackGameObjectName_m3788268004 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.IOSSponsorPayPlugin::_SPSetPluginVersion(System.String)
extern "C"  void IOSSponsorPayPlugin__SPSetPluginVersion_m1136011864 (Il2CppObject * __this /* static, unused */, String_t* ___pluginVersion0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SponsorPay.IOSSponsorPayPlugin::_SPStartSDK(System.String,System.String,System.String)
extern "C"  String_t* IOSSponsorPayPlugin__SPStartSDK_m4214948606 (Il2CppObject * __this /* static, unused */, String_t* ___appId0, String_t* ___userId1, String_t* ___secretToken2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.IOSSponsorPayPlugin::_SPLaunchOfferWall(System.String,System.String,System.String)
extern "C"  void IOSSponsorPayPlugin__SPLaunchOfferWall_m3509556140 (Il2CppObject * __this /* static, unused */, String_t* ___credentialsToken0, String_t* ___currencyName1, String_t* ___placementId2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.IOSSponsorPayPlugin::_SPSendDeltaOfCoinsRequest(System.String,System.String)
extern "C"  void IOSSponsorPayPlugin__SPSendDeltaOfCoinsRequest_m1631396879 (Il2CppObject * __this /* static, unused */, String_t* ___credentialsToken0, String_t* ___currencyId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.IOSSponsorPayPlugin::_SPSetShouldShowNotificationOnVCSCoins(System.Int32)
extern "C"  void IOSSponsorPayPlugin__SPSetShouldShowNotificationOnVCSCoins_m936362180 (Il2CppObject * __this /* static, unused */, int32_t ___should0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.IOSSponsorPayPlugin::_SPRequestBrandEngageOffers(System.String,System.String,System.Int32,System.String,System.String)
extern "C"  void IOSSponsorPayPlugin__SPRequestBrandEngageOffers_m48913352 (Il2CppObject * __this /* static, unused */, String_t* ___credentialsToken0, String_t* ___currencyName1, int32_t ___queryVCS2, String_t* ___currencyId3, String_t* ___placementId4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.IOSSponsorPayPlugin::_SPStartBrandEngage()
extern "C"  void IOSSponsorPayPlugin__SPStartBrandEngage_m878956365 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.IOSSponsorPayPlugin::_SPRequestIntersitialAds(System.String,System.String)
extern "C"  void IOSSponsorPayPlugin__SPRequestIntersitialAds_m3417161174 (Il2CppObject * __this /* static, unused */, String_t* ___credentialsToken0, String_t* ___placementId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.IOSSponsorPayPlugin::_SPShowInterstitialAd()
extern "C"  void IOSSponsorPayPlugin__SPShowInterstitialAd_m371168531 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.IOSSponsorPayPlugin::_SPSetShouldShowBrandEngageRewardNotification(System.Int32)
extern "C"  void IOSSponsorPayPlugin__SPSetShouldShowBrandEngageRewardNotification_m2783142280 (Il2CppObject * __this /* static, unused */, int32_t ___should0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.IOSSponsorPayPlugin::_SPReportActionCompletion(System.String,System.String)
extern "C"  void IOSSponsorPayPlugin__SPReportActionCompletion_m4128587985 (Il2CppObject * __this /* static, unused */, String_t* ___credentialsToken0, String_t* ___actionId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.IOSSponsorPayPlugin::_SPEnableLogging(System.Int32)
extern "C"  void IOSSponsorPayPlugin__SPEnableLogging_m2056811304 (Il2CppObject * __this /* static, unused */, int32_t ___should0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.IOSSponsorPayPlugin::_SPVideoDownloadPause(System.Boolean)
extern "C"  void IOSSponsorPayPlugin__SPVideoDownloadPause_m3386802193 (Il2CppObject * __this /* static, unused */, bool ___pause0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.IOSSponsorPayPlugin::_SPSetLogLevel(System.Int32)
extern "C"  void IOSSponsorPayPlugin__SPSetLogLevel_m718115822 (Il2CppObject * __this /* static, unused */, int32_t ___logLevel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.IOSSponsorPayPlugin::_SPAddParameters(System.String)
extern "C"  void IOSSponsorPayPlugin__SPAddParameters_m4080668444 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.IOSSponsorPayPlugin::_SPClearParameters()
extern "C"  void IOSSponsorPayPlugin__SPClearParameters_m2304570066 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.IOSSponsorPayPlugin::Awake()
extern "C"  void IOSSponsorPayPlugin_Awake_m1927489104 (IOSSponsorPayPlugin_t1785937352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.IOSSponsorPayPlugin::InitPlugin(System.String)
extern "C"  void IOSSponsorPayPlugin_InitPlugin_m886678376 (IOSSponsorPayPlugin_t1785937352 * __this, String_t* ___objectGameName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SponsorPay.IOSSponsorPayPlugin::StartSDK(System.String,System.String,System.String)
extern "C"  String_t* IOSSponsorPayPlugin_StartSDK_m837011240 (IOSSponsorPayPlugin_t1785937352 * __this, String_t* ___appId0, String_t* ___userId1, String_t* ___securityToken2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.IOSSponsorPayPlugin::ReportActionCompletion(System.String,System.String)
extern "C"  void IOSSponsorPayPlugin_ReportActionCompletion_m558738913 (IOSSponsorPayPlugin_t1785937352 * __this, String_t* ___credentialsToken0, String_t* ___actionId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.IOSSponsorPayPlugin::LaunchOfferWall(System.String,System.String,System.String)
extern "C"  void IOSSponsorPayPlugin_LaunchOfferWall_m1965380764 (IOSSponsorPayPlugin_t1785937352 * __this, String_t* ___credentialsToken0, String_t* ___currencyName1, String_t* ___placementId2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.IOSSponsorPayPlugin::RequestNewCoins(System.String,System.String,System.String)
extern "C"  void IOSSponsorPayPlugin_RequestNewCoins_m3375435710 (IOSSponsorPayPlugin_t1785937352 * __this, String_t* ___credentialsToken0, String_t* ___currencyName1, String_t* ___currencyId2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.IOSSponsorPayPlugin::ShowVCSNotifications(System.Boolean)
extern "C"  void IOSSponsorPayPlugin_ShowVCSNotifications_m915055149 (IOSSponsorPayPlugin_t1785937352 * __this, bool ___showNotification0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.IOSSponsorPayPlugin::RequestBrandEngageOffers(System.String,System.String,System.Boolean,System.String,System.String)
extern "C"  void IOSSponsorPayPlugin_RequestBrandEngageOffers_m852373534 (IOSSponsorPayPlugin_t1785937352 * __this, String_t* ___credentialsToken0, String_t* ___currencyName1, bool ___queryVCS2, String_t* ___currencyId3, String_t* ___placementId4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.IOSSponsorPayPlugin::StartBrandEngage()
extern "C"  void IOSSponsorPayPlugin_StartBrandEngage_m1885421629 (IOSSponsorPayPlugin_t1785937352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.IOSSponsorPayPlugin::ShowBrandEngageRewardNotification(System.Boolean)
extern "C"  void IOSSponsorPayPlugin_ShowBrandEngageRewardNotification_m581746407 (IOSSponsorPayPlugin_t1785937352 * __this, bool ___showNotification0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.IOSSponsorPayPlugin::RequestInterstitialAds(System.String,System.String)
extern "C"  void IOSSponsorPayPlugin_RequestInterstitialAds_m1949104370 (IOSSponsorPayPlugin_t1785937352 * __this, String_t* ___credentialsToken0, String_t* ___placementId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.IOSSponsorPayPlugin::ShowInterstitialAd()
extern "C"  void IOSSponsorPayPlugin_ShowInterstitialAd_m1216645635 (IOSSponsorPayPlugin_t1785937352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.IOSSponsorPayPlugin::VideoDownloadPause(System.Boolean)
extern "C"  void IOSSponsorPayPlugin_VideoDownloadPause_m1547440385 (IOSSponsorPayPlugin_t1785937352 * __this, bool ___pause0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.IOSSponsorPayPlugin::EnableLogging(System.Boolean)
extern "C"  void IOSSponsorPayPlugin_EnableLogging_m3514888862 (IOSSponsorPayPlugin_t1785937352 * __this, bool ___shouldEnableLogging0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.IOSSponsorPayPlugin::SetLogLevel(SponsorPay.SPLogLevel)
extern "C"  void IOSSponsorPayPlugin_SetLogLevel_m3072205960 (IOSSponsorPayPlugin_t1785937352 * __this, int32_t ___logLevel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.IOSSponsorPayPlugin::AddParameters(System.String)
extern "C"  void IOSSponsorPayPlugin_AddParameters_m3146978316 (IOSSponsorPayPlugin_t1785937352 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.IOSSponsorPayPlugin::RemoveAllParameters()
extern "C"  void IOSSponsorPayPlugin_RemoveAllParameters_m833805074 (IOSSponsorPayPlugin_t1785937352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SponsorPay.IOSSponsorPayPlugin::SkipFrameCoroutineWithBlock(System.Action`1<System.Int32>)
extern "C"  Il2CppObject * IOSSponsorPayPlugin_SkipFrameCoroutineWithBlock_m1435290969 (IOSSponsorPayPlugin_t1785937352 * __this, Action_1_t2995867492 * ___block0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.IOSSponsorPayPlugin::<StartBrandEngage>m__206(System.Int32)
extern "C"  void IOSSponsorPayPlugin_U3CStartBrandEngageU3Em__206_m520710863 (Il2CppObject * __this /* static, unused */, int32_t ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.IOSSponsorPayPlugin::<ShowInterstitialAd>m__207(System.Int32)
extern "C"  void IOSSponsorPayPlugin_U3CShowInterstitialAdU3Em__207_m2943135114 (Il2CppObject * __this /* static, unused */, int32_t ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
