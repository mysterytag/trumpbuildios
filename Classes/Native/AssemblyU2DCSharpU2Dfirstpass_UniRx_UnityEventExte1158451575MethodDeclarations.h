﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey9C`4<System.Object,System.Object,System.Object,System.Object>
struct U3CAsObservableU3Ec__AnonStorey9C_4_t1158451575;
// UnityEngine.Events.UnityAction`4<System.Object,System.Object,System.Object,System.Object>
struct UnityAction_4_t3193939828;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey9C`4<System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C"  void U3CAsObservableU3Ec__AnonStorey9C_4__ctor_m661952523_gshared (U3CAsObservableU3Ec__AnonStorey9C_4_t1158451575 * __this, const MethodInfo* method);
#define U3CAsObservableU3Ec__AnonStorey9C_4__ctor_m661952523(__this, method) ((  void (*) (U3CAsObservableU3Ec__AnonStorey9C_4_t1158451575 *, const MethodInfo*))U3CAsObservableU3Ec__AnonStorey9C_4__ctor_m661952523_gshared)(__this, method)
// System.Void UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey9C`4<System.Object,System.Object,System.Object,System.Object>::<>m__D3(UnityEngine.Events.UnityAction`4<T0,T1,T2,T3>)
extern "C"  void U3CAsObservableU3Ec__AnonStorey9C_4_U3CU3Em__D3_m2483626404_gshared (U3CAsObservableU3Ec__AnonStorey9C_4_t1158451575 * __this, UnityAction_4_t3193939828 * ___h0, const MethodInfo* method);
#define U3CAsObservableU3Ec__AnonStorey9C_4_U3CU3Em__D3_m2483626404(__this, ___h0, method) ((  void (*) (U3CAsObservableU3Ec__AnonStorey9C_4_t1158451575 *, UnityAction_4_t3193939828 *, const MethodInfo*))U3CAsObservableU3Ec__AnonStorey9C_4_U3CU3Em__D3_m2483626404_gshared)(__this, ___h0, method)
// System.Void UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey9C`4<System.Object,System.Object,System.Object,System.Object>::<>m__D4(UnityEngine.Events.UnityAction`4<T0,T1,T2,T3>)
extern "C"  void U3CAsObservableU3Ec__AnonStorey9C_4_U3CU3Em__D4_m3453207427_gshared (U3CAsObservableU3Ec__AnonStorey9C_4_t1158451575 * __this, UnityAction_4_t3193939828 * ___h0, const MethodInfo* method);
#define U3CAsObservableU3Ec__AnonStorey9C_4_U3CU3Em__D4_m3453207427(__this, ___h0, method) ((  void (*) (U3CAsObservableU3Ec__AnonStorey9C_4_t1158451575 *, UnityAction_4_t3193939828 *, const MethodInfo*))U3CAsObservableU3Ec__AnonStorey9C_4_U3CU3Em__D4_m3453207427_gshared)(__this, ___h0, method)
