﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/<Merge>c__AnonStorey112`3/<Merge>c__AnonStorey113`3<System.Double,System.Int32,System.Object>
struct U3CMergeU3Ec__AnonStorey113_3_t3675351849;

#include "codegen/il2cpp-codegen.h"

// System.Void CellReactiveApi/<Merge>c__AnonStorey112`3/<Merge>c__AnonStorey113`3<System.Double,System.Int32,System.Object>::.ctor()
extern "C"  void U3CMergeU3Ec__AnonStorey113_3__ctor_m150554692_gshared (U3CMergeU3Ec__AnonStorey113_3_t3675351849 * __this, const MethodInfo* method);
#define U3CMergeU3Ec__AnonStorey113_3__ctor_m150554692(__this, method) ((  void (*) (U3CMergeU3Ec__AnonStorey113_3_t3675351849 *, const MethodInfo*))U3CMergeU3Ec__AnonStorey113_3__ctor_m150554692_gshared)(__this, method)
// System.Void CellReactiveApi/<Merge>c__AnonStorey112`3/<Merge>c__AnonStorey113`3<System.Double,System.Int32,System.Object>::<>m__19A(T)
extern "C"  void U3CMergeU3Ec__AnonStorey113_3_U3CU3Em__19A_m1275787472_gshared (U3CMergeU3Ec__AnonStorey113_3_t3675351849 * __this, double ___val0, const MethodInfo* method);
#define U3CMergeU3Ec__AnonStorey113_3_U3CU3Em__19A_m1275787472(__this, ___val0, method) ((  void (*) (U3CMergeU3Ec__AnonStorey113_3_t3675351849 *, double, const MethodInfo*))U3CMergeU3Ec__AnonStorey113_3_U3CU3Em__19A_m1275787472_gshared)(__this, ___val0, method)
// System.Void CellReactiveApi/<Merge>c__AnonStorey112`3/<Merge>c__AnonStorey113`3<System.Double,System.Int32,System.Object>::<>m__19B(T2)
extern "C"  void U3CMergeU3Ec__AnonStorey113_3_U3CU3Em__19B_m895629809_gshared (U3CMergeU3Ec__AnonStorey113_3_t3675351849 * __this, int32_t ___val0, const MethodInfo* method);
#define U3CMergeU3Ec__AnonStorey113_3_U3CU3Em__19B_m895629809(__this, ___val0, method) ((  void (*) (U3CMergeU3Ec__AnonStorey113_3_t3675351849 *, int32_t, const MethodInfo*))U3CMergeU3Ec__AnonStorey113_3_U3CU3Em__19B_m895629809_gshared)(__this, ___val0, method)
