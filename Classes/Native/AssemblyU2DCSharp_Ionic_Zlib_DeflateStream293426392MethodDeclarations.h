﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ionic.Zlib.DeflateStream
struct DeflateStream_t293426392;
// System.IO.Stream
struct Stream_t219029575;
// System.Byte[]
struct ByteU5BU5D_t58506160;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream219029575.h"
#include "AssemblyU2DCSharp_Ionic_Zlib_CompressionMode1632830838.h"
#include "AssemblyU2DCSharp_Ionic_Zlib_CompressionLevel3940478795.h"
#include "AssemblyU2DCSharp_Ionic_Zlib_FlushType3984958891.h"
#include "AssemblyU2DCSharp_Ionic_Zlib_CompressionStrategy2328611910.h"
#include "mscorlib_System_IO_SeekOrigin3506139269.h"
#include "mscorlib_System_String968488902.h"

// System.Void Ionic.Zlib.DeflateStream::.ctor(System.IO.Stream,Ionic.Zlib.CompressionMode)
extern "C"  void DeflateStream__ctor_m2401061302 (DeflateStream_t293426392 * __this, Stream_t219029575 * ___stream0, int32_t ___mode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.DeflateStream::.ctor(System.IO.Stream,Ionic.Zlib.CompressionMode,Ionic.Zlib.CompressionLevel)
extern "C"  void DeflateStream__ctor_m2344533357 (DeflateStream_t293426392 * __this, Stream_t219029575 * ___stream0, int32_t ___mode1, int32_t ___level2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.DeflateStream::.ctor(System.IO.Stream,Ionic.Zlib.CompressionMode,System.Boolean)
extern "C"  void DeflateStream__ctor_m3028002567 (DeflateStream_t293426392 * __this, Stream_t219029575 * ___stream0, int32_t ___mode1, bool ___leaveOpen2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.DeflateStream::.ctor(System.IO.Stream,Ionic.Zlib.CompressionMode,Ionic.Zlib.CompressionLevel,System.Boolean)
extern "C"  void DeflateStream__ctor_m2276159088 (DeflateStream_t293426392 * __this, Stream_t219029575 * ___stream0, int32_t ___mode1, int32_t ___level2, bool ___leaveOpen3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Ionic.Zlib.FlushType Ionic.Zlib.DeflateStream::get_FlushMode()
extern "C"  int32_t DeflateStream_get_FlushMode_m62593997 (DeflateStream_t293426392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.DeflateStream::set_FlushMode(Ionic.Zlib.FlushType)
extern "C"  void DeflateStream_set_FlushMode_m801645896 (DeflateStream_t293426392 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.DeflateStream::get_BufferSize()
extern "C"  int32_t DeflateStream_get_BufferSize_m3732469845 (DeflateStream_t293426392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.DeflateStream::set_BufferSize(System.Int32)
extern "C"  void DeflateStream_set_BufferSize_m2498618124 (DeflateStream_t293426392 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Ionic.Zlib.CompressionStrategy Ionic.Zlib.DeflateStream::get_Strategy()
extern "C"  int32_t DeflateStream_get_Strategy_m98082324 (DeflateStream_t293426392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.DeflateStream::set_Strategy(Ionic.Zlib.CompressionStrategy)
extern "C"  void DeflateStream_set_Strategy_m4028577419 (DeflateStream_t293426392 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Ionic.Zlib.DeflateStream::get_TotalIn()
extern "C"  int64_t DeflateStream_get_TotalIn_m3486786902 (DeflateStream_t293426392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Ionic.Zlib.DeflateStream::get_TotalOut()
extern "C"  int64_t DeflateStream_get_TotalOut_m722034271 (DeflateStream_t293426392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.DeflateStream::Dispose(System.Boolean)
extern "C"  void DeflateStream_Dispose_m1853654267 (DeflateStream_t293426392 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Ionic.Zlib.DeflateStream::get_CanRead()
extern "C"  bool DeflateStream_get_CanRead_m1210075982 (DeflateStream_t293426392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Ionic.Zlib.DeflateStream::get_CanSeek()
extern "C"  bool DeflateStream_get_CanSeek_m1238831024 (DeflateStream_t293426392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Ionic.Zlib.DeflateStream::get_CanWrite()
extern "C"  bool DeflateStream_get_CanWrite_m3675270601 (DeflateStream_t293426392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.DeflateStream::Flush()
extern "C"  void DeflateStream_Flush_m3699797801 (DeflateStream_t293426392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Ionic.Zlib.DeflateStream::get_Length()
extern "C"  int64_t DeflateStream_get_Length_m2344604123 (DeflateStream_t293426392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Ionic.Zlib.DeflateStream::get_Position()
extern "C"  int64_t DeflateStream_get_Position_m2823200350 (DeflateStream_t293426392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.DeflateStream::set_Position(System.Int64)
extern "C"  void DeflateStream_set_Position_m1688458133 (DeflateStream_t293426392 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.DeflateStream::Read(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t DeflateStream_Read_m2601489992 (DeflateStream_t293426392 * __this, ByteU5BU5D_t58506160* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Ionic.Zlib.DeflateStream::Seek(System.Int64,System.IO.SeekOrigin)
extern "C"  int64_t DeflateStream_Seek_m2979932457 (DeflateStream_t293426392 * __this, int64_t ___offset0, int32_t ___origin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.DeflateStream::SetLength(System.Int64)
extern "C"  void DeflateStream_SetLength_m2400092255 (DeflateStream_t293426392 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.DeflateStream::Write(System.Byte[],System.Int32,System.Int32)
extern "C"  void DeflateStream_Write_m2678674981 (DeflateStream_t293426392 * __this, ByteU5BU5D_t58506160* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Ionic.Zlib.DeflateStream::CompressString(System.String)
extern "C"  ByteU5BU5D_t58506160* DeflateStream_CompressString_m3123210280 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Ionic.Zlib.DeflateStream::CompressBuffer(System.Byte[])
extern "C"  ByteU5BU5D_t58506160* DeflateStream_CompressBuffer_m982765792 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t58506160* ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Ionic.Zlib.DeflateStream::UncompressString(System.Byte[])
extern "C"  String_t* DeflateStream_UncompressString_m1292672349 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t58506160* ___compressed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Ionic.Zlib.DeflateStream::UncompressBuffer(System.Byte[])
extern "C"  ByteU5BU5D_t58506160* DeflateStream_UncompressBuffer_m3401596519 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t58506160* ___compressed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
