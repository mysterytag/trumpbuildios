﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.GameObjectFactory`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct GameObjectFactory_5_t2159204100;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException>
struct IEnumerable_1_t4073207355;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void Zenject.GameObjectFactory`5<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C"  void GameObjectFactory_5__ctor_m2975506806_gshared (GameObjectFactory_5_t2159204100 * __this, const MethodInfo* method);
#define GameObjectFactory_5__ctor_m2975506806(__this, method) ((  void (*) (GameObjectFactory_5_t2159204100 *, const MethodInfo*))GameObjectFactory_5__ctor_m2975506806_gshared)(__this, method)
// TValue Zenject.GameObjectFactory`5<System.Object,System.Object,System.Object,System.Object,System.Object>::Create(TParam1,TParam2,TParam3,TParam4)
extern "C"  Il2CppObject * GameObjectFactory_5_Create_m2371140262_gshared (GameObjectFactory_5_t2159204100 * __this, Il2CppObject * ___param10, Il2CppObject * ___param21, Il2CppObject * ___param32, Il2CppObject * ___param43, const MethodInfo* method);
#define GameObjectFactory_5_Create_m2371140262(__this, ___param10, ___param21, ___param32, ___param43, method) ((  Il2CppObject * (*) (GameObjectFactory_5_t2159204100 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))GameObjectFactory_5_Create_m2371140262_gshared)(__this, ___param10, ___param21, ___param32, ___param43, method)
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.GameObjectFactory`5<System.Object,System.Object,System.Object,System.Object,System.Object>::Validate()
extern "C"  Il2CppObject* GameObjectFactory_5_Validate_m912002371_gshared (GameObjectFactory_5_t2159204100 * __this, const MethodInfo* method);
#define GameObjectFactory_5_Validate_m912002371(__this, method) ((  Il2CppObject* (*) (GameObjectFactory_5_t2159204100 *, const MethodInfo*))GameObjectFactory_5_Validate_m912002371_gshared)(__this, method)
