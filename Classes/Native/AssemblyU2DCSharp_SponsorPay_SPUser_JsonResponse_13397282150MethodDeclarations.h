﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserGender>>
struct JsonResponse_1_t3397282150;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen2074414583.h"

// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserGender>>::.ctor()
extern "C"  void JsonResponse_1__ctor_m2502626008_gshared (JsonResponse_1_t3397282150 * __this, const MethodInfo* method);
#define JsonResponse_1__ctor_m2502626008(__this, method) ((  void (*) (JsonResponse_1_t3397282150 *, const MethodInfo*))JsonResponse_1__ctor_m2502626008_gshared)(__this, method)
// System.String SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserGender>>::get_key()
extern "C"  String_t* JsonResponse_1_get_key_m94153161_gshared (JsonResponse_1_t3397282150 * __this, const MethodInfo* method);
#define JsonResponse_1_get_key_m94153161(__this, method) ((  String_t* (*) (JsonResponse_1_t3397282150 *, const MethodInfo*))JsonResponse_1_get_key_m94153161_gshared)(__this, method)
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserGender>>::set_key(System.String)
extern "C"  void JsonResponse_1_set_key_m4178286250_gshared (JsonResponse_1_t3397282150 * __this, String_t* ___value0, const MethodInfo* method);
#define JsonResponse_1_set_key_m4178286250(__this, ___value0, method) ((  void (*) (JsonResponse_1_t3397282150 *, String_t*, const MethodInfo*))JsonResponse_1_set_key_m4178286250_gshared)(__this, ___value0, method)
// T SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserGender>>::get_value()
extern "C"  Nullable_1_t2074414583  JsonResponse_1_get_value_m13871167_gshared (JsonResponse_1_t3397282150 * __this, const MethodInfo* method);
#define JsonResponse_1_get_value_m13871167(__this, method) ((  Nullable_1_t2074414583  (*) (JsonResponse_1_t3397282150 *, const MethodInfo*))JsonResponse_1_get_value_m13871167_gshared)(__this, method)
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserGender>>::set_value(T)
extern "C"  void JsonResponse_1_set_value_m1189208756_gshared (JsonResponse_1_t3397282150 * __this, Nullable_1_t2074414583  ___value0, const MethodInfo* method);
#define JsonResponse_1_set_value_m1189208756(__this, ___value0, method) ((  void (*) (JsonResponse_1_t3397282150 *, Nullable_1_t2074414583 , const MethodInfo*))JsonResponse_1_set_value_m1189208756_gshared)(__this, ___value0, method)
// System.String SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserGender>>::get_error()
extern "C"  String_t* JsonResponse_1_get_error_m3919687922_gshared (JsonResponse_1_t3397282150 * __this, const MethodInfo* method);
#define JsonResponse_1_get_error_m3919687922(__this, method) ((  String_t* (*) (JsonResponse_1_t3397282150 *, const MethodInfo*))JsonResponse_1_get_error_m3919687922_gshared)(__this, method)
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserGender>>::set_error(System.String)
extern "C"  void JsonResponse_1_set_error_m2194524641_gshared (JsonResponse_1_t3397282150 * __this, String_t* ___value0, const MethodInfo* method);
#define JsonResponse_1_set_error_m2194524641(__this, ___value0, method) ((  void (*) (JsonResponse_1_t3397282150 *, String_t*, const MethodInfo*))JsonResponse_1_set_error_m2194524641_gshared)(__this, ___value0, method)
