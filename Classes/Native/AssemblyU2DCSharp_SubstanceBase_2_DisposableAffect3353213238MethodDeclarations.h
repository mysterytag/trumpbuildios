﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SubstanceBase`2/DisposableAffect<System.Object,System.Object>
struct DisposableAffect_t3353213238;

#include "codegen/il2cpp-codegen.h"

// System.Void SubstanceBase`2/DisposableAffect<System.Object,System.Object>::.ctor()
extern "C"  void DisposableAffect__ctor_m2653883762_gshared (DisposableAffect_t3353213238 * __this, const MethodInfo* method);
#define DisposableAffect__ctor_m2653883762(__this, method) ((  void (*) (DisposableAffect_t3353213238 *, const MethodInfo*))DisposableAffect__ctor_m2653883762_gshared)(__this, method)
// System.Void SubstanceBase`2/DisposableAffect<System.Object,System.Object>::Dispose()
extern "C"  void DisposableAffect_Dispose_m3364971823_gshared (DisposableAffect_t3353213238 * __this, const MethodInfo* method);
#define DisposableAffect_Dispose_m3364971823(__this, method) ((  void (*) (DisposableAffect_t3353213238 *, const MethodInfo*))DisposableAffect_Dispose_m3364971823_gshared)(__this, method)
