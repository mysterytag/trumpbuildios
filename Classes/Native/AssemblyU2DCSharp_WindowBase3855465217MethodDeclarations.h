﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WindowBase
struct WindowBase_t3855465217;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Action
struct Action_t437523947;
// WindowManager
struct WindowManager_t3316821373;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"
#include "AssemblyU2DCSharp_WindowManager3316821373.h"

// System.Void WindowBase::.ctor()
extern "C"  void WindowBase__ctor_m2257170106 (WindowBase_t3855465217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator WindowBase::waitAndDisable(System.Single)
extern "C"  Il2CppObject * WindowBase_waitAndDisable_m3411452819 (WindowBase_t3855465217 * __this, float ___inSeconds0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WindowBase::Close()
extern "C"  void WindowBase_Close_m3968029648 (WindowBase_t3855465217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WindowBase::PostInject()
extern "C"  void WindowBase_PostInject_m2309130907 (WindowBase_t3855465217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WindowBase::SetCallback(System.Action)
extern "C"  void WindowBase_SetCallback_m1002528830 (WindowBase_t3855465217 * __this, Action_t437523947 * ___inCallback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WindowBase::SetManager(WindowManager)
extern "C"  void WindowBase_SetManager_m2629130048 (WindowBase_t3855465217 * __this, WindowManager_t3316821373 * ___inManager0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WindowBase::Show()
extern "C"  void WindowBase_Show_m2660585223 (WindowBase_t3855465217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
