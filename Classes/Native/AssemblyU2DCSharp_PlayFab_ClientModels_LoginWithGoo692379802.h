﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Nullable_1_gen3097043249.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.LoginWithGoogleAccountRequest
struct  LoginWithGoogleAccountRequest_t692379802  : public Il2CppObject
{
public:
	// System.String PlayFab.ClientModels.LoginWithGoogleAccountRequest::<TitleId>k__BackingField
	String_t* ___U3CTitleIdU3Ek__BackingField_0;
	// System.String PlayFab.ClientModels.LoginWithGoogleAccountRequest::<AccessToken>k__BackingField
	String_t* ___U3CAccessTokenU3Ek__BackingField_1;
	// System.Nullable`1<System.Boolean> PlayFab.ClientModels.LoginWithGoogleAccountRequest::<CreateAccount>k__BackingField
	Nullable_1_t3097043249  ___U3CCreateAccountU3Ek__BackingField_2;
	// System.String PlayFab.ClientModels.LoginWithGoogleAccountRequest::<PublisherId>k__BackingField
	String_t* ___U3CPublisherIdU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CTitleIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(LoginWithGoogleAccountRequest_t692379802, ___U3CTitleIdU3Ek__BackingField_0)); }
	inline String_t* get_U3CTitleIdU3Ek__BackingField_0() const { return ___U3CTitleIdU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CTitleIdU3Ek__BackingField_0() { return &___U3CTitleIdU3Ek__BackingField_0; }
	inline void set_U3CTitleIdU3Ek__BackingField_0(String_t* value)
	{
		___U3CTitleIdU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTitleIdU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CAccessTokenU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(LoginWithGoogleAccountRequest_t692379802, ___U3CAccessTokenU3Ek__BackingField_1)); }
	inline String_t* get_U3CAccessTokenU3Ek__BackingField_1() const { return ___U3CAccessTokenU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CAccessTokenU3Ek__BackingField_1() { return &___U3CAccessTokenU3Ek__BackingField_1; }
	inline void set_U3CAccessTokenU3Ek__BackingField_1(String_t* value)
	{
		___U3CAccessTokenU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAccessTokenU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CCreateAccountU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(LoginWithGoogleAccountRequest_t692379802, ___U3CCreateAccountU3Ek__BackingField_2)); }
	inline Nullable_1_t3097043249  get_U3CCreateAccountU3Ek__BackingField_2() const { return ___U3CCreateAccountU3Ek__BackingField_2; }
	inline Nullable_1_t3097043249 * get_address_of_U3CCreateAccountU3Ek__BackingField_2() { return &___U3CCreateAccountU3Ek__BackingField_2; }
	inline void set_U3CCreateAccountU3Ek__BackingField_2(Nullable_1_t3097043249  value)
	{
		___U3CCreateAccountU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CPublisherIdU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(LoginWithGoogleAccountRequest_t692379802, ___U3CPublisherIdU3Ek__BackingField_3)); }
	inline String_t* get_U3CPublisherIdU3Ek__BackingField_3() const { return ___U3CPublisherIdU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CPublisherIdU3Ek__BackingField_3() { return &___U3CPublisherIdU3Ek__BackingField_3; }
	inline void set_U3CPublisherIdU3Ek__BackingField_3(String_t* value)
	{
		___U3CPublisherIdU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPublisherIdU3Ek__BackingField_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
