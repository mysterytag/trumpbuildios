﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t3161373071;
// System.Func`2<System.Object,System.Object>
struct Func_2_t2135783352;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4196218687.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.DistinctUntilChangedObservable`2<System.Object,System.Object>
struct  DistinctUntilChangedObservable_2_t1596230355  : public OperatorObservableBase_1_t4196218687
{
public:
	// UniRx.IObservable`1<T> UniRx.Operators.DistinctUntilChangedObservable`2::source
	Il2CppObject* ___source_1;
	// System.Collections.Generic.IEqualityComparer`1<TKey> UniRx.Operators.DistinctUntilChangedObservable`2::comparer
	Il2CppObject* ___comparer_2;
	// System.Func`2<T,TKey> UniRx.Operators.DistinctUntilChangedObservable`2::keySelector
	Func_2_t2135783352 * ___keySelector_3;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(DistinctUntilChangedObservable_2_t1596230355, ___source_1)); }
	inline Il2CppObject* get_source_1() const { return ___source_1; }
	inline Il2CppObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(Il2CppObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier(&___source_1, value);
	}

	inline static int32_t get_offset_of_comparer_2() { return static_cast<int32_t>(offsetof(DistinctUntilChangedObservable_2_t1596230355, ___comparer_2)); }
	inline Il2CppObject* get_comparer_2() const { return ___comparer_2; }
	inline Il2CppObject** get_address_of_comparer_2() { return &___comparer_2; }
	inline void set_comparer_2(Il2CppObject* value)
	{
		___comparer_2 = value;
		Il2CppCodeGenWriteBarrier(&___comparer_2, value);
	}

	inline static int32_t get_offset_of_keySelector_3() { return static_cast<int32_t>(offsetof(DistinctUntilChangedObservable_2_t1596230355, ___keySelector_3)); }
	inline Func_2_t2135783352 * get_keySelector_3() const { return ___keySelector_3; }
	inline Func_2_t2135783352 ** get_address_of_keySelector_3() { return &___keySelector_3; }
	inline void set_keySelector_3(Func_2_t2135783352 * value)
	{
		___keySelector_3 = value;
		Il2CppCodeGenWriteBarrier(&___keySelector_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
