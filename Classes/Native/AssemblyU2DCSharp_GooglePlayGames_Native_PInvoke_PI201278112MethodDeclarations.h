﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutStringMethod
struct OutStringMethod_t201278112;
// System.Object
struct Il2CppObject;
// System.Text.StringBuilder
struct StringBuilder_t3822575854;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_UIntPtr3100060873.h"
#include "mscorlib_System_Text_StringBuilder3822575854.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutStringMethod::.ctor(System.Object,System.IntPtr)
extern "C"  void OutStringMethod__ctor_m2730868781 (OutStringMethod_t201278112 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutStringMethod::Invoke(System.Text.StringBuilder,System.UIntPtr)
extern "C"  UIntPtr_t  OutStringMethod_Invoke_m1759385702 (OutStringMethod_t201278112 * __this, StringBuilder_t3822575854 * ___out_string0, UIntPtr_t  ___out_size1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" UIntPtr_t  pinvoke_delegate_wrapper_OutStringMethod_t201278112(Il2CppObject* delegate, StringBuilder_t3822575854 * ___out_string0, UIntPtr_t  ___out_size1);
// System.IAsyncResult GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutStringMethod::BeginInvoke(System.Text.StringBuilder,System.UIntPtr,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OutStringMethod_BeginInvoke_m4157640307 (OutStringMethod_t201278112 * __this, StringBuilder_t3822575854 * ___out_string0, UIntPtr_t  ___out_size1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutStringMethod::EndInvoke(System.IAsyncResult)
extern "C"  UIntPtr_t  OutStringMethod_EndInvoke_m3783760053 (OutStringMethod_t201278112 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
