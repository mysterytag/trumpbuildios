﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1929422447MethodDeclarations.h"

// System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Collections.Generic.IList`1<System.Int64>>::.ctor(UniRx.IObserver`1<TResult>,System.IDisposable)
#define OperatorObserverBase_2__ctor_m3388513441(__this, ___observer0, ___cancel1, method) ((  void (*) (OperatorObserverBase_2_t1811255927 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))OperatorObserverBase_2__ctor_m1945314260_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Collections.Generic.IList`1<System.Int64>>::Dispose()
#define OperatorObserverBase_2_Dispose_m3577574377(__this, method) ((  void (*) (OperatorObserverBase_2_t1811255927 *, const MethodInfo*))OperatorObserverBase_2_Dispose_m1235051158_gshared)(__this, method)
