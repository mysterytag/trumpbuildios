﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/RefreshPSNAuthTokenRequestCallback
struct RefreshPSNAuthTokenRequestCallback_t2712704179;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.RefreshPSNAuthTokenRequest
struct RefreshPSNAuthTokenRequest_t4115941982;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_RefreshPSNA4115941982.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/RefreshPSNAuthTokenRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void RefreshPSNAuthTokenRequestCallback__ctor_m3793480738 (RefreshPSNAuthTokenRequestCallback_t2712704179 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/RefreshPSNAuthTokenRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.RefreshPSNAuthTokenRequest,System.Object)
extern "C"  void RefreshPSNAuthTokenRequestCallback_Invoke_m1422128639 (RefreshPSNAuthTokenRequestCallback_t2712704179 * __this, String_t* ___urlPath0, int32_t ___callId1, RefreshPSNAuthTokenRequest_t4115941982 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_RefreshPSNAuthTokenRequestCallback_t2712704179(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, RefreshPSNAuthTokenRequest_t4115941982 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/RefreshPSNAuthTokenRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.RefreshPSNAuthTokenRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * RefreshPSNAuthTokenRequestCallback_BeginInvoke_m678035148 (RefreshPSNAuthTokenRequestCallback_t2712704179 * __this, String_t* ___urlPath0, int32_t ___callId1, RefreshPSNAuthTokenRequest_t4115941982 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/RefreshPSNAuthTokenRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void RefreshPSNAuthTokenRequestCallback_EndInvoke_m3640573746 (RefreshPSNAuthTokenRequestCallback_t2712704179 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
