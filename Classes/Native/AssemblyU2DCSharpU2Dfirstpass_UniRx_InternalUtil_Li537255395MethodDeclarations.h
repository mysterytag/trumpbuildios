﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.ListObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>
struct ListObserver_1_t537255395;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>>
struct ImmutableList_1_t3514841126;
// System.Exception
struct Exception_t1967233988;
// UniRx.IObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>
struct IObserver_1_t2454636627;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionRemov242637724.h"

// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m3040940214_gshared (ListObserver_1_t537255395 * __this, ImmutableList_1_t3514841126 * ___observers0, const MethodInfo* method);
#define ListObserver_1__ctor_m3040940214(__this, ___observers0, method) ((  void (*) (ListObserver_1_t537255395 *, ImmutableList_1_t3514841126 *, const MethodInfo*))ListObserver_1__ctor_m3040940214_gshared)(__this, ___observers0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m795407470_gshared (ListObserver_1_t537255395 * __this, const MethodInfo* method);
#define ListObserver_1_OnCompleted_m795407470(__this, method) ((  void (*) (ListObserver_1_t537255395 *, const MethodInfo*))ListObserver_1_OnCompleted_m795407470_gshared)(__this, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m3465703579_gshared (ListObserver_1_t537255395 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ListObserver_1_OnError_m3465703579(__this, ___error0, method) ((  void (*) (ListObserver_1_t537255395 *, Exception_t1967233988 *, const MethodInfo*))ListObserver_1_OnError_m3465703579_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m2279796108_gshared (ListObserver_1_t537255395 * __this, CollectionRemoveEvent_1_t242637724  ___value0, const MethodInfo* method);
#define ListObserver_1_OnNext_m2279796108(__this, ___value0, method) ((  void (*) (ListObserver_1_t537255395 *, CollectionRemoveEvent_1_t242637724 , const MethodInfo*))ListObserver_1_OnNext_m2279796108_gshared)(__this, ___value0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m1993239148_gshared (ListObserver_1_t537255395 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Add_m1993239148(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t537255395 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Add_m1993239148_gshared)(__this, ___observer0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m3098036575_gshared (ListObserver_1_t537255395 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Remove_m3098036575(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t537255395 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Remove_m3098036575_gshared)(__this, ___observer0, method)
