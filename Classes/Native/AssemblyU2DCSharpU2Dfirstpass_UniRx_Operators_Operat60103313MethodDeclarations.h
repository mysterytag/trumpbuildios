﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.OperatorObserverBase`2<System.Boolean,System.Boolean>
struct OperatorObserverBase_2_t60103313;
// UniRx.IObserver`1<System.Boolean>
struct IObserver_1_t2423004244;
// System.IDisposable
struct IDisposable_t1628921374;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.OperatorObserverBase`2<System.Boolean,System.Boolean>::.ctor(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void OperatorObserverBase_2__ctor_m1574733264_gshared (OperatorObserverBase_2_t60103313 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define OperatorObserverBase_2__ctor_m1574733264(__this, ___observer0, ___cancel1, method) ((  void (*) (OperatorObserverBase_2_t60103313 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))OperatorObserverBase_2__ctor_m1574733264_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.OperatorObserverBase`2<System.Boolean,System.Boolean>::Dispose()
extern "C"  void OperatorObserverBase_2_Dispose_m2651616794_gshared (OperatorObserverBase_2_t60103313 * __this, const MethodInfo* method);
#define OperatorObserverBase_2_Dispose_m2651616794(__this, method) ((  void (*) (OperatorObserverBase_2_t60103313 *, const MethodInfo*))OperatorObserverBase_2_Dispose_m2651616794_gshared)(__this, method)
