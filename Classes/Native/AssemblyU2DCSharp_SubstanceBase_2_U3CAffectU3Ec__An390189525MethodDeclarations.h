﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SubstanceBase`2/<Affect>c__AnonStorey143<System.Boolean,System.Boolean>
struct U3CAffectU3Ec__AnonStorey143_t390189525;

#include "codegen/il2cpp-codegen.h"

// System.Void SubstanceBase`2/<Affect>c__AnonStorey143<System.Boolean,System.Boolean>::.ctor()
extern "C"  void U3CAffectU3Ec__AnonStorey143__ctor_m17649775_gshared (U3CAffectU3Ec__AnonStorey143_t390189525 * __this, const MethodInfo* method);
#define U3CAffectU3Ec__AnonStorey143__ctor_m17649775(__this, method) ((  void (*) (U3CAffectU3Ec__AnonStorey143_t390189525 *, const MethodInfo*))U3CAffectU3Ec__AnonStorey143__ctor_m17649775_gshared)(__this, method)
// System.Void SubstanceBase`2/<Affect>c__AnonStorey143<System.Boolean,System.Boolean>::<>m__1D9(InfT)
extern "C"  void U3CAffectU3Ec__AnonStorey143_U3CU3Em__1D9_m91688573_gshared (U3CAffectU3Ec__AnonStorey143_t390189525 * __this, bool ___val0, const MethodInfo* method);
#define U3CAffectU3Ec__AnonStorey143_U3CU3Em__1D9_m91688573(__this, ___val0, method) ((  void (*) (U3CAffectU3Ec__AnonStorey143_t390189525 *, bool, const MethodInfo*))U3CAffectU3Ec__AnonStorey143_U3CU3Em__1D9_m91688573_gshared)(__this, ___val0, method)
