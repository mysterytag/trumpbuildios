﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SponsorPay.RequestError
struct RequestError_t1597064339;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void SponsorPay.RequestError::.ctor()
extern "C"  void RequestError__ctor_m3095653394 (RequestError_t1597064339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SponsorPay.RequestError::get_Type()
extern "C"  String_t* RequestError_get_Type_m964078386 (RequestError_t1597064339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.RequestError::set_Type(System.String)
extern "C"  void RequestError_set_Type_m2893257721 (RequestError_t1597064339 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SponsorPay.RequestError::get_Code()
extern "C"  String_t* RequestError_get_Code_m467790117 (RequestError_t1597064339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.RequestError::set_Code(System.String)
extern "C"  void RequestError_set_Code_m2390352102 (RequestError_t1597064339 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SponsorPay.RequestError::get_Message()
extern "C"  String_t* RequestError_get_Message_m2801605233 (RequestError_t1597064339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.RequestError::set_Message(System.String)
extern "C"  void RequestError_set_Message_m1027836072 (RequestError_t1597064339 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
