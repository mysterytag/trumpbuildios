﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Color>
struct DisposedObserver_1_t762633222;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"

// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Color>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m816678020_gshared (DisposedObserver_1_t762633222 * __this, const MethodInfo* method);
#define DisposedObserver_1__ctor_m816678020(__this, method) ((  void (*) (DisposedObserver_1_t762633222 *, const MethodInfo*))DisposedObserver_1__ctor_m816678020_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Color>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m3360085929_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define DisposedObserver_1__cctor_m3360085929(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))DisposedObserver_1__cctor_m3360085929_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Color>::OnCompleted()
extern "C"  void DisposedObserver_1_OnCompleted_m2837202190_gshared (DisposedObserver_1_t762633222 * __this, const MethodInfo* method);
#define DisposedObserver_1_OnCompleted_m2837202190(__this, method) ((  void (*) (DisposedObserver_1_t762633222 *, const MethodInfo*))DisposedObserver_1_OnCompleted_m2837202190_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Color>::OnError(System.Exception)
extern "C"  void DisposedObserver_1_OnError_m1471271227_gshared (DisposedObserver_1_t762633222 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define DisposedObserver_1_OnError_m1471271227(__this, ___error0, method) ((  void (*) (DisposedObserver_1_t762633222 *, Exception_t1967233988 *, const MethodInfo*))DisposedObserver_1_OnError_m1471271227_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Color>::OnNext(T)
extern "C"  void DisposedObserver_1_OnNext_m1679265324_gshared (DisposedObserver_1_t762633222 * __this, Color_t1588175760  ___value0, const MethodInfo* method);
#define DisposedObserver_1_OnNext_m1679265324(__this, ___value0, method) ((  void (*) (DisposedObserver_1_t762633222 *, Color_t1588175760 , const MethodInfo*))DisposedObserver_1_OnNext_m1679265324_gshared)(__this, ___value0, method)
