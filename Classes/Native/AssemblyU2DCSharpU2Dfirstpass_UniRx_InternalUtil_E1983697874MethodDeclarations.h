﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.EmptyObserver`1<UnityEngine.NetworkMessageInfo>
struct EmptyObserver_1_t1983697874;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_NetworkMessageInfo2574344884.h"

// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.NetworkMessageInfo>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m3647941742_gshared (EmptyObserver_1_t1983697874 * __this, const MethodInfo* method);
#define EmptyObserver_1__ctor_m3647941742(__this, method) ((  void (*) (EmptyObserver_1_t1983697874 *, const MethodInfo*))EmptyObserver_1__ctor_m3647941742_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.NetworkMessageInfo>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m934948095_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EmptyObserver_1__cctor_m934948095(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EmptyObserver_1__cctor_m934948095_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.NetworkMessageInfo>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m2285294968_gshared (EmptyObserver_1_t1983697874 * __this, const MethodInfo* method);
#define EmptyObserver_1_OnCompleted_m2285294968(__this, method) ((  void (*) (EmptyObserver_1_t1983697874 *, const MethodInfo*))EmptyObserver_1_OnCompleted_m2285294968_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.NetworkMessageInfo>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m3585413797_gshared (EmptyObserver_1_t1983697874 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define EmptyObserver_1_OnError_m3585413797(__this, ___error0, method) ((  void (*) (EmptyObserver_1_t1983697874 *, Exception_t1967233988 *, const MethodInfo*))EmptyObserver_1_OnError_m3585413797_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.NetworkMessageInfo>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m3809403798_gshared (EmptyObserver_1_t1983697874 * __this, NetworkMessageInfo_t2574344884  ___value0, const MethodInfo* method);
#define EmptyObserver_1_OnNext_m3809403798(__this, ___value0, method) ((  void (*) (EmptyObserver_1_t1983697874 *, NetworkMessageInfo_t2574344884 , const MethodInfo*))EmptyObserver_1_OnNext_m3809403798_gshared)(__this, ___value0, method)
