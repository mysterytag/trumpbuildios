﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.CombineLatestObservable`1/CombineLatest/CombineLatestObserver<System.Boolean>
struct CombineLatestObserver_t3970536766;
// UniRx.Operators.CombineLatestObservable`1/CombineLatest<System.Boolean>
struct CombineLatest_t242152888;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.CombineLatestObservable`1/CombineLatest/CombineLatestObserver<System.Boolean>::.ctor(UniRx.Operators.CombineLatestObservable`1/CombineLatest<T>,System.Int32)
extern "C"  void CombineLatestObserver__ctor_m1836044108_gshared (CombineLatestObserver_t3970536766 * __this, CombineLatest_t242152888 * ___parent0, int32_t ___index1, const MethodInfo* method);
#define CombineLatestObserver__ctor_m1836044108(__this, ___parent0, ___index1, method) ((  void (*) (CombineLatestObserver_t3970536766 *, CombineLatest_t242152888 *, int32_t, const MethodInfo*))CombineLatestObserver__ctor_m1836044108_gshared)(__this, ___parent0, ___index1, method)
// System.Void UniRx.Operators.CombineLatestObservable`1/CombineLatest/CombineLatestObserver<System.Boolean>::OnNext(T)
extern "C"  void CombineLatestObserver_OnNext_m279410687_gshared (CombineLatestObserver_t3970536766 * __this, bool ___value0, const MethodInfo* method);
#define CombineLatestObserver_OnNext_m279410687(__this, ___value0, method) ((  void (*) (CombineLatestObserver_t3970536766 *, bool, const MethodInfo*))CombineLatestObserver_OnNext_m279410687_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.CombineLatestObservable`1/CombineLatest/CombineLatestObserver<System.Boolean>::OnError(System.Exception)
extern "C"  void CombineLatestObserver_OnError_m491894030_gshared (CombineLatestObserver_t3970536766 * __this, Exception_t1967233988 * ___ex0, const MethodInfo* method);
#define CombineLatestObserver_OnError_m491894030(__this, ___ex0, method) ((  void (*) (CombineLatestObserver_t3970536766 *, Exception_t1967233988 *, const MethodInfo*))CombineLatestObserver_OnError_m491894030_gshared)(__this, ___ex0, method)
// System.Void UniRx.Operators.CombineLatestObservable`1/CombineLatest/CombineLatestObserver<System.Boolean>::OnCompleted()
extern "C"  void CombineLatestObserver_OnCompleted_m1429015905_gshared (CombineLatestObserver_t3970536766 * __this, const MethodInfo* method);
#define CombineLatestObserver_OnCompleted_m1429015905(__this, method) ((  void (*) (CombineLatestObserver_t3970536766 *, const MethodInfo*))CombineLatestObserver_OnCompleted_m1429015905_gshared)(__this, method)
