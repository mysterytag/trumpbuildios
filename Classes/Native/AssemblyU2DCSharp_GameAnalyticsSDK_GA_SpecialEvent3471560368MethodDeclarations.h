﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameAnalyticsSDK.GA_SpecialEvents/<SubmitFPSRoutine>c__Iterator2
struct U3CSubmitFPSRoutineU3Ec__Iterator2_t3471560368;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GameAnalyticsSDK.GA_SpecialEvents/<SubmitFPSRoutine>c__Iterator2::.ctor()
extern "C"  void U3CSubmitFPSRoutineU3Ec__Iterator2__ctor_m150154033 (U3CSubmitFPSRoutineU3Ec__Iterator2_t3471560368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameAnalyticsSDK.GA_SpecialEvents/<SubmitFPSRoutine>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSubmitFPSRoutineU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m291632971 (U3CSubmitFPSRoutineU3Ec__Iterator2_t3471560368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameAnalyticsSDK.GA_SpecialEvents/<SubmitFPSRoutine>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSubmitFPSRoutineU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m2554982623 (U3CSubmitFPSRoutineU3Ec__Iterator2_t3471560368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameAnalyticsSDK.GA_SpecialEvents/<SubmitFPSRoutine>c__Iterator2::MoveNext()
extern "C"  bool U3CSubmitFPSRoutineU3Ec__Iterator2_MoveNext_m2612908299 (U3CSubmitFPSRoutineU3Ec__Iterator2_t3471560368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_SpecialEvents/<SubmitFPSRoutine>c__Iterator2::Dispose()
extern "C"  void U3CSubmitFPSRoutineU3Ec__Iterator2_Dispose_m2462388014 (U3CSubmitFPSRoutineU3Ec__Iterator2_t3471560368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_SpecialEvents/<SubmitFPSRoutine>c__Iterator2::Reset()
extern "C"  void U3CSubmitFPSRoutineU3Ec__Iterator2_Reset_m2091554270 (U3CSubmitFPSRoutineU3Ec__Iterator2_t3471560368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
