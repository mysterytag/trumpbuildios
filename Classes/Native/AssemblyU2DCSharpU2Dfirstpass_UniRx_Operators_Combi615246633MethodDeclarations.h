﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.CombineLatestObservable`3/CombineLatest/RightObserver<System.Boolean,System.Int32,System.Object>
struct RightObserver_t615246633;
// UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Int32,System.Object>
struct CombineLatest_t2150356605;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/RightObserver<System.Boolean,System.Int32,System.Object>::.ctor(UniRx.Operators.CombineLatestObservable`3/CombineLatest<TLeft,TRight,TResult>)
extern "C"  void RightObserver__ctor_m1518668666_gshared (RightObserver_t615246633 * __this, CombineLatest_t2150356605 * ___parent0, const MethodInfo* method);
#define RightObserver__ctor_m1518668666(__this, ___parent0, method) ((  void (*) (RightObserver_t615246633 *, CombineLatest_t2150356605 *, const MethodInfo*))RightObserver__ctor_m1518668666_gshared)(__this, ___parent0, method)
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/RightObserver<System.Boolean,System.Int32,System.Object>::OnNext(TRight)
extern "C"  void RightObserver_OnNext_m76743542_gshared (RightObserver_t615246633 * __this, int32_t ___value0, const MethodInfo* method);
#define RightObserver_OnNext_m76743542(__this, ___value0, method) ((  void (*) (RightObserver_t615246633 *, int32_t, const MethodInfo*))RightObserver_OnNext_m76743542_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/RightObserver<System.Boolean,System.Int32,System.Object>::OnError(System.Exception)
extern "C"  void RightObserver_OnError_m1432800207_gshared (RightObserver_t615246633 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define RightObserver_OnError_m1432800207(__this, ___error0, method) ((  void (*) (RightObserver_t615246633 *, Exception_t1967233988 *, const MethodInfo*))RightObserver_OnError_m1432800207_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/RightObserver<System.Boolean,System.Int32,System.Object>::OnCompleted()
extern "C"  void RightObserver_OnCompleted_m3743776162_gshared (RightObserver_t615246633 * __this, const MethodInfo* method);
#define RightObserver_OnCompleted_m3743776162(__this, method) ((  void (*) (RightObserver_t615246633 *, const MethodInfo*))RightObserver_OnCompleted_m3743776162_gshared)(__this, method)
