﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3591453091MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Zenject.ZenjectTypeInfo>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2259192662(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2162789364 *, Dictionary_2_t2395761423 *, const MethodInfo*))Enumerator__ctor_m3920831137_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Zenject.ZenjectTypeInfo>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m855083787(__this, method) ((  Il2CppObject * (*) (Enumerator_t2162789364 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3262087712_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Zenject.ZenjectTypeInfo>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1754782623(__this, method) ((  void (*) (Enumerator_t2162789364 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2959141748_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Zenject.ZenjectTypeInfo>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1628382952(__this, method) ((  DictionaryEntry_t130027246  (*) (Enumerator_t2162789364 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2279524093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Zenject.ZenjectTypeInfo>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4082570151(__this, method) ((  Il2CppObject * (*) (Enumerator_t2162789364 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1201448700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Zenject.ZenjectTypeInfo>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3093210233(__this, method) ((  Il2CppObject * (*) (Enumerator_t2162789364 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m294434446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Zenject.ZenjectTypeInfo>::MoveNext()
#define Enumerator_MoveNext_m3542734603(__this, method) ((  bool (*) (Enumerator_t2162789364 *, const MethodInfo*))Enumerator_MoveNext_m217327200_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Zenject.ZenjectTypeInfo>::get_Current()
#define Enumerator_get_Current_m2700058949(__this, method) ((  KeyValuePair_2_t1884292721  (*) (Enumerator_t2162789364 *, const MethodInfo*))Enumerator_get_Current_m4240003024_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Zenject.ZenjectTypeInfo>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m3439492248(__this, method) ((  Type_t * (*) (Enumerator_t2162789364 *, const MethodInfo*))Enumerator_get_CurrentKey_m3062159917_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Zenject.ZenjectTypeInfo>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2484971836(__this, method) ((  ZenjectTypeInfo_t283213708 * (*) (Enumerator_t2162789364 *, const MethodInfo*))Enumerator_get_CurrentValue_m592783249_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Zenject.ZenjectTypeInfo>::Reset()
#define Enumerator_Reset_m717653160(__this, method) ((  void (*) (Enumerator_t2162789364 *, const MethodInfo*))Enumerator_Reset_m3001375603_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Zenject.ZenjectTypeInfo>::VerifyState()
#define Enumerator_VerifyState_m503166641(__this, method) ((  void (*) (Enumerator_t2162789364 *, const MethodInfo*))Enumerator_VerifyState_m4290054460_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Zenject.ZenjectTypeInfo>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m956709337(__this, method) ((  void (*) (Enumerator_t2162789364 *, const MethodInfo*))Enumerator_VerifyCurrent_m2318603684_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,Zenject.ZenjectTypeInfo>::Dispose()
#define Enumerator_Dispose_m698381176(__this, method) ((  void (*) (Enumerator_t2162789364 *, const MethodInfo*))Enumerator_Dispose_m627360643_gshared)(__this, method)
