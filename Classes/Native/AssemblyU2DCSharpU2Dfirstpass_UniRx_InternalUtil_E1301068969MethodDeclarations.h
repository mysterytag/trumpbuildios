﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Quaternion>
struct EmptyObserver_1_t1301068969;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979.h"

// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Quaternion>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m793319045_gshared (EmptyObserver_1_t1301068969 * __this, const MethodInfo* method);
#define EmptyObserver_1__ctor_m793319045(__this, method) ((  void (*) (EmptyObserver_1_t1301068969 *, const MethodInfo*))EmptyObserver_1__ctor_m793319045_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Quaternion>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m2635957704_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EmptyObserver_1__cctor_m2635957704(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EmptyObserver_1__cctor_m2635957704_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Quaternion>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m2317914703_gshared (EmptyObserver_1_t1301068969 * __this, const MethodInfo* method);
#define EmptyObserver_1_OnCompleted_m2317914703(__this, method) ((  void (*) (EmptyObserver_1_t1301068969 *, const MethodInfo*))EmptyObserver_1_OnCompleted_m2317914703_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Quaternion>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m4277617916_gshared (EmptyObserver_1_t1301068969 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define EmptyObserver_1_OnError_m4277617916(__this, ___error0, method) ((  void (*) (EmptyObserver_1_t1301068969 *, Exception_t1967233988 *, const MethodInfo*))EmptyObserver_1_OnError_m4277617916_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Quaternion>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m706126829_gshared (EmptyObserver_1_t1301068969 * __this, Quaternion_t1891715979  ___value0, const MethodInfo* method);
#define EmptyObserver_1_OnNext_m706126829(__this, ___value0, method) ((  void (*) (EmptyObserver_1_t1301068969 *, Quaternion_t1891715979 , const MethodInfo*))EmptyObserver_1_OnNext_m706126829_gshared)(__this, ___value0, method)
