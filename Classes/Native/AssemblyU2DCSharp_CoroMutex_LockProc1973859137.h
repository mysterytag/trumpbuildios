﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CoroMutex
struct CoroMutex_t686805814;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CoroMutex/LockProc
struct  LockProc_t1973859137  : public Il2CppObject
{
public:
	// CoroMutex CoroMutex/LockProc::parent
	CoroMutex_t686805814 * ___parent_0;
	// System.Boolean CoroMutex/LockProc::locked
	bool ___locked_1;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(LockProc_t1973859137, ___parent_0)); }
	inline CoroMutex_t686805814 * get_parent_0() const { return ___parent_0; }
	inline CoroMutex_t686805814 ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(CoroMutex_t686805814 * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier(&___parent_0, value);
	}

	inline static int32_t get_offset_of_locked_1() { return static_cast<int32_t>(offsetof(LockProc_t1973859137, ___locked_1)); }
	inline bool get_locked_1() const { return ___locked_1; }
	inline bool* get_address_of_locked_1() { return &___locked_1; }
	inline void set_locked_1(bool value)
	{
		___locked_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
