﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<UpdateUserStatistics>c__AnonStorey9A
struct U3CUpdateUserStatisticsU3Ec__AnonStorey9A_t3097803716;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<UpdateUserStatistics>c__AnonStorey9A::.ctor()
extern "C"  void U3CUpdateUserStatisticsU3Ec__AnonStorey9A__ctor_m2597942383 (U3CUpdateUserStatisticsU3Ec__AnonStorey9A_t3097803716 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<UpdateUserStatistics>c__AnonStorey9A::<>m__87(PlayFab.CallRequestContainer)
extern "C"  void U3CUpdateUserStatisticsU3Ec__AnonStorey9A_U3CU3Em__87_m3264304588 (U3CUpdateUserStatisticsU3Ec__AnonStorey9A_t3097803716 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
