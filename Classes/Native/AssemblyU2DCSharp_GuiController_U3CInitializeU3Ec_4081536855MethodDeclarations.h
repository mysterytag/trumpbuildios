﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GuiController/<Initialize>c__AnonStorey56/<Initialize>c__AnonStorey57
struct U3CInitializeU3Ec__AnonStorey57_t4081536855;

#include "codegen/il2cpp-codegen.h"

// System.Void GuiController/<Initialize>c__AnonStorey56/<Initialize>c__AnonStorey57::.ctor()
extern "C"  void U3CInitializeU3Ec__AnonStorey57__ctor_m569229061 (U3CInitializeU3Ec__AnonStorey57_t4081536855 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GuiController/<Initialize>c__AnonStorey56/<Initialize>c__AnonStorey57::<>m__39()
extern "C"  void U3CInitializeU3Ec__AnonStorey57_U3CU3Em__39_m3876389236 (U3CInitializeU3Ec__AnonStorey57_t4081536855 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GuiController/<Initialize>c__AnonStorey56/<Initialize>c__AnonStorey57::<>m__3A()
extern "C"  void U3CInitializeU3Ec__AnonStorey57_U3CU3Em__3A_m3876396924 (U3CInitializeU3Ec__AnonStorey57_t4081536855 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
