﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.ListObserver`1<UnityEngine.Vector3>
struct ListObserver_1_t3819947460;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.Vector3>>
struct ImmutableList_1_t2502565895;
// System.Exception
struct Exception_t1967233988;
// UniRx.IObserver`1<UnityEngine.Vector3>
struct IObserver_1_t1442361396;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Vector3>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m1623890684_gshared (ListObserver_1_t3819947460 * __this, ImmutableList_1_t2502565895 * ___observers0, const MethodInfo* method);
#define ListObserver_1__ctor_m1623890684(__this, ___observers0, method) ((  void (*) (ListObserver_1_t3819947460 *, ImmutableList_1_t2502565895 *, const MethodInfo*))ListObserver_1__ctor_m1623890684_gshared)(__this, ___observers0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Vector3>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m2484681780_gshared (ListObserver_1_t3819947460 * __this, const MethodInfo* method);
#define ListObserver_1_OnCompleted_m2484681780(__this, method) ((  void (*) (ListObserver_1_t3819947460 *, const MethodInfo*))ListObserver_1_OnCompleted_m2484681780_gshared)(__this, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Vector3>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m2070364001_gshared (ListObserver_1_t3819947460 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ListObserver_1_OnError_m2070364001(__this, ___error0, method) ((  void (*) (ListObserver_1_t3819947460 *, Exception_t1967233988 *, const MethodInfo*))ListObserver_1_OnError_m2070364001_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Vector3>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m1053427794_gshared (ListObserver_1_t3819947460 * __this, Vector3_t3525329789  ___value0, const MethodInfo* method);
#define ListObserver_1_OnNext_m1053427794(__this, ___value0, method) ((  void (*) (ListObserver_1_t3819947460 *, Vector3_t3525329789 , const MethodInfo*))ListObserver_1_OnNext_m1053427794_gshared)(__this, ___value0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UnityEngine.Vector3>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m2791623460_gshared (ListObserver_1_t3819947460 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Add_m2791623460(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t3819947460 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Add_m2791623460_gshared)(__this, ___observer0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UnityEngine.Vector3>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m2236190119_gshared (ListObserver_1_t3819947460 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Remove_m2236190119(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t3819947460 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Remove_m2236190119_gshared)(__this, ___observer0, method)
