﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.UploadProgressChangedEventHandler
struct UploadProgressChangedEventHandler_t1611049280;
// System.Object
struct Il2CppObject;
// System.Net.UploadProgressChangedEventArgs
struct UploadProgressChangedEventArgs_t1420200251;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "System_System_Net_UploadProgressChangedEventArgs1420200251.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Net.UploadProgressChangedEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void UploadProgressChangedEventHandler__ctor_m4161358796 (UploadProgressChangedEventHandler_t1611049280 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.UploadProgressChangedEventHandler::Invoke(System.Object,System.Net.UploadProgressChangedEventArgs)
extern "C"  void UploadProgressChangedEventHandler_Invoke_m2650283737 (UploadProgressChangedEventHandler_t1611049280 * __this, Il2CppObject * ___sender0, UploadProgressChangedEventArgs_t1420200251 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_UploadProgressChangedEventHandler_t1611049280(Il2CppObject* delegate, Il2CppObject * ___sender0, UploadProgressChangedEventArgs_t1420200251 * ___e1);
// System.IAsyncResult System.Net.UploadProgressChangedEventHandler::BeginInvoke(System.Object,System.Net.UploadProgressChangedEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UploadProgressChangedEventHandler_BeginInvoke_m1713599236 (UploadProgressChangedEventHandler_t1611049280 * __this, Il2CppObject * ___sender0, UploadProgressChangedEventArgs_t1420200251 * ___e1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.UploadProgressChangedEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void UploadProgressChangedEventHandler_EndInvoke_m1561494492 (UploadProgressChangedEventHandler_t1611049280 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
