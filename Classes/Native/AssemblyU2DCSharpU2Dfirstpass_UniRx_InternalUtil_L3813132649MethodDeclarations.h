﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.ListObserver`1<UnityEngine.Bounds>
struct ListObserver_1_t3813132649;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.Bounds>>
struct ImmutableList_1_t2495751084;
// System.Exception
struct Exception_t1967233988;
// UniRx.IObserver`1<UnityEngine.Bounds>
struct IObserver_1_t1435546585;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_Bounds3518514978.h"

// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Bounds>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m3379697367_gshared (ListObserver_1_t3813132649 * __this, ImmutableList_1_t2495751084 * ___observers0, const MethodInfo* method);
#define ListObserver_1__ctor_m3379697367(__this, ___observers0, method) ((  void (*) (ListObserver_1_t3813132649 *, ImmutableList_1_t2495751084 *, const MethodInfo*))ListObserver_1__ctor_m3379697367_gshared)(__this, ___observers0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Bounds>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m2484319439_gshared (ListObserver_1_t3813132649 * __this, const MethodInfo* method);
#define ListObserver_1_OnCompleted_m2484319439(__this, method) ((  void (*) (ListObserver_1_t3813132649 *, const MethodInfo*))ListObserver_1_OnCompleted_m2484319439_gshared)(__this, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Bounds>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m683845500_gshared (ListObserver_1_t3813132649 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ListObserver_1_OnError_m683845500(__this, ___error0, method) ((  void (*) (ListObserver_1_t3813132649 *, Exception_t1967233988 *, const MethodInfo*))ListObserver_1_OnError_m683845500_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Bounds>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m3086419565_gshared (ListObserver_1_t3813132649 * __this, Bounds_t3518514978  ___value0, const MethodInfo* method);
#define ListObserver_1_OnNext_m3086419565(__this, ___value0, method) ((  void (*) (ListObserver_1_t3813132649 *, Bounds_t3518514978 , const MethodInfo*))ListObserver_1_OnNext_m3086419565_gshared)(__this, ___value0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UnityEngine.Bounds>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m2923515917_gshared (ListObserver_1_t3813132649 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Add_m2923515917(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t3813132649 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Add_m2923515917_gshared)(__this, ___observer0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UnityEngine.Bounds>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m1549300766_gshared (ListObserver_1_t3813132649 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Remove_m1549300766(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t3813132649 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Remove_m1549300766_gshared)(__this, ___observer0, method)
