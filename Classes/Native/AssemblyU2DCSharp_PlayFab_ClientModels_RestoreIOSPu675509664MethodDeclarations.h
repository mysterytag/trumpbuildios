﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.RestoreIOSPurchasesResult
struct RestoreIOSPurchasesResult_t675509664;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.RestoreIOSPurchasesResult::.ctor()
extern "C"  void RestoreIOSPurchasesResult__ctor_m3961500297 (RestoreIOSPurchasesResult_t675509664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
