﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1/Subscription<UnityEngine.NetworkDisconnection>
struct Subscription_t2412974213;
// UniRx.Subject`1<UnityEngine.NetworkDisconnection>
struct Subject_1_t2276795015;
// UniRx.IObserver`1<UnityEngine.NetworkDisconnection>
struct IObserver_1_t2550759298;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Subject`1/Subscription<UnityEngine.NetworkDisconnection>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern "C"  void Subscription__ctor_m3858878238_gshared (Subscription_t2412974213 * __this, Subject_1_t2276795015 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method);
#define Subscription__ctor_m3858878238(__this, ___parent0, ___unsubscribeTarget1, method) ((  void (*) (Subscription_t2412974213 *, Subject_1_t2276795015 *, Il2CppObject*, const MethodInfo*))Subscription__ctor_m3858878238_gshared)(__this, ___parent0, ___unsubscribeTarget1, method)
// System.Void UniRx.Subject`1/Subscription<UnityEngine.NetworkDisconnection>::Dispose()
extern "C"  void Subscription_Dispose_m2635555864_gshared (Subscription_t2412974213 * __this, const MethodInfo* method);
#define Subscription_Dispose_m2635555864(__this, method) ((  void (*) (Subscription_t2412974213 *, const MethodInfo*))Subscription_Dispose_m2635555864_gshared)(__this, method)
