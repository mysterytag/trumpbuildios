﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<UnityEngine.EventSystems.BaseEventData>
struct Action_1_t3695556431;
// ZergEngineTools/<PressStream>c__AnonStorey14E
struct U3CPressStreamU3Ec__AnonStorey14E_t2542632690;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZergEngineTools/<PressStream>c__AnonStorey14E/<PressStream>c__AnonStorey14D
struct  U3CPressStreamU3Ec__AnonStorey14D_t2542632689  : public Il2CppObject
{
public:
	// System.Action`1<UnityEngine.EventSystems.BaseEventData> ZergEngineTools/<PressStream>c__AnonStorey14E/<PressStream>c__AnonStorey14D::reaction
	Action_1_t3695556431 * ___reaction_0;
	// ZergEngineTools/<PressStream>c__AnonStorey14E ZergEngineTools/<PressStream>c__AnonStorey14E/<PressStream>c__AnonStorey14D::<>f__ref$334
	U3CPressStreamU3Ec__AnonStorey14E_t2542632690 * ___U3CU3Ef__refU24334_1;

public:
	inline static int32_t get_offset_of_reaction_0() { return static_cast<int32_t>(offsetof(U3CPressStreamU3Ec__AnonStorey14D_t2542632689, ___reaction_0)); }
	inline Action_1_t3695556431 * get_reaction_0() const { return ___reaction_0; }
	inline Action_1_t3695556431 ** get_address_of_reaction_0() { return &___reaction_0; }
	inline void set_reaction_0(Action_1_t3695556431 * value)
	{
		___reaction_0 = value;
		Il2CppCodeGenWriteBarrier(&___reaction_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24334_1() { return static_cast<int32_t>(offsetof(U3CPressStreamU3Ec__AnonStorey14D_t2542632689, ___U3CU3Ef__refU24334_1)); }
	inline U3CPressStreamU3Ec__AnonStorey14E_t2542632690 * get_U3CU3Ef__refU24334_1() const { return ___U3CU3Ef__refU24334_1; }
	inline U3CPressStreamU3Ec__AnonStorey14E_t2542632690 ** get_address_of_U3CU3Ef__refU24334_1() { return &___U3CU3Ef__refU24334_1; }
	inline void set_U3CU3Ef__refU24334_1(U3CPressStreamU3Ec__AnonStorey14E_t2542632690 * value)
	{
		___U3CU3Ef__refU24334_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24334_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
