﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/RemoveFriendRequestCallback
struct RemoveFriendRequestCallback_t2222935858;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.RemoveFriendRequest
struct RemoveFriendRequest_t4041798621;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_RemoveFrien4041798621.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/RemoveFriendRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void RemoveFriendRequestCallback__ctor_m2484245185 (RemoveFriendRequestCallback_t2222935858 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/RemoveFriendRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.RemoveFriendRequest,System.Object)
extern "C"  void RemoveFriendRequestCallback_Invoke_m2352095285 (RemoveFriendRequestCallback_t2222935858 * __this, String_t* ___urlPath0, int32_t ___callId1, RemoveFriendRequest_t4041798621 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_RemoveFriendRequestCallback_t2222935858(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, RemoveFriendRequest_t4041798621 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/RemoveFriendRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.RemoveFriendRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * RemoveFriendRequestCallback_BeginInvoke_m3016543518 (RemoveFriendRequestCallback_t2222935858 * __this, String_t* ___urlPath0, int32_t ___callId1, RemoveFriendRequest_t4041798621 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/RemoveFriendRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void RemoveFriendRequestCallback_EndInvoke_m2656537937 (RemoveFriendRequestCallback_t2222935858 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
