﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GlobalStat/<PostInject>c__AnonStoreyE2/<PostInject>c__AnonStoreyE4
struct U3CPostInjectU3Ec__AnonStoreyE4_t980490757;

#include "codegen/il2cpp-codegen.h"

// System.Void GlobalStat/<PostInject>c__AnonStoreyE2/<PostInject>c__AnonStoreyE4::.ctor()
extern "C"  void U3CPostInjectU3Ec__AnonStoreyE4__ctor_m3421512474 (U3CPostInjectU3Ec__AnonStoreyE4_t980490757 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GlobalStat/<PostInject>c__AnonStoreyE2/<PostInject>c__AnonStoreyE4::<>m__116(System.Int64)
extern "C"  bool U3CPostInjectU3Ec__AnonStoreyE4_U3CU3Em__116_m386008699 (U3CPostInjectU3Ec__AnonStoreyE4_t980490757 * __this, int64_t ___l0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalStat/<PostInject>c__AnonStoreyE2/<PostInject>c__AnonStoreyE4::<>m__117(System.Int64)
extern "C"  void U3CPostInjectU3Ec__AnonStoreyE4_U3CU3Em__117_m929094408 (U3CPostInjectU3Ec__AnonStoreyE4_t980490757 * __this, int64_t ____0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
