﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.CredentialCache/CredentialCacheKey
struct CredentialCacheKey_t2749690132;
// System.Uri
struct Uri_t2776692961;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Uri2776692961.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Net.CredentialCache/CredentialCacheKey::.ctor(System.Uri,System.String)
extern "C"  void CredentialCacheKey__ctor_m2391819729 (CredentialCacheKey_t2749690132 * __this, Uri_t2776692961 * ___uriPrefix0, String_t* ___authType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.CredentialCache/CredentialCacheKey::get_Length()
extern "C"  int32_t CredentialCacheKey_get_Length_m3054526753 (CredentialCacheKey_t2749690132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.CredentialCache/CredentialCacheKey::get_AbsPath()
extern "C"  String_t* CredentialCacheKey_get_AbsPath_m2857234225 (CredentialCacheKey_t2749690132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri System.Net.CredentialCache/CredentialCacheKey::get_UriPrefix()
extern "C"  Uri_t2776692961 * CredentialCacheKey_get_UriPrefix_m4200746567 (CredentialCacheKey_t2749690132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.CredentialCache/CredentialCacheKey::get_AuthType()
extern "C"  String_t* CredentialCacheKey_get_AuthType_m3893816426 (CredentialCacheKey_t2749690132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.CredentialCache/CredentialCacheKey::GetHashCode()
extern "C"  int32_t CredentialCacheKey_GetHashCode_m2782141569 (CredentialCacheKey_t2749690132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.CredentialCache/CredentialCacheKey::Equals(System.Object)
extern "C"  bool CredentialCacheKey_Equals_m2605006121 (CredentialCacheKey_t2749690132 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.CredentialCache/CredentialCacheKey::ToString()
extern "C"  String_t* CredentialCacheKey_ToString_m4049508811 (CredentialCacheKey_t2749690132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
