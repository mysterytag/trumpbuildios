﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Subject`1<UnityEngine.EventSystems.BaseEventData>
struct Subject_1_t1190171050;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Triggers_Obser1883237895.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableSelectTrigger
struct  ObservableSelectTrigger_t1357966138  : public ObservableTriggerBase_t1883237895
{
public:
	// UniRx.Subject`1<UnityEngine.EventSystems.BaseEventData> UniRx.Triggers.ObservableSelectTrigger::onSelect
	Subject_1_t1190171050 * ___onSelect_8;

public:
	inline static int32_t get_offset_of_onSelect_8() { return static_cast<int32_t>(offsetof(ObservableSelectTrigger_t1357966138, ___onSelect_8)); }
	inline Subject_1_t1190171050 * get_onSelect_8() const { return ___onSelect_8; }
	inline Subject_1_t1190171050 ** get_address_of_onSelect_8() { return &___onSelect_8; }
	inline void set_onSelect_8(Subject_1_t1190171050 * value)
	{
		___onSelect_8 = value;
		Il2CppCodeGenWriteBarrier(&___onSelect_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
