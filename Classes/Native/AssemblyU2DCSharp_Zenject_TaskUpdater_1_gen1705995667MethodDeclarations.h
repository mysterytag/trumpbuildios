﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.TaskUpdater`1<System.Object>
struct TaskUpdater_1_t1705995667;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// System.Collections.Generic.IEnumerable`1<Zenject.TaskUpdater`1/TaskInfo<System.Object>>
struct IEnumerable_1_t3936662760;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<System.Object>>
struct LinkedList_1_t2804637950;
// Zenject.TaskUpdater`1/TaskInfo<System.Object>
struct TaskInfo_t1064508404;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void Zenject.TaskUpdater`1<System.Object>::.ctor(System.Action`1<TTask>)
extern "C"  void TaskUpdater_1__ctor_m1842795290_gshared (TaskUpdater_1_t1705995667 * __this, Action_1_t985559125 * ___updateFunc0, const MethodInfo* method);
#define TaskUpdater_1__ctor_m1842795290(__this, ___updateFunc0, method) ((  void (*) (TaskUpdater_1_t1705995667 *, Action_1_t985559125 *, const MethodInfo*))TaskUpdater_1__ctor_m1842795290_gshared)(__this, ___updateFunc0, method)
// System.Collections.Generic.IEnumerable`1<Zenject.TaskUpdater`1/TaskInfo<TTask>> Zenject.TaskUpdater`1<System.Object>::get_AllTasks()
extern "C"  Il2CppObject* TaskUpdater_1_get_AllTasks_m2200620774_gshared (TaskUpdater_1_t1705995667 * __this, const MethodInfo* method);
#define TaskUpdater_1_get_AllTasks_m2200620774(__this, method) ((  Il2CppObject* (*) (TaskUpdater_1_t1705995667 *, const MethodInfo*))TaskUpdater_1_get_AllTasks_m2200620774_gshared)(__this, method)
// System.Collections.Generic.IEnumerable`1<Zenject.TaskUpdater`1/TaskInfo<TTask>> Zenject.TaskUpdater`1<System.Object>::get_ActiveTasks()
extern "C"  Il2CppObject* TaskUpdater_1_get_ActiveTasks_m515292529_gshared (TaskUpdater_1_t1705995667 * __this, const MethodInfo* method);
#define TaskUpdater_1_get_ActiveTasks_m515292529(__this, method) ((  Il2CppObject* (*) (TaskUpdater_1_t1705995667 *, const MethodInfo*))TaskUpdater_1_get_ActiveTasks_m515292529_gshared)(__this, method)
// System.Void Zenject.TaskUpdater`1<System.Object>::AddTask(TTask,System.Int32)
extern "C"  void TaskUpdater_1_AddTask_m720801655_gshared (TaskUpdater_1_t1705995667 * __this, Il2CppObject * ___task0, int32_t ___priority1, const MethodInfo* method);
#define TaskUpdater_1_AddTask_m720801655(__this, ___task0, ___priority1, method) ((  void (*) (TaskUpdater_1_t1705995667 *, Il2CppObject *, int32_t, const MethodInfo*))TaskUpdater_1_AddTask_m720801655_gshared)(__this, ___task0, ___priority1, method)
// System.Void Zenject.TaskUpdater`1<System.Object>::AddTaskInternal(TTask,System.Int32)
extern "C"  void TaskUpdater_1_AddTaskInternal_m1657606612_gshared (TaskUpdater_1_t1705995667 * __this, Il2CppObject * ___task0, int32_t ___priority1, const MethodInfo* method);
#define TaskUpdater_1_AddTaskInternal_m1657606612(__this, ___task0, ___priority1, method) ((  void (*) (TaskUpdater_1_t1705995667 *, Il2CppObject *, int32_t, const MethodInfo*))TaskUpdater_1_AddTaskInternal_m1657606612_gshared)(__this, ___task0, ___priority1, method)
// System.Void Zenject.TaskUpdater`1<System.Object>::RemoveTask(TTask)
extern "C"  void TaskUpdater_1_RemoveTask_m1986106881_gshared (TaskUpdater_1_t1705995667 * __this, Il2CppObject * ___task0, const MethodInfo* method);
#define TaskUpdater_1_RemoveTask_m1986106881(__this, ___task0, method) ((  void (*) (TaskUpdater_1_t1705995667 *, Il2CppObject *, const MethodInfo*))TaskUpdater_1_RemoveTask_m1986106881_gshared)(__this, ___task0, method)
// System.Void Zenject.TaskUpdater`1<System.Object>::OnFrameStart()
extern "C"  void TaskUpdater_1_OnFrameStart_m1489365667_gshared (TaskUpdater_1_t1705995667 * __this, const MethodInfo* method);
#define TaskUpdater_1_OnFrameStart_m1489365667(__this, method) ((  void (*) (TaskUpdater_1_t1705995667 *, const MethodInfo*))TaskUpdater_1_OnFrameStart_m1489365667_gshared)(__this, method)
// System.Void Zenject.TaskUpdater`1<System.Object>::UpdateAll()
extern "C"  void TaskUpdater_1_UpdateAll_m611209579_gshared (TaskUpdater_1_t1705995667 * __this, const MethodInfo* method);
#define TaskUpdater_1_UpdateAll_m611209579(__this, method) ((  void (*) (TaskUpdater_1_t1705995667 *, const MethodInfo*))TaskUpdater_1_UpdateAll_m611209579_gshared)(__this, method)
// System.Void Zenject.TaskUpdater`1<System.Object>::UpdateRange(System.Int32,System.Int32)
extern "C"  void TaskUpdater_1_UpdateRange_m1084380479_gshared (TaskUpdater_1_t1705995667 * __this, int32_t ___minPriority0, int32_t ___maxPriority1, const MethodInfo* method);
#define TaskUpdater_1_UpdateRange_m1084380479(__this, ___minPriority0, ___maxPriority1, method) ((  void (*) (TaskUpdater_1_t1705995667 *, int32_t, int32_t, const MethodInfo*))TaskUpdater_1_UpdateRange_m1084380479_gshared)(__this, ___minPriority0, ___maxPriority1, method)
// System.Void Zenject.TaskUpdater`1<System.Object>::ClearRemovedTasks(System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<TTask>>)
extern "C"  void TaskUpdater_1_ClearRemovedTasks_m204003072_gshared (TaskUpdater_1_t1705995667 * __this, LinkedList_1_t2804637950 * ___tasks0, const MethodInfo* method);
#define TaskUpdater_1_ClearRemovedTasks_m204003072(__this, ___tasks0, method) ((  void (*) (TaskUpdater_1_t1705995667 *, LinkedList_1_t2804637950 *, const MethodInfo*))TaskUpdater_1_ClearRemovedTasks_m204003072_gshared)(__this, ___tasks0, method)
// System.Void Zenject.TaskUpdater`1<System.Object>::AddQueuedTasks()
extern "C"  void TaskUpdater_1_AddQueuedTasks_m2029046249_gshared (TaskUpdater_1_t1705995667 * __this, const MethodInfo* method);
#define TaskUpdater_1_AddQueuedTasks_m2029046249(__this, method) ((  void (*) (TaskUpdater_1_t1705995667 *, const MethodInfo*))TaskUpdater_1_AddQueuedTasks_m2029046249_gshared)(__this, method)
// System.Void Zenject.TaskUpdater`1<System.Object>::InsertTaskSorted(Zenject.TaskUpdater`1/TaskInfo<TTask>)
extern "C"  void TaskUpdater_1_InsertTaskSorted_m3714982387_gshared (TaskUpdater_1_t1705995667 * __this, TaskInfo_t1064508404 * ___task0, const MethodInfo* method);
#define TaskUpdater_1_InsertTaskSorted_m3714982387(__this, ___task0, method) ((  void (*) (TaskUpdater_1_t1705995667 *, TaskInfo_t1064508404 *, const MethodInfo*))TaskUpdater_1_InsertTaskSorted_m3714982387_gshared)(__this, ___task0, method)
// TTask Zenject.TaskUpdater`1<System.Object>::<AddTaskInternal>m__2E5(Zenject.TaskUpdater`1/TaskInfo<TTask>)
extern "C"  Il2CppObject * TaskUpdater_1_U3CAddTaskInternalU3Em__2E5_m2398513880_gshared (Il2CppObject * __this /* static, unused */, TaskInfo_t1064508404 * ___x0, const MethodInfo* method);
#define TaskUpdater_1_U3CAddTaskInternalU3Em__2E5_m2398513880(__this /* static, unused */, ___x0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, TaskInfo_t1064508404 *, const MethodInfo*))TaskUpdater_1_U3CAddTaskInternalU3Em__2E5_m2398513880_gshared)(__this /* static, unused */, ___x0, method)
