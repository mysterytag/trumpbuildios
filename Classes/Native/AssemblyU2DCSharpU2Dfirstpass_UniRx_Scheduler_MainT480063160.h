﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.Object>
struct Action_1_t985559125;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Scheduler/MainThreadScheduler
struct  MainThreadScheduler_t480063160  : public Il2CppObject
{
public:
	// System.Action`1<System.Object> UniRx.Scheduler/MainThreadScheduler::scheduleAction
	Action_1_t985559125 * ___scheduleAction_0;

public:
	inline static int32_t get_offset_of_scheduleAction_0() { return static_cast<int32_t>(offsetof(MainThreadScheduler_t480063160, ___scheduleAction_0)); }
	inline Action_1_t985559125 * get_scheduleAction_0() const { return ___scheduleAction_0; }
	inline Action_1_t985559125 ** get_address_of_scheduleAction_0() { return &___scheduleAction_0; }
	inline void set_scheduleAction_0(Action_1_t985559125 * value)
	{
		___scheduleAction_0 = value;
		Il2CppCodeGenWriteBarrier(&___scheduleAction_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
