﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// CellReactiveApi/<SelectMany>c__AnonStorey119`3<System.Object,System.Object,System.Object>
struct U3CSelectManyU3Ec__AnonStorey119_3_t1259363341;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CellReactiveApi/<SelectMany>c__AnonStorey119`3/<SelectMany>c__AnonStorey11A`3<System.Object,System.Object,System.Object>
struct  U3CSelectManyU3Ec__AnonStorey11A_3_t2895282581  : public Il2CppObject
{
public:
	// T CellReactiveApi/<SelectMany>c__AnonStorey119`3/<SelectMany>c__AnonStorey11A`3::x
	Il2CppObject * ___x_0;
	// CellReactiveApi/<SelectMany>c__AnonStorey119`3<T,TC,TR> CellReactiveApi/<SelectMany>c__AnonStorey119`3/<SelectMany>c__AnonStorey11A`3::<>f__ref$281
	U3CSelectManyU3Ec__AnonStorey119_3_t1259363341 * ___U3CU3Ef__refU24281_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(U3CSelectManyU3Ec__AnonStorey11A_3_t2895282581, ___x_0)); }
	inline Il2CppObject * get_x_0() const { return ___x_0; }
	inline Il2CppObject ** get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(Il2CppObject * value)
	{
		___x_0 = value;
		Il2CppCodeGenWriteBarrier(&___x_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24281_1() { return static_cast<int32_t>(offsetof(U3CSelectManyU3Ec__AnonStorey11A_3_t2895282581, ___U3CU3Ef__refU24281_1)); }
	inline U3CSelectManyU3Ec__AnonStorey119_3_t1259363341 * get_U3CU3Ef__refU24281_1() const { return ___U3CU3Ef__refU24281_1; }
	inline U3CSelectManyU3Ec__AnonStorey119_3_t1259363341 ** get_address_of_U3CU3Ef__refU24281_1() { return &___U3CU3Ef__refU24281_1; }
	inline void set_U3CU3Ef__refU24281_1(U3CSelectManyU3Ec__AnonStorey119_3_t1259363341 * value)
	{
		___U3CU3Ef__refU24281_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24281_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
