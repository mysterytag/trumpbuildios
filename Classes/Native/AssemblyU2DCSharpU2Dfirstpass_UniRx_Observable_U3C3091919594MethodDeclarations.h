﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/<FromAsyncPattern>c__AnonStorey3F`1<UniRx.Unit>
struct U3CFromAsyncPatternU3Ec__AnonStorey3F_1_t3091919594;
// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Observable/<FromAsyncPattern>c__AnonStorey3F`1<UniRx.Unit>::.ctor()
extern "C"  void U3CFromAsyncPatternU3Ec__AnonStorey3F_1__ctor_m3315155734_gshared (U3CFromAsyncPatternU3Ec__AnonStorey3F_1_t3091919594 * __this, const MethodInfo* method);
#define U3CFromAsyncPatternU3Ec__AnonStorey3F_1__ctor_m3315155734(__this, method) ((  void (*) (U3CFromAsyncPatternU3Ec__AnonStorey3F_1_t3091919594 *, const MethodInfo*))U3CFromAsyncPatternU3Ec__AnonStorey3F_1__ctor_m3315155734_gshared)(__this, method)
// UniRx.IObservable`1<TResult> UniRx.Observable/<FromAsyncPattern>c__AnonStorey3F`1<UniRx.Unit>::<>m__40()
extern "C"  Il2CppObject* U3CFromAsyncPatternU3Ec__AnonStorey3F_1_U3CU3Em__40_m112548026_gshared (U3CFromAsyncPatternU3Ec__AnonStorey3F_1_t3091919594 * __this, const MethodInfo* method);
#define U3CFromAsyncPatternU3Ec__AnonStorey3F_1_U3CU3Em__40_m112548026(__this, method) ((  Il2CppObject* (*) (U3CFromAsyncPatternU3Ec__AnonStorey3F_1_t3091919594 *, const MethodInfo*))U3CFromAsyncPatternU3Ec__AnonStorey3F_1_U3CU3Em__40_m112548026_gshared)(__this, method)
