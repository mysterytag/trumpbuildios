﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.WhenAllObservable`1<System.Object>
struct WhenAllObservable_1_t2782777313;
// UniRx.IObservable`1<System.Object>[]
struct IObservable_1U5BU5D_t2205425841;
// System.Collections.Generic.IEnumerable`1<UniRx.IObservable`1<System.Object>>
struct IEnumerable_1_t3468059140;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object[]>
struct IObserver_1_t2223522676;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.WhenAllObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>[])
extern "C"  void WhenAllObservable_1__ctor_m265851796_gshared (WhenAllObservable_1_t2782777313 * __this, IObservable_1U5BU5D_t2205425841* ___sources0, const MethodInfo* method);
#define WhenAllObservable_1__ctor_m265851796(__this, ___sources0, method) ((  void (*) (WhenAllObservable_1_t2782777313 *, IObservable_1U5BU5D_t2205425841*, const MethodInfo*))WhenAllObservable_1__ctor_m265851796_gshared)(__this, ___sources0, method)
// System.Void UniRx.Operators.WhenAllObservable`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<UniRx.IObservable`1<T>>)
extern "C"  void WhenAllObservable_1__ctor_m2775679043_gshared (WhenAllObservable_1_t2782777313 * __this, Il2CppObject* ___sources0, const MethodInfo* method);
#define WhenAllObservable_1__ctor_m2775679043(__this, ___sources0, method) ((  void (*) (WhenAllObservable_1_t2782777313 *, Il2CppObject*, const MethodInfo*))WhenAllObservable_1__ctor_m2775679043_gshared)(__this, ___sources0, method)
// System.IDisposable UniRx.Operators.WhenAllObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T[]>,System.IDisposable)
extern "C"  Il2CppObject * WhenAllObservable_1_SubscribeCore_m1880134789_gshared (WhenAllObservable_1_t2782777313 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define WhenAllObservable_1_SubscribeCore_m1880134789(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (WhenAllObservable_1_t2782777313 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))WhenAllObservable_1_SubscribeCore_m1880134789_gshared)(__this, ___observer0, ___cancel1, method)
