﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ADSFlyer
struct ADSFlyer_t4278433200;

#include "codegen/il2cpp-codegen.h"

// System.Void ADSFlyer::.ctor()
extern "C"  void ADSFlyer__ctor_m475426539 (ADSFlyer_t4278433200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ADSFlyer::Start()
extern "C"  void ADSFlyer_Start_m3717531627 (ADSFlyer_t4278433200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ADSFlyer::Update()
extern "C"  void ADSFlyer_Update_m3580182914 (ADSFlyer_t4278433200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
