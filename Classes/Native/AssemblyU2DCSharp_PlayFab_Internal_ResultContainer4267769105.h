﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.String>>
struct Dictionary_2_t3403145775;
// PlayFab.ClientModels.GetCharacterLeaderboardResult
struct GetCharacterLeaderboardResult_t3315494327;
// System.Object[]
struct ObjectU5BU5D_t11523773;

#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Nullable_1_gen1438485399.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.Internal.ResultContainer`1<PlayFab.ClientModels.GetCharacterLeaderboardResult>
struct  ResultContainer_1_t4267769105  : public Il2CppObject
{
public:
	// System.Int32 PlayFab.Internal.ResultContainer`1::code
	int32_t ___code_0;
	// System.String PlayFab.Internal.ResultContainer`1::status
	String_t* ___status_1;
	// System.Nullable`1<System.Int32> PlayFab.Internal.ResultContainer`1::errorCode
	Nullable_1_t1438485399  ___errorCode_2;
	// System.String PlayFab.Internal.ResultContainer`1::errorMessage
	String_t* ___errorMessage_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.String>> PlayFab.Internal.ResultContainer`1::errorDetails
	Dictionary_2_t3403145775 * ___errorDetails_4;
	// TResultType PlayFab.Internal.ResultContainer`1::data
	GetCharacterLeaderboardResult_t3315494327 * ___data_5;

public:
	inline static int32_t get_offset_of_code_0() { return static_cast<int32_t>(offsetof(ResultContainer_1_t4267769105, ___code_0)); }
	inline int32_t get_code_0() const { return ___code_0; }
	inline int32_t* get_address_of_code_0() { return &___code_0; }
	inline void set_code_0(int32_t value)
	{
		___code_0 = value;
	}

	inline static int32_t get_offset_of_status_1() { return static_cast<int32_t>(offsetof(ResultContainer_1_t4267769105, ___status_1)); }
	inline String_t* get_status_1() const { return ___status_1; }
	inline String_t** get_address_of_status_1() { return &___status_1; }
	inline void set_status_1(String_t* value)
	{
		___status_1 = value;
		Il2CppCodeGenWriteBarrier(&___status_1, value);
	}

	inline static int32_t get_offset_of_errorCode_2() { return static_cast<int32_t>(offsetof(ResultContainer_1_t4267769105, ___errorCode_2)); }
	inline Nullable_1_t1438485399  get_errorCode_2() const { return ___errorCode_2; }
	inline Nullable_1_t1438485399 * get_address_of_errorCode_2() { return &___errorCode_2; }
	inline void set_errorCode_2(Nullable_1_t1438485399  value)
	{
		___errorCode_2 = value;
	}

	inline static int32_t get_offset_of_errorMessage_3() { return static_cast<int32_t>(offsetof(ResultContainer_1_t4267769105, ___errorMessage_3)); }
	inline String_t* get_errorMessage_3() const { return ___errorMessage_3; }
	inline String_t** get_address_of_errorMessage_3() { return &___errorMessage_3; }
	inline void set_errorMessage_3(String_t* value)
	{
		___errorMessage_3 = value;
		Il2CppCodeGenWriteBarrier(&___errorMessage_3, value);
	}

	inline static int32_t get_offset_of_errorDetails_4() { return static_cast<int32_t>(offsetof(ResultContainer_1_t4267769105, ___errorDetails_4)); }
	inline Dictionary_2_t3403145775 * get_errorDetails_4() const { return ___errorDetails_4; }
	inline Dictionary_2_t3403145775 ** get_address_of_errorDetails_4() { return &___errorDetails_4; }
	inline void set_errorDetails_4(Dictionary_2_t3403145775 * value)
	{
		___errorDetails_4 = value;
		Il2CppCodeGenWriteBarrier(&___errorDetails_4, value);
	}

	inline static int32_t get_offset_of_data_5() { return static_cast<int32_t>(offsetof(ResultContainer_1_t4267769105, ___data_5)); }
	inline GetCharacterLeaderboardResult_t3315494327 * get_data_5() const { return ___data_5; }
	inline GetCharacterLeaderboardResult_t3315494327 ** get_address_of_data_5() { return &___data_5; }
	inline void set_data_5(GetCharacterLeaderboardResult_t3315494327 * value)
	{
		___data_5 = value;
		Il2CppCodeGenWriteBarrier(&___data_5, value);
	}
};

struct ResultContainer_1_t4267769105_StaticFields
{
public:
	// System.Object[] PlayFab.Internal.ResultContainer`1::_invokeParams
	ObjectU5BU5D_t11523773* ____invokeParams_6;

public:
	inline static int32_t get_offset_of__invokeParams_6() { return static_cast<int32_t>(offsetof(ResultContainer_1_t4267769105_StaticFields, ____invokeParams_6)); }
	inline ObjectU5BU5D_t11523773* get__invokeParams_6() const { return ____invokeParams_6; }
	inline ObjectU5BU5D_t11523773** get_address_of__invokeParams_6() { return &____invokeParams_6; }
	inline void set__invokeParams_6(ObjectU5BU5D_t11523773* value)
	{
		____invokeParams_6 = value;
		Il2CppCodeGenWriteBarrier(&____invokeParams_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
