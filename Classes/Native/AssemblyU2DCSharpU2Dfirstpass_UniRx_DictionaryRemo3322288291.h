﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;

#include "mscorlib_System_ValueType4014882752.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>
struct  DictionaryRemoveEvent_2_t3322288291 
{
public:
	// TKey UniRx.DictionaryRemoveEvent`2::<Key>k__BackingField
	Il2CppObject * ___U3CKeyU3Ek__BackingField_0;
	// TValue UniRx.DictionaryRemoveEvent`2::<Value>k__BackingField
	Il2CppObject * ___U3CValueU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CKeyU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DictionaryRemoveEvent_2_t3322288291, ___U3CKeyU3Ek__BackingField_0)); }
	inline Il2CppObject * get_U3CKeyU3Ek__BackingField_0() const { return ___U3CKeyU3Ek__BackingField_0; }
	inline Il2CppObject ** get_address_of_U3CKeyU3Ek__BackingField_0() { return &___U3CKeyU3Ek__BackingField_0; }
	inline void set_U3CKeyU3Ek__BackingField_0(Il2CppObject * value)
	{
		___U3CKeyU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CKeyU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DictionaryRemoveEvent_2_t3322288291, ___U3CValueU3Ek__BackingField_1)); }
	inline Il2CppObject * get_U3CValueU3Ek__BackingField_1() const { return ___U3CValueU3Ek__BackingField_1; }
	inline Il2CppObject ** get_address_of_U3CValueU3Ek__BackingField_1() { return &___U3CValueU3Ek__BackingField_1; }
	inline void set_U3CValueU3Ek__BackingField_1(Il2CppObject * value)
	{
		___U3CValueU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CValueU3Ek__BackingField_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
