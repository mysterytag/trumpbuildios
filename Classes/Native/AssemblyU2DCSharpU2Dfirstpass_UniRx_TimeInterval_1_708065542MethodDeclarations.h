﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_TimeInterval_1_708065542.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_TimeSpan763862892.h"

// System.Void UniRx.TimeInterval`1<System.Object>::.ctor(T,System.TimeSpan)
extern "C"  void TimeInterval_1__ctor_m421673278_gshared (TimeInterval_1_t708065542 * __this, Il2CppObject * ___value0, TimeSpan_t763862892  ___interval1, const MethodInfo* method);
#define TimeInterval_1__ctor_m421673278(__this, ___value0, ___interval1, method) ((  void (*) (TimeInterval_1_t708065542 *, Il2CppObject *, TimeSpan_t763862892 , const MethodInfo*))TimeInterval_1__ctor_m421673278_gshared)(__this, ___value0, ___interval1, method)
// T UniRx.TimeInterval`1<System.Object>::get_Value()
extern "C"  Il2CppObject * TimeInterval_1_get_Value_m1281272987_gshared (TimeInterval_1_t708065542 * __this, const MethodInfo* method);
#define TimeInterval_1_get_Value_m1281272987(__this, method) ((  Il2CppObject * (*) (TimeInterval_1_t708065542 *, const MethodInfo*))TimeInterval_1_get_Value_m1281272987_gshared)(__this, method)
// System.TimeSpan UniRx.TimeInterval`1<System.Object>::get_Interval()
extern "C"  TimeSpan_t763862892  TimeInterval_1_get_Interval_m2089374975_gshared (TimeInterval_1_t708065542 * __this, const MethodInfo* method);
#define TimeInterval_1_get_Interval_m2089374975(__this, method) ((  TimeSpan_t763862892  (*) (TimeInterval_1_t708065542 *, const MethodInfo*))TimeInterval_1_get_Interval_m2089374975_gshared)(__this, method)
// System.Boolean UniRx.TimeInterval`1<System.Object>::Equals(UniRx.TimeInterval`1<T>)
extern "C"  bool TimeInterval_1_Equals_m4095380182_gshared (TimeInterval_1_t708065542 * __this, TimeInterval_1_t708065542  ___other0, const MethodInfo* method);
#define TimeInterval_1_Equals_m4095380182(__this, ___other0, method) ((  bool (*) (TimeInterval_1_t708065542 *, TimeInterval_1_t708065542 , const MethodInfo*))TimeInterval_1_Equals_m4095380182_gshared)(__this, ___other0, method)
// System.Boolean UniRx.TimeInterval`1<System.Object>::Equals(System.Object)
extern "C"  bool TimeInterval_1_Equals_m967181635_gshared (TimeInterval_1_t708065542 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define TimeInterval_1_Equals_m967181635(__this, ___obj0, method) ((  bool (*) (TimeInterval_1_t708065542 *, Il2CppObject *, const MethodInfo*))TimeInterval_1_Equals_m967181635_gshared)(__this, ___obj0, method)
// System.Int32 UniRx.TimeInterval`1<System.Object>::GetHashCode()
extern "C"  int32_t TimeInterval_1_GetHashCode_m3997542363_gshared (TimeInterval_1_t708065542 * __this, const MethodInfo* method);
#define TimeInterval_1_GetHashCode_m3997542363(__this, method) ((  int32_t (*) (TimeInterval_1_t708065542 *, const MethodInfo*))TimeInterval_1_GetHashCode_m3997542363_gshared)(__this, method)
// System.String UniRx.TimeInterval`1<System.Object>::ToString()
extern "C"  String_t* TimeInterval_1_ToString_m3288404663_gshared (TimeInterval_1_t708065542 * __this, const MethodInfo* method);
#define TimeInterval_1_ToString_m3288404663(__this, method) ((  String_t* (*) (TimeInterval_1_t708065542 *, const MethodInfo*))TimeInterval_1_ToString_m3288404663_gshared)(__this, method)
// System.Boolean UniRx.TimeInterval`1<System.Object>::op_Equality(UniRx.TimeInterval`1<T>,UniRx.TimeInterval`1<T>)
extern "C"  bool TimeInterval_1_op_Equality_m990693888_gshared (Il2CppObject * __this /* static, unused */, TimeInterval_1_t708065542  ___first0, TimeInterval_1_t708065542  ___second1, const MethodInfo* method);
#define TimeInterval_1_op_Equality_m990693888(__this /* static, unused */, ___first0, ___second1, method) ((  bool (*) (Il2CppObject * /* static, unused */, TimeInterval_1_t708065542 , TimeInterval_1_t708065542 , const MethodInfo*))TimeInterval_1_op_Equality_m990693888_gshared)(__this /* static, unused */, ___first0, ___second1, method)
// System.Boolean UniRx.TimeInterval`1<System.Object>::op_Inequality(UniRx.TimeInterval`1<T>,UniRx.TimeInterval`1<T>)
extern "C"  bool TimeInterval_1_op_Inequality_m2461346747_gshared (Il2CppObject * __this /* static, unused */, TimeInterval_1_t708065542  ___first0, TimeInterval_1_t708065542  ___second1, const MethodInfo* method);
#define TimeInterval_1_op_Inequality_m2461346747(__this /* static, unused */, ___first0, ___second1, method) ((  bool (*) (Il2CppObject * /* static, unused */, TimeInterval_1_t708065542 , TimeInterval_1_t708065542 , const MethodInfo*))TimeInterval_1_op_Inequality_m2461346747_gshared)(__this /* static, unused */, ___first0, ___second1, method)
