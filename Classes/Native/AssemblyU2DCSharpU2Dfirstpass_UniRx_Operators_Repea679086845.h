﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.IEnumerable`1<UniRx.IObservable`1<System.Object>>
struct IEnumerable_1_t3468059140;
// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;
// UnityEngine.GameObject
struct GameObject_t4012695102;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4196218687.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.RepeatUntilObservable`1<System.Object>
struct  RepeatUntilObservable_1_t679086845  : public OperatorObservableBase_1_t4196218687
{
public:
	// System.Collections.Generic.IEnumerable`1<UniRx.IObservable`1<T>> UniRx.Operators.RepeatUntilObservable`1::sources
	Il2CppObject* ___sources_1;
	// UniRx.IObservable`1<UniRx.Unit> UniRx.Operators.RepeatUntilObservable`1::trigger
	Il2CppObject* ___trigger_2;
	// UnityEngine.GameObject UniRx.Operators.RepeatUntilObservable`1::lifeTimeChecker
	GameObject_t4012695102 * ___lifeTimeChecker_3;

public:
	inline static int32_t get_offset_of_sources_1() { return static_cast<int32_t>(offsetof(RepeatUntilObservable_1_t679086845, ___sources_1)); }
	inline Il2CppObject* get_sources_1() const { return ___sources_1; }
	inline Il2CppObject** get_address_of_sources_1() { return &___sources_1; }
	inline void set_sources_1(Il2CppObject* value)
	{
		___sources_1 = value;
		Il2CppCodeGenWriteBarrier(&___sources_1, value);
	}

	inline static int32_t get_offset_of_trigger_2() { return static_cast<int32_t>(offsetof(RepeatUntilObservable_1_t679086845, ___trigger_2)); }
	inline Il2CppObject* get_trigger_2() const { return ___trigger_2; }
	inline Il2CppObject** get_address_of_trigger_2() { return &___trigger_2; }
	inline void set_trigger_2(Il2CppObject* value)
	{
		___trigger_2 = value;
		Il2CppCodeGenWriteBarrier(&___trigger_2, value);
	}

	inline static int32_t get_offset_of_lifeTimeChecker_3() { return static_cast<int32_t>(offsetof(RepeatUntilObservable_1_t679086845, ___lifeTimeChecker_3)); }
	inline GameObject_t4012695102 * get_lifeTimeChecker_3() const { return ___lifeTimeChecker_3; }
	inline GameObject_t4012695102 ** get_address_of_lifeTimeChecker_3() { return &___lifeTimeChecker_3; }
	inline void set_lifeTimeChecker_3(GameObject_t4012695102 * value)
	{
		___lifeTimeChecker_3 = value;
		Il2CppCodeGenWriteBarrier(&___lifeTimeChecker_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
