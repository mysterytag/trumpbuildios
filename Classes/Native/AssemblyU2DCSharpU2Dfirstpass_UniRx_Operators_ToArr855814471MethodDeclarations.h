﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ToArrayObservable`1/ToArray<System.Object>
struct ToArray_t855814471;
// UniRx.IObserver`1<System.Object[]>
struct IObserver_1_t2223522676;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.ToArrayObservable`1/ToArray<System.Object>::.ctor(UniRx.IObserver`1<TSource[]>,System.IDisposable)
extern "C"  void ToArray__ctor_m90900848_gshared (ToArray_t855814471 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define ToArray__ctor_m90900848(__this, ___observer0, ___cancel1, method) ((  void (*) (ToArray_t855814471 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))ToArray__ctor_m90900848_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.ToArrayObservable`1/ToArray<System.Object>::OnNext(TSource)
extern "C"  void ToArray_OnNext_m736148330_gshared (ToArray_t855814471 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ToArray_OnNext_m736148330(__this, ___value0, method) ((  void (*) (ToArray_t855814471 *, Il2CppObject *, const MethodInfo*))ToArray_OnNext_m736148330_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.ToArrayObservable`1/ToArray<System.Object>::OnError(System.Exception)
extern "C"  void ToArray_OnError_m2782772820_gshared (ToArray_t855814471 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ToArray_OnError_m2782772820(__this, ___error0, method) ((  void (*) (ToArray_t855814471 *, Exception_t1967233988 *, const MethodInfo*))ToArray_OnError_m2782772820_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.ToArrayObservable`1/ToArray<System.Object>::OnCompleted()
extern "C"  void ToArray_OnCompleted_m3602403239_gshared (ToArray_t855814471 * __this, const MethodInfo* method);
#define ToArray_OnCompleted_m3602403239(__this, method) ((  void (*) (ToArray_t855814471 *, const MethodInfo*))ToArray_OnCompleted_m3602403239_gshared)(__this, method)
