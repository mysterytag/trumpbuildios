﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RateTextEnabler
struct RateTextEnabler_t1261398082;

#include "codegen/il2cpp-codegen.h"

// System.Void RateTextEnabler::.ctor()
extern "C"  void RateTextEnabler__ctor_m2092730729 (RateTextEnabler_t1261398082 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RateTextEnabler::Start()
extern "C"  void RateTextEnabler_Start_m1039868521 (RateTextEnabler_t1261398082 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RateTextEnabler::Update()
extern "C"  void RateTextEnabler_Update_m2177005252 (RateTextEnabler_t1261398082 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
