﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetUserCombinedInfoRequest
struct GetUserCombinedInfoRequest_t3431761899;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen3097043249.h"

// System.Void PlayFab.ClientModels.GetUserCombinedInfoRequest::.ctor()
extern "C"  void GetUserCombinedInfoRequest__ctor_m3517944882 (GetUserCombinedInfoRequest_t3431761899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GetUserCombinedInfoRequest::get_PlayFabId()
extern "C"  String_t* GetUserCombinedInfoRequest_get_PlayFabId_m309528888 (GetUserCombinedInfoRequest_t3431761899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetUserCombinedInfoRequest::set_PlayFabId(System.String)
extern "C"  void GetUserCombinedInfoRequest_set_PlayFabId_m1550776961 (GetUserCombinedInfoRequest_t3431761899 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GetUserCombinedInfoRequest::get_Username()
extern "C"  String_t* GetUserCombinedInfoRequest_get_Username_m610182958 (GetUserCombinedInfoRequest_t3431761899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetUserCombinedInfoRequest::set_Username(System.String)
extern "C"  void GetUserCombinedInfoRequest_set_Username_m410473981 (GetUserCombinedInfoRequest_t3431761899 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GetUserCombinedInfoRequest::get_Email()
extern "C"  String_t* GetUserCombinedInfoRequest_get_Email_m2038835750 (GetUserCombinedInfoRequest_t3431761899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetUserCombinedInfoRequest::set_Email(System.String)
extern "C"  void GetUserCombinedInfoRequest_set_Email_m1483784147 (GetUserCombinedInfoRequest_t3431761899 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GetUserCombinedInfoRequest::get_TitleDisplayName()
extern "C"  String_t* GetUserCombinedInfoRequest_get_TitleDisplayName_m3669409933 (GetUserCombinedInfoRequest_t3431761899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetUserCombinedInfoRequest::set_TitleDisplayName(System.String)
extern "C"  void GetUserCombinedInfoRequest_set_TitleDisplayName_m1773942270 (GetUserCombinedInfoRequest_t3431761899 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.GetUserCombinedInfoRequest::get_GetAccountInfo()
extern "C"  Nullable_1_t3097043249  GetUserCombinedInfoRequest_get_GetAccountInfo_m2409247799 (GetUserCombinedInfoRequest_t3431761899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetUserCombinedInfoRequest::set_GetAccountInfo(System.Nullable`1<System.Boolean>)
extern "C"  void GetUserCombinedInfoRequest_set_GetAccountInfo_m2619418964 (GetUserCombinedInfoRequest_t3431761899 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.GetUserCombinedInfoRequest::get_GetInventory()
extern "C"  Nullable_1_t3097043249  GetUserCombinedInfoRequest_get_GetInventory_m2281453464 (GetUserCombinedInfoRequest_t3431761899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetUserCombinedInfoRequest::set_GetInventory(System.Nullable`1<System.Boolean>)
extern "C"  void GetUserCombinedInfoRequest_set_GetInventory_m4180974291 (GetUserCombinedInfoRequest_t3431761899 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.GetUserCombinedInfoRequest::get_GetVirtualCurrency()
extern "C"  Nullable_1_t3097043249  GetUserCombinedInfoRequest_get_GetVirtualCurrency_m3644900920 (GetUserCombinedInfoRequest_t3431761899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetUserCombinedInfoRequest::set_GetVirtualCurrency(System.Nullable`1<System.Boolean>)
extern "C"  void GetUserCombinedInfoRequest_set_GetVirtualCurrency_m4098073075 (GetUserCombinedInfoRequest_t3431761899 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.GetUserCombinedInfoRequest::get_GetUserData()
extern "C"  Nullable_1_t3097043249  GetUserCombinedInfoRequest_get_GetUserData_m818336315 (GetUserCombinedInfoRequest_t3431761899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetUserCombinedInfoRequest::set_GetUserData(System.Nullable`1<System.Boolean>)
extern "C"  void GetUserCombinedInfoRequest_set_GetUserData_m3368863146 (GetUserCombinedInfoRequest_t3431761899 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.GetUserCombinedInfoRequest::get_UserDataKeys()
extern "C"  List_1_t1765447871 * GetUserCombinedInfoRequest_get_UserDataKeys_m4138181939 (GetUserCombinedInfoRequest_t3431761899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetUserCombinedInfoRequest::set_UserDataKeys(System.Collections.Generic.List`1<System.String>)
extern "C"  void GetUserCombinedInfoRequest_set_UserDataKeys_m3378071148 (GetUserCombinedInfoRequest_t3431761899 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.GetUserCombinedInfoRequest::get_GetReadOnlyData()
extern "C"  Nullable_1_t3097043249  GetUserCombinedInfoRequest_get_GetReadOnlyData_m3967930642 (GetUserCombinedInfoRequest_t3431761899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetUserCombinedInfoRequest::set_GetReadOnlyData(System.Nullable`1<System.Boolean>)
extern "C"  void GetUserCombinedInfoRequest_set_GetReadOnlyData_m1707722547 (GetUserCombinedInfoRequest_t3431761899 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.GetUserCombinedInfoRequest::get_ReadOnlyDataKeys()
extern "C"  List_1_t1765447871 * GetUserCombinedInfoRequest_get_ReadOnlyDataKeys_m1165840010 (GetUserCombinedInfoRequest_t3431761899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetUserCombinedInfoRequest::set_ReadOnlyDataKeys(System.Collections.Generic.List`1<System.String>)
extern "C"  void GetUserCombinedInfoRequest_set_ReadOnlyDataKeys_m1903770435 (GetUserCombinedInfoRequest_t3431761899 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
