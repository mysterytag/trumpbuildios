﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ValueCell
struct ValueCell_t1237749331;

#include "codegen/il2cpp-codegen.h"

// System.Void ValueCell::.ctor(System.Int64)
extern "C"  void ValueCell__ctor_m2890842442 (ValueCell_t1237749331 * __this, int64_t ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ValueCell::.ctor()
extern "C"  void ValueCell__ctor_m3265524856 (ValueCell_t1237749331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
