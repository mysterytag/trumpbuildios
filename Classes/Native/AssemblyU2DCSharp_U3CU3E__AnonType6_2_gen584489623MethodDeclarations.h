﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// <>__AnonType6`2<System.Object,System.Object>
struct U3CU3E__AnonType6_2_t584489623;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void <>__AnonType6`2<System.Object,System.Object>::.ctor(<money>__T,<level>__T)
extern "C"  void U3CU3E__AnonType6_2__ctor_m3585939793_gshared (U3CU3E__AnonType6_2_t584489623 * __this, Il2CppObject * ___money0, Il2CppObject * ___level1, const MethodInfo* method);
#define U3CU3E__AnonType6_2__ctor_m3585939793(__this, ___money0, ___level1, method) ((  void (*) (U3CU3E__AnonType6_2_t584489623 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))U3CU3E__AnonType6_2__ctor_m3585939793_gshared)(__this, ___money0, ___level1, method)
// <money>__T <>__AnonType6`2<System.Object,System.Object>::get_money()
extern "C"  Il2CppObject * U3CU3E__AnonType6_2_get_money_m3568823683_gshared (U3CU3E__AnonType6_2_t584489623 * __this, const MethodInfo* method);
#define U3CU3E__AnonType6_2_get_money_m3568823683(__this, method) ((  Il2CppObject * (*) (U3CU3E__AnonType6_2_t584489623 *, const MethodInfo*))U3CU3E__AnonType6_2_get_money_m3568823683_gshared)(__this, method)
// <level>__T <>__AnonType6`2<System.Object,System.Object>::get_level()
extern "C"  Il2CppObject * U3CU3E__AnonType6_2_get_level_m2638676747_gshared (U3CU3E__AnonType6_2_t584489623 * __this, const MethodInfo* method);
#define U3CU3E__AnonType6_2_get_level_m2638676747(__this, method) ((  Il2CppObject * (*) (U3CU3E__AnonType6_2_t584489623 *, const MethodInfo*))U3CU3E__AnonType6_2_get_level_m2638676747_gshared)(__this, method)
// System.Boolean <>__AnonType6`2<System.Object,System.Object>::Equals(System.Object)
extern "C"  bool U3CU3E__AnonType6_2_Equals_m4010295630_gshared (U3CU3E__AnonType6_2_t584489623 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define U3CU3E__AnonType6_2_Equals_m4010295630(__this, ___obj0, method) ((  bool (*) (U3CU3E__AnonType6_2_t584489623 *, Il2CppObject *, const MethodInfo*))U3CU3E__AnonType6_2_Equals_m4010295630_gshared)(__this, ___obj0, method)
// System.Int32 <>__AnonType6`2<System.Object,System.Object>::GetHashCode()
extern "C"  int32_t U3CU3E__AnonType6_2_GetHashCode_m4201317746_gshared (U3CU3E__AnonType6_2_t584489623 * __this, const MethodInfo* method);
#define U3CU3E__AnonType6_2_GetHashCode_m4201317746(__this, method) ((  int32_t (*) (U3CU3E__AnonType6_2_t584489623 *, const MethodInfo*))U3CU3E__AnonType6_2_GetHashCode_m4201317746_gshared)(__this, method)
// System.String <>__AnonType6`2<System.Object,System.Object>::ToString()
extern "C"  String_t* U3CU3E__AnonType6_2_ToString_m1619254882_gshared (U3CU3E__AnonType6_2_t584489623 * __this, const MethodInfo* method);
#define U3CU3E__AnonType6_2_ToString_m1619254882(__this, method) ((  String_t* (*) (U3CU3E__AnonType6_2_t584489623 *, const MethodInfo*))U3CU3E__AnonType6_2_ToString_m1619254882_gshared)(__this, method)
