﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectObservable`2/Select_<System.Int64,System.Object>
struct Select__t3915064890;
// UniRx.Operators.SelectObservable`2<System.Int64,System.Object>
struct SelectObservable_2_t1317503851;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SelectObservable`2/Select_<System.Int64,System.Object>::.ctor(UniRx.Operators.SelectObservable`2<T,TR>,UniRx.IObserver`1<TR>,System.IDisposable)
extern "C"  void Select___ctor_m4085565039_gshared (Select__t3915064890 * __this, SelectObservable_2_t1317503851 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Select___ctor_m4085565039(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Select__t3915064890 *, SelectObservable_2_t1317503851 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Select___ctor_m4085565039_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.SelectObservable`2/Select_<System.Int64,System.Object>::OnNext(T)
extern "C"  void Select__OnNext_m1804265977_gshared (Select__t3915064890 * __this, int64_t ___value0, const MethodInfo* method);
#define Select__OnNext_m1804265977(__this, ___value0, method) ((  void (*) (Select__t3915064890 *, int64_t, const MethodInfo*))Select__OnNext_m1804265977_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SelectObservable`2/Select_<System.Int64,System.Object>::OnError(System.Exception)
extern "C"  void Select__OnError_m320205576_gshared (Select__t3915064890 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Select__OnError_m320205576(__this, ___error0, method) ((  void (*) (Select__t3915064890 *, Exception_t1967233988 *, const MethodInfo*))Select__OnError_m320205576_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SelectObservable`2/Select_<System.Int64,System.Object>::OnCompleted()
extern "C"  void Select__OnCompleted_m3434279515_gshared (Select__t3915064890 * __this, const MethodInfo* method);
#define Select__OnCompleted_m3434279515(__this, method) ((  void (*) (Select__t3915064890 *, const MethodInfo*))Select__OnCompleted_m3434279515_gshared)(__this, method)
