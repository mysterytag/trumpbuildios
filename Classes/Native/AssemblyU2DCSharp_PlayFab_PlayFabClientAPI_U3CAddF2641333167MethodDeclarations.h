﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<AddFriend>c__AnonStoreyAD
struct U3CAddFriendU3Ec__AnonStoreyAD_t2641333167;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<AddFriend>c__AnonStoreyAD::.ctor()
extern "C"  void U3CAddFriendU3Ec__AnonStoreyAD__ctor_m1027613828 (U3CAddFriendU3Ec__AnonStoreyAD_t2641333167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<AddFriend>c__AnonStoreyAD::<>m__9A(PlayFab.CallRequestContainer)
extern "C"  void U3CAddFriendU3Ec__AnonStoreyAD_U3CU3Em__9A_m3364128138 (U3CAddFriendU3Ec__AnonStoreyAD_t2641333167 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
