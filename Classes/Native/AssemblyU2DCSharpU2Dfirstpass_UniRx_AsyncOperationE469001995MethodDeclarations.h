﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.AsyncOperationExtensions/<AsAsyncOperationObservable>c__AnonStorey7C`1<System.Object>
struct U3CAsAsyncOperationObservableU3Ec__AnonStorey7C_1_t469001995;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560.h"

// System.Void UniRx.AsyncOperationExtensions/<AsAsyncOperationObservable>c__AnonStorey7C`1<System.Object>::.ctor()
extern "C"  void U3CAsAsyncOperationObservableU3Ec__AnonStorey7C_1__ctor_m1425434309_gshared (U3CAsAsyncOperationObservableU3Ec__AnonStorey7C_1_t469001995 * __this, const MethodInfo* method);
#define U3CAsAsyncOperationObservableU3Ec__AnonStorey7C_1__ctor_m1425434309(__this, method) ((  void (*) (U3CAsAsyncOperationObservableU3Ec__AnonStorey7C_1_t469001995 *, const MethodInfo*))U3CAsAsyncOperationObservableU3Ec__AnonStorey7C_1__ctor_m1425434309_gshared)(__this, method)
// System.Collections.IEnumerator UniRx.AsyncOperationExtensions/<AsAsyncOperationObservable>c__AnonStorey7C`1<System.Object>::<>m__A4(UniRx.IObserver`1<T>,UniRx.CancellationToken)
extern "C"  Il2CppObject * U3CAsAsyncOperationObservableU3Ec__AnonStorey7C_1_U3CU3Em__A4_m3523013009_gshared (U3CAsAsyncOperationObservableU3Ec__AnonStorey7C_1_t469001995 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellation1, const MethodInfo* method);
#define U3CAsAsyncOperationObservableU3Ec__AnonStorey7C_1_U3CU3Em__A4_m3523013009(__this, ___observer0, ___cancellation1, method) ((  Il2CppObject * (*) (U3CAsAsyncOperationObservableU3Ec__AnonStorey7C_1_t469001995 *, Il2CppObject*, CancellationToken_t1439151560 , const MethodInfo*))U3CAsAsyncOperationObservableU3Ec__AnonStorey7C_1_U3CU3Em__A4_m3523013009_gshared)(__this, ___observer0, ___cancellation1, method)
