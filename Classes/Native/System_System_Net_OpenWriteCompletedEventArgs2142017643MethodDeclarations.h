﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.OpenWriteCompletedEventArgs
struct OpenWriteCompletedEventArgs_t2142017643;
// System.IO.Stream
struct Stream_t219029575;
// System.Exception
struct Exception_t1967233988;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream219029575.h"
#include "mscorlib_System_Exception1967233988.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Net.OpenWriteCompletedEventArgs::.ctor(System.IO.Stream,System.Exception,System.Boolean,System.Object)
extern "C"  void OpenWriteCompletedEventArgs__ctor_m94443959 (OpenWriteCompletedEventArgs_t2142017643 * __this, Stream_t219029575 * ___result0, Exception_t1967233988 * ___error1, bool ___cancelled2, Il2CppObject * ___userState3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.Net.OpenWriteCompletedEventArgs::get_Result()
extern "C"  Stream_t219029575 * OpenWriteCompletedEventArgs_get_Result_m2927475189 (OpenWriteCompletedEventArgs_t2142017643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
