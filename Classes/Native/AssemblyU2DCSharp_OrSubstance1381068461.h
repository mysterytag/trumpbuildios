﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`2<SubstanceBase`2/Intrusion<System.Boolean,System.Boolean>,System.Boolean>
struct Func_2_t2346177373;

#include "AssemblyU2DCSharp_SubstanceBase_2_gen3707538316.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OrSubstance
struct  OrSubstance_t1381068461  : public SubstanceBase_2_t3707538316
{
public:

public:
};

struct OrSubstance_t1381068461_StaticFields
{
public:
	// System.Func`2<SubstanceBase`2/Intrusion<System.Boolean,System.Boolean>,System.Boolean> OrSubstance::<>f__am$cache0
	Func_2_t2346177373 * ___U3CU3Ef__amU24cache0_6;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_6() { return static_cast<int32_t>(offsetof(OrSubstance_t1381068461_StaticFields, ___U3CU3Ef__amU24cache0_6)); }
	inline Func_2_t2346177373 * get_U3CU3Ef__amU24cache0_6() const { return ___U3CU3Ef__amU24cache0_6; }
	inline Func_2_t2346177373 ** get_address_of_U3CU3Ef__amU24cache0_6() { return &___U3CU3Ef__amU24cache0_6; }
	inline void set_U3CU3Ef__amU24cache0_6(Func_2_t2346177373 * value)
	{
		___U3CU3Ef__amU24cache0_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
