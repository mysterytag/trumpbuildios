﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.UInt32>
struct Dictionary_2_t2623623230;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.StoreItem
struct  StoreItem_t2872884100  : public Il2CppObject
{
public:
	// System.String PlayFab.ClientModels.StoreItem::<ItemId>k__BackingField
	String_t* ___U3CItemIdU3Ek__BackingField_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.UInt32> PlayFab.ClientModels.StoreItem::<VirtualCurrencyPrices>k__BackingField
	Dictionary_2_t2623623230 * ___U3CVirtualCurrencyPricesU3Ek__BackingField_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.UInt32> PlayFab.ClientModels.StoreItem::<RealCurrencyPrices>k__BackingField
	Dictionary_2_t2623623230 * ___U3CRealCurrencyPricesU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CItemIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StoreItem_t2872884100, ___U3CItemIdU3Ek__BackingField_0)); }
	inline String_t* get_U3CItemIdU3Ek__BackingField_0() const { return ___U3CItemIdU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CItemIdU3Ek__BackingField_0() { return &___U3CItemIdU3Ek__BackingField_0; }
	inline void set_U3CItemIdU3Ek__BackingField_0(String_t* value)
	{
		___U3CItemIdU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CItemIdU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CVirtualCurrencyPricesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(StoreItem_t2872884100, ___U3CVirtualCurrencyPricesU3Ek__BackingField_1)); }
	inline Dictionary_2_t2623623230 * get_U3CVirtualCurrencyPricesU3Ek__BackingField_1() const { return ___U3CVirtualCurrencyPricesU3Ek__BackingField_1; }
	inline Dictionary_2_t2623623230 ** get_address_of_U3CVirtualCurrencyPricesU3Ek__BackingField_1() { return &___U3CVirtualCurrencyPricesU3Ek__BackingField_1; }
	inline void set_U3CVirtualCurrencyPricesU3Ek__BackingField_1(Dictionary_2_t2623623230 * value)
	{
		___U3CVirtualCurrencyPricesU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CVirtualCurrencyPricesU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CRealCurrencyPricesU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(StoreItem_t2872884100, ___U3CRealCurrencyPricesU3Ek__BackingField_2)); }
	inline Dictionary_2_t2623623230 * get_U3CRealCurrencyPricesU3Ek__BackingField_2() const { return ___U3CRealCurrencyPricesU3Ek__BackingField_2; }
	inline Dictionary_2_t2623623230 ** get_address_of_U3CRealCurrencyPricesU3Ek__BackingField_2() { return &___U3CRealCurrencyPricesU3Ek__BackingField_2; }
	inline void set_U3CRealCurrencyPricesU3Ek__BackingField_2(Dictionary_2_t2623623230 * value)
	{
		___U3CRealCurrencyPricesU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CRealCurrencyPricesU3Ek__BackingField_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
