﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionAddEvent`1<System.Object>>
struct DisposedObserver_1_t1590979449;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionAddE2416521987.h"

// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionAddEvent`1<System.Object>>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m2621332788_gshared (DisposedObserver_1_t1590979449 * __this, const MethodInfo* method);
#define DisposedObserver_1__ctor_m2621332788(__this, method) ((  void (*) (DisposedObserver_1_t1590979449 *, const MethodInfo*))DisposedObserver_1__ctor_m2621332788_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionAddEvent`1<System.Object>>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m3469808889_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define DisposedObserver_1__cctor_m3469808889(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))DisposedObserver_1__cctor_m3469808889_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionAddEvent`1<System.Object>>::OnCompleted()
extern "C"  void DisposedObserver_1_OnCompleted_m3371424190_gshared (DisposedObserver_1_t1590979449 * __this, const MethodInfo* method);
#define DisposedObserver_1_OnCompleted_m3371424190(__this, method) ((  void (*) (DisposedObserver_1_t1590979449 *, const MethodInfo*))DisposedObserver_1_OnCompleted_m3371424190_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionAddEvent`1<System.Object>>::OnError(System.Exception)
extern "C"  void DisposedObserver_1_OnError_m652205547_gshared (DisposedObserver_1_t1590979449 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define DisposedObserver_1_OnError_m652205547(__this, ___error0, method) ((  void (*) (DisposedObserver_1_t1590979449 *, Exception_t1967233988 *, const MethodInfo*))DisposedObserver_1_OnError_m652205547_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionAddEvent`1<System.Object>>::OnNext(T)
extern "C"  void DisposedObserver_1_OnNext_m785709788_gshared (DisposedObserver_1_t1590979449 * __this, CollectionAddEvent_1_t2416521987  ___value0, const MethodInfo* method);
#define DisposedObserver_1_OnNext_m785709788(__this, ___value0, method) ((  void (*) (DisposedObserver_1_t1590979449 *, CollectionAddEvent_1_t2416521987 , const MethodInfo*))DisposedObserver_1_OnNext_m785709788_gshared)(__this, ___value0, method)
