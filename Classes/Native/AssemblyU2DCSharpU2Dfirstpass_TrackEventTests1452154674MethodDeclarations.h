﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TrackEventTests
struct TrackEventTests_t1452154674;

#include "codegen/il2cpp-codegen.h"

// System.Void TrackEventTests::.ctor()
extern "C"  void TrackEventTests__ctor_m3305739077 (TrackEventTests_t1452154674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TrackEventTests::Start()
extern "C"  void TrackEventTests_Start_m2252876869 (TrackEventTests_t1452154674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TrackEventTests::Update()
extern "C"  void TrackEventTests_Update_m1125558376 (TrackEventTests_t1452154674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TrackEventTests::TrackRichEventTest()
extern "C"  void TrackEventTests_TrackRichEventTest_m2404782308 (TrackEventTests_t1452154674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TrackEventTests::ValidateReceiptTest()
extern "C"  void TrackEventTests_ValidateReceiptTest_m2345315287 (TrackEventTests_t1452154674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
