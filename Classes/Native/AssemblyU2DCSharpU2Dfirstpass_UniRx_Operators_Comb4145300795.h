﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.CombineLatestObservable`3<System.Boolean,System.Boolean,System.Object>
struct CombineLatestObservable_3_t243429454;
// System.Object
struct Il2CppObject;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Boolean,System.Object>
struct  CombineLatest_t4145300795  : public OperatorObserverBase_2_t1187768149
{
public:
	// UniRx.Operators.CombineLatestObservable`3<TLeft,TRight,TResult> UniRx.Operators.CombineLatestObservable`3/CombineLatest::parent
	CombineLatestObservable_3_t243429454 * ___parent_2;
	// System.Object UniRx.Operators.CombineLatestObservable`3/CombineLatest::gate
	Il2CppObject * ___gate_3;
	// TLeft UniRx.Operators.CombineLatestObservable`3/CombineLatest::leftValue
	bool ___leftValue_4;
	// System.Boolean UniRx.Operators.CombineLatestObservable`3/CombineLatest::leftStarted
	bool ___leftStarted_5;
	// System.Boolean UniRx.Operators.CombineLatestObservable`3/CombineLatest::leftCompleted
	bool ___leftCompleted_6;
	// TRight UniRx.Operators.CombineLatestObservable`3/CombineLatest::rightValue
	bool ___rightValue_7;
	// System.Boolean UniRx.Operators.CombineLatestObservable`3/CombineLatest::rightStarted
	bool ___rightStarted_8;
	// System.Boolean UniRx.Operators.CombineLatestObservable`3/CombineLatest::rightCompleted
	bool ___rightCompleted_9;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(CombineLatest_t4145300795, ___parent_2)); }
	inline CombineLatestObservable_3_t243429454 * get_parent_2() const { return ___parent_2; }
	inline CombineLatestObservable_3_t243429454 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(CombineLatestObservable_3_t243429454 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_gate_3() { return static_cast<int32_t>(offsetof(CombineLatest_t4145300795, ___gate_3)); }
	inline Il2CppObject * get_gate_3() const { return ___gate_3; }
	inline Il2CppObject ** get_address_of_gate_3() { return &___gate_3; }
	inline void set_gate_3(Il2CppObject * value)
	{
		___gate_3 = value;
		Il2CppCodeGenWriteBarrier(&___gate_3, value);
	}

	inline static int32_t get_offset_of_leftValue_4() { return static_cast<int32_t>(offsetof(CombineLatest_t4145300795, ___leftValue_4)); }
	inline bool get_leftValue_4() const { return ___leftValue_4; }
	inline bool* get_address_of_leftValue_4() { return &___leftValue_4; }
	inline void set_leftValue_4(bool value)
	{
		___leftValue_4 = value;
	}

	inline static int32_t get_offset_of_leftStarted_5() { return static_cast<int32_t>(offsetof(CombineLatest_t4145300795, ___leftStarted_5)); }
	inline bool get_leftStarted_5() const { return ___leftStarted_5; }
	inline bool* get_address_of_leftStarted_5() { return &___leftStarted_5; }
	inline void set_leftStarted_5(bool value)
	{
		___leftStarted_5 = value;
	}

	inline static int32_t get_offset_of_leftCompleted_6() { return static_cast<int32_t>(offsetof(CombineLatest_t4145300795, ___leftCompleted_6)); }
	inline bool get_leftCompleted_6() const { return ___leftCompleted_6; }
	inline bool* get_address_of_leftCompleted_6() { return &___leftCompleted_6; }
	inline void set_leftCompleted_6(bool value)
	{
		___leftCompleted_6 = value;
	}

	inline static int32_t get_offset_of_rightValue_7() { return static_cast<int32_t>(offsetof(CombineLatest_t4145300795, ___rightValue_7)); }
	inline bool get_rightValue_7() const { return ___rightValue_7; }
	inline bool* get_address_of_rightValue_7() { return &___rightValue_7; }
	inline void set_rightValue_7(bool value)
	{
		___rightValue_7 = value;
	}

	inline static int32_t get_offset_of_rightStarted_8() { return static_cast<int32_t>(offsetof(CombineLatest_t4145300795, ___rightStarted_8)); }
	inline bool get_rightStarted_8() const { return ___rightStarted_8; }
	inline bool* get_address_of_rightStarted_8() { return &___rightStarted_8; }
	inline void set_rightStarted_8(bool value)
	{
		___rightStarted_8 = value;
	}

	inline static int32_t get_offset_of_rightCompleted_9() { return static_cast<int32_t>(offsetof(CombineLatest_t4145300795, ___rightCompleted_9)); }
	inline bool get_rightCompleted_9() const { return ___rightCompleted_9; }
	inline bool* get_address_of_rightCompleted_9() { return &___rightCompleted_9; }
	inline void set_rightCompleted_9(bool value)
	{
		___rightCompleted_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
