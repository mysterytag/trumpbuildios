﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.SelectManyObservable`2<UniRx.Unit,UniRx.Unit>
struct SelectManyObservable_2_t3540602214;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4165376397.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.SelectManyObservable`2/SelectManyEnumerableObserver<UniRx.Unit,UniRx.Unit>
struct  SelectManyEnumerableObserver_t499971322  : public OperatorObserverBase_2_t4165376397
{
public:
	// UniRx.Operators.SelectManyObservable`2<TSource,TResult> UniRx.Operators.SelectManyObservable`2/SelectManyEnumerableObserver::parent
	SelectManyObservable_2_t3540602214 * ___parent_2;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(SelectManyEnumerableObserver_t499971322, ___parent_2)); }
	inline SelectManyObservable_2_t3540602214 * get_parent_2() const { return ___parent_2; }
	inline SelectManyObservable_2_t3540602214 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(SelectManyObservable_2_t3540602214 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
