﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.EmptyObservable`1/Empty<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>
struct Empty_t986138829;
// UniRx.IObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>
struct IObserver_1_t1239319898;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_DictionaryRemo3322288291.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Empty__ctor_m1088404525_gshared (Empty_t986138829 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define Empty__ctor_m1088404525(__this, ___observer0, ___cancel1, method) ((  void (*) (Empty_t986138829 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Empty__ctor_m1088404525_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::OnNext(T)
extern "C"  void Empty_OnNext_m4125513541_gshared (Empty_t986138829 * __this, DictionaryRemoveEvent_2_t3322288291  ___value0, const MethodInfo* method);
#define Empty_OnNext_m4125513541(__this, ___value0, method) ((  void (*) (Empty_t986138829 *, DictionaryRemoveEvent_2_t3322288291 , const MethodInfo*))Empty_OnNext_m4125513541_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::OnError(System.Exception)
extern "C"  void Empty_OnError_m3445966932_gshared (Empty_t986138829 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Empty_OnError_m3445966932(__this, ___error0, method) ((  void (*) (Empty_t986138829 *, Exception_t1967233988 *, const MethodInfo*))Empty_OnError_m3445966932_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::OnCompleted()
extern "C"  void Empty_OnCompleted_m3049183655_gshared (Empty_t986138829 * __this, const MethodInfo* method);
#define Empty_OnCompleted_m3049183655(__this, method) ((  void (*) (Empty_t986138829 *, const MethodInfo*))Empty_OnCompleted_m3049183655_gshared)(__this, method)
