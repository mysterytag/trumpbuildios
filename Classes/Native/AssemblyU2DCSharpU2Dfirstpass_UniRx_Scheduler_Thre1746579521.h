﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.ICancelable
struct ICancelable_t4109686575;
// System.Action`1<System.Object>
struct Action_1_t985559125;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Scheduler/ThreadPoolScheduler/<ScheduleQueueing>c__AnonStorey7A`1<System.Object>
struct  U3CScheduleQueueingU3Ec__AnonStorey7A_1_t1746579521  : public Il2CppObject
{
public:
	// UniRx.ICancelable UniRx.Scheduler/ThreadPoolScheduler/<ScheduleQueueing>c__AnonStorey7A`1::cancel
	Il2CppObject * ___cancel_0;
	// System.Action`1<T> UniRx.Scheduler/ThreadPoolScheduler/<ScheduleQueueing>c__AnonStorey7A`1::action
	Action_1_t985559125 * ___action_1;

public:
	inline static int32_t get_offset_of_cancel_0() { return static_cast<int32_t>(offsetof(U3CScheduleQueueingU3Ec__AnonStorey7A_1_t1746579521, ___cancel_0)); }
	inline Il2CppObject * get_cancel_0() const { return ___cancel_0; }
	inline Il2CppObject ** get_address_of_cancel_0() { return &___cancel_0; }
	inline void set_cancel_0(Il2CppObject * value)
	{
		___cancel_0 = value;
		Il2CppCodeGenWriteBarrier(&___cancel_0, value);
	}

	inline static int32_t get_offset_of_action_1() { return static_cast<int32_t>(offsetof(U3CScheduleQueueingU3Ec__AnonStorey7A_1_t1746579521, ___action_1)); }
	inline Action_1_t985559125 * get_action_1() const { return ___action_1; }
	inline Action_1_t985559125 ** get_address_of_action_1() { return &___action_1; }
	inline void set_action_1(Action_1_t985559125 * value)
	{
		___action_1 = value;
		Il2CppCodeGenWriteBarrier(&___action_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
