﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.HttpListenerRequest
struct HttpListenerRequest_t404699357;
// System.Net.HttpListenerContext
struct HttpListenerContext_t260537341;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t2956870243;
// System.Text.Encoding
struct Encoding_t180559927;
// System.Net.CookieCollection
struct CookieCollection_t2006989740;
// System.Collections.Specialized.NameValueCollection
struct NameValueCollection_t3455488335;
// System.IO.Stream
struct Stream_t219029575;
// System.Net.IPEndPoint
struct IPEndPoint_t1265996582;
// System.Version
struct Version_t497901645;
// System.Uri
struct Uri_t2776692961;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;
// System.Object
struct Il2CppObject;
// System.Security.Cryptography.X509Certificates.X509Certificate2
struct X509Certificate2_t2583282360;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_HttpListenerContext260537341.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Guid2778838590.h"
#include "mscorlib_System_AsyncCallback1363551830.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Net.HttpListenerRequest::.ctor(System.Net.HttpListenerContext)
extern "C"  void HttpListenerRequest__ctor_m657808310 (HttpListenerRequest_t404699357 * __this, HttpListenerContext_t260537341 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListenerRequest::.cctor()
extern "C"  void HttpListenerRequest__cctor_m3051050868 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListenerRequest::SetRequestLine(System.String)
extern "C"  void HttpListenerRequest_SetRequestLine_m2504697622 (HttpListenerRequest_t404699357 * __this, String_t* ___req0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListenerRequest::CreateQueryString(System.String)
extern "C"  void HttpListenerRequest_CreateQueryString_m1416761070 (HttpListenerRequest_t404699357 * __this, String_t* ___query0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListenerRequest::FinishInitialization()
extern "C"  void HttpListenerRequest_FinishInitialization_m3789342238 (HttpListenerRequest_t404699357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.HttpListenerRequest::Unquote(System.String)
extern "C"  String_t* HttpListenerRequest_Unquote_m2735301035 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.HttpListenerRequest::AddHeader(System.String)
extern "C"  void HttpListenerRequest_AddHeader_m122889885 (HttpListenerRequest_t404699357 * __this, String_t* ___header0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.HttpListenerRequest::FlushInput()
extern "C"  bool HttpListenerRequest_FlushInput_m3611663495 (HttpListenerRequest_t404699357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.Net.HttpListenerRequest::get_AcceptTypes()
extern "C"  StringU5BU5D_t2956870243* HttpListenerRequest_get_AcceptTypes_m1390914878 (HttpListenerRequest_t404699357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.HttpListenerRequest::get_ClientCertificateError()
extern "C"  int32_t HttpListenerRequest_get_ClientCertificateError_m3459550688 (HttpListenerRequest_t404699357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoding System.Net.HttpListenerRequest::get_ContentEncoding()
extern "C"  Encoding_t180559927 * HttpListenerRequest_get_ContentEncoding_m1589242896 (HttpListenerRequest_t404699357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.HttpListenerRequest::get_ContentLength64()
extern "C"  int64_t HttpListenerRequest_get_ContentLength64_m1649097402 (HttpListenerRequest_t404699357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.HttpListenerRequest::get_ContentType()
extern "C"  String_t* HttpListenerRequest_get_ContentType_m2607969886 (HttpListenerRequest_t404699357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.CookieCollection System.Net.HttpListenerRequest::get_Cookies()
extern "C"  CookieCollection_t2006989740 * HttpListenerRequest_get_Cookies_m161824700 (HttpListenerRequest_t404699357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.HttpListenerRequest::get_HasEntityBody()
extern "C"  bool HttpListenerRequest_get_HasEntityBody_m1543928535 (HttpListenerRequest_t404699357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Specialized.NameValueCollection System.Net.HttpListenerRequest::get_Headers()
extern "C"  NameValueCollection_t3455488335 * HttpListenerRequest_get_Headers_m2606027260 (HttpListenerRequest_t404699357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.HttpListenerRequest::get_HttpMethod()
extern "C"  String_t* HttpListenerRequest_get_HttpMethod_m3279085120 (HttpListenerRequest_t404699357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.Net.HttpListenerRequest::get_InputStream()
extern "C"  Stream_t219029575 * HttpListenerRequest_get_InputStream_m2143746 (HttpListenerRequest_t404699357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.HttpListenerRequest::get_IsAuthenticated()
extern "C"  bool HttpListenerRequest_get_IsAuthenticated_m156310621 (HttpListenerRequest_t404699357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.HttpListenerRequest::get_IsLocal()
extern "C"  bool HttpListenerRequest_get_IsLocal_m4247692697 (HttpListenerRequest_t404699357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.HttpListenerRequest::get_IsSecureConnection()
extern "C"  bool HttpListenerRequest_get_IsSecureConnection_m1387676521 (HttpListenerRequest_t404699357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.HttpListenerRequest::get_KeepAlive()
extern "C"  bool HttpListenerRequest_get_KeepAlive_m681337344 (HttpListenerRequest_t404699357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IPEndPoint System.Net.HttpListenerRequest::get_LocalEndPoint()
extern "C"  IPEndPoint_t1265996582 * HttpListenerRequest_get_LocalEndPoint_m193597671 (HttpListenerRequest_t404699357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Version System.Net.HttpListenerRequest::get_ProtocolVersion()
extern "C"  Version_t497901645 * HttpListenerRequest_get_ProtocolVersion_m4199372360 (HttpListenerRequest_t404699357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Specialized.NameValueCollection System.Net.HttpListenerRequest::get_QueryString()
extern "C"  NameValueCollection_t3455488335 * HttpListenerRequest_get_QueryString_m2779414063 (HttpListenerRequest_t404699357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.HttpListenerRequest::get_RawUrl()
extern "C"  String_t* HttpListenerRequest_get_RawUrl_m2946244190 (HttpListenerRequest_t404699357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IPEndPoint System.Net.HttpListenerRequest::get_RemoteEndPoint()
extern "C"  IPEndPoint_t1265996582 * HttpListenerRequest_get_RemoteEndPoint_m1327326454 (HttpListenerRequest_t404699357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Guid System.Net.HttpListenerRequest::get_RequestTraceIdentifier()
extern "C"  Guid_t2778838590  HttpListenerRequest_get_RequestTraceIdentifier_m2689213630 (HttpListenerRequest_t404699357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri System.Net.HttpListenerRequest::get_Url()
extern "C"  Uri_t2776692961 * HttpListenerRequest_get_Url_m3787620459 (HttpListenerRequest_t404699357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri System.Net.HttpListenerRequest::get_UrlReferrer()
extern "C"  Uri_t2776692961 * HttpListenerRequest_get_UrlReferrer_m259998442 (HttpListenerRequest_t404699357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.HttpListenerRequest::get_UserAgent()
extern "C"  String_t* HttpListenerRequest_get_UserAgent_m119492037 (HttpListenerRequest_t404699357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.HttpListenerRequest::get_UserHostAddress()
extern "C"  String_t* HttpListenerRequest_get_UserHostAddress_m1388032748 (HttpListenerRequest_t404699357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.HttpListenerRequest::get_UserHostName()
extern "C"  String_t* HttpListenerRequest_get_UserHostName_m3525444917 (HttpListenerRequest_t404699357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.Net.HttpListenerRequest::get_UserLanguages()
extern "C"  StringU5BU5D_t2956870243* HttpListenerRequest_get_UserLanguages_m1281279677 (HttpListenerRequest_t404699357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Net.HttpListenerRequest::BeginGetClientCertificate(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * HttpListenerRequest_BeginGetClientCertificate_m3682268222 (HttpListenerRequest_t404699357 * __this, AsyncCallback_t1363551830 * ___requestCallback0, Il2CppObject * ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.X509Certificates.X509Certificate2 System.Net.HttpListenerRequest::EndGetClientCertificate(System.IAsyncResult)
extern "C"  X509Certificate2_t2583282360 * HttpListenerRequest_EndGetClientCertificate_m2625811904 (HttpListenerRequest_t404699357 * __this, Il2CppObject * ___asyncResult0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.X509Certificates.X509Certificate2 System.Net.HttpListenerRequest::GetClientCertificate()
extern "C"  X509Certificate2_t2583282360 * HttpListenerRequest_GetClientCertificate_m229735494 (HttpListenerRequest_t404699357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
