﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.UserCustomIdInfo
struct UserCustomIdInfo_t1941075669;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.UserCustomIdInfo::.ctor()
extern "C"  void UserCustomIdInfo__ctor_m1268537608 (UserCustomIdInfo_t1941075669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.UserCustomIdInfo::get_CustomId()
extern "C"  String_t* UserCustomIdInfo_get_CustomId_m3837826030 (UserCustomIdInfo_t1941075669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UserCustomIdInfo::set_CustomId(System.String)
extern "C"  void UserCustomIdInfo_set_CustomId_m678625661 (UserCustomIdInfo_t1941075669 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
