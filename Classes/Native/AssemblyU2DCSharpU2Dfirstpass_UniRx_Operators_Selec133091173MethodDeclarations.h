﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectObservable`2/Select<System.Int64,System.Object>
struct Select_t133091173;
// UniRx.Operators.SelectObservable`2<System.Int64,System.Object>
struct SelectObservable_2_t1317503851;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SelectObservable`2/Select<System.Int64,System.Object>::.ctor(UniRx.Operators.SelectObservable`2<T,TR>,UniRx.IObserver`1<TR>,System.IDisposable)
extern "C"  void Select__ctor_m3681417166_gshared (Select_t133091173 * __this, SelectObservable_2_t1317503851 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Select__ctor_m3681417166(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Select_t133091173 *, SelectObservable_2_t1317503851 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Select__ctor_m3681417166_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.SelectObservable`2/Select<System.Int64,System.Object>::OnNext(T)
extern "C"  void Select_OnNext_m2827718266_gshared (Select_t133091173 * __this, int64_t ___value0, const MethodInfo* method);
#define Select_OnNext_m2827718266(__this, ___value0, method) ((  void (*) (Select_t133091173 *, int64_t, const MethodInfo*))Select_OnNext_m2827718266_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SelectObservable`2/Select<System.Int64,System.Object>::OnError(System.Exception)
extern "C"  void Select_OnError_m3353834889_gshared (Select_t133091173 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Select_OnError_m3353834889(__this, ___error0, method) ((  void (*) (Select_t133091173 *, Exception_t1967233988 *, const MethodInfo*))Select_OnError_m3353834889_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SelectObservable`2/Select<System.Int64,System.Object>::OnCompleted()
extern "C"  void Select_OnCompleted_m2547740252_gshared (Select_t133091173 * __this, const MethodInfo* method);
#define Select_OnCompleted_m2547740252(__this, method) ((  void (*) (Select_t133091173 *, const MethodInfo*))Select_OnCompleted_m2547740252_gshared)(__this, method)
