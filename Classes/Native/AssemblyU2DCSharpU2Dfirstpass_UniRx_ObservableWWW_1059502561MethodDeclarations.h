﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ObservableWWW/<LoadFromCacheOrDownload>c__AnonStorey90
struct U3CLoadFromCacheOrDownloadU3Ec__AnonStorey90_t1059502561;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// UniRx.IObserver`1<UnityEngine.AssetBundle>
struct IObserver_1_t1876462710;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560.h"

// System.Void UniRx.ObservableWWW/<LoadFromCacheOrDownload>c__AnonStorey90::.ctor()
extern "C"  void U3CLoadFromCacheOrDownloadU3Ec__AnonStorey90__ctor_m579137467 (U3CLoadFromCacheOrDownloadU3Ec__AnonStorey90_t1059502561 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UniRx.ObservableWWW/<LoadFromCacheOrDownload>c__AnonStorey90::<>m__BC(UniRx.IObserver`1<UnityEngine.AssetBundle>,UniRx.CancellationToken)
extern "C"  Il2CppObject * U3CLoadFromCacheOrDownloadU3Ec__AnonStorey90_U3CU3Em__BC_m1871593532 (U3CLoadFromCacheOrDownloadU3Ec__AnonStorey90_t1059502561 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellation1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
