﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21028297614.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2388733843_gshared (KeyValuePair_2_t1028297614 * __this, Il2CppObject * ___key0, int64_t ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m2388733843(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1028297614 *, Il2CppObject *, int64_t, const MethodInfo*))KeyValuePair_2__ctor_m2388733843_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2182089461_gshared (KeyValuePair_2_t1028297614 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m2182089461(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t1028297614 *, const MethodInfo*))KeyValuePair_2_get_Key_m2182089461_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m903707318_gshared (KeyValuePair_2_t1028297614 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m903707318(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1028297614 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Key_m903707318_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>::get_Value()
extern "C"  int64_t KeyValuePair_2_get_Value_m4174236661_gshared (KeyValuePair_2_t1028297614 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m4174236661(__this, method) ((  int64_t (*) (KeyValuePair_2_t1028297614 *, const MethodInfo*))KeyValuePair_2_get_Value_m4174236661_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3035661238_gshared (KeyValuePair_2_t1028297614 * __this, int64_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m3035661238(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1028297614 *, int64_t, const MethodInfo*))KeyValuePair_2_set_Value_m3035661238_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1268853100_gshared (KeyValuePair_2_t1028297614 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m1268853100(__this, method) ((  String_t* (*) (KeyValuePair_2_t1028297614 *, const MethodInfo*))KeyValuePair_2_ToString_m1268853100_gshared)(__this, method)
