﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_LinkedListNode_1_539097040MethodDeclarations.h"

// System.Void System.Collections.Generic.LinkedListNode`1<Tacticsoft.TableViewCell>::.ctor(System.Collections.Generic.LinkedList`1<T>,T)
#define LinkedListNode_1__ctor_m4149219969(__this, ___list0, ___value1, method) ((  void (*) (LinkedListNode_1_t478410375 *, LinkedList_1_t2516549301 *, TableViewCell_t776419755 *, const MethodInfo*))LinkedListNode_1__ctor_m648136130_gshared)(__this, ___list0, ___value1, method)
// System.Void System.Collections.Generic.LinkedListNode`1<Tacticsoft.TableViewCell>::.ctor(System.Collections.Generic.LinkedList`1<T>,T,System.Collections.Generic.LinkedListNode`1<T>,System.Collections.Generic.LinkedListNode`1<T>)
#define LinkedListNode_1__ctor_m2120557985(__this, ___list0, ___value1, ___previousNode2, ___nextNode3, method) ((  void (*) (LinkedListNode_1_t478410375 *, LinkedList_1_t2516549301 *, TableViewCell_t776419755 *, LinkedListNode_1_t478410375 *, LinkedListNode_1_t478410375 *, const MethodInfo*))LinkedListNode_1__ctor_m448391458_gshared)(__this, ___list0, ___value1, ___previousNode2, ___nextNode3, method)
// System.Void System.Collections.Generic.LinkedListNode`1<Tacticsoft.TableViewCell>::Detach()
#define LinkedListNode_1_Detach_m580668415(__this, method) ((  void (*) (LinkedListNode_1_t478410375 *, const MethodInfo*))LinkedListNode_1_Detach_m3406254942_gshared)(__this, method)
// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1<Tacticsoft.TableViewCell>::get_List()
#define LinkedListNode_1_get_List_m1634024739(__this, method) ((  LinkedList_1_t2516549301 * (*) (LinkedListNode_1_t478410375 *, const MethodInfo*))LinkedListNode_1_get_List_m3467110818_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<Tacticsoft.TableViewCell>::get_Next()
#define LinkedListNode_1_get_Next_m1464267734(__this, method) ((  LinkedListNode_1_t478410375 * (*) (LinkedListNode_1_t478410375 *, const MethodInfo*))LinkedListNode_1_get_Next_m1427618777_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<Tacticsoft.TableViewCell>::get_Previous()
#define LinkedListNode_1_get_Previous_m1199313370(__this, method) ((  LinkedListNode_1_t478410375 * (*) (LinkedListNode_1_t478410375 *, const MethodInfo*))LinkedListNode_1_get_Previous_m3755155549_gshared)(__this, method)
// T System.Collections.Generic.LinkedListNode`1<Tacticsoft.TableViewCell>::get_Value()
#define LinkedListNode_1_get_Value_m1726777293(__this, method) ((  TableViewCell_t776419755 * (*) (LinkedListNode_1_t478410375 *, const MethodInfo*))LinkedListNode_1_get_Value_m702633824_gshared)(__this, method)
