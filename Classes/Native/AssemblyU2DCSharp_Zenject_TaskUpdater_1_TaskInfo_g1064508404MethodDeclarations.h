﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.TaskUpdater`1/TaskInfo<System.Object>
struct TaskInfo_t1064508404;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void Zenject.TaskUpdater`1/TaskInfo<System.Object>::.ctor(TTask,System.Int32)
extern "C"  void TaskInfo__ctor_m1167730649_gshared (TaskInfo_t1064508404 * __this, Il2CppObject * ___task0, int32_t ___priority1, const MethodInfo* method);
#define TaskInfo__ctor_m1167730649(__this, ___task0, ___priority1, method) ((  void (*) (TaskInfo_t1064508404 *, Il2CppObject *, int32_t, const MethodInfo*))TaskInfo__ctor_m1167730649_gshared)(__this, ___task0, ___priority1, method)
