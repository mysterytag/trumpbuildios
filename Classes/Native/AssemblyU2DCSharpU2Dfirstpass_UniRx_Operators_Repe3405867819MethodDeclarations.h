﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.RepeatUntilObservable`1/RepeatUntil/<SubscribeAfterEndOfFrame>c__Iterator28<System.Object>
struct U3CSubscribeAfterEndOfFrameU3Ec__Iterator28_t3405867819;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.RepeatUntilObservable`1/RepeatUntil/<SubscribeAfterEndOfFrame>c__Iterator28<System.Object>::.ctor()
extern "C"  void U3CSubscribeAfterEndOfFrameU3Ec__Iterator28__ctor_m1405062938_gshared (U3CSubscribeAfterEndOfFrameU3Ec__Iterator28_t3405867819 * __this, const MethodInfo* method);
#define U3CSubscribeAfterEndOfFrameU3Ec__Iterator28__ctor_m1405062938(__this, method) ((  void (*) (U3CSubscribeAfterEndOfFrameU3Ec__Iterator28_t3405867819 *, const MethodInfo*))U3CSubscribeAfterEndOfFrameU3Ec__Iterator28__ctor_m1405062938_gshared)(__this, method)
// System.Object UniRx.Operators.RepeatUntilObservable`1/RepeatUntil/<SubscribeAfterEndOfFrame>c__Iterator28<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSubscribeAfterEndOfFrameU3Ec__Iterator28_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m398781890_gshared (U3CSubscribeAfterEndOfFrameU3Ec__Iterator28_t3405867819 * __this, const MethodInfo* method);
#define U3CSubscribeAfterEndOfFrameU3Ec__Iterator28_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m398781890(__this, method) ((  Il2CppObject * (*) (U3CSubscribeAfterEndOfFrameU3Ec__Iterator28_t3405867819 *, const MethodInfo*))U3CSubscribeAfterEndOfFrameU3Ec__Iterator28_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m398781890_gshared)(__this, method)
// System.Object UniRx.Operators.RepeatUntilObservable`1/RepeatUntil/<SubscribeAfterEndOfFrame>c__Iterator28<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSubscribeAfterEndOfFrameU3Ec__Iterator28_System_Collections_IEnumerator_get_Current_m2042541398_gshared (U3CSubscribeAfterEndOfFrameU3Ec__Iterator28_t3405867819 * __this, const MethodInfo* method);
#define U3CSubscribeAfterEndOfFrameU3Ec__Iterator28_System_Collections_IEnumerator_get_Current_m2042541398(__this, method) ((  Il2CppObject * (*) (U3CSubscribeAfterEndOfFrameU3Ec__Iterator28_t3405867819 *, const MethodInfo*))U3CSubscribeAfterEndOfFrameU3Ec__Iterator28_System_Collections_IEnumerator_get_Current_m2042541398_gshared)(__this, method)
// System.Boolean UniRx.Operators.RepeatUntilObservable`1/RepeatUntil/<SubscribeAfterEndOfFrame>c__Iterator28<System.Object>::MoveNext()
extern "C"  bool U3CSubscribeAfterEndOfFrameU3Ec__Iterator28_MoveNext_m1319872618_gshared (U3CSubscribeAfterEndOfFrameU3Ec__Iterator28_t3405867819 * __this, const MethodInfo* method);
#define U3CSubscribeAfterEndOfFrameU3Ec__Iterator28_MoveNext_m1319872618(__this, method) ((  bool (*) (U3CSubscribeAfterEndOfFrameU3Ec__Iterator28_t3405867819 *, const MethodInfo*))U3CSubscribeAfterEndOfFrameU3Ec__Iterator28_MoveNext_m1319872618_gshared)(__this, method)
// System.Void UniRx.Operators.RepeatUntilObservable`1/RepeatUntil/<SubscribeAfterEndOfFrame>c__Iterator28<System.Object>::Dispose()
extern "C"  void U3CSubscribeAfterEndOfFrameU3Ec__Iterator28_Dispose_m1544035543_gshared (U3CSubscribeAfterEndOfFrameU3Ec__Iterator28_t3405867819 * __this, const MethodInfo* method);
#define U3CSubscribeAfterEndOfFrameU3Ec__Iterator28_Dispose_m1544035543(__this, method) ((  void (*) (U3CSubscribeAfterEndOfFrameU3Ec__Iterator28_t3405867819 *, const MethodInfo*))U3CSubscribeAfterEndOfFrameU3Ec__Iterator28_Dispose_m1544035543_gshared)(__this, method)
// System.Void UniRx.Operators.RepeatUntilObservable`1/RepeatUntil/<SubscribeAfterEndOfFrame>c__Iterator28<System.Object>::Reset()
extern "C"  void U3CSubscribeAfterEndOfFrameU3Ec__Iterator28_Reset_m3346463175_gshared (U3CSubscribeAfterEndOfFrameU3Ec__Iterator28_t3405867819 * __this, const MethodInfo* method);
#define U3CSubscribeAfterEndOfFrameU3Ec__Iterator28_Reset_m3346463175(__this, method) ((  void (*) (U3CSubscribeAfterEndOfFrameU3Ec__Iterator28_t3405867819 *, const MethodInfo*))U3CSubscribeAfterEndOfFrameU3Ec__Iterator28_Reset_m3346463175_gshared)(__this, method)
