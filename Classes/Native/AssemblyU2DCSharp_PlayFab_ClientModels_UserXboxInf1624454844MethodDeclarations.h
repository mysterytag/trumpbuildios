﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.UserXboxInfo
struct UserXboxInfo_t1624454844;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.UserXboxInfo::.ctor()
extern "C"  void UserXboxInfo__ctor_m3880055297 (UserXboxInfo_t1624454844 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.UserXboxInfo::get_XboxUserId()
extern "C"  String_t* UserXboxInfo_get_XboxUserId_m3827858466 (UserXboxInfo_t1624454844 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UserXboxInfo::set_XboxUserId(System.String)
extern "C"  void UserXboxInfo_set_XboxUserId_m2279800329 (UserXboxInfo_t1624454844 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
