﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<LinkGameCenterAccount>c__AnonStorey78
struct U3CLinkGameCenterAccountU3Ec__AnonStorey78_t1114137658;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<LinkGameCenterAccount>c__AnonStorey78::.ctor()
extern "C"  void U3CLinkGameCenterAccountU3Ec__AnonStorey78__ctor_m3891593305 (U3CLinkGameCenterAccountU3Ec__AnonStorey78_t1114137658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<LinkGameCenterAccount>c__AnonStorey78::<>m__65(PlayFab.CallRequestContainer)
extern "C"  void U3CLinkGameCenterAccountU3Ec__AnonStorey78_U3CU3Em__65_m3049036790 (U3CLinkGameCenterAccountU3Ec__AnonStorey78_t1114137658 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
