﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.CatchObservable`1/Catch<System.Object>
struct Catch_t3617674332;
// UniRx.Operators.CatchObservable`1<System.Object>
struct CatchObservable_1_t1624243829;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action
struct Action_t437523947;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.CatchObservable`1/Catch<System.Object>::.ctor(UniRx.Operators.CatchObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Catch__ctor_m1707511405_gshared (Catch_t3617674332 * __this, CatchObservable_1_t1624243829 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Catch__ctor_m1707511405(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Catch_t3617674332 *, CatchObservable_1_t1624243829 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Catch__ctor_m1707511405_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.CatchObservable`1/Catch<System.Object>::Run()
extern "C"  Il2CppObject * Catch_Run_m3086992037_gshared (Catch_t3617674332 * __this, const MethodInfo* method);
#define Catch_Run_m3086992037(__this, method) ((  Il2CppObject * (*) (Catch_t3617674332 *, const MethodInfo*))Catch_Run_m3086992037_gshared)(__this, method)
// System.Void UniRx.Operators.CatchObservable`1/Catch<System.Object>::RecursiveRun(System.Action)
extern "C"  void Catch_RecursiveRun_m2866324535_gshared (Catch_t3617674332 * __this, Action_t437523947 * ___self0, const MethodInfo* method);
#define Catch_RecursiveRun_m2866324535(__this, ___self0, method) ((  void (*) (Catch_t3617674332 *, Action_t437523947 *, const MethodInfo*))Catch_RecursiveRun_m2866324535_gshared)(__this, ___self0, method)
// System.Void UniRx.Operators.CatchObservable`1/Catch<System.Object>::OnNext(T)
extern "C"  void Catch_OnNext_m4177295103_gshared (Catch_t3617674332 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Catch_OnNext_m4177295103(__this, ___value0, method) ((  void (*) (Catch_t3617674332 *, Il2CppObject *, const MethodInfo*))Catch_OnNext_m4177295103_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.CatchObservable`1/Catch<System.Object>::OnError(System.Exception)
extern "C"  void Catch_OnError_m298365966_gshared (Catch_t3617674332 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Catch_OnError_m298365966(__this, ___error0, method) ((  void (*) (Catch_t3617674332 *, Exception_t1967233988 *, const MethodInfo*))Catch_OnError_m298365966_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.CatchObservable`1/Catch<System.Object>::OnCompleted()
extern "C"  void Catch_OnCompleted_m4243229793_gshared (Catch_t3617674332 * __this, const MethodInfo* method);
#define Catch_OnCompleted_m4243229793(__this, method) ((  void (*) (Catch_t3617674332 *, const MethodInfo*))Catch_OnCompleted_m4243229793_gshared)(__this, method)
// System.Void UniRx.Operators.CatchObservable`1/Catch<System.Object>::<Run>m__77()
extern "C"  void Catch_U3CRunU3Em__77_m3241015371_gshared (Catch_t3617674332 * __this, const MethodInfo* method);
#define Catch_U3CRunU3Em__77_m3241015371(__this, method) ((  void (*) (Catch_t3617674332 *, const MethodInfo*))Catch_U3CRunU3Em__77_m3241015371_gshared)(__this, method)
