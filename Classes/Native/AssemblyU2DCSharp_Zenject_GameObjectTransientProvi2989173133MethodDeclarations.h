﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.GameObjectTransientProviderFromPrefab
struct GameObjectTransientProviderFromPrefab_t2989173133;
// System.Type
struct Type_t;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// System.Object
struct Il2CppObject;
// Zenject.InjectContext
struct InjectContext_t3456483891;
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException>
struct IEnumerable_1_t4073207355;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2779229935.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"
#include "AssemblyU2DCSharp_Zenject_InjectContext3456483891.h"

// System.Void Zenject.GameObjectTransientProviderFromPrefab::.ctor(System.Type,Zenject.DiContainer,UnityEngine.GameObject)
extern "C"  void GameObjectTransientProviderFromPrefab__ctor_m2837127112 (GameObjectTransientProviderFromPrefab_t2989173133 * __this, Type_t * ___concreteType0, DiContainer_t2383114449 * ___container1, GameObject_t4012695102 * ___template2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Zenject.GameObjectTransientProviderFromPrefab::GetInstanceType()
extern "C"  Type_t * GameObjectTransientProviderFromPrefab_GetInstanceType_m899269199 (GameObjectTransientProviderFromPrefab_t2989173133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Zenject.GameObjectTransientProviderFromPrefab::GetInstance(Zenject.InjectContext)
extern "C"  Il2CppObject * GameObjectTransientProviderFromPrefab_GetInstance_m2764616963 (GameObjectTransientProviderFromPrefab_t2989173133 * __this, InjectContext_t3456483891 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.GameObjectTransientProviderFromPrefab::ValidateBinding(Zenject.InjectContext)
extern "C"  Il2CppObject* GameObjectTransientProviderFromPrefab_ValidateBinding_m4245681557 (GameObjectTransientProviderFromPrefab_t2989173133 * __this, InjectContext_t3456483891 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
