﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/AddUserVirtualCurrencyRequestCallback
struct AddUserVirtualCurrencyRequestCallback_t2037651236;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.AddUserVirtualCurrencyRequest
struct AddUserVirtualCurrencyRequest_t2803224015;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_AddUserVirt2803224015.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/AddUserVirtualCurrencyRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void AddUserVirtualCurrencyRequestCallback__ctor_m3729687987 (AddUserVirtualCurrencyRequestCallback_t2037651236 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/AddUserVirtualCurrencyRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.AddUserVirtualCurrencyRequest,System.Object)
extern "C"  void AddUserVirtualCurrencyRequestCallback_Invoke_m521966993 (AddUserVirtualCurrencyRequestCallback_t2037651236 * __this, String_t* ___urlPath0, int32_t ___callId1, AddUserVirtualCurrencyRequest_t2803224015 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_AddUserVirtualCurrencyRequestCallback_t2037651236(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, AddUserVirtualCurrencyRequest_t2803224015 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/AddUserVirtualCurrencyRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.AddUserVirtualCurrencyRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * AddUserVirtualCurrencyRequestCallback_BeginInvoke_m1816425822 (AddUserVirtualCurrencyRequestCallback_t2037651236 * __this, String_t* ___urlPath0, int32_t ___callId1, AddUserVirtualCurrencyRequest_t2803224015 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/AddUserVirtualCurrencyRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void AddUserVirtualCurrencyRequestCallback_EndInvoke_m861325635 (AddUserVirtualCurrencyRequestCallback_t2037651236 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
