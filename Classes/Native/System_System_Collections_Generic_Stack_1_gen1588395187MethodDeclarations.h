﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Stack`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Stack_1_t1588395187;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerable_1_t1890143508;
// System.Object
struct Il2CppObject;
// System.Array
struct Il2CppArray;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t501095600;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23312956448.h"
#include "System_System_Collections_Generic_Stack_1_Enumerat2195698409.h"

// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor()
extern "C"  void Stack_1__ctor_m3329604301_gshared (Stack_1_t1588395187 * __this, const MethodInfo* method);
#define Stack_1__ctor_m3329604301(__this, method) ((  void (*) (Stack_1_t1588395187 *, const MethodInfo*))Stack_1__ctor_m3329604301_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void Stack_1__ctor_m1217418130_gshared (Stack_1_t1588395187 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define Stack_1__ctor_m1217418130(__this, ___collection0, method) ((  void (*) (Stack_1_t1588395187 *, Il2CppObject*, const MethodInfo*))Stack_1__ctor_m1217418130_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.Stack`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Stack_1_System_Collections_ICollection_get_IsSynchronized_m888626651_gshared (Stack_1_t1588395187 * __this, const MethodInfo* method);
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m888626651(__this, method) ((  bool (*) (Stack_1_t1588395187 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_IsSynchronized_m888626651_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Stack_1_System_Collections_ICollection_get_SyncRoot_m613022299_gshared (Stack_1_t1588395187 * __this, const MethodInfo* method);
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m613022299(__this, method) ((  Il2CppObject * (*) (Stack_1_t1588395187 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m613022299_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Stack_1_System_Collections_ICollection_CopyTo_m1931486999_gshared (Stack_1_t1588395187 * __this, Il2CppArray * ___dest0, int32_t ___idx1, const MethodInfo* method);
#define Stack_1_System_Collections_ICollection_CopyTo_m1931486999(__this, ___dest0, ___idx1, method) ((  void (*) (Stack_1_t1588395187 *, Il2CppArray *, int32_t, const MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m1931486999_gshared)(__this, ___dest0, ___idx1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m50117087_gshared (Stack_1_t1588395187 * __this, const MethodInfo* method);
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m50117087(__this, method) ((  Il2CppObject* (*) (Stack_1_t1588395187 *, const MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m50117087_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Stack_1_System_Collections_IEnumerable_GetEnumerator_m1044611302_gshared (Stack_1_t1588395187 * __this, const MethodInfo* method);
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m1044611302(__this, method) ((  Il2CppObject * (*) (Stack_1_t1588395187 *, const MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m1044611302_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Clear()
extern "C"  void Stack_1_Clear_m735737592_gshared (Stack_1_t1588395187 * __this, const MethodInfo* method);
#define Stack_1_Clear_m735737592(__this, method) ((  void (*) (Stack_1_t1588395187 *, const MethodInfo*))Stack_1_Clear_m735737592_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Contains(T)
extern "C"  bool Stack_1_Contains_m799370898_gshared (Stack_1_t1588395187 * __this, KeyValuePair_2_t3312956448  ___t0, const MethodInfo* method);
#define Stack_1_Contains_m799370898(__this, ___t0, method) ((  bool (*) (Stack_1_t1588395187 *, KeyValuePair_2_t3312956448 , const MethodInfo*))Stack_1_Contains_m799370898_gshared)(__this, ___t0, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Peek()
extern "C"  KeyValuePair_2_t3312956448  Stack_1_Peek_m1591094257_gshared (Stack_1_t1588395187 * __this, const MethodInfo* method);
#define Stack_1_Peek_m1591094257(__this, method) ((  KeyValuePair_2_t3312956448  (*) (Stack_1_t1588395187 *, const MethodInfo*))Stack_1_Peek_m1591094257_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Pop()
extern "C"  KeyValuePair_2_t3312956448  Stack_1_Pop_m190179357_gshared (Stack_1_t1588395187 * __this, const MethodInfo* method);
#define Stack_1_Pop_m190179357(__this, method) ((  KeyValuePair_2_t3312956448  (*) (Stack_1_t1588395187 *, const MethodInfo*))Stack_1_Pop_m190179357_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Push(T)
extern "C"  void Stack_1_Push_m3954081293_gshared (Stack_1_t1588395187 * __this, KeyValuePair_2_t3312956448  ___t0, const MethodInfo* method);
#define Stack_1_Push_m3954081293(__this, ___t0, method) ((  void (*) (Stack_1_t1588395187 *, KeyValuePair_2_t3312956448 , const MethodInfo*))Stack_1_Push_m3954081293_gshared)(__this, ___t0, method)
// System.Int32 System.Collections.Generic.Stack`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Count()
extern "C"  int32_t Stack_1_get_Count_m3785819681_gshared (Stack_1_t1588395187 * __this, const MethodInfo* method);
#define Stack_1_get_Count_m3785819681(__this, method) ((  int32_t (*) (Stack_1_t1588395187 *, const MethodInfo*))Stack_1_get_Count_m3785819681_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetEnumerator()
extern "C"  Enumerator_t2195698410  Stack_1_GetEnumerator_m530402883_gshared (Stack_1_t1588395187 * __this, const MethodInfo* method);
#define Stack_1_GetEnumerator_m530402883(__this, method) ((  Enumerator_t2195698410  (*) (Stack_1_t1588395187 *, const MethodInfo*))Stack_1_GetEnumerator_m530402883_gshared)(__this, method)
