﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.UpdatePlayerStatisticsRequest
struct UpdatePlayerStatisticsRequest_t2201995250;
// System.Collections.Generic.List`1<PlayFab.ClientModels.StatisticUpdate>
struct List_1_t4208248610;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.UpdatePlayerStatisticsRequest::.ctor()
extern "C"  void UpdatePlayerStatisticsRequest__ctor_m1171726711 (UpdatePlayerStatisticsRequest_t2201995250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PlayFab.ClientModels.StatisticUpdate> PlayFab.ClientModels.UpdatePlayerStatisticsRequest::get_Statistics()
extern "C"  List_1_t4208248610 * UpdatePlayerStatisticsRequest_get_Statistics_m1709936775 (UpdatePlayerStatisticsRequest_t2201995250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UpdatePlayerStatisticsRequest::set_Statistics(System.Collections.Generic.List`1<PlayFab.ClientModels.StatisticUpdate>)
extern "C"  void UpdatePlayerStatisticsRequest_set_Statistics_m163940000 (UpdatePlayerStatisticsRequest_t2201995250 * __this, List_1_t4208248610 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
