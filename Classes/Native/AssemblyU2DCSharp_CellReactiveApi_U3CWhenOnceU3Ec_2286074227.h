﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action
struct Action_t437523947;
// CellReactiveApi/<WhenOnce>c__AnonStorey107`1<System.Object>
struct U3CWhenOnceU3Ec__AnonStorey107_1_t2731099226;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CellReactiveApi/<WhenOnce>c__AnonStorey107`1/<WhenOnce>c__AnonStorey108`1<System.Object>
struct  U3CWhenOnceU3Ec__AnonStorey108_1_t2286074227  : public Il2CppObject
{
public:
	// System.Action CellReactiveApi/<WhenOnce>c__AnonStorey107`1/<WhenOnce>c__AnonStorey108`1::reaction
	Action_t437523947 * ___reaction_0;
	// CellReactiveApi/<WhenOnce>c__AnonStorey107`1<T> CellReactiveApi/<WhenOnce>c__AnonStorey107`1/<WhenOnce>c__AnonStorey108`1::<>f__ref$263
	U3CWhenOnceU3Ec__AnonStorey107_1_t2731099226 * ___U3CU3Ef__refU24263_1;

public:
	inline static int32_t get_offset_of_reaction_0() { return static_cast<int32_t>(offsetof(U3CWhenOnceU3Ec__AnonStorey108_1_t2286074227, ___reaction_0)); }
	inline Action_t437523947 * get_reaction_0() const { return ___reaction_0; }
	inline Action_t437523947 ** get_address_of_reaction_0() { return &___reaction_0; }
	inline void set_reaction_0(Action_t437523947 * value)
	{
		___reaction_0 = value;
		Il2CppCodeGenWriteBarrier(&___reaction_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24263_1() { return static_cast<int32_t>(offsetof(U3CWhenOnceU3Ec__AnonStorey108_1_t2286074227, ___U3CU3Ef__refU24263_1)); }
	inline U3CWhenOnceU3Ec__AnonStorey107_1_t2731099226 * get_U3CU3Ef__refU24263_1() const { return ___U3CU3Ef__refU24263_1; }
	inline U3CWhenOnceU3Ec__AnonStorey107_1_t2731099226 ** get_address_of_U3CU3Ef__refU24263_1() { return &___U3CU3Ef__refU24263_1; }
	inline void set_U3CU3Ef__refU24263_1(U3CWhenOnceU3Ec__AnonStorey107_1_t2731099226 * value)
	{
		___U3CU3Ef__refU24263_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24263_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
