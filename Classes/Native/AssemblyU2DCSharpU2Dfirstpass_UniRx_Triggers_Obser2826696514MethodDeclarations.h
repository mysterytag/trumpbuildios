﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Triggers.ObservableDragTrigger
struct ObservableDragTrigger_t2826696514;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t3205101634;
// UniRx.IObservable`1<UnityEngine.EventSystems.PointerEventData>
struct IObservable_1_t2963899998;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve3205101634.h"

// System.Void UniRx.Triggers.ObservableDragTrigger::.ctor()
extern "C"  void ObservableDragTrigger__ctor_m3366764457 (ObservableDragTrigger_t2826696514 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableDragTrigger::UnityEngine.EventSystems.IDragHandler.OnDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ObservableDragTrigger_UnityEngine_EventSystems_IDragHandler_OnDrag_m3591008316 (ObservableDragTrigger_t2826696514 * __this, PointerEventData_t3205101634 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableDragTrigger::OnDragAsObservable()
extern "C"  Il2CppObject* ObservableDragTrigger_OnDragAsObservable_m1379614535 (ObservableDragTrigger_t2826696514 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableDragTrigger::RaiseOnCompletedOnDestroy()
extern "C"  void ObservableDragTrigger_RaiseOnCompletedOnDestroy_m1223043170 (ObservableDragTrigger_t2826696514 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
