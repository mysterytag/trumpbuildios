﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.AudioClip
struct AudioClip_t3714538611;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.Int64>
struct List_1_t3644373851;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"
#include "mscorlib_System_String968488902.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"

// System.Void Utils::.cctor()
extern "C"  void Utils__cctor_m3721170675 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip Utils::get_buttonSound()
extern "C"  AudioClip_t3714538611 * Utils_get_buttonSound_m79639504 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Utils::get_screenRatio()
extern "C"  float Utils_get_screenRatio_m1775270746 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Utils::screenAspect(UnityEngine.Vector2)
extern "C"  float Utils_screenAspect_m3271177720 (Il2CppObject * __this /* static, unused */, Vector2_t3525329788  ___aspect0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Utils::GetAdjustedVector(UnityEngine.Vector3)
extern "C"  Vector3_t3525329789  Utils_GetAdjustedVector_m3498590496 (Il2CppObject * __this /* static, unused */, Vector3_t3525329789  ___vector0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Utils::GetNumberRepresentation(System.Double)
extern "C"  String_t* Utils_GetNumberRepresentation_m4179076411 (Il2CppObject * __this /* static, unused */, double ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Utils::TenPower(System.Int32)
extern "C"  float Utils_TenPower_m2580080503 (Il2CppObject * __this /* static, unused */, int32_t ___power0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Utils::GetCurrentTime()
extern "C"  int64_t Utils_GetCurrentTime_m727518987 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Utils::ConvertStringToLong(System.String)
extern "C"  int64_t Utils_ConvertStringToLong_m2991309504 (Il2CppObject * __this /* static, unused */, String_t* ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double Utils::ConvertStringToDouble(System.String)
extern "C"  double Utils_ConvertStringToDouble_m3890780343 (Il2CppObject * __this /* static, unused */, String_t* ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Utils::FormatString(System.String,System.Int32)
extern "C"  String_t* Utils_FormatString_m1390523396 (Il2CppObject * __this /* static, unused */, String_t* ___original0, int32_t ___maxCharacters1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color Utils::HexToColor(System.String)
extern "C"  Color_t1588175760  Utils_HexToColor_m445707922 (Il2CppObject * __this /* static, unused */, String_t* ___hex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Utils::HexToInt(System.Char)
extern "C"  int32_t Utils_HexToInt_m702162856 (Il2CppObject * __this /* static, unused */, uint16_t ___hex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Utils::ListToString(System.Collections.Generic.List`1<System.Int64>)
extern "C"  String_t* Utils_ListToString_m2054492687 (Il2CppObject * __this /* static, unused */, List_1_t3644373851 * ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Int64> Utils::StringToList(System.String)
extern "C"  List_1_t3644373851 * Utils_StringToList_m355533263 (Il2CppObject * __this /* static, unused */, String_t* ___liststring0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Utils::EaseIn(System.Single,System.Single,System.Single)
extern "C"  float Utils_EaseIn_m470274276 (Il2CppObject * __this /* static, unused */, float ___t0, float ___b1, float ___c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Utils::EaseOut(System.Single,System.Single,System.Single)
extern "C"  float Utils_EaseOut_m1505578001 (Il2CppObject * __this /* static, unused */, float ___t0, float ___b1, float ___c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
