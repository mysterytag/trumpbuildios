﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UpgradeButton
struct UpgradeButton_t1475467342;
// TableButtons
struct TableButtons_t1868573683;

#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharp_GlobalStat_UpgradeType4118271830.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TableButtons/<ClickHandler>c__AnonStorey159
struct  U3CClickHandlerU3Ec__AnonStorey159_t1424393390  : public Il2CppObject
{
public:
	// UpgradeButton TableButtons/<ClickHandler>c__AnonStorey159::button
	UpgradeButton_t1475467342 * ___button_0;
	// GlobalStat/UpgradeType TableButtons/<ClickHandler>c__AnonStorey159::upgradeType
	int32_t ___upgradeType_1;
	// TableButtons TableButtons/<ClickHandler>c__AnonStorey159::<>f__this
	TableButtons_t1868573683 * ___U3CU3Ef__this_2;

public:
	inline static int32_t get_offset_of_button_0() { return static_cast<int32_t>(offsetof(U3CClickHandlerU3Ec__AnonStorey159_t1424393390, ___button_0)); }
	inline UpgradeButton_t1475467342 * get_button_0() const { return ___button_0; }
	inline UpgradeButton_t1475467342 ** get_address_of_button_0() { return &___button_0; }
	inline void set_button_0(UpgradeButton_t1475467342 * value)
	{
		___button_0 = value;
		Il2CppCodeGenWriteBarrier(&___button_0, value);
	}

	inline static int32_t get_offset_of_upgradeType_1() { return static_cast<int32_t>(offsetof(U3CClickHandlerU3Ec__AnonStorey159_t1424393390, ___upgradeType_1)); }
	inline int32_t get_upgradeType_1() const { return ___upgradeType_1; }
	inline int32_t* get_address_of_upgradeType_1() { return &___upgradeType_1; }
	inline void set_upgradeType_1(int32_t value)
	{
		___upgradeType_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_2() { return static_cast<int32_t>(offsetof(U3CClickHandlerU3Ec__AnonStorey159_t1424393390, ___U3CU3Ef__this_2)); }
	inline TableButtons_t1868573683 * get_U3CU3Ef__this_2() const { return ___U3CU3Ef__this_2; }
	inline TableButtons_t1868573683 ** get_address_of_U3CU3Ef__this_2() { return &___U3CU3Ef__this_2; }
	inline void set_U3CU3Ef__this_2(TableButtons_t1868573683 * value)
	{
		___U3CU3Ef__this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
