﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.WebClient
struct WebClient_t4210428809;
// System.Net.DownloadDataCompletedEventHandler
struct DownloadDataCompletedEventHandler_t2491706195;
// System.ComponentModel.AsyncCompletedEventHandler
struct AsyncCompletedEventHandler_t2512468712;
// System.Net.DownloadProgressChangedEventHandler
struct DownloadProgressChangedEventHandler_t3634764377;
// System.Net.DownloadStringCompletedEventHandler
struct DownloadStringCompletedEventHandler_t4056081228;
// System.Net.OpenReadCompletedEventHandler
struct OpenReadCompletedEventHandler_t2398543205;
// System.Net.OpenWriteCompletedEventHandler
struct OpenWriteCompletedEventHandler_t371723280;
// System.Net.UploadDataCompletedEventHandler
struct UploadDataCompletedEventHandler_t1421445114;
// System.Net.UploadFileCompletedEventHandler
struct UploadFileCompletedEventHandler_t95467880;
// System.Net.UploadProgressChangedEventHandler
struct UploadProgressChangedEventHandler_t1611049280;
// System.Net.UploadStringCompletedEventHandler
struct UploadStringCompletedEventHandler_t2032366131;
// System.Net.UploadValuesCompletedEventHandler
struct UploadValuesCompletedEventHandler_t1802287554;
// System.String
struct String_t;
// System.Exception
struct Exception_t1967233988;
// System.Net.Cache.RequestCachePolicy
struct RequestCachePolicy_t3921445643;
// System.Net.ICredentials
struct ICredentials_t2307785309;
// System.Net.WebHeaderCollection
struct WebHeaderCollection_t1099177929;
// System.Collections.Specialized.NameValueCollection
struct NameValueCollection_t3455488335;
// System.Text.Encoding
struct Encoding_t180559927;
// System.Net.IWebProxy
struct IWebProxy_t49946189;
// System.Byte[]
struct ByteU5BU5D_t58506160;
// System.Uri
struct Uri_t2776692961;
// System.Object
struct Il2CppObject;
// System.IO.Stream
struct Stream_t219029575;
// System.Net.WebRequest
struct WebRequest_t3488810021;
// System.Net.DownloadDataCompletedEventArgs
struct DownloadDataCompletedEventArgs_t3432986632;
// System.ComponentModel.AsyncCompletedEventArgs
struct AsyncCompletedEventArgs_t2437453489;
// System.Net.DownloadProgressChangedEventArgs
struct DownloadProgressChangedEventArgs_t1454724802;
// System.Net.DownloadStringCompletedEventArgs
struct DownloadStringCompletedEventArgs_t4050086575;
// System.Net.OpenReadCompletedEventArgs
struct OpenReadCompletedEventArgs_t223760182;
// System.Net.OpenWriteCompletedEventArgs
struct OpenWriteCompletedEventArgs_t2142017643;
// System.Net.UploadDataCompletedEventArgs
struct UploadDataCompletedEventArgs_t737981633;
// System.Net.UploadFileCompletedEventArgs
struct UploadFileCompletedEventArgs_t2540349971;
// System.Net.UploadProgressChangedEventArgs
struct UploadProgressChangedEventArgs_t1420200251;
// System.Net.UploadStringCompletedEventArgs
struct UploadStringCompletedEventArgs_t4015562024;
// System.Net.UploadValuesCompletedEventArgs
struct UploadValuesCompletedEventArgs_t1359222777;
// System.Net.WebResponse
struct WebResponse_t2411292415;
// System.IAsyncResult
struct IAsyncResult_t537683269;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_DownloadDataCompletedEventHandle2491706195.h"
#include "System_System_ComponentModel_AsyncCompletedEventHa2512468712.h"
#include "System_System_Net_DownloadProgressChangedEventHand3634764377.h"
#include "System_System_Net_DownloadStringCompletedEventHand4056081228.h"
#include "System_System_Net_OpenReadCompletedEventHandler2398543205.h"
#include "System_System_Net_OpenWriteCompletedEventHandler371723280.h"
#include "System_System_Net_UploadDataCompletedEventHandler1421445114.h"
#include "System_System_Net_UploadFileCompletedEventHandler95467880.h"
#include "System_System_Net_UploadProgressChangedEventHandle1611049280.h"
#include "System_System_Net_UploadStringCompletedEventHandle2032366131.h"
#include "System_System_Net_UploadValuesCompletedEventHandle1802287554.h"
#include "mscorlib_System_String968488902.h"
#include "System_System_Net_Cache_RequestCachePolicy3921445643.h"
#include "System_System_Net_WebHeaderCollection1099177929.h"
#include "System_System_Collections_Specialized_NameValueCol3455488335.h"
#include "mscorlib_System_Text_Encoding180559927.h"
#include "System_System_Uri2776692961.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IO_Stream219029575.h"
#include "System_System_Net_DownloadDataCompletedEventArgs3432986632.h"
#include "System_System_ComponentModel_AsyncCompletedEventAr2437453489.h"
#include "System_System_Net_DownloadProgressChangedEventArgs1454724802.h"
#include "System_System_Net_DownloadStringCompletedEventArgs4050086575.h"
#include "System_System_Net_OpenReadCompletedEventArgs223760182.h"
#include "System_System_Net_OpenWriteCompletedEventArgs2142017643.h"
#include "System_System_Net_UploadDataCompletedEventArgs737981633.h"
#include "System_System_Net_UploadFileCompletedEventArgs2540349971.h"
#include "System_System_Net_UploadProgressChangedEventArgs1420200251.h"
#include "System_System_Net_UploadStringCompletedEventArgs4015562024.h"
#include "System_System_Net_UploadValuesCompletedEventArgs1359222777.h"
#include "System_System_Net_WebRequest3488810021.h"

// System.Void System.Net.WebClient::.ctor()
extern "C"  void WebClient__ctor_m2477960557 (WebClient_t4210428809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::.cctor()
extern "C"  void WebClient__cctor_m3320237024 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::add_DownloadDataCompleted(System.Net.DownloadDataCompletedEventHandler)
extern "C"  void WebClient_add_DownloadDataCompleted_m1644233741 (WebClient_t4210428809 * __this, DownloadDataCompletedEventHandler_t2491706195 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::remove_DownloadDataCompleted(System.Net.DownloadDataCompletedEventHandler)
extern "C"  void WebClient_remove_DownloadDataCompleted_m2230722268 (WebClient_t4210428809 * __this, DownloadDataCompletedEventHandler_t2491706195 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::add_DownloadFileCompleted(System.ComponentModel.AsyncCompletedEventHandler)
extern "C"  void WebClient_add_DownloadFileCompleted_m1127005874 (WebClient_t4210428809 * __this, AsyncCompletedEventHandler_t2512468712 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::remove_DownloadFileCompleted(System.ComponentModel.AsyncCompletedEventHandler)
extern "C"  void WebClient_remove_DownloadFileCompleted_m1567218177 (WebClient_t4210428809 * __this, AsyncCompletedEventHandler_t2512468712 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::add_DownloadProgressChanged(System.Net.DownloadProgressChangedEventHandler)
extern "C"  void WebClient_add_DownloadProgressChanged_m1157107597 (WebClient_t4210428809 * __this, DownloadProgressChangedEventHandler_t3634764377 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::remove_DownloadProgressChanged(System.Net.DownloadProgressChangedEventHandler)
extern "C"  void WebClient_remove_DownloadProgressChanged_m1597319900 (WebClient_t4210428809 * __this, DownloadProgressChangedEventHandler_t3634764377 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::add_DownloadStringCompleted(System.Net.DownloadStringCompletedEventHandler)
extern "C"  void WebClient_add_DownloadStringCompleted_m3491426413 (WebClient_t4210428809 * __this, DownloadStringCompletedEventHandler_t4056081228 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::remove_DownloadStringCompleted(System.Net.DownloadStringCompletedEventHandler)
extern "C"  void WebClient_remove_DownloadStringCompleted_m3931638716 (WebClient_t4210428809 * __this, DownloadStringCompletedEventHandler_t4056081228 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::add_OpenReadCompleted(System.Net.OpenReadCompletedEventHandler)
extern "C"  void WebClient_add_OpenReadCompleted_m4165967693 (WebClient_t4210428809 * __this, OpenReadCompletedEventHandler_t2398543205 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::remove_OpenReadCompleted(System.Net.OpenReadCompletedEventHandler)
extern "C"  void WebClient_remove_OpenReadCompleted_m4116183324 (WebClient_t4210428809 * __this, OpenReadCompletedEventHandler_t2398543205 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::add_OpenWriteCompleted(System.Net.OpenWriteCompletedEventHandler)
extern "C"  void WebClient_add_OpenWriteCompleted_m2996882417 (WebClient_t4210428809 * __this, OpenWriteCompletedEventHandler_t371723280 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::remove_OpenWriteCompleted(System.Net.OpenWriteCompletedEventHandler)
extern "C"  void WebClient_remove_OpenWriteCompleted_m2398744064 (WebClient_t4210428809 * __this, OpenWriteCompletedEventHandler_t371723280 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::add_UploadDataCompleted(System.Net.UploadDataCompletedEventHandler)
extern "C"  void WebClient_add_UploadDataCompleted_m3980498733 (WebClient_t4210428809 * __this, UploadDataCompletedEventHandler_t1421445114 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::remove_UploadDataCompleted(System.Net.UploadDataCompletedEventHandler)
extern "C"  void WebClient_remove_UploadDataCompleted_m400191868 (WebClient_t4210428809 * __this, UploadDataCompletedEventHandler_t1421445114 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::add_UploadFileCompleted(System.Net.UploadFileCompletedEventHandler)
extern "C"  void WebClient_add_UploadFileCompleted_m3453390061 (WebClient_t4210428809 * __this, UploadFileCompletedEventHandler_t95467880 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::remove_UploadFileCompleted(System.Net.UploadFileCompletedEventHandler)
extern "C"  void WebClient_remove_UploadFileCompleted_m4168050492 (WebClient_t4210428809 * __this, UploadFileCompletedEventHandler_t95467880 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::add_UploadProgressChanged(System.Net.UploadProgressChangedEventHandler)
extern "C"  void WebClient_add_UploadProgressChanged_m1310918253 (WebClient_t4210428809 * __this, UploadProgressChangedEventHandler_t1611049280 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::remove_UploadProgressChanged(System.Net.UploadProgressChangedEventHandler)
extern "C"  void WebClient_remove_UploadProgressChanged_m1897406780 (WebClient_t4210428809 * __this, UploadProgressChangedEventHandler_t1611049280 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::add_UploadStringCompleted(System.Net.UploadStringCompletedEventHandler)
extern "C"  void WebClient_add_UploadStringCompleted_m541599757 (WebClient_t4210428809 * __this, UploadStringCompletedEventHandler_t2032366131 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::remove_UploadStringCompleted(System.Net.UploadStringCompletedEventHandler)
extern "C"  void WebClient_remove_UploadStringCompleted_m1128088284 (WebClient_t4210428809 * __this, UploadStringCompletedEventHandler_t2032366131 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::add_UploadValuesCompleted(System.Net.UploadValuesCompletedEventHandler)
extern "C"  void WebClient_add_UploadValuesCompleted_m661812781 (WebClient_t4210428809 * __this, UploadValuesCompletedEventHandler_t1802287554 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::remove_UploadValuesCompleted(System.Net.UploadValuesCompletedEventHandler)
extern "C"  void WebClient_remove_UploadValuesCompleted_m1248301308 (WebClient_t4210428809 * __this, UploadValuesCompletedEventHandler_t1802287554 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.WebClient::get_BaseAddress()
extern "C"  String_t* WebClient_get_BaseAddress_m2499968066 (WebClient_t4210428809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::set_BaseAddress(System.String)
extern "C"  void WebClient_set_BaseAddress_m784500369 (WebClient_t4210428809 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception System.Net.WebClient::GetMustImplement()
extern "C"  Exception_t1967233988 * WebClient_GetMustImplement_m3275099818 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.Cache.RequestCachePolicy System.Net.WebClient::get_CachePolicy()
extern "C"  RequestCachePolicy_t3921445643 * WebClient_get_CachePolicy_m2217008964 (WebClient_t4210428809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::set_CachePolicy(System.Net.Cache.RequestCachePolicy)
extern "C"  void WebClient_set_CachePolicy_m1836419215 (WebClient_t4210428809 * __this, RequestCachePolicy_t3921445643 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.WebClient::get_UseDefaultCredentials()
extern "C"  bool WebClient_get_UseDefaultCredentials_m3885254958 (WebClient_t4210428809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::set_UseDefaultCredentials(System.Boolean)
extern "C"  void WebClient_set_UseDefaultCredentials_m3104701831 (WebClient_t4210428809 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.ICredentials System.Net.WebClient::get_Credentials()
extern "C"  Il2CppObject * WebClient_get_Credentials_m2715496302 (WebClient_t4210428809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::set_Credentials(System.Net.ICredentials)
extern "C"  void WebClient_set_Credentials_m1832201509 (WebClient_t4210428809 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebHeaderCollection System.Net.WebClient::get_Headers()
extern "C"  WebHeaderCollection_t1099177929 * WebClient_get_Headers_m4159086712 (WebClient_t4210428809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::set_Headers(System.Net.WebHeaderCollection)
extern "C"  void WebClient_set_Headers_m2006085189 (WebClient_t4210428809 * __this, WebHeaderCollection_t1099177929 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Specialized.NameValueCollection System.Net.WebClient::get_QueryString()
extern "C"  NameValueCollection_t3455488335 * WebClient_get_QueryString_m877049155 (WebClient_t4210428809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::set_QueryString(System.Collections.Specialized.NameValueCollection)
extern "C"  void WebClient_set_QueryString_m3484543904 (WebClient_t4210428809 * __this, NameValueCollection_t3455488335 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebHeaderCollection System.Net.WebClient::get_ResponseHeaders()
extern "C"  WebHeaderCollection_t1099177929 * WebClient_get_ResponseHeaders_m426632247 (WebClient_t4210428809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoding System.Net.WebClient::get_Encoding()
extern "C"  Encoding_t180559927 * WebClient_get_Encoding_m303928765 (WebClient_t4210428809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::set_Encoding(System.Text.Encoding)
extern "C"  void WebClient_set_Encoding_m1478904018 (WebClient_t4210428809 * __this, Encoding_t180559927 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IWebProxy System.Net.WebClient::get_Proxy()
extern "C"  Il2CppObject * WebClient_get_Proxy_m212112292 (WebClient_t4210428809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::set_Proxy(System.Net.IWebProxy)
extern "C"  void WebClient_set_Proxy_m4092987945 (WebClient_t4210428809 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.WebClient::get_IsBusy()
extern "C"  bool WebClient_get_IsBusy_m2414763289 (WebClient_t4210428809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::CheckBusy()
extern "C"  void WebClient_CheckBusy_m581754988 (WebClient_t4210428809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::SetBusy()
extern "C"  void WebClient_SetBusy_m1659299526 (WebClient_t4210428809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.WebClient::DownloadData(System.String)
extern "C"  ByteU5BU5D_t58506160* WebClient_DownloadData_m4193784751 (WebClient_t4210428809 * __this, String_t* ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.WebClient::DownloadData(System.Uri)
extern "C"  ByteU5BU5D_t58506160* WebClient_DownloadData_m2315567654 (WebClient_t4210428809 * __this, Uri_t2776692961 * ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.WebClient::DownloadDataCore(System.Uri,System.Object)
extern "C"  ByteU5BU5D_t58506160* WebClient_DownloadDataCore_m2361543795 (WebClient_t4210428809 * __this, Uri_t2776692961 * ___address0, Il2CppObject * ___userToken1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::DownloadFile(System.String,System.String)
extern "C"  void WebClient_DownloadFile_m3157892483 (WebClient_t4210428809 * __this, String_t* ___address0, String_t* ___fileName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::DownloadFile(System.Uri,System.String)
extern "C"  void WebClient_DownloadFile_m1375207178 (WebClient_t4210428809 * __this, Uri_t2776692961 * ___address0, String_t* ___fileName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::DownloadFileCore(System.Uri,System.String,System.Object)
extern "C"  void WebClient_DownloadFileCore_m346385431 (WebClient_t4210428809 * __this, Uri_t2776692961 * ___address0, String_t* ___fileName1, Il2CppObject * ___userToken2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.Net.WebClient::OpenRead(System.String)
extern "C"  Stream_t219029575 * WebClient_OpenRead_m3296615413 (WebClient_t4210428809 * __this, String_t* ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.Net.WebClient::OpenRead(System.Uri)
extern "C"  Stream_t219029575 * WebClient_OpenRead_m2405355424 (WebClient_t4210428809 * __this, Uri_t2776692961 * ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.Net.WebClient::OpenWrite(System.String)
extern "C"  Stream_t219029575 * WebClient_OpenWrite_m1699042936 (WebClient_t4210428809 * __this, String_t* ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.Net.WebClient::OpenWrite(System.String,System.String)
extern "C"  Stream_t219029575 * WebClient_OpenWrite_m764005556 (WebClient_t4210428809 * __this, String_t* ___address0, String_t* ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.Net.WebClient::OpenWrite(System.Uri)
extern "C"  Stream_t219029575 * WebClient_OpenWrite_m3206454269 (WebClient_t4210428809 * __this, Uri_t2776692961 * ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.Net.WebClient::OpenWrite(System.Uri,System.String)
extern "C"  Stream_t219029575 * WebClient_OpenWrite_m2856617337 (WebClient_t4210428809 * __this, Uri_t2776692961 * ___address0, String_t* ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.WebClient::DetermineMethod(System.Uri,System.String,System.Boolean)
extern "C"  String_t* WebClient_DetermineMethod_m196435218 (WebClient_t4210428809 * __this, Uri_t2776692961 * ___address0, String_t* ___method1, bool ___is_upload2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.WebClient::UploadData(System.String,System.Byte[])
extern "C"  ByteU5BU5D_t58506160* WebClient_UploadData_m1404318745 (WebClient_t4210428809 * __this, String_t* ___address0, ByteU5BU5D_t58506160* ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.WebClient::UploadData(System.String,System.String,System.Byte[])
extern "C"  ByteU5BU5D_t58506160* WebClient_UploadData_m1237853397 (WebClient_t4210428809 * __this, String_t* ___address0, String_t* ___method1, ByteU5BU5D_t58506160* ___data2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.WebClient::UploadData(System.Uri,System.Byte[])
extern "C"  ByteU5BU5D_t58506160* WebClient_UploadData_m1312003170 (WebClient_t4210428809 * __this, Uri_t2776692961 * ___address0, ByteU5BU5D_t58506160* ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.WebClient::UploadData(System.Uri,System.String,System.Byte[])
extern "C"  ByteU5BU5D_t58506160* WebClient_UploadData_m1634783582 (WebClient_t4210428809 * __this, Uri_t2776692961 * ___address0, String_t* ___method1, ByteU5BU5D_t58506160* ___data2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.WebClient::UploadDataCore(System.Uri,System.String,System.Byte[],System.Object)
extern "C"  ByteU5BU5D_t58506160* WebClient_UploadDataCore_m7188651 (WebClient_t4210428809 * __this, Uri_t2776692961 * ___address0, String_t* ___method1, ByteU5BU5D_t58506160* ___data2, Il2CppObject * ___userToken3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.WebClient::UploadFile(System.String,System.String)
extern "C"  ByteU5BU5D_t58506160* WebClient_UploadFile_m1727641280 (WebClient_t4210428809 * __this, String_t* ___address0, String_t* ___fileName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.WebClient::UploadFile(System.Uri,System.String)
extern "C"  ByteU5BU5D_t58506160* WebClient_UploadFile_m1936701165 (WebClient_t4210428809 * __this, Uri_t2776692961 * ___address0, String_t* ___fileName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.WebClient::UploadFile(System.String,System.String,System.String)
extern "C"  ByteU5BU5D_t58506160* WebClient_UploadFile_m2982940412 (WebClient_t4210428809 * __this, String_t* ___address0, String_t* ___method1, String_t* ___fileName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.WebClient::UploadFile(System.Uri,System.String,System.String)
extern "C"  ByteU5BU5D_t58506160* WebClient_UploadFile_m2203303017 (WebClient_t4210428809 * __this, Uri_t2776692961 * ___address0, String_t* ___method1, String_t* ___fileName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.WebClient::UploadFileCore(System.Uri,System.String,System.String,System.Object)
extern "C"  ByteU5BU5D_t58506160* WebClient_UploadFileCore_m2727375734 (WebClient_t4210428809 * __this, Uri_t2776692961 * ___address0, String_t* ___method1, String_t* ___fileName2, Il2CppObject * ___userToken3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.WebClient::UploadValues(System.String,System.Collections.Specialized.NameValueCollection)
extern "C"  ByteU5BU5D_t58506160* WebClient_UploadValues_m917739169 (WebClient_t4210428809 * __this, String_t* ___address0, NameValueCollection_t3455488335 * ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.WebClient::UploadValues(System.String,System.String,System.Collections.Specialized.NameValueCollection)
extern "C"  ByteU5BU5D_t58506160* WebClient_UploadValues_m3677268325 (WebClient_t4210428809 * __this, String_t* ___address0, String_t* ___method1, NameValueCollection_t3455488335 * ___data2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.WebClient::UploadValues(System.Uri,System.Collections.Specialized.NameValueCollection)
extern "C"  ByteU5BU5D_t58506160* WebClient_UploadValues_m1572136072 (WebClient_t4210428809 * __this, Uri_t2776692961 * ___address0, NameValueCollection_t3455488335 * ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.WebClient::UploadValues(System.Uri,System.String,System.Collections.Specialized.NameValueCollection)
extern "C"  ByteU5BU5D_t58506160* WebClient_UploadValues_m1080170252 (WebClient_t4210428809 * __this, Uri_t2776692961 * ___address0, String_t* ___method1, NameValueCollection_t3455488335 * ___data2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.WebClient::UploadValuesCore(System.Uri,System.String,System.Collections.Specialized.NameValueCollection,System.Object)
extern "C"  ByteU5BU5D_t58506160* WebClient_UploadValuesCore_m741780283 (WebClient_t4210428809 * __this, Uri_t2776692961 * ___uri0, String_t* ___method1, NameValueCollection_t3455488335 * ___data2, Il2CppObject * ___userToken3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.WebClient::DownloadString(System.String)
extern "C"  String_t* WebClient_DownloadString_m2198465167 (WebClient_t4210428809 * __this, String_t* ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.WebClient::DownloadString(System.Uri)
extern "C"  String_t* WebClient_DownloadString_m2310887238 (WebClient_t4210428809 * __this, Uri_t2776692961 * ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.WebClient::UploadString(System.String,System.String)
extern "C"  String_t* WebClient_UploadString_m1273373234 (WebClient_t4210428809 * __this, String_t* ___address0, String_t* ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.WebClient::UploadString(System.String,System.String,System.String)
extern "C"  String_t* WebClient_UploadString_m1121356526 (WebClient_t4210428809 * __this, String_t* ___address0, String_t* ___method1, String_t* ___data2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.WebClient::UploadString(System.Uri,System.String)
extern "C"  String_t* WebClient_UploadString_m61755579 (WebClient_t4210428809 * __this, Uri_t2776692961 * ___address0, String_t* ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.WebClient::UploadString(System.Uri,System.String,System.String)
extern "C"  String_t* WebClient_UploadString_m1075975607 (WebClient_t4210428809 * __this, Uri_t2776692961 * ___address0, String_t* ___method1, String_t* ___data2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri System.Net.WebClient::CreateUri(System.String)
extern "C"  Uri_t2776692961 * WebClient_CreateUri_m3126770009 (WebClient_t4210428809 * __this, String_t* ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri System.Net.WebClient::CreateUri(System.Uri)
extern "C"  Uri_t2776692961 * WebClient_CreateUri_m2481327292 (WebClient_t4210428809 * __this, Uri_t2776692961 * ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.WebClient::GetQueryString(System.Boolean)
extern "C"  String_t* WebClient_GetQueryString_m3086610068 (WebClient_t4210428809 * __this, bool ___add_qmark0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri System.Net.WebClient::MakeUri(System.String)
extern "C"  Uri_t2776692961 * WebClient_MakeUri_m1674872779 (WebClient_t4210428809 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebRequest System.Net.WebClient::SetupRequest(System.Uri)
extern "C"  WebRequest_t3488810021 * WebClient_SetupRequest_m931338404 (WebClient_t4210428809 * __this, Uri_t2776692961 * ___uri0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebRequest System.Net.WebClient::SetupRequest(System.Uri,System.String,System.Boolean)
extern "C"  WebRequest_t3488810021 * WebClient_SetupRequest_m479462813 (WebClient_t4210428809 * __this, Uri_t2776692961 * ___uri0, String_t* ___method1, bool ___is_upload2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.WebClient::ReadAll(System.IO.Stream,System.Int32,System.Object)
extern "C"  ByteU5BU5D_t58506160* WebClient_ReadAll_m1240881634 (WebClient_t4210428809 * __this, Stream_t219029575 * ___stream0, int32_t ___length1, Il2CppObject * ___userToken2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.WebClient::UrlEncode(System.String)
extern "C"  String_t* WebClient_UrlEncode_m1541238133 (WebClient_t4210428809 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::UrlEncodeAndWrite(System.IO.Stream,System.Byte[])
extern "C"  void WebClient_UrlEncodeAndWrite_m3552878962 (Il2CppObject * __this /* static, unused */, Stream_t219029575 * ___stream0, ByteU5BU5D_t58506160* ___bytes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::CancelAsync()
extern "C"  void WebClient_CancelAsync_m64529069 (WebClient_t4210428809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::CompleteAsync()
extern "C"  void WebClient_CompleteAsync_m4142956686 (WebClient_t4210428809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::DownloadDataAsync(System.Uri)
extern "C"  void WebClient_DownloadDataAsync_m2780217288 (WebClient_t4210428809 * __this, Uri_t2776692961 * ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::DownloadDataAsync(System.Uri,System.Object)
extern "C"  void WebClient_DownloadDataAsync_m1956498582 (WebClient_t4210428809 * __this, Uri_t2776692961 * ___address0, Il2CppObject * ___userToken1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::DownloadFileAsync(System.Uri,System.String)
extern "C"  void WebClient_DownloadFileAsync_m1343449586 (WebClient_t4210428809 * __this, Uri_t2776692961 * ___address0, String_t* ___fileName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::DownloadFileAsync(System.Uri,System.String,System.Object)
extern "C"  void WebClient_DownloadFileAsync_m1082680640 (WebClient_t4210428809 * __this, Uri_t2776692961 * ___address0, String_t* ___fileName1, Il2CppObject * ___userToken2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::DownloadStringAsync(System.Uri)
extern "C"  void WebClient_DownloadStringAsync_m3513006529 (WebClient_t4210428809 * __this, Uri_t2776692961 * ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::DownloadStringAsync(System.Uri,System.Object)
extern "C"  void WebClient_DownloadStringAsync_m1227742927 (WebClient_t4210428809 * __this, Uri_t2776692961 * ___address0, Il2CppObject * ___userToken1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::OpenReadAsync(System.Uri)
extern "C"  void WebClient_OpenReadAsync_m2812325850 (WebClient_t4210428809 * __this, Uri_t2776692961 * ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::OpenReadAsync(System.Uri,System.Object)
extern "C"  void WebClient_OpenReadAsync_m1875981608 (WebClient_t4210428809 * __this, Uri_t2776692961 * ___address0, Il2CppObject * ___userToken1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::OpenWriteAsync(System.Uri)
extern "C"  void WebClient_OpenWriteAsync_m1687655153 (WebClient_t4210428809 * __this, Uri_t2776692961 * ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::OpenWriteAsync(System.Uri,System.String)
extern "C"  void WebClient_OpenWriteAsync_m3673702765 (WebClient_t4210428809 * __this, Uri_t2776692961 * ___address0, String_t* ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::OpenWriteAsync(System.Uri,System.String,System.Object)
extern "C"  void WebClient_OpenWriteAsync_m1595214203 (WebClient_t4210428809 * __this, Uri_t2776692961 * ___address0, String_t* ___method1, Il2CppObject * ___userToken2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::UploadDataAsync(System.Uri,System.Byte[])
extern "C"  void WebClient_UploadDataAsync_m488760626 (WebClient_t4210428809 * __this, Uri_t2776692961 * ___address0, ByteU5BU5D_t58506160* ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::UploadDataAsync(System.Uri,System.String,System.Byte[])
extern "C"  void WebClient_UploadDataAsync_m190412334 (WebClient_t4210428809 * __this, Uri_t2776692961 * ___address0, String_t* ___method1, ByteU5BU5D_t58506160* ___data2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::UploadDataAsync(System.Uri,System.String,System.Byte[],System.Object)
extern "C"  void WebClient_UploadDataAsync_m565598332 (WebClient_t4210428809 * __this, Uri_t2776692961 * ___address0, String_t* ___method1, ByteU5BU5D_t58506160* ___data2, Il2CppObject * ___userToken3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::UploadFileAsync(System.Uri,System.String)
extern "C"  void WebClient_UploadFileAsync_m2155627609 (WebClient_t4210428809 * __this, Uri_t2776692961 * ___address0, String_t* ___fileName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::UploadFileAsync(System.Uri,System.String,System.String)
extern "C"  void WebClient_UploadFileAsync_m2373344469 (WebClient_t4210428809 * __this, Uri_t2776692961 * ___address0, String_t* ___method1, String_t* ___fileName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::UploadFileAsync(System.Uri,System.String,System.String,System.Object)
extern "C"  void WebClient_UploadFileAsync_m2188054243 (WebClient_t4210428809 * __this, Uri_t2776692961 * ___address0, String_t* ___method1, String_t* ___fileName2, Il2CppObject * ___userToken3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::UploadStringAsync(System.Uri,System.String)
extern "C"  void WebClient_UploadStringAsync_m4119632100 (WebClient_t4210428809 * __this, Uri_t2776692961 * ___address0, String_t* ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::UploadStringAsync(System.Uri,System.String,System.String)
extern "C"  void WebClient_UploadStringAsync_m2944819744 (WebClient_t4210428809 * __this, Uri_t2776692961 * ___address0, String_t* ___method1, String_t* ___data2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::UploadStringAsync(System.Uri,System.String,System.String,System.Object)
extern "C"  void WebClient_UploadStringAsync_m1691776238 (WebClient_t4210428809 * __this, Uri_t2776692961 * ___address0, String_t* ___method1, String_t* ___data2, Il2CppObject * ___userToken3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::UploadValuesAsync(System.Uri,System.Collections.Specialized.NameValueCollection)
extern "C"  void WebClient_UploadValuesAsync_m239453608 (WebClient_t4210428809 * __this, Uri_t2776692961 * ___address0, NameValueCollection_t3455488335 * ___values1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::UploadValuesAsync(System.Uri,System.String,System.Collections.Specialized.NameValueCollection)
extern "C"  void WebClient_UploadValuesAsync_m3368189996 (WebClient_t4210428809 * __this, Uri_t2776692961 * ___address0, String_t* ___method1, NameValueCollection_t3455488335 * ___values2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::UploadValuesAsync(System.Uri,System.String,System.Collections.Specialized.NameValueCollection,System.Object)
extern "C"  void WebClient_UploadValuesAsync_m2446265850 (WebClient_t4210428809 * __this, Uri_t2776692961 * ___address0, String_t* ___method1, NameValueCollection_t3455488335 * ___values2, Il2CppObject * ___userToken3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::OnDownloadDataCompleted(System.Net.DownloadDataCompletedEventArgs)
extern "C"  void WebClient_OnDownloadDataCompleted_m729441119 (WebClient_t4210428809 * __this, DownloadDataCompletedEventArgs_t3432986632 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::OnDownloadFileCompleted(System.ComponentModel.AsyncCompletedEventArgs)
extern "C"  void WebClient_OnDownloadFileCompleted_m2547743322 (WebClient_t4210428809 * __this, AsyncCompletedEventArgs_t2437453489 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::OnDownloadProgressChanged(System.Net.DownloadProgressChangedEventArgs)
extern "C"  void WebClient_OnDownloadProgressChanged_m2326155103 (WebClient_t4210428809 * __this, DownloadProgressChangedEventArgs_t1454724802 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::OnDownloadStringCompleted(System.Net.DownloadStringCompletedEventArgs)
extern "C"  void WebClient_OnDownloadStringCompleted_m2532973183 (WebClient_t4210428809 * __this, DownloadStringCompletedEventArgs_t4050086575 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::OnOpenReadCompleted(System.Net.OpenReadCompletedEventArgs)
extern "C"  void WebClient_OnOpenReadCompleted_m1759363359 (WebClient_t4210428809 * __this, OpenReadCompletedEventArgs_t223760182 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::OnOpenWriteCompleted(System.Net.OpenWriteCompletedEventArgs)
extern "C"  void WebClient_OnOpenWriteCompleted_m3285832379 (WebClient_t4210428809 * __this, OpenWriteCompletedEventArgs_t2142017643 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::OnUploadDataCompleted(System.Net.UploadDataCompletedEventArgs)
extern "C"  void WebClient_OnUploadDataCompleted_m2674695359 (WebClient_t4210428809 * __this, UploadDataCompletedEventArgs_t737981633 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::OnUploadFileCompleted(System.Net.UploadFileCompletedEventArgs)
extern "C"  void WebClient_OnUploadFileCompleted_m3744562943 (WebClient_t4210428809 * __this, UploadFileCompletedEventArgs_t2540349971 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::OnUploadProgressChanged(System.Net.UploadProgressChangedEventArgs)
extern "C"  void WebClient_OnUploadProgressChanged_m2088664319 (WebClient_t4210428809 * __this, UploadProgressChangedEventArgs_t1420200251 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::OnUploadStringCompleted(System.Net.UploadStringCompletedEventArgs)
extern "C"  void WebClient_OnUploadStringCompleted_m4079337311 (WebClient_t4210428809 * __this, UploadStringCompletedEventArgs_t4015562024 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::OnUploadValuesCompleted(System.Net.UploadValuesCompletedEventArgs)
extern "C"  void WebClient_OnUploadValuesCompleted_m1076569407 (WebClient_t4210428809 * __this, UploadValuesCompletedEventArgs_t1359222777 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebResponse System.Net.WebClient::GetWebResponse(System.Net.WebRequest,System.IAsyncResult)
extern "C"  WebResponse_t2411292415 * WebClient_GetWebResponse_m3621727860 (WebClient_t4210428809 * __this, WebRequest_t3488810021 * ___request0, Il2CppObject * ___result1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebRequest System.Net.WebClient::GetWebRequest(System.Uri)
extern "C"  WebRequest_t3488810021 * WebClient_GetWebRequest_m2859966311 (WebClient_t4210428809 * __this, Uri_t2776692961 * ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebResponse System.Net.WebClient::GetWebResponse(System.Net.WebRequest)
extern "C"  WebResponse_t2411292415 * WebClient_GetWebResponse_m242843735 (WebClient_t4210428809 * __this, WebRequest_t3488810021 * ___request0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::<DownloadDataAsync>m__C(System.Object)
extern "C"  void WebClient_U3CDownloadDataAsyncU3Em__C_m3979308067 (WebClient_t4210428809 * __this, Il2CppObject * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::<DownloadFileAsync>m__D(System.Object)
extern "C"  void WebClient_U3CDownloadFileAsyncU3Em__D_m3838186608 (WebClient_t4210428809 * __this, Il2CppObject * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::<DownloadStringAsync>m__E(System.Object)
extern "C"  void WebClient_U3CDownloadStringAsyncU3Em__E_m2056899802 (WebClient_t4210428809 * __this, Il2CppObject * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::<OpenReadAsync>m__F(System.Object)
extern "C"  void WebClient_U3COpenReadAsyncU3Em__F_m2253609938 (WebClient_t4210428809 * __this, Il2CppObject * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::<OpenWriteAsync>m__10(System.Object)
extern "C"  void WebClient_U3COpenWriteAsyncU3Em__10_m1217823064 (WebClient_t4210428809 * __this, Il2CppObject * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::<UploadDataAsync>m__11(System.Object)
extern "C"  void WebClient_U3CUploadDataAsyncU3Em__11_m148451097 (WebClient_t4210428809 * __this, Il2CppObject * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::<UploadFileAsync>m__12(System.Object)
extern "C"  void WebClient_U3CUploadFileAsyncU3Em__12_m2499776586 (WebClient_t4210428809 * __this, Il2CppObject * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::<UploadStringAsync>m__13(System.Object)
extern "C"  void WebClient_U3CUploadStringAsyncU3Em__13_m3325196990 (WebClient_t4210428809 * __this, Il2CppObject * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::<UploadValuesAsync>m__14(System.Object)
extern "C"  void WebClient_U3CUploadValuesAsyncU3Em__14_m3610538606 (WebClient_t4210428809 * __this, Il2CppObject * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
