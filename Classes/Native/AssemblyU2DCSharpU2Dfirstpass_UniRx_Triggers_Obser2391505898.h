﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Subject`1<UniRx.Unit>
struct Subject_1_t201353362;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Triggers_Obser1883237895.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableVisibleTrigger
struct  ObservableVisibleTrigger_t2391505898  : public ObservableTriggerBase_t1883237895
{
public:
	// UniRx.Subject`1<UniRx.Unit> UniRx.Triggers.ObservableVisibleTrigger::onBecameInvisible
	Subject_1_t201353362 * ___onBecameInvisible_8;
	// UniRx.Subject`1<UniRx.Unit> UniRx.Triggers.ObservableVisibleTrigger::onBecameVisible
	Subject_1_t201353362 * ___onBecameVisible_9;

public:
	inline static int32_t get_offset_of_onBecameInvisible_8() { return static_cast<int32_t>(offsetof(ObservableVisibleTrigger_t2391505898, ___onBecameInvisible_8)); }
	inline Subject_1_t201353362 * get_onBecameInvisible_8() const { return ___onBecameInvisible_8; }
	inline Subject_1_t201353362 ** get_address_of_onBecameInvisible_8() { return &___onBecameInvisible_8; }
	inline void set_onBecameInvisible_8(Subject_1_t201353362 * value)
	{
		___onBecameInvisible_8 = value;
		Il2CppCodeGenWriteBarrier(&___onBecameInvisible_8, value);
	}

	inline static int32_t get_offset_of_onBecameVisible_9() { return static_cast<int32_t>(offsetof(ObservableVisibleTrigger_t2391505898, ___onBecameVisible_9)); }
	inline Subject_1_t201353362 * get_onBecameVisible_9() const { return ___onBecameVisible_9; }
	inline Subject_1_t201353362 ** get_address_of_onBecameVisible_9() { return &___onBecameVisible_9; }
	inline void set_onBecameVisible_9(Subject_1_t201353362 * value)
	{
		___onBecameVisible_9 = value;
		Il2CppCodeGenWriteBarrier(&___onBecameVisible_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
