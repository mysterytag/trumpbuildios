﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.EmptyObserver`1<UnityEngine.MasterServerEvent>
struct EmptyObserver_1_t1687160480;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_MasterServerEvent2277807490.h"

// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.MasterServerEvent>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m1797483512_gshared (EmptyObserver_1_t1687160480 * __this, const MethodInfo* method);
#define EmptyObserver_1__ctor_m1797483512(__this, method) ((  void (*) (EmptyObserver_1_t1687160480 *, const MethodInfo*))EmptyObserver_1__ctor_m1797483512_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.MasterServerEvent>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m3700285109_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EmptyObserver_1__cctor_m3700285109(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EmptyObserver_1__cctor_m3700285109_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.MasterServerEvent>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m798737794_gshared (EmptyObserver_1_t1687160480 * __this, const MethodInfo* method);
#define EmptyObserver_1_OnCompleted_m798737794(__this, method) ((  void (*) (EmptyObserver_1_t1687160480 *, const MethodInfo*))EmptyObserver_1_OnCompleted_m798737794_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.MasterServerEvent>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m2924495791_gshared (EmptyObserver_1_t1687160480 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define EmptyObserver_1_OnError_m2924495791(__this, ___error0, method) ((  void (*) (EmptyObserver_1_t1687160480 *, Exception_t1967233988 *, const MethodInfo*))EmptyObserver_1_OnError_m2924495791_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.MasterServerEvent>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m3635505312_gshared (EmptyObserver_1_t1687160480 * __this, int32_t ___value0, const MethodInfo* method);
#define EmptyObserver_1_OnNext_m3635505312(__this, ___value0, method) ((  void (*) (EmptyObserver_1_t1687160480 *, int32_t, const MethodInfo*))EmptyObserver_1_OnNext_m3635505312_gshared)(__this, ___value0, method)
