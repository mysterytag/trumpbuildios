﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.WebRequestExtensions/<GetResponseAsObservable>c__AnonStorey35
struct U3CGetResponseAsObservableU3Ec__AnonStorey35_t2663964859;
// System.Net.HttpWebResponse
struct HttpWebResponse_t2667966807;
// System.IAsyncResult
struct IAsyncResult_t537683269;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.WebRequestExtensions/<GetResponseAsObservable>c__AnonStorey35::.ctor()
extern "C"  void U3CGetResponseAsObservableU3Ec__AnonStorey35__ctor_m1648715956 (U3CGetResponseAsObservableU3Ec__AnonStorey35_t2663964859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.HttpWebResponse UniRx.WebRequestExtensions/<GetResponseAsObservable>c__AnonStorey35::<>m__37(System.IAsyncResult)
extern "C"  HttpWebResponse_t2667966807 * U3CGetResponseAsObservableU3Ec__AnonStorey35_U3CU3Em__37_m661311674 (U3CGetResponseAsObservableU3Ec__AnonStorey35_t2663964859 * __this, Il2CppObject * ___ar0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
