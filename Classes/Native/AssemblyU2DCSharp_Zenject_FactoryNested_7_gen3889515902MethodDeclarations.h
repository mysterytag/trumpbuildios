﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.FactoryNested`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct FactoryNested_7_t3889515902;
// Zenject.IFactory`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct IFactory_6_t4091256523;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void Zenject.FactoryNested`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(Zenject.IFactory`6<TParam1,TParam2,TParam3,TParam4,TParam5,TConcrete>)
extern "C"  void FactoryNested_7__ctor_m2694773353_gshared (FactoryNested_7_t3889515902 * __this, Il2CppObject* ___concreteFactory0, const MethodInfo* method);
#define FactoryNested_7__ctor_m2694773353(__this, ___concreteFactory0, method) ((  void (*) (FactoryNested_7_t3889515902 *, Il2CppObject*, const MethodInfo*))FactoryNested_7__ctor_m2694773353_gshared)(__this, ___concreteFactory0, method)
// TContract Zenject.FactoryNested`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Create(TParam1,TParam2,TParam3,TParam4,TParam5)
extern "C"  Il2CppObject * FactoryNested_7_Create_m3784719255_gshared (FactoryNested_7_t3889515902 * __this, Il2CppObject * ___param10, Il2CppObject * ___param21, Il2CppObject * ___param32, Il2CppObject * ___param43, Il2CppObject * ___param54, const MethodInfo* method);
#define FactoryNested_7_Create_m3784719255(__this, ___param10, ___param21, ___param32, ___param43, ___param54, method) ((  Il2CppObject * (*) (FactoryNested_7_t3889515902 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))FactoryNested_7_Create_m3784719255_gshared)(__this, ___param10, ___param21, ___param32, ___param43, ___param54, method)
