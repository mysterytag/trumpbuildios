﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<LinkGoogleAccount>c__AnonStorey79
struct U3CLinkGoogleAccountU3Ec__AnonStorey79_t1425040809;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<LinkGoogleAccount>c__AnonStorey79::.ctor()
extern "C"  void U3CLinkGoogleAccountU3Ec__AnonStorey79__ctor_m3009680330 (U3CLinkGoogleAccountU3Ec__AnonStorey79_t1425040809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<LinkGoogleAccount>c__AnonStorey79::<>m__66(PlayFab.CallRequestContainer)
extern "C"  void U3CLinkGoogleAccountU3Ec__AnonStorey79_U3CU3Em__66_m2876349416 (U3CLinkGoogleAccountU3Ec__AnonStorey79_t1425040809 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
