﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.FactoryNested`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct FactoryNested_6_t1301445835;
// Zenject.IFactory`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct IFactory_5_t363284204;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void Zenject.FactoryNested`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(Zenject.IFactory`5<TParam1,TParam2,TParam3,TParam4,TConcrete>)
extern "C"  void FactoryNested_6__ctor_m2385829163_gshared (FactoryNested_6_t1301445835 * __this, Il2CppObject* ___concreteFactory0, const MethodInfo* method);
#define FactoryNested_6__ctor_m2385829163(__this, ___concreteFactory0, method) ((  void (*) (FactoryNested_6_t1301445835 *, Il2CppObject*, const MethodInfo*))FactoryNested_6__ctor_m2385829163_gshared)(__this, ___concreteFactory0, method)
// TContract Zenject.FactoryNested`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Create(TParam1,TParam2,TParam3,TParam4)
extern "C"  Il2CppObject * FactoryNested_6_Create_m4214630324_gshared (FactoryNested_6_t1301445835 * __this, Il2CppObject * ___param10, Il2CppObject * ___param21, Il2CppObject * ___param32, Il2CppObject * ___param43, const MethodInfo* method);
#define FactoryNested_6_Create_m4214630324(__this, ___param10, ___param21, ___param32, ___param43, method) ((  Il2CppObject * (*) (FactoryNested_6_t1301445835 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))FactoryNested_6_Create_m4214630324_gshared)(__this, ___param10, ___param21, ___param32, ___param43, method)
