﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Cell`1<System.Int32>
struct Cell_1_t3029512419;
// System.Collections.Generic.List`1<Cell`1<System.Single>>
struct List_1_t1937265622;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Transaction/<TestCalculation>c__AnonStorey14C
struct  U3CTestCalculationU3Ec__AnonStorey14C_t340545084  : public Il2CppObject
{
public:
	// Cell`1<System.Int32> Transaction/<TestCalculation>c__AnonStorey14C::count
	Cell_1_t3029512419 * ___count_0;
	// System.Collections.Generic.List`1<Cell`1<System.Single>> Transaction/<TestCalculation>c__AnonStorey14C::cells
	List_1_t1937265622 * ___cells_1;

public:
	inline static int32_t get_offset_of_count_0() { return static_cast<int32_t>(offsetof(U3CTestCalculationU3Ec__AnonStorey14C_t340545084, ___count_0)); }
	inline Cell_1_t3029512419 * get_count_0() const { return ___count_0; }
	inline Cell_1_t3029512419 ** get_address_of_count_0() { return &___count_0; }
	inline void set_count_0(Cell_1_t3029512419 * value)
	{
		___count_0 = value;
		Il2CppCodeGenWriteBarrier(&___count_0, value);
	}

	inline static int32_t get_offset_of_cells_1() { return static_cast<int32_t>(offsetof(U3CTestCalculationU3Ec__AnonStorey14C_t340545084, ___cells_1)); }
	inline List_1_t1937265622 * get_cells_1() const { return ___cells_1; }
	inline List_1_t1937265622 ** get_address_of_cells_1() { return &___cells_1; }
	inline void set_cells_1(List_1_t1937265622 * value)
	{
		___cells_1 = value;
		Il2CppCodeGenWriteBarrier(&___cells_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
