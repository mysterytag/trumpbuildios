﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.BindingConditionSetter/<WhenInjectedInto>c__AnonStorey171/<WhenInjectedInto>c__AnonStorey172
struct U3CWhenInjectedIntoU3Ec__AnonStorey172_t2299533245;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2779229935.h"

// System.Void Zenject.BindingConditionSetter/<WhenInjectedInto>c__AnonStorey171/<WhenInjectedInto>c__AnonStorey172::.ctor()
extern "C"  void U3CWhenInjectedIntoU3Ec__AnonStorey172__ctor_m3943007206 (U3CWhenInjectedIntoU3Ec__AnonStorey172_t2299533245 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.BindingConditionSetter/<WhenInjectedInto>c__AnonStorey171/<WhenInjectedInto>c__AnonStorey172::<>m__292(System.Type)
extern "C"  bool U3CWhenInjectedIntoU3Ec__AnonStorey172_U3CU3Em__292_m3723952871 (U3CWhenInjectedIntoU3Ec__AnonStorey172_t2299533245 * __this, Type_t * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
