﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Rect>
struct ReactivePropertyObserver_t1761411386;
// UniRx.ReactiveProperty`1<UnityEngine.Rect>
struct ReactiveProperty_1_t2133761855;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Rect1525428817.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Rect>::.ctor(UniRx.ReactiveProperty`1<T>)
extern "C"  void ReactivePropertyObserver__ctor_m1486346191_gshared (ReactivePropertyObserver_t1761411386 * __this, ReactiveProperty_1_t2133761855 * ___parent0, const MethodInfo* method);
#define ReactivePropertyObserver__ctor_m1486346191(__this, ___parent0, method) ((  void (*) (ReactivePropertyObserver_t1761411386 *, ReactiveProperty_1_t2133761855 *, const MethodInfo*))ReactivePropertyObserver__ctor_m1486346191_gshared)(__this, ___parent0, method)
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Rect>::OnNext(T)
extern "C"  void ReactivePropertyObserver_OnNext_m739417852_gshared (ReactivePropertyObserver_t1761411386 * __this, Rect_t1525428817  ___value0, const MethodInfo* method);
#define ReactivePropertyObserver_OnNext_m739417852(__this, ___value0, method) ((  void (*) (ReactivePropertyObserver_t1761411386 *, Rect_t1525428817 , const MethodInfo*))ReactivePropertyObserver_OnNext_m739417852_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Rect>::OnError(System.Exception)
extern "C"  void ReactivePropertyObserver_OnError_m4052828683_gshared (ReactivePropertyObserver_t1761411386 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ReactivePropertyObserver_OnError_m4052828683(__this, ___error0, method) ((  void (*) (ReactivePropertyObserver_t1761411386 *, Exception_t1967233988 *, const MethodInfo*))ReactivePropertyObserver_OnError_m4052828683_gshared)(__this, ___error0, method)
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Rect>::OnCompleted()
extern "C"  void ReactivePropertyObserver_OnCompleted_m3900861918_gshared (ReactivePropertyObserver_t1761411386 * __this, const MethodInfo* method);
#define ReactivePropertyObserver_OnCompleted_m3900861918(__this, method) ((  void (*) (ReactivePropertyObserver_t1761411386 *, const MethodInfo*))ReactivePropertyObserver_OnCompleted_m3900861918_gshared)(__this, method)
