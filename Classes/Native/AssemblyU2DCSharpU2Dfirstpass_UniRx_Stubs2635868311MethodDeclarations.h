﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Stubs::.cctor()
extern "C"  void Stubs__cctor_m2920355523 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Stubs::<Nop>m__6F()
extern "C"  void Stubs_U3CNopU3Em__6F_m1274693708 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Stubs::<Throw>m__70(System.Exception)
extern "C"  void Stubs_U3CThrowU3Em__70_m1537148060 (Il2CppObject * __this /* static, unused */, Exception_t1967233988 * ___ex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
