﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.NthZipObserverBase`1<System.Object>
struct NthZipObserverBase_1_t3759589597;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Collections.ICollection[]
struct ICollectionU5BU5D_t1434690852;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.NthZipObserverBase`1<System.Object>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void NthZipObserverBase_1__ctor_m216454881_gshared (NthZipObserverBase_1_t3759589597 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define NthZipObserverBase_1__ctor_m216454881(__this, ___observer0, ___cancel1, method) ((  void (*) (NthZipObserverBase_1_t3759589597 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))NthZipObserverBase_1__ctor_m216454881_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.NthZipObserverBase`1<System.Object>::SetQueue(System.Collections.ICollection[])
extern "C"  void NthZipObserverBase_1_SetQueue_m1622220249_gshared (NthZipObserverBase_1_t3759589597 * __this, ICollectionU5BU5D_t1434690852* ___queues0, const MethodInfo* method);
#define NthZipObserverBase_1_SetQueue_m1622220249(__this, ___queues0, method) ((  void (*) (NthZipObserverBase_1_t3759589597 *, ICollectionU5BU5D_t1434690852*, const MethodInfo*))NthZipObserverBase_1_SetQueue_m1622220249_gshared)(__this, ___queues0, method)
// System.Void UniRx.Operators.NthZipObserverBase`1<System.Object>::Dequeue(System.Int32)
extern "C"  void NthZipObserverBase_1_Dequeue_m89794376_gshared (NthZipObserverBase_1_t3759589597 * __this, int32_t ___index0, const MethodInfo* method);
#define NthZipObserverBase_1_Dequeue_m89794376(__this, ___index0, method) ((  void (*) (NthZipObserverBase_1_t3759589597 *, int32_t, const MethodInfo*))NthZipObserverBase_1_Dequeue_m89794376_gshared)(__this, ___index0, method)
// System.Void UniRx.Operators.NthZipObserverBase`1<System.Object>::Done(System.Int32)
extern "C"  void NthZipObserverBase_1_Done_m2951121230_gshared (NthZipObserverBase_1_t3759589597 * __this, int32_t ___index0, const MethodInfo* method);
#define NthZipObserverBase_1_Done_m2951121230(__this, ___index0, method) ((  void (*) (NthZipObserverBase_1_t3759589597 *, int32_t, const MethodInfo*))NthZipObserverBase_1_Done_m2951121230_gshared)(__this, ___index0, method)
// System.Void UniRx.Operators.NthZipObserverBase`1<System.Object>::Fail(System.Exception)
extern "C"  void NthZipObserverBase_1_Fail_m681288393_gshared (NthZipObserverBase_1_t3759589597 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define NthZipObserverBase_1_Fail_m681288393(__this, ___error0, method) ((  void (*) (NthZipObserverBase_1_t3759589597 *, Exception_t1967233988 *, const MethodInfo*))NthZipObserverBase_1_Fail_m681288393_gshared)(__this, ___error0, method)
