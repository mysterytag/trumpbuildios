﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1634065389MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::.ctor()
#define List_1__ctor_m3265843610(__this, method) ((  void (*) (List_1_t2081125191 *, const MethodInfo*))List_1__ctor_m348833073_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m1725586940(__this, ___collection0, method) ((  void (*) (List_1_t2081125191 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1160795371_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::.ctor(System.Int32)
#define List_1__ctor_m3065036660(__this, ___capacity0, method) ((  void (*) (List_1_t2081125191 *, int32_t, const MethodInfo*))List_1__ctor_m2892763214_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::.cctor()
#define List_1__cctor_m4148279658(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3826137881_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1848804333(__this, method) ((  Il2CppObject* (*) (List_1_t2081125191 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2808422246_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m2397745153(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2081125191 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m4034025648_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m4237382736(__this, method) ((  Il2CppObject * (*) (List_1_t2081125191 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1841330603_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m54005229(__this, ___item0, method) ((  int32_t (*) (List_1_t2081125191 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3794749222_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m1995228019(__this, ___item0, method) ((  bool (*) (List_1_t2081125191 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2659633254_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m448103877(__this, ___item0, method) ((  int32_t (*) (List_1_t2081125191 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3431692926_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m1722587064(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2081125191 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2067529129_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m2314397296(__this, ___item0, method) ((  void (*) (List_1_t2081125191 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1644145887_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1987919796(__this, method) ((  bool (*) (List_1_t2081125191 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1299706087_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m1780480777(__this, method) ((  bool (*) (List_1_t2081125191 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3867536694_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m3331547963(__this, method) ((  Il2CppObject * (*) (List_1_t2081125191 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m4244374434_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m1222427938(__this, method) ((  bool (*) (List_1_t2081125191 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m432946261_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m3125841303(__this, method) ((  bool (*) (List_1_t2081125191 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2961826820_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m2932568578(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t2081125191 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3985478825_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m2457712015(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2081125191 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3234554688_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::Add(T)
#define List_1_Add_m1916746108(__this, ___item0, method) ((  void (*) (List_1_t2081125191 *, DisposableInfo_t1284166222 *, const MethodInfo*))List_1_Add_m642669291_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m2972897719(__this, ___newCount0, method) ((  void (*) (List_1_t2081125191 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m4122600870_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m2882964661(__this, ___collection0, method) ((  void (*) (List_1_t2081125191 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2478449828_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m2143936885(__this, ___enumerable0, method) ((  void (*) (List_1_t2081125191 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1739422052_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m252917762(__this, ___collection0, method) ((  void (*) (List_1_t2081125191 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2229151411_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::AsReadOnly()
#define List_1_AsReadOnly_m611104963(__this, method) ((  ReadOnlyCollection_1_t152344274 * (*) (List_1_t2081125191 *, const MethodInfo*))List_1_AsReadOnly_m769820182_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::Clear()
#define List_1_Clear_m1296278222(__this, method) ((  void (*) (List_1_t2081125191 *, const MethodInfo*))List_1_Clear_m454602559_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::Contains(T)
#define List_1_Contains_m2229621184(__this, ___item0, method) ((  bool (*) (List_1_t2081125191 *, DisposableInfo_t1284166222 *, const MethodInfo*))List_1_Contains_m4186092781_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m1542470252(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2081125191 *, DisposableInfoU5BU5D_t1006847163*, int32_t, const MethodInfo*))List_1_CopyTo_m3988356635_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::Find(System.Predicate`1<T>)
#define List_1_Find_m2917528090(__this, ___match0, method) ((  DisposableInfo_t1284166222 * (*) (List_1_t2081125191 *, Predicate_1_t1855130120 *, const MethodInfo*))List_1_Find_m3379773421_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m854515191(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t1855130120 *, const MethodInfo*))List_1_CheckMatch_m3390394152_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m1067730132(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t2081125191 *, int32_t, int32_t, Predicate_1_t1855130120 *, const MethodInfo*))List_1_GetIndex_m4275988045_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m749289131(__this, ___action0, method) ((  void (*) (List_1_t2081125191 *, Action_1_t1432618927 *, const MethodInfo*))List_1_ForEach_m2030348444_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::GetEnumerator()
#define List_1_GetEnumerator_m423709095(__this, method) ((  Enumerator_t166908183  (*) (List_1_t2081125191 *, const MethodInfo*))List_1_GetEnumerator_m1820263692_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::IndexOf(T)
#define List_1_IndexOf_m2097246008(__this, ___item0, method) ((  int32_t (*) (List_1_t2081125191 *, DisposableInfo_t1284166222 *, const MethodInfo*))List_1_IndexOf_m1752303327_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m3863798979(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t2081125191 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3807054194_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m1288837436(__this, ___index0, method) ((  void (*) (List_1_t2081125191 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3734723819_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::Insert(System.Int32,T)
#define List_1_Insert_m921828323(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2081125191 *, int32_t, DisposableInfo_t1284166222 *, const MethodInfo*))List_1_Insert_m3427163986_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m801801752(__this, ___collection0, method) ((  void (*) (List_1_t2081125191 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2905071175_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::Remove(T)
#define List_1_Remove_m113476027(__this, ___item0, method) ((  bool (*) (List_1_t2081125191 *, DisposableInfo_t1284166222 *, const MethodInfo*))List_1_Remove_m2747911208_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m1336442107(__this, ___match0, method) ((  int32_t (*) (List_1_t2081125191 *, Predicate_1_t1855130120 *, const MethodInfo*))List_1_RemoveAll_m2933443938_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m3090648489(__this, ___index0, method) ((  void (*) (List_1_t2081125191 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1301016856_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::Reverse()
#define List_1_Reverse_m1845542435(__this, method) ((  void (*) (List_1_t2081125191 *, const MethodInfo*))List_1_Reverse_m449081940_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::Sort()
#define List_1_Sort_m2442718303(__this, method) ((  void (*) (List_1_t2081125191 *, const MethodInfo*))List_1_Sort_m1168641486_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m3029635173(__this, ___comparer0, method) ((  void (*) (List_1_t2081125191 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m3726677974_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m3134052210(__this, ___comparison0, method) ((  void (*) (List_1_t2081125191 *, Comparison_1_t3987841098 *, const MethodInfo*))List_1_Sort_m4192185249_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::ToArray()
#define List_1_ToArray_m243939516(__this, method) ((  DisposableInfoU5BU5D_t1006847163* (*) (List_1_t2081125191 *, const MethodInfo*))List_1_ToArray_m1046025517_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::TrimExcess()
#define List_1_TrimExcess_m3352758456(__this, method) ((  void (*) (List_1_t2081125191 *, const MethodInfo*))List_1_TrimExcess_m2451380967_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::get_Capacity()
#define List_1_get_Capacity_m333908840(__this, method) ((  int32_t (*) (List_1_t2081125191 *, const MethodInfo*))List_1_get_Capacity_m543520655_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m183086537(__this, ___value0, method) ((  void (*) (List_1_t2081125191 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1332789688_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::get_Count()
#define List_1_get_Count_m407424323(__this, method) ((  int32_t (*) (List_1_t2081125191 *, const MethodInfo*))List_1_get_Count_m2599103100_gshared)(__this, method)
// T System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::get_Item(System.Int32)
#define List_1_get_Item_m2555912271(__this, ___index0, method) ((  DisposableInfo_t1284166222 * (*) (List_1_t2081125191 *, int32_t, const MethodInfo*))List_1_get_Item_m2771401372_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Zenject.DisposableManager/DisposableInfo>::set_Item(System.Int32,T)
#define List_1_set_Item_m2923352058(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2081125191 *, int32_t, DisposableInfo_t1284166222 *, const MethodInfo*))List_1_set_Item_m1074271145_gshared)(__this, ___index0, ___value1, method)
