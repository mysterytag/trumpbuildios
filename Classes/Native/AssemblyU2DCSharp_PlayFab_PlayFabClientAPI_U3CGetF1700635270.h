﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PlayFab.PlayFabClientAPI/ProcessApiCallback`1<PlayFab.ClientModels.GetFriendLeaderboardAroundCurrentUserResult>
struct ProcessApiCallback_1_t3247531673;
// PlayFab.ErrorCallback
struct ErrorCallback_t582108558;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.PlayFabClientAPI/<GetFriendLeaderboardAroundCurrentUser>c__AnonStorey8C
struct  U3CGetFriendLeaderboardAroundCurrentUserU3Ec__AnonStorey8C_t1700635270  : public Il2CppObject
{
public:
	// PlayFab.PlayFabClientAPI/ProcessApiCallback`1<PlayFab.ClientModels.GetFriendLeaderboardAroundCurrentUserResult> PlayFab.PlayFabClientAPI/<GetFriendLeaderboardAroundCurrentUser>c__AnonStorey8C::resultCallback
	ProcessApiCallback_1_t3247531673 * ___resultCallback_0;
	// PlayFab.ErrorCallback PlayFab.PlayFabClientAPI/<GetFriendLeaderboardAroundCurrentUser>c__AnonStorey8C::errorCallback
	ErrorCallback_t582108558 * ___errorCallback_1;

public:
	inline static int32_t get_offset_of_resultCallback_0() { return static_cast<int32_t>(offsetof(U3CGetFriendLeaderboardAroundCurrentUserU3Ec__AnonStorey8C_t1700635270, ___resultCallback_0)); }
	inline ProcessApiCallback_1_t3247531673 * get_resultCallback_0() const { return ___resultCallback_0; }
	inline ProcessApiCallback_1_t3247531673 ** get_address_of_resultCallback_0() { return &___resultCallback_0; }
	inline void set_resultCallback_0(ProcessApiCallback_1_t3247531673 * value)
	{
		___resultCallback_0 = value;
		Il2CppCodeGenWriteBarrier(&___resultCallback_0, value);
	}

	inline static int32_t get_offset_of_errorCallback_1() { return static_cast<int32_t>(offsetof(U3CGetFriendLeaderboardAroundCurrentUserU3Ec__AnonStorey8C_t1700635270, ___errorCallback_1)); }
	inline ErrorCallback_t582108558 * get_errorCallback_1() const { return ___errorCallback_1; }
	inline ErrorCallback_t582108558 ** get_address_of_errorCallback_1() { return &___errorCallback_1; }
	inline void set_errorCallback_1(ErrorCallback_t582108558 * value)
	{
		___errorCallback_1 = value;
		Il2CppCodeGenWriteBarrier(&___errorCallback_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
