﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass35_0
struct U3CU3Ec__DisplayClass35_0_t2750137162;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"

// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass35_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass35_0__ctor_m214926348 (U3CU3Ec__DisplayClass35_0_t2750137162 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 DG.Tweening.ShortcutExtensions/<>c__DisplayClass35_0::<DOResize>b__0()
extern "C"  Vector2_t3525329788  U3CU3Ec__DisplayClass35_0_U3CDOResizeU3Eb__0_m11030308 (U3CU3Ec__DisplayClass35_0_t2750137162 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass35_0::<DOResize>b__1(UnityEngine.Vector2)
extern "C"  void U3CU3Ec__DisplayClass35_0_U3CDOResizeU3Eb__1_m544211888 (U3CU3Ec__DisplayClass35_0_t2750137162 * __this, Vector2_t3525329788  ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
