﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SimpleJSON.JSONNode
struct JSONNode_t580622632;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSONClass/<Remove>c__AnonStorey2E
struct  U3CRemoveU3Ec__AnonStorey2E_t1556685308  : public Il2CppObject
{
public:
	// SimpleJSON.JSONNode SimpleJSON.JSONClass/<Remove>c__AnonStorey2E::aNode
	JSONNode_t580622632 * ___aNode_0;

public:
	inline static int32_t get_offset_of_aNode_0() { return static_cast<int32_t>(offsetof(U3CRemoveU3Ec__AnonStorey2E_t1556685308, ___aNode_0)); }
	inline JSONNode_t580622632 * get_aNode_0() const { return ___aNode_0; }
	inline JSONNode_t580622632 ** get_address_of_aNode_0() { return &___aNode_0; }
	inline void set_aNode_0(JSONNode_t580622632 * value)
	{
		___aNode_0 = value;
		Il2CppCodeGenWriteBarrier(&___aNode_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
