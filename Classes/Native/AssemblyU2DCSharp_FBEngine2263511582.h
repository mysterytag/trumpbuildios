﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGroupJoinResult>
struct FacebookDelegate_1_t1900386660;
// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult>
struct FacebookDelegate_1_t2757548155;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FBEngine
struct  FBEngine_t2263511582  : public MonoBehaviour_t3012272455
{
public:
	// System.Boolean FBEngine::FBLogIn
	bool ___FBLogIn_2;

public:
	inline static int32_t get_offset_of_FBLogIn_2() { return static_cast<int32_t>(offsetof(FBEngine_t2263511582, ___FBLogIn_2)); }
	inline bool get_FBLogIn_2() const { return ___FBLogIn_2; }
	inline bool* get_address_of_FBLogIn_2() { return &___FBLogIn_2; }
	inline void set_FBLogIn_2(bool value)
	{
		___FBLogIn_2 = value;
	}
};

struct FBEngine_t2263511582_StaticFields
{
public:
	// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGroupJoinResult> FBEngine::<>f__am$cache1
	FacebookDelegate_1_t1900386660 * ___U3CU3Ef__amU24cache1_3;
	// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult> FBEngine::<>f__am$cache2
	FacebookDelegate_1_t2757548155 * ___U3CU3Ef__amU24cache2_4;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_3() { return static_cast<int32_t>(offsetof(FBEngine_t2263511582_StaticFields, ___U3CU3Ef__amU24cache1_3)); }
	inline FacebookDelegate_1_t1900386660 * get_U3CU3Ef__amU24cache1_3() const { return ___U3CU3Ef__amU24cache1_3; }
	inline FacebookDelegate_1_t1900386660 ** get_address_of_U3CU3Ef__amU24cache1_3() { return &___U3CU3Ef__amU24cache1_3; }
	inline void set_U3CU3Ef__amU24cache1_3(FacebookDelegate_1_t1900386660 * value)
	{
		___U3CU3Ef__amU24cache1_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_4() { return static_cast<int32_t>(offsetof(FBEngine_t2263511582_StaticFields, ___U3CU3Ef__amU24cache2_4)); }
	inline FacebookDelegate_1_t2757548155 * get_U3CU3Ef__amU24cache2_4() const { return ___U3CU3Ef__amU24cache2_4; }
	inline FacebookDelegate_1_t2757548155 ** get_address_of_U3CU3Ef__amU24cache2_4() { return &___U3CU3Ef__amU24cache2_4; }
	inline void set_U3CU3Ef__amU24cache2_4(FacebookDelegate_1_t2757548155 * value)
	{
		___U3CU3Ef__amU24cache2_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
