﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.LastObservable`1/Last_<System.Object>
struct Last__t3329173242;
// UniRx.Operators.LastObservable`1<System.Object>
struct LastObservable_1_t3225858136;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.LastObservable`1/Last_<System.Object>::.ctor(UniRx.Operators.LastObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Last___ctor_m1155955451_gshared (Last__t3329173242 * __this, LastObservable_1_t3225858136 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Last___ctor_m1155955451(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Last__t3329173242 *, LastObservable_1_t3225858136 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Last___ctor_m1155955451_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.LastObservable`1/Last_<System.Object>::OnNext(T)
extern "C"  void Last__OnNext_m2733218846_gshared (Last__t3329173242 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Last__OnNext_m2733218846(__this, ___value0, method) ((  void (*) (Last__t3329173242 *, Il2CppObject *, const MethodInfo*))Last__OnNext_m2733218846_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.LastObservable`1/Last_<System.Object>::OnError(System.Exception)
extern "C"  void Last__OnError_m2472393517_gshared (Last__t3329173242 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Last__OnError_m2472393517(__this, ___error0, method) ((  void (*) (Last__t3329173242 *, Exception_t1967233988 *, const MethodInfo*))Last__OnError_m2472393517_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.LastObservable`1/Last_<System.Object>::OnCompleted()
extern "C"  void Last__OnCompleted_m4084337152_gshared (Last__t3329173242 * __this, const MethodInfo* method);
#define Last__OnCompleted_m4084337152(__this, method) ((  void (*) (Last__t3329173242 *, const MethodInfo*))Last__OnCompleted_m4084337152_gshared)(__this, method)
