﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkConnectionError>
struct ListObserver_1_t1318423664;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.NetworkConnectionError>>
struct ImmutableList_1_t1042099;
// System.Exception
struct Exception_t1967233988;
// UniRx.IObserver`1<UnityEngine.NetworkConnectionError>
struct IObserver_1_t3235804896;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_NetworkConnectionError1023805993.h"

// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkConnectionError>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m1984644638_gshared (ListObserver_1_t1318423664 * __this, ImmutableList_1_t1042099 * ___observers0, const MethodInfo* method);
#define ListObserver_1__ctor_m1984644638(__this, ___observers0, method) ((  void (*) (ListObserver_1_t1318423664 *, ImmutableList_1_t1042099 *, const MethodInfo*))ListObserver_1__ctor_m1984644638_gshared)(__this, ___observers0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkConnectionError>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m1036309462_gshared (ListObserver_1_t1318423664 * __this, const MethodInfo* method);
#define ListObserver_1_OnCompleted_m1036309462(__this, method) ((  void (*) (ListObserver_1_t1318423664 *, const MethodInfo*))ListObserver_1_OnCompleted_m1036309462_gshared)(__this, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkConnectionError>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m877818883_gshared (ListObserver_1_t1318423664 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ListObserver_1_OnError_m877818883(__this, ___error0, method) ((  void (*) (ListObserver_1_t1318423664 *, Exception_t1967233988 *, const MethodInfo*))ListObserver_1_OnError_m877818883_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkConnectionError>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m212254964_gshared (ListObserver_1_t1318423664 * __this, int32_t ___value0, const MethodInfo* method);
#define ListObserver_1_OnNext_m212254964(__this, ___value0, method) ((  void (*) (ListObserver_1_t1318423664 *, int32_t, const MethodInfo*))ListObserver_1_OnNext_m212254964_gshared)(__this, ___value0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkConnectionError>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m1921289876_gshared (ListObserver_1_t1318423664 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Add_m1921289876(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t1318423664 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Add_m1921289876_gshared)(__this, ___observer0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UnityEngine.NetworkConnectionError>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m2845955127_gshared (ListObserver_1_t1318423664 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Remove_m2845955127(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t1318423664 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Remove_m2845955127_gshared)(__this, ___observer0, method)
