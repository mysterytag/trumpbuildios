﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ThrottleObservable`1<System.Int64>
struct ThrottleObservable_1_t3227609402;
// UniRx.IObservable`1<System.Int64>
struct IObservable_1_t2606213246;
// UniRx.IScheduler
struct IScheduler_t2938318244;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Int64>
struct IObserver_1_t764446489;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_TimeSpan763862892.h"

// System.Void UniRx.Operators.ThrottleObservable`1<System.Int64>::.ctor(UniRx.IObservable`1<T>,System.TimeSpan,UniRx.IScheduler)
extern "C"  void ThrottleObservable_1__ctor_m2092983233_gshared (ThrottleObservable_1_t3227609402 * __this, Il2CppObject* ___source0, TimeSpan_t763862892  ___dueTime1, Il2CppObject * ___scheduler2, const MethodInfo* method);
#define ThrottleObservable_1__ctor_m2092983233(__this, ___source0, ___dueTime1, ___scheduler2, method) ((  void (*) (ThrottleObservable_1_t3227609402 *, Il2CppObject*, TimeSpan_t763862892 , Il2CppObject *, const MethodInfo*))ThrottleObservable_1__ctor_m2092983233_gshared)(__this, ___source0, ___dueTime1, ___scheduler2, method)
// System.IDisposable UniRx.Operators.ThrottleObservable`1<System.Int64>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * ThrottleObservable_1_SubscribeCore_m2387645976_gshared (ThrottleObservable_1_t3227609402 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define ThrottleObservable_1_SubscribeCore_m2387645976(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (ThrottleObservable_1_t3227609402 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))ThrottleObservable_1_SubscribeCore_m2387645976_gshared)(__this, ___observer0, ___cancel1, method)
