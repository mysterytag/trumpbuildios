﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.CombineLatestObservable`3<System.Boolean,System.Boolean,System.Boolean>
struct CombineLatestObservable_3_t3912295671;
// UniRx.IObservable`1<System.Boolean>
struct IObservable_1_t4264771001;
// System.Func`3<System.Boolean,System.Boolean,System.Boolean>
struct Func_3_t3063550794;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Boolean>
struct IObserver_1_t2423004244;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.CombineLatestObservable`3<System.Boolean,System.Boolean,System.Boolean>::.ctor(UniRx.IObservable`1<TLeft>,UniRx.IObservable`1<TRight>,System.Func`3<TLeft,TRight,TResult>)
extern "C"  void CombineLatestObservable_3__ctor_m1235902814_gshared (CombineLatestObservable_3_t3912295671 * __this, Il2CppObject* ___left0, Il2CppObject* ___right1, Func_3_t3063550794 * ___selector2, const MethodInfo* method);
#define CombineLatestObservable_3__ctor_m1235902814(__this, ___left0, ___right1, ___selector2, method) ((  void (*) (CombineLatestObservable_3_t3912295671 *, Il2CppObject*, Il2CppObject*, Func_3_t3063550794 *, const MethodInfo*))CombineLatestObservable_3__ctor_m1235902814_gshared)(__this, ___left0, ___right1, ___selector2, method)
// System.IDisposable UniRx.Operators.CombineLatestObservable`3<System.Boolean,System.Boolean,System.Boolean>::SubscribeCore(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  Il2CppObject * CombineLatestObservable_3_SubscribeCore_m3527518386_gshared (CombineLatestObservable_3_t3912295671 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define CombineLatestObservable_3_SubscribeCore_m3527518386(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (CombineLatestObservable_3_t3912295671 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))CombineLatestObservable_3_SubscribeCore_m3527518386_gshared)(__this, ___observer0, ___cancel1, method)
