﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Diagnostics.LogEntry
struct LogEntry_t1891155722;
// System.String
struct String_t;
// UnityEngine.Object
struct Object_t3878351788;
struct Object_t3878351788_marshaled_pinvoke;
// System.Exception
struct Exception_t1967233988;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "UnityEngine_UnityEngine_LogType3529269451.h"
#include "mscorlib_System_DateTime339033936.h"
#include "UnityEngine_UnityEngine_Object3878351788.h"
#include "mscorlib_System_Exception1967233988.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.Diagnostics.LogEntry::.ctor(System.String,UnityEngine.LogType,System.DateTime,System.String,UnityEngine.Object,System.Exception,System.String,System.Object)
extern "C"  void LogEntry__ctor_m3810061886 (LogEntry_t1891155722 * __this, String_t* ___loggerName0, int32_t ___logType1, DateTime_t339033936  ___timestamp2, String_t* ___message3, Object_t3878351788 * ___context4, Exception_t1967233988 * ___exception5, String_t* ___stackTrace6, Il2CppObject * ___state7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UniRx.Diagnostics.LogEntry::get_LoggerName()
extern "C"  String_t* LogEntry_get_LoggerName_m969967108 (LogEntry_t1891155722 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Diagnostics.LogEntry::set_LoggerName(System.String)
extern "C"  void LogEntry_set_LoggerName_m1213850445 (LogEntry_t1891155722 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.LogType UniRx.Diagnostics.LogEntry::get_LogType()
extern "C"  int32_t LogEntry_get_LogType_m344512238 (LogEntry_t1891155722 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Diagnostics.LogEntry::set_LogType(UnityEngine.LogType)
extern "C"  void LogEntry_set_LogType_m3919054117 (LogEntry_t1891155722 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UniRx.Diagnostics.LogEntry::get_Message()
extern "C"  String_t* LogEntry_get_Message_m3138159808 (LogEntry_t1891155722 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Diagnostics.LogEntry::set_Message(System.String)
extern "C"  void LogEntry_set_Message_m3538884819 (LogEntry_t1891155722 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime UniRx.Diagnostics.LogEntry::get_Timestamp()
extern "C"  DateTime_t339033936  LogEntry_get_Timestamp_m708156057 (LogEntry_t1891155722 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Diagnostics.LogEntry::set_Timestamp(System.DateTime)
extern "C"  void LogEntry_set_Timestamp_m3000247386 (LogEntry_t1891155722 * __this, DateTime_t339033936  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UniRx.Diagnostics.LogEntry::get_Context()
extern "C"  Object_t3878351788 * LogEntry_get_Context_m364543516 (LogEntry_t1891155722 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Diagnostics.LogEntry::set_Context(UnityEngine.Object)
extern "C"  void LogEntry_set_Context_m12151361 (LogEntry_t1891155722 * __this, Object_t3878351788 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception UniRx.Diagnostics.LogEntry::get_Exception()
extern "C"  Exception_t1967233988 * LogEntry_get_Exception_m1674930326 (LogEntry_t1891155722 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Diagnostics.LogEntry::set_Exception(System.Exception)
extern "C"  void LogEntry_set_Exception_m2294780359 (LogEntry_t1891155722 * __this, Exception_t1967233988 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UniRx.Diagnostics.LogEntry::get_StackTrace()
extern "C"  String_t* LogEntry_get_StackTrace_m3339660518 (LogEntry_t1891155722 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Diagnostics.LogEntry::set_StackTrace(System.String)
extern "C"  void LogEntry_set_StackTrace_m3865377707 (LogEntry_t1891155722 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UniRx.Diagnostics.LogEntry::get_State()
extern "C"  Il2CppObject * LogEntry_get_State_m3935062520 (LogEntry_t1891155722 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Diagnostics.LogEntry::set_State(System.Object)
extern "C"  void LogEntry_set_State_m683787227 (LogEntry_t1891155722 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UniRx.Diagnostics.LogEntry::ToString()
extern "C"  String_t* LogEntry_ToString_m1215367404 (LogEntry_t1891155722 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
