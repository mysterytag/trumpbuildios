﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.String>
struct Action_1_t1116941607;
// System.Action`1<System.Exception>
struct Action_1_t2115686693;
// System.Func`2<System.String,UniRx.IObservable`1<System.String>>
struct Func_2_t676343372;
// System.Func`3<System.String,System.String,<>__AnonType0`2<System.String,System.String>>
struct Func_3_t3610964670;
// System.Action`1<<>__AnonType0`2<System.String,System.String>>
struct Action_1_t1519452702;
// System.Action`1<System.String[]>
struct Action_1_t3105322948;
// System.Action`1<System.Single>
struct Action_1_t1106661726;
// System.Action`1<UniRx.WWWErrorException>
struct Action_1_t3907447057;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Examples.Sample01_ObservableWWW
struct  Sample01_ObservableWWW_t1646932859  : public MonoBehaviour_t3012272455
{
public:

public:
};

struct Sample01_ObservableWWW_t1646932859_StaticFields
{
public:
	// System.Action`1<System.String> UniRx.Examples.Sample01_ObservableWWW::<>f__am$cache0
	Action_1_t1116941607 * ___U3CU3Ef__amU24cache0_2;
	// System.Action`1<System.Exception> UniRx.Examples.Sample01_ObservableWWW::<>f__am$cache1
	Action_1_t2115686693 * ___U3CU3Ef__amU24cache1_3;
	// System.Func`2<System.String,UniRx.IObservable`1<System.String>> UniRx.Examples.Sample01_ObservableWWW::<>f__am$cache2
	Func_2_t676343372 * ___U3CU3Ef__amU24cache2_4;
	// System.Func`3<System.String,System.String,<>__AnonType0`2<System.String,System.String>> UniRx.Examples.Sample01_ObservableWWW::<>f__am$cache3
	Func_3_t3610964670 * ___U3CU3Ef__amU24cache3_5;
	// System.Action`1<<>__AnonType0`2<System.String,System.String>> UniRx.Examples.Sample01_ObservableWWW::<>f__am$cache4
	Action_1_t1519452702 * ___U3CU3Ef__amU24cache4_6;
	// System.Action`1<System.String[]> UniRx.Examples.Sample01_ObservableWWW::<>f__am$cache5
	Action_1_t3105322948 * ___U3CU3Ef__amU24cache5_7;
	// System.Action`1<System.Single> UniRx.Examples.Sample01_ObservableWWW::<>f__am$cache6
	Action_1_t1106661726 * ___U3CU3Ef__amU24cache6_8;
	// System.Action`1<UniRx.WWWErrorException> UniRx.Examples.Sample01_ObservableWWW::<>f__am$cache7
	Action_1_t3907447057 * ___U3CU3Ef__amU24cache7_9;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_2() { return static_cast<int32_t>(offsetof(Sample01_ObservableWWW_t1646932859_StaticFields, ___U3CU3Ef__amU24cache0_2)); }
	inline Action_1_t1116941607 * get_U3CU3Ef__amU24cache0_2() const { return ___U3CU3Ef__amU24cache0_2; }
	inline Action_1_t1116941607 ** get_address_of_U3CU3Ef__amU24cache0_2() { return &___U3CU3Ef__amU24cache0_2; }
	inline void set_U3CU3Ef__amU24cache0_2(Action_1_t1116941607 * value)
	{
		___U3CU3Ef__amU24cache0_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_3() { return static_cast<int32_t>(offsetof(Sample01_ObservableWWW_t1646932859_StaticFields, ___U3CU3Ef__amU24cache1_3)); }
	inline Action_1_t2115686693 * get_U3CU3Ef__amU24cache1_3() const { return ___U3CU3Ef__amU24cache1_3; }
	inline Action_1_t2115686693 ** get_address_of_U3CU3Ef__amU24cache1_3() { return &___U3CU3Ef__amU24cache1_3; }
	inline void set_U3CU3Ef__amU24cache1_3(Action_1_t2115686693 * value)
	{
		___U3CU3Ef__amU24cache1_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_4() { return static_cast<int32_t>(offsetof(Sample01_ObservableWWW_t1646932859_StaticFields, ___U3CU3Ef__amU24cache2_4)); }
	inline Func_2_t676343372 * get_U3CU3Ef__amU24cache2_4() const { return ___U3CU3Ef__amU24cache2_4; }
	inline Func_2_t676343372 ** get_address_of_U3CU3Ef__amU24cache2_4() { return &___U3CU3Ef__amU24cache2_4; }
	inline void set_U3CU3Ef__amU24cache2_4(Func_2_t676343372 * value)
	{
		___U3CU3Ef__amU24cache2_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_5() { return static_cast<int32_t>(offsetof(Sample01_ObservableWWW_t1646932859_StaticFields, ___U3CU3Ef__amU24cache3_5)); }
	inline Func_3_t3610964670 * get_U3CU3Ef__amU24cache3_5() const { return ___U3CU3Ef__amU24cache3_5; }
	inline Func_3_t3610964670 ** get_address_of_U3CU3Ef__amU24cache3_5() { return &___U3CU3Ef__amU24cache3_5; }
	inline void set_U3CU3Ef__amU24cache3_5(Func_3_t3610964670 * value)
	{
		___U3CU3Ef__amU24cache3_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_6() { return static_cast<int32_t>(offsetof(Sample01_ObservableWWW_t1646932859_StaticFields, ___U3CU3Ef__amU24cache4_6)); }
	inline Action_1_t1519452702 * get_U3CU3Ef__amU24cache4_6() const { return ___U3CU3Ef__amU24cache4_6; }
	inline Action_1_t1519452702 ** get_address_of_U3CU3Ef__amU24cache4_6() { return &___U3CU3Ef__amU24cache4_6; }
	inline void set_U3CU3Ef__amU24cache4_6(Action_1_t1519452702 * value)
	{
		___U3CU3Ef__amU24cache4_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_7() { return static_cast<int32_t>(offsetof(Sample01_ObservableWWW_t1646932859_StaticFields, ___U3CU3Ef__amU24cache5_7)); }
	inline Action_1_t3105322948 * get_U3CU3Ef__amU24cache5_7() const { return ___U3CU3Ef__amU24cache5_7; }
	inline Action_1_t3105322948 ** get_address_of_U3CU3Ef__amU24cache5_7() { return &___U3CU3Ef__amU24cache5_7; }
	inline void set_U3CU3Ef__amU24cache5_7(Action_1_t3105322948 * value)
	{
		___U3CU3Ef__amU24cache5_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_8() { return static_cast<int32_t>(offsetof(Sample01_ObservableWWW_t1646932859_StaticFields, ___U3CU3Ef__amU24cache6_8)); }
	inline Action_1_t1106661726 * get_U3CU3Ef__amU24cache6_8() const { return ___U3CU3Ef__amU24cache6_8; }
	inline Action_1_t1106661726 ** get_address_of_U3CU3Ef__amU24cache6_8() { return &___U3CU3Ef__amU24cache6_8; }
	inline void set_U3CU3Ef__amU24cache6_8(Action_1_t1106661726 * value)
	{
		___U3CU3Ef__amU24cache6_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_9() { return static_cast<int32_t>(offsetof(Sample01_ObservableWWW_t1646932859_StaticFields, ___U3CU3Ef__amU24cache7_9)); }
	inline Action_1_t3907447057 * get_U3CU3Ef__amU24cache7_9() const { return ___U3CU3Ef__amU24cache7_9; }
	inline Action_1_t3907447057 ** get_address_of_U3CU3Ef__amU24cache7_9() { return &___U3CU3Ef__amU24cache7_9; }
	inline void set_U3CU3Ef__amU24cache7_9(Action_1_t3907447057 * value)
	{
		___U3CU3Ef__amU24cache7_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
