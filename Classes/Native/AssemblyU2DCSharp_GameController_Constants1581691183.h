﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object837106420.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameController/Constants
struct  Constants_t1581691183  : public Il2CppObject
{
public:

public:
};

struct Constants_t1581691183_StaticFields
{
public:
	// UnityEngine.Vector3 GameController/Constants::defaultBillPosition
	Vector3_t3525329789  ___defaultBillPosition_0;
	// UnityEngine.Vector3 GameController/Constants::finalBillPosition
	Vector3_t3525329789  ___finalBillPosition_1;
	// System.Single GameController/Constants::moneyBillDistance
	float ___moneyBillDistance_2;
	// System.Single GameController/Constants::makecomboSwipeTime
	float ___makecomboSwipeTime_3;
	// System.Single GameController/Constants::staycomboSwipeTime
	float ___staycomboSwipeTime_4;
	// System.Int32 GameController/Constants::multipliedClick
	int32_t ___multipliedClick_5;
	// System.Single GameController/Constants::timedUpdateInterval
	float ___timedUpdateInterval_6;
	// System.Single GameController/Constants::passiveIncomeInterval
	float ___passiveIncomeInterval_7;
	// System.Single GameController/Constants::achieveUpdateInterval
	float ___achieveUpdateInterval_8;
	// UnityEngine.Vector3 GameController/Constants::cityPosition
	Vector3_t3525329789  ___cityPosition_9;

public:
	inline static int32_t get_offset_of_defaultBillPosition_0() { return static_cast<int32_t>(offsetof(Constants_t1581691183_StaticFields, ___defaultBillPosition_0)); }
	inline Vector3_t3525329789  get_defaultBillPosition_0() const { return ___defaultBillPosition_0; }
	inline Vector3_t3525329789 * get_address_of_defaultBillPosition_0() { return &___defaultBillPosition_0; }
	inline void set_defaultBillPosition_0(Vector3_t3525329789  value)
	{
		___defaultBillPosition_0 = value;
	}

	inline static int32_t get_offset_of_finalBillPosition_1() { return static_cast<int32_t>(offsetof(Constants_t1581691183_StaticFields, ___finalBillPosition_1)); }
	inline Vector3_t3525329789  get_finalBillPosition_1() const { return ___finalBillPosition_1; }
	inline Vector3_t3525329789 * get_address_of_finalBillPosition_1() { return &___finalBillPosition_1; }
	inline void set_finalBillPosition_1(Vector3_t3525329789  value)
	{
		___finalBillPosition_1 = value;
	}

	inline static int32_t get_offset_of_moneyBillDistance_2() { return static_cast<int32_t>(offsetof(Constants_t1581691183_StaticFields, ___moneyBillDistance_2)); }
	inline float get_moneyBillDistance_2() const { return ___moneyBillDistance_2; }
	inline float* get_address_of_moneyBillDistance_2() { return &___moneyBillDistance_2; }
	inline void set_moneyBillDistance_2(float value)
	{
		___moneyBillDistance_2 = value;
	}

	inline static int32_t get_offset_of_makecomboSwipeTime_3() { return static_cast<int32_t>(offsetof(Constants_t1581691183_StaticFields, ___makecomboSwipeTime_3)); }
	inline float get_makecomboSwipeTime_3() const { return ___makecomboSwipeTime_3; }
	inline float* get_address_of_makecomboSwipeTime_3() { return &___makecomboSwipeTime_3; }
	inline void set_makecomboSwipeTime_3(float value)
	{
		___makecomboSwipeTime_3 = value;
	}

	inline static int32_t get_offset_of_staycomboSwipeTime_4() { return static_cast<int32_t>(offsetof(Constants_t1581691183_StaticFields, ___staycomboSwipeTime_4)); }
	inline float get_staycomboSwipeTime_4() const { return ___staycomboSwipeTime_4; }
	inline float* get_address_of_staycomboSwipeTime_4() { return &___staycomboSwipeTime_4; }
	inline void set_staycomboSwipeTime_4(float value)
	{
		___staycomboSwipeTime_4 = value;
	}

	inline static int32_t get_offset_of_multipliedClick_5() { return static_cast<int32_t>(offsetof(Constants_t1581691183_StaticFields, ___multipliedClick_5)); }
	inline int32_t get_multipliedClick_5() const { return ___multipliedClick_5; }
	inline int32_t* get_address_of_multipliedClick_5() { return &___multipliedClick_5; }
	inline void set_multipliedClick_5(int32_t value)
	{
		___multipliedClick_5 = value;
	}

	inline static int32_t get_offset_of_timedUpdateInterval_6() { return static_cast<int32_t>(offsetof(Constants_t1581691183_StaticFields, ___timedUpdateInterval_6)); }
	inline float get_timedUpdateInterval_6() const { return ___timedUpdateInterval_6; }
	inline float* get_address_of_timedUpdateInterval_6() { return &___timedUpdateInterval_6; }
	inline void set_timedUpdateInterval_6(float value)
	{
		___timedUpdateInterval_6 = value;
	}

	inline static int32_t get_offset_of_passiveIncomeInterval_7() { return static_cast<int32_t>(offsetof(Constants_t1581691183_StaticFields, ___passiveIncomeInterval_7)); }
	inline float get_passiveIncomeInterval_7() const { return ___passiveIncomeInterval_7; }
	inline float* get_address_of_passiveIncomeInterval_7() { return &___passiveIncomeInterval_7; }
	inline void set_passiveIncomeInterval_7(float value)
	{
		___passiveIncomeInterval_7 = value;
	}

	inline static int32_t get_offset_of_achieveUpdateInterval_8() { return static_cast<int32_t>(offsetof(Constants_t1581691183_StaticFields, ___achieveUpdateInterval_8)); }
	inline float get_achieveUpdateInterval_8() const { return ___achieveUpdateInterval_8; }
	inline float* get_address_of_achieveUpdateInterval_8() { return &___achieveUpdateInterval_8; }
	inline void set_achieveUpdateInterval_8(float value)
	{
		___achieveUpdateInterval_8 = value;
	}

	inline static int32_t get_offset_of_cityPosition_9() { return static_cast<int32_t>(offsetof(Constants_t1581691183_StaticFields, ___cityPosition_9)); }
	inline Vector3_t3525329789  get_cityPosition_9() const { return ___cityPosition_9; }
	inline Vector3_t3525329789 * get_address_of_cityPosition_9() { return &___cityPosition_9; }
	inline void set_cityPosition_9(Vector3_t3525329789  value)
	{
		___cityPosition_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
