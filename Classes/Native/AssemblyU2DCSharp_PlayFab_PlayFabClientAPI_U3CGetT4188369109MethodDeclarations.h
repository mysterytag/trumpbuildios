﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<GetTradeStatus>c__AnonStoreyD4
struct U3CGetTradeStatusU3Ec__AnonStoreyD4_t4188369109;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<GetTradeStatus>c__AnonStoreyD4::.ctor()
extern "C"  void U3CGetTradeStatusU3Ec__AnonStoreyD4__ctor_m2887845502 (U3CGetTradeStatusU3Ec__AnonStoreyD4_t4188369109 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<GetTradeStatus>c__AnonStoreyD4::<>m__C1(PlayFab.CallRequestContainer)
extern "C"  void U3CGetTradeStatusU3Ec__AnonStoreyD4_U3CU3Em__C1_m4128190634 (U3CGetTradeStatusU3Ec__AnonStoreyD4_t4188369109 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
