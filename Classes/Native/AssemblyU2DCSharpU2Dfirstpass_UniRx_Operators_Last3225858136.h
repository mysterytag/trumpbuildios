﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t1509682273;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4196218687.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.LastObservable`1<System.Object>
struct  LastObservable_1_t3225858136  : public OperatorObservableBase_1_t4196218687
{
public:
	// UniRx.IObservable`1<T> UniRx.Operators.LastObservable`1::source
	Il2CppObject* ___source_1;
	// System.Boolean UniRx.Operators.LastObservable`1::useDefault
	bool ___useDefault_2;
	// System.Func`2<T,System.Boolean> UniRx.Operators.LastObservable`1::predicate
	Func_2_t1509682273 * ___predicate_3;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(LastObservable_1_t3225858136, ___source_1)); }
	inline Il2CppObject* get_source_1() const { return ___source_1; }
	inline Il2CppObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(Il2CppObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier(&___source_1, value);
	}

	inline static int32_t get_offset_of_useDefault_2() { return static_cast<int32_t>(offsetof(LastObservable_1_t3225858136, ___useDefault_2)); }
	inline bool get_useDefault_2() const { return ___useDefault_2; }
	inline bool* get_address_of_useDefault_2() { return &___useDefault_2; }
	inline void set_useDefault_2(bool value)
	{
		___useDefault_2 = value;
	}

	inline static int32_t get_offset_of_predicate_3() { return static_cast<int32_t>(offsetof(LastObservable_1_t3225858136, ___predicate_3)); }
	inline Func_2_t1509682273 * get_predicate_3() const { return ___predicate_3; }
	inline Func_2_t1509682273 ** get_address_of_predicate_3() { return &___predicate_3; }
	inline void set_predicate_3(Func_2_t1509682273 * value)
	{
		___predicate_3 = value;
		Il2CppCodeGenWriteBarrier(&___predicate_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
