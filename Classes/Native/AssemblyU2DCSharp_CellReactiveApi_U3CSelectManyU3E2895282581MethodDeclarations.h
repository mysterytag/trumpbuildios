﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/<SelectMany>c__AnonStorey119`3/<SelectMany>c__AnonStorey11A`3<System.Object,System.Object,System.Object>
struct U3CSelectManyU3Ec__AnonStorey11A_3_t2895282581;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void CellReactiveApi/<SelectMany>c__AnonStorey119`3/<SelectMany>c__AnonStorey11A`3<System.Object,System.Object,System.Object>::.ctor()
extern "C"  void U3CSelectManyU3Ec__AnonStorey11A_3__ctor_m90349578_gshared (U3CSelectManyU3Ec__AnonStorey11A_3_t2895282581 * __this, const MethodInfo* method);
#define U3CSelectManyU3Ec__AnonStorey11A_3__ctor_m90349578(__this, method) ((  void (*) (U3CSelectManyU3Ec__AnonStorey11A_3_t2895282581 *, const MethodInfo*))U3CSelectManyU3Ec__AnonStorey11A_3__ctor_m90349578_gshared)(__this, method)
// TR CellReactiveApi/<SelectMany>c__AnonStorey119`3/<SelectMany>c__AnonStorey11A`3<System.Object,System.Object,System.Object>::<>m__19F(TC)
extern "C"  Il2CppObject * U3CSelectManyU3Ec__AnonStorey11A_3_U3CU3Em__19F_m205515901_gshared (U3CSelectManyU3Ec__AnonStorey11A_3_t2895282581 * __this, Il2CppObject * ___y0, const MethodInfo* method);
#define U3CSelectManyU3Ec__AnonStorey11A_3_U3CU3Em__19F_m205515901(__this, ___y0, method) ((  Il2CppObject * (*) (U3CSelectManyU3Ec__AnonStorey11A_3_t2895282581 *, Il2CppObject *, const MethodInfo*))U3CSelectManyU3Ec__AnonStorey11A_3_U3CU3Em__19F_m205515901_gshared)(__this, ___y0, method)
