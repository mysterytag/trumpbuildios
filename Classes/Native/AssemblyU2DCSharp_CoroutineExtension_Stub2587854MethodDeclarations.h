﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CoroutineExtension/Stub
struct Stub_t2587854;

#include "codegen/il2cpp-codegen.h"

// System.Void CoroutineExtension/Stub::.ctor()
extern "C"  void Stub__ctor_m3530102453 (Stub_t2587854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
