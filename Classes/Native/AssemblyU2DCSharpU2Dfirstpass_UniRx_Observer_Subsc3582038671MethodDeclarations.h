﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observer/Subscribe_`1<System.Object>
struct Subscribe__1_t3582038671;
// System.Action`1<System.Exception>
struct Action_1_t2115686693;
// System.Action
struct Action_t437523947;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Observer/Subscribe_`1<System.Object>::.ctor(System.Action`1<System.Exception>,System.Action)
extern "C"  void Subscribe__1__ctor_m84139991_gshared (Subscribe__1_t3582038671 * __this, Action_1_t2115686693 * ___onError0, Action_t437523947 * ___onCompleted1, const MethodInfo* method);
#define Subscribe__1__ctor_m84139991(__this, ___onError0, ___onCompleted1, method) ((  void (*) (Subscribe__1_t3582038671 *, Action_1_t2115686693 *, Action_t437523947 *, const MethodInfo*))Subscribe__1__ctor_m84139991_gshared)(__this, ___onError0, ___onCompleted1, method)
// System.Void UniRx.Observer/Subscribe_`1<System.Object>::OnNext(T)
extern "C"  void Subscribe__1_OnNext_m4014976610_gshared (Subscribe__1_t3582038671 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Subscribe__1_OnNext_m4014976610(__this, ___value0, method) ((  void (*) (Subscribe__1_t3582038671 *, Il2CppObject *, const MethodInfo*))Subscribe__1_OnNext_m4014976610_gshared)(__this, ___value0, method)
// System.Void UniRx.Observer/Subscribe_`1<System.Object>::OnError(System.Exception)
extern "C"  void Subscribe__1_OnError_m2994652529_gshared (Subscribe__1_t3582038671 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Subscribe__1_OnError_m2994652529(__this, ___error0, method) ((  void (*) (Subscribe__1_t3582038671 *, Exception_t1967233988 *, const MethodInfo*))Subscribe__1_OnError_m2994652529_gshared)(__this, ___error0, method)
// System.Void UniRx.Observer/Subscribe_`1<System.Object>::OnCompleted()
extern "C"  void Subscribe__1_OnCompleted_m2654820932_gshared (Subscribe__1_t3582038671 * __this, const MethodInfo* method);
#define Subscribe__1_OnCompleted_m2654820932(__this, method) ((  void (*) (Subscribe__1_t3582038671 *, const MethodInfo*))Subscribe__1_OnCompleted_m2654820932_gshared)(__this, method)
