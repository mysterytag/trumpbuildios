﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellUtils.CellJoinDisposable`1<System.Double>
struct CellJoinDisposable_1_t3433903597;

#include "codegen/il2cpp-codegen.h"

// System.Void CellUtils.CellJoinDisposable`1<System.Double>::.ctor()
extern "C"  void CellJoinDisposable_1__ctor_m3806792957_gshared (CellJoinDisposable_1_t3433903597 * __this, const MethodInfo* method);
#define CellJoinDisposable_1__ctor_m3806792957(__this, method) ((  void (*) (CellJoinDisposable_1_t3433903597 *, const MethodInfo*))CellJoinDisposable_1__ctor_m3806792957_gshared)(__this, method)
