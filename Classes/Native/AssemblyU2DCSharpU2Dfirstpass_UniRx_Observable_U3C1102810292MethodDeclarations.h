﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/<SelectMany>c__AnonStorey49`2<UniRx.Unit,UniRx.Unit>
struct U3CSelectManyU3Ec__AnonStorey49_2_t1102810292;
// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

// System.Void UniRx.Observable/<SelectMany>c__AnonStorey49`2<UniRx.Unit,UniRx.Unit>::.ctor()
extern "C"  void U3CSelectManyU3Ec__AnonStorey49_2__ctor_m1605157740_gshared (U3CSelectManyU3Ec__AnonStorey49_2_t1102810292 * __this, const MethodInfo* method);
#define U3CSelectManyU3Ec__AnonStorey49_2__ctor_m1605157740(__this, method) ((  void (*) (U3CSelectManyU3Ec__AnonStorey49_2_t1102810292 *, const MethodInfo*))U3CSelectManyU3Ec__AnonStorey49_2__ctor_m1605157740_gshared)(__this, method)
// UniRx.IObservable`1<TR> UniRx.Observable/<SelectMany>c__AnonStorey49`2<UniRx.Unit,UniRx.Unit>::<>m__47(T)
extern "C"  Il2CppObject* U3CSelectManyU3Ec__AnonStorey49_2_U3CU3Em__47_m3016679996_gshared (U3CSelectManyU3Ec__AnonStorey49_2_t1102810292 * __this, Unit_t2558286038  ____0, const MethodInfo* method);
#define U3CSelectManyU3Ec__AnonStorey49_2_U3CU3Em__47_m3016679996(__this, ____0, method) ((  Il2CppObject* (*) (U3CSelectManyU3Ec__AnonStorey49_2_t1102810292 *, Unit_t2558286038 , const MethodInfo*))U3CSelectManyU3Ec__AnonStorey49_2_U3CU3Em__47_m3016679996_gshared)(__this, ____0, method)
