﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_2_gen1608951971MethodDeclarations.h"

// System.Void System.Action`2<System.Int32,System.String>::.ctor(System.Object,System.IntPtr)
#define Action_2__ctor_m2044607693(__this, ___object0, ___method1, method) ((  void (*) (Action_2_t1740334453 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_2__ctor_m3580276559_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`2<System.Int32,System.String>::Invoke(T1,T2)
#define Action_2_Invoke_m2349932106(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t1740334453 *, int32_t, String_t*, const MethodInfo*))Action_2_Invoke_m2811796860_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Action`2<System.Int32,System.String>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Action_2_BeginInvoke_m1901497333(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Action_2_t1740334453 *, int32_t, String_t*, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Action_2_BeginInvoke_m1802923299_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// System.Void System.Action`2<System.Int32,System.String>::EndInvoke(System.IAsyncResult)
#define Action_2_EndInvoke_m488490573(__this, ___result0, method) ((  void (*) (Action_2_t1740334453 *, Il2CppObject *, const MethodInfo*))Action_2_EndInvoke_m1650204895_gshared)(__this, ___result0, method)
