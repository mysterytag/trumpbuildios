﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<LinkKongregate>c__AnonStorey7B
struct U3CLinkKongregateU3Ec__AnonStorey7B_t203880837;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<LinkKongregate>c__AnonStorey7B::.ctor()
extern "C"  void U3CLinkKongregateU3Ec__AnonStorey7B__ctor_m3512091598 (U3CLinkKongregateU3Ec__AnonStorey7B_t203880837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<LinkKongregate>c__AnonStorey7B::<>m__68(PlayFab.CallRequestContainer)
extern "C"  void U3CLinkKongregateU3Ec__AnonStorey7B_U3CU3Em__68_m2028240238 (U3CLinkKongregateU3Ec__AnonStorey7B_t203880837 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
