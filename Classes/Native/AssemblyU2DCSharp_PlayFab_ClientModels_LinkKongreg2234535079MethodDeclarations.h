﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.LinkKongregateAccountRequest
struct LinkKongregateAccountRequest_t2234535079;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.LinkKongregateAccountRequest::.ctor()
extern "C"  void LinkKongregateAccountRequest__ctor_m992198774 (LinkKongregateAccountRequest_t2234535079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LinkKongregateAccountRequest::get_KongregateId()
extern "C"  String_t* LinkKongregateAccountRequest_get_KongregateId_m1405813034 (LinkKongregateAccountRequest_t2234535079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LinkKongregateAccountRequest::set_KongregateId(System.String)
extern "C"  void LinkKongregateAccountRequest_set_KongregateId_m718452161 (LinkKongregateAccountRequest_t2234535079 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LinkKongregateAccountRequest::get_AuthTicket()
extern "C"  String_t* LinkKongregateAccountRequest_get_AuthTicket_m1523538152 (LinkKongregateAccountRequest_t2234535079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LinkKongregateAccountRequest::set_AuthTicket(System.String)
extern "C"  void LinkKongregateAccountRequest_set_AuthTicket_m152451331 (LinkKongregateAccountRequest_t2234535079 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
