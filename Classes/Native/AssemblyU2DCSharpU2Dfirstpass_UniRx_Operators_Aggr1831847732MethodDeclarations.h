﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.AggregateObservable`2/Aggregate<System.Object,System.Object>
struct Aggregate_t1831847732;
// UniRx.Operators.AggregateObservable`2<System.Object,System.Object>
struct AggregateObservable_2_t1747477946;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.AggregateObservable`2/Aggregate<System.Object,System.Object>::.ctor(UniRx.Operators.AggregateObservable`2<TSource,TAccumulate>,UniRx.IObserver`1<TAccumulate>,System.IDisposable)
extern "C"  void Aggregate__ctor_m1480151888_gshared (Aggregate_t1831847732 * __this, AggregateObservable_2_t1747477946 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Aggregate__ctor_m1480151888(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Aggregate_t1831847732 *, AggregateObservable_2_t1747477946 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Aggregate__ctor_m1480151888_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.AggregateObservable`2/Aggregate<System.Object,System.Object>::OnNext(TSource)
extern "C"  void Aggregate_OnNext_m1410486751_gshared (Aggregate_t1831847732 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Aggregate_OnNext_m1410486751(__this, ___value0, method) ((  void (*) (Aggregate_t1831847732 *, Il2CppObject *, const MethodInfo*))Aggregate_OnNext_m1410486751_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.AggregateObservable`2/Aggregate<System.Object,System.Object>::OnError(System.Exception)
extern "C"  void Aggregate_OnError_m303138441_gshared (Aggregate_t1831847732 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Aggregate_OnError_m303138441(__this, ___error0, method) ((  void (*) (Aggregate_t1831847732 *, Exception_t1967233988 *, const MethodInfo*))Aggregate_OnError_m303138441_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.AggregateObservable`2/Aggregate<System.Object,System.Object>::OnCompleted()
extern "C"  void Aggregate_OnCompleted_m3419864924_gshared (Aggregate_t1831847732 * __this, const MethodInfo* method);
#define Aggregate_OnCompleted_m3419864924(__this, method) ((  void (*) (Aggregate_t1831847732 *, const MethodInfo*))Aggregate_OnCompleted_m3419864924_gshared)(__this, method)
