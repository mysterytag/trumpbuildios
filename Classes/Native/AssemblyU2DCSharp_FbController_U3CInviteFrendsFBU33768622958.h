﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// FbController
struct FbController_t3069175192;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FbController/<InviteFrendsFB>c__AnonStorey55
struct  U3CInviteFrendsFBU3Ec__AnonStorey55_t3768622958  : public Il2CppObject
{
public:
	// System.Int32 FbController/<InviteFrendsFB>c__AnonStorey55::its
	int32_t ___its_0;
	// FbController FbController/<InviteFrendsFB>c__AnonStorey55::<>f__this
	FbController_t3069175192 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_its_0() { return static_cast<int32_t>(offsetof(U3CInviteFrendsFBU3Ec__AnonStorey55_t3768622958, ___its_0)); }
	inline int32_t get_its_0() const { return ___its_0; }
	inline int32_t* get_address_of_its_0() { return &___its_0; }
	inline void set_its_0(int32_t value)
	{
		___its_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CInviteFrendsFBU3Ec__AnonStorey55_t3768622958, ___U3CU3Ef__this_1)); }
	inline FbController_t3069175192 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline FbController_t3069175192 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(FbController_t3069175192 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
