﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.DistinctUntilChangedObservable`1<System.Int32>
struct DistinctUntilChangedObservable_1_t1541420035;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera701568569.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.DistinctUntilChangedObservable`1/DistinctUntilChanged<System.Int32>
struct  DistinctUntilChanged_t18354282  : public OperatorObserverBase_2_t701568569
{
public:
	// UniRx.Operators.DistinctUntilChangedObservable`1<T> UniRx.Operators.DistinctUntilChangedObservable`1/DistinctUntilChanged::parent
	DistinctUntilChangedObservable_1_t1541420035 * ___parent_2;
	// System.Boolean UniRx.Operators.DistinctUntilChangedObservable`1/DistinctUntilChanged::isFirst
	bool ___isFirst_3;
	// T UniRx.Operators.DistinctUntilChangedObservable`1/DistinctUntilChanged::prevKey
	int32_t ___prevKey_4;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(DistinctUntilChanged_t18354282, ___parent_2)); }
	inline DistinctUntilChangedObservable_1_t1541420035 * get_parent_2() const { return ___parent_2; }
	inline DistinctUntilChangedObservable_1_t1541420035 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(DistinctUntilChangedObservable_1_t1541420035 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_isFirst_3() { return static_cast<int32_t>(offsetof(DistinctUntilChanged_t18354282, ___isFirst_3)); }
	inline bool get_isFirst_3() const { return ___isFirst_3; }
	inline bool* get_address_of_isFirst_3() { return &___isFirst_3; }
	inline void set_isFirst_3(bool value)
	{
		___isFirst_3 = value;
	}

	inline static int32_t get_offset_of_prevKey_4() { return static_cast<int32_t>(offsetof(DistinctUntilChanged_t18354282, ___prevKey_4)); }
	inline int32_t get_prevKey_4() const { return ___prevKey_4; }
	inline int32_t* get_address_of_prevKey_4() { return &___prevKey_4; }
	inline void set_prevKey_4(int32_t value)
	{
		___prevKey_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
