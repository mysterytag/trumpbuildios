﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraAdjuster
struct CameraAdjuster_t1037408993;

#include "codegen/il2cpp-codegen.h"

// System.Void CameraAdjuster::.ctor()
extern "C"  void CameraAdjuster__ctor_m3396984538 (CameraAdjuster_t1037408993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraAdjuster::Awake()
extern "C"  void CameraAdjuster_Awake_m3634589757 (CameraAdjuster_t1037408993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
