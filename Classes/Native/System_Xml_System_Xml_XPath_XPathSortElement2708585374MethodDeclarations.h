﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XPath.XPathSortElement
struct XPathSortElement_t2708585374;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Xml.XPath.XPathSortElement::.ctor()
extern "C"  void XPathSortElement__ctor_m2356815033 (XPathSortElement_t2708585374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
