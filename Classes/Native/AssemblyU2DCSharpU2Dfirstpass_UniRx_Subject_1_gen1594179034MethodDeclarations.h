﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen22637510MethodDeclarations.h"

// System.Void UniRx.Subject`1<UniRx.Tuple`2<System.Single[],System.Int32>>::.ctor()
#define Subject_1__ctor_m1523924396(__this, method) ((  void (*) (Subject_1_t1594179034 *, const MethodInfo*))Subject_1__ctor_m2775933633_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.Tuple`2<System.Single[],System.Int32>>::get_HasObservers()
#define Subject_1_get_HasObservers_m2196738912(__this, method) ((  bool (*) (Subject_1_t1594179034 *, const MethodInfo*))Subject_1_get_HasObservers_m3071120939_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.Tuple`2<System.Single[],System.Int32>>::OnCompleted()
#define Subject_1_OnCompleted_m4274946102(__this, method) ((  void (*) (Subject_1_t1594179034 *, const MethodInfo*))Subject_1_OnCompleted_m1860288907_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.Tuple`2<System.Single[],System.Int32>>::OnError(System.Exception)
#define Subject_1_OnError_m693932131(__this, ___error0, method) ((  void (*) (Subject_1_t1594179034 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m2333571640_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<UniRx.Tuple`2<System.Single[],System.Int32>>::OnNext(T)
#define Subject_1_OnNext_m2738199892(__this, ___value0, method) ((  void (*) (Subject_1_t1594179034 *, Tuple_2_t3951111710 , const MethodInfo*))Subject_1_OnNext_m3328233769_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<UniRx.Tuple`2<System.Single[],System.Int32>>::Subscribe(UniRx.IObserver`1<T>)
#define Subject_1_Subscribe_m3559273707(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t1594179034 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m2458390592_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<UniRx.Tuple`2<System.Single[],System.Int32>>::Dispose()
#define Subject_1_Dispose_m4100746985(__this, method) ((  void (*) (Subject_1_t1594179034 *, const MethodInfo*))Subject_1_Dispose_m395813566_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.Tuple`2<System.Single[],System.Int32>>::ThrowIfDisposed()
#define Subject_1_ThrowIfDisposed_m830807922(__this, method) ((  void (*) (Subject_1_t1594179034 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m4173180487_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.Tuple`2<System.Single[],System.Int32>>::IsRequiredSubscribeOnCurrentThread()
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m270657623(__this, method) ((  bool (*) (Subject_1_t1594179034 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m597618274_gshared)(__this, method)
