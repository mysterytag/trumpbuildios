﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.WhenAllObservable`1/WhenAll/WhenAllCollectionObserver<System.Object>
struct WhenAllCollectionObserver_t3737216924;
// UniRx.Operators.WhenAllObservable`1/WhenAll<System.Object>
struct WhenAll_t959244232;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.WhenAllObservable`1/WhenAll/WhenAllCollectionObserver<System.Object>::.ctor(UniRx.Operators.WhenAllObservable`1/WhenAll<T>,System.Int32)
extern "C"  void WhenAllCollectionObserver__ctor_m2588162662_gshared (WhenAllCollectionObserver_t3737216924 * __this, WhenAll_t959244232 * ___parent0, int32_t ___index1, const MethodInfo* method);
#define WhenAllCollectionObserver__ctor_m2588162662(__this, ___parent0, ___index1, method) ((  void (*) (WhenAllCollectionObserver_t3737216924 *, WhenAll_t959244232 *, int32_t, const MethodInfo*))WhenAllCollectionObserver__ctor_m2588162662_gshared)(__this, ___parent0, ___index1, method)
// System.Void UniRx.Operators.WhenAllObservable`1/WhenAll/WhenAllCollectionObserver<System.Object>::OnNext(T)
extern "C"  void WhenAllCollectionObserver_OnNext_m2027391139_gshared (WhenAllCollectionObserver_t3737216924 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define WhenAllCollectionObserver_OnNext_m2027391139(__this, ___value0, method) ((  void (*) (WhenAllCollectionObserver_t3737216924 *, Il2CppObject *, const MethodInfo*))WhenAllCollectionObserver_OnNext_m2027391139_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.WhenAllObservable`1/WhenAll/WhenAllCollectionObserver<System.Object>::OnError(System.Exception)
extern "C"  void WhenAllCollectionObserver_OnError_m1882455474_gshared (WhenAllCollectionObserver_t3737216924 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define WhenAllCollectionObserver_OnError_m1882455474(__this, ___error0, method) ((  void (*) (WhenAllCollectionObserver_t3737216924 *, Exception_t1967233988 *, const MethodInfo*))WhenAllCollectionObserver_OnError_m1882455474_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.WhenAllObservable`1/WhenAll/WhenAllCollectionObserver<System.Object>::OnCompleted()
extern "C"  void WhenAllCollectionObserver_OnCompleted_m266087429_gshared (WhenAllCollectionObserver_t3737216924 * __this, const MethodInfo* method);
#define WhenAllCollectionObserver_OnCompleted_m266087429(__this, method) ((  void (*) (WhenAllCollectionObserver_t3737216924 *, const MethodInfo*))WhenAllCollectionObserver_OnCompleted_m266087429_gshared)(__this, method)
