﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.DisposedObserver`1<UniRx.Tuple`2<System.Object,System.Object>>
struct DisposedObserver_1_t3838686577;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen369261819.h"

// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m2529361114_gshared (DisposedObserver_1_t3838686577 * __this, const MethodInfo* method);
#define DisposedObserver_1__ctor_m2529361114(__this, method) ((  void (*) (DisposedObserver_1_t3838686577 *, const MethodInfo*))DisposedObserver_1__ctor_m2529361114_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m618686995_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define DisposedObserver_1__cctor_m618686995(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))DisposedObserver_1__cctor_m618686995_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::OnCompleted()
extern "C"  void DisposedObserver_1_OnCompleted_m479175908_gshared (DisposedObserver_1_t3838686577 * __this, const MethodInfo* method);
#define DisposedObserver_1_OnCompleted_m479175908(__this, method) ((  void (*) (DisposedObserver_1_t3838686577 *, const MethodInfo*))DisposedObserver_1_OnCompleted_m479175908_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::OnError(System.Exception)
extern "C"  void DisposedObserver_1_OnError_m932585489_gshared (DisposedObserver_1_t3838686577 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define DisposedObserver_1_OnError_m932585489(__this, ___error0, method) ((  void (*) (DisposedObserver_1_t3838686577 *, Exception_t1967233988 *, const MethodInfo*))DisposedObserver_1_OnError_m932585489_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.Tuple`2<System.Object,System.Object>>::OnNext(T)
extern "C"  void DisposedObserver_1_OnNext_m2595244290_gshared (DisposedObserver_1_t3838686577 * __this, Tuple_2_t369261819  ___value0, const MethodInfo* method);
#define DisposedObserver_1_OnNext_m2595244290(__this, ___value0, method) ((  void (*) (DisposedObserver_1_t3838686577 *, Tuple_2_t369261819 , const MethodInfo*))DisposedObserver_1_OnNext_m2595244290_gshared)(__this, ___value0, method)
