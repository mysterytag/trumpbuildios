﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;
// UnityEngine.UI.Graphic
struct Graphic_t933884113;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic933884113.h"

// UniRx.IObservable`1<UniRx.Unit> UniRx.UnityGraphicExtensions::DirtyLayoutCallbackAsObservable(UnityEngine.UI.Graphic)
extern "C"  Il2CppObject* UnityGraphicExtensions_DirtyLayoutCallbackAsObservable_m3926999319 (Il2CppObject * __this /* static, unused */, Graphic_t933884113 * ___graphic0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.UnityGraphicExtensions::DirtyMaterialCallbackAsObservable(UnityEngine.UI.Graphic)
extern "C"  Il2CppObject* UnityGraphicExtensions_DirtyMaterialCallbackAsObservable_m3189178836 (Il2CppObject * __this /* static, unused */, Graphic_t933884113 * ___graphic0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.UnityGraphicExtensions::DirtyVerticesCallbackAsObservable(UnityEngine.UI.Graphic)
extern "C"  Il2CppObject* UnityGraphicExtensions_DirtyVerticesCallbackAsObservable_m3588526502 (Il2CppObject * __this /* static, unused */, Graphic_t933884113 * ___graphic0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
