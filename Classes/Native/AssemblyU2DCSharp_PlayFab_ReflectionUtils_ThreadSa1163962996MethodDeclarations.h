﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>
struct ThreadSafeDictionary_2_t1163962996;
// PlayFab.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Object,System.Object>
struct ThreadSafeDictionaryValueFactory_2_t1771086527;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1302937806;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t346249057;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t501095600;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23312956448.h"

// System.Void PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::.ctor(PlayFab.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<TKey,TValue>)
extern "C"  void ThreadSafeDictionary_2__ctor_m145995086_gshared (ThreadSafeDictionary_2_t1163962996 * __this, ThreadSafeDictionaryValueFactory_2_t1771086527 * ___valueFactory0, const MethodInfo* method);
#define ThreadSafeDictionary_2__ctor_m145995086(__this, ___valueFactory0, method) ((  void (*) (ThreadSafeDictionary_2_t1163962996 *, ThreadSafeDictionaryValueFactory_2_t1771086527 *, const MethodInfo*))ThreadSafeDictionary_2__ctor_m145995086_gshared)(__this, ___valueFactory0, method)
// System.Collections.IEnumerator PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m647792317_gshared (ThreadSafeDictionary_2_t1163962996 * __this, const MethodInfo* method);
#define ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m647792317(__this, method) ((  Il2CppObject * (*) (ThreadSafeDictionary_2_t1163962996 *, const MethodInfo*))ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m647792317_gshared)(__this, method)
// TValue PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Get(TKey)
extern "C"  Il2CppObject * ThreadSafeDictionary_2_Get_m3585103853_gshared (ThreadSafeDictionary_2_t1163962996 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define ThreadSafeDictionary_2_Get_m3585103853(__this, ___key0, method) ((  Il2CppObject * (*) (ThreadSafeDictionary_2_t1163962996 *, Il2CppObject *, const MethodInfo*))ThreadSafeDictionary_2_Get_m3585103853_gshared)(__this, ___key0, method)
// TValue PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::AddValue(TKey)
extern "C"  Il2CppObject * ThreadSafeDictionary_2_AddValue_m844388037_gshared (ThreadSafeDictionary_2_t1163962996 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define ThreadSafeDictionary_2_AddValue_m844388037(__this, ___key0, method) ((  Il2CppObject * (*) (ThreadSafeDictionary_2_t1163962996 *, Il2CppObject *, const MethodInfo*))ThreadSafeDictionary_2_AddValue_m844388037_gshared)(__this, ___key0, method)
// System.Void PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Add(TKey,TValue)
extern "C"  void ThreadSafeDictionary_2_Add_m3884150033_gshared (ThreadSafeDictionary_2_t1163962996 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define ThreadSafeDictionary_2_Add_m3884150033(__this, ___key0, ___value1, method) ((  void (*) (ThreadSafeDictionary_2_t1163962996 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))ThreadSafeDictionary_2_Add_m3884150033_gshared)(__this, ___key0, ___value1, method)
// System.Boolean PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::ContainsKey(TKey)
extern "C"  bool ThreadSafeDictionary_2_ContainsKey_m2111272811_gshared (ThreadSafeDictionary_2_t1163962996 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define ThreadSafeDictionary_2_ContainsKey_m2111272811(__this, ___key0, method) ((  bool (*) (ThreadSafeDictionary_2_t1163962996 *, Il2CppObject *, const MethodInfo*))ThreadSafeDictionary_2_ContainsKey_m2111272811_gshared)(__this, ___key0, method)
// System.Collections.Generic.ICollection`1<TKey> PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_Keys()
extern "C"  Il2CppObject* ThreadSafeDictionary_2_get_Keys_m79078016_gshared (ThreadSafeDictionary_2_t1163962996 * __this, const MethodInfo* method);
#define ThreadSafeDictionary_2_get_Keys_m79078016(__this, method) ((  Il2CppObject* (*) (ThreadSafeDictionary_2_t1163962996 *, const MethodInfo*))ThreadSafeDictionary_2_get_Keys_m79078016_gshared)(__this, method)
// System.Boolean PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Remove(TKey)
extern "C"  bool ThreadSafeDictionary_2_Remove_m3287321253_gshared (ThreadSafeDictionary_2_t1163962996 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define ThreadSafeDictionary_2_Remove_m3287321253(__this, ___key0, method) ((  bool (*) (ThreadSafeDictionary_2_t1163962996 *, Il2CppObject *, const MethodInfo*))ThreadSafeDictionary_2_Remove_m3287321253_gshared)(__this, ___key0, method)
// System.Boolean PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool ThreadSafeDictionary_2_TryGetValue_m2392274116_gshared (ThreadSafeDictionary_2_t1163962996 * __this, Il2CppObject * ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define ThreadSafeDictionary_2_TryGetValue_m2392274116(__this, ___key0, ___value1, method) ((  bool (*) (ThreadSafeDictionary_2_t1163962996 *, Il2CppObject *, Il2CppObject **, const MethodInfo*))ThreadSafeDictionary_2_TryGetValue_m2392274116_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.ICollection`1<TValue> PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_Values()
extern "C"  Il2CppObject* ThreadSafeDictionary_2_get_Values_m2528035356_gshared (ThreadSafeDictionary_2_t1163962996 * __this, const MethodInfo* method);
#define ThreadSafeDictionary_2_get_Values_m2528035356(__this, method) ((  Il2CppObject* (*) (ThreadSafeDictionary_2_t1163962996 *, const MethodInfo*))ThreadSafeDictionary_2_get_Values_m2528035356_gshared)(__this, method)
// TValue PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * ThreadSafeDictionary_2_get_Item_m1128780497_gshared (ThreadSafeDictionary_2_t1163962996 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define ThreadSafeDictionary_2_get_Item_m1128780497(__this, ___key0, method) ((  Il2CppObject * (*) (ThreadSafeDictionary_2_t1163962996 *, Il2CppObject *, const MethodInfo*))ThreadSafeDictionary_2_get_Item_m1128780497_gshared)(__this, ___key0, method)
// System.Void PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::set_Item(TKey,TValue)
extern "C"  void ThreadSafeDictionary_2_set_Item_m1582880484_gshared (ThreadSafeDictionary_2_t1163962996 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define ThreadSafeDictionary_2_set_Item_m1582880484(__this, ___key0, ___value1, method) ((  void (*) (ThreadSafeDictionary_2_t1163962996 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))ThreadSafeDictionary_2_set_Item_m1582880484_gshared)(__this, ___key0, ___value1, method)
// System.Void PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void ThreadSafeDictionary_2_Add_m1635310772_gshared (ThreadSafeDictionary_2_t1163962996 * __this, KeyValuePair_2_t3312956448  ___item0, const MethodInfo* method);
#define ThreadSafeDictionary_2_Add_m1635310772(__this, ___item0, method) ((  void (*) (ThreadSafeDictionary_2_t1163962996 *, KeyValuePair_2_t3312956448 , const MethodInfo*))ThreadSafeDictionary_2_Add_m1635310772_gshared)(__this, ___item0, method)
// System.Void PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Clear()
extern "C"  void ThreadSafeDictionary_2_Clear_m3600968783_gshared (ThreadSafeDictionary_2_t1163962996 * __this, const MethodInfo* method);
#define ThreadSafeDictionary_2_Clear_m3600968783(__this, method) ((  void (*) (ThreadSafeDictionary_2_t1163962996 *, const MethodInfo*))ThreadSafeDictionary_2_Clear_m3600968783_gshared)(__this, method)
// System.Boolean PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool ThreadSafeDictionary_2_Contains_m78891804_gshared (ThreadSafeDictionary_2_t1163962996 * __this, KeyValuePair_2_t3312956448  ___item0, const MethodInfo* method);
#define ThreadSafeDictionary_2_Contains_m78891804(__this, ___item0, method) ((  bool (*) (ThreadSafeDictionary_2_t1163962996 *, KeyValuePair_2_t3312956448 , const MethodInfo*))ThreadSafeDictionary_2_Contains_m78891804_gshared)(__this, ___item0, method)
// System.Void PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void ThreadSafeDictionary_2_CopyTo_m3361965976_gshared (ThreadSafeDictionary_2_t1163962996 * __this, KeyValuePair_2U5BU5D_t346249057* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define ThreadSafeDictionary_2_CopyTo_m3361965976(__this, ___array0, ___arrayIndex1, method) ((  void (*) (ThreadSafeDictionary_2_t1163962996 *, KeyValuePair_2U5BU5D_t346249057*, int32_t, const MethodInfo*))ThreadSafeDictionary_2_CopyTo_m3361965976_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Int32 PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_Count()
extern "C"  int32_t ThreadSafeDictionary_2_get_Count_m187477558_gshared (ThreadSafeDictionary_2_t1163962996 * __this, const MethodInfo* method);
#define ThreadSafeDictionary_2_get_Count_m187477558(__this, method) ((  int32_t (*) (ThreadSafeDictionary_2_t1163962996 *, const MethodInfo*))ThreadSafeDictionary_2_get_Count_m187477558_gshared)(__this, method)
// System.Boolean PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_IsReadOnly()
extern "C"  bool ThreadSafeDictionary_2_get_IsReadOnly_m3744395425_gshared (ThreadSafeDictionary_2_t1163962996 * __this, const MethodInfo* method);
#define ThreadSafeDictionary_2_get_IsReadOnly_m3744395425(__this, method) ((  bool (*) (ThreadSafeDictionary_2_t1163962996 *, const MethodInfo*))ThreadSafeDictionary_2_get_IsReadOnly_m3744395425_gshared)(__this, method)
// System.Boolean PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool ThreadSafeDictionary_2_Remove_m4054976833_gshared (ThreadSafeDictionary_2_t1163962996 * __this, KeyValuePair_2_t3312956448  ___item0, const MethodInfo* method);
#define ThreadSafeDictionary_2_Remove_m4054976833(__this, ___item0, method) ((  bool (*) (ThreadSafeDictionary_2_t1163962996 *, KeyValuePair_2_t3312956448 , const MethodInfo*))ThreadSafeDictionary_2_Remove_m4054976833_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> PlayFab.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::GetEnumerator()
extern "C"  Il2CppObject* ThreadSafeDictionary_2_GetEnumerator_m2011494711_gshared (ThreadSafeDictionary_2_t1163962996 * __this, const MethodInfo* method);
#define ThreadSafeDictionary_2_GetEnumerator_m2011494711(__this, method) ((  Il2CppObject* (*) (ThreadSafeDictionary_2_t1163962996 *, const MethodInfo*))ThreadSafeDictionary_2_GetEnumerator_m2011494711_gshared)(__this, method)
