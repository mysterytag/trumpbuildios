﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen2484541604.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_TitleActiva3893470992.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Nullable`1<PlayFab.ClientModels.TitleActivationStatus>::.ctor(T)
extern "C"  void Nullable_1__ctor_m1462692091_gshared (Nullable_1_t2484541604 * __this, int32_t ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m1462692091(__this, ___value0, method) ((  void (*) (Nullable_1_t2484541604 *, int32_t, const MethodInfo*))Nullable_1__ctor_m1462692091_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<PlayFab.ClientModels.TitleActivationStatus>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m2222856735_gshared (Nullable_1_t2484541604 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m2222856735(__this, method) ((  bool (*) (Nullable_1_t2484541604 *, const MethodInfo*))Nullable_1_get_HasValue_m2222856735_gshared)(__this, method)
// T System.Nullable`1<PlayFab.ClientModels.TitleActivationStatus>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m1896401704_gshared (Nullable_1_t2484541604 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m1896401704(__this, method) ((  int32_t (*) (Nullable_1_t2484541604 *, const MethodInfo*))Nullable_1_get_Value_m1896401704_gshared)(__this, method)
// System.Boolean System.Nullable`1<PlayFab.ClientModels.TitleActivationStatus>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1095735158_gshared (Nullable_1_t2484541604 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m1095735158(__this, ___other0, method) ((  bool (*) (Nullable_1_t2484541604 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m1095735158_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<PlayFab.ClientModels.TitleActivationStatus>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2123680521_gshared (Nullable_1_t2484541604 * __this, Nullable_1_t2484541604  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m2123680521(__this, ___other0, method) ((  bool (*) (Nullable_1_t2484541604 *, Nullable_1_t2484541604 , const MethodInfo*))Nullable_1_Equals_m2123680521_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<PlayFab.ClientModels.TitleActivationStatus>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1168911694_gshared (Nullable_1_t2484541604 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m1168911694(__this, method) ((  int32_t (*) (Nullable_1_t2484541604 *, const MethodInfo*))Nullable_1_GetHashCode_m1168911694_gshared)(__this, method)
// T System.Nullable`1<PlayFab.ClientModels.TitleActivationStatus>::GetValueOrDefault()
extern "C"  int32_t Nullable_1_GetValueOrDefault_m1565473667_gshared (Nullable_1_t2484541604 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m1565473667(__this, method) ((  int32_t (*) (Nullable_1_t2484541604 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m1565473667_gshared)(__this, method)
// System.String System.Nullable`1<PlayFab.ClientModels.TitleActivationStatus>::ToString()
extern "C"  String_t* Nullable_1_ToString_m926414602_gshared (Nullable_1_t2484541604 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m926414602(__this, method) ((  String_t* (*) (Nullable_1_t2484541604 *, const MethodInfo*))Nullable_1_ToString_m926414602_gshared)(__this, method)
// System.Nullable`1<T> System.Nullable`1<PlayFab.ClientModels.TitleActivationStatus>::op_Implicit(T)
extern "C"  Nullable_1_t2484541604  Nullable_1_op_Implicit_m2734339268_gshared (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method);
#define Nullable_1_op_Implicit_m2734339268(__this /* static, unused */, ___value0, method) ((  Nullable_1_t2484541604  (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))Nullable_1_op_Implicit_m2734339268_gshared)(__this /* static, unused */, ___value0, method)
