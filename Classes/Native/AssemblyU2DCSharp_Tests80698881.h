﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`3<System.Int32,System.Int32,System.Int32>
struct Func_3_t543102568;
// System.Action`1<System.Int32>
struct Action_1_t2995867492;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tests
struct  Tests_t80698881  : public Il2CppObject
{
public:

public:
};

struct Tests_t80698881_StaticFields
{
public:
	// System.Func`3<System.Int32,System.Int32,System.Int32> Tests::<>f__am$cache0
	Func_3_t543102568 * ___U3CU3Ef__amU24cache0_0;
	// System.Action`1<System.Int32> Tests::<>f__am$cache1
	Action_1_t2995867492 * ___U3CU3Ef__amU24cache1_1;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(Tests_t80698881_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline Func_3_t543102568 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline Func_3_t543102568 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(Func_3_t543102568 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(Tests_t80698881_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline Action_1_t2995867492 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline Action_1_t2995867492 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(Action_1_t2995867492 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
