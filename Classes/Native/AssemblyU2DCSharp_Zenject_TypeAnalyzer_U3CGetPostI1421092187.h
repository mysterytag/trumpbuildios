﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.Type>
struct List_1_t3576188904;
// System.Type
struct Type_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TypeAnalyzer/<GetPostInjectMethods>c__AnonStorey182
struct  U3CGetPostInjectMethodsU3Ec__AnonStorey182_t1421092187  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<System.Type> Zenject.TypeAnalyzer/<GetPostInjectMethods>c__AnonStorey182::heirarchyList
	List_1_t3576188904 * ___heirarchyList_0;
	// System.Type Zenject.TypeAnalyzer/<GetPostInjectMethods>c__AnonStorey182::type
	Type_t * ___type_1;

public:
	inline static int32_t get_offset_of_heirarchyList_0() { return static_cast<int32_t>(offsetof(U3CGetPostInjectMethodsU3Ec__AnonStorey182_t1421092187, ___heirarchyList_0)); }
	inline List_1_t3576188904 * get_heirarchyList_0() const { return ___heirarchyList_0; }
	inline List_1_t3576188904 ** get_address_of_heirarchyList_0() { return &___heirarchyList_0; }
	inline void set_heirarchyList_0(List_1_t3576188904 * value)
	{
		___heirarchyList_0 = value;
		Il2CppCodeGenWriteBarrier(&___heirarchyList_0, value);
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(U3CGetPostInjectMethodsU3Ec__AnonStorey182_t1421092187, ___type_1)); }
	inline Type_t * get_type_1() const { return ___type_1; }
	inline Type_t ** get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(Type_t * value)
	{
		___type_1 = value;
		Il2CppCodeGenWriteBarrier(&___type_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
