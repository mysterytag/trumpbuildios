﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.LinkedList`1<IProcess>
struct LinkedList_1_t1471399664;
// System.Action
struct Action_t437523947;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Executer
struct  Executer_t2107661245  : public Il2CppObject
{
public:
	// System.Collections.Generic.LinkedList`1<IProcess> Executer::bearutines
	LinkedList_1_t1471399664 * ___bearutines_0;
	// System.Action Executer::disposeAction
	Action_t437523947 * ___disposeAction_1;

public:
	inline static int32_t get_offset_of_bearutines_0() { return static_cast<int32_t>(offsetof(Executer_t2107661245, ___bearutines_0)); }
	inline LinkedList_1_t1471399664 * get_bearutines_0() const { return ___bearutines_0; }
	inline LinkedList_1_t1471399664 ** get_address_of_bearutines_0() { return &___bearutines_0; }
	inline void set_bearutines_0(LinkedList_1_t1471399664 * value)
	{
		___bearutines_0 = value;
		Il2CppCodeGenWriteBarrier(&___bearutines_0, value);
	}

	inline static int32_t get_offset_of_disposeAction_1() { return static_cast<int32_t>(offsetof(Executer_t2107661245, ___disposeAction_1)); }
	inline Action_t437523947 * get_disposeAction_1() const { return ___disposeAction_1; }
	inline Action_t437523947 ** get_address_of_disposeAction_1() { return &___disposeAction_1; }
	inline void set_disposeAction_1(Action_t437523947 * value)
	{
		___disposeAction_1 = value;
		Il2CppCodeGenWriteBarrier(&___disposeAction_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
