﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen3856962970MethodDeclarations.h"

// System.Void System.Func`2<System.IAsyncResult,UniRx.Unit>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m1937720617(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t2571569893 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m298322708_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<System.IAsyncResult,UniRx.Unit>::Invoke(T)
#define Func_2_Invoke_m718556385(__this, ___arg10, method) ((  Unit_t2558286038  (*) (Func_2_t2571569893 *, Il2CppObject *, const MethodInfo*))Func_2_Invoke_m4026479570_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<System.IAsyncResult,UniRx.Unit>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m2733127828(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t2571569893 *, Il2CppObject *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m3097046021_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<System.IAsyncResult,UniRx.Unit>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m3480255891(__this, ___result0, method) ((  Unit_t2558286038  (*) (Func_2_t2571569893 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m2450917186_gshared)(__this, ___result0, method)
