﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.AggregateObservable`1<System.Object>
struct AggregateObservable_1_t497085657;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Func`3<System.Object,System.Object,System.Object>
struct Func_3_t1892209229;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.AggregateObservable`1<System.Object>::.ctor(UniRx.IObservable`1<TSource>,System.Func`3<TSource,TSource,TSource>)
extern "C"  void AggregateObservable_1__ctor_m2888832910_gshared (AggregateObservable_1_t497085657 * __this, Il2CppObject* ___source0, Func_3_t1892209229 * ___accumulator1, const MethodInfo* method);
#define AggregateObservable_1__ctor_m2888832910(__this, ___source0, ___accumulator1, method) ((  void (*) (AggregateObservable_1_t497085657 *, Il2CppObject*, Func_3_t1892209229 *, const MethodInfo*))AggregateObservable_1__ctor_m2888832910_gshared)(__this, ___source0, ___accumulator1, method)
// System.IDisposable UniRx.Operators.AggregateObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<TSource>,System.IDisposable)
extern "C"  Il2CppObject * AggregateObservable_1_SubscribeCore_m3472909844_gshared (AggregateObservable_1_t497085657 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define AggregateObservable_1_SubscribeCore_m3472909844(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (AggregateObservable_1_t497085657 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))AggregateObservable_1_SubscribeCore_m3472909844_gshared)(__this, ___observer0, ___cancel1, method)
