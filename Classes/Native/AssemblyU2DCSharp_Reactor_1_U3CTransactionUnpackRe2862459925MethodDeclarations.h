﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Reactor`1/<TransactionUnpackReact>c__AnonStorey141<System.Double>
struct U3CTransactionUnpackReactU3Ec__AnonStorey141_t2862459925;

#include "codegen/il2cpp-codegen.h"

// System.Void Reactor`1/<TransactionUnpackReact>c__AnonStorey141<System.Double>::.ctor()
extern "C"  void U3CTransactionUnpackReactU3Ec__AnonStorey141__ctor_m1518789239_gshared (U3CTransactionUnpackReactU3Ec__AnonStorey141_t2862459925 * __this, const MethodInfo* method);
#define U3CTransactionUnpackReactU3Ec__AnonStorey141__ctor_m1518789239(__this, method) ((  void (*) (U3CTransactionUnpackReactU3Ec__AnonStorey141_t2862459925 *, const MethodInfo*))U3CTransactionUnpackReactU3Ec__AnonStorey141__ctor_m1518789239_gshared)(__this, method)
// System.Void Reactor`1/<TransactionUnpackReact>c__AnonStorey141<System.Double>::<>m__1D6()
extern "C"  void U3CTransactionUnpackReactU3Ec__AnonStorey141_U3CU3Em__1D6_m1600127493_gshared (U3CTransactionUnpackReactU3Ec__AnonStorey141_t2862459925 * __this, const MethodInfo* method);
#define U3CTransactionUnpackReactU3Ec__AnonStorey141_U3CU3Em__1D6_m1600127493(__this, method) ((  void (*) (U3CTransactionUnpackReactU3Ec__AnonStorey141_t2862459925 *, const MethodInfo*))U3CTransactionUnpackReactU3Ec__AnonStorey141_U3CU3Em__1D6_m1600127493_gshared)(__this, method)
