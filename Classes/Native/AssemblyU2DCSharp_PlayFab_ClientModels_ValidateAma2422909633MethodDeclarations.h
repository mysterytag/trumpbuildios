﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.ValidateAmazonReceiptRequest
struct ValidateAmazonReceiptRequest_t2422909633;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.ValidateAmazonReceiptRequest::.ctor()
extern "C"  void ValidateAmazonReceiptRequest__ctor_m4163411868 (ValidateAmazonReceiptRequest_t2422909633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.ValidateAmazonReceiptRequest::get_ReceiptId()
extern "C"  String_t* ValidateAmazonReceiptRequest_get_ReceiptId_m2710579815 (ValidateAmazonReceiptRequest_t2422909633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ValidateAmazonReceiptRequest::set_ReceiptId(System.String)
extern "C"  void ValidateAmazonReceiptRequest_set_ReceiptId_m706230514 (ValidateAmazonReceiptRequest_t2422909633 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.ValidateAmazonReceiptRequest::get_UserId()
extern "C"  String_t* ValidateAmazonReceiptRequest_get_UserId_m2902640116 (ValidateAmazonReceiptRequest_t2422909633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ValidateAmazonReceiptRequest::set_UserId(System.String)
extern "C"  void ValidateAmazonReceiptRequest_set_UserId_m3958684663 (ValidateAmazonReceiptRequest_t2422909633 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.ValidateAmazonReceiptRequest::get_CatalogVersion()
extern "C"  String_t* ValidateAmazonReceiptRequest_get_CatalogVersion_m3004278509 (ValidateAmazonReceiptRequest_t2422909633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ValidateAmazonReceiptRequest::set_CatalogVersion(System.String)
extern "C"  void ValidateAmazonReceiptRequest_set_CatalogVersion_m2112257694 (ValidateAmazonReceiptRequest_t2422909633 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.ValidateAmazonReceiptRequest::get_CurrencyCode()
extern "C"  String_t* ValidateAmazonReceiptRequest_get_CurrencyCode_m3458936204 (ValidateAmazonReceiptRequest_t2422909633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ValidateAmazonReceiptRequest::set_CurrencyCode(System.String)
extern "C"  void ValidateAmazonReceiptRequest_set_CurrencyCode_m570505375 (ValidateAmazonReceiptRequest_t2422909633 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlayFab.ClientModels.ValidateAmazonReceiptRequest::get_PurchasePrice()
extern "C"  int32_t ValidateAmazonReceiptRequest_get_PurchasePrice_m2505618087 (ValidateAmazonReceiptRequest_t2422909633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ValidateAmazonReceiptRequest::set_PurchasePrice(System.Int32)
extern "C"  void ValidateAmazonReceiptRequest_set_PurchasePrice_m2346286582 (ValidateAmazonReceiptRequest_t2422909633 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
