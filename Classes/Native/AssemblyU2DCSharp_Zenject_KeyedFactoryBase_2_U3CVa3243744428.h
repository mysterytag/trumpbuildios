﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type
struct Type_t;
// System.Collections.Generic.IEnumerator`1<Zenject.ZenjectResolveException>
struct IEnumerator_1_t2684159447;
// Zenject.ZenjectResolveException
struct ZenjectResolveException_t1201052999;
// Zenject.KeyedFactoryBase`2<System.Object,System.Object>
struct KeyedFactoryBase_2_t2864689411;

#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1238609310.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.KeyedFactoryBase`2/<Validate>c__Iterator46<System.Object,System.Object>
struct  U3CValidateU3Ec__Iterator46_t3243744428  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,System.Type> Zenject.KeyedFactoryBase`2/<Validate>c__Iterator46::<$s_264>__0
	Enumerator_t1238609311  ___U3CU24s_264U3E__0_0;
	// System.Type Zenject.KeyedFactoryBase`2/<Validate>c__Iterator46::<constructType>__1
	Type_t * ___U3CconstructTypeU3E__1_1;
	// System.Collections.Generic.IEnumerator`1<Zenject.ZenjectResolveException> Zenject.KeyedFactoryBase`2/<Validate>c__Iterator46::<$s_265>__2
	Il2CppObject* ___U3CU24s_265U3E__2_2;
	// Zenject.ZenjectResolveException Zenject.KeyedFactoryBase`2/<Validate>c__Iterator46::<error>__3
	ZenjectResolveException_t1201052999 * ___U3CerrorU3E__3_3;
	// System.Int32 Zenject.KeyedFactoryBase`2/<Validate>c__Iterator46::$PC
	int32_t ___U24PC_4;
	// Zenject.ZenjectResolveException Zenject.KeyedFactoryBase`2/<Validate>c__Iterator46::$current
	ZenjectResolveException_t1201052999 * ___U24current_5;
	// Zenject.KeyedFactoryBase`2<TBase,TKey> Zenject.KeyedFactoryBase`2/<Validate>c__Iterator46::<>f__this
	KeyedFactoryBase_2_t2864689411 * ___U3CU3Ef__this_6;

public:
	inline static int32_t get_offset_of_U3CU24s_264U3E__0_0() { return static_cast<int32_t>(offsetof(U3CValidateU3Ec__Iterator46_t3243744428, ___U3CU24s_264U3E__0_0)); }
	inline Enumerator_t1238609311  get_U3CU24s_264U3E__0_0() const { return ___U3CU24s_264U3E__0_0; }
	inline Enumerator_t1238609311 * get_address_of_U3CU24s_264U3E__0_0() { return &___U3CU24s_264U3E__0_0; }
	inline void set_U3CU24s_264U3E__0_0(Enumerator_t1238609311  value)
	{
		___U3CU24s_264U3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CconstructTypeU3E__1_1() { return static_cast<int32_t>(offsetof(U3CValidateU3Ec__Iterator46_t3243744428, ___U3CconstructTypeU3E__1_1)); }
	inline Type_t * get_U3CconstructTypeU3E__1_1() const { return ___U3CconstructTypeU3E__1_1; }
	inline Type_t ** get_address_of_U3CconstructTypeU3E__1_1() { return &___U3CconstructTypeU3E__1_1; }
	inline void set_U3CconstructTypeU3E__1_1(Type_t * value)
	{
		___U3CconstructTypeU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CconstructTypeU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CU24s_265U3E__2_2() { return static_cast<int32_t>(offsetof(U3CValidateU3Ec__Iterator46_t3243744428, ___U3CU24s_265U3E__2_2)); }
	inline Il2CppObject* get_U3CU24s_265U3E__2_2() const { return ___U3CU24s_265U3E__2_2; }
	inline Il2CppObject** get_address_of_U3CU24s_265U3E__2_2() { return &___U3CU24s_265U3E__2_2; }
	inline void set_U3CU24s_265U3E__2_2(Il2CppObject* value)
	{
		___U3CU24s_265U3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_265U3E__2_2, value);
	}

	inline static int32_t get_offset_of_U3CerrorU3E__3_3() { return static_cast<int32_t>(offsetof(U3CValidateU3Ec__Iterator46_t3243744428, ___U3CerrorU3E__3_3)); }
	inline ZenjectResolveException_t1201052999 * get_U3CerrorU3E__3_3() const { return ___U3CerrorU3E__3_3; }
	inline ZenjectResolveException_t1201052999 ** get_address_of_U3CerrorU3E__3_3() { return &___U3CerrorU3E__3_3; }
	inline void set_U3CerrorU3E__3_3(ZenjectResolveException_t1201052999 * value)
	{
		___U3CerrorU3E__3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CerrorU3E__3_3, value);
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CValidateU3Ec__Iterator46_t3243744428, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CValidateU3Ec__Iterator46_t3243744428, ___U24current_5)); }
	inline ZenjectResolveException_t1201052999 * get_U24current_5() const { return ___U24current_5; }
	inline ZenjectResolveException_t1201052999 ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(ZenjectResolveException_t1201052999 * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_6() { return static_cast<int32_t>(offsetof(U3CValidateU3Ec__Iterator46_t3243744428, ___U3CU3Ef__this_6)); }
	inline KeyedFactoryBase_2_t2864689411 * get_U3CU3Ef__this_6() const { return ___U3CU3Ef__this_6; }
	inline KeyedFactoryBase_2_t2864689411 ** get_address_of_U3CU3Ef__this_6() { return &___U3CU3Ef__this_6; }
	inline void set_U3CU3Ef__this_6(KeyedFactoryBase_2_t2864689411 * value)
	{
		___U3CU3Ef__this_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
