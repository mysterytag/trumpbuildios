﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/UnlinkGoogleAccountResponseCallback
struct UnlinkGoogleAccountResponseCallback_t2168639495;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.UnlinkGoogleAccountRequest
struct UnlinkGoogleAccountRequest_t4045133086;
// PlayFab.ClientModels.UnlinkGoogleAccountResult
struct UnlinkGoogleAccountResult_t2936582606;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkGoogl4045133086.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkGoogl2936582606.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/UnlinkGoogleAccountResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void UnlinkGoogleAccountResponseCallback__ctor_m2073640214 (UnlinkGoogleAccountResponseCallback_t2168639495 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UnlinkGoogleAccountResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.UnlinkGoogleAccountRequest,PlayFab.ClientModels.UnlinkGoogleAccountResult,PlayFab.PlayFabError,System.Object)
extern "C"  void UnlinkGoogleAccountResponseCallback_Invoke_m2968116099 (UnlinkGoogleAccountResponseCallback_t2168639495 * __this, String_t* ___urlPath0, int32_t ___callId1, UnlinkGoogleAccountRequest_t4045133086 * ___request2, UnlinkGoogleAccountResult_t2936582606 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_UnlinkGoogleAccountResponseCallback_t2168639495(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, UnlinkGoogleAccountRequest_t4045133086 * ___request2, UnlinkGoogleAccountResult_t2936582606 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/UnlinkGoogleAccountResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.UnlinkGoogleAccountRequest,PlayFab.ClientModels.UnlinkGoogleAccountResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnlinkGoogleAccountResponseCallback_BeginInvoke_m3392150584 (UnlinkGoogleAccountResponseCallback_t2168639495 * __this, String_t* ___urlPath0, int32_t ___callId1, UnlinkGoogleAccountRequest_t4045133086 * ___request2, UnlinkGoogleAccountResult_t2936582606 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UnlinkGoogleAccountResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void UnlinkGoogleAccountResponseCallback_EndInvoke_m3306334758 (UnlinkGoogleAccountResponseCallback_t2168639495 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
