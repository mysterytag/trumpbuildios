﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionRepl1841750536.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.CollectionReplaceEvent`1<System.Object>::.ctor(System.Int32,T,T)
extern "C"  void CollectionReplaceEvent_1__ctor_m3222019849_gshared (CollectionReplaceEvent_1_t1841750536 * __this, int32_t ___index0, Il2CppObject * ___oldValue1, Il2CppObject * ___newValue2, const MethodInfo* method);
#define CollectionReplaceEvent_1__ctor_m3222019849(__this, ___index0, ___oldValue1, ___newValue2, method) ((  void (*) (CollectionReplaceEvent_1_t1841750536 *, int32_t, Il2CppObject *, Il2CppObject *, const MethodInfo*))CollectionReplaceEvent_1__ctor_m3222019849_gshared)(__this, ___index0, ___oldValue1, ___newValue2, method)
// System.Int32 UniRx.CollectionReplaceEvent`1<System.Object>::get_Index()
extern "C"  int32_t CollectionReplaceEvent_1_get_Index_m128297605_gshared (CollectionReplaceEvent_1_t1841750536 * __this, const MethodInfo* method);
#define CollectionReplaceEvent_1_get_Index_m128297605(__this, method) ((  int32_t (*) (CollectionReplaceEvent_1_t1841750536 *, const MethodInfo*))CollectionReplaceEvent_1_get_Index_m128297605_gshared)(__this, method)
// System.Void UniRx.CollectionReplaceEvent`1<System.Object>::set_Index(System.Int32)
extern "C"  void CollectionReplaceEvent_1_set_Index_m1732157932_gshared (CollectionReplaceEvent_1_t1841750536 * __this, int32_t ___value0, const MethodInfo* method);
#define CollectionReplaceEvent_1_set_Index_m1732157932(__this, ___value0, method) ((  void (*) (CollectionReplaceEvent_1_t1841750536 *, int32_t, const MethodInfo*))CollectionReplaceEvent_1_set_Index_m1732157932_gshared)(__this, ___value0, method)
// T UniRx.CollectionReplaceEvent`1<System.Object>::get_OldValue()
extern "C"  Il2CppObject * CollectionReplaceEvent_1_get_OldValue_m609402800_gshared (CollectionReplaceEvent_1_t1841750536 * __this, const MethodInfo* method);
#define CollectionReplaceEvent_1_get_OldValue_m609402800(__this, method) ((  Il2CppObject * (*) (CollectionReplaceEvent_1_t1841750536 *, const MethodInfo*))CollectionReplaceEvent_1_get_OldValue_m609402800_gshared)(__this, method)
// System.Void UniRx.CollectionReplaceEvent`1<System.Object>::set_OldValue(T)
extern "C"  void CollectionReplaceEvent_1_set_OldValue_m1043865723_gshared (CollectionReplaceEvent_1_t1841750536 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define CollectionReplaceEvent_1_set_OldValue_m1043865723(__this, ___value0, method) ((  void (*) (CollectionReplaceEvent_1_t1841750536 *, Il2CppObject *, const MethodInfo*))CollectionReplaceEvent_1_set_OldValue_m1043865723_gshared)(__this, ___value0, method)
// T UniRx.CollectionReplaceEvent`1<System.Object>::get_NewValue()
extern "C"  Il2CppObject * CollectionReplaceEvent_1_get_NewValue_m3621820951_gshared (CollectionReplaceEvent_1_t1841750536 * __this, const MethodInfo* method);
#define CollectionReplaceEvent_1_get_NewValue_m3621820951(__this, method) ((  Il2CppObject * (*) (CollectionReplaceEvent_1_t1841750536 *, const MethodInfo*))CollectionReplaceEvent_1_get_NewValue_m3621820951_gshared)(__this, method)
// System.Void UniRx.CollectionReplaceEvent`1<System.Object>::set_NewValue(T)
extern "C"  void CollectionReplaceEvent_1_set_NewValue_m4234515188_gshared (CollectionReplaceEvent_1_t1841750536 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define CollectionReplaceEvent_1_set_NewValue_m4234515188(__this, ___value0, method) ((  void (*) (CollectionReplaceEvent_1_t1841750536 *, Il2CppObject *, const MethodInfo*))CollectionReplaceEvent_1_set_NewValue_m4234515188_gshared)(__this, ___value0, method)
// System.String UniRx.CollectionReplaceEvent`1<System.Object>::ToString()
extern "C"  String_t* CollectionReplaceEvent_1_ToString_m3965201253_gshared (CollectionReplaceEvent_1_t1841750536 * __this, const MethodInfo* method);
#define CollectionReplaceEvent_1_ToString_m3965201253(__this, method) ((  String_t* (*) (CollectionReplaceEvent_1_t1841750536 *, const MethodInfo*))CollectionReplaceEvent_1_ToString_m3965201253_gshared)(__this, method)
// System.Int32 UniRx.CollectionReplaceEvent`1<System.Object>::GetHashCode()
extern "C"  int32_t CollectionReplaceEvent_1_GetHashCode_m2028793005_gshared (CollectionReplaceEvent_1_t1841750536 * __this, const MethodInfo* method);
#define CollectionReplaceEvent_1_GetHashCode_m2028793005(__this, method) ((  int32_t (*) (CollectionReplaceEvent_1_t1841750536 *, const MethodInfo*))CollectionReplaceEvent_1_GetHashCode_m2028793005_gshared)(__this, method)
// System.Boolean UniRx.CollectionReplaceEvent`1<System.Object>::Equals(UniRx.CollectionReplaceEvent`1<T>)
extern "C"  bool CollectionReplaceEvent_1_Equals_m2739738362_gshared (CollectionReplaceEvent_1_t1841750536 * __this, CollectionReplaceEvent_1_t1841750536  ___other0, const MethodInfo* method);
#define CollectionReplaceEvent_1_Equals_m2739738362(__this, ___other0, method) ((  bool (*) (CollectionReplaceEvent_1_t1841750536 *, CollectionReplaceEvent_1_t1841750536 , const MethodInfo*))CollectionReplaceEvent_1_Equals_m2739738362_gshared)(__this, ___other0, method)
