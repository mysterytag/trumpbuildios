﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// DonateButton
struct DonateButton_t670753441;
// System.Action`1<DonateCell>
struct Action_1_t2946927090;
// TableButtons
struct TableButtons_t1868573683;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TableButtons/<AddOpenIabButton>c__AnonStorey15C
struct  U3CAddOpenIabButtonU3Ec__AnonStorey15C_t3212335977  : public Il2CppObject
{
public:
	// System.String TableButtons/<AddOpenIabButton>c__AnonStorey15C::text
	String_t* ___text_0;
	// DonateButton TableButtons/<AddOpenIabButton>c__AnonStorey15C::buttonInfo
	DonateButton_t670753441 * ___buttonInfo_1;
	// System.String TableButtons/<AddOpenIabButton>c__AnonStorey15C::sprite
	String_t* ___sprite_2;
	// System.Action`1<DonateCell> TableButtons/<AddOpenIabButton>c__AnonStorey15C::initCell
	Action_1_t2946927090 * ___initCell_3;
	// TableButtons TableButtons/<AddOpenIabButton>c__AnonStorey15C::<>f__this
	TableButtons_t1868573683 * ___U3CU3Ef__this_4;

public:
	inline static int32_t get_offset_of_text_0() { return static_cast<int32_t>(offsetof(U3CAddOpenIabButtonU3Ec__AnonStorey15C_t3212335977, ___text_0)); }
	inline String_t* get_text_0() const { return ___text_0; }
	inline String_t** get_address_of_text_0() { return &___text_0; }
	inline void set_text_0(String_t* value)
	{
		___text_0 = value;
		Il2CppCodeGenWriteBarrier(&___text_0, value);
	}

	inline static int32_t get_offset_of_buttonInfo_1() { return static_cast<int32_t>(offsetof(U3CAddOpenIabButtonU3Ec__AnonStorey15C_t3212335977, ___buttonInfo_1)); }
	inline DonateButton_t670753441 * get_buttonInfo_1() const { return ___buttonInfo_1; }
	inline DonateButton_t670753441 ** get_address_of_buttonInfo_1() { return &___buttonInfo_1; }
	inline void set_buttonInfo_1(DonateButton_t670753441 * value)
	{
		___buttonInfo_1 = value;
		Il2CppCodeGenWriteBarrier(&___buttonInfo_1, value);
	}

	inline static int32_t get_offset_of_sprite_2() { return static_cast<int32_t>(offsetof(U3CAddOpenIabButtonU3Ec__AnonStorey15C_t3212335977, ___sprite_2)); }
	inline String_t* get_sprite_2() const { return ___sprite_2; }
	inline String_t** get_address_of_sprite_2() { return &___sprite_2; }
	inline void set_sprite_2(String_t* value)
	{
		___sprite_2 = value;
		Il2CppCodeGenWriteBarrier(&___sprite_2, value);
	}

	inline static int32_t get_offset_of_initCell_3() { return static_cast<int32_t>(offsetof(U3CAddOpenIabButtonU3Ec__AnonStorey15C_t3212335977, ___initCell_3)); }
	inline Action_1_t2946927090 * get_initCell_3() const { return ___initCell_3; }
	inline Action_1_t2946927090 ** get_address_of_initCell_3() { return &___initCell_3; }
	inline void set_initCell_3(Action_1_t2946927090 * value)
	{
		___initCell_3 = value;
		Il2CppCodeGenWriteBarrier(&___initCell_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_4() { return static_cast<int32_t>(offsetof(U3CAddOpenIabButtonU3Ec__AnonStorey15C_t3212335977, ___U3CU3Ef__this_4)); }
	inline TableButtons_t1868573683 * get_U3CU3Ef__this_4() const { return ___U3CU3Ef__this_4; }
	inline TableButtons_t1868573683 ** get_address_of_U3CU3Ef__this_4() { return &___U3CU3Ef__this_4; }
	inline void set_U3CU3Ef__this_4(TableButtons_t1868573683 * value)
	{
		___U3CU3Ef__this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
