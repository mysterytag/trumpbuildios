﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.TimeoutFrameObservable`1/TimeoutFrame/TimeoutFrameTick<System.Object>
struct TimeoutFrameTick_t1345043194;
// UniRx.Operators.TimeoutFrameObservable`1/TimeoutFrame<System.Object>
struct TimeoutFrame_t2437429061;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.TimeoutFrameObservable`1/TimeoutFrame/TimeoutFrameTick<System.Object>::.ctor(UniRx.Operators.TimeoutFrameObservable`1/TimeoutFrame<T>,System.UInt64)
extern "C"  void TimeoutFrameTick__ctor_m2057949826_gshared (TimeoutFrameTick_t1345043194 * __this, TimeoutFrame_t2437429061 * ___parent0, uint64_t ___timerId1, const MethodInfo* method);
#define TimeoutFrameTick__ctor_m2057949826(__this, ___parent0, ___timerId1, method) ((  void (*) (TimeoutFrameTick_t1345043194 *, TimeoutFrame_t2437429061 *, uint64_t, const MethodInfo*))TimeoutFrameTick__ctor_m2057949826_gshared)(__this, ___parent0, ___timerId1, method)
// System.Void UniRx.Operators.TimeoutFrameObservable`1/TimeoutFrame/TimeoutFrameTick<System.Object>::OnCompleted()
extern "C"  void TimeoutFrameTick_OnCompleted_m872066233_gshared (TimeoutFrameTick_t1345043194 * __this, const MethodInfo* method);
#define TimeoutFrameTick_OnCompleted_m872066233(__this, method) ((  void (*) (TimeoutFrameTick_t1345043194 *, const MethodInfo*))TimeoutFrameTick_OnCompleted_m872066233_gshared)(__this, method)
// System.Void UniRx.Operators.TimeoutFrameObservable`1/TimeoutFrame/TimeoutFrameTick<System.Object>::OnError(System.Exception)
extern "C"  void TimeoutFrameTick_OnError_m2394099814_gshared (TimeoutFrameTick_t1345043194 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define TimeoutFrameTick_OnError_m2394099814(__this, ___error0, method) ((  void (*) (TimeoutFrameTick_t1345043194 *, Exception_t1967233988 *, const MethodInfo*))TimeoutFrameTick_OnError_m2394099814_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.TimeoutFrameObservable`1/TimeoutFrame/TimeoutFrameTick<System.Object>::OnNext(System.Int64)
extern "C"  void TimeoutFrameTick_OnNext_m3132947769_gshared (TimeoutFrameTick_t1345043194 * __this, int64_t ____0, const MethodInfo* method);
#define TimeoutFrameTick_OnNext_m3132947769(__this, ____0, method) ((  void (*) (TimeoutFrameTick_t1345043194 *, int64_t, const MethodInfo*))TimeoutFrameTick_OnNext_m3132947769_gshared)(__this, ____0, method)
