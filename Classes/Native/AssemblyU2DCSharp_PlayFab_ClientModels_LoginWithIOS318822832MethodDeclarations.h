﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.LoginWithIOSDeviceIDRequest
struct LoginWithIOSDeviceIDRequest_t318822832;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen3097043249.h"

// System.Void PlayFab.ClientModels.LoginWithIOSDeviceIDRequest::.ctor()
extern "C"  void LoginWithIOSDeviceIDRequest__ctor_m1121702649 (LoginWithIOSDeviceIDRequest_t318822832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LoginWithIOSDeviceIDRequest::get_TitleId()
extern "C"  String_t* LoginWithIOSDeviceIDRequest_get_TitleId_m2455060542 (LoginWithIOSDeviceIDRequest_t318822832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LoginWithIOSDeviceIDRequest::set_TitleId(System.String)
extern "C"  void LoginWithIOSDeviceIDRequest_set_TitleId_m2617316181 (LoginWithIOSDeviceIDRequest_t318822832 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LoginWithIOSDeviceIDRequest::get_DeviceId()
extern "C"  String_t* LoginWithIOSDeviceIDRequest_get_DeviceId_m1372528040 (LoginWithIOSDeviceIDRequest_t318822832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LoginWithIOSDeviceIDRequest::set_DeviceId(System.String)
extern "C"  void LoginWithIOSDeviceIDRequest_set_DeviceId_m2103184553 (LoginWithIOSDeviceIDRequest_t318822832 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LoginWithIOSDeviceIDRequest::get_OS()
extern "C"  String_t* LoginWithIOSDeviceIDRequest_get_OS_m1339128091 (LoginWithIOSDeviceIDRequest_t318822832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LoginWithIOSDeviceIDRequest::set_OS(System.String)
extern "C"  void LoginWithIOSDeviceIDRequest_set_OS_m2078329686 (LoginWithIOSDeviceIDRequest_t318822832 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LoginWithIOSDeviceIDRequest::get_DeviceModel()
extern "C"  String_t* LoginWithIOSDeviceIDRequest_get_DeviceModel_m521453598 (LoginWithIOSDeviceIDRequest_t318822832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LoginWithIOSDeviceIDRequest::set_DeviceModel(System.String)
extern "C"  void LoginWithIOSDeviceIDRequest_set_DeviceModel_m372379381 (LoginWithIOSDeviceIDRequest_t318822832 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.LoginWithIOSDeviceIDRequest::get_CreateAccount()
extern "C"  Nullable_1_t3097043249  LoginWithIOSDeviceIDRequest_get_CreateAccount_m3694256342 (LoginWithIOSDeviceIDRequest_t318822832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LoginWithIOSDeviceIDRequest::set_CreateAccount(System.Nullable`1<System.Boolean>)
extern "C"  void LoginWithIOSDeviceIDRequest_set_CreateAccount_m1414575869 (LoginWithIOSDeviceIDRequest_t318822832 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
