﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type
struct Type_t;
// System.Collections.Generic.IEnumerator`1<System.Type>
struct IEnumerator_1_t4262336383;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.TypeExtensions/<GetParentTypes>c__Iterator3E
struct  U3CGetParentTypesU3Ec__Iterator3E_t1503887130  : public Il2CppObject
{
public:
	// System.Type ModestTree.TypeExtensions/<GetParentTypes>c__Iterator3E::type
	Type_t * ___type_0;
	// System.Collections.Generic.IEnumerator`1<System.Type> ModestTree.TypeExtensions/<GetParentTypes>c__Iterator3E::<$s_243>__0
	Il2CppObject* ___U3CU24s_243U3E__0_1;
	// System.Type ModestTree.TypeExtensions/<GetParentTypes>c__Iterator3E::<ancestor>__1
	Type_t * ___U3CancestorU3E__1_2;
	// System.Int32 ModestTree.TypeExtensions/<GetParentTypes>c__Iterator3E::$PC
	int32_t ___U24PC_3;
	// System.Type ModestTree.TypeExtensions/<GetParentTypes>c__Iterator3E::$current
	Type_t * ___U24current_4;
	// System.Type ModestTree.TypeExtensions/<GetParentTypes>c__Iterator3E::<$>type
	Type_t * ___U3CU24U3Etype_5;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(U3CGetParentTypesU3Ec__Iterator3E_t1503887130, ___type_0)); }
	inline Type_t * get_type_0() const { return ___type_0; }
	inline Type_t ** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(Type_t * value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier(&___type_0, value);
	}

	inline static int32_t get_offset_of_U3CU24s_243U3E__0_1() { return static_cast<int32_t>(offsetof(U3CGetParentTypesU3Ec__Iterator3E_t1503887130, ___U3CU24s_243U3E__0_1)); }
	inline Il2CppObject* get_U3CU24s_243U3E__0_1() const { return ___U3CU24s_243U3E__0_1; }
	inline Il2CppObject** get_address_of_U3CU24s_243U3E__0_1() { return &___U3CU24s_243U3E__0_1; }
	inline void set_U3CU24s_243U3E__0_1(Il2CppObject* value)
	{
		___U3CU24s_243U3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_243U3E__0_1, value);
	}

	inline static int32_t get_offset_of_U3CancestorU3E__1_2() { return static_cast<int32_t>(offsetof(U3CGetParentTypesU3Ec__Iterator3E_t1503887130, ___U3CancestorU3E__1_2)); }
	inline Type_t * get_U3CancestorU3E__1_2() const { return ___U3CancestorU3E__1_2; }
	inline Type_t ** get_address_of_U3CancestorU3E__1_2() { return &___U3CancestorU3E__1_2; }
	inline void set_U3CancestorU3E__1_2(Type_t * value)
	{
		___U3CancestorU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CancestorU3E__1_2, value);
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CGetParentTypesU3Ec__Iterator3E_t1503887130, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CGetParentTypesU3Ec__Iterator3E_t1503887130, ___U24current_4)); }
	inline Type_t * get_U24current_4() const { return ___U24current_4; }
	inline Type_t ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Type_t * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Etype_5() { return static_cast<int32_t>(offsetof(U3CGetParentTypesU3Ec__Iterator3E_t1503887130, ___U3CU24U3Etype_5)); }
	inline Type_t * get_U3CU24U3Etype_5() const { return ___U3CU24U3Etype_5; }
	inline Type_t ** get_address_of_U3CU24U3Etype_5() { return &___U3CU24U3Etype_5; }
	inline void set_U3CU24U3Etype_5(Type_t * value)
	{
		___U3CU24U3Etype_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Etype_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
