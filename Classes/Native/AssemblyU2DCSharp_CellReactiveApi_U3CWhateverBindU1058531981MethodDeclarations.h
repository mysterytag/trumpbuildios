﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/<WhateverBind>c__AnonStoreyFE`1<System.Object>
struct U3CWhateverBindU3Ec__AnonStoreyFE_1_t1058531981;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void CellReactiveApi/<WhateverBind>c__AnonStoreyFE`1<System.Object>::.ctor()
extern "C"  void U3CWhateverBindU3Ec__AnonStoreyFE_1__ctor_m3541360995_gshared (U3CWhateverBindU3Ec__AnonStoreyFE_1_t1058531981 * __this, const MethodInfo* method);
#define U3CWhateverBindU3Ec__AnonStoreyFE_1__ctor_m3541360995(__this, method) ((  void (*) (U3CWhateverBindU3Ec__AnonStoreyFE_1_t1058531981 *, const MethodInfo*))U3CWhateverBindU3Ec__AnonStoreyFE_1__ctor_m3541360995_gshared)(__this, method)
// System.Void CellReactiveApi/<WhateverBind>c__AnonStoreyFE`1<System.Object>::<>m__174(T)
extern "C"  void U3CWhateverBindU3Ec__AnonStoreyFE_1_U3CU3Em__174_m4265923226_gshared (U3CWhateverBindU3Ec__AnonStoreyFE_1_t1058531981 * __this, Il2CppObject * ___val0, const MethodInfo* method);
#define U3CWhateverBindU3Ec__AnonStoreyFE_1_U3CU3Em__174_m4265923226(__this, ___val0, method) ((  void (*) (U3CWhateverBindU3Ec__AnonStoreyFE_1_t1058531981 *, Il2CppObject *, const MethodInfo*))U3CWhateverBindU3Ec__AnonStoreyFE_1_U3CU3Em__174_m4265923226_gshared)(__this, ___val0, method)
