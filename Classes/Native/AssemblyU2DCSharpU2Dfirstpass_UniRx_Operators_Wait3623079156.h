﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t2399218676;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_TimeSpan763862892.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.Wait`1<System.Object>
struct  Wait_1_t3623079156  : public Il2CppObject
{
public:
	// UniRx.IObservable`1<T> UniRx.Operators.Wait`1::source
	Il2CppObject* ___source_1;
	// System.TimeSpan UniRx.Operators.Wait`1::timeout
	TimeSpan_t763862892  ___timeout_2;
	// System.Threading.ManualResetEvent UniRx.Operators.Wait`1::semaphore
	ManualResetEvent_t2399218676 * ___semaphore_3;
	// System.Boolean UniRx.Operators.Wait`1::seenValue
	bool ___seenValue_4;
	// T UniRx.Operators.Wait`1::value
	Il2CppObject * ___value_5;
	// System.Exception UniRx.Operators.Wait`1::ex
	Exception_t1967233988 * ___ex_6;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(Wait_1_t3623079156, ___source_1)); }
	inline Il2CppObject* get_source_1() const { return ___source_1; }
	inline Il2CppObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(Il2CppObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier(&___source_1, value);
	}

	inline static int32_t get_offset_of_timeout_2() { return static_cast<int32_t>(offsetof(Wait_1_t3623079156, ___timeout_2)); }
	inline TimeSpan_t763862892  get_timeout_2() const { return ___timeout_2; }
	inline TimeSpan_t763862892 * get_address_of_timeout_2() { return &___timeout_2; }
	inline void set_timeout_2(TimeSpan_t763862892  value)
	{
		___timeout_2 = value;
	}

	inline static int32_t get_offset_of_semaphore_3() { return static_cast<int32_t>(offsetof(Wait_1_t3623079156, ___semaphore_3)); }
	inline ManualResetEvent_t2399218676 * get_semaphore_3() const { return ___semaphore_3; }
	inline ManualResetEvent_t2399218676 ** get_address_of_semaphore_3() { return &___semaphore_3; }
	inline void set_semaphore_3(ManualResetEvent_t2399218676 * value)
	{
		___semaphore_3 = value;
		Il2CppCodeGenWriteBarrier(&___semaphore_3, value);
	}

	inline static int32_t get_offset_of_seenValue_4() { return static_cast<int32_t>(offsetof(Wait_1_t3623079156, ___seenValue_4)); }
	inline bool get_seenValue_4() const { return ___seenValue_4; }
	inline bool* get_address_of_seenValue_4() { return &___seenValue_4; }
	inline void set_seenValue_4(bool value)
	{
		___seenValue_4 = value;
	}

	inline static int32_t get_offset_of_value_5() { return static_cast<int32_t>(offsetof(Wait_1_t3623079156, ___value_5)); }
	inline Il2CppObject * get_value_5() const { return ___value_5; }
	inline Il2CppObject ** get_address_of_value_5() { return &___value_5; }
	inline void set_value_5(Il2CppObject * value)
	{
		___value_5 = value;
		Il2CppCodeGenWriteBarrier(&___value_5, value);
	}

	inline static int32_t get_offset_of_ex_6() { return static_cast<int32_t>(offsetof(Wait_1_t3623079156, ___ex_6)); }
	inline Exception_t1967233988 * get_ex_6() const { return ___ex_6; }
	inline Exception_t1967233988 ** get_address_of_ex_6() { return &___ex_6; }
	inline void set_ex_6(Exception_t1967233988 * value)
	{
		___ex_6 = value;
		Il2CppCodeGenWriteBarrier(&___ex_6, value);
	}
};

struct Wait_1_t3623079156_StaticFields
{
public:
	// System.TimeSpan UniRx.Operators.Wait`1::InfiniteTimeSpan
	TimeSpan_t763862892  ___InfiniteTimeSpan_0;

public:
	inline static int32_t get_offset_of_InfiniteTimeSpan_0() { return static_cast<int32_t>(offsetof(Wait_1_t3623079156_StaticFields, ___InfiniteTimeSpan_0)); }
	inline TimeSpan_t763862892  get_InfiniteTimeSpan_0() const { return ___InfiniteTimeSpan_0; }
	inline TimeSpan_t763862892 * get_address_of_InfiniteTimeSpan_0() { return &___InfiniteTimeSpan_0; }
	inline void set_InfiniteTimeSpan_0(TimeSpan_t763862892  value)
	{
		___InfiniteTimeSpan_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
