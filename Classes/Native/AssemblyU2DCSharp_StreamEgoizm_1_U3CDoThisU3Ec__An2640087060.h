﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// StreamEgoizm`1<System.Object>
struct StreamEgoizm_1_t3012409721;

#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23615068783.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StreamEgoizm`1/<DoThis>c__AnonStorey12B<System.Object>
struct  U3CDoThisU3Ec__AnonStorey12B_t2640087060  : public Il2CppObject
{
public:
	// System.Collections.Generic.KeyValuePair`2<System.IDisposable,System.Action`1<T>> StreamEgoizm`1/<DoThis>c__AnonStorey12B::tuple
	KeyValuePair_2_t3615068783  ___tuple_0;
	// StreamEgoizm`1<T> StreamEgoizm`1/<DoThis>c__AnonStorey12B::<>f__this
	StreamEgoizm_1_t3012409721 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_tuple_0() { return static_cast<int32_t>(offsetof(U3CDoThisU3Ec__AnonStorey12B_t2640087060, ___tuple_0)); }
	inline KeyValuePair_2_t3615068783  get_tuple_0() const { return ___tuple_0; }
	inline KeyValuePair_2_t3615068783 * get_address_of_tuple_0() { return &___tuple_0; }
	inline void set_tuple_0(KeyValuePair_2_t3615068783  value)
	{
		___tuple_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CDoThisU3Ec__AnonStorey12B_t2640087060, ___U3CU3Ef__this_1)); }
	inline StreamEgoizm_1_t3012409721 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline StreamEgoizm_1_t3012409721 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(StreamEgoizm_1_t3012409721 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
