﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GlobalStat/<TwentyRevenueListCheck>c__Iterator19
struct U3CTwentyRevenueListCheckU3Ec__Iterator19_t1382458870;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GlobalStat/<TwentyRevenueListCheck>c__Iterator19::.ctor()
extern "C"  void U3CTwentyRevenueListCheckU3Ec__Iterator19__ctor_m1987535293 (U3CTwentyRevenueListCheckU3Ec__Iterator19_t1382458870 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GlobalStat/<TwentyRevenueListCheck>c__Iterator19::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTwentyRevenueListCheckU3Ec__Iterator19_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1525238335 (U3CTwentyRevenueListCheckU3Ec__Iterator19_t1382458870 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GlobalStat/<TwentyRevenueListCheck>c__Iterator19::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTwentyRevenueListCheckU3Ec__Iterator19_System_Collections_IEnumerator_get_Current_m2594024403 (U3CTwentyRevenueListCheckU3Ec__Iterator19_t1382458870 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GlobalStat/<TwentyRevenueListCheck>c__Iterator19::MoveNext()
extern "C"  bool U3CTwentyRevenueListCheckU3Ec__Iterator19_MoveNext_m2510644991 (U3CTwentyRevenueListCheckU3Ec__Iterator19_t1382458870 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalStat/<TwentyRevenueListCheck>c__Iterator19::Dispose()
extern "C"  void U3CTwentyRevenueListCheckU3Ec__Iterator19_Dispose_m2954220218 (U3CTwentyRevenueListCheckU3Ec__Iterator19_t1382458870 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalStat/<TwentyRevenueListCheck>c__Iterator19::Reset()
extern "C"  void U3CTwentyRevenueListCheckU3Ec__Iterator19_Reset_m3928935530 (U3CTwentyRevenueListCheckU3Ec__Iterator19_t1382458870 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GlobalStat/<TwentyRevenueListCheck>c__Iterator19::<>m__113(System.Int64)
extern "C"  bool U3CTwentyRevenueListCheckU3Ec__Iterator19_U3CU3Em__113_m570086325 (Il2CppObject * __this /* static, unused */, int64_t ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
