﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType>
struct DefaultComparer_t2607132919;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GameAnalyticsSDK_GA_ServerFieldT3269179188.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType>::.ctor()
extern "C"  void DefaultComparer__ctor_m2836314128_gshared (DefaultComparer_t2607132919 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2836314128(__this, method) ((  void (*) (DefaultComparer_t2607132919 *, const MethodInfo*))DefaultComparer__ctor_m2836314128_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2272118243_gshared (DefaultComparer_t2607132919 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m2272118243(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t2607132919 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m2272118243_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1544383653_gshared (DefaultComparer_t2607132919 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m1544383653(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t2607132919 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m1544383653_gshared)(__this, ___x0, ___y1, method)
