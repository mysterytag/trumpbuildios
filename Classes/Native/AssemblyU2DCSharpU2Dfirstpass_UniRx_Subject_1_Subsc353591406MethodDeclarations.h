﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1/Subscription<UnityEngine.NetworkMessageInfo>
struct Subscription_t353591406;
// UniRx.Subject`1<UnityEngine.NetworkMessageInfo>
struct Subject_1_t217412208;
// UniRx.IObserver`1<UnityEngine.NetworkMessageInfo>
struct IObserver_1_t491376491;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Subject`1/Subscription<UnityEngine.NetworkMessageInfo>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern "C"  void Subscription__ctor_m1995492437_gshared (Subscription_t353591406 * __this, Subject_1_t217412208 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method);
#define Subscription__ctor_m1995492437(__this, ___parent0, ___unsubscribeTarget1, method) ((  void (*) (Subscription_t353591406 *, Subject_1_t217412208 *, Il2CppObject*, const MethodInfo*))Subscription__ctor_m1995492437_gshared)(__this, ___parent0, ___unsubscribeTarget1, method)
// System.Void UniRx.Subject`1/Subscription<UnityEngine.NetworkMessageInfo>::Dispose()
extern "C"  void Subscription_Dispose_m3474105473_gshared (Subscription_t353591406 * __this, const MethodInfo* method);
#define Subscription_Dispose_m3474105473(__this, method) ((  void (*) (Subscription_t353591406 *, const MethodInfo*))Subscription_Dispose_m3474105473_gshared)(__this, method)
