﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectObservable`2<System.Int32,UnityEngine.Color>
struct SelectObservable_2_t3125378242;
// UniRx.IObservable`1<System.Int32>
struct IObservable_1_t2606213151;
// System.Func`2<System.Int32,UnityEngine.Color>
struct Func_2_t390344745;
// System.Func`3<System.Int32,System.Int32,UnityEngine.Color>
struct Func_3_t3578830837;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UnityEngine.Color>
struct IObserver_1_t3800174663;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.SelectObservable`2<System.Int32,UnityEngine.Color>::.ctor(UniRx.IObservable`1<T>,System.Func`2<T,TR>)
extern "C"  void SelectObservable_2__ctor_m3316517214_gshared (SelectObservable_2_t3125378242 * __this, Il2CppObject* ___source0, Func_2_t390344745 * ___selector1, const MethodInfo* method);
#define SelectObservable_2__ctor_m3316517214(__this, ___source0, ___selector1, method) ((  void (*) (SelectObservable_2_t3125378242 *, Il2CppObject*, Func_2_t390344745 *, const MethodInfo*))SelectObservable_2__ctor_m3316517214_gshared)(__this, ___source0, ___selector1, method)
// System.Void UniRx.Operators.SelectObservable`2<System.Int32,UnityEngine.Color>::.ctor(UniRx.IObservable`1<T>,System.Func`3<T,System.Int32,TR>)
extern "C"  void SelectObservable_2__ctor_m1941539090_gshared (SelectObservable_2_t3125378242 * __this, Il2CppObject* ___source0, Func_3_t3578830837 * ___selector1, const MethodInfo* method);
#define SelectObservable_2__ctor_m1941539090(__this, ___source0, ___selector1, method) ((  void (*) (SelectObservable_2_t3125378242 *, Il2CppObject*, Func_3_t3578830837 *, const MethodInfo*))SelectObservable_2__ctor_m1941539090_gshared)(__this, ___source0, ___selector1, method)
// System.IDisposable UniRx.Operators.SelectObservable`2<System.Int32,UnityEngine.Color>::SubscribeCore(UniRx.IObserver`1<TR>,System.IDisposable)
extern "C"  Il2CppObject * SelectObservable_2_SubscribeCore_m3166000742_gshared (SelectObservable_2_t3125378242 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define SelectObservable_2_SubscribeCore_m3166000742(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (SelectObservable_2_t3125378242 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SelectObservable_2_SubscribeCore_m3166000742_gshared)(__this, ___observer0, ___cancel1, method)
