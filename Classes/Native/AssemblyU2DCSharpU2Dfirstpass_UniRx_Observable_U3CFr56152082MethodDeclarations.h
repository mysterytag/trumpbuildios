﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/<FromAsyncPattern>c__AnonStorey41`2/<FromAsyncPattern>c__AnonStorey42`2<System.Object,UniRx.Unit>
struct U3CFromAsyncPatternU3Ec__AnonStorey42_2_t56152082;
// System.IAsyncResult
struct IAsyncResult_t537683269;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Observable/<FromAsyncPattern>c__AnonStorey41`2/<FromAsyncPattern>c__AnonStorey42`2<System.Object,UniRx.Unit>::.ctor()
extern "C"  void U3CFromAsyncPatternU3Ec__AnonStorey42_2__ctor_m190503573_gshared (U3CFromAsyncPatternU3Ec__AnonStorey42_2_t56152082 * __this, const MethodInfo* method);
#define U3CFromAsyncPatternU3Ec__AnonStorey42_2__ctor_m190503573(__this, method) ((  void (*) (U3CFromAsyncPatternU3Ec__AnonStorey42_2_t56152082 *, const MethodInfo*))U3CFromAsyncPatternU3Ec__AnonStorey42_2__ctor_m190503573_gshared)(__this, method)
// System.Void UniRx.Observable/<FromAsyncPattern>c__AnonStorey41`2/<FromAsyncPattern>c__AnonStorey42`2<System.Object,UniRx.Unit>::<>m__66(System.IAsyncResult)
extern "C"  void U3CFromAsyncPatternU3Ec__AnonStorey42_2_U3CU3Em__66_m789636677_gshared (U3CFromAsyncPatternU3Ec__AnonStorey42_2_t56152082 * __this, Il2CppObject * ___iar0, const MethodInfo* method);
#define U3CFromAsyncPatternU3Ec__AnonStorey42_2_U3CU3Em__66_m789636677(__this, ___iar0, method) ((  void (*) (U3CFromAsyncPatternU3Ec__AnonStorey42_2_t56152082 *, Il2CppObject *, const MethodInfo*))U3CFromAsyncPatternU3Ec__AnonStorey42_2_U3CU3Em__66_m789636677_gshared)(__this, ___iar0, method)
