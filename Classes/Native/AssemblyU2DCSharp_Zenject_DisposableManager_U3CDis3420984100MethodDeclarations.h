﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.DisposableManager/<DisposableManager>c__AnonStorey18E
struct U3CDisposableManagerU3Ec__AnonStorey18E_t3420984100;
// ModestTree.Util.Tuple`2<System.Type,System.Int32>
struct Tuple_2_t3719549051;

#include "codegen/il2cpp-codegen.h"

// System.Void Zenject.DisposableManager/<DisposableManager>c__AnonStorey18E::.ctor()
extern "C"  void U3CDisposableManagerU3Ec__AnonStorey18E__ctor_m3884414312 (U3CDisposableManagerU3Ec__AnonStorey18E_t3420984100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.DisposableManager/<DisposableManager>c__AnonStorey18E::<>m__2D5(ModestTree.Util.Tuple`2<System.Type,System.Int32>)
extern "C"  bool U3CDisposableManagerU3Ec__AnonStorey18E_U3CU3Em__2D5_m1647669368 (U3CDisposableManagerU3Ec__AnonStorey18E_t3420984100 * __this, Tuple_2_t3719549051 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
