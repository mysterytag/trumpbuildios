﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.Exception
struct Exception_t1967233988;
// System.IDisposable
struct IDisposable_t1628921374;

#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/<WrapEnumeratorYieldValue>c__Iterator10`1<System.Object>
struct  U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_t896010658  : public Il2CppObject
{
public:
	// System.Boolean UniRx.Observable/<WrapEnumeratorYieldValue>c__Iterator10`1::<hasNext>__0
	bool ___U3ChasNextU3E__0_0;
	// System.Object UniRx.Observable/<WrapEnumeratorYieldValue>c__Iterator10`1::<current>__1
	Il2CppObject * ___U3CcurrentU3E__1_1;
	// System.Boolean UniRx.Observable/<WrapEnumeratorYieldValue>c__Iterator10`1::<raisedError>__2
	bool ___U3CraisedErrorU3E__2_2;
	// System.Collections.IEnumerator UniRx.Observable/<WrapEnumeratorYieldValue>c__Iterator10`1::enumerator
	Il2CppObject * ___enumerator_3;
	// UniRx.IObserver`1<T> UniRx.Observable/<WrapEnumeratorYieldValue>c__Iterator10`1::observer
	Il2CppObject* ___observer_4;
	// System.Exception UniRx.Observable/<WrapEnumeratorYieldValue>c__Iterator10`1::<ex>__3
	Exception_t1967233988 * ___U3CexU3E__3_5;
	// System.IDisposable UniRx.Observable/<WrapEnumeratorYieldValue>c__Iterator10`1::<d>__4
	Il2CppObject * ___U3CdU3E__4_6;
	// System.Boolean UniRx.Observable/<WrapEnumeratorYieldValue>c__Iterator10`1::nullAsNextUpdate
	bool ___nullAsNextUpdate_7;
	// System.IDisposable UniRx.Observable/<WrapEnumeratorYieldValue>c__Iterator10`1::<d>__5
	Il2CppObject * ___U3CdU3E__5_8;
	// UniRx.CancellationToken UniRx.Observable/<WrapEnumeratorYieldValue>c__Iterator10`1::cancellationToken
	CancellationToken_t1439151560  ___cancellationToken_9;
	// System.IDisposable UniRx.Observable/<WrapEnumeratorYieldValue>c__Iterator10`1::<d>__6
	Il2CppObject * ___U3CdU3E__6_10;
	// System.Int32 UniRx.Observable/<WrapEnumeratorYieldValue>c__Iterator10`1::$PC
	int32_t ___U24PC_11;
	// System.Object UniRx.Observable/<WrapEnumeratorYieldValue>c__Iterator10`1::$current
	Il2CppObject * ___U24current_12;
	// System.Collections.IEnumerator UniRx.Observable/<WrapEnumeratorYieldValue>c__Iterator10`1::<$>enumerator
	Il2CppObject * ___U3CU24U3Eenumerator_13;
	// UniRx.IObserver`1<T> UniRx.Observable/<WrapEnumeratorYieldValue>c__Iterator10`1::<$>observer
	Il2CppObject* ___U3CU24U3Eobserver_14;
	// System.Boolean UniRx.Observable/<WrapEnumeratorYieldValue>c__Iterator10`1::<$>nullAsNextUpdate
	bool ___U3CU24U3EnullAsNextUpdate_15;
	// UniRx.CancellationToken UniRx.Observable/<WrapEnumeratorYieldValue>c__Iterator10`1::<$>cancellationToken
	CancellationToken_t1439151560  ___U3CU24U3EcancellationToken_16;

public:
	inline static int32_t get_offset_of_U3ChasNextU3E__0_0() { return static_cast<int32_t>(offsetof(U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_t896010658, ___U3ChasNextU3E__0_0)); }
	inline bool get_U3ChasNextU3E__0_0() const { return ___U3ChasNextU3E__0_0; }
	inline bool* get_address_of_U3ChasNextU3E__0_0() { return &___U3ChasNextU3E__0_0; }
	inline void set_U3ChasNextU3E__0_0(bool value)
	{
		___U3ChasNextU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CcurrentU3E__1_1() { return static_cast<int32_t>(offsetof(U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_t896010658, ___U3CcurrentU3E__1_1)); }
	inline Il2CppObject * get_U3CcurrentU3E__1_1() const { return ___U3CcurrentU3E__1_1; }
	inline Il2CppObject ** get_address_of_U3CcurrentU3E__1_1() { return &___U3CcurrentU3E__1_1; }
	inline void set_U3CcurrentU3E__1_1(Il2CppObject * value)
	{
		___U3CcurrentU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcurrentU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CraisedErrorU3E__2_2() { return static_cast<int32_t>(offsetof(U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_t896010658, ___U3CraisedErrorU3E__2_2)); }
	inline bool get_U3CraisedErrorU3E__2_2() const { return ___U3CraisedErrorU3E__2_2; }
	inline bool* get_address_of_U3CraisedErrorU3E__2_2() { return &___U3CraisedErrorU3E__2_2; }
	inline void set_U3CraisedErrorU3E__2_2(bool value)
	{
		___U3CraisedErrorU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_enumerator_3() { return static_cast<int32_t>(offsetof(U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_t896010658, ___enumerator_3)); }
	inline Il2CppObject * get_enumerator_3() const { return ___enumerator_3; }
	inline Il2CppObject ** get_address_of_enumerator_3() { return &___enumerator_3; }
	inline void set_enumerator_3(Il2CppObject * value)
	{
		___enumerator_3 = value;
		Il2CppCodeGenWriteBarrier(&___enumerator_3, value);
	}

	inline static int32_t get_offset_of_observer_4() { return static_cast<int32_t>(offsetof(U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_t896010658, ___observer_4)); }
	inline Il2CppObject* get_observer_4() const { return ___observer_4; }
	inline Il2CppObject** get_address_of_observer_4() { return &___observer_4; }
	inline void set_observer_4(Il2CppObject* value)
	{
		___observer_4 = value;
		Il2CppCodeGenWriteBarrier(&___observer_4, value);
	}

	inline static int32_t get_offset_of_U3CexU3E__3_5() { return static_cast<int32_t>(offsetof(U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_t896010658, ___U3CexU3E__3_5)); }
	inline Exception_t1967233988 * get_U3CexU3E__3_5() const { return ___U3CexU3E__3_5; }
	inline Exception_t1967233988 ** get_address_of_U3CexU3E__3_5() { return &___U3CexU3E__3_5; }
	inline void set_U3CexU3E__3_5(Exception_t1967233988 * value)
	{
		___U3CexU3E__3_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CexU3E__3_5, value);
	}

	inline static int32_t get_offset_of_U3CdU3E__4_6() { return static_cast<int32_t>(offsetof(U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_t896010658, ___U3CdU3E__4_6)); }
	inline Il2CppObject * get_U3CdU3E__4_6() const { return ___U3CdU3E__4_6; }
	inline Il2CppObject ** get_address_of_U3CdU3E__4_6() { return &___U3CdU3E__4_6; }
	inline void set_U3CdU3E__4_6(Il2CppObject * value)
	{
		___U3CdU3E__4_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CdU3E__4_6, value);
	}

	inline static int32_t get_offset_of_nullAsNextUpdate_7() { return static_cast<int32_t>(offsetof(U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_t896010658, ___nullAsNextUpdate_7)); }
	inline bool get_nullAsNextUpdate_7() const { return ___nullAsNextUpdate_7; }
	inline bool* get_address_of_nullAsNextUpdate_7() { return &___nullAsNextUpdate_7; }
	inline void set_nullAsNextUpdate_7(bool value)
	{
		___nullAsNextUpdate_7 = value;
	}

	inline static int32_t get_offset_of_U3CdU3E__5_8() { return static_cast<int32_t>(offsetof(U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_t896010658, ___U3CdU3E__5_8)); }
	inline Il2CppObject * get_U3CdU3E__5_8() const { return ___U3CdU3E__5_8; }
	inline Il2CppObject ** get_address_of_U3CdU3E__5_8() { return &___U3CdU3E__5_8; }
	inline void set_U3CdU3E__5_8(Il2CppObject * value)
	{
		___U3CdU3E__5_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CdU3E__5_8, value);
	}

	inline static int32_t get_offset_of_cancellationToken_9() { return static_cast<int32_t>(offsetof(U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_t896010658, ___cancellationToken_9)); }
	inline CancellationToken_t1439151560  get_cancellationToken_9() const { return ___cancellationToken_9; }
	inline CancellationToken_t1439151560 * get_address_of_cancellationToken_9() { return &___cancellationToken_9; }
	inline void set_cancellationToken_9(CancellationToken_t1439151560  value)
	{
		___cancellationToken_9 = value;
	}

	inline static int32_t get_offset_of_U3CdU3E__6_10() { return static_cast<int32_t>(offsetof(U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_t896010658, ___U3CdU3E__6_10)); }
	inline Il2CppObject * get_U3CdU3E__6_10() const { return ___U3CdU3E__6_10; }
	inline Il2CppObject ** get_address_of_U3CdU3E__6_10() { return &___U3CdU3E__6_10; }
	inline void set_U3CdU3E__6_10(Il2CppObject * value)
	{
		___U3CdU3E__6_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CdU3E__6_10, value);
	}

	inline static int32_t get_offset_of_U24PC_11() { return static_cast<int32_t>(offsetof(U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_t896010658, ___U24PC_11)); }
	inline int32_t get_U24PC_11() const { return ___U24PC_11; }
	inline int32_t* get_address_of_U24PC_11() { return &___U24PC_11; }
	inline void set_U24PC_11(int32_t value)
	{
		___U24PC_11 = value;
	}

	inline static int32_t get_offset_of_U24current_12() { return static_cast<int32_t>(offsetof(U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_t896010658, ___U24current_12)); }
	inline Il2CppObject * get_U24current_12() const { return ___U24current_12; }
	inline Il2CppObject ** get_address_of_U24current_12() { return &___U24current_12; }
	inline void set_U24current_12(Il2CppObject * value)
	{
		___U24current_12 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_12, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eenumerator_13() { return static_cast<int32_t>(offsetof(U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_t896010658, ___U3CU24U3Eenumerator_13)); }
	inline Il2CppObject * get_U3CU24U3Eenumerator_13() const { return ___U3CU24U3Eenumerator_13; }
	inline Il2CppObject ** get_address_of_U3CU24U3Eenumerator_13() { return &___U3CU24U3Eenumerator_13; }
	inline void set_U3CU24U3Eenumerator_13(Il2CppObject * value)
	{
		___U3CU24U3Eenumerator_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eenumerator_13, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eobserver_14() { return static_cast<int32_t>(offsetof(U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_t896010658, ___U3CU24U3Eobserver_14)); }
	inline Il2CppObject* get_U3CU24U3Eobserver_14() const { return ___U3CU24U3Eobserver_14; }
	inline Il2CppObject** get_address_of_U3CU24U3Eobserver_14() { return &___U3CU24U3Eobserver_14; }
	inline void set_U3CU24U3Eobserver_14(Il2CppObject* value)
	{
		___U3CU24U3Eobserver_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eobserver_14, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EnullAsNextUpdate_15() { return static_cast<int32_t>(offsetof(U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_t896010658, ___U3CU24U3EnullAsNextUpdate_15)); }
	inline bool get_U3CU24U3EnullAsNextUpdate_15() const { return ___U3CU24U3EnullAsNextUpdate_15; }
	inline bool* get_address_of_U3CU24U3EnullAsNextUpdate_15() { return &___U3CU24U3EnullAsNextUpdate_15; }
	inline void set_U3CU24U3EnullAsNextUpdate_15(bool value)
	{
		___U3CU24U3EnullAsNextUpdate_15 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3EcancellationToken_16() { return static_cast<int32_t>(offsetof(U3CWrapEnumeratorYieldValueU3Ec__Iterator10_1_t896010658, ___U3CU24U3EcancellationToken_16)); }
	inline CancellationToken_t1439151560  get_U3CU24U3EcancellationToken_16() const { return ___U3CU24U3EcancellationToken_16; }
	inline CancellationToken_t1439151560 * get_address_of_U3CU24U3EcancellationToken_16() { return &___U3CU24U3EcancellationToken_16; }
	inline void set_U3CU24U3EcancellationToken_16(CancellationToken_t1439151560  value)
	{
		___U3CU24U3EcancellationToken_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
