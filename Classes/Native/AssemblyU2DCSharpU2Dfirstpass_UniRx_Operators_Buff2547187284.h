﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.BufferObservable`2/Buffer<System.Object,System.Object>
struct Buffer_t3527656087;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.BufferObservable`2/Buffer/Buffer_<System.Object,System.Object>
struct  Buffer__t2547187284  : public Il2CppObject
{
public:
	// UniRx.Operators.BufferObservable`2/Buffer<TSource,TWindowBoundary> UniRx.Operators.BufferObservable`2/Buffer/Buffer_::parent
	Buffer_t3527656087 * ___parent_0;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(Buffer__t2547187284, ___parent_0)); }
	inline Buffer_t3527656087 * get_parent_0() const { return ___parent_0; }
	inline Buffer_t3527656087 ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(Buffer_t3527656087 * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier(&___parent_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
