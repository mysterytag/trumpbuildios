﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.Boolean>
struct Action_1_t359458046;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VkConnectorBase/<InviteFriend>c__AnonStoreyDC
struct  U3CInviteFriendU3Ec__AnonStoreyDC_t3048000427  : public Il2CppObject
{
public:
	// System.Action`1<System.Boolean> VkConnectorBase/<InviteFriend>c__AnonStoreyDC::succesfull
	Action_1_t359458046 * ___succesfull_0;

public:
	inline static int32_t get_offset_of_succesfull_0() { return static_cast<int32_t>(offsetof(U3CInviteFriendU3Ec__AnonStoreyDC_t3048000427, ___succesfull_0)); }
	inline Action_1_t359458046 * get_succesfull_0() const { return ___succesfull_0; }
	inline Action_1_t359458046 ** get_address_of_succesfull_0() { return &___succesfull_0; }
	inline void set_succesfull_0(Action_1_t359458046 * value)
	{
		___succesfull_0 = value;
		Il2CppCodeGenWriteBarrier(&___succesfull_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
