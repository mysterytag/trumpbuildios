﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.AddSharedGroupMembersResult
struct AddSharedGroupMembersResult_t3325132109;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.AddSharedGroupMembersResult::.ctor()
extern "C"  void AddSharedGroupMembersResult__ctor_m3116167164 (AddSharedGroupMembersResult_t3325132109 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
