﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.FromEventObservable`1/FromEvent<System.Object>
struct FromEvent_t2990734057;
// UniRx.Operators.FromEventObservable`1<System.Object>
struct FromEventObservable_1_t2790472066;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.FromEventObservable`1/FromEvent<System.Object>::.ctor(UniRx.Operators.FromEventObservable`1<TDelegate>,UniRx.IObserver`1<UniRx.Unit>)
extern "C"  void FromEvent__ctor_m3529457115_gshared (FromEvent_t2990734057 * __this, FromEventObservable_1_t2790472066 * ___parent0, Il2CppObject* ___observer1, const MethodInfo* method);
#define FromEvent__ctor_m3529457115(__this, ___parent0, ___observer1, method) ((  void (*) (FromEvent_t2990734057 *, FromEventObservable_1_t2790472066 *, Il2CppObject*, const MethodInfo*))FromEvent__ctor_m3529457115_gshared)(__this, ___parent0, ___observer1, method)
// System.Boolean UniRx.Operators.FromEventObservable`1/FromEvent<System.Object>::Register()
extern "C"  bool FromEvent_Register_m4289606306_gshared (FromEvent_t2990734057 * __this, const MethodInfo* method);
#define FromEvent_Register_m4289606306(__this, method) ((  bool (*) (FromEvent_t2990734057 *, const MethodInfo*))FromEvent_Register_m4289606306_gshared)(__this, method)
// System.Void UniRx.Operators.FromEventObservable`1/FromEvent<System.Object>::OnNext()
extern "C"  void FromEvent_OnNext_m2066540437_gshared (FromEvent_t2990734057 * __this, const MethodInfo* method);
#define FromEvent_OnNext_m2066540437(__this, method) ((  void (*) (FromEvent_t2990734057 *, const MethodInfo*))FromEvent_OnNext_m2066540437_gshared)(__this, method)
// System.Void UniRx.Operators.FromEventObservable`1/FromEvent<System.Object>::Dispose()
extern "C"  void FromEvent_Dispose_m1000792574_gshared (FromEvent_t2990734057 * __this, const MethodInfo* method);
#define FromEvent_Dispose_m1000792574(__this, method) ((  void (*) (FromEvent_t2990734057 *, const MethodInfo*))FromEvent_Dispose_m1000792574_gshared)(__this, method)
