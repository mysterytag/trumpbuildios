﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_DateTime339033936.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.VirtualCurrencyRechargeTime
struct  VirtualCurrencyRechargeTime_t1584308320  : public Il2CppObject
{
public:
	// System.Int32 PlayFab.ClientModels.VirtualCurrencyRechargeTime::<SecondsToRecharge>k__BackingField
	int32_t ___U3CSecondsToRechargeU3Ek__BackingField_0;
	// System.DateTime PlayFab.ClientModels.VirtualCurrencyRechargeTime::<RechargeTime>k__BackingField
	DateTime_t339033936  ___U3CRechargeTimeU3Ek__BackingField_1;
	// System.Int32 PlayFab.ClientModels.VirtualCurrencyRechargeTime::<RechargeMax>k__BackingField
	int32_t ___U3CRechargeMaxU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CSecondsToRechargeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(VirtualCurrencyRechargeTime_t1584308320, ___U3CSecondsToRechargeU3Ek__BackingField_0)); }
	inline int32_t get_U3CSecondsToRechargeU3Ek__BackingField_0() const { return ___U3CSecondsToRechargeU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CSecondsToRechargeU3Ek__BackingField_0() { return &___U3CSecondsToRechargeU3Ek__BackingField_0; }
	inline void set_U3CSecondsToRechargeU3Ek__BackingField_0(int32_t value)
	{
		___U3CSecondsToRechargeU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CRechargeTimeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(VirtualCurrencyRechargeTime_t1584308320, ___U3CRechargeTimeU3Ek__BackingField_1)); }
	inline DateTime_t339033936  get_U3CRechargeTimeU3Ek__BackingField_1() const { return ___U3CRechargeTimeU3Ek__BackingField_1; }
	inline DateTime_t339033936 * get_address_of_U3CRechargeTimeU3Ek__BackingField_1() { return &___U3CRechargeTimeU3Ek__BackingField_1; }
	inline void set_U3CRechargeTimeU3Ek__BackingField_1(DateTime_t339033936  value)
	{
		___U3CRechargeTimeU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CRechargeMaxU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(VirtualCurrencyRechargeTime_t1584308320, ___U3CRechargeMaxU3Ek__BackingField_2)); }
	inline int32_t get_U3CRechargeMaxU3Ek__BackingField_2() const { return ___U3CRechargeMaxU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CRechargeMaxU3Ek__BackingField_2() { return &___U3CRechargeMaxU3Ek__BackingField_2; }
	inline void set_U3CRechargeMaxU3Ek__BackingField_2(int32_t value)
	{
		___U3CRechargeMaxU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
