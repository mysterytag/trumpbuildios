﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.UpdateCharacterDataRequest
struct UpdateCharacterDataRequest_t3002197813;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen2611574664.h"

// System.Void PlayFab.ClientModels.UpdateCharacterDataRequest::.ctor()
extern "C"  void UpdateCharacterDataRequest__ctor_m723503144 (UpdateCharacterDataRequest_t3002197813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.UpdateCharacterDataRequest::get_CharacterId()
extern "C"  String_t* UpdateCharacterDataRequest_get_CharacterId_m2768037988 (UpdateCharacterDataRequest_t3002197813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UpdateCharacterDataRequest::set_CharacterId(System.String)
extern "C"  void UpdateCharacterDataRequest_set_CharacterId_m3397554453 (UpdateCharacterDataRequest_t3002197813 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PlayFab.ClientModels.UpdateCharacterDataRequest::get_Data()
extern "C"  Dictionary_2_t2606186806 * UpdateCharacterDataRequest_get_Data_m2830515745 (UpdateCharacterDataRequest_t3002197813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UpdateCharacterDataRequest::set_Data(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void UpdateCharacterDataRequest_set_Data_m34486260 (UpdateCharacterDataRequest_t3002197813 * __this, Dictionary_2_t2606186806 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.UpdateCharacterDataRequest::get_KeysToRemove()
extern "C"  List_1_t1765447871 * UpdateCharacterDataRequest_get_KeysToRemove_m325457991 (UpdateCharacterDataRequest_t3002197813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UpdateCharacterDataRequest::set_KeysToRemove(System.Collections.Generic.List`1<System.String>)
extern "C"  void UpdateCharacterDataRequest_set_KeysToRemove_m4177904000 (UpdateCharacterDataRequest_t3002197813 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<PlayFab.ClientModels.UserDataPermission> PlayFab.ClientModels.UpdateCharacterDataRequest::get_Permission()
extern "C"  Nullable_1_t2611574664  UpdateCharacterDataRequest_get_Permission_m185268094 (UpdateCharacterDataRequest_t3002197813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UpdateCharacterDataRequest::set_Permission(System.Nullable`1<PlayFab.ClientModels.UserDataPermission>)
extern "C"  void UpdateCharacterDataRequest_set_Permission_m723143905 (UpdateCharacterDataRequest_t3002197813 * __this, Nullable_1_t2611574664  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
