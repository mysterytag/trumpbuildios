﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PlayFab.ClientModels.TradeInfo
struct TradeInfo_t1933812770;

#include "AssemblyU2DCSharp_PlayFab_Internal_PlayFabResultCom379512675.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.OpenTradeResponse
struct  OpenTradeResponse_t3658687371  : public PlayFabResultCommon_t379512675
{
public:
	// PlayFab.ClientModels.TradeInfo PlayFab.ClientModels.OpenTradeResponse::<Trade>k__BackingField
	TradeInfo_t1933812770 * ___U3CTradeU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CTradeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(OpenTradeResponse_t3658687371, ___U3CTradeU3Ek__BackingField_2)); }
	inline TradeInfo_t1933812770 * get_U3CTradeU3Ek__BackingField_2() const { return ___U3CTradeU3Ek__BackingField_2; }
	inline TradeInfo_t1933812770 ** get_address_of_U3CTradeU3Ek__BackingField_2() { return &___U3CTradeU3Ek__BackingField_2; }
	inline void set_U3CTradeU3Ek__BackingField_2(TradeInfo_t1933812770 * value)
	{
		___U3CTradeU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTradeU3Ek__BackingField_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
