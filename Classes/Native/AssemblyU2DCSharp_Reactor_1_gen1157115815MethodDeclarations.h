﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Reactor_1_gen96911316MethodDeclarations.h"

// System.Void Reactor`1<UniRx.InternalUtil.ImmutableList`1<System.Object>>::.ctor()
#define Reactor_1__ctor_m2940188438(__this, method) ((  void (*) (Reactor_1_t1157115815 *, const MethodInfo*))Reactor_1__ctor_m1161914514_gshared)(__this, method)
// System.Void Reactor`1<UniRx.InternalUtil.ImmutableList`1<System.Object>>::React(T)
#define Reactor_1_React_m488223179(__this, ___t0, method) ((  void (*) (Reactor_1_t1157115815 *, ImmutableList_1_t1897310919 *, const MethodInfo*))Reactor_1_React_m1196306383_gshared)(__this, ___t0, method)
// System.Void Reactor`1<UniRx.InternalUtil.ImmutableList`1<System.Object>>::TransactionUnpackReact(T,System.Int32)
#define Reactor_1_TransactionUnpackReact_m4268469910(__this, ___t0, ___i1, method) ((  void (*) (Reactor_1_t1157115815 *, ImmutableList_1_t1897310919 *, int32_t, const MethodInfo*))Reactor_1_TransactionUnpackReact_m1003670938_gshared)(__this, ___t0, ___i1, method)
// System.Boolean Reactor`1<UniRx.InternalUtil.ImmutableList`1<System.Object>>::Empty()
#define Reactor_1_Empty_m995116045(__this, method) ((  bool (*) (Reactor_1_t1157115815 *, const MethodInfo*))Reactor_1_Empty_m132443017_gshared)(__this, method)
// System.IDisposable Reactor`1<UniRx.InternalUtil.ImmutableList`1<System.Object>>::AddReaction(System.Action`1<T>,Priority)
#define Reactor_1_AddReaction_m222833271(__this, ___reaction0, ___priority1, method) ((  Il2CppObject * (*) (Reactor_1_t1157115815 *, Action_1_t2045763624 *, int32_t, const MethodInfo*))Reactor_1_AddReaction_m1495770619_gshared)(__this, ___reaction0, ___priority1, method)
// System.Void Reactor`1<UniRx.InternalUtil.ImmutableList`1<System.Object>>::UpgradeToMultyPriority()
#define Reactor_1_UpgradeToMultyPriority_m373951044(__this, method) ((  void (*) (Reactor_1_t1157115815 *, const MethodInfo*))Reactor_1_UpgradeToMultyPriority_m912916552_gshared)(__this, method)
// System.Void Reactor`1<UniRx.InternalUtil.ImmutableList`1<System.Object>>::AddToMultipriority(System.Action`1<T>,Priority)
#define Reactor_1_AddToMultipriority_m758694003(__this, ___reaction0, ___p1, method) ((  void (*) (Reactor_1_t1157115815 *, Action_1_t2045763624 *, int32_t, const MethodInfo*))Reactor_1_AddToMultipriority_m3381282287_gshared)(__this, ___reaction0, ___p1, method)
// System.Void Reactor`1<UniRx.InternalUtil.ImmutableList`1<System.Object>>::RemoveReaction(System.Action`1<T>,Priority)
#define Reactor_1_RemoveReaction_m65932007(__this, ___reaction0, ___p1, method) ((  void (*) (Reactor_1_t1157115815 *, Action_1_t2045763624 *, int32_t, const MethodInfo*))Reactor_1_RemoveReaction_m4227182179_gshared)(__this, ___reaction0, ___p1, method)
// System.Void Reactor`1<UniRx.InternalUtil.ImmutableList`1<System.Object>>::Clear()
#define Reactor_1_Clear_m346321729(__this, method) ((  void (*) (Reactor_1_t1157115815 *, const MethodInfo*))Reactor_1_Clear_m2863015101_gshared)(__this, method)
