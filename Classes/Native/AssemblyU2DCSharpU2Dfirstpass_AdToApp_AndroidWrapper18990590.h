﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdToApp.AndroidWrapper.AdToAppAndroidWrapper/<ShowBanner>c__AnonStorey2B
struct  U3CShowBannerU3Ec__AnonStorey2B_t18990590  : public Il2CppObject
{
public:
	// System.Int32 AdToApp.AndroidWrapper.AdToAppAndroidWrapper/<ShowBanner>c__AnonStorey2B::x
	int32_t ___x_0;
	// System.Int32 AdToApp.AndroidWrapper.AdToAppAndroidWrapper/<ShowBanner>c__AnonStorey2B::y
	int32_t ___y_1;
	// System.String AdToApp.AndroidWrapper.AdToAppAndroidWrapper/<ShowBanner>c__AnonStorey2B::bannerSize
	String_t* ___bannerSize_2;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(U3CShowBannerU3Ec__AnonStorey2B_t18990590, ___x_0)); }
	inline int32_t get_x_0() const { return ___x_0; }
	inline int32_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(int32_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(U3CShowBannerU3Ec__AnonStorey2B_t18990590, ___y_1)); }
	inline int32_t get_y_1() const { return ___y_1; }
	inline int32_t* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(int32_t value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_bannerSize_2() { return static_cast<int32_t>(offsetof(U3CShowBannerU3Ec__AnonStorey2B_t18990590, ___bannerSize_2)); }
	inline String_t* get_bannerSize_2() const { return ___bannerSize_2; }
	inline String_t** get_address_of_bannerSize_2() { return &___bannerSize_2; }
	inline void set_bannerSize_2(String_t* value)
	{
		___bannerSize_2 = value;
		Il2CppCodeGenWriteBarrier(&___bannerSize_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
