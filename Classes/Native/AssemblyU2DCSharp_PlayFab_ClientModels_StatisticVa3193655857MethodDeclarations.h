﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.StatisticValue
struct StatisticValue_t3193655857;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.StatisticValue::.ctor()
extern "C"  void StatisticValue__ctor_m477226924 (StatisticValue_t3193655857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.StatisticValue::get_StatisticName()
extern "C"  String_t* StatisticValue_get_StatisticName_m2142139071 (StatisticValue_t3193655857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.StatisticValue::set_StatisticName(System.String)
extern "C"  void StatisticValue_set_StatisticName_m3419217242 (StatisticValue_t3193655857 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlayFab.ClientModels.StatisticValue::get_Value()
extern "C"  int32_t StatisticValue_get_Value_m3624155040 (StatisticValue_t3193655857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.StatisticValue::set_Value(System.Int32)
extern "C"  void StatisticValue_set_Value_m2817245935 (StatisticValue_t3193655857 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.StatisticValue::get_Version()
extern "C"  String_t* StatisticValue_get_Version_m3742689948 (StatisticValue_t3193655857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.StatisticValue::set_Version(System.String)
extern "C"  void StatisticValue_set_Version_m3755026909 (StatisticValue_t3193655857 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
