﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.TimeoutObservable`1<System.Object>
struct TimeoutObservable_1_t1895307051;
// System.Object
struct Il2CppObject;
// UniRx.SingleAssignmentDisposable
struct SingleAssignmentDisposable_t2336378823;
// System.IDisposable
struct IDisposable_t1628921374;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.TimeoutObservable`1/Timeout_<System.Object>
struct  Timeout__t2369373703  : public OperatorObserverBase_2_t1187768149
{
public:
	// UniRx.Operators.TimeoutObservable`1<T> UniRx.Operators.TimeoutObservable`1/Timeout_::parent
	TimeoutObservable_1_t1895307051 * ___parent_2;
	// System.Object UniRx.Operators.TimeoutObservable`1/Timeout_::gate
	Il2CppObject * ___gate_3;
	// System.Boolean UniRx.Operators.TimeoutObservable`1/Timeout_::isFinished
	bool ___isFinished_4;
	// UniRx.SingleAssignmentDisposable UniRx.Operators.TimeoutObservable`1/Timeout_::sourceSubscription
	SingleAssignmentDisposable_t2336378823 * ___sourceSubscription_5;
	// System.IDisposable UniRx.Operators.TimeoutObservable`1/Timeout_::timerSubscription
	Il2CppObject * ___timerSubscription_6;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(Timeout__t2369373703, ___parent_2)); }
	inline TimeoutObservable_1_t1895307051 * get_parent_2() const { return ___parent_2; }
	inline TimeoutObservable_1_t1895307051 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(TimeoutObservable_1_t1895307051 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_gate_3() { return static_cast<int32_t>(offsetof(Timeout__t2369373703, ___gate_3)); }
	inline Il2CppObject * get_gate_3() const { return ___gate_3; }
	inline Il2CppObject ** get_address_of_gate_3() { return &___gate_3; }
	inline void set_gate_3(Il2CppObject * value)
	{
		___gate_3 = value;
		Il2CppCodeGenWriteBarrier(&___gate_3, value);
	}

	inline static int32_t get_offset_of_isFinished_4() { return static_cast<int32_t>(offsetof(Timeout__t2369373703, ___isFinished_4)); }
	inline bool get_isFinished_4() const { return ___isFinished_4; }
	inline bool* get_address_of_isFinished_4() { return &___isFinished_4; }
	inline void set_isFinished_4(bool value)
	{
		___isFinished_4 = value;
	}

	inline static int32_t get_offset_of_sourceSubscription_5() { return static_cast<int32_t>(offsetof(Timeout__t2369373703, ___sourceSubscription_5)); }
	inline SingleAssignmentDisposable_t2336378823 * get_sourceSubscription_5() const { return ___sourceSubscription_5; }
	inline SingleAssignmentDisposable_t2336378823 ** get_address_of_sourceSubscription_5() { return &___sourceSubscription_5; }
	inline void set_sourceSubscription_5(SingleAssignmentDisposable_t2336378823 * value)
	{
		___sourceSubscription_5 = value;
		Il2CppCodeGenWriteBarrier(&___sourceSubscription_5, value);
	}

	inline static int32_t get_offset_of_timerSubscription_6() { return static_cast<int32_t>(offsetof(Timeout__t2369373703, ___timerSubscription_6)); }
	inline Il2CppObject * get_timerSubscription_6() const { return ___timerSubscription_6; }
	inline Il2CppObject ** get_address_of_timerSubscription_6() { return &___timerSubscription_6; }
	inline void set_timerSubscription_6(Il2CppObject * value)
	{
		___timerSubscription_6 = value;
		Il2CppCodeGenWriteBarrier(&___timerSubscription_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
