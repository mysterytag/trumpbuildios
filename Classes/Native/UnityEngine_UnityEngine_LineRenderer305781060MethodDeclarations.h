﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.LineRenderer
struct LineRenderer_t305781060;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"
#include "UnityEngine_UnityEngine_LineRenderer305781060.h"

// System.Void UnityEngine.LineRenderer::SetColors(UnityEngine.Color,UnityEngine.Color)
extern "C"  void LineRenderer_SetColors_m657519031 (LineRenderer_t305781060 * __this, Color_t1588175760  ___start0, Color_t1588175760  ___end1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LineRenderer::INTERNAL_CALL_SetColors(UnityEngine.LineRenderer,UnityEngine.Color&,UnityEngine.Color&)
extern "C"  void LineRenderer_INTERNAL_CALL_SetColors_m3942994585 (Il2CppObject * __this /* static, unused */, LineRenderer_t305781060 * ___self0, Color_t1588175760 * ___start1, Color_t1588175760 * ___end2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
