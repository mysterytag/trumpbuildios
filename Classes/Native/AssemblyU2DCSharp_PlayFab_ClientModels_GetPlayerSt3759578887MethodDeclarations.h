﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetPlayerStatisticsResult
struct GetPlayerStatisticsResult_t3759578887;
// System.Collections.Generic.List`1<PlayFab.ClientModels.StatisticValue>
struct List_1_t3990614826;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.GetPlayerStatisticsResult::.ctor()
extern "C"  void GetPlayerStatisticsResult__ctor_m1582771458 (GetPlayerStatisticsResult_t3759578887 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PlayFab.ClientModels.StatisticValue> PlayFab.ClientModels.GetPlayerStatisticsResult::get_Statistics()
extern "C"  List_1_t3990614826 * GetPlayerStatisticsResult_get_Statistics_m3488335438 (GetPlayerStatisticsResult_t3759578887 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetPlayerStatisticsResult::set_Statistics(System.Collections.Generic.List`1<PlayFab.ClientModels.StatisticValue>)
extern "C"  void GetPlayerStatisticsResult_set_Statistics_m3130283013 (GetPlayerStatisticsResult_t3759578887 * __this, List_1_t3990614826 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
