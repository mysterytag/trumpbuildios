﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Reactor_1_gen1676326883MethodDeclarations.h"

// System.Void Reactor`1<UniRx.CollectionAddEvent`1<UpgradeButton>>::.ctor()
#define Reactor_1__ctor_m1873899136(__this, method) ((  void (*) (Reactor_1_t2314687805 *, const MethodInfo*))Reactor_1__ctor_m3042049264_gshared)(__this, method)
// System.Void Reactor`1<UniRx.CollectionAddEvent`1<UpgradeButton>>::React(T)
#define Reactor_1_React_m1792993185(__this, ___t0, method) ((  void (*) (Reactor_1_t2314687805 *, CollectionAddEvent_1_t3054882909 , const MethodInfo*))Reactor_1_React_m3645908785_gshared)(__this, ___t0, method)
// System.Void Reactor`1<UniRx.CollectionAddEvent`1<UpgradeButton>>::TransactionUnpackReact(T,System.Int32)
#define Reactor_1_TransactionUnpackReact_m1367181804(__this, ___t0, ___i1, method) ((  void (*) (Reactor_1_t2314687805 *, CollectionAddEvent_1_t3054882909 , int32_t, const MethodInfo*))Reactor_1_TransactionUnpackReact_m2959551868_gshared)(__this, ___t0, ___i1, method)
// System.Boolean Reactor`1<UniRx.CollectionAddEvent`1<UpgradeButton>>::Empty()
#define Reactor_1_Empty_m181831543(__this, method) ((  bool (*) (Reactor_1_t2314687805 *, const MethodInfo*))Reactor_1_Empty_m1349981671_gshared)(__this, method)
// System.IDisposable Reactor`1<UniRx.CollectionAddEvent`1<UpgradeButton>>::AddReaction(System.Action`1<T>,Priority)
#define Reactor_1_AddReaction_m3278850893(__this, ___reaction0, ___priority1, method) ((  Il2CppObject * (*) (Reactor_1_t2314687805 *, Action_1_t3203335614 *, int32_t, const MethodInfo*))Reactor_1_AddReaction_m243157725_gshared)(__this, ___reaction0, ___priority1, method)
// System.Void Reactor`1<UniRx.CollectionAddEvent`1<UpgradeButton>>::UpgradeToMultyPriority()
#define Reactor_1_UpgradeToMultyPriority_m3540309018(__this, method) ((  void (*) (Reactor_1_t2314687805 *, const MethodInfo*))Reactor_1_UpgradeToMultyPriority_m1822749610_gshared)(__this, method)
// System.Void Reactor`1<UniRx.CollectionAddEvent`1<UpgradeButton>>::AddToMultipriority(System.Action`1<T>,Priority)
#define Reactor_1_AddToMultipriority_m2713273821(__this, ___reaction0, ___p1, method) ((  void (*) (Reactor_1_t2314687805 *, Action_1_t3203335614 *, int32_t, const MethodInfo*))Reactor_1_AddToMultipriority_m625386573_gshared)(__this, ___reaction0, ___p1, method)
// System.Void Reactor`1<UniRx.CollectionAddEvent`1<UpgradeButton>>::RemoveReaction(System.Action`1<T>,Priority)
#define Reactor_1_RemoveReaction_m999150929(__this, ___reaction0, ___p1, method) ((  void (*) (Reactor_1_t2314687805 *, Action_1_t3203335614 *, int32_t, const MethodInfo*))Reactor_1_RemoveReaction_m3790334913_gshared)(__this, ___reaction0, ___p1, method)
// System.Void Reactor`1<UniRx.CollectionAddEvent`1<UpgradeButton>>::Clear()
#define Reactor_1_Clear_m3574999723(__this, method) ((  void (*) (Reactor_1_t2314687805 *, const MethodInfo*))Reactor_1_Clear_m448182555_gshared)(__this, method)
