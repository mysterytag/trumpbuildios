﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ZergEngineTools_UnityActionDispo3710771925MethodDeclarations.h"

// System.Void ZergEngineTools/UnityActionDisposable`1<UnityEngine.EventSystems.BaseEventData>::.ctor()
#define UnityActionDisposable_1__ctor_m3788165897(__this, method) ((  void (*) (UnityActionDisposable_1_t2125801935 *, const MethodInfo*))UnityActionDisposable_1__ctor_m3503343429_gshared)(__this, method)
// System.Void ZergEngineTools/UnityActionDisposable`1<UnityEngine.EventSystems.BaseEventData>::Dispose()
#define UnityActionDisposable_1_Dispose_m2488410374(__this, method) ((  void (*) (UnityActionDisposable_1_t2125801935 *, const MethodInfo*))UnityActionDisposable_1_Dispose_m3651925570_gshared)(__this, method)
