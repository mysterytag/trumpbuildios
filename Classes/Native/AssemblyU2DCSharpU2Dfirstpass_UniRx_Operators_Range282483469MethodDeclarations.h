﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.RangeObservable
struct RangeObservable_t282483469;
// UniRx.IScheduler
struct IScheduler_t2938318244;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Int32>
struct IObserver_1_t764446394;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.RangeObservable::.ctor(System.Int32,System.Int32,UniRx.IScheduler)
extern "C"  void RangeObservable__ctor_m1043085892 (RangeObservable_t282483469 * __this, int32_t ___start0, int32_t ___count1, Il2CppObject * ___scheduler2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable UniRx.Operators.RangeObservable::SubscribeCore(UniRx.IObserver`1<System.Int32>,System.IDisposable)
extern "C"  Il2CppObject * RangeObservable_SubscribeCore_m1549044613 (RangeObservable_t282483469 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
