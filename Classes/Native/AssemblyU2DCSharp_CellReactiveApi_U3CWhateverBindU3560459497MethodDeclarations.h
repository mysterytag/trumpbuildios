﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/<WhateverBind>c__AnonStoreyFE`1<System.DateTime>
struct U3CWhateverBindU3Ec__AnonStoreyFE_1_t560459497;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime339033936.h"

// System.Void CellReactiveApi/<WhateverBind>c__AnonStoreyFE`1<System.DateTime>::.ctor()
extern "C"  void U3CWhateverBindU3Ec__AnonStoreyFE_1__ctor_m4104613631_gshared (U3CWhateverBindU3Ec__AnonStoreyFE_1_t560459497 * __this, const MethodInfo* method);
#define U3CWhateverBindU3Ec__AnonStoreyFE_1__ctor_m4104613631(__this, method) ((  void (*) (U3CWhateverBindU3Ec__AnonStoreyFE_1_t560459497 *, const MethodInfo*))U3CWhateverBindU3Ec__AnonStoreyFE_1__ctor_m4104613631_gshared)(__this, method)
// System.Void CellReactiveApi/<WhateverBind>c__AnonStoreyFE`1<System.DateTime>::<>m__174(T)
extern "C"  void U3CWhateverBindU3Ec__AnonStoreyFE_1_U3CU3Em__174_m3529454134_gshared (U3CWhateverBindU3Ec__AnonStoreyFE_1_t560459497 * __this, DateTime_t339033936  ___val0, const MethodInfo* method);
#define U3CWhateverBindU3Ec__AnonStoreyFE_1_U3CU3Em__174_m3529454134(__this, ___val0, method) ((  void (*) (U3CWhateverBindU3Ec__AnonStoreyFE_1_t560459497 *, DateTime_t339033936 , const MethodInfo*))U3CWhateverBindU3Ec__AnonStoreyFE_1_U3CU3Em__174_m3529454134_gshared)(__this, ___val0, method)
