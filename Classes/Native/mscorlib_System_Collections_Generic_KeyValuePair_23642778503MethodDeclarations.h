﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23312956448MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3297211003(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3642778503 *, String_t*, LinkedList_1_t2516549301 *, const MethodInfo*))KeyValuePair_2__ctor_m4168265535_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>>::get_Key()
#define KeyValuePair_2_get_Key_m3725912717(__this, method) ((  String_t* (*) (KeyValuePair_2_t3642778503 *, const MethodInfo*))KeyValuePair_2_get_Key_m3256475977_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2252539598(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3642778503 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m1278074762_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>>::get_Value()
#define KeyValuePair_2_get_Value_m1556381425(__this, method) ((  LinkedList_1_t2516549301 * (*) (KeyValuePair_2_t3642778503 *, const MethodInfo*))KeyValuePair_2_get_Value_m3899079597_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m16925646(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3642778503 *, LinkedList_1_t2516549301 *, const MethodInfo*))KeyValuePair_2_set_Value_m2954518154_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.LinkedList`1<Tacticsoft.TableViewCell>>::ToString()
#define KeyValuePair_2_ToString_m2528734074(__this, method) ((  String_t* (*) (KeyValuePair_2_t3642778503 *, const MethodInfo*))KeyValuePair_2_ToString_m1313859518_gshared)(__this, method)
