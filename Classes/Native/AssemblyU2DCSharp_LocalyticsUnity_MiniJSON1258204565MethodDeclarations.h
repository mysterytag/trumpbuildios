﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LocalyticsUnity.MiniJSON
struct MiniJSON_t1258204565;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Collections.IDictionary
struct IDictionary_t1654916945;
// System.Char[]
struct CharU5BU5D_t3416858730;
// System.Collections.IList
struct IList_t1612618265;
// System.Text.StringBuilder
struct StringBuilder_t3822575854;
// System.Collections.Hashtable
struct Hashtable_t3875263730;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t2474804324;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Text_StringBuilder3822575854.h"
#include "mscorlib_System_Collections_Hashtable3875263730.h"

// System.Void LocalyticsUnity.MiniJSON::.ctor()
extern "C"  void MiniJSON__ctor_m2176978608 (MiniJSON_t1258204565 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.MiniJSON::.cctor()
extern "C"  void MiniJSON__cctor_m2579731197 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LocalyticsUnity.MiniJSON::jsonDecode(System.String)
extern "C"  Il2CppObject * MiniJSON_jsonDecode_m3895311043 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LocalyticsUnity.MiniJSON::jsonDecode(System.String,System.Boolean)
extern "C"  Il2CppObject * MiniJSON_jsonDecode_m4039334490 (Il2CppObject * __this /* static, unused */, String_t* ___json0, bool ___decodeUsingGenericContainers1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LocalyticsUnity.MiniJSON::jsonEncode(System.Object)
extern "C"  String_t* MiniJSON_jsonEncode_m2701702143 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LocalyticsUnity.MiniJSON::lastDecodeSuccessful()
extern "C"  bool MiniJSON_lastDecodeSuccessful_m4144103302 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LocalyticsUnity.MiniJSON::getLastErrorIndex()
extern "C"  int32_t MiniJSON_getLastErrorIndex_m2143355766 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LocalyticsUnity.MiniJSON::getLastErrorSnippet()
extern "C"  String_t* MiniJSON_getLastErrorSnippet_m1590990732 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionary LocalyticsUnity.MiniJSON::parseObject(System.Char[],System.Int32&)
extern "C"  Il2CppObject * MiniJSON_parseObject_m3656828422 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json0, int32_t* ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IList LocalyticsUnity.MiniJSON::parseArray(System.Char[],System.Int32&)
extern "C"  Il2CppObject * MiniJSON_parseArray_m3688880904 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json0, int32_t* ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LocalyticsUnity.MiniJSON::parseValue(System.Char[],System.Int32&,System.Boolean&)
extern "C"  Il2CppObject * MiniJSON_parseValue_m1644210360 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json0, int32_t* ___index1, bool* ___success2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LocalyticsUnity.MiniJSON::parseString(System.Char[],System.Int32&)
extern "C"  String_t* MiniJSON_parseString_m3312926113 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json0, int32_t* ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double LocalyticsUnity.MiniJSON::parseNumber(System.Char[],System.Int32&)
extern "C"  double MiniJSON_parseNumber_m3888675241 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json0, int32_t* ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LocalyticsUnity.MiniJSON::getLastIndexOfNumber(System.Char[],System.Int32)
extern "C"  int32_t MiniJSON_getLastIndexOfNumber_m2770042436 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.MiniJSON::eatWhitespace(System.Char[],System.Int32&)
extern "C"  void MiniJSON_eatWhitespace_m1163578829 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json0, int32_t* ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LocalyticsUnity.MiniJSON::lookAhead(System.Char[],System.Int32)
extern "C"  int32_t MiniJSON_lookAhead_m2404043134 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LocalyticsUnity.MiniJSON::nextToken(System.Char[],System.Int32&)
extern "C"  int32_t MiniJSON_nextToken_m2445417514 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t3416858730* ___json0, int32_t* ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LocalyticsUnity.MiniJSON::serializeHashtableOrArrayList(System.Object,System.Text.StringBuilder)
extern "C"  bool MiniJSON_serializeHashtableOrArrayList_m269540330 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___objectOrArray0, StringBuilder_t3822575854 * ___builder1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LocalyticsUnity.MiniJSON::serializeHashtable(System.Collections.Hashtable,System.Text.StringBuilder)
extern "C"  bool MiniJSON_serializeHashtable_m391278148 (Il2CppObject * __this /* static, unused */, Hashtable_t3875263730 * ___anObject0, StringBuilder_t3822575854 * ___builder1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LocalyticsUnity.MiniJSON::serializeStringStringDictionary(System.Collections.Generic.Dictionary`2<System.String,System.String>,System.Text.StringBuilder)
extern "C"  bool MiniJSON_serializeStringStringDictionary_m3545383879 (Il2CppObject * __this /* static, unused */, Dictionary_2_t2606186806 * ___dict0, StringBuilder_t3822575854 * ___builder1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LocalyticsUnity.MiniJSON::serializeStringObjectDictionary(System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.Text.StringBuilder)
extern "C"  bool MiniJSON_serializeStringObjectDictionary_m4272623523 (Il2CppObject * __this /* static, unused */, Dictionary_2_t2474804324 * ___dict0, StringBuilder_t3822575854 * ___builder1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LocalyticsUnity.MiniJSON::serializeIList(System.Collections.IList,System.Text.StringBuilder)
extern "C"  bool MiniJSON_serializeIList_m1914476388 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___anArray0, StringBuilder_t3822575854 * ___builder1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LocalyticsUnity.MiniJSON::serializeObject(System.Object,System.Text.StringBuilder)
extern "C"  bool MiniJSON_serializeObject_m2295199 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, StringBuilder_t3822575854 * ___builder1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.MiniJSON::serializeString(System.String,System.Text.StringBuilder)
extern "C"  void MiniJSON_serializeString_m3127747399 (Il2CppObject * __this /* static, unused */, String_t* ___aString0, StringBuilder_t3822575854 * ___builder1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.MiniJSON::serializeNumber(System.Double,System.Text.StringBuilder)
extern "C"  void MiniJSON_serializeNumber_m236893007 (Il2CppObject * __this /* static, unused */, double ___number0, StringBuilder_t3822575854 * ___builder1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
