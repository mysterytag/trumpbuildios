﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GenericDataSource`2<System.Object,System.Object>
struct GenericDataSource_2_t1679676449;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// Tacticsoft.TableView
struct TableView_t692333993;
// Tacticsoft.TableViewCell
struct TableViewCell_t776419755;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"
#include "AssemblyU2DCSharp_Tacticsoft_TableView692333993.h"

// System.Void GenericDataSource`2<System.Object,System.Object>::.ctor()
extern "C"  void GenericDataSource_2__ctor_m1409035199_gshared (GenericDataSource_2_t1679676449 * __this, const MethodInfo* method);
#define GenericDataSource_2__ctor_m1409035199(__this, method) ((  void (*) (GenericDataSource_2_t1679676449 *, const MethodInfo*))GenericDataSource_2__ctor_m1409035199_gshared)(__this, method)
// System.Void GenericDataSource`2<System.Object,System.Object>::set_cellPrefab(UnityEngine.GameObject)
extern "C"  void GenericDataSource_2_set_cellPrefab_m1627438784_gshared (GenericDataSource_2_t1679676449 * __this, GameObject_t4012695102 * ___value0, const MethodInfo* method);
#define GenericDataSource_2_set_cellPrefab_m1627438784(__this, ___value0, method) ((  void (*) (GenericDataSource_2_t1679676449 *, GameObject_t4012695102 *, const MethodInfo*))GenericDataSource_2_set_cellPrefab_m1627438784_gshared)(__this, ___value0, method)
// System.Int32 GenericDataSource`2<System.Object,System.Object>::GetNumberOfRowsForTableView(Tacticsoft.TableView)
extern "C"  int32_t GenericDataSource_2_GetNumberOfRowsForTableView_m4207950081_gshared (GenericDataSource_2_t1679676449 * __this, TableView_t692333993 * ___tableView0, const MethodInfo* method);
#define GenericDataSource_2_GetNumberOfRowsForTableView_m4207950081(__this, ___tableView0, method) ((  int32_t (*) (GenericDataSource_2_t1679676449 *, TableView_t692333993 *, const MethodInfo*))GenericDataSource_2_GetNumberOfRowsForTableView_m4207950081_gshared)(__this, ___tableView0, method)
// System.Single GenericDataSource`2<System.Object,System.Object>::GetHeightForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  float GenericDataSource_2_GetHeightForRowInTableView_m2709442445_gshared (GenericDataSource_2_t1679676449 * __this, TableView_t692333993 * ___tableView0, int32_t ___row1, const MethodInfo* method);
#define GenericDataSource_2_GetHeightForRowInTableView_m2709442445(__this, ___tableView0, ___row1, method) ((  float (*) (GenericDataSource_2_t1679676449 *, TableView_t692333993 *, int32_t, const MethodInfo*))GenericDataSource_2_GetHeightForRowInTableView_m2709442445_gshared)(__this, ___tableView0, ___row1, method)
// Tacticsoft.TableViewCell GenericDataSource`2<System.Object,System.Object>::GetCellForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  TableViewCell_t776419755 * GenericDataSource_2_GetCellForRowInTableView_m102593472_gshared (GenericDataSource_2_t1679676449 * __this, TableView_t692333993 * ___tableView0, int32_t ___row1, const MethodInfo* method);
#define GenericDataSource_2_GetCellForRowInTableView_m102593472(__this, ___tableView0, ___row1, method) ((  TableViewCell_t776419755 * (*) (GenericDataSource_2_t1679676449 *, TableView_t692333993 *, int32_t, const MethodInfo*))GenericDataSource_2_GetCellForRowInTableView_m102593472_gshared)(__this, ___tableView0, ___row1, method)
