﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetCharacterLeaderboardRequestCallback
struct GetCharacterLeaderboardRequestCallback_t2066086314;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetCharacterLeaderboardRequest
struct GetCharacterLeaderboardRequest_t2906494549;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetCharacte2906494549.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetCharacterLeaderboardRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetCharacterLeaderboardRequestCallback__ctor_m2948286873 (GetCharacterLeaderboardRequestCallback_t2066086314 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetCharacterLeaderboardRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetCharacterLeaderboardRequest,System.Object)
extern "C"  void GetCharacterLeaderboardRequestCallback_Invoke_m2213520863 (GetCharacterLeaderboardRequestCallback_t2066086314 * __this, String_t* ___urlPath0, int32_t ___callId1, GetCharacterLeaderboardRequest_t2906494549 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetCharacterLeaderboardRequestCallback_t2066086314(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetCharacterLeaderboardRequest_t2906494549 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetCharacterLeaderboardRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetCharacterLeaderboardRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetCharacterLeaderboardRequestCallback_BeginInvoke_m1212225594 (GetCharacterLeaderboardRequestCallback_t2066086314 * __this, String_t* ___urlPath0, int32_t ___callId1, GetCharacterLeaderboardRequest_t2906494549 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetCharacterLeaderboardRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetCharacterLeaderboardRequestCallback_EndInvoke_m2279436329 (GetCharacterLeaderboardRequestCallback_t2066086314 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
