﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.BufferObservable`1<System.Object>
struct BufferObservable_1_t574294898;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.Queue`1<System.Collections.Generic.IList`1<System.Object>>
struct Queue_1_t416718978;
// UniRx.SerialDisposable
struct SerialDisposable_t2547852742;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3354260463.h"
#include "mscorlib_System_TimeSpan763862892.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.BufferObservable`1/BufferTS<System.Object>
struct  BufferTS_t71212032  : public OperatorObserverBase_2_t3354260463
{
public:
	// UniRx.Operators.BufferObservable`1<T> UniRx.Operators.BufferObservable`1/BufferTS::parent
	BufferObservable_1_t574294898 * ___parent_2;
	// System.Object UniRx.Operators.BufferObservable`1/BufferTS::gate
	Il2CppObject * ___gate_3;
	// System.Collections.Generic.Queue`1<System.Collections.Generic.IList`1<T>> UniRx.Operators.BufferObservable`1/BufferTS::q
	Queue_1_t416718978 * ___q_4;
	// System.TimeSpan UniRx.Operators.BufferObservable`1/BufferTS::totalTime
	TimeSpan_t763862892  ___totalTime_5;
	// System.TimeSpan UniRx.Operators.BufferObservable`1/BufferTS::nextShift
	TimeSpan_t763862892  ___nextShift_6;
	// System.TimeSpan UniRx.Operators.BufferObservable`1/BufferTS::nextSpan
	TimeSpan_t763862892  ___nextSpan_7;
	// UniRx.SerialDisposable UniRx.Operators.BufferObservable`1/BufferTS::timerD
	SerialDisposable_t2547852742 * ___timerD_8;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(BufferTS_t71212032, ___parent_2)); }
	inline BufferObservable_1_t574294898 * get_parent_2() const { return ___parent_2; }
	inline BufferObservable_1_t574294898 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(BufferObservable_1_t574294898 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_gate_3() { return static_cast<int32_t>(offsetof(BufferTS_t71212032, ___gate_3)); }
	inline Il2CppObject * get_gate_3() const { return ___gate_3; }
	inline Il2CppObject ** get_address_of_gate_3() { return &___gate_3; }
	inline void set_gate_3(Il2CppObject * value)
	{
		___gate_3 = value;
		Il2CppCodeGenWriteBarrier(&___gate_3, value);
	}

	inline static int32_t get_offset_of_q_4() { return static_cast<int32_t>(offsetof(BufferTS_t71212032, ___q_4)); }
	inline Queue_1_t416718978 * get_q_4() const { return ___q_4; }
	inline Queue_1_t416718978 ** get_address_of_q_4() { return &___q_4; }
	inline void set_q_4(Queue_1_t416718978 * value)
	{
		___q_4 = value;
		Il2CppCodeGenWriteBarrier(&___q_4, value);
	}

	inline static int32_t get_offset_of_totalTime_5() { return static_cast<int32_t>(offsetof(BufferTS_t71212032, ___totalTime_5)); }
	inline TimeSpan_t763862892  get_totalTime_5() const { return ___totalTime_5; }
	inline TimeSpan_t763862892 * get_address_of_totalTime_5() { return &___totalTime_5; }
	inline void set_totalTime_5(TimeSpan_t763862892  value)
	{
		___totalTime_5 = value;
	}

	inline static int32_t get_offset_of_nextShift_6() { return static_cast<int32_t>(offsetof(BufferTS_t71212032, ___nextShift_6)); }
	inline TimeSpan_t763862892  get_nextShift_6() const { return ___nextShift_6; }
	inline TimeSpan_t763862892 * get_address_of_nextShift_6() { return &___nextShift_6; }
	inline void set_nextShift_6(TimeSpan_t763862892  value)
	{
		___nextShift_6 = value;
	}

	inline static int32_t get_offset_of_nextSpan_7() { return static_cast<int32_t>(offsetof(BufferTS_t71212032, ___nextSpan_7)); }
	inline TimeSpan_t763862892  get_nextSpan_7() const { return ___nextSpan_7; }
	inline TimeSpan_t763862892 * get_address_of_nextSpan_7() { return &___nextSpan_7; }
	inline void set_nextSpan_7(TimeSpan_t763862892  value)
	{
		___nextSpan_7 = value;
	}

	inline static int32_t get_offset_of_timerD_8() { return static_cast<int32_t>(offsetof(BufferTS_t71212032, ___timerD_8)); }
	inline SerialDisposable_t2547852742 * get_timerD_8() const { return ___timerD_8; }
	inline SerialDisposable_t2547852742 ** get_address_of_timerD_8() { return &___timerD_8; }
	inline void set_timerD_8(SerialDisposable_t2547852742 * value)
	{
		___timerD_8 = value;
		Il2CppCodeGenWriteBarrier(&___timerD_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
