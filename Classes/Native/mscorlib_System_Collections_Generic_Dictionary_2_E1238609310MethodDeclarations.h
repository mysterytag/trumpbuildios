﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3591453091MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Type>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2812469724(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1238609310 *, Dictionary_2_t1471581369 *, const MethodInfo*))Enumerator__ctor_m3920831137_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Type>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2330723077(__this, method) ((  Il2CppObject * (*) (Enumerator_t1238609310 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3262087712_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Type>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m82689369(__this, method) ((  void (*) (Enumerator_t1238609310 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2959141748_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Type>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2787649122(__this, method) ((  DictionaryEntry_t130027246  (*) (Enumerator_t1238609310 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2279524093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Type>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m850143265(__this, method) ((  Il2CppObject * (*) (Enumerator_t1238609310 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1201448700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Type>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1992327795(__this, method) ((  Il2CppObject * (*) (Enumerator_t1238609310 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m294434446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Type>::MoveNext()
#define Enumerator_MoveNext_m2840275269(__this, method) ((  bool (*) (Enumerator_t1238609310 *, const MethodInfo*))Enumerator_MoveNext_m217327200_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Type>::get_Current()
#define Enumerator_get_Current_m770581835(__this, method) ((  KeyValuePair_2_t960112667  (*) (Enumerator_t1238609310 *, const MethodInfo*))Enumerator_get_Current_m4240003024_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Type>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1169102290(__this, method) ((  Il2CppObject * (*) (Enumerator_t1238609310 *, const MethodInfo*))Enumerator_get_CurrentKey_m3062159917_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Type>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3858590326(__this, method) ((  Type_t * (*) (Enumerator_t1238609310 *, const MethodInfo*))Enumerator_get_CurrentValue_m592783249_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Type>::Reset()
#define Enumerator_Reset_m1966371374(__this, method) ((  void (*) (Enumerator_t1238609310 *, const MethodInfo*))Enumerator_Reset_m3001375603_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Type>::VerifyState()
#define Enumerator_VerifyState_m4161333175(__this, method) ((  void (*) (Enumerator_t1238609310 *, const MethodInfo*))Enumerator_VerifyState_m4290054460_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Type>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m3171500383(__this, method) ((  void (*) (Enumerator_t1238609310 *, const MethodInfo*))Enumerator_VerifyCurrent_m2318603684_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Type>::Dispose()
#define Enumerator_Dispose_m2420709246(__this, method) ((  void (*) (Enumerator_t1238609310 *, const MethodInfo*))Enumerator_Dispose_m627360643_gshared)(__this, method)
