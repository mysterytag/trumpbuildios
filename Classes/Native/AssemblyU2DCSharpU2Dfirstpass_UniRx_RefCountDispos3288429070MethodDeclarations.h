﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.RefCountDisposable
struct RefCountDisposable_t3288429070;
// System.IDisposable
struct IDisposable_t1628921374;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.RefCountDisposable::.ctor(System.IDisposable)
extern "C"  void RefCountDisposable__ctor_m3639770769 (RefCountDisposable_t3288429070 * __this, Il2CppObject * ___disposable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.RefCountDisposable::get_IsDisposed()
extern "C"  bool RefCountDisposable_get_IsDisposed_m4030042941 (RefCountDisposable_t3288429070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable UniRx.RefCountDisposable::GetDisposable()
extern "C"  Il2CppObject * RefCountDisposable_GetDisposable_m4263689044 (RefCountDisposable_t3288429070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.RefCountDisposable::Dispose()
extern "C"  void RefCountDisposable_Dispose_m4047242424 (RefCountDisposable_t3288429070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.RefCountDisposable::Release()
extern "C"  void RefCountDisposable_Release_m3885968544 (RefCountDisposable_t3288429070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
