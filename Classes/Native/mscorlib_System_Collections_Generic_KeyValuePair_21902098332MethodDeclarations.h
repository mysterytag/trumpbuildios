﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23312956448MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2772128529(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1902098332 *, Type_t *, Il2CppObject*, const MethodInfo*))KeyValuePair_2__ctor_m4168265535_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>>::get_Key()
#define KeyValuePair_2_get_Key_m3948429751(__this, method) ((  Type_t * (*) (KeyValuePair_2_t1902098332 *, const MethodInfo*))KeyValuePair_2_get_Key_m3256475977_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m4194214008(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1902098332 *, Type_t *, const MethodInfo*))KeyValuePair_2_set_Key_m1278074762_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>>::get_Value()
#define KeyValuePair_2_get_Value_m530641719(__this, method) ((  Il2CppObject* (*) (KeyValuePair_2_t1902098332 *, const MethodInfo*))KeyValuePair_2_get_Value_m3899079597_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2493839480(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1902098332 *, Il2CppObject*, const MethodInfo*))KeyValuePair_2_set_Value_m2954518154_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,PlayFab.ReflectionUtils/SetDelegate>>>::ToString()
#define KeyValuePair_2_ToString_m3893580906(__this, method) ((  String_t* (*) (KeyValuePair_2_t1902098332 *, const MethodInfo*))KeyValuePair_2_ToString_m1313859518_gshared)(__this, method)
