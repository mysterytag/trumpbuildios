﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1634065389MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::.ctor()
#define List_1__ctor_m3541516847(__this, method) ((  void (*) (List_1_t2233047014 *, const MethodInfo*))List_1__ctor_m348833073_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m1264632560(__this, ___collection0, method) ((  void (*) (List_1_t2233047014 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1160795371_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::.ctor(System.Int32)
#define List_1__ctor_m2430609664(__this, ___capacity0, method) ((  void (*) (List_1_t2233047014 *, int32_t, const MethodInfo*))List_1__ctor_m2892763214_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::.cctor()
#define List_1__cctor_m1930743646(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3826137881_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m217261433(__this, method) ((  Il2CppObject* (*) (List_1_t2233047014 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2808422246_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m1804818933(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2233047014 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m4034025648_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3435779268(__this, method) ((  Il2CppObject * (*) (List_1_t2233047014 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1841330603_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m545248377(__this, ___item0, method) ((  int32_t (*) (List_1_t2233047014 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3794749222_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m147130855(__this, ___item0, method) ((  bool (*) (List_1_t2233047014 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2659633254_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m710878801(__this, ___item0, method) ((  int32_t (*) (List_1_t2233047014 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3431692926_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m3205924164(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2233047014 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2067529129_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m951706980(__this, ___item0, method) ((  void (*) (List_1_t2233047014 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1644145887_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2597782824(__this, method) ((  bool (*) (List_1_t2233047014 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1299706087_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m2303635477(__this, method) ((  bool (*) (List_1_t2233047014 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3867536694_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m107830919(__this, method) ((  Il2CppObject * (*) (List_1_t2233047014 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m4244374434_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m439874198(__this, method) ((  bool (*) (List_1_t2233047014 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m432946261_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m4208976291(__this, method) ((  bool (*) (List_1_t2233047014 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2961826820_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m1611909710(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t2233047014 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3985478825_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m2015522843(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2233047014 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3234554688_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::Add(T)
#define List_1_Add_m1074216048(__this, ___item0, method) ((  void (*) (List_1_t2233047014 *, Intrusion_t1436088045 *, const MethodInfo*))List_1_Add_m642669291_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m4272935595(__this, ___newCount0, method) ((  void (*) (List_1_t2233047014 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m4122600870_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m3069361577(__this, ___collection0, method) ((  void (*) (List_1_t2233047014 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2478449828_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m2330333801(__this, ___enumerable0, method) ((  void (*) (List_1_t2233047014 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1739422052_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m3266395790(__this, ___collection0, method) ((  void (*) (List_1_t2233047014 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2229151411_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::AsReadOnly()
#define List_1_AsReadOnly_m4273874231(__this, method) ((  ReadOnlyCollection_1_t304266097 * (*) (List_1_t2233047014 *, const MethodInfo*))List_1_AsReadOnly_m769820182_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::Clear()
#define List_1_Clear_m947650138(__this, method) ((  void (*) (List_1_t2233047014 *, const MethodInfo*))List_1_Clear_m454602559_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::Contains(T)
#define List_1_Contains_m3554083532(__this, ___item0, method) ((  bool (*) (List_1_t2233047014 *, Intrusion_t1436088045 *, const MethodInfo*))List_1_Contains_m4186092781_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m2732648544(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2233047014 *, IntrusionU5BU5D_t361707200*, int32_t, const MethodInfo*))List_1_CopyTo_m3988356635_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::Find(System.Predicate`1<T>)
#define List_1_Find_m3638198758(__this, ___match0, method) ((  Intrusion_t1436088045 * (*) (List_1_t2233047014 *, Predicate_1_t2007051943 *, const MethodInfo*))List_1_Find_m3379773421_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m829465219(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t2007051943 *, const MethodInfo*))List_1_CheckMatch_m3390394152_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m3973555808(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t2233047014 *, int32_t, int32_t, Predicate_1_t2007051943 *, const MethodInfo*))List_1_GetIndex_m4275988045_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m2395757623(__this, ___action0, method) ((  void (*) (List_1_t2233047014 *, Action_1_t1584540750 *, const MethodInfo*))List_1_ForEach_m2030348444_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::GetEnumerator()
#define List_1_GetEnumerator_m3993959945(__this, method) ((  Enumerator_t318830006  (*) (List_1_t2233047014 *, const MethodInfo*))List_1_GetEnumerator_m1820263692_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::IndexOf(T)
#define List_1_IndexOf_m3420288812(__this, ___item0, method) ((  int32_t (*) (List_1_t2233047014 *, Intrusion_t1436088045 *, const MethodInfo*))List_1_IndexOf_m1752303327_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m3447348919(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t2233047014 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3807054194_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m2479015728(__this, ___index0, method) ((  void (*) (List_1_t2233047014 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3734723819_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::Insert(System.Int32,T)
#define List_1_Insert_m2858260183(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2233047014 *, int32_t, Intrusion_t1436088045 *, const MethodInfo*))List_1_Insert_m3427163986_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m3835578892(__this, ___collection0, method) ((  void (*) (List_1_t2233047014 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2905071175_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::Remove(T)
#define List_1_Remove_m2890270151(__this, ___item0, method) ((  bool (*) (List_1_t2233047014 *, Intrusion_t1436088045 *, const MethodInfo*))List_1_Remove_m2747911208_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m3824899567(__this, ___match0, method) ((  int32_t (*) (List_1_t2233047014 *, Predicate_1_t2007051943 *, const MethodInfo*))List_1_RemoveAll_m2933443938_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m732113053(__this, ___index0, method) ((  void (*) (List_1_t2233047014 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1301016856_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::Reverse()
#define List_1_Reverse_m1821402799(__this, method) ((  void (*) (List_1_t2233047014 *, const MethodInfo*))List_1_Reverse_m449081940_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::Sort()
#define List_1_Sort_m1600188243(__this, method) ((  void (*) (List_1_t2233047014 *, const MethodInfo*))List_1_Sort_m1168641486_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m3735908337(__this, ___comparer0, method) ((  void (*) (List_1_t2233047014 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m3726677974_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m2634967910(__this, ___comparison0, method) ((  void (*) (List_1_t2233047014 *, Comparison_1_t4139762921 *, const MethodInfo*))List_1_Sort_m4192185249_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::ToArray()
#define List_1_ToArray_m3750481928(__this, method) ((  IntrusionU5BU5D_t361707200* (*) (List_1_t2233047014 *, const MethodInfo*))List_1_ToArray_m1046025517_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::TrimExcess()
#define List_1_TrimExcess_m1468400812(__this, method) ((  void (*) (List_1_t2233047014 *, const MethodInfo*))List_1_TrimExcess_m2451380967_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::get_Capacity()
#define List_1_get_Capacity_m81131868(__this, method) ((  int32_t (*) (List_1_t2233047014 *, const MethodInfo*))List_1_get_Capacity_m543520655_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m1483124413(__this, ___value0, method) ((  void (*) (List_1_t2233047014 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1332789688_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::get_Count()
#define List_1_get_Count_m2767045583(__this, method) ((  int32_t (*) (List_1_t2233047014 *, const MethodInfo*))List_1_get_Count_m2599103100_gshared)(__this, method)
// T System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::get_Item(System.Int32)
#define List_1_get_Item_m2179195139(__this, ___index0, method) ((  Intrusion_t1436088045 * (*) (List_1_t2233047014 *, int32_t, const MethodInfo*))List_1_get_Item_m2771401372_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::set_Item(System.Int32,T)
#define List_1_set_Item_m4113530350(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2233047014 *, int32_t, Intrusion_t1436088045 *, const MethodInfo*))List_1_set_Item_m1074271145_gshared)(__this, ___index0, ___value1, method)
