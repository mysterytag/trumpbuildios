﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SubstanceBase`2/<Affect>c__AnonStorey143<System.Single,System.Object>
struct U3CAffectU3Ec__AnonStorey143_t2725761868;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void SubstanceBase`2/<Affect>c__AnonStorey143<System.Single,System.Object>::.ctor()
extern "C"  void U3CAffectU3Ec__AnonStorey143__ctor_m2992756604_gshared (U3CAffectU3Ec__AnonStorey143_t2725761868 * __this, const MethodInfo* method);
#define U3CAffectU3Ec__AnonStorey143__ctor_m2992756604(__this, method) ((  void (*) (U3CAffectU3Ec__AnonStorey143_t2725761868 *, const MethodInfo*))U3CAffectU3Ec__AnonStorey143__ctor_m2992756604_gshared)(__this, method)
// System.Void SubstanceBase`2/<Affect>c__AnonStorey143<System.Single,System.Object>::<>m__1D9(InfT)
extern "C"  void U3CAffectU3Ec__AnonStorey143_U3CU3Em__1D9_m4072402640_gshared (U3CAffectU3Ec__AnonStorey143_t2725761868 * __this, Il2CppObject * ___val0, const MethodInfo* method);
#define U3CAffectU3Ec__AnonStorey143_U3CU3Em__1D9_m4072402640(__this, ___val0, method) ((  void (*) (U3CAffectU3Ec__AnonStorey143_t2725761868 *, Il2CppObject *, const MethodInfo*))U3CAffectU3Ec__AnonStorey143_U3CU3Em__1D9_m4072402640_gshared)(__this, ___val0, method)
