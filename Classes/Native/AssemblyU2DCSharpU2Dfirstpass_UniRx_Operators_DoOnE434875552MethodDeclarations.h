﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.DoOnErrorObservable`1<System.Object>
struct DoOnErrorObservable_1_t434875552;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Action`1<System.Exception>
struct Action_1_t2115686693;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.DoOnErrorObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Action`1<System.Exception>)
extern "C"  void DoOnErrorObservable_1__ctor_m1696072545_gshared (DoOnErrorObservable_1_t434875552 * __this, Il2CppObject* ___source0, Action_1_t2115686693 * ___onError1, const MethodInfo* method);
#define DoOnErrorObservable_1__ctor_m1696072545(__this, ___source0, ___onError1, method) ((  void (*) (DoOnErrorObservable_1_t434875552 *, Il2CppObject*, Action_1_t2115686693 *, const MethodInfo*))DoOnErrorObservable_1__ctor_m1696072545_gshared)(__this, ___source0, ___onError1, method)
// System.IDisposable UniRx.Operators.DoOnErrorObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * DoOnErrorObservable_1_SubscribeCore_m3106849008_gshared (DoOnErrorObservable_1_t434875552 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define DoOnErrorObservable_1_SubscribeCore_m3106849008(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (DoOnErrorObservable_1_t434875552 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))DoOnErrorObservable_1_SubscribeCore_m3106849008_gshared)(__this, ___observer0, ___cancel1, method)
