﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <>__AnonType7`2<System.Object,System.Object>
struct  U3CU3E__AnonType7_2_t1682020602  : public Il2CppObject
{
public:
	// <reward>__T <>__AnonType7`2::<reward>
	Il2CppObject * ___U3CrewardU3E_0;
	// <loggedIn>__T <>__AnonType7`2::<loggedIn>
	Il2CppObject * ___U3CloggedInU3E_1;

public:
	inline static int32_t get_offset_of_U3CrewardU3E_0() { return static_cast<int32_t>(offsetof(U3CU3E__AnonType7_2_t1682020602, ___U3CrewardU3E_0)); }
	inline Il2CppObject * get_U3CrewardU3E_0() const { return ___U3CrewardU3E_0; }
	inline Il2CppObject ** get_address_of_U3CrewardU3E_0() { return &___U3CrewardU3E_0; }
	inline void set_U3CrewardU3E_0(Il2CppObject * value)
	{
		___U3CrewardU3E_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrewardU3E_0, value);
	}

	inline static int32_t get_offset_of_U3CloggedInU3E_1() { return static_cast<int32_t>(offsetof(U3CU3E__AnonType7_2_t1682020602, ___U3CloggedInU3E_1)); }
	inline Il2CppObject * get_U3CloggedInU3E_1() const { return ___U3CloggedInU3E_1; }
	inline Il2CppObject ** get_address_of_U3CloggedInU3E_1() { return &___U3CloggedInU3E_1; }
	inline void set_U3CloggedInU3E_1(Il2CppObject * value)
	{
		___U3CloggedInU3E_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CloggedInU3E_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
