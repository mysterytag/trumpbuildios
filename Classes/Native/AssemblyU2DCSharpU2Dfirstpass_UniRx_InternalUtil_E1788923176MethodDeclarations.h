﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.EmptyObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>
struct EmptyObserver_1_t1788923176;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen2379570186.h"

// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m4137202945_gshared (EmptyObserver_1_t1788923176 * __this, const MethodInfo* method);
#define EmptyObserver_1__ctor_m4137202945(__this, method) ((  void (*) (EmptyObserver_1_t1788923176 *, const MethodInfo*))EmptyObserver_1__ctor_m4137202945_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m3217143500_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EmptyObserver_1__cctor_m3217143500(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EmptyObserver_1__cctor_m3217143500_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m307127243_gshared (EmptyObserver_1_t1788923176 * __this, const MethodInfo* method);
#define EmptyObserver_1_OnCompleted_m307127243(__this, method) ((  void (*) (EmptyObserver_1_t1788923176 *, const MethodInfo*))EmptyObserver_1_OnCompleted_m307127243_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m4150443128_gshared (EmptyObserver_1_t1788923176 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define EmptyObserver_1_OnError_m4150443128(__this, ___error0, method) ((  void (*) (EmptyObserver_1_t1788923176 *, Exception_t1967233988 *, const MethodInfo*))EmptyObserver_1_OnError_m4150443128_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m1543017321_gshared (EmptyObserver_1_t1788923176 * __this, Tuple_2_t2379570186  ___value0, const MethodInfo* method);
#define EmptyObserver_1_OnNext_m1543017321(__this, ___value0, method) ((  void (*) (EmptyObserver_1_t1788923176 *, Tuple_2_t2379570186 , const MethodInfo*))EmptyObserver_1_OnNext_m1543017321_gshared)(__this, ___value0, method)
