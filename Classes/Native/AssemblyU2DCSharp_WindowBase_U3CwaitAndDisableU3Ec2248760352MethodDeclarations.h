﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WindowBase/<waitAndDisable>c__Iterator5
struct U3CwaitAndDisableU3Ec__Iterator5_t2248760352;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void WindowBase/<waitAndDisable>c__Iterator5::.ctor()
extern "C"  void U3CwaitAndDisableU3Ec__Iterator5__ctor_m3411348157 (U3CwaitAndDisableU3Ec__Iterator5_t2248760352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object WindowBase/<waitAndDisable>c__Iterator5::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CwaitAndDisableU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3450145205 (U3CwaitAndDisableU3Ec__Iterator5_t2248760352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object WindowBase/<waitAndDisable>c__Iterator5::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CwaitAndDisableU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m3376617289 (U3CwaitAndDisableU3Ec__Iterator5_t2248760352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WindowBase/<waitAndDisable>c__Iterator5::MoveNext()
extern "C"  bool U3CwaitAndDisableU3Ec__Iterator5_MoveNext_m4141691415 (U3CwaitAndDisableU3Ec__Iterator5_t2248760352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WindowBase/<waitAndDisable>c__Iterator5::Dispose()
extern "C"  void U3CwaitAndDisableU3Ec__Iterator5_Dispose_m1143815098 (U3CwaitAndDisableU3Ec__Iterator5_t2248760352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WindowBase/<waitAndDisable>c__Iterator5::Reset()
extern "C"  void U3CwaitAndDisableU3Ec__Iterator5_Reset_m1057781098 (U3CwaitAndDisableU3Ec__Iterator5_t2248760352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
