﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetCharacterReadOnlyDataResponseCallback
struct GetCharacterReadOnlyDataResponseCallback_t761172773;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetCharacterDataRequest
struct GetCharacterDataRequest_t572698882;
// PlayFab.ClientModels.GetCharacterDataResult
struct GetCharacterDataResult_t1300547946;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetCharacter572698882.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetCharacte1300547946.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetCharacterReadOnlyDataResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetCharacterReadOnlyDataResponseCallback__ctor_m3695834516 (GetCharacterReadOnlyDataResponseCallback_t761172773 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetCharacterReadOnlyDataResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetCharacterDataRequest,PlayFab.ClientModels.GetCharacterDataResult,PlayFab.PlayFabError,System.Object)
extern "C"  void GetCharacterReadOnlyDataResponseCallback_Invoke_m1020641729 (GetCharacterReadOnlyDataResponseCallback_t761172773 * __this, String_t* ___urlPath0, int32_t ___callId1, GetCharacterDataRequest_t572698882 * ___request2, GetCharacterDataResult_t1300547946 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetCharacterReadOnlyDataResponseCallback_t761172773(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetCharacterDataRequest_t572698882 * ___request2, GetCharacterDataResult_t1300547946 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetCharacterReadOnlyDataResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetCharacterDataRequest,PlayFab.ClientModels.GetCharacterDataResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetCharacterReadOnlyDataResponseCallback_BeginInvoke_m880464174 (GetCharacterReadOnlyDataResponseCallback_t761172773 * __this, String_t* ___urlPath0, int32_t ___callId1, GetCharacterDataRequest_t572698882 * ___request2, GetCharacterDataResult_t1300547946 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetCharacterReadOnlyDataResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetCharacterReadOnlyDataResponseCallback_EndInvoke_m58675620 (GetCharacterReadOnlyDataResponseCallback_t761172773 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
