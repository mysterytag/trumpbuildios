﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct Subject_1_t3886712006;
// System.Exception
struct Exception_t1967233988;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct IObserver_1_t4160676289;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionAddE1948677386.h"

// System.Void UniRx.Subject`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor()
extern "C"  void Subject_1__ctor_m2394553130_gshared (Subject_1_t3886712006 * __this, const MethodInfo* method);
#define Subject_1__ctor_m2394553130(__this, method) ((  void (*) (Subject_1_t3886712006 *, const MethodInfo*))Subject_1__ctor_m2394553130_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::get_HasObservers()
extern "C"  bool Subject_1_get_HasObservers_m3662622314_gshared (Subject_1_t3886712006 * __this, const MethodInfo* method);
#define Subject_1_get_HasObservers_m3662622314(__this, method) ((  bool (*) (Subject_1_t3886712006 *, const MethodInfo*))Subject_1_get_HasObservers_m3662622314_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnCompleted()
extern "C"  void Subject_1_OnCompleted_m3133318964_gshared (Subject_1_t3886712006 * __this, const MethodInfo* method);
#define Subject_1_OnCompleted_m3133318964(__this, method) ((  void (*) (Subject_1_t3886712006 *, const MethodInfo*))Subject_1_OnCompleted_m3133318964_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnError(System.Exception)
extern "C"  void Subject_1_OnError_m2957519457_gshared (Subject_1_t3886712006 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Subject_1_OnError_m2957519457(__this, ___error0, method) ((  void (*) (Subject_1_t3886712006 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m2957519457_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnNext(T)
extern "C"  void Subject_1_OnNext_m1893790546_gshared (Subject_1_t3886712006 * __this, CollectionAddEvent_1_t1948677386  ___value0, const MethodInfo* method);
#define Subject_1_OnNext_m1893790546(__this, ___value0, method) ((  void (*) (Subject_1_t3886712006 *, CollectionAddEvent_1_t1948677386 , const MethodInfo*))Subject_1_OnNext_m1893790546_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * Subject_1_Subscribe_m3412382719_gshared (Subject_1_t3886712006 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define Subject_1_Subscribe_m3412382719(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t3886712006 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m3412382719_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::Dispose()
extern "C"  void Subject_1_Dispose_m3256337639_gshared (Subject_1_t3886712006 * __this, const MethodInfo* method);
#define Subject_1_Dispose_m3256337639(__this, method) ((  void (*) (Subject_1_t3886712006 *, const MethodInfo*))Subject_1_Dispose_m3256337639_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::ThrowIfDisposed()
extern "C"  void Subject_1_ThrowIfDisposed_m4176582512_gshared (Subject_1_t3886712006 * __this, const MethodInfo* method);
#define Subject_1_ThrowIfDisposed_m4176582512(__this, method) ((  void (*) (Subject_1_t3886712006 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m4176582512_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool Subject_1_IsRequiredSubscribeOnCurrentThread_m530093281_gshared (Subject_1_t3886712006 * __this, const MethodInfo* method);
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m530093281(__this, method) ((  bool (*) (Subject_1_t3886712006 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m530093281_gshared)(__this, method)
