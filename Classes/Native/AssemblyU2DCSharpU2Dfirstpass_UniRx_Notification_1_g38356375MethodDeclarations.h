﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Notification`1<System.Object>
struct Notification_1_t38356375;
// System.Object
struct Il2CppObject;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// UniRx.IScheduler
struct IScheduler_t2938318244;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.Notification`1<System.Object>::.ctor()
extern "C"  void Notification_1__ctor_m2783159343_gshared (Notification_1_t38356375 * __this, const MethodInfo* method);
#define Notification_1__ctor_m2783159343(__this, method) ((  void (*) (Notification_1_t38356375 *, const MethodInfo*))Notification_1__ctor_m2783159343_gshared)(__this, method)
// System.Boolean UniRx.Notification`1<System.Object>::Equals(System.Object)
extern "C"  bool Notification_1_Equals_m1024238204_gshared (Notification_1_t38356375 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define Notification_1_Equals_m1024238204(__this, ___obj0, method) ((  bool (*) (Notification_1_t38356375 *, Il2CppObject *, const MethodInfo*))Notification_1_Equals_m1024238204_gshared)(__this, ___obj0, method)
// UniRx.IObservable`1<T> UniRx.Notification`1<System.Object>::ToObservable()
extern "C"  Il2CppObject* Notification_1_ToObservable_m2814509903_gshared (Notification_1_t38356375 * __this, const MethodInfo* method);
#define Notification_1_ToObservable_m2814509903(__this, method) ((  Il2CppObject* (*) (Notification_1_t38356375 *, const MethodInfo*))Notification_1_ToObservable_m2814509903_gshared)(__this, method)
// UniRx.IObservable`1<T> UniRx.Notification`1<System.Object>::ToObservable(UniRx.IScheduler)
extern "C"  Il2CppObject* Notification_1_ToObservable_m3055295589_gshared (Notification_1_t38356375 * __this, Il2CppObject * ___scheduler0, const MethodInfo* method);
#define Notification_1_ToObservable_m3055295589(__this, ___scheduler0, method) ((  Il2CppObject* (*) (Notification_1_t38356375 *, Il2CppObject *, const MethodInfo*))Notification_1_ToObservable_m3055295589_gshared)(__this, ___scheduler0, method)
// System.Boolean UniRx.Notification`1<System.Object>::op_Equality(UniRx.Notification`1<T>,UniRx.Notification`1<T>)
extern "C"  bool Notification_1_op_Equality_m2804091545_gshared (Il2CppObject * __this /* static, unused */, Notification_1_t38356375 * ___left0, Notification_1_t38356375 * ___right1, const MethodInfo* method);
#define Notification_1_op_Equality_m2804091545(__this /* static, unused */, ___left0, ___right1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Notification_1_t38356375 *, Notification_1_t38356375 *, const MethodInfo*))Notification_1_op_Equality_m2804091545_gshared)(__this /* static, unused */, ___left0, ___right1, method)
// System.Boolean UniRx.Notification`1<System.Object>::op_Inequality(UniRx.Notification`1<T>,UniRx.Notification`1<T>)
extern "C"  bool Notification_1_op_Inequality_m3842301588_gshared (Il2CppObject * __this /* static, unused */, Notification_1_t38356375 * ___left0, Notification_1_t38356375 * ___right1, const MethodInfo* method);
#define Notification_1_op_Inequality_m3842301588(__this /* static, unused */, ___left0, ___right1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Notification_1_t38356375 *, Notification_1_t38356375 *, const MethodInfo*))Notification_1_op_Inequality_m3842301588_gshared)(__this /* static, unused */, ___left0, ___right1, method)
