﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectObservable`2/Select_<System.Int32,UnityEngine.Color>
struct Select__t1427971985;
// UniRx.Operators.SelectObservable`2<System.Int32,UnityEngine.Color>
struct SelectObservable_2_t3125378242;
// UniRx.IObserver`1<UnityEngine.Color>
struct IObserver_1_t3800174663;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SelectObservable`2/Select_<System.Int32,UnityEngine.Color>::.ctor(UniRx.Operators.SelectObservable`2<T,TR>,UniRx.IObserver`1<TR>,System.IDisposable)
extern "C"  void Select___ctor_m3063233024_gshared (Select__t1427971985 * __this, SelectObservable_2_t3125378242 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Select___ctor_m3063233024(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Select__t1427971985 *, SelectObservable_2_t3125378242 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Select___ctor_m3063233024_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.SelectObservable`2/Select_<System.Int32,UnityEngine.Color>::OnNext(T)
extern "C"  void Select__OnNext_m751427208_gshared (Select__t1427971985 * __this, int32_t ___value0, const MethodInfo* method);
#define Select__OnNext_m751427208(__this, ___value0, method) ((  void (*) (Select__t1427971985 *, int32_t, const MethodInfo*))Select__OnNext_m751427208_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SelectObservable`2/Select_<System.Int32,UnityEngine.Color>::OnError(System.Exception)
extern "C"  void Select__OnError_m2395990423_gshared (Select__t1427971985 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Select__OnError_m2395990423(__this, ___error0, method) ((  void (*) (Select__t1427971985 *, Exception_t1967233988 *, const MethodInfo*))Select__OnError_m2395990423_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SelectObservable`2/Select_<System.Int32,UnityEngine.Color>::OnCompleted()
extern "C"  void Select__OnCompleted_m892798826_gshared (Select__t1427971985 * __this, const MethodInfo* method);
#define Select__OnCompleted_m892798826(__this, method) ((  void (*) (Select__t1427971985 *, const MethodInfo*))Select__OnCompleted_m892798826_gshared)(__this, method)
