﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MaterialContainer
struct MaterialContainer_t3323339514;

#include "codegen/il2cpp-codegen.h"

// System.Void MaterialContainer::.ctor()
extern "C"  void MaterialContainer__ctor_m2604417969 (MaterialContainer_t3323339514 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MaterialContainer MaterialContainer::get_instance()
extern "C"  MaterialContainer_t3323339514 * MaterialContainer_get_instance_m1292130488 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MaterialContainer::Start()
extern "C"  void MaterialContainer_Start_m1551555761 (MaterialContainer_t3323339514 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MaterialContainer::Update()
extern "C"  void MaterialContainer_Update_m859440508 (MaterialContainer_t3323339514 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
