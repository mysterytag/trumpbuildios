﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Astray`1<System.Object>
struct Astray_1_t3225947498;

#include "codegen/il2cpp-codegen.h"

// System.Void Astray`1<System.Object>::.ctor()
extern "C"  void Astray_1__ctor_m2603205864_gshared (Astray_1_t3225947498 * __this, const MethodInfo* method);
#define Astray_1__ctor_m2603205864(__this, method) ((  void (*) (Astray_1_t3225947498 *, const MethodInfo*))Astray_1__ctor_m2603205864_gshared)(__this, method)
