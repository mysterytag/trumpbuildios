﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserMaritalStatus>>
struct JsonResponse_1_t3715343323;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen2392475756.h"

// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserMaritalStatus>>::.ctor()
extern "C"  void JsonResponse_1__ctor_m1769364167_gshared (JsonResponse_1_t3715343323 * __this, const MethodInfo* method);
#define JsonResponse_1__ctor_m1769364167(__this, method) ((  void (*) (JsonResponse_1_t3715343323 *, const MethodInfo*))JsonResponse_1__ctor_m1769364167_gshared)(__this, method)
// System.String SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserMaritalStatus>>::get_key()
extern "C"  String_t* JsonResponse_1_get_key_m269850206_gshared (JsonResponse_1_t3715343323 * __this, const MethodInfo* method);
#define JsonResponse_1_get_key_m269850206(__this, method) ((  String_t* (*) (JsonResponse_1_t3715343323 *, const MethodInfo*))JsonResponse_1_get_key_m269850206_gshared)(__this, method)
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserMaritalStatus>>::set_key(System.String)
extern "C"  void JsonResponse_1_set_key_m1074170587_gshared (JsonResponse_1_t3715343323 * __this, String_t* ___value0, const MethodInfo* method);
#define JsonResponse_1_set_key_m1074170587(__this, ___value0, method) ((  void (*) (JsonResponse_1_t3715343323 *, String_t*, const MethodInfo*))JsonResponse_1_set_key_m1074170587_gshared)(__this, ___value0, method)
// T SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserMaritalStatus>>::get_value()
extern "C"  Nullable_1_t2392475756  JsonResponse_1_get_value_m2948429516_gshared (JsonResponse_1_t3715343323 * __this, const MethodInfo* method);
#define JsonResponse_1_get_value_m2948429516(__this, method) ((  Nullable_1_t2392475756  (*) (JsonResponse_1_t3715343323 *, const MethodInfo*))JsonResponse_1_get_value_m2948429516_gshared)(__this, method)
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserMaritalStatus>>::set_value(T)
extern "C"  void JsonResponse_1_set_value_m3492163621_gshared (JsonResponse_1_t3715343323 * __this, Nullable_1_t2392475756  ___value0, const MethodInfo* method);
#define JsonResponse_1_set_value_m3492163621(__this, ___value0, method) ((  void (*) (JsonResponse_1_t3715343323 *, Nullable_1_t2392475756 , const MethodInfo*))JsonResponse_1_set_value_m3492163621_gshared)(__this, ___value0, method)
// System.String SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserMaritalStatus>>::get_error()
extern "C"  String_t* JsonResponse_1_get_error_m965856327_gshared (JsonResponse_1_t3715343323 * __this, const MethodInfo* method);
#define JsonResponse_1_get_error_m965856327(__this, method) ((  String_t* (*) (JsonResponse_1_t3715343323 *, const MethodInfo*))JsonResponse_1_get_error_m965856327_gshared)(__this, method)
// System.Void SponsorPay.SPUser/JsonResponse`1<System.Nullable`1<SponsorPay.SPUserMaritalStatus>>::set_error(System.String)
extern "C"  void JsonResponse_1_set_error_m4141643218_gshared (JsonResponse_1_t3715343323 * __this, String_t* ___value0, const MethodInfo* method);
#define JsonResponse_1_set_error_m4141643218(__this, ___value0, method) ((  void (*) (JsonResponse_1_t3715343323 *, String_t*, const MethodInfo*))JsonResponse_1_set_error_m4141643218_gshared)(__this, ___value0, method)
