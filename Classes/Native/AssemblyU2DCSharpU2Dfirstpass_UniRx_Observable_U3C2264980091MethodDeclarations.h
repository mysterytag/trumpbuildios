﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/<TimerFrameCore>c__Iterator16
struct U3CTimerFrameCoreU3Ec__Iterator16_t2264980091;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Observable/<TimerFrameCore>c__Iterator16::.ctor()
extern "C"  void U3CTimerFrameCoreU3Ec__Iterator16__ctor_m121519528 (U3CTimerFrameCoreU3Ec__Iterator16_t2264980091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UniRx.Observable/<TimerFrameCore>c__Iterator16::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTimerFrameCoreU3Ec__Iterator16_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1445069172 (U3CTimerFrameCoreU3Ec__Iterator16_t2264980091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UniRx.Observable/<TimerFrameCore>c__Iterator16::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTimerFrameCoreU3Ec__Iterator16_System_Collections_IEnumerator_get_Current_m273509640 (U3CTimerFrameCoreU3Ec__Iterator16_t2264980091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.Observable/<TimerFrameCore>c__Iterator16::MoveNext()
extern "C"  bool U3CTimerFrameCoreU3Ec__Iterator16_MoveNext_m1069021468 (U3CTimerFrameCoreU3Ec__Iterator16_t2264980091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Observable/<TimerFrameCore>c__Iterator16::Dispose()
extern "C"  void U3CTimerFrameCoreU3Ec__Iterator16_Dispose_m714432485 (U3CTimerFrameCoreU3Ec__Iterator16_t2264980091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Observable/<TimerFrameCore>c__Iterator16::Reset()
extern "C"  void U3CTimerFrameCoreU3Ec__Iterator16_Reset_m2062919765 (U3CTimerFrameCoreU3Ec__Iterator16_t2264980091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
