﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameAnalyticsSDK.Settings
struct Settings_t1104664933;
// System.String[]
struct StringU5BU5D_t2956870243;
// System.Int32[]
struct Int32U5BU5D_t1809983122;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void GameAnalyticsSDK.Settings::.ctor()
extern "C"  void Settings__ctor_m3268142240 (Settings_t1104664933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.Settings::.cctor()
extern "C"  void Settings__cctor_m2046065421 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] GameAnalyticsSDK.Settings::CreateStringArrayWithEmptyStrings(System.Int32)
extern "C"  StringU5BU5D_t2956870243* Settings_CreateStringArrayWithEmptyStrings_m1448664567 (Il2CppObject * __this /* static, unused */, int32_t ___size0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] GameAnalyticsSDK.Settings::CreateStringArrayWithPlatformStrings(System.Int32)
extern "C"  StringU5BU5D_t2956870243* Settings_CreateStringArrayWithPlatformStrings_m3809535409 (Il2CppObject * __this /* static, unused */, int32_t ___size0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] GameAnalyticsSDK.Settings::CreateIntArrayWithIDs(System.Int32,System.Int32)
extern "C"  Int32U5BU5D_t1809983122* Settings_CreateIntArrayWithIDs_m3504813104 (Il2CppObject * __this /* static, unused */, int32_t ___size0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.Settings::SetCustomUserID(System.String)
extern "C"  void Settings_SetCustomUserID_m2807479979 (Settings_t1104664933 * __this, String_t* ___customID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameAnalyticsSDK.Settings::IsGameKeyValid(System.Int32,System.String)
extern "C"  bool Settings_IsGameKeyValid_m4256449334 (Settings_t1104664933 * __this, int32_t ___index0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameAnalyticsSDK.Settings::IsSecretKeyValid(System.Int32,System.String)
extern "C"  bool Settings_IsSecretKeyValid_m2705020084 (Settings_t1104664933 * __this, int32_t ___index0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.Settings::UpdateGameKey(System.Int32,System.String)
extern "C"  void Settings_UpdateGameKey_m1877184783 (Settings_t1104664933 * __this, int32_t ___index0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.Settings::UpdateSecretKey(System.Int32,System.String)
extern "C"  void Settings_UpdateSecretKey_m2746059153 (Settings_t1104664933 * __this, int32_t ___index0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GameAnalyticsSDK.Settings::GetGameKey(System.Int32)
extern "C"  String_t* Settings_GetGameKey_m3744465737 (Settings_t1104664933 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GameAnalyticsSDK.Settings::GetSecretKey(System.Int32)
extern "C"  String_t* Settings_GetSecretKey_m1422373323 (Settings_t1104664933 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.Settings::SetCustomArea(System.String)
extern "C"  void Settings_SetCustomArea_m2047991140 (Settings_t1104664933 * __this, String_t* ___customArea0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.Settings::SetKeys(System.String,System.String)
extern "C"  void Settings_SetKeys_m1094388938 (Settings_t1104664933 * __this, String_t* ___gamekey0, String_t* ___secretkey1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
