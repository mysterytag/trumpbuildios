﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/<FromAsyncPattern>c__AnonStorey3F`1<System.Object>
struct U3CFromAsyncPatternU3Ec__AnonStorey3F_1_t1370739976;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Observable/<FromAsyncPattern>c__AnonStorey3F`1<System.Object>::.ctor()
extern "C"  void U3CFromAsyncPatternU3Ec__AnonStorey3F_1__ctor_m994084238_gshared (U3CFromAsyncPatternU3Ec__AnonStorey3F_1_t1370739976 * __this, const MethodInfo* method);
#define U3CFromAsyncPatternU3Ec__AnonStorey3F_1__ctor_m994084238(__this, method) ((  void (*) (U3CFromAsyncPatternU3Ec__AnonStorey3F_1_t1370739976 *, const MethodInfo*))U3CFromAsyncPatternU3Ec__AnonStorey3F_1__ctor_m994084238_gshared)(__this, method)
// UniRx.IObservable`1<TResult> UniRx.Observable/<FromAsyncPattern>c__AnonStorey3F`1<System.Object>::<>m__40()
extern "C"  Il2CppObject* U3CFromAsyncPatternU3Ec__AnonStorey3F_1_U3CU3Em__40_m2354625300_gshared (U3CFromAsyncPatternU3Ec__AnonStorey3F_1_t1370739976 * __this, const MethodInfo* method);
#define U3CFromAsyncPatternU3Ec__AnonStorey3F_1_U3CU3Em__40_m2354625300(__this, method) ((  Il2CppObject* (*) (U3CFromAsyncPatternU3Ec__AnonStorey3F_1_t1370739976 *, const MethodInfo*))U3CFromAsyncPatternU3Ec__AnonStorey3F_1_U3CU3Em__40_m2354625300_gshared)(__this, method)
