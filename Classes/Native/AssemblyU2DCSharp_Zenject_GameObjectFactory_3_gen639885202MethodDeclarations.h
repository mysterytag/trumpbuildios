﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.GameObjectFactory`3<System.Object,System.Object,System.Object>
struct GameObjectFactory_3_t639885202;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException>
struct IEnumerable_1_t4073207355;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void Zenject.GameObjectFactory`3<System.Object,System.Object,System.Object>::.ctor()
extern "C"  void GameObjectFactory_3__ctor_m4107911696_gshared (GameObjectFactory_3_t639885202 * __this, const MethodInfo* method);
#define GameObjectFactory_3__ctor_m4107911696(__this, method) ((  void (*) (GameObjectFactory_3_t639885202 *, const MethodInfo*))GameObjectFactory_3__ctor_m4107911696_gshared)(__this, method)
// TValue Zenject.GameObjectFactory`3<System.Object,System.Object,System.Object>::Create(TParam1,TParam2)
extern "C"  Il2CppObject * GameObjectFactory_3_Create_m463918013_gshared (GameObjectFactory_3_t639885202 * __this, Il2CppObject * ___param10, Il2CppObject * ___param21, const MethodInfo* method);
#define GameObjectFactory_3_Create_m463918013(__this, ___param10, ___param21, method) ((  Il2CppObject * (*) (GameObjectFactory_3_t639885202 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))GameObjectFactory_3_Create_m463918013_gshared)(__this, ___param10, ___param21, method)
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.GameObjectFactory`3<System.Object,System.Object,System.Object>::Validate()
extern "C"  Il2CppObject* GameObjectFactory_3_Validate_m1056211689_gshared (GameObjectFactory_3_t639885202 * __this, const MethodInfo* method);
#define GameObjectFactory_3_Validate_m1056211689(__this, method) ((  Il2CppObject* (*) (GameObjectFactory_3_t639885202 *, const MethodInfo*))GameObjectFactory_3_Validate_m1056211689_gshared)(__this, method)
