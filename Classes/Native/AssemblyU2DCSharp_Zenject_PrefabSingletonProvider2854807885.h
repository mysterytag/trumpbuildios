﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Zenject.PrefabSingletonLazyCreator
struct PrefabSingletonLazyCreator_t3719004230;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.Type
struct Type_t;

#include "AssemblyU2DCSharp_Zenject_ProviderBase1627494391.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PrefabSingletonProvider
struct  PrefabSingletonProvider_t2854807885  : public ProviderBase_t1627494391
{
public:
	// Zenject.PrefabSingletonLazyCreator Zenject.PrefabSingletonProvider::_creator
	PrefabSingletonLazyCreator_t3719004230 * ____creator_2;
	// Zenject.DiContainer Zenject.PrefabSingletonProvider::_container
	DiContainer_t2383114449 * ____container_3;
	// System.Type Zenject.PrefabSingletonProvider::_concreteType
	Type_t * ____concreteType_4;

public:
	inline static int32_t get_offset_of__creator_2() { return static_cast<int32_t>(offsetof(PrefabSingletonProvider_t2854807885, ____creator_2)); }
	inline PrefabSingletonLazyCreator_t3719004230 * get__creator_2() const { return ____creator_2; }
	inline PrefabSingletonLazyCreator_t3719004230 ** get_address_of__creator_2() { return &____creator_2; }
	inline void set__creator_2(PrefabSingletonLazyCreator_t3719004230 * value)
	{
		____creator_2 = value;
		Il2CppCodeGenWriteBarrier(&____creator_2, value);
	}

	inline static int32_t get_offset_of__container_3() { return static_cast<int32_t>(offsetof(PrefabSingletonProvider_t2854807885, ____container_3)); }
	inline DiContainer_t2383114449 * get__container_3() const { return ____container_3; }
	inline DiContainer_t2383114449 ** get_address_of__container_3() { return &____container_3; }
	inline void set__container_3(DiContainer_t2383114449 * value)
	{
		____container_3 = value;
		Il2CppCodeGenWriteBarrier(&____container_3, value);
	}

	inline static int32_t get_offset_of__concreteType_4() { return static_cast<int32_t>(offsetof(PrefabSingletonProvider_t2854807885, ____concreteType_4)); }
	inline Type_t * get__concreteType_4() const { return ____concreteType_4; }
	inline Type_t ** get_address_of__concreteType_4() { return &____concreteType_4; }
	inline void set__concreteType_4(Type_t * value)
	{
		____concreteType_4 = value;
		Il2CppCodeGenWriteBarrier(&____concreteType_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
