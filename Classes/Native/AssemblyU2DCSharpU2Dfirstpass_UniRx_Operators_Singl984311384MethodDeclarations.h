﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SingleObservable`1/Single_<System.Object>
struct Single__t984311384;
// UniRx.Operators.SingleObservable`1<System.Object>
struct SingleObservable_1_t427280954;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SingleObservable`1/Single_<System.Object>::.ctor(UniRx.Operators.SingleObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Single___ctor_m1197900589_gshared (Single__t984311384 * __this, SingleObservable_1_t427280954 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Single___ctor_m1197900589(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Single__t984311384 *, SingleObservable_1_t427280954 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Single___ctor_m1197900589_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.SingleObservable`1/Single_<System.Object>::OnNext(T)
extern "C"  void Single__OnNext_m675265822_gshared (Single__t984311384 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Single__OnNext_m675265822(__this, ___value0, method) ((  void (*) (Single__t984311384 *, Il2CppObject *, const MethodInfo*))Single__OnNext_m675265822_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SingleObservable`1/Single_<System.Object>::OnError(System.Exception)
extern "C"  void Single__OnError_m2454838317_gshared (Single__t984311384 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Single__OnError_m2454838317(__this, ___error0, method) ((  void (*) (Single__t984311384 *, Exception_t1967233988 *, const MethodInfo*))Single__OnError_m2454838317_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SingleObservable`1/Single_<System.Object>::OnCompleted()
extern "C"  void Single__OnCompleted_m2932845312_gshared (Single__t984311384 * __this, const MethodInfo* method);
#define Single__OnCompleted_m2932845312(__this, method) ((  void (*) (Single__t984311384 *, const MethodInfo*))Single__OnCompleted_m2932845312_gshared)(__this, method)
