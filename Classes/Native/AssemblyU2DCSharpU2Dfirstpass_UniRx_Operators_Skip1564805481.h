﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Operat60103313.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.SkipObservable`1/Skip<System.Boolean>
struct  Skip_t1564805481  : public OperatorObserverBase_2_t60103313
{
public:
	// System.Int32 UniRx.Operators.SkipObservable`1/Skip::remaining
	int32_t ___remaining_2;

public:
	inline static int32_t get_offset_of_remaining_2() { return static_cast<int32_t>(offsetof(Skip_t1564805481, ___remaining_2)); }
	inline int32_t get_remaining_2() const { return ___remaining_2; }
	inline int32_t* get_address_of_remaining_2() { return &___remaining_2; }
	inline void set_remaining_2(int32_t value)
	{
		___remaining_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
