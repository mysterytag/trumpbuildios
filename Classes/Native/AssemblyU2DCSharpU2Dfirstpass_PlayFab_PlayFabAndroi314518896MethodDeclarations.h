﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabAndroidPlugin
struct PlayFabAndroidPlugin_t314518896;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.PlayFabAndroidPlugin::.ctor()
extern "C"  void PlayFabAndroidPlugin__ctor_m1466085477 (PlayFabAndroidPlugin_t314518896 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabAndroidPlugin::.cctor()
extern "C"  void PlayFabAndroidPlugin__cctor_m2016880616 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayFab.PlayFabAndroidPlugin::IsAvailable()
extern "C"  bool PlayFabAndroidPlugin_IsAvailable_m3443727846 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabAndroidPlugin::Init(System.String)
extern "C"  void PlayFabAndroidPlugin_Init_m573356563 (Il2CppObject * __this /* static, unused */, String_t* ___SenderID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayFab.PlayFabAndroidPlugin::IsPlayServicesAvailable()
extern "C"  bool PlayFabAndroidPlugin_IsPlayServicesAvailable_m2306452692 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabAndroidPlugin::UpdateRouting(System.Boolean)
extern "C"  void PlayFabAndroidPlugin_UpdateRouting_m3838781815 (Il2CppObject * __this /* static, unused */, bool ___routeToNotificationArea0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
