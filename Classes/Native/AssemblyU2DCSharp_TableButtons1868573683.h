﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.ReactiveProperty`1<TableButtons/StatkaPoTableViev>
struct ReactiveProperty_1_t4116728855;
// AnalyticsController
struct AnalyticsController_t2265609122;
// System.Collections.Generic.Dictionary`2<System.String,System.Int64>
struct Dictionary_2_t190145490;
// System.String
struct String_t;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// WindowManager
struct WindowManager_t3316821373;
// UnityEngine.Sprite
struct Sprite_t4006040370;
// Tacticsoft.TableView
struct TableView_t692333993;
// ZergRush.InorganicCollection`1<UpgradeButton>
struct InorganicCollection_1_t4089338440;
// ZergRush.InorganicCollection`1<DonateButton>
struct InorganicCollection_1_t3284624539;
// ZergRush.InorganicCollection`1<SettingsButton>
struct InorganicCollection_1_t3529127759;
// UnityEngine.RectTransform
struct RectTransform_t3317474837;
// ConnectionCollector
struct ConnectionCollector_t444796719;
// GuiController
struct GuiController_t1495593751;
// GlobalStat
struct GlobalStat_t1134678199;
// UnityEngine.UI.Text
struct Text_t3286458198;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// UnityEngine.UI.Image
struct Image_t3354615620;
// UnityEngine.UI.Button
struct Button_t990034267;
// OligarchParameters
struct OligarchParameters_t821144443;
// UnityEngine.UI.Mask
struct Mask_t3286245653;
// IconsProvider
struct IconsProvider_t3884200715;
// GenericDataSource`2<DonateButton,DonateCell>
struct GenericDataSource_2_t3164199101;
// GenericDataSource`2<UpgradeButton,UpgradeCell>
struct GenericDataSource_2_t837417641;
// GenericDataSource`2<SettingsButton,SettingsCell>
struct GenericDataSource_2_t337768557;
// TutorialPlayer
struct TutorialPlayer_t4281139199;
// BarygaVkController
struct BarygaVkController_t3617163921;
// AchievementController
struct AchievementController_t3677499403;
// GameController
struct GameController_t2782302542;
// IVkConnector
struct IVkConnector_t2786758927;
// FbController
struct FbController_t3069175192;
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t2279835437;
// System.Action`2<SettingsButton,SettingsCell>
struct Action_2_t2763552026;
// System.Action`2<DonateButton,DonateCell>
struct Action_2_t1295015274;
// System.Func`3<System.Double,System.Int32,System.Double>
struct Func_3_t2695431030;
// System.Func`3<System.DateTime,System.Int64,<>__AnonType5`2<System.DateTime,System.Int64>>
struct Func_3_t1001475269;
// System.Func`3<System.Double,System.Int32,<>__AnonType6`2<System.Double,System.Int32>>
struct Func_3_t1497502332;
// System.Action`1<DonateCell>
struct Action_1_t2946927090;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TableButtons
struct  TableButtons_t1868573683  : public Il2CppObject
{
public:
	// UniRx.ReactiveProperty`1<TableButtons/StatkaPoTableViev> TableButtons::StatusTableButtons
	ReactiveProperty_1_t4116728855 * ___StatusTableButtons_0;
	// AnalyticsController TableButtons::analyticsController
	AnalyticsController_t2265609122 * ___analyticsController_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int64> TableButtons::NotificationDictionary
	Dictionary_2_t190145490 * ___NotificationDictionary_2;
	// System.String TableButtons::fblogStat
	String_t* ___fblogStat_3;
	// Zenject.DiContainer TableButtons::DiContainer
	DiContainer_t2383114449 * ___DiContainer_4;
	// WindowManager TableButtons::WindowManager
	WindowManager_t3316821373 * ___WindowManager_5;
	// UnityEngine.Sprite TableButtons::SoundICon
	Sprite_t4006040370 * ___SoundICon_6;
	// Tacticsoft.TableView TableButtons::TableView
	TableView_t692333993 * ___TableView_7;
	// ZergRush.InorganicCollection`1<UpgradeButton> TableButtons::_billButtons
	InorganicCollection_1_t4089338440 * ____billButtons_8;
	// ZergRush.InorganicCollection`1<UpgradeButton> TableButtons::_passiveButtons
	InorganicCollection_1_t4089338440 * ____passiveButtons_9;
	// ZergRush.InorganicCollection`1<UpgradeButton> TableButtons::_safeButtons
	InorganicCollection_1_t4089338440 * ____safeButtons_10;
	// ZergRush.InorganicCollection`1<DonateButton> TableButtons::_donateButtons
	InorganicCollection_1_t3284624539 * ____donateButtons_11;
	// ZergRush.InorganicCollection`1<SettingsButton> TableButtons::_settingsButtons
	InorganicCollection_1_t3529127759 * ____settingsButtons_12;
	// UnityEngine.RectTransform TableButtons::_tableRectTransform
	RectTransform_t3317474837 * ____tableRectTransform_13;
	// ConnectionCollector TableButtons::_tableDataConnections
	ConnectionCollector_t444796719 * ____tableDataConnections_14;
	// GuiController TableButtons::_guiController
	GuiController_t1495593751 * ____guiController_15;
	// GlobalStat TableButtons::GlobalStat
	GlobalStat_t1134678199 * ___GlobalStat_16;
	// UnityEngine.UI.Text TableButtons::MenuName
	Text_t3286458198 * ___MenuName_17;
	// UnityEngine.GameObject TableButtons::TableParent
	GameObject_t4012695102 * ___TableParent_18;
	// UnityEngine.UI.Text TableButtons::MenuDescription
	Text_t3286458198 * ___MenuDescription_19;
	// UnityEngine.UI.Image TableButtons::premiumIcon
	Image_t3354615620 * ___premiumIcon_20;
	// UnityEngine.UI.Image TableButtons::premium
	Image_t3354615620 * ___premium_21;
	// UnityEngine.UI.Button TableButtons::premiumButton
	Button_t990034267 * ___premiumButton_22;
	// UnityEngine.UI.Text TableButtons::premiumText
	Text_t3286458198 * ___premiumText_23;
	// UnityEngine.UI.Text TableButtons::premiumButtonText
	Text_t3286458198 * ___premiumButtonText_24;
	// UnityEngine.UI.Text TableButtons::premiumDescriptionText
	Text_t3286458198 * ___premiumDescriptionText_25;
	// OligarchParameters TableButtons::OligarchParameters
	OligarchParameters_t821144443 * ___OligarchParameters_26;
	// UnityEngine.GameObject TableButtons::HeaderGameObject
	GameObject_t4012695102 * ___HeaderGameObject_27;
	// UnityEngine.UI.Mask TableButtons::TableMask
	Mask_t3286245653 * ___TableMask_28;
	// UnityEngine.UI.Image TableButtons::TabIMGMask
	Image_t3354615620 * ___TabIMGMask_29;
	// GuiController TableButtons::GuiController
	GuiController_t1495593751 * ___GuiController_30;
	// IconsProvider TableButtons::IconsProvider
	IconsProvider_t3884200715 * ___IconsProvider_31;
	// GenericDataSource`2<DonateButton,DonateCell> TableButtons::_donateDataSource
	GenericDataSource_2_t3164199101 * ____donateDataSource_32;
	// GenericDataSource`2<UpgradeButton,UpgradeCell> TableButtons::_passiveDataSource
	GenericDataSource_2_t837417641 * ____passiveDataSource_33;
	// GenericDataSource`2<UpgradeButton,UpgradeCell> TableButtons::_billDataSource
	GenericDataSource_2_t837417641 * ____billDataSource_34;
	// GenericDataSource`2<UpgradeButton,UpgradeCell> TableButtons::_safeDataSource
	GenericDataSource_2_t837417641 * ____safeDataSource_35;
	// GenericDataSource`2<SettingsButton,SettingsCell> TableButtons::_settingsDataSource
	GenericDataSource_2_t337768557 * ____settingsDataSource_36;
	// TutorialPlayer TableButtons::TutorialPlayer
	TutorialPlayer_t4281139199 * ___TutorialPlayer_37;
	// BarygaVkController TableButtons::BarygaVkController
	BarygaVkController_t3617163921 * ___BarygaVkController_38;
	// AchievementController TableButtons::AchievementController
	AchievementController_t3677499403 * ___AchievementController_39;
	// GameController TableButtons::GameController
	GameController_t2782302542 * ___GameController_40;
	// IVkConnector TableButtons::vkConnector
	Il2CppObject * ___vkConnector_41;
	// FbController TableButtons::fbConnector
	FbController_t3069175192 * ___fbConnector_42;
	// UnityEngine.UI.Image[] TableButtons::_premiumImages
	ImageU5BU5D_t2279835437* ____premiumImages_43;

public:
	inline static int32_t get_offset_of_StatusTableButtons_0() { return static_cast<int32_t>(offsetof(TableButtons_t1868573683, ___StatusTableButtons_0)); }
	inline ReactiveProperty_1_t4116728855 * get_StatusTableButtons_0() const { return ___StatusTableButtons_0; }
	inline ReactiveProperty_1_t4116728855 ** get_address_of_StatusTableButtons_0() { return &___StatusTableButtons_0; }
	inline void set_StatusTableButtons_0(ReactiveProperty_1_t4116728855 * value)
	{
		___StatusTableButtons_0 = value;
		Il2CppCodeGenWriteBarrier(&___StatusTableButtons_0, value);
	}

	inline static int32_t get_offset_of_analyticsController_1() { return static_cast<int32_t>(offsetof(TableButtons_t1868573683, ___analyticsController_1)); }
	inline AnalyticsController_t2265609122 * get_analyticsController_1() const { return ___analyticsController_1; }
	inline AnalyticsController_t2265609122 ** get_address_of_analyticsController_1() { return &___analyticsController_1; }
	inline void set_analyticsController_1(AnalyticsController_t2265609122 * value)
	{
		___analyticsController_1 = value;
		Il2CppCodeGenWriteBarrier(&___analyticsController_1, value);
	}

	inline static int32_t get_offset_of_NotificationDictionary_2() { return static_cast<int32_t>(offsetof(TableButtons_t1868573683, ___NotificationDictionary_2)); }
	inline Dictionary_2_t190145490 * get_NotificationDictionary_2() const { return ___NotificationDictionary_2; }
	inline Dictionary_2_t190145490 ** get_address_of_NotificationDictionary_2() { return &___NotificationDictionary_2; }
	inline void set_NotificationDictionary_2(Dictionary_2_t190145490 * value)
	{
		___NotificationDictionary_2 = value;
		Il2CppCodeGenWriteBarrier(&___NotificationDictionary_2, value);
	}

	inline static int32_t get_offset_of_fblogStat_3() { return static_cast<int32_t>(offsetof(TableButtons_t1868573683, ___fblogStat_3)); }
	inline String_t* get_fblogStat_3() const { return ___fblogStat_3; }
	inline String_t** get_address_of_fblogStat_3() { return &___fblogStat_3; }
	inline void set_fblogStat_3(String_t* value)
	{
		___fblogStat_3 = value;
		Il2CppCodeGenWriteBarrier(&___fblogStat_3, value);
	}

	inline static int32_t get_offset_of_DiContainer_4() { return static_cast<int32_t>(offsetof(TableButtons_t1868573683, ___DiContainer_4)); }
	inline DiContainer_t2383114449 * get_DiContainer_4() const { return ___DiContainer_4; }
	inline DiContainer_t2383114449 ** get_address_of_DiContainer_4() { return &___DiContainer_4; }
	inline void set_DiContainer_4(DiContainer_t2383114449 * value)
	{
		___DiContainer_4 = value;
		Il2CppCodeGenWriteBarrier(&___DiContainer_4, value);
	}

	inline static int32_t get_offset_of_WindowManager_5() { return static_cast<int32_t>(offsetof(TableButtons_t1868573683, ___WindowManager_5)); }
	inline WindowManager_t3316821373 * get_WindowManager_5() const { return ___WindowManager_5; }
	inline WindowManager_t3316821373 ** get_address_of_WindowManager_5() { return &___WindowManager_5; }
	inline void set_WindowManager_5(WindowManager_t3316821373 * value)
	{
		___WindowManager_5 = value;
		Il2CppCodeGenWriteBarrier(&___WindowManager_5, value);
	}

	inline static int32_t get_offset_of_SoundICon_6() { return static_cast<int32_t>(offsetof(TableButtons_t1868573683, ___SoundICon_6)); }
	inline Sprite_t4006040370 * get_SoundICon_6() const { return ___SoundICon_6; }
	inline Sprite_t4006040370 ** get_address_of_SoundICon_6() { return &___SoundICon_6; }
	inline void set_SoundICon_6(Sprite_t4006040370 * value)
	{
		___SoundICon_6 = value;
		Il2CppCodeGenWriteBarrier(&___SoundICon_6, value);
	}

	inline static int32_t get_offset_of_TableView_7() { return static_cast<int32_t>(offsetof(TableButtons_t1868573683, ___TableView_7)); }
	inline TableView_t692333993 * get_TableView_7() const { return ___TableView_7; }
	inline TableView_t692333993 ** get_address_of_TableView_7() { return &___TableView_7; }
	inline void set_TableView_7(TableView_t692333993 * value)
	{
		___TableView_7 = value;
		Il2CppCodeGenWriteBarrier(&___TableView_7, value);
	}

	inline static int32_t get_offset_of__billButtons_8() { return static_cast<int32_t>(offsetof(TableButtons_t1868573683, ____billButtons_8)); }
	inline InorganicCollection_1_t4089338440 * get__billButtons_8() const { return ____billButtons_8; }
	inline InorganicCollection_1_t4089338440 ** get_address_of__billButtons_8() { return &____billButtons_8; }
	inline void set__billButtons_8(InorganicCollection_1_t4089338440 * value)
	{
		____billButtons_8 = value;
		Il2CppCodeGenWriteBarrier(&____billButtons_8, value);
	}

	inline static int32_t get_offset_of__passiveButtons_9() { return static_cast<int32_t>(offsetof(TableButtons_t1868573683, ____passiveButtons_9)); }
	inline InorganicCollection_1_t4089338440 * get__passiveButtons_9() const { return ____passiveButtons_9; }
	inline InorganicCollection_1_t4089338440 ** get_address_of__passiveButtons_9() { return &____passiveButtons_9; }
	inline void set__passiveButtons_9(InorganicCollection_1_t4089338440 * value)
	{
		____passiveButtons_9 = value;
		Il2CppCodeGenWriteBarrier(&____passiveButtons_9, value);
	}

	inline static int32_t get_offset_of__safeButtons_10() { return static_cast<int32_t>(offsetof(TableButtons_t1868573683, ____safeButtons_10)); }
	inline InorganicCollection_1_t4089338440 * get__safeButtons_10() const { return ____safeButtons_10; }
	inline InorganicCollection_1_t4089338440 ** get_address_of__safeButtons_10() { return &____safeButtons_10; }
	inline void set__safeButtons_10(InorganicCollection_1_t4089338440 * value)
	{
		____safeButtons_10 = value;
		Il2CppCodeGenWriteBarrier(&____safeButtons_10, value);
	}

	inline static int32_t get_offset_of__donateButtons_11() { return static_cast<int32_t>(offsetof(TableButtons_t1868573683, ____donateButtons_11)); }
	inline InorganicCollection_1_t3284624539 * get__donateButtons_11() const { return ____donateButtons_11; }
	inline InorganicCollection_1_t3284624539 ** get_address_of__donateButtons_11() { return &____donateButtons_11; }
	inline void set__donateButtons_11(InorganicCollection_1_t3284624539 * value)
	{
		____donateButtons_11 = value;
		Il2CppCodeGenWriteBarrier(&____donateButtons_11, value);
	}

	inline static int32_t get_offset_of__settingsButtons_12() { return static_cast<int32_t>(offsetof(TableButtons_t1868573683, ____settingsButtons_12)); }
	inline InorganicCollection_1_t3529127759 * get__settingsButtons_12() const { return ____settingsButtons_12; }
	inline InorganicCollection_1_t3529127759 ** get_address_of__settingsButtons_12() { return &____settingsButtons_12; }
	inline void set__settingsButtons_12(InorganicCollection_1_t3529127759 * value)
	{
		____settingsButtons_12 = value;
		Il2CppCodeGenWriteBarrier(&____settingsButtons_12, value);
	}

	inline static int32_t get_offset_of__tableRectTransform_13() { return static_cast<int32_t>(offsetof(TableButtons_t1868573683, ____tableRectTransform_13)); }
	inline RectTransform_t3317474837 * get__tableRectTransform_13() const { return ____tableRectTransform_13; }
	inline RectTransform_t3317474837 ** get_address_of__tableRectTransform_13() { return &____tableRectTransform_13; }
	inline void set__tableRectTransform_13(RectTransform_t3317474837 * value)
	{
		____tableRectTransform_13 = value;
		Il2CppCodeGenWriteBarrier(&____tableRectTransform_13, value);
	}

	inline static int32_t get_offset_of__tableDataConnections_14() { return static_cast<int32_t>(offsetof(TableButtons_t1868573683, ____tableDataConnections_14)); }
	inline ConnectionCollector_t444796719 * get__tableDataConnections_14() const { return ____tableDataConnections_14; }
	inline ConnectionCollector_t444796719 ** get_address_of__tableDataConnections_14() { return &____tableDataConnections_14; }
	inline void set__tableDataConnections_14(ConnectionCollector_t444796719 * value)
	{
		____tableDataConnections_14 = value;
		Il2CppCodeGenWriteBarrier(&____tableDataConnections_14, value);
	}

	inline static int32_t get_offset_of__guiController_15() { return static_cast<int32_t>(offsetof(TableButtons_t1868573683, ____guiController_15)); }
	inline GuiController_t1495593751 * get__guiController_15() const { return ____guiController_15; }
	inline GuiController_t1495593751 ** get_address_of__guiController_15() { return &____guiController_15; }
	inline void set__guiController_15(GuiController_t1495593751 * value)
	{
		____guiController_15 = value;
		Il2CppCodeGenWriteBarrier(&____guiController_15, value);
	}

	inline static int32_t get_offset_of_GlobalStat_16() { return static_cast<int32_t>(offsetof(TableButtons_t1868573683, ___GlobalStat_16)); }
	inline GlobalStat_t1134678199 * get_GlobalStat_16() const { return ___GlobalStat_16; }
	inline GlobalStat_t1134678199 ** get_address_of_GlobalStat_16() { return &___GlobalStat_16; }
	inline void set_GlobalStat_16(GlobalStat_t1134678199 * value)
	{
		___GlobalStat_16 = value;
		Il2CppCodeGenWriteBarrier(&___GlobalStat_16, value);
	}

	inline static int32_t get_offset_of_MenuName_17() { return static_cast<int32_t>(offsetof(TableButtons_t1868573683, ___MenuName_17)); }
	inline Text_t3286458198 * get_MenuName_17() const { return ___MenuName_17; }
	inline Text_t3286458198 ** get_address_of_MenuName_17() { return &___MenuName_17; }
	inline void set_MenuName_17(Text_t3286458198 * value)
	{
		___MenuName_17 = value;
		Il2CppCodeGenWriteBarrier(&___MenuName_17, value);
	}

	inline static int32_t get_offset_of_TableParent_18() { return static_cast<int32_t>(offsetof(TableButtons_t1868573683, ___TableParent_18)); }
	inline GameObject_t4012695102 * get_TableParent_18() const { return ___TableParent_18; }
	inline GameObject_t4012695102 ** get_address_of_TableParent_18() { return &___TableParent_18; }
	inline void set_TableParent_18(GameObject_t4012695102 * value)
	{
		___TableParent_18 = value;
		Il2CppCodeGenWriteBarrier(&___TableParent_18, value);
	}

	inline static int32_t get_offset_of_MenuDescription_19() { return static_cast<int32_t>(offsetof(TableButtons_t1868573683, ___MenuDescription_19)); }
	inline Text_t3286458198 * get_MenuDescription_19() const { return ___MenuDescription_19; }
	inline Text_t3286458198 ** get_address_of_MenuDescription_19() { return &___MenuDescription_19; }
	inline void set_MenuDescription_19(Text_t3286458198 * value)
	{
		___MenuDescription_19 = value;
		Il2CppCodeGenWriteBarrier(&___MenuDescription_19, value);
	}

	inline static int32_t get_offset_of_premiumIcon_20() { return static_cast<int32_t>(offsetof(TableButtons_t1868573683, ___premiumIcon_20)); }
	inline Image_t3354615620 * get_premiumIcon_20() const { return ___premiumIcon_20; }
	inline Image_t3354615620 ** get_address_of_premiumIcon_20() { return &___premiumIcon_20; }
	inline void set_premiumIcon_20(Image_t3354615620 * value)
	{
		___premiumIcon_20 = value;
		Il2CppCodeGenWriteBarrier(&___premiumIcon_20, value);
	}

	inline static int32_t get_offset_of_premium_21() { return static_cast<int32_t>(offsetof(TableButtons_t1868573683, ___premium_21)); }
	inline Image_t3354615620 * get_premium_21() const { return ___premium_21; }
	inline Image_t3354615620 ** get_address_of_premium_21() { return &___premium_21; }
	inline void set_premium_21(Image_t3354615620 * value)
	{
		___premium_21 = value;
		Il2CppCodeGenWriteBarrier(&___premium_21, value);
	}

	inline static int32_t get_offset_of_premiumButton_22() { return static_cast<int32_t>(offsetof(TableButtons_t1868573683, ___premiumButton_22)); }
	inline Button_t990034267 * get_premiumButton_22() const { return ___premiumButton_22; }
	inline Button_t990034267 ** get_address_of_premiumButton_22() { return &___premiumButton_22; }
	inline void set_premiumButton_22(Button_t990034267 * value)
	{
		___premiumButton_22 = value;
		Il2CppCodeGenWriteBarrier(&___premiumButton_22, value);
	}

	inline static int32_t get_offset_of_premiumText_23() { return static_cast<int32_t>(offsetof(TableButtons_t1868573683, ___premiumText_23)); }
	inline Text_t3286458198 * get_premiumText_23() const { return ___premiumText_23; }
	inline Text_t3286458198 ** get_address_of_premiumText_23() { return &___premiumText_23; }
	inline void set_premiumText_23(Text_t3286458198 * value)
	{
		___premiumText_23 = value;
		Il2CppCodeGenWriteBarrier(&___premiumText_23, value);
	}

	inline static int32_t get_offset_of_premiumButtonText_24() { return static_cast<int32_t>(offsetof(TableButtons_t1868573683, ___premiumButtonText_24)); }
	inline Text_t3286458198 * get_premiumButtonText_24() const { return ___premiumButtonText_24; }
	inline Text_t3286458198 ** get_address_of_premiumButtonText_24() { return &___premiumButtonText_24; }
	inline void set_premiumButtonText_24(Text_t3286458198 * value)
	{
		___premiumButtonText_24 = value;
		Il2CppCodeGenWriteBarrier(&___premiumButtonText_24, value);
	}

	inline static int32_t get_offset_of_premiumDescriptionText_25() { return static_cast<int32_t>(offsetof(TableButtons_t1868573683, ___premiumDescriptionText_25)); }
	inline Text_t3286458198 * get_premiumDescriptionText_25() const { return ___premiumDescriptionText_25; }
	inline Text_t3286458198 ** get_address_of_premiumDescriptionText_25() { return &___premiumDescriptionText_25; }
	inline void set_premiumDescriptionText_25(Text_t3286458198 * value)
	{
		___premiumDescriptionText_25 = value;
		Il2CppCodeGenWriteBarrier(&___premiumDescriptionText_25, value);
	}

	inline static int32_t get_offset_of_OligarchParameters_26() { return static_cast<int32_t>(offsetof(TableButtons_t1868573683, ___OligarchParameters_26)); }
	inline OligarchParameters_t821144443 * get_OligarchParameters_26() const { return ___OligarchParameters_26; }
	inline OligarchParameters_t821144443 ** get_address_of_OligarchParameters_26() { return &___OligarchParameters_26; }
	inline void set_OligarchParameters_26(OligarchParameters_t821144443 * value)
	{
		___OligarchParameters_26 = value;
		Il2CppCodeGenWriteBarrier(&___OligarchParameters_26, value);
	}

	inline static int32_t get_offset_of_HeaderGameObject_27() { return static_cast<int32_t>(offsetof(TableButtons_t1868573683, ___HeaderGameObject_27)); }
	inline GameObject_t4012695102 * get_HeaderGameObject_27() const { return ___HeaderGameObject_27; }
	inline GameObject_t4012695102 ** get_address_of_HeaderGameObject_27() { return &___HeaderGameObject_27; }
	inline void set_HeaderGameObject_27(GameObject_t4012695102 * value)
	{
		___HeaderGameObject_27 = value;
		Il2CppCodeGenWriteBarrier(&___HeaderGameObject_27, value);
	}

	inline static int32_t get_offset_of_TableMask_28() { return static_cast<int32_t>(offsetof(TableButtons_t1868573683, ___TableMask_28)); }
	inline Mask_t3286245653 * get_TableMask_28() const { return ___TableMask_28; }
	inline Mask_t3286245653 ** get_address_of_TableMask_28() { return &___TableMask_28; }
	inline void set_TableMask_28(Mask_t3286245653 * value)
	{
		___TableMask_28 = value;
		Il2CppCodeGenWriteBarrier(&___TableMask_28, value);
	}

	inline static int32_t get_offset_of_TabIMGMask_29() { return static_cast<int32_t>(offsetof(TableButtons_t1868573683, ___TabIMGMask_29)); }
	inline Image_t3354615620 * get_TabIMGMask_29() const { return ___TabIMGMask_29; }
	inline Image_t3354615620 ** get_address_of_TabIMGMask_29() { return &___TabIMGMask_29; }
	inline void set_TabIMGMask_29(Image_t3354615620 * value)
	{
		___TabIMGMask_29 = value;
		Il2CppCodeGenWriteBarrier(&___TabIMGMask_29, value);
	}

	inline static int32_t get_offset_of_GuiController_30() { return static_cast<int32_t>(offsetof(TableButtons_t1868573683, ___GuiController_30)); }
	inline GuiController_t1495593751 * get_GuiController_30() const { return ___GuiController_30; }
	inline GuiController_t1495593751 ** get_address_of_GuiController_30() { return &___GuiController_30; }
	inline void set_GuiController_30(GuiController_t1495593751 * value)
	{
		___GuiController_30 = value;
		Il2CppCodeGenWriteBarrier(&___GuiController_30, value);
	}

	inline static int32_t get_offset_of_IconsProvider_31() { return static_cast<int32_t>(offsetof(TableButtons_t1868573683, ___IconsProvider_31)); }
	inline IconsProvider_t3884200715 * get_IconsProvider_31() const { return ___IconsProvider_31; }
	inline IconsProvider_t3884200715 ** get_address_of_IconsProvider_31() { return &___IconsProvider_31; }
	inline void set_IconsProvider_31(IconsProvider_t3884200715 * value)
	{
		___IconsProvider_31 = value;
		Il2CppCodeGenWriteBarrier(&___IconsProvider_31, value);
	}

	inline static int32_t get_offset_of__donateDataSource_32() { return static_cast<int32_t>(offsetof(TableButtons_t1868573683, ____donateDataSource_32)); }
	inline GenericDataSource_2_t3164199101 * get__donateDataSource_32() const { return ____donateDataSource_32; }
	inline GenericDataSource_2_t3164199101 ** get_address_of__donateDataSource_32() { return &____donateDataSource_32; }
	inline void set__donateDataSource_32(GenericDataSource_2_t3164199101 * value)
	{
		____donateDataSource_32 = value;
		Il2CppCodeGenWriteBarrier(&____donateDataSource_32, value);
	}

	inline static int32_t get_offset_of__passiveDataSource_33() { return static_cast<int32_t>(offsetof(TableButtons_t1868573683, ____passiveDataSource_33)); }
	inline GenericDataSource_2_t837417641 * get__passiveDataSource_33() const { return ____passiveDataSource_33; }
	inline GenericDataSource_2_t837417641 ** get_address_of__passiveDataSource_33() { return &____passiveDataSource_33; }
	inline void set__passiveDataSource_33(GenericDataSource_2_t837417641 * value)
	{
		____passiveDataSource_33 = value;
		Il2CppCodeGenWriteBarrier(&____passiveDataSource_33, value);
	}

	inline static int32_t get_offset_of__billDataSource_34() { return static_cast<int32_t>(offsetof(TableButtons_t1868573683, ____billDataSource_34)); }
	inline GenericDataSource_2_t837417641 * get__billDataSource_34() const { return ____billDataSource_34; }
	inline GenericDataSource_2_t837417641 ** get_address_of__billDataSource_34() { return &____billDataSource_34; }
	inline void set__billDataSource_34(GenericDataSource_2_t837417641 * value)
	{
		____billDataSource_34 = value;
		Il2CppCodeGenWriteBarrier(&____billDataSource_34, value);
	}

	inline static int32_t get_offset_of__safeDataSource_35() { return static_cast<int32_t>(offsetof(TableButtons_t1868573683, ____safeDataSource_35)); }
	inline GenericDataSource_2_t837417641 * get__safeDataSource_35() const { return ____safeDataSource_35; }
	inline GenericDataSource_2_t837417641 ** get_address_of__safeDataSource_35() { return &____safeDataSource_35; }
	inline void set__safeDataSource_35(GenericDataSource_2_t837417641 * value)
	{
		____safeDataSource_35 = value;
		Il2CppCodeGenWriteBarrier(&____safeDataSource_35, value);
	}

	inline static int32_t get_offset_of__settingsDataSource_36() { return static_cast<int32_t>(offsetof(TableButtons_t1868573683, ____settingsDataSource_36)); }
	inline GenericDataSource_2_t337768557 * get__settingsDataSource_36() const { return ____settingsDataSource_36; }
	inline GenericDataSource_2_t337768557 ** get_address_of__settingsDataSource_36() { return &____settingsDataSource_36; }
	inline void set__settingsDataSource_36(GenericDataSource_2_t337768557 * value)
	{
		____settingsDataSource_36 = value;
		Il2CppCodeGenWriteBarrier(&____settingsDataSource_36, value);
	}

	inline static int32_t get_offset_of_TutorialPlayer_37() { return static_cast<int32_t>(offsetof(TableButtons_t1868573683, ___TutorialPlayer_37)); }
	inline TutorialPlayer_t4281139199 * get_TutorialPlayer_37() const { return ___TutorialPlayer_37; }
	inline TutorialPlayer_t4281139199 ** get_address_of_TutorialPlayer_37() { return &___TutorialPlayer_37; }
	inline void set_TutorialPlayer_37(TutorialPlayer_t4281139199 * value)
	{
		___TutorialPlayer_37 = value;
		Il2CppCodeGenWriteBarrier(&___TutorialPlayer_37, value);
	}

	inline static int32_t get_offset_of_BarygaVkController_38() { return static_cast<int32_t>(offsetof(TableButtons_t1868573683, ___BarygaVkController_38)); }
	inline BarygaVkController_t3617163921 * get_BarygaVkController_38() const { return ___BarygaVkController_38; }
	inline BarygaVkController_t3617163921 ** get_address_of_BarygaVkController_38() { return &___BarygaVkController_38; }
	inline void set_BarygaVkController_38(BarygaVkController_t3617163921 * value)
	{
		___BarygaVkController_38 = value;
		Il2CppCodeGenWriteBarrier(&___BarygaVkController_38, value);
	}

	inline static int32_t get_offset_of_AchievementController_39() { return static_cast<int32_t>(offsetof(TableButtons_t1868573683, ___AchievementController_39)); }
	inline AchievementController_t3677499403 * get_AchievementController_39() const { return ___AchievementController_39; }
	inline AchievementController_t3677499403 ** get_address_of_AchievementController_39() { return &___AchievementController_39; }
	inline void set_AchievementController_39(AchievementController_t3677499403 * value)
	{
		___AchievementController_39 = value;
		Il2CppCodeGenWriteBarrier(&___AchievementController_39, value);
	}

	inline static int32_t get_offset_of_GameController_40() { return static_cast<int32_t>(offsetof(TableButtons_t1868573683, ___GameController_40)); }
	inline GameController_t2782302542 * get_GameController_40() const { return ___GameController_40; }
	inline GameController_t2782302542 ** get_address_of_GameController_40() { return &___GameController_40; }
	inline void set_GameController_40(GameController_t2782302542 * value)
	{
		___GameController_40 = value;
		Il2CppCodeGenWriteBarrier(&___GameController_40, value);
	}

	inline static int32_t get_offset_of_vkConnector_41() { return static_cast<int32_t>(offsetof(TableButtons_t1868573683, ___vkConnector_41)); }
	inline Il2CppObject * get_vkConnector_41() const { return ___vkConnector_41; }
	inline Il2CppObject ** get_address_of_vkConnector_41() { return &___vkConnector_41; }
	inline void set_vkConnector_41(Il2CppObject * value)
	{
		___vkConnector_41 = value;
		Il2CppCodeGenWriteBarrier(&___vkConnector_41, value);
	}

	inline static int32_t get_offset_of_fbConnector_42() { return static_cast<int32_t>(offsetof(TableButtons_t1868573683, ___fbConnector_42)); }
	inline FbController_t3069175192 * get_fbConnector_42() const { return ___fbConnector_42; }
	inline FbController_t3069175192 ** get_address_of_fbConnector_42() { return &___fbConnector_42; }
	inline void set_fbConnector_42(FbController_t3069175192 * value)
	{
		___fbConnector_42 = value;
		Il2CppCodeGenWriteBarrier(&___fbConnector_42, value);
	}

	inline static int32_t get_offset_of__premiumImages_43() { return static_cast<int32_t>(offsetof(TableButtons_t1868573683, ____premiumImages_43)); }
	inline ImageU5BU5D_t2279835437* get__premiumImages_43() const { return ____premiumImages_43; }
	inline ImageU5BU5D_t2279835437** get_address_of__premiumImages_43() { return &____premiumImages_43; }
	inline void set__premiumImages_43(ImageU5BU5D_t2279835437* value)
	{
		____premiumImages_43 = value;
		Il2CppCodeGenWriteBarrier(&____premiumImages_43, value);
	}
};

struct TableButtons_t1868573683_StaticFields
{
public:
	// System.Action`2<SettingsButton,SettingsCell> TableButtons::<>f__am$cache2C
	Action_2_t2763552026 * ___U3CU3Ef__amU24cache2C_44;
	// System.Action`2<DonateButton,DonateCell> TableButtons::<>f__am$cache2D
	Action_2_t1295015274 * ___U3CU3Ef__amU24cache2D_45;
	// System.Func`3<System.Double,System.Int32,System.Double> TableButtons::<>f__am$cache2E
	Func_3_t2695431030 * ___U3CU3Ef__amU24cache2E_46;
	// System.Func`3<System.DateTime,System.Int64,<>__AnonType5`2<System.DateTime,System.Int64>> TableButtons::<>f__am$cache2F
	Func_3_t1001475269 * ___U3CU3Ef__amU24cache2F_47;
	// System.Func`3<System.Double,System.Int32,<>__AnonType6`2<System.Double,System.Int32>> TableButtons::<>f__am$cache30
	Func_3_t1497502332 * ___U3CU3Ef__amU24cache30_48;
	// System.Action`1<DonateCell> TableButtons::<>f__am$cache31
	Action_1_t2946927090 * ___U3CU3Ef__amU24cache31_49;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2C_44() { return static_cast<int32_t>(offsetof(TableButtons_t1868573683_StaticFields, ___U3CU3Ef__amU24cache2C_44)); }
	inline Action_2_t2763552026 * get_U3CU3Ef__amU24cache2C_44() const { return ___U3CU3Ef__amU24cache2C_44; }
	inline Action_2_t2763552026 ** get_address_of_U3CU3Ef__amU24cache2C_44() { return &___U3CU3Ef__amU24cache2C_44; }
	inline void set_U3CU3Ef__amU24cache2C_44(Action_2_t2763552026 * value)
	{
		___U3CU3Ef__amU24cache2C_44 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2C_44, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2D_45() { return static_cast<int32_t>(offsetof(TableButtons_t1868573683_StaticFields, ___U3CU3Ef__amU24cache2D_45)); }
	inline Action_2_t1295015274 * get_U3CU3Ef__amU24cache2D_45() const { return ___U3CU3Ef__amU24cache2D_45; }
	inline Action_2_t1295015274 ** get_address_of_U3CU3Ef__amU24cache2D_45() { return &___U3CU3Ef__amU24cache2D_45; }
	inline void set_U3CU3Ef__amU24cache2D_45(Action_2_t1295015274 * value)
	{
		___U3CU3Ef__amU24cache2D_45 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2D_45, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2E_46() { return static_cast<int32_t>(offsetof(TableButtons_t1868573683_StaticFields, ___U3CU3Ef__amU24cache2E_46)); }
	inline Func_3_t2695431030 * get_U3CU3Ef__amU24cache2E_46() const { return ___U3CU3Ef__amU24cache2E_46; }
	inline Func_3_t2695431030 ** get_address_of_U3CU3Ef__amU24cache2E_46() { return &___U3CU3Ef__amU24cache2E_46; }
	inline void set_U3CU3Ef__amU24cache2E_46(Func_3_t2695431030 * value)
	{
		___U3CU3Ef__amU24cache2E_46 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2E_46, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2F_47() { return static_cast<int32_t>(offsetof(TableButtons_t1868573683_StaticFields, ___U3CU3Ef__amU24cache2F_47)); }
	inline Func_3_t1001475269 * get_U3CU3Ef__amU24cache2F_47() const { return ___U3CU3Ef__amU24cache2F_47; }
	inline Func_3_t1001475269 ** get_address_of_U3CU3Ef__amU24cache2F_47() { return &___U3CU3Ef__amU24cache2F_47; }
	inline void set_U3CU3Ef__amU24cache2F_47(Func_3_t1001475269 * value)
	{
		___U3CU3Ef__amU24cache2F_47 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2F_47, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache30_48() { return static_cast<int32_t>(offsetof(TableButtons_t1868573683_StaticFields, ___U3CU3Ef__amU24cache30_48)); }
	inline Func_3_t1497502332 * get_U3CU3Ef__amU24cache30_48() const { return ___U3CU3Ef__amU24cache30_48; }
	inline Func_3_t1497502332 ** get_address_of_U3CU3Ef__amU24cache30_48() { return &___U3CU3Ef__amU24cache30_48; }
	inline void set_U3CU3Ef__amU24cache30_48(Func_3_t1497502332 * value)
	{
		___U3CU3Ef__amU24cache30_48 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache30_48, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache31_49() { return static_cast<int32_t>(offsetof(TableButtons_t1868573683_StaticFields, ___U3CU3Ef__amU24cache31_49)); }
	inline Action_1_t2946927090 * get_U3CU3Ef__amU24cache31_49() const { return ___U3CU3Ef__amU24cache31_49; }
	inline Action_1_t2946927090 ** get_address_of_U3CU3Ef__amU24cache31_49() { return &___U3CU3Ef__amU24cache31_49; }
	inline void set_U3CU3Ef__amU24cache31_49(Action_1_t2946927090 * value)
	{
		___U3CU3Ef__amU24cache31_49 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache31_49, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
