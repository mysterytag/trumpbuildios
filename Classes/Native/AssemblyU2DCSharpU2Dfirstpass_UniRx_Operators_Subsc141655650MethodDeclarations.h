﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SubscribeOnMainThreadObservable`1/<SubscribeCore>c__AnonStorey94<System.Object>
struct U3CSubscribeCoreU3Ec__AnonStorey94_t141655650;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.SubscribeOnMainThreadObservable`1/<SubscribeCore>c__AnonStorey94<System.Object>::.ctor()
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey94__ctor_m3217542610_gshared (U3CSubscribeCoreU3Ec__AnonStorey94_t141655650 * __this, const MethodInfo* method);
#define U3CSubscribeCoreU3Ec__AnonStorey94__ctor_m3217542610(__this, method) ((  void (*) (U3CSubscribeCoreU3Ec__AnonStorey94_t141655650 *, const MethodInfo*))U3CSubscribeCoreU3Ec__AnonStorey94__ctor_m3217542610_gshared)(__this, method)
// System.Void UniRx.Operators.SubscribeOnMainThreadObservable`1/<SubscribeCore>c__AnonStorey94<System.Object>::<>m__C2(System.Int64)
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey94_U3CU3Em__C2_m1501286012_gshared (U3CSubscribeCoreU3Ec__AnonStorey94_t141655650 * __this, int64_t ____0, const MethodInfo* method);
#define U3CSubscribeCoreU3Ec__AnonStorey94_U3CU3Em__C2_m1501286012(__this, ____0, method) ((  void (*) (U3CSubscribeCoreU3Ec__AnonStorey94_t141655650 *, int64_t, const MethodInfo*))U3CSubscribeCoreU3Ec__AnonStorey94_U3CU3Em__C2_m1501286012_gshared)(__this, ____0, method)
