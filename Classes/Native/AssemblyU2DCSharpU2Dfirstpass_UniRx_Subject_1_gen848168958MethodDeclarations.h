﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen2775141040MethodDeclarations.h"

// System.Void UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData>::.ctor()
#define Subject_1__ctor_m4218960110(__this, method) ((  void (*) (Subject_1_t848168958 *, const MethodInfo*))Subject_1__ctor_m3752525464_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData>::get_HasObservers()
#define Subject_1_get_HasObservers_m3326284254(__this, method) ((  bool (*) (Subject_1_t848168958 *, const MethodInfo*))Subject_1_get_HasObservers_m2673131508_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData>::OnCompleted()
#define Subject_1_OnCompleted_m2103738872(__this, method) ((  void (*) (Subject_1_t848168958 *, const MethodInfo*))Subject_1_OnCompleted_m1083367458_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData>::OnError(System.Exception)
#define Subject_1_OnError_m3108241189(__this, ___error0, method) ((  void (*) (Subject_1_t848168958 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m1700032079_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData>::OnNext(T)
#define Subject_1_OnNext_m2802241558(__this, ___value0, method) ((  void (*) (Subject_1_t848168958 *, PointerEventData_t3205101634 *, const MethodInfo*))Subject_1_OnNext_m1235145536_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData>::Subscribe(UniRx.IObserver`1<T>)
#define Subject_1_Subscribe_m526806893(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t848168958 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m2428743831_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData>::Dispose()
#define Subject_1_Dispose_m4164788651(__this, method) ((  void (*) (Subject_1_t848168958 *, const MethodInfo*))Subject_1_Dispose_m2597692629_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData>::ThrowIfDisposed()
#define Subject_1_ThrowIfDisposed_m2380296244(__this, method) ((  void (*) (Subject_1_t848168958 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m956279134_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData>::IsRequiredSubscribeOnCurrentThread()
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m1521353557(__this, method) ((  bool (*) (Subject_1_t848168958 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m3969161195_gshared)(__this, method)
