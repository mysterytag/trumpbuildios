﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.DoOnTerminateObservable`1<System.Object>
struct DoOnTerminateObservable_1_t3263525393;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Action
struct Action_t437523947;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"

// System.Void UniRx.Operators.DoOnTerminateObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Action)
extern "C"  void DoOnTerminateObservable_1__ctor_m1619754013_gshared (DoOnTerminateObservable_1_t3263525393 * __this, Il2CppObject* ___source0, Action_t437523947 * ___onTerminate1, const MethodInfo* method);
#define DoOnTerminateObservable_1__ctor_m1619754013(__this, ___source0, ___onTerminate1, method) ((  void (*) (DoOnTerminateObservable_1_t3263525393 *, Il2CppObject*, Action_t437523947 *, const MethodInfo*))DoOnTerminateObservable_1__ctor_m1619754013_gshared)(__this, ___source0, ___onTerminate1, method)
// System.IDisposable UniRx.Operators.DoOnTerminateObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * DoOnTerminateObservable_1_SubscribeCore_m2956385271_gshared (DoOnTerminateObservable_1_t3263525393 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define DoOnTerminateObservable_1_SubscribeCore_m2956385271(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (DoOnTerminateObservable_1_t3263525393 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))DoOnTerminateObservable_1_SubscribeCore_m2956385271_gshared)(__this, ___observer0, ___cancel1, method)
