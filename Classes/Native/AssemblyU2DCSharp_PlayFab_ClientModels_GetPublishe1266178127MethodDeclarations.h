﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetPublisherDataRequest
struct GetPublisherDataRequest_t1266178127;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.GetPublisherDataRequest::.ctor()
extern "C"  void GetPublisherDataRequest__ctor_m3868250682 (GetPublisherDataRequest_t1266178127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.GetPublisherDataRequest::get_Keys()
extern "C"  List_1_t1765447871 * GetPublisherDataRequest_get_Keys_m2168981560 (GetPublisherDataRequest_t1266178127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetPublisherDataRequest::set_Keys(System.Collections.Generic.List`1<System.String>)
extern "C"  void GetPublisherDataRequest_set_Keys_m766881007 (GetPublisherDataRequest_t1266178127 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
