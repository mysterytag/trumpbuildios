﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BioCollection`1<System.Object>
struct BioCollection_1_t694114648;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t3709260776;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t1634065389;
// IProcess
struct IProcess_t4026237414;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// IOnceEmptyStream
struct IOnceEmptyStream_t172682851;
// System.IDisposable
struct IDisposable_t1628921374;
// IOrganism
struct IOrganism_t2580843579;
// IRoot
struct IRoot_t69970123;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// System.Object
struct Il2CppObject;
// IStream`1<System.Int32>
struct IStream_1_t3400105778;
// IStream`1<UniRx.Unit>
struct IStream_1_t3110977029;
// IStream`1<UniRx.CollectionAddEvent`1<System.Object>>
struct IStream_1_t2969212978;
// IStream`1<CollectionMoveEvent`1<System.Object>>
struct IStream_1_t4124746372;
// IStream`1<CollectionRemoveEvent`1<System.Object>>
struct IStream_1_t1450950249;
// IStream`1<CollectionReplaceEvent`1<System.Object>>
struct IStream_1_t3050063061;
// System.Action
struct Action_t437523947;
// ICell`1<System.Object>
struct ICell_1_t2388737397;
// System.Action`1<System.Collections.Generic.ICollection`1<System.Object>>
struct Action_1_t1451390511;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1302937806;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "System_Core_System_Action437523947.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"

// System.Void BioCollection`1<System.Object>::.ctor()
extern "C"  void BioCollection_1__ctor_m2236466102_gshared (BioCollection_1_t694114648 * __this, const MethodInfo* method);
#define BioCollection_1__ctor_m2236466102(__this, method) ((  void (*) (BioCollection_1_t694114648 *, const MethodInfo*))BioCollection_1__ctor_m2236466102_gshared)(__this, method)
// System.Void BioCollection`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void BioCollection_1__ctor_m2133799689_gshared (BioCollection_1_t694114648 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define BioCollection_1__ctor_m2133799689(__this, ___collection0, method) ((  void (*) (BioCollection_1_t694114648 *, Il2CppObject*, const MethodInfo*))BioCollection_1__ctor_m2133799689_gshared)(__this, ___collection0, method)
// System.Void BioCollection`1<System.Object>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void BioCollection_1__ctor_m2006345998_gshared (BioCollection_1_t694114648 * __this, List_1_t1634065389 * ___list0, const MethodInfo* method);
#define BioCollection_1__ctor_m2006345998(__this, ___list0, method) ((  void (*) (BioCollection_1_t694114648 *, List_1_t1634065389 *, const MethodInfo*))BioCollection_1__ctor_m2006345998_gshared)(__this, ___list0, method)
// IProcess BioCollection`1<System.Object>::Start(System.Collections.IEnumerator)
extern "C"  Il2CppObject * BioCollection_1_Start_m277942794_gshared (BioCollection_1_t694114648 * __this, Il2CppObject * ___bearutine0, const MethodInfo* method);
#define BioCollection_1_Start_m277942794(__this, ___bearutine0, method) ((  Il2CppObject * (*) (BioCollection_1_t694114648 *, Il2CppObject *, const MethodInfo*))BioCollection_1_Start_m277942794_gshared)(__this, ___bearutine0, method)
// IOnceEmptyStream BioCollection`1<System.Object>::get_fellAsleep()
extern "C"  Il2CppObject * BioCollection_1_get_fellAsleep_m825523110_gshared (BioCollection_1_t694114648 * __this, const MethodInfo* method);
#define BioCollection_1_get_fellAsleep_m825523110(__this, method) ((  Il2CppObject * (*) (BioCollection_1_t694114648 *, const MethodInfo*))BioCollection_1_get_fellAsleep_m825523110_gshared)(__this, method)
// System.Void BioCollection`1<System.Object>::DisposeWhenAsleep(System.IDisposable)
extern "C"  void BioCollection_1_DisposeWhenAsleep_m3673322585_gshared (BioCollection_1_t694114648 * __this, Il2CppObject * ___disposable0, const MethodInfo* method);
#define BioCollection_1_DisposeWhenAsleep_m3673322585(__this, ___disposable0, method) ((  void (*) (BioCollection_1_t694114648 *, Il2CppObject *, const MethodInfo*))BioCollection_1_DisposeWhenAsleep_m3673322585_gshared)(__this, ___disposable0, method)
// System.Boolean BioCollection`1<System.Object>::get_isAlive()
extern "C"  bool BioCollection_1_get_isAlive_m846113754_gshared (BioCollection_1_t694114648 * __this, const MethodInfo* method);
#define BioCollection_1_get_isAlive_m846113754(__this, method) ((  bool (*) (BioCollection_1_t694114648 *, const MethodInfo*))BioCollection_1_get_isAlive_m846113754_gshared)(__this, method)
// IOrganism BioCollection`1<System.Object>::get_carrier()
extern "C"  Il2CppObject * BioCollection_1_get_carrier_m256481163_gshared (BioCollection_1_t694114648 * __this, const MethodInfo* method);
#define BioCollection_1_get_carrier_m256481163(__this, method) ((  Il2CppObject * (*) (BioCollection_1_t694114648 *, const MethodInfo*))BioCollection_1_get_carrier_m256481163_gshared)(__this, method)
// System.Void BioCollection`1<System.Object>::set_carrier(IOrganism)
extern "C"  void BioCollection_1_set_carrier_m1747995176_gshared (BioCollection_1_t694114648 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define BioCollection_1_set_carrier_m1747995176(__this, ___value0, method) ((  void (*) (BioCollection_1_t694114648 *, Il2CppObject *, const MethodInfo*))BioCollection_1_set_carrier_m1747995176_gshared)(__this, ___value0, method)
// IRoot BioCollection`1<System.Object>::get_root()
extern "C"  Il2CppObject * BioCollection_1_get_root_m4145318913_gshared (BioCollection_1_t694114648 * __this, const MethodInfo* method);
#define BioCollection_1_get_root_m4145318913(__this, method) ((  Il2CppObject * (*) (BioCollection_1_t694114648 *, const MethodInfo*))BioCollection_1_get_root_m4145318913_gshared)(__this, method)
// System.Void BioCollection`1<System.Object>::set_root(IRoot)
extern "C"  void BioCollection_1_set_root_m346443866_gshared (BioCollection_1_t694114648 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define BioCollection_1_set_root_m346443866(__this, ___value0, method) ((  void (*) (BioCollection_1_t694114648 *, Il2CppObject *, const MethodInfo*))BioCollection_1_set_root_m346443866_gshared)(__this, ___value0, method)
// IOrganism BioCollection`1<System.Object>::Carrier()
extern "C"  Il2CppObject * BioCollection_1_Carrier_m738768532_gshared (BioCollection_1_t694114648 * __this, const MethodInfo* method);
#define BioCollection_1_Carrier_m738768532(__this, method) ((  Il2CppObject * (*) (BioCollection_1_t694114648 *, const MethodInfo*))BioCollection_1_Carrier_m738768532_gshared)(__this, method)
// System.Void BioCollection`1<System.Object>::Setup(IOrganism,IRoot)
extern "C"  void BioCollection_1_Setup_m3787613799_gshared (BioCollection_1_t694114648 * __this, Il2CppObject * ___c0, Il2CppObject * ___r1, const MethodInfo* method);
#define BioCollection_1_Setup_m3787613799(__this, ___c0, ___r1, method) ((  void (*) (BioCollection_1_t694114648 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))BioCollection_1_Setup_m3787613799_gshared)(__this, ___c0, ___r1, method)
// System.Void BioCollection`1<System.Object>::WakeUp()
extern "C"  void BioCollection_1_WakeUp_m1304019757_gshared (BioCollection_1_t694114648 * __this, const MethodInfo* method);
#define BioCollection_1_WakeUp_m1304019757(__this, method) ((  void (*) (BioCollection_1_t694114648 *, const MethodInfo*))BioCollection_1_WakeUp_m1304019757_gshared)(__this, method)
// System.Void BioCollection`1<System.Object>::Spread()
extern "C"  void BioCollection_1_Spread_m3551116353_gshared (BioCollection_1_t694114648 * __this, const MethodInfo* method);
#define BioCollection_1_Spread_m3551116353(__this, method) ((  void (*) (BioCollection_1_t694114648 *, const MethodInfo*))BioCollection_1_Spread_m3551116353_gshared)(__this, method)
// System.Void BioCollection`1<System.Object>::ToSleep()
extern "C"  void BioCollection_1_ToSleep_m1916513840_gshared (BioCollection_1_t694114648 * __this, const MethodInfo* method);
#define BioCollection_1_ToSleep_m1916513840(__this, method) ((  void (*) (BioCollection_1_t694114648 *, const MethodInfo*))BioCollection_1_ToSleep_m1916513840_gshared)(__this, method)
// System.Void BioCollection`1<System.Object>::ForEach(System.Action`1<T>)
extern "C"  void BioCollection_1_ForEach_m3261191230_gshared (BioCollection_1_t694114648 * __this, Action_1_t985559125 * ___action0, const MethodInfo* method);
#define BioCollection_1_ForEach_m3261191230(__this, ___action0, method) ((  void (*) (BioCollection_1_t694114648 *, Action_1_t985559125 *, const MethodInfo*))BioCollection_1_ForEach_m3261191230_gshared)(__this, ___action0, method)
// System.Void BioCollection`1<System.Object>::Collect(System.IDisposable)
extern "C"  void BioCollection_1_Collect_m1657079636_gshared (BioCollection_1_t694114648 * __this, Il2CppObject * ___tail0, const MethodInfo* method);
#define BioCollection_1_Collect_m1657079636(__this, ___tail0, method) ((  void (*) (BioCollection_1_t694114648 *, Il2CppObject *, const MethodInfo*))BioCollection_1_Collect_m1657079636_gshared)(__this, ___tail0, method)
// System.Void BioCollection`1<System.Object>::ClearItems()
extern "C"  void BioCollection_1_ClearItems_m2204296641_gshared (BioCollection_1_t694114648 * __this, const MethodInfo* method);
#define BioCollection_1_ClearItems_m2204296641(__this, method) ((  void (*) (BioCollection_1_t694114648 *, const MethodInfo*))BioCollection_1_ClearItems_m2204296641_gshared)(__this, method)
// System.Void BioCollection`1<System.Object>::InsertItem(System.Int32,T)
extern "C"  void BioCollection_1_InsertItem_m3700911267_gshared (BioCollection_1_t694114648 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define BioCollection_1_InsertItem_m3700911267(__this, ___index0, ___item1, method) ((  void (*) (BioCollection_1_t694114648 *, int32_t, Il2CppObject *, const MethodInfo*))BioCollection_1_InsertItem_m3700911267_gshared)(__this, ___index0, ___item1, method)
// System.Void BioCollection`1<System.Object>::Move(System.Int32,System.Int32)
extern "C"  void BioCollection_1_Move_m522700199_gshared (BioCollection_1_t694114648 * __this, int32_t ___oldIndex0, int32_t ___newIndex1, const MethodInfo* method);
#define BioCollection_1_Move_m522700199(__this, ___oldIndex0, ___newIndex1, method) ((  void (*) (BioCollection_1_t694114648 *, int32_t, int32_t, const MethodInfo*))BioCollection_1_Move_m522700199_gshared)(__this, ___oldIndex0, ___newIndex1, method)
// System.Void BioCollection`1<System.Object>::Implant(IOrganism,T)
extern "C"  void BioCollection_1_Implant_m2746653748_gshared (BioCollection_1_t694114648 * __this, Il2CppObject * ___intruder0, Il2CppObject * ___orphan1, const MethodInfo* method);
#define BioCollection_1_Implant_m2746653748(__this, ___intruder0, ___orphan1, method) ((  void (*) (BioCollection_1_t694114648 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))BioCollection_1_Implant_m2746653748_gshared)(__this, ___intruder0, ___orphan1, method)
// System.Void BioCollection`1<System.Object>::TransplantOrganTo(T,BioCollection`1<T>)
extern "C"  void BioCollection_1_TransplantOrganTo_m1526006390_gshared (BioCollection_1_t694114648 * __this, Il2CppObject * ___organ0, BioCollection_1_t694114648 * ___anotherBody1, const MethodInfo* method);
#define BioCollection_1_TransplantOrganTo_m1526006390(__this, ___organ0, ___anotherBody1, method) ((  void (*) (BioCollection_1_t694114648 *, Il2CppObject *, BioCollection_1_t694114648 *, const MethodInfo*))BioCollection_1_TransplantOrganTo_m1526006390_gshared)(__this, ___organ0, ___anotherBody1, method)
// System.Void BioCollection`1<System.Object>::MoveItem(System.Int32,System.Int32)
extern "C"  void BioCollection_1_MoveItem_m869199252_gshared (BioCollection_1_t694114648 * __this, int32_t ___oldIndex0, int32_t ___newIndex1, const MethodInfo* method);
#define BioCollection_1_MoveItem_m869199252(__this, ___oldIndex0, ___newIndex1, method) ((  void (*) (BioCollection_1_t694114648 *, int32_t, int32_t, const MethodInfo*))BioCollection_1_MoveItem_m869199252_gshared)(__this, ___oldIndex0, ___newIndex1, method)
// System.Void BioCollection`1<System.Object>::RemoveItem(System.Int32)
extern "C"  void BioCollection_1_RemoveItem_m3904509654_gshared (BioCollection_1_t694114648 * __this, int32_t ___index0, const MethodInfo* method);
#define BioCollection_1_RemoveItem_m3904509654(__this, ___index0, method) ((  void (*) (BioCollection_1_t694114648 *, int32_t, const MethodInfo*))BioCollection_1_RemoveItem_m3904509654_gshared)(__this, ___index0, method)
// System.Void BioCollection`1<System.Object>::SetItem(System.Int32,T)
extern "C"  void BioCollection_1_SetItem_m2557962322_gshared (BioCollection_1_t694114648 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define BioCollection_1_SetItem_m2557962322(__this, ___index0, ___item1, method) ((  void (*) (BioCollection_1_t694114648 *, int32_t, Il2CppObject *, const MethodInfo*))BioCollection_1_SetItem_m2557962322_gshared)(__this, ___index0, ___item1, method)
// IStream`1<System.Int32> BioCollection`1<System.Object>::ObserveCountChanged()
extern "C"  Il2CppObject* BioCollection_1_ObserveCountChanged_m3434133199_gshared (BioCollection_1_t694114648 * __this, const MethodInfo* method);
#define BioCollection_1_ObserveCountChanged_m3434133199(__this, method) ((  Il2CppObject* (*) (BioCollection_1_t694114648 *, const MethodInfo*))BioCollection_1_ObserveCountChanged_m3434133199_gshared)(__this, method)
// IStream`1<UniRx.Unit> BioCollection`1<System.Object>::ObserveReset()
extern "C"  Il2CppObject* BioCollection_1_ObserveReset_m1437768404_gshared (BioCollection_1_t694114648 * __this, const MethodInfo* method);
#define BioCollection_1_ObserveReset_m1437768404(__this, method) ((  Il2CppObject* (*) (BioCollection_1_t694114648 *, const MethodInfo*))BioCollection_1_ObserveReset_m1437768404_gshared)(__this, method)
// IStream`1<UniRx.CollectionAddEvent`1<T>> BioCollection`1<System.Object>::ObserveAdd()
extern "C"  Il2CppObject* BioCollection_1_ObserveAdd_m3776200054_gshared (BioCollection_1_t694114648 * __this, const MethodInfo* method);
#define BioCollection_1_ObserveAdd_m3776200054(__this, method) ((  Il2CppObject* (*) (BioCollection_1_t694114648 *, const MethodInfo*))BioCollection_1_ObserveAdd_m3776200054_gshared)(__this, method)
// IStream`1<CollectionMoveEvent`1<T>> BioCollection`1<System.Object>::ObserveMove()
extern "C"  Il2CppObject* BioCollection_1_ObserveMove_m2430852416_gshared (BioCollection_1_t694114648 * __this, const MethodInfo* method);
#define BioCollection_1_ObserveMove_m2430852416(__this, method) ((  Il2CppObject* (*) (BioCollection_1_t694114648 *, const MethodInfo*))BioCollection_1_ObserveMove_m2430852416_gshared)(__this, method)
// IStream`1<CollectionRemoveEvent`1<T>> BioCollection`1<System.Object>::ObserveRemove()
extern "C"  Il2CppObject* BioCollection_1_ObserveRemove_m3770763232_gshared (BioCollection_1_t694114648 * __this, const MethodInfo* method);
#define BioCollection_1_ObserveRemove_m3770763232(__this, method) ((  Il2CppObject* (*) (BioCollection_1_t694114648 *, const MethodInfo*))BioCollection_1_ObserveRemove_m3770763232_gshared)(__this, method)
// IStream`1<CollectionReplaceEvent`1<T>> BioCollection`1<System.Object>::ObserveReplace()
extern "C"  Il2CppObject* BioCollection_1_ObserveReplace_m612405460_gshared (BioCollection_1_t694114648 * __this, const MethodInfo* method);
#define BioCollection_1_ObserveReplace_m612405460(__this, method) ((  Il2CppObject* (*) (BioCollection_1_t694114648 *, const MethodInfo*))BioCollection_1_ObserveReplace_m612405460_gshared)(__this, method)
// System.IDisposable BioCollection`1<System.Object>::OnChanged(System.Action,Priority)
extern "C"  Il2CppObject * BioCollection_1_OnChanged_m4072865435_gshared (BioCollection_1_t694114648 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method);
#define BioCollection_1_OnChanged_m4072865435(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (BioCollection_1_t694114648 *, Action_t437523947 *, int32_t, const MethodInfo*))BioCollection_1_OnChanged_m4072865435_gshared)(__this, ___action0, ___p1, method)
// ICell`1<System.Object> BioCollection`1<System.Object>::AsCellObject()
extern "C"  Il2CppObject* BioCollection_1_AsCellObject_m190317022_gshared (BioCollection_1_t694114648 * __this, const MethodInfo* method);
#define BioCollection_1_AsCellObject_m190317022(__this, method) ((  Il2CppObject* (*) (BioCollection_1_t694114648 *, const MethodInfo*))BioCollection_1_AsCellObject_m190317022_gshared)(__this, method)
// System.IDisposable BioCollection`1<System.Object>::Bind(System.Action`1<System.Collections.Generic.ICollection`1<T>>,Priority)
extern "C"  Il2CppObject * BioCollection_1_Bind_m75966107_gshared (BioCollection_1_t694114648 * __this, Action_1_t1451390511 * ___action0, int32_t ___p1, const MethodInfo* method);
#define BioCollection_1_Bind_m75966107(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (BioCollection_1_t694114648 *, Action_1_t1451390511 *, int32_t, const MethodInfo*))BioCollection_1_Bind_m75966107_gshared)(__this, ___action0, ___p1, method)
// System.IDisposable BioCollection`1<System.Object>::ListenUpdates(System.Action`1<System.Collections.Generic.ICollection`1<T>>,Priority)
extern "C"  Il2CppObject * BioCollection_1_ListenUpdates_m181791445_gshared (BioCollection_1_t694114648 * __this, Action_1_t1451390511 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define BioCollection_1_ListenUpdates_m181791445(__this, ___reaction0, ___p1, method) ((  Il2CppObject * (*) (BioCollection_1_t694114648 *, Action_1_t1451390511 *, int32_t, const MethodInfo*))BioCollection_1_ListenUpdates_m181791445_gshared)(__this, ___reaction0, ___p1, method)
// System.Collections.Generic.ICollection`1<T> BioCollection`1<System.Object>::get_value()
extern "C"  Il2CppObject* BioCollection_1_get_value_m3891915910_gshared (BioCollection_1_t694114648 * __this, const MethodInfo* method);
#define BioCollection_1_get_value_m3891915910(__this, method) ((  Il2CppObject* (*) (BioCollection_1_t694114648 *, const MethodInfo*))BioCollection_1_get_value_m3891915910_gshared)(__this, method)
// System.Object BioCollection`1<System.Object>::get_valueObject()
extern "C"  Il2CppObject * BioCollection_1_get_valueObject_m1074670598_gshared (BioCollection_1_t694114648 * __this, const MethodInfo* method);
#define BioCollection_1_get_valueObject_m1074670598(__this, method) ((  Il2CppObject * (*) (BioCollection_1_t694114648 *, const MethodInfo*))BioCollection_1_get_valueObject_m1074670598_gshared)(__this, method)
// System.Void BioCollection`1<System.Object>::<Setup>m__165(T)
extern "C"  void BioCollection_1_U3CSetupU3Em__165_m2328322154_gshared (BioCollection_1_t694114648 * __this, Il2CppObject * ___child0, const MethodInfo* method);
#define BioCollection_1_U3CSetupU3Em__165_m2328322154(__this, ___child0, method) ((  void (*) (BioCollection_1_t694114648 *, Il2CppObject *, const MethodInfo*))BioCollection_1_U3CSetupU3Em__165_m2328322154_gshared)(__this, ___child0, method)
// System.Void BioCollection`1<System.Object>::<WakeUp>m__166(T)
extern "C"  void BioCollection_1_U3CWakeUpU3Em__166_m337445417_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___child0, const MethodInfo* method);
#define BioCollection_1_U3CWakeUpU3Em__166_m337445417(__this /* static, unused */, ___child0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))BioCollection_1_U3CWakeUpU3Em__166_m337445417_gshared)(__this /* static, unused */, ___child0, method)
// System.Void BioCollection`1<System.Object>::<Spread>m__167(T)
extern "C"  void BioCollection_1_U3CSpreadU3Em__167_m1616714652_gshared (BioCollection_1_t694114648 * __this, Il2CppObject * ___child0, const MethodInfo* method);
#define BioCollection_1_U3CSpreadU3Em__167_m1616714652(__this, ___child0, method) ((  void (*) (BioCollection_1_t694114648 *, Il2CppObject *, const MethodInfo*))BioCollection_1_U3CSpreadU3Em__167_m1616714652_gshared)(__this, ___child0, method)
// System.Void BioCollection`1<System.Object>::<ToSleep>m__168(System.IDisposable)
extern "C"  void BioCollection_1_U3CToSleepU3Em__168_m1604672846_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___d0, const MethodInfo* method);
#define BioCollection_1_U3CToSleepU3Em__168_m1604672846(__this /* static, unused */, ___d0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))BioCollection_1_U3CToSleepU3Em__168_m1604672846_gshared)(__this /* static, unused */, ___d0, method)
// System.Void BioCollection`1<System.Object>::<ToSleep>m__169(T)
extern "C"  void BioCollection_1_U3CToSleepU3Em__169_m4149698949_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___child0, const MethodInfo* method);
#define BioCollection_1_U3CToSleepU3Em__169_m4149698949(__this /* static, unused */, ___child0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))BioCollection_1_U3CToSleepU3Em__169_m4149698949_gshared)(__this /* static, unused */, ___child0, method)
// System.Void BioCollection`1<System.Object>::<ClearItems>m__16A(T)
extern "C"  void BioCollection_1_U3CClearItemsU3Em__16A_m3742891986_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___child0, const MethodInfo* method);
#define BioCollection_1_U3CClearItemsU3Em__16A_m3742891986(__this /* static, unused */, ___child0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))BioCollection_1_U3CClearItemsU3Em__16A_m3742891986_gshared)(__this /* static, unused */, ___child0, method)
// System.Object BioCollection`1<System.Object>::<AsCellObject>m__16D(System.Collections.Generic.ICollection`1<T>)
extern "C"  Il2CppObject * BioCollection_1_U3CAsCellObjectU3Em__16D_m3797917425_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___collection0, const MethodInfo* method);
#define BioCollection_1_U3CAsCellObjectU3Em__16D_m3797917425(__this /* static, unused */, ___collection0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))BioCollection_1_U3CAsCellObjectU3Em__16D_m3797917425_gshared)(__this /* static, unused */, ___collection0, method)
