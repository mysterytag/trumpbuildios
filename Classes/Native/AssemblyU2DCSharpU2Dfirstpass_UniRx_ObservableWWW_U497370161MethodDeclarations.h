﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ObservableWWW/<Get>c__AnonStorey7E
struct U3CGetU3Ec__AnonStorey7E_t497370161;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// UniRx.IObserver`1<System.String>
struct IObserver_1_t3180487805;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560.h"

// System.Void UniRx.ObservableWWW/<Get>c__AnonStorey7E::.ctor()
extern "C"  void U3CGetU3Ec__AnonStorey7E__ctor_m3918657003 (U3CGetU3Ec__AnonStorey7E_t497370161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UniRx.ObservableWWW/<Get>c__AnonStorey7E::<>m__AA(UniRx.IObserver`1<System.String>,UniRx.CancellationToken)
extern "C"  Il2CppObject * U3CGetU3Ec__AnonStorey7E_U3CU3Em__AA_m1036982784 (U3CGetU3Ec__AnonStorey7E_t497370161 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellation1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
