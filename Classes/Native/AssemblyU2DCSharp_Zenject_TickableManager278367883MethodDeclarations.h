﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.TickableManager
struct TickableManager_t278367883;
// Zenject.ILateTickable
struct ILateTickable_t3465810843;
// Zenject.IFixedTickable
struct IFixedTickable_t3328391607;
// Zenject.ITickable
struct ITickable_t789512021;
// System.Type
struct Type_t;
// ModestTree.Util.Tuple`2<System.Type,System.Int32>
struct Tuple_2_t3719549051;

#include "codegen/il2cpp-codegen.h"

// System.Void Zenject.TickableManager::.ctor()
extern "C"  void TickableManager__ctor_m2585051700 (TickableManager_t278367883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.TickableManager::Initialize()
extern "C"  void TickableManager_Initialize_m733795552 (TickableManager_t278367883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.TickableManager::WarnForMissingBindings()
extern "C"  void TickableManager_WarnForMissingBindings_m956088769 (TickableManager_t278367883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.TickableManager::InitFixedTickables()
extern "C"  void TickableManager_InitFixedTickables_m557236040 (TickableManager_t278367883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.TickableManager::InitTickables()
extern "C"  void TickableManager_InitTickables_m2762782462 (TickableManager_t278367883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.TickableManager::InitLateTickables()
extern "C"  void TickableManager_InitLateTickables_m1376374776 (TickableManager_t278367883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.TickableManager::UpdateLateTickable(Zenject.ILateTickable)
extern "C"  void TickableManager_UpdateLateTickable_m2045923701 (TickableManager_t278367883 * __this, Il2CppObject * ___tickable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.TickableManager::UpdateFixedTickable(Zenject.IFixedTickable)
extern "C"  void TickableManager_UpdateFixedTickable_m2742900371 (TickableManager_t278367883 * __this, Il2CppObject * ___tickable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.TickableManager::UpdateTickable(Zenject.ITickable)
extern "C"  void TickableManager_UpdateTickable_m4186942593 (TickableManager_t278367883 * __this, Il2CppObject * ___tickable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.TickableManager::Add(Zenject.ITickable,System.Int32)
extern "C"  void TickableManager_Add_m1141490809 (TickableManager_t278367883 * __this, Il2CppObject * ___tickable0, int32_t ___priority1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.TickableManager::Add(Zenject.ITickable)
extern "C"  void TickableManager_Add_m3656882462 (TickableManager_t278367883 * __this, Il2CppObject * ___tickable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.TickableManager::AddLate(Zenject.ILateTickable,System.Int32)
extern "C"  void TickableManager_AddLate_m2428459909 (TickableManager_t278367883 * __this, Il2CppObject * ___tickable0, int32_t ___priority1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.TickableManager::AddLate(Zenject.ILateTickable)
extern "C"  void TickableManager_AddLate_m1128103570 (TickableManager_t278367883 * __this, Il2CppObject * ___tickable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.TickableManager::AddFixed(Zenject.IFixedTickable,System.Int32)
extern "C"  void TickableManager_AddFixed_m4043982325 (TickableManager_t278367883 * __this, Il2CppObject * ___tickable0, int32_t ___priority1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.TickableManager::AddFixed(Zenject.IFixedTickable)
extern "C"  void TickableManager_AddFixed_m3377880610 (TickableManager_t278367883 * __this, Il2CppObject * ___tickable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.TickableManager::Remove(Zenject.ITickable)
extern "C"  void TickableManager_Remove_m2180943581 (TickableManager_t278367883 * __this, Il2CppObject * ___tickable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.TickableManager::RemoveLate(Zenject.ILateTickable)
extern "C"  void TickableManager_RemoveLate_m2727232849 (TickableManager_t278367883 * __this, Il2CppObject * ___tickable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.TickableManager::RemoveFixed(Zenject.IFixedTickable)
extern "C"  void TickableManager_RemoveFixed_m2542825761 (TickableManager_t278367883 * __this, Il2CppObject * ___tickable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.TickableManager::Update()
extern "C"  void TickableManager_Update_m259086169 (TickableManager_t278367883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.TickableManager::FixedUpdate()
extern "C"  void TickableManager_FixedUpdate_m3368626351 (TickableManager_t278367883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.TickableManager::LateUpdate()
extern "C"  void TickableManager_LateUpdate_m1881524383 (TickableManager_t278367883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Zenject.TickableManager::<WarnForMissingBindings>m__2E7(Zenject.ITickable)
extern "C"  Type_t * TickableManager_U3CWarnForMissingBindingsU3Em__2E7_m2179551731 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Zenject.TickableManager::<InitFixedTickables>m__2E8(ModestTree.Util.Tuple`2<System.Type,System.Int32>)
extern "C"  Type_t * TickableManager_U3CInitFixedTickablesU3Em__2E8_m3385276832 (Il2CppObject * __this /* static, unused */, Tuple_2_t3719549051 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Zenject.TickableManager::<InitFixedTickables>m__2EA(ModestTree.Util.Tuple`2<System.Type,System.Int32>)
extern "C"  int32_t TickableManager_U3CInitFixedTickablesU3Em__2EA_m1451350763 (Il2CppObject * __this /* static, unused */, Tuple_2_t3719549051 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Zenject.TickableManager::<InitTickables>m__2EB(ModestTree.Util.Tuple`2<System.Type,System.Int32>)
extern "C"  Type_t * TickableManager_U3CInitTickablesU3Em__2EB_m4229921116 (Il2CppObject * __this /* static, unused */, Tuple_2_t3719549051 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Zenject.TickableManager::<InitTickables>m__2ED(ModestTree.Util.Tuple`2<System.Type,System.Int32>)
extern "C"  int32_t TickableManager_U3CInitTickablesU3Em__2ED_m4103590310 (Il2CppObject * __this /* static, unused */, Tuple_2_t3719549051 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Zenject.TickableManager::<InitLateTickables>m__2EE(ModestTree.Util.Tuple`2<System.Type,System.Int32>)
extern "C"  Type_t * TickableManager_U3CInitLateTickablesU3Em__2EE_m2757629811 (Il2CppObject * __this /* static, unused */, Tuple_2_t3719549051 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Zenject.TickableManager::<InitLateTickables>m__2F0(ModestTree.Util.Tuple`2<System.Type,System.Int32>)
extern "C"  int32_t TickableManager_U3CInitLateTickablesU3Em__2F0_m1158061237 (Il2CppObject * __this /* static, unused */, Tuple_2_t3719549051 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
