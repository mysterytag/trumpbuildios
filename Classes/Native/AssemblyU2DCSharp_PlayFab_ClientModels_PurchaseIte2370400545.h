﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance>
struct List_1_t1322103281;

#include "AssemblyU2DCSharp_PlayFab_Internal_PlayFabResultCom379512675.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.PurchaseItemResult
struct  PurchaseItemResult_t2370400545  : public PlayFabResultCommon_t379512675
{
public:
	// System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance> PlayFab.ClientModels.PurchaseItemResult::<Items>k__BackingField
	List_1_t1322103281 * ___U3CItemsU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CItemsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PurchaseItemResult_t2370400545, ___U3CItemsU3Ek__BackingField_2)); }
	inline List_1_t1322103281 * get_U3CItemsU3Ek__BackingField_2() const { return ___U3CItemsU3Ek__BackingField_2; }
	inline List_1_t1322103281 ** get_address_of_U3CItemsU3Ek__BackingField_2() { return &___U3CItemsU3Ek__BackingField_2; }
	inline void set_U3CItemsU3Ek__BackingField_2(List_1_t1322103281 * value)
	{
		___U3CItemsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CItemsU3Ek__BackingField_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
