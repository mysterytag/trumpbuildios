﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass54_0
struct U3CU3Ec__DisplayClass54_0_t2750195783;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979.h"

// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass54_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass54_0__ctor_m3794004911 (U3CU3Ec__DisplayClass54_0_t2750195783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion DG.Tweening.ShortcutExtensions/<>c__DisplayClass54_0::<DOLookAt>b__0()
extern "C"  Quaternion_t1891715979  U3CU3Ec__DisplayClass54_0_U3CDOLookAtU3Eb__0_m1364073332 (U3CU3Ec__DisplayClass54_0_t2750195783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass54_0::<DOLookAt>b__1(UnityEngine.Quaternion)
extern "C"  void U3CU3Ec__DisplayClass54_0_U3CDOLookAtU3Eb__1_m122511956 (U3CU3Ec__DisplayClass54_0_t2750195783 * __this, Quaternion_t1891715979  ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
