﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.WhereObservable`1<System.Int64>
struct WhereObservable_1_t4045974831;
// UniRx.IObservable`1<System.Int64>
struct IObservable_1_t2606213246;
// System.Func`2<System.Int64,System.Boolean>
struct Func_2_t2251336571;
// System.Func`3<System.Int64,System.Int32,System.Boolean>
struct Func_3_t1612416521;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Int64>
struct IObserver_1_t764446489;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.WhereObservable`1<System.Int64>::.ctor(UniRx.IObservable`1<T>,System.Func`2<T,System.Boolean>)
extern "C"  void WhereObservable_1__ctor_m3687130132_gshared (WhereObservable_1_t4045974831 * __this, Il2CppObject* ___source0, Func_2_t2251336571 * ___predicate1, const MethodInfo* method);
#define WhereObservable_1__ctor_m3687130132(__this, ___source0, ___predicate1, method) ((  void (*) (WhereObservable_1_t4045974831 *, Il2CppObject*, Func_2_t2251336571 *, const MethodInfo*))WhereObservable_1__ctor_m3687130132_gshared)(__this, ___source0, ___predicate1, method)
// System.Void UniRx.Operators.WhereObservable`1<System.Int64>::.ctor(UniRx.IObservable`1<T>,System.Func`3<T,System.Int32,System.Boolean>)
extern "C"  void WhereObservable_1__ctor_m2388955026_gshared (WhereObservable_1_t4045974831 * __this, Il2CppObject* ___source0, Func_3_t1612416521 * ___predicateWithIndex1, const MethodInfo* method);
#define WhereObservable_1__ctor_m2388955026(__this, ___source0, ___predicateWithIndex1, method) ((  void (*) (WhereObservable_1_t4045974831 *, Il2CppObject*, Func_3_t1612416521 *, const MethodInfo*))WhereObservable_1__ctor_m2388955026_gshared)(__this, ___source0, ___predicateWithIndex1, method)
// UniRx.IObservable`1<T> UniRx.Operators.WhereObservable`1<System.Int64>::CombinePredicate(System.Func`2<T,System.Boolean>)
extern "C"  Il2CppObject* WhereObservable_1_CombinePredicate_m1957189507_gshared (WhereObservable_1_t4045974831 * __this, Func_2_t2251336571 * ___combinePredicate0, const MethodInfo* method);
#define WhereObservable_1_CombinePredicate_m1957189507(__this, ___combinePredicate0, method) ((  Il2CppObject* (*) (WhereObservable_1_t4045974831 *, Func_2_t2251336571 *, const MethodInfo*))WhereObservable_1_CombinePredicate_m1957189507_gshared)(__this, ___combinePredicate0, method)
// System.IDisposable UniRx.Operators.WhereObservable`1<System.Int64>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * WhereObservable_1_SubscribeCore_m3632666121_gshared (WhereObservable_1_t4045974831 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define WhereObservable_1_SubscribeCore_m3632666121(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (WhereObservable_1_t4045974831 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))WhereObservable_1_SubscribeCore_m3632666121_gshared)(__this, ___observer0, ___cancel1, method)
