﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CollectionReplaceEvent_1_gen2497372070MethodDeclarations.h"

// System.Void CollectionReplaceEvent`1<UpgradeButton>::.ctor(System.Int32,T,T)
#define CollectionReplaceEvent_1__ctor_m3987666933(__this, ___index0, ___oldValue1, ___newValue2, method) ((  void (*) (CollectionReplaceEvent_1_t3135732992 *, int32_t, UpgradeButton_t1475467342 *, UpgradeButton_t1475467342 *, const MethodInfo*))CollectionReplaceEvent_1__ctor_m1290910597_gshared)(__this, ___index0, ___oldValue1, ___newValue2, method)
// System.Int32 CollectionReplaceEvent`1<UpgradeButton>::get_Index()
#define CollectionReplaceEvent_1_get_Index_m4000714249(__this, method) ((  int32_t (*) (CollectionReplaceEvent_1_t3135732992 *, const MethodInfo*))CollectionReplaceEvent_1_get_Index_m2241178009_gshared)(__this, method)
// System.Void CollectionReplaceEvent`1<UpgradeButton>::set_Index(System.Int32)
#define CollectionReplaceEvent_1_set_Index_m2497805016(__this, ___value0, method) ((  void (*) (CollectionReplaceEvent_1_t3135732992 *, int32_t, const MethodInfo*))CollectionReplaceEvent_1_set_Index_m4096015976_gshared)(__this, ___value0, method)
// T CollectionReplaceEvent`1<UpgradeButton>::get_OldValue()
#define CollectionReplaceEvent_1_get_OldValue_m1919342852(__this, method) ((  UpgradeButton_t1475467342 * (*) (CollectionReplaceEvent_1_t3135732992 *, const MethodInfo*))CollectionReplaceEvent_1_get_OldValue_m3651064692_gshared)(__this, method)
// System.Void CollectionReplaceEvent`1<UpgradeButton>::set_OldValue(T)
#define CollectionReplaceEvent_1_set_OldValue_m3280512359(__this, ___value0, method) ((  void (*) (CollectionReplaceEvent_1_t3135732992 *, UpgradeButton_t1475467342 *, const MethodInfo*))CollectionReplaceEvent_1_set_OldValue_m1129314551_gshared)(__this, ___value0, method)
// T CollectionReplaceEvent`1<UpgradeButton>::get_NewValue()
#define CollectionReplaceEvent_1_get_NewValue_m636793707(__this, method) ((  UpgradeButton_t1475467342 * (*) (CollectionReplaceEvent_1_t3135732992 *, const MethodInfo*))CollectionReplaceEvent_1_get_NewValue_m2368515547_gshared)(__this, method)
// System.Void CollectionReplaceEvent`1<UpgradeButton>::set_NewValue(T)
#define CollectionReplaceEvent_1_set_NewValue_m2176194528(__this, ___value0, method) ((  void (*) (CollectionReplaceEvent_1_t3135732992 *, UpgradeButton_t1475467342 *, const MethodInfo*))CollectionReplaceEvent_1_set_NewValue_m24996720_gshared)(__this, ___value0, method)
// System.String CollectionReplaceEvent`1<UpgradeButton>::ToString()
#define CollectionReplaceEvent_1_ToString_m918917561(__this, method) ((  String_t* (*) (CollectionReplaceEvent_1_t3135732992 *, const MethodInfo*))CollectionReplaceEvent_1_ToString_m2524726313_gshared)(__this, method)
