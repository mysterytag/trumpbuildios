﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper3951240486.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.AnimationCurveReactiveProperty
struct  AnimationCurveReactiveProperty_t3280306475  : public ReactiveProperty_1_t3951240486
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
