﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// System.Collections.Generic.List`1<System.IDisposable>
struct List_1_t2425880343;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.CompositeDisposable
struct  CompositeDisposable_t1894629977  : public Il2CppObject
{
public:
	// System.Object UniRx.CompositeDisposable::_gate
	Il2CppObject * ____gate_1;
	// System.Boolean UniRx.CompositeDisposable::_disposed
	bool ____disposed_2;
	// System.Collections.Generic.List`1<System.IDisposable> UniRx.CompositeDisposable::_disposables
	List_1_t2425880343 * ____disposables_3;
	// System.Int32 UniRx.CompositeDisposable::_count
	int32_t ____count_4;

public:
	inline static int32_t get_offset_of__gate_1() { return static_cast<int32_t>(offsetof(CompositeDisposable_t1894629977, ____gate_1)); }
	inline Il2CppObject * get__gate_1() const { return ____gate_1; }
	inline Il2CppObject ** get_address_of__gate_1() { return &____gate_1; }
	inline void set__gate_1(Il2CppObject * value)
	{
		____gate_1 = value;
		Il2CppCodeGenWriteBarrier(&____gate_1, value);
	}

	inline static int32_t get_offset_of__disposed_2() { return static_cast<int32_t>(offsetof(CompositeDisposable_t1894629977, ____disposed_2)); }
	inline bool get__disposed_2() const { return ____disposed_2; }
	inline bool* get_address_of__disposed_2() { return &____disposed_2; }
	inline void set__disposed_2(bool value)
	{
		____disposed_2 = value;
	}

	inline static int32_t get_offset_of__disposables_3() { return static_cast<int32_t>(offsetof(CompositeDisposable_t1894629977, ____disposables_3)); }
	inline List_1_t2425880343 * get__disposables_3() const { return ____disposables_3; }
	inline List_1_t2425880343 ** get_address_of__disposables_3() { return &____disposables_3; }
	inline void set__disposables_3(List_1_t2425880343 * value)
	{
		____disposables_3 = value;
		Il2CppCodeGenWriteBarrier(&____disposables_3, value);
	}

	inline static int32_t get_offset_of__count_4() { return static_cast<int32_t>(offsetof(CompositeDisposable_t1894629977, ____count_4)); }
	inline int32_t get__count_4() const { return ____count_4; }
	inline int32_t* get_address_of__count_4() { return &____count_4; }
	inline void set__count_4(int32_t value)
	{
		____count_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
