﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.OperatorObserverBase`2<System.Int32,UnityEngine.Color>
struct OperatorObserverBase_2_t3737296838;
// UniRx.IObserver`1<UnityEngine.Color>
struct IObserver_1_t3800174663;
// System.IDisposable
struct IDisposable_t1628921374;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.OperatorObserverBase`2<System.Int32,UnityEngine.Color>::.ctor(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void OperatorObserverBase_2__ctor_m3519008741_gshared (OperatorObserverBase_2_t3737296838 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define OperatorObserverBase_2__ctor_m3519008741(__this, ___observer0, ___cancel1, method) ((  void (*) (OperatorObserverBase_2_t3737296838 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))OperatorObserverBase_2__ctor_m3519008741_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.OperatorObserverBase`2<System.Int32,UnityEngine.Color>::Dispose()
extern "C"  void OperatorObserverBase_2_Dispose_m1461818149_gshared (OperatorObserverBase_2_t3737296838 * __this, const MethodInfo* method);
#define OperatorObserverBase_2_Dispose_m1461818149(__this, method) ((  void (*) (OperatorObserverBase_2_t3737296838 *, const MethodInfo*))OperatorObserverBase_2_Dispose_m1461818149_gshared)(__this, method)
