﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Sprite
struct Sprite_t4006040370;
// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoneyPackInfo
struct  MoneyPackInfo_t3013049127  : public Il2CppObject
{
public:
	// UnityEngine.Sprite MoneyPackInfo::icon
	Sprite_t4006040370 * ___icon_0;
	// System.String MoneyPackInfo::name
	String_t* ___name_1;
	// System.String MoneyPackInfo::price
	String_t* ___price_2;
	// System.String MoneyPackInfo::marketAction
	String_t* ___marketAction_3;

public:
	inline static int32_t get_offset_of_icon_0() { return static_cast<int32_t>(offsetof(MoneyPackInfo_t3013049127, ___icon_0)); }
	inline Sprite_t4006040370 * get_icon_0() const { return ___icon_0; }
	inline Sprite_t4006040370 ** get_address_of_icon_0() { return &___icon_0; }
	inline void set_icon_0(Sprite_t4006040370 * value)
	{
		___icon_0 = value;
		Il2CppCodeGenWriteBarrier(&___icon_0, value);
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(MoneyPackInfo_t3013049127, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier(&___name_1, value);
	}

	inline static int32_t get_offset_of_price_2() { return static_cast<int32_t>(offsetof(MoneyPackInfo_t3013049127, ___price_2)); }
	inline String_t* get_price_2() const { return ___price_2; }
	inline String_t** get_address_of_price_2() { return &___price_2; }
	inline void set_price_2(String_t* value)
	{
		___price_2 = value;
		Il2CppCodeGenWriteBarrier(&___price_2, value);
	}

	inline static int32_t get_offset_of_marketAction_3() { return static_cast<int32_t>(offsetof(MoneyPackInfo_t3013049127, ___marketAction_3)); }
	inline String_t* get_marketAction_3() const { return ___marketAction_3; }
	inline String_t** get_address_of_marketAction_3() { return &___marketAction_3; }
	inline void set_marketAction_3(String_t* value)
	{
		___marketAction_3 = value;
		Il2CppCodeGenWriteBarrier(&___marketAction_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
