﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetPlayFabIDsFromGoogleIDsResponseCallback
struct GetPlayFabIDsFromGoogleIDsResponseCallback_t2812431968;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetPlayFabIDsFromGoogleIDsRequest
struct GetPlayFabIDsFromGoogleIDsRequest_t3277397669;
// PlayFab.ClientModels.GetPlayFabIDsFromGoogleIDsResult
struct GetPlayFabIDsFromGoogleIDsResult_t2496174951;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayFabI3277397669.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayFabI2496174951.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetPlayFabIDsFromGoogleIDsResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetPlayFabIDsFromGoogleIDsResponseCallback__ctor_m2757288783 (GetPlayFabIDsFromGoogleIDsResponseCallback_t2812431968 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetPlayFabIDsFromGoogleIDsResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetPlayFabIDsFromGoogleIDsRequest,PlayFab.ClientModels.GetPlayFabIDsFromGoogleIDsResult,PlayFab.PlayFabError,System.Object)
extern "C"  void GetPlayFabIDsFromGoogleIDsResponseCallback_Invoke_m4188661724 (GetPlayFabIDsFromGoogleIDsResponseCallback_t2812431968 * __this, String_t* ___urlPath0, int32_t ___callId1, GetPlayFabIDsFromGoogleIDsRequest_t3277397669 * ___request2, GetPlayFabIDsFromGoogleIDsResult_t2496174951 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetPlayFabIDsFromGoogleIDsResponseCallback_t2812431968(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetPlayFabIDsFromGoogleIDsRequest_t3277397669 * ___request2, GetPlayFabIDsFromGoogleIDsResult_t2496174951 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetPlayFabIDsFromGoogleIDsResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetPlayFabIDsFromGoogleIDsRequest,PlayFab.ClientModels.GetPlayFabIDsFromGoogleIDsResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetPlayFabIDsFromGoogleIDsResponseCallback_BeginInvoke_m3353405449 (GetPlayFabIDsFromGoogleIDsResponseCallback_t2812431968 * __this, String_t* ___urlPath0, int32_t ___callId1, GetPlayFabIDsFromGoogleIDsRequest_t3277397669 * ___request2, GetPlayFabIDsFromGoogleIDsResult_t2496174951 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetPlayFabIDsFromGoogleIDsResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetPlayFabIDsFromGoogleIDsResponseCallback_EndInvoke_m1250448607 (GetPlayFabIDsFromGoogleIDsResponseCallback_t2812431968 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
