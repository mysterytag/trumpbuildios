﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ByteReactiveProperty
struct ByteReactiveProperty_t1619126376;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.ByteReactiveProperty::.ctor()
extern "C"  void ByteReactiveProperty__ctor_m2510542113 (ByteReactiveProperty_t1619126376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ByteReactiveProperty::.ctor(System.Byte)
extern "C"  void ByteReactiveProperty__ctor_m3329278890 (ByteReactiveProperty_t1619126376 * __this, uint8_t ___initialValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
