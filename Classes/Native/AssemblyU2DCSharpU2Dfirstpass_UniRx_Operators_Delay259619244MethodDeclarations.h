﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.DelayFrameObservable`1/DelayFrame/<OnCompletedDelay>c__Iterator27<System.Object>
struct U3COnCompletedDelayU3Ec__Iterator27_t259619244;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.DelayFrameObservable`1/DelayFrame/<OnCompletedDelay>c__Iterator27<System.Object>::.ctor()
extern "C"  void U3COnCompletedDelayU3Ec__Iterator27__ctor_m3657059411_gshared (U3COnCompletedDelayU3Ec__Iterator27_t259619244 * __this, const MethodInfo* method);
#define U3COnCompletedDelayU3Ec__Iterator27__ctor_m3657059411(__this, method) ((  void (*) (U3COnCompletedDelayU3Ec__Iterator27_t259619244 *, const MethodInfo*))U3COnCompletedDelayU3Ec__Iterator27__ctor_m3657059411_gshared)(__this, method)
// System.Object UniRx.Operators.DelayFrameObservable`1/DelayFrame/<OnCompletedDelay>c__Iterator27<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3COnCompletedDelayU3Ec__Iterator27_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1027862633_gshared (U3COnCompletedDelayU3Ec__Iterator27_t259619244 * __this, const MethodInfo* method);
#define U3COnCompletedDelayU3Ec__Iterator27_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1027862633(__this, method) ((  Il2CppObject * (*) (U3COnCompletedDelayU3Ec__Iterator27_t259619244 *, const MethodInfo*))U3COnCompletedDelayU3Ec__Iterator27_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1027862633_gshared)(__this, method)
// System.Object UniRx.Operators.DelayFrameObservable`1/DelayFrame/<OnCompletedDelay>c__Iterator27<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3COnCompletedDelayU3Ec__Iterator27_System_Collections_IEnumerator_get_Current_m617473533_gshared (U3COnCompletedDelayU3Ec__Iterator27_t259619244 * __this, const MethodInfo* method);
#define U3COnCompletedDelayU3Ec__Iterator27_System_Collections_IEnumerator_get_Current_m617473533(__this, method) ((  Il2CppObject * (*) (U3COnCompletedDelayU3Ec__Iterator27_t259619244 *, const MethodInfo*))U3COnCompletedDelayU3Ec__Iterator27_System_Collections_IEnumerator_get_Current_m617473533_gshared)(__this, method)
// System.Boolean UniRx.Operators.DelayFrameObservable`1/DelayFrame/<OnCompletedDelay>c__Iterator27<System.Object>::MoveNext()
extern "C"  bool U3COnCompletedDelayU3Ec__Iterator27_MoveNext_m1159235473_gshared (U3COnCompletedDelayU3Ec__Iterator27_t259619244 * __this, const MethodInfo* method);
#define U3COnCompletedDelayU3Ec__Iterator27_MoveNext_m1159235473(__this, method) ((  bool (*) (U3COnCompletedDelayU3Ec__Iterator27_t259619244 *, const MethodInfo*))U3COnCompletedDelayU3Ec__Iterator27_MoveNext_m1159235473_gshared)(__this, method)
// System.Void UniRx.Operators.DelayFrameObservable`1/DelayFrame/<OnCompletedDelay>c__Iterator27<System.Object>::Dispose()
extern "C"  void U3COnCompletedDelayU3Ec__Iterator27_Dispose_m1049128912_gshared (U3COnCompletedDelayU3Ec__Iterator27_t259619244 * __this, const MethodInfo* method);
#define U3COnCompletedDelayU3Ec__Iterator27_Dispose_m1049128912(__this, method) ((  void (*) (U3COnCompletedDelayU3Ec__Iterator27_t259619244 *, const MethodInfo*))U3COnCompletedDelayU3Ec__Iterator27_Dispose_m1049128912_gshared)(__this, method)
// System.Void UniRx.Operators.DelayFrameObservable`1/DelayFrame/<OnCompletedDelay>c__Iterator27<System.Object>::Reset()
extern "C"  void U3COnCompletedDelayU3Ec__Iterator27_Reset_m1303492352_gshared (U3COnCompletedDelayU3Ec__Iterator27_t259619244 * __this, const MethodInfo* method);
#define U3COnCompletedDelayU3Ec__Iterator27_Reset_m1303492352(__this, method) ((  void (*) (U3COnCompletedDelayU3Ec__Iterator27_t259619244 *, const MethodInfo*))U3COnCompletedDelayU3Ec__Iterator27_Reset_m1303492352_gshared)(__this, method)
// System.Void UniRx.Operators.DelayFrameObservable`1/DelayFrame/<OnCompletedDelay>c__Iterator27<System.Object>::<>__Finally0()
extern "C"  void U3COnCompletedDelayU3Ec__Iterator27_U3CU3E__Finally0_m3281021632_gshared (U3COnCompletedDelayU3Ec__Iterator27_t259619244 * __this, const MethodInfo* method);
#define U3COnCompletedDelayU3Ec__Iterator27_U3CU3E__Finally0_m3281021632(__this, method) ((  void (*) (U3COnCompletedDelayU3Ec__Iterator27_t259619244 *, const MethodInfo*))U3COnCompletedDelayU3Ec__Iterator27_U3CU3E__Finally0_m3281021632_gshared)(__this, method)
