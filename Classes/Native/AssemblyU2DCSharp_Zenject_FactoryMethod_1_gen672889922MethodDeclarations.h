﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.FactoryMethod`1<System.Object>
struct FactoryMethod_1_t672889922;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t3431720054;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Zenject.FactoryMethod`1<System.Object>::.ctor()
extern "C"  void FactoryMethod_1__ctor_m3264198972_gshared (FactoryMethod_1_t672889922 * __this, const MethodInfo* method);
#define FactoryMethod_1__ctor_m3264198972(__this, method) ((  void (*) (FactoryMethod_1_t672889922 *, const MethodInfo*))FactoryMethod_1__ctor_m3264198972_gshared)(__this, method)
// System.Type Zenject.FactoryMethod`1<System.Object>::get_ConstructedType()
extern "C"  Type_t * FactoryMethod_1_get_ConstructedType_m209262631_gshared (FactoryMethod_1_t672889922 * __this, const MethodInfo* method);
#define FactoryMethod_1_get_ConstructedType_m209262631(__this, method) ((  Type_t * (*) (FactoryMethod_1_t672889922 *, const MethodInfo*))FactoryMethod_1_get_ConstructedType_m209262631_gshared)(__this, method)
// System.Type[] Zenject.FactoryMethod`1<System.Object>::get_ProvidedTypes()
extern "C"  TypeU5BU5D_t3431720054* FactoryMethod_1_get_ProvidedTypes_m3925029455_gshared (FactoryMethod_1_t672889922 * __this, const MethodInfo* method);
#define FactoryMethod_1_get_ProvidedTypes_m3925029455(__this, method) ((  TypeU5BU5D_t3431720054* (*) (FactoryMethod_1_t672889922 *, const MethodInfo*))FactoryMethod_1_get_ProvidedTypes_m3925029455_gshared)(__this, method)
// TValue Zenject.FactoryMethod`1<System.Object>::Create()
extern "C"  Il2CppObject * FactoryMethod_1_Create_m3635440868_gshared (FactoryMethod_1_t672889922 * __this, const MethodInfo* method);
#define FactoryMethod_1_Create_m3635440868(__this, method) ((  Il2CppObject * (*) (FactoryMethod_1_t672889922 *, const MethodInfo*))FactoryMethod_1_Create_m3635440868_gshared)(__this, method)
