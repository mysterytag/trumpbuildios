﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FiniteTimeProc
struct FiniteTimeProc_t2933053874;

#include "codegen/il2cpp-codegen.h"

// System.Void FiniteTimeProc::.ctor(System.Single)
extern "C"  void FiniteTimeProc__ctor_m2887282978 (FiniteTimeProc_t2933053874 * __this, float ___duration0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FiniteTimeProc::Tick(System.Single)
extern "C"  void FiniteTimeProc_Tick_m2276583763 (FiniteTimeProc_t2933053874 * __this, float ___dt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FiniteTimeProc::get_finished()
extern "C"  bool FiniteTimeProc_get_finished_m547069706 (FiniteTimeProc_t2933053874 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FiniteTimeProc::Dispose()
extern "C"  void FiniteTimeProc_Dispose_m1516601638 (FiniteTimeProc_t2933053874 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
