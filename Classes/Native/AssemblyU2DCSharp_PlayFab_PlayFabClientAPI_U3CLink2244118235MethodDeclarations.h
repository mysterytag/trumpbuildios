﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<LinkXboxAccount>c__AnonStorey7E
struct U3CLinkXboxAccountU3Ec__AnonStorey7E_t2244118235;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<LinkXboxAccount>c__AnonStorey7E::.ctor()
extern "C"  void U3CLinkXboxAccountU3Ec__AnonStorey7E__ctor_m2921963224 (U3CLinkXboxAccountU3Ec__AnonStorey7E_t2244118235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<LinkXboxAccount>c__AnonStorey7E::<>m__6B(PlayFab.CallRequestContainer)
extern "C"  void U3CLinkXboxAccountU3Ec__AnonStorey7E_U3CU3Em__6B_m3285597570 (U3CLinkXboxAccountU3Ec__AnonStorey7E_t2244118235 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
