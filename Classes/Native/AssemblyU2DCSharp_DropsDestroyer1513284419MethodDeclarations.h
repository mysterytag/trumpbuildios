﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DropsDestroyer
struct DropsDestroyer_t1513284419;
// UnityEngine.Collider
struct Collider_t955670625;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider955670625.h"

// System.Void DropsDestroyer::.ctor()
extern "C"  void DropsDestroyer__ctor_m1923802296 (DropsDestroyer_t1513284419 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DropsDestroyer::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void DropsDestroyer_OnTriggerEnter_m807854528 (DropsDestroyer_t1513284419 * __this, Collider_t955670625 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
