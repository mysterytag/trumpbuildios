﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.QuaternionReactiveProperty
struct QuaternionReactiveProperty_t512953982;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Quaternion>
struct IEqualityComparer_1_t4215982630;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979.h"

// System.Void UniRx.QuaternionReactiveProperty::.ctor()
extern "C"  void QuaternionReactiveProperty__ctor_m2144602315 (QuaternionReactiveProperty_t512953982 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.QuaternionReactiveProperty::.ctor(UnityEngine.Quaternion)
extern "C"  void QuaternionReactiveProperty__ctor_m3790748246 (QuaternionReactiveProperty_t512953982 * __this, Quaternion_t1891715979  ___initialValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Quaternion> UniRx.QuaternionReactiveProperty::get_EqualityComparer()
extern "C"  Il2CppObject* QuaternionReactiveProperty_get_EqualityComparer_m3490441263 (QuaternionReactiveProperty_t512953982 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
