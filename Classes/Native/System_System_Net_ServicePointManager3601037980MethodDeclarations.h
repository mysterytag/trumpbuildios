﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.ServicePointManager
struct ServicePointManager_t3601037980;
// System.Net.ICertificatePolicy
struct ICertificatePolicy_t2221592522;
// System.Exception
struct Exception_t1967233988;
// System.Net.Security.RemoteCertificateValidationCallback
struct RemoteCertificateValidationCallback_t4087051103;
// System.Net.ServicePoint
struct ServicePoint_t28240741;
// System.Uri
struct Uri_t2776692961;
// System.String
struct String_t;
// System.Net.IWebProxy
struct IWebProxy_t49946189;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_SecurityProtocolType2026288348.h"
#include "System_System_Net_Security_RemoteCertificateValida4087051103.h"
#include "System_System_Uri2776692961.h"
#include "mscorlib_System_String968488902.h"

// System.Void System.Net.ServicePointManager::.ctor()
extern "C"  void ServicePointManager__ctor_m3933512826 (ServicePointManager_t3601037980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.ServicePointManager::.cctor()
extern "C"  void ServicePointManager__cctor_m1197717107 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.ICertificatePolicy System.Net.ServicePointManager::get_CertificatePolicy()
extern "C"  Il2CppObject * ServicePointManager_get_CertificatePolicy_m16190897 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.ServicePointManager::set_CertificatePolicy(System.Net.ICertificatePolicy)
extern "C"  void ServicePointManager_set_CertificatePolicy_m4081751454 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.ServicePointManager::get_CheckCertificateRevocationList()
extern "C"  bool ServicePointManager_get_CheckCertificateRevocationList_m838738096 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.ServicePointManager::set_CheckCertificateRevocationList(System.Boolean)
extern "C"  void ServicePointManager_set_CheckCertificateRevocationList_m2299899369 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.ServicePointManager::get_DefaultConnectionLimit()
extern "C"  int32_t ServicePointManager_get_DefaultConnectionLimit_m954926527 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.ServicePointManager::set_DefaultConnectionLimit(System.Int32)
extern "C"  void ServicePointManager_set_DefaultConnectionLimit_m2689382100 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception System.Net.ServicePointManager::GetMustImplement()
extern "C"  Exception_t1967233988 * ServicePointManager_GetMustImplement_m4096284349 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.ServicePointManager::get_DnsRefreshTimeout()
extern "C"  int32_t ServicePointManager_get_DnsRefreshTimeout_m1111594702 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.ServicePointManager::set_DnsRefreshTimeout(System.Int32)
extern "C"  void ServicePointManager_set_DnsRefreshTimeout_m3253242459 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.ServicePointManager::get_EnableDnsRoundRobin()
extern "C"  bool ServicePointManager_get_EnableDnsRoundRobin_m2428273499 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.ServicePointManager::set_EnableDnsRoundRobin(System.Boolean)
extern "C"  void ServicePointManager_set_EnableDnsRoundRobin_m755909108 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.ServicePointManager::get_MaxServicePointIdleTime()
extern "C"  int32_t ServicePointManager_get_MaxServicePointIdleTime_m929892799 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.ServicePointManager::set_MaxServicePointIdleTime(System.Int32)
extern "C"  void ServicePointManager_set_MaxServicePointIdleTime_m423214796 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.ServicePointManager::get_MaxServicePoints()
extern "C"  int32_t ServicePointManager_get_MaxServicePoints_m2020315959 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.ServicePointManager::set_MaxServicePoints(System.Int32)
extern "C"  void ServicePointManager_set_MaxServicePoints_m758436684 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.SecurityProtocolType System.Net.ServicePointManager::get_SecurityProtocol()
extern "C"  int32_t ServicePointManager_get_SecurityProtocol_m1940055552 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.ServicePointManager::set_SecurityProtocol(System.Net.SecurityProtocolType)
extern "C"  void ServicePointManager_set_SecurityProtocol_m2631495505 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.Security.RemoteCertificateValidationCallback System.Net.ServicePointManager::get_ServerCertificateValidationCallback()
extern "C"  RemoteCertificateValidationCallback_t4087051103 * ServicePointManager_get_ServerCertificateValidationCallback_m451731131 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.ServicePointManager::set_ServerCertificateValidationCallback(System.Net.Security.RemoteCertificateValidationCallback)
extern "C"  void ServicePointManager_set_ServerCertificateValidationCallback_m1255728468 (Il2CppObject * __this /* static, unused */, RemoteCertificateValidationCallback_t4087051103 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.ServicePointManager::get_Expect100Continue()
extern "C"  bool ServicePointManager_get_Expect100Continue_m2108396504 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.ServicePointManager::set_Expect100Continue(System.Boolean)
extern "C"  void ServicePointManager_set_Expect100Continue_m2810045489 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.ServicePointManager::get_UseNagleAlgorithm()
extern "C"  bool ServicePointManager_get_UseNagleAlgorithm_m2302899298 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.ServicePointManager::set_UseNagleAlgorithm(System.Boolean)
extern "C"  void ServicePointManager_set_UseNagleAlgorithm_m2350189371 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.ServicePoint System.Net.ServicePointManager::FindServicePoint(System.Uri)
extern "C"  ServicePoint_t28240741 * ServicePointManager_FindServicePoint_m1327567065 (Il2CppObject * __this /* static, unused */, Uri_t2776692961 * ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.ServicePoint System.Net.ServicePointManager::FindServicePoint(System.String,System.Net.IWebProxy)
extern "C"  ServicePoint_t28240741 * ServicePointManager_FindServicePoint_m1199833463 (Il2CppObject * __this /* static, unused */, String_t* ___uriString0, Il2CppObject * ___proxy1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.ServicePoint System.Net.ServicePointManager::FindServicePoint(System.Uri,System.Net.IWebProxy)
extern "C"  ServicePoint_t28240741 * ServicePointManager_FindServicePoint_m543399130 (Il2CppObject * __this /* static, unused */, Uri_t2776692961 * ___address0, Il2CppObject * ___proxy1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.ServicePointManager::RecycleServicePoints()
extern "C"  void ServicePointManager_RecycleServicePoints_m3960742511 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
