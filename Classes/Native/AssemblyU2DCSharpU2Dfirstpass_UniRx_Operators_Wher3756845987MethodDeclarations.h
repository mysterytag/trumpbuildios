﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.WhereObservable`1<UniRx.Unit>
struct WhereObservable_1_t3756845987;
// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;
// System.Func`2<UniRx.Unit,System.Boolean>
struct Func_2_t2766110903;
// System.Func`3<UniRx.Unit,System.Int32,System.Boolean>
struct Func_3_t255060061;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.WhereObservable`1<UniRx.Unit>::.ctor(UniRx.IObservable`1<T>,System.Func`2<T,System.Boolean>)
extern "C"  void WhereObservable_1__ctor_m779014658_gshared (WhereObservable_1_t3756845987 * __this, Il2CppObject* ___source0, Func_2_t2766110903 * ___predicate1, const MethodInfo* method);
#define WhereObservable_1__ctor_m779014658(__this, ___source0, ___predicate1, method) ((  void (*) (WhereObservable_1_t3756845987 *, Il2CppObject*, Func_2_t2766110903 *, const MethodInfo*))WhereObservable_1__ctor_m779014658_gshared)(__this, ___source0, ___predicate1, method)
// System.Void UniRx.Operators.WhereObservable`1<UniRx.Unit>::.ctor(UniRx.IObservable`1<T>,System.Func`3<T,System.Int32,System.Boolean>)
extern "C"  void WhereObservable_1__ctor_m1942844516_gshared (WhereObservable_1_t3756845987 * __this, Il2CppObject* ___source0, Func_3_t255060061 * ___predicateWithIndex1, const MethodInfo* method);
#define WhereObservable_1__ctor_m1942844516(__this, ___source0, ___predicateWithIndex1, method) ((  void (*) (WhereObservable_1_t3756845987 *, Il2CppObject*, Func_3_t255060061 *, const MethodInfo*))WhereObservable_1__ctor_m1942844516_gshared)(__this, ___source0, ___predicateWithIndex1, method)
// UniRx.IObservable`1<T> UniRx.Operators.WhereObservable`1<UniRx.Unit>::CombinePredicate(System.Func`2<T,System.Boolean>)
extern "C"  Il2CppObject* WhereObservable_1_CombinePredicate_m1256450929_gshared (WhereObservable_1_t3756845987 * __this, Func_2_t2766110903 * ___combinePredicate0, const MethodInfo* method);
#define WhereObservable_1_CombinePredicate_m1256450929(__this, ___combinePredicate0, method) ((  Il2CppObject* (*) (WhereObservable_1_t3756845987 *, Func_2_t2766110903 *, const MethodInfo*))WhereObservable_1_CombinePredicate_m1256450929_gshared)(__this, ___combinePredicate0, method)
// System.IDisposable UniRx.Operators.WhereObservable`1<UniRx.Unit>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * WhereObservable_1_SubscribeCore_m945614107_gshared (WhereObservable_1_t3756845987 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define WhereObservable_1_SubscribeCore_m945614107(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (WhereObservable_1_t3756845987 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))WhereObservable_1_SubscribeCore_m945614107_gshared)(__this, ___observer0, ___cancel1, method)
