﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.TakeObservable`1<System.Int64>
struct TakeObservable_1_t794160655;
// UniRx.IObservable`1<System.Int64>
struct IObservable_1_t2606213246;
// UniRx.IScheduler
struct IScheduler_t2938318244;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Int64>
struct IObserver_1_t764446489;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_TimeSpan763862892.h"

// System.Void UniRx.Operators.TakeObservable`1<System.Int64>::.ctor(UniRx.IObservable`1<T>,System.Int32)
extern "C"  void TakeObservable_1__ctor_m3160701743_gshared (TakeObservable_1_t794160655 * __this, Il2CppObject* ___source0, int32_t ___count1, const MethodInfo* method);
#define TakeObservable_1__ctor_m3160701743(__this, ___source0, ___count1, method) ((  void (*) (TakeObservable_1_t794160655 *, Il2CppObject*, int32_t, const MethodInfo*))TakeObservable_1__ctor_m3160701743_gshared)(__this, ___source0, ___count1, method)
// System.Void UniRx.Operators.TakeObservable`1<System.Int64>::.ctor(UniRx.IObservable`1<T>,System.TimeSpan,UniRx.IScheduler)
extern "C"  void TakeObservable_1__ctor_m658868318_gshared (TakeObservable_1_t794160655 * __this, Il2CppObject* ___source0, TimeSpan_t763862892  ___duration1, Il2CppObject * ___scheduler2, const MethodInfo* method);
#define TakeObservable_1__ctor_m658868318(__this, ___source0, ___duration1, ___scheduler2, method) ((  void (*) (TakeObservable_1_t794160655 *, Il2CppObject*, TimeSpan_t763862892 , Il2CppObject *, const MethodInfo*))TakeObservable_1__ctor_m658868318_gshared)(__this, ___source0, ___duration1, ___scheduler2, method)
// UniRx.IObservable`1<T> UniRx.Operators.TakeObservable`1<System.Int64>::Combine(System.Int32)
extern "C"  Il2CppObject* TakeObservable_1_Combine_m2626963827_gshared (TakeObservable_1_t794160655 * __this, int32_t ___count0, const MethodInfo* method);
#define TakeObservable_1_Combine_m2626963827(__this, ___count0, method) ((  Il2CppObject* (*) (TakeObservable_1_t794160655 *, int32_t, const MethodInfo*))TakeObservable_1_Combine_m2626963827_gshared)(__this, ___count0, method)
// UniRx.IObservable`1<T> UniRx.Operators.TakeObservable`1<System.Int64>::Combine(System.TimeSpan)
extern "C"  Il2CppObject* TakeObservable_1_Combine_m2295577146_gshared (TakeObservable_1_t794160655 * __this, TimeSpan_t763862892  ___duration0, const MethodInfo* method);
#define TakeObservable_1_Combine_m2295577146(__this, ___duration0, method) ((  Il2CppObject* (*) (TakeObservable_1_t794160655 *, TimeSpan_t763862892 , const MethodInfo*))TakeObservable_1_Combine_m2295577146_gshared)(__this, ___duration0, method)
// System.IDisposable UniRx.Operators.TakeObservable`1<System.Int64>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * TakeObservable_1_SubscribeCore_m2094046517_gshared (TakeObservable_1_t794160655 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define TakeObservable_1_SubscribeCore_m2094046517(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (TakeObservable_1_t794160655 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))TakeObservable_1_SubscribeCore_m2094046517_gshared)(__this, ___observer0, ___cancel1, method)
