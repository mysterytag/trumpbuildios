﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Comparison_1_gen3540781296MethodDeclarations.h"

// System.Void System.Comparison`1<Zenject.TaskUpdater`1/TaskInfo<System.Object>>::.ctor(System.Object,System.IntPtr)
#define Comparison_1__ctor_m227828193(__this, ___object0, ___method1, method) ((  void (*) (Comparison_1_t3768183280 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m487232819_gshared)(__this, ___object0, ___method1, method)
// System.Int32 System.Comparison`1<Zenject.TaskUpdater`1/TaskInfo<System.Object>>::Invoke(T,T)
#define Comparison_1_Invoke_m170256511(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t3768183280 *, TaskInfo_t1064508404 *, TaskInfo_t1064508404 *, const MethodInfo*))Comparison_1_Invoke_m1888033133_gshared)(__this, ___x0, ___y1, method)
// System.IAsyncResult System.Comparison`1<Zenject.TaskUpdater`1/TaskInfo<System.Object>>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m661182200(__this, ___x0, ___y1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Comparison_1_t3768183280 *, TaskInfo_t1064508404 *, TaskInfo_t1064508404 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Comparison_1_BeginInvoke_m3177996774_gshared)(__this, ___x0, ___y1, ___callback2, ___object3, method)
// System.Int32 System.Comparison`1<Zenject.TaskUpdater`1/TaskInfo<System.Object>>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m3044123021(__this, ___result0, method) ((  int32_t (*) (Comparison_1_t3768183280 *, Il2CppObject *, const MethodInfo*))Comparison_1_EndInvoke_m651541983_gshared)(__this, ___result0, method)
