﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback>
struct Action_1_t3384115434;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Examples.Sample04_ConvertFromUnityCallback/LogHelper/<LogCallbackAsObservable>c__AnonStorey2F
struct  U3CLogCallbackAsObservableU3Ec__AnonStorey2F_t1039971231  : public Il2CppObject
{
public:
	// System.Action`1<UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback> UniRx.Examples.Sample04_ConvertFromUnityCallback/LogHelper/<LogCallbackAsObservable>c__AnonStorey2F::h
	Action_1_t3384115434 * ___h_0;

public:
	inline static int32_t get_offset_of_h_0() { return static_cast<int32_t>(offsetof(U3CLogCallbackAsObservableU3Ec__AnonStorey2F_t1039971231, ___h_0)); }
	inline Action_1_t3384115434 * get_h_0() const { return ___h_0; }
	inline Action_1_t3384115434 ** get_address_of_h_0() { return &___h_0; }
	inline void set_h_0(Action_1_t3384115434 * value)
	{
		___h_0 = value;
		Il2CppCodeGenWriteBarrier(&___h_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
