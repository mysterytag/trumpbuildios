﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1/Subscription<System.Int32>
struct Subscription_t626661309;
// UniRx.Subject`1<System.Int32>
struct Subject_1_t490482111;
// UniRx.IObserver`1<System.Int32>
struct IObserver_1_t764446394;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Subject`1/Subscription<System.Int32>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern "C"  void Subscription__ctor_m169029604_gshared (Subscription_t626661309 * __this, Subject_1_t490482111 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method);
#define Subscription__ctor_m169029604(__this, ___parent0, ___unsubscribeTarget1, method) ((  void (*) (Subscription_t626661309 *, Subject_1_t490482111 *, Il2CppObject*, const MethodInfo*))Subscription__ctor_m169029604_gshared)(__this, ___parent0, ___unsubscribeTarget1, method)
// System.Void UniRx.Subject`1/Subscription<System.Int32>::Dispose()
extern "C"  void Subscription_Dispose_m405702546_gshared (Subscription_t626661309 * __this, const MethodInfo* method);
#define Subscription_Dispose_m405702546(__this, method) ((  void (*) (Subscription_t626661309 *, const MethodInfo*))Subscription_Dispose_m405702546_gshared)(__this, method)
