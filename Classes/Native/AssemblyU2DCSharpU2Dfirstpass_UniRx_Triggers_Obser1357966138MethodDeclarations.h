﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Triggers.ObservableSelectTrigger
struct ObservableSelectTrigger_t1357966138;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t3547103726;
// UniRx.IObservable`1<UnityEngine.EventSystems.BaseEventData>
struct IObservable_1_t3305902090;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseEventD3547103726.h"

// System.Void UniRx.Triggers.ObservableSelectTrigger::.ctor()
extern "C"  void ObservableSelectTrigger__ctor_m3960552689 (ObservableSelectTrigger_t1357966138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableSelectTrigger::UnityEngine.EventSystems.ISelectHandler.OnSelect(UnityEngine.EventSystems.BaseEventData)
extern "C"  void ObservableSelectTrigger_UnityEngine_EventSystems_ISelectHandler_OnSelect_m666308324 (ObservableSelectTrigger_t1357966138 * __this, BaseEventData_t3547103726 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.BaseEventData> UniRx.Triggers.ObservableSelectTrigger::OnSelectAsObservable()
extern "C"  Il2CppObject* ObservableSelectTrigger_OnSelectAsObservable_m3601589557 (ObservableSelectTrigger_t1357966138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableSelectTrigger::RaiseOnCompletedOnDestroy()
extern "C"  void ObservableSelectTrigger_RaiseOnCompletedOnDestroy_m221196714 (ObservableSelectTrigger_t1357966138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
