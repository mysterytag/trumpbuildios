﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TutorialPlayer/<WaitForBill>c__Iterator25
struct U3CWaitForBillU3Ec__Iterator25_t2713529525;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void TutorialPlayer/<WaitForBill>c__Iterator25::.ctor()
extern "C"  void U3CWaitForBillU3Ec__Iterator25__ctor_m240077382 (U3CWaitForBillU3Ec__Iterator25_t2713529525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TutorialPlayer/<WaitForBill>c__Iterator25::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitForBillU3Ec__Iterator25_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2085256972 (U3CWaitForBillU3Ec__Iterator25_t2713529525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TutorialPlayer/<WaitForBill>c__Iterator25::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitForBillU3Ec__Iterator25_System_Collections_IEnumerator_get_Current_m3532921504 (U3CWaitForBillU3Ec__Iterator25_t2713529525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TutorialPlayer/<WaitForBill>c__Iterator25::MoveNext()
extern "C"  bool U3CWaitForBillU3Ec__Iterator25_MoveNext_m369153774 (U3CWaitForBillU3Ec__Iterator25_t2713529525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialPlayer/<WaitForBill>c__Iterator25::Dispose()
extern "C"  void U3CWaitForBillU3Ec__Iterator25_Dispose_m2979380483 (U3CWaitForBillU3Ec__Iterator25_t2713529525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialPlayer/<WaitForBill>c__Iterator25::Reset()
extern "C"  void U3CWaitForBillU3Ec__Iterator25_Reset_m2181477619 (U3CWaitForBillU3Ec__Iterator25_t2713529525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialPlayer/<WaitForBill>c__Iterator25::<>m__152()
extern "C"  void U3CWaitForBillU3Ec__Iterator25_U3CU3Em__152_m3854660417 (U3CWaitForBillU3Ec__Iterator25_t2713529525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
