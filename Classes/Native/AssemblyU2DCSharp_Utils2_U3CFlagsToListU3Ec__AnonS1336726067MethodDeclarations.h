﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Utils2/<FlagsToList>c__AnonStoreyF2`1<System.Object>
struct U3CFlagsToListU3Ec__AnonStoreyF2_1_t1336726067;

#include "codegen/il2cpp-codegen.h"

// System.Void Utils2/<FlagsToList>c__AnonStoreyF2`1<System.Object>::.ctor()
extern "C"  void U3CFlagsToListU3Ec__AnonStoreyF2_1__ctor_m2532669463_gshared (U3CFlagsToListU3Ec__AnonStoreyF2_1_t1336726067 * __this, const MethodInfo* method);
#define U3CFlagsToListU3Ec__AnonStoreyF2_1__ctor_m2532669463(__this, method) ((  void (*) (U3CFlagsToListU3Ec__AnonStoreyF2_1_t1336726067 *, const MethodInfo*))U3CFlagsToListU3Ec__AnonStoreyF2_1__ctor_m2532669463_gshared)(__this, method)
// System.Boolean Utils2/<FlagsToList>c__AnonStoreyF2`1<System.Object>::<>m__15A(System.Int32)
extern "C"  bool U3CFlagsToListU3Ec__AnonStoreyF2_1_U3CU3Em__15A_m1099480996_gshared (U3CFlagsToListU3Ec__AnonStoreyF2_1_t1336726067 * __this, int32_t ___m0, const MethodInfo* method);
#define U3CFlagsToListU3Ec__AnonStoreyF2_1_U3CU3Em__15A_m1099480996(__this, ___m0, method) ((  bool (*) (U3CFlagsToListU3Ec__AnonStoreyF2_1_t1336726067 *, int32_t, const MethodInfo*))U3CFlagsToListU3Ec__AnonStoreyF2_1_U3CU3Em__15A_m1099480996_gshared)(__this, ___m0, method)
