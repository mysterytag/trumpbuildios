﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/<TimerFrame>c__AnonStorey53
struct U3CTimerFrameU3Ec__AnonStorey53_t1969998059;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// UniRx.IObserver`1<System.Int64>
struct IObserver_1_t764446489;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560.h"

// System.Void UniRx.Observable/<TimerFrame>c__AnonStorey53::.ctor()
extern "C"  void U3CTimerFrameU3Ec__AnonStorey53__ctor_m3135036728 (U3CTimerFrameU3Ec__AnonStorey53_t1969998059 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UniRx.Observable/<TimerFrame>c__AnonStorey53::<>m__56(UniRx.IObserver`1<System.Int64>,UniRx.CancellationToken)
extern "C"  Il2CppObject * U3CTimerFrameU3Ec__AnonStorey53_U3CU3Em__56_m685773346 (U3CTimerFrameU3Ec__AnonStorey53_t1969998059 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellation1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
