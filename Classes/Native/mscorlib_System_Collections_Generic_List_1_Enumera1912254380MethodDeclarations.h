﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4014815677MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Cell`1<System.Int32>>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3857894393(__this, ___l0, method) ((  void (*) (Enumerator_t1912254380 *, List_1_t3826471388 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Cell`1<System.Int32>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1032384697(__this, method) ((  void (*) (Enumerator_t1912254380 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Cell`1<System.Int32>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1101548143(__this, method) ((  Il2CppObject * (*) (Enumerator_t1912254380 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Cell`1<System.Int32>>::Dispose()
#define Enumerator_Dispose_m1788028958(__this, method) ((  void (*) (Enumerator_t1912254380 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Cell`1<System.Int32>>::VerifyState()
#define Enumerator_VerifyState_m2274994263(__this, method) ((  void (*) (Enumerator_t1912254380 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Cell`1<System.Int32>>::MoveNext()
#define Enumerator_MoveNext_m3055605993(__this, method) ((  bool (*) (Enumerator_t1912254380 *, const MethodInfo*))Enumerator_MoveNext_m302222300_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Cell`1<System.Int32>>::get_Current()
#define Enumerator_get_Current_m3733889904(__this, method) ((  Cell_1_t3029512419 * (*) (Enumerator_t1912254380 *, const MethodInfo*))Enumerator_get_Current_m1113061044_gshared)(__this, method)
