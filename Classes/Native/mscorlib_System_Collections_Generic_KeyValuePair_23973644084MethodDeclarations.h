﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21028297614MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m735328229(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3973644084 *, String_t*, int64_t, const MethodInfo*))KeyValuePair_2__ctor_m2388733843_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>::get_Key()
#define KeyValuePair_2_get_Key_m583521992(__this, method) ((  String_t* (*) (KeyValuePair_2_t3973644084 *, const MethodInfo*))KeyValuePair_2_get_Key_m2182089461_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m171378468(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3973644084 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m903707318_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>::get_Value()
#define KeyValuePair_2_get_Value_m380273081(__this, method) ((  int64_t (*) (KeyValuePair_2_t3973644084 *, const MethodInfo*))KeyValuePair_2_get_Value_m4174236661_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1873946916(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3973644084 *, int64_t, const MethodInfo*))KeyValuePair_2_set_Value_m3035661238_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>::ToString()
#define KeyValuePair_2_ToString_m4282557374(__this, method) ((  String_t* (*) (KeyValuePair_2_t3973644084 *, const MethodInfo*))KeyValuePair_2_ToString_m1268853100_gshared)(__this, method)
