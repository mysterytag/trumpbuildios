﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.InternalUtil.ScheduledItem
struct ScheduledItem_t1912903245;

#include "mscorlib_System_ValueType4014882752.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.InternalUtil.PriorityQueue`1/IndexedItem<UniRx.InternalUtil.ScheduledItem>
struct  IndexedItem_t1029390262 
{
public:
	// T UniRx.InternalUtil.PriorityQueue`1/IndexedItem::Value
	ScheduledItem_t1912903245 * ___Value_0;
	// System.Int64 UniRx.InternalUtil.PriorityQueue`1/IndexedItem::Id
	int64_t ___Id_1;

public:
	inline static int32_t get_offset_of_Value_0() { return static_cast<int32_t>(offsetof(IndexedItem_t1029390262, ___Value_0)); }
	inline ScheduledItem_t1912903245 * get_Value_0() const { return ___Value_0; }
	inline ScheduledItem_t1912903245 ** get_address_of_Value_0() { return &___Value_0; }
	inline void set_Value_0(ScheduledItem_t1912903245 * value)
	{
		___Value_0 = value;
		Il2CppCodeGenWriteBarrier(&___Value_0, value);
	}

	inline static int32_t get_offset_of_Id_1() { return static_cast<int32_t>(offsetof(IndexedItem_t1029390262, ___Id_1)); }
	inline int64_t get_Id_1() const { return ___Id_1; }
	inline int64_t* get_address_of_Id_1() { return &___Id_1; }
	inline void set_Id_1(int64_t value)
	{
		___Id_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
