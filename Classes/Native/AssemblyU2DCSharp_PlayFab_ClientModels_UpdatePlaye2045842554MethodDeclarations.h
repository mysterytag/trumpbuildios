﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.UpdatePlayerStatisticsResult
struct UpdatePlayerStatisticsResult_t2045842554;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.UpdatePlayerStatisticsResult::.ctor()
extern "C"  void UpdatePlayerStatisticsResult__ctor_m232461827 (UpdatePlayerStatisticsResult_t2045842554 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
