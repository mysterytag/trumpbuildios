﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.ListObserver`1<System.Single>
struct ListObserver_1_t1252826692;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<System.Single>>
struct ImmutableList_1_t4230412423;
// System.Exception
struct Exception_t1967233988;
// UniRx.IObserver`1<System.Single>
struct IObserver_1_t3170207924;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.InternalUtil.ListObserver`1<System.Single>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m3940489002_gshared (ListObserver_1_t1252826692 * __this, ImmutableList_1_t4230412423 * ___observers0, const MethodInfo* method);
#define ListObserver_1__ctor_m3940489002(__this, ___observers0, method) ((  void (*) (ListObserver_1_t1252826692 *, ImmutableList_1_t4230412423 *, const MethodInfo*))ListObserver_1__ctor_m3940489002_gshared)(__this, ___observers0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<System.Single>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m696664034_gshared (ListObserver_1_t1252826692 * __this, const MethodInfo* method);
#define ListObserver_1_OnCompleted_m696664034(__this, method) ((  void (*) (ListObserver_1_t1252826692 *, const MethodInfo*))ListObserver_1_OnCompleted_m696664034_gshared)(__this, method)
// System.Void UniRx.InternalUtil.ListObserver`1<System.Single>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m4218826255_gshared (ListObserver_1_t1252826692 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ListObserver_1_OnError_m4218826255(__this, ___error0, method) ((  void (*) (ListObserver_1_t1252826692 *, Exception_t1967233988 *, const MethodInfo*))ListObserver_1_OnError_m4218826255_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<System.Single>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m3837268736_gshared (ListObserver_1_t1252826692 * __this, float ___value0, const MethodInfo* method);
#define ListObserver_1_OnNext_m3837268736(__this, ___value0, method) ((  void (*) (ListObserver_1_t1252826692 *, float, const MethodInfo*))ListObserver_1_OnNext_m3837268736_gshared)(__this, ___value0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<System.Single>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m2564945042_gshared (ListObserver_1_t1252826692 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Add_m2564945042(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t1252826692 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Add_m2564945042_gshared)(__this, ___observer0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<System.Single>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m948028793_gshared (ListObserver_1_t1252826692 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Remove_m948028793(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t1252826692 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Remove_m948028793_gshared)(__this, ___observer0, method)
