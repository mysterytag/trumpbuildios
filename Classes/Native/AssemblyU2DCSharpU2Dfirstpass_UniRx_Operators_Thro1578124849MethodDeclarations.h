﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ThrowObservable`1/Throw<UniRx.Unit>
struct Throw_t1578124849;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.ThrowObservable`1/Throw<UniRx.Unit>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Throw__ctor_m1884098931_gshared (Throw_t1578124849 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define Throw__ctor_m1884098931(__this, ___observer0, ___cancel1, method) ((  void (*) (Throw_t1578124849 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Throw__ctor_m1884098931_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.ThrowObservable`1/Throw<UniRx.Unit>::OnNext(T)
extern "C"  void Throw_OnNext_m2682828351_gshared (Throw_t1578124849 * __this, Unit_t2558286038  ___value0, const MethodInfo* method);
#define Throw_OnNext_m2682828351(__this, ___value0, method) ((  void (*) (Throw_t1578124849 *, Unit_t2558286038 , const MethodInfo*))Throw_OnNext_m2682828351_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.ThrowObservable`1/Throw<UniRx.Unit>::OnError(System.Exception)
extern "C"  void Throw_OnError_m1489924942_gshared (Throw_t1578124849 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Throw_OnError_m1489924942(__this, ___error0, method) ((  void (*) (Throw_t1578124849 *, Exception_t1967233988 *, const MethodInfo*))Throw_OnError_m1489924942_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.ThrowObservable`1/Throw<UniRx.Unit>::OnCompleted()
extern "C"  void Throw_OnCompleted_m3374656417_gshared (Throw_t1578124849 * __this, const MethodInfo* method);
#define Throw_OnCompleted_m3374656417(__this, method) ((  void (*) (Throw_t1578124849 *, const MethodInfo*))Throw_OnCompleted_m3374656417_gshared)(__this, method)
