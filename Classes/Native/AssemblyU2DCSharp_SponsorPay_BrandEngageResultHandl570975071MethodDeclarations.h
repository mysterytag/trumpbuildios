﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SponsorPay.BrandEngageResultHandler
struct BrandEngageResultHandler_t570975071;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void SponsorPay.BrandEngageResultHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void BrandEngageResultHandler__ctor_m73435548 (BrandEngageResultHandler_t570975071 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.BrandEngageResultHandler::Invoke(System.String)
extern "C"  void BrandEngageResultHandler_Invoke_m2137873868 (BrandEngageResultHandler_t570975071 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_BrandEngageResultHandler_t570975071(Il2CppObject* delegate, String_t* ___message0);
// System.IAsyncResult SponsorPay.BrandEngageResultHandler::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * BrandEngageResultHandler_BeginInvoke_m1646005465 (BrandEngageResultHandler_t570975071 * __this, String_t* ___message0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.BrandEngageResultHandler::EndInvoke(System.IAsyncResult)
extern "C"  void BrandEngageResultHandler_EndInvoke_m3620809132 (BrandEngageResultHandler_t570975071 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
