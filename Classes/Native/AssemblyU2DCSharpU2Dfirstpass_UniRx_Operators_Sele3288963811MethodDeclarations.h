﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectObservable`2/Select_<System.Int64,System.Boolean>
struct Select__t3288963811;
// UniRx.Operators.SelectObservable`2<System.Int64,System.Boolean>
struct SelectObservable_2_t691402772;
// UniRx.IObserver`1<System.Boolean>
struct IObserver_1_t2423004244;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SelectObservable`2/Select_<System.Int64,System.Boolean>::.ctor(UniRx.Operators.SelectObservable`2<T,TR>,UniRx.IObserver`1<TR>,System.IDisposable)
extern "C"  void Select___ctor_m998639904_gshared (Select__t3288963811 * __this, SelectObservable_2_t691402772 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Select___ctor_m998639904(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Select__t3288963811 *, SelectObservable_2_t691402772 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Select___ctor_m998639904_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.SelectObservable`2/Select_<System.Int64,System.Boolean>::OnNext(T)
extern "C"  void Select__OnNext_m848278376_gshared (Select__t3288963811 * __this, int64_t ___value0, const MethodInfo* method);
#define Select__OnNext_m848278376(__this, ___value0, method) ((  void (*) (Select__t3288963811 *, int64_t, const MethodInfo*))Select__OnNext_m848278376_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SelectObservable`2/Select_<System.Int64,System.Boolean>::OnError(System.Exception)
extern "C"  void Select__OnError_m2869952119_gshared (Select__t3288963811 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Select__OnError_m2869952119(__this, ___error0, method) ((  void (*) (Select__t3288963811 *, Exception_t1967233988 *, const MethodInfo*))Select__OnError_m2869952119_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SelectObservable`2/Select_<System.Int64,System.Boolean>::OnCompleted()
extern "C"  void Select__OnCompleted_m2286382154_gshared (Select__t3288963811 * __this, const MethodInfo* method);
#define Select__OnCompleted_m2286382154(__this, method) ((  void (*) (Select__t3288963811 *, const MethodInfo*))Select__OnCompleted_m2286382154_gshared)(__this, method)
