﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.MasterServerEvent>>
struct ImmutableList_1_t1255043596;
// UniRx.IObserver`1<UnityEngine.MasterServerEvent>[]
struct IObserver_1U5BU5D_t2992830660;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.MasterServerEvent>>
struct  ImmutableList_1_t1255043596  : public Il2CppObject
{
public:
	// T[] UniRx.InternalUtil.ImmutableList`1::data
	IObserver_1U5BU5D_t2992830660* ___data_1;

public:
	inline static int32_t get_offset_of_data_1() { return static_cast<int32_t>(offsetof(ImmutableList_1_t1255043596, ___data_1)); }
	inline IObserver_1U5BU5D_t2992830660* get_data_1() const { return ___data_1; }
	inline IObserver_1U5BU5D_t2992830660** get_address_of_data_1() { return &___data_1; }
	inline void set_data_1(IObserver_1U5BU5D_t2992830660* value)
	{
		___data_1 = value;
		Il2CppCodeGenWriteBarrier(&___data_1, value);
	}
};

struct ImmutableList_1_t1255043596_StaticFields
{
public:
	// UniRx.InternalUtil.ImmutableList`1<T> UniRx.InternalUtil.ImmutableList`1::Empty
	ImmutableList_1_t1255043596 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(ImmutableList_1_t1255043596_StaticFields, ___Empty_0)); }
	inline ImmutableList_1_t1255043596 * get_Empty_0() const { return ___Empty_0; }
	inline ImmutableList_1_t1255043596 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(ImmutableList_1_t1255043596 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier(&___Empty_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
