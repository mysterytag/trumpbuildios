﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.KeyedFactoryBase`2<System.Object,System.Object>
struct KeyedFactoryBase_2_t2864689411;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1302937806;
// System.Collections.Generic.Dictionary`2<System.Object,System.Type>
struct Dictionary_2_t1471581369;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException>
struct IEnumerable_1_t4073207355;
// ModestTree.Util.Tuple`2<System.Object,System.Type>
struct Tuple_2_t231167918;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void Zenject.KeyedFactoryBase`2<System.Object,System.Object>::.ctor()
extern "C"  void KeyedFactoryBase_2__ctor_m1400309807_gshared (KeyedFactoryBase_2_t2864689411 * __this, const MethodInfo* method);
#define KeyedFactoryBase_2__ctor_m1400309807(__this, method) ((  void (*) (KeyedFactoryBase_2_t2864689411 *, const MethodInfo*))KeyedFactoryBase_2__ctor_m1400309807_gshared)(__this, method)
// Zenject.DiContainer Zenject.KeyedFactoryBase`2<System.Object,System.Object>::get_Container()
extern "C"  DiContainer_t2383114449 * KeyedFactoryBase_2_get_Container_m199406203_gshared (KeyedFactoryBase_2_t2864689411 * __this, const MethodInfo* method);
#define KeyedFactoryBase_2_get_Container_m199406203(__this, method) ((  DiContainer_t2383114449 * (*) (KeyedFactoryBase_2_t2864689411 *, const MethodInfo*))KeyedFactoryBase_2_get_Container_m199406203_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TKey> Zenject.KeyedFactoryBase`2<System.Object,System.Object>::get_Keys()
extern "C"  Il2CppObject* KeyedFactoryBase_2_get_Keys_m1582078485_gshared (KeyedFactoryBase_2_t2864689411 * __this, const MethodInfo* method);
#define KeyedFactoryBase_2_get_Keys_m1582078485(__this, method) ((  Il2CppObject* (*) (KeyedFactoryBase_2_t2864689411 *, const MethodInfo*))KeyedFactoryBase_2_get_Keys_m1582078485_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2<TKey,System.Type> Zenject.KeyedFactoryBase`2<System.Object,System.Object>::get_TypeMap()
extern "C"  Dictionary_2_t1471581369 * KeyedFactoryBase_2_get_TypeMap_m2764580472_gshared (KeyedFactoryBase_2_t2864689411 * __this, const MethodInfo* method);
#define KeyedFactoryBase_2_get_TypeMap_m2764580472(__this, method) ((  Dictionary_2_t1471581369 * (*) (KeyedFactoryBase_2_t2864689411 *, const MethodInfo*))KeyedFactoryBase_2_get_TypeMap_m2764580472_gshared)(__this, method)
// System.Void Zenject.KeyedFactoryBase`2<System.Object,System.Object>::Initialize()
extern "C"  void KeyedFactoryBase_2_Initialize_m1783501765_gshared (KeyedFactoryBase_2_t2864689411 * __this, const MethodInfo* method);
#define KeyedFactoryBase_2_Initialize_m1783501765(__this, method) ((  void (*) (KeyedFactoryBase_2_t2864689411 *, const MethodInfo*))KeyedFactoryBase_2_Initialize_m1783501765_gshared)(__this, method)
// System.Boolean Zenject.KeyedFactoryBase`2<System.Object,System.Object>::HasKey(TKey)
extern "C"  bool KeyedFactoryBase_2_HasKey_m976801691_gshared (KeyedFactoryBase_2_t2864689411 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define KeyedFactoryBase_2_HasKey_m976801691(__this, ___key0, method) ((  bool (*) (KeyedFactoryBase_2_t2864689411 *, Il2CppObject *, const MethodInfo*))KeyedFactoryBase_2_HasKey_m976801691_gshared)(__this, ___key0, method)
// System.Type Zenject.KeyedFactoryBase`2<System.Object,System.Object>::GetTypeForKey(TKey)
extern "C"  Type_t * KeyedFactoryBase_2_GetTypeForKey_m3482229634_gshared (KeyedFactoryBase_2_t2864689411 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define KeyedFactoryBase_2_GetTypeForKey_m3482229634(__this, ___key0, method) ((  Type_t * (*) (KeyedFactoryBase_2_t2864689411 *, Il2CppObject *, const MethodInfo*))KeyedFactoryBase_2_GetTypeForKey_m3482229634_gshared)(__this, ___key0, method)
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.KeyedFactoryBase`2<System.Object,System.Object>::Validate()
extern "C"  Il2CppObject* KeyedFactoryBase_2_Validate_m1994894092_gshared (KeyedFactoryBase_2_t2864689411 * __this, const MethodInfo* method);
#define KeyedFactoryBase_2_Validate_m1994894092(__this, method) ((  Il2CppObject* (*) (KeyedFactoryBase_2_t2864689411 *, const MethodInfo*))KeyedFactoryBase_2_Validate_m1994894092_gshared)(__this, method)
// TKey Zenject.KeyedFactoryBase`2<System.Object,System.Object>::<Initialize>m__2AC(ModestTree.Util.Tuple`2<TKey,System.Type>)
extern "C"  Il2CppObject * KeyedFactoryBase_2_U3CInitializeU3Em__2AC_m1544969010_gshared (Il2CppObject * __this /* static, unused */, Tuple_2_t231167918 * ___x0, const MethodInfo* method);
#define KeyedFactoryBase_2_U3CInitializeU3Em__2AC_m1544969010(__this /* static, unused */, ___x0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Tuple_2_t231167918 *, const MethodInfo*))KeyedFactoryBase_2_U3CInitializeU3Em__2AC_m1544969010_gshared)(__this /* static, unused */, ___x0, method)
// System.String Zenject.KeyedFactoryBase`2<System.Object,System.Object>::<Initialize>m__2AD(TKey)
extern "C"  String_t* KeyedFactoryBase_2_U3CInitializeU3Em__2AD_m1089584577_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___x0, const MethodInfo* method);
#define KeyedFactoryBase_2_U3CInitializeU3Em__2AD_m1089584577(__this /* static, unused */, ___x0, method) ((  String_t* (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))KeyedFactoryBase_2_U3CInitializeU3Em__2AD_m1089584577_gshared)(__this /* static, unused */, ___x0, method)
// TKey Zenject.KeyedFactoryBase`2<System.Object,System.Object>::<Initialize>m__2AE(ModestTree.Util.Tuple`2<TKey,System.Type>)
extern "C"  Il2CppObject * KeyedFactoryBase_2_U3CInitializeU3Em__2AE_m3790548464_gshared (Il2CppObject * __this /* static, unused */, Tuple_2_t231167918 * ___x0, const MethodInfo* method);
#define KeyedFactoryBase_2_U3CInitializeU3Em__2AE_m3790548464(__this /* static, unused */, ___x0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Tuple_2_t231167918 *, const MethodInfo*))KeyedFactoryBase_2_U3CInitializeU3Em__2AE_m3790548464_gshared)(__this /* static, unused */, ___x0, method)
// System.Type Zenject.KeyedFactoryBase`2<System.Object,System.Object>::<Initialize>m__2AF(ModestTree.Util.Tuple`2<TKey,System.Type>)
extern "C"  Type_t * KeyedFactoryBase_2_U3CInitializeU3Em__2AF_m2468757815_gshared (Il2CppObject * __this /* static, unused */, Tuple_2_t231167918 * ___x0, const MethodInfo* method);
#define KeyedFactoryBase_2_U3CInitializeU3Em__2AF_m2468757815(__this /* static, unused */, ___x0, method) ((  Type_t * (*) (Il2CppObject * /* static, unused */, Tuple_2_t231167918 *, const MethodInfo*))KeyedFactoryBase_2_U3CInitializeU3Em__2AF_m2468757815_gshared)(__this /* static, unused */, ___x0, method)
