﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SettingsButton
struct SettingsButton_t915256661;

#include "codegen/il2cpp-codegen.h"

// System.Void SettingsButton::.ctor()
extern "C"  void SettingsButton__ctor_m1199982310 (SettingsButton_t915256661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
