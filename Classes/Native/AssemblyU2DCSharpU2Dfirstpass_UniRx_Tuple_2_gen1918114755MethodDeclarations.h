﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen369261819MethodDeclarations.h"

// System.Void UniRx.Tuple`2<UnityEngine.RenderTexture,UnityEngine.RenderTexture>::.ctor(T1,T2)
#define Tuple_2__ctor_m2788700894(__this, ___item10, ___item21, method) ((  void (*) (Tuple_2_t1918114755 *, RenderTexture_t12905170 *, RenderTexture_t12905170 *, const MethodInfo*))Tuple_2__ctor_m101027774_gshared)(__this, ___item10, ___item21, method)
// System.Int32 UniRx.Tuple`2<UnityEngine.RenderTexture,UnityEngine.RenderTexture>::System.IComparable.CompareTo(System.Object)
#define Tuple_2_System_IComparable_CompareTo_m539239723(__this, ___obj0, method) ((  int32_t (*) (Tuple_2_t1918114755 *, Il2CppObject *, const MethodInfo*))Tuple_2_System_IComparable_CompareTo_m964946507_gshared)(__this, ___obj0, method)
// System.Int32 UniRx.Tuple`2<UnityEngine.RenderTexture,UnityEngine.RenderTexture>::UniRx.IStructuralComparable.CompareTo(System.Object,System.Collections.IComparer)
#define Tuple_2_UniRx_IStructuralComparable_CompareTo_m1733092797(__this, ___other0, ___comparer1, method) ((  int32_t (*) (Tuple_2_t1918114755 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_2_UniRx_IStructuralComparable_CompareTo_m1607455453_gshared)(__this, ___other0, ___comparer1, method)
// System.Boolean UniRx.Tuple`2<UnityEngine.RenderTexture,UnityEngine.RenderTexture>::UniRx.IStructuralEquatable.Equals(System.Object,System.Collections.IEqualityComparer)
#define Tuple_2_UniRx_IStructuralEquatable_Equals_m2599265960(__this, ___other0, ___comparer1, method) ((  bool (*) (Tuple_2_t1918114755 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_2_UniRx_IStructuralEquatable_Equals_m2678862280_gshared)(__this, ___other0, ___comparer1, method)
// System.Int32 UniRx.Tuple`2<UnityEngine.RenderTexture,UnityEngine.RenderTexture>::UniRx.IStructuralEquatable.GetHashCode(System.Collections.IEqualityComparer)
#define Tuple_2_UniRx_IStructuralEquatable_GetHashCode_m3145457542(__this, ___comparer0, method) ((  int32_t (*) (Tuple_2_t1918114755 *, Il2CppObject *, const MethodInfo*))Tuple_2_UniRx_IStructuralEquatable_GetHashCode_m2682338918_gshared)(__this, ___comparer0, method)
// System.String UniRx.Tuple`2<UnityEngine.RenderTexture,UnityEngine.RenderTexture>::UniRx.ITuple.ToString()
#define Tuple_2_UniRx_ITuple_ToString_m4074299685(__this, method) ((  String_t* (*) (Tuple_2_t1918114755 *, const MethodInfo*))Tuple_2_UniRx_ITuple_ToString_m2079956805_gshared)(__this, method)
// T1 UniRx.Tuple`2<UnityEngine.RenderTexture,UnityEngine.RenderTexture>::get_Item1()
#define Tuple_2_get_Item1_m62808402(__this, method) ((  RenderTexture_t12905170 * (*) (Tuple_2_t1918114755 *, const MethodInfo*))Tuple_2_get_Item1_m425160818_gshared)(__this, method)
// T2 UniRx.Tuple`2<UnityEngine.RenderTexture,UnityEngine.RenderTexture>::get_Item2()
#define Tuple_2_get_Item2_m3828857620(__this, method) ((  RenderTexture_t12905170 * (*) (Tuple_2_t1918114755 *, const MethodInfo*))Tuple_2_get_Item2_m1055620404_gshared)(__this, method)
// System.Boolean UniRx.Tuple`2<UnityEngine.RenderTexture,UnityEngine.RenderTexture>::Equals(System.Object)
#define Tuple_2_Equals_m2243293532(__this, ___obj0, method) ((  bool (*) (Tuple_2_t1918114755 *, Il2CppObject *, const MethodInfo*))Tuple_2_Equals_m2489154684_gshared)(__this, ___obj0, method)
// System.Int32 UniRx.Tuple`2<UnityEngine.RenderTexture,UnityEngine.RenderTexture>::GetHashCode()
#define Tuple_2_GetHashCode_m1379563008(__this, method) ((  int32_t (*) (Tuple_2_t1918114755 *, const MethodInfo*))Tuple_2_GetHashCode_m1023013664_gshared)(__this, method)
// System.String UniRx.Tuple`2<UnityEngine.RenderTexture,UnityEngine.RenderTexture>::ToString()
#define Tuple_2_ToString_m1472987628(__this, method) ((  String_t* (*) (Tuple_2_t1918114755 *, const MethodInfo*))Tuple_2_ToString_m3182481356_gshared)(__this, method)
// System.Boolean UniRx.Tuple`2<UnityEngine.RenderTexture,UnityEngine.RenderTexture>::Equals(UniRx.Tuple`2<T1,T2>)
#define Tuple_2_Equals_m2830985997(__this, ___other0, method) ((  bool (*) (Tuple_2_t1918114755 *, Tuple_2_t1918114755 , const MethodInfo*))Tuple_2_Equals_m1605966829_gshared)(__this, ___other0, method)
