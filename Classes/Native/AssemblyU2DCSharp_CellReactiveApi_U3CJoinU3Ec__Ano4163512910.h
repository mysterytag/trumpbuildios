﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ICell`1<ICell`1<System.Int32>>
struct ICell_1_t1655709445;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CellReactiveApi/<Join>c__AnonStorey114`1<System.Int32>
struct  U3CJoinU3Ec__AnonStorey114_1_t4163512910  : public Il2CppObject
{
public:
	// ICell`1<ICell`1<T>> CellReactiveApi/<Join>c__AnonStorey114`1::cell
	Il2CppObject* ___cell_0;

public:
	inline static int32_t get_offset_of_cell_0() { return static_cast<int32_t>(offsetof(U3CJoinU3Ec__AnonStorey114_1_t4163512910, ___cell_0)); }
	inline Il2CppObject* get_cell_0() const { return ___cell_0; }
	inline Il2CppObject** get_address_of_cell_0() { return &___cell_0; }
	inline void set_cell_0(Il2CppObject* value)
	{
		___cell_0 = value;
		Il2CppCodeGenWriteBarrier(&___cell_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
