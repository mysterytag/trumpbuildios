﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen128212638.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharp_SponsorPay_SPLogLevel220722135.h"

// System.Void System.Array/InternalEnumerator`1<SponsorPay.SPLogLevel>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m172376194_gshared (InternalEnumerator_1_t128212638 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m172376194(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t128212638 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m172376194_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<SponsorPay.SPLogLevel>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2517553630_gshared (InternalEnumerator_1_t128212638 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2517553630(__this, method) ((  void (*) (InternalEnumerator_1_t128212638 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2517553630_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<SponsorPay.SPLogLevel>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2226199060_gshared (InternalEnumerator_1_t128212638 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2226199060(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t128212638 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2226199060_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<SponsorPay.SPLogLevel>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3567264985_gshared (InternalEnumerator_1_t128212638 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3567264985(__this, method) ((  void (*) (InternalEnumerator_1_t128212638 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3567264985_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<SponsorPay.SPLogLevel>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1464831502_gshared (InternalEnumerator_1_t128212638 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1464831502(__this, method) ((  bool (*) (InternalEnumerator_1_t128212638 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1464831502_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<SponsorPay.SPLogLevel>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m1771638763_gshared (InternalEnumerator_1_t128212638 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1771638763(__this, method) ((  int32_t (*) (InternalEnumerator_1_t128212638 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1771638763_gshared)(__this, method)
