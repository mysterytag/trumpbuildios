﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.EmptyObservable`1/Empty<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>
struct Empty_t2209798842;
// UniRx.IObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>
struct IObserver_1_t2462979911;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_DictionaryAddEv250981008.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Empty__ctor_m681291356_gshared (Empty_t2209798842 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define Empty__ctor_m681291356(__this, ___observer0, ___cancel1, method) ((  void (*) (Empty_t2209798842 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Empty__ctor_m681291356_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::OnNext(T)
extern "C"  void Empty_OnNext_m4036986806_gshared (Empty_t2209798842 * __this, DictionaryAddEvent_2_t250981008  ___value0, const MethodInfo* method);
#define Empty_OnNext_m4036986806(__this, ___value0, method) ((  void (*) (Empty_t2209798842 *, DictionaryAddEvent_2_t250981008 , const MethodInfo*))Empty_OnNext_m4036986806_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::OnError(System.Exception)
extern "C"  void Empty_OnError_m2864281285_gshared (Empty_t2209798842 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Empty_OnError_m2864281285(__this, ___error0, method) ((  void (*) (Empty_t2209798842 *, Exception_t1967233988 *, const MethodInfo*))Empty_OnError_m2864281285_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::OnCompleted()
extern "C"  void Empty_OnCompleted_m1452829080_gshared (Empty_t2209798842 * __this, const MethodInfo* method);
#define Empty_OnCompleted_m1452829080(__this, method) ((  void (*) (Empty_t2209798842 *, const MethodInfo*))Empty_OnCompleted_m1452829080_gshared)(__this, method)
