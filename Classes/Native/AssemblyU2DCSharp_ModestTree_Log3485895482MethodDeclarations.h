﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void ModestTree.Log::Debug(System.String,System.Object[])
extern "C"  void Log_Debug_m3985269754 (Il2CppObject * __this /* static, unused */, String_t* ___message0, ObjectU5BU5D_t11523773* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModestTree.Log::Info(System.String,System.Object[])
extern "C"  void Log_Info_m1110695167 (Il2CppObject * __this /* static, unused */, String_t* ___message0, ObjectU5BU5D_t11523773* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModestTree.Log::Warn(System.String,System.Object[])
extern "C"  void Log_Warn_m626914343 (Il2CppObject * __this /* static, unused */, String_t* ___message0, ObjectU5BU5D_t11523773* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModestTree.Log::Trace(System.String,System.Object[])
extern "C"  void Log_Trace_m1137978056 (Il2CppObject * __this /* static, unused */, String_t* ___message0, ObjectU5BU5D_t11523773* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModestTree.Log::ErrorException(System.Exception)
extern "C"  void Log_ErrorException_m1940003768 (Il2CppObject * __this /* static, unused */, Exception_t1967233988 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModestTree.Log::ErrorException(System.String,System.Exception)
extern "C"  void Log_ErrorException_m1080846204 (Il2CppObject * __this /* static, unused */, String_t* ___message0, Exception_t1967233988 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModestTree.Log::Error(System.String,System.Object[])
extern "C"  void Log_Error_m1405874597 (Il2CppObject * __this /* static, unused */, String_t* ___message0, ObjectU5BU5D_t11523773* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
