﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Examples.Sample06_ConvertToCoroutine/<ComplexCoroutineTest>c__Iterator7
struct U3CComplexCoroutineTestU3Ec__Iterator7_t4070488366;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Examples.Sample06_ConvertToCoroutine/<ComplexCoroutineTest>c__Iterator7::.ctor()
extern "C"  void U3CComplexCoroutineTestU3Ec__Iterator7__ctor_m1602681414 (U3CComplexCoroutineTestU3Ec__Iterator7_t4070488366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UniRx.Examples.Sample06_ConvertToCoroutine/<ComplexCoroutineTest>c__Iterator7::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CComplexCoroutineTestU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2109536396 (U3CComplexCoroutineTestU3Ec__Iterator7_t4070488366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UniRx.Examples.Sample06_ConvertToCoroutine/<ComplexCoroutineTest>c__Iterator7::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CComplexCoroutineTestU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m4280914976 (U3CComplexCoroutineTestU3Ec__Iterator7_t4070488366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.Examples.Sample06_ConvertToCoroutine/<ComplexCoroutineTest>c__Iterator7::MoveNext()
extern "C"  bool U3CComplexCoroutineTestU3Ec__Iterator7_MoveNext_m1892660998 (U3CComplexCoroutineTestU3Ec__Iterator7_t4070488366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample06_ConvertToCoroutine/<ComplexCoroutineTest>c__Iterator7::Dispose()
extern "C"  void U3CComplexCoroutineTestU3Ec__Iterator7_Dispose_m2476829955 (U3CComplexCoroutineTestU3Ec__Iterator7_t4070488366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample06_ConvertToCoroutine/<ComplexCoroutineTest>c__Iterator7::Reset()
extern "C"  void U3CComplexCoroutineTestU3Ec__Iterator7_Reset_m3544081651 (U3CComplexCoroutineTestU3Ec__Iterator7_t4070488366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample06_ConvertToCoroutine/<ComplexCoroutineTest>c__Iterator7::<>m__1C(System.Int32)
extern "C"  void U3CComplexCoroutineTestU3Ec__Iterator7_U3CU3Em__1C_m2281343250 (U3CComplexCoroutineTestU3Ec__Iterator7_t4070488366 * __this, int32_t ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample06_ConvertToCoroutine/<ComplexCoroutineTest>c__Iterator7::<>m__1D(System.Int32)
extern "C"  void U3CComplexCoroutineTestU3Ec__Iterator7_U3CU3Em__1D_m3788895059 (U3CComplexCoroutineTestU3Ec__Iterator7_t4070488366 * __this, int32_t ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
