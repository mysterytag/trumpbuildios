﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SponsorPay.SuccessfulCurrencyResponseReceivedHandler
struct SuccessfulCurrencyResponseReceivedHandler_t2285112823;
// System.Object
struct Il2CppObject;
// SponsorPay.SuccessfulCurrencyResponse
struct SuccessfulCurrencyResponse_t823827974;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "AssemblyU2DCSharp_SponsorPay_SuccessfulCurrencyResp823827974.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void SponsorPay.SuccessfulCurrencyResponseReceivedHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void SuccessfulCurrencyResponseReceivedHandler__ctor_m3240696260 (SuccessfulCurrencyResponseReceivedHandler_t2285112823 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SuccessfulCurrencyResponseReceivedHandler::Invoke(SponsorPay.SuccessfulCurrencyResponse)
extern "C"  void SuccessfulCurrencyResponseReceivedHandler_Invoke_m2015740648 (SuccessfulCurrencyResponseReceivedHandler_t2285112823 * __this, SuccessfulCurrencyResponse_t823827974 * ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_SuccessfulCurrencyResponseReceivedHandler_t2285112823(Il2CppObject* delegate, SuccessfulCurrencyResponse_t823827974 * ___response0);
// System.IAsyncResult SponsorPay.SuccessfulCurrencyResponseReceivedHandler::BeginInvoke(SponsorPay.SuccessfulCurrencyResponse,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * SuccessfulCurrencyResponseReceivedHandler_BeginInvoke_m2425495909 (SuccessfulCurrencyResponseReceivedHandler_t2285112823 * __this, SuccessfulCurrencyResponse_t823827974 * ___response0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SponsorPay.SuccessfulCurrencyResponseReceivedHandler::EndInvoke(System.IAsyncResult)
extern "C"  void SuccessfulCurrencyResponseReceivedHandler_EndInvoke_m3363311060 (SuccessfulCurrencyResponseReceivedHandler_t2285112823 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
