﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ReadOnlyReactiveProperty`1<System.Object>
struct ReadOnlyReactiveProperty_1_t3626508436;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Object
struct Il2CppObject;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.ReadOnlyReactiveProperty`1<System.Object>::.ctor(UniRx.IObservable`1<T>)
extern "C"  void ReadOnlyReactiveProperty_1__ctor_m3555015251_gshared (ReadOnlyReactiveProperty_1_t3626508436 * __this, Il2CppObject* ___source0, const MethodInfo* method);
#define ReadOnlyReactiveProperty_1__ctor_m3555015251(__this, ___source0, method) ((  void (*) (ReadOnlyReactiveProperty_1_t3626508436 *, Il2CppObject*, const MethodInfo*))ReadOnlyReactiveProperty_1__ctor_m3555015251_gshared)(__this, ___source0, method)
// System.Void UniRx.ReadOnlyReactiveProperty`1<System.Object>::.ctor(UniRx.IObservable`1<T>,T)
extern "C"  void ReadOnlyReactiveProperty_1__ctor_m1870661419_gshared (ReadOnlyReactiveProperty_1_t3626508436 * __this, Il2CppObject* ___source0, Il2CppObject * ___initialValue1, const MethodInfo* method);
#define ReadOnlyReactiveProperty_1__ctor_m1870661419(__this, ___source0, ___initialValue1, method) ((  void (*) (ReadOnlyReactiveProperty_1_t3626508436 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))ReadOnlyReactiveProperty_1__ctor_m1870661419_gshared)(__this, ___source0, ___initialValue1, method)
// T UniRx.ReadOnlyReactiveProperty`1<System.Object>::get_Value()
extern "C"  Il2CppObject * ReadOnlyReactiveProperty_1_get_Value_m4163066713_gshared (ReadOnlyReactiveProperty_1_t3626508436 * __this, const MethodInfo* method);
#define ReadOnlyReactiveProperty_1_get_Value_m4163066713(__this, method) ((  Il2CppObject * (*) (ReadOnlyReactiveProperty_1_t3626508436 *, const MethodInfo*))ReadOnlyReactiveProperty_1_get_Value_m4163066713_gshared)(__this, method)
// System.IDisposable UniRx.ReadOnlyReactiveProperty`1<System.Object>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * ReadOnlyReactiveProperty_1_Subscribe_m2445628745_gshared (ReadOnlyReactiveProperty_1_t3626508436 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ReadOnlyReactiveProperty_1_Subscribe_m2445628745(__this, ___observer0, method) ((  Il2CppObject * (*) (ReadOnlyReactiveProperty_1_t3626508436 *, Il2CppObject*, const MethodInfo*))ReadOnlyReactiveProperty_1_Subscribe_m2445628745_gshared)(__this, ___observer0, method)
// System.Void UniRx.ReadOnlyReactiveProperty`1<System.Object>::Dispose()
extern "C"  void ReadOnlyReactiveProperty_1_Dispose_m3221357233_gshared (ReadOnlyReactiveProperty_1_t3626508436 * __this, const MethodInfo* method);
#define ReadOnlyReactiveProperty_1_Dispose_m3221357233(__this, method) ((  void (*) (ReadOnlyReactiveProperty_1_t3626508436 *, const MethodInfo*))ReadOnlyReactiveProperty_1_Dispose_m3221357233_gshared)(__this, method)
// System.Void UniRx.ReadOnlyReactiveProperty`1<System.Object>::Dispose(System.Boolean)
extern "C"  void ReadOnlyReactiveProperty_1_Dispose_m1852943976_gshared (ReadOnlyReactiveProperty_1_t3626508436 * __this, bool ___disposing0, const MethodInfo* method);
#define ReadOnlyReactiveProperty_1_Dispose_m1852943976(__this, ___disposing0, method) ((  void (*) (ReadOnlyReactiveProperty_1_t3626508436 *, bool, const MethodInfo*))ReadOnlyReactiveProperty_1_Dispose_m1852943976_gshared)(__this, ___disposing0, method)
// System.String UniRx.ReadOnlyReactiveProperty`1<System.Object>::ToString()
extern "C"  String_t* ReadOnlyReactiveProperty_1_ToString_m3439667769_gshared (ReadOnlyReactiveProperty_1_t3626508436 * __this, const MethodInfo* method);
#define ReadOnlyReactiveProperty_1_ToString_m3439667769(__this, method) ((  String_t* (*) (ReadOnlyReactiveProperty_1_t3626508436 *, const MethodInfo*))ReadOnlyReactiveProperty_1_ToString_m3439667769_gshared)(__this, method)
// System.Boolean UniRx.ReadOnlyReactiveProperty`1<System.Object>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool ReadOnlyReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m2991635671_gshared (ReadOnlyReactiveProperty_1_t3626508436 * __this, const MethodInfo* method);
#define ReadOnlyReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m2991635671(__this, method) ((  bool (*) (ReadOnlyReactiveProperty_1_t3626508436 *, const MethodInfo*))ReadOnlyReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m2991635671_gshared)(__this, method)
