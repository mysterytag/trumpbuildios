﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.DefaultIfEmptyObservable`1/DefaultIfEmpty<System.Object>
struct DefaultIfEmpty_t3912419440;
// UniRx.Operators.DefaultIfEmptyObservable`1<System.Object>
struct DefaultIfEmptyObservable_1_t2718954633;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.DefaultIfEmptyObservable`1/DefaultIfEmpty<System.Object>::.ctor(UniRx.Operators.DefaultIfEmptyObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void DefaultIfEmpty__ctor_m1237426643_gshared (DefaultIfEmpty_t3912419440 * __this, DefaultIfEmptyObservable_1_t2718954633 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define DefaultIfEmpty__ctor_m1237426643(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (DefaultIfEmpty_t3912419440 *, DefaultIfEmptyObservable_1_t2718954633 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))DefaultIfEmpty__ctor_m1237426643_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.DefaultIfEmptyObservable`1/DefaultIfEmpty<System.Object>::OnNext(T)
extern "C"  void DefaultIfEmpty_OnNext_m2296050239_gshared (DefaultIfEmpty_t3912419440 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define DefaultIfEmpty_OnNext_m2296050239(__this, ___value0, method) ((  void (*) (DefaultIfEmpty_t3912419440 *, Il2CppObject *, const MethodInfo*))DefaultIfEmpty_OnNext_m2296050239_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.DefaultIfEmptyObservable`1/DefaultIfEmpty<System.Object>::OnError(System.Exception)
extern "C"  void DefaultIfEmpty_OnError_m3989352270_gshared (DefaultIfEmpty_t3912419440 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define DefaultIfEmpty_OnError_m3989352270(__this, ___error0, method) ((  void (*) (DefaultIfEmpty_t3912419440 *, Exception_t1967233988 *, const MethodInfo*))DefaultIfEmpty_OnError_m3989352270_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.DefaultIfEmptyObservable`1/DefaultIfEmpty<System.Object>::OnCompleted()
extern "C"  void DefaultIfEmpty_OnCompleted_m916023201_gshared (DefaultIfEmpty_t3912419440 * __this, const MethodInfo* method);
#define DefaultIfEmpty_OnCompleted_m916023201(__this, method) ((  void (*) (DefaultIfEmpty_t3912419440 *, const MethodInfo*))DefaultIfEmpty_OnCompleted_m916023201_gshared)(__this, method)
