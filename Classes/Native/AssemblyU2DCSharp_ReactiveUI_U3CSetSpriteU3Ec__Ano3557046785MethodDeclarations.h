﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ReactiveUI/<SetSprite>c__AnonStorey126
struct U3CSetSpriteU3Ec__AnonStorey126_t3557046785;
// UnityEngine.Sprite
struct Sprite_t4006040370;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Sprite4006040370.h"

// System.Void ReactiveUI/<SetSprite>c__AnonStorey126::.ctor()
extern "C"  void U3CSetSpriteU3Ec__AnonStorey126__ctor_m278296860 (U3CSetSpriteU3Ec__AnonStorey126_t3557046785 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReactiveUI/<SetSprite>c__AnonStorey126::<>m__1B6(UnityEngine.Sprite)
extern "C"  void U3CSetSpriteU3Ec__AnonStorey126_U3CU3Em__1B6_m3310653702 (U3CSetSpriteU3Ec__AnonStorey126_t3557046785 * __this, Sprite_t4006040370 * ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
