﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Zenject.SingletonId
struct SingletonId_t1838183899;
// Zenject.SingletonLazyCreator
struct SingletonLazyCreator_t2762284194;

#include "mscorlib_System_ValueType4014882752.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<Zenject.SingletonId,Zenject.SingletonLazyCreator>
struct  KeyValuePair_2_t1754212203 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	SingletonId_t1838183899 * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	SingletonLazyCreator_t2762284194 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1754212203, ___key_0)); }
	inline SingletonId_t1838183899 * get_key_0() const { return ___key_0; }
	inline SingletonId_t1838183899 ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(SingletonId_t1838183899 * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier(&___key_0, value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1754212203, ___value_1)); }
	inline SingletonLazyCreator_t2762284194 * get_value_1() const { return ___value_1; }
	inline SingletonLazyCreator_t2762284194 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(SingletonLazyCreator_t2762284194 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier(&___value_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
