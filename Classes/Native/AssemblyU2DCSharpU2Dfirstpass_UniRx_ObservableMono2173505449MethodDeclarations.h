﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ObservableMonoBehaviour
struct ObservableMonoBehaviour_t2173505449;
// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;
// UniRx.IObservable`1<System.Int32>
struct IObservable_1_t2606213151;
// UniRx.IObservable`1<System.Boolean>
struct IObservable_1_t4264771001;
// System.Single[]
struct SingleU5BU5D_t1219431280;
// UniRx.IObservable`1<UniRx.Tuple`2<System.Single[],System.Int32>>
struct IObservable_1_t3709910074;
// UnityEngine.Collision
struct Collision_t1119538015;
// UniRx.IObservable`1<UnityEngine.Collision>
struct IObservable_1_t878336379;
// UnityEngine.Collision2D
struct Collision2D_t452810033;
// UniRx.IObservable`1<UnityEngine.Collision2D>
struct IObservable_1_t211608397;
// UnityEngine.ControllerColliderHit
struct ControllerColliderHit_t2693066224;
// UniRx.IObservable`1<UnityEngine.ControllerColliderHit>
struct IObservable_1_t2451864588;
// UniRx.IObservable`1<System.Single>
struct IObservable_1_t717007385;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// UniRx.IObservable`1<UnityEngine.GameObject>
struct IObservable_1_t3771493466;
// UnityEngine.RenderTexture
struct RenderTexture_t12905170;
// UniRx.IObservable`1<UniRx.Tuple`2<UnityEngine.RenderTexture,UnityEngine.RenderTexture>>
struct IObservable_1_t1676913119;
// UnityEngine.Collider
struct Collider_t955670625;
// UniRx.IObservable`1<UnityEngine.Collider>
struct IObservable_1_t714468989;
// UnityEngine.Collider2D
struct Collider2D_t1890038195;
// UniRx.IObservable`1<UnityEngine.Collider2D>
struct IObservable_1_t1648836559;
// UniRx.IObservable`1<UnityEngine.NetworkDisconnection>
struct IObservable_1_t97558759;
// UniRx.IObservable`1<UnityEngine.NetworkConnectionError>
struct IObservable_1_t782604357;
// UniRx.IObservable`1<UnityEngine.MasterServerEvent>
struct IObservable_1_t2036605854;
// UniRx.IObservable`1<UnityEngine.NetworkMessageInfo>
struct IObservable_1_t2333143248;
// UniRx.IObservable`1<UnityEngine.NetworkPlayer>
struct IObservable_1_t1039935736;
// UnityEngine.BitStream
struct BitStream_t3159523098;
// UniRx.IObservable`1<UniRx.Tuple`2<UnityEngine.BitStream,UnityEngine.NetworkMessageInfo>>
struct IObservable_1_t3034811225;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collision1119538015.h"
#include "UnityEngine_UnityEngine_Collision2D452810033.h"
#include "UnityEngine_UnityEngine_ControllerColliderHit2693066224.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"
#include "UnityEngine_UnityEngine_RenderTexture12905170.h"
#include "UnityEngine_UnityEngine_Collider955670625.h"
#include "UnityEngine_UnityEngine_Collider2D1890038195.h"
#include "UnityEngine_UnityEngine_NetworkDisconnection338760395.h"
#include "UnityEngine_UnityEngine_NetworkConnectionError1023805993.h"
#include "UnityEngine_UnityEngine_MasterServerEvent2277807490.h"
#include "UnityEngine_UnityEngine_NetworkMessageInfo2574344884.h"
#include "UnityEngine_UnityEngine_NetworkPlayer1281137372.h"
#include "UnityEngine_UnityEngine_BitStream3159523098.h"

// System.Void UniRx.ObservableMonoBehaviour::.ctor()
extern "C"  void ObservableMonoBehaviour__ctor_m1531372632 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::Awake()
extern "C"  void ObservableMonoBehaviour_Awake_m1768977851 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::AwakeAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_AwakeAsObservable_m2790939256 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::FixedUpdate()
extern "C"  void ObservableMonoBehaviour_FixedUpdate_m1622810579 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::FixedUpdateAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_FixedUpdateAsObservable_m330377872 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::LateUpdate()
extern "C"  void ObservableMonoBehaviour_LateUpdate_m3764870395 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::LateUpdateAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_LateUpdateAsObservable_m2981047080 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::OnAnimatorIK(System.Int32)
extern "C"  void ObservableMonoBehaviour_OnAnimatorIK_m2050465221 (ObservableMonoBehaviour_t2173505449 * __this, int32_t ___layerIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.Int32> UniRx.ObservableMonoBehaviour::OnAnimatorIKAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnAnimatorIKAsObservable_m2682374158 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::OnAnimatorMove()
extern "C"  void ObservableMonoBehaviour_OnAnimatorMove_m4133172547 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::OnAnimatorMoveAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnAnimatorMoveAsObservable_m2632743792 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::OnApplicationFocus(System.Boolean)
extern "C"  void ObservableMonoBehaviour_OnApplicationFocus_m1603483274 (ObservableMonoBehaviour_t2173505449 * __this, bool ___focus0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.Boolean> UniRx.ObservableMonoBehaviour::OnApplicationFocusAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnApplicationFocusAsObservable_m3673414419 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::OnApplicationPause(System.Boolean)
extern "C"  void ObservableMonoBehaviour_OnApplicationPause_m3057650600 (ObservableMonoBehaviour_t2173505449 * __this, bool ___pause0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.Boolean> UniRx.ObservableMonoBehaviour::OnApplicationPauseAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnApplicationPauseAsObservable_m577724337 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::OnApplicationQuit()
extern "C"  void ObservableMonoBehaviour_OnApplicationQuit_m1042517462 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::OnApplicationQuitAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnApplicationQuitAsObservable_m1251128851 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::OnAudioFilterRead(System.Single[],System.Int32)
extern "C"  void ObservableMonoBehaviour_OnAudioFilterRead_m428845417 (ObservableMonoBehaviour_t2173505449 * __this, SingleU5BU5D_t1219431280* ___data0, int32_t ___channels1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Tuple`2<System.Single[],System.Int32>> UniRx.ObservableMonoBehaviour::OnAudioFilterReadAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnAudioFilterReadAsObservable_m595639890 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::OnBecameInvisible()
extern "C"  void ObservableMonoBehaviour_OnBecameInvisible_m4223550283 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::OnBecameInvisibleAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnBecameInvisibleAsObservable_m380101640 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::OnBecameVisible()
extern "C"  void ObservableMonoBehaviour_OnBecameVisible_m2724211216 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::OnBecameVisibleAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnBecameVisibleAsObservable_m1725013325 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::OnCollisionEnter(UnityEngine.Collision)
extern "C"  void ObservableMonoBehaviour_OnCollisionEnter_m3972548134 (ObservableMonoBehaviour_t2173505449 * __this, Collision_t1119538015 * ___collision0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.Collision> UniRx.ObservableMonoBehaviour::OnCollisionEnterAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnCollisionEnterAsObservable_m3681595323 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::OnCollisionEnter2D(UnityEngine.Collision2D)
extern "C"  void ObservableMonoBehaviour_OnCollisionEnter2D_m1819542722 (ObservableMonoBehaviour_t2173505449 * __this, Collision2D_t452810033 * ___coll0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.Collision2D> UniRx.ObservableMonoBehaviour::OnCollisionEnter2DAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnCollisionEnter2DAsObservable_m933896315 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::OnCollisionExit(UnityEngine.Collision)
extern "C"  void ObservableMonoBehaviour_OnCollisionExit_m373064656 (ObservableMonoBehaviour_t2173505449 * __this, Collision_t1119538015 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.Collision> UniRx.ObservableMonoBehaviour::OnCollisionExitAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnCollisionExitAsObservable_m496272903 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::OnCollisionExit2D(UnityEngine.Collision2D)
extern "C"  void ObservableMonoBehaviour_OnCollisionExit2D_m551378284 (ObservableMonoBehaviour_t2173505449 * __this, Collision2D_t452810033 * ___coll0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.Collision2D> UniRx.ObservableMonoBehaviour::OnCollisionExit2DAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnCollisionExit2DAsObservable_m704116651 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::OnCollisionStay(UnityEngine.Collision)
extern "C"  void ObservableMonoBehaviour_OnCollisionStay_m4224418869 (ObservableMonoBehaviour_t2173505449 * __this, Collision_t1119538015 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.Collision> UniRx.ObservableMonoBehaviour::OnCollisionStayAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnCollisionStayAsObservable_m1335988226 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::OnCollisionStay2D(UnityEngine.Collision2D)
extern "C"  void ObservableMonoBehaviour_OnCollisionStay2D_m2893783889 (ObservableMonoBehaviour_t2173505449 * __this, Collision2D_t452810033 * ___coll0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.Collision2D> UniRx.ObservableMonoBehaviour::OnCollisionStay2DAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnCollisionStay2DAsObservable_m216690406 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::OnConnectedToServer()
extern "C"  void ObservableMonoBehaviour_OnConnectedToServer_m2381232734 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::OnConnectedToServerAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnConnectedToServerAsObservable_m3492218011 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::OnControllerColliderHit(UnityEngine.ControllerColliderHit)
extern "C"  void ObservableMonoBehaviour_OnControllerColliderHit_m4134442156 (ObservableMonoBehaviour_t2173505449 * __this, ControllerColliderHit_t2693066224 * ___hit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.ControllerColliderHit> UniRx.ObservableMonoBehaviour::OnControllerColliderHitAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnControllerColliderHitAsObservable_m3711125355 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::OnDestroy()
extern "C"  void ObservableMonoBehaviour_OnDestroy_m1776209233 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::OnDestroyAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnDestroyAsObservable_m966384910 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::OnDisable()
extern "C"  void ObservableMonoBehaviour_OnDisable_m3893644479 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::OnDisableAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnDisableAsObservable_m2362812284 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::OnDrawGizmos()
extern "C"  void ObservableMonoBehaviour_OnDrawGizmos_m3009550792 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::OnDrawGizmosAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnDrawGizmosAsObservable_m2079600245 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::OnDrawGizmosSelected()
extern "C"  void ObservableMonoBehaviour_OnDrawGizmosSelected_m3748001475 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::OnDrawGizmosSelectedAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnDrawGizmosSelectedAsObservable_m197702384 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::OnEnable()
extern "C"  void ObservableMonoBehaviour_OnEnable_m2051040302 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::OnEnableAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnEnableAsObservable_m1977314267 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::OnJointBreak(System.Single)
extern "C"  void ObservableMonoBehaviour_OnJointBreak_m3735926251 (ObservableMonoBehaviour_t2173505449 * __this, float ___breakForce0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.Single> UniRx.ObservableMonoBehaviour::OnJointBreakAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnJointBreakAsObservable_m1818163774 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::OnLevelWasLoaded(System.Int32)
extern "C"  void ObservableMonoBehaviour_OnLevelWasLoaded_m1194111462 (ObservableMonoBehaviour_t2173505449 * __this, int32_t ___level0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.Int32> UniRx.ObservableMonoBehaviour::OnLevelWasLoadedAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnLevelWasLoadedAsObservable_m1159610799 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::OnParticleCollision(UnityEngine.GameObject)
extern "C"  void ObservableMonoBehaviour_OnParticleCollision_m3194519515 (ObservableMonoBehaviour_t2173505449 * __this, GameObject_t4012695102 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.GameObject> UniRx.ObservableMonoBehaviour::OnParticleCollisionAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnParticleCollisionAsObservable_m1577085004 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::OnPostRender()
extern "C"  void ObservableMonoBehaviour_OnPostRender_m1333452609 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::OnPostRenderAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnPostRenderAsObservable_m1140557422 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::OnPreCull()
extern "C"  void ObservableMonoBehaviour_OnPreCull_m1802980684 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::OnPreCullAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnPreCullAsObservable_m1910717577 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::OnPreRender()
extern "C"  void ObservableMonoBehaviour_OnPreRender_m901570096 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::OnPreRenderAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnPreRenderAsObservable_m1512064365 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void ObservableMonoBehaviour_OnRenderImage_m1046315270 (ObservableMonoBehaviour_t2173505449 * __this, RenderTexture_t12905170 * ___src0, RenderTexture_t12905170 * ___dest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Tuple`2<UnityEngine.RenderTexture,UnityEngine.RenderTexture>> UniRx.ObservableMonoBehaviour::OnRenderImageAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnRenderImageAsObservable_m3614982245 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::OnRenderObject()
extern "C"  void ObservableMonoBehaviour_OnRenderObject_m216210208 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::OnRenderObjectAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnRenderObjectAsObservable_m1021410765 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::OnServerInitialized()
extern "C"  void ObservableMonoBehaviour_OnServerInitialized_m2228721576 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::OnServerInitializedAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnServerInitializedAsObservable_m3112977125 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void ObservableMonoBehaviour_OnTriggerEnter_m2622129696 (ObservableMonoBehaviour_t2173505449 * __this, Collider_t955670625 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.Collider> UniRx.ObservableMonoBehaviour::OnTriggerEnterAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnTriggerEnterAsObservable_m3105090857 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void ObservableMonoBehaviour_OnTriggerEnter2D_m1897276960 (ObservableMonoBehaviour_t2173505449 * __this, Collider2D_t1890038195 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.Collider2D> UniRx.ObservableMonoBehaviour::OnTriggerEnter2DAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnTriggerEnter2DAsObservable_m4281614569 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::OnTriggerExit(UnityEngine.Collider)
extern "C"  void ObservableMonoBehaviour_OnTriggerExit_m427244418 (ObservableMonoBehaviour_t2173505449 * __this, Collider_t955670625 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.Collider> UniRx.ObservableMonoBehaviour::OnTriggerExitAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnTriggerExitAsObservable_m754770649 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::OnTriggerExit2D(UnityEngine.Collider2D)
extern "C"  void ObservableMonoBehaviour_OnTriggerExit2D_m1950702210 (ObservableMonoBehaviour_t2173505449 * __this, Collider2D_t1890038195 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.Collider2D> UniRx.ObservableMonoBehaviour::OnTriggerExit2DAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnTriggerExit2DAsObservable_m3583054205 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::OnTriggerStay(UnityEngine.Collider)
extern "C"  void ObservableMonoBehaviour_OnTriggerStay_m3045333629 (ObservableMonoBehaviour_t2173505449 * __this, Collider_t955670625 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.Collider> UniRx.ObservableMonoBehaviour::OnTriggerStayAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnTriggerStayAsObservable_m1594485972 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::OnTriggerStay2D(UnityEngine.Collider2D)
extern "C"  void ObservableMonoBehaviour_OnTriggerStay2D_m1887716349 (ObservableMonoBehaviour_t2173505449 * __this, Collider2D_t1890038195 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.Collider2D> UniRx.ObservableMonoBehaviour::OnTriggerStay2DAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnTriggerStay2DAsObservable_m3095627960 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::OnValidate()
extern "C"  void ObservableMonoBehaviour_OnValidate_m2019282049 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::OnValidateAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnValidateAsObservable_m2752151982 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::OnWillRenderObject()
extern "C"  void ObservableMonoBehaviour_OnWillRenderObject_m526842514 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::OnWillRenderObjectAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnWillRenderObjectAsObservable_m2565432895 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::Reset()
extern "C"  void ObservableMonoBehaviour_Reset_m3472772869 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::ResetAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_ResetAsObservable_m583701698 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::Start()
extern "C"  void ObservableMonoBehaviour_Start_m478510424 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::StartAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_StartAsObservable_m1031265941 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::Update()
extern "C"  void ObservableMonoBehaviour_Update_m1954773429 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.ObservableMonoBehaviour::UpdateAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_UpdateAsObservable_m550395106 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::OnDisconnectedFromServer(UnityEngine.NetworkDisconnection)
extern "C"  void ObservableMonoBehaviour_OnDisconnectedFromServer_m1195767326 (ObservableMonoBehaviour_t2173505449 * __this, int32_t ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.NetworkDisconnection> UniRx.ObservableMonoBehaviour::OnDisconnectedFromServerAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnDisconnectedFromServerAsObservable_m2701963623 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::OnFailedToConnect(UnityEngine.NetworkConnectionError)
extern "C"  void ObservableMonoBehaviour_OnFailedToConnect_m3518302774 (ObservableMonoBehaviour_t2173505449 * __this, int32_t ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.NetworkConnectionError> UniRx.ObservableMonoBehaviour::OnFailedToConnectAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnFailedToConnectAsObservable_m1266481053 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::OnFailedToConnectToMasterServer(UnityEngine.NetworkConnectionError)
extern "C"  void ObservableMonoBehaviour_OnFailedToConnectToMasterServer_m160169654 (ObservableMonoBehaviour_t2173505449 * __this, int32_t ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.NetworkConnectionError> UniRx.ObservableMonoBehaviour::OnFailedToConnectToMasterServerAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnFailedToConnectToMasterServerAsObservable_m2464000477 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::OnMasterServerEvent(UnityEngine.MasterServerEvent)
extern "C"  void ObservableMonoBehaviour_OnMasterServerEvent_m2416233864 (ObservableMonoBehaviour_t2173505449 * __this, int32_t ___msEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.MasterServerEvent> UniRx.ObservableMonoBehaviour::OnMasterServerEventAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnMasterServerEventAsObservable_m3495966991 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::OnNetworkInstantiate(UnityEngine.NetworkMessageInfo)
extern "C"  void ObservableMonoBehaviour_OnNetworkInstantiate_m140847305 (ObservableMonoBehaviour_t2173505449 * __this, NetworkMessageInfo_t2574344884  ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.NetworkMessageInfo> UniRx.ObservableMonoBehaviour::OnNetworkInstantiateAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnNetworkInstantiateAsObservable_m3722898642 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::OnPlayerConnected(UnityEngine.NetworkPlayer)
extern "C"  void ObservableMonoBehaviour_OnPlayerConnected_m748806203 (ObservableMonoBehaviour_t2173505449 * __this, NetworkPlayer_t1281137372  ___player0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.NetworkPlayer> UniRx.ObservableMonoBehaviour::OnPlayerConnectedAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnPlayerConnectedAsObservable_m4277027708 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::OnPlayerDisconnected(UnityEngine.NetworkPlayer)
extern "C"  void ObservableMonoBehaviour_OnPlayerDisconnected_m3687566611 (ObservableMonoBehaviour_t2173505449 * __this, NetworkPlayer_t1281137372  ___player0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.NetworkPlayer> UniRx.ObservableMonoBehaviour::OnPlayerDisconnectedAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnPlayerDisconnectedAsObservable_m3565600660 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableMonoBehaviour::OnSerializeNetworkView(UnityEngine.BitStream,UnityEngine.NetworkMessageInfo)
extern "C"  void ObservableMonoBehaviour_OnSerializeNetworkView_m749135242 (ObservableMonoBehaviour_t2173505449 * __this, BitStream_t3159523098 * ___stream0, NetworkMessageInfo_t2574344884  ___info1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Tuple`2<UnityEngine.BitStream,UnityEngine.NetworkMessageInfo>> UniRx.ObservableMonoBehaviour::OnSerializeNetworkViewAsObservable()
extern "C"  Il2CppObject* ObservableMonoBehaviour_OnSerializeNetworkViewAsObservable_m3383906739 (ObservableMonoBehaviour_t2173505449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
