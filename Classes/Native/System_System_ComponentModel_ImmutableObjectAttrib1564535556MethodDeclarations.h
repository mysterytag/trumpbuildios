﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ComponentModel.ImmutableObjectAttribute
struct ImmutableObjectAttribute_t1564535556;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.ComponentModel.ImmutableObjectAttribute::.ctor(System.Boolean)
extern "C"  void ImmutableObjectAttribute__ctor_m685171317 (ImmutableObjectAttribute_t1564535556 * __this, bool ___immutable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.ImmutableObjectAttribute::.cctor()
extern "C"  void ImmutableObjectAttribute__cctor_m2656590319 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.ImmutableObjectAttribute::get_Immutable()
extern "C"  bool ImmutableObjectAttribute_get_Immutable_m2327045599 (ImmutableObjectAttribute_t1564535556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.ImmutableObjectAttribute::Equals(System.Object)
extern "C"  bool ImmutableObjectAttribute_Equals_m3038256153 (ImmutableObjectAttribute_t1564535556 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.ComponentModel.ImmutableObjectAttribute::GetHashCode()
extern "C"  int32_t ImmutableObjectAttribute_GetHashCode_m2498332029 (ImmutableObjectAttribute_t1564535556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.ImmutableObjectAttribute::IsDefaultAttribute()
extern "C"  bool ImmutableObjectAttribute_IsDefaultAttribute_m3851274817 (ImmutableObjectAttribute_t1564535556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
