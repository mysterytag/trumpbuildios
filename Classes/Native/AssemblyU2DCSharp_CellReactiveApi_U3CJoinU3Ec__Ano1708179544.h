﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CellUtils.SingleAssignmentDisposable
struct SingleAssignmentDisposable_t1832432170;
// CellUtils.CellJoinDisposable`1<System.Object>
struct CellJoinDisposable_1_t3736493403;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// CellReactiveApi/<Join>c__AnonStorey114`1<System.Object>
struct U3CJoinU3Ec__AnonStorey114_1_t2153204543;

#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CellReactiveApi/<Join>c__AnonStorey114`1/<Join>c__AnonStorey115`1<System.Object>
struct  U3CJoinU3Ec__AnonStorey115_1_t1708179544  : public Il2CppObject
{
public:
	// CellUtils.SingleAssignmentDisposable CellReactiveApi/<Join>c__AnonStorey114`1/<Join>c__AnonStorey115`1::inner
	SingleAssignmentDisposable_t1832432170 * ___inner_0;
	// CellUtils.CellJoinDisposable`1<T> CellReactiveApi/<Join>c__AnonStorey114`1/<Join>c__AnonStorey115`1::group
	CellJoinDisposable_1_t3736493403 * ___group_1;
	// System.Action`1<T> CellReactiveApi/<Join>c__AnonStorey114`1/<Join>c__AnonStorey115`1::reaction
	Action_1_t985559125 * ___reaction_2;
	// Priority CellReactiveApi/<Join>c__AnonStorey114`1/<Join>c__AnonStorey115`1::p
	int32_t ___p_3;
	// CellReactiveApi/<Join>c__AnonStorey114`1<T> CellReactiveApi/<Join>c__AnonStorey114`1/<Join>c__AnonStorey115`1::<>f__ref$276
	U3CJoinU3Ec__AnonStorey114_1_t2153204543 * ___U3CU3Ef__refU24276_4;

public:
	inline static int32_t get_offset_of_inner_0() { return static_cast<int32_t>(offsetof(U3CJoinU3Ec__AnonStorey115_1_t1708179544, ___inner_0)); }
	inline SingleAssignmentDisposable_t1832432170 * get_inner_0() const { return ___inner_0; }
	inline SingleAssignmentDisposable_t1832432170 ** get_address_of_inner_0() { return &___inner_0; }
	inline void set_inner_0(SingleAssignmentDisposable_t1832432170 * value)
	{
		___inner_0 = value;
		Il2CppCodeGenWriteBarrier(&___inner_0, value);
	}

	inline static int32_t get_offset_of_group_1() { return static_cast<int32_t>(offsetof(U3CJoinU3Ec__AnonStorey115_1_t1708179544, ___group_1)); }
	inline CellJoinDisposable_1_t3736493403 * get_group_1() const { return ___group_1; }
	inline CellJoinDisposable_1_t3736493403 ** get_address_of_group_1() { return &___group_1; }
	inline void set_group_1(CellJoinDisposable_1_t3736493403 * value)
	{
		___group_1 = value;
		Il2CppCodeGenWriteBarrier(&___group_1, value);
	}

	inline static int32_t get_offset_of_reaction_2() { return static_cast<int32_t>(offsetof(U3CJoinU3Ec__AnonStorey115_1_t1708179544, ___reaction_2)); }
	inline Action_1_t985559125 * get_reaction_2() const { return ___reaction_2; }
	inline Action_1_t985559125 ** get_address_of_reaction_2() { return &___reaction_2; }
	inline void set_reaction_2(Action_1_t985559125 * value)
	{
		___reaction_2 = value;
		Il2CppCodeGenWriteBarrier(&___reaction_2, value);
	}

	inline static int32_t get_offset_of_p_3() { return static_cast<int32_t>(offsetof(U3CJoinU3Ec__AnonStorey115_1_t1708179544, ___p_3)); }
	inline int32_t get_p_3() const { return ___p_3; }
	inline int32_t* get_address_of_p_3() { return &___p_3; }
	inline void set_p_3(int32_t value)
	{
		___p_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24276_4() { return static_cast<int32_t>(offsetof(U3CJoinU3Ec__AnonStorey115_1_t1708179544, ___U3CU3Ef__refU24276_4)); }
	inline U3CJoinU3Ec__AnonStorey114_1_t2153204543 * get_U3CU3Ef__refU24276_4() const { return ___U3CU3Ef__refU24276_4; }
	inline U3CJoinU3Ec__AnonStorey114_1_t2153204543 ** get_address_of_U3CU3Ef__refU24276_4() { return &___U3CU3Ef__refU24276_4; }
	inline void set_U3CU3Ef__refU24276_4(U3CJoinU3Ec__AnonStorey114_1_t2153204543 * value)
	{
		___U3CU3Ef__refU24276_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24276_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
