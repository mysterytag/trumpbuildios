﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SponsorPay.SPLocation
struct  SPLocation_t126034156  : public Il2CppObject
{
public:
	// System.Double SponsorPay.SPLocation::<Long>k__BackingField
	double ___U3CLongU3Ek__BackingField_0;
	// System.Double SponsorPay.SPLocation::<Lat>k__BackingField
	double ___U3CLatU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CLongU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SPLocation_t126034156, ___U3CLongU3Ek__BackingField_0)); }
	inline double get_U3CLongU3Ek__BackingField_0() const { return ___U3CLongU3Ek__BackingField_0; }
	inline double* get_address_of_U3CLongU3Ek__BackingField_0() { return &___U3CLongU3Ek__BackingField_0; }
	inline void set_U3CLongU3Ek__BackingField_0(double value)
	{
		___U3CLongU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CLatU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SPLocation_t126034156, ___U3CLatU3Ek__BackingField_1)); }
	inline double get_U3CLatU3Ek__BackingField_1() const { return ___U3CLatU3Ek__BackingField_1; }
	inline double* get_address_of_U3CLatU3Ek__BackingField_1() { return &___U3CLatU3Ek__BackingField_1; }
	inline void set_U3CLatU3Ek__BackingField_1(double value)
	{
		___U3CLatU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
