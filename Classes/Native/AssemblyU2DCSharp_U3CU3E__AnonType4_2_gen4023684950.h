﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <>__AnonType4`2<System.Single,System.String>
struct  U3CU3E__AnonType4_2_t4023684950  : public Il2CppObject
{
public:
	// <f>__T <>__AnonType4`2::<f>
	float ___U3CfU3E_0;
	// <valuta>__T <>__AnonType4`2::<valuta>
	String_t* ___U3CvalutaU3E_1;

public:
	inline static int32_t get_offset_of_U3CfU3E_0() { return static_cast<int32_t>(offsetof(U3CU3E__AnonType4_2_t4023684950, ___U3CfU3E_0)); }
	inline float get_U3CfU3E_0() const { return ___U3CfU3E_0; }
	inline float* get_address_of_U3CfU3E_0() { return &___U3CfU3E_0; }
	inline void set_U3CfU3E_0(float value)
	{
		___U3CfU3E_0 = value;
	}

	inline static int32_t get_offset_of_U3CvalutaU3E_1() { return static_cast<int32_t>(offsetof(U3CU3E__AnonType4_2_t4023684950, ___U3CvalutaU3E_1)); }
	inline String_t* get_U3CvalutaU3E_1() const { return ___U3CvalutaU3E_1; }
	inline String_t** get_address_of_U3CvalutaU3E_1() { return &___U3CvalutaU3E_1; }
	inline void set_U3CvalutaU3E_1(String_t* value)
	{
		___U3CvalutaU3E_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CvalutaU3E_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
