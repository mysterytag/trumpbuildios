﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23312956448MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,AchievementDescription>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3834940742(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3449563711 *, String_t*, AchievementDescription_t2323334509 *, const MethodInfo*))KeyValuePair_2__ctor_m4168265535_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,AchievementDescription>::get_Key()
#define KeyValuePair_2_get_Key_m1611306978(__this, method) ((  String_t* (*) (KeyValuePair_2_t3449563711 *, const MethodInfo*))KeyValuePair_2_get_Key_m3256475977_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,AchievementDescription>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1790777635(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3449563711 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m1278074762_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,AchievementDescription>::get_Value()
#define KeyValuePair_2_get_Value_m738758690(__this, method) ((  AchievementDescription_t2323334509 * (*) (KeyValuePair_2_t3449563711 *, const MethodInfo*))KeyValuePair_2_get_Value_m3899079597_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,AchievementDescription>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m449913763(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3449563711 *, AchievementDescription_t2323334509 *, const MethodInfo*))KeyValuePair_2_set_Value_m2954518154_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,AchievementDescription>::ToString()
#define KeyValuePair_2_ToString_m1411428831(__this, method) ((  String_t* (*) (KeyValuePair_2_t3449563711 *, const MethodInfo*))KeyValuePair_2_ToString_m1313859518_gshared)(__this, method)
