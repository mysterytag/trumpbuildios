﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Substance`1<System.Object>
struct Substance_1_t3068566258;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Substance`1<System.Object>::.ctor()
extern "C"  void Substance_1__ctor_m2762360544_gshared (Substance_1_t3068566258 * __this, const MethodInfo* method);
#define Substance_1__ctor_m2762360544(__this, method) ((  void (*) (Substance_1_t3068566258 *, const MethodInfo*))Substance_1__ctor_m2762360544_gshared)(__this, method)
// T Substance`1<System.Object>::Result()
extern "C"  Il2CppObject * Substance_1_Result_m448858752_gshared (Substance_1_t3068566258 * __this, const MethodInfo* method);
#define Substance_1_Result_m448858752(__this, method) ((  Il2CppObject * (*) (Substance_1_t3068566258 *, const MethodInfo*))Substance_1_Result_m448858752_gshared)(__this, method)
