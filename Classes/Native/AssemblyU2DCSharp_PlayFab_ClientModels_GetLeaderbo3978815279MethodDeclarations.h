﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetLeaderboardForUsersCharactersRequest
struct GetLeaderboardForUsersCharactersRequest_t3978815279;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.GetLeaderboardForUsersCharactersRequest::.ctor()
extern "C"  void GetLeaderboardForUsersCharactersRequest__ctor_m1929319258 (GetLeaderboardForUsersCharactersRequest_t3978815279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GetLeaderboardForUsersCharactersRequest::get_StatisticName()
extern "C"  String_t* GetLeaderboardForUsersCharactersRequest_get_StatisticName_m2909510791 (GetLeaderboardForUsersCharactersRequest_t3978815279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetLeaderboardForUsersCharactersRequest::set_StatisticName(System.String)
extern "C"  void GetLeaderboardForUsersCharactersRequest_set_StatisticName_m3230454124 (GetLeaderboardForUsersCharactersRequest_t3978815279 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlayFab.ClientModels.GetLeaderboardForUsersCharactersRequest::get_MaxResultsCount()
extern "C"  int32_t GetLeaderboardForUsersCharactersRequest_get_MaxResultsCount_m1455258046 (GetLeaderboardForUsersCharactersRequest_t3978815279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetLeaderboardForUsersCharactersRequest::set_MaxResultsCount(System.Int32)
extern "C"  void GetLeaderboardForUsersCharactersRequest_set_MaxResultsCount_m2549597673 (GetLeaderboardForUsersCharactersRequest_t3978815279 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
