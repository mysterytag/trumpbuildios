﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.RegisterForIOSPushNotificationRequest
struct RegisterForIOSPushNotificationRequest_t2944199315;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen3097043249.h"

// System.Void PlayFab.ClientModels.RegisterForIOSPushNotificationRequest::.ctor()
extern "C"  void RegisterForIOSPushNotificationRequest__ctor_m3037949430 (RegisterForIOSPushNotificationRequest_t2944199315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.RegisterForIOSPushNotificationRequest::get_DeviceToken()
extern "C"  String_t* RegisterForIOSPushNotificationRequest_get_DeviceToken_m3877369195 (RegisterForIOSPushNotificationRequest_t2944199315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.RegisterForIOSPushNotificationRequest::set_DeviceToken(System.String)
extern "C"  void RegisterForIOSPushNotificationRequest_set_DeviceToken_m2351246984 (RegisterForIOSPushNotificationRequest_t2944199315 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.RegisterForIOSPushNotificationRequest::get_SendPushNotificationConfirmation()
extern "C"  Nullable_1_t3097043249  RegisterForIOSPushNotificationRequest_get_SendPushNotificationConfirmation_m701229890 (RegisterForIOSPushNotificationRequest_t2944199315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.RegisterForIOSPushNotificationRequest::set_SendPushNotificationConfirmation(System.Nullable`1<System.Boolean>)
extern "C"  void RegisterForIOSPushNotificationRequest_set_SendPushNotificationConfirmation_m4029145883 (RegisterForIOSPushNotificationRequest_t2944199315 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.RegisterForIOSPushNotificationRequest::get_ConfirmationMessage()
extern "C"  String_t* RegisterForIOSPushNotificationRequest_get_ConfirmationMessage_m1632947066 (RegisterForIOSPushNotificationRequest_t2944199315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.RegisterForIOSPushNotificationRequest::set_ConfirmationMessage(System.String)
extern "C"  void RegisterForIOSPushNotificationRequest_set_ConfirmationMessage_m2071152601 (RegisterForIOSPushNotificationRequest_t2944199315 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
