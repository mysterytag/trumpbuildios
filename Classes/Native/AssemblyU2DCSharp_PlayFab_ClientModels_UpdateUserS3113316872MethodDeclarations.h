﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.UpdateUserStatisticsRequest
struct UpdateUserStatisticsRequest_t3113316872;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t190145395;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.UpdateUserStatisticsRequest::.ctor()
extern "C"  void UpdateUserStatisticsRequest__ctor_m2461343137 (UpdateUserStatisticsRequest_t3113316872 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32> PlayFab.ClientModels.UpdateUserStatisticsRequest::get_UserStatistics()
extern "C"  Dictionary_2_t190145395 * UpdateUserStatisticsRequest_get_UserStatistics_m277909471 (UpdateUserStatisticsRequest_t3113316872 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UpdateUserStatisticsRequest::set_UserStatistics(System.Collections.Generic.Dictionary`2<System.String,System.Int32>)
extern "C"  void UpdateUserStatisticsRequest_set_UserStatistics_m359084950 (UpdateUserStatisticsRequest_t3113316872 * __this, Dictionary_2_t190145395 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
