﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LocalizedTextSetter
struct LocalizedTextSetter_t1100117447;

#include "codegen/il2cpp-codegen.h"

// System.Void LocalizedTextSetter::.ctor()
extern "C"  void LocalizedTextSetter__ctor_m2056662404 (LocalizedTextSetter_t1100117447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalizedTextSetter::Awake()
extern "C"  void LocalizedTextSetter_Awake_m2294267623 (LocalizedTextSetter_t1100117447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
