﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3591453091MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,PlayFab.ReflectionUtils/GetDelegate>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2981843500(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1674849584 *, Dictionary_2_t1907821643 *, const MethodInfo*))Enumerator__ctor_m3920831137_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,PlayFab.ReflectionUtils/GetDelegate>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1650928565(__this, method) ((  Il2CppObject * (*) (Enumerator_t1674849584 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3262087712_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,PlayFab.ReflectionUtils/GetDelegate>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m4271225609(__this, method) ((  void (*) (Enumerator_t1674849584 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2959141748_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,PlayFab.ReflectionUtils/GetDelegate>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2479518994(__this, method) ((  DictionaryEntry_t130027246  (*) (Enumerator_t1674849584 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2279524093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,PlayFab.ReflectionUtils/GetDelegate>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2574388433(__this, method) ((  Il2CppObject * (*) (Enumerator_t1674849584 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1201448700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,PlayFab.ReflectionUtils/GetDelegate>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1134557987(__this, method) ((  Il2CppObject * (*) (Enumerator_t1674849584 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m294434446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,PlayFab.ReflectionUtils/GetDelegate>::MoveNext()
#define Enumerator_MoveNext_m924660981(__this, method) ((  bool (*) (Enumerator_t1674849584 *, const MethodInfo*))Enumerator_MoveNext_m217327200_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,PlayFab.ReflectionUtils/GetDelegate>::get_Current()
#define Enumerator_get_Current_m3464203675(__this, method) ((  KeyValuePair_2_t1396352941  (*) (Enumerator_t1674849584 *, const MethodInfo*))Enumerator_get_Current_m4240003024_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,PlayFab.ReflectionUtils/GetDelegate>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m4162926466(__this, method) ((  String_t* (*) (Enumerator_t1674849584 *, const MethodInfo*))Enumerator_get_CurrentKey_m3062159917_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,PlayFab.ReflectionUtils/GetDelegate>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1588840998(__this, method) ((  GetDelegate_t270123739 * (*) (Enumerator_t1674849584 *, const MethodInfo*))Enumerator_get_CurrentValue_m592783249_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,PlayFab.ReflectionUtils/GetDelegate>::Reset()
#define Enumerator_Reset_m2399234686(__this, method) ((  void (*) (Enumerator_t1674849584 *, const MethodInfo*))Enumerator_Reset_m3001375603_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,PlayFab.ReflectionUtils/GetDelegate>::VerifyState()
#define Enumerator_VerifyState_m439247879(__this, method) ((  void (*) (Enumerator_t1674849584 *, const MethodInfo*))Enumerator_VerifyState_m4290054460_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,PlayFab.ReflectionUtils/GetDelegate>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m3955288495(__this, method) ((  void (*) (Enumerator_t1674849584 *, const MethodInfo*))Enumerator_VerifyCurrent_m2318603684_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,PlayFab.ReflectionUtils/GetDelegate>::Dispose()
#define Enumerator_Dispose_m1790524366(__this, method) ((  void (*) (Enumerator_t1674849584 *, const MethodInfo*))Enumerator_Dispose_m627360643_gshared)(__this, method)
