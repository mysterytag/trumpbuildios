﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GUIStyle
struct GUIStyle_t1006925219;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScreenLog
struct  ScreenLog_t2129044248  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.GUIStyle ScreenLog::logStyle
	GUIStyle_t1006925219 * ___logStyle_2;

public:
	inline static int32_t get_offset_of_logStyle_2() { return static_cast<int32_t>(offsetof(ScreenLog_t2129044248, ___logStyle_2)); }
	inline GUIStyle_t1006925219 * get_logStyle_2() const { return ___logStyle_2; }
	inline GUIStyle_t1006925219 ** get_address_of_logStyle_2() { return &___logStyle_2; }
	inline void set_logStyle_2(GUIStyle_t1006925219 * value)
	{
		___logStyle_2 = value;
		Il2CppCodeGenWriteBarrier(&___logStyle_2, value);
	}
};

struct ScreenLog_t2129044248_StaticFields
{
public:
	// System.String ScreenLog::text
	String_t* ___text_3;

public:
	inline static int32_t get_offset_of_text_3() { return static_cast<int32_t>(offsetof(ScreenLog_t2129044248_StaticFields, ___text_3)); }
	inline String_t* get_text_3() const { return ___text_3; }
	inline String_t** get_address_of_text_3() { return &___text_3; }
	inline void set_text_3(String_t* value)
	{
		___text_3 = value;
		Il2CppCodeGenWriteBarrier(&___text_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
