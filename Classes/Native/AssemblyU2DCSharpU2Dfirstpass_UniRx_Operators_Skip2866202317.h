﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.SkipUntilObservable`2<System.Object,System.Object>
struct SkipUntilObservable_2_t2740161498;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver<System.Object,System.Object>
struct  SkipUntilOuterObserver_t2866202317  : public OperatorObserverBase_2_t1187768149
{
public:
	// UniRx.Operators.SkipUntilObservable`2<T,TOther> UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver::parent
	SkipUntilObservable_2_t2740161498 * ___parent_2;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(SkipUntilOuterObserver_t2866202317, ___parent_2)); }
	inline SkipUntilObservable_2_t2740161498 * get_parent_2() const { return ___parent_2; }
	inline SkipUntilObservable_2_t2740161498 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(SkipUntilObservable_2_t2740161498 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
