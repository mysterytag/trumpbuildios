﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.CreateSharedGroupResult
struct CreateSharedGroupResult_t2976015403;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.CreateSharedGroupResult::.ctor()
extern "C"  void CreateSharedGroupResult__ctor_m2665504222 (CreateSharedGroupResult_t2976015403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.CreateSharedGroupResult::get_SharedGroupId()
extern "C"  String_t* CreateSharedGroupResult_get_SharedGroupId_m2963015781 (CreateSharedGroupResult_t2976015403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CreateSharedGroupResult::set_SharedGroupId(System.String)
extern "C"  void CreateSharedGroupResult_set_SharedGroupId_m273086542 (CreateSharedGroupResult_t2976015403 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
