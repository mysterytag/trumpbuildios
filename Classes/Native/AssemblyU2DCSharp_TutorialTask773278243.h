﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// BottomButton
struct BottomButton_t1932555613;
// System.Action
struct Action_t437523947;

#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharp_TutorialTask_Type2622298.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorialTask
struct  TutorialTask_t773278243  : public Il2CppObject
{
public:
	// TutorialTask/Type TutorialTask::TaskType
	int32_t ___TaskType_0;
	// System.Int32 TutorialTask::PressCount
	int32_t ___PressCount_1;
	// System.String TutorialTask::Text
	String_t* ___Text_2;
	// BottomButton TutorialTask::MenuButton
	BottomButton_t1932555613 * ___MenuButton_3;
	// System.Action TutorialTask::MenuOpen
	Action_t437523947 * ___MenuOpen_4;
	// System.Action TutorialTask::BuyItem
	Action_t437523947 * ___BuyItem_5;
	// System.String TutorialTask::ButtonSpriteName
	String_t* ___ButtonSpriteName_6;

public:
	inline static int32_t get_offset_of_TaskType_0() { return static_cast<int32_t>(offsetof(TutorialTask_t773278243, ___TaskType_0)); }
	inline int32_t get_TaskType_0() const { return ___TaskType_0; }
	inline int32_t* get_address_of_TaskType_0() { return &___TaskType_0; }
	inline void set_TaskType_0(int32_t value)
	{
		___TaskType_0 = value;
	}

	inline static int32_t get_offset_of_PressCount_1() { return static_cast<int32_t>(offsetof(TutorialTask_t773278243, ___PressCount_1)); }
	inline int32_t get_PressCount_1() const { return ___PressCount_1; }
	inline int32_t* get_address_of_PressCount_1() { return &___PressCount_1; }
	inline void set_PressCount_1(int32_t value)
	{
		___PressCount_1 = value;
	}

	inline static int32_t get_offset_of_Text_2() { return static_cast<int32_t>(offsetof(TutorialTask_t773278243, ___Text_2)); }
	inline String_t* get_Text_2() const { return ___Text_2; }
	inline String_t** get_address_of_Text_2() { return &___Text_2; }
	inline void set_Text_2(String_t* value)
	{
		___Text_2 = value;
		Il2CppCodeGenWriteBarrier(&___Text_2, value);
	}

	inline static int32_t get_offset_of_MenuButton_3() { return static_cast<int32_t>(offsetof(TutorialTask_t773278243, ___MenuButton_3)); }
	inline BottomButton_t1932555613 * get_MenuButton_3() const { return ___MenuButton_3; }
	inline BottomButton_t1932555613 ** get_address_of_MenuButton_3() { return &___MenuButton_3; }
	inline void set_MenuButton_3(BottomButton_t1932555613 * value)
	{
		___MenuButton_3 = value;
		Il2CppCodeGenWriteBarrier(&___MenuButton_3, value);
	}

	inline static int32_t get_offset_of_MenuOpen_4() { return static_cast<int32_t>(offsetof(TutorialTask_t773278243, ___MenuOpen_4)); }
	inline Action_t437523947 * get_MenuOpen_4() const { return ___MenuOpen_4; }
	inline Action_t437523947 ** get_address_of_MenuOpen_4() { return &___MenuOpen_4; }
	inline void set_MenuOpen_4(Action_t437523947 * value)
	{
		___MenuOpen_4 = value;
		Il2CppCodeGenWriteBarrier(&___MenuOpen_4, value);
	}

	inline static int32_t get_offset_of_BuyItem_5() { return static_cast<int32_t>(offsetof(TutorialTask_t773278243, ___BuyItem_5)); }
	inline Action_t437523947 * get_BuyItem_5() const { return ___BuyItem_5; }
	inline Action_t437523947 ** get_address_of_BuyItem_5() { return &___BuyItem_5; }
	inline void set_BuyItem_5(Action_t437523947 * value)
	{
		___BuyItem_5 = value;
		Il2CppCodeGenWriteBarrier(&___BuyItem_5, value);
	}

	inline static int32_t get_offset_of_ButtonSpriteName_6() { return static_cast<int32_t>(offsetof(TutorialTask_t773278243, ___ButtonSpriteName_6)); }
	inline String_t* get_ButtonSpriteName_6() const { return ___ButtonSpriteName_6; }
	inline String_t** get_address_of_ButtonSpriteName_6() { return &___ButtonSpriteName_6; }
	inline void set_ButtonSpriteName_6(String_t* value)
	{
		___ButtonSpriteName_6 = value;
		Il2CppCodeGenWriteBarrier(&___ButtonSpriteName_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
