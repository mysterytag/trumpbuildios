﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.DisposedObserver`1<UnityEngine.NetworkDisconnection>
struct DisposedObserver_1_t3808185153;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_NetworkDisconnection338760395.h"

// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.NetworkDisconnection>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m4055986311_gshared (DisposedObserver_1_t3808185153 * __this, const MethodInfo* method);
#define DisposedObserver_1__ctor_m4055986311(__this, method) ((  void (*) (DisposedObserver_1_t3808185153 *, const MethodInfo*))DisposedObserver_1__ctor_m4055986311_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.NetworkDisconnection>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m699427846_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define DisposedObserver_1__cctor_m699427846(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))DisposedObserver_1__cctor_m699427846_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.NetworkDisconnection>::OnCompleted()
extern "C"  void DisposedObserver_1_OnCompleted_m3980518097_gshared (DisposedObserver_1_t3808185153 * __this, const MethodInfo* method);
#define DisposedObserver_1_OnCompleted_m3980518097(__this, method) ((  void (*) (DisposedObserver_1_t3808185153 *, const MethodInfo*))DisposedObserver_1_OnCompleted_m3980518097_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.NetworkDisconnection>::OnError(System.Exception)
extern "C"  void DisposedObserver_1_OnError_m2502025854_gshared (DisposedObserver_1_t3808185153 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define DisposedObserver_1_OnError_m2502025854(__this, ___error0, method) ((  void (*) (DisposedObserver_1_t3808185153 *, Exception_t1967233988 *, const MethodInfo*))DisposedObserver_1_OnError_m2502025854_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.NetworkDisconnection>::OnNext(T)
extern "C"  void DisposedObserver_1_OnNext_m803243375_gshared (DisposedObserver_1_t3808185153 * __this, int32_t ___value0, const MethodInfo* method);
#define DisposedObserver_1_OnNext_m803243375(__this, ___value0, method) ((  void (*) (DisposedObserver_1_t3808185153 *, int32_t, const MethodInfo*))DisposedObserver_1_OnNext_m803243375_gshared)(__this, ___value0, method)
