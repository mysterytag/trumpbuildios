﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;

#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Nullable_1_gen1403402234.h"
#include "mscorlib_System_Nullable_1_gen3225071844.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.TradeInfo
struct  TradeInfo_t1933812770  : public Il2CppObject
{
public:
	// System.Nullable`1<PlayFab.ClientModels.TradeStatus> PlayFab.ClientModels.TradeInfo::<Status>k__BackingField
	Nullable_1_t1403402234  ___U3CStatusU3Ek__BackingField_0;
	// System.String PlayFab.ClientModels.TradeInfo::<TradeId>k__BackingField
	String_t* ___U3CTradeIdU3Ek__BackingField_1;
	// System.String PlayFab.ClientModels.TradeInfo::<OfferingPlayerId>k__BackingField
	String_t* ___U3COfferingPlayerIdU3Ek__BackingField_2;
	// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.TradeInfo::<OfferedInventoryInstanceIds>k__BackingField
	List_1_t1765447871 * ___U3COfferedInventoryInstanceIdsU3Ek__BackingField_3;
	// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.TradeInfo::<OfferedCatalogItemIds>k__BackingField
	List_1_t1765447871 * ___U3COfferedCatalogItemIdsU3Ek__BackingField_4;
	// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.TradeInfo::<RequestedCatalogItemIds>k__BackingField
	List_1_t1765447871 * ___U3CRequestedCatalogItemIdsU3Ek__BackingField_5;
	// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.TradeInfo::<AllowedPlayerIds>k__BackingField
	List_1_t1765447871 * ___U3CAllowedPlayerIdsU3Ek__BackingField_6;
	// System.String PlayFab.ClientModels.TradeInfo::<AcceptedPlayerId>k__BackingField
	String_t* ___U3CAcceptedPlayerIdU3Ek__BackingField_7;
	// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.TradeInfo::<AcceptedInventoryInstanceIds>k__BackingField
	List_1_t1765447871 * ___U3CAcceptedInventoryInstanceIdsU3Ek__BackingField_8;
	// System.Nullable`1<System.DateTime> PlayFab.ClientModels.TradeInfo::<OpenedAt>k__BackingField
	Nullable_1_t3225071844  ___U3COpenedAtU3Ek__BackingField_9;
	// System.Nullable`1<System.DateTime> PlayFab.ClientModels.TradeInfo::<FilledAt>k__BackingField
	Nullable_1_t3225071844  ___U3CFilledAtU3Ek__BackingField_10;
	// System.Nullable`1<System.DateTime> PlayFab.ClientModels.TradeInfo::<CancelledAt>k__BackingField
	Nullable_1_t3225071844  ___U3CCancelledAtU3Ek__BackingField_11;
	// System.Nullable`1<System.DateTime> PlayFab.ClientModels.TradeInfo::<InvalidatedAt>k__BackingField
	Nullable_1_t3225071844  ___U3CInvalidatedAtU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CStatusU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TradeInfo_t1933812770, ___U3CStatusU3Ek__BackingField_0)); }
	inline Nullable_1_t1403402234  get_U3CStatusU3Ek__BackingField_0() const { return ___U3CStatusU3Ek__BackingField_0; }
	inline Nullable_1_t1403402234 * get_address_of_U3CStatusU3Ek__BackingField_0() { return &___U3CStatusU3Ek__BackingField_0; }
	inline void set_U3CStatusU3Ek__BackingField_0(Nullable_1_t1403402234  value)
	{
		___U3CStatusU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CTradeIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TradeInfo_t1933812770, ___U3CTradeIdU3Ek__BackingField_1)); }
	inline String_t* get_U3CTradeIdU3Ek__BackingField_1() const { return ___U3CTradeIdU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CTradeIdU3Ek__BackingField_1() { return &___U3CTradeIdU3Ek__BackingField_1; }
	inline void set_U3CTradeIdU3Ek__BackingField_1(String_t* value)
	{
		___U3CTradeIdU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTradeIdU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3COfferingPlayerIdU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TradeInfo_t1933812770, ___U3COfferingPlayerIdU3Ek__BackingField_2)); }
	inline String_t* get_U3COfferingPlayerIdU3Ek__BackingField_2() const { return ___U3COfferingPlayerIdU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3COfferingPlayerIdU3Ek__BackingField_2() { return &___U3COfferingPlayerIdU3Ek__BackingField_2; }
	inline void set_U3COfferingPlayerIdU3Ek__BackingField_2(String_t* value)
	{
		___U3COfferingPlayerIdU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3COfferingPlayerIdU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3COfferedInventoryInstanceIdsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(TradeInfo_t1933812770, ___U3COfferedInventoryInstanceIdsU3Ek__BackingField_3)); }
	inline List_1_t1765447871 * get_U3COfferedInventoryInstanceIdsU3Ek__BackingField_3() const { return ___U3COfferedInventoryInstanceIdsU3Ek__BackingField_3; }
	inline List_1_t1765447871 ** get_address_of_U3COfferedInventoryInstanceIdsU3Ek__BackingField_3() { return &___U3COfferedInventoryInstanceIdsU3Ek__BackingField_3; }
	inline void set_U3COfferedInventoryInstanceIdsU3Ek__BackingField_3(List_1_t1765447871 * value)
	{
		___U3COfferedInventoryInstanceIdsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3COfferedInventoryInstanceIdsU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3COfferedCatalogItemIdsU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(TradeInfo_t1933812770, ___U3COfferedCatalogItemIdsU3Ek__BackingField_4)); }
	inline List_1_t1765447871 * get_U3COfferedCatalogItemIdsU3Ek__BackingField_4() const { return ___U3COfferedCatalogItemIdsU3Ek__BackingField_4; }
	inline List_1_t1765447871 ** get_address_of_U3COfferedCatalogItemIdsU3Ek__BackingField_4() { return &___U3COfferedCatalogItemIdsU3Ek__BackingField_4; }
	inline void set_U3COfferedCatalogItemIdsU3Ek__BackingField_4(List_1_t1765447871 * value)
	{
		___U3COfferedCatalogItemIdsU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3COfferedCatalogItemIdsU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CRequestedCatalogItemIdsU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(TradeInfo_t1933812770, ___U3CRequestedCatalogItemIdsU3Ek__BackingField_5)); }
	inline List_1_t1765447871 * get_U3CRequestedCatalogItemIdsU3Ek__BackingField_5() const { return ___U3CRequestedCatalogItemIdsU3Ek__BackingField_5; }
	inline List_1_t1765447871 ** get_address_of_U3CRequestedCatalogItemIdsU3Ek__BackingField_5() { return &___U3CRequestedCatalogItemIdsU3Ek__BackingField_5; }
	inline void set_U3CRequestedCatalogItemIdsU3Ek__BackingField_5(List_1_t1765447871 * value)
	{
		___U3CRequestedCatalogItemIdsU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CRequestedCatalogItemIdsU3Ek__BackingField_5, value);
	}

	inline static int32_t get_offset_of_U3CAllowedPlayerIdsU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(TradeInfo_t1933812770, ___U3CAllowedPlayerIdsU3Ek__BackingField_6)); }
	inline List_1_t1765447871 * get_U3CAllowedPlayerIdsU3Ek__BackingField_6() const { return ___U3CAllowedPlayerIdsU3Ek__BackingField_6; }
	inline List_1_t1765447871 ** get_address_of_U3CAllowedPlayerIdsU3Ek__BackingField_6() { return &___U3CAllowedPlayerIdsU3Ek__BackingField_6; }
	inline void set_U3CAllowedPlayerIdsU3Ek__BackingField_6(List_1_t1765447871 * value)
	{
		___U3CAllowedPlayerIdsU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAllowedPlayerIdsU3Ek__BackingField_6, value);
	}

	inline static int32_t get_offset_of_U3CAcceptedPlayerIdU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(TradeInfo_t1933812770, ___U3CAcceptedPlayerIdU3Ek__BackingField_7)); }
	inline String_t* get_U3CAcceptedPlayerIdU3Ek__BackingField_7() const { return ___U3CAcceptedPlayerIdU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CAcceptedPlayerIdU3Ek__BackingField_7() { return &___U3CAcceptedPlayerIdU3Ek__BackingField_7; }
	inline void set_U3CAcceptedPlayerIdU3Ek__BackingField_7(String_t* value)
	{
		___U3CAcceptedPlayerIdU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAcceptedPlayerIdU3Ek__BackingField_7, value);
	}

	inline static int32_t get_offset_of_U3CAcceptedInventoryInstanceIdsU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(TradeInfo_t1933812770, ___U3CAcceptedInventoryInstanceIdsU3Ek__BackingField_8)); }
	inline List_1_t1765447871 * get_U3CAcceptedInventoryInstanceIdsU3Ek__BackingField_8() const { return ___U3CAcceptedInventoryInstanceIdsU3Ek__BackingField_8; }
	inline List_1_t1765447871 ** get_address_of_U3CAcceptedInventoryInstanceIdsU3Ek__BackingField_8() { return &___U3CAcceptedInventoryInstanceIdsU3Ek__BackingField_8; }
	inline void set_U3CAcceptedInventoryInstanceIdsU3Ek__BackingField_8(List_1_t1765447871 * value)
	{
		___U3CAcceptedInventoryInstanceIdsU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAcceptedInventoryInstanceIdsU3Ek__BackingField_8, value);
	}

	inline static int32_t get_offset_of_U3COpenedAtU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(TradeInfo_t1933812770, ___U3COpenedAtU3Ek__BackingField_9)); }
	inline Nullable_1_t3225071844  get_U3COpenedAtU3Ek__BackingField_9() const { return ___U3COpenedAtU3Ek__BackingField_9; }
	inline Nullable_1_t3225071844 * get_address_of_U3COpenedAtU3Ek__BackingField_9() { return &___U3COpenedAtU3Ek__BackingField_9; }
	inline void set_U3COpenedAtU3Ek__BackingField_9(Nullable_1_t3225071844  value)
	{
		___U3COpenedAtU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CFilledAtU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(TradeInfo_t1933812770, ___U3CFilledAtU3Ek__BackingField_10)); }
	inline Nullable_1_t3225071844  get_U3CFilledAtU3Ek__BackingField_10() const { return ___U3CFilledAtU3Ek__BackingField_10; }
	inline Nullable_1_t3225071844 * get_address_of_U3CFilledAtU3Ek__BackingField_10() { return &___U3CFilledAtU3Ek__BackingField_10; }
	inline void set_U3CFilledAtU3Ek__BackingField_10(Nullable_1_t3225071844  value)
	{
		___U3CFilledAtU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CCancelledAtU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(TradeInfo_t1933812770, ___U3CCancelledAtU3Ek__BackingField_11)); }
	inline Nullable_1_t3225071844  get_U3CCancelledAtU3Ek__BackingField_11() const { return ___U3CCancelledAtU3Ek__BackingField_11; }
	inline Nullable_1_t3225071844 * get_address_of_U3CCancelledAtU3Ek__BackingField_11() { return &___U3CCancelledAtU3Ek__BackingField_11; }
	inline void set_U3CCancelledAtU3Ek__BackingField_11(Nullable_1_t3225071844  value)
	{
		___U3CCancelledAtU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CInvalidatedAtU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(TradeInfo_t1933812770, ___U3CInvalidatedAtU3Ek__BackingField_12)); }
	inline Nullable_1_t3225071844  get_U3CInvalidatedAtU3Ek__BackingField_12() const { return ___U3CInvalidatedAtU3Ek__BackingField_12; }
	inline Nullable_1_t3225071844 * get_address_of_U3CInvalidatedAtU3Ek__BackingField_12() { return &___U3CInvalidatedAtU3Ek__BackingField_12; }
	inline void set_U3CInvalidatedAtU3Ek__BackingField_12(Nullable_1_t3225071844  value)
	{
		___U3CInvalidatedAtU3Ek__BackingField_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
