﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectManyObservable`2/SelectManyOuterObserver<UniRx.Unit,UniRx.Unit>
struct SelectManyOuterObserver_t2776126513;
// UniRx.Operators.SelectManyObservable`2<UniRx.Unit,UniRx.Unit>
struct SelectManyObservable_2_t3540602214;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyOuterObserver<UniRx.Unit,UniRx.Unit>::.ctor(UniRx.Operators.SelectManyObservable`2<TSource,TResult>,UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void SelectManyOuterObserver__ctor_m3735130851_gshared (SelectManyOuterObserver_t2776126513 * __this, SelectManyObservable_2_t3540602214 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define SelectManyOuterObserver__ctor_m3735130851(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (SelectManyOuterObserver_t2776126513 *, SelectManyObservable_2_t3540602214 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SelectManyOuterObserver__ctor_m3735130851_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.SelectManyObservable`2/SelectManyOuterObserver<UniRx.Unit,UniRx.Unit>::Run()
extern "C"  Il2CppObject * SelectManyOuterObserver_Run_m476554939_gshared (SelectManyOuterObserver_t2776126513 * __this, const MethodInfo* method);
#define SelectManyOuterObserver_Run_m476554939(__this, method) ((  Il2CppObject * (*) (SelectManyOuterObserver_t2776126513 *, const MethodInfo*))SelectManyOuterObserver_Run_m476554939_gshared)(__this, method)
// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyOuterObserver<UniRx.Unit,UniRx.Unit>::OnNext(TSource)
extern "C"  void SelectManyOuterObserver_OnNext_m1550702500_gshared (SelectManyOuterObserver_t2776126513 * __this, Unit_t2558286038  ___value0, const MethodInfo* method);
#define SelectManyOuterObserver_OnNext_m1550702500(__this, ___value0, method) ((  void (*) (SelectManyOuterObserver_t2776126513 *, Unit_t2558286038 , const MethodInfo*))SelectManyOuterObserver_OnNext_m1550702500_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyOuterObserver<UniRx.Unit,UniRx.Unit>::OnError(System.Exception)
extern "C"  void SelectManyOuterObserver_OnError_m159930894_gshared (SelectManyOuterObserver_t2776126513 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define SelectManyOuterObserver_OnError_m159930894(__this, ___error0, method) ((  void (*) (SelectManyOuterObserver_t2776126513 *, Exception_t1967233988 *, const MethodInfo*))SelectManyOuterObserver_OnError_m159930894_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyOuterObserver<UniRx.Unit,UniRx.Unit>::OnCompleted()
extern "C"  void SelectManyOuterObserver_OnCompleted_m175321697_gshared (SelectManyOuterObserver_t2776126513 * __this, const MethodInfo* method);
#define SelectManyOuterObserver_OnCompleted_m175321697(__this, method) ((  void (*) (SelectManyOuterObserver_t2776126513 *, const MethodInfo*))SelectManyOuterObserver_OnCompleted_m175321697_gshared)(__this, method)
