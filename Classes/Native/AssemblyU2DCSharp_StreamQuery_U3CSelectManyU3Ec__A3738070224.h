﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// IStream`1<System.Object>
struct IStream_1_t1389797411;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StreamQuery/<SelectMany>c__AnonStorey13C`2<System.Object,System.Object>
struct  U3CSelectManyU3Ec__AnonStorey13C_2_t3738070224  : public Il2CppObject
{
public:
	// IStream`1<TR> StreamQuery/<SelectMany>c__AnonStorey13C`2::other
	Il2CppObject* ___other_0;

public:
	inline static int32_t get_offset_of_other_0() { return static_cast<int32_t>(offsetof(U3CSelectManyU3Ec__AnonStorey13C_2_t3738070224, ___other_0)); }
	inline Il2CppObject* get_other_0() const { return ___other_0; }
	inline Il2CppObject** get_address_of_other_0() { return &___other_0; }
	inline void set_other_0(Il2CppObject* value)
	{
		___other_0 = value;
		Il2CppCodeGenWriteBarrier(&___other_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
