﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.WebRequestExtensions/<AbortableDeferredAsyncRequest>c__AnonStorey33`1<System.Object>
struct U3CAbortableDeferredAsyncRequestU3Ec__AnonStorey33_1_t4099961315;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.WebRequestExtensions/<AbortableDeferredAsyncRequest>c__AnonStorey33`1/<AbortableDeferredAsyncRequest>c__AnonStorey34`1<System.Object>
struct  U3CAbortableDeferredAsyncRequestU3Ec__AnonStorey34_1_t3654936316  : public Il2CppObject
{
public:
	// System.Int32 UniRx.WebRequestExtensions/<AbortableDeferredAsyncRequest>c__AnonStorey33`1/<AbortableDeferredAsyncRequest>c__AnonStorey34`1::isCompleted
	int32_t ___isCompleted_0;
	// System.IDisposable UniRx.WebRequestExtensions/<AbortableDeferredAsyncRequest>c__AnonStorey33`1/<AbortableDeferredAsyncRequest>c__AnonStorey34`1::subscription
	Il2CppObject * ___subscription_1;
	// UniRx.WebRequestExtensions/<AbortableDeferredAsyncRequest>c__AnonStorey33`1<TResult> UniRx.WebRequestExtensions/<AbortableDeferredAsyncRequest>c__AnonStorey33`1/<AbortableDeferredAsyncRequest>c__AnonStorey34`1::<>f__ref$51
	U3CAbortableDeferredAsyncRequestU3Ec__AnonStorey33_1_t4099961315 * ___U3CU3Ef__refU2451_2;

public:
	inline static int32_t get_offset_of_isCompleted_0() { return static_cast<int32_t>(offsetof(U3CAbortableDeferredAsyncRequestU3Ec__AnonStorey34_1_t3654936316, ___isCompleted_0)); }
	inline int32_t get_isCompleted_0() const { return ___isCompleted_0; }
	inline int32_t* get_address_of_isCompleted_0() { return &___isCompleted_0; }
	inline void set_isCompleted_0(int32_t value)
	{
		___isCompleted_0 = value;
	}

	inline static int32_t get_offset_of_subscription_1() { return static_cast<int32_t>(offsetof(U3CAbortableDeferredAsyncRequestU3Ec__AnonStorey34_1_t3654936316, ___subscription_1)); }
	inline Il2CppObject * get_subscription_1() const { return ___subscription_1; }
	inline Il2CppObject ** get_address_of_subscription_1() { return &___subscription_1; }
	inline void set_subscription_1(Il2CppObject * value)
	{
		___subscription_1 = value;
		Il2CppCodeGenWriteBarrier(&___subscription_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU2451_2() { return static_cast<int32_t>(offsetof(U3CAbortableDeferredAsyncRequestU3Ec__AnonStorey34_1_t3654936316, ___U3CU3Ef__refU2451_2)); }
	inline U3CAbortableDeferredAsyncRequestU3Ec__AnonStorey33_1_t4099961315 * get_U3CU3Ef__refU2451_2() const { return ___U3CU3Ef__refU2451_2; }
	inline U3CAbortableDeferredAsyncRequestU3Ec__AnonStorey33_1_t4099961315 ** get_address_of_U3CU3Ef__refU2451_2() { return &___U3CU3Ef__refU2451_2; }
	inline void set_U3CU3Ef__refU2451_2(U3CAbortableDeferredAsyncRequestU3Ec__AnonStorey33_1_t4099961315 * value)
	{
		___U3CU3Ef__refU2451_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU2451_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
