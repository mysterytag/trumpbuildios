﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Comparison_1_gen3540781296MethodDeclarations.h"

// System.Void System.Comparison`1<OnePF.SkuDetails>::.ctor(System.Object,System.IntPtr)
#define Comparison_1__ctor_m206411922(__this, ___object0, ___method1, method) ((  void (*) (Comparison_1_t2773804885 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m487232819_gshared)(__this, ___object0, ___method1, method)
// System.Int32 System.Comparison`1<OnePF.SkuDetails>::Invoke(T,T)
#define Comparison_1_Invoke_m137623974(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t2773804885 *, SkuDetails_t70130009 *, SkuDetails_t70130009 *, const MethodInfo*))Comparison_1_Invoke_m1888033133_gshared)(__this, ___x0, ___y1, method)
// System.IAsyncResult System.Comparison`1<OnePF.SkuDetails>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m1000148207(__this, ___x0, ___y1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Comparison_1_t2773804885 *, SkuDetails_t70130009 *, SkuDetails_t70130009 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Comparison_1_BeginInvoke_m3177996774_gshared)(__this, ___x0, ___y1, ___callback2, ___object3, method)
// System.Int32 System.Comparison`1<OnePF.SkuDetails>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m3112671494(__this, ___result0, method) ((  int32_t (*) (Comparison_1_t2773804885 *, Il2CppObject *, const MethodInfo*))Comparison_1_EndInvoke_m651541983_gshared)(__this, ___result0, method)
