﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ThrottleFirstFrameObservable`1/ThrottleFirstFrame/ThrottleFirstFrameTick<System.Object>
struct ThrottleFirstFrameTick_t2464530589;
// UniRx.Operators.ThrottleFirstFrameObservable`1/ThrottleFirstFrame<System.Object>
struct ThrottleFirstFrame_t517244520;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.ThrottleFirstFrameObservable`1/ThrottleFirstFrame/ThrottleFirstFrameTick<System.Object>::.ctor(UniRx.Operators.ThrottleFirstFrameObservable`1/ThrottleFirstFrame<T>)
extern "C"  void ThrottleFirstFrameTick__ctor_m2743899176_gshared (ThrottleFirstFrameTick_t2464530589 * __this, ThrottleFirstFrame_t517244520 * ___parent0, const MethodInfo* method);
#define ThrottleFirstFrameTick__ctor_m2743899176(__this, ___parent0, method) ((  void (*) (ThrottleFirstFrameTick_t2464530589 *, ThrottleFirstFrame_t517244520 *, const MethodInfo*))ThrottleFirstFrameTick__ctor_m2743899176_gshared)(__this, ___parent0, method)
// System.Void UniRx.Operators.ThrottleFirstFrameObservable`1/ThrottleFirstFrame/ThrottleFirstFrameTick<System.Object>::OnCompleted()
extern "C"  void ThrottleFirstFrameTick_OnCompleted_m665198516_gshared (ThrottleFirstFrameTick_t2464530589 * __this, const MethodInfo* method);
#define ThrottleFirstFrameTick_OnCompleted_m665198516(__this, method) ((  void (*) (ThrottleFirstFrameTick_t2464530589 *, const MethodInfo*))ThrottleFirstFrameTick_OnCompleted_m665198516_gshared)(__this, method)
// System.Void UniRx.Operators.ThrottleFirstFrameObservable`1/ThrottleFirstFrame/ThrottleFirstFrameTick<System.Object>::OnError(System.Exception)
extern "C"  void ThrottleFirstFrameTick_OnError_m2369446625_gshared (ThrottleFirstFrameTick_t2464530589 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ThrottleFirstFrameTick_OnError_m2369446625(__this, ___error0, method) ((  void (*) (ThrottleFirstFrameTick_t2464530589 *, Exception_t1967233988 *, const MethodInfo*))ThrottleFirstFrameTick_OnError_m2369446625_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.ThrottleFirstFrameObservable`1/ThrottleFirstFrame/ThrottleFirstFrameTick<System.Object>::OnNext(System.Int64)
extern "C"  void ThrottleFirstFrameTick_OnNext_m2155104734_gshared (ThrottleFirstFrameTick_t2464530589 * __this, int64_t ____0, const MethodInfo* method);
#define ThrottleFirstFrameTick_OnNext_m2155104734(__this, ____0, method) ((  void (*) (ThrottleFirstFrameTick_t2464530589 *, int64_t, const MethodInfo*))ThrottleFirstFrameTick_OnNext_m2155104734_gshared)(__this, ____0, method)
