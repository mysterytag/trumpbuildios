﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.CombineLatestObservable`5<System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object>
struct CombineLatestObservable_5_t3418069172;
// UniRx.IObservable`1<System.Boolean>
struct IObservable_1_t4264771001;
// UniRx.Operators.CombineLatestFunc`5<System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object>
struct CombineLatestFunc_5_t2231862077;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.CombineLatestObservable`5<System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object>::.ctor(UniRx.IObservable`1<T1>,UniRx.IObservable`1<T2>,UniRx.IObservable`1<T3>,UniRx.IObservable`1<T4>,UniRx.Operators.CombineLatestFunc`5<T1,T2,T3,T4,TR>)
extern "C"  void CombineLatestObservable_5__ctor_m1765463864_gshared (CombineLatestObservable_5_t3418069172 * __this, Il2CppObject* ___source10, Il2CppObject* ___source21, Il2CppObject* ___source32, Il2CppObject* ___source43, CombineLatestFunc_5_t2231862077 * ___resultSelector4, const MethodInfo* method);
#define CombineLatestObservable_5__ctor_m1765463864(__this, ___source10, ___source21, ___source32, ___source43, ___resultSelector4, method) ((  void (*) (CombineLatestObservable_5_t3418069172 *, Il2CppObject*, Il2CppObject*, Il2CppObject*, Il2CppObject*, CombineLatestFunc_5_t2231862077 *, const MethodInfo*))CombineLatestObservable_5__ctor_m1765463864_gshared)(__this, ___source10, ___source21, ___source32, ___source43, ___resultSelector4, method)
// System.IDisposable UniRx.Operators.CombineLatestObservable`5<System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object>::SubscribeCore(UniRx.IObserver`1<TR>,System.IDisposable)
extern "C"  Il2CppObject * CombineLatestObservable_5_SubscribeCore_m4118138892_gshared (CombineLatestObservable_5_t3418069172 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define CombineLatestObservable_5_SubscribeCore_m4118138892(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (CombineLatestObservable_5_t3418069172 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))CombineLatestObservable_5_SubscribeCore_m4118138892_gshared)(__this, ___observer0, ___cancel1, method)
