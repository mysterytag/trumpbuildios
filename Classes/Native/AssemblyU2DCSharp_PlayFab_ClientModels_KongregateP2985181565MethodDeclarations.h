﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.KongregatePlayFabIdPair
struct KongregatePlayFabIdPair_t2985181565;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.KongregatePlayFabIdPair::.ctor()
extern "C"  void KongregatePlayFabIdPair__ctor_m2967690444 (KongregatePlayFabIdPair_t2985181565 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.KongregatePlayFabIdPair::get_KongregateId()
extern "C"  String_t* KongregatePlayFabIdPair_get_KongregateId_m522639738 (KongregatePlayFabIdPair_t2985181565 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.KongregatePlayFabIdPair::set_KongregateId(System.String)
extern "C"  void KongregatePlayFabIdPair_set_KongregateId_m3937939479 (KongregatePlayFabIdPair_t2985181565 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.KongregatePlayFabIdPair::get_PlayFabId()
extern "C"  String_t* KongregatePlayFabIdPair_get_PlayFabId_m3729014380 (KongregatePlayFabIdPair_t2985181565 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.KongregatePlayFabIdPair::set_PlayFabId(System.String)
extern "C"  void KongregatePlayFabIdPair_set_PlayFabId_m1901753895 (KongregatePlayFabIdPair_t2985181565 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
