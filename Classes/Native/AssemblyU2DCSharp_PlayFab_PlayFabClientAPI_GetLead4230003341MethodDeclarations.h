﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetLeaderboardRequestCallback
struct GetLeaderboardRequestCallback_t4230003341;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetLeaderboardRequest
struct GetLeaderboardRequest_t1150085688;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetLeaderbo1150085688.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetLeaderboardRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetLeaderboardRequestCallback__ctor_m1374107804 (GetLeaderboardRequestCallback_t4230003341 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetLeaderboardRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetLeaderboardRequest,System.Object)
extern "C"  void GetLeaderboardRequestCallback_Invoke_m2576981055 (GetLeaderboardRequestCallback_t4230003341 * __this, String_t* ___urlPath0, int32_t ___callId1, GetLeaderboardRequest_t1150085688 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetLeaderboardRequestCallback_t4230003341(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetLeaderboardRequest_t1150085688 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetLeaderboardRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetLeaderboardRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetLeaderboardRequestCallback_BeginInvoke_m2124209086 (GetLeaderboardRequestCallback_t4230003341 * __this, String_t* ___urlPath0, int32_t ___callId1, GetLeaderboardRequest_t1150085688 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetLeaderboardRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetLeaderboardRequestCallback_EndInvoke_m2584172716 (GetLeaderboardRequestCallback_t4230003341 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
