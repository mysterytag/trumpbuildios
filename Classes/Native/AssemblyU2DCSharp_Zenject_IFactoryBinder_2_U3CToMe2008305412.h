﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`3<Zenject.DiContainer,System.Object,System.Object>
struct Func_3_t1309472546;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.IFactoryBinder`2/<ToMethod>c__AnonStorey176<System.Object,System.Object>
struct  U3CToMethodU3Ec__AnonStorey176_t2008305412  : public Il2CppObject
{
public:
	// System.Func`3<Zenject.DiContainer,TParam1,TContract> Zenject.IFactoryBinder`2/<ToMethod>c__AnonStorey176::method
	Func_3_t1309472546 * ___method_0;

public:
	inline static int32_t get_offset_of_method_0() { return static_cast<int32_t>(offsetof(U3CToMethodU3Ec__AnonStorey176_t2008305412, ___method_0)); }
	inline Func_3_t1309472546 * get_method_0() const { return ___method_0; }
	inline Func_3_t1309472546 ** get_address_of_method_0() { return &___method_0; }
	inline void set_method_0(Func_3_t1309472546 * value)
	{
		___method_0 = value;
		Il2CppCodeGenWriteBarrier(&___method_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
