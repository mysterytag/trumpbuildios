﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MoneyDisplay
struct MoneyDisplay_t913476610;
// UniRx.IObservable`1<System.Double>
struct IObservable_1_t293314978;

#include "codegen/il2cpp-codegen.h"

// System.Void MoneyDisplay::.ctor()
extern "C"  void MoneyDisplay__ctor_m2214971609 (MoneyDisplay_t913476610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoneyDisplay::PostInject()
extern "C"  void MoneyDisplay_PostInject_m747432924 (MoneyDisplay_t913476610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoneyDisplay::Awake()
extern "C"  void MoneyDisplay_Awake_m2452576828 (MoneyDisplay_t913476610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoneyDisplay::Update()
extern "C"  void MoneyDisplay_Update_m1671505236 (MoneyDisplay_t913476610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.Double> MoneyDisplay::<Awake>m__42(System.Double)
extern "C"  Il2CppObject* MoneyDisplay_U3CAwakeU3Em__42_m2048272969 (MoneyDisplay_t913476610 * __this, double ___doubleass0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double MoneyDisplay::<Awake>m__43(System.Double,System.Double)
extern "C"  double MoneyDisplay_U3CAwakeU3Em__43_m1050540313 (Il2CppObject * __this /* static, unused */, double ___doubleass0, double ___ass1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoneyDisplay::<Awake>m__44(System.Double)
extern "C"  void MoneyDisplay_U3CAwakeU3Em__44_m936231999 (MoneyDisplay_t913476610 * __this, double ___varass0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
