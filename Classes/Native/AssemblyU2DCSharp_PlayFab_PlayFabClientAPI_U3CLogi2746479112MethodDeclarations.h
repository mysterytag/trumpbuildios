﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<LoginWithGoogleAccount>c__AnonStorey64
struct U3CLoginWithGoogleAccountU3Ec__AnonStorey64_t2746479112;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<LoginWithGoogleAccount>c__AnonStorey64::.ctor()
extern "C"  void U3CLoginWithGoogleAccountU3Ec__AnonStorey64__ctor_m4278910635 (U3CLoginWithGoogleAccountU3Ec__AnonStorey64_t2746479112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<LoginWithGoogleAccount>c__AnonStorey64::<>m__51(PlayFab.CallRequestContainer)
extern "C"  void U3CLoginWithGoogleAccountU3Ec__AnonStorey64_U3CU3Em__51_m3346980325 (U3CLoginWithGoogleAccountU3Ec__AnonStorey64_t2746479112 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
