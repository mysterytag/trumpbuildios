﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LocalyticsUnity.Localytics/ReceiveMessagingDelegate
struct ReceiveMessagingDelegate_t3408128038;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void LocalyticsUnity.Localytics/ReceiveMessagingDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void ReceiveMessagingDelegate__ctor_m1867403885 (ReceiveMessagingDelegate_t3408128038 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics/ReceiveMessagingDelegate::Invoke(System.String)
extern "C"  void ReceiveMessagingDelegate_Invoke_m2366936411 (ReceiveMessagingDelegate_t3408128038 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_ReceiveMessagingDelegate_t3408128038(Il2CppObject* delegate, String_t* ___message0);
// System.IAsyncResult LocalyticsUnity.Localytics/ReceiveMessagingDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ReceiveMessagingDelegate_BeginInvoke_m3853697384 (ReceiveMessagingDelegate_t3408128038 * __this, String_t* ___message0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics/ReceiveMessagingDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void ReceiveMessagingDelegate_EndInvoke_m1918763773 (ReceiveMessagingDelegate_t3408128038 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
