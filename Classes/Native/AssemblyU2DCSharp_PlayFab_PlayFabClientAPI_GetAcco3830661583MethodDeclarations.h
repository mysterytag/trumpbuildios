﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetAccountInfoRequestCallback
struct GetAccountInfoRequestCallback_t3830661583;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetAccountInfoRequest
struct GetAccountInfoRequest_t2417218426;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetAccountI2417218426.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetAccountInfoRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetAccountInfoRequestCallback__ctor_m451331806 (GetAccountInfoRequestCallback_t3830661583 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetAccountInfoRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetAccountInfoRequest,System.Object)
extern "C"  void GetAccountInfoRequestCallback_Invoke_m330493627 (GetAccountInfoRequestCallback_t3830661583 * __this, String_t* ___urlPath0, int32_t ___callId1, GetAccountInfoRequest_t2417218426 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetAccountInfoRequestCallback_t3830661583(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetAccountInfoRequest_t2417218426 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetAccountInfoRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetAccountInfoRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetAccountInfoRequestCallback_BeginInvoke_m1172374142 (GetAccountInfoRequestCallback_t3830661583 * __this, String_t* ___urlPath0, int32_t ___callId1, GetAccountInfoRequest_t2417218426 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetAccountInfoRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetAccountInfoRequestCallback_EndInvoke_m1878808558 (GetAccountInfoRequestCallback_t3830661583 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
