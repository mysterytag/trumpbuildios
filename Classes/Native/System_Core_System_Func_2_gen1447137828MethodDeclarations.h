﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen3172540574MethodDeclarations.h"

// System.Void System.Func`2<System.Double,System.Collections.Generic.IEnumerable`1<System.Double>>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m3595123009(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t1447137828 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m489610042_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<System.Double,System.Collections.Generic.IEnumerable`1<System.Double>>::Invoke(T)
#define Func_2_Invoke_m3591836161(__this, ___arg10, method) ((  Il2CppObject* (*) (Func_2_t1447137828 *, double, const MethodInfo*))Func_2_Invoke_m1334556520_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<System.Double,System.Collections.Generic.IEnumerable`1<System.Double>>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m1699053488(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t1447137828 *, double, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m745473943_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<System.Double,System.Collections.Generic.IEnumerable`1<System.Double>>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m3670158451(__this, ___result0, method) ((  Il2CppObject* (*) (Func_2_t1447137828 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m3806641516_gshared)(__this, ___result0, method)
