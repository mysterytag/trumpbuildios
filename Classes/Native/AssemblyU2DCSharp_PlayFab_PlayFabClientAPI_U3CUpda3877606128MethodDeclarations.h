﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<UpdateCharacterStatistics>c__AnonStoreyCC
struct U3CUpdateCharacterStatisticsU3Ec__AnonStoreyCC_t3877606128;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<UpdateCharacterStatistics>c__AnonStoreyCC::.ctor()
extern "C"  void U3CUpdateCharacterStatisticsU3Ec__AnonStoreyCC__ctor_m154015331 (U3CUpdateCharacterStatisticsU3Ec__AnonStoreyCC_t3877606128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<UpdateCharacterStatistics>c__AnonStoreyCC::<>m__B9(PlayFab.CallRequestContainer)
extern "C"  void U3CUpdateCharacterStatisticsU3Ec__AnonStoreyCC_U3CU3Em__B9_m896082936 (U3CUpdateCharacterStatisticsU3Ec__AnonStoreyCC_t3877606128 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
