﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SynchronizedObserver`1<System.Object>
struct SynchronizedObserver_1_t2847152769;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SynchronizedObserver`1<System.Object>::.ctor(UniRx.IObserver`1<T>,System.Object)
extern "C"  void SynchronizedObserver_1__ctor_m3475009133_gshared (SynchronizedObserver_1_t2847152769 * __this, Il2CppObject* ___observer0, Il2CppObject * ___gate1, const MethodInfo* method);
#define SynchronizedObserver_1__ctor_m3475009133(__this, ___observer0, ___gate1, method) ((  void (*) (SynchronizedObserver_1_t2847152769 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SynchronizedObserver_1__ctor_m3475009133_gshared)(__this, ___observer0, ___gate1, method)
// System.Void UniRx.Operators.SynchronizedObserver`1<System.Object>::OnNext(T)
extern "C"  void SynchronizedObserver_1_OnNext_m3514316437_gshared (SynchronizedObserver_1_t2847152769 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define SynchronizedObserver_1_OnNext_m3514316437(__this, ___value0, method) ((  void (*) (SynchronizedObserver_1_t2847152769 *, Il2CppObject *, const MethodInfo*))SynchronizedObserver_1_OnNext_m3514316437_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SynchronizedObserver`1<System.Object>::OnError(System.Exception)
extern "C"  void SynchronizedObserver_1_OnError_m2490271652_gshared (SynchronizedObserver_1_t2847152769 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define SynchronizedObserver_1_OnError_m2490271652(__this, ___error0, method) ((  void (*) (SynchronizedObserver_1_t2847152769 *, Exception_t1967233988 *, const MethodInfo*))SynchronizedObserver_1_OnError_m2490271652_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SynchronizedObserver`1<System.Object>::OnCompleted()
extern "C"  void SynchronizedObserver_1_OnCompleted_m2880475383_gshared (SynchronizedObserver_1_t2847152769 * __this, const MethodInfo* method);
#define SynchronizedObserver_1_OnCompleted_m2880475383(__this, method) ((  void (*) (SynchronizedObserver_1_t2847152769 *, const MethodInfo*))SynchronizedObserver_1_OnCompleted_m2880475383_gshared)(__this, method)
