﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GlobalStat
struct GlobalStat_t1134678199;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GlobalStat/<Init>c__AnonStoreyE7
struct  U3CInitU3Ec__AnonStoreyE7_t1005789127  : public Il2CppObject
{
public:
	// System.Int32 GlobalStat/<Init>c__AnonStoreyE7::index1
	int32_t ___index1_0;
	// GlobalStat GlobalStat/<Init>c__AnonStoreyE7::<>f__this
	GlobalStat_t1134678199 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_index1_0() { return static_cast<int32_t>(offsetof(U3CInitU3Ec__AnonStoreyE7_t1005789127, ___index1_0)); }
	inline int32_t get_index1_0() const { return ___index1_0; }
	inline int32_t* get_address_of_index1_0() { return &___index1_0; }
	inline void set_index1_0(int32_t value)
	{
		___index1_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CInitU3Ec__AnonStoreyE7_t1005789127, ___U3CU3Ef__this_1)); }
	inline GlobalStat_t1134678199 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline GlobalStat_t1134678199 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(GlobalStat_t1134678199 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
