﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnalyticsController/<Start>c__Iterator1C
struct U3CStartU3Ec__Iterator1C_t3751742091;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void AnalyticsController/<Start>c__Iterator1C::.ctor()
extern "C"  void U3CStartU3Ec__Iterator1C__ctor_m1248882371 (U3CStartU3Ec__Iterator1C_t3751742091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AnalyticsController/<Start>c__Iterator1C::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator1C_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2649299449 (U3CStartU3Ec__Iterator1C_t3751742091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AnalyticsController/<Start>c__Iterator1C::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator1C_System_Collections_IEnumerator_get_Current_m2308452237 (U3CStartU3Ec__Iterator1C_t3751742091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AnalyticsController/<Start>c__Iterator1C::MoveNext()
extern "C"  bool U3CStartU3Ec__Iterator1C_MoveNext_m1112926265 (U3CStartU3Ec__Iterator1C_t3751742091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController/<Start>c__Iterator1C::Dispose()
extern "C"  void U3CStartU3Ec__Iterator1C_Dispose_m1778366016 (U3CStartU3Ec__Iterator1C_t3751742091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController/<Start>c__Iterator1C::Reset()
extern "C"  void U3CStartU3Ec__Iterator1C_Reset_m3190282608 (U3CStartU3Ec__Iterator1C_t3751742091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
