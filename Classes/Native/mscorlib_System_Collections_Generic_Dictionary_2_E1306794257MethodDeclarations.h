﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Int64>
struct Dictionary_2_t1539766316;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1306794257.h"
#include "mscorlib_System_Collections_DictionaryEntry130027246.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21028297614.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2232813323_gshared (Enumerator_t1306794257 * __this, Dictionary_2_t1539766316 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m2232813323(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1306794257 *, Dictionary_2_t1539766316 *, const MethodInfo*))Enumerator__ctor_m2232813323_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2861177408_gshared (Enumerator_t1306794257 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2861177408(__this, method) ((  Il2CppObject * (*) (Enumerator_t1306794257 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2861177408_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1729157322_gshared (Enumerator_t1306794257 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1729157322(__this, method) ((  void (*) (Enumerator_t1306794257 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1729157322_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t130027246  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3820758529_gshared (Enumerator_t1306794257 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3820758529(__this, method) ((  DictionaryEntry_t130027246  (*) (Enumerator_t1306794257 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3820758529_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2217518876_gshared (Enumerator_t1306794257 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2217518876(__this, method) ((  Il2CppObject * (*) (Enumerator_t1306794257 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2217518876_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1780297390_gshared (Enumerator_t1306794257 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1780297390(__this, method) ((  Il2CppObject * (*) (Enumerator_t1306794257 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1780297390_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1990959482_gshared (Enumerator_t1306794257 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1990959482(__this, method) ((  bool (*) (Enumerator_t1306794257 *, const MethodInfo*))Enumerator_MoveNext_m1990959482_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::get_Current()
extern "C"  KeyValuePair_2_t1028297614  Enumerator_get_Current_m2369121538_gshared (Enumerator_t1306794257 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2369121538(__this, method) ((  KeyValuePair_2_t1028297614  (*) (Enumerator_t1306794257 *, const MethodInfo*))Enumerator_get_Current_m2369121538_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::get_CurrentKey()
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m1962050691_gshared (Enumerator_t1306794257 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m1962050691(__this, method) ((  Il2CppObject * (*) (Enumerator_t1306794257 *, const MethodInfo*))Enumerator_get_CurrentKey_m1962050691_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::get_CurrentValue()
extern "C"  int64_t Enumerator_get_CurrentValue_m1467416067_gshared (Enumerator_t1306794257 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m1467416067(__this, method) ((  int64_t (*) (Enumerator_t1306794257 *, const MethodInfo*))Enumerator_get_CurrentValue_m1467416067_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::Reset()
extern "C"  void Enumerator_Reset_m800276701_gshared (Enumerator_t1306794257 * __this, const MethodInfo* method);
#define Enumerator_Reset_m800276701(__this, method) ((  void (*) (Enumerator_t1306794257 *, const MethodInfo*))Enumerator_Reset_m800276701_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::VerifyState()
extern "C"  void Enumerator_VerifyState_m3373774630_gshared (Enumerator_t1306794257 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m3373774630(__this, method) ((  void (*) (Enumerator_t1306794257 *, const MethodInfo*))Enumerator_VerifyState_m3373774630_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m2241982734_gshared (Enumerator_t1306794257 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m2241982734(__this, method) ((  void (*) (Enumerator_t1306794257 *, const MethodInfo*))Enumerator_VerifyCurrent_m2241982734_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::Dispose()
extern "C"  void Enumerator_Dispose_m2790192749_gshared (Enumerator_t1306794257 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2790192749(__this, method) ((  void (*) (Enumerator_t1306794257 *, const MethodInfo*))Enumerator_Dispose_m2790192749_gshared)(__this, method)
