﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Triggers.ObservableRectTransformTrigger
struct ObservableRectTransformTrigger_t3969084276;
// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Triggers.ObservableRectTransformTrigger::.ctor()
extern "C"  void ObservableRectTransformTrigger__ctor_m367464073 (ObservableRectTransformTrigger_t3969084276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableRectTransformTrigger::OnRectTransformDimensionsChange()
extern "C"  void ObservableRectTransformTrigger_OnRectTransformDimensionsChange_m4088942829 (ObservableRectTransformTrigger_t3969084276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Triggers.ObservableRectTransformTrigger::OnRectTransformDimensionsChangeAsObservable()
extern "C"  Il2CppObject* ObservableRectTransformTrigger_OnRectTransformDimensionsChangeAsObservable_m2303475882 (ObservableRectTransformTrigger_t3969084276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableRectTransformTrigger::OnRectTransformRemoved()
extern "C"  void ObservableRectTransformTrigger_OnRectTransformRemoved_m2091426610 (ObservableRectTransformTrigger_t3969084276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Triggers.ObservableRectTransformTrigger::OnRectTransformRemovedAsObservable()
extern "C"  Il2CppObject* ObservableRectTransformTrigger_OnRectTransformRemovedAsObservable_m3123532511 (ObservableRectTransformTrigger_t3969084276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableRectTransformTrigger::RaiseOnCompletedOnDestroy()
extern "C"  void ObservableRectTransformTrigger_RaiseOnCompletedOnDestroy_m1438926658 (ObservableRectTransformTrigger_t3969084276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
