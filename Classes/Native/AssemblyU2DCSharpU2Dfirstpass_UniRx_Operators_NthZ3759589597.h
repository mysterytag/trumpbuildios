﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.ICollection[]
struct ICollectionU5BU5D_t1434690852;
// System.Boolean[]
struct BooleanU5BU5D_t3804927312;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.NthZipObserverBase`1<System.Object>
struct  NthZipObserverBase_1_t3759589597  : public OperatorObserverBase_2_t1187768149
{
public:
	// System.Collections.ICollection[] UniRx.Operators.NthZipObserverBase`1::queues
	ICollectionU5BU5D_t1434690852* ___queues_2;
	// System.Boolean[] UniRx.Operators.NthZipObserverBase`1::isDone
	BooleanU5BU5D_t3804927312* ___isDone_3;
	// System.Int32 UniRx.Operators.NthZipObserverBase`1::length
	int32_t ___length_4;

public:
	inline static int32_t get_offset_of_queues_2() { return static_cast<int32_t>(offsetof(NthZipObserverBase_1_t3759589597, ___queues_2)); }
	inline ICollectionU5BU5D_t1434690852* get_queues_2() const { return ___queues_2; }
	inline ICollectionU5BU5D_t1434690852** get_address_of_queues_2() { return &___queues_2; }
	inline void set_queues_2(ICollectionU5BU5D_t1434690852* value)
	{
		___queues_2 = value;
		Il2CppCodeGenWriteBarrier(&___queues_2, value);
	}

	inline static int32_t get_offset_of_isDone_3() { return static_cast<int32_t>(offsetof(NthZipObserverBase_1_t3759589597, ___isDone_3)); }
	inline BooleanU5BU5D_t3804927312* get_isDone_3() const { return ___isDone_3; }
	inline BooleanU5BU5D_t3804927312** get_address_of_isDone_3() { return &___isDone_3; }
	inline void set_isDone_3(BooleanU5BU5D_t3804927312* value)
	{
		___isDone_3 = value;
		Il2CppCodeGenWriteBarrier(&___isDone_3, value);
	}

	inline static int32_t get_offset_of_length_4() { return static_cast<int32_t>(offsetof(NthZipObserverBase_1_t3759589597, ___length_4)); }
	inline int32_t get_length_4() const { return ___length_4; }
	inline int32_t* get_address_of_length_4() { return &___length_4; }
	inline void set_length_4(int32_t value)
	{
		___length_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
