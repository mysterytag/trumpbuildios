﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Triggers.ObservableCollision2DTrigger
struct ObservableCollision2DTrigger_t2617106840;
// UnityEngine.Collision2D
struct Collision2D_t452810033;
// UniRx.IObservable`1<UnityEngine.Collision2D>
struct IObservable_1_t211608397;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collision2D452810033.h"

// System.Void UniRx.Triggers.ObservableCollision2DTrigger::.ctor()
extern "C"  void ObservableCollision2DTrigger__ctor_m3331965733 (ObservableCollision2DTrigger_t2617106840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableCollision2DTrigger::OnCollisionEnter2D(UnityEngine.Collision2D)
extern "C"  void ObservableCollision2DTrigger_OnCollisionEnter2D_m1569997071 (ObservableCollision2DTrigger_t2617106840 * __this, Collision2D_t452810033 * ___coll0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.Collision2D> UniRx.Triggers.ObservableCollision2DTrigger::OnCollisionEnter2DAsObservable()
extern "C"  Il2CppObject* ObservableCollision2DTrigger_OnCollisionEnter2DAsObservable_m2867204366 (ObservableCollision2DTrigger_t2617106840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableCollision2DTrigger::OnCollisionExit2D(UnityEngine.Collision2D)
extern "C"  void ObservableCollision2DTrigger_OnCollisionExit2D_m3452822399 (ObservableCollision2DTrigger_t2617106840 * __this, Collision2D_t452810033 * ___coll0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.Collision2D> UniRx.Triggers.ObservableCollision2DTrigger::OnCollisionExit2DAsObservable()
extern "C"  Il2CppObject* ObservableCollision2DTrigger_OnCollisionExit2DAsObservable_m2013407416 (ObservableCollision2DTrigger_t2617106840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableCollision2DTrigger::OnCollisionStay2D(UnityEngine.Collision2D)
extern "C"  void ObservableCollision2DTrigger_OnCollisionStay2D_m1500260708 (ObservableCollision2DTrigger_t2617106840 * __this, Collision2D_t452810033 * ___coll0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.Collision2D> UniRx.Triggers.ObservableCollision2DTrigger::OnCollisionStay2DAsObservable()
extern "C"  Il2CppObject* ObservableCollision2DTrigger_OnCollisionStay2DAsObservable_m1525981171 (ObservableCollision2DTrigger_t2617106840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableCollision2DTrigger::RaiseOnCompletedOnDestroy()
extern "C"  void ObservableCollision2DTrigger_RaiseOnCompletedOnDestroy_m4081751518 (ObservableCollision2DTrigger_t2617106840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
