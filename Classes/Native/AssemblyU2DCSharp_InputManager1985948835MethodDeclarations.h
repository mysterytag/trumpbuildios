﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InputManager
struct InputManager_t1985948835;
// InputEventDescriptor
struct InputEventDescriptor_t4263964831;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_InputEventDescriptor4263964831.h"

// System.Void InputManager::.ctor()
extern "C"  void InputManager__ctor_m4135406936 (InputManager_t1985948835 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputManager::PushEvent(InputEventDescriptor)
extern "C"  void InputManager_PushEvent_m3327321687 (InputManager_t1985948835 * __this, InputEventDescriptor_t4263964831 * ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// InputManager InputManager::get_instance()
extern "C"  InputManager_t1985948835 * InputManager_get_instance_m3038876836 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputManager::OnApplicationQuit()
extern "C"  void InputManager_OnApplicationQuit_m1980004054 (InputManager_t1985948835 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputManager::Awake()
extern "C"  void InputManager_Awake_m78044859 (InputManager_t1985948835 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputManager::Update()
extern "C"  void InputManager_Update_m1075458229 (InputManager_t1985948835 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
