﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// IStream`1<System.Object>
struct IStream_1_t1389797411;
// IStream`1<System.Object>[]
struct IStream_1U5BU5D_t3844023282;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StreamAPI/<MergeWith>c__AnonStorey139`1<System.Object>
struct  U3CMergeWithU3Ec__AnonStorey139_1_t1732385262  : public Il2CppObject
{
public:
	// IStream`1<T> StreamAPI/<MergeWith>c__AnonStorey139`1::stream
	Il2CppObject* ___stream_0;
	// IStream`1<T>[] StreamAPI/<MergeWith>c__AnonStorey139`1::others
	IStream_1U5BU5D_t3844023282* ___others_1;

public:
	inline static int32_t get_offset_of_stream_0() { return static_cast<int32_t>(offsetof(U3CMergeWithU3Ec__AnonStorey139_1_t1732385262, ___stream_0)); }
	inline Il2CppObject* get_stream_0() const { return ___stream_0; }
	inline Il2CppObject** get_address_of_stream_0() { return &___stream_0; }
	inline void set_stream_0(Il2CppObject* value)
	{
		___stream_0 = value;
		Il2CppCodeGenWriteBarrier(&___stream_0, value);
	}

	inline static int32_t get_offset_of_others_1() { return static_cast<int32_t>(offsetof(U3CMergeWithU3Ec__AnonStorey139_1_t1732385262, ___others_1)); }
	inline IStream_1U5BU5D_t3844023282* get_others_1() const { return ___others_1; }
	inline IStream_1U5BU5D_t3844023282** get_address_of_others_1() { return &___others_1; }
	inline void set_others_1(IStream_1U5BU5D_t3844023282* value)
	{
		___others_1 = value;
		Il2CppCodeGenWriteBarrier(&___others_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
