﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.LinkGoogleAccountRequest
struct LinkGoogleAccountRequest_t117459333;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.LinkGoogleAccountRequest::.ctor()
extern "C"  void LinkGoogleAccountRequest__ctor_m4000137816 (LinkGoogleAccountRequest_t117459333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LinkGoogleAccountRequest::get_AccessToken()
extern "C"  String_t* LinkGoogleAccountRequest_get_AccessToken_m2982258661 (LinkGoogleAccountRequest_t117459333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LinkGoogleAccountRequest::set_AccessToken(System.String)
extern "C"  void LinkGoogleAccountRequest_set_AccessToken_m3481145332 (LinkGoogleAccountRequest_t117459333 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
