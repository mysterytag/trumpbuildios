﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/LoginWithEmailAddressRequestCallback
struct LoginWithEmailAddressRequestCallback_t1473970925;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.LoginWithEmailAddressRequest
struct LoginWithEmailAddressRequest_t3586495128;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginWithEm3586495128.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/LoginWithEmailAddressRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void LoginWithEmailAddressRequestCallback__ctor_m529742172 (LoginWithEmailAddressRequestCallback_t1473970925 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LoginWithEmailAddressRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.LoginWithEmailAddressRequest,System.Object)
extern "C"  void LoginWithEmailAddressRequestCallback_Invoke_m1899958847 (LoginWithEmailAddressRequestCallback_t1473970925 * __this, String_t* ___urlPath0, int32_t ___callId1, LoginWithEmailAddressRequest_t3586495128 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_LoginWithEmailAddressRequestCallback_t1473970925(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, LoginWithEmailAddressRequest_t3586495128 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/LoginWithEmailAddressRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.LoginWithEmailAddressRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LoginWithEmailAddressRequestCallback_BeginInvoke_m2130428928 (LoginWithEmailAddressRequestCallback_t1473970925 * __this, String_t* ___urlPath0, int32_t ___callId1, LoginWithEmailAddressRequest_t3586495128 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LoginWithEmailAddressRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void LoginWithEmailAddressRequestCallback_EndInvoke_m3377576300 (LoginWithEmailAddressRequestCallback_t1473970925 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
