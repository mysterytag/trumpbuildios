﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// Stream`1/<Listen>c__AnonStorey128<System.Int64>
struct U3CListenU3Ec__AnonStorey128_t2078774906;
// Stream`1/<Listen>c__AnonStorey128<System.Object>
struct U3CListenU3Ec__AnonStorey128_t68466444;
// System.Object
struct Il2CppObject;
// Stream`1/<Listen>c__AnonStorey128<System.Single>
struct U3CListenU3Ec__AnonStorey128_t189569045;
// Stream`1/<Listen>c__AnonStorey128<UniRx.CollectionAddEvent`1<System.Object>>
struct U3CListenU3Ec__AnonStorey128_t1647882011;
// Stream`1/<Listen>c__AnonStorey128<UniRx.Unit>
struct U3CListenU3Ec__AnonStorey128_t1789646062;
// Stream`1<System.Boolean>
struct Stream_1_t3197111659;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<System.Boolean>
struct Action_1_t359458046;
// System.Action
struct Action_t437523947;
// IStream`1<System.Boolean>
struct IStream_1_t763696332;
// Stream`1<System.DateTime>
struct Stream_1_t3325140254;
// System.Action`1<System.DateTime>
struct Action_1_t487486641;
// IStream`1<System.DateTime>
struct IStream_1_t891724927;
// Stream`1<System.Double>
struct Stream_1_t3520622932;
// System.Action`1<System.Double>
struct Action_1_t682969319;
// IStream`1<System.Double>
struct IStream_1_t1087207605;
// Stream`1<System.Int32>
struct Stream_1_t1538553809;
// System.Action`1<System.Int32>
struct Action_1_t2995867492;
// IStream`1<System.Int32>
struct IStream_1_t3400105778;
// Stream`1<System.Int64>
struct Stream_1_t1538553904;
// System.Action`1<System.Int64>
struct Action_1_t2995867587;
// IStream`1<System.Int64>
struct IStream_1_t3400105873;
// Stream`1<System.Object>
struct Stream_1_t3823212738;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// IStream`1<System.Object>
struct IStream_1_t1389797411;
// Stream`1<System.Single>
struct Stream_1_t3944315339;
// System.Action`1<System.Single>
struct Action_1_t1106661726;
// IStream`1<System.Single>
struct IStream_1_t1510900012;
// Stream`1<UniRx.CollectionAddEvent`1<System.Object>>
struct Stream_1_t1107661009;
// System.Action`1<UniRx.CollectionAddEvent`1<System.Object>>
struct Action_1_t2564974692;
// IStream`1<UniRx.CollectionAddEvent`1<System.Object>>
struct IStream_1_t2969212978;
// Stream`1<UniRx.Unit>
struct Stream_1_t1249425060;
// System.Action`1<UniRx.Unit>
struct Action_1_t2706738743;
// IStream`1<UniRx.Unit>
struct IStream_1_t3110977029;
// StreamAPI/<Filter>c__AnonStorey134`1/<Filter>c__AnonStorey135`1<System.Object>
struct U3CFilterU3Ec__AnonStorey135_1_t999690120;
// StreamAPI/<Filter>c__AnonStorey134`1<System.Object>
struct U3CFilterU3Ec__AnonStorey134_1_t1444715119;
// StreamAPI/<FirstOnly>c__AnonStorey136`1/<FirstOnly>c__AnonStorey137`1<System.Object>
struct U3CFirstOnlyU3Ec__AnonStorey137_1_t4126677326;
// StreamAPI/<FirstOnly>c__AnonStorey136`1<System.Object>
struct U3CFirstOnlyU3Ec__AnonStorey136_1_t276735029;
// StreamAPI/<ListenQueue>c__AnonStorey12D`1<System.Object>
struct U3CListenQueueU3Ec__AnonStorey12D_1_t38028782;
// System.Collections.Generic.IEnumerable`1<System.Action`1<System.Object>>
struct IEnumerable_1_t3857713482;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t3709260776;
// StreamAPI/<Map>c__AnonStorey130`2/<Map>c__AnonStorey131`2<System.Object,System.Object>
struct U3CMapU3Ec__AnonStorey131_2_t3825613791;
// StreamAPI/<Map>c__AnonStorey130`2<System.Object,System.Object>
struct U3CMapU3Ec__AnonStorey130_2_t2728082812;
// StreamAPI/<MergeWith>c__AnonStorey139`1<System.Object>
struct U3CMergeWithU3Ec__AnonStorey139_1_t1732385262;
// StreamAPI/<ToEmpty>c__AnonStorey132`1/<ToEmpty>c__AnonStorey133`1<System.Object>
struct U3CToEmptyU3Ec__AnonStorey133_1_t2600382340;
// StreamAPI/<ToEmpty>c__AnonStorey132`1<System.Object>
struct U3CToEmptyU3Ec__AnonStorey132_1_t3045407339;
// StreamEgoizm`1/<DoThis>c__AnonStorey12B<System.Object>
struct U3CDoThisU3Ec__AnonStorey12B_t2640087060;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.IDisposable,System.Action`1<System.Object>>>
struct IEnumerable_1_t2192255843;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerable_1_t1890143508;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.IDisposable,System.Action`1<System.Object>>,System.Boolean>
struct Func_2_t3244071066;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Boolean>
struct Func_2_t1844407557;
// StreamEgoizm`1/<Listen>c__AnonStorey129<System.Object>
struct U3CListenU3Ec__AnonStorey129_t1735040613;
// StreamEgoizm`1/<Listen>c__AnonStorey12A<System.Object>
struct U3CListenU3Ec__AnonStorey12A_t2182732077;
// StreamEgoizm`1<System.Object>
struct StreamEgoizm_1_t3012409721;
// StreamQuery/<Join>c__AnonStorey13F`1/<Join>c__AnonStorey140`1<System.Object>
struct U3CJoinU3Ec__AnonStorey140_1_t1200685296;
// StreamQuery/<Join>c__AnonStorey13F`1<System.Object>
struct U3CJoinU3Ec__AnonStorey13F_1_t910942991;
// StreamQuery/<SelectMany>c__AnonStorey13C`2<System.Object,System.Object>
struct U3CSelectManyU3Ec__AnonStorey13C_2_t3738070224;
// StreamQuery/<SelectMany>c__AnonStorey13D`3/<SelectMany>c__AnonStorey13E`3<System.Object,System.Object,System.Object>
struct U3CSelectManyU3Ec__AnonStorey13E_3_t2432972599;
// StreamQuery/<SelectMany>c__AnonStorey13D`3<System.Object,System.Object,System.Object>
struct U3CSelectManyU3Ec__AnonStorey13D_3_t2765353606;
// System.Func`2<System.Object,System.Object>
struct Func_2_t2135783352;
// Substance`1<System.Object>
struct Substance_1_t3068566258;
// SubstanceBase`2/<Affect>c__AnonStorey143<System.Boolean,System.Boolean>
struct U3CAffectU3Ec__AnonStorey143_t390189525;
// SubstanceBase`2/<Affect>c__AnonStorey143<System.Double,System.Double>
struct U3CAffectU3Ec__AnonStorey143_t2252021777;
// SubstanceBase`2/<Affect>c__AnonStorey143<System.Object,System.Object>
struct U3CAffectU3Ec__AnonStorey143_t1517854361;
// SubstanceBase`2/<Affect>c__AnonStorey143<System.Single,System.Object>
struct U3CAffectU3Ec__AnonStorey143_t2725761868;
// SubstanceBase`2/<Affect>c__AnonStorey143<System.Single,System.Single>
struct U3CAffectU3Ec__AnonStorey143_t2846864469;
// SubstanceBase`2/<Affect>c__AnonStorey144<System.Boolean,System.Boolean>
struct U3CAffectU3Ec__AnonStorey144_t386862328;
// SubstanceBase`2/<Affect>c__AnonStorey144<System.Double,System.Double>
struct U3CAffectU3Ec__AnonStorey144_t2248694580;
// SubstanceBase`2/<Affect>c__AnonStorey144<System.Object,System.Object>
struct U3CAffectU3Ec__AnonStorey144_t1514527164;
// SubstanceBase`2/<Affect>c__AnonStorey144<System.Single,System.Object>
struct U3CAffectU3Ec__AnonStorey144_t2722434671;
// SubstanceBase`2/<Affect>c__AnonStorey144<System.Single,System.Single>
struct U3CAffectU3Ec__AnonStorey144_t2843537272;
// SubstanceBase`2/<OnChanged>c__AnonStorey142<System.Boolean,System.Boolean>
struct U3COnChangedU3Ec__AnonStorey142_t1335955226;
// SubstanceBase`2/<OnChanged>c__AnonStorey142<System.Double,System.Double>
struct U3COnChangedU3Ec__AnonStorey142_t3197787478;
// SubstanceBase`2/<OnChanged>c__AnonStorey142<System.Object,System.Object>
struct U3COnChangedU3Ec__AnonStorey142_t2463620062;
// SubstanceBase`2/<OnChanged>c__AnonStorey142<System.Single,System.Object>
struct U3COnChangedU3Ec__AnonStorey142_t3671527569;
// SubstanceBase`2/<OnChanged>c__AnonStorey142<System.Single,System.Single>
struct U3COnChangedU3Ec__AnonStorey142_t3792630170;
// SubstanceBase`2/DisposableAffect<System.Boolean,System.Boolean>
struct DisposableAffect_t2225548402;
// SubstanceBase`2/DisposableAffect<System.Double,System.Double>
struct DisposableAffect_t4087380654;
// SubstanceBase`2/DisposableAffect<System.Object,System.Object>
struct DisposableAffect_t3353213238;
// SubstanceBase`2/DisposableAffect<System.Single,System.Object>
struct DisposableAffect_t266153449;
// SubstanceBase`2/DisposableAffect<System.Single,System.Single>
struct DisposableAffect_t387256050;
// SubstanceBase`2/Intrusion<System.Boolean,System.Boolean>
struct Intrusion_t1917188776;
// SubstanceBase`2/Intrusion<System.Double,System.Double>
struct Intrusion_t3779021028;
// SubstanceBase`2/Intrusion<System.Object,System.Object>
struct Intrusion_t3044853612;
// SubstanceBase`2/Intrusion<System.Single,System.Object>
struct Intrusion_t4252761119;
// SubstanceBase`2/Intrusion<System.Single,System.Single>
struct Intrusion_t78896424;
// SubstanceBase`2<System.Boolean,System.Boolean>
struct SubstanceBase_2_t3707538316;
// ICell`1<System.Boolean>
struct ICell_1_t1762636318;
// ILiving
struct ILiving_t2639664210;
// IEmptyStream
struct IEmptyStream_t3684082468;
// SubstanceBase`2<System.Double,System.Double>
struct SubstanceBase_2_t1274403272;
// ICell`1<System.Double>
struct ICell_1_t2086147591;
// SubstanceBase`2<System.Object,System.Object>
struct SubstanceBase_2_t540235856;
// ICell`1<System.Object>
struct ICell_1_t2388737397;
// SubstanceBase`2<System.Single,System.Object>
struct SubstanceBase_2_t1748143363;
// ICell`1<System.Single>
struct ICell_1_t2509839998;
// SubstanceBase`2<System.Single,System.Single>
struct SubstanceBase_2_t1869245964;
// SubstanceCollection`1<System.Object>
struct SubstanceCollection_1_t1980381568;
// UniRx.InternalUtil.ImmutableList`1<System.Object>
struct ImmutableList_1_t1897310919;
// System.Action`1<GameAnalyticsSDK.Settings/HelpTypes>
struct Action_1_t3439808249;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;
// System.Action`1<GooglePlayGames.BasicApi.Nearby.AdvertisingResult>
struct Action_1_t3869889887;
// System.Action`1<GooglePlayGames.BasicApi.Nearby.ConnectionRequest>
struct Action_1_t3665980111;
// System.Action`1<GooglePlayGames.BasicApi.Nearby.ConnectionResponse>
struct Action_1_t1697901181;
// System.Action`1<GooglePlayGames.BasicApi.Nearby.InitializationStatus>
struct Action_1_t805070320;
// System.Action`1<GooglePlayGames.BasicApi.UIStatus>
struct Action_1_t284412649;
// System.Action`1<LitJson.PropertyMetadata>
struct Action_1_t4090926794;
// System.Action`1<ModestTree.Util.MouseWheelScrollDirections>
struct Action_1_t3075561458;
// System.Action`1<PlayFab.Internal.GMFB_327/testRegion>
struct Action_1_t1833392007;
// System.Action`1<System.Byte>
struct Action_1_t2927146526;
// System.Action`1<System.Char>
struct Action_1_t2927159404;
// System.Action`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Action_1_t3461409153;
// System.Action`1<System.DateTimeOffset>
struct Action_1_t3860712740;
// System.Action`1<System.Reflection.CustomAttributeNamedArgument>
struct Action_1_t467187834;
// System.Action`1<System.Reflection.CustomAttributeTypedArgument>
struct Action_1_t708868267;
// System.Action`1<System.TimeSpan>
struct Action_1_t912315597;
// System.Action`1<System.UInt64>
struct Action_1_t1134378126;
// System.Action`1<UniRx.Tuple`2<System.Object,System.Object>>
struct Action_1_t517714524;
// System.Action`1<UniRx.Tuple`3<System.Object,System.Object,System.Object>>
struct Action_1_t1799351807;
// System.Action`1<UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>>
struct Action_1_t1175682986;
// System.Action`1<UnityEngine.Color>
struct Action_1_t1736628465;
// System.Action`1<UnityEngine.Color32>
struct Action_1_t4285536912;
// System.Action`1<UnityEngine.EventSystems.RaycastResult>
struct Action_1_t1108351394;
// System.Action`1<UnityEngine.Quaternion>
struct Action_1_t2040168684;
// System.Action`1<UnityEngine.Rect>
struct Action_1_t1673881522;
// System.Action`1<UnityEngine.UICharInfo>
struct Action_1_t552273286;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharp_Stream_1_U3CListenU3Ec__AnonStor2078774906.h"
#include "AssemblyU2DCSharp_Stream_1_U3CListenU3Ec__AnonStor2078774906MethodDeclarations.h"
#include "mscorlib_System_Void2779279689.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "mscorlib_System_Int642847414882.h"
#include "System_Core_System_Action437523947MethodDeclarations.h"
#include "System_Core_System_Action437523947.h"
#include "AssemblyU2DCSharp_Stream_1_U3CListenU3Ec__AnonStorey68466444.h"
#include "AssemblyU2DCSharp_Stream_1_U3CListenU3Ec__AnonStorey68466444MethodDeclarations.h"
#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharp_Stream_1_U3CListenU3Ec__AnonStore189569045.h"
#include "AssemblyU2DCSharp_Stream_1_U3CListenU3Ec__AnonStore189569045MethodDeclarations.h"
#include "mscorlib_System_Single958209021.h"
#include "AssemblyU2DCSharp_Stream_1_U3CListenU3Ec__AnonStor1647882011.h"
#include "AssemblyU2DCSharp_Stream_1_U3CListenU3Ec__AnonStor1647882011MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionAddE2416521987.h"
#include "AssemblyU2DCSharp_Stream_1_U3CListenU3Ec__AnonStor1789646062.h"
#include "AssemblyU2DCSharp_Stream_1_U3CListenU3Ec__AnonStor1789646062MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "AssemblyU2DCSharp_Stream_1_gen3197111659.h"
#include "AssemblyU2DCSharp_Stream_1_gen3197111659MethodDeclarations.h"
#include "AssemblyU2DCSharp_Reactor_1_gen3765777533.h"
#include "AssemblyU2DCSharp_Reactor_1_gen3765777533MethodDeclarations.h"
#include "mscorlib_System_Boolean211005341.h"
#include "UnityEngine_UnityEngine_Debug1588791936MethodDeclarations.h"
#include "AssemblyU2DCSharp_Transaction3809114814MethodDeclarations.h"
#include "mscorlib_System_Int322847414787.h"
#include "AssemblyU2DCSharp_Transaction3809114814.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1007964310.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1007964310MethodDeclarations.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"
#include "mscorlib_System_Action_1_gen359458046.h"
#include "AssemblyU2DCSharp_Stream_1_U3CListenU3Ec__AnonStor3737332661.h"
#include "AssemblyU2DCSharp_Stream_1_U3CListenU3Ec__AnonStor3737332661MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen359458046MethodDeclarations.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3388714598.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3388714598MethodDeclarations.h"
#include "AssemblyU2DCSharp_Stream_1_gen3325140254.h"
#include "AssemblyU2DCSharp_Stream_1_gen3325140254MethodDeclarations.h"
#include "AssemblyU2DCSharp_Reactor_1_gen3893806128.h"
#include "AssemblyU2DCSharp_Reactor_1_gen3893806128MethodDeclarations.h"
#include "mscorlib_System_DateTime339033936.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1135992905.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1135992905MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen487486641.h"
#include "AssemblyU2DCSharp_Stream_1_U3CListenU3Ec__AnonStor3865361256.h"
#include "AssemblyU2DCSharp_Stream_1_U3CListenU3Ec__AnonStor3865361256MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen487486641MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3516743193.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3516743193MethodDeclarations.h"
#include "AssemblyU2DCSharp_Stream_1_gen3520622932.h"
#include "AssemblyU2DCSharp_Stream_1_gen3520622932MethodDeclarations.h"
#include "AssemblyU2DCSharp_Reactor_1_gen4089288806.h"
#include "AssemblyU2DCSharp_Reactor_1_gen4089288806MethodDeclarations.h"
#include "mscorlib_System_Double534516614.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1331475583.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1331475583MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen682969319.h"
#include "AssemblyU2DCSharp_Stream_1_U3CListenU3Ec__AnonStor4060843934.h"
#include "AssemblyU2DCSharp_Stream_1_U3CListenU3Ec__AnonStor4060843934MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen682969319MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3712225871.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3712225871MethodDeclarations.h"
#include "AssemblyU2DCSharp_Stream_1_gen1538553809.h"
#include "AssemblyU2DCSharp_Stream_1_gen1538553809MethodDeclarations.h"
#include "AssemblyU2DCSharp_Reactor_1_gen2107219683.h"
#include "AssemblyU2DCSharp_Reactor_1_gen2107219683MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3644373756.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3644373756MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2995867492.h"
#include "AssemblyU2DCSharp_Stream_1_U3CListenU3Ec__AnonStor2078774811.h"
#include "AssemblyU2DCSharp_Stream_1_U3CListenU3Ec__AnonStor2078774811MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2995867492MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1730156748.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1730156748MethodDeclarations.h"
#include "AssemblyU2DCSharp_Stream_1_gen1538553904.h"
#include "AssemblyU2DCSharp_Stream_1_gen1538553904MethodDeclarations.h"
#include "AssemblyU2DCSharp_Reactor_1_gen2107219778.h"
#include "AssemblyU2DCSharp_Reactor_1_gen2107219778MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3644373851.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3644373851MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2995867587.h"
#include "mscorlib_System_Action_1_gen2995867587MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1730156843.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1730156843MethodDeclarations.h"
#include "AssemblyU2DCSharp_Stream_1_gen3823212738.h"
#include "AssemblyU2DCSharp_Stream_1_gen3823212738MethodDeclarations.h"
#include "AssemblyU2DCSharp_Reactor_1_gen96911316.h"
#include "AssemblyU2DCSharp_Reactor_1_gen96911316MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1634065389.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1634065389MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen985559125.h"
#include "mscorlib_System_Action_1_gen985559125MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4014815677.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4014815677MethodDeclarations.h"
#include "AssemblyU2DCSharp_Stream_1_gen3944315339.h"
#include "AssemblyU2DCSharp_Stream_1_gen3944315339MethodDeclarations.h"
#include "AssemblyU2DCSharp_Reactor_1_gen218013917.h"
#include "AssemblyU2DCSharp_Reactor_1_gen218013917MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1755167990.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1755167990MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen1106661726.h"
#include "mscorlib_System_Action_1_gen1106661726MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4135918278.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4135918278MethodDeclarations.h"
#include "AssemblyU2DCSharp_Stream_1_gen1107661009.h"
#include "AssemblyU2DCSharp_Stream_1_gen1107661009MethodDeclarations.h"
#include "AssemblyU2DCSharp_Reactor_1_gen1676326883.h"
#include "AssemblyU2DCSharp_Reactor_1_gen1676326883MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3213480956.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3213480956MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2564974692.h"
#include "mscorlib_System_Action_1_gen2564974692MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1299263948.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1299263948MethodDeclarations.h"
#include "AssemblyU2DCSharp_Stream_1_gen1249425060.h"
#include "AssemblyU2DCSharp_Stream_1_gen1249425060MethodDeclarations.h"
#include "AssemblyU2DCSharp_Reactor_1_gen1818090934.h"
#include "AssemblyU2DCSharp_Reactor_1_gen1818090934MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3355245007.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3355245007MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2706738743.h"
#include "mscorlib_System_Action_1_gen2706738743MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1441027999.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1441027999MethodDeclarations.h"
#include "AssemblyU2DCSharp_StreamAPI_U3CFilterU3Ec__AnonStor999690120.h"
#include "AssemblyU2DCSharp_StreamAPI_U3CFilterU3Ec__AnonStor999690120MethodDeclarations.h"
#include "AssemblyU2DCSharp_StreamAPI_U3CFilterU3Ec__AnonSto1444715119.h"
#include "System_Core_System_Func_2_gen1509682273.h"
#include "System_Core_System_Func_2_gen1509682273MethodDeclarations.h"
#include "AssemblyU2DCSharp_StreamAPI_U3CFilterU3Ec__AnonSto1444715119MethodDeclarations.h"
#include "AssemblyU2DCSharp_StreamAPI_U3CFirstOnlyU3Ec__Anon4126677326.h"
#include "AssemblyU2DCSharp_StreamAPI_U3CFirstOnlyU3Ec__Anon4126677326MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellUtils_SingleAssignmentDispos1832432170.h"
#include "AssemblyU2DCSharp_CellUtils_SingleAssignmentDispos1832432170MethodDeclarations.h"
#include "AssemblyU2DCSharp_StreamAPI_U3CFirstOnlyU3Ec__AnonS276735029.h"
#include "AssemblyU2DCSharp_StreamAPI_U3CFirstOnlyU3Ec__AnonS276735029MethodDeclarations.h"
#include "AssemblyU2DCSharp_StreamAPI_U3CListenQueueU3Ec__Anon38028782.h"
#include "AssemblyU2DCSharp_StreamAPI_U3CListenQueueU3Ec__Anon38028782MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable4285654829MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable4285654829.h"
#include "AssemblyU2DCSharp_StreamAPI_U3CMapU3Ec__AnonStorey3825613791.h"
#include "AssemblyU2DCSharp_StreamAPI_U3CMapU3Ec__AnonStorey3825613791MethodDeclarations.h"
#include "AssemblyU2DCSharp_StreamAPI_U3CMapU3Ec__AnonStorey2728082812.h"
#include "System_Core_System_Func_2_gen2135783352.h"
#include "System_Core_System_Func_2_gen2135783352MethodDeclarations.h"
#include "AssemblyU2DCSharp_StreamAPI_U3CMapU3Ec__AnonStorey2728082812MethodDeclarations.h"
#include "AssemblyU2DCSharp_StreamAPI_U3CMergeWithU3Ec__Anon1732385262.h"
#include "AssemblyU2DCSharp_StreamAPI_U3CMergeWithU3Ec__Anon1732385262MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellUtils_ListDisposable2393830995MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellUtils_ListDisposable2393830995.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include "AssemblyU2DCSharp_StreamAPI_U3CToEmptyU3Ec__AnonSt2600382340.h"
#include "AssemblyU2DCSharp_StreamAPI_U3CToEmptyU3Ec__AnonSt2600382340MethodDeclarations.h"
#include "AssemblyU2DCSharp_StreamAPI_U3CToEmptyU3Ec__AnonSt3045407339.h"
#include "AssemblyU2DCSharp_StreamAPI_U3CToEmptyU3Ec__AnonSt3045407339MethodDeclarations.h"
#include "AssemblyU2DCSharp_StreamEgoizm_1_U3CDoThisU3Ec__An2640087060.h"
#include "AssemblyU2DCSharp_StreamEgoizm_1_U3CDoThisU3Ec__An2640087060MethodDeclarations.h"
#include "AssemblyU2DCSharp_StreamEgoizm_1_gen3012409721.h"
#include "System_System_Collections_Generic_Stack_1_gen1890507522.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23615068783.h"
#include "System_System_Collections_Generic_Stack_1_gen1890507522MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23312956448.h"
#include "System_Core_System_Func_2_gen3244071066.h"
#include "System_Core_System_Func_2_gen3244071066MethodDeclarations.h"
#include "AssemblyU2DCSharp_StreamEgoizm_1_U3CListenU3Ec__An1735040613.h"
#include "AssemblyU2DCSharp_StreamEgoizm_1_U3CListenU3Ec__An1735040613MethodDeclarations.h"
#include "AssemblyU2DCSharp_StreamEgoizm_1_U3CListenU3Ec__An2182732077.h"
#include "AssemblyU2DCSharp_StreamEgoizm_1_U3CListenU3Ec__An2182732077MethodDeclarations.h"
#include "AssemblyU2DCSharp_StreamEgoizm_1_gen3012409721MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CompositeDispo1894629977MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Disposable3736388786MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CompositeDispo1894629977.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23615068783MethodDeclarations.h"
#include "AssemblyU2DCSharp_StreamQuery_U3CJoinU3Ec__AnonSto1200685296.h"
#include "AssemblyU2DCSharp_StreamQuery_U3CJoinU3Ec__AnonSto1200685296MethodDeclarations.h"
#include "AssemblyU2DCSharp_StreamQuery_U3CJoinU3Ec__AnonStor910942991.h"
#include "AssemblyU2DCSharp_StreamQuery_U3CJoinU3Ec__AnonStor910942991MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen1538250116.h"
#include "mscorlib_System_Action_1_gen1538250116MethodDeclarations.h"
#include "AssemblyU2DCSharp_StreamQuery_U3CSelectManyU3Ec__A3738070224.h"
#include "AssemblyU2DCSharp_StreamQuery_U3CSelectManyU3Ec__A3738070224MethodDeclarations.h"
#include "AssemblyU2DCSharp_StreamQuery_U3CSelectManyU3Ec__A2432972599.h"
#include "AssemblyU2DCSharp_StreamQuery_U3CSelectManyU3Ec__A2432972599MethodDeclarations.h"
#include "AssemblyU2DCSharp_StreamQuery_U3CSelectManyU3Ec__A2765353606.h"
#include "System_Core_System_Func_3_gen1892209229.h"
#include "System_Core_System_Func_3_gen1892209229MethodDeclarations.h"
#include "AssemblyU2DCSharp_StreamQuery_U3CSelectManyU3Ec__A2765353606MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2688474343.h"
#include "System_Core_System_Func_2_gen2688474343MethodDeclarations.h"
#include "AssemblyU2DCSharp_StreamQuery3282448648MethodDeclarations.h"
#include "AssemblyU2DCSharp_StreamQuery3282448648.h"
#include "AssemblyU2DCSharp_Substance_1_gen3068566258.h"
#include "AssemblyU2DCSharp_Substance_1_gen3068566258MethodDeclarations.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_gen1838912788MethodDeclarations.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_Intrusion_gen48563248.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3226272505.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_gen1838912788.h"
#include "mscorlib_System_Collections_Generic_List_1_gen845522217.h"
#include "mscorlib_System_Collections_Generic_List_1_gen845522217MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3226272505MethodDeclarations.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_U3CAffectU3Ec__An390189525.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_U3CAffectU3Ec__An390189525MethodDeclarations.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_Intrusion_gen1917188776.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_gen3707538316.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_gen3707538316MethodDeclarations.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_U3CAffectU3Ec__A2252021777.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_U3CAffectU3Ec__A2252021777MethodDeclarations.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_Intrusion_gen3779021028.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_gen1274403272.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_gen1274403272MethodDeclarations.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_U3CAffectU3Ec__A1517854361.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_U3CAffectU3Ec__A1517854361MethodDeclarations.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_Intrusion_gen3044853612.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_gen540235856.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_gen540235856MethodDeclarations.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_U3CAffectU3Ec__A2725761868.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_U3CAffectU3Ec__A2725761868MethodDeclarations.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_Intrusion_gen4252761119.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_gen1748143363.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_gen1748143363MethodDeclarations.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_U3CAffectU3Ec__A2846864469.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_U3CAffectU3Ec__A2846864469MethodDeclarations.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_Intrusion_gen78896424.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_gen1869245964.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_gen1869245964MethodDeclarations.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_U3CAffectU3Ec__An386862328.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_U3CAffectU3Ec__An386862328MethodDeclarations.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_U3CAffectU3Ec__A2248694580.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_U3CAffectU3Ec__A2248694580MethodDeclarations.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_U3CAffectU3Ec__A1514527164.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_U3CAffectU3Ec__A1514527164MethodDeclarations.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_U3CAffectU3Ec__A2722434671.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_U3CAffectU3Ec__A2722434671MethodDeclarations.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_U3CAffectU3Ec__A2843537272.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_U3CAffectU3Ec__A2843537272MethodDeclarations.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_U3COnChangedU3Ec1335955226.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_U3COnChangedU3Ec1335955226MethodDeclarations.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_U3COnChangedU3Ec3197787478.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_U3COnChangedU3Ec3197787478MethodDeclarations.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_U3COnChangedU3Ec2463620062.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_U3COnChangedU3Ec2463620062MethodDeclarations.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_U3COnChangedU3Ec3671527569.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_U3COnChangedU3Ec3671527569MethodDeclarations.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_U3COnChangedU3Ec3792630170.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_U3COnChangedU3Ec3792630170MethodDeclarations.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_DisposableAffect2225548402.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_DisposableAffect2225548402MethodDeclarations.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_DisposableAffect4087380654.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_DisposableAffect4087380654MethodDeclarations.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_DisposableAffect3353213238.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_DisposableAffect3353213238MethodDeclarations.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_DisposableAffect_266153449.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_DisposableAffect_266153449MethodDeclarations.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_DisposableAffect_387256050.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_DisposableAffect_387256050MethodDeclarations.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_Intrusion_gen1917188776MethodDeclarations.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_Intrusion_gen3779021028MethodDeclarations.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_Intrusion_gen3044853612MethodDeclarations.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_Intrusion_gen4252761119MethodDeclarations.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_Intrusion_gen78896424MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2714147745.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2714147745MethodDeclarations.h"
#include "AssemblyU2DCSharp_TagCollector573861427MethodDeclarations.h"
#include "AssemblyU2DCSharp_TagCollector573861427.h"
#include "mscorlib_System_Collections_Generic_List_1_gen281012701.h"
#include "mscorlib_System_Collections_Generic_List_1_gen281012701MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3841812581.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3841812581MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen754752792.h"
#include "mscorlib_System_Collections_Generic_List_1_gen754752792MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen875855393.h"
#include "mscorlib_System_Collections_Generic_List_1_gen875855393MethodDeclarations.h"
#include "AssemblyU2DCSharp_SubstanceCollection_1_gen1980381568.h"
#include "AssemblyU2DCSharp_SubstanceCollection_1_gen1980381568MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I1897310919.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I1897310919MethodDeclarations.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_gen3226437585MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_Intrusion_gen1436088045.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat318830006.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_gen3226437585.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2233047014.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2233047014MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat318830006MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3439808249.h"
#include "mscorlib_System_Action_1_gen3439808249MethodDeclarations.h"
#include "AssemblyU2DCSharp_GameAnalyticsSDK_Settings_HelpTy3291355544.h"
#include "mscorlib_System_AsyncCallback1363551830.h"
#include "mscorlib_System_Action_1_gen3869889887.h"
#include "mscorlib_System_Action_1_gen3869889887MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_3721437182.h"
#include "mscorlib_System_Action_1_gen3665980111.h"
#include "mscorlib_System_Action_1_gen3665980111MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_3517527406.h"
#include "mscorlib_System_Action_1_gen1697901181.h"
#include "mscorlib_System_Action_1_gen1697901181MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_1549448476.h"
#include "mscorlib_System_Action_1_gen805070320.h"
#include "mscorlib_System_Action_1_gen805070320MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_I656617615.h"
#include "mscorlib_System_Action_1_gen284412649.h"
#include "mscorlib_System_Action_1_gen284412649MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_UIStatus135959944.h"
#include "mscorlib_System_Action_1_gen4090926794.h"
#include "mscorlib_System_Action_1_gen4090926794MethodDeclarations.h"
#include "AssemblyU2DCSharp_LitJson_PropertyMetadata3942474089.h"
#include "mscorlib_System_Action_1_gen3075561458.h"
#include "mscorlib_System_Action_1_gen3075561458MethodDeclarations.h"
#include "AssemblyU2DCSharp_ModestTree_Util_MouseWheelScroll2927108753.h"
#include "mscorlib_System_Action_1_gen1833392007.h"
#include "mscorlib_System_Action_1_gen1833392007MethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayFab_Internal_GMFB_327_testRe1684939302.h"
#include "mscorlib_System_Action_1_gen2927146526.h"
#include "mscorlib_System_Action_1_gen2927146526MethodDeclarations.h"
#include "mscorlib_System_Byte2778693821.h"
#include "mscorlib_System_Action_1_gen2927159404.h"
#include "mscorlib_System_Action_1_gen2927159404MethodDeclarations.h"
#include "mscorlib_System_Char2778706699.h"
#include "mscorlib_System_Action_1_gen3461409153.h"
#include "mscorlib_System_Action_1_gen3461409153MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3860712740.h"
#include "mscorlib_System_Action_1_gen3860712740MethodDeclarations.h"
#include "mscorlib_System_DateTimeOffset3712260035.h"
#include "mscorlib_System_Action_1_gen467187834.h"
#include "mscorlib_System_Action_1_gen467187834MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgu318735129.h"
#include "mscorlib_System_Action_1_gen708868267.h"
#include "mscorlib_System_Action_1_gen708868267MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArgu560415562.h"
#include "mscorlib_System_Action_1_gen912315597.h"
#include "mscorlib_System_Action_1_gen912315597MethodDeclarations.h"
#include "mscorlib_System_TimeSpan763862892.h"
#include "mscorlib_System_Action_1_gen1134378126.h"
#include "mscorlib_System_Action_1_gen1134378126MethodDeclarations.h"
#include "mscorlib_System_UInt64985925421.h"
#include "mscorlib_System_Action_1_gen517714524.h"
#include "mscorlib_System_Action_1_gen517714524MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen369261819.h"
#include "mscorlib_System_Action_1_gen1799351807.h"
#include "mscorlib_System_Action_1_gen1799351807MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_3_gen1650899102.h"
#include "mscorlib_System_Action_1_gen1175682986.h"
#include "mscorlib_System_Action_1_gen1175682986MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_4_gen1027230281.h"
#include "mscorlib_System_Action_1_gen1736628465.h"
#include "mscorlib_System_Action_1_gen1736628465MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"
#include "mscorlib_System_Action_1_gen4285536912.h"
#include "mscorlib_System_Action_1_gen4285536912MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color324137084207.h"
#include "mscorlib_System_Action_1_gen1108351394.h"
#include "mscorlib_System_Action_1_gen1108351394MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResu959898689.h"
#include "mscorlib_System_Action_1_gen2040168684.h"
#include "mscorlib_System_Action_1_gen2040168684MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979.h"
#include "mscorlib_System_Action_1_gen1673881522.h"
#include "mscorlib_System_Action_1_gen1673881522MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect1525428817.h"
#include "mscorlib_System_Action_1_gen552273286.h"
#include "mscorlib_System_Action_1_gen552273286MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UICharInfo403820581.h"

// !!0 System.Linq.Enumerable::Last<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  Il2CppObject * Enumerable_Last_TisIl2CppObject_m1543967606_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_Last_TisIl2CppObject_m1543967606(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_Last_TisIl2CppObject_m1543967606_gshared)(__this /* static, unused */, p0, method)
// !!0 System.Linq.Enumerable::Last<System.Action`1<System.Object>>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_Last_TisAction_1_t985559125_m1335602390(__this /* static, unused */, p0, method) ((  Action_1_t985559125 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_Last_TisIl2CppObject_m1543967606_gshared)(__this /* static, unused */, p0, method)
// !!0 System.Linq.Enumerable::First<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  KeyValuePair_2_t3312956448  Enumerable_First_TisKeyValuePair_2_t3312956448_m1563542323_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_First_TisKeyValuePair_2_t3312956448_m1563542323(__this /* static, unused */, p0, method) ((  KeyValuePair_2_t3312956448  (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_First_TisKeyValuePair_2_t3312956448_m1563542323_gshared)(__this /* static, unused */, p0, method)
// !!0 System.Linq.Enumerable::First<System.Collections.Generic.KeyValuePair`2<System.IDisposable,System.Action`1<System.Object>>>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_First_TisKeyValuePair_2_t3615068783_m3404206207(__this /* static, unused */, p0, method) ((  KeyValuePair_2_t3615068783  (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_First_TisKeyValuePair_2_t3312956448_m1563542323_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C"  Il2CppObject* Enumerable_Where_TisKeyValuePair_2_t3312956448_m1955579279_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t1844407557 * p1, const MethodInfo* method);
#define Enumerable_Where_TisKeyValuePair_2_t3312956448_m1955579279(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1844407557 *, const MethodInfo*))Enumerable_Where_TisKeyValuePair_2_t3312956448_m1955579279_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<System.Collections.Generic.KeyValuePair`2<System.IDisposable,System.Action`1<System.Object>>>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Enumerable_Where_TisKeyValuePair_2_t3615068783_m1857675611(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t3244071066 *, const MethodInfo*))Enumerable_Where_TisKeyValuePair_2_t3312956448_m1955579279_gshared)(__this /* static, unused */, p0, p1, method)
// System.Boolean System.Linq.Enumerable::Any<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  bool Enumerable_Any_TisKeyValuePair_2_t3312956448_m4180880971_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_Any_TisKeyValuePair_2_t3312956448_m4180880971(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_Any_TisKeyValuePair_2_t3312956448_m4180880971_gshared)(__this /* static, unused */, p0, method)
// System.Boolean System.Linq.Enumerable::Any<System.Collections.Generic.KeyValuePair`2<System.IDisposable,System.Action`1<System.Object>>>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_Any_TisKeyValuePair_2_t3615068783_m1448971380(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_Any_TisKeyValuePair_2_t3312956448_m4180880971_gshared)(__this /* static, unused */, p0, method)
// IStream`1<!!1> StreamQuery::Select<System.Object,System.Object>(IStream`1<!!0>,System.Func`2<!!0,!!1>)
extern "C"  Il2CppObject* StreamQuery_Select_TisIl2CppObject_TisIl2CppObject_m2396847683_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t2135783352 * p1, const MethodInfo* method);
#define StreamQuery_Select_TisIl2CppObject_TisIl2CppObject_m2396847683(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t2135783352 *, const MethodInfo*))StreamQuery_Select_TisIl2CppObject_TisIl2CppObject_m2396847683_gshared)(__this /* static, unused */, p0, p1, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Stream`1/<Listen>c__AnonStorey128<System.Int64>::.ctor()
extern "C"  void U3CListenU3Ec__AnonStorey128__ctor_m481776974_gshared (U3CListenU3Ec__AnonStorey128_t2078774906 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Stream`1/<Listen>c__AnonStorey128<System.Int64>::<>m__1B9(T)
extern "C"  void U3CListenU3Ec__AnonStorey128_U3CU3Em__1B9_m151167339_gshared (U3CListenU3Ec__AnonStorey128_t2078774906 * __this, int64_t ____0, const MethodInfo* method)
{
	{
		Action_t437523947 * L_0 = (Action_t437523947 *)__this->get_action_0();
		NullCheck((Action_t437523947 *)L_0);
		Action_Invoke_m1445970038((Action_t437523947 *)L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Stream`1/<Listen>c__AnonStorey128<System.Object>::.ctor()
extern "C"  void U3CListenU3Ec__AnonStorey128__ctor_m4125250920_gshared (U3CListenU3Ec__AnonStorey128_t68466444 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Stream`1/<Listen>c__AnonStorey128<System.Object>::<>m__1B9(T)
extern "C"  void U3CListenU3Ec__AnonStorey128_U3CU3Em__1B9_m1444676741_gshared (U3CListenU3Ec__AnonStorey128_t68466444 * __this, Il2CppObject * ____0, const MethodInfo* method)
{
	{
		Action_t437523947 * L_0 = (Action_t437523947 *)__this->get_action_0();
		NullCheck((Action_t437523947 *)L_0);
		Action_Invoke_m1445970038((Action_t437523947 *)L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Stream`1/<Listen>c__AnonStorey128<System.Single>::.ctor()
extern "C"  void U3CListenU3Ec__AnonStorey128__ctor_m3783212849_gshared (U3CListenU3Ec__AnonStorey128_t189569045 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Stream`1/<Listen>c__AnonStorey128<System.Single>::<>m__1B9(T)
extern "C"  void U3CListenU3Ec__AnonStorey128_U3CU3Em__1B9_m4063027662_gshared (U3CListenU3Ec__AnonStorey128_t189569045 * __this, float ____0, const MethodInfo* method)
{
	{
		Action_t437523947 * L_0 = (Action_t437523947 *)__this->get_action_0();
		NullCheck((Action_t437523947 *)L_0);
		Action_Invoke_m1445970038((Action_t437523947 *)L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Stream`1/<Listen>c__AnonStorey128<UniRx.CollectionAddEvent`1<System.Object>>::.ctor()
extern "C"  void U3CListenU3Ec__AnonStorey128__ctor_m1700916422_gshared (U3CListenU3Ec__AnonStorey128_t1647882011 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Stream`1/<Listen>c__AnonStorey128<UniRx.CollectionAddEvent`1<System.Object>>::<>m__1B9(T)
extern "C"  void U3CListenU3Ec__AnonStorey128_U3CU3Em__1B9_m1126481123_gshared (U3CListenU3Ec__AnonStorey128_t1647882011 * __this, CollectionAddEvent_1_t2416521987  ____0, const MethodInfo* method)
{
	{
		Action_t437523947 * L_0 = (Action_t437523947 *)__this->get_action_0();
		NullCheck((Action_t437523947 *)L_0);
		Action_Invoke_m1445970038((Action_t437523947 *)L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Stream`1/<Listen>c__AnonStorey128<UniRx.Unit>::.ctor()
extern "C"  void U3CListenU3Ec__AnonStorey128__ctor_m858460540_gshared (U3CListenU3Ec__AnonStorey128_t1789646062 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Stream`1/<Listen>c__AnonStorey128<UniRx.Unit>::<>m__1B9(T)
extern "C"  void U3CListenU3Ec__AnonStorey128_U3CU3Em__1B9_m163616409_gshared (U3CListenU3Ec__AnonStorey128_t1789646062 * __this, Unit_t2558286038  ____0, const MethodInfo* method)
{
	{
		Action_t437523947 * L_0 = (Action_t437523947 *)__this->get_action_0();
		NullCheck((Action_t437523947 *)L_0);
		Action_Invoke_m1445970038((Action_t437523947 *)L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Stream`1<System.Boolean>::.ctor()
extern "C"  void Stream_1__ctor_m3459184177_gshared (Stream_1_t3197111659 * __this, const MethodInfo* method)
{
	{
		Reactor_1_t3765777533 * L_0 = (Reactor_1_t3765777533 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (Reactor_1_t3765777533 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->set_observer_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Stream`1<System.Boolean>::Send(T)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppClass* Transaction_t3809114814_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5960200;
extern const uint32_t Stream_1_Send_m1988401731_MetadataUsageId;
extern "C"  void Stream_1_Send_m1988401731_gshared (Stream_1_t3197111659 * __this, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Stream_1_Send_m1988401731_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
		__this->set_inputStreamConnection_1((Il2CppObject *)NULL);
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_2 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_calculationMode_8();
		if (!L_2)
		{
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m1836388681(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral5960200, /*hidden argument*/NULL);
	}

IL_0031:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_3 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_performing_5();
		if (!L_3)
		{
			goto IL_0056;
		}
	}
	{
		V_0 = (int32_t)0;
		goto IL_004e;
	}

IL_0042:
	{
		bool L_4 = ___value0;
		int32_t L_5 = V_0;
		NullCheck((Stream_1_t3197111659 *)__this);
		((  void (*) (Stream_1_t3197111659 *, bool, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Stream_1_t3197111659 *)__this, (bool)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		int32_t L_6 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_004e:
	{
		int32_t L_7 = V_0;
		if ((((int32_t)L_7) < ((int32_t)3)))
		{
			goto IL_0042;
		}
	}
	{
		return;
	}

IL_0056:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_8 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_hold_3();
		if (!L_8)
		{
			goto IL_0089;
		}
	}
	{
		List_1_t1007964310 * L_9 = (List_1_t1007964310 *)__this->get_holdedValues_2();
		if (L_9)
		{
			goto IL_0076;
		}
	}
	{
		List_1_t1007964310 * L_10 = (List_1_t1007964310 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (List_1_t1007964310 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		__this->set_holdedValues_2(L_10);
	}

IL_0076:
	{
		List_1_t1007964310 * L_11 = (List_1_t1007964310 *)__this->get_holdedValues_2();
		bool L_12 = ___value0;
		NullCheck((List_1_t1007964310 *)L_11);
		VirtActionInvoker1< bool >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Boolean>::Add(!0) */, (List_1_t1007964310 *)L_11, (bool)L_12);
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		Transaction_RegisterPackedReaction_m1156829996(NULL /*static, unused*/, (Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}

IL_0089:
	{
		Reactor_1_t3765777533 * L_13 = (Reactor_1_t3765777533 *)__this->get_observer_0();
		bool L_14 = ___value0;
		NullCheck((Reactor_1_t3765777533 *)L_13);
		((  void (*) (Reactor_1_t3765777533 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Reactor_1_t3765777533 *)L_13, (bool)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		bool L_15 = (bool)__this->get_autoDisconnectAfterEvent_4();
		if (!L_15)
		{
			goto IL_00ab;
		}
	}
	{
		Reactor_1_t3765777533 * L_16 = (Reactor_1_t3765777533 *)__this->get_observer_0();
		NullCheck((Reactor_1_t3765777533 *)L_16);
		((  void (*) (Reactor_1_t3765777533 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((Reactor_1_t3765777533 *)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
	}

IL_00ab:
	{
		return;
	}
}
// System.Void Stream`1<System.Boolean>::SendInTransaction(T,System.Int32)
extern "C"  void Stream_1_SendInTransaction_m800469017_gshared (Stream_1_t3197111659 * __this, bool ___value0, int32_t ___p1, const MethodInfo* method)
{
	{
		Reactor_1_t3765777533 * L_0 = (Reactor_1_t3765777533 *)__this->get_observer_0();
		bool L_1 = ___value0;
		int32_t L_2 = ___p1;
		NullCheck((Reactor_1_t3765777533 *)L_0);
		((  void (*) (Reactor_1_t3765777533 *, bool, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((Reactor_1_t3765777533 *)L_0, (bool)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return;
	}
}
// System.IDisposable Stream`1<System.Boolean>::Listen(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Stream_1_Listen_m1071073365_gshared (Stream_1_t3197111659 * __this, Action_1_t359458046 * ___action0, int32_t ___priority1, const MethodInfo* method)
{
	{
		Reactor_1_t3765777533 * L_0 = (Reactor_1_t3765777533 *)__this->get_observer_0();
		Action_1_t359458046 * L_1 = ___action0;
		int32_t L_2 = ___priority1;
		NullCheck((Reactor_1_t3765777533 *)L_0);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Reactor_1_t3765777533 *, Action_1_t359458046 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Reactor_1_t3765777533 *)L_0, (Action_1_t359458046 *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		return L_3;
	}
}
// System.IDisposable Stream`1<System.Boolean>::Listen(System.Action,Priority)
extern "C"  Il2CppObject * Stream_1_Listen_m3921221554_gshared (Stream_1_t3197111659 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method)
{
	U3CListenU3Ec__AnonStorey128_t3737332661 * V_0 = NULL;
	{
		U3CListenU3Ec__AnonStorey128_t3737332661 * L_0 = (U3CListenU3Ec__AnonStorey128_t3737332661 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (U3CListenU3Ec__AnonStorey128_t3737332661 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		V_0 = (U3CListenU3Ec__AnonStorey128_t3737332661 *)L_0;
		U3CListenU3Ec__AnonStorey128_t3737332661 * L_1 = V_0;
		Action_t437523947 * L_2 = ___action0;
		NullCheck(L_1);
		L_1->set_action_0(L_2);
		Reactor_1_t3765777533 * L_3 = (Reactor_1_t3765777533 *)__this->get_observer_0();
		U3CListenU3Ec__AnonStorey128_t3737332661 * L_4 = V_0;
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		Action_1_t359458046 * L_6 = (Action_1_t359458046 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		((  void (*) (Action_1_t359458046 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)(L_6, (Il2CppObject *)L_4, (IntPtr_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		int32_t L_7 = ___p1;
		NullCheck((Reactor_1_t3765777533 *)L_3);
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (Reactor_1_t3765777533 *, Action_1_t359458046 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Reactor_1_t3765777533 *)L_3, (Action_1_t359458046 *)L_6, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		return L_8;
	}
}
// System.Void Stream`1<System.Boolean>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Stream_1_Dispose_m4164557358_MetadataUsageId;
extern "C"  void Stream_1_Dispose_m4164557358_gshared (Stream_1_t3197111659 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Stream_1_Dispose_m4164557358_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_observer_0((Reactor_1_t3765777533 *)NULL);
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
	}

IL_001d:
	{
		__this->set_inputStreamConnection_1((Il2CppObject *)NULL);
		return;
	}
}
// System.Void Stream`1<System.Boolean>::SetInputStream(IStream`1<T>)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Stream_1_SetInputStream_m1717621359_MetadataUsageId;
extern "C"  void Stream_1_SetInputStream_m1717621359_gshared (Stream_1_t3197111659 * __this, Il2CppObject* ___stream0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Stream_1_SetInputStream_m1717621359_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = ___stream0;
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		Action_1_t359458046 * L_4 = (Action_1_t359458046 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		((  void (*) (Action_1_t359458046 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)(L_4, (Il2CppObject *)__this, (IntPtr_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_5 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t359458046 *, int32_t >::Invoke(0 /* System.IDisposable IStream`1<System.Boolean>::Listen(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16), (Il2CppObject*)L_2, (Action_1_t359458046 *)L_4, (int32_t)1);
		__this->set_inputStreamConnection_1(L_5);
		return;
	}
}
// System.Void Stream`1<System.Boolean>::ClearInputStream()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Stream_1_ClearInputStream_m1684075024_MetadataUsageId;
extern "C"  void Stream_1_ClearInputStream_m1684075024_gshared (Stream_1_t3197111659 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Stream_1_ClearInputStream_m1684075024_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
	}

IL_0016:
	{
		__this->set_inputStreamConnection_1((Il2CppObject *)NULL);
		return;
	}
}
// System.Void Stream`1<System.Boolean>::TransactionIterationFinished()
extern "C"  void Stream_1_TransactionIterationFinished_m3659143652_gshared (Stream_1_t3197111659 * __this, const MethodInfo* method)
{
	{
		__this->set_holdedValues_2((List_1_t1007964310 *)NULL);
		return;
	}
}
// System.Void Stream`1<System.Boolean>::Unpack(System.Int32)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Stream_1_Unpack_m3770242550_MetadataUsageId;
extern "C"  void Stream_1_Unpack_m3770242550_gshared (Stream_1_t3197111659 * __this, int32_t ___p0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Stream_1_Unpack_m3770242550_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Enumerator_t3388714598  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t1007964310 * L_0 = (List_1_t1007964310 *)__this->get_holdedValues_2();
		NullCheck((List_1_t1007964310 *)L_0);
		Enumerator_t3388714598  L_1 = ((  Enumerator_t3388714598  (*) (List_1_t1007964310 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((List_1_t1007964310 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		V_1 = (Enumerator_t3388714598 )L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0026;
		}

IL_0011:
		{
			bool L_2 = ((  bool (*) (Enumerator_t3388714598 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->method)((Enumerator_t3388714598 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
			V_0 = (bool)L_2;
			Reactor_1_t3765777533 * L_3 = (Reactor_1_t3765777533 *)__this->get_observer_0();
			bool L_4 = V_0;
			int32_t L_5 = ___p0;
			NullCheck((Reactor_1_t3765777533 *)L_3);
			((  void (*) (Reactor_1_t3765777533 *, bool, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((Reactor_1_t3765777533 *)L_3, (bool)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		}

IL_0026:
		{
			bool L_6 = ((  bool (*) (Enumerator_t3388714598 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19)->method)((Enumerator_t3388714598 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
			if (L_6)
			{
				goto IL_0011;
			}
		}

IL_0032:
		{
			IL2CPP_LEAVE(0x43, FINALLY_0037);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0037;
	}

FINALLY_0037:
	{ // begin finally (depth: 1)
		Enumerator_t3388714598  L_7 = V_1;
		Enumerator_t3388714598  L_8 = L_7;
		Il2CppObject * L_9 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20), &L_8);
		NullCheck((Il2CppObject *)L_9);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_9);
		IL2CPP_END_FINALLY(55)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(55)
	{
		IL2CPP_JUMP_TBL(0x43, IL_0043)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0043:
	{
		return;
	}
}
// System.Void Stream`1<System.Boolean>::<SetInputStream>m__1BA(T)
extern "C"  void Stream_1_U3CSetInputStreamU3Em__1BA_m541567630_gshared (Stream_1_t3197111659 * __this, bool ___val0, const MethodInfo* method)
{
	{
		Reactor_1_t3765777533 * L_0 = (Reactor_1_t3765777533 *)__this->get_observer_0();
		bool L_1 = ___val0;
		NullCheck((Reactor_1_t3765777533 *)L_0);
		((  void (*) (Reactor_1_t3765777533 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Reactor_1_t3765777533 *)L_0, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return;
	}
}
// System.Void Stream`1<System.DateTime>::.ctor()
extern "C"  void Stream_1__ctor_m3308497500_gshared (Stream_1_t3325140254 * __this, const MethodInfo* method)
{
	{
		Reactor_1_t3893806128 * L_0 = (Reactor_1_t3893806128 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (Reactor_1_t3893806128 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->set_observer_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Stream`1<System.DateTime>::Send(T)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppClass* Transaction_t3809114814_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5960200;
extern const uint32_t Stream_1_Send_m1837715054_MetadataUsageId;
extern "C"  void Stream_1_Send_m1837715054_gshared (Stream_1_t3325140254 * __this, DateTime_t339033936  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Stream_1_Send_m1837715054_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
		__this->set_inputStreamConnection_1((Il2CppObject *)NULL);
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_2 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_calculationMode_8();
		if (!L_2)
		{
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m1836388681(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral5960200, /*hidden argument*/NULL);
	}

IL_0031:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_3 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_performing_5();
		if (!L_3)
		{
			goto IL_0056;
		}
	}
	{
		V_0 = (int32_t)0;
		goto IL_004e;
	}

IL_0042:
	{
		DateTime_t339033936  L_4 = ___value0;
		int32_t L_5 = V_0;
		NullCheck((Stream_1_t3325140254 *)__this);
		((  void (*) (Stream_1_t3325140254 *, DateTime_t339033936 , int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Stream_1_t3325140254 *)__this, (DateTime_t339033936 )L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		int32_t L_6 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_004e:
	{
		int32_t L_7 = V_0;
		if ((((int32_t)L_7) < ((int32_t)3)))
		{
			goto IL_0042;
		}
	}
	{
		return;
	}

IL_0056:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_8 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_hold_3();
		if (!L_8)
		{
			goto IL_0089;
		}
	}
	{
		List_1_t1135992905 * L_9 = (List_1_t1135992905 *)__this->get_holdedValues_2();
		if (L_9)
		{
			goto IL_0076;
		}
	}
	{
		List_1_t1135992905 * L_10 = (List_1_t1135992905 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (List_1_t1135992905 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		__this->set_holdedValues_2(L_10);
	}

IL_0076:
	{
		List_1_t1135992905 * L_11 = (List_1_t1135992905 *)__this->get_holdedValues_2();
		DateTime_t339033936  L_12 = ___value0;
		NullCheck((List_1_t1135992905 *)L_11);
		VirtActionInvoker1< DateTime_t339033936  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.DateTime>::Add(!0) */, (List_1_t1135992905 *)L_11, (DateTime_t339033936 )L_12);
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		Transaction_RegisterPackedReaction_m1156829996(NULL /*static, unused*/, (Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}

IL_0089:
	{
		Reactor_1_t3893806128 * L_13 = (Reactor_1_t3893806128 *)__this->get_observer_0();
		DateTime_t339033936  L_14 = ___value0;
		NullCheck((Reactor_1_t3893806128 *)L_13);
		((  void (*) (Reactor_1_t3893806128 *, DateTime_t339033936 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Reactor_1_t3893806128 *)L_13, (DateTime_t339033936 )L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		bool L_15 = (bool)__this->get_autoDisconnectAfterEvent_4();
		if (!L_15)
		{
			goto IL_00ab;
		}
	}
	{
		Reactor_1_t3893806128 * L_16 = (Reactor_1_t3893806128 *)__this->get_observer_0();
		NullCheck((Reactor_1_t3893806128 *)L_16);
		((  void (*) (Reactor_1_t3893806128 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((Reactor_1_t3893806128 *)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
	}

IL_00ab:
	{
		return;
	}
}
// System.Void Stream`1<System.DateTime>::SendInTransaction(T,System.Int32)
extern "C"  void Stream_1_SendInTransaction_m1243591812_gshared (Stream_1_t3325140254 * __this, DateTime_t339033936  ___value0, int32_t ___p1, const MethodInfo* method)
{
	{
		Reactor_1_t3893806128 * L_0 = (Reactor_1_t3893806128 *)__this->get_observer_0();
		DateTime_t339033936  L_1 = ___value0;
		int32_t L_2 = ___p1;
		NullCheck((Reactor_1_t3893806128 *)L_0);
		((  void (*) (Reactor_1_t3893806128 *, DateTime_t339033936 , int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((Reactor_1_t3893806128 *)L_0, (DateTime_t339033936 )L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return;
	}
}
// System.IDisposable Stream`1<System.DateTime>::Listen(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Stream_1_Listen_m970467974_gshared (Stream_1_t3325140254 * __this, Action_1_t487486641 * ___action0, int32_t ___priority1, const MethodInfo* method)
{
	{
		Reactor_1_t3893806128 * L_0 = (Reactor_1_t3893806128 *)__this->get_observer_0();
		Action_1_t487486641 * L_1 = ___action0;
		int32_t L_2 = ___priority1;
		NullCheck((Reactor_1_t3893806128 *)L_0);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Reactor_1_t3893806128 *, Action_1_t487486641 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Reactor_1_t3893806128 *)L_0, (Action_1_t487486641 *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		return L_3;
	}
}
// System.IDisposable Stream`1<System.DateTime>::Listen(System.Action,Priority)
extern "C"  Il2CppObject * Stream_1_Listen_m2976766689_gshared (Stream_1_t3325140254 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method)
{
	U3CListenU3Ec__AnonStorey128_t3865361256 * V_0 = NULL;
	{
		U3CListenU3Ec__AnonStorey128_t3865361256 * L_0 = (U3CListenU3Ec__AnonStorey128_t3865361256 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (U3CListenU3Ec__AnonStorey128_t3865361256 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		V_0 = (U3CListenU3Ec__AnonStorey128_t3865361256 *)L_0;
		U3CListenU3Ec__AnonStorey128_t3865361256 * L_1 = V_0;
		Action_t437523947 * L_2 = ___action0;
		NullCheck(L_1);
		L_1->set_action_0(L_2);
		Reactor_1_t3893806128 * L_3 = (Reactor_1_t3893806128 *)__this->get_observer_0();
		U3CListenU3Ec__AnonStorey128_t3865361256 * L_4 = V_0;
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		Action_1_t487486641 * L_6 = (Action_1_t487486641 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		((  void (*) (Action_1_t487486641 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)(L_6, (Il2CppObject *)L_4, (IntPtr_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		int32_t L_7 = ___p1;
		NullCheck((Reactor_1_t3893806128 *)L_3);
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (Reactor_1_t3893806128 *, Action_1_t487486641 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Reactor_1_t3893806128 *)L_3, (Action_1_t487486641 *)L_6, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		return L_8;
	}
}
// System.Void Stream`1<System.DateTime>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Stream_1_Dispose_m1088581529_MetadataUsageId;
extern "C"  void Stream_1_Dispose_m1088581529_gshared (Stream_1_t3325140254 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Stream_1_Dispose_m1088581529_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_observer_0((Reactor_1_t3893806128 *)NULL);
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
	}

IL_001d:
	{
		__this->set_inputStreamConnection_1((Il2CppObject *)NULL);
		return;
	}
}
// System.Void Stream`1<System.DateTime>::SetInputStream(IStream`1<T>)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Stream_1_SetInputStream_m1502279460_MetadataUsageId;
extern "C"  void Stream_1_SetInputStream_m1502279460_gshared (Stream_1_t3325140254 * __this, Il2CppObject* ___stream0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Stream_1_SetInputStream_m1502279460_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = ___stream0;
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		Action_1_t487486641 * L_4 = (Action_1_t487486641 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		((  void (*) (Action_1_t487486641 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)(L_4, (Il2CppObject *)__this, (IntPtr_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_5 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t487486641 *, int32_t >::Invoke(0 /* System.IDisposable IStream`1<System.DateTime>::Listen(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16), (Il2CppObject*)L_2, (Action_1_t487486641 *)L_4, (int32_t)1);
		__this->set_inputStreamConnection_1(L_5);
		return;
	}
}
// System.Void Stream`1<System.DateTime>::ClearInputStream()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Stream_1_ClearInputStream_m3589965061_MetadataUsageId;
extern "C"  void Stream_1_ClearInputStream_m3589965061_gshared (Stream_1_t3325140254 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Stream_1_ClearInputStream_m3589965061_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
	}

IL_0016:
	{
		__this->set_inputStreamConnection_1((Il2CppObject *)NULL);
		return;
	}
}
// System.Void Stream`1<System.DateTime>::TransactionIterationFinished()
extern "C"  void Stream_1_TransactionIterationFinished_m2874008921_gshared (Stream_1_t3325140254 * __this, const MethodInfo* method)
{
	{
		__this->set_holdedValues_2((List_1_t1135992905 *)NULL);
		return;
	}
}
// System.Void Stream`1<System.DateTime>::Unpack(System.Int32)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Stream_1_Unpack_m1379532715_MetadataUsageId;
extern "C"  void Stream_1_Unpack_m1379532715_gshared (Stream_1_t3325140254 * __this, int32_t ___p0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Stream_1_Unpack_m1379532715_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t339033936  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Enumerator_t3516743193  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t1135992905 * L_0 = (List_1_t1135992905 *)__this->get_holdedValues_2();
		NullCheck((List_1_t1135992905 *)L_0);
		Enumerator_t3516743193  L_1 = ((  Enumerator_t3516743193  (*) (List_1_t1135992905 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((List_1_t1135992905 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		V_1 = (Enumerator_t3516743193 )L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0026;
		}

IL_0011:
		{
			DateTime_t339033936  L_2 = ((  DateTime_t339033936  (*) (Enumerator_t3516743193 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->method)((Enumerator_t3516743193 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
			V_0 = (DateTime_t339033936 )L_2;
			Reactor_1_t3893806128 * L_3 = (Reactor_1_t3893806128 *)__this->get_observer_0();
			DateTime_t339033936  L_4 = V_0;
			int32_t L_5 = ___p0;
			NullCheck((Reactor_1_t3893806128 *)L_3);
			((  void (*) (Reactor_1_t3893806128 *, DateTime_t339033936 , int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((Reactor_1_t3893806128 *)L_3, (DateTime_t339033936 )L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		}

IL_0026:
		{
			bool L_6 = ((  bool (*) (Enumerator_t3516743193 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19)->method)((Enumerator_t3516743193 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
			if (L_6)
			{
				goto IL_0011;
			}
		}

IL_0032:
		{
			IL2CPP_LEAVE(0x43, FINALLY_0037);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0037;
	}

FINALLY_0037:
	{ // begin finally (depth: 1)
		Enumerator_t3516743193  L_7 = V_1;
		Enumerator_t3516743193  L_8 = L_7;
		Il2CppObject * L_9 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20), &L_8);
		NullCheck((Il2CppObject *)L_9);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_9);
		IL2CPP_END_FINALLY(55)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(55)
	{
		IL2CPP_JUMP_TBL(0x43, IL_0043)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0043:
	{
		return;
	}
}
// System.Void Stream`1<System.DateTime>::<SetInputStream>m__1BA(T)
extern "C"  void Stream_1_U3CSetInputStreamU3Em__1BA_m916834809_gshared (Stream_1_t3325140254 * __this, DateTime_t339033936  ___val0, const MethodInfo* method)
{
	{
		Reactor_1_t3893806128 * L_0 = (Reactor_1_t3893806128 *)__this->get_observer_0();
		DateTime_t339033936  L_1 = ___val0;
		NullCheck((Reactor_1_t3893806128 *)L_0);
		((  void (*) (Reactor_1_t3893806128 *, DateTime_t339033936 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Reactor_1_t3893806128 *)L_0, (DateTime_t339033936 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return;
	}
}
// System.Void Stream`1<System.Double>::.ctor()
extern "C"  void Stream_1__ctor_m3438877202_gshared (Stream_1_t3520622932 * __this, const MethodInfo* method)
{
	{
		Reactor_1_t4089288806 * L_0 = (Reactor_1_t4089288806 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (Reactor_1_t4089288806 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->set_observer_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Stream`1<System.Double>::Send(T)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppClass* Transaction_t3809114814_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5960200;
extern const uint32_t Stream_1_Send_m1968094756_MetadataUsageId;
extern "C"  void Stream_1_Send_m1968094756_gshared (Stream_1_t3520622932 * __this, double ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Stream_1_Send_m1968094756_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
		__this->set_inputStreamConnection_1((Il2CppObject *)NULL);
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_2 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_calculationMode_8();
		if (!L_2)
		{
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m1836388681(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral5960200, /*hidden argument*/NULL);
	}

IL_0031:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_3 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_performing_5();
		if (!L_3)
		{
			goto IL_0056;
		}
	}
	{
		V_0 = (int32_t)0;
		goto IL_004e;
	}

IL_0042:
	{
		double L_4 = ___value0;
		int32_t L_5 = V_0;
		NullCheck((Stream_1_t3520622932 *)__this);
		((  void (*) (Stream_1_t3520622932 *, double, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Stream_1_t3520622932 *)__this, (double)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		int32_t L_6 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_004e:
	{
		int32_t L_7 = V_0;
		if ((((int32_t)L_7) < ((int32_t)3)))
		{
			goto IL_0042;
		}
	}
	{
		return;
	}

IL_0056:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_8 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_hold_3();
		if (!L_8)
		{
			goto IL_0089;
		}
	}
	{
		List_1_t1331475583 * L_9 = (List_1_t1331475583 *)__this->get_holdedValues_2();
		if (L_9)
		{
			goto IL_0076;
		}
	}
	{
		List_1_t1331475583 * L_10 = (List_1_t1331475583 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (List_1_t1331475583 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		__this->set_holdedValues_2(L_10);
	}

IL_0076:
	{
		List_1_t1331475583 * L_11 = (List_1_t1331475583 *)__this->get_holdedValues_2();
		double L_12 = ___value0;
		NullCheck((List_1_t1331475583 *)L_11);
		VirtActionInvoker1< double >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Double>::Add(!0) */, (List_1_t1331475583 *)L_11, (double)L_12);
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		Transaction_RegisterPackedReaction_m1156829996(NULL /*static, unused*/, (Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}

IL_0089:
	{
		Reactor_1_t4089288806 * L_13 = (Reactor_1_t4089288806 *)__this->get_observer_0();
		double L_14 = ___value0;
		NullCheck((Reactor_1_t4089288806 *)L_13);
		((  void (*) (Reactor_1_t4089288806 *, double, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Reactor_1_t4089288806 *)L_13, (double)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		bool L_15 = (bool)__this->get_autoDisconnectAfterEvent_4();
		if (!L_15)
		{
			goto IL_00ab;
		}
	}
	{
		Reactor_1_t4089288806 * L_16 = (Reactor_1_t4089288806 *)__this->get_observer_0();
		NullCheck((Reactor_1_t4089288806 *)L_16);
		((  void (*) (Reactor_1_t4089288806 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((Reactor_1_t4089288806 *)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
	}

IL_00ab:
	{
		return;
	}
}
// System.Void Stream`1<System.Double>::SendInTransaction(T,System.Int32)
extern "C"  void Stream_1_SendInTransaction_m1535790266_gshared (Stream_1_t3520622932 * __this, double ___value0, int32_t ___p1, const MethodInfo* method)
{
	{
		Reactor_1_t4089288806 * L_0 = (Reactor_1_t4089288806 *)__this->get_observer_0();
		double L_1 = ___value0;
		int32_t L_2 = ___p1;
		NullCheck((Reactor_1_t4089288806 *)L_0);
		((  void (*) (Reactor_1_t4089288806 *, double, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((Reactor_1_t4089288806 *)L_0, (double)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return;
	}
}
// System.IDisposable Stream`1<System.Double>::Listen(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Stream_1_Listen_m1840002556_gshared (Stream_1_t3520622932 * __this, Action_1_t682969319 * ___action0, int32_t ___priority1, const MethodInfo* method)
{
	{
		Reactor_1_t4089288806 * L_0 = (Reactor_1_t4089288806 *)__this->get_observer_0();
		Action_1_t682969319 * L_1 = ___action0;
		int32_t L_2 = ___priority1;
		NullCheck((Reactor_1_t4089288806 *)L_0);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Reactor_1_t4089288806 *, Action_1_t682969319 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Reactor_1_t4089288806 *)L_0, (Action_1_t682969319 *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		return L_3;
	}
}
// System.IDisposable Stream`1<System.Double>::Listen(System.Action,Priority)
extern "C"  Il2CppObject * Stream_1_Listen_m1368522155_gshared (Stream_1_t3520622932 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method)
{
	U3CListenU3Ec__AnonStorey128_t4060843934 * V_0 = NULL;
	{
		U3CListenU3Ec__AnonStorey128_t4060843934 * L_0 = (U3CListenU3Ec__AnonStorey128_t4060843934 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (U3CListenU3Ec__AnonStorey128_t4060843934 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		V_0 = (U3CListenU3Ec__AnonStorey128_t4060843934 *)L_0;
		U3CListenU3Ec__AnonStorey128_t4060843934 * L_1 = V_0;
		Action_t437523947 * L_2 = ___action0;
		NullCheck(L_1);
		L_1->set_action_0(L_2);
		Reactor_1_t4089288806 * L_3 = (Reactor_1_t4089288806 *)__this->get_observer_0();
		U3CListenU3Ec__AnonStorey128_t4060843934 * L_4 = V_0;
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		Action_1_t682969319 * L_6 = (Action_1_t682969319 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		((  void (*) (Action_1_t682969319 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)(L_6, (Il2CppObject *)L_4, (IntPtr_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		int32_t L_7 = ___p1;
		NullCheck((Reactor_1_t4089288806 *)L_3);
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (Reactor_1_t4089288806 *, Action_1_t682969319 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Reactor_1_t4089288806 *)L_3, (Action_1_t682969319 *)L_6, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		return L_8;
	}
}
// System.Void Stream`1<System.Double>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Stream_1_Dispose_m1829423567_MetadataUsageId;
extern "C"  void Stream_1_Dispose_m1829423567_gshared (Stream_1_t3520622932 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Stream_1_Dispose_m1829423567_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_observer_0((Reactor_1_t4089288806 *)NULL);
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
	}

IL_001d:
	{
		__this->set_inputStreamConnection_1((Il2CppObject *)NULL);
		return;
	}
}
// System.Void Stream`1<System.Double>::SetInputStream(IStream`1<T>)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Stream_1_SetInputStream_m3991115054_MetadataUsageId;
extern "C"  void Stream_1_SetInputStream_m3991115054_gshared (Stream_1_t3520622932 * __this, Il2CppObject* ___stream0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Stream_1_SetInputStream_m3991115054_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = ___stream0;
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		Action_1_t682969319 * L_4 = (Action_1_t682969319 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		((  void (*) (Action_1_t682969319 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)(L_4, (Il2CppObject *)__this, (IntPtr_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_5 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t682969319 *, int32_t >::Invoke(0 /* System.IDisposable IStream`1<System.Double>::Listen(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16), (Il2CppObject*)L_2, (Action_1_t682969319 *)L_4, (int32_t)1);
		__this->set_inputStreamConnection_1(L_5);
		return;
	}
}
// System.Void Stream`1<System.Double>::ClearInputStream()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Stream_1_ClearInputStream_m806700943_MetadataUsageId;
extern "C"  void Stream_1_ClearInputStream_m806700943_gshared (Stream_1_t3520622932 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Stream_1_ClearInputStream_m806700943_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
	}

IL_0016:
	{
		__this->set_inputStreamConnection_1((Il2CppObject *)NULL);
		return;
	}
}
// System.Void Stream`1<System.Double>::TransactionIterationFinished()
extern "C"  void Stream_1_TransactionIterationFinished_m2348230883_gshared (Stream_1_t3520622932 * __this, const MethodInfo* method)
{
	{
		__this->set_holdedValues_2((List_1_t1331475583 *)NULL);
		return;
	}
}
// System.Void Stream`1<System.Double>::Unpack(System.Int32)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Stream_1_Unpack_m2427340725_MetadataUsageId;
extern "C"  void Stream_1_Unpack_m2427340725_gshared (Stream_1_t3520622932 * __this, int32_t ___p0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Stream_1_Unpack_m2427340725_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	double V_0 = 0.0;
	Enumerator_t3712225871  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t1331475583 * L_0 = (List_1_t1331475583 *)__this->get_holdedValues_2();
		NullCheck((List_1_t1331475583 *)L_0);
		Enumerator_t3712225871  L_1 = ((  Enumerator_t3712225871  (*) (List_1_t1331475583 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((List_1_t1331475583 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		V_1 = (Enumerator_t3712225871 )L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0026;
		}

IL_0011:
		{
			double L_2 = ((  double (*) (Enumerator_t3712225871 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->method)((Enumerator_t3712225871 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
			V_0 = (double)L_2;
			Reactor_1_t4089288806 * L_3 = (Reactor_1_t4089288806 *)__this->get_observer_0();
			double L_4 = V_0;
			int32_t L_5 = ___p0;
			NullCheck((Reactor_1_t4089288806 *)L_3);
			((  void (*) (Reactor_1_t4089288806 *, double, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((Reactor_1_t4089288806 *)L_3, (double)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		}

IL_0026:
		{
			bool L_6 = ((  bool (*) (Enumerator_t3712225871 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19)->method)((Enumerator_t3712225871 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
			if (L_6)
			{
				goto IL_0011;
			}
		}

IL_0032:
		{
			IL2CPP_LEAVE(0x43, FINALLY_0037);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0037;
	}

FINALLY_0037:
	{ // begin finally (depth: 1)
		Enumerator_t3712225871  L_7 = V_1;
		Enumerator_t3712225871  L_8 = L_7;
		Il2CppObject * L_9 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20), &L_8);
		NullCheck((Il2CppObject *)L_9);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_9);
		IL2CPP_END_FINALLY(55)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(55)
	{
		IL2CPP_JUMP_TBL(0x43, IL_0043)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0043:
	{
		return;
	}
}
// System.Void Stream`1<System.Double>::<SetInputStream>m__1BA(T)
extern "C"  void Stream_1_U3CSetInputStreamU3Em__1BA_m3467573295_gshared (Stream_1_t3520622932 * __this, double ___val0, const MethodInfo* method)
{
	{
		Reactor_1_t4089288806 * L_0 = (Reactor_1_t4089288806 *)__this->get_observer_0();
		double L_1 = ___val0;
		NullCheck((Reactor_1_t4089288806 *)L_0);
		((  void (*) (Reactor_1_t4089288806 *, double, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Reactor_1_t4089288806 *)L_0, (double)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return;
	}
}
// System.Void Stream`1<System.Int32>::.ctor()
extern "C"  void Stream_1__ctor_m2512605719_gshared (Stream_1_t1538553809 * __this, const MethodInfo* method)
{
	{
		Reactor_1_t2107219683 * L_0 = (Reactor_1_t2107219683 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (Reactor_1_t2107219683 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->set_observer_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Stream`1<System.Int32>::Send(T)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppClass* Transaction_t3809114814_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5960200;
extern const uint32_t Stream_1_Send_m1041823273_MetadataUsageId;
extern "C"  void Stream_1_Send_m1041823273_gshared (Stream_1_t1538553809 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Stream_1_Send_m1041823273_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
		__this->set_inputStreamConnection_1((Il2CppObject *)NULL);
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_2 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_calculationMode_8();
		if (!L_2)
		{
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m1836388681(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral5960200, /*hidden argument*/NULL);
	}

IL_0031:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_3 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_performing_5();
		if (!L_3)
		{
			goto IL_0056;
		}
	}
	{
		V_0 = (int32_t)0;
		goto IL_004e;
	}

IL_0042:
	{
		int32_t L_4 = ___value0;
		int32_t L_5 = V_0;
		NullCheck((Stream_1_t1538553809 *)__this);
		((  void (*) (Stream_1_t1538553809 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Stream_1_t1538553809 *)__this, (int32_t)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		int32_t L_6 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_004e:
	{
		int32_t L_7 = V_0;
		if ((((int32_t)L_7) < ((int32_t)3)))
		{
			goto IL_0042;
		}
	}
	{
		return;
	}

IL_0056:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_8 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_hold_3();
		if (!L_8)
		{
			goto IL_0089;
		}
	}
	{
		List_1_t3644373756 * L_9 = (List_1_t3644373756 *)__this->get_holdedValues_2();
		if (L_9)
		{
			goto IL_0076;
		}
	}
	{
		List_1_t3644373756 * L_10 = (List_1_t3644373756 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (List_1_t3644373756 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		__this->set_holdedValues_2(L_10);
	}

IL_0076:
	{
		List_1_t3644373756 * L_11 = (List_1_t3644373756 *)__this->get_holdedValues_2();
		int32_t L_12 = ___value0;
		NullCheck((List_1_t3644373756 *)L_11);
		VirtActionInvoker1< int32_t >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0) */, (List_1_t3644373756 *)L_11, (int32_t)L_12);
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		Transaction_RegisterPackedReaction_m1156829996(NULL /*static, unused*/, (Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}

IL_0089:
	{
		Reactor_1_t2107219683 * L_13 = (Reactor_1_t2107219683 *)__this->get_observer_0();
		int32_t L_14 = ___value0;
		NullCheck((Reactor_1_t2107219683 *)L_13);
		((  void (*) (Reactor_1_t2107219683 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Reactor_1_t2107219683 *)L_13, (int32_t)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		bool L_15 = (bool)__this->get_autoDisconnectAfterEvent_4();
		if (!L_15)
		{
			goto IL_00ab;
		}
	}
	{
		Reactor_1_t2107219683 * L_16 = (Reactor_1_t2107219683 *)__this->get_observer_0();
		NullCheck((Reactor_1_t2107219683 *)L_16);
		((  void (*) (Reactor_1_t2107219683 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((Reactor_1_t2107219683 *)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
	}

IL_00ab:
	{
		return;
	}
}
// System.Void Stream`1<System.Int32>::SendInTransaction(T,System.Int32)
extern "C"  void Stream_1_SendInTransaction_m907270271_gshared (Stream_1_t1538553809 * __this, int32_t ___value0, int32_t ___p1, const MethodInfo* method)
{
	{
		Reactor_1_t2107219683 * L_0 = (Reactor_1_t2107219683 *)__this->get_observer_0();
		int32_t L_1 = ___value0;
		int32_t L_2 = ___p1;
		NullCheck((Reactor_1_t2107219683 *)L_0);
		((  void (*) (Reactor_1_t2107219683 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((Reactor_1_t2107219683 *)L_0, (int32_t)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return;
	}
}
// System.IDisposable Stream`1<System.Int32>::Listen(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Stream_1_Listen_m3789926011_gshared (Stream_1_t1538553809 * __this, Action_1_t2995867492 * ___action0, int32_t ___priority1, const MethodInfo* method)
{
	{
		Reactor_1_t2107219683 * L_0 = (Reactor_1_t2107219683 *)__this->get_observer_0();
		Action_1_t2995867492 * L_1 = ___action0;
		int32_t L_2 = ___priority1;
		NullCheck((Reactor_1_t2107219683 *)L_0);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Reactor_1_t2107219683 *, Action_1_t2995867492 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Reactor_1_t2107219683 *)L_0, (Action_1_t2995867492 *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		return L_3;
	}
}
// System.IDisposable Stream`1<System.Int32>::Listen(System.Action,Priority)
extern "C"  Il2CppObject * Stream_1_Listen_m1424028108_gshared (Stream_1_t1538553809 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method)
{
	U3CListenU3Ec__AnonStorey128_t2078774811 * V_0 = NULL;
	{
		U3CListenU3Ec__AnonStorey128_t2078774811 * L_0 = (U3CListenU3Ec__AnonStorey128_t2078774811 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (U3CListenU3Ec__AnonStorey128_t2078774811 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		V_0 = (U3CListenU3Ec__AnonStorey128_t2078774811 *)L_0;
		U3CListenU3Ec__AnonStorey128_t2078774811 * L_1 = V_0;
		Action_t437523947 * L_2 = ___action0;
		NullCheck(L_1);
		L_1->set_action_0(L_2);
		Reactor_1_t2107219683 * L_3 = (Reactor_1_t2107219683 *)__this->get_observer_0();
		U3CListenU3Ec__AnonStorey128_t2078774811 * L_4 = V_0;
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		Action_1_t2995867492 * L_6 = (Action_1_t2995867492 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		((  void (*) (Action_1_t2995867492 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)(L_6, (Il2CppObject *)L_4, (IntPtr_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		int32_t L_7 = ___p1;
		NullCheck((Reactor_1_t2107219683 *)L_3);
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (Reactor_1_t2107219683 *, Action_1_t2995867492 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Reactor_1_t2107219683 *)L_3, (Action_1_t2995867492 *)L_6, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		return L_8;
	}
}
// System.Void Stream`1<System.Int32>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Stream_1_Dispose_m740758676_MetadataUsageId;
extern "C"  void Stream_1_Dispose_m740758676_gshared (Stream_1_t1538553809 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Stream_1_Dispose_m740758676_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_observer_0((Reactor_1_t2107219683 *)NULL);
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
	}

IL_001d:
	{
		__this->set_inputStreamConnection_1((Il2CppObject *)NULL);
		return;
	}
}
// System.Void Stream`1<System.Int32>::SetInputStream(IStream`1<T>)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Stream_1_SetInputStream_m1221132873_MetadataUsageId;
extern "C"  void Stream_1_SetInputStream_m1221132873_gshared (Stream_1_t1538553809 * __this, Il2CppObject* ___stream0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Stream_1_SetInputStream_m1221132873_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = ___stream0;
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		Action_1_t2995867492 * L_4 = (Action_1_t2995867492 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		((  void (*) (Action_1_t2995867492 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)(L_4, (Il2CppObject *)__this, (IntPtr_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_5 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t2995867492 *, int32_t >::Invoke(0 /* System.IDisposable IStream`1<System.Int32>::Listen(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16), (Il2CppObject*)L_2, (Action_1_t2995867492 *)L_4, (int32_t)1);
		__this->set_inputStreamConnection_1(L_5);
		return;
	}
}
// System.Void Stream`1<System.Int32>::ClearInputStream()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Stream_1_ClearInputStream_m3887090282_MetadataUsageId;
extern "C"  void Stream_1_ClearInputStream_m3887090282_gshared (Stream_1_t1538553809 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Stream_1_ClearInputStream_m3887090282_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
	}

IL_0016:
	{
		__this->set_inputStreamConnection_1((Il2CppObject *)NULL);
		return;
	}
}
// System.Void Stream`1<System.Int32>::TransactionIterationFinished()
extern "C"  void Stream_1_TransactionIterationFinished_m3275078462_gshared (Stream_1_t1538553809 * __this, const MethodInfo* method)
{
	{
		__this->set_holdedValues_2((List_1_t3644373756 *)NULL);
		return;
	}
}
// System.Void Stream`1<System.Int32>::Unpack(System.Int32)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Stream_1_Unpack_m3449028560_MetadataUsageId;
extern "C"  void Stream_1_Unpack_m3449028560_gshared (Stream_1_t1538553809 * __this, int32_t ___p0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Stream_1_Unpack_m3449028560_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Enumerator_t1730156748  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t3644373756 * L_0 = (List_1_t3644373756 *)__this->get_holdedValues_2();
		NullCheck((List_1_t3644373756 *)L_0);
		Enumerator_t1730156748  L_1 = ((  Enumerator_t1730156748  (*) (List_1_t3644373756 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((List_1_t3644373756 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		V_1 = (Enumerator_t1730156748 )L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0026;
		}

IL_0011:
		{
			int32_t L_2 = ((  int32_t (*) (Enumerator_t1730156748 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->method)((Enumerator_t1730156748 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
			V_0 = (int32_t)L_2;
			Reactor_1_t2107219683 * L_3 = (Reactor_1_t2107219683 *)__this->get_observer_0();
			int32_t L_4 = V_0;
			int32_t L_5 = ___p0;
			NullCheck((Reactor_1_t2107219683 *)L_3);
			((  void (*) (Reactor_1_t2107219683 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((Reactor_1_t2107219683 *)L_3, (int32_t)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		}

IL_0026:
		{
			bool L_6 = ((  bool (*) (Enumerator_t1730156748 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19)->method)((Enumerator_t1730156748 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
			if (L_6)
			{
				goto IL_0011;
			}
		}

IL_0032:
		{
			IL2CPP_LEAVE(0x43, FINALLY_0037);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0037;
	}

FINALLY_0037:
	{ // begin finally (depth: 1)
		Enumerator_t1730156748  L_7 = V_1;
		Enumerator_t1730156748  L_8 = L_7;
		Il2CppObject * L_9 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20), &L_8);
		NullCheck((Il2CppObject *)L_9);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_9);
		IL2CPP_END_FINALLY(55)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(55)
	{
		IL2CPP_JUMP_TBL(0x43, IL_0043)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0043:
	{
		return;
	}
}
// System.Void Stream`1<System.Int32>::<SetInputStream>m__1BA(T)
extern "C"  void Stream_1_U3CSetInputStreamU3Em__1BA_m45029620_gshared (Stream_1_t1538553809 * __this, int32_t ___val0, const MethodInfo* method)
{
	{
		Reactor_1_t2107219683 * L_0 = (Reactor_1_t2107219683 *)__this->get_observer_0();
		int32_t L_1 = ___val0;
		NullCheck((Reactor_1_t2107219683 *)L_0);
		((  void (*) (Reactor_1_t2107219683 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Reactor_1_t2107219683 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return;
	}
}
// System.Void Stream`1<System.Int64>::.ctor()
extern "C"  void Stream_1__ctor_m3600918454_gshared (Stream_1_t1538553904 * __this, const MethodInfo* method)
{
	{
		Reactor_1_t2107219778 * L_0 = (Reactor_1_t2107219778 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (Reactor_1_t2107219778 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->set_observer_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Stream`1<System.Int64>::Send(T)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppClass* Transaction_t3809114814_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5960200;
extern const uint32_t Stream_1_Send_m2130136008_MetadataUsageId;
extern "C"  void Stream_1_Send_m2130136008_gshared (Stream_1_t1538553904 * __this, int64_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Stream_1_Send_m2130136008_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
		__this->set_inputStreamConnection_1((Il2CppObject *)NULL);
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_2 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_calculationMode_8();
		if (!L_2)
		{
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m1836388681(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral5960200, /*hidden argument*/NULL);
	}

IL_0031:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_3 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_performing_5();
		if (!L_3)
		{
			goto IL_0056;
		}
	}
	{
		V_0 = (int32_t)0;
		goto IL_004e;
	}

IL_0042:
	{
		int64_t L_4 = ___value0;
		int32_t L_5 = V_0;
		NullCheck((Stream_1_t1538553904 *)__this);
		((  void (*) (Stream_1_t1538553904 *, int64_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Stream_1_t1538553904 *)__this, (int64_t)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		int32_t L_6 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_004e:
	{
		int32_t L_7 = V_0;
		if ((((int32_t)L_7) < ((int32_t)3)))
		{
			goto IL_0042;
		}
	}
	{
		return;
	}

IL_0056:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_8 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_hold_3();
		if (!L_8)
		{
			goto IL_0089;
		}
	}
	{
		List_1_t3644373851 * L_9 = (List_1_t3644373851 *)__this->get_holdedValues_2();
		if (L_9)
		{
			goto IL_0076;
		}
	}
	{
		List_1_t3644373851 * L_10 = (List_1_t3644373851 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (List_1_t3644373851 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		__this->set_holdedValues_2(L_10);
	}

IL_0076:
	{
		List_1_t3644373851 * L_11 = (List_1_t3644373851 *)__this->get_holdedValues_2();
		int64_t L_12 = ___value0;
		NullCheck((List_1_t3644373851 *)L_11);
		VirtActionInvoker1< int64_t >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Int64>::Add(!0) */, (List_1_t3644373851 *)L_11, (int64_t)L_12);
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		Transaction_RegisterPackedReaction_m1156829996(NULL /*static, unused*/, (Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}

IL_0089:
	{
		Reactor_1_t2107219778 * L_13 = (Reactor_1_t2107219778 *)__this->get_observer_0();
		int64_t L_14 = ___value0;
		NullCheck((Reactor_1_t2107219778 *)L_13);
		((  void (*) (Reactor_1_t2107219778 *, int64_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Reactor_1_t2107219778 *)L_13, (int64_t)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		bool L_15 = (bool)__this->get_autoDisconnectAfterEvent_4();
		if (!L_15)
		{
			goto IL_00ab;
		}
	}
	{
		Reactor_1_t2107219778 * L_16 = (Reactor_1_t2107219778 *)__this->get_observer_0();
		NullCheck((Reactor_1_t2107219778 *)L_16);
		((  void (*) (Reactor_1_t2107219778 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((Reactor_1_t2107219778 *)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
	}

IL_00ab:
	{
		return;
	}
}
// System.Void Stream`1<System.Int64>::SendInTransaction(T,System.Int32)
extern "C"  void Stream_1_SendInTransaction_m2170246494_gshared (Stream_1_t1538553904 * __this, int64_t ___value0, int32_t ___p1, const MethodInfo* method)
{
	{
		Reactor_1_t2107219778 * L_0 = (Reactor_1_t2107219778 *)__this->get_observer_0();
		int64_t L_1 = ___value0;
		int32_t L_2 = ___p1;
		NullCheck((Reactor_1_t2107219778 *)L_0);
		((  void (*) (Reactor_1_t2107219778 *, int64_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((Reactor_1_t2107219778 *)L_0, (int64_t)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return;
	}
}
// System.IDisposable Stream`1<System.Int64>::Listen(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Stream_1_Listen_m2034331546_gshared (Stream_1_t1538553904 * __this, Action_1_t2995867587 * ___action0, int32_t ___priority1, const MethodInfo* method)
{
	{
		Reactor_1_t2107219778 * L_0 = (Reactor_1_t2107219778 *)__this->get_observer_0();
		Action_1_t2995867587 * L_1 = ___action0;
		int32_t L_2 = ___priority1;
		NullCheck((Reactor_1_t2107219778 *)L_0);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Reactor_1_t2107219778 *, Action_1_t2995867587 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Reactor_1_t2107219778 *)L_0, (Action_1_t2995867587 *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		return L_3;
	}
}
// System.IDisposable Stream`1<System.Int64>::Listen(System.Action,Priority)
extern "C"  Il2CppObject * Stream_1_Listen_m61520205_gshared (Stream_1_t1538553904 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method)
{
	U3CListenU3Ec__AnonStorey128_t2078774906 * V_0 = NULL;
	{
		U3CListenU3Ec__AnonStorey128_t2078774906 * L_0 = (U3CListenU3Ec__AnonStorey128_t2078774906 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (U3CListenU3Ec__AnonStorey128_t2078774906 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		V_0 = (U3CListenU3Ec__AnonStorey128_t2078774906 *)L_0;
		U3CListenU3Ec__AnonStorey128_t2078774906 * L_1 = V_0;
		Action_t437523947 * L_2 = ___action0;
		NullCheck(L_1);
		L_1->set_action_0(L_2);
		Reactor_1_t2107219778 * L_3 = (Reactor_1_t2107219778 *)__this->get_observer_0();
		U3CListenU3Ec__AnonStorey128_t2078774906 * L_4 = V_0;
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		Action_1_t2995867587 * L_6 = (Action_1_t2995867587 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		((  void (*) (Action_1_t2995867587 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)(L_6, (Il2CppObject *)L_4, (IntPtr_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		int32_t L_7 = ___p1;
		NullCheck((Reactor_1_t2107219778 *)L_3);
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (Reactor_1_t2107219778 *, Action_1_t2995867587 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Reactor_1_t2107219778 *)L_3, (Action_1_t2995867587 *)L_6, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		return L_8;
	}
}
// System.Void Stream`1<System.Int64>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Stream_1_Dispose_m2932244083_MetadataUsageId;
extern "C"  void Stream_1_Dispose_m2932244083_gshared (Stream_1_t1538553904 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Stream_1_Dispose_m2932244083_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_observer_0((Reactor_1_t2107219778 *)NULL);
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
	}

IL_001d:
	{
		__this->set_inputStreamConnection_1((Il2CppObject *)NULL);
		return;
	}
}
// System.Void Stream`1<System.Int64>::SetInputStream(IStream`1<T>)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Stream_1_SetInputStream_m3534796298_MetadataUsageId;
extern "C"  void Stream_1_SetInputStream_m3534796298_gshared (Stream_1_t1538553904 * __this, Il2CppObject* ___stream0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Stream_1_SetInputStream_m3534796298_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = ___stream0;
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		Action_1_t2995867587 * L_4 = (Action_1_t2995867587 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		((  void (*) (Action_1_t2995867587 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)(L_4, (Il2CppObject *)__this, (IntPtr_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_5 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t2995867587 *, int32_t >::Invoke(0 /* System.IDisposable IStream`1<System.Int64>::Listen(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16), (Il2CppObject*)L_2, (Action_1_t2995867587 *)L_4, (int32_t)1);
		__this->set_inputStreamConnection_1(L_5);
		return;
	}
}
// System.Void Stream`1<System.Int64>::ClearInputStream()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Stream_1_ClearInputStream_m3545271147_MetadataUsageId;
extern "C"  void Stream_1_ClearInputStream_m3545271147_gshared (Stream_1_t1538553904 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Stream_1_ClearInputStream_m3545271147_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
	}

IL_0016:
	{
		__this->set_inputStreamConnection_1((Il2CppObject *)NULL);
		return;
	}
}
// System.Void Stream`1<System.Int64>::TransactionIterationFinished()
extern "C"  void Stream_1_TransactionIterationFinished_m1912570559_gshared (Stream_1_t1538553904 * __this, const MethodInfo* method)
{
	{
		__this->set_holdedValues_2((List_1_t3644373851 *)NULL);
		return;
	}
}
// System.Void Stream`1<System.Int64>::Unpack(System.Int32)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Stream_1_Unpack_m1378354321_MetadataUsageId;
extern "C"  void Stream_1_Unpack_m1378354321_gshared (Stream_1_t1538553904 * __this, int32_t ___p0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Stream_1_Unpack_m1378354321_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int64_t V_0 = 0;
	Enumerator_t1730156843  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t3644373851 * L_0 = (List_1_t3644373851 *)__this->get_holdedValues_2();
		NullCheck((List_1_t3644373851 *)L_0);
		Enumerator_t1730156843  L_1 = ((  Enumerator_t1730156843  (*) (List_1_t3644373851 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((List_1_t3644373851 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		V_1 = (Enumerator_t1730156843 )L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0026;
		}

IL_0011:
		{
			int64_t L_2 = ((  int64_t (*) (Enumerator_t1730156843 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->method)((Enumerator_t1730156843 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
			V_0 = (int64_t)L_2;
			Reactor_1_t2107219778 * L_3 = (Reactor_1_t2107219778 *)__this->get_observer_0();
			int64_t L_4 = V_0;
			int32_t L_5 = ___p0;
			NullCheck((Reactor_1_t2107219778 *)L_3);
			((  void (*) (Reactor_1_t2107219778 *, int64_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((Reactor_1_t2107219778 *)L_3, (int64_t)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		}

IL_0026:
		{
			bool L_6 = ((  bool (*) (Enumerator_t1730156843 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19)->method)((Enumerator_t1730156843 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
			if (L_6)
			{
				goto IL_0011;
			}
		}

IL_0032:
		{
			IL2CPP_LEAVE(0x43, FINALLY_0037);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0037;
	}

FINALLY_0037:
	{ // begin finally (depth: 1)
		Enumerator_t1730156843  L_7 = V_1;
		Enumerator_t1730156843  L_8 = L_7;
		Il2CppObject * L_9 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20), &L_8);
		NullCheck((Il2CppObject *)L_9);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_9);
		IL2CPP_END_FINALLY(55)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(55)
	{
		IL2CPP_JUMP_TBL(0x43, IL_0043)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0043:
	{
		return;
	}
}
// System.Void Stream`1<System.Int64>::<SetInputStream>m__1BA(T)
extern "C"  void Stream_1_U3CSetInputStreamU3Em__1BA_m1465181395_gshared (Stream_1_t1538553904 * __this, int64_t ___val0, const MethodInfo* method)
{
	{
		Reactor_1_t2107219778 * L_0 = (Reactor_1_t2107219778 *)__this->get_observer_0();
		int64_t L_1 = ___val0;
		NullCheck((Reactor_1_t2107219778 *)L_0);
		((  void (*) (Reactor_1_t2107219778 *, int64_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Reactor_1_t2107219778 *)L_0, (int64_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return;
	}
}
// System.Void Stream`1<System.Object>::.ctor()
extern "C"  void Stream_1__ctor_m2034388992_gshared (Stream_1_t3823212738 * __this, const MethodInfo* method)
{
	{
		Reactor_1_t96911316 * L_0 = (Reactor_1_t96911316 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (Reactor_1_t96911316 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->set_observer_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Stream`1<System.Object>::Send(T)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppClass* Transaction_t3809114814_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5960200;
extern const uint32_t Stream_1_Send_m563606546_MetadataUsageId;
extern "C"  void Stream_1_Send_m563606546_gshared (Stream_1_t3823212738 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Stream_1_Send_m563606546_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
		__this->set_inputStreamConnection_1((Il2CppObject *)NULL);
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_2 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_calculationMode_8();
		if (!L_2)
		{
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m1836388681(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral5960200, /*hidden argument*/NULL);
	}

IL_0031:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_3 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_performing_5();
		if (!L_3)
		{
			goto IL_0056;
		}
	}
	{
		V_0 = (int32_t)0;
		goto IL_004e;
	}

IL_0042:
	{
		Il2CppObject * L_4 = ___value0;
		int32_t L_5 = V_0;
		NullCheck((Stream_1_t3823212738 *)__this);
		((  void (*) (Stream_1_t3823212738 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Stream_1_t3823212738 *)__this, (Il2CppObject *)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		int32_t L_6 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_004e:
	{
		int32_t L_7 = V_0;
		if ((((int32_t)L_7) < ((int32_t)3)))
		{
			goto IL_0042;
		}
	}
	{
		return;
	}

IL_0056:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_8 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_hold_3();
		if (!L_8)
		{
			goto IL_0089;
		}
	}
	{
		List_1_t1634065389 * L_9 = (List_1_t1634065389 *)__this->get_holdedValues_2();
		if (L_9)
		{
			goto IL_0076;
		}
	}
	{
		List_1_t1634065389 * L_10 = (List_1_t1634065389 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (List_1_t1634065389 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		__this->set_holdedValues_2(L_10);
	}

IL_0076:
	{
		List_1_t1634065389 * L_11 = (List_1_t1634065389 *)__this->get_holdedValues_2();
		Il2CppObject * L_12 = ___value0;
		NullCheck((List_1_t1634065389 *)L_11);
		VirtActionInvoker1< Il2CppObject * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Object>::Add(!0) */, (List_1_t1634065389 *)L_11, (Il2CppObject *)L_12);
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		Transaction_RegisterPackedReaction_m1156829996(NULL /*static, unused*/, (Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}

IL_0089:
	{
		Reactor_1_t96911316 * L_13 = (Reactor_1_t96911316 *)__this->get_observer_0();
		Il2CppObject * L_14 = ___value0;
		NullCheck((Reactor_1_t96911316 *)L_13);
		((  void (*) (Reactor_1_t96911316 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Reactor_1_t96911316 *)L_13, (Il2CppObject *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		bool L_15 = (bool)__this->get_autoDisconnectAfterEvent_4();
		if (!L_15)
		{
			goto IL_00ab;
		}
	}
	{
		Reactor_1_t96911316 * L_16 = (Reactor_1_t96911316 *)__this->get_observer_0();
		NullCheck((Reactor_1_t96911316 *)L_16);
		((  void (*) (Reactor_1_t96911316 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((Reactor_1_t96911316 *)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
	}

IL_00ab:
	{
		return;
	}
}
// System.Void Stream`1<System.Object>::SendInTransaction(T,System.Int32)
extern "C"  void Stream_1_SendInTransaction_m3764966696_gshared (Stream_1_t3823212738 * __this, Il2CppObject * ___value0, int32_t ___p1, const MethodInfo* method)
{
	{
		Reactor_1_t96911316 * L_0 = (Reactor_1_t96911316 *)__this->get_observer_0();
		Il2CppObject * L_1 = ___value0;
		int32_t L_2 = ___p1;
		NullCheck((Reactor_1_t96911316 *)L_0);
		((  void (*) (Reactor_1_t96911316 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((Reactor_1_t96911316 *)L_0, (Il2CppObject *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return;
	}
}
// System.IDisposable Stream`1<System.Object>::Listen(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Stream_1_Listen_m889871082_gshared (Stream_1_t3823212738 * __this, Action_1_t985559125 * ___action0, int32_t ___priority1, const MethodInfo* method)
{
	{
		Reactor_1_t96911316 * L_0 = (Reactor_1_t96911316 *)__this->get_observer_0();
		Action_1_t985559125 * L_1 = ___action0;
		int32_t L_2 = ___priority1;
		NullCheck((Reactor_1_t96911316 *)L_0);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Reactor_1_t96911316 *, Action_1_t985559125 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Reactor_1_t96911316 *)L_0, (Action_1_t985559125 *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		return L_3;
	}
}
// System.IDisposable Stream`1<System.Object>::Listen(System.Action,Priority)
extern "C"  Il2CppObject * Stream_1_Listen_m2133851133_gshared (Stream_1_t3823212738 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method)
{
	U3CListenU3Ec__AnonStorey128_t68466444 * V_0 = NULL;
	{
		U3CListenU3Ec__AnonStorey128_t68466444 * L_0 = (U3CListenU3Ec__AnonStorey128_t68466444 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (U3CListenU3Ec__AnonStorey128_t68466444 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		V_0 = (U3CListenU3Ec__AnonStorey128_t68466444 *)L_0;
		U3CListenU3Ec__AnonStorey128_t68466444 * L_1 = V_0;
		Action_t437523947 * L_2 = ___action0;
		NullCheck(L_1);
		L_1->set_action_0(L_2);
		Reactor_1_t96911316 * L_3 = (Reactor_1_t96911316 *)__this->get_observer_0();
		U3CListenU3Ec__AnonStorey128_t68466444 * L_4 = V_0;
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		Action_1_t985559125 * L_6 = (Action_1_t985559125 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)(L_6, (Il2CppObject *)L_4, (IntPtr_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		int32_t L_7 = ___p1;
		NullCheck((Reactor_1_t96911316 *)L_3);
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (Reactor_1_t96911316 *, Action_1_t985559125 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Reactor_1_t96911316 *)L_3, (Action_1_t985559125 *)L_6, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		return L_8;
	}
}
// System.Void Stream`1<System.Object>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Stream_1_Dispose_m735984701_MetadataUsageId;
extern "C"  void Stream_1_Dispose_m735984701_gshared (Stream_1_t3823212738 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Stream_1_Dispose_m735984701_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_observer_0((Reactor_1_t96911316 *)NULL);
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
	}

IL_001d:
	{
		__this->set_inputStreamConnection_1((Il2CppObject *)NULL);
		return;
	}
}
// System.Void Stream`1<System.Object>::SetInputStream(IStream`1<T>)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Stream_1_SetInputStream_m3106996224_MetadataUsageId;
extern "C"  void Stream_1_SetInputStream_m3106996224_gshared (Stream_1_t3823212738 * __this, Il2CppObject* ___stream0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Stream_1_SetInputStream_m3106996224_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = ___stream0;
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		Action_1_t985559125 * L_4 = (Action_1_t985559125 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)(L_4, (Il2CppObject *)__this, (IntPtr_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_5 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t985559125 *, int32_t >::Invoke(0 /* System.IDisposable IStream`1<System.Object>::Listen(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16), (Il2CppObject*)L_2, (Action_1_t985559125 *)L_4, (int32_t)1);
		__this->set_inputStreamConnection_1(L_5);
		return;
	}
}
// System.Void Stream`1<System.Object>::ClearInputStream()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Stream_1_ClearInputStream_m445690081_MetadataUsageId;
extern "C"  void Stream_1_ClearInputStream_m445690081_gshared (Stream_1_t3823212738 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Stream_1_ClearInputStream_m445690081_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
	}

IL_0016:
	{
		__this->set_inputStreamConnection_1((Il2CppObject *)NULL);
		return;
	}
}
// System.Void Stream`1<System.Object>::TransactionIterationFinished()
extern "C"  void Stream_1_TransactionIterationFinished_m3113559861_gshared (Stream_1_t3823212738 * __this, const MethodInfo* method)
{
	{
		__this->set_holdedValues_2((List_1_t1634065389 *)NULL);
		return;
	}
}
// System.Void Stream`1<System.Object>::Unpack(System.Int32)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Stream_1_Unpack_m3388253319_MetadataUsageId;
extern "C"  void Stream_1_Unpack_m3388253319_gshared (Stream_1_t3823212738 * __this, int32_t ___p0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Stream_1_Unpack_m3388253319_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Enumerator_t4014815677  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t1634065389 * L_0 = (List_1_t1634065389 *)__this->get_holdedValues_2();
		NullCheck((List_1_t1634065389 *)L_0);
		Enumerator_t4014815677  L_1 = ((  Enumerator_t4014815677  (*) (List_1_t1634065389 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((List_1_t1634065389 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		V_1 = (Enumerator_t4014815677 )L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0026;
		}

IL_0011:
		{
			Il2CppObject * L_2 = ((  Il2CppObject * (*) (Enumerator_t4014815677 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->method)((Enumerator_t4014815677 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
			V_0 = (Il2CppObject *)L_2;
			Reactor_1_t96911316 * L_3 = (Reactor_1_t96911316 *)__this->get_observer_0();
			Il2CppObject * L_4 = V_0;
			int32_t L_5 = ___p0;
			NullCheck((Reactor_1_t96911316 *)L_3);
			((  void (*) (Reactor_1_t96911316 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((Reactor_1_t96911316 *)L_3, (Il2CppObject *)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		}

IL_0026:
		{
			bool L_6 = ((  bool (*) (Enumerator_t4014815677 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19)->method)((Enumerator_t4014815677 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
			if (L_6)
			{
				goto IL_0011;
			}
		}

IL_0032:
		{
			IL2CPP_LEAVE(0x43, FINALLY_0037);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0037;
	}

FINALLY_0037:
	{ // begin finally (depth: 1)
		Enumerator_t4014815677  L_7 = V_1;
		Enumerator_t4014815677  L_8 = L_7;
		Il2CppObject * L_9 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20), &L_8);
		NullCheck((Il2CppObject *)L_9);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_9);
		IL2CPP_END_FINALLY(55)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(55)
	{
		IL2CPP_JUMP_TBL(0x43, IL_0043)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0043:
	{
		return;
	}
}
// System.Void Stream`1<System.Object>::<SetInputStream>m__1BA(T)
extern "C"  void Stream_1_U3CSetInputStreamU3Em__1BA_m3579563677_gshared (Stream_1_t3823212738 * __this, Il2CppObject * ___val0, const MethodInfo* method)
{
	{
		Reactor_1_t96911316 * L_0 = (Reactor_1_t96911316 *)__this->get_observer_0();
		Il2CppObject * L_1 = ___val0;
		NullCheck((Reactor_1_t96911316 *)L_0);
		((  void (*) (Reactor_1_t96911316 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Reactor_1_t96911316 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return;
	}
}
// System.Void Stream`1<System.Single>::.ctor()
extern "C"  void Stream_1__ctor_m1692350921_gshared (Stream_1_t3944315339 * __this, const MethodInfo* method)
{
	{
		Reactor_1_t218013917 * L_0 = (Reactor_1_t218013917 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (Reactor_1_t218013917 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->set_observer_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Stream`1<System.Single>::Send(T)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppClass* Transaction_t3809114814_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5960200;
extern const uint32_t Stream_1_Send_m221568475_MetadataUsageId;
extern "C"  void Stream_1_Send_m221568475_gshared (Stream_1_t3944315339 * __this, float ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Stream_1_Send_m221568475_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
		__this->set_inputStreamConnection_1((Il2CppObject *)NULL);
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_2 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_calculationMode_8();
		if (!L_2)
		{
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m1836388681(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral5960200, /*hidden argument*/NULL);
	}

IL_0031:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_3 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_performing_5();
		if (!L_3)
		{
			goto IL_0056;
		}
	}
	{
		V_0 = (int32_t)0;
		goto IL_004e;
	}

IL_0042:
	{
		float L_4 = ___value0;
		int32_t L_5 = V_0;
		NullCheck((Stream_1_t3944315339 *)__this);
		((  void (*) (Stream_1_t3944315339 *, float, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Stream_1_t3944315339 *)__this, (float)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		int32_t L_6 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_004e:
	{
		int32_t L_7 = V_0;
		if ((((int32_t)L_7) < ((int32_t)3)))
		{
			goto IL_0042;
		}
	}
	{
		return;
	}

IL_0056:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_8 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_hold_3();
		if (!L_8)
		{
			goto IL_0089;
		}
	}
	{
		List_1_t1755167990 * L_9 = (List_1_t1755167990 *)__this->get_holdedValues_2();
		if (L_9)
		{
			goto IL_0076;
		}
	}
	{
		List_1_t1755167990 * L_10 = (List_1_t1755167990 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (List_1_t1755167990 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		__this->set_holdedValues_2(L_10);
	}

IL_0076:
	{
		List_1_t1755167990 * L_11 = (List_1_t1755167990 *)__this->get_holdedValues_2();
		float L_12 = ___value0;
		NullCheck((List_1_t1755167990 *)L_11);
		VirtActionInvoker1< float >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Single>::Add(!0) */, (List_1_t1755167990 *)L_11, (float)L_12);
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		Transaction_RegisterPackedReaction_m1156829996(NULL /*static, unused*/, (Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}

IL_0089:
	{
		Reactor_1_t218013917 * L_13 = (Reactor_1_t218013917 *)__this->get_observer_0();
		float L_14 = ___value0;
		NullCheck((Reactor_1_t218013917 *)L_13);
		((  void (*) (Reactor_1_t218013917 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Reactor_1_t218013917 *)L_13, (float)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		bool L_15 = (bool)__this->get_autoDisconnectAfterEvent_4();
		if (!L_15)
		{
			goto IL_00ab;
		}
	}
	{
		Reactor_1_t218013917 * L_16 = (Reactor_1_t218013917 *)__this->get_observer_0();
		NullCheck((Reactor_1_t218013917 *)L_16);
		((  void (*) (Reactor_1_t218013917 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((Reactor_1_t218013917 *)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
	}

IL_00ab:
	{
		return;
	}
}
// System.Void Stream`1<System.Single>::SendInTransaction(T,System.Int32)
extern "C"  void Stream_1_SendInTransaction_m166372785_gshared (Stream_1_t3944315339 * __this, float ___value0, int32_t ___p1, const MethodInfo* method)
{
	{
		Reactor_1_t218013917 * L_0 = (Reactor_1_t218013917 *)__this->get_observer_0();
		float L_1 = ___value0;
		int32_t L_2 = ___p1;
		NullCheck((Reactor_1_t218013917 *)L_0);
		((  void (*) (Reactor_1_t218013917 *, float, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((Reactor_1_t218013917 *)L_0, (float)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return;
	}
}
// System.IDisposable Stream`1<System.Single>::Listen(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Stream_1_Listen_m89795891_gshared (Stream_1_t3944315339 * __this, Action_1_t1106661726 * ___action0, int32_t ___priority1, const MethodInfo* method)
{
	{
		Reactor_1_t218013917 * L_0 = (Reactor_1_t218013917 *)__this->get_observer_0();
		Action_1_t1106661726 * L_1 = ___action0;
		int32_t L_2 = ___priority1;
		NullCheck((Reactor_1_t218013917 *)L_0);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Reactor_1_t218013917 *, Action_1_t1106661726 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Reactor_1_t218013917 *)L_0, (Action_1_t1106661726 *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		return L_3;
	}
}
// System.IDisposable Stream`1<System.Single>::Listen(System.Action,Priority)
extern "C"  Il2CppObject * Stream_1_Listen_m2354454548_gshared (Stream_1_t3944315339 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method)
{
	U3CListenU3Ec__AnonStorey128_t189569045 * V_0 = NULL;
	{
		U3CListenU3Ec__AnonStorey128_t189569045 * L_0 = (U3CListenU3Ec__AnonStorey128_t189569045 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (U3CListenU3Ec__AnonStorey128_t189569045 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		V_0 = (U3CListenU3Ec__AnonStorey128_t189569045 *)L_0;
		U3CListenU3Ec__AnonStorey128_t189569045 * L_1 = V_0;
		Action_t437523947 * L_2 = ___action0;
		NullCheck(L_1);
		L_1->set_action_0(L_2);
		Reactor_1_t218013917 * L_3 = (Reactor_1_t218013917 *)__this->get_observer_0();
		U3CListenU3Ec__AnonStorey128_t189569045 * L_4 = V_0;
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		Action_1_t1106661726 * L_6 = (Action_1_t1106661726 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		((  void (*) (Action_1_t1106661726 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)(L_6, (Il2CppObject *)L_4, (IntPtr_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		int32_t L_7 = ___p1;
		NullCheck((Reactor_1_t218013917 *)L_3);
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (Reactor_1_t218013917 *, Action_1_t1106661726 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Reactor_1_t218013917 *)L_3, (Action_1_t1106661726 *)L_6, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		return L_8;
	}
}
// System.Void Stream`1<System.Single>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Stream_1_Dispose_m2749880262_MetadataUsageId;
extern "C"  void Stream_1_Dispose_m2749880262_gshared (Stream_1_t3944315339 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Stream_1_Dispose_m2749880262_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_observer_0((Reactor_1_t218013917 *)NULL);
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
	}

IL_001d:
	{
		__this->set_inputStreamConnection_1((Il2CppObject *)NULL);
		return;
	}
}
// System.Void Stream`1<System.Single>::SetInputStream(IStream`1<T>)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Stream_1_SetInputStream_m3169795543_MetadataUsageId;
extern "C"  void Stream_1_SetInputStream_m3169795543_gshared (Stream_1_t3944315339 * __this, Il2CppObject* ___stream0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Stream_1_SetInputStream_m3169795543_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = ___stream0;
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		Action_1_t1106661726 * L_4 = (Action_1_t1106661726 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		((  void (*) (Action_1_t1106661726 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)(L_4, (Il2CppObject *)__this, (IntPtr_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_5 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t1106661726 *, int32_t >::Invoke(0 /* System.IDisposable IStream`1<System.Single>::Listen(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16), (Il2CppObject*)L_2, (Action_1_t1106661726 *)L_4, (int32_t)1);
		__this->set_inputStreamConnection_1(L_5);
		return;
	}
}
// System.Void Stream`1<System.Single>::ClearInputStream()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Stream_1_ClearInputStream_m3480420216_MetadataUsageId;
extern "C"  void Stream_1_ClearInputStream_m3480420216_gshared (Stream_1_t3944315339 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Stream_1_ClearInputStream_m3480420216_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
	}

IL_0016:
	{
		__this->set_inputStreamConnection_1((Il2CppObject *)NULL);
		return;
	}
}
// System.Void Stream`1<System.Single>::TransactionIterationFinished()
extern "C"  void Stream_1_TransactionIterationFinished_m3334163276_gshared (Stream_1_t3944315339 * __this, const MethodInfo* method)
{
	{
		__this->set_holdedValues_2((List_1_t1755167990 *)NULL);
		return;
	}
}
// System.Void Stream`1<System.Single>::Unpack(System.Int32)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Stream_1_Unpack_m3481119070_MetadataUsageId;
extern "C"  void Stream_1_Unpack_m3481119070_gshared (Stream_1_t3944315339 * __this, int32_t ___p0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Stream_1_Unpack_m3481119070_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	Enumerator_t4135918278  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t1755167990 * L_0 = (List_1_t1755167990 *)__this->get_holdedValues_2();
		NullCheck((List_1_t1755167990 *)L_0);
		Enumerator_t4135918278  L_1 = ((  Enumerator_t4135918278  (*) (List_1_t1755167990 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((List_1_t1755167990 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		V_1 = (Enumerator_t4135918278 )L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0026;
		}

IL_0011:
		{
			float L_2 = ((  float (*) (Enumerator_t4135918278 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->method)((Enumerator_t4135918278 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
			V_0 = (float)L_2;
			Reactor_1_t218013917 * L_3 = (Reactor_1_t218013917 *)__this->get_observer_0();
			float L_4 = V_0;
			int32_t L_5 = ___p0;
			NullCheck((Reactor_1_t218013917 *)L_3);
			((  void (*) (Reactor_1_t218013917 *, float, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((Reactor_1_t218013917 *)L_3, (float)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		}

IL_0026:
		{
			bool L_6 = ((  bool (*) (Enumerator_t4135918278 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19)->method)((Enumerator_t4135918278 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
			if (L_6)
			{
				goto IL_0011;
			}
		}

IL_0032:
		{
			IL2CPP_LEAVE(0x43, FINALLY_0037);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0037;
	}

FINALLY_0037:
	{ // begin finally (depth: 1)
		Enumerator_t4135918278  L_7 = V_1;
		Enumerator_t4135918278  L_8 = L_7;
		Il2CppObject * L_9 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20), &L_8);
		NullCheck((Il2CppObject *)L_9);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_9);
		IL2CPP_END_FINALLY(55)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(55)
	{
		IL2CPP_JUMP_TBL(0x43, IL_0043)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0043:
	{
		return;
	}
}
// System.Void Stream`1<System.Single>::<SetInputStream>m__1BA(T)
extern "C"  void Stream_1_U3CSetInputStreamU3Em__1BA_m532101158_gshared (Stream_1_t3944315339 * __this, float ___val0, const MethodInfo* method)
{
	{
		Reactor_1_t218013917 * L_0 = (Reactor_1_t218013917 *)__this->get_observer_0();
		float L_1 = ___val0;
		NullCheck((Reactor_1_t218013917 *)L_0);
		((  void (*) (Reactor_1_t218013917 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Reactor_1_t218013917 *)L_0, (float)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return;
	}
}
// System.Void Stream`1<UniRx.CollectionAddEvent`1<System.Object>>::.ctor()
extern "C"  void Stream_1__ctor_m3235287902_gshared (Stream_1_t1107661009 * __this, const MethodInfo* method)
{
	{
		Reactor_1_t1676326883 * L_0 = (Reactor_1_t1676326883 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (Reactor_1_t1676326883 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->set_observer_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Stream`1<UniRx.CollectionAddEvent`1<System.Object>>::Send(T)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppClass* Transaction_t3809114814_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5960200;
extern const uint32_t Stream_1_Send_m1764505456_MetadataUsageId;
extern "C"  void Stream_1_Send_m1764505456_gshared (Stream_1_t1107661009 * __this, CollectionAddEvent_1_t2416521987  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Stream_1_Send_m1764505456_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
		__this->set_inputStreamConnection_1((Il2CppObject *)NULL);
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_2 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_calculationMode_8();
		if (!L_2)
		{
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m1836388681(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral5960200, /*hidden argument*/NULL);
	}

IL_0031:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_3 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_performing_5();
		if (!L_3)
		{
			goto IL_0056;
		}
	}
	{
		V_0 = (int32_t)0;
		goto IL_004e;
	}

IL_0042:
	{
		CollectionAddEvent_1_t2416521987  L_4 = ___value0;
		int32_t L_5 = V_0;
		NullCheck((Stream_1_t1107661009 *)__this);
		((  void (*) (Stream_1_t1107661009 *, CollectionAddEvent_1_t2416521987 , int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Stream_1_t1107661009 *)__this, (CollectionAddEvent_1_t2416521987 )L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		int32_t L_6 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_004e:
	{
		int32_t L_7 = V_0;
		if ((((int32_t)L_7) < ((int32_t)3)))
		{
			goto IL_0042;
		}
	}
	{
		return;
	}

IL_0056:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_8 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_hold_3();
		if (!L_8)
		{
			goto IL_0089;
		}
	}
	{
		List_1_t3213480956 * L_9 = (List_1_t3213480956 *)__this->get_holdedValues_2();
		if (L_9)
		{
			goto IL_0076;
		}
	}
	{
		List_1_t3213480956 * L_10 = (List_1_t3213480956 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (List_1_t3213480956 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		__this->set_holdedValues_2(L_10);
	}

IL_0076:
	{
		List_1_t3213480956 * L_11 = (List_1_t3213480956 *)__this->get_holdedValues_2();
		CollectionAddEvent_1_t2416521987  L_12 = ___value0;
		NullCheck((List_1_t3213480956 *)L_11);
		VirtActionInvoker1< CollectionAddEvent_1_t2416521987  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UniRx.CollectionAddEvent`1<System.Object>>::Add(!0) */, (List_1_t3213480956 *)L_11, (CollectionAddEvent_1_t2416521987 )L_12);
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		Transaction_RegisterPackedReaction_m1156829996(NULL /*static, unused*/, (Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}

IL_0089:
	{
		Reactor_1_t1676326883 * L_13 = (Reactor_1_t1676326883 *)__this->get_observer_0();
		CollectionAddEvent_1_t2416521987  L_14 = ___value0;
		NullCheck((Reactor_1_t1676326883 *)L_13);
		((  void (*) (Reactor_1_t1676326883 *, CollectionAddEvent_1_t2416521987 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Reactor_1_t1676326883 *)L_13, (CollectionAddEvent_1_t2416521987 )L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		bool L_15 = (bool)__this->get_autoDisconnectAfterEvent_4();
		if (!L_15)
		{
			goto IL_00ab;
		}
	}
	{
		Reactor_1_t1676326883 * L_16 = (Reactor_1_t1676326883 *)__this->get_observer_0();
		NullCheck((Reactor_1_t1676326883 *)L_16);
		((  void (*) (Reactor_1_t1676326883 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((Reactor_1_t1676326883 *)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
	}

IL_00ab:
	{
		return;
	}
}
// System.Void Stream`1<UniRx.CollectionAddEvent`1<System.Object>>::SendInTransaction(T,System.Int32)
extern "C"  void Stream_1_SendInTransaction_m1512275718_gshared (Stream_1_t1107661009 * __this, CollectionAddEvent_1_t2416521987  ___value0, int32_t ___p1, const MethodInfo* method)
{
	{
		Reactor_1_t1676326883 * L_0 = (Reactor_1_t1676326883 *)__this->get_observer_0();
		CollectionAddEvent_1_t2416521987  L_1 = ___value0;
		int32_t L_2 = ___p1;
		NullCheck((Reactor_1_t1676326883 *)L_0);
		((  void (*) (Reactor_1_t1676326883 *, CollectionAddEvent_1_t2416521987 , int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((Reactor_1_t1676326883 *)L_0, (CollectionAddEvent_1_t2416521987 )L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return;
	}
}
// System.IDisposable Stream`1<UniRx.CollectionAddEvent`1<System.Object>>::Listen(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Stream_1_Listen_m1890769608_gshared (Stream_1_t1107661009 * __this, Action_1_t2564974692 * ___action0, int32_t ___priority1, const MethodInfo* method)
{
	{
		Reactor_1_t1676326883 * L_0 = (Reactor_1_t1676326883 *)__this->get_observer_0();
		Action_1_t2564974692 * L_1 = ___action0;
		int32_t L_2 = ___priority1;
		NullCheck((Reactor_1_t1676326883 *)L_0);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Reactor_1_t1676326883 *, Action_1_t2564974692 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Reactor_1_t1676326883 *)L_0, (Action_1_t2564974692 *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		return L_3;
	}
}
// System.IDisposable Stream`1<UniRx.CollectionAddEvent`1<System.Object>>::Listen(System.Action,Priority)
extern "C"  Il2CppObject * Stream_1_Listen_m3411183967_gshared (Stream_1_t1107661009 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method)
{
	U3CListenU3Ec__AnonStorey128_t1647882011 * V_0 = NULL;
	{
		U3CListenU3Ec__AnonStorey128_t1647882011 * L_0 = (U3CListenU3Ec__AnonStorey128_t1647882011 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (U3CListenU3Ec__AnonStorey128_t1647882011 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		V_0 = (U3CListenU3Ec__AnonStorey128_t1647882011 *)L_0;
		U3CListenU3Ec__AnonStorey128_t1647882011 * L_1 = V_0;
		Action_t437523947 * L_2 = ___action0;
		NullCheck(L_1);
		L_1->set_action_0(L_2);
		Reactor_1_t1676326883 * L_3 = (Reactor_1_t1676326883 *)__this->get_observer_0();
		U3CListenU3Ec__AnonStorey128_t1647882011 * L_4 = V_0;
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		Action_1_t2564974692 * L_6 = (Action_1_t2564974692 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		((  void (*) (Action_1_t2564974692 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)(L_6, (Il2CppObject *)L_4, (IntPtr_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		int32_t L_7 = ___p1;
		NullCheck((Reactor_1_t1676326883 *)L_3);
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (Reactor_1_t1676326883 *, Action_1_t2564974692 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Reactor_1_t1676326883 *)L_3, (Action_1_t2564974692 *)L_6, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		return L_8;
	}
}
// System.Void Stream`1<UniRx.CollectionAddEvent`1<System.Object>>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Stream_1_Dispose_m3748601883_MetadataUsageId;
extern "C"  void Stream_1_Dispose_m3748601883_gshared (Stream_1_t1107661009 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Stream_1_Dispose_m3748601883_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_observer_0((Reactor_1_t1676326883 *)NULL);
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
	}

IL_001d:
	{
		__this->set_inputStreamConnection_1((Il2CppObject *)NULL);
		return;
	}
}
// System.Void Stream`1<UniRx.CollectionAddEvent`1<System.Object>>::SetInputStream(IStream`1<T>)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Stream_1_SetInputStream_m453976930_MetadataUsageId;
extern "C"  void Stream_1_SetInputStream_m453976930_gshared (Stream_1_t1107661009 * __this, Il2CppObject* ___stream0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Stream_1_SetInputStream_m453976930_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = ___stream0;
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		Action_1_t2564974692 * L_4 = (Action_1_t2564974692 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		((  void (*) (Action_1_t2564974692 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)(L_4, (Il2CppObject *)__this, (IntPtr_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_5 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t2564974692 *, int32_t >::Invoke(0 /* System.IDisposable IStream`1<UniRx.CollectionAddEvent`1<System.Object>>::Listen(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16), (Il2CppObject*)L_2, (Action_1_t2564974692 *)L_4, (int32_t)1);
		__this->set_inputStreamConnection_1(L_5);
		return;
	}
}
// System.Void Stream`1<UniRx.CollectionAddEvent`1<System.Object>>::ClearInputStream()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Stream_1_ClearInputStream_m1610565315_MetadataUsageId;
extern "C"  void Stream_1_ClearInputStream_m1610565315_gshared (Stream_1_t1107661009 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Stream_1_ClearInputStream_m1610565315_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
	}

IL_0016:
	{
		__this->set_inputStreamConnection_1((Il2CppObject *)NULL);
		return;
	}
}
// System.Void Stream`1<UniRx.CollectionAddEvent`1<System.Object>>::TransactionIterationFinished()
extern "C"  void Stream_1_TransactionIterationFinished_m477624855_gshared (Stream_1_t1107661009 * __this, const MethodInfo* method)
{
	{
		__this->set_holdedValues_2((List_1_t3213480956 *)NULL);
		return;
	}
}
// System.Void Stream`1<UniRx.CollectionAddEvent`1<System.Object>>::Unpack(System.Int32)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Stream_1_Unpack_m1846888937_MetadataUsageId;
extern "C"  void Stream_1_Unpack_m1846888937_gshared (Stream_1_t1107661009 * __this, int32_t ___p0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Stream_1_Unpack_m1846888937_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	CollectionAddEvent_1_t2416521987  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Enumerator_t1299263948  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t3213480956 * L_0 = (List_1_t3213480956 *)__this->get_holdedValues_2();
		NullCheck((List_1_t3213480956 *)L_0);
		Enumerator_t1299263948  L_1 = ((  Enumerator_t1299263948  (*) (List_1_t3213480956 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((List_1_t3213480956 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		V_1 = (Enumerator_t1299263948 )L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0026;
		}

IL_0011:
		{
			CollectionAddEvent_1_t2416521987  L_2 = ((  CollectionAddEvent_1_t2416521987  (*) (Enumerator_t1299263948 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->method)((Enumerator_t1299263948 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
			V_0 = (CollectionAddEvent_1_t2416521987 )L_2;
			Reactor_1_t1676326883 * L_3 = (Reactor_1_t1676326883 *)__this->get_observer_0();
			CollectionAddEvent_1_t2416521987  L_4 = V_0;
			int32_t L_5 = ___p0;
			NullCheck((Reactor_1_t1676326883 *)L_3);
			((  void (*) (Reactor_1_t1676326883 *, CollectionAddEvent_1_t2416521987 , int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((Reactor_1_t1676326883 *)L_3, (CollectionAddEvent_1_t2416521987 )L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		}

IL_0026:
		{
			bool L_6 = ((  bool (*) (Enumerator_t1299263948 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19)->method)((Enumerator_t1299263948 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
			if (L_6)
			{
				goto IL_0011;
			}
		}

IL_0032:
		{
			IL2CPP_LEAVE(0x43, FINALLY_0037);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0037;
	}

FINALLY_0037:
	{ // begin finally (depth: 1)
		Enumerator_t1299263948  L_7 = V_1;
		Enumerator_t1299263948  L_8 = L_7;
		Il2CppObject * L_9 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20), &L_8);
		NullCheck((Il2CppObject *)L_9);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_9);
		IL2CPP_END_FINALLY(55)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(55)
	{
		IL2CPP_JUMP_TBL(0x43, IL_0043)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0043:
	{
		return;
	}
}
// System.Void Stream`1<UniRx.CollectionAddEvent`1<System.Object>>::<SetInputStream>m__1BA(T)
extern "C"  void Stream_1_U3CSetInputStreamU3Em__1BA_m4229248635_gshared (Stream_1_t1107661009 * __this, CollectionAddEvent_1_t2416521987  ___val0, const MethodInfo* method)
{
	{
		Reactor_1_t1676326883 * L_0 = (Reactor_1_t1676326883 *)__this->get_observer_0();
		CollectionAddEvent_1_t2416521987  L_1 = ___val0;
		NullCheck((Reactor_1_t1676326883 *)L_0);
		((  void (*) (Reactor_1_t1676326883 *, CollectionAddEvent_1_t2416521987 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Reactor_1_t1676326883 *)L_0, (CollectionAddEvent_1_t2416521987 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return;
	}
}
// System.Void Stream`1<UniRx.Unit>::.ctor()
extern "C"  void Stream_1__ctor_m2832653796_gshared (Stream_1_t1249425060 * __this, const MethodInfo* method)
{
	{
		Reactor_1_t1818090934 * L_0 = (Reactor_1_t1818090934 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (Reactor_1_t1818090934 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->set_observer_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Stream`1<UniRx.Unit>::Send(T)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppClass* Transaction_t3809114814_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5960200;
extern const uint32_t Stream_1_Send_m1361871350_MetadataUsageId;
extern "C"  void Stream_1_Send_m1361871350_gshared (Stream_1_t1249425060 * __this, Unit_t2558286038  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Stream_1_Send_m1361871350_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
		__this->set_inputStreamConnection_1((Il2CppObject *)NULL);
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_2 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_calculationMode_8();
		if (!L_2)
		{
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m1836388681(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral5960200, /*hidden argument*/NULL);
	}

IL_0031:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_3 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_performing_5();
		if (!L_3)
		{
			goto IL_0056;
		}
	}
	{
		V_0 = (int32_t)0;
		goto IL_004e;
	}

IL_0042:
	{
		Unit_t2558286038  L_4 = ___value0;
		int32_t L_5 = V_0;
		NullCheck((Stream_1_t1249425060 *)__this);
		((  void (*) (Stream_1_t1249425060 *, Unit_t2558286038 , int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Stream_1_t1249425060 *)__this, (Unit_t2558286038 )L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		int32_t L_6 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_004e:
	{
		int32_t L_7 = V_0;
		if ((((int32_t)L_7) < ((int32_t)3)))
		{
			goto IL_0042;
		}
	}
	{
		return;
	}

IL_0056:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_8 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_hold_3();
		if (!L_8)
		{
			goto IL_0089;
		}
	}
	{
		List_1_t3355245007 * L_9 = (List_1_t3355245007 *)__this->get_holdedValues_2();
		if (L_9)
		{
			goto IL_0076;
		}
	}
	{
		List_1_t3355245007 * L_10 = (List_1_t3355245007 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (List_1_t3355245007 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		__this->set_holdedValues_2(L_10);
	}

IL_0076:
	{
		List_1_t3355245007 * L_11 = (List_1_t3355245007 *)__this->get_holdedValues_2();
		Unit_t2558286038  L_12 = ___value0;
		NullCheck((List_1_t3355245007 *)L_11);
		VirtActionInvoker1< Unit_t2558286038  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UniRx.Unit>::Add(!0) */, (List_1_t3355245007 *)L_11, (Unit_t2558286038 )L_12);
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		Transaction_RegisterPackedReaction_m1156829996(NULL /*static, unused*/, (Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}

IL_0089:
	{
		Reactor_1_t1818090934 * L_13 = (Reactor_1_t1818090934 *)__this->get_observer_0();
		Unit_t2558286038  L_14 = ___value0;
		NullCheck((Reactor_1_t1818090934 *)L_13);
		((  void (*) (Reactor_1_t1818090934 *, Unit_t2558286038 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Reactor_1_t1818090934 *)L_13, (Unit_t2558286038 )L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		bool L_15 = (bool)__this->get_autoDisconnectAfterEvent_4();
		if (!L_15)
		{
			goto IL_00ab;
		}
	}
	{
		Reactor_1_t1818090934 * L_16 = (Reactor_1_t1818090934 *)__this->get_observer_0();
		NullCheck((Reactor_1_t1818090934 *)L_16);
		((  void (*) (Reactor_1_t1818090934 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((Reactor_1_t1818090934 *)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
	}

IL_00ab:
	{
		return;
	}
}
// System.Void Stream`1<UniRx.Unit>::SendInTransaction(T,System.Int32)
extern "C"  void Stream_1_SendInTransaction_m2976517644_gshared (Stream_1_t1249425060 * __this, Unit_t2558286038  ___value0, int32_t ___p1, const MethodInfo* method)
{
	{
		Reactor_1_t1818090934 * L_0 = (Reactor_1_t1818090934 *)__this->get_observer_0();
		Unit_t2558286038  L_1 = ___value0;
		int32_t L_2 = ___p1;
		NullCheck((Reactor_1_t1818090934 *)L_0);
		((  void (*) (Reactor_1_t1818090934 *, Unit_t2558286038 , int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((Reactor_1_t1818090934 *)L_0, (Unit_t2558286038 )L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return;
	}
}
// System.IDisposable Stream`1<UniRx.Unit>::Listen(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Stream_1_Listen_m2898576392_gshared (Stream_1_t1249425060 * __this, Action_1_t2706738743 * ___action0, int32_t ___priority1, const MethodInfo* method)
{
	{
		Reactor_1_t1818090934 * L_0 = (Reactor_1_t1818090934 *)__this->get_observer_0();
		Action_1_t2706738743 * L_1 = ___action0;
		int32_t L_2 = ___priority1;
		NullCheck((Reactor_1_t1818090934 *)L_0);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Reactor_1_t1818090934 *, Action_1_t2706738743 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Reactor_1_t1818090934 *)L_0, (Action_1_t2706738743 *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		return L_3;
	}
}
// System.IDisposable Stream`1<UniRx.Unit>::Listen(System.Action,Priority)
extern "C"  Il2CppObject * Stream_1_Listen_m3884177439_gshared (Stream_1_t1249425060 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method)
{
	U3CListenU3Ec__AnonStorey128_t1789646062 * V_0 = NULL;
	{
		U3CListenU3Ec__AnonStorey128_t1789646062 * L_0 = (U3CListenU3Ec__AnonStorey128_t1789646062 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		((  void (*) (U3CListenU3Ec__AnonStorey128_t1789646062 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		V_0 = (U3CListenU3Ec__AnonStorey128_t1789646062 *)L_0;
		U3CListenU3Ec__AnonStorey128_t1789646062 * L_1 = V_0;
		Action_t437523947 * L_2 = ___action0;
		NullCheck(L_1);
		L_1->set_action_0(L_2);
		Reactor_1_t1818090934 * L_3 = (Reactor_1_t1818090934 *)__this->get_observer_0();
		U3CListenU3Ec__AnonStorey128_t1789646062 * L_4 = V_0;
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		Action_1_t2706738743 * L_6 = (Action_1_t2706738743 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		((  void (*) (Action_1_t2706738743 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)(L_6, (Il2CppObject *)L_4, (IntPtr_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		int32_t L_7 = ___p1;
		NullCheck((Reactor_1_t1818090934 *)L_3);
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (Reactor_1_t1818090934 *, Action_1_t2706738743 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Reactor_1_t1818090934 *)L_3, (Action_1_t2706738743 *)L_6, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		return L_8;
	}
}
// System.Void Stream`1<UniRx.Unit>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Stream_1_Dispose_m3364282657_MetadataUsageId;
extern "C"  void Stream_1_Dispose_m3364282657_gshared (Stream_1_t1249425060 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Stream_1_Dispose_m3364282657_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_observer_0((Reactor_1_t1818090934 *)NULL);
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
	}

IL_001d:
	{
		__this->set_inputStreamConnection_1((Il2CppObject *)NULL);
		return;
	}
}
// System.Void Stream`1<UniRx.Unit>::SetInputStream(IStream`1<T>)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Stream_1_SetInputStream_m1157679260_MetadataUsageId;
extern "C"  void Stream_1_SetInputStream_m1157679260_gshared (Stream_1_t1249425060 * __this, Il2CppObject* ___stream0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Stream_1_SetInputStream_m1157679260_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
	}

IL_0016:
	{
		Il2CppObject* L_2 = ___stream0;
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		Action_1_t2706738743 * L_4 = (Action_1_t2706738743 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		((  void (*) (Action_1_t2706738743 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)(L_4, (Il2CppObject *)__this, (IntPtr_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_5 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t2706738743 *, int32_t >::Invoke(0 /* System.IDisposable IStream`1<UniRx.Unit>::Listen(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16), (Il2CppObject*)L_2, (Action_1_t2706738743 *)L_4, (int32_t)1);
		__this->set_inputStreamConnection_1(L_5);
		return;
	}
}
// System.Void Stream`1<UniRx.Unit>::ClearInputStream()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Stream_1_ClearInputStream_m3637070973_MetadataUsageId;
extern "C"  void Stream_1_ClearInputStream_m3637070973_gshared (Stream_1_t1249425060 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Stream_1_ClearInputStream_m3637070973_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_inputStreamConnection_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
	}

IL_0016:
	{
		__this->set_inputStreamConnection_1((Il2CppObject *)NULL);
		return;
	}
}
// System.Void Stream`1<UniRx.Unit>::TransactionIterationFinished()
extern "C"  void Stream_1_TransactionIterationFinished_m2425698513_gshared (Stream_1_t1249425060 * __this, const MethodInfo* method)
{
	{
		__this->set_holdedValues_2((List_1_t3355245007 *)NULL);
		return;
	}
}
// System.Void Stream`1<UniRx.Unit>::Unpack(System.Int32)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Stream_1_Unpack_m3698641187_MetadataUsageId;
extern "C"  void Stream_1_Unpack_m3698641187_gshared (Stream_1_t1249425060 * __this, int32_t ___p0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Stream_1_Unpack_m3698641187_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Unit_t2558286038  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Enumerator_t1441027999  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t3355245007 * L_0 = (List_1_t3355245007 *)__this->get_holdedValues_2();
		NullCheck((List_1_t3355245007 *)L_0);
		Enumerator_t1441027999  L_1 = ((  Enumerator_t1441027999  (*) (List_1_t3355245007 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((List_1_t3355245007 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		V_1 = (Enumerator_t1441027999 )L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0026;
		}

IL_0011:
		{
			Unit_t2558286038  L_2 = ((  Unit_t2558286038  (*) (Enumerator_t1441027999 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->method)((Enumerator_t1441027999 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
			V_0 = (Unit_t2558286038 )L_2;
			Reactor_1_t1818090934 * L_3 = (Reactor_1_t1818090934 *)__this->get_observer_0();
			Unit_t2558286038  L_4 = V_0;
			int32_t L_5 = ___p0;
			NullCheck((Reactor_1_t1818090934 *)L_3);
			((  void (*) (Reactor_1_t1818090934 *, Unit_t2558286038 , int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((Reactor_1_t1818090934 *)L_3, (Unit_t2558286038 )L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		}

IL_0026:
		{
			bool L_6 = ((  bool (*) (Enumerator_t1441027999 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19)->method)((Enumerator_t1441027999 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
			if (L_6)
			{
				goto IL_0011;
			}
		}

IL_0032:
		{
			IL2CPP_LEAVE(0x43, FINALLY_0037);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0037;
	}

FINALLY_0037:
	{ // begin finally (depth: 1)
		Enumerator_t1441027999  L_7 = V_1;
		Enumerator_t1441027999  L_8 = L_7;
		Il2CppObject * L_9 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20), &L_8);
		NullCheck((Il2CppObject *)L_9);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_9);
		IL2CPP_END_FINALLY(55)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(55)
	{
		IL2CPP_JUMP_TBL(0x43, IL_0043)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0043:
	{
		return;
	}
}
// System.Void Stream`1<UniRx.Unit>::<SetInputStream>m__1BA(T)
extern "C"  void Stream_1_U3CSetInputStreamU3Em__1BA_m3414567809_gshared (Stream_1_t1249425060 * __this, Unit_t2558286038  ___val0, const MethodInfo* method)
{
	{
		Reactor_1_t1818090934 * L_0 = (Reactor_1_t1818090934 *)__this->get_observer_0();
		Unit_t2558286038  L_1 = ___val0;
		NullCheck((Reactor_1_t1818090934 *)L_0);
		((  void (*) (Reactor_1_t1818090934 *, Unit_t2558286038 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Reactor_1_t1818090934 *)L_0, (Unit_t2558286038 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return;
	}
}
// System.Void StreamAPI/<Filter>c__AnonStorey134`1/<Filter>c__AnonStorey135`1<System.Object>::.ctor()
extern "C"  void U3CFilterU3Ec__AnonStorey135_1__ctor_m3374779706_gshared (U3CFilterU3Ec__AnonStorey135_1_t999690120 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StreamAPI/<Filter>c__AnonStorey134`1/<Filter>c__AnonStorey135`1<System.Object>::<>m__1CD(T)
extern "C"  void U3CFilterU3Ec__AnonStorey135_1_U3CU3Em__1CD_m97491693_gshared (U3CFilterU3Ec__AnonStorey135_1_t999690120 * __this, Il2CppObject * ___val0, const MethodInfo* method)
{
	{
		U3CFilterU3Ec__AnonStorey134_1_t1444715119 * L_0 = (U3CFilterU3Ec__AnonStorey134_1_t1444715119 *)__this->get_U3CU3Ef__refU24308_1();
		NullCheck(L_0);
		Func_2_t1509682273 * L_1 = (Func_2_t1509682273 *)L_0->get_filter_1();
		Il2CppObject * L_2 = ___val0;
		NullCheck((Func_2_t1509682273 *)L_1);
		bool L_3 = ((  bool (*) (Func_2_t1509682273 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Func_2_t1509682273 *)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		if (!L_3)
		{
			goto IL_0022;
		}
	}
	{
		Action_1_t985559125 * L_4 = (Action_1_t985559125 *)__this->get_reaction_0();
		Il2CppObject * L_5 = ___val0;
		NullCheck((Action_1_t985559125 *)L_4);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Action_1_t985559125 *)L_4, (Il2CppObject *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
	}

IL_0022:
	{
		return;
	}
}
// System.Void StreamAPI/<Filter>c__AnonStorey134`1<System.Object>::.ctor()
extern "C"  void U3CFilterU3Ec__AnonStorey134_1__ctor_m3863718218_gshared (U3CFilterU3Ec__AnonStorey134_1_t1444715119 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.IDisposable StreamAPI/<Filter>c__AnonStorey134`1<System.Object>::<>m__1C5(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * U3CFilterU3Ec__AnonStorey134_1_U3CU3Em__1C5_m262601123_gshared (U3CFilterU3Ec__AnonStorey134_1_t1444715119 * __this, Action_1_t985559125 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	U3CFilterU3Ec__AnonStorey135_1_t999690120 * V_0 = NULL;
	{
		U3CFilterU3Ec__AnonStorey135_1_t999690120 * L_0 = (U3CFilterU3Ec__AnonStorey135_1_t999690120 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CFilterU3Ec__AnonStorey135_1_t999690120 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CFilterU3Ec__AnonStorey135_1_t999690120 *)L_0;
		U3CFilterU3Ec__AnonStorey135_1_t999690120 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU24308_1(__this);
		U3CFilterU3Ec__AnonStorey135_1_t999690120 * L_2 = V_0;
		Action_1_t985559125 * L_3 = ___reaction0;
		NullCheck(L_2);
		L_2->set_reaction_0(L_3);
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_stream_0();
		U3CFilterU3Ec__AnonStorey135_1_t999690120 * L_5 = V_0;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_1_t985559125 * L_7 = (Action_1_t985559125 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_7, (Il2CppObject *)L_5, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		int32_t L_8 = ___p1;
		NullCheck((Il2CppObject*)L_4);
		Il2CppObject * L_9 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t985559125 *, int32_t >::Invoke(0 /* System.IDisposable IStream`1<System.Object>::Listen(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_4, (Action_1_t985559125 *)L_7, (int32_t)L_8);
		return L_9;
	}
}
// System.Void StreamAPI/<FirstOnly>c__AnonStorey136`1/<FirstOnly>c__AnonStorey137`1<System.Object>::.ctor()
extern "C"  void U3CFirstOnlyU3Ec__AnonStorey137_1__ctor_m630990004_gshared (U3CFirstOnlyU3Ec__AnonStorey137_1_t4126677326 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StreamAPI/<FirstOnly>c__AnonStorey136`1/<FirstOnly>c__AnonStorey137`1<System.Object>::<>m__1CE(T)
extern "C"  void U3CFirstOnlyU3Ec__AnonStorey137_1_U3CU3Em__1CE_m1788402118_gshared (U3CFirstOnlyU3Ec__AnonStorey137_1_t4126677326 * __this, Il2CppObject * ___val0, const MethodInfo* method)
{
	{
		Action_1_t985559125 * L_0 = (Action_1_t985559125 *)__this->get_reaction_0();
		Il2CppObject * L_1 = ___val0;
		NullCheck((Action_1_t985559125 *)L_0);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Action_1_t985559125 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		SingleAssignmentDisposable_t1832432170 * L_2 = (SingleAssignmentDisposable_t1832432170 *)__this->get_disp_1();
		NullCheck((SingleAssignmentDisposable_t1832432170 *)L_2);
		VirtActionInvoker0::Invoke(4 /* System.Void CellUtils.SingleAssignmentDisposable::Dispose() */, (SingleAssignmentDisposable_t1832432170 *)L_2);
		return;
	}
}
// System.Void StreamAPI/<FirstOnly>c__AnonStorey136`1<System.Object>::.ctor()
extern "C"  void U3CFirstOnlyU3Ec__AnonStorey136_1__ctor_m1609024998_gshared (U3CFirstOnlyU3Ec__AnonStorey136_1_t276735029 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.IDisposable StreamAPI/<FirstOnly>c__AnonStorey136`1<System.Object>::<>m__1C6(System.Action`1<T>,Priority)
extern Il2CppClass* SingleAssignmentDisposable_t1832432170_il2cpp_TypeInfo_var;
extern const uint32_t U3CFirstOnlyU3Ec__AnonStorey136_1_U3CU3Em__1C6_m268636888_MetadataUsageId;
extern "C"  Il2CppObject * U3CFirstOnlyU3Ec__AnonStorey136_1_U3CU3Em__1C6_m268636888_gshared (U3CFirstOnlyU3Ec__AnonStorey136_1_t276735029 * __this, Action_1_t985559125 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CFirstOnlyU3Ec__AnonStorey136_1_U3CU3Em__1C6_m268636888_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CFirstOnlyU3Ec__AnonStorey137_1_t4126677326 * V_0 = NULL;
	{
		U3CFirstOnlyU3Ec__AnonStorey137_1_t4126677326 * L_0 = (U3CFirstOnlyU3Ec__AnonStorey137_1_t4126677326 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CFirstOnlyU3Ec__AnonStorey137_1_t4126677326 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CFirstOnlyU3Ec__AnonStorey137_1_t4126677326 *)L_0;
		U3CFirstOnlyU3Ec__AnonStorey137_1_t4126677326 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU24310_2(__this);
		U3CFirstOnlyU3Ec__AnonStorey137_1_t4126677326 * L_2 = V_0;
		Action_1_t985559125 * L_3 = ___reaction0;
		NullCheck(L_2);
		L_2->set_reaction_0(L_3);
		U3CFirstOnlyU3Ec__AnonStorey137_1_t4126677326 * L_4 = V_0;
		SingleAssignmentDisposable_t1832432170 * L_5 = (SingleAssignmentDisposable_t1832432170 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t1832432170_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m2805131143(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_disp_1(L_5);
		U3CFirstOnlyU3Ec__AnonStorey137_1_t4126677326 * L_6 = V_0;
		NullCheck(L_6);
		SingleAssignmentDisposable_t1832432170 * L_7 = (SingleAssignmentDisposable_t1832432170 *)L_6->get_disp_1();
		Il2CppObject* L_8 = (Il2CppObject*)__this->get_stream_0();
		U3CFirstOnlyU3Ec__AnonStorey137_1_t4126677326 * L_9 = V_0;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_1_t985559125 * L_11 = (Action_1_t985559125 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		int32_t L_12 = ___p1;
		NullCheck((Il2CppObject*)L_8);
		Il2CppObject * L_13 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t985559125 *, int32_t >::Invoke(0 /* System.IDisposable IStream`1<System.Object>::Listen(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_8, (Action_1_t985559125 *)L_11, (int32_t)L_12);
		NullCheck((SingleAssignmentDisposable_t1832432170 *)L_7);
		SingleAssignmentDisposable_set_Disposable_m1918433680((SingleAssignmentDisposable_t1832432170 *)L_7, (Il2CppObject *)L_13, /*hidden argument*/NULL);
		U3CFirstOnlyU3Ec__AnonStorey137_1_t4126677326 * L_14 = V_0;
		NullCheck(L_14);
		SingleAssignmentDisposable_t1832432170 * L_15 = (SingleAssignmentDisposable_t1832432170 *)L_14->get_disp_1();
		return L_15;
	}
}
// System.Void StreamAPI/<ListenQueue>c__AnonStorey12D`1<System.Object>::.ctor()
extern "C"  void U3CListenQueueU3Ec__AnonStorey12D_1__ctor_m2689374087_gshared (U3CListenQueueU3Ec__AnonStorey12D_1_t38028782 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StreamAPI/<ListenQueue>c__AnonStorey12D`1<System.Object>::<>m__1C1(T)
extern "C"  void U3CListenQueueU3Ec__AnonStorey12D_1_U3CU3Em__1C1_m1599378349_gshared (U3CListenQueueU3Ec__AnonStorey12D_1_t38028782 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_collection_0();
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Action`1<System.Object>>::get_Count() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_0);
		if (L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_collection_0();
		Action_1_t985559125 * L_3 = ((  Action_1_t985559125 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject * L_4 = ___obj0;
		NullCheck((Action_1_t985559125 *)L_3);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Action_1_t985559125 *)L_3, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
// System.Void StreamAPI/<Map>c__AnonStorey130`2/<Map>c__AnonStorey131`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CMapU3Ec__AnonStorey131_2__ctor_m2740834076_gshared (U3CMapU3Ec__AnonStorey131_2_t3825613791 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StreamAPI/<Map>c__AnonStorey130`2/<Map>c__AnonStorey131`2<System.Object,System.Object>::<>m__1CB(T)
extern "C"  void U3CMapU3Ec__AnonStorey131_2_U3CU3Em__1CB_m2167255825_gshared (U3CMapU3Ec__AnonStorey131_2_t3825613791 * __this, Il2CppObject * ___val0, const MethodInfo* method)
{
	{
		Action_1_t985559125 * L_0 = (Action_1_t985559125 *)__this->get_reaction_0();
		U3CMapU3Ec__AnonStorey130_2_t2728082812 * L_1 = (U3CMapU3Ec__AnonStorey130_2_t2728082812 *)__this->get_U3CU3Ef__refU24304_1();
		NullCheck(L_1);
		Func_2_t2135783352 * L_2 = (Func_2_t2135783352 *)L_1->get_map_1();
		Il2CppObject * L_3 = ___val0;
		NullCheck((Func_2_t2135783352 *)L_2);
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (Func_2_t2135783352 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Func_2_t2135783352 *)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((Action_1_t985559125 *)L_0);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Action_1_t985559125 *)L_0, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
// System.Void StreamAPI/<Map>c__AnonStorey130`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CMapU3Ec__AnonStorey130_2__ctor_m1735843443_gshared (U3CMapU3Ec__AnonStorey130_2_t2728082812 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.IDisposable StreamAPI/<Map>c__AnonStorey130`2<System.Object,System.Object>::<>m__1C3(System.Action`1<T2>,Priority)
extern "C"  Il2CppObject * U3CMapU3Ec__AnonStorey130_2_U3CU3Em__1C3_m2514345428_gshared (U3CMapU3Ec__AnonStorey130_2_t2728082812 * __this, Action_1_t985559125 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	U3CMapU3Ec__AnonStorey131_2_t3825613791 * V_0 = NULL;
	{
		U3CMapU3Ec__AnonStorey131_2_t3825613791 * L_0 = (U3CMapU3Ec__AnonStorey131_2_t3825613791 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CMapU3Ec__AnonStorey131_2_t3825613791 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CMapU3Ec__AnonStorey131_2_t3825613791 *)L_0;
		U3CMapU3Ec__AnonStorey131_2_t3825613791 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU24304_1(__this);
		U3CMapU3Ec__AnonStorey131_2_t3825613791 * L_2 = V_0;
		Action_1_t985559125 * L_3 = ___reaction0;
		NullCheck(L_2);
		L_2->set_reaction_0(L_3);
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_stream_0();
		U3CMapU3Ec__AnonStorey131_2_t3825613791 * L_5 = V_0;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_1_t985559125 * L_7 = (Action_1_t985559125 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_7, (Il2CppObject *)L_5, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		int32_t L_8 = ___p1;
		NullCheck((Il2CppObject*)L_4);
		Il2CppObject * L_9 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t985559125 *, int32_t >::Invoke(0 /* System.IDisposable IStream`1<System.Object>::Listen(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_4, (Action_1_t985559125 *)L_7, (int32_t)L_8);
		return L_9;
	}
}
// System.Void StreamAPI/<MergeWith>c__AnonStorey139`1<System.Object>::.ctor()
extern "C"  void U3CMergeWithU3Ec__AnonStorey139_1__ctor_m3507914823_gshared (U3CMergeWithU3Ec__AnonStorey139_1_t1732385262 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.IDisposable StreamAPI/<MergeWith>c__AnonStorey139`1<System.Object>::<>m__1C8(System.Action`1<T>,Priority)
extern Il2CppClass* ListDisposable_t2393830995_il2cpp_TypeInfo_var;
extern const uint32_t U3CMergeWithU3Ec__AnonStorey139_1_U3CU3Em__1C8_m301960375_MetadataUsageId;
extern "C"  Il2CppObject * U3CMergeWithU3Ec__AnonStorey139_1_U3CU3Em__1C8_m301960375_gshared (U3CMergeWithU3Ec__AnonStorey139_1_t1732385262 * __this, Action_1_t985559125 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CMergeWithU3Ec__AnonStorey139_1_U3CU3Em__1C8_m301960375_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ListDisposable_t2393830995 * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	IStream_1U5BU5D_t3844023282* V_2 = NULL;
	int32_t V_3 = 0;
	{
		ListDisposable_t2393830995 * L_0 = (ListDisposable_t2393830995 *)il2cpp_codegen_object_new(ListDisposable_t2393830995_il2cpp_TypeInfo_var);
		ListDisposable__ctor_m4289441534(L_0, /*hidden argument*/NULL);
		V_0 = (ListDisposable_t2393830995 *)L_0;
		ListDisposable_t2393830995 * L_1 = V_0;
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_stream_0();
		Action_1_t985559125 * L_3 = ___reaction0;
		int32_t L_4 = ___p1;
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_5 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t985559125 *, int32_t >::Invoke(0 /* System.IDisposable IStream`1<System.Object>::Listen(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_2, (Action_1_t985559125 *)L_3, (int32_t)L_4);
		NullCheck((ListDisposable_t2393830995 *)L_1);
		VirtActionInvoker1< Il2CppObject * >::Invoke(9 /* System.Void CellUtils.ListDisposable::Add(System.IDisposable) */, (ListDisposable_t2393830995 *)L_1, (Il2CppObject *)L_5);
		IStream_1U5BU5D_t3844023282* L_6 = (IStream_1U5BU5D_t3844023282*)__this->get_others_1();
		V_2 = (IStream_1U5BU5D_t3844023282*)L_6;
		V_3 = (int32_t)0;
		goto IL_003d;
	}

IL_0027:
	{
		IStream_1U5BU5D_t3844023282* L_7 = V_2;
		int32_t L_8 = V_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		V_1 = (Il2CppObject*)((L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9)));
		ListDisposable_t2393830995 * L_10 = V_0;
		Il2CppObject* L_11 = V_1;
		Action_1_t985559125 * L_12 = ___reaction0;
		int32_t L_13 = ___p1;
		NullCheck((Il2CppObject*)L_11);
		Il2CppObject * L_14 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t985559125 *, int32_t >::Invoke(0 /* System.IDisposable IStream`1<System.Object>::Listen(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_11, (Action_1_t985559125 *)L_12, (int32_t)L_13);
		NullCheck((ListDisposable_t2393830995 *)L_10);
		VirtActionInvoker1< Il2CppObject * >::Invoke(9 /* System.Void CellUtils.ListDisposable::Add(System.IDisposable) */, (ListDisposable_t2393830995 *)L_10, (Il2CppObject *)L_14);
		int32_t L_15 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_003d:
	{
		int32_t L_16 = V_3;
		IStream_1U5BU5D_t3844023282* L_17 = V_2;
		NullCheck(L_17);
		if ((((int32_t)L_16) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_17)->max_length)))))))
		{
			goto IL_0027;
		}
	}
	{
		ListDisposable_t2393830995 * L_18 = V_0;
		return L_18;
	}
}
// System.Void StreamAPI/<ToEmpty>c__AnonStorey132`1/<ToEmpty>c__AnonStorey133`1<System.Object>::.ctor()
extern "C"  void U3CToEmptyU3Ec__AnonStorey133_1__ctor_m1027732160_gshared (U3CToEmptyU3Ec__AnonStorey133_1_t2600382340 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StreamAPI/<ToEmpty>c__AnonStorey132`1/<ToEmpty>c__AnonStorey133`1<System.Object>::<>m__1CC(T)
extern "C"  void U3CToEmptyU3Ec__AnonStorey133_1_U3CU3Em__1CC_m2135939348_gshared (U3CToEmptyU3Ec__AnonStorey133_1_t2600382340 * __this, Il2CppObject * ____0, const MethodInfo* method)
{
	{
		Action_t437523947 * L_0 = (Action_t437523947 *)__this->get_reaction_0();
		NullCheck((Action_t437523947 *)L_0);
		Action_Invoke_m1445970038((Action_t437523947 *)L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StreamAPI/<ToEmpty>c__AnonStorey132`1<System.Object>::.ctor()
extern "C"  void U3CToEmptyU3Ec__AnonStorey132_1__ctor_m3593844684_gshared (U3CToEmptyU3Ec__AnonStorey132_1_t3045407339 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.IDisposable StreamAPI/<ToEmpty>c__AnonStorey132`1<System.Object>::<>m__1C4(System.Action,Priority)
extern "C"  Il2CppObject * U3CToEmptyU3Ec__AnonStorey132_1_U3CU3Em__1C4_m1494221543_gshared (U3CToEmptyU3Ec__AnonStorey132_1_t3045407339 * __this, Action_t437523947 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	U3CToEmptyU3Ec__AnonStorey133_1_t2600382340 * V_0 = NULL;
	{
		U3CToEmptyU3Ec__AnonStorey133_1_t2600382340 * L_0 = (U3CToEmptyU3Ec__AnonStorey133_1_t2600382340 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CToEmptyU3Ec__AnonStorey133_1_t2600382340 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CToEmptyU3Ec__AnonStorey133_1_t2600382340 *)L_0;
		U3CToEmptyU3Ec__AnonStorey133_1_t2600382340 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU24306_1(__this);
		U3CToEmptyU3Ec__AnonStorey133_1_t2600382340 * L_2 = V_0;
		Action_t437523947 * L_3 = ___reaction0;
		NullCheck(L_2);
		L_2->set_reaction_0(L_3);
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_stream_0();
		U3CToEmptyU3Ec__AnonStorey133_1_t2600382340 * L_5 = V_0;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_1_t985559125 * L_7 = (Action_1_t985559125 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_7, (Il2CppObject *)L_5, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		int32_t L_8 = ___p1;
		NullCheck((Il2CppObject*)L_4);
		Il2CppObject * L_9 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t985559125 *, int32_t >::Invoke(0 /* System.IDisposable IStream`1<System.Object>::Listen(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_4, (Action_1_t985559125 *)L_7, (int32_t)L_8);
		return L_9;
	}
}
// System.Void StreamEgoizm`1/<DoThis>c__AnonStorey12B<System.Object>::.ctor()
extern "C"  void U3CDoThisU3Ec__AnonStorey12B__ctor_m1274937601_gshared (U3CDoThisU3Ec__AnonStorey12B_t2640087060 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StreamEgoizm`1/<DoThis>c__AnonStorey12B<System.Object>::<>m__1BD()
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1186369571;
extern const uint32_t U3CDoThisU3Ec__AnonStorey12B_U3CU3Em__1BD_m4100598539_MetadataUsageId;
extern "C"  void U3CDoThisU3Ec__AnonStorey12B_U3CU3Em__1BD_m4100598539_gshared (U3CDoThisU3Ec__AnonStorey12B_t2640087060 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CDoThisU3Ec__AnonStorey12B_U3CU3Em__1BD_m4100598539_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StreamEgoizm_1_t3012409721 * L_0 = (StreamEgoizm_1_t3012409721 *)__this->get_U3CU3Ef__this_1();
		NullCheck(L_0);
		Stack_1_t1890507522 * L_1 = (Stack_1_t1890507522 *)L_0->get__subscribes_0();
		KeyValuePair_2_t3615068783  L_2 = (KeyValuePair_2_t3615068783 )__this->get_tuple_0();
		NullCheck((Stack_1_t1890507522 *)L_1);
		bool L_3 = ((  bool (*) (Stack_1_t1890507522 *, KeyValuePair_2_t3615068783 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Stack_1_t1890507522 *)L_1, (KeyValuePair_2_t3615068783 )L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		if (!L_3)
		{
			goto IL_0091;
		}
	}
	{
		StreamEgoizm_1_t3012409721 * L_4 = (StreamEgoizm_1_t3012409721 *)__this->get_U3CU3Ef__this_1();
		NullCheck(L_4);
		Stack_1_t1890507522 * L_5 = (Stack_1_t1890507522 *)L_4->get__subscribes_0();
		KeyValuePair_2_t3615068783  L_6 = ((  KeyValuePair_2_t3615068783  (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, (Il2CppObject*)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		KeyValuePair_2_t3615068783  L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_7);
		KeyValuePair_2_t3615068783  L_9 = (KeyValuePair_2_t3615068783 )__this->get_tuple_0();
		KeyValuePair_2_t3615068783  L_10 = L_9;
		Il2CppObject * L_11 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_10);
		bool L_12 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_8, (Il2CppObject *)L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_005b;
		}
	}
	{
		StreamEgoizm_1_t3012409721 * L_13 = (StreamEgoizm_1_t3012409721 *)__this->get_U3CU3Ef__this_1();
		NullCheck(L_13);
		Stack_1_t1890507522 * L_14 = (Stack_1_t1890507522 *)L_13->get__subscribes_0();
		NullCheck((Stack_1_t1890507522 *)L_14);
		((  KeyValuePair_2_t3615068783  (*) (Stack_1_t1890507522 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Stack_1_t1890507522 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		goto IL_0091;
	}

IL_005b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m3349710197(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1186369571, /*hidden argument*/NULL);
		StreamEgoizm_1_t3012409721 * L_15 = (StreamEgoizm_1_t3012409721 *)__this->get_U3CU3Ef__this_1();
		StreamEgoizm_1_t3012409721 * L_16 = (StreamEgoizm_1_t3012409721 *)__this->get_U3CU3Ef__this_1();
		NullCheck(L_16);
		Stack_1_t1890507522 * L_17 = (Stack_1_t1890507522 *)L_16->get__subscribes_0();
		IntPtr_t L_18;
		L_18.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Func_2_t3244071066 * L_19 = (Func_2_t3244071066 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (Func_2_t3244071066 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_19, (Il2CppObject *)__this, (IntPtr_t)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		Il2CppObject* L_20 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t3244071066 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(NULL /*static, unused*/, (Il2CppObject*)L_17, (Func_2_t3244071066 *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		Stack_1_t1890507522 * L_21 = (Stack_1_t1890507522 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		((  void (*) (Stack_1_t1890507522 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(L_21, (Il2CppObject*)L_20, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		NullCheck(L_15);
		L_15->set__subscribes_0(L_21);
	}

IL_0091:
	{
		return;
	}
}
// System.Boolean StreamEgoizm`1/<DoThis>c__AnonStorey12B<System.Object>::<>m__1BE(System.Collections.Generic.KeyValuePair`2<System.IDisposable,System.Action`1<T>>)
extern "C"  bool U3CDoThisU3Ec__AnonStorey12B_U3CU3Em__1BE_m3459815349_gshared (U3CDoThisU3Ec__AnonStorey12B_t2640087060 * __this, KeyValuePair_2_t3615068783  ___tuple10, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3615068783  L_0 = ___tuple10;
		KeyValuePair_2_t3615068783  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		KeyValuePair_2_t3615068783  L_3 = (KeyValuePair_2_t3615068783 )__this->get_tuple_0();
		KeyValuePair_2_t3615068783  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_4);
		bool L_6 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_2, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_6) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void StreamEgoizm`1/<Listen>c__AnonStorey129<System.Object>::.ctor()
extern "C"  void U3CListenU3Ec__AnonStorey129__ctor_m467396026_gshared (U3CListenU3Ec__AnonStorey129_t1735040613 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StreamEgoizm`1/<Listen>c__AnonStorey129<System.Object>::<>m__1BB(T)
extern "C"  void U3CListenU3Ec__AnonStorey129_U3CU3Em__1BB_m3377804078_gshared (U3CListenU3Ec__AnonStorey129_t1735040613 * __this, Il2CppObject * ____0, const MethodInfo* method)
{
	{
		Action_t437523947 * L_0 = (Action_t437523947 *)__this->get_action_0();
		NullCheck((Action_t437523947 *)L_0);
		Action_Invoke_m1445970038((Action_t437523947 *)L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StreamEgoizm`1/<Listen>c__AnonStorey12A<System.Object>::.ctor()
extern "C"  void U3CListenU3Ec__AnonStorey12A__ctor_m1029590466_gshared (U3CListenU3Ec__AnonStorey12A_t2182732077 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StreamEgoizm`1/<Listen>c__AnonStorey12A<System.Object>::<>m__1BC(T)
extern "C"  void U3CListenU3Ec__AnonStorey12A_U3CU3Em__1BC_m332712853_gshared (U3CListenU3Ec__AnonStorey12A_t2182732077 * __this, Il2CppObject * ____0, const MethodInfo* method)
{
	{
		Action_1_t985559125 * L_0 = (Action_1_t985559125 *)__this->get_action_0();
		Il2CppObject * L_1 = ____0;
		NullCheck((Action_1_t985559125 *)L_0);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Action_1_t985559125 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void StreamEgoizm`1<System.Object>::.ctor()
extern "C"  void StreamEgoizm_1__ctor_m3384859535_gshared (StreamEgoizm_1_t3012409721 * __this, const MethodInfo* method)
{
	{
		Stack_1_t1890507522 * L_0 = (Stack_1_t1890507522 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (Stack_1_t1890507522 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->set__subscribes_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.IDisposable StreamEgoizm`1<System.Object>::Listen(System.Action,Priority)
extern "C"  Il2CppObject * StreamEgoizm_1_Listen_m3593449934_gshared (StreamEgoizm_1_t3012409721 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method)
{
	U3CListenU3Ec__AnonStorey129_t1735040613 * V_0 = NULL;
	{
		U3CListenU3Ec__AnonStorey129_t1735040613 * L_0 = (U3CListenU3Ec__AnonStorey129_t1735040613 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (U3CListenU3Ec__AnonStorey129_t1735040613 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (U3CListenU3Ec__AnonStorey129_t1735040613 *)L_0;
		U3CListenU3Ec__AnonStorey129_t1735040613 * L_1 = V_0;
		Action_t437523947 * L_2 = ___action0;
		NullCheck(L_1);
		L_1->set_action_0(L_2);
		U3CListenU3Ec__AnonStorey129_t1735040613 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Action_1_t985559125 * L_5 = (Action_1_t985559125 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_5, (Il2CppObject *)L_3, (IntPtr_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		NullCheck((StreamEgoizm_1_t3012409721 *)__this);
		Il2CppObject * L_6 = ((  Il2CppObject * (*) (StreamEgoizm_1_t3012409721 *, Action_1_t985559125 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((StreamEgoizm_1_t3012409721 *)__this, (Action_1_t985559125 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return L_6;
	}
}
// System.IDisposable StreamEgoizm`1<System.Object>::Listen(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * StreamEgoizm_1_Listen_m2805505977_gshared (StreamEgoizm_1_t3012409721 * __this, Action_1_t985559125 * ___action0, int32_t ___priority1, const MethodInfo* method)
{
	U3CListenU3Ec__AnonStorey12A_t2182732077 * V_0 = NULL;
	{
		U3CListenU3Ec__AnonStorey12A_t2182732077 * L_0 = (U3CListenU3Ec__AnonStorey12A_t2182732077 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		((  void (*) (U3CListenU3Ec__AnonStorey12A_t2182732077 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		V_0 = (U3CListenU3Ec__AnonStorey12A_t2182732077 *)L_0;
		U3CListenU3Ec__AnonStorey12A_t2182732077 * L_1 = V_0;
		Action_1_t985559125 * L_2 = ___action0;
		NullCheck(L_1);
		L_1->set_action_0(L_2);
		U3CListenU3Ec__AnonStorey12A_t2182732077 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		Action_1_t985559125 * L_5 = (Action_1_t985559125 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_5, (Il2CppObject *)L_3, (IntPtr_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		NullCheck((StreamEgoizm_1_t3012409721 *)__this);
		Il2CppObject * L_6 = ((  Il2CppObject * (*) (StreamEgoizm_1_t3012409721 *, Action_1_t985559125 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((StreamEgoizm_1_t3012409721 *)__this, (Action_1_t985559125 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return L_6;
	}
}
// System.IDisposable StreamEgoizm`1<System.Object>::DoThis(System.Action`1<T>)
extern Il2CppClass* CompositeDisposable_t1894629977_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const uint32_t StreamEgoizm_1_DoThis_m2826120107_MetadataUsageId;
extern "C"  Il2CppObject * StreamEgoizm_1_DoThis_m2826120107_gshared (StreamEgoizm_1_t3012409721 * __this, Action_1_t985559125 * ___action0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StreamEgoizm_1_DoThis_m2826120107_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	U3CDoThisU3Ec__AnonStorey12B_t2640087060 * V_2 = NULL;
	{
		U3CDoThisU3Ec__AnonStorey12B_t2640087060 * L_0 = (U3CDoThisU3Ec__AnonStorey12B_t2640087060 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		((  void (*) (U3CDoThisU3Ec__AnonStorey12B_t2640087060 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		V_2 = (U3CDoThisU3Ec__AnonStorey12B_t2640087060 *)L_0;
		U3CDoThisU3Ec__AnonStorey12B_t2640087060 * L_1 = V_2;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_1(__this);
		CompositeDisposable_t1894629977 * L_2 = (CompositeDisposable_t1894629977 *)il2cpp_codegen_object_new(CompositeDisposable_t1894629977_il2cpp_TypeInfo_var);
		CompositeDisposable__ctor_m1237994920(L_2, /*hidden argument*/NULL);
		V_0 = (Il2CppObject *)L_2;
		U3CDoThisU3Ec__AnonStorey12B_t2640087060 * L_3 = V_2;
		Il2CppObject * L_4 = V_0;
		Action_1_t985559125 * L_5 = ___action0;
		KeyValuePair_2_t3615068783  L_6;
		memset(&L_6, 0, sizeof(L_6));
		((  void (*) (KeyValuePair_2_t3615068783 *, Il2CppObject *, Action_1_t985559125 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)(&L_6, (Il2CppObject *)L_4, (Action_1_t985559125 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		NullCheck(L_3);
		L_3->set_tuple_0(L_6);
		Stack_1_t1890507522 * L_7 = (Stack_1_t1890507522 *)__this->get__subscribes_0();
		U3CDoThisU3Ec__AnonStorey12B_t2640087060 * L_8 = V_2;
		NullCheck(L_8);
		KeyValuePair_2_t3615068783  L_9 = (KeyValuePair_2_t3615068783 )L_8->get_tuple_0();
		NullCheck((Stack_1_t1890507522 *)L_7);
		((  void (*) (Stack_1_t1890507522 *, KeyValuePair_2_t3615068783 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->method)((Stack_1_t1890507522 *)L_7, (KeyValuePair_2_t3615068783 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		U3CDoThisU3Ec__AnonStorey12B_t2640087060 * L_10 = V_2;
		IntPtr_t L_11;
		L_11.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		Action_t437523947 * L_12 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_12, (Il2CppObject *)L_10, (IntPtr_t)L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_13 = Disposable_Create_m2910846009(NULL /*static, unused*/, (Action_t437523947 *)L_12, /*hidden argument*/NULL);
		V_1 = (Il2CppObject *)L_13;
		Il2CppObject * L_14 = V_1;
		return L_14;
	}
}
// System.Void StreamEgoizm`1<System.Object>::Send(T)
extern "C"  void StreamEgoizm_1_Send_m1914077089_gshared (StreamEgoizm_1_t3012409721 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	Action_1_t985559125 * V_0 = NULL;
	KeyValuePair_2_t3615068783  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Stack_1_t1890507522 * L_0 = (Stack_1_t1890507522 *)__this->get__subscribes_0();
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		if (!L_1)
		{
			goto IL_0031;
		}
	}
	{
		Stack_1_t1890507522 * L_2 = (Stack_1_t1890507522 *)__this->get__subscribes_0();
		KeyValuePair_2_t3615068783  L_3 = ((  KeyValuePair_2_t3615068783  (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->method)(NULL /*static, unused*/, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
		V_1 = (KeyValuePair_2_t3615068783 )L_3;
		Action_1_t985559125 * L_4 = ((  Action_1_t985559125 * (*) (KeyValuePair_2_t3615068783 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19)->method)((KeyValuePair_2_t3615068783 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
		V_0 = (Action_1_t985559125 *)L_4;
		Action_1_t985559125 * L_5 = V_0;
		if (!L_5)
		{
			goto IL_0031;
		}
	}
	{
		Action_1_t985559125 * L_6 = V_0;
		Il2CppObject * L_7 = ___obj0;
		NullCheck((Action_1_t985559125 *)L_6);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20)->method)((Action_1_t985559125 *)L_6, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20));
	}

IL_0031:
	{
		return;
	}
}
// System.Void StreamQuery/<Join>c__AnonStorey13F`1/<Join>c__AnonStorey140`1<System.Object>::.ctor()
extern "C"  void U3CJoinU3Ec__AnonStorey140_1__ctor_m2022263344_gshared (U3CJoinU3Ec__AnonStorey140_1_t1200685296 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StreamQuery/<Join>c__AnonStorey13F`1/<Join>c__AnonStorey140`1<System.Object>::<>m__1D4(IStream`1<T>)
extern "C"  void U3CJoinU3Ec__AnonStorey140_1_U3CU3Em__1D4_m1912173950_gshared (U3CJoinU3Ec__AnonStorey140_1_t1200685296 * __this, Il2CppObject* ___innerStream0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject* L_0 = ___innerStream0;
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Action_1_t985559125 * L_2 = (Action_1_t985559125 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(L_2, (Il2CppObject *)__this, (IntPtr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		int32_t L_3 = (int32_t)__this->get_p_0();
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_4 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t985559125 *, int32_t >::Invoke(0 /* System.IDisposable IStream`1<System.Object>::Listen(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0, (Action_1_t985559125 *)L_2, (int32_t)L_3);
		V_0 = (Il2CppObject *)L_4;
		SingleAssignmentDisposable_t1832432170 * L_5 = (SingleAssignmentDisposable_t1832432170 *)__this->get_inner_2();
		NullCheck((SingleAssignmentDisposable_t1832432170 *)L_5);
		VirtActionInvoker0::Invoke(4 /* System.Void CellUtils.SingleAssignmentDisposable::Dispose() */, (SingleAssignmentDisposable_t1832432170 *)L_5);
		SingleAssignmentDisposable_t1832432170 * L_6 = (SingleAssignmentDisposable_t1832432170 *)__this->get_inner_2();
		Il2CppObject * L_7 = V_0;
		NullCheck((SingleAssignmentDisposable_t1832432170 *)L_6);
		SingleAssignmentDisposable_set_Disposable_m1918433680((SingleAssignmentDisposable_t1832432170 *)L_6, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StreamQuery/<Join>c__AnonStorey13F`1/<Join>c__AnonStorey140`1<System.Object>::<>m__1D5(T)
extern "C"  void U3CJoinU3Ec__AnonStorey140_1_U3CU3Em__1D5_m2403709651_gshared (U3CJoinU3Ec__AnonStorey140_1_t1200685296 * __this, Il2CppObject * ___val0, const MethodInfo* method)
{
	{
		Action_1_t985559125 * L_0 = (Action_1_t985559125 *)__this->get_reaction_1();
		Il2CppObject * L_1 = ___val0;
		NullCheck((Action_1_t985559125 *)L_0);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Action_1_t985559125 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
// System.Void StreamQuery/<Join>c__AnonStorey13F`1<System.Object>::.ctor()
extern "C"  void U3CJoinU3Ec__AnonStorey13F_1__ctor_m1191177020_gshared (U3CJoinU3Ec__AnonStorey13F_1_t910942991 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.IDisposable StreamQuery/<Join>c__AnonStorey13F`1<System.Object>::<>m__1D2(System.Action`1<T>,Priority)
extern Il2CppClass* SingleAssignmentDisposable_t1832432170_il2cpp_TypeInfo_var;
extern Il2CppClass* ListDisposable_t2393830995_il2cpp_TypeInfo_var;
extern const uint32_t U3CJoinU3Ec__AnonStorey13F_1_U3CU3Em__1D2_m790859129_MetadataUsageId;
extern "C"  Il2CppObject * U3CJoinU3Ec__AnonStorey13F_1_U3CU3Em__1D2_m790859129_gshared (U3CJoinU3Ec__AnonStorey13F_1_t910942991 * __this, Action_1_t985559125 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CJoinU3Ec__AnonStorey13F_1_U3CU3Em__1D2_m790859129_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SingleAssignmentDisposable_t1832432170 * V_0 = NULL;
	ListDisposable_t2393830995 * V_1 = NULL;
	U3CJoinU3Ec__AnonStorey140_1_t1200685296 * V_2 = NULL;
	ListDisposable_t2393830995 * V_3 = NULL;
	{
		U3CJoinU3Ec__AnonStorey140_1_t1200685296 * L_0 = (U3CJoinU3Ec__AnonStorey140_1_t1200685296 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CJoinU3Ec__AnonStorey140_1_t1200685296 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_2 = (U3CJoinU3Ec__AnonStorey140_1_t1200685296 *)L_0;
		U3CJoinU3Ec__AnonStorey140_1_t1200685296 * L_1 = V_2;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU24319_3(__this);
		U3CJoinU3Ec__AnonStorey140_1_t1200685296 * L_2 = V_2;
		int32_t L_3 = ___p1;
		NullCheck(L_2);
		L_2->set_p_0(L_3);
		U3CJoinU3Ec__AnonStorey140_1_t1200685296 * L_4 = V_2;
		Action_1_t985559125 * L_5 = ___reaction0;
		NullCheck(L_4);
		L_4->set_reaction_1(L_5);
		SingleAssignmentDisposable_t1832432170 * L_6 = (SingleAssignmentDisposable_t1832432170 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t1832432170_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m2805131143(L_6, /*hidden argument*/NULL);
		V_0 = (SingleAssignmentDisposable_t1832432170 *)L_6;
		U3CJoinU3Ec__AnonStorey140_1_t1200685296 * L_7 = V_2;
		SingleAssignmentDisposable_t1832432170 * L_8 = (SingleAssignmentDisposable_t1832432170 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t1832432170_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m2805131143(L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		L_7->set_inner_2(L_8);
		ListDisposable_t2393830995 * L_9 = (ListDisposable_t2393830995 *)il2cpp_codegen_object_new(ListDisposable_t2393830995_il2cpp_TypeInfo_var);
		ListDisposable__ctor_m4289441534(L_9, /*hidden argument*/NULL);
		V_3 = (ListDisposable_t2393830995 *)L_9;
		ListDisposable_t2393830995 * L_10 = V_3;
		SingleAssignmentDisposable_t1832432170 * L_11 = V_0;
		NullCheck((ListDisposable_t2393830995 *)L_10);
		VirtActionInvoker1< Il2CppObject * >::Invoke(9 /* System.Void CellUtils.ListDisposable::Add(System.IDisposable) */, (ListDisposable_t2393830995 *)L_10, (Il2CppObject *)L_11);
		ListDisposable_t2393830995 * L_12 = V_3;
		U3CJoinU3Ec__AnonStorey140_1_t1200685296 * L_13 = V_2;
		NullCheck(L_13);
		SingleAssignmentDisposable_t1832432170 * L_14 = (SingleAssignmentDisposable_t1832432170 *)L_13->get_inner_2();
		NullCheck((ListDisposable_t2393830995 *)L_12);
		VirtActionInvoker1< Il2CppObject * >::Invoke(9 /* System.Void CellUtils.ListDisposable::Add(System.IDisposable) */, (ListDisposable_t2393830995 *)L_12, (Il2CppObject *)L_14);
		ListDisposable_t2393830995 * L_15 = V_3;
		V_1 = (ListDisposable_t2393830995 *)L_15;
		SingleAssignmentDisposable_t1832432170 * L_16 = V_0;
		Il2CppObject* L_17 = (Il2CppObject*)__this->get_stream_0();
		U3CJoinU3Ec__AnonStorey140_1_t1200685296 * L_18 = V_2;
		IntPtr_t L_19;
		L_19.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_1_t1538250116 * L_20 = (Action_1_t1538250116 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (Action_1_t1538250116 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_20, (Il2CppObject *)L_18, (IntPtr_t)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		U3CJoinU3Ec__AnonStorey140_1_t1200685296 * L_21 = V_2;
		NullCheck(L_21);
		int32_t L_22 = (int32_t)L_21->get_p_0();
		NullCheck((Il2CppObject*)L_17);
		Il2CppObject * L_23 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t1538250116 *, int32_t >::Invoke(0 /* System.IDisposable IStream`1<IStream`1<System.Object>>::Listen(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_17, (Action_1_t1538250116 *)L_20, (int32_t)L_22);
		NullCheck((SingleAssignmentDisposable_t1832432170 *)L_16);
		SingleAssignmentDisposable_set_Disposable_m1918433680((SingleAssignmentDisposable_t1832432170 *)L_16, (Il2CppObject *)L_23, /*hidden argument*/NULL);
		ListDisposable_t2393830995 * L_24 = V_1;
		return L_24;
	}
}
// System.Void StreamQuery/<SelectMany>c__AnonStorey13C`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CSelectManyU3Ec__AnonStorey13C_2__ctor_m3152285819_gshared (U3CSelectManyU3Ec__AnonStorey13C_2_t3738070224 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// IStream`1<TR> StreamQuery/<SelectMany>c__AnonStorey13C`2<System.Object,System.Object>::<>m__1D0(T)
extern "C"  Il2CppObject* U3CSelectManyU3Ec__AnonStorey13C_2_U3CU3Em__1D0_m3907841112_gshared (U3CSelectManyU3Ec__AnonStorey13C_2_t3738070224 * __this, Il2CppObject * ____0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_other_0();
		return L_0;
	}
}
// System.Void StreamQuery/<SelectMany>c__AnonStorey13D`3/<SelectMany>c__AnonStorey13E`3<System.Object,System.Object,System.Object>::.ctor()
extern "C"  void U3CSelectManyU3Ec__AnonStorey13E_3__ctor_m2415025516_gshared (U3CSelectManyU3Ec__AnonStorey13E_3_t2432972599 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TR StreamQuery/<SelectMany>c__AnonStorey13D`3/<SelectMany>c__AnonStorey13E`3<System.Object,System.Object,System.Object>::<>m__1D3(TC)
extern "C"  Il2CppObject * U3CSelectManyU3Ec__AnonStorey13E_3_U3CU3Em__1D3_m1289479197_gshared (U3CSelectManyU3Ec__AnonStorey13E_3_t2432972599 * __this, Il2CppObject * ___y0, const MethodInfo* method)
{
	{
		U3CSelectManyU3Ec__AnonStorey13D_3_t2765353606 * L_0 = (U3CSelectManyU3Ec__AnonStorey13D_3_t2765353606 *)__this->get_U3CU3Ef__refU24317_1();
		NullCheck(L_0);
		Func_3_t1892209229 * L_1 = (Func_3_t1892209229 *)L_0->get_resultSelector_1();
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_x_0();
		Il2CppObject * L_3 = ___y0;
		NullCheck((Func_3_t1892209229 *)L_1);
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (Func_3_t1892209229 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Func_3_t1892209229 *)L_1, (Il2CppObject *)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_4;
	}
}
// System.Void StreamQuery/<SelectMany>c__AnonStorey13D`3<System.Object,System.Object,System.Object>::.ctor()
extern "C"  void U3CSelectManyU3Ec__AnonStorey13D_3__ctor_m117789039_gshared (U3CSelectManyU3Ec__AnonStorey13D_3_t2765353606 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// IStream`1<TR> StreamQuery/<SelectMany>c__AnonStorey13D`3<System.Object,System.Object,System.Object>::<>m__1D1(T)
extern "C"  Il2CppObject* U3CSelectManyU3Ec__AnonStorey13D_3_U3CU3Em__1D1_m1180917483_gshared (U3CSelectManyU3Ec__AnonStorey13D_3_t2765353606 * __this, Il2CppObject * ___x0, const MethodInfo* method)
{
	U3CSelectManyU3Ec__AnonStorey13E_3_t2432972599 * V_0 = NULL;
	{
		U3CSelectManyU3Ec__AnonStorey13E_3_t2432972599 * L_0 = (U3CSelectManyU3Ec__AnonStorey13E_3_t2432972599 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CSelectManyU3Ec__AnonStorey13E_3_t2432972599 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CSelectManyU3Ec__AnonStorey13E_3_t2432972599 *)L_0;
		U3CSelectManyU3Ec__AnonStorey13E_3_t2432972599 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU24317_1(__this);
		U3CSelectManyU3Ec__AnonStorey13E_3_t2432972599 * L_2 = V_0;
		Il2CppObject * L_3 = ___x0;
		NullCheck(L_2);
		L_2->set_x_0(L_3);
		Func_2_t2688474343 * L_4 = (Func_2_t2688474343 *)__this->get_collectionSelector_0();
		U3CSelectManyU3Ec__AnonStorey13E_3_t2432972599 * L_5 = V_0;
		NullCheck(L_5);
		Il2CppObject * L_6 = (Il2CppObject *)L_5->get_x_0();
		NullCheck((Func_2_t2688474343 *)L_4);
		Il2CppObject* L_7 = ((  Il2CppObject* (*) (Func_2_t2688474343 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Func_2_t2688474343 *)L_4, (Il2CppObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		U3CSelectManyU3Ec__AnonStorey13E_3_t2432972599 * L_8 = V_0;
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Func_2_t2135783352 * L_10 = (Func_2_t2135783352 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (Func_2_t2135783352 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_10, (Il2CppObject *)L_8, (IntPtr_t)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Il2CppObject* L_11 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t2135783352 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, (Il2CppObject*)L_7, (Func_2_t2135783352 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_11;
	}
}
// System.Void Substance`1<System.Object>::.ctor()
extern "C"  void Substance_1__ctor_m2762360544_gshared (Substance_1_t3068566258 * __this, const MethodInfo* method)
{
	{
		NullCheck((SubstanceBase_2_t1838912788 *)__this);
		((  void (*) (SubstanceBase_2_t1838912788 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((SubstanceBase_2_t1838912788 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// T Substance`1<System.Object>::Result()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Substance_1_Result_m448858752_MetadataUsageId;
extern "C"  Il2CppObject * Substance_1_Result_m448858752_gshared (Substance_1_t3068566258 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Substance_1_Result_m448858752_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Intrusion_t48563248 * V_1 = NULL;
	Enumerator_t3226272505  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)((SubstanceBase_2_t1838912788 *)__this)->get_localBase_0();
		V_0 = (Il2CppObject *)L_0;
		List_1_t845522217 * L_1 = (List_1_t845522217 *)((SubstanceBase_2_t1838912788 *)__this)->get_intrusions_5();
		NullCheck((List_1_t845522217 *)L_1);
		Enumerator_t3226272505  L_2 = ((  Enumerator_t3226272505  (*) (List_1_t845522217 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((List_1_t845522217 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_2 = (Enumerator_t3226272505 )L_2;
	}

IL_0013:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002d;
		}

IL_0018:
		{
			Intrusion_t48563248 * L_3 = ((  Intrusion_t48563248 * (*) (Enumerator_t3226272505 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Enumerator_t3226272505 *)(&V_2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
			V_1 = (Intrusion_t48563248 *)L_3;
			Intrusion_t48563248 * L_4 = V_1;
			NullCheck(L_4);
			Func_2_t2135783352 * L_5 = (Func_2_t2135783352 *)L_4->get_influence_1();
			Il2CppObject * L_6 = V_0;
			NullCheck((Func_2_t2135783352 *)L_5);
			Il2CppObject * L_7 = ((  Il2CppObject * (*) (Func_2_t2135783352 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Func_2_t2135783352 *)L_5, (Il2CppObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			V_0 = (Il2CppObject *)L_7;
		}

IL_002d:
		{
			bool L_8 = ((  bool (*) (Enumerator_t3226272505 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Enumerator_t3226272505 *)(&V_2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			if (L_8)
			{
				goto IL_0018;
			}
		}

IL_0039:
		{
			IL2CPP_LEAVE(0x4A, FINALLY_003e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003e;
	}

FINALLY_003e:
	{ // begin finally (depth: 1)
		Enumerator_t3226272505  L_9 = V_2;
		Enumerator_t3226272505  L_10 = L_9;
		Il2CppObject * L_11 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_10);
		NullCheck((Il2CppObject *)L_11);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_11);
		IL2CPP_END_FINALLY(62)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(62)
	{
		IL2CPP_JUMP_TBL(0x4A, IL_004a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_004a:
	{
		Il2CppObject * L_12 = V_0;
		return L_12;
	}
}
// System.Void SubstanceBase`2/<Affect>c__AnonStorey143<System.Boolean,System.Boolean>::.ctor()
extern "C"  void U3CAffectU3Ec__AnonStorey143__ctor_m17649775_gshared (U3CAffectU3Ec__AnonStorey143_t390189525 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SubstanceBase`2/<Affect>c__AnonStorey143<System.Boolean,System.Boolean>::<>m__1D9(InfT)
extern "C"  void U3CAffectU3Ec__AnonStorey143_U3CU3Em__1D9_m91688573_gshared (U3CAffectU3Ec__AnonStorey143_t390189525 * __this, bool ___val0, const MethodInfo* method)
{
	{
		Intrusion_t1917188776 * L_0 = (Intrusion_t1917188776 *)__this->get_intrusion_0();
		bool L_1 = ___val0;
		NullCheck(L_0);
		L_0->set_influence_1(L_1);
		SubstanceBase_2_t3707538316 * L_2 = (SubstanceBase_2_t3707538316 *)__this->get_U3CU3Ef__this_1();
		Intrusion_t1917188776 * L_3 = (Intrusion_t1917188776 *)__this->get_intrusion_0();
		NullCheck((SubstanceBase_2_t3707538316 *)L_2);
		VirtActionInvoker1< Intrusion_t1917188776 * >::Invoke(16 /* System.Void SubstanceBase`2<System.Boolean,System.Boolean>::OnUpdate(SubstanceBase`2/Intrusion<T,InfT>) */, (SubstanceBase_2_t3707538316 *)L_2, (Intrusion_t1917188776 *)L_3);
		return;
	}
}
// System.Void SubstanceBase`2/<Affect>c__AnonStorey143<System.Double,System.Double>::.ctor()
extern "C"  void U3CAffectU3Ec__AnonStorey143__ctor_m1794887191_gshared (U3CAffectU3Ec__AnonStorey143_t2252021777 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SubstanceBase`2/<Affect>c__AnonStorey143<System.Double,System.Double>::<>m__1D9(InfT)
extern "C"  void U3CAffectU3Ec__AnonStorey143_U3CU3Em__1D9_m1383556053_gshared (U3CAffectU3Ec__AnonStorey143_t2252021777 * __this, double ___val0, const MethodInfo* method)
{
	{
		Intrusion_t3779021028 * L_0 = (Intrusion_t3779021028 *)__this->get_intrusion_0();
		double L_1 = ___val0;
		NullCheck(L_0);
		L_0->set_influence_1(L_1);
		SubstanceBase_2_t1274403272 * L_2 = (SubstanceBase_2_t1274403272 *)__this->get_U3CU3Ef__this_1();
		Intrusion_t3779021028 * L_3 = (Intrusion_t3779021028 *)__this->get_intrusion_0();
		NullCheck((SubstanceBase_2_t1274403272 *)L_2);
		VirtActionInvoker1< Intrusion_t3779021028 * >::Invoke(16 /* System.Void SubstanceBase`2<System.Double,System.Double>::OnUpdate(SubstanceBase`2/Intrusion<T,InfT>) */, (SubstanceBase_2_t1274403272 *)L_2, (Intrusion_t3779021028 *)L_3);
		return;
	}
}
// System.Void SubstanceBase`2/<Affect>c__AnonStorey143<System.Object,System.Object>::.ctor()
extern "C"  void U3CAffectU3Ec__AnonStorey143__ctor_m113918323_gshared (U3CAffectU3Ec__AnonStorey143_t1517854361 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SubstanceBase`2/<Affect>c__AnonStorey143<System.Object,System.Object>::<>m__1D9(InfT)
extern "C"  void U3CAffectU3Ec__AnonStorey143_U3CU3Em__1D9_m4009603321_gshared (U3CAffectU3Ec__AnonStorey143_t1517854361 * __this, Il2CppObject * ___val0, const MethodInfo* method)
{
	{
		Intrusion_t3044853612 * L_0 = (Intrusion_t3044853612 *)__this->get_intrusion_0();
		Il2CppObject * L_1 = ___val0;
		NullCheck(L_0);
		L_0->set_influence_1(L_1);
		SubstanceBase_2_t540235856 * L_2 = (SubstanceBase_2_t540235856 *)__this->get_U3CU3Ef__this_1();
		Intrusion_t3044853612 * L_3 = (Intrusion_t3044853612 *)__this->get_intrusion_0();
		NullCheck((SubstanceBase_2_t540235856 *)L_2);
		VirtActionInvoker1< Intrusion_t3044853612 * >::Invoke(16 /* System.Void SubstanceBase`2<System.Object,System.Object>::OnUpdate(SubstanceBase`2/Intrusion<T,InfT>) */, (SubstanceBase_2_t540235856 *)L_2, (Intrusion_t3044853612 *)L_3);
		return;
	}
}
// System.Void SubstanceBase`2/<Affect>c__AnonStorey143<System.Single,System.Object>::.ctor()
extern "C"  void U3CAffectU3Ec__AnonStorey143__ctor_m2992756604_gshared (U3CAffectU3Ec__AnonStorey143_t2725761868 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SubstanceBase`2/<Affect>c__AnonStorey143<System.Single,System.Object>::<>m__1D9(InfT)
extern "C"  void U3CAffectU3Ec__AnonStorey143_U3CU3Em__1D9_m4072402640_gshared (U3CAffectU3Ec__AnonStorey143_t2725761868 * __this, Il2CppObject * ___val0, const MethodInfo* method)
{
	{
		Intrusion_t4252761119 * L_0 = (Intrusion_t4252761119 *)__this->get_intrusion_0();
		Il2CppObject * L_1 = ___val0;
		NullCheck(L_0);
		L_0->set_influence_1(L_1);
		SubstanceBase_2_t1748143363 * L_2 = (SubstanceBase_2_t1748143363 *)__this->get_U3CU3Ef__this_1();
		Intrusion_t4252761119 * L_3 = (Intrusion_t4252761119 *)__this->get_intrusion_0();
		NullCheck((SubstanceBase_2_t1748143363 *)L_2);
		VirtActionInvoker1< Intrusion_t4252761119 * >::Invoke(16 /* System.Void SubstanceBase`2<System.Single,System.Object>::OnUpdate(SubstanceBase`2/Intrusion<T,InfT>) */, (SubstanceBase_2_t1748143363 *)L_2, (Intrusion_t4252761119 *)L_3);
		return;
	}
}
// System.Void SubstanceBase`2/<Affect>c__AnonStorey143<System.Single,System.Single>::.ctor()
extern "C"  void U3CAffectU3Ec__AnonStorey143__ctor_m2650718533_gshared (U3CAffectU3Ec__AnonStorey143_t2846864469 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SubstanceBase`2/<Affect>c__AnonStorey143<System.Single,System.Single>::<>m__1D9(InfT)
extern "C"  void U3CAffectU3Ec__AnonStorey143_U3CU3Em__1D9_m2168660199_gshared (U3CAffectU3Ec__AnonStorey143_t2846864469 * __this, float ___val0, const MethodInfo* method)
{
	{
		Intrusion_t78896424 * L_0 = (Intrusion_t78896424 *)__this->get_intrusion_0();
		float L_1 = ___val0;
		NullCheck(L_0);
		L_0->set_influence_1(L_1);
		SubstanceBase_2_t1869245964 * L_2 = (SubstanceBase_2_t1869245964 *)__this->get_U3CU3Ef__this_1();
		Intrusion_t78896424 * L_3 = (Intrusion_t78896424 *)__this->get_intrusion_0();
		NullCheck((SubstanceBase_2_t1869245964 *)L_2);
		VirtActionInvoker1< Intrusion_t78896424 * >::Invoke(16 /* System.Void SubstanceBase`2<System.Single,System.Single>::OnUpdate(SubstanceBase`2/Intrusion<T,InfT>) */, (SubstanceBase_2_t1869245964 *)L_2, (Intrusion_t78896424 *)L_3);
		return;
	}
}
// System.Void SubstanceBase`2/<Affect>c__AnonStorey144<System.Boolean,System.Boolean>::.ctor()
extern "C"  void U3CAffectU3Ec__AnonStorey144__ctor_m2376956784_gshared (U3CAffectU3Ec__AnonStorey144_t386862328 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SubstanceBase`2/<Affect>c__AnonStorey144<System.Boolean,System.Boolean>::<>m__1DA()
extern "C"  void U3CAffectU3Ec__AnonStorey144_U3CU3Em__1DA_m3624125367_gshared (U3CAffectU3Ec__AnonStorey144_t386862328 * __this, const MethodInfo* method)
{
	{
		SubstanceBase_2_t3707538316 * L_0 = (SubstanceBase_2_t3707538316 *)__this->get_U3CU3Ef__this_1();
		Intrusion_t1917188776 * L_1 = (Intrusion_t1917188776 *)__this->get_intrusion_0();
		NullCheck((SubstanceBase_2_t3707538316 *)L_0);
		VirtActionInvoker1< Intrusion_t1917188776 * >::Invoke(16 /* System.Void SubstanceBase`2<System.Boolean,System.Boolean>::OnUpdate(SubstanceBase`2/Intrusion<T,InfT>) */, (SubstanceBase_2_t3707538316 *)L_0, (Intrusion_t1917188776 *)L_1);
		return;
	}
}
// System.Void SubstanceBase`2/<Affect>c__AnonStorey144<System.Double,System.Double>::.ctor()
extern "C"  void U3CAffectU3Ec__AnonStorey144__ctor_m872203608_gshared (U3CAffectU3Ec__AnonStorey144_t2248694580 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SubstanceBase`2/<Affect>c__AnonStorey144<System.Double,System.Double>::<>m__1DA()
extern "C"  void U3CAffectU3Ec__AnonStorey144_U3CU3Em__1DA_m2095927503_gshared (U3CAffectU3Ec__AnonStorey144_t2248694580 * __this, const MethodInfo* method)
{
	{
		SubstanceBase_2_t1274403272 * L_0 = (SubstanceBase_2_t1274403272 *)__this->get_U3CU3Ef__this_1();
		Intrusion_t3779021028 * L_1 = (Intrusion_t3779021028 *)__this->get_intrusion_0();
		NullCheck((SubstanceBase_2_t1274403272 *)L_0);
		VirtActionInvoker1< Intrusion_t3779021028 * >::Invoke(16 /* System.Void SubstanceBase`2<System.Double,System.Double>::OnUpdate(SubstanceBase`2/Intrusion<T,InfT>) */, (SubstanceBase_2_t1274403272 *)L_0, (Intrusion_t3779021028 *)L_1);
		return;
	}
}
// System.Void SubstanceBase`2/<Affect>c__AnonStorey144<System.Object,System.Object>::.ctor()
extern "C"  void U3CAffectU3Ec__AnonStorey144__ctor_m3486202036_gshared (U3CAffectU3Ec__AnonStorey144_t1514527164 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SubstanceBase`2/<Affect>c__AnonStorey144<System.Object,System.Object>::<>m__1DA()
extern "C"  void U3CAffectU3Ec__AnonStorey144_U3CU3Em__1DA_m3671052275_gshared (U3CAffectU3Ec__AnonStorey144_t1514527164 * __this, const MethodInfo* method)
{
	{
		SubstanceBase_2_t540235856 * L_0 = (SubstanceBase_2_t540235856 *)__this->get_U3CU3Ef__this_1();
		Intrusion_t3044853612 * L_1 = (Intrusion_t3044853612 *)__this->get_intrusion_0();
		NullCheck((SubstanceBase_2_t540235856 *)L_0);
		VirtActionInvoker1< Intrusion_t3044853612 * >::Invoke(16 /* System.Void SubstanceBase`2<System.Object,System.Object>::OnUpdate(SubstanceBase`2/Intrusion<T,InfT>) */, (SubstanceBase_2_t540235856 *)L_0, (Intrusion_t3044853612 *)L_1);
		return;
	}
}
// System.Void SubstanceBase`2/<Affect>c__AnonStorey144<System.Single,System.Object>::.ctor()
extern "C"  void U3CAffectU3Ec__AnonStorey144__ctor_m2070073021_gshared (U3CAffectU3Ec__AnonStorey144_t2722434671 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SubstanceBase`2/<Affect>c__AnonStorey144<System.Single,System.Object>::<>m__1DA()
extern "C"  void U3CAffectU3Ec__AnonStorey144_U3CU3Em__1DA_m940347722_gshared (U3CAffectU3Ec__AnonStorey144_t2722434671 * __this, const MethodInfo* method)
{
	{
		SubstanceBase_2_t1748143363 * L_0 = (SubstanceBase_2_t1748143363 *)__this->get_U3CU3Ef__this_1();
		Intrusion_t4252761119 * L_1 = (Intrusion_t4252761119 *)__this->get_intrusion_0();
		NullCheck((SubstanceBase_2_t1748143363 *)L_0);
		VirtActionInvoker1< Intrusion_t4252761119 * >::Invoke(16 /* System.Void SubstanceBase`2<System.Single,System.Object>::OnUpdate(SubstanceBase`2/Intrusion<T,InfT>) */, (SubstanceBase_2_t1748143363 *)L_0, (Intrusion_t4252761119 *)L_1);
		return;
	}
}
// System.Void SubstanceBase`2/<Affect>c__AnonStorey144<System.Single,System.Single>::.ctor()
extern "C"  void U3CAffectU3Ec__AnonStorey144__ctor_m1728034950_gshared (U3CAffectU3Ec__AnonStorey144_t2843537272 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SubstanceBase`2/<Affect>c__AnonStorey144<System.Single,System.Single>::<>m__1DA()
extern "C"  void U3CAffectU3Ec__AnonStorey144_U3CU3Em__1DA_m3241567969_gshared (U3CAffectU3Ec__AnonStorey144_t2843537272 * __this, const MethodInfo* method)
{
	{
		SubstanceBase_2_t1869245964 * L_0 = (SubstanceBase_2_t1869245964 *)__this->get_U3CU3Ef__this_1();
		Intrusion_t78896424 * L_1 = (Intrusion_t78896424 *)__this->get_intrusion_0();
		NullCheck((SubstanceBase_2_t1869245964 *)L_0);
		VirtActionInvoker1< Intrusion_t78896424 * >::Invoke(16 /* System.Void SubstanceBase`2<System.Single,System.Single>::OnUpdate(SubstanceBase`2/Intrusion<T,InfT>) */, (SubstanceBase_2_t1869245964 *)L_0, (Intrusion_t78896424 *)L_1);
		return;
	}
}
// System.Void SubstanceBase`2/<OnChanged>c__AnonStorey142<System.Boolean,System.Boolean>::.ctor()
extern "C"  void U3COnChangedU3Ec__AnonStorey142__ctor_m3102795806_gshared (U3COnChangedU3Ec__AnonStorey142_t1335955226 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SubstanceBase`2/<OnChanged>c__AnonStorey142<System.Boolean,System.Boolean>::<>m__1D8(T)
extern "C"  void U3COnChangedU3Ec__AnonStorey142_U3CU3Em__1D8_m4122085086_gshared (U3COnChangedU3Ec__AnonStorey142_t1335955226 * __this, bool ____0, const MethodInfo* method)
{
	{
		Action_t437523947 * L_0 = (Action_t437523947 *)__this->get_action_0();
		NullCheck((Action_t437523947 *)L_0);
		Action_Invoke_m1445970038((Action_t437523947 *)L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SubstanceBase`2/<OnChanged>c__AnonStorey142<System.Double,System.Double>::.ctor()
extern "C"  void U3COnChangedU3Ec__AnonStorey142__ctor_m3505358214_gshared (U3COnChangedU3Ec__AnonStorey142_t3197787478 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SubstanceBase`2/<OnChanged>c__AnonStorey142<System.Double,System.Double>::<>m__1D8(T)
extern "C"  void U3COnChangedU3Ec__AnonStorey142_U3CU3Em__1D8_m2295574598_gshared (U3COnChangedU3Ec__AnonStorey142_t3197787478 * __this, double ____0, const MethodInfo* method)
{
	{
		Action_t437523947 * L_0 = (Action_t437523947 *)__this->get_action_0();
		NullCheck((Action_t437523947 *)L_0);
		Action_Invoke_m1445970038((Action_t437523947 *)L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SubstanceBase`2/<OnChanged>c__AnonStorey142<System.Object,System.Object>::.ctor()
extern "C"  void U3COnChangedU3Ec__AnonStorey142__ctor_m1824389346_gshared (U3COnChangedU3Ec__AnonStorey142_t2463620062 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SubstanceBase`2/<OnChanged>c__AnonStorey142<System.Object,System.Object>::<>m__1D8(T)
extern "C"  void U3COnChangedU3Ec__AnonStorey142_U3CU3Em__1D8_m3879802274_gshared (U3COnChangedU3Ec__AnonStorey142_t2463620062 * __this, Il2CppObject * ____0, const MethodInfo* method)
{
	{
		Action_t437523947 * L_0 = (Action_t437523947 *)__this->get_action_0();
		NullCheck((Action_t437523947 *)L_0);
		Action_Invoke_m1445970038((Action_t437523947 *)L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SubstanceBase`2/<OnChanged>c__AnonStorey142<System.Single,System.Object>::.ctor()
extern "C"  void U3COnChangedU3Ec__AnonStorey142__ctor_m408260331_gshared (U3COnChangedU3Ec__AnonStorey142_t3671527569 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SubstanceBase`2/<OnChanged>c__AnonStorey142<System.Single,System.Object>::<>m__1D8(T)
extern "C"  void U3COnChangedU3Ec__AnonStorey142_U3CU3Em__1D8_m832339755_gshared (U3COnChangedU3Ec__AnonStorey142_t3671527569 * __this, float ____0, const MethodInfo* method)
{
	{
		Action_t437523947 * L_0 = (Action_t437523947 *)__this->get_action_0();
		NullCheck((Action_t437523947 *)L_0);
		Action_Invoke_m1445970038((Action_t437523947 *)L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SubstanceBase`2/<OnChanged>c__AnonStorey142<System.Single,System.Single>::.ctor()
extern "C"  void U3COnChangedU3Ec__AnonStorey142__ctor_m66222260_gshared (U3COnChangedU3Ec__AnonStorey142_t3792630170 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SubstanceBase`2/<OnChanged>c__AnonStorey142<System.Single,System.Single>::<>m__1D8(T)
extern "C"  void U3COnChangedU3Ec__AnonStorey142_U3CU3Em__1D8_m3450690676_gshared (U3COnChangedU3Ec__AnonStorey142_t3792630170 * __this, float ____0, const MethodInfo* method)
{
	{
		Action_t437523947 * L_0 = (Action_t437523947 *)__this->get_action_0();
		NullCheck((Action_t437523947 *)L_0);
		Action_Invoke_m1445970038((Action_t437523947 *)L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SubstanceBase`2/DisposableAffect<System.Boolean,System.Boolean>::.ctor()
extern "C"  void DisposableAffect__ctor_m1383012526_gshared (DisposableAffect_t2225548402 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SubstanceBase`2/DisposableAffect<System.Boolean,System.Boolean>::Dispose()
extern "C"  void DisposableAffect_Dispose_m1828426091_gshared (DisposableAffect_t2225548402 * __this, const MethodInfo* method)
{
	{
		SubstanceBase_2_t3707538316 * L_0 = (SubstanceBase_2_t3707538316 *)__this->get_substance_1();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		SubstanceBase_2_t3707538316 * L_1 = (SubstanceBase_2_t3707538316 *)__this->get_substance_1();
		Intrusion_t1917188776 * L_2 = (Intrusion_t1917188776 *)__this->get_intrusion_0();
		NullCheck((SubstanceBase_2_t3707538316 *)L_1);
		VirtActionInvoker1< Intrusion_t1917188776 * >::Invoke(19 /* System.Void SubstanceBase`2<System.Boolean,System.Boolean>::ClearIntrusion(SubstanceBase`2/Intrusion<T,InfT>) */, (SubstanceBase_2_t3707538316 *)L_1, (Intrusion_t1917188776 *)L_2);
	}

IL_001c:
	{
		__this->set_substance_1((SubstanceBase_2_t3707538316 *)NULL);
		__this->set_intrusion_0((Intrusion_t1917188776 *)NULL);
		return;
	}
}
// System.Void SubstanceBase`2/DisposableAffect<System.Double,System.Double>::.ctor()
extern "C"  void DisposableAffect__ctor_m39885334_gshared (DisposableAffect_t4087380654 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SubstanceBase`2/DisposableAffect<System.Double,System.Double>::Dispose()
extern "C"  void DisposableAffect_Dispose_m3868350675_gshared (DisposableAffect_t4087380654 * __this, const MethodInfo* method)
{
	{
		SubstanceBase_2_t1274403272 * L_0 = (SubstanceBase_2_t1274403272 *)__this->get_substance_1();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		SubstanceBase_2_t1274403272 * L_1 = (SubstanceBase_2_t1274403272 *)__this->get_substance_1();
		Intrusion_t3779021028 * L_2 = (Intrusion_t3779021028 *)__this->get_intrusion_0();
		NullCheck((SubstanceBase_2_t1274403272 *)L_1);
		VirtActionInvoker1< Intrusion_t3779021028 * >::Invoke(19 /* System.Void SubstanceBase`2<System.Double,System.Double>::ClearIntrusion(SubstanceBase`2/Intrusion<T,InfT>) */, (SubstanceBase_2_t1274403272 *)L_1, (Intrusion_t3779021028 *)L_2);
	}

IL_001c:
	{
		__this->set_substance_1((SubstanceBase_2_t1274403272 *)NULL);
		__this->set_intrusion_0((Intrusion_t3779021028 *)NULL);
		return;
	}
}
// System.Void SubstanceBase`2/DisposableAffect<System.Object,System.Object>::.ctor()
extern "C"  void DisposableAffect__ctor_m2653883762_gshared (DisposableAffect_t3353213238 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SubstanceBase`2/DisposableAffect<System.Object,System.Object>::Dispose()
extern "C"  void DisposableAffect_Dispose_m3364971823_gshared (DisposableAffect_t3353213238 * __this, const MethodInfo* method)
{
	{
		SubstanceBase_2_t540235856 * L_0 = (SubstanceBase_2_t540235856 *)__this->get_substance_1();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		SubstanceBase_2_t540235856 * L_1 = (SubstanceBase_2_t540235856 *)__this->get_substance_1();
		Intrusion_t3044853612 * L_2 = (Intrusion_t3044853612 *)__this->get_intrusion_0();
		NullCheck((SubstanceBase_2_t540235856 *)L_1);
		VirtActionInvoker1< Intrusion_t3044853612 * >::Invoke(19 /* System.Void SubstanceBase`2<System.Object,System.Object>::ClearIntrusion(SubstanceBase`2/Intrusion<T,InfT>) */, (SubstanceBase_2_t540235856 *)L_1, (Intrusion_t3044853612 *)L_2);
	}

IL_001c:
	{
		__this->set_substance_1((SubstanceBase_2_t540235856 *)NULL);
		__this->set_intrusion_0((Intrusion_t3044853612 *)NULL);
		return;
	}
}
// System.Void SubstanceBase`2/DisposableAffect<System.Single,System.Object>::.ctor()
extern "C"  void DisposableAffect__ctor_m1237754747_gshared (DisposableAffect_t266153449 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SubstanceBase`2/DisposableAffect<System.Single,System.Object>::Dispose()
extern "C"  void DisposableAffect_Dispose_m3969621240_gshared (DisposableAffect_t266153449 * __this, const MethodInfo* method)
{
	{
		SubstanceBase_2_t1748143363 * L_0 = (SubstanceBase_2_t1748143363 *)__this->get_substance_1();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		SubstanceBase_2_t1748143363 * L_1 = (SubstanceBase_2_t1748143363 *)__this->get_substance_1();
		Intrusion_t4252761119 * L_2 = (Intrusion_t4252761119 *)__this->get_intrusion_0();
		NullCheck((SubstanceBase_2_t1748143363 *)L_1);
		VirtActionInvoker1< Intrusion_t4252761119 * >::Invoke(19 /* System.Void SubstanceBase`2<System.Single,System.Object>::ClearIntrusion(SubstanceBase`2/Intrusion<T,InfT>) */, (SubstanceBase_2_t1748143363 *)L_1, (Intrusion_t4252761119 *)L_2);
	}

IL_001c:
	{
		__this->set_substance_1((SubstanceBase_2_t1748143363 *)NULL);
		__this->set_intrusion_0((Intrusion_t4252761119 *)NULL);
		return;
	}
}
// System.Void SubstanceBase`2/DisposableAffect<System.Single,System.Single>::.ctor()
extern "C"  void DisposableAffect__ctor_m895716676_gshared (DisposableAffect_t387256050 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SubstanceBase`2/DisposableAffect<System.Single,System.Single>::Dispose()
extern "C"  void DisposableAffect_Dispose_m1688549505_gshared (DisposableAffect_t387256050 * __this, const MethodInfo* method)
{
	{
		SubstanceBase_2_t1869245964 * L_0 = (SubstanceBase_2_t1869245964 *)__this->get_substance_1();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		SubstanceBase_2_t1869245964 * L_1 = (SubstanceBase_2_t1869245964 *)__this->get_substance_1();
		Intrusion_t78896424 * L_2 = (Intrusion_t78896424 *)__this->get_intrusion_0();
		NullCheck((SubstanceBase_2_t1869245964 *)L_1);
		VirtActionInvoker1< Intrusion_t78896424 * >::Invoke(19 /* System.Void SubstanceBase`2<System.Single,System.Single>::ClearIntrusion(SubstanceBase`2/Intrusion<T,InfT>) */, (SubstanceBase_2_t1869245964 *)L_1, (Intrusion_t78896424 *)L_2);
	}

IL_001c:
	{
		__this->set_substance_1((SubstanceBase_2_t1869245964 *)NULL);
		__this->set_intrusion_0((Intrusion_t78896424 *)NULL);
		return;
	}
}
// System.Void SubstanceBase`2/Intrusion<System.Boolean,System.Boolean>::.ctor()
extern "C"  void Intrusion__ctor_m2367869752_gshared (Intrusion_t1917188776 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SubstanceBase`2/Intrusion<System.Double,System.Double>::.ctor()
extern "C"  void Intrusion__ctor_m250965792_gshared (Intrusion_t3779021028 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SubstanceBase`2/Intrusion<System.Object,System.Object>::.ctor()
extern "C"  void Intrusion__ctor_m2864964220_gshared (Intrusion_t3044853612 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SubstanceBase`2/Intrusion<System.Single,System.Object>::.ctor()
extern "C"  void Intrusion__ctor_m1448835205_gshared (Intrusion_t4252761119 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SubstanceBase`2/Intrusion<System.Single,System.Single>::.ctor()
extern "C"  void Intrusion__ctor_m1106797134_gshared (Intrusion_t78896424 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SubstanceBase`2<System.Boolean,System.Boolean>::.ctor()
extern "C"  void SubstanceBase_2__ctor_m162542656_gshared (SubstanceBase_2_t3707538316 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SubstanceBase`2<System.Boolean,System.Boolean>::.ctor(T)
extern "C"  void SubstanceBase_2__ctor_m743856414_gshared (SubstanceBase_2_t3707538316 * __this, bool ___baseVal0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		bool L_0 = ___baseVal0;
		__this->set_localBase_0(L_0);
		bool L_1 = (bool)__this->get_localBase_0();
		__this->set_result_4(L_1);
		return;
	}
}
// T SubstanceBase`2<System.Boolean,System.Boolean>::get_baseValue()
extern "C"  bool SubstanceBase_2_get_baseValue_m837721814_gshared (SubstanceBase_2_t3707538316 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_localBase_0();
		return L_0;
	}
}
// System.Void SubstanceBase`2<System.Boolean,System.Boolean>::set_baseValue(T)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t SubstanceBase_2_set_baseValue_m3914724413_MetadataUsageId;
extern "C"  void SubstanceBase_2_set_baseValue_m3914724413_gshared (SubstanceBase_2_t3707538316 * __this, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SubstanceBase_2_set_baseValue_m3914724413_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_externalBaseConnection_2();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_externalBaseConnection_2();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
		__this->set_externalBaseConnection_2((Il2CppObject *)NULL);
	}

IL_001d:
	{
		bool L_2 = ___value0;
		__this->set_localBase_0(L_2);
		NullCheck((SubstanceBase_2_t3707538316 *)__this);
		VirtActionInvoker0::Invoke(17 /* System.Void SubstanceBase`2<System.Boolean,System.Boolean>::UpdateAll() */, (SubstanceBase_2_t3707538316 *)__this);
		return;
	}
}
// System.Void SubstanceBase`2<System.Boolean,System.Boolean>::set_baseValueReactive(ICell`1<T>)
extern "C"  void SubstanceBase_2_set_baseValueReactive_m822111824_gshared (SubstanceBase_2_t3707538316 * __this, Il2CppObject* ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___value0;
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Action_1_t359458046 * L_2 = (Action_1_t359458046 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Action_1_t359458046 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Il2CppObject *)__this, (IntPtr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_3 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t359458046 *, int32_t >::Invoke(0 /* System.IDisposable ICell`1<System.Boolean>::Bind(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_0, (Action_1_t359458046 *)L_2, (int32_t)1);
		__this->set_externalBaseConnection_2(L_3);
		return;
	}
}
// System.IDisposable SubstanceBase`2<System.Boolean,System.Boolean>::Bind(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * SubstanceBase_2_Bind_m276560366_gshared (SubstanceBase_2_t3707538316 * __this, Action_1_t359458046 * ___action0, int32_t ___p1, const MethodInfo* method)
{
	{
		Stream_1_t3197111659 * L_0 = (Stream_1_t3197111659 *)__this->get_update_3();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		Stream_1_t3197111659 * L_1 = (Stream_1_t3197111659 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (Stream_1_t3197111659 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		__this->set_update_3(L_1);
	}

IL_0016:
	{
		Action_1_t359458046 * L_2 = ___action0;
		bool L_3 = (bool)__this->get_result_4();
		NullCheck((Action_1_t359458046 *)L_2);
		((  void (*) (Action_1_t359458046 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((Action_1_t359458046 *)L_2, (bool)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		Stream_1_t3197111659 * L_4 = (Stream_1_t3197111659 *)__this->get_update_3();
		Action_1_t359458046 * L_5 = ___action0;
		int32_t L_6 = ___p1;
		NullCheck((Stream_1_t3197111659 *)L_4);
		Il2CppObject * L_7 = VirtFuncInvoker2< Il2CppObject *, Action_1_t359458046 *, int32_t >::Invoke(8 /* System.IDisposable Stream`1<System.Boolean>::Listen(System.Action`1<T>,Priority) */, (Stream_1_t3197111659 *)L_4, (Action_1_t359458046 *)L_5, (int32_t)L_6);
		return L_7;
	}
}
// System.IDisposable SubstanceBase`2<System.Boolean,System.Boolean>::OnChanged(System.Action,Priority)
extern "C"  Il2CppObject * SubstanceBase_2_OnChanged_m845462693_gshared (SubstanceBase_2_t3707538316 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method)
{
	U3COnChangedU3Ec__AnonStorey142_t1335955226 * V_0 = NULL;
	{
		U3COnChangedU3Ec__AnonStorey142_t1335955226 * L_0 = (U3COnChangedU3Ec__AnonStorey142_t1335955226 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		((  void (*) (U3COnChangedU3Ec__AnonStorey142_t1335955226 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		V_0 = (U3COnChangedU3Ec__AnonStorey142_t1335955226 *)L_0;
		U3COnChangedU3Ec__AnonStorey142_t1335955226 * L_1 = V_0;
		Action_t437523947 * L_2 = ___action0;
		NullCheck(L_1);
		L_1->set_action_0(L_2);
		U3COnChangedU3Ec__AnonStorey142_t1335955226 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		Action_1_t359458046 * L_5 = (Action_1_t359458046 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Action_1_t359458046 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_5, (Il2CppObject *)L_3, (IntPtr_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		int32_t L_6 = ___p1;
		NullCheck((SubstanceBase_2_t3707538316 *)__this);
		Il2CppObject * L_7 = VirtFuncInvoker2< Il2CppObject *, Action_1_t359458046 *, int32_t >::Invoke(7 /* System.IDisposable SubstanceBase`2<System.Boolean,System.Boolean>::ListenUpdates(System.Action`1<T>,Priority) */, (SubstanceBase_2_t3707538316 *)__this, (Action_1_t359458046 *)L_5, (int32_t)L_6);
		return L_7;
	}
}
// System.Object SubstanceBase`2<System.Boolean,System.Boolean>::get_valueObject()
extern "C"  Il2CppObject * SubstanceBase_2_get_valueObject_m2890777104_gshared (SubstanceBase_2_t3707538316 * __this, const MethodInfo* method)
{
	{
		NullCheck((SubstanceBase_2_t3707538316 *)__this);
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(8 /* T SubstanceBase`2<System.Boolean,System.Boolean>::get_value() */, (SubstanceBase_2_t3707538316 *)__this);
		bool L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14), &L_1);
		return L_2;
	}
}
// System.IDisposable SubstanceBase`2<System.Boolean,System.Boolean>::ListenUpdates(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * SubstanceBase_2_ListenUpdates_m3872388564_gshared (SubstanceBase_2_t3707538316 * __this, Action_1_t359458046 * ___action0, int32_t ___p1, const MethodInfo* method)
{
	{
		Stream_1_t3197111659 * L_0 = (Stream_1_t3197111659 *)__this->get_update_3();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		Stream_1_t3197111659 * L_1 = (Stream_1_t3197111659 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (Stream_1_t3197111659 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		__this->set_update_3(L_1);
	}

IL_0016:
	{
		Stream_1_t3197111659 * L_2 = (Stream_1_t3197111659 *)__this->get_update_3();
		Action_1_t359458046 * L_3 = ___action0;
		int32_t L_4 = ___p1;
		NullCheck((Stream_1_t3197111659 *)L_2);
		Il2CppObject * L_5 = VirtFuncInvoker2< Il2CppObject *, Action_1_t359458046 *, int32_t >::Invoke(8 /* System.IDisposable Stream`1<System.Boolean>::Listen(System.Action`1<T>,Priority) */, (Stream_1_t3197111659 *)L_2, (Action_1_t359458046 *)L_3, (int32_t)L_4);
		return L_5;
	}
}
// IStream`1<T> SubstanceBase`2<System.Boolean,System.Boolean>::get_updates()
extern "C"  Il2CppObject* SubstanceBase_2_get_updates_m2333122798_gshared (SubstanceBase_2_t3707538316 * __this, const MethodInfo* method)
{
	Stream_1_t3197111659 * V_0 = NULL;
	Stream_1_t3197111659 * G_B2_0 = NULL;
	Stream_1_t3197111659 * G_B1_0 = NULL;
	{
		Stream_1_t3197111659 * L_0 = (Stream_1_t3197111659 *)__this->get_update_3();
		Stream_1_t3197111659 * L_1 = (Stream_1_t3197111659 *)L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Stream_1_t3197111659 * L_2 = (Stream_1_t3197111659 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (Stream_1_t3197111659 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		Stream_1_t3197111659 * L_3 = (Stream_1_t3197111659 *)L_2;
		V_0 = (Stream_1_t3197111659 *)L_3;
		__this->set_update_3(L_3);
		Stream_1_t3197111659 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// T SubstanceBase`2<System.Boolean,System.Boolean>::get_value()
extern Il2CppClass* Transaction_t3809114814_il2cpp_TypeInfo_var;
extern const uint32_t SubstanceBase_2_get_value_m3696610663_MetadataUsageId;
extern "C"  bool SubstanceBase_2_get_value_m3696610663_gshared (SubstanceBase_2_t3707538316 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SubstanceBase_2_get_value_m3696610663_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_0 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_calculationMode_8();
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		Transaction_AddTouchedCell_m3715775313(NULL /*static, unused*/, (Il2CppObject *)__this, /*hidden argument*/NULL);
	}

IL_0010:
	{
		bool L_1 = (bool)__this->get_result_4();
		return L_1;
	}
}
// SubstanceBase`2/Intrusion<T,InfT> SubstanceBase`2<System.Boolean,System.Boolean>::AffectInner(ILiving,InfT)
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral968526109;
extern const uint32_t SubstanceBase_2_AffectInner_m2503779489_MetadataUsageId;
extern "C"  Intrusion_t1917188776 * SubstanceBase_2_AffectInner_m2503779489_gshared (SubstanceBase_2_t3707538316 * __this, Il2CppObject * ___intruder0, bool ___influence1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SubstanceBase_2_AffectInner_m2503779489_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Intrusion_t1917188776 * V_0 = NULL;
	Intrusion_t1917188776 * V_1 = NULL;
	{
		Il2CppObject * L_0 = ___intruder0;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m1836388681(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral968526109, /*hidden argument*/NULL);
		Intrusion_t1917188776 * L_1 = (Intrusion_t1917188776 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		((  void (*) (Intrusion_t1917188776 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		return L_1;
	}

IL_0016:
	{
		List_1_t2714147745 * L_2 = (List_1_t2714147745 *)__this->get_intrusions_5();
		if (L_2)
		{
			goto IL_002c;
		}
	}
	{
		List_1_t2714147745 * L_3 = (List_1_t2714147745 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		((  void (*) (List_1_t2714147745 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->method)(L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
		__this->set_intrusions_5(L_3);
	}

IL_002c:
	{
		Intrusion_t1917188776 * L_4 = (Intrusion_t1917188776 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		((  void (*) (Intrusion_t1917188776 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)(L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		V_1 = (Intrusion_t1917188776 *)L_4;
		Intrusion_t1917188776 * L_5 = V_1;
		Il2CppObject * L_6 = ___intruder0;
		NullCheck(L_5);
		L_5->set_intruder_0(L_6);
		Intrusion_t1917188776 * L_7 = V_1;
		bool L_8 = ___influence1;
		NullCheck(L_7);
		L_7->set_influence_1(L_8);
		Intrusion_t1917188776 * L_9 = V_1;
		V_0 = (Intrusion_t1917188776 *)L_9;
		List_1_t2714147745 * L_10 = (List_1_t2714147745 *)__this->get_intrusions_5();
		Intrusion_t1917188776 * L_11 = V_0;
		NullCheck((List_1_t2714147745 *)L_10);
		VirtActionInvoker1< Intrusion_t1917188776 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<System.Boolean,System.Boolean>>::Add(!0) */, (List_1_t2714147745 *)L_10, (Intrusion_t1917188776 *)L_11);
		Intrusion_t1917188776 * L_12 = V_0;
		NullCheck((SubstanceBase_2_t3707538316 *)__this);
		VirtActionInvoker1< Intrusion_t1917188776 * >::Invoke(14 /* System.Void SubstanceBase`2<System.Boolean,System.Boolean>::OnAdd(SubstanceBase`2/Intrusion<T,InfT>) */, (SubstanceBase_2_t3707538316 *)__this, (Intrusion_t1917188776 *)L_12);
		Intrusion_t1917188776 * L_13 = V_0;
		return L_13;
	}
}
// System.IDisposable SubstanceBase`2<System.Boolean,System.Boolean>::AffectUnsafe(InfT)
extern Il2CppClass* TagCollector_t573861427_il2cpp_TypeInfo_var;
extern const uint32_t SubstanceBase_2_AffectUnsafe_m4234747039_MetadataUsageId;
extern "C"  Il2CppObject * SubstanceBase_2_AffectUnsafe_m4234747039_gshared (SubstanceBase_2_t3707538316 * __this, bool ___influence0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SubstanceBase_2_AffectUnsafe_m4234747039_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TagCollector_t573861427 * L_0 = (TagCollector_t573861427 *)il2cpp_codegen_object_new(TagCollector_t573861427_il2cpp_TypeInfo_var);
		TagCollector__ctor_m3645951432(L_0, /*hidden argument*/NULL);
		bool L_1 = ___influence0;
		NullCheck((SubstanceBase_2_t3707538316 *)__this);
		Il2CppObject * L_2 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject *, bool >::Invoke(11 /* System.IDisposable SubstanceBase`2<System.Boolean,System.Boolean>::Affect(ILiving,InfT) */, (SubstanceBase_2_t3707538316 *)__this, (Il2CppObject *)L_0, (bool)L_1);
		return L_2;
	}
}
// System.IDisposable SubstanceBase`2<System.Boolean,System.Boolean>::AffectUnsafe(ICell`1<InfT>)
extern Il2CppClass* TagCollector_t573861427_il2cpp_TypeInfo_var;
extern const uint32_t SubstanceBase_2_AffectUnsafe_m3711033429_MetadataUsageId;
extern "C"  Il2CppObject * SubstanceBase_2_AffectUnsafe_m3711033429_gshared (SubstanceBase_2_t3707538316 * __this, Il2CppObject* ___influence0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SubstanceBase_2_AffectUnsafe_m3711033429_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TagCollector_t573861427 * L_0 = (TagCollector_t573861427 *)il2cpp_codegen_object_new(TagCollector_t573861427_il2cpp_TypeInfo_var);
		TagCollector__ctor_m3645951432(L_0, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___influence0;
		NullCheck((SubstanceBase_2_t3707538316 *)__this);
		Il2CppObject * L_2 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject *, Il2CppObject* >::Invoke(12 /* System.IDisposable SubstanceBase`2<System.Boolean,System.Boolean>::Affect(ILiving,ICell`1<InfT>) */, (SubstanceBase_2_t3707538316 *)__this, (Il2CppObject *)L_0, (Il2CppObject*)L_1);
		return L_2;
	}
}
// System.IDisposable SubstanceBase`2<System.Boolean,System.Boolean>::Affect(ILiving,InfT)
extern Il2CppClass* ILiving_t2639664210_il2cpp_TypeInfo_var;
extern const uint32_t SubstanceBase_2_Affect_m2462561983_MetadataUsageId;
extern "C"  Il2CppObject * SubstanceBase_2_Affect_m2462561983_gshared (SubstanceBase_2_t3707538316 * __this, Il2CppObject * ___intruder0, bool ___influence1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SubstanceBase_2_Affect_m2462561983_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Intrusion_t1917188776 * V_0 = NULL;
	DisposableAffect_t2225548402 * V_1 = NULL;
	DisposableAffect_t2225548402 * V_2 = NULL;
	{
		Il2CppObject * L_0 = ___intruder0;
		bool L_1 = ___influence1;
		NullCheck((SubstanceBase_2_t3707538316 *)__this);
		Intrusion_t1917188776 * L_2 = ((  Intrusion_t1917188776 * (*) (SubstanceBase_2_t3707538316 *, Il2CppObject *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23)->method)((SubstanceBase_2_t3707538316 *)__this, (Il2CppObject *)L_0, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23));
		V_0 = (Intrusion_t1917188776 *)L_2;
		DisposableAffect_t2225548402 * L_3 = (DisposableAffect_t2225548402 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		((  void (*) (DisposableAffect_t2225548402 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25)->method)(L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25));
		V_2 = (DisposableAffect_t2225548402 *)L_3;
		DisposableAffect_t2225548402 * L_4 = V_2;
		Intrusion_t1917188776 * L_5 = V_0;
		NullCheck(L_4);
		L_4->set_intrusion_0(L_5);
		DisposableAffect_t2225548402 * L_6 = V_2;
		NullCheck(L_6);
		L_6->set_substance_1(__this);
		DisposableAffect_t2225548402 * L_7 = V_2;
		V_1 = (DisposableAffect_t2225548402 *)L_7;
		Il2CppObject * L_8 = ___intruder0;
		DisposableAffect_t2225548402 * L_9 = V_1;
		NullCheck((Il2CppObject *)L_8);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(1 /* System.Void ILiving::DisposeWhenAsleep(System.IDisposable) */, ILiving_t2639664210_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Il2CppObject *)L_9);
		DisposableAffect_t2225548402 * L_10 = V_1;
		return L_10;
	}
}
// System.IDisposable SubstanceBase`2<System.Boolean,System.Boolean>::Affect(ILiving,ICell`1<InfT>)
extern Il2CppClass* ILiving_t2639664210_il2cpp_TypeInfo_var;
extern const uint32_t SubstanceBase_2_Affect_m1204726325_MetadataUsageId;
extern "C"  Il2CppObject * SubstanceBase_2_Affect_m1204726325_gshared (SubstanceBase_2_t3707538316 * __this, Il2CppObject * ___intruder0, Il2CppObject* ___influence1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SubstanceBase_2_Affect_m1204726325_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DisposableAffect_t2225548402 * V_0 = NULL;
	U3CAffectU3Ec__AnonStorey143_t390189525 * V_1 = NULL;
	DisposableAffect_t2225548402 * V_2 = NULL;
	{
		U3CAffectU3Ec__AnonStorey143_t390189525 * L_0 = (U3CAffectU3Ec__AnonStorey143_t390189525 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26));
		((  void (*) (U3CAffectU3Ec__AnonStorey143_t390189525 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27));
		V_1 = (U3CAffectU3Ec__AnonStorey143_t390189525 *)L_0;
		U3CAffectU3Ec__AnonStorey143_t390189525 * L_1 = V_1;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_1(__this);
		U3CAffectU3Ec__AnonStorey143_t390189525 * L_2 = V_1;
		Il2CppObject * L_3 = ___intruder0;
		Il2CppObject* L_4 = ___influence1;
		NullCheck((Il2CppObject*)L_4);
		bool L_5 = InterfaceFuncInvoker0< bool >::Invoke(2 /* T ICell`1<System.Boolean>::get_value() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28), (Il2CppObject*)L_4);
		NullCheck((SubstanceBase_2_t3707538316 *)__this);
		Intrusion_t1917188776 * L_6 = ((  Intrusion_t1917188776 * (*) (SubstanceBase_2_t3707538316 *, Il2CppObject *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23)->method)((SubstanceBase_2_t3707538316 *)__this, (Il2CppObject *)L_3, (bool)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23));
		NullCheck(L_2);
		L_2->set_intrusion_0(L_6);
		U3CAffectU3Ec__AnonStorey143_t390189525 * L_7 = V_1;
		NullCheck(L_7);
		Intrusion_t1917188776 * L_8 = (Intrusion_t1917188776 *)L_7->get_intrusion_0();
		Il2CppObject* L_9 = ___influence1;
		U3CAffectU3Ec__AnonStorey143_t390189525 * L_10 = V_1;
		IntPtr_t L_11;
		L_11.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29));
		Action_1_t359458046 * L_12 = (Action_1_t359458046 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30));
		((  void (*) (Action_1_t359458046 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31)->method)(L_12, (Il2CppObject *)L_10, (IntPtr_t)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31));
		NullCheck((Il2CppObject*)L_9);
		Il2CppObject * L_13 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t359458046 *, int32_t >::Invoke(1 /* System.IDisposable ICell`1<System.Boolean>::ListenUpdates(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28), (Il2CppObject*)L_9, (Action_1_t359458046 *)L_12, (int32_t)1);
		NullCheck(L_8);
		L_8->set_updateConnection_3(L_13);
		DisposableAffect_t2225548402 * L_14 = (DisposableAffect_t2225548402 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		((  void (*) (DisposableAffect_t2225548402 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25)->method)(L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25));
		V_2 = (DisposableAffect_t2225548402 *)L_14;
		DisposableAffect_t2225548402 * L_15 = V_2;
		U3CAffectU3Ec__AnonStorey143_t390189525 * L_16 = V_1;
		NullCheck(L_16);
		Intrusion_t1917188776 * L_17 = (Intrusion_t1917188776 *)L_16->get_intrusion_0();
		NullCheck(L_15);
		L_15->set_intrusion_0(L_17);
		DisposableAffect_t2225548402 * L_18 = V_2;
		NullCheck(L_18);
		L_18->set_substance_1(__this);
		DisposableAffect_t2225548402 * L_19 = V_2;
		V_0 = (DisposableAffect_t2225548402 *)L_19;
		Il2CppObject * L_20 = ___intruder0;
		DisposableAffect_t2225548402 * L_21 = V_0;
		NullCheck((Il2CppObject *)L_20);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(1 /* System.Void ILiving::DisposeWhenAsleep(System.IDisposable) */, ILiving_t2639664210_il2cpp_TypeInfo_var, (Il2CppObject *)L_20, (Il2CppObject *)L_21);
		DisposableAffect_t2225548402 * L_22 = V_0;
		return L_22;
	}
}
// System.IDisposable SubstanceBase`2<System.Boolean,System.Boolean>::Affect(ILiving,InfT,IEmptyStream)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IEmptyStream_t3684082468_il2cpp_TypeInfo_var;
extern Il2CppClass* ILiving_t2639664210_il2cpp_TypeInfo_var;
extern const uint32_t SubstanceBase_2_Affect_m3611334147_MetadataUsageId;
extern "C"  Il2CppObject * SubstanceBase_2_Affect_m3611334147_gshared (SubstanceBase_2_t3707538316 * __this, Il2CppObject * ___intruder0, bool ___influence1, Il2CppObject * ___update2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SubstanceBase_2_Affect_m3611334147_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DisposableAffect_t2225548402 * V_0 = NULL;
	U3CAffectU3Ec__AnonStorey144_t386862328 * V_1 = NULL;
	DisposableAffect_t2225548402 * V_2 = NULL;
	{
		U3CAffectU3Ec__AnonStorey144_t386862328 * L_0 = (U3CAffectU3Ec__AnonStorey144_t386862328 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32));
		((  void (*) (U3CAffectU3Ec__AnonStorey144_t386862328 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 33)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 33));
		V_1 = (U3CAffectU3Ec__AnonStorey144_t386862328 *)L_0;
		U3CAffectU3Ec__AnonStorey144_t386862328 * L_1 = V_1;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_1(__this);
		U3CAffectU3Ec__AnonStorey144_t386862328 * L_2 = V_1;
		Il2CppObject * L_3 = ___intruder0;
		bool L_4 = ___influence1;
		NullCheck((SubstanceBase_2_t3707538316 *)__this);
		Intrusion_t1917188776 * L_5 = ((  Intrusion_t1917188776 * (*) (SubstanceBase_2_t3707538316 *, Il2CppObject *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23)->method)((SubstanceBase_2_t3707538316 *)__this, (Il2CppObject *)L_3, (bool)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23));
		NullCheck(L_2);
		L_2->set_intrusion_0(L_5);
		U3CAffectU3Ec__AnonStorey144_t386862328 * L_6 = V_1;
		NullCheck(L_6);
		Intrusion_t1917188776 * L_7 = (Intrusion_t1917188776 *)L_6->get_intrusion_0();
		Il2CppObject * L_8 = ___update2;
		U3CAffectU3Ec__AnonStorey144_t386862328 * L_9 = V_1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 34));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_8);
		Il2CppObject * L_12 = InterfaceFuncInvoker2< Il2CppObject *, Action_t437523947 *, int32_t >::Invoke(0 /* System.IDisposable IEmptyStream::Listen(System.Action,Priority) */, IEmptyStream_t3684082468_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Action_t437523947 *)L_11, (int32_t)1);
		NullCheck(L_7);
		L_7->set_updateConnection_3(L_12);
		DisposableAffect_t2225548402 * L_13 = (DisposableAffect_t2225548402 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		((  void (*) (DisposableAffect_t2225548402 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25)->method)(L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25));
		V_2 = (DisposableAffect_t2225548402 *)L_13;
		DisposableAffect_t2225548402 * L_14 = V_2;
		U3CAffectU3Ec__AnonStorey144_t386862328 * L_15 = V_1;
		NullCheck(L_15);
		Intrusion_t1917188776 * L_16 = (Intrusion_t1917188776 *)L_15->get_intrusion_0();
		NullCheck(L_14);
		L_14->set_intrusion_0(L_16);
		DisposableAffect_t2225548402 * L_17 = V_2;
		NullCheck(L_17);
		L_17->set_substance_1(__this);
		DisposableAffect_t2225548402 * L_18 = V_2;
		V_0 = (DisposableAffect_t2225548402 *)L_18;
		Il2CppObject * L_19 = ___intruder0;
		DisposableAffect_t2225548402 * L_20 = V_0;
		NullCheck((Il2CppObject *)L_19);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(1 /* System.Void ILiving::DisposeWhenAsleep(System.IDisposable) */, ILiving_t2639664210_il2cpp_TypeInfo_var, (Il2CppObject *)L_19, (Il2CppObject *)L_20);
		DisposableAffect_t2225548402 * L_21 = V_0;
		return L_21;
	}
}
// System.Void SubstanceBase`2<System.Boolean,System.Boolean>::OnAdd(SubstanceBase`2/Intrusion<T,InfT>)
extern "C"  void SubstanceBase_2_OnAdd_m1962503088_gshared (SubstanceBase_2_t3707538316 * __this, Intrusion_t1917188776 * ___intrusion0, const MethodInfo* method)
{
	{
		NullCheck((SubstanceBase_2_t3707538316 *)__this);
		VirtActionInvoker0::Invoke(17 /* System.Void SubstanceBase`2<System.Boolean,System.Boolean>::UpdateAll() */, (SubstanceBase_2_t3707538316 *)__this);
		return;
	}
}
// System.Void SubstanceBase`2<System.Boolean,System.Boolean>::OnRemove(SubstanceBase`2/Intrusion<T,InfT>)
extern "C"  void SubstanceBase_2_OnRemove_m4132644265_gshared (SubstanceBase_2_t3707538316 * __this, Intrusion_t1917188776 * ___intrusion0, const MethodInfo* method)
{
	{
		NullCheck((SubstanceBase_2_t3707538316 *)__this);
		VirtActionInvoker0::Invoke(17 /* System.Void SubstanceBase`2<System.Boolean,System.Boolean>::UpdateAll() */, (SubstanceBase_2_t3707538316 *)__this);
		return;
	}
}
// System.Void SubstanceBase`2<System.Boolean,System.Boolean>::OnUpdate(SubstanceBase`2/Intrusion<T,InfT>)
extern "C"  void SubstanceBase_2_OnUpdate_m3483375428_gshared (SubstanceBase_2_t3707538316 * __this, Intrusion_t1917188776 * ___intrusion0, const MethodInfo* method)
{
	{
		NullCheck((SubstanceBase_2_t3707538316 *)__this);
		VirtActionInvoker0::Invoke(17 /* System.Void SubstanceBase`2<System.Boolean,System.Boolean>::UpdateAll() */, (SubstanceBase_2_t3707538316 *)__this);
		return;
	}
}
// System.Void SubstanceBase`2<System.Boolean,System.Boolean>::UpdateAll()
extern "C"  void SubstanceBase_2_UpdateAll_m2304746742_gshared (SubstanceBase_2_t3707538316 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		NullCheck((SubstanceBase_2_t3707538316 *)__this);
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(18 /* T SubstanceBase`2<System.Boolean,System.Boolean>::Result() */, (SubstanceBase_2_t3707538316 *)__this);
		V_0 = (bool)L_0;
		bool L_1 = V_0;
		bool L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14), &L_2);
		bool L_4 = (bool)__this->get_result_4();
		bool L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14), &L_5);
		bool L_7 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0045;
		}
	}
	{
		bool L_8 = V_0;
		__this->set_result_4(L_8);
		Stream_1_t3197111659 * L_9 = (Stream_1_t3197111659 *)__this->get_update_3();
		if (!L_9)
		{
			goto IL_0045;
		}
	}
	{
		Stream_1_t3197111659 * L_10 = (Stream_1_t3197111659 *)__this->get_update_3();
		bool L_11 = (bool)__this->get_result_4();
		NullCheck((Stream_1_t3197111659 *)L_10);
		((  void (*) (Stream_1_t3197111659 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 36)->method)((Stream_1_t3197111659 *)L_10, (bool)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 36));
	}

IL_0045:
	{
		return;
	}
}
// System.Void SubstanceBase`2<System.Boolean,System.Boolean>::ClearIntrusion(SubstanceBase`2/Intrusion<T,InfT>)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t SubstanceBase_2_ClearIntrusion_m1183670130_MetadataUsageId;
extern "C"  void SubstanceBase_2_ClearIntrusion_m1183670130_gshared (SubstanceBase_2_t3707538316 * __this, Intrusion_t1917188776 * ___intr0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SubstanceBase_2_ClearIntrusion_m1183670130_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t2714147745 * L_0 = (List_1_t2714147745 *)__this->get_intrusions_5();
		Intrusion_t1917188776 * L_1 = ___intr0;
		NullCheck((List_1_t2714147745 *)L_0);
		VirtFuncInvoker1< bool, Intrusion_t1917188776 * >::Invoke(26 /* System.Boolean System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<System.Boolean,System.Boolean>>::Remove(!0) */, (List_1_t2714147745 *)L_0, (Intrusion_t1917188776 *)L_1);
		Intrusion_t1917188776 * L_2 = ___intr0;
		NullCheck(L_2);
		Il2CppObject * L_3 = (Il2CppObject *)L_2->get_awakeConnection_2();
		if (!L_3)
		{
			goto IL_0023;
		}
	}
	{
		Intrusion_t1917188776 * L_4 = ___intr0;
		NullCheck(L_4);
		Il2CppObject * L_5 = (Il2CppObject *)L_4->get_awakeConnection_2();
		NullCheck((Il2CppObject *)L_5);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
	}

IL_0023:
	{
		Intrusion_t1917188776 * L_6 = ___intr0;
		NullCheck(L_6);
		Il2CppObject * L_7 = (Il2CppObject *)L_6->get_updateConnection_3();
		if (!L_7)
		{
			goto IL_0039;
		}
	}
	{
		Intrusion_t1917188776 * L_8 = ___intr0;
		NullCheck(L_8);
		Il2CppObject * L_9 = (Il2CppObject *)L_8->get_updateConnection_3();
		NullCheck((Il2CppObject *)L_9);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_9);
	}

IL_0039:
	{
		Intrusion_t1917188776 * L_10 = ___intr0;
		NullCheck((SubstanceBase_2_t3707538316 *)__this);
		VirtActionInvoker1< Intrusion_t1917188776 * >::Invoke(15 /* System.Void SubstanceBase`2<System.Boolean,System.Boolean>::OnRemove(SubstanceBase`2/Intrusion<T,InfT>) */, (SubstanceBase_2_t3707538316 *)__this, (Intrusion_t1917188776 *)L_10);
		return;
	}
}
// System.Void SubstanceBase`2<System.Boolean,System.Boolean>::<set_baseValueReactive>m__1D7(T)
extern "C"  void SubstanceBase_2_U3Cset_baseValueReactiveU3Em__1D7_m797099883_gshared (SubstanceBase_2_t3707538316 * __this, bool ___val0, const MethodInfo* method)
{
	{
		bool L_0 = ___val0;
		NullCheck((SubstanceBase_2_t3707538316 *)__this);
		((  void (*) (SubstanceBase_2_t3707538316 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39)->method)((SubstanceBase_2_t3707538316 *)__this, (bool)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39));
		return;
	}
}
// System.Void SubstanceBase`2<System.Double,System.Double>::.ctor()
extern "C"  void SubstanceBase_2__ctor_m3287773736_gshared (SubstanceBase_2_t1274403272 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SubstanceBase`2<System.Double,System.Double>::.ctor(T)
extern "C"  void SubstanceBase_2__ctor_m3136739382_gshared (SubstanceBase_2_t1274403272 * __this, double ___baseVal0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		double L_0 = ___baseVal0;
		__this->set_localBase_0(L_0);
		double L_1 = (double)__this->get_localBase_0();
		__this->set_result_4(L_1);
		return;
	}
}
// T SubstanceBase`2<System.Double,System.Double>::get_baseValue()
extern "C"  double SubstanceBase_2_get_baseValue_m1197418750_gshared (SubstanceBase_2_t1274403272 * __this, const MethodInfo* method)
{
	{
		double L_0 = (double)__this->get_localBase_0();
		return L_0;
	}
}
// System.Void SubstanceBase`2<System.Double,System.Double>::set_baseValue(T)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t SubstanceBase_2_set_baseValue_m2977028949_MetadataUsageId;
extern "C"  void SubstanceBase_2_set_baseValue_m2977028949_gshared (SubstanceBase_2_t1274403272 * __this, double ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SubstanceBase_2_set_baseValue_m2977028949_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_externalBaseConnection_2();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_externalBaseConnection_2();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
		__this->set_externalBaseConnection_2((Il2CppObject *)NULL);
	}

IL_001d:
	{
		double L_2 = ___value0;
		__this->set_localBase_0(L_2);
		NullCheck((SubstanceBase_2_t1274403272 *)__this);
		VirtActionInvoker0::Invoke(17 /* System.Void SubstanceBase`2<System.Double,System.Double>::UpdateAll() */, (SubstanceBase_2_t1274403272 *)__this);
		return;
	}
}
// System.Void SubstanceBase`2<System.Double,System.Double>::set_baseValueReactive(ICell`1<T>)
extern "C"  void SubstanceBase_2_set_baseValueReactive_m1392286264_gshared (SubstanceBase_2_t1274403272 * __this, Il2CppObject* ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___value0;
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Action_1_t682969319 * L_2 = (Action_1_t682969319 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Action_1_t682969319 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Il2CppObject *)__this, (IntPtr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_3 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t682969319 *, int32_t >::Invoke(0 /* System.IDisposable ICell`1<System.Double>::Bind(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_0, (Action_1_t682969319 *)L_2, (int32_t)1);
		__this->set_externalBaseConnection_2(L_3);
		return;
	}
}
// System.IDisposable SubstanceBase`2<System.Double,System.Double>::Bind(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * SubstanceBase_2_Bind_m3273213206_gshared (SubstanceBase_2_t1274403272 * __this, Action_1_t682969319 * ___action0, int32_t ___p1, const MethodInfo* method)
{
	{
		Stream_1_t3520622932 * L_0 = (Stream_1_t3520622932 *)__this->get_update_3();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		Stream_1_t3520622932 * L_1 = (Stream_1_t3520622932 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (Stream_1_t3520622932 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		__this->set_update_3(L_1);
	}

IL_0016:
	{
		Action_1_t682969319 * L_2 = ___action0;
		double L_3 = (double)__this->get_result_4();
		NullCheck((Action_1_t682969319 *)L_2);
		((  void (*) (Action_1_t682969319 *, double, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((Action_1_t682969319 *)L_2, (double)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		Stream_1_t3520622932 * L_4 = (Stream_1_t3520622932 *)__this->get_update_3();
		Action_1_t682969319 * L_5 = ___action0;
		int32_t L_6 = ___p1;
		NullCheck((Stream_1_t3520622932 *)L_4);
		Il2CppObject * L_7 = VirtFuncInvoker2< Il2CppObject *, Action_1_t682969319 *, int32_t >::Invoke(8 /* System.IDisposable Stream`1<System.Double>::Listen(System.Action`1<T>,Priority) */, (Stream_1_t3520622932 *)L_4, (Action_1_t682969319 *)L_5, (int32_t)L_6);
		return L_7;
	}
}
// System.IDisposable SubstanceBase`2<System.Double,System.Double>::OnChanged(System.Action,Priority)
extern "C"  Il2CppObject * SubstanceBase_2_OnChanged_m3842115533_gshared (SubstanceBase_2_t1274403272 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method)
{
	U3COnChangedU3Ec__AnonStorey142_t3197787478 * V_0 = NULL;
	{
		U3COnChangedU3Ec__AnonStorey142_t3197787478 * L_0 = (U3COnChangedU3Ec__AnonStorey142_t3197787478 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		((  void (*) (U3COnChangedU3Ec__AnonStorey142_t3197787478 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		V_0 = (U3COnChangedU3Ec__AnonStorey142_t3197787478 *)L_0;
		U3COnChangedU3Ec__AnonStorey142_t3197787478 * L_1 = V_0;
		Action_t437523947 * L_2 = ___action0;
		NullCheck(L_1);
		L_1->set_action_0(L_2);
		U3COnChangedU3Ec__AnonStorey142_t3197787478 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		Action_1_t682969319 * L_5 = (Action_1_t682969319 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Action_1_t682969319 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_5, (Il2CppObject *)L_3, (IntPtr_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		int32_t L_6 = ___p1;
		NullCheck((SubstanceBase_2_t1274403272 *)__this);
		Il2CppObject * L_7 = VirtFuncInvoker2< Il2CppObject *, Action_1_t682969319 *, int32_t >::Invoke(7 /* System.IDisposable SubstanceBase`2<System.Double,System.Double>::ListenUpdates(System.Action`1<T>,Priority) */, (SubstanceBase_2_t1274403272 *)__this, (Action_1_t682969319 *)L_5, (int32_t)L_6);
		return L_7;
	}
}
// System.Object SubstanceBase`2<System.Double,System.Double>::get_valueObject()
extern "C"  Il2CppObject * SubstanceBase_2_get_valueObject_m2827238584_gshared (SubstanceBase_2_t1274403272 * __this, const MethodInfo* method)
{
	{
		NullCheck((SubstanceBase_2_t1274403272 *)__this);
		double L_0 = VirtFuncInvoker0< double >::Invoke(8 /* T SubstanceBase`2<System.Double,System.Double>::get_value() */, (SubstanceBase_2_t1274403272 *)__this);
		double L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14), &L_1);
		return L_2;
	}
}
// System.IDisposable SubstanceBase`2<System.Double,System.Double>::ListenUpdates(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * SubstanceBase_2_ListenUpdates_m2221979564_gshared (SubstanceBase_2_t1274403272 * __this, Action_1_t682969319 * ___action0, int32_t ___p1, const MethodInfo* method)
{
	{
		Stream_1_t3520622932 * L_0 = (Stream_1_t3520622932 *)__this->get_update_3();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		Stream_1_t3520622932 * L_1 = (Stream_1_t3520622932 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (Stream_1_t3520622932 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		__this->set_update_3(L_1);
	}

IL_0016:
	{
		Stream_1_t3520622932 * L_2 = (Stream_1_t3520622932 *)__this->get_update_3();
		Action_1_t682969319 * L_3 = ___action0;
		int32_t L_4 = ___p1;
		NullCheck((Stream_1_t3520622932 *)L_2);
		Il2CppObject * L_5 = VirtFuncInvoker2< Il2CppObject *, Action_1_t682969319 *, int32_t >::Invoke(8 /* System.IDisposable Stream`1<System.Double>::Listen(System.Action`1<T>,Priority) */, (Stream_1_t3520622932 *)L_2, (Action_1_t682969319 *)L_3, (int32_t)L_4);
		return L_5;
	}
}
// IStream`1<T> SubstanceBase`2<System.Double,System.Double>::get_updates()
extern "C"  Il2CppObject* SubstanceBase_2_get_updates_m3563169942_gshared (SubstanceBase_2_t1274403272 * __this, const MethodInfo* method)
{
	Stream_1_t3520622932 * V_0 = NULL;
	Stream_1_t3520622932 * G_B2_0 = NULL;
	Stream_1_t3520622932 * G_B1_0 = NULL;
	{
		Stream_1_t3520622932 * L_0 = (Stream_1_t3520622932 *)__this->get_update_3();
		Stream_1_t3520622932 * L_1 = (Stream_1_t3520622932 *)L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Stream_1_t3520622932 * L_2 = (Stream_1_t3520622932 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (Stream_1_t3520622932 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		Stream_1_t3520622932 * L_3 = (Stream_1_t3520622932 *)L_2;
		V_0 = (Stream_1_t3520622932 *)L_3;
		__this->set_update_3(L_3);
		Stream_1_t3520622932 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// T SubstanceBase`2<System.Double,System.Double>::get_value()
extern Il2CppClass* Transaction_t3809114814_il2cpp_TypeInfo_var;
extern const uint32_t SubstanceBase_2_get_value_m1555147663_MetadataUsageId;
extern "C"  double SubstanceBase_2_get_value_m1555147663_gshared (SubstanceBase_2_t1274403272 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SubstanceBase_2_get_value_m1555147663_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_0 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_calculationMode_8();
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		Transaction_AddTouchedCell_m3715775313(NULL /*static, unused*/, (Il2CppObject *)__this, /*hidden argument*/NULL);
	}

IL_0010:
	{
		double L_1 = (double)__this->get_result_4();
		return L_1;
	}
}
// SubstanceBase`2/Intrusion<T,InfT> SubstanceBase`2<System.Double,System.Double>::AffectInner(ILiving,InfT)
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral968526109;
extern const uint32_t SubstanceBase_2_AffectInner_m1808201801_MetadataUsageId;
extern "C"  Intrusion_t3779021028 * SubstanceBase_2_AffectInner_m1808201801_gshared (SubstanceBase_2_t1274403272 * __this, Il2CppObject * ___intruder0, double ___influence1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SubstanceBase_2_AffectInner_m1808201801_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Intrusion_t3779021028 * V_0 = NULL;
	Intrusion_t3779021028 * V_1 = NULL;
	{
		Il2CppObject * L_0 = ___intruder0;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m1836388681(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral968526109, /*hidden argument*/NULL);
		Intrusion_t3779021028 * L_1 = (Intrusion_t3779021028 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		((  void (*) (Intrusion_t3779021028 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		return L_1;
	}

IL_0016:
	{
		List_1_t281012701 * L_2 = (List_1_t281012701 *)__this->get_intrusions_5();
		if (L_2)
		{
			goto IL_002c;
		}
	}
	{
		List_1_t281012701 * L_3 = (List_1_t281012701 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		((  void (*) (List_1_t281012701 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->method)(L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
		__this->set_intrusions_5(L_3);
	}

IL_002c:
	{
		Intrusion_t3779021028 * L_4 = (Intrusion_t3779021028 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		((  void (*) (Intrusion_t3779021028 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)(L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		V_1 = (Intrusion_t3779021028 *)L_4;
		Intrusion_t3779021028 * L_5 = V_1;
		Il2CppObject * L_6 = ___intruder0;
		NullCheck(L_5);
		L_5->set_intruder_0(L_6);
		Intrusion_t3779021028 * L_7 = V_1;
		double L_8 = ___influence1;
		NullCheck(L_7);
		L_7->set_influence_1(L_8);
		Intrusion_t3779021028 * L_9 = V_1;
		V_0 = (Intrusion_t3779021028 *)L_9;
		List_1_t281012701 * L_10 = (List_1_t281012701 *)__this->get_intrusions_5();
		Intrusion_t3779021028 * L_11 = V_0;
		NullCheck((List_1_t281012701 *)L_10);
		VirtActionInvoker1< Intrusion_t3779021028 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<System.Double,System.Double>>::Add(!0) */, (List_1_t281012701 *)L_10, (Intrusion_t3779021028 *)L_11);
		Intrusion_t3779021028 * L_12 = V_0;
		NullCheck((SubstanceBase_2_t1274403272 *)__this);
		VirtActionInvoker1< Intrusion_t3779021028 * >::Invoke(14 /* System.Void SubstanceBase`2<System.Double,System.Double>::OnAdd(SubstanceBase`2/Intrusion<T,InfT>) */, (SubstanceBase_2_t1274403272 *)__this, (Intrusion_t3779021028 *)L_12);
		Intrusion_t3779021028 * L_13 = V_0;
		return L_13;
	}
}
// System.IDisposable SubstanceBase`2<System.Double,System.Double>::AffectUnsafe(InfT)
extern Il2CppClass* TagCollector_t573861427_il2cpp_TypeInfo_var;
extern const uint32_t SubstanceBase_2_AffectUnsafe_m1121183351_MetadataUsageId;
extern "C"  Il2CppObject * SubstanceBase_2_AffectUnsafe_m1121183351_gshared (SubstanceBase_2_t1274403272 * __this, double ___influence0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SubstanceBase_2_AffectUnsafe_m1121183351_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TagCollector_t573861427 * L_0 = (TagCollector_t573861427 *)il2cpp_codegen_object_new(TagCollector_t573861427_il2cpp_TypeInfo_var);
		TagCollector__ctor_m3645951432(L_0, /*hidden argument*/NULL);
		double L_1 = ___influence0;
		NullCheck((SubstanceBase_2_t1274403272 *)__this);
		Il2CppObject * L_2 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject *, double >::Invoke(11 /* System.IDisposable SubstanceBase`2<System.Double,System.Double>::Affect(ILiving,InfT) */, (SubstanceBase_2_t1274403272 *)__this, (Il2CppObject *)L_0, (double)L_1);
		return L_2;
	}
}
// System.IDisposable SubstanceBase`2<System.Double,System.Double>::AffectUnsafe(ICell`1<InfT>)
extern Il2CppClass* TagCollector_t573861427_il2cpp_TypeInfo_var;
extern const uint32_t SubstanceBase_2_AffectUnsafe_m4124092797_MetadataUsageId;
extern "C"  Il2CppObject * SubstanceBase_2_AffectUnsafe_m4124092797_gshared (SubstanceBase_2_t1274403272 * __this, Il2CppObject* ___influence0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SubstanceBase_2_AffectUnsafe_m4124092797_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TagCollector_t573861427 * L_0 = (TagCollector_t573861427 *)il2cpp_codegen_object_new(TagCollector_t573861427_il2cpp_TypeInfo_var);
		TagCollector__ctor_m3645951432(L_0, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___influence0;
		NullCheck((SubstanceBase_2_t1274403272 *)__this);
		Il2CppObject * L_2 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject *, Il2CppObject* >::Invoke(12 /* System.IDisposable SubstanceBase`2<System.Double,System.Double>::Affect(ILiving,ICell`1<InfT>) */, (SubstanceBase_2_t1274403272 *)__this, (Il2CppObject *)L_0, (Il2CppObject*)L_1);
		return L_2;
	}
}
// System.IDisposable SubstanceBase`2<System.Double,System.Double>::Affect(ILiving,InfT)
extern Il2CppClass* ILiving_t2639664210_il2cpp_TypeInfo_var;
extern const uint32_t SubstanceBase_2_Affect_m3920063127_MetadataUsageId;
extern "C"  Il2CppObject * SubstanceBase_2_Affect_m3920063127_gshared (SubstanceBase_2_t1274403272 * __this, Il2CppObject * ___intruder0, double ___influence1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SubstanceBase_2_Affect_m3920063127_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Intrusion_t3779021028 * V_0 = NULL;
	DisposableAffect_t4087380654 * V_1 = NULL;
	DisposableAffect_t4087380654 * V_2 = NULL;
	{
		Il2CppObject * L_0 = ___intruder0;
		double L_1 = ___influence1;
		NullCheck((SubstanceBase_2_t1274403272 *)__this);
		Intrusion_t3779021028 * L_2 = ((  Intrusion_t3779021028 * (*) (SubstanceBase_2_t1274403272 *, Il2CppObject *, double, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23)->method)((SubstanceBase_2_t1274403272 *)__this, (Il2CppObject *)L_0, (double)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23));
		V_0 = (Intrusion_t3779021028 *)L_2;
		DisposableAffect_t4087380654 * L_3 = (DisposableAffect_t4087380654 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		((  void (*) (DisposableAffect_t4087380654 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25)->method)(L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25));
		V_2 = (DisposableAffect_t4087380654 *)L_3;
		DisposableAffect_t4087380654 * L_4 = V_2;
		Intrusion_t3779021028 * L_5 = V_0;
		NullCheck(L_4);
		L_4->set_intrusion_0(L_5);
		DisposableAffect_t4087380654 * L_6 = V_2;
		NullCheck(L_6);
		L_6->set_substance_1(__this);
		DisposableAffect_t4087380654 * L_7 = V_2;
		V_1 = (DisposableAffect_t4087380654 *)L_7;
		Il2CppObject * L_8 = ___intruder0;
		DisposableAffect_t4087380654 * L_9 = V_1;
		NullCheck((Il2CppObject *)L_8);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(1 /* System.Void ILiving::DisposeWhenAsleep(System.IDisposable) */, ILiving_t2639664210_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Il2CppObject *)L_9);
		DisposableAffect_t4087380654 * L_10 = V_1;
		return L_10;
	}
}
// System.IDisposable SubstanceBase`2<System.Double,System.Double>::Affect(ILiving,ICell`1<InfT>)
extern Il2CppClass* ILiving_t2639664210_il2cpp_TypeInfo_var;
extern const uint32_t SubstanceBase_2_Affect_m3017787741_MetadataUsageId;
extern "C"  Il2CppObject * SubstanceBase_2_Affect_m3017787741_gshared (SubstanceBase_2_t1274403272 * __this, Il2CppObject * ___intruder0, Il2CppObject* ___influence1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SubstanceBase_2_Affect_m3017787741_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DisposableAffect_t4087380654 * V_0 = NULL;
	U3CAffectU3Ec__AnonStorey143_t2252021777 * V_1 = NULL;
	DisposableAffect_t4087380654 * V_2 = NULL;
	{
		U3CAffectU3Ec__AnonStorey143_t2252021777 * L_0 = (U3CAffectU3Ec__AnonStorey143_t2252021777 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26));
		((  void (*) (U3CAffectU3Ec__AnonStorey143_t2252021777 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27));
		V_1 = (U3CAffectU3Ec__AnonStorey143_t2252021777 *)L_0;
		U3CAffectU3Ec__AnonStorey143_t2252021777 * L_1 = V_1;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_1(__this);
		U3CAffectU3Ec__AnonStorey143_t2252021777 * L_2 = V_1;
		Il2CppObject * L_3 = ___intruder0;
		Il2CppObject* L_4 = ___influence1;
		NullCheck((Il2CppObject*)L_4);
		double L_5 = InterfaceFuncInvoker0< double >::Invoke(2 /* T ICell`1<System.Double>::get_value() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28), (Il2CppObject*)L_4);
		NullCheck((SubstanceBase_2_t1274403272 *)__this);
		Intrusion_t3779021028 * L_6 = ((  Intrusion_t3779021028 * (*) (SubstanceBase_2_t1274403272 *, Il2CppObject *, double, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23)->method)((SubstanceBase_2_t1274403272 *)__this, (Il2CppObject *)L_3, (double)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23));
		NullCheck(L_2);
		L_2->set_intrusion_0(L_6);
		U3CAffectU3Ec__AnonStorey143_t2252021777 * L_7 = V_1;
		NullCheck(L_7);
		Intrusion_t3779021028 * L_8 = (Intrusion_t3779021028 *)L_7->get_intrusion_0();
		Il2CppObject* L_9 = ___influence1;
		U3CAffectU3Ec__AnonStorey143_t2252021777 * L_10 = V_1;
		IntPtr_t L_11;
		L_11.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29));
		Action_1_t682969319 * L_12 = (Action_1_t682969319 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30));
		((  void (*) (Action_1_t682969319 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31)->method)(L_12, (Il2CppObject *)L_10, (IntPtr_t)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31));
		NullCheck((Il2CppObject*)L_9);
		Il2CppObject * L_13 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t682969319 *, int32_t >::Invoke(1 /* System.IDisposable ICell`1<System.Double>::ListenUpdates(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28), (Il2CppObject*)L_9, (Action_1_t682969319 *)L_12, (int32_t)1);
		NullCheck(L_8);
		L_8->set_updateConnection_3(L_13);
		DisposableAffect_t4087380654 * L_14 = (DisposableAffect_t4087380654 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		((  void (*) (DisposableAffect_t4087380654 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25)->method)(L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25));
		V_2 = (DisposableAffect_t4087380654 *)L_14;
		DisposableAffect_t4087380654 * L_15 = V_2;
		U3CAffectU3Ec__AnonStorey143_t2252021777 * L_16 = V_1;
		NullCheck(L_16);
		Intrusion_t3779021028 * L_17 = (Intrusion_t3779021028 *)L_16->get_intrusion_0();
		NullCheck(L_15);
		L_15->set_intrusion_0(L_17);
		DisposableAffect_t4087380654 * L_18 = V_2;
		NullCheck(L_18);
		L_18->set_substance_1(__this);
		DisposableAffect_t4087380654 * L_19 = V_2;
		V_0 = (DisposableAffect_t4087380654 *)L_19;
		Il2CppObject * L_20 = ___intruder0;
		DisposableAffect_t4087380654 * L_21 = V_0;
		NullCheck((Il2CppObject *)L_20);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(1 /* System.Void ILiving::DisposeWhenAsleep(System.IDisposable) */, ILiving_t2639664210_il2cpp_TypeInfo_var, (Il2CppObject *)L_20, (Il2CppObject *)L_21);
		DisposableAffect_t4087380654 * L_22 = V_0;
		return L_22;
	}
}
// System.IDisposable SubstanceBase`2<System.Double,System.Double>::Affect(ILiving,InfT,IEmptyStream)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IEmptyStream_t3684082468_il2cpp_TypeInfo_var;
extern Il2CppClass* ILiving_t2639664210_il2cpp_TypeInfo_var;
extern const uint32_t SubstanceBase_2_Affect_m2313019691_MetadataUsageId;
extern "C"  Il2CppObject * SubstanceBase_2_Affect_m2313019691_gshared (SubstanceBase_2_t1274403272 * __this, Il2CppObject * ___intruder0, double ___influence1, Il2CppObject * ___update2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SubstanceBase_2_Affect_m2313019691_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DisposableAffect_t4087380654 * V_0 = NULL;
	U3CAffectU3Ec__AnonStorey144_t2248694580 * V_1 = NULL;
	DisposableAffect_t4087380654 * V_2 = NULL;
	{
		U3CAffectU3Ec__AnonStorey144_t2248694580 * L_0 = (U3CAffectU3Ec__AnonStorey144_t2248694580 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32));
		((  void (*) (U3CAffectU3Ec__AnonStorey144_t2248694580 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 33)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 33));
		V_1 = (U3CAffectU3Ec__AnonStorey144_t2248694580 *)L_0;
		U3CAffectU3Ec__AnonStorey144_t2248694580 * L_1 = V_1;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_1(__this);
		U3CAffectU3Ec__AnonStorey144_t2248694580 * L_2 = V_1;
		Il2CppObject * L_3 = ___intruder0;
		double L_4 = ___influence1;
		NullCheck((SubstanceBase_2_t1274403272 *)__this);
		Intrusion_t3779021028 * L_5 = ((  Intrusion_t3779021028 * (*) (SubstanceBase_2_t1274403272 *, Il2CppObject *, double, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23)->method)((SubstanceBase_2_t1274403272 *)__this, (Il2CppObject *)L_3, (double)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23));
		NullCheck(L_2);
		L_2->set_intrusion_0(L_5);
		U3CAffectU3Ec__AnonStorey144_t2248694580 * L_6 = V_1;
		NullCheck(L_6);
		Intrusion_t3779021028 * L_7 = (Intrusion_t3779021028 *)L_6->get_intrusion_0();
		Il2CppObject * L_8 = ___update2;
		U3CAffectU3Ec__AnonStorey144_t2248694580 * L_9 = V_1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 34));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_8);
		Il2CppObject * L_12 = InterfaceFuncInvoker2< Il2CppObject *, Action_t437523947 *, int32_t >::Invoke(0 /* System.IDisposable IEmptyStream::Listen(System.Action,Priority) */, IEmptyStream_t3684082468_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Action_t437523947 *)L_11, (int32_t)1);
		NullCheck(L_7);
		L_7->set_updateConnection_3(L_12);
		DisposableAffect_t4087380654 * L_13 = (DisposableAffect_t4087380654 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		((  void (*) (DisposableAffect_t4087380654 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25)->method)(L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25));
		V_2 = (DisposableAffect_t4087380654 *)L_13;
		DisposableAffect_t4087380654 * L_14 = V_2;
		U3CAffectU3Ec__AnonStorey144_t2248694580 * L_15 = V_1;
		NullCheck(L_15);
		Intrusion_t3779021028 * L_16 = (Intrusion_t3779021028 *)L_15->get_intrusion_0();
		NullCheck(L_14);
		L_14->set_intrusion_0(L_16);
		DisposableAffect_t4087380654 * L_17 = V_2;
		NullCheck(L_17);
		L_17->set_substance_1(__this);
		DisposableAffect_t4087380654 * L_18 = V_2;
		V_0 = (DisposableAffect_t4087380654 *)L_18;
		Il2CppObject * L_19 = ___intruder0;
		DisposableAffect_t4087380654 * L_20 = V_0;
		NullCheck((Il2CppObject *)L_19);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(1 /* System.Void ILiving::DisposeWhenAsleep(System.IDisposable) */, ILiving_t2639664210_il2cpp_TypeInfo_var, (Il2CppObject *)L_19, (Il2CppObject *)L_20);
		DisposableAffect_t4087380654 * L_21 = V_0;
		return L_21;
	}
}
// System.Void SubstanceBase`2<System.Double,System.Double>::OnAdd(SubstanceBase`2/Intrusion<T,InfT>)
extern "C"  void SubstanceBase_2_OnAdd_m1444383432_gshared (SubstanceBase_2_t1274403272 * __this, Intrusion_t3779021028 * ___intrusion0, const MethodInfo* method)
{
	{
		NullCheck((SubstanceBase_2_t1274403272 *)__this);
		VirtActionInvoker0::Invoke(17 /* System.Void SubstanceBase`2<System.Double,System.Double>::UpdateAll() */, (SubstanceBase_2_t1274403272 *)__this);
		return;
	}
}
// System.Void SubstanceBase`2<System.Double,System.Double>::OnRemove(SubstanceBase`2/Intrusion<T,InfT>)
extern "C"  void SubstanceBase_2_OnRemove_m647466897_gshared (SubstanceBase_2_t1274403272 * __this, Intrusion_t3779021028 * ___intrusion0, const MethodInfo* method)
{
	{
		NullCheck((SubstanceBase_2_t1274403272 *)__this);
		VirtActionInvoker0::Invoke(17 /* System.Void SubstanceBase`2<System.Double,System.Double>::UpdateAll() */, (SubstanceBase_2_t1274403272 *)__this);
		return;
	}
}
// System.Void SubstanceBase`2<System.Double,System.Double>::OnUpdate(SubstanceBase`2/Intrusion<T,InfT>)
extern "C"  void SubstanceBase_2_OnUpdate_m4293165356_gshared (SubstanceBase_2_t1274403272 * __this, Intrusion_t3779021028 * ___intrusion0, const MethodInfo* method)
{
	{
		NullCheck((SubstanceBase_2_t1274403272 *)__this);
		VirtActionInvoker0::Invoke(17 /* System.Void SubstanceBase`2<System.Double,System.Double>::UpdateAll() */, (SubstanceBase_2_t1274403272 *)__this);
		return;
	}
}
// System.Void SubstanceBase`2<System.Double,System.Double>::UpdateAll()
extern "C"  void SubstanceBase_2_UpdateAll_m814067422_gshared (SubstanceBase_2_t1274403272 * __this, const MethodInfo* method)
{
	double V_0 = 0.0;
	{
		NullCheck((SubstanceBase_2_t1274403272 *)__this);
		double L_0 = VirtFuncInvoker0< double >::Invoke(18 /* T SubstanceBase`2<System.Double,System.Double>::Result() */, (SubstanceBase_2_t1274403272 *)__this);
		V_0 = (double)L_0;
		double L_1 = V_0;
		double L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14), &L_2);
		double L_4 = (double)__this->get_result_4();
		double L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14), &L_5);
		bool L_7 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0045;
		}
	}
	{
		double L_8 = V_0;
		__this->set_result_4(L_8);
		Stream_1_t3520622932 * L_9 = (Stream_1_t3520622932 *)__this->get_update_3();
		if (!L_9)
		{
			goto IL_0045;
		}
	}
	{
		Stream_1_t3520622932 * L_10 = (Stream_1_t3520622932 *)__this->get_update_3();
		double L_11 = (double)__this->get_result_4();
		NullCheck((Stream_1_t3520622932 *)L_10);
		((  void (*) (Stream_1_t3520622932 *, double, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 36)->method)((Stream_1_t3520622932 *)L_10, (double)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 36));
	}

IL_0045:
	{
		return;
	}
}
// System.Void SubstanceBase`2<System.Double,System.Double>::ClearIntrusion(SubstanceBase`2/Intrusion<T,InfT>)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t SubstanceBase_2_ClearIntrusion_m2462366554_MetadataUsageId;
extern "C"  void SubstanceBase_2_ClearIntrusion_m2462366554_gshared (SubstanceBase_2_t1274403272 * __this, Intrusion_t3779021028 * ___intr0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SubstanceBase_2_ClearIntrusion_m2462366554_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t281012701 * L_0 = (List_1_t281012701 *)__this->get_intrusions_5();
		Intrusion_t3779021028 * L_1 = ___intr0;
		NullCheck((List_1_t281012701 *)L_0);
		VirtFuncInvoker1< bool, Intrusion_t3779021028 * >::Invoke(26 /* System.Boolean System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<System.Double,System.Double>>::Remove(!0) */, (List_1_t281012701 *)L_0, (Intrusion_t3779021028 *)L_1);
		Intrusion_t3779021028 * L_2 = ___intr0;
		NullCheck(L_2);
		Il2CppObject * L_3 = (Il2CppObject *)L_2->get_awakeConnection_2();
		if (!L_3)
		{
			goto IL_0023;
		}
	}
	{
		Intrusion_t3779021028 * L_4 = ___intr0;
		NullCheck(L_4);
		Il2CppObject * L_5 = (Il2CppObject *)L_4->get_awakeConnection_2();
		NullCheck((Il2CppObject *)L_5);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
	}

IL_0023:
	{
		Intrusion_t3779021028 * L_6 = ___intr0;
		NullCheck(L_6);
		Il2CppObject * L_7 = (Il2CppObject *)L_6->get_updateConnection_3();
		if (!L_7)
		{
			goto IL_0039;
		}
	}
	{
		Intrusion_t3779021028 * L_8 = ___intr0;
		NullCheck(L_8);
		Il2CppObject * L_9 = (Il2CppObject *)L_8->get_updateConnection_3();
		NullCheck((Il2CppObject *)L_9);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_9);
	}

IL_0039:
	{
		Intrusion_t3779021028 * L_10 = ___intr0;
		NullCheck((SubstanceBase_2_t1274403272 *)__this);
		VirtActionInvoker1< Intrusion_t3779021028 * >::Invoke(15 /* System.Void SubstanceBase`2<System.Double,System.Double>::OnRemove(SubstanceBase`2/Intrusion<T,InfT>) */, (SubstanceBase_2_t1274403272 *)__this, (Intrusion_t3779021028 *)L_10);
		return;
	}
}
// System.Void SubstanceBase`2<System.Double,System.Double>::<set_baseValueReactive>m__1D7(T)
extern "C"  void SubstanceBase_2_U3Cset_baseValueReactiveU3Em__1D7_m954039939_gshared (SubstanceBase_2_t1274403272 * __this, double ___val0, const MethodInfo* method)
{
	{
		double L_0 = ___val0;
		NullCheck((SubstanceBase_2_t1274403272 *)__this);
		((  void (*) (SubstanceBase_2_t1274403272 *, double, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39)->method)((SubstanceBase_2_t1274403272 *)__this, (double)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39));
		return;
	}
}
// System.Void SubstanceBase`2<System.Object,System.Object>::.ctor()
extern "C"  void SubstanceBase_2__ctor_m1606804868_gshared (SubstanceBase_2_t540235856 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SubstanceBase`2<System.Object,System.Object>::.ctor(T)
extern "C"  void SubstanceBase_2__ctor_m2566312026_gshared (SubstanceBase_2_t540235856 * __this, Il2CppObject * ___baseVal0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___baseVal0;
		__this->set_localBase_0(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_localBase_0();
		__this->set_result_4(L_1);
		return;
	}
}
// T SubstanceBase`2<System.Object,System.Object>::get_baseValue()
extern "C"  Il2CppObject * SubstanceBase_2_get_baseValue_m1000505434_gshared (SubstanceBase_2_t540235856 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_localBase_0();
		return L_0;
	}
}
// System.Void SubstanceBase`2<System.Object,System.Object>::set_baseValue(T)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t SubstanceBase_2_set_baseValue_m1167683449_MetadataUsageId;
extern "C"  void SubstanceBase_2_set_baseValue_m1167683449_gshared (SubstanceBase_2_t540235856 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SubstanceBase_2_set_baseValue_m1167683449_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_externalBaseConnection_2();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_externalBaseConnection_2();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
		__this->set_externalBaseConnection_2((Il2CppObject *)NULL);
	}

IL_001d:
	{
		Il2CppObject * L_2 = ___value0;
		__this->set_localBase_0(L_2);
		NullCheck((SubstanceBase_2_t540235856 *)__this);
		VirtActionInvoker0::Invoke(17 /* System.Void SubstanceBase`2<System.Object,System.Object>::UpdateAll() */, (SubstanceBase_2_t540235856 *)__this);
		return;
	}
}
// System.Void SubstanceBase`2<System.Object,System.Object>::set_baseValueReactive(ICell`1<T>)
extern "C"  void SubstanceBase_2_set_baseValueReactive_m2901627540_gshared (SubstanceBase_2_t540235856 * __this, Il2CppObject* ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___value0;
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Action_1_t985559125 * L_2 = (Action_1_t985559125 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Il2CppObject *)__this, (IntPtr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_3 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t985559125 *, int32_t >::Invoke(0 /* System.IDisposable ICell`1<System.Object>::Bind(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_0, (Action_1_t985559125 *)L_2, (int32_t)1);
		__this->set_externalBaseConnection_2(L_3);
		return;
	}
}
// System.IDisposable SubstanceBase`2<System.Object,System.Object>::Bind(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * SubstanceBase_2_Bind_m487587186_gshared (SubstanceBase_2_t540235856 * __this, Action_1_t985559125 * ___action0, int32_t ___p1, const MethodInfo* method)
{
	{
		Stream_1_t3823212738 * L_0 = (Stream_1_t3823212738 *)__this->get_update_3();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		Stream_1_t3823212738 * L_1 = (Stream_1_t3823212738 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (Stream_1_t3823212738 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		__this->set_update_3(L_1);
	}

IL_0016:
	{
		Action_1_t985559125 * L_2 = ___action0;
		Il2CppObject * L_3 = (Il2CppObject *)__this->get_result_4();
		NullCheck((Action_1_t985559125 *)L_2);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((Action_1_t985559125 *)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		Stream_1_t3823212738 * L_4 = (Stream_1_t3823212738 *)__this->get_update_3();
		Action_1_t985559125 * L_5 = ___action0;
		int32_t L_6 = ___p1;
		NullCheck((Stream_1_t3823212738 *)L_4);
		Il2CppObject * L_7 = VirtFuncInvoker2< Il2CppObject *, Action_1_t985559125 *, int32_t >::Invoke(8 /* System.IDisposable Stream`1<System.Object>::Listen(System.Action`1<T>,Priority) */, (Stream_1_t3823212738 *)L_4, (Action_1_t985559125 *)L_5, (int32_t)L_6);
		return L_7;
	}
}
// System.IDisposable SubstanceBase`2<System.Object,System.Object>::OnChanged(System.Action,Priority)
extern "C"  Il2CppObject * SubstanceBase_2_OnChanged_m1056489513_gshared (SubstanceBase_2_t540235856 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method)
{
	U3COnChangedU3Ec__AnonStorey142_t2463620062 * V_0 = NULL;
	{
		U3COnChangedU3Ec__AnonStorey142_t2463620062 * L_0 = (U3COnChangedU3Ec__AnonStorey142_t2463620062 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		((  void (*) (U3COnChangedU3Ec__AnonStorey142_t2463620062 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		V_0 = (U3COnChangedU3Ec__AnonStorey142_t2463620062 *)L_0;
		U3COnChangedU3Ec__AnonStorey142_t2463620062 * L_1 = V_0;
		Action_t437523947 * L_2 = ___action0;
		NullCheck(L_1);
		L_1->set_action_0(L_2);
		U3COnChangedU3Ec__AnonStorey142_t2463620062 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		Action_1_t985559125 * L_5 = (Action_1_t985559125 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_5, (Il2CppObject *)L_3, (IntPtr_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		int32_t L_6 = ___p1;
		NullCheck((SubstanceBase_2_t540235856 *)__this);
		Il2CppObject * L_7 = VirtFuncInvoker2< Il2CppObject *, Action_1_t985559125 *, int32_t >::Invoke(7 /* System.IDisposable SubstanceBase`2<System.Object,System.Object>::ListenUpdates(System.Action`1<T>,Priority) */, (SubstanceBase_2_t540235856 *)__this, (Action_1_t985559125 *)L_5, (int32_t)L_6);
		return L_7;
	}
}
// System.Object SubstanceBase`2<System.Object,System.Object>::get_valueObject()
extern "C"  Il2CppObject * SubstanceBase_2_get_valueObject_m2572102932_gshared (SubstanceBase_2_t540235856 * __this, const MethodInfo* method)
{
	{
		NullCheck((SubstanceBase_2_t540235856 *)__this);
		Il2CppObject * L_0 = VirtFuncInvoker0< Il2CppObject * >::Invoke(8 /* T SubstanceBase`2<System.Object,System.Object>::get_value() */, (SubstanceBase_2_t540235856 *)__this);
		return L_0;
	}
}
// System.IDisposable SubstanceBase`2<System.Object,System.Object>::ListenUpdates(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * SubstanceBase_2_ListenUpdates_m2465048272_gshared (SubstanceBase_2_t540235856 * __this, Action_1_t985559125 * ___action0, int32_t ___p1, const MethodInfo* method)
{
	{
		Stream_1_t3823212738 * L_0 = (Stream_1_t3823212738 *)__this->get_update_3();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		Stream_1_t3823212738 * L_1 = (Stream_1_t3823212738 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (Stream_1_t3823212738 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		__this->set_update_3(L_1);
	}

IL_0016:
	{
		Stream_1_t3823212738 * L_2 = (Stream_1_t3823212738 *)__this->get_update_3();
		Action_1_t985559125 * L_3 = ___action0;
		int32_t L_4 = ___p1;
		NullCheck((Stream_1_t3823212738 *)L_2);
		Il2CppObject * L_5 = VirtFuncInvoker2< Il2CppObject *, Action_1_t985559125 *, int32_t >::Invoke(8 /* System.IDisposable Stream`1<System.Object>::Listen(System.Action`1<T>,Priority) */, (Stream_1_t3823212738 *)L_2, (Action_1_t985559125 *)L_3, (int32_t)L_4);
		return L_5;
	}
}
// IStream`1<T> SubstanceBase`2<System.Object,System.Object>::get_updates()
extern "C"  Il2CppObject* SubstanceBase_2_get_updates_m1292576498_gshared (SubstanceBase_2_t540235856 * __this, const MethodInfo* method)
{
	Stream_1_t3823212738 * V_0 = NULL;
	Stream_1_t3823212738 * G_B2_0 = NULL;
	Stream_1_t3823212738 * G_B1_0 = NULL;
	{
		Stream_1_t3823212738 * L_0 = (Stream_1_t3823212738 *)__this->get_update_3();
		Stream_1_t3823212738 * L_1 = (Stream_1_t3823212738 *)L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Stream_1_t3823212738 * L_2 = (Stream_1_t3823212738 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (Stream_1_t3823212738 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		Stream_1_t3823212738 * L_3 = (Stream_1_t3823212738 *)L_2;
		V_0 = (Stream_1_t3823212738 *)L_3;
		__this->set_update_3(L_3);
		Stream_1_t3823212738 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// T SubstanceBase`2<System.Object,System.Object>::get_value()
extern Il2CppClass* Transaction_t3809114814_il2cpp_TypeInfo_var;
extern const uint32_t SubstanceBase_2_get_value_m3139375339_MetadataUsageId;
extern "C"  Il2CppObject * SubstanceBase_2_get_value_m3139375339_gshared (SubstanceBase_2_t540235856 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SubstanceBase_2_get_value_m3139375339_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_0 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_calculationMode_8();
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		Transaction_AddTouchedCell_m3715775313(NULL /*static, unused*/, (Il2CppObject *)__this, /*hidden argument*/NULL);
	}

IL_0010:
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_result_4();
		return L_1;
	}
}
// SubstanceBase`2/Intrusion<T,InfT> SubstanceBase`2<System.Object,System.Object>::AffectInner(ILiving,InfT)
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral968526109;
extern const uint32_t SubstanceBase_2_AffectInner_m1664732325_MetadataUsageId;
extern "C"  Intrusion_t3044853612 * SubstanceBase_2_AffectInner_m1664732325_gshared (SubstanceBase_2_t540235856 * __this, Il2CppObject * ___intruder0, Il2CppObject * ___influence1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SubstanceBase_2_AffectInner_m1664732325_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Intrusion_t3044853612 * V_0 = NULL;
	Intrusion_t3044853612 * V_1 = NULL;
	{
		Il2CppObject * L_0 = ___intruder0;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m1836388681(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral968526109, /*hidden argument*/NULL);
		Intrusion_t3044853612 * L_1 = (Intrusion_t3044853612 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		((  void (*) (Intrusion_t3044853612 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		return L_1;
	}

IL_0016:
	{
		List_1_t3841812581 * L_2 = (List_1_t3841812581 *)__this->get_intrusions_5();
		if (L_2)
		{
			goto IL_002c;
		}
	}
	{
		List_1_t3841812581 * L_3 = (List_1_t3841812581 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		((  void (*) (List_1_t3841812581 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->method)(L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
		__this->set_intrusions_5(L_3);
	}

IL_002c:
	{
		Intrusion_t3044853612 * L_4 = (Intrusion_t3044853612 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		((  void (*) (Intrusion_t3044853612 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)(L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		V_1 = (Intrusion_t3044853612 *)L_4;
		Intrusion_t3044853612 * L_5 = V_1;
		Il2CppObject * L_6 = ___intruder0;
		NullCheck(L_5);
		L_5->set_intruder_0(L_6);
		Intrusion_t3044853612 * L_7 = V_1;
		Il2CppObject * L_8 = ___influence1;
		NullCheck(L_7);
		L_7->set_influence_1(L_8);
		Intrusion_t3044853612 * L_9 = V_1;
		V_0 = (Intrusion_t3044853612 *)L_9;
		List_1_t3841812581 * L_10 = (List_1_t3841812581 *)__this->get_intrusions_5();
		Intrusion_t3044853612 * L_11 = V_0;
		NullCheck((List_1_t3841812581 *)L_10);
		VirtActionInvoker1< Intrusion_t3044853612 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<System.Object,System.Object>>::Add(!0) */, (List_1_t3841812581 *)L_10, (Intrusion_t3044853612 *)L_11);
		Intrusion_t3044853612 * L_12 = V_0;
		NullCheck((SubstanceBase_2_t540235856 *)__this);
		VirtActionInvoker1< Intrusion_t3044853612 * >::Invoke(14 /* System.Void SubstanceBase`2<System.Object,System.Object>::OnAdd(SubstanceBase`2/Intrusion<T,InfT>) */, (SubstanceBase_2_t540235856 *)__this, (Intrusion_t3044853612 *)L_12);
		Intrusion_t3044853612 * L_13 = V_0;
		return L_13;
	}
}
// System.IDisposable SubstanceBase`2<System.Object,System.Object>::AffectUnsafe(InfT)
extern Il2CppClass* TagCollector_t573861427_il2cpp_TypeInfo_var;
extern const uint32_t SubstanceBase_2_AffectUnsafe_m1801912731_MetadataUsageId;
extern "C"  Il2CppObject * SubstanceBase_2_AffectUnsafe_m1801912731_gshared (SubstanceBase_2_t540235856 * __this, Il2CppObject * ___influence0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SubstanceBase_2_AffectUnsafe_m1801912731_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TagCollector_t573861427 * L_0 = (TagCollector_t573861427 *)il2cpp_codegen_object_new(TagCollector_t573861427_il2cpp_TypeInfo_var);
		TagCollector__ctor_m3645951432(L_0, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___influence0;
		NullCheck((SubstanceBase_2_t540235856 *)__this);
		Il2CppObject * L_2 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject *, Il2CppObject * >::Invoke(11 /* System.IDisposable SubstanceBase`2<System.Object,System.Object>::Affect(ILiving,InfT) */, (SubstanceBase_2_t540235856 *)__this, (Il2CppObject *)L_0, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.IDisposable SubstanceBase`2<System.Object,System.Object>::AffectUnsafe(ICell`1<InfT>)
extern Il2CppClass* TagCollector_t573861427_il2cpp_TypeInfo_var;
extern const uint32_t SubstanceBase_2_AffectUnsafe_m3688879833_MetadataUsageId;
extern "C"  Il2CppObject * SubstanceBase_2_AffectUnsafe_m3688879833_gshared (SubstanceBase_2_t540235856 * __this, Il2CppObject* ___influence0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SubstanceBase_2_AffectUnsafe_m3688879833_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TagCollector_t573861427 * L_0 = (TagCollector_t573861427 *)il2cpp_codegen_object_new(TagCollector_t573861427_il2cpp_TypeInfo_var);
		TagCollector__ctor_m3645951432(L_0, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___influence0;
		NullCheck((SubstanceBase_2_t540235856 *)__this);
		Il2CppObject * L_2 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject *, Il2CppObject* >::Invoke(12 /* System.IDisposable SubstanceBase`2<System.Object,System.Object>::Affect(ILiving,ICell`1<InfT>) */, (SubstanceBase_2_t540235856 *)__this, (Il2CppObject *)L_0, (Il2CppObject*)L_1);
		return L_2;
	}
}
// System.IDisposable SubstanceBase`2<System.Object,System.Object>::Affect(ILiving,InfT)
extern Il2CppClass* ILiving_t2639664210_il2cpp_TypeInfo_var;
extern const uint32_t SubstanceBase_2_Affect_m971001019_MetadataUsageId;
extern "C"  Il2CppObject * SubstanceBase_2_Affect_m971001019_gshared (SubstanceBase_2_t540235856 * __this, Il2CppObject * ___intruder0, Il2CppObject * ___influence1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SubstanceBase_2_Affect_m971001019_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Intrusion_t3044853612 * V_0 = NULL;
	DisposableAffect_t3353213238 * V_1 = NULL;
	DisposableAffect_t3353213238 * V_2 = NULL;
	{
		Il2CppObject * L_0 = ___intruder0;
		Il2CppObject * L_1 = ___influence1;
		NullCheck((SubstanceBase_2_t540235856 *)__this);
		Intrusion_t3044853612 * L_2 = ((  Intrusion_t3044853612 * (*) (SubstanceBase_2_t540235856 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23)->method)((SubstanceBase_2_t540235856 *)__this, (Il2CppObject *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23));
		V_0 = (Intrusion_t3044853612 *)L_2;
		DisposableAffect_t3353213238 * L_3 = (DisposableAffect_t3353213238 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		((  void (*) (DisposableAffect_t3353213238 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25)->method)(L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25));
		V_2 = (DisposableAffect_t3353213238 *)L_3;
		DisposableAffect_t3353213238 * L_4 = V_2;
		Intrusion_t3044853612 * L_5 = V_0;
		NullCheck(L_4);
		L_4->set_intrusion_0(L_5);
		DisposableAffect_t3353213238 * L_6 = V_2;
		NullCheck(L_6);
		L_6->set_substance_1(__this);
		DisposableAffect_t3353213238 * L_7 = V_2;
		V_1 = (DisposableAffect_t3353213238 *)L_7;
		Il2CppObject * L_8 = ___intruder0;
		DisposableAffect_t3353213238 * L_9 = V_1;
		NullCheck((Il2CppObject *)L_8);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(1 /* System.Void ILiving::DisposeWhenAsleep(System.IDisposable) */, ILiving_t2639664210_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Il2CppObject *)L_9);
		DisposableAffect_t3353213238 * L_10 = V_1;
		return L_10;
	}
}
// System.IDisposable SubstanceBase`2<System.Object,System.Object>::Affect(ILiving,ICell`1<InfT>)
extern Il2CppClass* ILiving_t2639664210_il2cpp_TypeInfo_var;
extern const uint32_t SubstanceBase_2_Affect_m1389957049_MetadataUsageId;
extern "C"  Il2CppObject * SubstanceBase_2_Affect_m1389957049_gshared (SubstanceBase_2_t540235856 * __this, Il2CppObject * ___intruder0, Il2CppObject* ___influence1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SubstanceBase_2_Affect_m1389957049_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DisposableAffect_t3353213238 * V_0 = NULL;
	U3CAffectU3Ec__AnonStorey143_t1517854361 * V_1 = NULL;
	DisposableAffect_t3353213238 * V_2 = NULL;
	{
		U3CAffectU3Ec__AnonStorey143_t1517854361 * L_0 = (U3CAffectU3Ec__AnonStorey143_t1517854361 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26));
		((  void (*) (U3CAffectU3Ec__AnonStorey143_t1517854361 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27));
		V_1 = (U3CAffectU3Ec__AnonStorey143_t1517854361 *)L_0;
		U3CAffectU3Ec__AnonStorey143_t1517854361 * L_1 = V_1;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_1(__this);
		U3CAffectU3Ec__AnonStorey143_t1517854361 * L_2 = V_1;
		Il2CppObject * L_3 = ___intruder0;
		Il2CppObject* L_4 = ___influence1;
		NullCheck((Il2CppObject*)L_4);
		Il2CppObject * L_5 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* T ICell`1<System.Object>::get_value() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28), (Il2CppObject*)L_4);
		NullCheck((SubstanceBase_2_t540235856 *)__this);
		Intrusion_t3044853612 * L_6 = ((  Intrusion_t3044853612 * (*) (SubstanceBase_2_t540235856 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23)->method)((SubstanceBase_2_t540235856 *)__this, (Il2CppObject *)L_3, (Il2CppObject *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23));
		NullCheck(L_2);
		L_2->set_intrusion_0(L_6);
		U3CAffectU3Ec__AnonStorey143_t1517854361 * L_7 = V_1;
		NullCheck(L_7);
		Intrusion_t3044853612 * L_8 = (Intrusion_t3044853612 *)L_7->get_intrusion_0();
		Il2CppObject* L_9 = ___influence1;
		U3CAffectU3Ec__AnonStorey143_t1517854361 * L_10 = V_1;
		IntPtr_t L_11;
		L_11.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29));
		Action_1_t985559125 * L_12 = (Action_1_t985559125 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30));
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31)->method)(L_12, (Il2CppObject *)L_10, (IntPtr_t)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31));
		NullCheck((Il2CppObject*)L_9);
		Il2CppObject * L_13 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t985559125 *, int32_t >::Invoke(1 /* System.IDisposable ICell`1<System.Object>::ListenUpdates(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28), (Il2CppObject*)L_9, (Action_1_t985559125 *)L_12, (int32_t)1);
		NullCheck(L_8);
		L_8->set_updateConnection_3(L_13);
		DisposableAffect_t3353213238 * L_14 = (DisposableAffect_t3353213238 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		((  void (*) (DisposableAffect_t3353213238 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25)->method)(L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25));
		V_2 = (DisposableAffect_t3353213238 *)L_14;
		DisposableAffect_t3353213238 * L_15 = V_2;
		U3CAffectU3Ec__AnonStorey143_t1517854361 * L_16 = V_1;
		NullCheck(L_16);
		Intrusion_t3044853612 * L_17 = (Intrusion_t3044853612 *)L_16->get_intrusion_0();
		NullCheck(L_15);
		L_15->set_intrusion_0(L_17);
		DisposableAffect_t3353213238 * L_18 = V_2;
		NullCheck(L_18);
		L_18->set_substance_1(__this);
		DisposableAffect_t3353213238 * L_19 = V_2;
		V_0 = (DisposableAffect_t3353213238 *)L_19;
		Il2CppObject * L_20 = ___intruder0;
		DisposableAffect_t3353213238 * L_21 = V_0;
		NullCheck((Il2CppObject *)L_20);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(1 /* System.Void ILiving::DisposeWhenAsleep(System.IDisposable) */, ILiving_t2639664210_il2cpp_TypeInfo_var, (Il2CppObject *)L_20, (Il2CppObject *)L_21);
		DisposableAffect_t3353213238 * L_22 = V_0;
		return L_22;
	}
}
// System.IDisposable SubstanceBase`2<System.Object,System.Object>::Affect(ILiving,InfT,IEmptyStream)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IEmptyStream_t3684082468_il2cpp_TypeInfo_var;
extern Il2CppClass* ILiving_t2639664210_il2cpp_TypeInfo_var;
extern const uint32_t SubstanceBase_2_Affect_m3822360967_MetadataUsageId;
extern "C"  Il2CppObject * SubstanceBase_2_Affect_m3822360967_gshared (SubstanceBase_2_t540235856 * __this, Il2CppObject * ___intruder0, Il2CppObject * ___influence1, Il2CppObject * ___update2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SubstanceBase_2_Affect_m3822360967_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DisposableAffect_t3353213238 * V_0 = NULL;
	U3CAffectU3Ec__AnonStorey144_t1514527164 * V_1 = NULL;
	DisposableAffect_t3353213238 * V_2 = NULL;
	{
		U3CAffectU3Ec__AnonStorey144_t1514527164 * L_0 = (U3CAffectU3Ec__AnonStorey144_t1514527164 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32));
		((  void (*) (U3CAffectU3Ec__AnonStorey144_t1514527164 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 33)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 33));
		V_1 = (U3CAffectU3Ec__AnonStorey144_t1514527164 *)L_0;
		U3CAffectU3Ec__AnonStorey144_t1514527164 * L_1 = V_1;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_1(__this);
		U3CAffectU3Ec__AnonStorey144_t1514527164 * L_2 = V_1;
		Il2CppObject * L_3 = ___intruder0;
		Il2CppObject * L_4 = ___influence1;
		NullCheck((SubstanceBase_2_t540235856 *)__this);
		Intrusion_t3044853612 * L_5 = ((  Intrusion_t3044853612 * (*) (SubstanceBase_2_t540235856 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23)->method)((SubstanceBase_2_t540235856 *)__this, (Il2CppObject *)L_3, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23));
		NullCheck(L_2);
		L_2->set_intrusion_0(L_5);
		U3CAffectU3Ec__AnonStorey144_t1514527164 * L_6 = V_1;
		NullCheck(L_6);
		Intrusion_t3044853612 * L_7 = (Intrusion_t3044853612 *)L_6->get_intrusion_0();
		Il2CppObject * L_8 = ___update2;
		U3CAffectU3Ec__AnonStorey144_t1514527164 * L_9 = V_1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 34));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_8);
		Il2CppObject * L_12 = InterfaceFuncInvoker2< Il2CppObject *, Action_t437523947 *, int32_t >::Invoke(0 /* System.IDisposable IEmptyStream::Listen(System.Action,Priority) */, IEmptyStream_t3684082468_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Action_t437523947 *)L_11, (int32_t)1);
		NullCheck(L_7);
		L_7->set_updateConnection_3(L_12);
		DisposableAffect_t3353213238 * L_13 = (DisposableAffect_t3353213238 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		((  void (*) (DisposableAffect_t3353213238 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25)->method)(L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25));
		V_2 = (DisposableAffect_t3353213238 *)L_13;
		DisposableAffect_t3353213238 * L_14 = V_2;
		U3CAffectU3Ec__AnonStorey144_t1514527164 * L_15 = V_1;
		NullCheck(L_15);
		Intrusion_t3044853612 * L_16 = (Intrusion_t3044853612 *)L_15->get_intrusion_0();
		NullCheck(L_14);
		L_14->set_intrusion_0(L_16);
		DisposableAffect_t3353213238 * L_17 = V_2;
		NullCheck(L_17);
		L_17->set_substance_1(__this);
		DisposableAffect_t3353213238 * L_18 = V_2;
		V_0 = (DisposableAffect_t3353213238 *)L_18;
		Il2CppObject * L_19 = ___intruder0;
		DisposableAffect_t3353213238 * L_20 = V_0;
		NullCheck((Il2CppObject *)L_19);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(1 /* System.Void ILiving::DisposeWhenAsleep(System.IDisposable) */, ILiving_t2639664210_il2cpp_TypeInfo_var, (Il2CppObject *)L_19, (Il2CppObject *)L_20);
		DisposableAffect_t3353213238 * L_21 = V_0;
		return L_21;
	}
}
// System.Void SubstanceBase`2<System.Object,System.Object>::OnAdd(SubstanceBase`2/Intrusion<T,InfT>)
extern "C"  void SubstanceBase_2_OnAdd_m680391404_gshared (SubstanceBase_2_t540235856 * __this, Intrusion_t3044853612 * ___intrusion0, const MethodInfo* method)
{
	{
		NullCheck((SubstanceBase_2_t540235856 *)__this);
		VirtActionInvoker0::Invoke(17 /* System.Void SubstanceBase`2<System.Object,System.Object>::UpdateAll() */, (SubstanceBase_2_t540235856 *)__this);
		return;
	}
}
// System.Void SubstanceBase`2<System.Object,System.Object>::OnRemove(SubstanceBase`2/Intrusion<T,InfT>)
extern "C"  void SubstanceBase_2_OnRemove_m3887629549_gshared (SubstanceBase_2_t540235856 * __this, Intrusion_t3044853612 * ___intrusion0, const MethodInfo* method)
{
	{
		NullCheck((SubstanceBase_2_t540235856 *)__this);
		VirtActionInvoker0::Invoke(17 /* System.Void SubstanceBase`2<System.Object,System.Object>::UpdateAll() */, (SubstanceBase_2_t540235856 *)__this);
		return;
	}
}
// System.Void SubstanceBase`2<System.Object,System.Object>::OnUpdate(SubstanceBase`2/Intrusion<T,InfT>)
extern "C"  void SubstanceBase_2_OnUpdate_m3238360712_gshared (SubstanceBase_2_t540235856 * __this, Intrusion_t3044853612 * ___intrusion0, const MethodInfo* method)
{
	{
		NullCheck((SubstanceBase_2_t540235856 *)__this);
		VirtActionInvoker0::Invoke(17 /* System.Void SubstanceBase`2<System.Object,System.Object>::UpdateAll() */, (SubstanceBase_2_t540235856 *)__this);
		return;
	}
}
// System.Void SubstanceBase`2<System.Object,System.Object>::UpdateAll()
extern "C"  void SubstanceBase_2_UpdateAll_m2398295098_gshared (SubstanceBase_2_t540235856 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		NullCheck((SubstanceBase_2_t540235856 *)__this);
		Il2CppObject * L_0 = VirtFuncInvoker0< Il2CppObject * >::Invoke(18 /* T SubstanceBase`2<System.Object,System.Object>::Result() */, (SubstanceBase_2_t540235856 *)__this);
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_result_4();
		bool L_3 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_1, (Il2CppObject *)L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = V_0;
		__this->set_result_4(L_4);
		Stream_1_t3823212738 * L_5 = (Stream_1_t3823212738 *)__this->get_update_3();
		if (!L_5)
		{
			goto IL_0045;
		}
	}
	{
		Stream_1_t3823212738 * L_6 = (Stream_1_t3823212738 *)__this->get_update_3();
		Il2CppObject * L_7 = (Il2CppObject *)__this->get_result_4();
		NullCheck((Stream_1_t3823212738 *)L_6);
		((  void (*) (Stream_1_t3823212738 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 36)->method)((Stream_1_t3823212738 *)L_6, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 36));
	}

IL_0045:
	{
		return;
	}
}
// System.Void SubstanceBase`2<System.Object,System.Object>::ClearIntrusion(SubstanceBase`2/Intrusion<T,InfT>)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t SubstanceBase_2_ClearIntrusion_m1431780278_MetadataUsageId;
extern "C"  void SubstanceBase_2_ClearIntrusion_m1431780278_gshared (SubstanceBase_2_t540235856 * __this, Intrusion_t3044853612 * ___intr0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SubstanceBase_2_ClearIntrusion_m1431780278_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t3841812581 * L_0 = (List_1_t3841812581 *)__this->get_intrusions_5();
		Intrusion_t3044853612 * L_1 = ___intr0;
		NullCheck((List_1_t3841812581 *)L_0);
		VirtFuncInvoker1< bool, Intrusion_t3044853612 * >::Invoke(26 /* System.Boolean System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<System.Object,System.Object>>::Remove(!0) */, (List_1_t3841812581 *)L_0, (Intrusion_t3044853612 *)L_1);
		Intrusion_t3044853612 * L_2 = ___intr0;
		NullCheck(L_2);
		Il2CppObject * L_3 = (Il2CppObject *)L_2->get_awakeConnection_2();
		if (!L_3)
		{
			goto IL_0023;
		}
	}
	{
		Intrusion_t3044853612 * L_4 = ___intr0;
		NullCheck(L_4);
		Il2CppObject * L_5 = (Il2CppObject *)L_4->get_awakeConnection_2();
		NullCheck((Il2CppObject *)L_5);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
	}

IL_0023:
	{
		Intrusion_t3044853612 * L_6 = ___intr0;
		NullCheck(L_6);
		Il2CppObject * L_7 = (Il2CppObject *)L_6->get_updateConnection_3();
		if (!L_7)
		{
			goto IL_0039;
		}
	}
	{
		Intrusion_t3044853612 * L_8 = ___intr0;
		NullCheck(L_8);
		Il2CppObject * L_9 = (Il2CppObject *)L_8->get_updateConnection_3();
		NullCheck((Il2CppObject *)L_9);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_9);
	}

IL_0039:
	{
		Intrusion_t3044853612 * L_10 = ___intr0;
		NullCheck((SubstanceBase_2_t540235856 *)__this);
		VirtActionInvoker1< Intrusion_t3044853612 * >::Invoke(15 /* System.Void SubstanceBase`2<System.Object,System.Object>::OnRemove(SubstanceBase`2/Intrusion<T,InfT>) */, (SubstanceBase_2_t540235856 *)__this, (Intrusion_t3044853612 *)L_10);
		return;
	}
}
// System.Void SubstanceBase`2<System.Object,System.Object>::<set_baseValueReactive>m__1D7(T)
extern "C"  void SubstanceBase_2_U3Cset_baseValueReactiveU3Em__1D7_m725633703_gshared (SubstanceBase_2_t540235856 * __this, Il2CppObject * ___val0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___val0;
		NullCheck((SubstanceBase_2_t540235856 *)__this);
		((  void (*) (SubstanceBase_2_t540235856 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39)->method)((SubstanceBase_2_t540235856 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39));
		return;
	}
}
// System.Void SubstanceBase`2<System.Single,System.Object>::.ctor()
extern "C"  void SubstanceBase_2__ctor_m190675853_gshared (SubstanceBase_2_t1748143363 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SubstanceBase`2<System.Single,System.Object>::.ctor(T)
extern "C"  void SubstanceBase_2__ctor_m1615985521_gshared (SubstanceBase_2_t1748143363 * __this, float ___baseVal0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		float L_0 = ___baseVal0;
		__this->set_localBase_0(L_0);
		float L_1 = (float)__this->get_localBase_0();
		__this->set_result_4(L_1);
		return;
	}
}
// T SubstanceBase`2<System.Single,System.Object>::get_baseValue()
extern "C"  float SubstanceBase_2_get_baseValue_m2947284323_gshared (SubstanceBase_2_t1748143363 * __this, const MethodInfo* method)
{
	{
		float L_0 = (float)__this->get_localBase_0();
		return L_0;
	}
}
// System.Void SubstanceBase`2<System.Single,System.Object>::set_baseValue(T)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t SubstanceBase_2_set_baseValue_m1388286864_MetadataUsageId;
extern "C"  void SubstanceBase_2_set_baseValue_m1388286864_gshared (SubstanceBase_2_t1748143363 * __this, float ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SubstanceBase_2_set_baseValue_m1388286864_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_externalBaseConnection_2();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_externalBaseConnection_2();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
		__this->set_externalBaseConnection_2((Il2CppObject *)NULL);
	}

IL_001d:
	{
		float L_2 = ___value0;
		__this->set_localBase_0(L_2);
		NullCheck((SubstanceBase_2_t1748143363 *)__this);
		VirtActionInvoker0::Invoke(17 /* System.Void SubstanceBase`2<System.Single,System.Object>::UpdateAll() */, (SubstanceBase_2_t1748143363 *)__this);
		return;
	}
}
// System.Void SubstanceBase`2<System.Single,System.Object>::set_baseValueReactive(ICell`1<T>)
extern "C"  void SubstanceBase_2_set_baseValueReactive_m4252303197_gshared (SubstanceBase_2_t1748143363 * __this, Il2CppObject* ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___value0;
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Action_1_t1106661726 * L_2 = (Action_1_t1106661726 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Action_1_t1106661726 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Il2CppObject *)__this, (IntPtr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_3 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t1106661726 *, int32_t >::Invoke(0 /* System.IDisposable ICell`1<System.Single>::Bind(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_0, (Action_1_t1106661726 *)L_2, (int32_t)1);
		__this->set_externalBaseConnection_2(L_3);
		return;
	}
}
// System.IDisposable SubstanceBase`2<System.Single,System.Object>::Bind(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * SubstanceBase_2_Bind_m1838262843_gshared (SubstanceBase_2_t1748143363 * __this, Action_1_t1106661726 * ___action0, int32_t ___p1, const MethodInfo* method)
{
	{
		Stream_1_t3944315339 * L_0 = (Stream_1_t3944315339 *)__this->get_update_3();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		Stream_1_t3944315339 * L_1 = (Stream_1_t3944315339 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (Stream_1_t3944315339 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		__this->set_update_3(L_1);
	}

IL_0016:
	{
		Action_1_t1106661726 * L_2 = ___action0;
		float L_3 = (float)__this->get_result_4();
		NullCheck((Action_1_t1106661726 *)L_2);
		((  void (*) (Action_1_t1106661726 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((Action_1_t1106661726 *)L_2, (float)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		Stream_1_t3944315339 * L_4 = (Stream_1_t3944315339 *)__this->get_update_3();
		Action_1_t1106661726 * L_5 = ___action0;
		int32_t L_6 = ___p1;
		NullCheck((Stream_1_t3944315339 *)L_4);
		Il2CppObject * L_7 = VirtFuncInvoker2< Il2CppObject *, Action_1_t1106661726 *, int32_t >::Invoke(8 /* System.IDisposable Stream`1<System.Single>::Listen(System.Action`1<T>,Priority) */, (Stream_1_t3944315339 *)L_4, (Action_1_t1106661726 *)L_5, (int32_t)L_6);
		return L_7;
	}
}
// System.IDisposable SubstanceBase`2<System.Single,System.Object>::OnChanged(System.Action,Priority)
extern "C"  Il2CppObject * SubstanceBase_2_OnChanged_m2407165170_gshared (SubstanceBase_2_t1748143363 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method)
{
	U3COnChangedU3Ec__AnonStorey142_t3671527569 * V_0 = NULL;
	{
		U3COnChangedU3Ec__AnonStorey142_t3671527569 * L_0 = (U3COnChangedU3Ec__AnonStorey142_t3671527569 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		((  void (*) (U3COnChangedU3Ec__AnonStorey142_t3671527569 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		V_0 = (U3COnChangedU3Ec__AnonStorey142_t3671527569 *)L_0;
		U3COnChangedU3Ec__AnonStorey142_t3671527569 * L_1 = V_0;
		Action_t437523947 * L_2 = ___action0;
		NullCheck(L_1);
		L_1->set_action_0(L_2);
		U3COnChangedU3Ec__AnonStorey142_t3671527569 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		Action_1_t1106661726 * L_5 = (Action_1_t1106661726 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Action_1_t1106661726 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_5, (Il2CppObject *)L_3, (IntPtr_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		int32_t L_6 = ___p1;
		NullCheck((SubstanceBase_2_t1748143363 *)__this);
		Il2CppObject * L_7 = VirtFuncInvoker2< Il2CppObject *, Action_1_t1106661726 *, int32_t >::Invoke(7 /* System.IDisposable SubstanceBase`2<System.Single,System.Object>::ListenUpdates(System.Action`1<T>,Priority) */, (SubstanceBase_2_t1748143363 *)__this, (Action_1_t1106661726 *)L_5, (int32_t)L_6);
		return L_7;
	}
}
// System.Object SubstanceBase`2<System.Single,System.Object>::get_valueObject()
extern "C"  Il2CppObject * SubstanceBase_2_get_valueObject_m820874205_gshared (SubstanceBase_2_t1748143363 * __this, const MethodInfo* method)
{
	{
		NullCheck((SubstanceBase_2_t1748143363 *)__this);
		float L_0 = VirtFuncInvoker0< float >::Invoke(8 /* T SubstanceBase`2<System.Single,System.Object>::get_value() */, (SubstanceBase_2_t1748143363 *)__this);
		float L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14), &L_1);
		return L_2;
	}
}
// System.IDisposable SubstanceBase`2<System.Single,System.Object>::ListenUpdates(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * SubstanceBase_2_ListenUpdates_m1765231655_gshared (SubstanceBase_2_t1748143363 * __this, Action_1_t1106661726 * ___action0, int32_t ___p1, const MethodInfo* method)
{
	{
		Stream_1_t3944315339 * L_0 = (Stream_1_t3944315339 *)__this->get_update_3();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		Stream_1_t3944315339 * L_1 = (Stream_1_t3944315339 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (Stream_1_t3944315339 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		__this->set_update_3(L_1);
	}

IL_0016:
	{
		Stream_1_t3944315339 * L_2 = (Stream_1_t3944315339 *)__this->get_update_3();
		Action_1_t1106661726 * L_3 = ___action0;
		int32_t L_4 = ___p1;
		NullCheck((Stream_1_t3944315339 *)L_2);
		Il2CppObject * L_5 = VirtFuncInvoker2< Il2CppObject *, Action_1_t1106661726 *, int32_t >::Invoke(8 /* System.IDisposable Stream`1<System.Single>::Listen(System.Action`1<T>,Priority) */, (Stream_1_t3944315339 *)L_2, (Action_1_t1106661726 *)L_3, (int32_t)L_4);
		return L_5;
	}
}
// IStream`1<T> SubstanceBase`2<System.Single,System.Object>::get_updates()
extern "C"  Il2CppObject* SubstanceBase_2_get_updates_m1848791611_gshared (SubstanceBase_2_t1748143363 * __this, const MethodInfo* method)
{
	Stream_1_t3944315339 * V_0 = NULL;
	Stream_1_t3944315339 * G_B2_0 = NULL;
	Stream_1_t3944315339 * G_B1_0 = NULL;
	{
		Stream_1_t3944315339 * L_0 = (Stream_1_t3944315339 *)__this->get_update_3();
		Stream_1_t3944315339 * L_1 = (Stream_1_t3944315339 *)L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Stream_1_t3944315339 * L_2 = (Stream_1_t3944315339 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (Stream_1_t3944315339 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		Stream_1_t3944315339 * L_3 = (Stream_1_t3944315339 *)L_2;
		V_0 = (Stream_1_t3944315339 *)L_3;
		__this->set_update_3(L_3);
		Stream_1_t3944315339 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// T SubstanceBase`2<System.Single,System.Object>::get_value()
extern Il2CppClass* Transaction_t3809114814_il2cpp_TypeInfo_var;
extern const uint32_t SubstanceBase_2_get_value_m91912820_MetadataUsageId;
extern "C"  float SubstanceBase_2_get_value_m91912820_gshared (SubstanceBase_2_t1748143363 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SubstanceBase_2_get_value_m91912820_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_0 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_calculationMode_8();
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		Transaction_AddTouchedCell_m3715775313(NULL /*static, unused*/, (Il2CppObject *)__this, /*hidden argument*/NULL);
	}

IL_0010:
	{
		float L_1 = (float)__this->get_result_4();
		return L_1;
	}
}
// SubstanceBase`2/Intrusion<T,InfT> SubstanceBase`2<System.Single,System.Object>::AffectInner(ILiving,InfT)
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral968526109;
extern const uint32_t SubstanceBase_2_AffectInner_m123032174_MetadataUsageId;
extern "C"  Intrusion_t4252761119 * SubstanceBase_2_AffectInner_m123032174_gshared (SubstanceBase_2_t1748143363 * __this, Il2CppObject * ___intruder0, Il2CppObject * ___influence1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SubstanceBase_2_AffectInner_m123032174_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Intrusion_t4252761119 * V_0 = NULL;
	Intrusion_t4252761119 * V_1 = NULL;
	{
		Il2CppObject * L_0 = ___intruder0;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m1836388681(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral968526109, /*hidden argument*/NULL);
		Intrusion_t4252761119 * L_1 = (Intrusion_t4252761119 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		((  void (*) (Intrusion_t4252761119 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		return L_1;
	}

IL_0016:
	{
		List_1_t754752792 * L_2 = (List_1_t754752792 *)__this->get_intrusions_5();
		if (L_2)
		{
			goto IL_002c;
		}
	}
	{
		List_1_t754752792 * L_3 = (List_1_t754752792 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		((  void (*) (List_1_t754752792 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->method)(L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
		__this->set_intrusions_5(L_3);
	}

IL_002c:
	{
		Intrusion_t4252761119 * L_4 = (Intrusion_t4252761119 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		((  void (*) (Intrusion_t4252761119 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)(L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		V_1 = (Intrusion_t4252761119 *)L_4;
		Intrusion_t4252761119 * L_5 = V_1;
		Il2CppObject * L_6 = ___intruder0;
		NullCheck(L_5);
		L_5->set_intruder_0(L_6);
		Intrusion_t4252761119 * L_7 = V_1;
		Il2CppObject * L_8 = ___influence1;
		NullCheck(L_7);
		L_7->set_influence_1(L_8);
		Intrusion_t4252761119 * L_9 = V_1;
		V_0 = (Intrusion_t4252761119 *)L_9;
		List_1_t754752792 * L_10 = (List_1_t754752792 *)__this->get_intrusions_5();
		Intrusion_t4252761119 * L_11 = V_0;
		NullCheck((List_1_t754752792 *)L_10);
		VirtActionInvoker1< Intrusion_t4252761119 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<System.Single,System.Object>>::Add(!0) */, (List_1_t754752792 *)L_10, (Intrusion_t4252761119 *)L_11);
		Intrusion_t4252761119 * L_12 = V_0;
		NullCheck((SubstanceBase_2_t1748143363 *)__this);
		VirtActionInvoker1< Intrusion_t4252761119 * >::Invoke(14 /* System.Void SubstanceBase`2<System.Single,System.Object>::OnAdd(SubstanceBase`2/Intrusion<T,InfT>) */, (SubstanceBase_2_t1748143363 *)__this, (Intrusion_t4252761119 *)L_12);
		Intrusion_t4252761119 * L_13 = V_0;
		return L_13;
	}
}
// System.IDisposable SubstanceBase`2<System.Single,System.Object>::AffectUnsafe(InfT)
extern Il2CppClass* TagCollector_t573861427_il2cpp_TypeInfo_var;
extern const uint32_t SubstanceBase_2_AffectUnsafe_m3348397042_MetadataUsageId;
extern "C"  Il2CppObject * SubstanceBase_2_AffectUnsafe_m3348397042_gshared (SubstanceBase_2_t1748143363 * __this, Il2CppObject * ___influence0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SubstanceBase_2_AffectUnsafe_m3348397042_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TagCollector_t573861427 * L_0 = (TagCollector_t573861427 *)il2cpp_codegen_object_new(TagCollector_t573861427_il2cpp_TypeInfo_var);
		TagCollector__ctor_m3645951432(L_0, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___influence0;
		NullCheck((SubstanceBase_2_t1748143363 *)__this);
		Il2CppObject * L_2 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject *, Il2CppObject * >::Invoke(11 /* System.IDisposable SubstanceBase`2<System.Single,System.Object>::Affect(ILiving,InfT) */, (SubstanceBase_2_t1748143363 *)__this, (Il2CppObject *)L_0, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.IDisposable SubstanceBase`2<System.Single,System.Object>::AffectUnsafe(ICell`1<InfT>)
extern Il2CppClass* TagCollector_t573861427_il2cpp_TypeInfo_var;
extern const uint32_t SubstanceBase_2_AffectUnsafe_m3878751842_MetadataUsageId;
extern "C"  Il2CppObject * SubstanceBase_2_AffectUnsafe_m3878751842_gshared (SubstanceBase_2_t1748143363 * __this, Il2CppObject* ___influence0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SubstanceBase_2_AffectUnsafe_m3878751842_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TagCollector_t573861427 * L_0 = (TagCollector_t573861427 *)il2cpp_codegen_object_new(TagCollector_t573861427_il2cpp_TypeInfo_var);
		TagCollector__ctor_m3645951432(L_0, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___influence0;
		NullCheck((SubstanceBase_2_t1748143363 *)__this);
		Il2CppObject * L_2 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject *, Il2CppObject* >::Invoke(12 /* System.IDisposable SubstanceBase`2<System.Single,System.Object>::Affect(ILiving,ICell`1<InfT>) */, (SubstanceBase_2_t1748143363 *)__this, (Il2CppObject *)L_0, (Il2CppObject*)L_1);
		return L_2;
	}
}
// System.IDisposable SubstanceBase`2<System.Single,System.Object>::Affect(ILiving,InfT)
extern Il2CppClass* ILiving_t2639664210_il2cpp_TypeInfo_var;
extern const uint32_t SubstanceBase_2_Affect_m1083739474_MetadataUsageId;
extern "C"  Il2CppObject * SubstanceBase_2_Affect_m1083739474_gshared (SubstanceBase_2_t1748143363 * __this, Il2CppObject * ___intruder0, Il2CppObject * ___influence1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SubstanceBase_2_Affect_m1083739474_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Intrusion_t4252761119 * V_0 = NULL;
	DisposableAffect_t266153449 * V_1 = NULL;
	DisposableAffect_t266153449 * V_2 = NULL;
	{
		Il2CppObject * L_0 = ___intruder0;
		Il2CppObject * L_1 = ___influence1;
		NullCheck((SubstanceBase_2_t1748143363 *)__this);
		Intrusion_t4252761119 * L_2 = ((  Intrusion_t4252761119 * (*) (SubstanceBase_2_t1748143363 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23)->method)((SubstanceBase_2_t1748143363 *)__this, (Il2CppObject *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23));
		V_0 = (Intrusion_t4252761119 *)L_2;
		DisposableAffect_t266153449 * L_3 = (DisposableAffect_t266153449 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		((  void (*) (DisposableAffect_t266153449 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25)->method)(L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25));
		V_2 = (DisposableAffect_t266153449 *)L_3;
		DisposableAffect_t266153449 * L_4 = V_2;
		Intrusion_t4252761119 * L_5 = V_0;
		NullCheck(L_4);
		L_4->set_intrusion_0(L_5);
		DisposableAffect_t266153449 * L_6 = V_2;
		NullCheck(L_6);
		L_6->set_substance_1(__this);
		DisposableAffect_t266153449 * L_7 = V_2;
		V_1 = (DisposableAffect_t266153449 *)L_7;
		Il2CppObject * L_8 = ___intruder0;
		DisposableAffect_t266153449 * L_9 = V_1;
		NullCheck((Il2CppObject *)L_8);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(1 /* System.Void ILiving::DisposeWhenAsleep(System.IDisposable) */, ILiving_t2639664210_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Il2CppObject *)L_9);
		DisposableAffect_t266153449 * L_10 = V_1;
		return L_10;
	}
}
// System.IDisposable SubstanceBase`2<System.Single,System.Object>::Affect(ILiving,ICell`1<InfT>)
extern Il2CppClass* ILiving_t2639664210_il2cpp_TypeInfo_var;
extern const uint32_t SubstanceBase_2_Affect_m3468331266_MetadataUsageId;
extern "C"  Il2CppObject * SubstanceBase_2_Affect_m3468331266_gshared (SubstanceBase_2_t1748143363 * __this, Il2CppObject * ___intruder0, Il2CppObject* ___influence1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SubstanceBase_2_Affect_m3468331266_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DisposableAffect_t266153449 * V_0 = NULL;
	U3CAffectU3Ec__AnonStorey143_t2725761868 * V_1 = NULL;
	DisposableAffect_t266153449 * V_2 = NULL;
	{
		U3CAffectU3Ec__AnonStorey143_t2725761868 * L_0 = (U3CAffectU3Ec__AnonStorey143_t2725761868 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26));
		((  void (*) (U3CAffectU3Ec__AnonStorey143_t2725761868 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27));
		V_1 = (U3CAffectU3Ec__AnonStorey143_t2725761868 *)L_0;
		U3CAffectU3Ec__AnonStorey143_t2725761868 * L_1 = V_1;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_1(__this);
		U3CAffectU3Ec__AnonStorey143_t2725761868 * L_2 = V_1;
		Il2CppObject * L_3 = ___intruder0;
		Il2CppObject* L_4 = ___influence1;
		NullCheck((Il2CppObject*)L_4);
		Il2CppObject * L_5 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* T ICell`1<System.Object>::get_value() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28), (Il2CppObject*)L_4);
		NullCheck((SubstanceBase_2_t1748143363 *)__this);
		Intrusion_t4252761119 * L_6 = ((  Intrusion_t4252761119 * (*) (SubstanceBase_2_t1748143363 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23)->method)((SubstanceBase_2_t1748143363 *)__this, (Il2CppObject *)L_3, (Il2CppObject *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23));
		NullCheck(L_2);
		L_2->set_intrusion_0(L_6);
		U3CAffectU3Ec__AnonStorey143_t2725761868 * L_7 = V_1;
		NullCheck(L_7);
		Intrusion_t4252761119 * L_8 = (Intrusion_t4252761119 *)L_7->get_intrusion_0();
		Il2CppObject* L_9 = ___influence1;
		U3CAffectU3Ec__AnonStorey143_t2725761868 * L_10 = V_1;
		IntPtr_t L_11;
		L_11.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29));
		Action_1_t985559125 * L_12 = (Action_1_t985559125 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30));
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31)->method)(L_12, (Il2CppObject *)L_10, (IntPtr_t)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31));
		NullCheck((Il2CppObject*)L_9);
		Il2CppObject * L_13 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t985559125 *, int32_t >::Invoke(1 /* System.IDisposable ICell`1<System.Object>::ListenUpdates(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28), (Il2CppObject*)L_9, (Action_1_t985559125 *)L_12, (int32_t)1);
		NullCheck(L_8);
		L_8->set_updateConnection_3(L_13);
		DisposableAffect_t266153449 * L_14 = (DisposableAffect_t266153449 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		((  void (*) (DisposableAffect_t266153449 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25)->method)(L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25));
		V_2 = (DisposableAffect_t266153449 *)L_14;
		DisposableAffect_t266153449 * L_15 = V_2;
		U3CAffectU3Ec__AnonStorey143_t2725761868 * L_16 = V_1;
		NullCheck(L_16);
		Intrusion_t4252761119 * L_17 = (Intrusion_t4252761119 *)L_16->get_intrusion_0();
		NullCheck(L_15);
		L_15->set_intrusion_0(L_17);
		DisposableAffect_t266153449 * L_18 = V_2;
		NullCheck(L_18);
		L_18->set_substance_1(__this);
		DisposableAffect_t266153449 * L_19 = V_2;
		V_0 = (DisposableAffect_t266153449 *)L_19;
		Il2CppObject * L_20 = ___intruder0;
		DisposableAffect_t266153449 * L_21 = V_0;
		NullCheck((Il2CppObject *)L_20);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(1 /* System.Void ILiving::DisposeWhenAsleep(System.IDisposable) */, ILiving_t2639664210_il2cpp_TypeInfo_var, (Il2CppObject *)L_20, (Il2CppObject *)L_21);
		DisposableAffect_t266153449 * L_22 = V_0;
		return L_22;
	}
}
// System.IDisposable SubstanceBase`2<System.Single,System.Object>::Affect(ILiving,InfT,IEmptyStream)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IEmptyStream_t3684082468_il2cpp_TypeInfo_var;
extern Il2CppClass* ILiving_t2639664210_il2cpp_TypeInfo_var;
extern const uint32_t SubstanceBase_2_Affect_m878069328_MetadataUsageId;
extern "C"  Il2CppObject * SubstanceBase_2_Affect_m878069328_gshared (SubstanceBase_2_t1748143363 * __this, Il2CppObject * ___intruder0, Il2CppObject * ___influence1, Il2CppObject * ___update2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SubstanceBase_2_Affect_m878069328_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DisposableAffect_t266153449 * V_0 = NULL;
	U3CAffectU3Ec__AnonStorey144_t2722434671 * V_1 = NULL;
	DisposableAffect_t266153449 * V_2 = NULL;
	{
		U3CAffectU3Ec__AnonStorey144_t2722434671 * L_0 = (U3CAffectU3Ec__AnonStorey144_t2722434671 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32));
		((  void (*) (U3CAffectU3Ec__AnonStorey144_t2722434671 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 33)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 33));
		V_1 = (U3CAffectU3Ec__AnonStorey144_t2722434671 *)L_0;
		U3CAffectU3Ec__AnonStorey144_t2722434671 * L_1 = V_1;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_1(__this);
		U3CAffectU3Ec__AnonStorey144_t2722434671 * L_2 = V_1;
		Il2CppObject * L_3 = ___intruder0;
		Il2CppObject * L_4 = ___influence1;
		NullCheck((SubstanceBase_2_t1748143363 *)__this);
		Intrusion_t4252761119 * L_5 = ((  Intrusion_t4252761119 * (*) (SubstanceBase_2_t1748143363 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23)->method)((SubstanceBase_2_t1748143363 *)__this, (Il2CppObject *)L_3, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23));
		NullCheck(L_2);
		L_2->set_intrusion_0(L_5);
		U3CAffectU3Ec__AnonStorey144_t2722434671 * L_6 = V_1;
		NullCheck(L_6);
		Intrusion_t4252761119 * L_7 = (Intrusion_t4252761119 *)L_6->get_intrusion_0();
		Il2CppObject * L_8 = ___update2;
		U3CAffectU3Ec__AnonStorey144_t2722434671 * L_9 = V_1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 34));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_8);
		Il2CppObject * L_12 = InterfaceFuncInvoker2< Il2CppObject *, Action_t437523947 *, int32_t >::Invoke(0 /* System.IDisposable IEmptyStream::Listen(System.Action,Priority) */, IEmptyStream_t3684082468_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Action_t437523947 *)L_11, (int32_t)1);
		NullCheck(L_7);
		L_7->set_updateConnection_3(L_12);
		DisposableAffect_t266153449 * L_13 = (DisposableAffect_t266153449 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		((  void (*) (DisposableAffect_t266153449 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25)->method)(L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25));
		V_2 = (DisposableAffect_t266153449 *)L_13;
		DisposableAffect_t266153449 * L_14 = V_2;
		U3CAffectU3Ec__AnonStorey144_t2722434671 * L_15 = V_1;
		NullCheck(L_15);
		Intrusion_t4252761119 * L_16 = (Intrusion_t4252761119 *)L_15->get_intrusion_0();
		NullCheck(L_14);
		L_14->set_intrusion_0(L_16);
		DisposableAffect_t266153449 * L_17 = V_2;
		NullCheck(L_17);
		L_17->set_substance_1(__this);
		DisposableAffect_t266153449 * L_18 = V_2;
		V_0 = (DisposableAffect_t266153449 *)L_18;
		Il2CppObject * L_19 = ___intruder0;
		DisposableAffect_t266153449 * L_20 = V_0;
		NullCheck((Il2CppObject *)L_19);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(1 /* System.Void ILiving::DisposeWhenAsleep(System.IDisposable) */, ILiving_t2639664210_il2cpp_TypeInfo_var, (Il2CppObject *)L_19, (Il2CppObject *)L_20);
		DisposableAffect_t266153449 * L_21 = V_0;
		return L_21;
	}
}
// System.Void SubstanceBase`2<System.Single,System.Object>::OnAdd(SubstanceBase`2/Intrusion<T,InfT>)
extern "C"  void SubstanceBase_2_OnAdd_m1323237891_gshared (SubstanceBase_2_t1748143363 * __this, Intrusion_t4252761119 * ___intrusion0, const MethodInfo* method)
{
	{
		NullCheck((SubstanceBase_2_t1748143363 *)__this);
		VirtActionInvoker0::Invoke(17 /* System.Void SubstanceBase`2<System.Single,System.Object>::UpdateAll() */, (SubstanceBase_2_t1748143363 *)__this);
		return;
	}
}
// System.Void SubstanceBase`2<System.Single,System.Object>::OnRemove(SubstanceBase`2/Intrusion<T,InfT>)
extern "C"  void SubstanceBase_2_OnRemove_m3668150902_gshared (SubstanceBase_2_t1748143363 * __this, Intrusion_t4252761119 * ___intrusion0, const MethodInfo* method)
{
	{
		NullCheck((SubstanceBase_2_t1748143363 *)__this);
		VirtActionInvoker0::Invoke(17 /* System.Void SubstanceBase`2<System.Single,System.Object>::UpdateAll() */, (SubstanceBase_2_t1748143363 *)__this);
		return;
	}
}
// System.Void SubstanceBase`2<System.Single,System.Object>::OnUpdate(SubstanceBase`2/Intrusion<T,InfT>)
extern "C"  void SubstanceBase_2_OnUpdate_m3018882065_gshared (SubstanceBase_2_t1748143363 * __this, Intrusion_t4252761119 * ___intrusion0, const MethodInfo* method)
{
	{
		NullCheck((SubstanceBase_2_t1748143363 *)__this);
		VirtActionInvoker0::Invoke(17 /* System.Void SubstanceBase`2<System.Single,System.Object>::UpdateAll() */, (SubstanceBase_2_t1748143363 *)__this);
		return;
	}
}
// System.Void SubstanceBase`2<System.Single,System.Object>::UpdateAll()
extern "C"  void SubstanceBase_2_UpdateAll_m3645799875_gshared (SubstanceBase_2_t1748143363 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		NullCheck((SubstanceBase_2_t1748143363 *)__this);
		float L_0 = VirtFuncInvoker0< float >::Invoke(18 /* T SubstanceBase`2<System.Single,System.Object>::Result() */, (SubstanceBase_2_t1748143363 *)__this);
		V_0 = (float)L_0;
		float L_1 = V_0;
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14), &L_2);
		float L_4 = (float)__this->get_result_4();
		float L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14), &L_5);
		bool L_7 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0045;
		}
	}
	{
		float L_8 = V_0;
		__this->set_result_4(L_8);
		Stream_1_t3944315339 * L_9 = (Stream_1_t3944315339 *)__this->get_update_3();
		if (!L_9)
		{
			goto IL_0045;
		}
	}
	{
		Stream_1_t3944315339 * L_10 = (Stream_1_t3944315339 *)__this->get_update_3();
		float L_11 = (float)__this->get_result_4();
		NullCheck((Stream_1_t3944315339 *)L_10);
		((  void (*) (Stream_1_t3944315339 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 36)->method)((Stream_1_t3944315339 *)L_10, (float)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 36));
	}

IL_0045:
	{
		return;
	}
}
// System.Void SubstanceBase`2<System.Single,System.Object>::ClearIntrusion(SubstanceBase`2/Intrusion<T,InfT>)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t SubstanceBase_2_ClearIntrusion_m4200609407_MetadataUsageId;
extern "C"  void SubstanceBase_2_ClearIntrusion_m4200609407_gshared (SubstanceBase_2_t1748143363 * __this, Intrusion_t4252761119 * ___intr0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SubstanceBase_2_ClearIntrusion_m4200609407_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t754752792 * L_0 = (List_1_t754752792 *)__this->get_intrusions_5();
		Intrusion_t4252761119 * L_1 = ___intr0;
		NullCheck((List_1_t754752792 *)L_0);
		VirtFuncInvoker1< bool, Intrusion_t4252761119 * >::Invoke(26 /* System.Boolean System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<System.Single,System.Object>>::Remove(!0) */, (List_1_t754752792 *)L_0, (Intrusion_t4252761119 *)L_1);
		Intrusion_t4252761119 * L_2 = ___intr0;
		NullCheck(L_2);
		Il2CppObject * L_3 = (Il2CppObject *)L_2->get_awakeConnection_2();
		if (!L_3)
		{
			goto IL_0023;
		}
	}
	{
		Intrusion_t4252761119 * L_4 = ___intr0;
		NullCheck(L_4);
		Il2CppObject * L_5 = (Il2CppObject *)L_4->get_awakeConnection_2();
		NullCheck((Il2CppObject *)L_5);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
	}

IL_0023:
	{
		Intrusion_t4252761119 * L_6 = ___intr0;
		NullCheck(L_6);
		Il2CppObject * L_7 = (Il2CppObject *)L_6->get_updateConnection_3();
		if (!L_7)
		{
			goto IL_0039;
		}
	}
	{
		Intrusion_t4252761119 * L_8 = ___intr0;
		NullCheck(L_8);
		Il2CppObject * L_9 = (Il2CppObject *)L_8->get_updateConnection_3();
		NullCheck((Il2CppObject *)L_9);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_9);
	}

IL_0039:
	{
		Intrusion_t4252761119 * L_10 = ___intr0;
		NullCheck((SubstanceBase_2_t1748143363 *)__this);
		VirtActionInvoker1< Intrusion_t4252761119 * >::Invoke(15 /* System.Void SubstanceBase`2<System.Single,System.Object>::OnRemove(SubstanceBase`2/Intrusion<T,InfT>) */, (SubstanceBase_2_t1748143363 *)__this, (Intrusion_t4252761119 *)L_10);
		return;
	}
}
// System.Void SubstanceBase`2<System.Single,System.Object>::<set_baseValueReactive>m__1D7(T)
extern "C"  void SubstanceBase_2_U3Cset_baseValueReactiveU3Em__1D7_m1323393214_gshared (SubstanceBase_2_t1748143363 * __this, float ___val0, const MethodInfo* method)
{
	{
		float L_0 = ___val0;
		NullCheck((SubstanceBase_2_t1748143363 *)__this);
		((  void (*) (SubstanceBase_2_t1748143363 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39)->method)((SubstanceBase_2_t1748143363 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39));
		return;
	}
}
// System.Void SubstanceBase`2<System.Single,System.Single>::.ctor()
extern "C"  void SubstanceBase_2__ctor_m4143605078_gshared (SubstanceBase_2_t1869245964 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SubstanceBase`2<System.Single,System.Single>::.ctor(T)
extern "C"  void SubstanceBase_2__ctor_m3897707208_gshared (SubstanceBase_2_t1869245964 * __this, float ___baseVal0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		float L_0 = ___baseVal0;
		__this->set_localBase_0(L_0);
		float L_1 = (float)__this->get_localBase_0();
		__this->set_result_4(L_1);
		return;
	}
}
// T SubstanceBase`2<System.Single,System.Single>::get_baseValue()
extern "C"  float SubstanceBase_2_get_baseValue_m4060810796_gshared (SubstanceBase_2_t1869245964 * __this, const MethodInfo* method)
{
	{
		float L_0 = (float)__this->get_localBase_0();
		return L_0;
	}
}
// System.Void SubstanceBase`2<System.Single,System.Single>::set_baseValue(T)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t SubstanceBase_2_set_baseValue_m1547869159_MetadataUsageId;
extern "C"  void SubstanceBase_2_set_baseValue_m1547869159_gshared (SubstanceBase_2_t1869245964 * __this, float ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SubstanceBase_2_set_baseValue_m1547869159_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_externalBaseConnection_2();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_externalBaseConnection_2();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
		__this->set_externalBaseConnection_2((Il2CppObject *)NULL);
	}

IL_001d:
	{
		float L_2 = ___value0;
		__this->set_localBase_0(L_2);
		NullCheck((SubstanceBase_2_t1869245964 *)__this);
		VirtActionInvoker0::Invoke(17 /* System.Void SubstanceBase`2<System.Single,System.Single>::UpdateAll() */, (SubstanceBase_2_t1869245964 *)__this);
		return;
	}
}
// System.Void SubstanceBase`2<System.Single,System.Single>::set_baseValueReactive(ICell`1<T>)
extern "C"  void SubstanceBase_2_set_baseValueReactive_m653709286_gshared (SubstanceBase_2_t1869245964 * __this, Il2CppObject* ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___value0;
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Action_1_t1106661726 * L_2 = (Action_1_t1106661726 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Action_1_t1106661726 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Il2CppObject *)__this, (IntPtr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_3 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t1106661726 *, int32_t >::Invoke(0 /* System.IDisposable ICell`1<System.Single>::Bind(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_0, (Action_1_t1106661726 *)L_2, (int32_t)1);
		__this->set_externalBaseConnection_2(L_3);
		return;
	}
}
// System.IDisposable SubstanceBase`2<System.Single,System.Single>::Bind(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * SubstanceBase_2_Bind_m2534636228_gshared (SubstanceBase_2_t1869245964 * __this, Action_1_t1106661726 * ___action0, int32_t ___p1, const MethodInfo* method)
{
	{
		Stream_1_t3944315339 * L_0 = (Stream_1_t3944315339 *)__this->get_update_3();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		Stream_1_t3944315339 * L_1 = (Stream_1_t3944315339 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (Stream_1_t3944315339 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		__this->set_update_3(L_1);
	}

IL_0016:
	{
		Action_1_t1106661726 * L_2 = ___action0;
		float L_3 = (float)__this->get_result_4();
		NullCheck((Action_1_t1106661726 *)L_2);
		((  void (*) (Action_1_t1106661726 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((Action_1_t1106661726 *)L_2, (float)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		Stream_1_t3944315339 * L_4 = (Stream_1_t3944315339 *)__this->get_update_3();
		Action_1_t1106661726 * L_5 = ___action0;
		int32_t L_6 = ___p1;
		NullCheck((Stream_1_t3944315339 *)L_4);
		Il2CppObject * L_7 = VirtFuncInvoker2< Il2CppObject *, Action_1_t1106661726 *, int32_t >::Invoke(8 /* System.IDisposable Stream`1<System.Single>::Listen(System.Action`1<T>,Priority) */, (Stream_1_t3944315339 *)L_4, (Action_1_t1106661726 *)L_5, (int32_t)L_6);
		return L_7;
	}
}
// System.IDisposable SubstanceBase`2<System.Single,System.Single>::OnChanged(System.Action,Priority)
extern "C"  Il2CppObject * SubstanceBase_2_OnChanged_m3103538555_gshared (SubstanceBase_2_t1869245964 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method)
{
	U3COnChangedU3Ec__AnonStorey142_t3792630170 * V_0 = NULL;
	{
		U3COnChangedU3Ec__AnonStorey142_t3792630170 * L_0 = (U3COnChangedU3Ec__AnonStorey142_t3792630170 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		((  void (*) (U3COnChangedU3Ec__AnonStorey142_t3792630170 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		V_0 = (U3COnChangedU3Ec__AnonStorey142_t3792630170 *)L_0;
		U3COnChangedU3Ec__AnonStorey142_t3792630170 * L_1 = V_0;
		Action_t437523947 * L_2 = ___action0;
		NullCheck(L_1);
		L_1->set_action_0(L_2);
		U3COnChangedU3Ec__AnonStorey142_t3792630170 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		Action_1_t1106661726 * L_5 = (Action_1_t1106661726 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Action_1_t1106661726 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_5, (Il2CppObject *)L_3, (IntPtr_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		int32_t L_6 = ___p1;
		NullCheck((SubstanceBase_2_t1869245964 *)__this);
		Il2CppObject * L_7 = VirtFuncInvoker2< Il2CppObject *, Action_1_t1106661726 *, int32_t >::Invoke(7 /* System.IDisposable SubstanceBase`2<System.Single,System.Single>::ListenUpdates(System.Action`1<T>,Priority) */, (SubstanceBase_2_t1869245964 *)__this, (Action_1_t1106661726 *)L_5, (int32_t)L_6);
		return L_7;
	}
}
// System.Object SubstanceBase`2<System.Single,System.Single>::get_valueObject()
extern "C"  Il2CppObject * SubstanceBase_2_get_valueObject_m1472958054_gshared (SubstanceBase_2_t1869245964 * __this, const MethodInfo* method)
{
	{
		NullCheck((SubstanceBase_2_t1869245964 *)__this);
		float L_0 = VirtFuncInvoker0< float >::Invoke(8 /* T SubstanceBase`2<System.Single,System.Single>::get_value() */, (SubstanceBase_2_t1869245964 *)__this);
		float L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14), &L_1);
		return L_2;
	}
}
// System.IDisposable SubstanceBase`2<System.Single,System.Single>::ListenUpdates(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * SubstanceBase_2_ListenUpdates_m3356296638_gshared (SubstanceBase_2_t1869245964 * __this, Action_1_t1106661726 * ___action0, int32_t ___p1, const MethodInfo* method)
{
	{
		Stream_1_t3944315339 * L_0 = (Stream_1_t3944315339 *)__this->get_update_3();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		Stream_1_t3944315339 * L_1 = (Stream_1_t3944315339 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (Stream_1_t3944315339 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		__this->set_update_3(L_1);
	}

IL_0016:
	{
		Stream_1_t3944315339 * L_2 = (Stream_1_t3944315339 *)__this->get_update_3();
		Action_1_t1106661726 * L_3 = ___action0;
		int32_t L_4 = ___p1;
		NullCheck((Stream_1_t3944315339 *)L_2);
		Il2CppObject * L_5 = VirtFuncInvoker2< Il2CppObject *, Action_1_t1106661726 *, int32_t >::Invoke(8 /* System.IDisposable Stream`1<System.Single>::Listen(System.Action`1<T>,Priority) */, (Stream_1_t3944315339 *)L_2, (Action_1_t1106661726 *)L_3, (int32_t)L_4);
		return L_5;
	}
}
// IStream`1<T> SubstanceBase`2<System.Single,System.Single>::get_updates()
extern "C"  Il2CppObject* SubstanceBase_2_get_updates_m1233191236_gshared (SubstanceBase_2_t1869245964 * __this, const MethodInfo* method)
{
	Stream_1_t3944315339 * V_0 = NULL;
	Stream_1_t3944315339 * G_B2_0 = NULL;
	Stream_1_t3944315339 * G_B1_0 = NULL;
	{
		Stream_1_t3944315339 * L_0 = (Stream_1_t3944315339 *)__this->get_update_3();
		Stream_1_t3944315339 * L_1 = (Stream_1_t3944315339 *)L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Stream_1_t3944315339 * L_2 = (Stream_1_t3944315339 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (Stream_1_t3944315339 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		Stream_1_t3944315339 * L_3 = (Stream_1_t3944315339 *)L_2;
		V_0 = (Stream_1_t3944315339 *)L_3;
		__this->set_update_3(L_3);
		Stream_1_t3944315339 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
// T SubstanceBase`2<System.Single,System.Single>::get_value()
extern Il2CppClass* Transaction_t3809114814_il2cpp_TypeInfo_var;
extern const uint32_t SubstanceBase_2_get_value_m2710263741_MetadataUsageId;
extern "C"  float SubstanceBase_2_get_value_m2710263741_gshared (SubstanceBase_2_t1869245964 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SubstanceBase_2_get_value_m2710263741_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		bool L_0 = ((Transaction_t3809114814_StaticFields*)Transaction_t3809114814_il2cpp_TypeInfo_var->static_fields)->get_calculationMode_8();
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Transaction_t3809114814_il2cpp_TypeInfo_var);
		Transaction_AddTouchedCell_m3715775313(NULL /*static, unused*/, (Il2CppObject *)__this, /*hidden argument*/NULL);
	}

IL_0010:
	{
		float L_1 = (float)__this->get_result_4();
		return L_1;
	}
}
// SubstanceBase`2/Intrusion<T,InfT> SubstanceBase`2<System.Single,System.Single>::AffectInner(ILiving,InfT)
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral968526109;
extern const uint32_t SubstanceBase_2_AffectInner_m1370536951_MetadataUsageId;
extern "C"  Intrusion_t78896424 * SubstanceBase_2_AffectInner_m1370536951_gshared (SubstanceBase_2_t1869245964 * __this, Il2CppObject * ___intruder0, float ___influence1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SubstanceBase_2_AffectInner_m1370536951_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Intrusion_t78896424 * V_0 = NULL;
	Intrusion_t78896424 * V_1 = NULL;
	{
		Il2CppObject * L_0 = ___intruder0;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m1836388681(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral968526109, /*hidden argument*/NULL);
		Intrusion_t78896424 * L_1 = (Intrusion_t78896424 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		((  void (*) (Intrusion_t78896424 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		return L_1;
	}

IL_0016:
	{
		List_1_t875855393 * L_2 = (List_1_t875855393 *)__this->get_intrusions_5();
		if (L_2)
		{
			goto IL_002c;
		}
	}
	{
		List_1_t875855393 * L_3 = (List_1_t875855393 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		((  void (*) (List_1_t875855393 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->method)(L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
		__this->set_intrusions_5(L_3);
	}

IL_002c:
	{
		Intrusion_t78896424 * L_4 = (Intrusion_t78896424 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		((  void (*) (Intrusion_t78896424 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)(L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		V_1 = (Intrusion_t78896424 *)L_4;
		Intrusion_t78896424 * L_5 = V_1;
		Il2CppObject * L_6 = ___intruder0;
		NullCheck(L_5);
		L_5->set_intruder_0(L_6);
		Intrusion_t78896424 * L_7 = V_1;
		float L_8 = ___influence1;
		NullCheck(L_7);
		L_7->set_influence_1(L_8);
		Intrusion_t78896424 * L_9 = V_1;
		V_0 = (Intrusion_t78896424 *)L_9;
		List_1_t875855393 * L_10 = (List_1_t875855393 *)__this->get_intrusions_5();
		Intrusion_t78896424 * L_11 = V_0;
		NullCheck((List_1_t875855393 *)L_10);
		VirtActionInvoker1< Intrusion_t78896424 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<System.Single,System.Single>>::Add(!0) */, (List_1_t875855393 *)L_10, (Intrusion_t78896424 *)L_11);
		Intrusion_t78896424 * L_12 = V_0;
		NullCheck((SubstanceBase_2_t1869245964 *)__this);
		VirtActionInvoker1< Intrusion_t78896424 * >::Invoke(14 /* System.Void SubstanceBase`2<System.Single,System.Single>::OnAdd(SubstanceBase`2/Intrusion<T,InfT>) */, (SubstanceBase_2_t1869245964 *)__this, (Intrusion_t78896424 *)L_12);
		Intrusion_t78896424 * L_13 = V_0;
		return L_13;
	}
}
// System.IDisposable SubstanceBase`2<System.Single,System.Single>::AffectUnsafe(InfT)
extern Il2CppClass* TagCollector_t573861427_il2cpp_TypeInfo_var;
extern const uint32_t SubstanceBase_2_AffectUnsafe_m2088159881_MetadataUsageId;
extern "C"  Il2CppObject * SubstanceBase_2_AffectUnsafe_m2088159881_gshared (SubstanceBase_2_t1869245964 * __this, float ___influence0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SubstanceBase_2_AffectUnsafe_m2088159881_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TagCollector_t573861427 * L_0 = (TagCollector_t573861427 *)il2cpp_codegen_object_new(TagCollector_t573861427_il2cpp_TypeInfo_var);
		TagCollector__ctor_m3645951432(L_0, /*hidden argument*/NULL);
		float L_1 = ___influence0;
		NullCheck((SubstanceBase_2_t1869245964 *)__this);
		Il2CppObject * L_2 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject *, float >::Invoke(11 /* System.IDisposable SubstanceBase`2<System.Single,System.Single>::Affect(ILiving,InfT) */, (SubstanceBase_2_t1869245964 *)__this, (Il2CppObject *)L_0, (float)L_1);
		return L_2;
	}
}
// System.IDisposable SubstanceBase`2<System.Single,System.Single>::AffectUnsafe(ICell`1<InfT>)
extern Il2CppClass* TagCollector_t573861427_il2cpp_TypeInfo_var;
extern const uint32_t SubstanceBase_2_AffectUnsafe_m139999659_MetadataUsageId;
extern "C"  Il2CppObject * SubstanceBase_2_AffectUnsafe_m139999659_gshared (SubstanceBase_2_t1869245964 * __this, Il2CppObject* ___influence0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SubstanceBase_2_AffectUnsafe_m139999659_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TagCollector_t573861427 * L_0 = (TagCollector_t573861427 *)il2cpp_codegen_object_new(TagCollector_t573861427_il2cpp_TypeInfo_var);
		TagCollector__ctor_m3645951432(L_0, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___influence0;
		NullCheck((SubstanceBase_2_t1869245964 *)__this);
		Il2CppObject * L_2 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject *, Il2CppObject* >::Invoke(12 /* System.IDisposable SubstanceBase`2<System.Single,System.Single>::Affect(ILiving,ICell`1<InfT>) */, (SubstanceBase_2_t1869245964 *)__this, (Il2CppObject *)L_0, (Il2CppObject*)L_1);
		return L_2;
	}
}
// System.IDisposable SubstanceBase`2<System.Single,System.Single>::Affect(ILiving,InfT)
extern Il2CppClass* ILiving_t2639664210_il2cpp_TypeInfo_var;
extern const uint32_t SubstanceBase_2_Affect_m1176605225_MetadataUsageId;
extern "C"  Il2CppObject * SubstanceBase_2_Affect_m1176605225_gshared (SubstanceBase_2_t1869245964 * __this, Il2CppObject * ___intruder0, float ___influence1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SubstanceBase_2_Affect_m1176605225_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Intrusion_t78896424 * V_0 = NULL;
	DisposableAffect_t387256050 * V_1 = NULL;
	DisposableAffect_t387256050 * V_2 = NULL;
	{
		Il2CppObject * L_0 = ___intruder0;
		float L_1 = ___influence1;
		NullCheck((SubstanceBase_2_t1869245964 *)__this);
		Intrusion_t78896424 * L_2 = ((  Intrusion_t78896424 * (*) (SubstanceBase_2_t1869245964 *, Il2CppObject *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23)->method)((SubstanceBase_2_t1869245964 *)__this, (Il2CppObject *)L_0, (float)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23));
		V_0 = (Intrusion_t78896424 *)L_2;
		DisposableAffect_t387256050 * L_3 = (DisposableAffect_t387256050 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		((  void (*) (DisposableAffect_t387256050 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25)->method)(L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25));
		V_2 = (DisposableAffect_t387256050 *)L_3;
		DisposableAffect_t387256050 * L_4 = V_2;
		Intrusion_t78896424 * L_5 = V_0;
		NullCheck(L_4);
		L_4->set_intrusion_0(L_5);
		DisposableAffect_t387256050 * L_6 = V_2;
		NullCheck(L_6);
		L_6->set_substance_1(__this);
		DisposableAffect_t387256050 * L_7 = V_2;
		V_1 = (DisposableAffect_t387256050 *)L_7;
		Il2CppObject * L_8 = ___intruder0;
		DisposableAffect_t387256050 * L_9 = V_1;
		NullCheck((Il2CppObject *)L_8);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(1 /* System.Void ILiving::DisposeWhenAsleep(System.IDisposable) */, ILiving_t2639664210_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Il2CppObject *)L_9);
		DisposableAffect_t387256050 * L_10 = V_1;
		return L_10;
	}
}
// System.IDisposable SubstanceBase`2<System.Single,System.Single>::Affect(ILiving,ICell`1<InfT>)
extern Il2CppClass* ILiving_t2639664210_il2cpp_TypeInfo_var;
extern const uint32_t SubstanceBase_2_Affect_m1120142859_MetadataUsageId;
extern "C"  Il2CppObject * SubstanceBase_2_Affect_m1120142859_gshared (SubstanceBase_2_t1869245964 * __this, Il2CppObject * ___intruder0, Il2CppObject* ___influence1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SubstanceBase_2_Affect_m1120142859_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DisposableAffect_t387256050 * V_0 = NULL;
	U3CAffectU3Ec__AnonStorey143_t2846864469 * V_1 = NULL;
	DisposableAffect_t387256050 * V_2 = NULL;
	{
		U3CAffectU3Ec__AnonStorey143_t2846864469 * L_0 = (U3CAffectU3Ec__AnonStorey143_t2846864469 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26));
		((  void (*) (U3CAffectU3Ec__AnonStorey143_t2846864469 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27));
		V_1 = (U3CAffectU3Ec__AnonStorey143_t2846864469 *)L_0;
		U3CAffectU3Ec__AnonStorey143_t2846864469 * L_1 = V_1;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_1(__this);
		U3CAffectU3Ec__AnonStorey143_t2846864469 * L_2 = V_1;
		Il2CppObject * L_3 = ___intruder0;
		Il2CppObject* L_4 = ___influence1;
		NullCheck((Il2CppObject*)L_4);
		float L_5 = InterfaceFuncInvoker0< float >::Invoke(2 /* T ICell`1<System.Single>::get_value() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28), (Il2CppObject*)L_4);
		NullCheck((SubstanceBase_2_t1869245964 *)__this);
		Intrusion_t78896424 * L_6 = ((  Intrusion_t78896424 * (*) (SubstanceBase_2_t1869245964 *, Il2CppObject *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23)->method)((SubstanceBase_2_t1869245964 *)__this, (Il2CppObject *)L_3, (float)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23));
		NullCheck(L_2);
		L_2->set_intrusion_0(L_6);
		U3CAffectU3Ec__AnonStorey143_t2846864469 * L_7 = V_1;
		NullCheck(L_7);
		Intrusion_t78896424 * L_8 = (Intrusion_t78896424 *)L_7->get_intrusion_0();
		Il2CppObject* L_9 = ___influence1;
		U3CAffectU3Ec__AnonStorey143_t2846864469 * L_10 = V_1;
		IntPtr_t L_11;
		L_11.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29));
		Action_1_t1106661726 * L_12 = (Action_1_t1106661726 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30));
		((  void (*) (Action_1_t1106661726 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31)->method)(L_12, (Il2CppObject *)L_10, (IntPtr_t)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31));
		NullCheck((Il2CppObject*)L_9);
		Il2CppObject * L_13 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t1106661726 *, int32_t >::Invoke(1 /* System.IDisposable ICell`1<System.Single>::ListenUpdates(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28), (Il2CppObject*)L_9, (Action_1_t1106661726 *)L_12, (int32_t)1);
		NullCheck(L_8);
		L_8->set_updateConnection_3(L_13);
		DisposableAffect_t387256050 * L_14 = (DisposableAffect_t387256050 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		((  void (*) (DisposableAffect_t387256050 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25)->method)(L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25));
		V_2 = (DisposableAffect_t387256050 *)L_14;
		DisposableAffect_t387256050 * L_15 = V_2;
		U3CAffectU3Ec__AnonStorey143_t2846864469 * L_16 = V_1;
		NullCheck(L_16);
		Intrusion_t78896424 * L_17 = (Intrusion_t78896424 *)L_16->get_intrusion_0();
		NullCheck(L_15);
		L_15->set_intrusion_0(L_17);
		DisposableAffect_t387256050 * L_18 = V_2;
		NullCheck(L_18);
		L_18->set_substance_1(__this);
		DisposableAffect_t387256050 * L_19 = V_2;
		V_0 = (DisposableAffect_t387256050 *)L_19;
		Il2CppObject * L_20 = ___intruder0;
		DisposableAffect_t387256050 * L_21 = V_0;
		NullCheck((Il2CppObject *)L_20);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(1 /* System.Void ILiving::DisposeWhenAsleep(System.IDisposable) */, ILiving_t2639664210_il2cpp_TypeInfo_var, (Il2CppObject *)L_20, (Il2CppObject *)L_21);
		DisposableAffect_t387256050 * L_22 = V_0;
		return L_22;
	}
}
// System.IDisposable SubstanceBase`2<System.Single,System.Single>::Affect(ILiving,InfT,IEmptyStream)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IEmptyStream_t3684082468_il2cpp_TypeInfo_var;
extern Il2CppClass* ILiving_t2639664210_il2cpp_TypeInfo_var;
extern const uint32_t SubstanceBase_2_Affect_m1574442713_MetadataUsageId;
extern "C"  Il2CppObject * SubstanceBase_2_Affect_m1574442713_gshared (SubstanceBase_2_t1869245964 * __this, Il2CppObject * ___intruder0, float ___influence1, Il2CppObject * ___update2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SubstanceBase_2_Affect_m1574442713_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DisposableAffect_t387256050 * V_0 = NULL;
	U3CAffectU3Ec__AnonStorey144_t2843537272 * V_1 = NULL;
	DisposableAffect_t387256050 * V_2 = NULL;
	{
		U3CAffectU3Ec__AnonStorey144_t2843537272 * L_0 = (U3CAffectU3Ec__AnonStorey144_t2843537272 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32));
		((  void (*) (U3CAffectU3Ec__AnonStorey144_t2843537272 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 33)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 33));
		V_1 = (U3CAffectU3Ec__AnonStorey144_t2843537272 *)L_0;
		U3CAffectU3Ec__AnonStorey144_t2843537272 * L_1 = V_1;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_1(__this);
		U3CAffectU3Ec__AnonStorey144_t2843537272 * L_2 = V_1;
		Il2CppObject * L_3 = ___intruder0;
		float L_4 = ___influence1;
		NullCheck((SubstanceBase_2_t1869245964 *)__this);
		Intrusion_t78896424 * L_5 = ((  Intrusion_t78896424 * (*) (SubstanceBase_2_t1869245964 *, Il2CppObject *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23)->method)((SubstanceBase_2_t1869245964 *)__this, (Il2CppObject *)L_3, (float)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23));
		NullCheck(L_2);
		L_2->set_intrusion_0(L_5);
		U3CAffectU3Ec__AnonStorey144_t2843537272 * L_6 = V_1;
		NullCheck(L_6);
		Intrusion_t78896424 * L_7 = (Intrusion_t78896424 *)L_6->get_intrusion_0();
		Il2CppObject * L_8 = ___update2;
		U3CAffectU3Ec__AnonStorey144_t2843537272 * L_9 = V_1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 34));
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_8);
		Il2CppObject * L_12 = InterfaceFuncInvoker2< Il2CppObject *, Action_t437523947 *, int32_t >::Invoke(0 /* System.IDisposable IEmptyStream::Listen(System.Action,Priority) */, IEmptyStream_t3684082468_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (Action_t437523947 *)L_11, (int32_t)1);
		NullCheck(L_7);
		L_7->set_updateConnection_3(L_12);
		DisposableAffect_t387256050 * L_13 = (DisposableAffect_t387256050 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		((  void (*) (DisposableAffect_t387256050 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25)->method)(L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25));
		V_2 = (DisposableAffect_t387256050 *)L_13;
		DisposableAffect_t387256050 * L_14 = V_2;
		U3CAffectU3Ec__AnonStorey144_t2843537272 * L_15 = V_1;
		NullCheck(L_15);
		Intrusion_t78896424 * L_16 = (Intrusion_t78896424 *)L_15->get_intrusion_0();
		NullCheck(L_14);
		L_14->set_intrusion_0(L_16);
		DisposableAffect_t387256050 * L_17 = V_2;
		NullCheck(L_17);
		L_17->set_substance_1(__this);
		DisposableAffect_t387256050 * L_18 = V_2;
		V_0 = (DisposableAffect_t387256050 *)L_18;
		Il2CppObject * L_19 = ___intruder0;
		DisposableAffect_t387256050 * L_20 = V_0;
		NullCheck((Il2CppObject *)L_19);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(1 /* System.Void ILiving::DisposeWhenAsleep(System.IDisposable) */, ILiving_t2639664210_il2cpp_TypeInfo_var, (Il2CppObject *)L_19, (Il2CppObject *)L_20);
		DisposableAffect_t387256050 * L_21 = V_0;
		return L_21;
	}
}
// System.Void SubstanceBase`2<System.Single,System.Single>::OnAdd(SubstanceBase`2/Intrusion<T,InfT>)
extern "C"  void SubstanceBase_2_OnAdd_m775173466_gshared (SubstanceBase_2_t1869245964 * __this, Intrusion_t78896424 * ___intrusion0, const MethodInfo* method)
{
	{
		NullCheck((SubstanceBase_2_t1869245964 *)__this);
		VirtActionInvoker0::Invoke(17 /* System.Void SubstanceBase`2<System.Single,System.Single>::UpdateAll() */, (SubstanceBase_2_t1869245964 *)__this);
		return;
	}
}
// System.Void SubstanceBase`2<System.Single,System.Single>::OnRemove(SubstanceBase`2/Intrusion<T,InfT>)
extern "C"  void SubstanceBase_2_OnRemove_m1451557823_gshared (SubstanceBase_2_t1869245964 * __this, Intrusion_t78896424 * ___intrusion0, const MethodInfo* method)
{
	{
		NullCheck((SubstanceBase_2_t1869245964 *)__this);
		VirtActionInvoker0::Invoke(17 /* System.Void SubstanceBase`2<System.Single,System.Single>::UpdateAll() */, (SubstanceBase_2_t1869245964 *)__this);
		return;
	}
}
// System.Void SubstanceBase`2<System.Single,System.Single>::OnUpdate(SubstanceBase`2/Intrusion<T,InfT>)
extern "C"  void SubstanceBase_2_OnUpdate_m802288986_gshared (SubstanceBase_2_t1869245964 * __this, Intrusion_t78896424 * ___intrusion0, const MethodInfo* method)
{
	{
		NullCheck((SubstanceBase_2_t1869245964 *)__this);
		VirtActionInvoker0::Invoke(17 /* System.Void SubstanceBase`2<System.Single,System.Single>::UpdateAll() */, (SubstanceBase_2_t1869245964 *)__this);
		return;
	}
}
// System.Void SubstanceBase`2<System.Single,System.Single>::UpdateAll()
extern "C"  void SubstanceBase_2_UpdateAll_m1969183500_gshared (SubstanceBase_2_t1869245964 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		NullCheck((SubstanceBase_2_t1869245964 *)__this);
		float L_0 = VirtFuncInvoker0< float >::Invoke(18 /* T SubstanceBase`2<System.Single,System.Single>::Result() */, (SubstanceBase_2_t1869245964 *)__this);
		V_0 = (float)L_0;
		float L_1 = V_0;
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14), &L_2);
		float L_4 = (float)__this->get_result_4();
		float L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14), &L_5);
		bool L_7 = Object_Equals_m3175838359(NULL /*static, unused*/, (Il2CppObject *)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0045;
		}
	}
	{
		float L_8 = V_0;
		__this->set_result_4(L_8);
		Stream_1_t3944315339 * L_9 = (Stream_1_t3944315339 *)__this->get_update_3();
		if (!L_9)
		{
			goto IL_0045;
		}
	}
	{
		Stream_1_t3944315339 * L_10 = (Stream_1_t3944315339 *)__this->get_update_3();
		float L_11 = (float)__this->get_result_4();
		NullCheck((Stream_1_t3944315339 *)L_10);
		((  void (*) (Stream_1_t3944315339 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 36)->method)((Stream_1_t3944315339 *)L_10, (float)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 36));
	}

IL_0045:
	{
		return;
	}
}
// System.Void SubstanceBase`2<System.Single,System.Single>::ClearIntrusion(SubstanceBase`2/Intrusion<T,InfT>)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t SubstanceBase_2_ClearIntrusion_m824825096_MetadataUsageId;
extern "C"  void SubstanceBase_2_ClearIntrusion_m824825096_gshared (SubstanceBase_2_t1869245964 * __this, Intrusion_t78896424 * ___intr0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SubstanceBase_2_ClearIntrusion_m824825096_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t875855393 * L_0 = (List_1_t875855393 *)__this->get_intrusions_5();
		Intrusion_t78896424 * L_1 = ___intr0;
		NullCheck((List_1_t875855393 *)L_0);
		VirtFuncInvoker1< bool, Intrusion_t78896424 * >::Invoke(26 /* System.Boolean System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<System.Single,System.Single>>::Remove(!0) */, (List_1_t875855393 *)L_0, (Intrusion_t78896424 *)L_1);
		Intrusion_t78896424 * L_2 = ___intr0;
		NullCheck(L_2);
		Il2CppObject * L_3 = (Il2CppObject *)L_2->get_awakeConnection_2();
		if (!L_3)
		{
			goto IL_0023;
		}
	}
	{
		Intrusion_t78896424 * L_4 = ___intr0;
		NullCheck(L_4);
		Il2CppObject * L_5 = (Il2CppObject *)L_4->get_awakeConnection_2();
		NullCheck((Il2CppObject *)L_5);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
	}

IL_0023:
	{
		Intrusion_t78896424 * L_6 = ___intr0;
		NullCheck(L_6);
		Il2CppObject * L_7 = (Il2CppObject *)L_6->get_updateConnection_3();
		if (!L_7)
		{
			goto IL_0039;
		}
	}
	{
		Intrusion_t78896424 * L_8 = ___intr0;
		NullCheck(L_8);
		Il2CppObject * L_9 = (Il2CppObject *)L_8->get_updateConnection_3();
		NullCheck((Il2CppObject *)L_9);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_9);
	}

IL_0039:
	{
		Intrusion_t78896424 * L_10 = ___intr0;
		NullCheck((SubstanceBase_2_t1869245964 *)__this);
		VirtActionInvoker1< Intrusion_t78896424 * >::Invoke(15 /* System.Void SubstanceBase`2<System.Single,System.Single>::OnRemove(SubstanceBase`2/Intrusion<T,InfT>) */, (SubstanceBase_2_t1869245964 *)__this, (Intrusion_t78896424 *)L_10);
		return;
	}
}
// System.Void SubstanceBase`2<System.Single,System.Single>::<set_baseValueReactive>m__1D7(T)
extern "C"  void SubstanceBase_2_U3Cset_baseValueReactiveU3Em__1D7_m2869877525_gshared (SubstanceBase_2_t1869245964 * __this, float ___val0, const MethodInfo* method)
{
	{
		float L_0 = ___val0;
		NullCheck((SubstanceBase_2_t1869245964 *)__this);
		((  void (*) (SubstanceBase_2_t1869245964 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39)->method)((SubstanceBase_2_t1869245964 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39));
		return;
	}
}
// System.Void SubstanceCollection`1<System.Object>::.ctor()
extern "C"  void SubstanceCollection_1__ctor_m2479562270_gshared (SubstanceCollection_1_t1980381568 * __this, const MethodInfo* method)
{
	{
		ImmutableList_1_t1897310919 * L_0 = (ImmutableList_1_t1897310919 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (ImmutableList_1_t1897310919 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		NullCheck((SubstanceBase_2_t3226437585 *)__this);
		((  void (*) (SubstanceBase_2_t3226437585 *, ImmutableList_1_t1897310919 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((SubstanceBase_2_t3226437585 *)__this, (ImmutableList_1_t1897310919 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
// UniRx.InternalUtil.ImmutableList`1<T> SubstanceCollection`1<System.Object>::Result()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t SubstanceCollection_1_Result_m1905662954_MetadataUsageId;
extern "C"  ImmutableList_1_t1897310919 * SubstanceCollection_1_Result_m1905662954_gshared (SubstanceCollection_1_t1980381568 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SubstanceCollection_1_Result_m1905662954_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t11523773* V_0 = NULL;
	int32_t V_1 = 0;
	Intrusion_t1436088045 * V_2 = NULL;
	Enumerator_t318830006  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t2233047014 * L_0 = (List_1_t2233047014 *)((SubstanceBase_2_t3226437585 *)__this)->get_intrusions_5();
		NullCheck((List_1_t2233047014 *)L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>>::get_Count() */, (List_1_t2233047014 *)L_0);
		V_0 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (uint32_t)L_1));
		V_1 = (int32_t)0;
		List_1_t2233047014 * L_2 = (List_1_t2233047014 *)((SubstanceBase_2_t3226437585 *)__this)->get_intrusions_5();
		NullCheck((List_1_t2233047014 *)L_2);
		Enumerator_t318830006  L_3 = ((  Enumerator_t318830006  (*) (List_1_t2233047014 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((List_1_t2233047014 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		V_3 = (Enumerator_t318830006 )L_3;
	}

IL_001f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_003d;
		}

IL_0024:
		{
			Intrusion_t1436088045 * L_4 = ((  Intrusion_t1436088045 * (*) (Enumerator_t318830006 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((Enumerator_t318830006 *)(&V_3), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
			V_2 = (Intrusion_t1436088045 *)L_4;
			ObjectU5BU5D_t11523773* L_5 = V_0;
			int32_t L_6 = V_1;
			Intrusion_t1436088045 * L_7 = V_2;
			NullCheck(L_7);
			Il2CppObject * L_8 = (Il2CppObject *)L_7->get_influence_1();
			NullCheck(L_5);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
			(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (Il2CppObject *)L_8);
			int32_t L_9 = V_1;
			V_1 = (int32_t)((int32_t)((int32_t)L_9+(int32_t)1));
		}

IL_003d:
		{
			bool L_10 = ((  bool (*) (Enumerator_t318830006 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((Enumerator_t318830006 *)(&V_3), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
			if (L_10)
			{
				goto IL_0024;
			}
		}

IL_0049:
		{
			IL2CPP_LEAVE(0x5A, FINALLY_004e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_004e;
	}

FINALLY_004e:
	{ // begin finally (depth: 1)
		Enumerator_t318830006  L_11 = V_3;
		Enumerator_t318830006  L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), &L_12);
		NullCheck((Il2CppObject *)L_13);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_13);
		IL2CPP_END_FINALLY(78)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(78)
	{
		IL2CPP_JUMP_TBL(0x5A, IL_005a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_005a:
	{
		ObjectU5BU5D_t11523773* L_14 = V_0;
		ImmutableList_1_t1897310919 * L_15 = (ImmutableList_1_t1897310919 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (ImmutableList_1_t1897310919 *, ObjectU5BU5D_t11523773*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(L_15, (ObjectU5BU5D_t11523773*)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		return L_15;
	}
}
// System.Void System.Action`1<GameAnalyticsSDK.Settings/HelpTypes>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m384607952_gshared (Action_1_t3439808249 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<GameAnalyticsSDK.Settings/HelpTypes>::Invoke(T)
extern "C"  void Action_1_Invoke_m2783359860_gshared (Action_1_t3439808249 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m2783359860((Action_1_t3439808249 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<GameAnalyticsSDK.Settings/HelpTypes>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* HelpTypes_t3291355544_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m1758067521_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1758067521_gshared (Action_1_t3439808249 * __this, int32_t ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m1758067521_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(HelpTypes_t3291355544_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<GameAnalyticsSDK.Settings/HelpTypes>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m3670278368_gshared (Action_1_t3439808249 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<GooglePlayGames.BasicApi.Nearby.AdvertisingResult>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m1976743594_gshared (Action_1_t3869889887 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<GooglePlayGames.BasicApi.Nearby.AdvertisingResult>::Invoke(T)
extern "C"  void Action_1_Invoke_m1737599660_gshared (Action_1_t3869889887 * __this, AdvertisingResult_t3721437182  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m1737599660((Action_1_t3869889887 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, AdvertisingResult_t3721437182  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, AdvertisingResult_t3721437182  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<GooglePlayGames.BasicApi.Nearby.AdvertisingResult>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* AdvertisingResult_t3721437182_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m4261843751_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m4261843751_gshared (Action_1_t3869889887 * __this, AdvertisingResult_t3721437182  ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m4261843751_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(AdvertisingResult_t3721437182_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<GooglePlayGames.BasicApi.Nearby.AdvertisingResult>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m1250954682_gshared (Action_1_t3869889887 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<GooglePlayGames.BasicApi.Nearby.ConnectionRequest>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m843564858_gshared (Action_1_t3665980111 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<GooglePlayGames.BasicApi.Nearby.ConnectionRequest>::Invoke(T)
extern "C"  void Action_1_Invoke_m1153105866_gshared (Action_1_t3665980111 * __this, ConnectionRequest_t3517527406  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m1153105866((Action_1_t3665980111 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, ConnectionRequest_t3517527406  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, ConnectionRequest_t3517527406  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<GooglePlayGames.BasicApi.Nearby.ConnectionRequest>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* ConnectionRequest_t3517527406_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m685527191_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m685527191_gshared (Action_1_t3665980111 * __this, ConnectionRequest_t3517527406  ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m685527191_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(ConnectionRequest_t3517527406_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<GooglePlayGames.BasicApi.Nearby.ConnectionRequest>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m1824777802_gshared (Action_1_t3665980111 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<GooglePlayGames.BasicApi.Nearby.ConnectionResponse>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m2084931440_gshared (Action_1_t1697901181 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<GooglePlayGames.BasicApi.Nearby.ConnectionResponse>::Invoke(T)
extern "C"  void Action_1_Invoke_m1137087460_gshared (Action_1_t1697901181 * __this, ConnectionResponse_t1549448476  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m1137087460((Action_1_t1697901181 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, ConnectionResponse_t1549448476  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, ConnectionResponse_t1549448476  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<GooglePlayGames.BasicApi.Nearby.ConnectionResponse>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* ConnectionResponse_t1549448476_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m2383279401_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m2383279401_gshared (Action_1_t1697901181 * __this, ConnectionResponse_t1549448476  ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m2383279401_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(ConnectionResponse_t1549448476_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<GooglePlayGames.BasicApi.Nearby.ConnectionResponse>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m4091949440_gshared (Action_1_t1697901181 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<GooglePlayGames.BasicApi.Nearby.InitializationStatus>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m2262397789_gshared (Action_1_t805070320 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<GooglePlayGames.BasicApi.Nearby.InitializationStatus>::Invoke(T)
extern "C"  void Action_1_Invoke_m2110401095_gshared (Action_1_t805070320 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m2110401095((Action_1_t805070320 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<GooglePlayGames.BasicApi.Nearby.InitializationStatus>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* InitializationStatus_t656617615_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m1004290204_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1004290204_gshared (Action_1_t805070320 * __this, int32_t ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m1004290204_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(InitializationStatus_t656617615_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<GooglePlayGames.BasicApi.Nearby.InitializationStatus>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m1660885997_gshared (Action_1_t805070320 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<GooglePlayGames.BasicApi.UIStatus>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m1165386806_gshared (Action_1_t284412649 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<GooglePlayGames.BasicApi.UIStatus>::Invoke(T)
extern "C"  void Action_1_Invoke_m2641070648_gshared (Action_1_t284412649 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m2641070648((Action_1_t284412649 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<GooglePlayGames.BasicApi.UIStatus>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* UIStatus_t135959944_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m3585610779_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m3585610779_gshared (Action_1_t284412649 * __this, int32_t ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m3585610779_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UIStatus_t135959944_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<GooglePlayGames.BasicApi.UIStatus>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m685793606_gshared (Action_1_t284412649 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<LitJson.PropertyMetadata>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m423598563_gshared (Action_1_t4090926794 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<LitJson.PropertyMetadata>::Invoke(T)
extern "C"  void Action_1_Invoke_m2179213569_gshared (Action_1_t4090926794 * __this, PropertyMetadata_t3942474089  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m2179213569((Action_1_t4090926794 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, PropertyMetadata_t3942474089  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, PropertyMetadata_t3942474089  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<LitJson.PropertyMetadata>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* PropertyMetadata_t3942474089_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m658090838_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m658090838_gshared (Action_1_t4090926794 * __this, PropertyMetadata_t3942474089  ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m658090838_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(PropertyMetadata_t3942474089_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<LitJson.PropertyMetadata>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m2228843891_gshared (Action_1_t4090926794 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<ModestTree.Util.MouseWheelScrollDirections>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m1139518760_gshared (Action_1_t3075561458 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<ModestTree.Util.MouseWheelScrollDirections>::Invoke(T)
extern "C"  void Action_1_Invoke_m2996669171_gshared (Action_1_t3075561458 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m2996669171((Action_1_t3075561458 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<ModestTree.Util.MouseWheelScrollDirections>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* MouseWheelScrollDirections_t2927108753_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m133552186_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m133552186_gshared (Action_1_t3075561458 * __this, int32_t ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m133552186_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(MouseWheelScrollDirections_t2927108753_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<ModestTree.Util.MouseWheelScrollDirections>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m3520516623_gshared (Action_1_t3075561458 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<PlayFab.Internal.GMFB_327/testRegion>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m765264978_gshared (Action_1_t1833392007 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<PlayFab.Internal.GMFB_327/testRegion>::Invoke(T)
extern "C"  void Action_1_Invoke_m111998898_gshared (Action_1_t1833392007 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m111998898((Action_1_t1833392007 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<PlayFab.Internal.GMFB_327/testRegion>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* testRegion_t1684939302_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m1397591815_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1397591815_gshared (Action_1_t1833392007 * __this, int32_t ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m1397591815_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(testRegion_t1684939302_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<PlayFab.Internal.GMFB_327/testRegion>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m3954405218_gshared (Action_1_t1833392007 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m2036539306_gshared (Action_1_t359458046 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Boolean>::Invoke(T)
extern "C"  void Action_1_Invoke_m510242183_gshared (Action_1_t359458046 * __this, bool ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m510242183((Action_1_t359458046 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m647183148_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m647183148_gshared (Action_1_t359458046 * __this, bool ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m647183148_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m1601629789_gshared (Action_1_t359458046 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Byte>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m2238172157_gshared (Action_1_t2927146526 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Byte>::Invoke(T)
extern "C"  void Action_1_Invoke_m3427425703_gshared (Action_1_t2927146526 * __this, uint8_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3427425703((Action_1_t2927146526 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, uint8_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, uint8_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Byte>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Byte_t2778693821_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m4157106804_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m4157106804_gshared (Action_1_t2927146526 * __this, uint8_t ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m4157106804_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Byte_t2778693821_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Byte>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m986891405_gshared (Action_1_t2927146526 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Char>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m1328190063_gshared (Action_1_t2927159404 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Char>::Invoke(T)
extern "C"  void Action_1_Invoke_m259754741_gshared (Action_1_t2927159404 * __this, uint16_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m259754741((Action_1_t2927159404 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, uint16_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, uint16_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Char>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Char_t2778706699_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m1985327554_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1985327554_gshared (Action_1_t2927159404 * __this, uint16_t ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m1985327554_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Char_t2778706699_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Char>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m248419327_gshared (Action_1_t2927159404 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m372701337_gshared (Action_1_t3461409153 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Invoke(T)
extern "C"  void Action_1_Invoke_m2500958603_gshared (Action_1_t3461409153 * __this, KeyValuePair_2_t3312956448  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m2500958603((Action_1_t3461409153 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, KeyValuePair_2_t3312956448  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, KeyValuePair_2_t3312956448  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* KeyValuePair_2_t3312956448_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m192152288_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m192152288_gshared (Action_1_t3461409153 * __this, KeyValuePair_2_t3312956448  ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m192152288_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(KeyValuePair_2_t3312956448_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m629710633_gshared (Action_1_t3461409153 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.DateTime>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m3448891722_gshared (Action_1_t487486641 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.DateTime>::Invoke(T)
extern "C"  void Action_1_Invoke_m2573544378_gshared (Action_1_t487486641 * __this, DateTime_t339033936  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m2573544378((Action_1_t487486641 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, DateTime_t339033936  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, DateTime_t339033936  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.DateTime>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* DateTime_t339033936_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m3178728327_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m3178728327_gshared (Action_1_t487486641 * __this, DateTime_t339033936  ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m3178728327_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(DateTime_t339033936_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.DateTime>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m437358682_gshared (Action_1_t487486641 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.DateTimeOffset>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m3791682106_gshared (Action_1_t3860712740 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.DateTimeOffset>::Invoke(T)
extern "C"  void Action_1_Invoke_m1246866861_gshared (Action_1_t3860712740 * __this, DateTimeOffset_t3712260035  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m1246866861((Action_1_t3860712740 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, DateTimeOffset_t3712260035  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, DateTimeOffset_t3712260035  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.DateTimeOffset>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* DateTimeOffset_t3712260035_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m1110225274_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1110225274_gshared (Action_1_t3860712740 * __this, DateTimeOffset_t3712260035  ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m1110225274_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(DateTimeOffset_t3712260035_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.DateTimeOffset>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m2292175943_gshared (Action_1_t3860712740 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Double>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m366957451_gshared (Action_1_t682969319 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Double>::Invoke(T)
extern "C"  void Action_1_Invoke_m1757410544_gshared (Action_1_t682969319 * __this, double ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m1757410544((Action_1_t682969319 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, double ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, double ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Double>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m1189541309_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1189541309_gshared (Action_1_t682969319 * __this, double ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m1189541309_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Double_t534516614_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Double>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m2796799204_gshared (Action_1_t682969319 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m1091880516_gshared (Action_1_t2995867492 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Int32>::Invoke(T)
extern "C"  void Action_1_Invoke_m4053252443_gshared (Action_1_t2995867492 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m4053252443((Action_1_t2995867492 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Int32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m3640105362_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m3640105362_gshared (Action_1_t2995867492 * __this, int32_t ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m3640105362_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m2973204151_gshared (Action_1_t2995867492 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Int64>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m1589437765_gshared (Action_1_t2995867587 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Int64>::Invoke(T)
extern "C"  void Action_1_Invoke_m312854300_gshared (Action_1_t2995867587 * __this, int64_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m312854300((Action_1_t2995867587 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int64_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int64_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Int64>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Int64_t2847414882_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m1103067249_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1103067249_gshared (Action_1_t2995867587 * __this, int64_t ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m1103067249_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int64_t2847414882_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Int64>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m1610696248_gshared (Action_1_t2995867587 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m1498188969_gshared (Action_1_t985559125 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Object>::Invoke(T)
extern "C"  void Action_1_Invoke_m2789055860_gshared (Action_1_t985559125 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m2789055860((Action_1_t985559125 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m917692971_gshared (Action_1_t985559125 * __this, Il2CppObject * ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___obj0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m3562128182_gshared (Action_1_t985559125 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m1819105799_gshared (Action_1_t467187834 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Reflection.CustomAttributeNamedArgument>::Invoke(T)
extern "C"  void Action_1_Invoke_m4053608541_gshared (Action_1_t467187834 * __this, CustomAttributeNamedArgument_t318735129  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m4053608541((Action_1_t467187834 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, CustomAttributeNamedArgument_t318735129  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, CustomAttributeNamedArgument_t318735129  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Reflection.CustomAttributeNamedArgument>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* CustomAttributeNamedArgument_t318735129_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m4060881842_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m4060881842_gshared (Action_1_t467187834 * __this, CustomAttributeNamedArgument_t318735129  ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m4060881842_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(CustomAttributeNamedArgument_t318735129_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Reflection.CustomAttributeNamedArgument>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m622126999_gshared (Action_1_t467187834 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m3098383478_gshared (Action_1_t708868267 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Reflection.CustomAttributeTypedArgument>::Invoke(T)
extern "C"  void Action_1_Invoke_m2306418446_gshared (Action_1_t708868267 * __this, CustomAttributeTypedArgument_t560415562  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m2306418446((Action_1_t708868267 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, CustomAttributeTypedArgument_t560415562  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, CustomAttributeTypedArgument_t560415562  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Reflection.CustomAttributeTypedArgument>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* CustomAttributeTypedArgument_t560415562_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m2018499939_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m2018499939_gshared (Action_1_t708868267 * __this, CustomAttributeTypedArgument_t560415562  ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m2018499939_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(CustomAttributeTypedArgument_t560415562_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Reflection.CustomAttributeTypedArgument>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m1169797510_gshared (Action_1_t708868267 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m1610927424_gshared (Action_1_t1106661726 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Single>::Invoke(T)
extern "C"  void Action_1_Invoke_m3985972031_gshared (Action_1_t1106661726 * __this, float ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3985972031((Action_1_t1106661726 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, float ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, float ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Single>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Single_t958209021_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m1836875956_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1836875956_gshared (Action_1_t1106661726 * __this, float ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m1836875956_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Single_t958209021_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m3782731597_gshared (Action_1_t1106661726 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.TimeSpan>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m792089713_gshared (Action_1_t912315597 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.TimeSpan>::Invoke(T)
extern "C"  void Action_1_Invoke_m613956924_gshared (Action_1_t912315597 * __this, TimeSpan_t763862892  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m613956924((Action_1_t912315597 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, TimeSpan_t763862892  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, TimeSpan_t763862892  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.TimeSpan>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* TimeSpan_t763862892_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m2797950371_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m2797950371_gshared (Action_1_t912315597 * __this, TimeSpan_t763862892  ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m2797950371_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(TimeSpan_t763862892_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.TimeSpan>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m2349114302_gshared (Action_1_t912315597 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.UInt64>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m2124079501_gshared (Action_1_t1134378126 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.UInt64>::Invoke(T)
extern "C"  void Action_1_Invoke_m1473029655_gshared (Action_1_t1134378126 * __this, uint64_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m1473029655((Action_1_t1134378126 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, uint64_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, uint64_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.UInt64>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt64_t985925421_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m1679111140_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1679111140_gshared (Action_1_t1134378126 * __this, uint64_t ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m1679111140_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UInt64_t985925421_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.UInt64>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m3415155229_gshared (Action_1_t1134378126 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<UniRx.CollectionAddEvent`1<System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m3043521032_gshared (Action_1_t2564974692 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<UniRx.CollectionAddEvent`1<System.Object>>::Invoke(T)
extern "C"  void Action_1_Invoke_m3668670780_gshared (Action_1_t2564974692 * __this, CollectionAddEvent_1_t2416521987  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3668670780((Action_1_t2564974692 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, CollectionAddEvent_1_t2416521987  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, CollectionAddEvent_1_t2416521987  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<UniRx.CollectionAddEvent`1<System.Object>>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* CollectionAddEvent_1_t2416521987_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m3862211593_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m3862211593_gshared (Action_1_t2564974692 * __this, CollectionAddEvent_1_t2416521987  ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m3862211593_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(CollectionAddEvent_1_t2416521987_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<UniRx.CollectionAddEvent`1<System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m160835608_gshared (Action_1_t2564974692 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<UniRx.Tuple`2<System.Object,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m2013857454_gshared (Action_1_t517714524 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<UniRx.Tuple`2<System.Object,System.Object>>::Invoke(T)
extern "C"  void Action_1_Invoke_m3370673622_gshared (Action_1_t517714524 * __this, Tuple_2_t369261819  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3370673622((Action_1_t517714524 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Tuple_2_t369261819  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Tuple_2_t369261819  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<UniRx.Tuple`2<System.Object,System.Object>>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Tuple_2_t369261819_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m650516011_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m650516011_gshared (Action_1_t517714524 * __this, Tuple_2_t369261819  ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m650516011_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Tuple_2_t369261819_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<UniRx.Tuple`2<System.Object,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m3445030846_gshared (Action_1_t517714524 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<UniRx.Tuple`3<System.Object,System.Object,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m3514679329_gshared (Action_1_t1799351807 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<UniRx.Tuple`3<System.Object,System.Object,System.Object>>::Invoke(T)
extern "C"  void Action_1_Invoke_m1936238851_gshared (Action_1_t1799351807 * __this, Tuple_3_t1650899102  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m1936238851((Action_1_t1799351807 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Tuple_3_t1650899102  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Tuple_3_t1650899102  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<UniRx.Tuple`3<System.Object,System.Object,System.Object>>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Tuple_3_t1650899102_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m3306910040_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m3306910040_gshared (Action_1_t1799351807 * __this, Tuple_3_t1650899102  ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m3306910040_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Tuple_3_t1650899102_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<UniRx.Tuple`3<System.Object,System.Object,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m1479568049_gshared (Action_1_t1799351807 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m3306708628_gshared (Action_1_t1175682986 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>>::Invoke(T)
extern "C"  void Action_1_Invoke_m770926384_gshared (Action_1_t1175682986 * __this, Tuple_4_t1027230281  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m770926384((Action_1_t1175682986 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Tuple_4_t1027230281  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Tuple_4_t1027230281  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Tuple_4_t1027230281_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m1426098053_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1426098053_gshared (Action_1_t1175682986 * __this, Tuple_4_t1027230281  ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m1426098053_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Tuple_4_t1027230281_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<UniRx.Tuple`4<System.Object,System.Object,System.Object,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m1575631524_gshared (Action_1_t1175682986 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<UniRx.Unit>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m1024136343_gshared (Action_1_t2706738743 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<UniRx.Unit>::Invoke(T)
extern "C"  void Action_1_Invoke_m1625430730_gshared (Action_1_t2706738743 * __this, Unit_t2558286038  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m1625430730((Action_1_t2706738743 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Unit_t2558286038  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Unit_t2558286038  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<UniRx.Unit>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m446945055_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m446945055_gshared (Action_1_t2706738743 * __this, Unit_t2558286038  ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m446945055_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Unit_t2558286038_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<UniRx.Unit>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m1504422730_gshared (Action_1_t2706738743 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m834289135_gshared (Action_1_t1736628465 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<UnityEngine.Color>::Invoke(T)
extern "C"  void Action_1_Invoke_m2400296588_gshared (Action_1_t1736628465 * __this, Color_t1588175760  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m2400296588((Action_1_t1736628465 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Color_t1588175760  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Color_t1588175760  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<UnityEngine.Color>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Color_t1588175760_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m353138009_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m353138009_gshared (Action_1_t1736628465 * __this, Color_t1588175760  ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m353138009_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Color_t1588175760_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<UnityEngine.Color>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m2394125512_gshared (Action_1_t1736628465 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<UnityEngine.Color32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m1455275257_gshared (Action_1_t4285536912 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<UnityEngine.Color32>::Invoke(T)
extern "C"  void Action_1_Invoke_m2445125931_gshared (Action_1_t4285536912 * __this, Color32_t4137084207  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m2445125931((Action_1_t4285536912 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Color32_t4137084207  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Color32_t4137084207  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<UnityEngine.Color32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Color32_t4137084207_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m154674680_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m154674680_gshared (Action_1_t4285536912 * __this, Color32_t4137084207  ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m154674680_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Color32_t4137084207_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<UnityEngine.Color32>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m4125135753_gshared (Action_1_t4285536912 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m525831357_gshared (Action_1_t1108351394 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<UnityEngine.EventSystems.RaycastResult>::Invoke(T)
extern "C"  void Action_1_Invoke_m3111208167_gshared (Action_1_t1108351394 * __this, RaycastResult_t959898689  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3111208167((Action_1_t1108351394 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, RaycastResult_t959898689  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, RaycastResult_t959898689  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<UnityEngine.EventSystems.RaycastResult>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* RaycastResult_t959898689_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m2799907388_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m2799907388_gshared (Action_1_t1108351394 * __this, RaycastResult_t959898689  ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m2799907388_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(RaycastResult_t959898689_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<UnityEngine.EventSystems.RaycastResult>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m1849746765_gshared (Action_1_t1108351394 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<UnityEngine.Quaternion>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m1398163489_gshared (Action_1_t2040168684 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<UnityEngine.Quaternion>::Invoke(T)
extern "C"  void Action_1_Invoke_m642195715_gshared (Action_1_t2040168684 * __this, Quaternion_t1891715979  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m642195715((Action_1_t2040168684 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Quaternion_t1891715979  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Quaternion_t1891715979  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<UnityEngine.Quaternion>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Quaternion_t1891715979_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m1932347992_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1932347992_gshared (Action_1_t2040168684 * __this, Quaternion_t1891715979  ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m1932347992_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Quaternion_t1891715979_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<UnityEngine.Quaternion>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m1804399281_gshared (Action_1_t2040168684 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<UnityEngine.Rect>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m3149112987_gshared (Action_1_t1673881522 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<UnityEngine.Rect>::Invoke(T)
extern "C"  void Action_1_Invoke_m3833986377_gshared (Action_1_t1673881522 * __this, Rect_t1525428817  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3833986377((Action_1_t1673881522 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Rect_t1525428817  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Rect_t1525428817  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<UnityEngine.Rect>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Rect_t1525428817_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m4020657054_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m4020657054_gshared (Action_1_t1673881522 * __this, Rect_t1525428817  ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m4020657054_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Rect_t1525428817_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<UnityEngine.Rect>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m1426103851_gshared (Action_1_t1673881522 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<UnityEngine.UICharInfo>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m578452423_gshared (Action_1_t552273286 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<UnityEngine.UICharInfo>::Invoke(T)
extern "C"  void Action_1_Invoke_m1606651549_gshared (Action_1_t552273286 * __this, UICharInfo_t403820581  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m1606651549((Action_1_t552273286 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, UICharInfo_t403820581  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, UICharInfo_t403820581  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<UnityEngine.UICharInfo>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* UICharInfo_t403820581_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m1263703538_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1263703538_gshared (Action_1_t552273286 * __this, UICharInfo_t403820581  ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m1263703538_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UICharInfo_t403820581_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<UnityEngine.UICharInfo>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m343468375_gshared (Action_1_t552273286 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
