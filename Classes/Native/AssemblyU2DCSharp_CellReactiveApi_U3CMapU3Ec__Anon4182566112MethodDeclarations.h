﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/<Map>c__AnonStorey10E`2<System.Boolean,System.Boolean>
struct U3CMapU3Ec__AnonStorey10E_2_t4182566112;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<System.Boolean>
struct Action_1_t359458046;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"

// System.Void CellReactiveApi/<Map>c__AnonStorey10E`2<System.Boolean,System.Boolean>::.ctor()
extern "C"  void U3CMapU3Ec__AnonStorey10E_2__ctor_m3755338828_gshared (U3CMapU3Ec__AnonStorey10E_2_t4182566112 * __this, const MethodInfo* method);
#define U3CMapU3Ec__AnonStorey10E_2__ctor_m3755338828(__this, method) ((  void (*) (U3CMapU3Ec__AnonStorey10E_2_t4182566112 *, const MethodInfo*))U3CMapU3Ec__AnonStorey10E_2__ctor_m3755338828_gshared)(__this, method)
// System.IDisposable CellReactiveApi/<Map>c__AnonStorey10E`2<System.Boolean,System.Boolean>::<>m__180(System.Action`1<T2>,Priority)
extern "C"  Il2CppObject * U3CMapU3Ec__AnonStorey10E_2_U3CU3Em__180_m2588572291_gshared (U3CMapU3Ec__AnonStorey10E_2_t4182566112 * __this, Action_1_t359458046 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define U3CMapU3Ec__AnonStorey10E_2_U3CU3Em__180_m2588572291(__this, ___reaction0, ___p1, method) ((  Il2CppObject * (*) (U3CMapU3Ec__AnonStorey10E_2_t4182566112 *, Action_1_t359458046 *, int32_t, const MethodInfo*))U3CMapU3Ec__AnonStorey10E_2_U3CU3Em__180_m2588572291_gshared)(__this, ___reaction0, ___p1, method)
// T2 CellReactiveApi/<Map>c__AnonStorey10E`2<System.Boolean,System.Boolean>::<>m__181()
extern "C"  bool U3CMapU3Ec__AnonStorey10E_2_U3CU3Em__181_m2772125942_gshared (U3CMapU3Ec__AnonStorey10E_2_t4182566112 * __this, const MethodInfo* method);
#define U3CMapU3Ec__AnonStorey10E_2_U3CU3Em__181_m2772125942(__this, method) ((  bool (*) (U3CMapU3Ec__AnonStorey10E_2_t4182566112 *, const MethodInfo*))U3CMapU3Ec__AnonStorey10E_2_U3CU3Em__181_m2772125942_gshared)(__this, method)
