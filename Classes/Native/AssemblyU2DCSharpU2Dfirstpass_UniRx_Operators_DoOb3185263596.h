﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.DoObserverObservable`1<System.Object>
struct DoObserverObservable_1_t2771775371;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.DoObserverObservable`1/Do<System.Object>
struct  Do_t3185263596  : public OperatorObserverBase_2_t1187768149
{
public:
	// UniRx.Operators.DoObserverObservable`1<T> UniRx.Operators.DoObserverObservable`1/Do::parent
	DoObserverObservable_1_t2771775371 * ___parent_2;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(Do_t3185263596, ___parent_2)); }
	inline DoObserverObservable_1_t2771775371 * get_parent_2() const { return ___parent_2; }
	inline DoObserverObservable_1_t2771775371 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(DoObserverObservable_1_t2771775371 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
