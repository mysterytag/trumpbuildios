﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.RefCountObservable`1/RefCount<System.Object>
struct RefCount_t184354677;
// UniRx.Operators.RefCountObservable`1<System.Object>
struct RefCountObservable_1_t1275253774;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.RefCountObservable`1/RefCount<System.Object>::.ctor(UniRx.Operators.RefCountObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void RefCount__ctor_m3881395840_gshared (RefCount_t184354677 * __this, RefCountObservable_1_t1275253774 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define RefCount__ctor_m3881395840(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (RefCount_t184354677 *, RefCountObservable_1_t1275253774 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))RefCount__ctor_m3881395840_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.RefCountObservable`1/RefCount<System.Object>::Run()
extern "C"  Il2CppObject * RefCount_Run_m3829612741_gshared (RefCount_t184354677 * __this, const MethodInfo* method);
#define RefCount_Run_m3829612741(__this, method) ((  Il2CppObject * (*) (RefCount_t184354677 *, const MethodInfo*))RefCount_Run_m3829612741_gshared)(__this, method)
// System.Void UniRx.Operators.RefCountObservable`1/RefCount<System.Object>::OnNext(T)
extern "C"  void RefCount_OnNext_m2152355039_gshared (RefCount_t184354677 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define RefCount_OnNext_m2152355039(__this, ___value0, method) ((  void (*) (RefCount_t184354677 *, Il2CppObject *, const MethodInfo*))RefCount_OnNext_m2152355039_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.RefCountObservable`1/RefCount<System.Object>::OnError(System.Exception)
extern "C"  void RefCount_OnError_m2556350958_gshared (RefCount_t184354677 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define RefCount_OnError_m2556350958(__this, ___error0, method) ((  void (*) (RefCount_t184354677 *, Exception_t1967233988 *, const MethodInfo*))RefCount_OnError_m2556350958_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.RefCountObservable`1/RefCount<System.Object>::OnCompleted()
extern "C"  void RefCount_OnCompleted_m1280735809_gshared (RefCount_t184354677 * __this, const MethodInfo* method);
#define RefCount_OnCompleted_m1280735809(__this, method) ((  void (*) (RefCount_t184354677 *, const MethodInfo*))RefCount_OnCompleted_m1280735809_gshared)(__this, method)
