﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BioCell`1/<>c__AnonStoreyF4<System.Object>
struct U3CU3Ec__AnonStoreyF4_t4282333876;

#include "codegen/il2cpp-codegen.h"

// System.Void BioCell`1/<>c__AnonStoreyF4<System.Object>::.ctor()
extern "C"  void U3CU3Ec__AnonStoreyF4__ctor_m3819804446_gshared (U3CU3Ec__AnonStoreyF4_t4282333876 * __this, const MethodInfo* method);
#define U3CU3Ec__AnonStoreyF4__ctor_m3819804446(__this, method) ((  void (*) (U3CU3Ec__AnonStoreyF4_t4282333876 *, const MethodInfo*))U3CU3Ec__AnonStoreyF4__ctor_m3819804446_gshared)(__this, method)
// System.Void BioCell`1/<>c__AnonStoreyF4<System.Object>::<>m__163()
extern "C"  void U3CU3Ec__AnonStoreyF4_U3CU3Em__163_m3465695113_gshared (U3CU3Ec__AnonStoreyF4_t4282333876 * __this, const MethodInfo* method);
#define U3CU3Ec__AnonStoreyF4_U3CU3Em__163_m3465695113(__this, method) ((  void (*) (U3CU3Ec__AnonStoreyF4_t4282333876 *, const MethodInfo*))U3CU3Ec__AnonStoreyF4_U3CU3Em__163_m3465695113_gshared)(__this, method)
