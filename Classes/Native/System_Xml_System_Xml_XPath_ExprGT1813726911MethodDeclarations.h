﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XPath.ExprGT
struct ExprGT_t1813726911;
// System.Xml.XPath.Expression
struct Expression_t4217024437;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XPath_Expression4217024437.h"

// System.Void System.Xml.XPath.ExprGT::.ctor(System.Xml.XPath.Expression,System.Xml.XPath.Expression)
extern "C"  void ExprGT__ctor_m2309283432 (ExprGT_t1813726911 * __this, Expression_t4217024437 * ___left0, Expression_t4217024437 * ___right1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XPath.ExprGT::get_Operator()
extern "C"  String_t* ExprGT_get_Operator_m2603739638 (ExprGT_t1813726911 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.ExprGT::Compare(System.Double,System.Double)
extern "C"  bool ExprGT_Compare_m3941486427 (ExprGT_t1813726911 * __this, double ___arg10, double ___arg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
