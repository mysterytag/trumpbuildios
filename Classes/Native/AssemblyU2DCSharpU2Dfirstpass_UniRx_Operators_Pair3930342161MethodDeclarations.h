﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.PairwiseObservable`2/Pairwise<System.Object,System.Object>
struct Pairwise_t3930342161;
// UniRx.Operators.PairwiseObservable`2<System.Object,System.Object>
struct PairwiseObservable_2_t3262709271;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.PairwiseObservable`2/Pairwise<System.Object,System.Object>::.ctor(UniRx.Operators.PairwiseObservable`2<T,TR>,UniRx.IObserver`1<TR>,System.IDisposable)
extern "C"  void Pairwise__ctor_m2467662618_gshared (Pairwise_t3930342161 * __this, PairwiseObservable_2_t3262709271 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Pairwise__ctor_m2467662618(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Pairwise_t3930342161 *, PairwiseObservable_2_t3262709271 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Pairwise__ctor_m2467662618_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.PairwiseObservable`2/Pairwise<System.Object,System.Object>::OnNext(T)
extern "C"  void Pairwise_OnNext_m1956971760_gshared (Pairwise_t3930342161 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Pairwise_OnNext_m1956971760(__this, ___value0, method) ((  void (*) (Pairwise_t3930342161 *, Il2CppObject *, const MethodInfo*))Pairwise_OnNext_m1956971760_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.PairwiseObservable`2/Pairwise<System.Object,System.Object>::OnError(System.Exception)
extern "C"  void Pairwise_OnError_m3164684287_gshared (Pairwise_t3930342161 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Pairwise_OnError_m3164684287(__this, ___error0, method) ((  void (*) (Pairwise_t3930342161 *, Exception_t1967233988 *, const MethodInfo*))Pairwise_OnError_m3164684287_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.PairwiseObservable`2/Pairwise<System.Object,System.Object>::OnCompleted()
extern "C"  void Pairwise_OnCompleted_m885570002_gshared (Pairwise_t3930342161 * __this, const MethodInfo* method);
#define Pairwise_OnCompleted_m885570002(__this, method) ((  void (*) (Pairwise_t3930342161 *, const MethodInfo*))Pairwise_OnCompleted_m885570002_gshared)(__this, method)
