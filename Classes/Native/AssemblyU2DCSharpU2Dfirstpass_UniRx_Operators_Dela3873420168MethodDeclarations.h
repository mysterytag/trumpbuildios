﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.DelaySubscriptionObservable`1/<SubscribeCore>c__AnonStorey5F<System.Object>
struct U3CSubscribeCoreU3Ec__AnonStorey5F_t3873420168;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.DelaySubscriptionObservable`1/<SubscribeCore>c__AnonStorey5F<System.Object>::.ctor()
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey5F__ctor_m2995231988_gshared (U3CSubscribeCoreU3Ec__AnonStorey5F_t3873420168 * __this, const MethodInfo* method);
#define U3CSubscribeCoreU3Ec__AnonStorey5F__ctor_m2995231988(__this, method) ((  void (*) (U3CSubscribeCoreU3Ec__AnonStorey5F_t3873420168 *, const MethodInfo*))U3CSubscribeCoreU3Ec__AnonStorey5F__ctor_m2995231988_gshared)(__this, method)
// System.Void UniRx.Operators.DelaySubscriptionObservable`1/<SubscribeCore>c__AnonStorey5F<System.Object>::<>m__79()
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey5F_U3CU3Em__79_m3098079519_gshared (U3CSubscribeCoreU3Ec__AnonStorey5F_t3873420168 * __this, const MethodInfo* method);
#define U3CSubscribeCoreU3Ec__AnonStorey5F_U3CU3Em__79_m3098079519(__this, method) ((  void (*) (U3CSubscribeCoreU3Ec__AnonStorey5F_t3873420168 *, const MethodInfo*))U3CSubscribeCoreU3Ec__AnonStorey5F_U3CU3Em__79_m3098079519_gshared)(__this, method)
