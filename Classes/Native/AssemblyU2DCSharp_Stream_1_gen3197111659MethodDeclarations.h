﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Stream`1<System.Boolean>
struct Stream_1_t3197111659;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<System.Boolean>
struct Action_1_t359458046;
// System.Action
struct Action_t437523947;
// IStream`1<System.Boolean>
struct IStream_1_t763696332;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"
#include "System_Core_System_Action437523947.h"

// System.Void Stream`1<System.Boolean>::.ctor()
extern "C"  void Stream_1__ctor_m3459184177_gshared (Stream_1_t3197111659 * __this, const MethodInfo* method);
#define Stream_1__ctor_m3459184177(__this, method) ((  void (*) (Stream_1_t3197111659 *, const MethodInfo*))Stream_1__ctor_m3459184177_gshared)(__this, method)
// System.Void Stream`1<System.Boolean>::Send(T)
extern "C"  void Stream_1_Send_m1988401731_gshared (Stream_1_t3197111659 * __this, bool ___value0, const MethodInfo* method);
#define Stream_1_Send_m1988401731(__this, ___value0, method) ((  void (*) (Stream_1_t3197111659 *, bool, const MethodInfo*))Stream_1_Send_m1988401731_gshared)(__this, ___value0, method)
// System.Void Stream`1<System.Boolean>::SendInTransaction(T,System.Int32)
extern "C"  void Stream_1_SendInTransaction_m800469017_gshared (Stream_1_t3197111659 * __this, bool ___value0, int32_t ___p1, const MethodInfo* method);
#define Stream_1_SendInTransaction_m800469017(__this, ___value0, ___p1, method) ((  void (*) (Stream_1_t3197111659 *, bool, int32_t, const MethodInfo*))Stream_1_SendInTransaction_m800469017_gshared)(__this, ___value0, ___p1, method)
// System.IDisposable Stream`1<System.Boolean>::Listen(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Stream_1_Listen_m1071073365_gshared (Stream_1_t3197111659 * __this, Action_1_t359458046 * ___action0, int32_t ___priority1, const MethodInfo* method);
#define Stream_1_Listen_m1071073365(__this, ___action0, ___priority1, method) ((  Il2CppObject * (*) (Stream_1_t3197111659 *, Action_1_t359458046 *, int32_t, const MethodInfo*))Stream_1_Listen_m1071073365_gshared)(__this, ___action0, ___priority1, method)
// System.IDisposable Stream`1<System.Boolean>::Listen(System.Action,Priority)
extern "C"  Il2CppObject * Stream_1_Listen_m3921221554_gshared (Stream_1_t3197111659 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method);
#define Stream_1_Listen_m3921221554(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (Stream_1_t3197111659 *, Action_t437523947 *, int32_t, const MethodInfo*))Stream_1_Listen_m3921221554_gshared)(__this, ___action0, ___p1, method)
// System.Void Stream`1<System.Boolean>::Dispose()
extern "C"  void Stream_1_Dispose_m4164557358_gshared (Stream_1_t3197111659 * __this, const MethodInfo* method);
#define Stream_1_Dispose_m4164557358(__this, method) ((  void (*) (Stream_1_t3197111659 *, const MethodInfo*))Stream_1_Dispose_m4164557358_gshared)(__this, method)
// System.Void Stream`1<System.Boolean>::SetInputStream(IStream`1<T>)
extern "C"  void Stream_1_SetInputStream_m1717621359_gshared (Stream_1_t3197111659 * __this, Il2CppObject* ___stream0, const MethodInfo* method);
#define Stream_1_SetInputStream_m1717621359(__this, ___stream0, method) ((  void (*) (Stream_1_t3197111659 *, Il2CppObject*, const MethodInfo*))Stream_1_SetInputStream_m1717621359_gshared)(__this, ___stream0, method)
// System.Void Stream`1<System.Boolean>::ClearInputStream()
extern "C"  void Stream_1_ClearInputStream_m1684075024_gshared (Stream_1_t3197111659 * __this, const MethodInfo* method);
#define Stream_1_ClearInputStream_m1684075024(__this, method) ((  void (*) (Stream_1_t3197111659 *, const MethodInfo*))Stream_1_ClearInputStream_m1684075024_gshared)(__this, method)
// System.Void Stream`1<System.Boolean>::TransactionIterationFinished()
extern "C"  void Stream_1_TransactionIterationFinished_m3659143652_gshared (Stream_1_t3197111659 * __this, const MethodInfo* method);
#define Stream_1_TransactionIterationFinished_m3659143652(__this, method) ((  void (*) (Stream_1_t3197111659 *, const MethodInfo*))Stream_1_TransactionIterationFinished_m3659143652_gshared)(__this, method)
// System.Void Stream`1<System.Boolean>::Unpack(System.Int32)
extern "C"  void Stream_1_Unpack_m3770242550_gshared (Stream_1_t3197111659 * __this, int32_t ___p0, const MethodInfo* method);
#define Stream_1_Unpack_m3770242550(__this, ___p0, method) ((  void (*) (Stream_1_t3197111659 *, int32_t, const MethodInfo*))Stream_1_Unpack_m3770242550_gshared)(__this, ___p0, method)
// System.Void Stream`1<System.Boolean>::<SetInputStream>m__1BA(T)
extern "C"  void Stream_1_U3CSetInputStreamU3Em__1BA_m541567630_gshared (Stream_1_t3197111659 * __this, bool ___val0, const MethodInfo* method);
#define Stream_1_U3CSetInputStreamU3Em__1BA_m541567630(__this, ___val0, method) ((  void (*) (Stream_1_t3197111659 *, bool, const MethodInfo*))Stream_1_U3CSetInputStreamU3Em__1BA_m541567630_gshared)(__this, ___val0, method)
