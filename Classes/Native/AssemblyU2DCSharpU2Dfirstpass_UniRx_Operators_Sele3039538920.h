﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.SelectManyObservable`2<UniRx.Unit,UniRx.Unit>
struct SelectManyObservable_2_t3540602214;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4165376397.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.SelectManyObservable`2/SelectManyEnumerableObserverWithIndex<UniRx.Unit,UniRx.Unit>
struct  SelectManyEnumerableObserverWithIndex_t3039538920  : public OperatorObserverBase_2_t4165376397
{
public:
	// UniRx.Operators.SelectManyObservable`2<TSource,TResult> UniRx.Operators.SelectManyObservable`2/SelectManyEnumerableObserverWithIndex::parent
	SelectManyObservable_2_t3540602214 * ___parent_2;
	// System.Int32 UniRx.Operators.SelectManyObservable`2/SelectManyEnumerableObserverWithIndex::index
	int32_t ___index_3;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(SelectManyEnumerableObserverWithIndex_t3039538920, ___parent_2)); }
	inline SelectManyObservable_2_t3540602214 * get_parent_2() const { return ___parent_2; }
	inline SelectManyObservable_2_t3540602214 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(SelectManyObservable_2_t3540602214 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(SelectManyEnumerableObserverWithIndex_t3039538920, ___index_3)); }
	inline int32_t get_index_3() const { return ___index_3; }
	inline int32_t* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(int32_t value)
	{
		___index_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
