﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ionic.Zlib.ZlibCodec
struct ZlibCodec_t3910383704;
// System.Byte[]
struct ByteU5BU5D_t58506160;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Ionic_Zlib_CompressionMode1632830838.h"
#include "AssemblyU2DCSharp_Ionic_Zlib_FlushType3984958891.h"
#include "AssemblyU2DCSharp_Ionic_Zlib_CompressionLevel3940478795.h"
#include "AssemblyU2DCSharp_Ionic_Zlib_CompressionStrategy2328611910.h"

// System.Void Ionic.Zlib.ZlibCodec::.ctor()
extern "C"  void ZlibCodec__ctor_m2186324999 (ZlibCodec_t3910383704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.ZlibCodec::.ctor(Ionic.Zlib.CompressionMode)
extern "C"  void ZlibCodec__ctor_m3599167829 (ZlibCodec_t3910383704 * __this, int32_t ___mode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.ZlibCodec::get_Adler32()
extern "C"  int32_t ZlibCodec_get_Adler32_m454643299 (ZlibCodec_t3910383704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.ZlibCodec::InitializeInflate()
extern "C"  int32_t ZlibCodec_InitializeInflate_m195314542 (ZlibCodec_t3910383704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.ZlibCodec::InitializeInflate(System.Boolean)
extern "C"  int32_t ZlibCodec_InitializeInflate_m3894501477 (ZlibCodec_t3910383704 * __this, bool ___expectRfc1950Header0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.ZlibCodec::InitializeInflate(System.Int32)
extern "C"  int32_t ZlibCodec_InitializeInflate_m2469544895 (ZlibCodec_t3910383704 * __this, int32_t ___windowBits0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.ZlibCodec::InitializeInflate(System.Int32,System.Boolean)
extern "C"  int32_t ZlibCodec_InitializeInflate_m3870570974 (ZlibCodec_t3910383704 * __this, int32_t ___windowBits0, bool ___expectRfc1950Header1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.ZlibCodec::Inflate(Ionic.Zlib.FlushType)
extern "C"  int32_t ZlibCodec_Inflate_m1747914391 (ZlibCodec_t3910383704 * __this, int32_t ___flush0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.ZlibCodec::EndInflate()
extern "C"  int32_t ZlibCodec_EndInflate_m2112575383 (ZlibCodec_t3910383704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.ZlibCodec::SyncInflate()
extern "C"  int32_t ZlibCodec_SyncInflate_m3845245251 (ZlibCodec_t3910383704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.ZlibCodec::InitializeDeflate()
extern "C"  int32_t ZlibCodec_InitializeDeflate_m2137228434 (ZlibCodec_t3910383704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.ZlibCodec::InitializeDeflate(Ionic.Zlib.CompressionLevel)
extern "C"  int32_t ZlibCodec_InitializeDeflate_m2139280683 (ZlibCodec_t3910383704 * __this, int32_t ___level0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.ZlibCodec::InitializeDeflate(Ionic.Zlib.CompressionLevel,System.Boolean)
extern "C"  int32_t ZlibCodec_InitializeDeflate_m2560172786 (ZlibCodec_t3910383704 * __this, int32_t ___level0, bool ___wantRfc1950Header1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.ZlibCodec::InitializeDeflate(Ionic.Zlib.CompressionLevel,System.Int32)
extern "C"  int32_t ZlibCodec_InitializeDeflate_m2883798412 (ZlibCodec_t3910383704 * __this, int32_t ___level0, int32_t ___bits1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.ZlibCodec::InitializeDeflate(Ionic.Zlib.CompressionLevel,System.Int32,System.Boolean)
extern "C"  int32_t ZlibCodec_InitializeDeflate_m1771029617 (ZlibCodec_t3910383704 * __this, int32_t ___level0, int32_t ___bits1, bool ___wantRfc1950Header2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.ZlibCodec::_InternalInitializeDeflate(System.Boolean)
extern "C"  int32_t ZlibCodec__InternalInitializeDeflate_m2769852321 (ZlibCodec_t3910383704 * __this, bool ___wantRfc1950Header0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.ZlibCodec::Deflate(Ionic.Zlib.FlushType)
extern "C"  int32_t ZlibCodec_Deflate_m575034811 (ZlibCodec_t3910383704 * __this, int32_t ___flush0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.ZlibCodec::EndDeflate()
extern "C"  int32_t ZlibCodec_EndDeflate_m4054489275 (ZlibCodec_t3910383704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.ZlibCodec::ResetDeflate()
extern "C"  void ZlibCodec_ResetDeflate_m1996325177 (ZlibCodec_t3910383704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.ZlibCodec::SetDeflateParams(Ionic.Zlib.CompressionLevel,Ionic.Zlib.CompressionStrategy)
extern "C"  int32_t ZlibCodec_SetDeflateParams_m1285747745 (ZlibCodec_t3910383704 * __this, int32_t ___level0, int32_t ___strategy1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.ZlibCodec::SetDictionary(System.Byte[])
extern "C"  int32_t ZlibCodec_SetDictionary_m3210371002 (ZlibCodec_t3910383704 * __this, ByteU5BU5D_t58506160* ___dictionary0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.ZlibCodec::flush_pending()
extern "C"  void ZlibCodec_flush_pending_m303019425 (ZlibCodec_t3910383704 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.ZlibCodec::read_buf(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t ZlibCodec_read_buf_m1096775732 (ZlibCodec_t3910383704 * __this, ByteU5BU5D_t58506160* ___buf0, int32_t ___start1, int32_t ___size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
