﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/UpdatePlayerStatisticsResponseCallback
struct UpdatePlayerStatisticsResponseCallback_t1614859187;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.UpdatePlayerStatisticsRequest
struct UpdatePlayerStatisticsRequest_t2201995250;
// PlayFab.ClientModels.UpdatePlayerStatisticsResult
struct UpdatePlayerStatisticsResult_t2045842554;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UpdatePlaye2201995250.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UpdatePlaye2045842554.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/UpdatePlayerStatisticsResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void UpdatePlayerStatisticsResponseCallback__ctor_m3963325218 (UpdatePlayerStatisticsResponseCallback_t1614859187 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UpdatePlayerStatisticsResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.UpdatePlayerStatisticsRequest,PlayFab.ClientModels.UpdatePlayerStatisticsResult,PlayFab.PlayFabError,System.Object)
extern "C"  void UpdatePlayerStatisticsResponseCallback_Invoke_m595640399 (UpdatePlayerStatisticsResponseCallback_t1614859187 * __this, String_t* ___urlPath0, int32_t ___callId1, UpdatePlayerStatisticsRequest_t2201995250 * ___request2, UpdatePlayerStatisticsResult_t2045842554 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_UpdatePlayerStatisticsResponseCallback_t1614859187(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, UpdatePlayerStatisticsRequest_t2201995250 * ___request2, UpdatePlayerStatisticsResult_t2045842554 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/UpdatePlayerStatisticsResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.UpdatePlayerStatisticsRequest,PlayFab.ClientModels.UpdatePlayerStatisticsResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UpdatePlayerStatisticsResponseCallback_BeginInvoke_m2955970364 (UpdatePlayerStatisticsResponseCallback_t1614859187 * __this, String_t* ___urlPath0, int32_t ___callId1, UpdatePlayerStatisticsRequest_t2201995250 * ___request2, UpdatePlayerStatisticsResult_t2045842554 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UpdatePlayerStatisticsResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void UpdatePlayerStatisticsResponseCallback_EndInvoke_m3922058802 (UpdatePlayerStatisticsResponseCallback_t1614859187 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
