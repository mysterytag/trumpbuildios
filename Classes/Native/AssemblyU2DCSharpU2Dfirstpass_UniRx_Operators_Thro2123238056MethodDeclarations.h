﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ThrottleFirstObservable`1<System.Object>
struct ThrottleFirstObservable_1_t2123238056;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// UniRx.IScheduler
struct IScheduler_t2938318244;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_TimeSpan763862892.h"

// System.Void UniRx.Operators.ThrottleFirstObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.TimeSpan,UniRx.IScheduler)
extern "C"  void ThrottleFirstObservable_1__ctor_m2021260689_gshared (ThrottleFirstObservable_1_t2123238056 * __this, Il2CppObject* ___source0, TimeSpan_t763862892  ___dueTime1, Il2CppObject * ___scheduler2, const MethodInfo* method);
#define ThrottleFirstObservable_1__ctor_m2021260689(__this, ___source0, ___dueTime1, ___scheduler2, method) ((  void (*) (ThrottleFirstObservable_1_t2123238056 *, Il2CppObject*, TimeSpan_t763862892 , Il2CppObject *, const MethodInfo*))ThrottleFirstObservable_1__ctor_m2021260689_gshared)(__this, ___source0, ___dueTime1, ___scheduler2, method)
// System.IDisposable UniRx.Operators.ThrottleFirstObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * ThrottleFirstObservable_1_SubscribeCore_m1500428200_gshared (ThrottleFirstObservable_1_t2123238056 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define ThrottleFirstObservable_1_SubscribeCore_m1500428200(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (ThrottleFirstObservable_1_t2123238056 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))ThrottleFirstObservable_1_SubscribeCore_m1500428200_gshared)(__this, ___observer0, ___cancel1, method)
