﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observer/EmptyOnNextAnonymousObserver`1<System.Object>
struct EmptyOnNextAnonymousObserver_1_t1584423078;
// System.Action`1<System.Exception>
struct Action_1_t2115686693;
// System.Action
struct Action_t437523947;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Observer/EmptyOnNextAnonymousObserver`1<System.Object>::.ctor(System.Action`1<System.Exception>,System.Action)
extern "C"  void EmptyOnNextAnonymousObserver_1__ctor_m2864327496_gshared (EmptyOnNextAnonymousObserver_1_t1584423078 * __this, Action_1_t2115686693 * ___onError0, Action_t437523947 * ___onCompleted1, const MethodInfo* method);
#define EmptyOnNextAnonymousObserver_1__ctor_m2864327496(__this, ___onError0, ___onCompleted1, method) ((  void (*) (EmptyOnNextAnonymousObserver_1_t1584423078 *, Action_1_t2115686693 *, Action_t437523947 *, const MethodInfo*))EmptyOnNextAnonymousObserver_1__ctor_m2864327496_gshared)(__this, ___onError0, ___onCompleted1, method)
// System.Void UniRx.Observer/EmptyOnNextAnonymousObserver`1<System.Object>::OnNext(T)
extern "C"  void EmptyOnNextAnonymousObserver_1_OnNext_m230038353_gshared (EmptyOnNextAnonymousObserver_1_t1584423078 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define EmptyOnNextAnonymousObserver_1_OnNext_m230038353(__this, ___value0, method) ((  void (*) (EmptyOnNextAnonymousObserver_1_t1584423078 *, Il2CppObject *, const MethodInfo*))EmptyOnNextAnonymousObserver_1_OnNext_m230038353_gshared)(__this, ___value0, method)
// System.Void UniRx.Observer/EmptyOnNextAnonymousObserver`1<System.Object>::OnError(System.Exception)
extern "C"  void EmptyOnNextAnonymousObserver_1_OnError_m1025733728_gshared (EmptyOnNextAnonymousObserver_1_t1584423078 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define EmptyOnNextAnonymousObserver_1_OnError_m1025733728(__this, ___error0, method) ((  void (*) (EmptyOnNextAnonymousObserver_1_t1584423078 *, Exception_t1967233988 *, const MethodInfo*))EmptyOnNextAnonymousObserver_1_OnError_m1025733728_gshared)(__this, ___error0, method)
// System.Void UniRx.Observer/EmptyOnNextAnonymousObserver`1<System.Object>::OnCompleted()
extern "C"  void EmptyOnNextAnonymousObserver_1_OnCompleted_m414562227_gshared (EmptyOnNextAnonymousObserver_1_t1584423078 * __this, const MethodInfo* method);
#define EmptyOnNextAnonymousObserver_1_OnCompleted_m414562227(__this, method) ((  void (*) (EmptyOnNextAnonymousObserver_1_t1584423078 *, const MethodInfo*))EmptyOnNextAnonymousObserver_1_OnCompleted_m414562227_gshared)(__this, method)
