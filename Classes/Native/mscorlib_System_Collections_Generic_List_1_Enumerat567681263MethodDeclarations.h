﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<PlayFab.Internal.GMFB_327/testRegion>
struct List_1_t2481898271;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat567681263.h"
#include "AssemblyU2DCSharp_PlayFab_Internal_GMFB_327_testRe1684939302.h"

// System.Void System.Collections.Generic.List`1/Enumerator<PlayFab.Internal.GMFB_327/testRegion>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m168140695_gshared (Enumerator_t567681263 * __this, List_1_t2481898271 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m168140695(__this, ___l0, method) ((  void (*) (Enumerator_t567681263 *, List_1_t2481898271 *, const MethodInfo*))Enumerator__ctor_m168140695_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1368117851_gshared (Enumerator_t567681263 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1368117851(__this, method) ((  void (*) (Enumerator_t567681263 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1368117851_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m467820689_gshared (Enumerator_t567681263 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m467820689(__this, method) ((  Il2CppObject * (*) (Enumerator_t567681263 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m467820689_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PlayFab.Internal.GMFB_327/testRegion>::Dispose()
extern "C"  void Enumerator_Dispose_m835076924_gshared (Enumerator_t567681263 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m835076924(__this, method) ((  void (*) (Enumerator_t567681263 *, const MethodInfo*))Enumerator_Dispose_m835076924_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PlayFab.Internal.GMFB_327/testRegion>::VerifyState()
extern "C"  void Enumerator_VerifyState_m4218291317_gshared (Enumerator_t567681263 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m4218291317(__this, method) ((  void (*) (Enumerator_t567681263 *, const MethodInfo*))Enumerator_VerifyState_m4218291317_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<PlayFab.Internal.GMFB_327/testRegion>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m896392587_gshared (Enumerator_t567681263 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m896392587(__this, method) ((  bool (*) (Enumerator_t567681263 *, const MethodInfo*))Enumerator_MoveNext_m896392587_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<PlayFab.Internal.GMFB_327/testRegion>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m1890753934_gshared (Enumerator_t567681263 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1890753934(__this, method) ((  int32_t (*) (Enumerator_t567681263 *, const MethodInfo*))Enumerator_get_Current_m1890753934_gshared)(__this, method)
