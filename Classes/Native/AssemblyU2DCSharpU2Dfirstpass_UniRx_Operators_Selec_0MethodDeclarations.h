﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectManyObservable`2/SelectManyObserverWithIndex/SelectMany<UniRx.Unit,UniRx.Unit>
struct SelectMany_t993934561;
// UniRx.Operators.SelectManyObservable`2/SelectManyObserverWithIndex<UniRx.Unit,UniRx.Unit>
struct SelectManyObserverWithIndex_t795413344;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyObserverWithIndex/SelectMany<UniRx.Unit,UniRx.Unit>::.ctor(UniRx.Operators.SelectManyObservable`2/SelectManyObserverWithIndex<TSource,TResult>,System.IDisposable)
extern "C"  void SelectMany__ctor_m1189673813_gshared (SelectMany_t993934561 * __this, SelectManyObserverWithIndex_t795413344 * ___parent0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define SelectMany__ctor_m1189673813(__this, ___parent0, ___cancel1, method) ((  void (*) (SelectMany_t993934561 *, SelectManyObserverWithIndex_t795413344 *, Il2CppObject *, const MethodInfo*))SelectMany__ctor_m1189673813_gshared)(__this, ___parent0, ___cancel1, method)
// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyObserverWithIndex/SelectMany<UniRx.Unit,UniRx.Unit>::OnNext(TResult)
extern "C"  void SelectMany_OnNext_m3192022211_gshared (SelectMany_t993934561 * __this, Unit_t2558286038  ___value0, const MethodInfo* method);
#define SelectMany_OnNext_m3192022211(__this, ___value0, method) ((  void (*) (SelectMany_t993934561 *, Unit_t2558286038 , const MethodInfo*))SelectMany_OnNext_m3192022211_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyObserverWithIndex/SelectMany<UniRx.Unit,UniRx.Unit>::OnError(System.Exception)
extern "C"  void SelectMany_OnError_m2297979311_gshared (SelectMany_t993934561 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define SelectMany_OnError_m2297979311(__this, ___error0, method) ((  void (*) (SelectMany_t993934561 *, Exception_t1967233988 *, const MethodInfo*))SelectMany_OnError_m2297979311_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyObserverWithIndex/SelectMany<UniRx.Unit,UniRx.Unit>::OnCompleted()
extern "C"  void SelectMany_OnCompleted_m2667635586_gshared (SelectMany_t993934561 * __this, const MethodInfo* method);
#define SelectMany_OnCompleted_m2667635586(__this, method) ((  void (*) (SelectMany_t993934561 *, const MethodInfo*))SelectMany_OnCompleted_m2667635586_gshared)(__this, method)
