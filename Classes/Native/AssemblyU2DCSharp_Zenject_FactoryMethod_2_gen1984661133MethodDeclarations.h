﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.FactoryMethod`2<System.Object,System.Object>
struct FactoryMethod_2_t1984661133;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t3431720054;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void Zenject.FactoryMethod`2<System.Object,System.Object>::.ctor()
extern "C"  void FactoryMethod_2__ctor_m1426588911_gshared (FactoryMethod_2_t1984661133 * __this, const MethodInfo* method);
#define FactoryMethod_2__ctor_m1426588911(__this, method) ((  void (*) (FactoryMethod_2_t1984661133 *, const MethodInfo*))FactoryMethod_2__ctor_m1426588911_gshared)(__this, method)
// System.Type Zenject.FactoryMethod`2<System.Object,System.Object>::get_ConstructedType()
extern "C"  Type_t * FactoryMethod_2_get_ConstructedType_m2813654042_gshared (FactoryMethod_2_t1984661133 * __this, const MethodInfo* method);
#define FactoryMethod_2_get_ConstructedType_m2813654042(__this, method) ((  Type_t * (*) (FactoryMethod_2_t1984661133 *, const MethodInfo*))FactoryMethod_2_get_ConstructedType_m2813654042_gshared)(__this, method)
// System.Type[] Zenject.FactoryMethod`2<System.Object,System.Object>::get_ProvidedTypes()
extern "C"  TypeU5BU5D_t3431720054* FactoryMethod_2_get_ProvidedTypes_m2770746242_gshared (FactoryMethod_2_t1984661133 * __this, const MethodInfo* method);
#define FactoryMethod_2_get_ProvidedTypes_m2770746242(__this, method) ((  TypeU5BU5D_t3431720054* (*) (FactoryMethod_2_t1984661133 *, const MethodInfo*))FactoryMethod_2_get_ProvidedTypes_m2770746242_gshared)(__this, method)
// TValue Zenject.FactoryMethod`2<System.Object,System.Object>::Create(TParam1)
extern "C"  Il2CppObject * FactoryMethod_2_Create_m3412655113_gshared (FactoryMethod_2_t1984661133 * __this, Il2CppObject * ___param0, const MethodInfo* method);
#define FactoryMethod_2_Create_m3412655113(__this, ___param0, method) ((  Il2CppObject * (*) (FactoryMethod_2_t1984661133 *, Il2CppObject *, const MethodInfo*))FactoryMethod_2_Create_m3412655113_gshared)(__this, ___param0, method)
