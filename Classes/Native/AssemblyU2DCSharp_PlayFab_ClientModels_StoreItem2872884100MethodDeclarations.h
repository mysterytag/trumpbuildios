﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.StoreItem
struct StoreItem_t2872884100;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.UInt32>
struct Dictionary_2_t2623623230;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.StoreItem::.ctor()
extern "C"  void StoreItem__ctor_m2646522149 (StoreItem_t2872884100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.StoreItem::get_ItemId()
extern "C"  String_t* StoreItem_get_ItemId_m3392186745 (StoreItem_t2872884100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.StoreItem::set_ItemId(System.String)
extern "C"  void StoreItem_set_ItemId_m3311521528 (StoreItem_t2872884100 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.UInt32> PlayFab.ClientModels.StoreItem::get_VirtualCurrencyPrices()
extern "C"  Dictionary_2_t2623623230 * StoreItem_get_VirtualCurrencyPrices_m2503535818 (StoreItem_t2872884100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.StoreItem::set_VirtualCurrencyPrices(System.Collections.Generic.Dictionary`2<System.String,System.UInt32>)
extern "C"  void StoreItem_set_VirtualCurrencyPrices_m4140756165 (StoreItem_t2872884100 * __this, Dictionary_2_t2623623230 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.UInt32> PlayFab.ClientModels.StoreItem::get_RealCurrencyPrices()
extern "C"  Dictionary_2_t2623623230 * StoreItem_get_RealCurrencyPrices_m3970713847 (StoreItem_t2872884100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.StoreItem::set_RealCurrencyPrices(System.Collections.Generic.Dictionary`2<System.String,System.UInt32>)
extern "C"  void StoreItem_set_RealCurrencyPrices_m1937727534 (StoreItem_t2872884100 * __this, Dictionary_2_t2623623230 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
