﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen2307296439MethodDeclarations.h"

// System.Void UniRx.Subject`1<UniRx.Tuple`2<UnityEngine.RenderTexture,UnityEngine.RenderTexture>>::.ctor()
#define Subject_1__ctor_m499991160(__this, method) ((  void (*) (Subject_1_t3856149375 *, const MethodInfo*))Subject_1__ctor_m3673523288_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.Tuple`2<UnityEngine.RenderTexture,UnityEngine.RenderTexture>>::get_HasObservers()
#define Subject_1_get_HasObservers_m1988230236(__this, method) ((  bool (*) (Subject_1_t3856149375 *, const MethodInfo*))Subject_1_get_HasObservers_m1225079420_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.Tuple`2<UnityEngine.RenderTexture,UnityEngine.RenderTexture>>::OnCompleted()
#define Subject_1_OnCompleted_m1747631106(__this, method) ((  void (*) (Subject_1_t3856149375 *, const MethodInfo*))Subject_1_OnCompleted_m1708266978_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.Tuple`2<UnityEngine.RenderTexture,UnityEngine.RenderTexture>>::OnError(System.Exception)
#define Subject_1_OnError_m2879486511(__this, ___error0, method) ((  void (*) (Subject_1_t3856149375 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m2796618767_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<UniRx.Tuple`2<UnityEngine.RenderTexture,UnityEngine.RenderTexture>>::OnNext(T)
#define Subject_1_OnNext_m2285870880(__this, ___value0, method) ((  void (*) (Subject_1_t3856149375 *, Tuple_2_t1918114755 , const MethodInfo*))Subject_1_OnNext_m2623465728_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<UniRx.Tuple`2<UnityEngine.RenderTexture,UnityEngine.RenderTexture>>::Subscribe(UniRx.IObserver`1<T>)
#define Subject_1_Subscribe_m3045123789(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t3856149375 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m376261037_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<UniRx.Tuple`2<UnityEngine.RenderTexture,UnityEngine.RenderTexture>>::Dispose()
#define Subject_1_Dispose_m3648417973(__this, method) ((  void (*) (Subject_1_t3856149375 *, const MethodInfo*))Subject_1_Dispose_m3986012821_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.Tuple`2<UnityEngine.RenderTexture,UnityEngine.RenderTexture>>::ThrowIfDisposed()
#define Subject_1_ThrowIfDisposed_m3615921470(__this, method) ((  void (*) (Subject_1_t3856149375 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m2620260126_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.Tuple`2<UnityEngine.RenderTexture,UnityEngine.RenderTexture>>::IsRequiredSubscribeOnCurrentThread()
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m2929887827(__this, method) ((  bool (*) (Subject_1_t3856149375 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m4284383347_gshared)(__this, method)
