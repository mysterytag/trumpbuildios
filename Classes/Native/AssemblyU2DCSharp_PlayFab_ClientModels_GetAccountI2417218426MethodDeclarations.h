﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetAccountInfoRequest
struct GetAccountInfoRequest_t2417218426;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.GetAccountInfoRequest::.ctor()
extern "C"  void GetAccountInfoRequest__ctor_m2719384815 (GetAccountInfoRequest_t2417218426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GetAccountInfoRequest::get_PlayFabId()
extern "C"  String_t* GetAccountInfoRequest_get_PlayFabId_m3968835279 (GetAccountInfoRequest_t2417218426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetAccountInfoRequest::set_PlayFabId(System.String)
extern "C"  void GetAccountInfoRequest_set_PlayFabId_m2541277668 (GetAccountInfoRequest_t2417218426 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GetAccountInfoRequest::get_Username()
extern "C"  String_t* GetAccountInfoRequest_get_Username_m35488439 (GetAccountInfoRequest_t2417218426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetAccountInfoRequest::set_Username(System.String)
extern "C"  void GetAccountInfoRequest_set_Username_m1827898938 (GetAccountInfoRequest_t2417218426 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GetAccountInfoRequest::get_Email()
extern "C"  String_t* GetAccountInfoRequest_get_Email_m4219675453 (GetAccountInfoRequest_t2417218426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetAccountInfoRequest::set_Email(System.String)
extern "C"  void GetAccountInfoRequest_set_Email_m2222702774 (GetAccountInfoRequest_t2417218426 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GetAccountInfoRequest::get_TitleDisplayName()
extern "C"  String_t* GetAccountInfoRequest_get_TitleDisplayName_m4262069014 (GetAccountInfoRequest_t2417218426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetAccountInfoRequest::set_TitleDisplayName(System.String)
extern "C"  void GetAccountInfoRequest_set_TitleDisplayName_m2874595643 (GetAccountInfoRequest_t2417218426 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
