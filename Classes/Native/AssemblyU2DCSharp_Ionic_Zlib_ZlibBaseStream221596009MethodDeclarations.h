﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ionic.Zlib.ZlibBaseStream
struct ZlibBaseStream_t221596009;
// System.IO.Stream
struct Stream_t219029575;
// Ionic.Zlib.ZlibCodec
struct ZlibCodec_t3910383704;
// System.Byte[]
struct ByteU5BU5D_t58506160;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream219029575.h"
#include "AssemblyU2DCSharp_Ionic_Zlib_CompressionMode1632830838.h"
#include "AssemblyU2DCSharp_Ionic_Zlib_CompressionLevel3940478795.h"
#include "AssemblyU2DCSharp_Ionic_Zlib_ZlibStreamFlavor1582004022.h"
#include "mscorlib_System_IO_SeekOrigin3506139269.h"
#include "mscorlib_System_String968488902.h"

// System.Void Ionic.Zlib.ZlibBaseStream::.ctor(System.IO.Stream,Ionic.Zlib.CompressionMode,Ionic.Zlib.CompressionLevel,Ionic.Zlib.ZlibStreamFlavor,System.Boolean)
extern "C"  void ZlibBaseStream__ctor_m2526797141 (ZlibBaseStream_t221596009 * __this, Stream_t219029575 * ___stream0, int32_t ___compressionMode1, int32_t ___level2, int32_t ___flavor3, bool ___leaveOpen4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.ZlibBaseStream::get_Crc32()
extern "C"  int32_t ZlibBaseStream_get_Crc32_m3025573902 (ZlibBaseStream_t221596009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Ionic.Zlib.ZlibBaseStream::get__wantCompress()
extern "C"  bool ZlibBaseStream_get__wantCompress_m711852306 (ZlibBaseStream_t221596009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Ionic.Zlib.ZlibCodec Ionic.Zlib.ZlibBaseStream::get_z()
extern "C"  ZlibCodec_t3910383704 * ZlibBaseStream_get_z_m764204688 (ZlibBaseStream_t221596009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Ionic.Zlib.ZlibBaseStream::get_workingBuffer()
extern "C"  ByteU5BU5D_t58506160* ZlibBaseStream_get_workingBuffer_m35417640 (ZlibBaseStream_t221596009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.ZlibBaseStream::Write(System.Byte[],System.Int32,System.Int32)
extern "C"  void ZlibBaseStream_Write_m2295181748 (ZlibBaseStream_t221596009 * __this, ByteU5BU5D_t58506160* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.ZlibBaseStream::finish()
extern "C"  void ZlibBaseStream_finish_m4013975519 (ZlibBaseStream_t221596009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.ZlibBaseStream::end()
extern "C"  void ZlibBaseStream_end_m461989873 (ZlibBaseStream_t221596009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.ZlibBaseStream::Close()
extern "C"  void ZlibBaseStream_Close_m1435476078 (ZlibBaseStream_t221596009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.ZlibBaseStream::Flush()
extern "C"  void ZlibBaseStream_Flush_m4103531130 (ZlibBaseStream_t221596009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Ionic.Zlib.ZlibBaseStream::Seek(System.Int64,System.IO.SeekOrigin)
extern "C"  int64_t ZlibBaseStream_Seek_m3045437944 (ZlibBaseStream_t221596009 * __this, int64_t ___offset0, int32_t ___origin1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.ZlibBaseStream::SetLength(System.Int64)
extern "C"  void ZlibBaseStream_SetLength_m2861054384 (ZlibBaseStream_t221596009 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Ionic.Zlib.ZlibBaseStream::ReadZeroTerminatedString()
extern "C"  String_t* ZlibBaseStream_ReadZeroTerminatedString_m1640974907 (ZlibBaseStream_t221596009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.ZlibBaseStream::_ReadAndValidateGzipHeader()
extern "C"  int32_t ZlibBaseStream__ReadAndValidateGzipHeader_m688390109 (ZlibBaseStream_t221596009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.ZlibBaseStream::Read(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t ZlibBaseStream_Read_m4174991029 (ZlibBaseStream_t221596009 * __this, ByteU5BU5D_t58506160* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Ionic.Zlib.ZlibBaseStream::get_CanRead()
extern "C"  bool ZlibBaseStream_get_CanRead_m3365913031 (ZlibBaseStream_t221596009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Ionic.Zlib.ZlibBaseStream::get_CanSeek()
extern "C"  bool ZlibBaseStream_get_CanSeek_m3394668073 (ZlibBaseStream_t221596009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Ionic.Zlib.ZlibBaseStream::get_CanWrite()
extern "C"  bool ZlibBaseStream_get_CanWrite_m1786742384 (ZlibBaseStream_t221596009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Ionic.Zlib.ZlibBaseStream::get_Length()
extern "C"  int64_t ZlibBaseStream_get_Length_m317126252 (ZlibBaseStream_t221596009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Ionic.Zlib.ZlibBaseStream::get_Position()
extern "C"  int64_t ZlibBaseStream_get_Position_m37151407 (ZlibBaseStream_t221596009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.ZlibBaseStream::set_Position(System.Int64)
extern "C"  void ZlibBaseStream_set_Position_m3200797860 (ZlibBaseStream_t221596009 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.ZlibBaseStream::CompressString(System.String,System.IO.Stream)
extern "C"  void ZlibBaseStream_CompressString_m1357480602 (Il2CppObject * __this /* static, unused */, String_t* ___s0, Stream_t219029575 * ___compressor1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.ZlibBaseStream::CompressBuffer(System.Byte[],System.IO.Stream)
extern "C"  void ZlibBaseStream_CompressBuffer_m3767902434 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t58506160* ___b0, Stream_t219029575 * ___compressor1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Ionic.Zlib.ZlibBaseStream::UncompressString(System.Byte[],System.IO.Stream)
extern "C"  String_t* ZlibBaseStream_UncompressString_m2686353097 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t58506160* ___compressed0, Stream_t219029575 * ___decompressor1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Ionic.Zlib.ZlibBaseStream::UncompressBuffer(System.Byte[],System.IO.Stream)
extern "C"  ByteU5BU5D_t58506160* ZlibBaseStream_UncompressBuffer_m3701480401 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t58506160* ___compressed0, Stream_t219029575 * ___decompressor1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
