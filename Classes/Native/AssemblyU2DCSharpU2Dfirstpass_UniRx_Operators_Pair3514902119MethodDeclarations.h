﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.PairwiseObservable`1/Pairwise<System.Object>
struct Pairwise_t3514902119;
// UniRx.IObserver`1<UniRx.Pair`1<System.Object>>
struct IObserver_1_t1748129189;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.PairwiseObservable`1/Pairwise<System.Object>::.ctor(UniRx.IObserver`1<UniRx.Pair`1<T>>,System.IDisposable)
extern "C"  void Pairwise__ctor_m2314644484_gshared (Pairwise_t3514902119 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define Pairwise__ctor_m2314644484(__this, ___observer0, ___cancel1, method) ((  void (*) (Pairwise_t3514902119 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Pairwise__ctor_m2314644484_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.PairwiseObservable`1/Pairwise<System.Object>::OnNext(T)
extern "C"  void Pairwise_OnNext_m2793972255_gshared (Pairwise_t3514902119 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Pairwise_OnNext_m2793972255(__this, ___value0, method) ((  void (*) (Pairwise_t3514902119 *, Il2CppObject *, const MethodInfo*))Pairwise_OnNext_m2793972255_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.PairwiseObservable`1/Pairwise<System.Object>::OnError(System.Exception)
extern "C"  void Pairwise_OnError_m3302661934_gshared (Pairwise_t3514902119 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Pairwise_OnError_m3302661934(__this, ___error0, method) ((  void (*) (Pairwise_t3514902119 *, Exception_t1967233988 *, const MethodInfo*))Pairwise_OnError_m3302661934_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.PairwiseObservable`1/Pairwise<System.Object>::OnCompleted()
extern "C"  void Pairwise_OnCompleted_m1680615297_gshared (Pairwise_t3514902119 * __this, const MethodInfo* method);
#define Pairwise_OnCompleted_m1680615297(__this, method) ((  void (*) (Pairwise_t3514902119 *, const MethodInfo*))Pairwise_OnCompleted_m1680615297_gshared)(__this, method)
