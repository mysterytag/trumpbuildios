﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct EmptyObserver_1_t783258925;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionRepl1373905935.h"

// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m2696238903_gshared (EmptyObserver_1_t783258925 * __this, const MethodInfo* method);
#define EmptyObserver_1__ctor_m2696238903(__this, method) ((  void (*) (EmptyObserver_1_t783258925 *, const MethodInfo*))EmptyObserver_1__ctor_m2696238903_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m1496931158_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EmptyObserver_1__cctor_m1496931158(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EmptyObserver_1__cctor_m1496931158_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m2440693121_gshared (EmptyObserver_1_t783258925 * __this, const MethodInfo* method);
#define EmptyObserver_1_OnCompleted_m2440693121(__this, method) ((  void (*) (EmptyObserver_1_t783258925 *, const MethodInfo*))EmptyObserver_1_OnCompleted_m2440693121_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m105610542_gshared (EmptyObserver_1_t783258925 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define EmptyObserver_1_OnError_m105610542(__this, ___error0, method) ((  void (*) (EmptyObserver_1_t783258925 *, Exception_t1967233988 *, const MethodInfo*))EmptyObserver_1_OnError_m105610542_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m4051009567_gshared (EmptyObserver_1_t783258925 * __this, CollectionReplaceEvent_1_t1373905935  ___value0, const MethodInfo* method);
#define EmptyObserver_1_OnNext_m4051009567(__this, ___value0, method) ((  void (*) (EmptyObserver_1_t783258925 *, CollectionReplaceEvent_1_t1373905935 , const MethodInfo*))EmptyObserver_1_OnNext_m4051009567_gshared)(__this, ___value0, method)
