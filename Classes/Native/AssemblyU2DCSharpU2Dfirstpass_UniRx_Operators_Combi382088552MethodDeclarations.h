﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.CombineLatestFunc`4<System.Object,System.Object,System.Object,System.Object>
struct CombineLatestFunc_4_t382088552;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void UniRx.Operators.CombineLatestFunc`4<System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void CombineLatestFunc_4__ctor_m3400937874_gshared (CombineLatestFunc_4_t382088552 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define CombineLatestFunc_4__ctor_m3400937874(__this, ___object0, ___method1, method) ((  void (*) (CombineLatestFunc_4_t382088552 *, Il2CppObject *, IntPtr_t, const MethodInfo*))CombineLatestFunc_4__ctor_m3400937874_gshared)(__this, ___object0, ___method1, method)
// TR UniRx.Operators.CombineLatestFunc`4<System.Object,System.Object,System.Object,System.Object>::Invoke(T1,T2,T3)
extern "C"  Il2CppObject * CombineLatestFunc_4_Invoke_m3024523061_gshared (CombineLatestFunc_4_t382088552 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
#define CombineLatestFunc_4_Invoke_m3024523061(__this, ___arg10, ___arg21, ___arg32, method) ((  Il2CppObject * (*) (CombineLatestFunc_4_t382088552 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))CombineLatestFunc_4_Invoke_m3024523061_gshared)(__this, ___arg10, ___arg21, ___arg32, method)
// System.IAsyncResult UniRx.Operators.CombineLatestFunc`4<System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * CombineLatestFunc_4_BeginInvoke_m818044421_gshared (CombineLatestFunc_4_t382088552 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, AsyncCallback_t1363551830 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method);
#define CombineLatestFunc_4_BeginInvoke_m818044421(__this, ___arg10, ___arg21, ___arg32, ___callback3, ___object4, method) ((  Il2CppObject * (*) (CombineLatestFunc_4_t382088552 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))CombineLatestFunc_4_BeginInvoke_m818044421_gshared)(__this, ___arg10, ___arg21, ___arg32, ___callback3, ___object4, method)
// TR UniRx.Operators.CombineLatestFunc`4<System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * CombineLatestFunc_4_EndInvoke_m193277641_gshared (CombineLatestFunc_4_t382088552 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define CombineLatestFunc_4_EndInvoke_m193277641(__this, ___result0, method) ((  Il2CppObject * (*) (CombineLatestFunc_4_t382088552 *, Il2CppObject *, const MethodInfo*))CombineLatestFunc_4_EndInvoke_m193277641_gshared)(__this, ___result0, method)
