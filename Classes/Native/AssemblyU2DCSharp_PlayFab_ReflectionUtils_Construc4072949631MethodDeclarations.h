﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ReflectionUtils/ConstructorDelegate
struct ConstructorDelegate_t4072949631;
// System.Object
struct Il2CppObject;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.ReflectionUtils/ConstructorDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void ConstructorDelegate__ctor_m2830896658 (ConstructorDelegate_t4072949631 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayFab.ReflectionUtils/ConstructorDelegate::Invoke(System.Object[])
extern "C"  Il2CppObject * ConstructorDelegate_Invoke_m486974331 (ConstructorDelegate_t4072949631 * __this, ObjectU5BU5D_t11523773* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" Il2CppObject * pinvoke_delegate_wrapper_ConstructorDelegate_t4072949631(Il2CppObject* delegate, ObjectU5BU5D_t11523773* ___args0);
// System.IAsyncResult PlayFab.ReflectionUtils/ConstructorDelegate::BeginInvoke(System.Object[],System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ConstructorDelegate_BeginInvoke_m2944030515 (ConstructorDelegate_t4072949631 * __this, ObjectU5BU5D_t11523773* ___args0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayFab.ReflectionUtils/ConstructorDelegate::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * ConstructorDelegate_EndInvoke_m3161198925 (ConstructorDelegate_t4072949631 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
