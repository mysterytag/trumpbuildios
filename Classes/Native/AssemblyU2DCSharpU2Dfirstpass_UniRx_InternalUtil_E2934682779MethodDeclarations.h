﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Vector3>
struct EmptyObserver_1_t2934682779;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Vector3>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m2427068851_gshared (EmptyObserver_1_t2934682779 * __this, const MethodInfo* method);
#define EmptyObserver_1__ctor_m2427068851(__this, method) ((  void (*) (EmptyObserver_1_t2934682779 *, const MethodInfo*))EmptyObserver_1__ctor_m2427068851_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Vector3>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m1742594138_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EmptyObserver_1__cctor_m1742594138(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EmptyObserver_1__cctor_m1742594138_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Vector3>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m78906109_gshared (EmptyObserver_1_t2934682779 * __this, const MethodInfo* method);
#define EmptyObserver_1_OnCompleted_m78906109(__this, method) ((  void (*) (EmptyObserver_1_t2934682779 *, const MethodInfo*))EmptyObserver_1_OnCompleted_m78906109_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Vector3>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m1894326442_gshared (EmptyObserver_1_t2934682779 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define EmptyObserver_1_OnError_m1894326442(__this, ___error0, method) ((  void (*) (EmptyObserver_1_t2934682779 *, Exception_t1967233988 *, const MethodInfo*))EmptyObserver_1_OnError_m1894326442_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.Vector3>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m3076627355_gshared (EmptyObserver_1_t2934682779 * __this, Vector3_t3525329789  ___value0, const MethodInfo* method);
#define EmptyObserver_1_OnNext_m3076627355(__this, ___value0, method) ((  void (*) (EmptyObserver_1_t2934682779 *, Vector3_t3525329789 , const MethodInfo*))EmptyObserver_1_OnNext_m3076627355_gshared)(__this, ___value0, method)
