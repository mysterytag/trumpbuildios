﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetPurchaseResponseCallback
struct GetPurchaseResponseCallback_t670132253;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetPurchaseRequest
struct GetPurchaseRequest_t593210440;
// PlayFab.ClientModels.GetPurchaseResult
struct GetPurchaseResult_t54283620;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPurchaseR593210440.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPurchaseRe54283620.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetPurchaseResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetPurchaseResponseCallback__ctor_m2909262380 (GetPurchaseResponseCallback_t670132253 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetPurchaseResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetPurchaseRequest,PlayFab.ClientModels.GetPurchaseResult,PlayFab.PlayFabError,System.Object)
extern "C"  void GetPurchaseResponseCallback_Invoke_m35604037 (GetPurchaseResponseCallback_t670132253 * __this, String_t* ___urlPath0, int32_t ___callId1, GetPurchaseRequest_t593210440 * ___request2, GetPurchaseResult_t54283620 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetPurchaseResponseCallback_t670132253(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetPurchaseRequest_t593210440 * ___request2, GetPurchaseResult_t54283620 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetPurchaseResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetPurchaseRequest,PlayFab.ClientModels.GetPurchaseResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetPurchaseResponseCallback_BeginInvoke_m2973978914 (GetPurchaseResponseCallback_t670132253 * __this, String_t* ___urlPath0, int32_t ___callId1, GetPurchaseRequest_t593210440 * ___request2, GetPurchaseResult_t54283620 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetPurchaseResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetPurchaseResponseCallback_EndInvoke_m3094559292 (GetPurchaseResponseCallback_t670132253 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
