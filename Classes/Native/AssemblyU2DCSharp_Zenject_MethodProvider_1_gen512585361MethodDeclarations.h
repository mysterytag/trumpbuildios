﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.MethodProvider`1<System.Object>
struct MethodProvider_1_t512585361;
// System.Func`2<Zenject.InjectContext,System.Object>
struct Func_2_t2621245597;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// Zenject.InjectContext
struct InjectContext_t3456483891;
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException>
struct IEnumerable_1_t4073207355;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_InjectContext3456483891.h"

// System.Void Zenject.MethodProvider`1<System.Object>::.ctor(System.Func`2<Zenject.InjectContext,T>)
extern "C"  void MethodProvider_1__ctor_m1654905075_gshared (MethodProvider_1_t512585361 * __this, Func_2_t2621245597 * ___method0, const MethodInfo* method);
#define MethodProvider_1__ctor_m1654905075(__this, ___method0, method) ((  void (*) (MethodProvider_1_t512585361 *, Func_2_t2621245597 *, const MethodInfo*))MethodProvider_1__ctor_m1654905075_gshared)(__this, ___method0, method)
// System.Type Zenject.MethodProvider`1<System.Object>::GetInstanceType()
extern "C"  Type_t * MethodProvider_1_GetInstanceType_m1846484974_gshared (MethodProvider_1_t512585361 * __this, const MethodInfo* method);
#define MethodProvider_1_GetInstanceType_m1846484974(__this, method) ((  Type_t * (*) (MethodProvider_1_t512585361 *, const MethodInfo*))MethodProvider_1_GetInstanceType_m1846484974_gshared)(__this, method)
// System.Object Zenject.MethodProvider`1<System.Object>::GetInstance(Zenject.InjectContext)
extern "C"  Il2CppObject * MethodProvider_1_GetInstance_m2279203076_gshared (MethodProvider_1_t512585361 * __this, InjectContext_t3456483891 * ___context0, const MethodInfo* method);
#define MethodProvider_1_GetInstance_m2279203076(__this, ___context0, method) ((  Il2CppObject * (*) (MethodProvider_1_t512585361 *, InjectContext_t3456483891 *, const MethodInfo*))MethodProvider_1_GetInstance_m2279203076_gshared)(__this, ___context0, method)
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.MethodProvider`1<System.Object>::ValidateBinding(Zenject.InjectContext)
extern "C"  Il2CppObject* MethodProvider_1_ValidateBinding_m871735702_gshared (MethodProvider_1_t512585361 * __this, InjectContext_t3456483891 * ___context0, const MethodInfo* method);
#define MethodProvider_1_ValidateBinding_m871735702(__this, ___context0, method) ((  Il2CppObject* (*) (MethodProvider_1_t512585361 *, InjectContext_t3456483891 *, const MethodInfo*))MethodProvider_1_ValidateBinding_m871735702_gshared)(__this, ___context0, method)
