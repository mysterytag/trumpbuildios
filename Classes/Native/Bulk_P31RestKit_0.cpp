﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// Prime31.AbstractManager
struct AbstractManager_t2874768250;
// Prime31.LifecycleHelper
struct LifecycleHelper_t3719100935;
// System.Object
struct Il2CppObject;
// Prime31.ThreadingCallbackHelper
struct ThreadingCallbackHelper_t3423238746;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// System.Type
struct Type_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t2474804324;
// System.Action`1<System.Boolean>
struct Action_1_t359458046;
// Prime31.MonoBehaviourGUI
struct MonoBehaviourGUI_t2892502096;
// UnityEngine.Texture2D
struct Texture2D_t2509538522;
// System.String
struct String_t;
// System.Action
struct Action_t437523947;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "P31RestKit_U3CModuleU3E86524790.h"
#include "P31RestKit_U3CModuleU3E86524790MethodDeclarations.h"
#include "P31RestKit_Prime31_AbstractManager2874768250.h"
#include "P31RestKit_Prime31_AbstractManager2874768250MethodDeclarations.h"
#include "mscorlib_System_Void2779279689.h"
#include "UnityEngine_UnityEngine_MonoBehaviour3012272455MethodDeclarations.h"
#include "P31RestKit_Prime31_LifecycleHelper3719100935.h"
#include "UnityEngine_UnityEngine_Object3878351788MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject4012695102MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"
#include "mscorlib_System_Boolean211005341.h"
#include "UnityEngine_UnityEngine_Object3878351788.h"
#include "P31RestKit_Prime31_ThreadingCallbackHelper3423238746.h"
#include "mscorlib_System_Type2779229935MethodDeclarations.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_System_RuntimeTypeHandle1864875887.h"
#include "mscorlib_System_String968488902.h"
#include "UnityEngine_UnityEngine_Transform284553113MethodDeclarations.h"
#include "mscorlib_System_String968488902MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug1588791936MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "UnityEngine_UnityEngine_UnityException3148635335.h"
#include "mscorlib_System_Reflection_MemberInfo2843033814MethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfo2843033814.h"
#include "UnityEngine_UnityEngine_Component2126946602.h"
#include "UnityEngine_UnityEngine_Transform284553113.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Object837106420.h"
#include "UnityEngine_UnityEngine_Component2126946602MethodDeclarations.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "P31RestKit_Prime31_DeserializationExtensions2032005330.h"
#include "P31RestKit_Prime31_DeserializationExtensions2032005330MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2474804324.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2474804324MethodDeclarations.h"
#include "mscorlib_System_Collections_ArrayList2121638921MethodDeclarations.h"
#include "mscorlib_System_Reflection_FieldInfo1164929782.h"
#include "mscorlib_System_Int322847414787.h"
#include "P31RestKit_Prime31_P31DeserializeableFieldAttribut1330016988.h"
#include "mscorlib_System_Collections_ArrayList2121638921.h"
#include "mscorlib_System_Reflection_BindingFlags2090192240.h"
#include "mscorlib_System_Reflection_FieldInfo1164929782MethodDeclarations.h"
#include "P31RestKit_Prime31_LifecycleHelper3719100935MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen359458046.h"
#include "mscorlib_System_Delegate3660574010MethodDeclarations.h"
#include "mscorlib_System_Delegate3660574010.h"
#include "mscorlib_System_Threading_Interlocked3007803305MethodDeclarations.h"
#include "mscorlib_System_Threading_Interlocked3007803305.h"
#include "mscorlib_System_Action_1_gen359458046MethodDeclarations.h"
#include "P31RestKit_Prime31_MonoBehaviourGUI2892502096.h"
#include "P31RestKit_Prime31_MonoBehaviourGUI2892502096MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1848703245MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder3822575854MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1848703245.h"
#include "mscorlib_System_Text_StringBuilder3822575854.h"
#include "mscorlib_System_Single958209021.h"
#include "UnityEngine_UnityEngine_Texture2D2509538522.h"
#include "UnityEngine_UnityEngine_Texture2D2509538522MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color1588175760MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"
#include "UnityEngine_UnityEngine_Screen3994030297MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI1522956648MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUISkin2614611333MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStyle1006925219MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayout2490032242MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUISkin2614611333.h"
#include "UnityEngine_UnityEngine_GUIStyle1006925219.h"
#include "UnityEngine_UnityEngine_TextAnchor551935663.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_GUILayoutOption3151226183.h"
#include "UnityEngine_UnityEngine_LogType3529269451.h"
#include "UnityEngine_UnityEngine_Application450040189MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application_LogCallback3235662729.h"
#include "UnityEngine_UnityEngine_Application_LogCallback3235662729MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Input1593691127MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Time1525492538MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimePlatform1574985880.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "UnityEngine_UnityEngine_RectOffset3394170884MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStyleState47287592MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect1525428817MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectOffset3394170884.h"
#include "UnityEngine_UnityEngine_GUIStyleState47287592.h"
#include "UnityEngine_UnityEngine_Rect1525428817.h"
#include "UnityEngine_UnityEngine_GUI_WindowFunction999919624MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI_WindowFunction999919624.h"
#include "UnityEngine_UnityEngine_FontStyle1975910095.h"
#include "P31RestKit_Prime31_P31DeserializeableFieldAttribut1330016988MethodDeclarations.h"
#include "P31RestKit_Prime31_ThreadingCallbackHelper3423238746MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1234482916MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1234482916.h"
#include "System_Core_System_Action437523947.h"
#include "mscorlib_System_Threading_Monitor2071304733MethodDeclarations.h"
#include "System_Core_System_Action437523947MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Behaviour3120504042MethodDeclarations.h"

// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m3048268896_gshared (GameObject_t4012695102 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisIl2CppObject_m3048268896(__this, method) ((  Il2CppObject * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3048268896_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Prime31.LifecycleHelper>()
#define GameObject_AddComponent_TisLifecycleHelper_t3719100935_m1857424235(__this, method) ((  LifecycleHelper_t3719100935 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3048268896_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Prime31.ThreadingCallbackHelper>()
#define GameObject_AddComponent_TisThreadingCallbackHelper_t3423238746_m2759049784(__this, method) ((  ThreadingCallbackHelper_t3423238746 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3048268896_gshared)(__this, method)
// !!0 System.Threading.Interlocked::CompareExchange<System.Object>(!!0&,!!0,!!0)
extern "C"  Il2CppObject * Interlocked_CompareExchange_TisIl2CppObject_m2453340365_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject ** p0, Il2CppObject * p1, Il2CppObject * p2, const MethodInfo* method);
#define Interlocked_CompareExchange_TisIl2CppObject_m2453340365(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject **, Il2CppObject *, Il2CppObject *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2453340365_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 System.Threading.Interlocked::CompareExchange<System.Action`1<System.Boolean>>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisAction_1_t359458046_m210949228(__this /* static, unused */, p0, p1, p2, method) ((  Action_1_t359458046 * (*) (Il2CppObject * /* static, unused */, Action_1_t359458046 **, Action_1_t359458046 *, Action_1_t359458046 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2453340365_gshared)(__this /* static, unused */, p0, p1, p2, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Prime31.AbstractManager::.ctor()
extern "C"  void AbstractManager__ctor_m3282926219 (AbstractManager_t2874768250 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m350695606(__this, /*hidden argument*/NULL);
		return;
	}
}
// Prime31.LifecycleHelper Prime31.AbstractManager::get_coroutineSurrogate()
extern Il2CppClass* AbstractManager_t2874768250_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisLifecycleHelper_t3719100935_m1857424235_MethodInfo_var;
extern const uint32_t AbstractManager_get_coroutineSurrogate_m1448002710_MetadataUsageId;
extern "C"  LifecycleHelper_t3719100935 * AbstractManager_get_coroutineSurrogate_m1448002710 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AbstractManager_get_coroutineSurrogate_m1448002710_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t4012695102 * V_0 = NULL;
	{
		LifecycleHelper_t3719100935 * L_0 = ((AbstractManager_t2874768250_StaticFields*)AbstractManager_t2874768250_il2cpp_TypeInfo_var->static_fields)->get__prime31LifecycleHelperRef_2();
		bool L_1 = Object_op_Equality_m1690293457(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		GameObject_t4012695102 * L_2 = AbstractManager_getPrime31ManagerGameObject_m590714625(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t4012695102 * L_3 = V_0;
		NullCheck(L_3);
		LifecycleHelper_t3719100935 * L_4 = GameObject_AddComponent_TisLifecycleHelper_t3719100935_m1857424235(L_3, /*hidden argument*/GameObject_AddComponent_TisLifecycleHelper_t3719100935_m1857424235_MethodInfo_var);
		((AbstractManager_t2874768250_StaticFields*)AbstractManager_t2874768250_il2cpp_TypeInfo_var->static_fields)->set__prime31LifecycleHelperRef_2(L_4);
	}

IL_0021:
	{
		LifecycleHelper_t3719100935 * L_5 = ((AbstractManager_t2874768250_StaticFields*)AbstractManager_t2874768250_il2cpp_TypeInfo_var->static_fields)->get__prime31LifecycleHelperRef_2();
		return L_5;
	}
}
// Prime31.LifecycleHelper Prime31.AbstractManager::get_lifecycleHelper()
extern "C"  LifecycleHelper_t3719100935 * AbstractManager_get_lifecycleHelper_m3734877732 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		LifecycleHelper_t3719100935 * L_0 = AbstractManager_get_coroutineSurrogate_m1448002710(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// Prime31.ThreadingCallbackHelper Prime31.AbstractManager::getThreadingCallbackHelper()
extern Il2CppClass* AbstractManager_t2874768250_il2cpp_TypeInfo_var;
extern const uint32_t AbstractManager_getThreadingCallbackHelper_m23183125_MetadataUsageId;
extern "C"  ThreadingCallbackHelper_t3423238746 * AbstractManager_getThreadingCallbackHelper_m23183125 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AbstractManager_getThreadingCallbackHelper_m23183125_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ThreadingCallbackHelper_t3423238746 * L_0 = ((AbstractManager_t2874768250_StaticFields*)AbstractManager_t2874768250_il2cpp_TypeInfo_var->static_fields)->get__threadingCallbackHelper_3();
		return L_0;
	}
}
// System.Void Prime31.AbstractManager::createThreadingCallbackHelper()
extern const Il2CppType* ThreadingCallbackHelper_t3423238746_0_0_0_var;
extern Il2CppClass* AbstractManager_t2874768250_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ThreadingCallbackHelper_t3423238746_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisThreadingCallbackHelper_t3423238746_m2759049784_MethodInfo_var;
extern const uint32_t AbstractManager_createThreadingCallbackHelper_m461813624_MetadataUsageId;
extern "C"  void AbstractManager_createThreadingCallbackHelper_m461813624 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AbstractManager_createThreadingCallbackHelper_m461813624_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t4012695102 * V_0 = NULL;
	{
		ThreadingCallbackHelper_t3423238746 * L_0 = ((AbstractManager_t2874768250_StaticFields*)AbstractManager_t2874768250_il2cpp_TypeInfo_var->static_fields)->get__threadingCallbackHelper_3();
		bool L_1 = Object_op_Inequality_m1834679180(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(ThreadingCallbackHelper_t3423238746_0_0_0_var), /*hidden argument*/NULL);
		Object_t3878351788 * L_3 = Object_FindObjectOfType_m709000164(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		((AbstractManager_t2874768250_StaticFields*)AbstractManager_t2874768250_il2cpp_TypeInfo_var->static_fields)->set__threadingCallbackHelper_3(((ThreadingCallbackHelper_t3423238746 *)IsInstClass(L_3, ThreadingCallbackHelper_t3423238746_il2cpp_TypeInfo_var)));
		ThreadingCallbackHelper_t3423238746 * L_4 = ((AbstractManager_t2874768250_StaticFields*)AbstractManager_t2874768250_il2cpp_TypeInfo_var->static_fields)->get__threadingCallbackHelper_3();
		bool L_5 = Object_op_Inequality_m1834679180(NULL /*static, unused*/, L_4, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003b;
		}
	}
	{
		return;
	}

IL_003b:
	{
		GameObject_t4012695102 * L_6 = AbstractManager_getPrime31ManagerGameObject_m590714625(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_6;
		GameObject_t4012695102 * L_7 = V_0;
		NullCheck(L_7);
		ThreadingCallbackHelper_t3423238746 * L_8 = GameObject_AddComponent_TisThreadingCallbackHelper_t3423238746_m2759049784(L_7, /*hidden argument*/GameObject_AddComponent_TisThreadingCallbackHelper_t3423238746_m2759049784_MethodInfo_var);
		((AbstractManager_t2874768250_StaticFields*)AbstractManager_t2874768250_il2cpp_TypeInfo_var->static_fields)->set__threadingCallbackHelper_3(L_8);
		return;
	}
}
// UnityEngine.GameObject Prime31.AbstractManager::getPrime31ManagerGameObject()
extern Il2CppClass* AbstractManager_t2874768250_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t4012695102_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2455666307;
extern const uint32_t AbstractManager_getPrime31ManagerGameObject_m590714625_MetadataUsageId;
extern "C"  GameObject_t4012695102 * AbstractManager_getPrime31ManagerGameObject_m590714625 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AbstractManager_getPrime31ManagerGameObject_m590714625_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t4012695102 * L_0 = ((AbstractManager_t2874768250_StaticFields*)AbstractManager_t2874768250_il2cpp_TypeInfo_var->static_fields)->get__prime31GameObject_4();
		bool L_1 = Object_op_Inequality_m1834679180(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		GameObject_t4012695102 * L_2 = ((AbstractManager_t2874768250_StaticFields*)AbstractManager_t2874768250_il2cpp_TypeInfo_var->static_fields)->get__prime31GameObject_4();
		return L_2;
	}

IL_0016:
	{
		GameObject_t4012695102 * L_3 = GameObject_Find_m1853568733(NULL /*static, unused*/, _stringLiteral2455666307, /*hidden argument*/NULL);
		((AbstractManager_t2874768250_StaticFields*)AbstractManager_t2874768250_il2cpp_TypeInfo_var->static_fields)->set__prime31GameObject_4(L_3);
		GameObject_t4012695102 * L_4 = ((AbstractManager_t2874768250_StaticFields*)AbstractManager_t2874768250_il2cpp_TypeInfo_var->static_fields)->get__prime31GameObject_4();
		bool L_5 = Object_op_Equality_m1690293457(NULL /*static, unused*/, L_4, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_004e;
		}
	}
	{
		GameObject_t4012695102 * L_6 = (GameObject_t4012695102 *)il2cpp_codegen_object_new(GameObject_t4012695102_il2cpp_TypeInfo_var);
		GameObject__ctor_m1336994365(L_6, _stringLiteral2455666307, /*hidden argument*/NULL);
		((AbstractManager_t2874768250_StaticFields*)AbstractManager_t2874768250_il2cpp_TypeInfo_var->static_fields)->set__prime31GameObject_4(L_6);
		GameObject_t4012695102 * L_7 = ((AbstractManager_t2874768250_StaticFields*)AbstractManager_t2874768250_il2cpp_TypeInfo_var->static_fields)->get__prime31GameObject_4();
		Object_DontDestroyOnLoad_m2587754829(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
	}

IL_004e:
	{
		GameObject_t4012695102 * L_8 = ((AbstractManager_t2874768250_StaticFields*)AbstractManager_t2874768250_il2cpp_TypeInfo_var->static_fields)->get__prime31GameObject_4();
		return L_8;
	}
}
// System.Void Prime31.AbstractManager::initialize(System.Type)
extern Il2CppClass* MonoBehaviour_t3012272455_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t4012695102_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityException_t3148635335_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3597132062;
extern Il2CppCodeGenString* _stringLiteral2285966575;
extern Il2CppCodeGenString* _stringLiteral3127078854;
extern const uint32_t AbstractManager_initialize_m1689301264_MetadataUsageId;
extern "C"  void AbstractManager_initialize_m1689301264 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AbstractManager_initialize_m1689301264_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	MonoBehaviour_t3012272455 * V_0 = NULL;
	GameObject_t4012695102 * V_1 = NULL;
	GameObject_t4012695102 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Type_t * L_0 = ___type0;
			Object_t3878351788 * L_1 = Object_FindObjectOfType_m709000164(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
			V_0 = ((MonoBehaviour_t3012272455 *)IsInstClass(L_1, MonoBehaviour_t3012272455_il2cpp_TypeInfo_var));
			MonoBehaviour_t3012272455 * L_2 = V_0;
			bool L_3 = Object_op_Inequality_m1834679180(NULL /*static, unused*/, L_2, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
			if (!L_3)
			{
				goto IL_001d;
			}
		}

IL_0018:
		{
			goto IL_0089;
		}

IL_001d:
		{
			GameObject_t4012695102 * L_4 = AbstractManager_getPrime31ManagerGameObject_m590714625(NULL /*static, unused*/, /*hidden argument*/NULL);
			V_1 = L_4;
			Type_t * L_5 = ___type0;
			NullCheck(L_5);
			String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Reflection.MemberInfo::get_Name() */, L_5);
			GameObject_t4012695102 * L_7 = (GameObject_t4012695102 *)il2cpp_codegen_object_new(GameObject_t4012695102_il2cpp_TypeInfo_var);
			GameObject__ctor_m1336994365(L_7, L_6, /*hidden argument*/NULL);
			V_2 = L_7;
			GameObject_t4012695102 * L_8 = V_2;
			Type_t * L_9 = ___type0;
			NullCheck(L_8);
			GameObject_AddComponent_m3062249957(L_8, L_9, /*hidden argument*/NULL);
			GameObject_t4012695102 * L_10 = V_2;
			NullCheck(L_10);
			Transform_t284553113 * L_11 = GameObject_get_transform_m1258834620(L_10, /*hidden argument*/NULL);
			GameObject_t4012695102 * L_12 = V_1;
			NullCheck(L_12);
			Transform_t284553113 * L_13 = GameObject_get_transform_m1258834620(L_12, /*hidden argument*/NULL);
			NullCheck(L_11);
			Transform_set_parent_m120112962(L_11, L_13, /*hidden argument*/NULL);
			GameObject_t4012695102 * L_14 = V_2;
			Object_DontDestroyOnLoad_m2587754829(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
			goto IL_0089;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (UnityException_t3148635335_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0053;
		throw e;
	}

CATCH_0053:
	{ // begin catch(UnityEngine.UnityException)
		ObjectU5BU5D_t11523773* L_15 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 0);
		ArrayElementTypeCheck (L_15, _stringLiteral3597132062);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral3597132062);
		ObjectU5BU5D_t11523773* L_16 = L_15;
		Type_t * L_17 = ___type0;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 1);
		ArrayElementTypeCheck (L_16, L_17);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_17);
		ObjectU5BU5D_t11523773* L_18 = L_16;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 2);
		ArrayElementTypeCheck (L_18, _stringLiteral2285966575);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral2285966575);
		ObjectU5BU5D_t11523773* L_19 = L_18;
		Type_t * L_20 = ___type0;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 3);
		ArrayElementTypeCheck (L_19, L_20);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_20);
		ObjectU5BU5D_t11523773* L_21 = L_19;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 4);
		ArrayElementTypeCheck (L_21, _stringLiteral3127078854);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral3127078854);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Concat_m3016520001(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogWarning_m539478453(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		goto IL_0089;
	} // end catch (depth: 1)

IL_0089:
	{
		return;
	}
}
// System.Void Prime31.AbstractManager::Awake()
extern "C"  void AbstractManager_Awake_m3520531438 (AbstractManager_t2874768250 * __this, const MethodInfo* method)
{
	{
		GameObject_t4012695102 * L_0 = Component_get_gameObject_m2112202034(__this, /*hidden argument*/NULL);
		Type_t * L_1 = Object_GetType_m2022236990(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Reflection.MemberInfo::get_Name() */, L_1);
		NullCheck(L_0);
		Object_set_name_m2334706817(L_0, L_2, /*hidden argument*/NULL);
		Object_DontDestroyOnLoad_m2587754829(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.Object> Prime31.DeserializationExtensions::toDictionary(System.Object)
extern const Il2CppType* P31DeserializeableFieldAttribute_t1330016988_0_0_0_var;
extern Il2CppClass* Dictionary_2_t2474804324_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* P31DeserializeableFieldAttribute_t1330016988_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_t287189635_il2cpp_TypeInfo_var;
extern Il2CppClass* ArrayList_t2121638921_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2887377984_MethodInfo_var;
extern const uint32_t DeserializationExtensions_toDictionary_m3409470010_MetadataUsageId;
extern "C"  Dictionary_2_t2474804324 * DeserializationExtensions_toDictionary_m3409470010 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___self0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DeserializationExtensions_toDictionary_m3409470010_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t2474804324 * V_0 = NULL;
	FieldInfo_t * V_1 = NULL;
	FieldInfoU5BU5D_t1144794227* V_2 = NULL;
	int32_t V_3 = 0;
	Il2CppObject * V_4 = NULL;
	ObjectU5BU5D_t11523773* V_5 = NULL;
	int32_t V_6 = 0;
	P31DeserializeableFieldAttribute_t1330016988 * V_7 = NULL;
	Il2CppObject * V_8 = NULL;
	ArrayList_t2121638921 * V_9 = NULL;
	Il2CppObject * V_10 = NULL;
	Il2CppObject * V_11 = NULL;
	Il2CppObject * V_12 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t2474804324 * L_0 = (Dictionary_2_t2474804324 *)il2cpp_codegen_object_new(Dictionary_2_t2474804324_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2887377984(L_0, /*hidden argument*/Dictionary_2__ctor_m2887377984_MethodInfo_var);
		V_0 = L_0;
		Il2CppObject * L_1 = ___self0;
		NullCheck(L_1);
		Type_t * L_2 = Object_GetType_m2022236990(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		FieldInfoU5BU5D_t1144794227* L_3 = VirtFuncInvoker1< FieldInfoU5BU5D_t1144794227*, int32_t >::Invoke(57 /* System.Reflection.FieldInfo[] System.Type::GetFields(System.Reflection.BindingFlags) */, L_2, ((int32_t)52));
		V_2 = L_3;
		V_3 = 0;
		goto IL_0120;
	}

IL_001b:
	{
		FieldInfoU5BU5D_t1144794227* L_4 = V_2;
		int32_t L_5 = V_3;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		V_1 = ((L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6)));
		FieldInfo_t * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(P31DeserializeableFieldAttribute_t1330016988_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_7);
		ObjectU5BU5D_t11523773* L_9 = VirtFuncInvoker2< ObjectU5BU5D_t11523773*, Type_t *, bool >::Invoke(14 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_7, L_8, (bool)1);
		V_5 = L_9;
		V_6 = 0;
		goto IL_0111;
	}

IL_003a:
	{
		ObjectU5BU5D_t11523773* L_10 = V_5;
		int32_t L_11 = V_6;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		V_4 = ((L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12)));
		Il2CppObject * L_13 = V_4;
		V_7 = ((P31DeserializeableFieldAttribute_t1330016988 *)IsInstClass(L_13, P31DeserializeableFieldAttribute_t1330016988_il2cpp_TypeInfo_var));
		P31DeserializeableFieldAttribute_t1330016988 * L_14 = V_7;
		NullCheck(L_14);
		bool L_15 = L_14->get_isCollection_1();
		if (!L_15)
		{
			goto IL_00cd;
		}
	}
	{
		FieldInfo_t * L_16 = V_1;
		Il2CppObject * L_17 = ___self0;
		NullCheck(L_16);
		Il2CppObject * L_18 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(18 /* System.Object System.Reflection.FieldInfo::GetValue(System.Object) */, L_16, L_17);
		V_8 = ((Il2CppObject *)IsInst(L_18, IEnumerable_t287189635_il2cpp_TypeInfo_var));
		ArrayList_t2121638921 * L_19 = (ArrayList_t2121638921 *)il2cpp_codegen_object_new(ArrayList_t2121638921_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1878432947(L_19, /*hidden argument*/NULL);
		V_9 = L_19;
		Il2CppObject * L_20 = V_8;
		NullCheck(L_20);
		Il2CppObject * L_21 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t287189635_il2cpp_TypeInfo_var, L_20);
		V_11 = L_21;
	}

IL_0074:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0091;
		}

IL_0079:
		{
			Il2CppObject * L_22 = V_11;
			NullCheck(L_22);
			Il2CppObject * L_23 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_22);
			V_10 = L_23;
			ArrayList_t2121638921 * L_24 = V_9;
			Il2CppObject * L_25 = V_10;
			Dictionary_2_t2474804324 * L_26 = DeserializationExtensions_toDictionary_m3409470010(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
			NullCheck(L_24);
			VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_24, L_26);
		}

IL_0091:
		{
			Il2CppObject * L_27 = V_11;
			NullCheck(L_27);
			bool L_28 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_27);
			if (L_28)
			{
				goto IL_0079;
			}
		}

IL_009d:
		{
			IL2CPP_LEAVE(0xB9, FINALLY_00a2);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00a2;
	}

FINALLY_00a2:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_29 = V_11;
			Il2CppObject * L_30 = ((Il2CppObject *)IsInst(L_29, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			V_12 = L_30;
			if (!L_30)
			{
				goto IL_00b8;
			}
		}

IL_00b1:
		{
			Il2CppObject * L_31 = V_12;
			NullCheck(L_31);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_31);
		}

IL_00b8:
		{
			IL2CPP_END_FINALLY(162)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(162)
	{
		IL2CPP_JUMP_TBL(0xB9, IL_00b9)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00b9:
	{
		Dictionary_2_t2474804324 * L_32 = V_0;
		P31DeserializeableFieldAttribute_t1330016988 * L_33 = V_7;
		NullCheck(L_33);
		String_t* L_34 = L_33->get_key_0();
		ArrayList_t2121638921 * L_35 = V_9;
		NullCheck(L_32);
		VirtActionInvoker2< String_t*, Il2CppObject * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::set_Item(!0,!1) */, L_32, L_34, L_35);
		goto IL_010b;
	}

IL_00cd:
	{
		P31DeserializeableFieldAttribute_t1330016988 * L_36 = V_7;
		NullCheck(L_36);
		Type_t * L_37 = L_36->get_type_2();
		if (!L_37)
		{
			goto IL_00f7;
		}
	}
	{
		Dictionary_2_t2474804324 * L_38 = V_0;
		P31DeserializeableFieldAttribute_t1330016988 * L_39 = V_7;
		NullCheck(L_39);
		String_t* L_40 = L_39->get_key_0();
		FieldInfo_t * L_41 = V_1;
		Il2CppObject * L_42 = ___self0;
		NullCheck(L_41);
		Il2CppObject * L_43 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(18 /* System.Object System.Reflection.FieldInfo::GetValue(System.Object) */, L_41, L_42);
		Dictionary_2_t2474804324 * L_44 = DeserializationExtensions_toDictionary_m3409470010(NULL /*static, unused*/, L_43, /*hidden argument*/NULL);
		NullCheck(L_38);
		VirtActionInvoker2< String_t*, Il2CppObject * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::set_Item(!0,!1) */, L_38, L_40, L_44);
		goto IL_010b;
	}

IL_00f7:
	{
		Dictionary_2_t2474804324 * L_45 = V_0;
		P31DeserializeableFieldAttribute_t1330016988 * L_46 = V_7;
		NullCheck(L_46);
		String_t* L_47 = L_46->get_key_0();
		FieldInfo_t * L_48 = V_1;
		Il2CppObject * L_49 = ___self0;
		NullCheck(L_48);
		Il2CppObject * L_50 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(18 /* System.Object System.Reflection.FieldInfo::GetValue(System.Object) */, L_48, L_49);
		NullCheck(L_45);
		VirtActionInvoker2< String_t*, Il2CppObject * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::set_Item(!0,!1) */, L_45, L_47, L_50);
	}

IL_010b:
	{
		int32_t L_51 = V_6;
		V_6 = ((int32_t)((int32_t)L_51+(int32_t)1));
	}

IL_0111:
	{
		int32_t L_52 = V_6;
		ObjectU5BU5D_t11523773* L_53 = V_5;
		NullCheck(L_53);
		if ((((int32_t)L_52) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_53)->max_length)))))))
		{
			goto IL_003a;
		}
	}
	{
		int32_t L_54 = V_3;
		V_3 = ((int32_t)((int32_t)L_54+(int32_t)1));
	}

IL_0120:
	{
		int32_t L_55 = V_3;
		FieldInfoU5BU5D_t1144794227* L_56 = V_2;
		NullCheck(L_56);
		if ((((int32_t)L_55) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_56)->max_length)))))))
		{
			goto IL_001b;
		}
	}
	{
		Dictionary_2_t2474804324 * L_57 = V_0;
		return L_57;
	}
}
// System.Void Prime31.LifecycleHelper::.ctor()
extern "C"  void LifecycleHelper__ctor_m819661470 (LifecycleHelper_t3719100935 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m350695606(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Prime31.LifecycleHelper::add_onApplicationPausedEvent(System.Action`1<System.Boolean>)
extern Il2CppClass* Action_1_t359458046_il2cpp_TypeInfo_var;
extern const uint32_t LifecycleHelper_add_onApplicationPausedEvent_m1624423188_MetadataUsageId;
extern "C"  void LifecycleHelper_add_onApplicationPausedEvent_m1624423188 (LifecycleHelper_t3719100935 * __this, Action_1_t359458046 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LifecycleHelper_add_onApplicationPausedEvent_m1624423188_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t359458046 * V_0 = NULL;
	Action_1_t359458046 * V_1 = NULL;
	{
		Action_1_t359458046 * L_0 = __this->get_onApplicationPausedEvent_2();
		V_0 = L_0;
	}

IL_0007:
	{
		Action_1_t359458046 * L_1 = V_0;
		V_1 = L_1;
		Action_1_t359458046 ** L_2 = __this->get_address_of_onApplicationPausedEvent_2();
		Action_1_t359458046 * L_3 = V_1;
		Action_1_t359458046 * L_4 = ___value0;
		Delegate_t3660574010 * L_5 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Action_1_t359458046 * L_6 = V_0;
		Action_1_t359458046 * L_7 = InterlockedCompareExchangeImpl<Action_1_t359458046 *>(L_2, ((Action_1_t359458046 *)CastclassSealed(L_5, Action_1_t359458046_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		Action_1_t359458046 * L_8 = V_0;
		Action_1_t359458046 * L_9 = V_1;
		if ((!(((Il2CppObject*)(Action_1_t359458046 *)L_8) == ((Il2CppObject*)(Action_1_t359458046 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void Prime31.LifecycleHelper::remove_onApplicationPausedEvent(System.Action`1<System.Boolean>)
extern Il2CppClass* Action_1_t359458046_il2cpp_TypeInfo_var;
extern const uint32_t LifecycleHelper_remove_onApplicationPausedEvent_m3150970401_MetadataUsageId;
extern "C"  void LifecycleHelper_remove_onApplicationPausedEvent_m3150970401 (LifecycleHelper_t3719100935 * __this, Action_1_t359458046 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LifecycleHelper_remove_onApplicationPausedEvent_m3150970401_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t359458046 * V_0 = NULL;
	Action_1_t359458046 * V_1 = NULL;
	{
		Action_1_t359458046 * L_0 = __this->get_onApplicationPausedEvent_2();
		V_0 = L_0;
	}

IL_0007:
	{
		Action_1_t359458046 * L_1 = V_0;
		V_1 = L_1;
		Action_1_t359458046 ** L_2 = __this->get_address_of_onApplicationPausedEvent_2();
		Action_1_t359458046 * L_3 = V_1;
		Action_1_t359458046 * L_4 = ___value0;
		Delegate_t3660574010 * L_5 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Action_1_t359458046 * L_6 = V_0;
		Action_1_t359458046 * L_7 = InterlockedCompareExchangeImpl<Action_1_t359458046 *>(L_2, ((Action_1_t359458046 *)CastclassSealed(L_5, Action_1_t359458046_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		Action_1_t359458046 * L_8 = V_0;
		Action_1_t359458046 * L_9 = V_1;
		if ((!(((Il2CppObject*)(Action_1_t359458046 *)L_8) == ((Il2CppObject*)(Action_1_t359458046 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void Prime31.LifecycleHelper::OnApplicationPause(System.Boolean)
extern const MethodInfo* Action_1_Invoke_m510242183_MethodInfo_var;
extern const uint32_t LifecycleHelper_OnApplicationPause_m3832293794_MetadataUsageId;
extern "C"  void LifecycleHelper_OnApplicationPause_m3832293794 (LifecycleHelper_t3719100935 * __this, bool ___paused0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LifecycleHelper_OnApplicationPause_m3832293794_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t359458046 * L_0 = __this->get_onApplicationPausedEvent_2();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Action_1_t359458046 * L_1 = __this->get_onApplicationPausedEvent_2();
		bool L_2 = ___paused0;
		NullCheck(L_1);
		Action_1_Invoke_m510242183(L_1, L_2, /*hidden argument*/Action_1_Invoke_m510242183_MethodInfo_var);
	}

IL_0017:
	{
		return;
	}
}
// System.Void Prime31.MonoBehaviourGUI::.ctor()
extern Il2CppClass* Dictionary_2_t1848703245_il2cpp_TypeInfo_var;
extern Il2CppClass* StringBuilder_t3822575854_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m4132039153_MethodInfo_var;
extern const uint32_t MonoBehaviourGUI__ctor_m3698201455_MetadataUsageId;
extern "C"  void MonoBehaviourGUI__ctor_m3698201455 (MonoBehaviourGUI_t2892502096 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoBehaviourGUI__ctor_m3698201455_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t1848703245 * L_0 = (Dictionary_2_t1848703245 *)il2cpp_codegen_object_new(Dictionary_2_t1848703245_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m4132039153(L_0, /*hidden argument*/Dictionary_2__ctor_m4132039153_MethodInfo_var);
		__this->set__toggleButtons_4(L_0);
		StringBuilder_t3822575854 * L_1 = (StringBuilder_t3822575854 *)il2cpp_codegen_object_new(StringBuilder_t3822575854_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m135953004(L_1, /*hidden argument*/NULL);
		__this->set__logBuilder_5(L_1);
		__this->set__doubleClickDelay_9((0.15f));
		__this->set__lastTwoFingerTouchTime_11((-1.0f));
		MonoBehaviour__ctor_m350695606(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Texture2D Prime31.MonoBehaviourGUI::get_normalBackground()
extern Il2CppClass* Texture2D_t2509538522_il2cpp_TypeInfo_var;
extern const uint32_t MonoBehaviourGUI_get_normalBackground_m1355396176_MetadataUsageId;
extern "C"  Texture2D_t2509538522 * MonoBehaviourGUI_get_normalBackground_m1355396176 (MonoBehaviourGUI_t2892502096 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoBehaviourGUI_get_normalBackground_m1355396176_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Texture2D_t2509538522 * L_0 = __this->get__normalBackground_13();
		bool L_1 = Object_op_Implicit_m3627549526(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_003a;
		}
	}
	{
		Texture2D_t2509538522 * L_2 = (Texture2D_t2509538522 *)il2cpp_codegen_object_new(Texture2D_t2509538522_il2cpp_TypeInfo_var);
		Texture2D__ctor_m1783151287(L_2, 1, 1, /*hidden argument*/NULL);
		__this->set__normalBackground_13(L_2);
		Texture2D_t2509538522 * L_3 = __this->get__normalBackground_13();
		Color_t1588175760  L_4 = Color_get_gray_m2259102320(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		Texture2D_SetPixel_m4246572557(L_3, 0, 0, L_4, /*hidden argument*/NULL);
		Texture2D_t2509538522 * L_5 = __this->get__normalBackground_13();
		NullCheck(L_5);
		Texture2D_Apply_m544531547(L_5, /*hidden argument*/NULL);
	}

IL_003a:
	{
		Texture2D_t2509538522 * L_6 = __this->get__normalBackground_13();
		return L_6;
	}
}
// UnityEngine.Texture2D Prime31.MonoBehaviourGUI::get_bottomButtonBackground()
extern Il2CppClass* Texture2D_t2509538522_il2cpp_TypeInfo_var;
extern const uint32_t MonoBehaviourGUI_get_bottomButtonBackground_m261950502_MetadataUsageId;
extern "C"  Texture2D_t2509538522 * MonoBehaviourGUI_get_bottomButtonBackground_m261950502 (MonoBehaviourGUI_t2892502096 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoBehaviourGUI_get_bottomButtonBackground_m261950502_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Texture2D_t2509538522 * L_0 = __this->get__bottomButtonBackground_14();
		bool L_1 = Object_op_Implicit_m3627549526(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0049;
		}
	}
	{
		Texture2D_t2509538522 * L_2 = (Texture2D_t2509538522 *)il2cpp_codegen_object_new(Texture2D_t2509538522_il2cpp_TypeInfo_var);
		Texture2D__ctor_m1783151287(L_2, 1, 1, /*hidden argument*/NULL);
		__this->set__bottomButtonBackground_14(L_2);
		Texture2D_t2509538522 * L_3 = __this->get__bottomButtonBackground_14();
		Color_t1588175760  L_4 = Color_get_gray_m2259102320(NULL /*static, unused*/, /*hidden argument*/NULL);
		Color_t1588175760  L_5 = Color_get_black_m997778164(NULL /*static, unused*/, /*hidden argument*/NULL);
		Color_t1588175760  L_6 = Color_Lerp_m3478646704(NULL /*static, unused*/, L_4, L_5, (0.5f), /*hidden argument*/NULL);
		NullCheck(L_3);
		Texture2D_SetPixel_m4246572557(L_3, 0, 0, L_6, /*hidden argument*/NULL);
		Texture2D_t2509538522 * L_7 = __this->get__bottomButtonBackground_14();
		NullCheck(L_7);
		Texture2D_Apply_m544531547(L_7, /*hidden argument*/NULL);
	}

IL_0049:
	{
		Texture2D_t2509538522 * L_8 = __this->get__bottomButtonBackground_14();
		return L_8;
	}
}
// UnityEngine.Texture2D Prime31.MonoBehaviourGUI::get_activeBackground()
extern Il2CppClass* Texture2D_t2509538522_il2cpp_TypeInfo_var;
extern const uint32_t MonoBehaviourGUI_get_activeBackground_m3938271215_MetadataUsageId;
extern "C"  Texture2D_t2509538522 * MonoBehaviourGUI_get_activeBackground_m3938271215 (MonoBehaviourGUI_t2892502096 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoBehaviourGUI_get_activeBackground_m3938271215_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Texture2D_t2509538522 * L_0 = __this->get__activeBackground_15();
		bool L_1 = Object_op_Implicit_m3627549526(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_003a;
		}
	}
	{
		Texture2D_t2509538522 * L_2 = (Texture2D_t2509538522 *)il2cpp_codegen_object_new(Texture2D_t2509538522_il2cpp_TypeInfo_var);
		Texture2D__ctor_m1783151287(L_2, 1, 1, /*hidden argument*/NULL);
		__this->set__activeBackground_15(L_2);
		Texture2D_t2509538522 * L_3 = __this->get__activeBackground_15();
		Color_t1588175760  L_4 = Color_get_yellow_m702153025(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		Texture2D_SetPixel_m4246572557(L_3, 0, 0, L_4, /*hidden argument*/NULL);
		Texture2D_t2509538522 * L_5 = __this->get__activeBackground_15();
		NullCheck(L_5);
		Texture2D_Apply_m544531547(L_5, /*hidden argument*/NULL);
	}

IL_003a:
	{
		Texture2D_t2509538522 * L_6 = __this->get__activeBackground_15();
		return L_6;
	}
}
// UnityEngine.Texture2D Prime31.MonoBehaviourGUI::get_toggleButtonBackground()
extern Il2CppClass* Texture2D_t2509538522_il2cpp_TypeInfo_var;
extern const uint32_t MonoBehaviourGUI_get_toggleButtonBackground_m720475407_MetadataUsageId;
extern "C"  Texture2D_t2509538522 * MonoBehaviourGUI_get_toggleButtonBackground_m720475407 (MonoBehaviourGUI_t2892502096 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoBehaviourGUI_get_toggleButtonBackground_m720475407_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Texture2D_t2509538522 * L_0 = __this->get__toggleButtonBackground_16();
		bool L_1 = Object_op_Implicit_m3627549526(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_003a;
		}
	}
	{
		Texture2D_t2509538522 * L_2 = (Texture2D_t2509538522 *)il2cpp_codegen_object_new(Texture2D_t2509538522_il2cpp_TypeInfo_var);
		Texture2D__ctor_m1783151287(L_2, 1, 1, /*hidden argument*/NULL);
		__this->set__toggleButtonBackground_16(L_2);
		Texture2D_t2509538522 * L_3 = __this->get__toggleButtonBackground_16();
		Color_t1588175760  L_4 = Color_get_black_m997778164(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		Texture2D_SetPixel_m4246572557(L_3, 0, 0, L_4, /*hidden argument*/NULL);
		Texture2D_t2509538522 * L_5 = __this->get__toggleButtonBackground_16();
		NullCheck(L_5);
		Texture2D_Apply_m544531547(L_5, /*hidden argument*/NULL);
	}

IL_003a:
	{
		Texture2D_t2509538522 * L_6 = __this->get__toggleButtonBackground_16();
		return L_6;
	}
}
// System.Boolean Prime31.MonoBehaviourGUI::isRetinaOrLargeScreen()
extern "C"  bool MonoBehaviourGUI_isRetinaOrLargeScreen_m14176428 (MonoBehaviourGUI_t2892502096 * __this, const MethodInfo* method)
{
	int32_t G_B4_0 = 0;
	{
		bool L_0 = __this->get__isWindowsPhone_12();
		if (L_0)
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_1 = Screen_get_width_m1424489183(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_1) >= ((int32_t)((int32_t)960))))
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_2 = Screen_get_height_m1713306064(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B4_0 = ((((int32_t)((((int32_t)L_2) < ((int32_t)((int32_t)960)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_002c;
	}

IL_002b:
	{
		G_B4_0 = 1;
	}

IL_002c:
	{
		return (bool)G_B4_0;
	}
}
// System.Boolean Prime31.MonoBehaviourGUI::isRetinaIpad()
extern "C"  bool MonoBehaviourGUI_isRetinaIpad_m1920871204 (MonoBehaviourGUI_t2892502096 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get__didRetinaIpadCheck_17();
		if (L_0)
		{
			goto IL_0037;
		}
	}
	{
		int32_t L_1 = Screen_get_height_m1713306064(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_1) >= ((int32_t)((int32_t)2048))))
		{
			goto IL_0029;
		}
	}
	{
		int32_t L_2 = Screen_get_width_m1424489183(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_2) < ((int32_t)((int32_t)2048))))
		{
			goto IL_0030;
		}
	}

IL_0029:
	{
		__this->set__isRetinaIpad_18((bool)1);
	}

IL_0030:
	{
		__this->set__didRetinaIpadCheck_17((bool)1);
	}

IL_0037:
	{
		bool L_3 = __this->get__isRetinaIpad_18();
		return L_3;
	}
}
// System.Int32 Prime31.MonoBehaviourGUI::buttonHeight()
extern "C"  int32_t MonoBehaviourGUI_buttonHeight_m3177878504 (MonoBehaviourGUI_t2892502096 * __this, const MethodInfo* method)
{
	{
		bool L_0 = MonoBehaviourGUI_isRetinaOrLargeScreen_m14176428(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		bool L_1 = MonoBehaviourGUI_isRetinaIpad_m1920871204(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		return ((int32_t)140);
	}

IL_001c:
	{
		return ((int32_t)70);
	}

IL_001f:
	{
		return ((int32_t)30);
	}
}
// System.Int32 Prime31.MonoBehaviourGUI::buttonFontSize()
extern "C"  int32_t MonoBehaviourGUI_buttonFontSize_m2180707697 (MonoBehaviourGUI_t2892502096 * __this, const MethodInfo* method)
{
	{
		bool L_0 = MonoBehaviourGUI_isRetinaOrLargeScreen_m14176428(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		bool L_1 = MonoBehaviourGUI_isRetinaIpad_m1920871204(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		return ((int32_t)40);
	}

IL_0019:
	{
		return ((int32_t)25);
	}

IL_001c:
	{
		return ((int32_t)15);
	}
}
// System.Void Prime31.MonoBehaviourGUI::paintWindow(System.Int32)
extern Il2CppClass* GUI_t1522956648_il2cpp_TypeInfo_var;
extern Il2CppClass* GUILayoutOptionU5BU5D_t1890718142_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2547606084;
extern const uint32_t MonoBehaviourGUI_paintWindow_m1070764780_MetadataUsageId;
extern "C"  void MonoBehaviourGUI_paintWindow_m1070764780 (MonoBehaviourGUI_t2892502096 * __this, int32_t ___id0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoBehaviourGUI_paintWindow_m1070764780_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		GUISkin_t2614611333 * L_0 = GUI_get_skin_m2455194711(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		GUIStyle_t1006925219 * L_1 = GUISkin_get_label_m2034963657(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GUIStyle_set_alignment_m811403327(L_1, 0, /*hidden argument*/NULL);
		GUISkin_t2614611333 * L_2 = GUI_get_skin_m2455194711(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		GUIStyle_t1006925219 * L_3 = GUISkin_get_label_m2034963657(L_2, /*hidden argument*/NULL);
		int32_t L_4 = MonoBehaviourGUI_buttonFontSize_m2180707697(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		GUIStyle_set_fontSize_m2928388686(L_3, L_4, /*hidden argument*/NULL);
		Vector2_t3525329788  L_5 = __this->get__logScrollPosition_7();
		Vector2_t3525329788  L_6 = GUILayout_BeginScrollView_m310099062(NULL /*static, unused*/, L_5, ((GUILayoutOptionU5BU5D_t1890718142*)SZArrayNew(GUILayoutOptionU5BU5D_t1890718142_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		__this->set__logScrollPosition_7(L_6);
		bool L_7 = GUILayout_Button_m3959463722(NULL /*static, unused*/, _stringLiteral2547606084, ((GUILayoutOptionU5BU5D_t1890718142*)SZArrayNew(GUILayoutOptionU5BU5D_t1890718142_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0069;
		}
	}
	{
		StringBuilder_t3822575854 * L_8 = __this->get__logBuilder_5();
		StringBuilder_t3822575854 * L_9 = __this->get__logBuilder_5();
		NullCheck(L_9);
		int32_t L_10 = StringBuilder_get_Length_m2443133099(L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		StringBuilder_Remove_m970775893(L_8, 0, L_10, /*hidden argument*/NULL);
	}

IL_0069:
	{
		StringBuilder_t3822575854 * L_11 = __this->get__logBuilder_5();
		NullCheck(L_11);
		String_t* L_12 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_11);
		GUILayout_Label_m1096561152(NULL /*static, unused*/, L_12, ((GUILayoutOptionU5BU5D_t1890718142*)SZArrayNew(GUILayoutOptionU5BU5D_t1890718142_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		GUILayout_EndScrollView_m1004795618(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Prime31.MonoBehaviourGUI::handleLog(System.String,System.String,UnityEngine.LogType)
extern Il2CppCodeGenString* _stringLiteral3714306;
extern const uint32_t MonoBehaviourGUI_handleLog_m986101754_MetadataUsageId;
extern "C"  void MonoBehaviourGUI_handleLog_m986101754 (MonoBehaviourGUI_t2892502096 * __this, String_t* ___logString0, String_t* ___stackTrace1, int32_t ___type2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoBehaviourGUI_handleLog_m986101754_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringBuilder_t3822575854 * L_0 = __this->get__logBuilder_5();
		String_t* L_1 = ___logString0;
		NullCheck(L_0);
		StringBuilder_AppendFormat_m3723191730(L_0, _stringLiteral3714306, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Prime31.MonoBehaviourGUI::OnDestroy()
extern "C"  void MonoBehaviourGUI_OnDestroy_m2535102696 (MonoBehaviourGUI_t2892502096 * __this, const MethodInfo* method)
{
	{
		Application_RegisterLogCallback_m1399364207(NULL /*static, unused*/, (LogCallback_t3235662729 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Prime31.MonoBehaviourGUI::Update()
extern Il2CppClass* LogCallback_t3235662729_il2cpp_TypeInfo_var;
extern Il2CppClass* RuntimePlatform_t1574985880_il2cpp_TypeInfo_var;
extern Il2CppClass* Input_t1593691127_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoBehaviourGUI_handleLog_m986101754_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral117887;
extern const uint32_t MonoBehaviourGUI_Update_m406990206_MetadataUsageId;
extern "C"  void MonoBehaviourGUI_Update_m406990206 (MonoBehaviourGUI_t2892502096 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoBehaviourGUI_Update_m406990206_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	float V_2 = 0.0f;
	{
		bool L_0 = __this->get__logRegistered_6();
		if (L_0)
		{
			goto IL_004b;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)MonoBehaviourGUI_handleLog_m986101754_MethodInfo_var);
		LogCallback_t3235662729 * L_2 = (LogCallback_t3235662729 *)il2cpp_codegen_object_new(LogCallback_t3235662729_il2cpp_TypeInfo_var);
		LogCallback__ctor_m1350407504(L_2, __this, L_1, /*hidden argument*/NULL);
		Application_RegisterLogCallback_m1399364207(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set__logRegistered_6((bool)1);
		int32_t L_3 = Application_get_platform_m2818272885(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_3;
		Il2CppObject * L_4 = Box(RuntimePlatform_t1574985880_il2cpp_TypeInfo_var, (&V_0));
		NullCheck(L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_4);
		NullCheck(L_5);
		String_t* L_6 = String_ToLower_m2421900555(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		bool L_7 = String_Contains_m3032019141(L_6, _stringLiteral117887, /*hidden argument*/NULL);
		__this->set__isWindowsPhone_12(L_7);
	}

IL_004b:
	{
		V_1 = (bool)0;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_8 = Input_GetMouseButtonDown_m3552475078(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0083;
		}
	}
	{
		float L_9 = Time_get_time_m3817560969(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_10 = __this->get__previousClickTime_10();
		V_2 = ((float)((float)L_9-(float)L_10));
		float L_11 = V_2;
		float L_12 = __this->get__doubleClickDelay_9();
		if ((!(((float)L_11) < ((float)L_12))))
		{
			goto IL_0078;
		}
	}
	{
		V_1 = (bool)1;
		goto IL_0083;
	}

IL_0078:
	{
		float L_13 = Time_get_time_m3817560969(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__previousClickTime_10(L_13);
	}

IL_0083:
	{
		bool L_14 = V_1;
		if (!L_14)
		{
			goto IL_0098;
		}
	}
	{
		bool L_15 = __this->get__isShowingLogConsole_8();
		__this->set__isShowingLogConsole_8((bool)((((int32_t)L_15) == ((int32_t)0))? 1 : 0));
	}

IL_0098:
	{
		return;
	}
}
// System.Void Prime31.MonoBehaviourGUI::beginColumn()
extern Il2CppClass* GUI_t1522956648_il2cpp_TypeInfo_var;
extern Il2CppClass* RectOffset_t3394170884_il2cpp_TypeInfo_var;
extern Il2CppClass* GUILayoutOptionU5BU5D_t1890718142_il2cpp_TypeInfo_var;
extern const uint32_t MonoBehaviourGUI_beginColumn_m1784932684_MetadataUsageId;
extern "C"  void MonoBehaviourGUI_beginColumn_m1784932684 (MonoBehaviourGUI_t2892502096 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoBehaviourGUI_beginColumn_m1784932684_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Screen_get_width_m1424489183(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__width_2((((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_0/(int32_t)2))-(int32_t)((int32_t)15)))))));
		int32_t L_1 = MonoBehaviourGUI_buttonHeight_m3177878504(__this, /*hidden argument*/NULL);
		__this->set__buttonHeight_3((((float)((float)L_1))));
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		GUISkin_t2614611333 * L_2 = GUI_get_skin_m2455194711(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		GUIStyle_t1006925219 * L_3 = GUISkin_get_button_m3805635679(L_2, /*hidden argument*/NULL);
		int32_t L_4 = MonoBehaviourGUI_buttonFontSize_m2180707697(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		GUIStyle_set_fontSize_m2928388686(L_3, L_4, /*hidden argument*/NULL);
		GUISkin_t2614611333 * L_5 = GUI_get_skin_m2455194711(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		GUIStyle_t1006925219 * L_6 = GUISkin_get_button_m3805635679(L_5, /*hidden argument*/NULL);
		RectOffset_t3394170884 * L_7 = (RectOffset_t3394170884 *)il2cpp_codegen_object_new(RectOffset_t3394170884_il2cpp_TypeInfo_var);
		RectOffset__ctor_m1133879495(L_7, 0, 0, ((int32_t)10), 0, /*hidden argument*/NULL);
		NullCheck(L_6);
		GUIStyle_set_margin_m1882009837(L_6, L_7, /*hidden argument*/NULL);
		GUISkin_t2614611333 * L_8 = GUI_get_skin_m2455194711(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		GUIStyle_t1006925219 * L_9 = GUISkin_get_button_m3805635679(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		GUIStyle_set_stretchWidth_m2350270469(L_9, (bool)1, /*hidden argument*/NULL);
		GUISkin_t2614611333 * L_10 = GUI_get_skin_m2455194711(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_10);
		GUIStyle_t1006925219 * L_11 = GUISkin_get_button_m3805635679(L_10, /*hidden argument*/NULL);
		float L_12 = __this->get__buttonHeight_3();
		NullCheck(L_11);
		GUIStyle_set_fixedHeight_m593584571(L_11, L_12, /*hidden argument*/NULL);
		GUISkin_t2614611333 * L_13 = GUI_get_skin_m2455194711(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_13);
		GUIStyle_t1006925219 * L_14 = GUISkin_get_button_m3805635679(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		GUIStyle_set_wordWrap_m3186601144(L_14, (bool)0, /*hidden argument*/NULL);
		GUISkin_t2614611333 * L_15 = GUI_get_skin_m2455194711(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_15);
		GUIStyle_t1006925219 * L_16 = GUISkin_get_button_m3805635679(L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		GUIStyleState_t47287592 * L_17 = GUIStyle_get_hover_m1723373270(L_16, /*hidden argument*/NULL);
		Texture2D_t2509538522 * L_18 = MonoBehaviourGUI_get_normalBackground_m1355396176(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		GUIStyleState_set_background_m2586262574(L_17, L_18, /*hidden argument*/NULL);
		GUISkin_t2614611333 * L_19 = GUI_get_skin_m2455194711(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_19);
		GUIStyle_t1006925219 * L_20 = GUISkin_get_button_m3805635679(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		GUIStyleState_t47287592 * L_21 = GUIStyle_get_normal_m3644321711(L_20, /*hidden argument*/NULL);
		Texture2D_t2509538522 * L_22 = MonoBehaviourGUI_get_normalBackground_m1355396176(__this, /*hidden argument*/NULL);
		NullCheck(L_21);
		GUIStyleState_set_background_m2586262574(L_21, L_22, /*hidden argument*/NULL);
		GUISkin_t2614611333 * L_23 = GUI_get_skin_m2455194711(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_23);
		GUIStyle_t1006925219 * L_24 = GUISkin_get_button_m3805635679(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		GUIStyleState_t47287592 * L_25 = GUIStyle_get_active_m456697358(L_24, /*hidden argument*/NULL);
		Texture2D_t2509538522 * L_26 = MonoBehaviourGUI_get_activeBackground_m3938271215(__this, /*hidden argument*/NULL);
		NullCheck(L_25);
		GUIStyleState_set_background_m2586262574(L_25, L_26, /*hidden argument*/NULL);
		GUISkin_t2614611333 * L_27 = GUI_get_skin_m2455194711(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_27);
		GUIStyle_t1006925219 * L_28 = GUISkin_get_button_m3805635679(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		GUIStyleState_t47287592 * L_29 = GUIStyle_get_active_m456697358(L_28, /*hidden argument*/NULL);
		Color_t1588175760  L_30 = Color_get_black_m997778164(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_29);
		GUIStyleState_set_textColor_m3911936846(L_29, L_30, /*hidden argument*/NULL);
		GUISkin_t2614611333 * L_31 = GUI_get_skin_m2455194711(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_31);
		GUIStyle_t1006925219 * L_32 = GUISkin_get_label_m2034963657(L_31, /*hidden argument*/NULL);
		NullCheck(L_32);
		GUIStyleState_t47287592 * L_33 = GUIStyle_get_normal_m3644321711(L_32, /*hidden argument*/NULL);
		Color_t1588175760  L_34 = Color_get_black_m997778164(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_33);
		GUIStyleState_set_textColor_m3911936846(L_33, L_34, /*hidden argument*/NULL);
		GUISkin_t2614611333 * L_35 = GUI_get_skin_m2455194711(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_35);
		GUIStyle_t1006925219 * L_36 = GUISkin_get_label_m2034963657(L_35, /*hidden argument*/NULL);
		int32_t L_37 = MonoBehaviourGUI_buttonFontSize_m2180707697(__this, /*hidden argument*/NULL);
		NullCheck(L_36);
		GUIStyle_set_fontSize_m2928388686(L_36, L_37, /*hidden argument*/NULL);
		bool L_38 = __this->get__isShowingLogConsole_8();
		if (!L_38)
		{
			goto IL_0144;
		}
	}
	{
		Rect_t1525428817  L_39;
		memset(&L_39, 0, sizeof(L_39));
		Rect__ctor_m2378035624(&L_39, (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		GUILayout_BeginArea_m3506130160(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		goto IL_0164;
	}

IL_0144:
	{
		float L_40 = __this->get__width_2();
		int32_t L_41 = Screen_get_height_m1713306064(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t1525428817  L_42;
		memset(&L_42, 0, sizeof(L_42));
		Rect__ctor_m2378035624(&L_42, (10.0f), (10.0f), L_40, (((float)((float)L_41))), /*hidden argument*/NULL);
		GUILayout_BeginArea_m3506130160(NULL /*static, unused*/, L_42, /*hidden argument*/NULL);
	}

IL_0164:
	{
		GUILayout_BeginVertical_m1605398489(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t1890718142*)SZArrayNew(GUILayoutOptionU5BU5D_t1890718142_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void Prime31.MonoBehaviourGUI::endColumn()
extern "C"  void MonoBehaviourGUI_endColumn_m1045521022 (MonoBehaviourGUI_t2892502096 * __this, const MethodInfo* method)
{
	{
		MonoBehaviourGUI_endColumn_m2544034677(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Prime31.MonoBehaviourGUI::endColumn(System.Boolean)
extern Il2CppClass* WindowFunction_t999919624_il2cpp_TypeInfo_var;
extern Il2CppClass* GUILayoutOptionU5BU5D_t1890718142_il2cpp_TypeInfo_var;
extern const MethodInfo* MonoBehaviourGUI_paintWindow_m1070764780_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3694414588;
extern const uint32_t MonoBehaviourGUI_endColumn_m2544034677_MetadataUsageId;
extern "C"  void MonoBehaviourGUI_endColumn_m2544034677 (MonoBehaviourGUI_t2892502096 * __this, bool ___hasSecondColumn0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoBehaviourGUI_endColumn_m2544034677_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUILayout_EndVertical_m2996106758(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndArea_m3394786525(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_0 = __this->get__isShowingLogConsole_8();
		if (!L_0)
		{
			goto IL_004e;
		}
	}
	{
		int32_t L_1 = Screen_get_width_m1424489183(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = Screen_get_height_m1713306064(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t1525428817  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Rect__ctor_m2378035624(&L_3, (0.0f), (0.0f), (((float)((float)L_1))), (((float)((float)L_2))), /*hidden argument*/NULL);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)MonoBehaviourGUI_paintWindow_m1070764780_MethodInfo_var);
		WindowFunction_t999919624 * L_5 = (WindowFunction_t999919624 *)il2cpp_codegen_object_new(WindowFunction_t999919624_il2cpp_TypeInfo_var);
		WindowFunction__ctor_m853430132(L_5, __this, L_4, /*hidden argument*/NULL);
		GUILayout_Window_m359582379(NULL /*static, unused*/, 1, L_3, L_5, _stringLiteral3694414588, ((GUILayoutOptionU5BU5D_t1890718142*)SZArrayNew(GUILayoutOptionU5BU5D_t1890718142_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
	}

IL_004e:
	{
		bool L_6 = ___hasSecondColumn0;
		if (!L_6)
		{
			goto IL_005a;
		}
	}
	{
		MonoBehaviourGUI_beginRightColumn_m4052146110(__this, /*hidden argument*/NULL);
	}

IL_005a:
	{
		return;
	}
}
// System.Void Prime31.MonoBehaviourGUI::beginRightColumn()
extern Il2CppClass* GUILayoutOptionU5BU5D_t1890718142_il2cpp_TypeInfo_var;
extern const uint32_t MonoBehaviourGUI_beginRightColumn_m4052146110_MetadataUsageId;
extern "C"  void MonoBehaviourGUI_beginRightColumn_m4052146110 (MonoBehaviourGUI_t2892502096 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoBehaviourGUI_beginRightColumn_m4052146110_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = __this->get__isShowingLogConsole_8();
		if (!L_0)
		{
			goto IL_002e;
		}
	}
	{
		Rect_t1525428817  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Rect__ctor_m2378035624(&L_1, (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		GUILayout_BeginArea_m3506130160(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		goto IL_005c;
	}

IL_002e:
	{
		int32_t L_2 = Screen_get_width_m1424489183(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_3 = __this->get__width_2();
		float L_4 = __this->get__width_2();
		int32_t L_5 = Screen_get_height_m1713306064(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t1525428817  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Rect__ctor_m2378035624(&L_6, ((float)((float)((float)((float)(((float)((float)L_2)))-(float)L_3))-(float)(10.0f))), (10.0f), L_4, (((float)((float)L_5))), /*hidden argument*/NULL);
		GUILayout_BeginArea_m3506130160(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
	}

IL_005c:
	{
		GUILayout_BeginVertical_m1605398489(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t1890718142*)SZArrayNew(GUILayoutOptionU5BU5D_t1890718142_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Prime31.MonoBehaviourGUI::button(System.String)
extern Il2CppClass* GUILayoutOptionU5BU5D_t1890718142_il2cpp_TypeInfo_var;
extern const uint32_t MonoBehaviourGUI_button_m3362767963_MetadataUsageId;
extern "C"  bool MonoBehaviourGUI_button_m3362767963 (MonoBehaviourGUI_t2892502096 * __this, String_t* ___text0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoBehaviourGUI_button_m3362767963_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___text0;
		bool L_1 = GUILayout_Button_m3959463722(NULL /*static, unused*/, L_0, ((GUILayoutOptionU5BU5D_t1890718142*)SZArrayNew(GUILayoutOptionU5BU5D_t1890718142_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean Prime31.MonoBehaviourGUI::bottomRightButton(System.String,System.Single)
extern Il2CppClass* GUI_t1522956648_il2cpp_TypeInfo_var;
extern const uint32_t MonoBehaviourGUI_bottomRightButton_m939646391_MetadataUsageId;
extern "C"  bool MonoBehaviourGUI_bottomRightButton_m939646391 (MonoBehaviourGUI_t2892502096 * __this, String_t* ___text0, float ___width1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoBehaviourGUI_bottomRightButton_m939646391_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		GUISkin_t2614611333 * L_0 = GUI_get_skin_m2455194711(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		GUIStyle_t1006925219 * L_1 = GUISkin_get_button_m3805635679(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GUIStyleState_t47287592 * L_2 = GUIStyle_get_hover_m1723373270(L_1, /*hidden argument*/NULL);
		Texture2D_t2509538522 * L_3 = MonoBehaviourGUI_get_bottomButtonBackground_m261950502(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		GUIStyleState_set_background_m2586262574(L_2, L_3, /*hidden argument*/NULL);
		GUISkin_t2614611333 * L_4 = GUI_get_skin_m2455194711(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		GUIStyle_t1006925219 * L_5 = GUISkin_get_button_m3805635679(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		GUIStyleState_t47287592 * L_6 = GUIStyle_get_normal_m3644321711(L_5, /*hidden argument*/NULL);
		Texture2D_t2509538522 * L_7 = MonoBehaviourGUI_get_bottomButtonBackground_m261950502(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		GUIStyleState_set_background_m2586262574(L_6, L_7, /*hidden argument*/NULL);
		int32_t L_8 = Screen_get_width_m1424489183(NULL /*static, unused*/, /*hidden argument*/NULL);
		___width1 = ((float)((float)((float)((float)((float)((float)(((float)((float)L_8)))/(float)(2.0f)))-(float)(35.0f)))-(float)(20.0f)));
		int32_t L_9 = Screen_get_width_m1424489183(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_10 = ___width1;
		int32_t L_11 = Screen_get_height_m1713306064(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_12 = __this->get__buttonHeight_3();
		float L_13 = ___width1;
		float L_14 = __this->get__buttonHeight_3();
		Rect_t1525428817  L_15;
		memset(&L_15, 0, sizeof(L_15));
		Rect__ctor_m2378035624(&L_15, ((float)((float)((float)((float)(((float)((float)L_9)))-(float)L_10))-(float)(10.0f))), ((float)((float)((float)((float)(((float)((float)L_11)))-(float)L_12))-(float)(10.0f))), L_13, L_14, /*hidden argument*/NULL);
		String_t* L_16 = ___text0;
		bool L_17 = GUI_Button_m358435862(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		return L_17;
	}
}
// System.Boolean Prime31.MonoBehaviourGUI::bottomLeftButton(System.String,System.Single)
extern Il2CppClass* GUI_t1522956648_il2cpp_TypeInfo_var;
extern const uint32_t MonoBehaviourGUI_bottomLeftButton_m3846189774_MetadataUsageId;
extern "C"  bool MonoBehaviourGUI_bottomLeftButton_m3846189774 (MonoBehaviourGUI_t2892502096 * __this, String_t* ___text0, float ___width1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoBehaviourGUI_bottomLeftButton_m3846189774_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		GUISkin_t2614611333 * L_0 = GUI_get_skin_m2455194711(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		GUIStyle_t1006925219 * L_1 = GUISkin_get_button_m3805635679(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GUIStyleState_t47287592 * L_2 = GUIStyle_get_hover_m1723373270(L_1, /*hidden argument*/NULL);
		Texture2D_t2509538522 * L_3 = MonoBehaviourGUI_get_bottomButtonBackground_m261950502(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		GUIStyleState_set_background_m2586262574(L_2, L_3, /*hidden argument*/NULL);
		GUISkin_t2614611333 * L_4 = GUI_get_skin_m2455194711(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		GUIStyle_t1006925219 * L_5 = GUISkin_get_button_m3805635679(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		GUIStyleState_t47287592 * L_6 = GUIStyle_get_normal_m3644321711(L_5, /*hidden argument*/NULL);
		Texture2D_t2509538522 * L_7 = MonoBehaviourGUI_get_bottomButtonBackground_m261950502(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		GUIStyleState_set_background_m2586262574(L_6, L_7, /*hidden argument*/NULL);
		int32_t L_8 = Screen_get_width_m1424489183(NULL /*static, unused*/, /*hidden argument*/NULL);
		___width1 = ((float)((float)((float)((float)((float)((float)(((float)((float)L_8)))/(float)(2.0f)))-(float)(35.0f)))-(float)(20.0f)));
		int32_t L_9 = Screen_get_height_m1713306064(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_10 = __this->get__buttonHeight_3();
		float L_11 = ___width1;
		float L_12 = __this->get__buttonHeight_3();
		Rect_t1525428817  L_13;
		memset(&L_13, 0, sizeof(L_13));
		Rect__ctor_m2378035624(&L_13, (10.0f), ((float)((float)((float)((float)(((float)((float)L_9)))-(float)L_10))-(float)(10.0f))), L_11, L_12, /*hidden argument*/NULL);
		String_t* L_14 = ___text0;
		bool L_15 = GUI_Button_m358435862(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		return L_15;
	}
}
// System.Boolean Prime31.MonoBehaviourGUI::bottomCenterButton(System.String,System.Single)
extern Il2CppClass* GUI_t1522956648_il2cpp_TypeInfo_var;
extern const uint32_t MonoBehaviourGUI_bottomCenterButton_m4278278016_MetadataUsageId;
extern "C"  bool MonoBehaviourGUI_bottomCenterButton_m4278278016 (MonoBehaviourGUI_t2892502096 * __this, String_t* ___text0, float ___width1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoBehaviourGUI_bottomCenterButton_m4278278016_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		GUISkin_t2614611333 * L_0 = GUI_get_skin_m2455194711(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		GUIStyle_t1006925219 * L_1 = GUISkin_get_button_m3805635679(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GUIStyleState_t47287592 * L_2 = GUIStyle_get_hover_m1723373270(L_1, /*hidden argument*/NULL);
		Texture2D_t2509538522 * L_3 = MonoBehaviourGUI_get_bottomButtonBackground_m261950502(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		GUIStyleState_set_background_m2586262574(L_2, L_3, /*hidden argument*/NULL);
		GUISkin_t2614611333 * L_4 = GUI_get_skin_m2455194711(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		GUIStyle_t1006925219 * L_5 = GUISkin_get_button_m3805635679(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		GUIStyleState_t47287592 * L_6 = GUIStyle_get_normal_m3644321711(L_5, /*hidden argument*/NULL);
		Texture2D_t2509538522 * L_7 = MonoBehaviourGUI_get_bottomButtonBackground_m261950502(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		GUIStyleState_set_background_m2586262574(L_6, L_7, /*hidden argument*/NULL);
		int32_t L_8 = Screen_get_width_m1424489183(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_9 = ___width1;
		V_0 = ((float)((float)(((float)((float)((int32_t)((int32_t)L_8/(int32_t)2)))))-(float)((float)((float)L_9/(float)(2.0f)))));
		float L_10 = V_0;
		int32_t L_11 = Screen_get_height_m1713306064(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_12 = __this->get__buttonHeight_3();
		float L_13 = ___width1;
		float L_14 = __this->get__buttonHeight_3();
		Rect_t1525428817  L_15;
		memset(&L_15, 0, sizeof(L_15));
		Rect__ctor_m2378035624(&L_15, L_10, ((float)((float)((float)((float)(((float)((float)L_11)))-(float)L_12))-(float)(10.0f))), L_13, L_14, /*hidden argument*/NULL);
		String_t* L_16 = ___text0;
		bool L_17 = GUI_Button_m358435862(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		return L_17;
	}
}
// System.Boolean Prime31.MonoBehaviourGUI::toggleButton(System.String,System.String)
extern Il2CppClass* GUI_t1522956648_il2cpp_TypeInfo_var;
extern Il2CppClass* GUILayoutOptionU5BU5D_t1890718142_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t MonoBehaviourGUI_toggleButton_m2496466019_MetadataUsageId;
extern "C"  bool MonoBehaviourGUI_toggleButton_m2496466019 (MonoBehaviourGUI_t2892502096 * __this, String_t* ___defaultText0, String_t* ___selectedText1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoBehaviourGUI_toggleButton_m2496466019_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* G_B5_0 = NULL;
	{
		Dictionary_2_t1848703245 * L_0 = __this->get__toggleButtons_4();
		String_t* L_1 = ___defaultText0;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, String_t* >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Boolean>::ContainsKey(!0) */, L_0, L_1);
		if (L_2)
		{
			goto IL_001e;
		}
	}
	{
		Dictionary_2_t1848703245 * L_3 = __this->get__toggleButtons_4();
		String_t* L_4 = ___defaultText0;
		NullCheck(L_3);
		VirtActionInvoker2< String_t*, bool >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Boolean>::set_Item(!0,!1) */, L_3, L_4, (bool)1);
	}

IL_001e:
	{
		Dictionary_2_t1848703245 * L_5 = __this->get__toggleButtons_4();
		String_t* L_6 = ___defaultText0;
		NullCheck(L_5);
		bool L_7 = VirtFuncInvoker1< bool, String_t* >::Invoke(22 /* !1 System.Collections.Generic.Dictionary`2<System.String,System.Boolean>::get_Item(!0) */, L_5, L_6);
		if (!L_7)
		{
			goto IL_0035;
		}
	}
	{
		String_t* L_8 = ___defaultText0;
		G_B5_0 = L_8;
		goto IL_0036;
	}

IL_0035:
	{
		String_t* L_9 = ___selectedText1;
		G_B5_0 = L_9;
	}

IL_0036:
	{
		V_0 = G_B5_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		GUISkin_t2614611333 * L_10 = GUI_get_skin_m2455194711(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_10);
		GUIStyle_t1006925219 * L_11 = GUISkin_get_button_m3805635679(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		GUIStyleState_t47287592 * L_12 = GUIStyle_get_normal_m3644321711(L_11, /*hidden argument*/NULL);
		Texture2D_t2509538522 * L_13 = MonoBehaviourGUI_get_toggleButtonBackground_m720475407(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		GUIStyleState_set_background_m2586262574(L_12, L_13, /*hidden argument*/NULL);
		Dictionary_2_t1848703245 * L_14 = __this->get__toggleButtons_4();
		String_t* L_15 = ___defaultText0;
		NullCheck(L_14);
		bool L_16 = VirtFuncInvoker1< bool, String_t* >::Invoke(22 /* !1 System.Collections.Generic.Dictionary`2<System.String,System.Boolean>::get_Item(!0) */, L_14, L_15);
		if (L_16)
		{
			goto IL_0071;
		}
	}
	{
		Color_t1588175760  L_17 = Color_get_yellow_m702153025(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		GUI_set_contentColor_m1528425788(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		goto IL_008b;
	}

IL_0071:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		GUISkin_t2614611333 * L_18 = GUI_get_skin_m2455194711(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_18);
		GUIStyle_t1006925219 * L_19 = GUISkin_get_button_m3805635679(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		GUIStyle_set_fontStyle_m2771425552(L_19, 1, /*hidden argument*/NULL);
		Color_t1588175760  L_20 = Color_get_red_m2160856070(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUI_set_contentColor_m1528425788(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
	}

IL_008b:
	{
		String_t* L_21 = V_0;
		bool L_22 = GUILayout_Button_m3959463722(NULL /*static, unused*/, L_21, ((GUILayoutOptionU5BU5D_t1890718142*)SZArrayNew(GUILayoutOptionU5BU5D_t1890718142_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_00af;
		}
	}
	{
		Dictionary_2_t1848703245 * L_23 = __this->get__toggleButtons_4();
		String_t* L_24 = ___defaultText0;
		String_t* L_25 = V_0;
		String_t* L_26 = ___defaultText0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_27 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_25, L_26, /*hidden argument*/NULL);
		NullCheck(L_23);
		VirtActionInvoker2< String_t*, bool >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Boolean>::set_Item(!0,!1) */, L_23, L_24, L_27);
	}

IL_00af:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		GUISkin_t2614611333 * L_28 = GUI_get_skin_m2455194711(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_28);
		GUIStyle_t1006925219 * L_29 = GUISkin_get_button_m3805635679(L_28, /*hidden argument*/NULL);
		NullCheck(L_29);
		GUIStyleState_t47287592 * L_30 = GUIStyle_get_normal_m3644321711(L_29, /*hidden argument*/NULL);
		Texture2D_t2509538522 * L_31 = MonoBehaviourGUI_get_normalBackground_m1355396176(__this, /*hidden argument*/NULL);
		NullCheck(L_30);
		GUIStyleState_set_background_m2586262574(L_30, L_31, /*hidden argument*/NULL);
		GUISkin_t2614611333 * L_32 = GUI_get_skin_m2455194711(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_32);
		GUIStyle_t1006925219 * L_33 = GUISkin_get_button_m3805635679(L_32, /*hidden argument*/NULL);
		NullCheck(L_33);
		GUIStyle_set_fontStyle_m2771425552(L_33, 0, /*hidden argument*/NULL);
		Color_t1588175760  L_34 = Color_get_white_m2348858526(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUI_set_contentColor_m1528425788(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		Dictionary_2_t1848703245 * L_35 = __this->get__toggleButtons_4();
		String_t* L_36 = ___defaultText0;
		NullCheck(L_35);
		bool L_37 = VirtFuncInvoker1< bool, String_t* >::Invoke(22 /* !1 System.Collections.Generic.Dictionary`2<System.String,System.Boolean>::get_Item(!0) */, L_35, L_36);
		return L_37;
	}
}
// System.Boolean Prime31.MonoBehaviourGUI::toggleButtonState(System.String)
extern "C"  bool MonoBehaviourGUI_toggleButtonState_m2862615050 (MonoBehaviourGUI_t2892502096 * __this, String_t* ___defaultText0, const MethodInfo* method)
{
	{
		Dictionary_2_t1848703245 * L_0 = __this->get__toggleButtons_4();
		String_t* L_1 = ___defaultText0;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, String_t* >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Boolean>::ContainsKey(!0) */, L_0, L_1);
		if (L_2)
		{
			goto IL_001e;
		}
	}
	{
		Dictionary_2_t1848703245 * L_3 = __this->get__toggleButtons_4();
		String_t* L_4 = ___defaultText0;
		NullCheck(L_3);
		VirtActionInvoker2< String_t*, bool >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Boolean>::set_Item(!0,!1) */, L_3, L_4, (bool)1);
	}

IL_001e:
	{
		Dictionary_2_t1848703245 * L_5 = __this->get__toggleButtons_4();
		String_t* L_6 = ___defaultText0;
		NullCheck(L_5);
		bool L_7 = VirtFuncInvoker1< bool, String_t* >::Invoke(22 /* !1 System.Collections.Generic.Dictionary`2<System.String,System.Boolean>::get_Item(!0) */, L_5, L_6);
		return L_7;
	}
}
// System.Void Prime31.ThreadingCallbackHelper::.ctor()
extern Il2CppClass* List_1_t1234482916_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3014744570_MethodInfo_var;
extern const uint32_t ThreadingCallbackHelper__ctor_m2363282603_MetadataUsageId;
extern "C"  void ThreadingCallbackHelper__ctor_m2363282603 (ThreadingCallbackHelper_t3423238746 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThreadingCallbackHelper__ctor_m2363282603_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1234482916 * L_0 = (List_1_t1234482916 *)il2cpp_codegen_object_new(List_1_t1234482916_il2cpp_TypeInfo_var);
		List_1__ctor_m3014744570(L_0, /*hidden argument*/List_1__ctor_m3014744570_MethodInfo_var);
		__this->set__actions_2(L_0);
		List_1_t1234482916 * L_1 = (List_1_t1234482916 *)il2cpp_codegen_object_new(List_1_t1234482916_il2cpp_TypeInfo_var);
		List_1__ctor_m3014744570(L_1, /*hidden argument*/List_1__ctor_m3014744570_MethodInfo_var);
		__this->set__currentActions_3(L_1);
		MonoBehaviour__ctor_m350695606(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Prime31.ThreadingCallbackHelper::addActionToQueue(System.Action)
extern "C"  void ThreadingCallbackHelper_addActionToQueue_m1029015557 (ThreadingCallbackHelper_t3423238746 * __this, Action_t437523947 * ___action0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t1234482916 * L_0 = __this->get__actions_2();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		List_1_t1234482916 * L_2 = __this->get__actions_2();
		Action_t437523947 * L_3 = ___action0;
		NullCheck(L_2);
		VirtActionInvoker1< Action_t437523947 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Action>::Add(!0) */, L_2, L_3);
		IL2CPP_LEAVE(0x25, FINALLY_001e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_001e;
	}

FINALLY_001e:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(30)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(30)
	{
		IL2CPP_JUMP_TBL(0x25, IL_0025)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0025:
	{
		return;
	}
}
// System.Void Prime31.ThreadingCallbackHelper::Update()
extern const MethodInfo* List_1_AddRange_m465475392_MethodInfo_var;
extern const uint32_t ThreadingCallbackHelper_Update_m1974178754_MetadataUsageId;
extern "C"  void ThreadingCallbackHelper_Update_m1974178754 (ThreadingCallbackHelper_t3423238746 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThreadingCallbackHelper_Update_m1974178754_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t1234482916 * L_0 = __this->get__actions_2();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		List_1_t1234482916 * L_2 = __this->get__currentActions_3();
		List_1_t1234482916 * L_3 = __this->get__actions_2();
		NullCheck(L_2);
		List_1_AddRange_m465475392(L_2, L_3, /*hidden argument*/List_1_AddRange_m465475392_MethodInfo_var);
		List_1_t1234482916 * L_4 = __this->get__actions_2();
		NullCheck(L_4);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<System.Action>::Clear() */, L_4);
		IL2CPP_LEAVE(0x35, FINALLY_002e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002e;
	}

FINALLY_002e:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(46)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(46)
	{
		IL2CPP_JUMP_TBL(0x35, IL_0035)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0035:
	{
		V_1 = 0;
		goto IL_0051;
	}

IL_003c:
	{
		List_1_t1234482916 * L_6 = __this->get__currentActions_3();
		int32_t L_7 = V_1;
		NullCheck(L_6);
		Action_t437523947 * L_8 = VirtFuncInvoker1< Action_t437523947 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.Action>::get_Item(System.Int32) */, L_6, L_7);
		NullCheck(L_8);
		Action_Invoke_m1445970038(L_8, /*hidden argument*/NULL);
		int32_t L_9 = V_1;
		V_1 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0051:
	{
		int32_t L_10 = V_1;
		List_1_t1234482916 * L_11 = __this->get__currentActions_3();
		NullCheck(L_11);
		int32_t L_12 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Action>::get_Count() */, L_11);
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_003c;
		}
	}
	{
		List_1_t1234482916 * L_13 = __this->get__currentActions_3();
		NullCheck(L_13);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<System.Action>::Clear() */, L_13);
		return;
	}
}
// System.Void Prime31.ThreadingCallbackHelper::disableIfEmpty()
extern "C"  void ThreadingCallbackHelper_disableIfEmpty_m3972502849 (ThreadingCallbackHelper_t3423238746 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t1234482916 * L_0 = __this->get__actions_2();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			List_1_t1234482916 * L_2 = __this->get__actions_2();
			NullCheck(L_2);
			int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Action>::get_Count() */, L_2);
			if (L_3)
			{
				goto IL_0024;
			}
		}

IL_001d:
		{
			Behaviour_set_enabled_m1432835224(__this, (bool)0, /*hidden argument*/NULL);
		}

IL_0024:
		{
			IL2CPP_LEAVE(0x30, FINALLY_0029);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0029;
	}

FINALLY_0029:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(41)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(41)
	{
		IL2CPP_JUMP_TBL(0x30, IL_0030)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0030:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
