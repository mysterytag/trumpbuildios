﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.TimestampObservable`1<System.Object>
struct TimestampObservable_1_t2503612568;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// UniRx.IScheduler
struct IScheduler_t2938318244;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.Timestamped`1<System.Object>>
struct IObserver_1_t436215880;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.TimestampObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,UniRx.IScheduler)
extern "C"  void TimestampObservable_1__ctor_m2818472247_gshared (TimestampObservable_1_t2503612568 * __this, Il2CppObject* ___source0, Il2CppObject * ___scheduler1, const MethodInfo* method);
#define TimestampObservable_1__ctor_m2818472247(__this, ___source0, ___scheduler1, method) ((  void (*) (TimestampObservable_1_t2503612568 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))TimestampObservable_1__ctor_m2818472247_gshared)(__this, ___source0, ___scheduler1, method)
// System.IDisposable UniRx.Operators.TimestampObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<UniRx.Timestamped`1<T>>,System.IDisposable)
extern "C"  Il2CppObject * TimestampObservable_1_SubscribeCore_m3716861182_gshared (TimestampObservable_1_t2503612568 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define TimestampObservable_1_SubscribeCore_m3716861182(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (TimestampObservable_1_t2503612568 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))TimestampObservable_1_SubscribeCore_m3716861182_gshared)(__this, ___observer0, ___cancel1, method)
