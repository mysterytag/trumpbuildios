﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.ASN1
struct ASN1_t1254135647;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t58506160;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime339033936.h"
#include "mscorlib_System_String968488902.h"
#include "Mono_Security_Mono_Security_ASN11254135646.h"

// Mono.Security.ASN1 Mono.Security.ASN1Convert::FromDateTime(System.DateTime)
extern "C"  ASN1_t1254135647 * ASN1Convert_FromDateTime_m1243414849 (Il2CppObject * __this /* static, unused */, DateTime_t339033936  ___dt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.ASN1 Mono.Security.ASN1Convert::FromInt32(System.Int32)
extern "C"  ASN1_t1254135647 * ASN1Convert_FromInt32_m2294019845 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.ASN1 Mono.Security.ASN1Convert::FromOid(System.String)
extern "C"  ASN1_t1254135647 * ASN1Convert_FromOid_m1663059922 (Il2CppObject * __this /* static, unused */, String_t* ___oid0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.ASN1 Mono.Security.ASN1Convert::FromUnsignedBigInteger(System.Byte[])
extern "C"  ASN1_t1254135647 * ASN1Convert_FromUnsignedBigInteger_m2345595514 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t58506160* ___big0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Security.ASN1Convert::ToInt32(Mono.Security.ASN1)
extern "C"  int32_t ASN1Convert_ToInt32_m1508411796 (Il2CppObject * __this /* static, unused */, ASN1_t1254135647 * ___asn10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.ASN1Convert::ToOid(Mono.Security.ASN1)
extern "C"  String_t* ASN1Convert_ToOid_m1387992843 (Il2CppObject * __this /* static, unused */, ASN1_t1254135647 * ___asn10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Mono.Security.ASN1Convert::ToDateTime(Mono.Security.ASN1)
extern "C"  DateTime_t339033936  ASN1Convert_ToDateTime_m864003254 (Il2CppObject * __this /* static, unused */, ASN1_t1254135647 * ___time0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
