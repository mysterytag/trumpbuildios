﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Font
struct Font_t1525081276;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FontDef
struct  FontDef_t983695638  : public Il2CppObject
{
public:
	// UnityEngine.Font FontDef::Font
	Font_t1525081276 * ___Font_0;
	// System.Int32 FontDef::Size
	int32_t ___Size_1;

public:
	inline static int32_t get_offset_of_Font_0() { return static_cast<int32_t>(offsetof(FontDef_t983695638, ___Font_0)); }
	inline Font_t1525081276 * get_Font_0() const { return ___Font_0; }
	inline Font_t1525081276 ** get_address_of_Font_0() { return &___Font_0; }
	inline void set_Font_0(Font_t1525081276 * value)
	{
		___Font_0 = value;
		Il2CppCodeGenWriteBarrier(&___Font_0, value);
	}

	inline static int32_t get_offset_of_Size_1() { return static_cast<int32_t>(offsetof(FontDef_t983695638, ___Size_1)); }
	inline int32_t get_Size_1() const { return ___Size_1; }
	inline int32_t* get_address_of_Size_1() { return &___Size_1; }
	inline void set_Size_1(int32_t value)
	{
		___Size_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
