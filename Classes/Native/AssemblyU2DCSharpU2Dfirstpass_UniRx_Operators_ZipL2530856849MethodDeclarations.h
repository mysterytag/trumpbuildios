﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ZipLatestObservable`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct ZipLatestObservable_6_t2530856849;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// UniRx.Operators.ZipLatestFunc`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct ZipLatestFunc_6_t2313265476;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.ZipLatestObservable`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(UniRx.IObservable`1<T1>,UniRx.IObservable`1<T2>,UniRx.IObservable`1<T3>,UniRx.IObservable`1<T4>,UniRx.IObservable`1<T5>,UniRx.Operators.ZipLatestFunc`6<T1,T2,T3,T4,T5,TR>)
extern "C"  void ZipLatestObservable_6__ctor_m3953744639_gshared (ZipLatestObservable_6_t2530856849 * __this, Il2CppObject* ___source10, Il2CppObject* ___source21, Il2CppObject* ___source32, Il2CppObject* ___source43, Il2CppObject* ___source54, ZipLatestFunc_6_t2313265476 * ___resultSelector5, const MethodInfo* method);
#define ZipLatestObservable_6__ctor_m3953744639(__this, ___source10, ___source21, ___source32, ___source43, ___source54, ___resultSelector5, method) ((  void (*) (ZipLatestObservable_6_t2530856849 *, Il2CppObject*, Il2CppObject*, Il2CppObject*, Il2CppObject*, Il2CppObject*, ZipLatestFunc_6_t2313265476 *, const MethodInfo*))ZipLatestObservable_6__ctor_m3953744639_gshared)(__this, ___source10, ___source21, ___source32, ___source43, ___source54, ___resultSelector5, method)
// System.IDisposable UniRx.Operators.ZipLatestObservable`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::SubscribeCore(UniRx.IObserver`1<TR>,System.IDisposable)
extern "C"  Il2CppObject * ZipLatestObservable_6_SubscribeCore_m887248745_gshared (ZipLatestObservable_6_t2530856849 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define ZipLatestObservable_6_SubscribeCore_m887248745(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (ZipLatestObservable_6_t2530856849 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))ZipLatestObservable_6_SubscribeCore_m887248745_gshared)(__this, ___observer0, ___cancel1, method)
