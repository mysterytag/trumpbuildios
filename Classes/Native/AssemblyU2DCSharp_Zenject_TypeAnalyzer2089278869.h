﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.Type,Zenject.ZenjectTypeInfo>
struct Dictionary_2_t2395761423;
// System.Func`2<System.Reflection.MethodInfo,System.Boolean>
struct Func_2_t3166396148;
// System.Func`2<System.Reflection.ConstructorInfo,System.Boolean>
struct Func_2_t775214871;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TypeAnalyzer
struct  TypeAnalyzer_t2089278869  : public Il2CppObject
{
public:

public:
};

struct TypeAnalyzer_t2089278869_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,Zenject.ZenjectTypeInfo> Zenject.TypeAnalyzer::_typeInfo
	Dictionary_2_t2395761423 * ____typeInfo_0;
	// System.Func`2<System.Reflection.MethodInfo,System.Boolean> Zenject.TypeAnalyzer::<>f__am$cache1
	Func_2_t3166396148 * ___U3CU3Ef__amU24cache1_1;
	// System.Func`2<System.Reflection.ConstructorInfo,System.Boolean> Zenject.TypeAnalyzer::<>f__am$cache2
	Func_2_t775214871 * ___U3CU3Ef__amU24cache2_2;

public:
	inline static int32_t get_offset_of__typeInfo_0() { return static_cast<int32_t>(offsetof(TypeAnalyzer_t2089278869_StaticFields, ____typeInfo_0)); }
	inline Dictionary_2_t2395761423 * get__typeInfo_0() const { return ____typeInfo_0; }
	inline Dictionary_2_t2395761423 ** get_address_of__typeInfo_0() { return &____typeInfo_0; }
	inline void set__typeInfo_0(Dictionary_2_t2395761423 * value)
	{
		____typeInfo_0 = value;
		Il2CppCodeGenWriteBarrier(&____typeInfo_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(TypeAnalyzer_t2089278869_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline Func_2_t3166396148 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline Func_2_t3166396148 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(Func_2_t3166396148 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_2() { return static_cast<int32_t>(offsetof(TypeAnalyzer_t2089278869_StaticFields, ___U3CU3Ef__amU24cache2_2)); }
	inline Func_2_t775214871 * get_U3CU3Ef__amU24cache2_2() const { return ___U3CU3Ef__amU24cache2_2; }
	inline Func_2_t775214871 ** get_address_of_U3CU3Ef__amU24cache2_2() { return &___U3CU3Ef__amU24cache2_2; }
	inline void set_U3CU3Ef__amU24cache2_2(Func_2_t775214871 * value)
	{
		___U3CU3Ef__amU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
