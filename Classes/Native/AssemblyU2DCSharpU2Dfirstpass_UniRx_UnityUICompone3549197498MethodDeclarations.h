﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObservable`1<System.String>
struct IObservable_1_t727287266;
// UnityEngine.UI.Text
struct Text_t3286458198;
// UniRx.IObservable`1<System.Boolean>
struct IObservable_1_t4264771001;
// UnityEngine.UI.Selectable
struct Selectable_t3621744255;
// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;
// UnityEngine.UI.Button
struct Button_t990034267;
// UnityEngine.UI.Toggle
struct Toggle_t1499417981;
// UniRx.IObservable`1<System.Single>
struct IObservable_1_t717007385;
// UnityEngine.UI.Scrollbar
struct Scrollbar_t1410649103;
// UniRx.IObservable`1<UnityEngine.Vector2>
struct IObservable_1_t3284128152;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t1048578170;
// UnityEngine.UI.Slider
struct Slider_t1468074762;
// UnityEngine.UI.InputField
struct InputField_t2345609593;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_Text3286458198.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable3621744255.h"
#include "UnityEngine_UI_UnityEngine_UI_Button990034267.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle1499417981.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar1410649103.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect1048578170.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider1468074762.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField2345609593.h"

// System.IDisposable UniRx.UnityUIComponentExtensions::SubscribeToText(UniRx.IObservable`1<System.String>,UnityEngine.UI.Text)
extern "C"  Il2CppObject * UnityUIComponentExtensions_SubscribeToText_m1080731835 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, Text_t3286458198 * ___text1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable UniRx.UnityUIComponentExtensions::SubscribeToInteractable(UniRx.IObservable`1<System.Boolean>,UnityEngine.UI.Selectable)
extern "C"  Il2CppObject * UnityUIComponentExtensions_SubscribeToInteractable_m2518160928 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, Selectable_t3621744255 * ___selectable1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.UnityUIComponentExtensions::OnClickAsObservable(UnityEngine.UI.Button)
extern "C"  Il2CppObject* UnityUIComponentExtensions_OnClickAsObservable_m2309618682 (Il2CppObject * __this /* static, unused */, Button_t990034267 * ___button0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.Boolean> UniRx.UnityUIComponentExtensions::OnValueChangedAsObservable(UnityEngine.UI.Toggle)
extern "C"  Il2CppObject* UnityUIComponentExtensions_OnValueChangedAsObservable_m1250335802 (Il2CppObject * __this /* static, unused */, Toggle_t1499417981 * ___toggle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.Single> UniRx.UnityUIComponentExtensions::OnValueChangedAsObservable(UnityEngine.UI.Scrollbar)
extern "C"  Il2CppObject* UnityUIComponentExtensions_OnValueChangedAsObservable_m2967905152 (Il2CppObject * __this /* static, unused */, Scrollbar_t1410649103 * ___scrollbar0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.Vector2> UniRx.UnityUIComponentExtensions::OnValueChangedAsObservable(UnityEngine.UI.ScrollRect)
extern "C"  Il2CppObject* UnityUIComponentExtensions_OnValueChangedAsObservable_m4021716010 (Il2CppObject * __this /* static, unused */, ScrollRect_t1048578170 * ___scrollRect0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.Single> UniRx.UnityUIComponentExtensions::OnValueChangedAsObservable(UnityEngine.UI.Slider)
extern "C"  Il2CppObject* UnityUIComponentExtensions_OnValueChangedAsObservable_m3684253643 (Il2CppObject * __this /* static, unused */, Slider_t1468074762 * ___slider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.String> UniRx.UnityUIComponentExtensions::OnEndEditAsObservable(UnityEngine.UI.InputField)
extern "C"  Il2CppObject* UnityUIComponentExtensions_OnEndEditAsObservable_m7501143 (Il2CppObject * __this /* static, unused */, InputField_t2345609593 * ___inputField0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.String> UniRx.UnityUIComponentExtensions::OnValueChangeAsObservable(UnityEngine.UI.InputField)
extern "C"  Il2CppObject* UnityUIComponentExtensions_OnValueChangeAsObservable_m3165252955 (Il2CppObject * __this /* static, unused */, InputField_t2345609593 * ___inputField0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.String> UniRx.UnityUIComponentExtensions::OnValueChangedAsObservable(UnityEngine.UI.InputField)
extern "C"  Il2CppObject* UnityUIComponentExtensions_OnValueChangedAsObservable_m1175881235 (Il2CppObject * __this /* static, unused */, InputField_t2345609593 * ___inputField0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
