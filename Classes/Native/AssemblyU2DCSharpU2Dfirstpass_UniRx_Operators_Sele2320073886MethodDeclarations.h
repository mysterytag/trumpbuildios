﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectManyObservable`3/SelectManyEnumerableObserverWithIndex<System.Object,System.Int64,System.Object>
struct SelectManyEnumerableObserverWithIndex_t2320073886;
// UniRx.Operators.SelectManyObservable`3<System.Object,System.Int64,System.Object>
struct SelectManyObservable_3_t744723289;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyEnumerableObserverWithIndex<System.Object,System.Int64,System.Object>::.ctor(UniRx.Operators.SelectManyObservable`3<TSource,TCollection,TResult>,UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void SelectManyEnumerableObserverWithIndex__ctor_m1526518322_gshared (SelectManyEnumerableObserverWithIndex_t2320073886 * __this, SelectManyObservable_3_t744723289 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define SelectManyEnumerableObserverWithIndex__ctor_m1526518322(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (SelectManyEnumerableObserverWithIndex_t2320073886 *, SelectManyObservable_3_t744723289 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SelectManyEnumerableObserverWithIndex__ctor_m1526518322_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.SelectManyObservable`3/SelectManyEnumerableObserverWithIndex<System.Object,System.Int64,System.Object>::Run()
extern "C"  Il2CppObject * SelectManyEnumerableObserverWithIndex_Run_m1032547133_gshared (SelectManyEnumerableObserverWithIndex_t2320073886 * __this, const MethodInfo* method);
#define SelectManyEnumerableObserverWithIndex_Run_m1032547133(__this, method) ((  Il2CppObject * (*) (SelectManyEnumerableObserverWithIndex_t2320073886 *, const MethodInfo*))SelectManyEnumerableObserverWithIndex_Run_m1032547133_gshared)(__this, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyEnumerableObserverWithIndex<System.Object,System.Int64,System.Object>::OnNext(TSource)
extern "C"  void SelectManyEnumerableObserverWithIndex_OnNext_m2872058940_gshared (SelectManyEnumerableObserverWithIndex_t2320073886 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define SelectManyEnumerableObserverWithIndex_OnNext_m2872058940(__this, ___value0, method) ((  void (*) (SelectManyEnumerableObserverWithIndex_t2320073886 *, Il2CppObject *, const MethodInfo*))SelectManyEnumerableObserverWithIndex_OnNext_m2872058940_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyEnumerableObserverWithIndex<System.Object,System.Int64,System.Object>::OnError(System.Exception)
extern "C"  void SelectManyEnumerableObserverWithIndex_OnError_m2594556070_gshared (SelectManyEnumerableObserverWithIndex_t2320073886 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define SelectManyEnumerableObserverWithIndex_OnError_m2594556070(__this, ___error0, method) ((  void (*) (SelectManyEnumerableObserverWithIndex_t2320073886 *, Exception_t1967233988 *, const MethodInfo*))SelectManyEnumerableObserverWithIndex_OnError_m2594556070_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyEnumerableObserverWithIndex<System.Object,System.Int64,System.Object>::OnCompleted()
extern "C"  void SelectManyEnumerableObserverWithIndex_OnCompleted_m3457139961_gshared (SelectManyEnumerableObserverWithIndex_t2320073886 * __this, const MethodInfo* method);
#define SelectManyEnumerableObserverWithIndex_OnCompleted_m3457139961(__this, method) ((  void (*) (SelectManyEnumerableObserverWithIndex_t2320073886 *, const MethodInfo*))SelectManyEnumerableObserverWithIndex_OnCompleted_m3457139961_gshared)(__this, method)
