﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`3<System.Single,SubstanceBase`2/Intrusion<System.Single,System.Single>,System.Single>
struct Func_3_t3383474419;

#include "AssemblyU2DCSharp_SubstanceBase_2_gen1869245964.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MultiplicativeSubstance
struct  MultiplicativeSubstance_t581301658  : public SubstanceBase_2_t1869245964
{
public:

public:
};

struct MultiplicativeSubstance_t581301658_StaticFields
{
public:
	// System.Func`3<System.Single,SubstanceBase`2/Intrusion<System.Single,System.Single>,System.Single> MultiplicativeSubstance::<>f__am$cache0
	Func_3_t3383474419 * ___U3CU3Ef__amU24cache0_6;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_6() { return static_cast<int32_t>(offsetof(MultiplicativeSubstance_t581301658_StaticFields, ___U3CU3Ef__amU24cache0_6)); }
	inline Func_3_t3383474419 * get_U3CU3Ef__amU24cache0_6() const { return ___U3CU3Ef__amU24cache0_6; }
	inline Func_3_t3383474419 ** get_address_of_U3CU3Ef__amU24cache0_6() { return &___U3CU3Ef__amU24cache0_6; }
	inline void set_U3CU3Ef__amU24cache0_6(Func_3_t3383474419 * value)
	{
		___U3CU3Ef__amU24cache0_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
