﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.UUnit.UUnitTestCase/UUnitTestDelegate
struct UUnitTestDelegate_t1722340944;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.UUnit.UUnitTestCase/UUnitTestDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void UUnitTestDelegate__ctor_m856137603 (UUnitTestDelegate_t1722340944 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.UUnitTestCase/UUnitTestDelegate::Invoke()
extern "C"  void UUnitTestDelegate_Invoke_m3044747677 (UUnitTestDelegate_t1722340944 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_UUnitTestDelegate_t1722340944(Il2CppObject* delegate);
// System.IAsyncResult PlayFab.UUnit.UUnitTestCase/UUnitTestDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UUnitTestDelegate_BeginInvoke_m4089322574 (UUnitTestDelegate_t1722340944 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.UUnitTestCase/UUnitTestDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void UUnitTestDelegate_EndInvoke_m1237666579 (UUnitTestDelegate_t1722340944 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
