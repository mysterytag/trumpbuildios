﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.TakeObservable`1/Take<System.Int64>
struct Take_t2541561334;
// UniRx.Operators.TakeObservable`1<System.Int64>
struct TakeObservable_1_t794160655;
// UniRx.IObserver`1<System.Int64>
struct IObserver_1_t764446489;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.TakeObservable`1/Take<System.Int64>::.ctor(UniRx.Operators.TakeObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Take__ctor_m1191812163_gshared (Take_t2541561334 * __this, TakeObservable_1_t794160655 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Take__ctor_m1191812163(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Take_t2541561334 *, TakeObservable_1_t794160655 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Take__ctor_m1191812163_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.TakeObservable`1/Take<System.Int64>::OnNext(T)
extern "C"  void Take_OnNext_m3241971687_gshared (Take_t2541561334 * __this, int64_t ___value0, const MethodInfo* method);
#define Take_OnNext_m3241971687(__this, ___value0, method) ((  void (*) (Take_t2541561334 *, int64_t, const MethodInfo*))Take_OnNext_m3241971687_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.TakeObservable`1/Take<System.Int64>::OnError(System.Exception)
extern "C"  void Take_OnError_m1623850230_gshared (Take_t2541561334 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Take_OnError_m1623850230(__this, ___error0, method) ((  void (*) (Take_t2541561334 *, Exception_t1967233988 *, const MethodInfo*))Take_OnError_m1623850230_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.TakeObservable`1/Take<System.Int64>::OnCompleted()
extern "C"  void Take_OnCompleted_m69464393_gshared (Take_t2541561334 * __this, const MethodInfo* method);
#define Take_OnCompleted_m69464393(__this, method) ((  void (*) (Take_t2541561334 *, const MethodInfo*))Take_OnCompleted_m69464393_gshared)(__this, method)
