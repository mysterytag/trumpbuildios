﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<GetLeaderboard>c__AnonStorey8E
struct U3CGetLeaderboardU3Ec__AnonStorey8E_t3725514361;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<GetLeaderboard>c__AnonStorey8E::.ctor()
extern "C"  void U3CGetLeaderboardU3Ec__AnonStorey8E__ctor_m646297946 (U3CGetLeaderboardU3Ec__AnonStorey8E_t3725514361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<GetLeaderboard>c__AnonStorey8E::<>m__7B(PlayFab.CallRequestContainer)
extern "C"  void U3CGetLeaderboardU3Ec__AnonStorey8E_U3CU3Em__7B_m4260284515 (U3CGetLeaderboardU3Ec__AnonStorey8E_t3725514361 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
