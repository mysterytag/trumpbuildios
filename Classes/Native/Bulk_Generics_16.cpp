﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Func`1<System.Single>
struct Func_1_t2100990268;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;
// System.Func`1<UniRx.Unit>
struct Func_1_t3701067285;
// System.Func`2<System.Boolean,System.Boolean>
struct Func_2_t1008118516;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Boolean>
struct Func_2_t1844407557;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Object>
struct Func_2_t2470508636;
// System.Func`2<System.Double,System.Double>
struct Func_2_t2869950768;
// System.Func`2<System.Double,System.Object>
struct Func_2_t3172540574;
// System.Func`2<System.Int32,System.Boolean>
struct Func_2_t3308141622;
// System.Func`2<System.Int32,System.Int32>
struct Func_2_t1649583772;
// System.Func`2<System.Int32,System.Object>
struct Func_2_t3934242701;
// System.Func`2<System.Int32,UnityEngine.Color>
struct Func_2_t390344745;
// System.Func`2<System.Int64,System.Boolean>
struct Func_2_t2251336571;
// System.Func`2<System.Int64,System.Int32>
struct Func_2_t592778721;
// System.Func`2<System.Int64,System.Int64>
struct Func_2_t592778816;
// System.Func`2<System.Int64,System.Object>
struct Func_2_t2877437650;
// System.Func`2<System.Int64,UnityEngine.Vector2>
struct Func_2_t1270693722;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t1509682273;
// System.Func`2<System.Object,System.Double>
struct Func_2_t1833193546;
// System.Func`2<System.Object,System.Int32>
struct Func_2_t4146091719;
// System.Func`2<System.Object,System.Int64>
struct Func_2_t4146091814;
// System.Func`2<System.Object,System.Object>
struct Func_2_t2135783352;
// System.Func`2<System.Object,System.Single>
struct Func_2_t2256885953;
// System.Func`2<System.Object,UniRx.Unit>
struct Func_2_t3856962970;
// System.Func`2<System.Single,System.Single>
struct Func_2_t3464793460;
// System.Func`2<System.UIntPtr,System.Object>
struct Func_2_t1403134703;
// System.Func`2<UniRx.Tuple`2<System.Object,System.Object>,System.Boolean>
struct Func_2_t1101125214;
// System.Func`2<UniRx.Tuple`2<System.Object,System.Object>,UniRx.Tuple`2<System.Object,System.Object>>
struct Func_2_t1259381692;
// System.Func`2<UniRx.Unit,System.Boolean>
struct Func_2_t2766110903;
// System.Func`2<UniRx.Unit,System.Object>
struct Func_2_t3392211982;
// System.Func`2<UniRx.Unit,UniRx.Unit>
struct Func_2_t818424304;
// System.Func`2<UnityEngine.Color,UnityEngine.Color>
struct Func_2_t2057442760;
// System.Func`2<UnityEngine.Touch,System.Boolean>
struct Func_2_t1874381577;
// System.Func`2<UnityEngine.Vector2,System.Boolean>
struct Func_2_t4239542457;
// System.Func`3<System.Boolean,System.Boolean,System.Boolean>
struct Func_3_t3063550794;
// System.Func`3<System.Boolean,System.Boolean,System.Object>
struct Func_3_t3689651873;
// System.Func`3<System.Boolean,System.Int32,System.Boolean>
struct Func_3_t1068606604;
// System.Func`3<System.Boolean,System.Int32,System.Object>
struct Func_3_t1694707683;
// System.Func`3<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Object,System.Object>
struct Func_3_t698853017;
// System.Func`3<System.DateTime,System.Int64,System.Object>
struct Func_3_t2236225891;
// System.Func`3<System.Double,System.Double,System.Double>
struct Func_3_t1933728903;
// System.Func`3<System.Double,System.Int32,System.Double>
struct Func_3_t2695431030;
// System.Func`3<System.Double,System.Int32,System.Object>
struct Func_3_t2998020836;
// System.Func`3<System.Int32,System.Int32,System.Int32>
struct Func_3_t543102568;
// System.Func`3<System.Int32,System.Int32,UnityEngine.Color>
struct Func_3_t3578830837;
// System.Func`3<System.Int64,System.Int32,System.Boolean>
struct Func_3_t1612416521;
// System.Func`3<System.Int64,System.Int32,System.Int32>
struct Func_3_t4248825967;
// System.Func`3<System.Int64,System.Int32,System.Object>
struct Func_3_t2238517600;
// System.Func`3<System.Int64,System.Int32,UnityEngine.Vector2>
struct Func_3_t631773672;
// System.Func`3<System.Int64,UniRx.Unit,System.Int64>
struct Func_3_t3706795343;
// System.Func`3<System.Object,Priority,System.Object>
struct Func_3_t3765090877;
// System.Func`3<System.Object,System.Int32,System.Boolean>
struct Func_3_t3064567499;
// System.Func`3<System.Object,System.Int32,System.Object>
struct Func_3_t3690668578;
// System.Func`3<System.Object,System.Int64,System.Object>
struct Func_3_t2633863527;
// System.Func`3<System.Object,System.Object,System.Object>
struct Func_3_t1892209229;
// System.Func`3<System.Object,UniRx.CancellationToken,System.Object>
struct Func_3_t2554989993;
// System.Func`3<System.Object,UniRx.Unit,System.Object>
struct Func_3_t3148637859;
// System.Func`3<System.Single,System.Object,System.Object>
struct Func_3_t2453810542;
// System.Func`3<System.Single,System.Object,System.Single>
struct Func_3_t2574913143;
// System.Func`3<System.Single,System.Single,System.Single>
struct Func_3_t3782820650;
// System.Func`3<UniRx.Unit,System.Int32,System.Boolean>
struct Func_3_t255060061;
// System.Func`3<UniRx.Unit,System.Int32,System.Object>
struct Func_3_t881161140;
// System.Func`3<UnityEngine.Vector2,System.Int32,System.Boolean>
struct Func_3_t487212563;
// System.Func`4<System.Object,System.Object,System.Object,System.Object>
struct Func_4_t3628925182;
// System.Func`5<System.Double,System.Int32,System.Double,System.Int32,System.Double>
struct Func_5_t2889627055;
// System.Func`5<System.Object,System.Int32,System.Int64,System.Int32,System.Object>
struct Func_5_t3557632551;
// System.Func`5<System.Object,System.Int32,System.Object,System.Int32,System.Object>
struct Func_5_t714816233;
// System.Func`5<System.Object,System.Int32,UniRx.Unit,System.Int32,System.Object>
struct Func_5_t2200276091;
// System.Func`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct Func_5_t875099911;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "System_Core_System_Func_1_gen2100990268.h"
#include "System_Core_System_Func_1_gen2100990268MethodDeclarations.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_Void2779279689.h"
#include "mscorlib_System_Single958209021.h"
#include "mscorlib_System_AsyncCallback1363551830.h"
#include "System_Core_System_Func_1_gen3701067285.h"
#include "System_Core_System_Func_1_gen3701067285MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "System_Core_System_Func_2_gen1008118516.h"
#include "System_Core_System_Func_2_gen1008118516MethodDeclarations.h"
#include "mscorlib_System_Boolean211005341.h"
#include "System_Core_System_Func_2_gen1844407557.h"
#include "System_Core_System_Func_2_gen1844407557MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23312956448.h"
#include "System_Core_System_Func_2_gen2470508636.h"
#include "System_Core_System_Func_2_gen2470508636MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2869950768.h"
#include "System_Core_System_Func_2_gen2869950768MethodDeclarations.h"
#include "mscorlib_System_Double534516614.h"
#include "System_Core_System_Func_2_gen3172540574.h"
#include "System_Core_System_Func_2_gen3172540574MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3308141622.h"
#include "System_Core_System_Func_2_gen3308141622MethodDeclarations.h"
#include "mscorlib_System_Int322847414787.h"
#include "System_Core_System_Func_2_gen1649583772.h"
#include "System_Core_System_Func_2_gen1649583772MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3934242701.h"
#include "System_Core_System_Func_2_gen3934242701MethodDeclarations.h"
#include "System_Core_System_Func_2_gen390344745.h"
#include "System_Core_System_Func_2_gen390344745MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"
#include "System_Core_System_Func_2_gen2251336571.h"
#include "System_Core_System_Func_2_gen2251336571MethodDeclarations.h"
#include "mscorlib_System_Int642847414882.h"
#include "System_Core_System_Func_2_gen592778721.h"
#include "System_Core_System_Func_2_gen592778721MethodDeclarations.h"
#include "System_Core_System_Func_2_gen592778816.h"
#include "System_Core_System_Func_2_gen592778816MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2877437650.h"
#include "System_Core_System_Func_2_gen2877437650MethodDeclarations.h"
#include "System_Core_System_Func_2_gen1270693722.h"
#include "System_Core_System_Func_2_gen1270693722MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"
#include "System_Core_System_Func_2_gen1509682273.h"
#include "System_Core_System_Func_2_gen1509682273MethodDeclarations.h"
#include "System_Core_System_Func_2_gen1833193546.h"
#include "System_Core_System_Func_2_gen1833193546MethodDeclarations.h"
#include "System_Core_System_Func_2_gen4146091719.h"
#include "System_Core_System_Func_2_gen4146091719MethodDeclarations.h"
#include "System_Core_System_Func_2_gen4146091814.h"
#include "System_Core_System_Func_2_gen4146091814MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2135783352.h"
#include "System_Core_System_Func_2_gen2135783352MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2256885953.h"
#include "System_Core_System_Func_2_gen2256885953MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3856962970.h"
#include "System_Core_System_Func_2_gen3856962970MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3464793460.h"
#include "System_Core_System_Func_2_gen3464793460MethodDeclarations.h"
#include "System_Core_System_Func_2_gen1403134703.h"
#include "System_Core_System_Func_2_gen1403134703MethodDeclarations.h"
#include "mscorlib_System_UIntPtr3100060873.h"
#include "System_Core_System_Func_2_gen1101125214.h"
#include "System_Core_System_Func_2_gen1101125214MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen369261819.h"
#include "System_Core_System_Func_2_gen1259381692.h"
#include "System_Core_System_Func_2_gen1259381692MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2766110903.h"
#include "System_Core_System_Func_2_gen2766110903MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3392211982.h"
#include "System_Core_System_Func_2_gen3392211982MethodDeclarations.h"
#include "System_Core_System_Func_2_gen818424304.h"
#include "System_Core_System_Func_2_gen818424304MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2057442760.h"
#include "System_Core_System_Func_2_gen2057442760MethodDeclarations.h"
#include "System_Core_System_Func_2_gen1874381577.h"
#include "System_Core_System_Func_2_gen1874381577MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Touch1603883884.h"
#include "System_Core_System_Func_2_gen4239542457.h"
#include "System_Core_System_Func_2_gen4239542457MethodDeclarations.h"
#include "System_Core_System_Func_3_gen3063550794.h"
#include "System_Core_System_Func_3_gen3063550794MethodDeclarations.h"
#include "System_Core_System_Func_3_gen3689651873.h"
#include "System_Core_System_Func_3_gen3689651873MethodDeclarations.h"
#include "System_Core_System_Func_3_gen1068606604.h"
#include "System_Core_System_Func_3_gen1068606604MethodDeclarations.h"
#include "System_Core_System_Func_3_gen1694707683.h"
#include "System_Core_System_Func_3_gen1694707683MethodDeclarations.h"
#include "System_Core_System_Func_3_gen698853017.h"
#include "System_Core_System_Func_3_gen698853017MethodDeclarations.h"
#include "System_Core_System_Func_3_gen2236225891.h"
#include "System_Core_System_Func_3_gen2236225891MethodDeclarations.h"
#include "mscorlib_System_DateTime339033936.h"
#include "System_Core_System_Func_3_gen1933728903.h"
#include "System_Core_System_Func_3_gen1933728903MethodDeclarations.h"
#include "System_Core_System_Func_3_gen2695431030.h"
#include "System_Core_System_Func_3_gen2695431030MethodDeclarations.h"
#include "System_Core_System_Func_3_gen2998020836.h"
#include "System_Core_System_Func_3_gen2998020836MethodDeclarations.h"
#include "System_Core_System_Func_3_gen543102568.h"
#include "System_Core_System_Func_3_gen543102568MethodDeclarations.h"
#include "System_Core_System_Func_3_gen3578830837.h"
#include "System_Core_System_Func_3_gen3578830837MethodDeclarations.h"
#include "System_Core_System_Func_3_gen1612416521.h"
#include "System_Core_System_Func_3_gen1612416521MethodDeclarations.h"
#include "System_Core_System_Func_3_gen4248825967.h"
#include "System_Core_System_Func_3_gen4248825967MethodDeclarations.h"
#include "System_Core_System_Func_3_gen2238517600.h"
#include "System_Core_System_Func_3_gen2238517600MethodDeclarations.h"
#include "System_Core_System_Func_3_gen631773672.h"
#include "System_Core_System_Func_3_gen631773672MethodDeclarations.h"
#include "System_Core_System_Func_3_gen3706795343.h"
#include "System_Core_System_Func_3_gen3706795343MethodDeclarations.h"
#include "System_Core_System_Func_3_gen3765090877.h"
#include "System_Core_System_Func_3_gen3765090877MethodDeclarations.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"
#include "System_Core_System_Func_3_gen3064567499.h"
#include "System_Core_System_Func_3_gen3064567499MethodDeclarations.h"
#include "System_Core_System_Func_3_gen3690668578.h"
#include "System_Core_System_Func_3_gen3690668578MethodDeclarations.h"
#include "System_Core_System_Func_3_gen2633863527.h"
#include "System_Core_System_Func_3_gen2633863527MethodDeclarations.h"
#include "System_Core_System_Func_3_gen1892209229.h"
#include "System_Core_System_Func_3_gen1892209229MethodDeclarations.h"
#include "System_Core_System_Func_3_gen2554989993.h"
#include "System_Core_System_Func_3_gen2554989993MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560.h"
#include "System_Core_System_Func_3_gen3148637859.h"
#include "System_Core_System_Func_3_gen3148637859MethodDeclarations.h"
#include "System_Core_System_Func_3_gen2453810542.h"
#include "System_Core_System_Func_3_gen2453810542MethodDeclarations.h"
#include "System_Core_System_Func_3_gen2574913143.h"
#include "System_Core_System_Func_3_gen2574913143MethodDeclarations.h"
#include "System_Core_System_Func_3_gen3782820650.h"
#include "System_Core_System_Func_3_gen3782820650MethodDeclarations.h"
#include "System_Core_System_Func_3_gen255060061.h"
#include "System_Core_System_Func_3_gen255060061MethodDeclarations.h"
#include "System_Core_System_Func_3_gen881161140.h"
#include "System_Core_System_Func_3_gen881161140MethodDeclarations.h"
#include "System_Core_System_Func_3_gen487212563.h"
#include "System_Core_System_Func_3_gen487212563MethodDeclarations.h"
#include "System_Core_System_Func_4_gen3628925182.h"
#include "System_Core_System_Func_4_gen3628925182MethodDeclarations.h"
#include "System_Core_System_Func_5_gen2889627055.h"
#include "System_Core_System_Func_5_gen2889627055MethodDeclarations.h"
#include "System_Core_System_Func_5_gen3557632551.h"
#include "System_Core_System_Func_5_gen3557632551MethodDeclarations.h"
#include "System_Core_System_Func_5_gen714816233.h"
#include "System_Core_System_Func_5_gen714816233MethodDeclarations.h"
#include "System_Core_System_Func_5_gen2200276091.h"
#include "System_Core_System_Func_5_gen2200276091MethodDeclarations.h"
#include "System_Core_System_Func_5_gen875099911.h"
#include "System_Core_System_Func_5_gen875099911MethodDeclarations.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Func`1<System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_1__ctor_m2464158966_gshared (Func_1_t2100990268 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`1<System.Single>::Invoke()
extern "C"  float Func_1_Invoke_m4252412782_gshared (Func_1_t2100990268 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_1_Invoke_m4252412782((Func_1_t2100990268 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef float (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef float (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`1<System.Single>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_1_BeginInvoke_m1122328627_gshared (Func_1_t2100990268 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// TResult System.Func`1<System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  float Func_1_EndInvoke_m3577313448_gshared (Func_1_t2100990268 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(float*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Func`1<UniRx.Unit>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_1__ctor_m2615142501_gshared (Func_1_t3701067285 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`1<UniRx.Unit>::Invoke()
extern "C"  Unit_t2558286038  Func_1_Invoke_m4267058009_gshared (Func_1_t3701067285 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_1_Invoke_m4267058009((Func_1_t3701067285 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Unit_t2558286038  (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Unit_t2558286038  (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`1<UniRx.Unit>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_1_BeginInvoke_m3578428912_gshared (Func_1_t3701067285 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// TResult System.Func`1<UniRx.Unit>::EndInvoke(System.IAsyncResult)
extern "C"  Unit_t2558286038  Func_1_EndInvoke_m3449556943_gshared (Func_1_t3701067285 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Unit_t2558286038 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Func`2<System.Boolean,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m2765952064_gshared (Func_2_t1008118516 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`2<System.Boolean,System.Boolean>::Invoke(T)
extern "C"  bool Func_2_Invoke_m2596463890_gshared (Func_2_t1008118516 * __this, bool ___arg10, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_2_Invoke_m2596463890((Func_2_t1008118516 *)__this->get_prev_9(),___arg10, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, bool ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`2<System.Boolean,System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern const uint32_t Func_2_BeginInvoke_m3525108673_MetadataUsageId;
extern "C"  Il2CppObject * Func_2_BeginInvoke_m3525108673_gshared (Func_2_t1008118516 * __this, bool ___arg10, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_2_BeginInvoke_m3525108673_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &___arg10);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TResult System.Func`2<System.Boolean,System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  bool Func_2_EndInvoke_m1722392066_gshared (Func_2_t1008118516 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m1217527044_gshared (Func_2_t1844407557 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Boolean>::Invoke(T)
extern "C"  bool Func_2_Invoke_m3642283230_gshared (Func_2_t1844407557 * __this, KeyValuePair_2_t3312956448  ___arg10, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_2_Invoke_m3642283230((Func_2_t1844407557 *)__this->get_prev_9(),___arg10, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, KeyValuePair_2_t3312956448  ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, KeyValuePair_2_t3312956448  ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* KeyValuePair_2_t3312956448_il2cpp_TypeInfo_var;
extern const uint32_t Func_2_BeginInvoke_m2573919629_MetadataUsageId;
extern "C"  Il2CppObject * Func_2_BeginInvoke_m2573919629_gshared (Func_2_t1844407557 * __this, KeyValuePair_2_t3312956448  ___arg10, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_2_BeginInvoke_m2573919629_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(KeyValuePair_2_t3312956448_il2cpp_TypeInfo_var, &___arg10);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TResult System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  bool Func_2_EndInvoke_m3946825142_gshared (Func_2_t1844407557 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m1887411215_gshared (Func_2_t2470508636 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Object>::Invoke(T)
extern "C"  Il2CppObject * Func_2_Invoke_m3228321655_gshared (Func_2_t2470508636 * __this, KeyValuePair_2_t3312956448  ___arg10, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_2_Invoke_m3228321655((Func_2_t2470508636 *)__this->get_prev_9(),___arg10, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, KeyValuePair_2_t3312956448  ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, KeyValuePair_2_t3312956448  ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* KeyValuePair_2_t3312956448_il2cpp_TypeInfo_var;
extern const uint32_t Func_2_BeginInvoke_m2365319082_MetadataUsageId;
extern "C"  Il2CppObject * Func_2_BeginInvoke_m2365319082_gshared (Func_2_t2470508636 * __this, KeyValuePair_2_t3312956448  ___arg10, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_2_BeginInvoke_m2365319082_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(KeyValuePair_2_t3312956448_il2cpp_TypeInfo_var, &___arg10);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TResult System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Func_2_EndInvoke_m3637779645_gshared (Func_2_t2470508636 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Func`2<System.Double,System.Double>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m104617448_gshared (Func_2_t2869950768 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`2<System.Double,System.Double>::Invoke(T)
extern "C"  double Func_2_Invoke_m2427995386_gshared (Func_2_t2869950768 * __this, double ___arg10, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_2_Invoke_m2427995386((Func_2_t2869950768 *)__this->get_prev_9(),___arg10, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef double (*FunctionPointerType) (Il2CppObject *, void* __this, double ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef double (*FunctionPointerType) (void* __this, double ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`2<System.Double,System.Double>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern const uint32_t Func_2_BeginInvoke_m1017322281_MetadataUsageId;
extern "C"  Il2CppObject * Func_2_BeginInvoke_m1017322281_gshared (Func_2_t2869950768 * __this, double ___arg10, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_2_BeginInvoke_m1017322281_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Double_t534516614_il2cpp_TypeInfo_var, &___arg10);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TResult System.Func`2<System.Double,System.Double>::EndInvoke(System.IAsyncResult)
extern "C"  double Func_2_EndInvoke_m3041312538_gshared (Func_2_t2869950768 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(double*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Func`2<System.Double,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m489610042_gshared (Func_2_t3172540574 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`2<System.Double,System.Object>::Invoke(T)
extern "C"  Il2CppObject * Func_2_Invoke_m1334556520_gshared (Func_2_t3172540574 * __this, double ___arg10, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_2_Invoke_m1334556520((Func_2_t3172540574 *)__this->get_prev_9(),___arg10, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, double ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, double ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`2<System.Double,System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern const uint32_t Func_2_BeginInvoke_m745473943_MetadataUsageId;
extern "C"  Il2CppObject * Func_2_BeginInvoke_m745473943_gshared (Func_2_t3172540574 * __this, double ___arg10, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_2_BeginInvoke_m745473943_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Double_t534516614_il2cpp_TypeInfo_var, &___arg10);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TResult System.Func`2<System.Double,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Func_2_EndInvoke_m3806641516_gshared (Func_2_t3172540574 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Func`2<System.Int32,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m972235686_gshared (Func_2_t3308141622 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`2<System.Int32,System.Boolean>::Invoke(T)
extern "C"  bool Func_2_Invoke_m3440883198_gshared (Func_2_t3308141622 * __this, int32_t ___arg10, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_2_Invoke_m3440883198((Func_2_t3308141622 *)__this->get_prev_9(),___arg10, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, int32_t ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`2<System.Int32,System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t Func_2_BeginInvoke_m2685970075_MetadataUsageId;
extern "C"  Il2CppObject * Func_2_BeginInvoke_m2685970075_gshared (Func_2_t3308141622 * __this, int32_t ___arg10, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_2_BeginInvoke_m2685970075_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &___arg10);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TResult System.Func`2<System.Int32,System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  bool Func_2_EndInvoke_m2653525224_gshared (Func_2_t3308141622 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Func`2<System.Int32,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m908529232_gshared (Func_2_t1649583772 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`2<System.Int32,System.Int32>::Invoke(T)
extern "C"  int32_t Func_2_Invoke_m720598802_gshared (Func_2_t1649583772 * __this, int32_t ___arg10, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_2_Invoke_m720598802((Func_2_t1649583772 *)__this->get_prev_9(),___arg10, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`2<System.Int32,System.Int32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t Func_2_BeginInvoke_m3530495169_MetadataUsageId;
extern "C"  Il2CppObject * Func_2_BeginInvoke_m3530495169_gshared (Func_2_t1649583772 * __this, int32_t ___arg10, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_2_BeginInvoke_m3530495169_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &___arg10);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TResult System.Func`2<System.Int32,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Func_2_EndInvoke_m417876994_gshared (Func_2_t1649583772 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Func`2<System.Int32,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m109266461_gshared (Func_2_t3934242701 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`2<System.Int32,System.Object>::Invoke(T)
extern "C"  Il2CppObject * Func_2_Invoke_m3930687529_gshared (Func_2_t3934242701 * __this, int32_t ___arg10, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_2_Invoke_m3930687529((Func_2_t3934242701 *)__this->get_prev_9(),___arg10, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, int32_t ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`2<System.Int32,System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t Func_2_BeginInvoke_m1814744284_MetadataUsageId;
extern "C"  Il2CppObject * Func_2_BeginInvoke_m1814744284_gshared (Func_2_t3934242701 * __this, int32_t ___arg10, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_2_BeginInvoke_m1814744284_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &___arg10);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TResult System.Func`2<System.Int32,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Func_2_EndInvoke_m1656397643_gshared (Func_2_t3934242701 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Func`2<System.Int32,UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m773707327_gshared (Func_2_t390344745 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`2<System.Int32,UnityEngine.Color>::Invoke(T)
extern "C"  Color_t1588175760  Func_2_Invoke_m3571737047_gshared (Func_2_t390344745 * __this, int32_t ___arg10, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_2_Invoke_m3571737047((Func_2_t390344745 *)__this->get_prev_9(),___arg10, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Color_t1588175760  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Color_t1588175760  (*FunctionPointerType) (void* __this, int32_t ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`2<System.Int32,UnityEngine.Color>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t Func_2_BeginInvoke_m2721947530_MetadataUsageId;
extern "C"  Il2CppObject * Func_2_BeginInvoke_m2721947530_gshared (Func_2_t390344745 * __this, int32_t ___arg10, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_2_BeginInvoke_m2721947530_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &___arg10);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TResult System.Func`2<System.Int32,UnityEngine.Color>::EndInvoke(System.IAsyncResult)
extern "C"  Color_t1588175760  Func_2_EndInvoke_m3329637981_gshared (Func_2_t390344745 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Color_t1588175760 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Func`2<System.Int64,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m2519360593_gshared (Func_2_t2251336571 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`2<System.Int64,System.Boolean>::Invoke(T)
extern "C"  bool Func_2_Invoke_m1215653549_gshared (Func_2_t2251336571 * __this, int64_t ___arg10, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_2_Invoke_m1215653549((Func_2_t2251336571 *)__this->get_prev_9(),___arg10, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int64_t ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, int64_t ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`2<System.Int64,System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Int64_t2847414882_il2cpp_TypeInfo_var;
extern const uint32_t Func_2_BeginInvoke_m2900970972_MetadataUsageId;
extern "C"  Il2CppObject * Func_2_BeginInvoke_m2900970972_gshared (Func_2_t2251336571 * __this, int64_t ___arg10, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_2_BeginInvoke_m2900970972_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int64_t2847414882_il2cpp_TypeInfo_var, &___arg10);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TResult System.Func`2<System.Int64,System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  bool Func_2_EndInvoke_m1916386119_gshared (Func_2_t2251336571 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Func`2<System.Int64,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m221094879_gshared (Func_2_t592778721 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`2<System.Int64,System.Int32>::Invoke(T)
extern "C"  int32_t Func_2_Invoke_m3667480467_gshared (Func_2_t592778721 * __this, int64_t ___arg10, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_2_Invoke_m3667480467((Func_2_t592778721 *)__this->get_prev_9(),___arg10, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, int64_t ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int64_t ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`2<System.Int64,System.Int32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Int64_t2847414882_il2cpp_TypeInfo_var;
extern const uint32_t Func_2_BeginInvoke_m974297154_MetadataUsageId;
extern "C"  Il2CppObject * Func_2_BeginInvoke_m974297154_gshared (Func_2_t592778721 * __this, int64_t ___arg10, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_2_BeginInvoke_m974297154_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int64_t2847414882_il2cpp_TypeInfo_var, &___arg10);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TResult System.Func`2<System.Int64,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Func_2_EndInvoke_m2973531681_gshared (Func_2_t592778721 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Func`2<System.Int64,System.Int64>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m1685010416_gshared (Func_2_t592778816 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`2<System.Int64,System.Int64>::Invoke(T)
extern "C"  int64_t Func_2_Invoke_m1563998578_gshared (Func_2_t592778816 * __this, int64_t ___arg10, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_2_Invoke_m1563998578((Func_2_t592778816 *)__this->get_prev_9(),___arg10, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int64_t (*FunctionPointerType) (Il2CppObject *, void* __this, int64_t ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int64_t (*FunctionPointerType) (void* __this, int64_t ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`2<System.Int64,System.Int64>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Int64_t2847414882_il2cpp_TypeInfo_var;
extern const uint32_t Func_2_BeginInvoke_m2732226337_MetadataUsageId;
extern "C"  Il2CppObject * Func_2_BeginInvoke_m2732226337_gshared (Func_2_t592778816 * __this, int64_t ___arg10, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_2_BeginInvoke_m2732226337_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int64_t2847414882_il2cpp_TypeInfo_var, &___arg10);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TResult System.Func`2<System.Int64,System.Int64>::EndInvoke(System.IAsyncResult)
extern "C"  int64_t Func_2_EndInvoke_m1611023778_gshared (Func_2_t592778816 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int64_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Func`2<System.Int64,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m165973854_gshared (Func_2_t2877437650 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`2<System.Int64,System.Object>::Invoke(T)
extern "C"  Il2CppObject * Func_2_Invoke_m794738632_gshared (Func_2_t2877437650 * __this, int64_t ___arg10, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_2_Invoke_m794738632((Func_2_t2877437650 *)__this->get_prev_9(),___arg10, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, int64_t ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, int64_t ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`2<System.Int64,System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Int64_t2847414882_il2cpp_TypeInfo_var;
extern const uint32_t Func_2_BeginInvoke_m4176984443_MetadataUsageId;
extern "C"  Il2CppObject * Func_2_BeginInvoke_m4176984443_gshared (Func_2_t2877437650 * __this, int64_t ___arg10, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_2_BeginInvoke_m4176984443_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int64_t2847414882_il2cpp_TypeInfo_var, &___arg10);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TResult System.Func`2<System.Int64,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Func_2_EndInvoke_m3572281612_gshared (Func_2_t2877437650 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Func`2<System.Int64,UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m31193364_gshared (Func_2_t1270693722 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`2<System.Int64,UnityEngine.Vector2>::Invoke(T)
extern "C"  Vector2_t3525329788  Func_2_Invoke_m282123490_gshared (Func_2_t1270693722 * __this, int64_t ___arg10, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_2_Invoke_m282123490((Func_2_t1270693722 *)__this->get_prev_9(),___arg10, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Vector2_t3525329788  (*FunctionPointerType) (Il2CppObject *, void* __this, int64_t ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Vector2_t3525329788  (*FunctionPointerType) (void* __this, int64_t ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`2<System.Int64,UnityEngine.Vector2>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Int64_t2847414882_il2cpp_TypeInfo_var;
extern const uint32_t Func_2_BeginInvoke_m1292894997_MetadataUsageId;
extern "C"  Il2CppObject * Func_2_BeginInvoke_m1292894997_gshared (Func_2_t1270693722 * __this, int64_t ___arg10, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_2_BeginInvoke_m1292894997_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int64_t2847414882_il2cpp_TypeInfo_var, &___arg10);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TResult System.Func`2<System.Int64,UnityEngine.Vector2>::EndInvoke(System.IAsyncResult)
extern "C"  Vector2_t3525329788  Func_2_EndInvoke_m2121508914_gshared (Func_2_t1270693722 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Vector2_t3525329788 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Func`2<System.Object,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m563515303_gshared (Func_2_t1509682273 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`2<System.Object,System.Boolean>::Invoke(T)
extern "C"  bool Func_2_Invoke_m1882130143_gshared (Func_2_t1509682273 * __this, Il2CppObject * ___arg10, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_2_Invoke_m1882130143((Func_2_t1509682273 *)__this->get_prev_9(),___arg10, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`2<System.Object,System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_2_BeginInvoke_m1852288274_gshared (Func_2_t1509682273 * __this, Il2CppObject * ___arg10, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___arg10;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TResult System.Func`2<System.Object,System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  bool Func_2_EndInvoke_m1659014741_gshared (Func_2_t1509682273 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Func`2<System.Object,System.Double>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m3559531450_gshared (Func_2_t1833193546 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`2<System.Object,System.Double>::Invoke(T)
extern "C"  double Func_2_Invoke_m3018055400_gshared (Func_2_t1833193546 * __this, Il2CppObject * ___arg10, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_2_Invoke_m3018055400((Func_2_t1833193546 *)__this->get_prev_9(),___arg10, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef double (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef double (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef double (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`2<System.Object,System.Double>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_2_BeginInvoke_m258584343_gshared (Func_2_t1833193546 * __this, Il2CppObject * ___arg10, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___arg10;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TResult System.Func`2<System.Object,System.Double>::EndInvoke(System.IAsyncResult)
extern "C"  double Func_2_EndInvoke_m3352839660_gshared (Func_2_t1833193546 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(double*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Func`2<System.Object,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m3664646529_gshared (Func_2_t4146091719 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`2<System.Object,System.Int32>::Invoke(T)
extern "C"  int32_t Func_2_Invoke_m3426833477_gshared (Func_2_t4146091719 * __this, Il2CppObject * ___arg10, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_2_Invoke_m3426833477((Func_2_t4146091719 *)__this->get_prev_9(),___arg10, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`2<System.Object,System.Int32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_2_BeginInvoke_m3471527160_gshared (Func_2_t4146091719 * __this, Il2CppObject * ___arg10, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___arg10;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TResult System.Func`2<System.Object,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Func_2_EndInvoke_m2021309615_gshared (Func_2_t4146091719 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Func`2<System.Object,System.Int64>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m4162203778_gshared (Func_2_t4146091814 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`2<System.Object,System.Int64>::Invoke(T)
extern "C"  int64_t Func_2_Invoke_m1323351588_gshared (Func_2_t4146091814 * __this, Il2CppObject * ___arg10, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_2_Invoke_m1323351588((Func_2_t4146091814 *)__this->get_prev_9(),___arg10, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int64_t (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef int64_t (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int64_t (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`2<System.Object,System.Int64>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_2_BeginInvoke_m934489047_gshared (Func_2_t4146091814 * __this, Il2CppObject * ___arg10, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___arg10;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TResult System.Func`2<System.Object,System.Int64>::EndInvoke(System.IAsyncResult)
extern "C"  int64_t Func_2_EndInvoke_m658801712_gshared (Func_2_t4146091814 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int64_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Func`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m3944524044_gshared (Func_2_t2135783352 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`2<System.Object,System.Object>::Invoke(T)
extern "C"  Il2CppObject * Func_2_Invoke_m1924616534_gshared (Func_2_t2135783352 * __this, Il2CppObject * ___arg10, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_2_Invoke_m1924616534((Func_2_t2135783352 *)__this->get_prev_9(),___arg10, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`2<System.Object,System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_2_BeginInvoke_m4281703301_gshared (Func_2_t2135783352 * __this, Il2CppObject * ___arg10, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___arg10;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TResult System.Func`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Func_2_EndInvoke_m4118168638_gshared (Func_2_t2135783352 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Func`2<System.Object,System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m4057262499_gshared (Func_2_t2256885953 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`2<System.Object,System.Single>::Invoke(T)
extern "C"  float Func_2_Invoke_m3938512095_gshared (Func_2_t2256885953 * __this, Il2CppObject * ___arg10, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_2_Invoke_m3938512095((Func_2_t2256885953 *)__this->get_prev_9(),___arg10, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef float (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef float (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef float (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`2<System.Object,System.Single>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_2_BeginInvoke_m905918990_gshared (Func_2_t2256885953 * __this, Il2CppObject * ___arg10, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___arg10;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TResult System.Func`2<System.Object,System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  float Func_2_EndInvoke_m43804757_gshared (Func_2_t2256885953 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(float*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Func`2<System.Object,UniRx.Unit>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m298322708_gshared (Func_2_t3856962970 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`2<System.Object,UniRx.Unit>::Invoke(T)
extern "C"  Unit_t2558286038  Func_2_Invoke_m4026479570_gshared (Func_2_t3856962970 * __this, Il2CppObject * ___arg10, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_2_Invoke_m4026479570((Func_2_t3856962970 *)__this->get_prev_9(),___arg10, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Unit_t2558286038  (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Unit_t2558286038  (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Unit_t2558286038  (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`2<System.Object,UniRx.Unit>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_2_BeginInvoke_m3097046021_gshared (Func_2_t3856962970 * __this, Il2CppObject * ___arg10, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___arg10;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TResult System.Func`2<System.Object,UniRx.Unit>::EndInvoke(System.IAsyncResult)
extern "C"  Unit_t2558286038  Func_2_EndInvoke_m2450917186_gshared (Func_2_t3856962970 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Unit_t2558286038 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Func`2<System.Single,System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m2021156074_gshared (Func_2_t3464793460 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`2<System.Single,System.Single>::Invoke(T)
extern "C"  float Func_2_Invoke_m248194216_gshared (Func_2_t3464793460 * __this, float ___arg10, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_2_Invoke_m248194216((Func_2_t3464793460 *)__this->get_prev_9(),___arg10, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef float (*FunctionPointerType) (Il2CppObject *, void* __this, float ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef float (*FunctionPointerType) (void* __this, float ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`2<System.Single,System.Single>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Single_t958209021_il2cpp_TypeInfo_var;
extern const uint32_t Func_2_BeginInvoke_m3674748119_MetadataUsageId;
extern "C"  Il2CppObject * Func_2_BeginInvoke_m3674748119_gshared (Func_2_t3464793460 * __this, float ___arg10, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_2_BeginInvoke_m3674748119_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Single_t958209021_il2cpp_TypeInfo_var, &___arg10);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TResult System.Func`2<System.Single,System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  float Func_2_EndInvoke_m48896044_gshared (Func_2_t3464793460 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(float*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Func`2<System.UIntPtr,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m161237399_gshared (Func_2_t1403134703 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`2<System.UIntPtr,System.Object>::Invoke(T)
extern "C"  Il2CppObject * Func_2_Invoke_m527793903_gshared (Func_2_t1403134703 * __this, UIntPtr_t  ___arg10, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_2_Invoke_m527793903((Func_2_t1403134703 *)__this->get_prev_9(),___arg10, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, UIntPtr_t  ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, UIntPtr_t  ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`2<System.UIntPtr,System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* UIntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t Func_2_BeginInvoke_m3185104162_MetadataUsageId;
extern "C"  Il2CppObject * Func_2_BeginInvoke_m3185104162_gshared (Func_2_t1403134703 * __this, UIntPtr_t  ___arg10, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_2_BeginInvoke_m3185104162_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UIntPtr_t_il2cpp_TypeInfo_var, &___arg10);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TResult System.Func`2<System.UIntPtr,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Func_2_EndInvoke_m858964037_gshared (Func_2_t1403134703 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Func`2<UniRx.Tuple`2<System.Object,System.Object>,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m1978381903_gshared (Func_2_t1101125214 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`2<UniRx.Tuple`2<System.Object,System.Object>,System.Boolean>::Invoke(T)
extern "C"  bool Func_2_Invoke_m621375027_gshared (Func_2_t1101125214 * __this, Tuple_2_t369261819  ___arg10, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_2_Invoke_m621375027((Func_2_t1101125214 *)__this->get_prev_9(),___arg10, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Tuple_2_t369261819  ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Tuple_2_t369261819  ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`2<UniRx.Tuple`2<System.Object,System.Object>,System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Tuple_2_t369261819_il2cpp_TypeInfo_var;
extern const uint32_t Func_2_BeginInvoke_m2552012514_MetadataUsageId;
extern "C"  Il2CppObject * Func_2_BeginInvoke_m2552012514_gshared (Func_2_t1101125214 * __this, Tuple_2_t369261819  ___arg10, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_2_BeginInvoke_m2552012514_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Tuple_2_t369261819_il2cpp_TypeInfo_var, &___arg10);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TResult System.Func`2<UniRx.Tuple`2<System.Object,System.Object>,System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  bool Func_2_EndInvoke_m205505409_gshared (Func_2_t1101125214 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Func`2<UniRx.Tuple`2<System.Object,System.Object>,UniRx.Tuple`2<System.Object,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m930625584_gshared (Func_2_t1259381692 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`2<UniRx.Tuple`2<System.Object,System.Object>,UniRx.Tuple`2<System.Object,System.Object>>::Invoke(T)
extern "C"  Tuple_2_t369261819  Func_2_Invoke_m2703245362_gshared (Func_2_t1259381692 * __this, Tuple_2_t369261819  ___arg10, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_2_Invoke_m2703245362((Func_2_t1259381692 *)__this->get_prev_9(),___arg10, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Tuple_2_t369261819  (*FunctionPointerType) (Il2CppObject *, void* __this, Tuple_2_t369261819  ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Tuple_2_t369261819  (*FunctionPointerType) (void* __this, Tuple_2_t369261819  ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`2<UniRx.Tuple`2<System.Object,System.Object>,UniRx.Tuple`2<System.Object,System.Object>>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Tuple_2_t369261819_il2cpp_TypeInfo_var;
extern const uint32_t Func_2_BeginInvoke_m2767809761_MetadataUsageId;
extern "C"  Il2CppObject * Func_2_BeginInvoke_m2767809761_gshared (Func_2_t1259381692 * __this, Tuple_2_t369261819  ___arg10, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_2_BeginInvoke_m2767809761_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Tuple_2_t369261819_il2cpp_TypeInfo_var, &___arg10);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TResult System.Func`2<UniRx.Tuple`2<System.Object,System.Object>,UniRx.Tuple`2<System.Object,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  Tuple_2_t369261819  Func_2_EndInvoke_m1182430946_gshared (Func_2_t1259381692 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Tuple_2_t369261819 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Func`2<UniRx.Unit,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m285823539_gshared (Func_2_t2766110903 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`2<UniRx.Unit,System.Boolean>::Invoke(T)
extern "C"  bool Func_2_Invoke_m1314409151_gshared (Func_2_t2766110903 * __this, Unit_t2558286038  ___arg10, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_2_Invoke_m1314409151((Func_2_t2766110903 *)__this->get_prev_9(),___arg10, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Unit_t2558286038  ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Unit_t2558286038  ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`2<UniRx.Unit,System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern const uint32_t Func_2_BeginInvoke_m3447846766_MetadataUsageId;
extern "C"  Il2CppObject * Func_2_BeginInvoke_m3447846766_gshared (Func_2_t2766110903 * __this, Unit_t2558286038  ___arg10, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_2_BeginInvoke_m3447846766_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Unit_t2558286038_il2cpp_TypeInfo_var, &___arg10);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TResult System.Func`2<UniRx.Unit,System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  bool Func_2_EndInvoke_m3711202933_gshared (Func_2_t2766110903 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Func`2<UniRx.Unit,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m2719813104_gshared (Func_2_t3392211982 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`2<UniRx.Unit,System.Object>::Invoke(T)
extern "C"  Il2CppObject * Func_2_Invoke_m2321944950_gshared (Func_2_t3392211982 * __this, Unit_t2558286038  ___arg10, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_2_Invoke_m2321944950((Func_2_t3392211982 *)__this->get_prev_9(),___arg10, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Unit_t2558286038  ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Unit_t2558286038  ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`2<UniRx.Unit,System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern const uint32_t Func_2_BeginInvoke_m3501888937_MetadataUsageId;
extern "C"  Il2CppObject * Func_2_BeginInvoke_m3501888937_gshared (Func_2_t3392211982 * __this, Unit_t2558286038  ___arg10, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_2_BeginInvoke_m3501888937_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Unit_t2558286038_il2cpp_TypeInfo_var, &___arg10);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TResult System.Func`2<UniRx.Unit,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Func_2_EndInvoke_m859232286_gshared (Func_2_t3392211982 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Func`2<UniRx.Unit,UniRx.Unit>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m3136843952_gshared (Func_2_t818424304 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`2<UniRx.Unit,UniRx.Unit>::Invoke(T)
extern "C"  Unit_t2558286038  Func_2_Invoke_m1982162866_gshared (Func_2_t818424304 * __this, Unit_t2558286038  ___arg10, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_2_Invoke_m1982162866((Func_2_t818424304 *)__this->get_prev_9(),___arg10, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Unit_t2558286038  (*FunctionPointerType) (Il2CppObject *, void* __this, Unit_t2558286038  ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Unit_t2558286038  (*FunctionPointerType) (void* __this, Unit_t2558286038  ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`2<UniRx.Unit,UniRx.Unit>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern const uint32_t Func_2_BeginInvoke_m2437874785_MetadataUsageId;
extern "C"  Il2CppObject * Func_2_BeginInvoke_m2437874785_gshared (Func_2_t818424304 * __this, Unit_t2558286038  ___arg10, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_2_BeginInvoke_m2437874785_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Unit_t2558286038_il2cpp_TypeInfo_var, &___arg10);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TResult System.Func`2<UniRx.Unit,UniRx.Unit>::EndInvoke(System.IAsyncResult)
extern "C"  Unit_t2558286038  Func_2_EndInvoke_m3638624098_gshared (Func_2_t818424304 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Unit_t2558286038 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Func`2<UnityEngine.Color,UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m424454832_gshared (Func_2_t2057442760 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`2<UnityEngine.Color,UnityEngine.Color>::Invoke(T)
extern "C"  Color_t1588175760  Func_2_Invoke_m985981234_gshared (Func_2_t2057442760 * __this, Color_t1588175760  ___arg10, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_2_Invoke_m985981234((Func_2_t2057442760 *)__this->get_prev_9(),___arg10, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Color_t1588175760  (*FunctionPointerType) (Il2CppObject *, void* __this, Color_t1588175760  ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Color_t1588175760  (*FunctionPointerType) (void* __this, Color_t1588175760  ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`2<UnityEngine.Color,UnityEngine.Color>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Color_t1588175760_il2cpp_TypeInfo_var;
extern const uint32_t Func_2_BeginInvoke_m311746913_MetadataUsageId;
extern "C"  Il2CppObject * Func_2_BeginInvoke_m311746913_gshared (Func_2_t2057442760 * __this, Color_t1588175760  ___arg10, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_2_BeginInvoke_m311746913_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Color_t1588175760_il2cpp_TypeInfo_var, &___arg10);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TResult System.Func`2<UnityEngine.Color,UnityEngine.Color>::EndInvoke(System.IAsyncResult)
extern "C"  Color_t1588175760  Func_2_EndInvoke_m4122244578_gshared (Func_2_t2057442760 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Color_t1588175760 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Func`2<UnityEngine.Touch,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m880807361_gshared (Func_2_t1874381577 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`2<UnityEngine.Touch,System.Boolean>::Invoke(T)
extern "C"  bool Func_2_Invoke_m774449045_gshared (Func_2_t1874381577 * __this, Touch_t1603883884  ___arg10, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_2_Invoke_m774449045((Func_2_t1874381577 *)__this->get_prev_9(),___arg10, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Touch_t1603883884  ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Touch_t1603883884  ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`2<UnityEngine.Touch,System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Touch_t1603883884_il2cpp_TypeInfo_var;
extern const uint32_t Func_2_BeginInvoke_m3537133256_MetadataUsageId;
extern "C"  Il2CppObject * Func_2_BeginInvoke_m3537133256_gshared (Func_2_t1874381577 * __this, Touch_t1603883884  ___arg10, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_2_BeginInvoke_m3537133256_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Touch_t1603883884_il2cpp_TypeInfo_var, &___arg10);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TResult System.Func`2<UnityEngine.Touch,System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  bool Func_2_EndInvoke_m1603214175_gshared (Func_2_t1874381577 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Func`2<UnityEngine.Vector2,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m647878481_gshared (Func_2_t4239542457 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`2<UnityEngine.Vector2,System.Boolean>::Invoke(T)
extern "C"  bool Func_2_Invoke_m3779524485_gshared (Func_2_t4239542457 * __this, Vector2_t3525329788  ___arg10, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_2_Invoke_m3779524485((Func_2_t4239542457 *)__this->get_prev_9(),___arg10, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Vector2_t3525329788  ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Vector2_t3525329788  ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`2<UnityEngine.Vector2,System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Vector2_t3525329788_il2cpp_TypeInfo_var;
extern const uint32_t Func_2_BeginInvoke_m515157560_MetadataUsageId;
extern "C"  Il2CppObject * Func_2_BeginInvoke_m515157560_gshared (Func_2_t4239542457 * __this, Vector2_t3525329788  ___arg10, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_2_BeginInvoke_m515157560_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector2_t3525329788_il2cpp_TypeInfo_var, &___arg10);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TResult System.Func`2<UnityEngine.Vector2,System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  bool Func_2_EndInvoke_m4023886191_gshared (Func_2_t4239542457 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Func`3<System.Boolean,System.Boolean,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_3__ctor_m3670658982_gshared (Func_3_t3063550794 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`3<System.Boolean,System.Boolean,System.Boolean>::Invoke(T1,T2)
extern "C"  bool Func_3_Invoke_m3734720759_gshared (Func_3_t3063550794 * __this, bool ___arg10, bool ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_3_Invoke_m3734720759((Func_3_t3063550794 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___arg10, bool ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, bool ___arg10, bool ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`3<System.Boolean,System.Boolean,System.Boolean>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern const uint32_t Func_3_BeginInvoke_m2554513532_MetadataUsageId;
extern "C"  Il2CppObject * Func_3_BeginInvoke_m2554513532_gshared (Func_3_t3063550794 * __this, bool ___arg10, bool ___arg21, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_3_BeginInvoke_m2554513532_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &___arg21);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TResult System.Func`3<System.Boolean,System.Boolean,System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  bool Func_3_EndInvoke_m1120760516_gshared (Func_3_t3063550794 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Func`3<System.Boolean,System.Boolean,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_3__ctor_m4282269245_gshared (Func_3_t3689651873 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`3<System.Boolean,System.Boolean,System.Object>::Invoke(T1,T2)
extern "C"  Il2CppObject * Func_3_Invoke_m3636733548_gshared (Func_3_t3689651873 * __this, bool ___arg10, bool ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_3_Invoke_m3636733548((Func_3_t3689651873 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___arg10, bool ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, bool ___arg10, bool ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`3<System.Boolean,System.Boolean,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern const uint32_t Func_3_BeginInvoke_m3842807277_MetadataUsageId;
extern "C"  Il2CppObject * Func_3_BeginInvoke_m3842807277_gshared (Func_3_t3689651873 * __this, bool ___arg10, bool ___arg21, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_3_BeginInvoke_m3842807277_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &___arg21);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TResult System.Func`3<System.Boolean,System.Boolean,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Func_3_EndInvoke_m360027631_gshared (Func_3_t3689651873 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Func`3<System.Boolean,System.Int32,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_3__ctor_m1400950140_gshared (Func_3_t1068606604 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`3<System.Boolean,System.Int32,System.Boolean>::Invoke(T1,T2)
extern "C"  bool Func_3_Invoke_m3964109009_gshared (Func_3_t1068606604 * __this, bool ___arg10, int32_t ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_3_Invoke_m3964109009((Func_3_t1068606604 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___arg10, int32_t ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, bool ___arg10, int32_t ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`3<System.Boolean,System.Int32,System.Boolean>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t Func_3_BeginInvoke_m3018622678_MetadataUsageId;
extern "C"  Il2CppObject * Func_3_BeginInvoke_m3018622678_gshared (Func_3_t1068606604 * __this, bool ___arg10, int32_t ___arg21, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_3_BeginInvoke_m3018622678_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &___arg21);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TResult System.Func`3<System.Boolean,System.Int32,System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  bool Func_3_EndInvoke_m4016026154_gshared (Func_3_t1068606604 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Func`3<System.Boolean,System.Int32,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_3__ctor_m3971538071_gshared (Func_3_t1694707683 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`3<System.Boolean,System.Int32,System.Object>::Invoke(T1,T2)
extern "C"  Il2CppObject * Func_3_Invoke_m457544530_gshared (Func_3_t1694707683 * __this, bool ___arg10, int32_t ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_3_Invoke_m457544530((Func_3_t1694707683 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___arg10, int32_t ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, bool ___arg10, int32_t ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`3<System.Boolean,System.Int32,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t Func_3_BeginInvoke_m3026494547_MetadataUsageId;
extern "C"  Il2CppObject * Func_3_BeginInvoke_m3026494547_gshared (Func_3_t1694707683 * __this, bool ___arg10, int32_t ___arg21, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_3_BeginInvoke_m3026494547_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &___arg21);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TResult System.Func`3<System.Boolean,System.Int32,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Func_3_EndInvoke_m730517961_gshared (Func_3_t1694707683 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Func`3<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_3__ctor_m3649639326_gshared (Func_3_t698853017 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`3<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Object,System.Object>::Invoke(T1,T2)
extern "C"  Il2CppObject * Func_3_Invoke_m1601313647_gshared (Func_3_t698853017 * __this, KeyValuePair_2_t3312956448  ___arg10, Il2CppObject * ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_3_Invoke_m1601313647((Func_3_t698853017 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, KeyValuePair_2_t3312956448  ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, KeyValuePair_2_t3312956448  ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`3<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Object,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* KeyValuePair_2_t3312956448_il2cpp_TypeInfo_var;
extern const uint32_t Func_3_BeginInvoke_m3113524596_MetadataUsageId;
extern "C"  Il2CppObject * Func_3_BeginInvoke_m3113524596_gshared (Func_3_t698853017 * __this, KeyValuePair_2_t3312956448  ___arg10, Il2CppObject * ___arg21, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_3_BeginInvoke_m3113524596_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(KeyValuePair_2_t3312956448_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = ___arg21;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TResult System.Func`3<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Func_3_EndInvoke_m159770444_gshared (Func_3_t698853017 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Func`3<System.DateTime,System.Int64,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_3__ctor_m235171373_gshared (Func_3_t2236225891 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`3<System.DateTime,System.Int64,System.Object>::Invoke(T1,T2)
extern "C"  Il2CppObject * Func_3_Invoke_m2850141056_gshared (Func_3_t2236225891 * __this, DateTime_t339033936  ___arg10, int64_t ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_3_Invoke_m2850141056((Func_3_t2236225891 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, DateTime_t339033936  ___arg10, int64_t ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, DateTime_t339033936  ___arg10, int64_t ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`3<System.DateTime,System.Int64,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* DateTime_t339033936_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t2847414882_il2cpp_TypeInfo_var;
extern const uint32_t Func_3_BeginInvoke_m3110739589_MetadataUsageId;
extern "C"  Il2CppObject * Func_3_BeginInvoke_m3110739589_gshared (Func_3_t2236225891 * __this, DateTime_t339033936  ___arg10, int64_t ___arg21, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_3_BeginInvoke_m3110739589_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(DateTime_t339033936_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = Box(Int64_t2847414882_il2cpp_TypeInfo_var, &___arg21);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TResult System.Func`3<System.DateTime,System.Int64,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Func_3_EndInvoke_m3068744539_gshared (Func_3_t2236225891 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Func`3<System.Double,System.Double,System.Double>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_3__ctor_m2706065971_gshared (Func_3_t1933728903 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`3<System.Double,System.Double,System.Double>::Invoke(T1,T2)
extern "C"  double Func_3_Invoke_m2760899878_gshared (Func_3_t1933728903 * __this, double ___arg10, double ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_3_Invoke_m2760899878((Func_3_t1933728903 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef double (*FunctionPointerType) (Il2CppObject *, void* __this, double ___arg10, double ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef double (*FunctionPointerType) (void* __this, double ___arg10, double ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`3<System.Double,System.Double,System.Double>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern const uint32_t Func_3_BeginInvoke_m3522538023_MetadataUsageId;
extern "C"  Il2CppObject * Func_3_BeginInvoke_m3522538023_gshared (Func_3_t1933728903 * __this, double ___arg10, double ___arg21, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_3_BeginInvoke_m3522538023_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Double_t534516614_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = Box(Double_t534516614_il2cpp_TypeInfo_var, &___arg21);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TResult System.Func`3<System.Double,System.Double,System.Double>::EndInvoke(System.IAsyncResult)
extern "C"  double Func_3_EndInvoke_m3188640373_gshared (Func_3_t1933728903 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(double*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Func`3<System.Double,System.Int32,System.Double>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_3__ctor_m2717685536_gshared (Func_3_t2695431030 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`3<System.Double,System.Int32,System.Double>::Invoke(T1,T2)
extern "C"  double Func_3_Invoke_m12286525_gshared (Func_3_t2695431030 * __this, double ___arg10, int32_t ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_3_Invoke_m12286525((Func_3_t2695431030 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef double (*FunctionPointerType) (Il2CppObject *, void* __this, double ___arg10, int32_t ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef double (*FunctionPointerType) (void* __this, double ___arg10, int32_t ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`3<System.Double,System.Int32,System.Double>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t Func_3_BeginInvoke_m498840770_MetadataUsageId;
extern "C"  Il2CppObject * Func_3_BeginInvoke_m498840770_gshared (Func_3_t2695431030 * __this, double ___arg10, int32_t ___arg21, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_3_BeginInvoke_m498840770_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Double_t534516614_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &___arg21);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TResult System.Func`3<System.Double,System.Int32,System.Double>::EndInvoke(System.IAsyncResult)
extern "C"  double Func_3_EndInvoke_m3251125822_gshared (Func_3_t2695431030 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(double*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Func`3<System.Double,System.Int32,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_3__ctor_m3183142242_gshared (Func_3_t2998020836 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`3<System.Double,System.Int32,System.Object>::Invoke(T1,T2)
extern "C"  Il2CppObject * Func_3_Invoke_m1788085675_gshared (Func_3_t2998020836 * __this, double ___arg10, int32_t ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_3_Invoke_m1788085675((Func_3_t2998020836 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, double ___arg10, int32_t ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, double ___arg10, int32_t ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`3<System.Double,System.Int32,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t Func_3_BeginInvoke_m868203056_MetadataUsageId;
extern "C"  Il2CppObject * Func_3_BeginInvoke_m868203056_gshared (Func_3_t2998020836 * __this, double ___arg10, int32_t ___arg21, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_3_BeginInvoke_m868203056_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Double_t534516614_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &___arg21);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TResult System.Func`3<System.Double,System.Int32,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Func_3_EndInvoke_m4016454800_gshared (Func_3_t2998020836 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Func`3<System.Int32,System.Int32,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_3__ctor_m4219734208_gshared (Func_3_t543102568 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`3<System.Int32,System.Int32,System.Int32>::Invoke(T1,T2)
extern "C"  int32_t Func_3_Invoke_m161999133_gshared (Func_3_t543102568 * __this, int32_t ___arg10, int32_t ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_3_Invoke_m161999133((Func_3_t543102568 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___arg10, int32_t ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___arg10, int32_t ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`3<System.Int32,System.Int32,System.Int32>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t Func_3_BeginInvoke_m529003554_MetadataUsageId;
extern "C"  Il2CppObject * Func_3_BeginInvoke_m529003554_gshared (Func_3_t543102568 * __this, int32_t ___arg10, int32_t ___arg21, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_3_BeginInvoke_m529003554_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &___arg21);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TResult System.Func`3<System.Int32,System.Int32,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Func_3_EndInvoke_m3710610270_gshared (Func_3_t543102568 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Func`3<System.Int32,System.Int32,UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_3__ctor_m2079156175_gshared (Func_3_t3578830837 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`3<System.Int32,System.Int32,UnityEngine.Color>::Invoke(T1,T2)
extern "C"  Color_t1588175760  Func_3_Invoke_m1098507674_gshared (Func_3_t3578830837 * __this, int32_t ___arg10, int32_t ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_3_Invoke_m1098507674((Func_3_t3578830837 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Color_t1588175760  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___arg10, int32_t ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Color_t1588175760  (*FunctionPointerType) (void* __this, int32_t ___arg10, int32_t ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`3<System.Int32,System.Int32,UnityEngine.Color>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t Func_3_BeginInvoke_m2040467227_MetadataUsageId;
extern "C"  Il2CppObject * Func_3_BeginInvoke_m2040467227_gshared (Func_3_t3578830837 * __this, int32_t ___arg10, int32_t ___arg21, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_3_BeginInvoke_m2040467227_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &___arg21);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TResult System.Func`3<System.Int32,System.Int32,UnityEngine.Color>::EndInvoke(System.IAsyncResult)
extern "C"  Color_t1588175760  Func_3_EndInvoke_m3075233921_gshared (Func_3_t3578830837 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Color_t1588175760 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Func`3<System.Int64,System.Int32,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_3__ctor_m3246063511_gshared (Func_3_t1612416521 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`3<System.Int64,System.Int32,System.Boolean>::Invoke(T1,T2)
extern "C"  bool Func_3_Invoke_m654595158_gshared (Func_3_t1612416521 * __this, int64_t ___arg10, int32_t ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_3_Invoke_m654595158((Func_3_t1612416521 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int64_t ___arg10, int32_t ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, int64_t ___arg10, int32_t ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`3<System.Int64,System.Int32,System.Boolean>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Int64_t2847414882_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t Func_3_BeginInvoke_m2085011931_MetadataUsageId;
extern "C"  Il2CppObject * Func_3_BeginInvoke_m2085011931_gshared (Func_3_t1612416521 * __this, int64_t ___arg10, int32_t ___arg21, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_3_BeginInvoke_m2085011931_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int64_t2847414882_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &___arg21);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TResult System.Func`3<System.Int64,System.Int32,System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  bool Func_3_EndInvoke_m1632483653_gshared (Func_3_t1612416521 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Func`3<System.Int64,System.Int32,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_3__ctor_m1133362545_gshared (Func_3_t4248825967 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`3<System.Int64,System.Int32,System.Int32>::Invoke(T1,T2)
extern "C"  int32_t Func_3_Invoke_m485491388_gshared (Func_3_t4248825967 * __this, int64_t ___arg10, int32_t ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_3_Invoke_m485491388((Func_3_t4248825967 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, int64_t ___arg10, int32_t ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int64_t ___arg10, int32_t ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`3<System.Int64,System.Int32,System.Int32>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Int64_t2847414882_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t Func_3_BeginInvoke_m1182646977_MetadataUsageId;
extern "C"  Il2CppObject * Func_3_BeginInvoke_m1182646977_gshared (Func_3_t4248825967 * __this, int64_t ___arg10, int32_t ___arg21, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_3_BeginInvoke_m1182646977_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int64_t2847414882_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &___arg21);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TResult System.Func`3<System.Int64,System.Int32,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Func_3_EndInvoke_m412345247_gshared (Func_3_t4248825967 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Func`3<System.Int64,System.Int32,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_3__ctor_m2784131868_gshared (Func_3_t2238517600 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`3<System.Int64,System.Int32,System.Object>::Invoke(T1,T2)
extern "C"  Il2CppObject * Func_3_Invoke_m2844637997_gshared (Func_3_t2238517600 * __this, int64_t ___arg10, int32_t ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_3_Invoke_m2844637997((Func_3_t2238517600 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, int64_t ___arg10, int32_t ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, int64_t ___arg10, int32_t ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`3<System.Int64,System.Int32,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Int64_t2847414882_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t Func_3_BeginInvoke_m1610904750_MetadataUsageId;
extern "C"  Il2CppObject * Func_3_BeginInvoke_m1610904750_gshared (Func_3_t2238517600 * __this, int64_t ___arg10, int32_t ___arg21, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_3_BeginInvoke_m1610904750_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int64_t2847414882_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &___arg21);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TResult System.Func`3<System.Int64,System.Int32,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Func_3_EndInvoke_m1484913486_gshared (Func_3_t2238517600 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Func`3<System.Int64,System.Int32,UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_3__ctor_m457791298_gshared (Func_3_t631773672 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`3<System.Int64,System.Int32,UnityEngine.Vector2>::Invoke(T1,T2)
extern "C"  Vector2_t3525329788  Func_3_Invoke_m1150035143_gshared (Func_3_t631773672 * __this, int64_t ___arg10, int32_t ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_3_Invoke_m1150035143((Func_3_t631773672 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Vector2_t3525329788  (*FunctionPointerType) (Il2CppObject *, void* __this, int64_t ___arg10, int32_t ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Vector2_t3525329788  (*FunctionPointerType) (void* __this, int64_t ___arg10, int32_t ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`3<System.Int64,System.Int32,UnityEngine.Vector2>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Int64_t2847414882_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t Func_3_BeginInvoke_m1889779144_MetadataUsageId;
extern "C"  Il2CppObject * Func_3_BeginInvoke_m1889779144_gshared (Func_3_t631773672 * __this, int64_t ___arg10, int32_t ___arg21, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_3_BeginInvoke_m1889779144_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int64_t2847414882_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &___arg21);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TResult System.Func`3<System.Int64,System.Int32,UnityEngine.Vector2>::EndInvoke(System.IAsyncResult)
extern "C"  Vector2_t3525329788  Func_3_EndInvoke_m1858386164_gshared (Func_3_t631773672 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Vector2_t3525329788 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Func`3<System.Int64,UniRx.Unit,System.Int64>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_3__ctor_m33277251_gshared (Func_3_t3706795343 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`3<System.Int64,UniRx.Unit,System.Int64>::Invoke(T1,T2)
extern "C"  int64_t Func_3_Invoke_m2334658926_gshared (Func_3_t3706795343 * __this, int64_t ___arg10, Unit_t2558286038  ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_3_Invoke_m2334658926((Func_3_t3706795343 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int64_t (*FunctionPointerType) (Il2CppObject *, void* __this, int64_t ___arg10, Unit_t2558286038  ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int64_t (*FunctionPointerType) (void* __this, int64_t ___arg10, Unit_t2558286038  ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`3<System.Int64,UniRx.Unit,System.Int64>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Int64_t2847414882_il2cpp_TypeInfo_var;
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern const uint32_t Func_3_BeginInvoke_m939526131_MetadataUsageId;
extern "C"  Il2CppObject * Func_3_BeginInvoke_m939526131_gshared (Func_3_t3706795343 * __this, int64_t ___arg10, Unit_t2558286038  ___arg21, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_3_BeginInvoke_m939526131_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int64_t2847414882_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = Box(Unit_t2558286038_il2cpp_TypeInfo_var, &___arg21);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TResult System.Func`3<System.Int64,UniRx.Unit,System.Int64>::EndInvoke(System.IAsyncResult)
extern "C"  int64_t Func_3_EndInvoke_m2301153069_gshared (Func_3_t3706795343 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int64_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Func`3<System.Object,Priority,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_3__ctor_m112902395_gshared (Func_3_t3765090877 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`3<System.Object,Priority,System.Object>::Invoke(T1,T2)
extern "C"  Il2CppObject * Func_3_Invoke_m1059675506_gshared (Func_3_t3765090877 * __this, Il2CppObject * ___arg10, int32_t ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_3_Invoke_m1059675506((Func_3_t3765090877 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, int32_t ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, int32_t ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, int32_t ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`3<System.Object,Priority,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Priority_t3194150340_il2cpp_TypeInfo_var;
extern const uint32_t Func_3_BeginInvoke_m3594911735_MetadataUsageId;
extern "C"  Il2CppObject * Func_3_BeginInvoke_m3594911735_gshared (Func_3_t3765090877 * __this, Il2CppObject * ___arg10, int32_t ___arg21, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_3_BeginInvoke_m3594911735_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = Box(Priority_t3194150340_il2cpp_TypeInfo_var, &___arg21);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TResult System.Func`3<System.Object,Priority,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Func_3_EndInvoke_m2561706921_gshared (Func_3_t3765090877 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Func`3<System.Object,System.Int32,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_3__ctor_m2013271139_gshared (Func_3_t3064567499 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`3<System.Object,System.Int32,System.Boolean>::Invoke(T1,T2)
extern "C"  bool Func_3_Invoke_m1577423878_gshared (Func_3_t3064567499 * __this, Il2CppObject * ___arg10, int32_t ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_3_Invoke_m1577423878((Func_3_t3064567499 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, int32_t ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, int32_t ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, int32_t ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`3<System.Object,System.Int32,System.Boolean>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t Func_3_BeginInvoke_m398647559_MetadataUsageId;
extern "C"  Il2CppObject * Func_3_BeginInvoke_m398647559_gshared (Func_3_t3064567499 * __this, Il2CppObject * ___arg10, int32_t ___arg21, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_3_BeginInvoke_m398647559_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &___arg21);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TResult System.Func`3<System.Object,System.Int32,System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  bool Func_3_EndInvoke_m4150120341_gshared (Func_3_t3064567499 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Func`3<System.Object,System.Int32,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_3__ctor_m2605817040_gshared (Func_3_t3690668578 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`3<System.Object,System.Int32,System.Object>::Invoke(T1,T2)
extern "C"  Il2CppObject * Func_3_Invoke_m2458764669_gshared (Func_3_t3690668578 * __this, Il2CppObject * ___arg10, int32_t ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_3_Invoke_m2458764669((Func_3_t3690668578 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, int32_t ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, int32_t ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, int32_t ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`3<System.Object,System.Int32,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t Func_3_BeginInvoke_m171032578_MetadataUsageId;
extern "C"  Il2CppObject * Func_3_BeginInvoke_m171032578_gshared (Func_3_t3690668578 * __this, Il2CppObject * ___arg10, int32_t ___arg21, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_3_BeginInvoke_m171032578_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &___arg21);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TResult System.Func`3<System.Object,System.Int32,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Func_3_EndInvoke_m2951600894_gshared (Func_3_t3690668578 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Func`3<System.Object,System.Int64,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_3__ctor_m2662524433_gshared (Func_3_t2633863527 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`3<System.Object,System.Int64,System.Object>::Invoke(T1,T2)
extern "C"  Il2CppObject * Func_3_Invoke_m1425020316_gshared (Func_3_t2633863527 * __this, Il2CppObject * ___arg10, int64_t ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_3_Invoke_m1425020316((Func_3_t2633863527 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, int64_t ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, int64_t ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, int64_t ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`3<System.Object,System.Int64,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Int64_t2847414882_il2cpp_TypeInfo_var;
extern const uint32_t Func_3_BeginInvoke_m1466516769_MetadataUsageId;
extern "C"  Il2CppObject * Func_3_BeginInvoke_m1466516769_gshared (Func_3_t2633863527 * __this, Il2CppObject * ___arg10, int64_t ___arg21, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_3_BeginInvoke_m1466516769_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = Box(Int64_t2847414882_il2cpp_TypeInfo_var, &___arg21);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TResult System.Func`3<System.Object,System.Int64,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Func_3_EndInvoke_m572517567_gshared (Func_3_t2633863527 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Func`3<System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_3__ctor_m4028180665_gshared (Func_3_t1892209229 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`3<System.Object,System.Object,System.Object>::Invoke(T1,T2)
extern "C"  Il2CppObject * Func_3_Invoke_m4101486064_gshared (Func_3_t1892209229 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_3_Invoke_m4101486064((Func_3_t1892209229 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`3<System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_3_BeginInvoke_m69685489_gshared (Func_3_t1892209229 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TResult System.Func`3<System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Func_3_EndInvoke_m1319796459_gshared (Func_3_t1892209229 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Func`3<System.Object,UniRx.CancellationToken,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_3__ctor_m2183027641_gshared (Func_3_t2554989993 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`3<System.Object,UniRx.CancellationToken,System.Object>::Invoke(T1,T2)
extern "C"  Il2CppObject * Func_3_Invoke_m1848481136_gshared (Func_3_t2554989993 * __this, Il2CppObject * ___arg10, CancellationToken_t1439151560  ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_3_Invoke_m1848481136((Func_3_t2554989993 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, CancellationToken_t1439151560  ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, CancellationToken_t1439151560  ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, CancellationToken_t1439151560  ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`3<System.Object,UniRx.CancellationToken,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* CancellationToken_t1439151560_il2cpp_TypeInfo_var;
extern const uint32_t Func_3_BeginInvoke_m2647552753_MetadataUsageId;
extern "C"  Il2CppObject * Func_3_BeginInvoke_m2647552753_gshared (Func_3_t2554989993 * __this, Il2CppObject * ___arg10, CancellationToken_t1439151560  ___arg21, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_3_BeginInvoke_m2647552753_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = Box(CancellationToken_t1439151560_il2cpp_TypeInfo_var, &___arg21);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TResult System.Func`3<System.Object,UniRx.CancellationToken,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Func_3_EndInvoke_m847220587_gshared (Func_3_t2554989993 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Func`3<System.Object,UniRx.Unit,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_3__ctor_m2145875299_gshared (Func_3_t3148637859 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`3<System.Object,UniRx.Unit,System.Object>::Invoke(T1,T2)
extern "C"  Il2CppObject * Func_3_Invoke_m1196619658_gshared (Func_3_t3148637859 * __this, Il2CppObject * ___arg10, Unit_t2558286038  ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_3_Invoke_m1196619658((Func_3_t3148637859 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, Unit_t2558286038  ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, Unit_t2558286038  ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Unit_t2558286038  ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`3<System.Object,UniRx.Unit,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern const uint32_t Func_3_BeginInvoke_m3099545999_MetadataUsageId;
extern "C"  Il2CppObject * Func_3_BeginInvoke_m3099545999_gshared (Func_3_t3148637859 * __this, Il2CppObject * ___arg10, Unit_t2558286038  ___arg21, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_3_BeginInvoke_m3099545999_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = Box(Unit_t2558286038_il2cpp_TypeInfo_var, &___arg21);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TResult System.Func`3<System.Object,UniRx.Unit,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Func_3_EndInvoke_m4149961873_gshared (Func_3_t3148637859 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Func`3<System.Single,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_3__ctor_m2316382416_gshared (Func_3_t2453810542 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`3<System.Single,System.Object,System.Object>::Invoke(T1,T2)
extern "C"  Il2CppObject * Func_3_Invoke_m4291358073_gshared (Func_3_t2453810542 * __this, float ___arg10, Il2CppObject * ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_3_Invoke_m4291358073((Func_3_t2453810542 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, float ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, float ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`3<System.Single,System.Object,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Single_t958209021_il2cpp_TypeInfo_var;
extern const uint32_t Func_3_BeginInvoke_m1433176954_MetadataUsageId;
extern "C"  Il2CppObject * Func_3_BeginInvoke_m1433176954_gshared (Func_3_t2453810542 * __this, float ___arg10, Il2CppObject * ___arg21, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_3_BeginInvoke_m1433176954_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Single_t958209021_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = ___arg21;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TResult System.Func`3<System.Single,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Func_3_EndInvoke_m3105892994_gshared (Func_3_t2453810542 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Func`3<System.Single,System.Object,System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_3__ctor_m2429120871_gshared (Func_3_t2574913143 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`3<System.Single,System.Object,System.Single>::Invoke(T1,T2)
extern "C"  float Func_3_Invoke_m3675757698_gshared (Func_3_t2574913143 * __this, float ___arg10, Il2CppObject * ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_3_Invoke_m3675757698((Func_3_t2574913143 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef float (*FunctionPointerType) (Il2CppObject *, void* __this, float ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef float (*FunctionPointerType) (void* __this, float ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`3<System.Single,System.Object,System.Single>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Single_t958209021_il2cpp_TypeInfo_var;
extern const uint32_t Func_3_BeginInvoke_m3116481923_MetadataUsageId;
extern "C"  Il2CppObject * Func_3_BeginInvoke_m3116481923_gshared (Func_3_t2574913143 * __this, float ___arg10, Il2CppObject * ___arg21, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_3_BeginInvoke_m3116481923_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Single_t958209021_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = ___arg21;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TResult System.Func`3<System.Single,System.Object,System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  float Func_3_EndInvoke_m3326496409_gshared (Func_3_t2574913143 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(float*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Func`3<System.Single,System.Single,System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_3__ctor_m1350393278_gshared (Func_3_t3782820650 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`3<System.Single,System.Single,System.Single>::Invoke(T1,T2)
extern "C"  float Func_3_Invoke_m4231972811_gshared (Func_3_t3782820650 * __this, float ___arg10, float ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_3_Invoke_m4231972811((Func_3_t3782820650 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef float (*FunctionPointerType) (Il2CppObject *, void* __this, float ___arg10, float ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef float (*FunctionPointerType) (void* __this, float ___arg10, float ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`3<System.Single,System.Single,System.Single>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Single_t958209021_il2cpp_TypeInfo_var;
extern const uint32_t Func_3_BeginInvoke_m1758342092_MetadataUsageId;
extern "C"  Il2CppObject * Func_3_BeginInvoke_m1758342092_gshared (Func_3_t3782820650 * __this, float ___arg10, float ___arg21, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_3_BeginInvoke_m1758342092_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Single_t958209021_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = Box(Single_t958209021_il2cpp_TypeInfo_var, &___arg21);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TResult System.Func`3<System.Single,System.Single,System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  float Func_3_EndInvoke_m3331587696_gshared (Func_3_t3782820650 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(float*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Func`3<UniRx.Unit,System.Int32,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_3__ctor_m3469161193_gshared (Func_3_t255060061 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`3<UniRx.Unit,System.Int32,System.Boolean>::Invoke(T1,T2)
extern "C"  bool Func_3_Invoke_m2505118276_gshared (Func_3_t255060061 * __this, Unit_t2558286038  ___arg10, int32_t ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_3_Invoke_m2505118276((Func_3_t255060061 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Unit_t2558286038  ___arg10, int32_t ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Unit_t2558286038  ___arg10, int32_t ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`3<UniRx.Unit,System.Int32,System.Boolean>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t Func_3_BeginInvoke_m561687113_MetadataUsageId;
extern "C"  Il2CppObject * Func_3_BeginInvoke_m561687113_gshared (Func_3_t255060061 * __this, Unit_t2558286038  ___arg10, int32_t ___arg21, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_3_BeginInvoke_m561687113_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Unit_t2558286038_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &___arg21);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TResult System.Func`3<UniRx.Unit,System.Int32,System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  bool Func_3_EndInvoke_m1209193751_gshared (Func_3_t255060061 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Func`3<UniRx.Unit,System.Int32,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_3__ctor_m1267307914_gshared (Func_3_t881161140 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`3<UniRx.Unit,System.Int32,System.Object>::Invoke(T1,T2)
extern "C"  Il2CppObject * Func_3_Invoke_m2765784959_gshared (Func_3_t881161140 * __this, Unit_t2558286038  ___arg10, int32_t ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_3_Invoke_m2765784959((Func_3_t881161140 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Unit_t2558286038  ___arg10, int32_t ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Unit_t2558286038  ___arg10, int32_t ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`3<UniRx.Unit,System.Int32,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t Func_3_BeginInvoke_m1838859904_MetadataUsageId;
extern "C"  Il2CppObject * Func_3_BeginInvoke_m1838859904_gshared (Func_3_t881161140 * __this, Unit_t2558286038  ___arg10, int32_t ___arg21, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_3_BeginInvoke_m1838859904_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Unit_t2558286038_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &___arg21);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TResult System.Func`3<UniRx.Unit,System.Int32,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Func_3_EndInvoke_m362880316_gshared (Func_3_t881161140 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Func`3<UnityEngine.Vector2,System.Int32,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_3__ctor_m1563899593_gshared (Func_3_t487212563 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`3<UnityEngine.Vector2,System.Int32,System.Boolean>::Invoke(T1,T2)
extern "C"  bool Func_3_Invoke_m3580751712_gshared (Func_3_t487212563 * __this, Vector2_t3525329788  ___arg10, int32_t ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_3_Invoke_m3580751712((Func_3_t487212563 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Vector2_t3525329788  ___arg10, int32_t ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Vector2_t3525329788  ___arg10, int32_t ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`3<UnityEngine.Vector2,System.Int32,System.Boolean>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Vector2_t3525329788_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t Func_3_BeginInvoke_m258800609_MetadataUsageId;
extern "C"  Il2CppObject * Func_3_BeginInvoke_m258800609_gshared (Func_3_t487212563 * __this, Vector2_t3525329788  ___arg10, int32_t ___arg21, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_3_BeginInvoke_m258800609_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Vector2_t3525329788_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &___arg21);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TResult System.Func`3<UnityEngine.Vector2,System.Int32,System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  bool Func_3_EndInvoke_m866145147_gshared (Func_3_t487212563 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Func`4<System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_4__ctor_m283050854_gshared (Func_4_t3628925182 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`4<System.Object,System.Object,System.Object,System.Object>::Invoke(T1,T2,T3)
extern "C"  Il2CppObject * Func_4_Invoke_m4260158276_gshared (Func_4_t3628925182 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_4_Invoke_m4260158276((Func_4_t3628925182 *)__this->get_prev_9(),___arg10, ___arg21, ___arg32, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21, ___arg32,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`4<System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_4_BeginInvoke_m1091833817_gshared (Func_4_t3628925182 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, AsyncCallback_t1363551830 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	void *__d_args[4] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	__d_args[2] = ___arg32;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// TResult System.Func`4<System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Func_4_EndInvoke_m3792811160_gshared (Func_4_t3628925182 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Func`5<System.Double,System.Int32,System.Double,System.Int32,System.Double>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_5__ctor_m2446162665_gshared (Func_5_t2889627055 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`5<System.Double,System.Int32,System.Double,System.Int32,System.Double>::Invoke(T1,T2,T3,T4)
extern "C"  double Func_5_Invoke_m1072554751_gshared (Func_5_t2889627055 * __this, double ___arg10, int32_t ___arg21, double ___arg32, int32_t ___arg43, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_5_Invoke_m1072554751((Func_5_t2889627055 *)__this->get_prev_9(),___arg10, ___arg21, ___arg32, ___arg43, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef double (*FunctionPointerType) (Il2CppObject *, void* __this, double ___arg10, int32_t ___arg21, double ___arg32, int32_t ___arg43, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32, ___arg43,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef double (*FunctionPointerType) (void* __this, double ___arg10, int32_t ___arg21, double ___arg32, int32_t ___arg43, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32, ___arg43,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`5<System.Double,System.Int32,System.Double,System.Int32,System.Double>::BeginInvoke(T1,T2,T3,T4,System.AsyncCallback,System.Object)
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t Func_5_BeginInvoke_m175931234_MetadataUsageId;
extern "C"  Il2CppObject * Func_5_BeginInvoke_m175931234_gshared (Func_5_t2889627055 * __this, double ___arg10, int32_t ___arg21, double ___arg32, int32_t ___arg43, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_5_BeginInvoke_m175931234_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[5] = {0};
	__d_args[0] = Box(Double_t534516614_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &___arg21);
	__d_args[2] = Box(Double_t534516614_il2cpp_TypeInfo_var, &___arg32);
	__d_args[3] = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &___arg43);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback4, (Il2CppObject*)___object5);
}
// TResult System.Func`5<System.Double,System.Int32,System.Double,System.Int32,System.Double>::EndInvoke(System.IAsyncResult)
extern "C"  double Func_5_EndInvoke_m1047741083_gshared (Func_5_t2889627055 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(double*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Func`5<System.Object,System.Int32,System.Int64,System.Int32,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_5__ctor_m449268497_gshared (Func_5_t3557632551 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`5<System.Object,System.Int32,System.Int64,System.Int32,System.Object>::Invoke(T1,T2,T3,T4)
extern "C"  Il2CppObject * Func_5_Invoke_m1298299867_gshared (Func_5_t3557632551 * __this, Il2CppObject * ___arg10, int32_t ___arg21, int64_t ___arg32, int32_t ___arg43, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_5_Invoke_m1298299867((Func_5_t3557632551 *)__this->get_prev_9(),___arg10, ___arg21, ___arg32, ___arg43, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, int32_t ___arg21, int64_t ___arg32, int32_t ___arg43, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32, ___arg43,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, int32_t ___arg21, int64_t ___arg32, int32_t ___arg43, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32, ___arg43,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, int32_t ___arg21, int64_t ___arg32, int32_t ___arg43, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21, ___arg32, ___arg43,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`5<System.Object,System.Int32,System.Int64,System.Int32,System.Object>::BeginInvoke(T1,T2,T3,T4,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t2847414882_il2cpp_TypeInfo_var;
extern const uint32_t Func_5_BeginInvoke_m4275219650_MetadataUsageId;
extern "C"  Il2CppObject * Func_5_BeginInvoke_m4275219650_gshared (Func_5_t3557632551 * __this, Il2CppObject * ___arg10, int32_t ___arg21, int64_t ___arg32, int32_t ___arg43, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_5_BeginInvoke_m4275219650_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[5] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &___arg21);
	__d_args[2] = Box(Int64_t2847414882_il2cpp_TypeInfo_var, &___arg32);
	__d_args[3] = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &___arg43);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback4, (Il2CppObject*)___object5);
}
// TResult System.Func`5<System.Object,System.Int32,System.Int64,System.Int32,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Func_5_EndInvoke_m391759935_gshared (Func_5_t3557632551 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Func`5<System.Object,System.Int32,System.Object,System.Int32,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_5__ctor_m3239496571_gshared (Func_5_t714816233 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`5<System.Object,System.Int32,System.Object,System.Int32,System.Object>::Invoke(T1,T2,T3,T4)
extern "C"  Il2CppObject * Func_5_Invoke_m2880684205_gshared (Func_5_t714816233 * __this, Il2CppObject * ___arg10, int32_t ___arg21, Il2CppObject * ___arg32, int32_t ___arg43, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_5_Invoke_m2880684205((Func_5_t714816233 *)__this->get_prev_9(),___arg10, ___arg21, ___arg32, ___arg43, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, int32_t ___arg21, Il2CppObject * ___arg32, int32_t ___arg43, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32, ___arg43,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, int32_t ___arg21, Il2CppObject * ___arg32, int32_t ___arg43, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32, ___arg43,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, int32_t ___arg21, Il2CppObject * ___arg32, int32_t ___arg43, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21, ___arg32, ___arg43,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`5<System.Object,System.Int32,System.Object,System.Int32,System.Object>::BeginInvoke(T1,T2,T3,T4,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t Func_5_BeginInvoke_m1732155152_MetadataUsageId;
extern "C"  Il2CppObject * Func_5_BeginInvoke_m1732155152_gshared (Func_5_t714816233 * __this, Il2CppObject * ___arg10, int32_t ___arg21, Il2CppObject * ___arg32, int32_t ___arg43, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_5_BeginInvoke_m1732155152_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[5] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &___arg21);
	__d_args[2] = ___arg32;
	__d_args[3] = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &___arg43);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback4, (Il2CppObject*)___object5);
}
// TResult System.Func`5<System.Object,System.Int32,System.Object,System.Int32,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Func_5_EndInvoke_m3423579181_gshared (Func_5_t714816233 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Func`5<System.Object,System.Int32,UniRx.Unit,System.Int32,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_5__ctor_m3352026815_gshared (Func_5_t2200276091 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`5<System.Object,System.Int32,UniRx.Unit,System.Int32,System.Object>::Invoke(T1,T2,T3,T4)
extern "C"  Il2CppObject * Func_5_Invoke_m2934399597_gshared (Func_5_t2200276091 * __this, Il2CppObject * ___arg10, int32_t ___arg21, Unit_t2558286038  ___arg32, int32_t ___arg43, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_5_Invoke_m2934399597((Func_5_t2200276091 *)__this->get_prev_9(),___arg10, ___arg21, ___arg32, ___arg43, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, int32_t ___arg21, Unit_t2558286038  ___arg32, int32_t ___arg43, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32, ___arg43,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, int32_t ___arg21, Unit_t2558286038  ___arg32, int32_t ___arg43, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32, ___arg43,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, int32_t ___arg21, Unit_t2558286038  ___arg32, int32_t ___arg43, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21, ___arg32, ___arg43,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`5<System.Object,System.Int32,UniRx.Unit,System.Int32,System.Object>::BeginInvoke(T1,T2,T3,T4,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* Unit_t2558286038_il2cpp_TypeInfo_var;
extern const uint32_t Func_5_BeginInvoke_m2075150804_MetadataUsageId;
extern "C"  Il2CppObject * Func_5_BeginInvoke_m2075150804_gshared (Func_5_t2200276091 * __this, Il2CppObject * ___arg10, int32_t ___arg21, Unit_t2558286038  ___arg32, int32_t ___arg43, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Func_5_BeginInvoke_m2075150804_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[5] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &___arg21);
	__d_args[2] = Box(Unit_t2558286038_il2cpp_TypeInfo_var, &___arg32);
	__d_args[3] = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &___arg43);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback4, (Il2CppObject*)___object5);
}
// TResult System.Func`5<System.Object,System.Int32,UniRx.Unit,System.Int32,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Func_5_EndInvoke_m1496937069_gshared (Func_5_t2200276091 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Func`5<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_5__ctor_m2081941779_gshared (Func_5_t875099911 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`5<System.Object,System.Object,System.Object,System.Object,System.Object>::Invoke(T1,T2,T3,T4)
extern "C"  Il2CppObject * Func_5_Invoke_m3831797397_gshared (Func_5_t875099911 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_5_Invoke_m3831797397((Func_5_t875099911 *)__this->get_prev_9(),___arg10, ___arg21, ___arg32, ___arg43, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32, ___arg43,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21, ___arg32, ___arg43,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21, ___arg32, ___arg43,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`5<System.Object,System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,T3,T4,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_5_BeginInvoke_m149906808_gshared (Func_5_t875099911 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method)
{
	void *__d_args[5] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	__d_args[2] = ___arg32;
	__d_args[3] = ___arg43;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback4, (Il2CppObject*)___object5);
}
// TResult System.Func`5<System.Object,System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Func_5_EndInvoke_m217421125_gshared (Func_5_t875099911 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
