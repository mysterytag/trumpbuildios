﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GameAnalyticsSDK.Settings
struct Settings_t1104664933;
// GameAnalyticsSDK.GameAnalytics
struct GameAnalytics_t3523455158;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameAnalyticsSDK.GameAnalytics
struct  GameAnalytics_t3523455158  : public MonoBehaviour_t3012272455
{
public:

public:
};

struct GameAnalytics_t3523455158_StaticFields
{
public:
	// GameAnalyticsSDK.Settings GameAnalyticsSDK.GameAnalytics::_settings
	Settings_t1104664933 * ____settings_2;
	// GameAnalyticsSDK.GameAnalytics GameAnalyticsSDK.GameAnalytics::_instance
	GameAnalytics_t3523455158 * ____instance_3;

public:
	inline static int32_t get_offset_of__settings_2() { return static_cast<int32_t>(offsetof(GameAnalytics_t3523455158_StaticFields, ____settings_2)); }
	inline Settings_t1104664933 * get__settings_2() const { return ____settings_2; }
	inline Settings_t1104664933 ** get_address_of__settings_2() { return &____settings_2; }
	inline void set__settings_2(Settings_t1104664933 * value)
	{
		____settings_2 = value;
		Il2CppCodeGenWriteBarrier(&____settings_2, value);
	}

	inline static int32_t get_offset_of__instance_3() { return static_cast<int32_t>(offsetof(GameAnalytics_t3523455158_StaticFields, ____instance_3)); }
	inline GameAnalytics_t3523455158 * get__instance_3() const { return ____instance_3; }
	inline GameAnalytics_t3523455158 ** get_address_of__instance_3() { return &____instance_3; }
	inline void set__instance_3(GameAnalytics_t3523455158 * value)
	{
		____instance_3 = value;
		Il2CppCodeGenWriteBarrier(&____instance_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
