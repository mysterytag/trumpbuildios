﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ComponentModel.WeakObjectWrapper
struct WeakObjectWrapper_t2550797893;
// System.Object
struct Il2CppObject;
// System.WeakReference
struct WeakReference_t2193916456;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_WeakReference2193916456.h"

// System.Void System.ComponentModel.WeakObjectWrapper::.ctor(System.Object)
extern "C"  void WeakObjectWrapper__ctor_m2096946947 (WeakObjectWrapper_t2550797893 * __this, Il2CppObject * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.ComponentModel.WeakObjectWrapper::get_TargetHashCode()
extern "C"  int32_t WeakObjectWrapper_get_TargetHashCode_m1047807704 (WeakObjectWrapper_t2550797893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.WeakObjectWrapper::set_TargetHashCode(System.Int32)
extern "C"  void WeakObjectWrapper_set_TargetHashCode_m467278029 (WeakObjectWrapper_t2550797893 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.WeakReference System.ComponentModel.WeakObjectWrapper::get_Weak()
extern "C"  WeakReference_t2193916456 * WeakObjectWrapper_get_Weak_m3636826345 (WeakObjectWrapper_t2550797893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.WeakObjectWrapper::set_Weak(System.WeakReference)
extern "C"  void WeakObjectWrapper_set_Weak_m2861291444 (WeakObjectWrapper_t2550797893 * __this, WeakReference_t2193916456 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
