﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AdToApp.AndroidWrapper.AdToAppAndroidWrapper/<ShowBanner>c__AnonStorey2B
struct U3CShowBannerU3Ec__AnonStorey2B_t18990590;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper/<ShowBanner>c__AnonStorey2B::.ctor()
extern "C"  void U3CShowBannerU3Ec__AnonStorey2B__ctor_m2169297112 (U3CShowBannerU3Ec__AnonStorey2B_t18990590 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper/<ShowBanner>c__AnonStorey2B::<>m__2(System.Object)
extern "C"  void U3CShowBannerU3Ec__AnonStorey2B_U3CU3Em__2_m363263073 (U3CShowBannerU3Ec__AnonStorey2B_t18990590 * __this, Il2CppObject * ___wrapperContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
