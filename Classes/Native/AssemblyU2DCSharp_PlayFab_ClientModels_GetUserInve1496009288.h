﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance>
struct List_1_t1322103281;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t190145395;
// System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.VirtualCurrencyRechargeTime>
struct Dictionary_2_t3222006224;

#include "AssemblyU2DCSharp_PlayFab_Internal_PlayFabResultCom379512675.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.GetUserInventoryResult
struct  GetUserInventoryResult_t1496009288  : public PlayFabResultCommon_t379512675
{
public:
	// System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance> PlayFab.ClientModels.GetUserInventoryResult::<Inventory>k__BackingField
	List_1_t1322103281 * ___U3CInventoryU3Ek__BackingField_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> PlayFab.ClientModels.GetUserInventoryResult::<VirtualCurrency>k__BackingField
	Dictionary_2_t190145395 * ___U3CVirtualCurrencyU3Ek__BackingField_3;
	// System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.VirtualCurrencyRechargeTime> PlayFab.ClientModels.GetUserInventoryResult::<VirtualCurrencyRechargeTimes>k__BackingField
	Dictionary_2_t3222006224 * ___U3CVirtualCurrencyRechargeTimesU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CInventoryU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GetUserInventoryResult_t1496009288, ___U3CInventoryU3Ek__BackingField_2)); }
	inline List_1_t1322103281 * get_U3CInventoryU3Ek__BackingField_2() const { return ___U3CInventoryU3Ek__BackingField_2; }
	inline List_1_t1322103281 ** get_address_of_U3CInventoryU3Ek__BackingField_2() { return &___U3CInventoryU3Ek__BackingField_2; }
	inline void set_U3CInventoryU3Ek__BackingField_2(List_1_t1322103281 * value)
	{
		___U3CInventoryU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CInventoryU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CVirtualCurrencyU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(GetUserInventoryResult_t1496009288, ___U3CVirtualCurrencyU3Ek__BackingField_3)); }
	inline Dictionary_2_t190145395 * get_U3CVirtualCurrencyU3Ek__BackingField_3() const { return ___U3CVirtualCurrencyU3Ek__BackingField_3; }
	inline Dictionary_2_t190145395 ** get_address_of_U3CVirtualCurrencyU3Ek__BackingField_3() { return &___U3CVirtualCurrencyU3Ek__BackingField_3; }
	inline void set_U3CVirtualCurrencyU3Ek__BackingField_3(Dictionary_2_t190145395 * value)
	{
		___U3CVirtualCurrencyU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CVirtualCurrencyU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CVirtualCurrencyRechargeTimesU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(GetUserInventoryResult_t1496009288, ___U3CVirtualCurrencyRechargeTimesU3Ek__BackingField_4)); }
	inline Dictionary_2_t3222006224 * get_U3CVirtualCurrencyRechargeTimesU3Ek__BackingField_4() const { return ___U3CVirtualCurrencyRechargeTimesU3Ek__BackingField_4; }
	inline Dictionary_2_t3222006224 ** get_address_of_U3CVirtualCurrencyRechargeTimesU3Ek__BackingField_4() { return &___U3CVirtualCurrencyRechargeTimesU3Ek__BackingField_4; }
	inline void set_U3CVirtualCurrencyRechargeTimesU3Ek__BackingField_4(Dictionary_2_t3222006224 * value)
	{
		___U3CVirtualCurrencyRechargeTimesU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CVirtualCurrencyRechargeTimesU3Ek__BackingField_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
