﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.BufferObservable`1/BufferT<System.Object>
struct BufferT_t129222477;
// UniRx.Operators.BufferObservable`1<System.Object>
struct BufferObservable_1_t574294898;
// UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Object>>
struct IObserver_1_t920630341;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.BufferObservable`1/BufferT<System.Object>::.ctor(UniRx.Operators.BufferObservable`1<T>,UniRx.IObserver`1<System.Collections.Generic.IList`1<T>>,System.IDisposable)
extern "C"  void BufferT__ctor_m1023256711_gshared (BufferT_t129222477 * __this, BufferObservable_1_t574294898 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define BufferT__ctor_m1023256711(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (BufferT_t129222477 *, BufferObservable_1_t574294898 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))BufferT__ctor_m1023256711_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.BufferObservable`1/BufferT<System.Object>::.cctor()
extern "C"  void BufferT__cctor_m1244560674_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define BufferT__cctor_m1244560674(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))BufferT__cctor_m1244560674_gshared)(__this /* static, unused */, method)
// System.IDisposable UniRx.Operators.BufferObservable`1/BufferT<System.Object>::Run()
extern "C"  Il2CppObject * BufferT_Run_m3274670607_gshared (BufferT_t129222477 * __this, const MethodInfo* method);
#define BufferT_Run_m3274670607(__this, method) ((  Il2CppObject * (*) (BufferT_t129222477 *, const MethodInfo*))BufferT_Run_m3274670607_gshared)(__this, method)
// System.Void UniRx.Operators.BufferObservable`1/BufferT<System.Object>::OnNext(T)
extern "C"  void BufferT_OnNext_m522491859_gshared (BufferT_t129222477 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define BufferT_OnNext_m522491859(__this, ___value0, method) ((  void (*) (BufferT_t129222477 *, Il2CppObject *, const MethodInfo*))BufferT_OnNext_m522491859_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.BufferObservable`1/BufferT<System.Object>::OnError(System.Exception)
extern "C"  void BufferT_OnError_m4032605410_gshared (BufferT_t129222477 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define BufferT_OnError_m4032605410(__this, ___error0, method) ((  void (*) (BufferT_t129222477 *, Exception_t1967233988 *, const MethodInfo*))BufferT_OnError_m4032605410_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.BufferObservable`1/BufferT<System.Object>::OnCompleted()
extern "C"  void BufferT_OnCompleted_m2645435189_gshared (BufferT_t129222477 * __this, const MethodInfo* method);
#define BufferT_OnCompleted_m2645435189(__this, method) ((  void (*) (BufferT_t129222477 *, const MethodInfo*))BufferT_OnCompleted_m2645435189_gshared)(__this, method)
