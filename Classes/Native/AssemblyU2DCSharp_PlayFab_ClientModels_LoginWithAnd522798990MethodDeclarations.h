﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.LoginWithAndroidDeviceIDRequest
struct LoginWithAndroidDeviceIDRequest_t522798990;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen3097043249.h"

// System.Void PlayFab.ClientModels.LoginWithAndroidDeviceIDRequest::.ctor()
extern "C"  void LoginWithAndroidDeviceIDRequest__ctor_m3166839771 (LoginWithAndroidDeviceIDRequest_t522798990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LoginWithAndroidDeviceIDRequest::get_TitleId()
extern "C"  String_t* LoginWithAndroidDeviceIDRequest_get_TitleId_m4166179104 (LoginWithAndroidDeviceIDRequest_t522798990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LoginWithAndroidDeviceIDRequest::set_TitleId(System.String)
extern "C"  void LoginWithAndroidDeviceIDRequest_set_TitleId_m741868339 (LoginWithAndroidDeviceIDRequest_t522798990 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LoginWithAndroidDeviceIDRequest::get_AndroidDeviceId()
extern "C"  String_t* LoginWithAndroidDeviceIDRequest_get_AndroidDeviceId_m737258989 (LoginWithAndroidDeviceIDRequest_t522798990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LoginWithAndroidDeviceIDRequest::set_AndroidDeviceId(System.String)
extern "C"  void LoginWithAndroidDeviceIDRequest_set_AndroidDeviceId_m2535080582 (LoginWithAndroidDeviceIDRequest_t522798990 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LoginWithAndroidDeviceIDRequest::get_OS()
extern "C"  String_t* LoginWithAndroidDeviceIDRequest_get_OS_m1934383865 (LoginWithAndroidDeviceIDRequest_t522798990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LoginWithAndroidDeviceIDRequest::set_OS(System.String)
extern "C"  void LoginWithAndroidDeviceIDRequest_set_OS_m2418993336 (LoginWithAndroidDeviceIDRequest_t522798990 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LoginWithAndroidDeviceIDRequest::get_AndroidDevice()
extern "C"  String_t* LoginWithAndroidDeviceIDRequest_get_AndroidDevice_m2816405426 (LoginWithAndroidDeviceIDRequest_t522798990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LoginWithAndroidDeviceIDRequest::set_AndroidDevice(System.String)
extern "C"  void LoginWithAndroidDeviceIDRequest_set_AndroidDevice_m3655510305 (LoginWithAndroidDeviceIDRequest_t522798990 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.LoginWithAndroidDeviceIDRequest::get_CreateAccount()
extern "C"  Nullable_1_t3097043249  LoginWithAndroidDeviceIDRequest_get_CreateAccount_m260860728 (LoginWithAndroidDeviceIDRequest_t522798990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LoginWithAndroidDeviceIDRequest::set_CreateAccount(System.Nullable`1<System.Boolean>)
extern "C"  void LoginWithAndroidDeviceIDRequest_set_CreateAccount_m3489099099 (LoginWithAndroidDeviceIDRequest_t522798990 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
