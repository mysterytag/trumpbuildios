﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t1522972100;
// UniRx.IObserver`1<System.String>
struct IObserver_1_t3180487805;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Examples.Sample05_ConvertFromCoroutine/<GetWWWCore>c__Iterator6
struct  U3CGetWWWCoreU3Ec__Iterator6_t2275077895  : public Il2CppObject
{
public:
	// System.String UniRx.Examples.Sample05_ConvertFromCoroutine/<GetWWWCore>c__Iterator6::url
	String_t* ___url_0;
	// UnityEngine.WWW UniRx.Examples.Sample05_ConvertFromCoroutine/<GetWWWCore>c__Iterator6::<www>__0
	WWW_t1522972100 * ___U3CwwwU3E__0_1;
	// UniRx.CancellationToken UniRx.Examples.Sample05_ConvertFromCoroutine/<GetWWWCore>c__Iterator6::cancellationToken
	CancellationToken_t1439151560  ___cancellationToken_2;
	// UniRx.IObserver`1<System.String> UniRx.Examples.Sample05_ConvertFromCoroutine/<GetWWWCore>c__Iterator6::observer
	Il2CppObject* ___observer_3;
	// System.Int32 UniRx.Examples.Sample05_ConvertFromCoroutine/<GetWWWCore>c__Iterator6::$PC
	int32_t ___U24PC_4;
	// System.Object UniRx.Examples.Sample05_ConvertFromCoroutine/<GetWWWCore>c__Iterator6::$current
	Il2CppObject * ___U24current_5;
	// System.String UniRx.Examples.Sample05_ConvertFromCoroutine/<GetWWWCore>c__Iterator6::<$>url
	String_t* ___U3CU24U3Eurl_6;
	// UniRx.CancellationToken UniRx.Examples.Sample05_ConvertFromCoroutine/<GetWWWCore>c__Iterator6::<$>cancellationToken
	CancellationToken_t1439151560  ___U3CU24U3EcancellationToken_7;
	// UniRx.IObserver`1<System.String> UniRx.Examples.Sample05_ConvertFromCoroutine/<GetWWWCore>c__Iterator6::<$>observer
	Il2CppObject* ___U3CU24U3Eobserver_8;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CGetWWWCoreU3Ec__Iterator6_t2275077895, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier(&___url_0, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_1() { return static_cast<int32_t>(offsetof(U3CGetWWWCoreU3Ec__Iterator6_t2275077895, ___U3CwwwU3E__0_1)); }
	inline WWW_t1522972100 * get_U3CwwwU3E__0_1() const { return ___U3CwwwU3E__0_1; }
	inline WWW_t1522972100 ** get_address_of_U3CwwwU3E__0_1() { return &___U3CwwwU3E__0_1; }
	inline void set_U3CwwwU3E__0_1(WWW_t1522972100 * value)
	{
		___U3CwwwU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__0_1, value);
	}

	inline static int32_t get_offset_of_cancellationToken_2() { return static_cast<int32_t>(offsetof(U3CGetWWWCoreU3Ec__Iterator6_t2275077895, ___cancellationToken_2)); }
	inline CancellationToken_t1439151560  get_cancellationToken_2() const { return ___cancellationToken_2; }
	inline CancellationToken_t1439151560 * get_address_of_cancellationToken_2() { return &___cancellationToken_2; }
	inline void set_cancellationToken_2(CancellationToken_t1439151560  value)
	{
		___cancellationToken_2 = value;
	}

	inline static int32_t get_offset_of_observer_3() { return static_cast<int32_t>(offsetof(U3CGetWWWCoreU3Ec__Iterator6_t2275077895, ___observer_3)); }
	inline Il2CppObject* get_observer_3() const { return ___observer_3; }
	inline Il2CppObject** get_address_of_observer_3() { return &___observer_3; }
	inline void set_observer_3(Il2CppObject* value)
	{
		___observer_3 = value;
		Il2CppCodeGenWriteBarrier(&___observer_3, value);
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CGetWWWCoreU3Ec__Iterator6_t2275077895, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CGetWWWCoreU3Ec__Iterator6_t2275077895, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eurl_6() { return static_cast<int32_t>(offsetof(U3CGetWWWCoreU3Ec__Iterator6_t2275077895, ___U3CU24U3Eurl_6)); }
	inline String_t* get_U3CU24U3Eurl_6() const { return ___U3CU24U3Eurl_6; }
	inline String_t** get_address_of_U3CU24U3Eurl_6() { return &___U3CU24U3Eurl_6; }
	inline void set_U3CU24U3Eurl_6(String_t* value)
	{
		___U3CU24U3Eurl_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eurl_6, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EcancellationToken_7() { return static_cast<int32_t>(offsetof(U3CGetWWWCoreU3Ec__Iterator6_t2275077895, ___U3CU24U3EcancellationToken_7)); }
	inline CancellationToken_t1439151560  get_U3CU24U3EcancellationToken_7() const { return ___U3CU24U3EcancellationToken_7; }
	inline CancellationToken_t1439151560 * get_address_of_U3CU24U3EcancellationToken_7() { return &___U3CU24U3EcancellationToken_7; }
	inline void set_U3CU24U3EcancellationToken_7(CancellationToken_t1439151560  value)
	{
		___U3CU24U3EcancellationToken_7 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Eobserver_8() { return static_cast<int32_t>(offsetof(U3CGetWWWCoreU3Ec__Iterator6_t2275077895, ___U3CU24U3Eobserver_8)); }
	inline Il2CppObject* get_U3CU24U3Eobserver_8() const { return ___U3CU24U3Eobserver_8; }
	inline Il2CppObject** get_address_of_U3CU24U3Eobserver_8() { return &___U3CU24U3Eobserver_8; }
	inline void set_U3CU24U3Eobserver_8(Il2CppObject* value)
	{
		___U3CU24U3Eobserver_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eobserver_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
