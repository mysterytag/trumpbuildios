﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.TakeObservable`1<UniRx.Unit>
struct TakeObservable_1_t505031811;
// System.Object
struct Il2CppObject;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4165376397.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.TakeObservable`1/Take_<UniRx.Unit>
struct  Take__t4189452851  : public OperatorObserverBase_2_t4165376397
{
public:
	// UniRx.Operators.TakeObservable`1<T> UniRx.Operators.TakeObservable`1/Take_::parent
	TakeObservable_1_t505031811 * ___parent_2;
	// System.Object UniRx.Operators.TakeObservable`1/Take_::gate
	Il2CppObject * ___gate_3;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(Take__t4189452851, ___parent_2)); }
	inline TakeObservable_1_t505031811 * get_parent_2() const { return ___parent_2; }
	inline TakeObservable_1_t505031811 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(TakeObservable_1_t505031811 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_gate_3() { return static_cast<int32_t>(offsetof(Take__t4189452851, ___gate_3)); }
	inline Il2CppObject * get_gate_3() const { return ___gate_3; }
	inline Il2CppObject ** get_address_of_gate_3() { return &___gate_3; }
	inline void set_gate_3(Il2CppObject * value)
	{
		___gate_3 = value;
		Il2CppCodeGenWriteBarrier(&___gate_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
