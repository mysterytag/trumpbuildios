﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Triggers.ObservableFixedUpdateTrigger
struct ObservableFixedUpdateTrigger_t3436683039;
// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Triggers.ObservableFixedUpdateTrigger::.ctor()
extern "C"  void ObservableFixedUpdateTrigger__ctor_m2233421182 (ObservableFixedUpdateTrigger_t3436683039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableFixedUpdateTrigger::FixedUpdate()
extern "C"  void ObservableFixedUpdateTrigger_FixedUpdate_m286037113 (ObservableFixedUpdateTrigger_t3436683039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UniRx.Unit> UniRx.Triggers.ObservableFixedUpdateTrigger::FixedUpdateAsObservable()
extern "C"  Il2CppObject* ObservableFixedUpdateTrigger_FixedUpdateAsObservable_m706320438 (ObservableFixedUpdateTrigger_t3436683039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableFixedUpdateTrigger::RaiseOnCompletedOnDestroy()
extern "C"  void ObservableFixedUpdateTrigger_RaiseOnCompletedOnDestroy_m315914679 (ObservableFixedUpdateTrigger_t3436683039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
