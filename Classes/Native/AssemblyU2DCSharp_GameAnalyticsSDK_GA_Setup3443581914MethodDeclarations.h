﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_GameAnalyticsSDK_GA_Setup_GAGend3167968315.h"

// System.Void GameAnalyticsSDK.GA_Setup::SetAvailableCustomDimensions01(System.Collections.Generic.List`1<System.String>)
extern "C"  void GA_Setup_SetAvailableCustomDimensions01_m236814931 (Il2CppObject * __this /* static, unused */, List_1_t1765447871 * ___customDimensions0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Setup::SetAvailableCustomDimensions02(System.Collections.Generic.List`1<System.String>)
extern "C"  void GA_Setup_SetAvailableCustomDimensions02_m1370005524 (Il2CppObject * __this /* static, unused */, List_1_t1765447871 * ___customDimensions0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Setup::SetAvailableCustomDimensions03(System.Collections.Generic.List`1<System.String>)
extern "C"  void GA_Setup_SetAvailableCustomDimensions03_m2503196117 (Il2CppObject * __this /* static, unused */, List_1_t1765447871 * ___customDimensions0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Setup::SetAvailableResourceCurrencies(System.Collections.Generic.List`1<System.String>)
extern "C"  void GA_Setup_SetAvailableResourceCurrencies_m3966978385 (Il2CppObject * __this /* static, unused */, List_1_t1765447871 * ___resourceCurrencies0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Setup::SetAvailableResourceItemTypes(System.Collections.Generic.List`1<System.String>)
extern "C"  void GA_Setup_SetAvailableResourceItemTypes_m2170578382 (Il2CppObject * __this /* static, unused */, List_1_t1765447871 * ___resourceItemTypes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Setup::SetInfoLog(System.Boolean)
extern "C"  void GA_Setup_SetInfoLog_m2649153220 (Il2CppObject * __this /* static, unused */, bool ___enabled0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Setup::SetVerboseLog(System.Boolean)
extern "C"  void GA_Setup_SetVerboseLog_m3524959492 (Il2CppObject * __this /* static, unused */, bool ___enabled0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Setup::SetFacebookId(System.String)
extern "C"  void GA_Setup_SetFacebookId_m2008289238 (Il2CppObject * __this /* static, unused */, String_t* ___facebookId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Setup::SetGender(GameAnalyticsSDK.GA_Setup/GAGender)
extern "C"  void GA_Setup_SetGender_m2564531874 (Il2CppObject * __this /* static, unused */, int32_t ___gender0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Setup::SetBirthYear(System.Int32)
extern "C"  void GA_Setup_SetBirthYear_m1994358724 (Il2CppObject * __this /* static, unused */, int32_t ___birthYear0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Setup::SetCustomDimension01(System.String)
extern "C"  void GA_Setup_SetCustomDimension01_m1053896085 (Il2CppObject * __this /* static, unused */, String_t* ___customDimension0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Setup::SetCustomDimension02(System.String)
extern "C"  void GA_Setup_SetCustomDimension02_m543361908 (Il2CppObject * __this /* static, unused */, String_t* ___customDimension0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Setup::SetCustomDimension03(System.String)
extern "C"  void GA_Setup_SetCustomDimension03_m32827731 (Il2CppObject * __this /* static, unused */, String_t* ___customDimension0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
