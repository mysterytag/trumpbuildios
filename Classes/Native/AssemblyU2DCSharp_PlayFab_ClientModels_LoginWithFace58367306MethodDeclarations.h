﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.LoginWithFacebookRequest
struct LoginWithFacebookRequest_t58367306;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen3097043249.h"

// System.Void PlayFab.ClientModels.LoginWithFacebookRequest::.ctor()
extern "C"  void LoginWithFacebookRequest__ctor_m660888627 (LoginWithFacebookRequest_t58367306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LoginWithFacebookRequest::get_TitleId()
extern "C"  String_t* LoginWithFacebookRequest_get_TitleId_m2821035038 (LoginWithFacebookRequest_t58367306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LoginWithFacebookRequest::set_TitleId(System.String)
extern "C"  void LoginWithFacebookRequest_set_TitleId_m819755995 (LoginWithFacebookRequest_t58367306 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LoginWithFacebookRequest::get_AccessToken()
extern "C"  String_t* LoginWithFacebookRequest_get_AccessToken_m387083264 (LoginWithFacebookRequest_t58367306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LoginWithFacebookRequest::set_AccessToken(System.String)
extern "C"  void LoginWithFacebookRequest_set_AccessToken_m753930169 (LoginWithFacebookRequest_t58367306 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.LoginWithFacebookRequest::get_CreateAccount()
extern "C"  Nullable_1_t3097043249  LoginWithFacebookRequest_get_CreateAccount_m3978212290 (LoginWithFacebookRequest_t58367306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LoginWithFacebookRequest::set_CreateAccount(System.Nullable`1<System.Boolean>)
extern "C"  void LoginWithFacebookRequest_set_CreateAccount_m749265923 (LoginWithFacebookRequest_t58367306 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
