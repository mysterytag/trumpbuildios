﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`2<UnityEngine.Transform,System.Boolean>
struct Func_2_t884531080;
// System.Func`2<UnityEngine.Transform,UnityEngine.GameObject>
struct Func_2_t391253545;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.Util.UnityUtil
struct  UnityUtil_t126913041  : public Il2CppObject
{
public:

public:
};

struct UnityUtil_t126913041_StaticFields
{
public:
	// System.Func`2<UnityEngine.Transform,System.Boolean> ModestTree.Util.UnityUtil::<>f__am$cache0
	Func_2_t884531080 * ___U3CU3Ef__amU24cache0_0;
	// System.Func`2<UnityEngine.Transform,UnityEngine.GameObject> ModestTree.Util.UnityUtil::<>f__am$cache1
	Func_2_t391253545 * ___U3CU3Ef__amU24cache1_1;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(UnityUtil_t126913041_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline Func_2_t884531080 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline Func_2_t884531080 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(Func_2_t884531080 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(UnityUtil_t126913041_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline Func_2_t391253545 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline Func_2_t391253545 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(Func_2_t391253545 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
