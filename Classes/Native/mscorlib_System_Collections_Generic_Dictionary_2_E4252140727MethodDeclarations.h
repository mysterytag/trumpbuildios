﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1306794257MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3977708281(__this, ___dictionary0, method) ((  void (*) (Enumerator_t4252140727 *, Dictionary_2_t190145490 *, const MethodInfo*))Enumerator__ctor_m2232813323_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1924825106(__this, method) ((  Il2CppObject * (*) (Enumerator_t4252140727 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2861177408_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2645459996(__this, method) ((  void (*) (Enumerator_t4252140727 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1729157322_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m467311059(__this, method) ((  DictionaryEntry_t130027246  (*) (Enumerator_t4252140727 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3820758529_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2727995246(__this, method) ((  Il2CppObject * (*) (Enumerator_t4252140727 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2217518876_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2721817216(__this, method) ((  Il2CppObject * (*) (Enumerator_t4252140727 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1780297390_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::MoveNext()
#define Enumerator_MoveNext_m904304309(__this, method) ((  bool (*) (Enumerator_t4252140727 *, const MethodInfo*))Enumerator_MoveNext_m1990959482_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::get_Current()
#define Enumerator_get_Current_m1278295704(__this, method) ((  KeyValuePair_2_t3973644084  (*) (Enumerator_t4252140727 *, const MethodInfo*))Enumerator_get_Current_m2369121538_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m3587144021(__this, method) ((  String_t* (*) (Enumerator_t4252140727 *, const MethodInfo*))Enumerator_get_CurrentKey_m1962050691_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m4108977749(__this, method) ((  int64_t (*) (Enumerator_t4252140727 *, const MethodInfo*))Enumerator_get_CurrentValue_m1467416067_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::Reset()
#define Enumerator_Reset_m2006071243(__this, method) ((  void (*) (Enumerator_t4252140727 *, const MethodInfo*))Enumerator_Reset_m800276701_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::VerifyState()
#define Enumerator_VerifyState_m2641445780(__this, method) ((  void (*) (Enumerator_t4252140727 *, const MethodInfo*))Enumerator_VerifyState_m3373774630_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m2848594428(__this, method) ((  void (*) (Enumerator_t4252140727 *, const MethodInfo*))Enumerator_VerifyCurrent_m2241982734_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::Dispose()
#define Enumerator_Dispose_m1917577691(__this, method) ((  void (*) (Enumerator_t4252140727 *, const MethodInfo*))Enumerator_Dispose_m2790192749_gshared)(__this, method)
