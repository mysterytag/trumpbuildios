﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey96`1<System.Boolean>
struct U3CAsObservableU3Ec__AnonStorey96_1_t3903656010;
// UnityEngine.Events.UnityAction`1<System.Boolean>
struct UnityAction_1_t191467246;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey96`1<System.Boolean>::.ctor()
extern "C"  void U3CAsObservableU3Ec__AnonStorey96_1__ctor_m3460586284_gshared (U3CAsObservableU3Ec__AnonStorey96_1_t3903656010 * __this, const MethodInfo* method);
#define U3CAsObservableU3Ec__AnonStorey96_1__ctor_m3460586284(__this, method) ((  void (*) (U3CAsObservableU3Ec__AnonStorey96_1_t3903656010 *, const MethodInfo*))U3CAsObservableU3Ec__AnonStorey96_1__ctor_m3460586284_gshared)(__this, method)
// System.Void UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey96`1<System.Boolean>::<>m__CA(UnityEngine.Events.UnityAction`1<T>)
extern "C"  void U3CAsObservableU3Ec__AnonStorey96_1_U3CU3Em__CA_m719160567_gshared (U3CAsObservableU3Ec__AnonStorey96_1_t3903656010 * __this, UnityAction_1_t191467246 * ___h0, const MethodInfo* method);
#define U3CAsObservableU3Ec__AnonStorey96_1_U3CU3Em__CA_m719160567(__this, ___h0, method) ((  void (*) (U3CAsObservableU3Ec__AnonStorey96_1_t3903656010 *, UnityAction_1_t191467246 *, const MethodInfo*))U3CAsObservableU3Ec__AnonStorey96_1_U3CU3Em__CA_m719160567_gshared)(__this, ___h0, method)
// System.Void UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey96`1<System.Boolean>::<>m__CB(UnityEngine.Events.UnityAction`1<T>)
extern "C"  void U3CAsObservableU3Ec__AnonStorey96_1_U3CU3Em__CB_m2351964566_gshared (U3CAsObservableU3Ec__AnonStorey96_1_t3903656010 * __this, UnityAction_1_t191467246 * ___h0, const MethodInfo* method);
#define U3CAsObservableU3Ec__AnonStorey96_1_U3CU3Em__CB_m2351964566(__this, ___h0, method) ((  void (*) (U3CAsObservableU3Ec__AnonStorey96_1_t3903656010 *, UnityAction_1_t191467246 *, const MethodInfo*))U3CAsObservableU3Ec__AnonStorey96_1_U3CU3Em__CB_m2351964566_gshared)(__this, ___h0, method)
