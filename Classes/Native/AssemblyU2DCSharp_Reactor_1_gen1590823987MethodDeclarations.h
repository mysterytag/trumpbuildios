﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Reactor_1_gen96911316MethodDeclarations.h"

// System.Void Reactor`1<CollectionReplaceEvent`1<DonateButton>>::.ctor()
#define Reactor_1__ctor_m281830508(__this, method) ((  void (*) (Reactor_1_t1590823987 *, const MethodInfo*))Reactor_1__ctor_m1161914514_gshared)(__this, method)
// System.Void Reactor`1<CollectionReplaceEvent`1<DonateButton>>::React(T)
#define Reactor_1_React_m3978473269(__this, ___t0, method) ((  void (*) (Reactor_1_t1590823987 *, CollectionReplaceEvent_1_t2331019091 *, const MethodInfo*))Reactor_1_React_m1196306383_gshared)(__this, ___t0, method)
// System.Void Reactor`1<CollectionReplaceEvent`1<DonateButton>>::TransactionUnpackReact(T,System.Int32)
#define Reactor_1_TransactionUnpackReact_m3334395520(__this, ___t0, ___i1, method) ((  void (*) (Reactor_1_t1590823987 *, CollectionReplaceEvent_1_t2331019091 *, int32_t, const MethodInfo*))Reactor_1_TransactionUnpackReact_m1003670938_gshared)(__this, ___t0, ___i1, method)
// System.Boolean Reactor`1<CollectionReplaceEvent`1<DonateButton>>::Empty()
#define Reactor_1_Empty_m49714507(__this, method) ((  bool (*) (Reactor_1_t1590823987 *, const MethodInfo*))Reactor_1_Empty_m132443017_gshared)(__this, method)
// System.IDisposable Reactor`1<CollectionReplaceEvent`1<DonateButton>>::AddReaction(System.Action`1<T>,Priority)
#define Reactor_1_AddReaction_m2392378779(__this, ___reaction0, ___priority1, method) ((  Il2CppObject * (*) (Reactor_1_t1590823987 *, Action_1_t2479471796 *, int32_t, const MethodInfo*))Reactor_1_AddReaction_m1495770619_gshared)(__this, ___reaction0, ___priority1, method)
// System.Void Reactor`1<CollectionReplaceEvent`1<DonateButton>>::UpgradeToMultyPriority()
#define Reactor_1_UpgradeToMultyPriority_m487519150(__this, method) ((  void (*) (Reactor_1_t1590823987 *, const MethodInfo*))Reactor_1_UpgradeToMultyPriority_m912916552_gshared)(__this, method)
// System.Void Reactor`1<CollectionReplaceEvent`1<DonateButton>>::AddToMultipriority(System.Action`1<T>,Priority)
#define Reactor_1_AddToMultipriority_m2376261577(__this, ___reaction0, ___p1, method) ((  void (*) (Reactor_1_t1590823987 *, Action_1_t2479471796 *, int32_t, const MethodInfo*))Reactor_1_AddToMultipriority_m3381282287_gshared)(__this, ___reaction0, ___p1, method)
// System.Void Reactor`1<CollectionReplaceEvent`1<DonateButton>>::RemoveReaction(System.Action`1<T>,Priority)
#define Reactor_1_RemoveReaction_m2479441213(__this, ___reaction0, ___p1, method) ((  void (*) (Reactor_1_t1590823987 *, Action_1_t2479471796 *, int32_t, const MethodInfo*))Reactor_1_RemoveReaction_m4227182179_gshared)(__this, ___reaction0, ___p1, method)
// System.Void Reactor`1<CollectionReplaceEvent`1<DonateButton>>::Clear()
#define Reactor_1_Clear_m1982931095(__this, method) ((  void (*) (Reactor_1_t1590823987 *, const MethodInfo*))Reactor_1_Clear_m2863015101_gshared)(__this, method)
