﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObservable`1<System.Int32>
struct IObservable_1_t2606213151;
// System.Func`1<System.Int32>
struct Func_1_t3990196034;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1911559758.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.StartWithObservable`1<System.Int32>
struct  StartWithObservable_1_t338430729  : public OperatorObservableBase_1_t1911559758
{
public:
	// UniRx.IObservable`1<T> UniRx.Operators.StartWithObservable`1::source
	Il2CppObject* ___source_1;
	// T UniRx.Operators.StartWithObservable`1::value
	int32_t ___value_2;
	// System.Func`1<T> UniRx.Operators.StartWithObservable`1::valueFactory
	Func_1_t3990196034 * ___valueFactory_3;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(StartWithObservable_1_t338430729, ___source_1)); }
	inline Il2CppObject* get_source_1() const { return ___source_1; }
	inline Il2CppObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(Il2CppObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier(&___source_1, value);
	}

	inline static int32_t get_offset_of_value_2() { return static_cast<int32_t>(offsetof(StartWithObservable_1_t338430729, ___value_2)); }
	inline int32_t get_value_2() const { return ___value_2; }
	inline int32_t* get_address_of_value_2() { return &___value_2; }
	inline void set_value_2(int32_t value)
	{
		___value_2 = value;
	}

	inline static int32_t get_offset_of_valueFactory_3() { return static_cast<int32_t>(offsetof(StartWithObservable_1_t338430729, ___valueFactory_3)); }
	inline Func_1_t3990196034 * get_valueFactory_3() const { return ___valueFactory_3; }
	inline Func_1_t3990196034 ** get_address_of_valueFactory_3() { return &___valueFactory_3; }
	inline void set_valueFactory_3(Func_1_t3990196034 * value)
	{
		___valueFactory_3 = value;
		Il2CppCodeGenWriteBarrier(&___valueFactory_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
