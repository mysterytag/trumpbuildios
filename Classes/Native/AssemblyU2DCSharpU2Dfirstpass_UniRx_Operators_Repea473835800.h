﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.RepeatObservable`1/<SubscribeCore>c__AnonStorey66<System.Object>
struct U3CSubscribeCoreU3Ec__AnonStorey66_t3102228927;
// UniRx.Operators.RepeatObservable`1<System.Object>
struct RepeatObservable_1_t4255784085;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.RepeatObservable`1/<SubscribeCore>c__AnonStorey67<System.Object>
struct  U3CSubscribeCoreU3Ec__AnonStorey67_t473835800  : public Il2CppObject
{
public:
	// System.Int32 UniRx.Operators.RepeatObservable`1/<SubscribeCore>c__AnonStorey67::currentCount
	int32_t ___currentCount_0;
	// UniRx.Operators.RepeatObservable`1/<SubscribeCore>c__AnonStorey66<T> UniRx.Operators.RepeatObservable`1/<SubscribeCore>c__AnonStorey67::<>f__ref$102
	U3CSubscribeCoreU3Ec__AnonStorey66_t3102228927 * ___U3CU3Ef__refU24102_1;
	// UniRx.Operators.RepeatObservable`1<T> UniRx.Operators.RepeatObservable`1/<SubscribeCore>c__AnonStorey67::<>f__this
	RepeatObservable_1_t4255784085 * ___U3CU3Ef__this_2;

public:
	inline static int32_t get_offset_of_currentCount_0() { return static_cast<int32_t>(offsetof(U3CSubscribeCoreU3Ec__AnonStorey67_t473835800, ___currentCount_0)); }
	inline int32_t get_currentCount_0() const { return ___currentCount_0; }
	inline int32_t* get_address_of_currentCount_0() { return &___currentCount_0; }
	inline void set_currentCount_0(int32_t value)
	{
		___currentCount_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24102_1() { return static_cast<int32_t>(offsetof(U3CSubscribeCoreU3Ec__AnonStorey67_t473835800, ___U3CU3Ef__refU24102_1)); }
	inline U3CSubscribeCoreU3Ec__AnonStorey66_t3102228927 * get_U3CU3Ef__refU24102_1() const { return ___U3CU3Ef__refU24102_1; }
	inline U3CSubscribeCoreU3Ec__AnonStorey66_t3102228927 ** get_address_of_U3CU3Ef__refU24102_1() { return &___U3CU3Ef__refU24102_1; }
	inline void set_U3CU3Ef__refU24102_1(U3CSubscribeCoreU3Ec__AnonStorey66_t3102228927 * value)
	{
		___U3CU3Ef__refU24102_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24102_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_2() { return static_cast<int32_t>(offsetof(U3CSubscribeCoreU3Ec__AnonStorey67_t473835800, ___U3CU3Ef__this_2)); }
	inline RepeatObservable_1_t4255784085 * get_U3CU3Ef__this_2() const { return ___U3CU3Ef__this_2; }
	inline RepeatObservable_1_t4255784085 ** get_address_of_U3CU3Ef__this_2() { return &___U3CU3Ef__this_2; }
	inline void set_U3CU3Ef__this_2(RepeatObservable_1_t4255784085 * value)
	{
		___U3CU3Ef__this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
