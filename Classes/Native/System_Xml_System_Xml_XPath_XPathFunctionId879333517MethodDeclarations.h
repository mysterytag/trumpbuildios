﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XPath.XPathFunctionId
struct XPathFunctionId_t879333517;
// System.Xml.XPath.FunctionArguments
struct FunctionArguments_t3402201403;
// System.Object
struct Il2CppObject;
// System.Xml.XPath.BaseIterator
struct BaseIterator_t3696600956;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XPath_FunctionArguments3402201403.h"
#include "System_Xml_System_Xml_XPath_XPathResultType370286193.h"
#include "System_Xml_System_Xml_XPath_BaseIterator3696600956.h"

// System.Void System.Xml.XPath.XPathFunctionId::.ctor(System.Xml.XPath.FunctionArguments)
extern "C"  void XPathFunctionId__ctor_m3649212261 (XPathFunctionId_t879333517 * __this, FunctionArguments_t3402201403 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XPath.XPathFunctionId::.cctor()
extern "C"  void XPathFunctionId__cctor_m1596924113 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionId::get_ReturnType()
extern "C"  int32_t XPathFunctionId_get_ReturnType_m2668233119 (XPathFunctionId_t879333517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.XPathFunctionId::get_Peer()
extern "C"  bool XPathFunctionId_get_Peer_m1177386971 (XPathFunctionId_t879333517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Xml.XPath.XPathFunctionId::Evaluate(System.Xml.XPath.BaseIterator)
extern "C"  Il2CppObject * XPathFunctionId_Evaluate_m2633997366 (XPathFunctionId_t879333517 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XPath.XPathFunctionId::ToString()
extern "C"  String_t* XPathFunctionId_ToString_m2274108279 (XPathFunctionId_t879333517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
