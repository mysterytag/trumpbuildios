﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/ValidateIOSReceiptResponseCallback
struct ValidateIOSReceiptResponseCallback_t4135482631;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.ValidateIOSReceiptRequest
struct ValidateIOSReceiptRequest_t1279430686;
// PlayFab.ClientModels.ValidateIOSReceiptResult
struct ValidateIOSReceiptResult_t1184798414;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ValidateIOS1279430686.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ValidateIOS1184798414.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/ValidateIOSReceiptResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void ValidateIOSReceiptResponseCallback__ctor_m3767218806 (ValidateIOSReceiptResponseCallback_t4135482631 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/ValidateIOSReceiptResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.ValidateIOSReceiptRequest,PlayFab.ClientModels.ValidateIOSReceiptResult,PlayFab.PlayFabError,System.Object)
extern "C"  void ValidateIOSReceiptResponseCallback_Invoke_m616396067 (ValidateIOSReceiptResponseCallback_t4135482631 * __this, String_t* ___urlPath0, int32_t ___callId1, ValidateIOSReceiptRequest_t1279430686 * ___request2, ValidateIOSReceiptResult_t1184798414 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_ValidateIOSReceiptResponseCallback_t4135482631(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, ValidateIOSReceiptRequest_t1279430686 * ___request2, ValidateIOSReceiptResult_t1184798414 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/ValidateIOSReceiptResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.ValidateIOSReceiptRequest,PlayFab.ClientModels.ValidateIOSReceiptResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ValidateIOSReceiptResponseCallback_BeginInvoke_m3036836880 (ValidateIOSReceiptResponseCallback_t4135482631 * __this, String_t* ___urlPath0, int32_t ___callId1, ValidateIOSReceiptRequest_t1279430686 * ___request2, ValidateIOSReceiptResult_t1184798414 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/ValidateIOSReceiptResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void ValidateIOSReceiptResponseCallback_EndInvoke_m3373682566 (ValidateIOSReceiptResponseCallback_t4135482631 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
