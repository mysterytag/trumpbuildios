﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.TakeLastObservable`1<System.Object>
struct TakeLastObservable_1_t3265267047;
// System.Collections.Generic.Queue`1<System.Object>
struct Queue_1_t2545193960;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.TakeLastObservable`1/TakeLast<System.Object>
struct  TakeLast_t798930510  : public OperatorObserverBase_2_t1187768149
{
public:
	// UniRx.Operators.TakeLastObservable`1<T> UniRx.Operators.TakeLastObservable`1/TakeLast::parent
	TakeLastObservable_1_t3265267047 * ___parent_2;
	// System.Collections.Generic.Queue`1<T> UniRx.Operators.TakeLastObservable`1/TakeLast::q
	Queue_1_t2545193960 * ___q_3;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(TakeLast_t798930510, ___parent_2)); }
	inline TakeLastObservable_1_t3265267047 * get_parent_2() const { return ___parent_2; }
	inline TakeLastObservable_1_t3265267047 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(TakeLastObservable_1_t3265267047 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_q_3() { return static_cast<int32_t>(offsetof(TakeLast_t798930510, ___q_3)); }
	inline Queue_1_t2545193960 * get_q_3() const { return ___q_3; }
	inline Queue_1_t2545193960 ** get_address_of_q_3() { return &___q_3; }
	inline void set_q_3(Queue_1_t2545193960 * value)
	{
		___q_3 = value;
		Il2CppCodeGenWriteBarrier(&___q_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
