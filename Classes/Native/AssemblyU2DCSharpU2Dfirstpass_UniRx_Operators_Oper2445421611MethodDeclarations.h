﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<System.Int64>
struct U3CSubscribeU3Ec__AnonStorey5B_t2445421611;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<System.Int64>::.ctor()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B__ctor_m2123829296_gshared (U3CSubscribeU3Ec__AnonStorey5B_t2445421611 * __this, const MethodInfo* method);
#define U3CSubscribeU3Ec__AnonStorey5B__ctor_m2123829296(__this, method) ((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t2445421611 *, const MethodInfo*))U3CSubscribeU3Ec__AnonStorey5B__ctor_m2123829296_gshared)(__this, method)
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<System.Int64>::<>m__73()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m3198709461_gshared (U3CSubscribeU3Ec__AnonStorey5B_t2445421611 * __this, const MethodInfo* method);
#define U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m3198709461(__this, method) ((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t2445421611 *, const MethodInfo*))U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m3198709461_gshared)(__this, method)
