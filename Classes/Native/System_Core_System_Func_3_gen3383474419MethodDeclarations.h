﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_3_gen2574913143MethodDeclarations.h"

// System.Void System.Func`3<System.Single,SubstanceBase`2/Intrusion<System.Single,System.Single>,System.Single>::.ctor(System.Object,System.IntPtr)
#define Func_3__ctor_m2959012258(__this, ___object0, ___method1, method) ((  void (*) (Func_3_t3383474419 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_3__ctor_m2429120871_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`3<System.Single,SubstanceBase`2/Intrusion<System.Single,System.Single>,System.Single>::Invoke(T1,T2)
#define Func_3_Invoke_m136098811(__this, ___arg10, ___arg21, method) ((  float (*) (Func_3_t3383474419 *, float, Intrusion_t78896424 *, const MethodInfo*))Func_3_Invoke_m3675757698_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Func`3<System.Single,SubstanceBase`2/Intrusion<System.Single,System.Single>,System.Single>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Func_3_BeginInvoke_m2131672576(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Func_3_t3383474419 *, float, Intrusion_t78896424 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_3_BeginInvoke_m3116481923_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// TResult System.Func`3<System.Single,SubstanceBase`2/Intrusion<System.Single,System.Single>,System.Single>::EndInvoke(System.IAsyncResult)
#define Func_3_EndInvoke_m2821060672(__this, ___result0, method) ((  float (*) (Func_3_t3383474419 *, Il2CppObject *, const MethodInfo*))Func_3_EndInvoke_m3326496409_gshared)(__this, ___result0, method)
