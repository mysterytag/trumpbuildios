﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1634065389MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::.ctor()
#define List_1__ctor_m1713629699(__this, method) ((  void (*) (List_1_t1813872974 *, const MethodInfo*))List_1__ctor_m348833073_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m1834548892(__this, ___collection0, method) ((  void (*) (List_1_t1813872974 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1160795371_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::.ctor(System.Int32)
#define List_1__ctor_m4021007060(__this, ___capacity0, method) ((  void (*) (List_1_t1813872974 *, int32_t, const MethodInfo*))List_1__ctor_m2892763214_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::.cctor()
#define List_1__cctor_m1100816906(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3826137881_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m728464021(__this, method) ((  Il2CppObject* (*) (List_1_t1813872974 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2808422246_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m2893265057(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1813872974 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m4034025648_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3399848156(__this, method) ((  Il2CppObject * (*) (List_1_t1813872974 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1841330603_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m2630406293(__this, ___item0, method) ((  int32_t (*) (List_1_t1813872974 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3794749222_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m399151447(__this, ___item0, method) ((  bool (*) (List_1_t1813872974 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2659633254_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m592753773(__this, ___item0, method) ((  int32_t (*) (List_1_t1813872974 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3431692926_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m1653123864(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1813872974 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2067529129_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m1025169168(__this, ___item0, method) ((  void (*) (List_1_t1813872974 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1644145887_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m636354712(__this, method) ((  bool (*) (List_1_t1813872974 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1299706087_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m2646258341(__this, method) ((  bool (*) (List_1_t1813872974 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3867536694_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m1753511377(__this, method) ((  Il2CppObject * (*) (List_1_t1813872974 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m4244374434_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m538168838(__this, method) ((  bool (*) (List_1_t1813872974 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m432946261_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m1164105779(__this, method) ((  bool (*) (List_1_t1813872974 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2961826820_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m2562864920(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t1813872974 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3985478825_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m128086255(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1813872974 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3234554688_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::Add(T)
#define List_1_Add_m2677819932(__this, ___item0, method) ((  void (*) (List_1_t1813872974 *, TaskInfo_t1016914005 *, const MethodInfo*))List_1_Add_m642669291_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m4244722263(__this, ___newCount0, method) ((  void (*) (List_1_t1813872974 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m4122600870_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m3296365909(__this, ___collection0, method) ((  void (*) (List_1_t1813872974 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2478449828_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m2557338133(__this, ___enumerable0, method) ((  void (*) (List_1_t1813872974 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1739422052_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m3638121314(__this, ___collection0, method) ((  void (*) (List_1_t1813872974 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2229151411_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::AsReadOnly()
#define List_1_AsReadOnly_m2368207751(__this, method) ((  ReadOnlyCollection_1_t4180059353 * (*) (List_1_t1813872974 *, const MethodInfo*))List_1_AsReadOnly_m769820182_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::Clear()
#define List_1_Clear_m3414730286(__this, method) ((  void (*) (List_1_t1813872974 *, const MethodInfo*))List_1_Clear_m454602559_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::Contains(T)
#define List_1_Contains_m1004852572(__this, ___item0, method) ((  bool (*) (List_1_t1813872974 *, TaskInfo_t1016914005 *, const MethodInfo*))List_1_Contains_m4186092781_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m4238762764(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1813872974 *, TaskInfoU5BU5D_t2133570232*, int32_t, const MethodInfo*))List_1_CopyTo_m3988356635_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::Find(System.Predicate`1<T>)
#define List_1_Find_m947383516(__this, ___match0, method) ((  TaskInfo_t1016914005 * (*) (List_1_t1813872974 *, Predicate_1_t1587877903 *, const MethodInfo*))List_1_Find_m3379773421_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m646736215(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t1587877903 *, const MethodInfo*))List_1_CheckMatch_m3390394152_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m464994684(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t1813872974 *, int32_t, int32_t, Predicate_1_t1587877903 *, const MethodInfo*))List_1_GetIndex_m4275988045_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m1521144331(__this, ___action0, method) ((  void (*) (List_1_t1813872974 *, Action_1_t1165366710 *, const MethodInfo*))List_1_ForEach_m2030348444_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::GetEnumerator()
#define List_1_GetEnumerator_m3984866457(__this, method) ((  Enumerator_t4194623263  (*) (List_1_t1813872974 *, const MethodInfo*))List_1_GetEnumerator_m1820263692_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::IndexOf(T)
#define List_1_IndexOf_m955324304(__this, ___item0, method) ((  int32_t (*) (List_1_t1813872974 *, TaskInfo_t1016914005 *, const MethodInfo*))List_1_IndexOf_m1752303327_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m3441454435(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t1813872974 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3807054194_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m3985129948(__this, ___index0, method) ((  void (*) (List_1_t1813872974 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3734723819_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::Insert(System.Int32,T)
#define List_1_Insert_m307874947(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1813872974 *, int32_t, TaskInfo_t1016914005 *, const MethodInfo*))List_1_Insert_m3427163986_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m2943409848(__this, ___collection0, method) ((  void (*) (List_1_t1813872974 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2905071175_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::Remove(T)
#define List_1_Remove_m4022811735(__this, ___item0, method) ((  bool (*) (List_1_t1813872974 *, TaskInfo_t1016914005 *, const MethodInfo*))List_1_Remove_m2747911208_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m3958821715(__this, ___match0, method) ((  int32_t (*) (List_1_t1813872974 *, Predicate_1_t1587877903 *, const MethodInfo*))List_1_RemoveAll_m2933443938_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m2476695113(__this, ___index0, method) ((  void (*) (List_1_t1813872974 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1301016856_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::Reverse()
#define List_1_Reverse_m1863477635(__this, method) ((  void (*) (List_1_t1813872974 *, const MethodInfo*))List_1_Reverse_m449081940_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::Sort()
#define List_1_Sort_m3203792127(__this, method) ((  void (*) (List_1_t1813872974 *, const MethodInfo*))List_1_Sort_m1168641486_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m1718268869(__this, ___comparer0, method) ((  void (*) (List_1_t1813872974 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m3726677974_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m1291759634(__this, ___comparison0, method) ((  void (*) (List_1_t1813872974 *, Comparison_1_t3720588881 *, const MethodInfo*))List_1_Sort_m4192185249_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::ToArray()
#define List_1_ToArray_m1357243650(__this, method) ((  TaskInfoU5BU5D_t2133570232* (*) (List_1_t1813872974 *, const MethodInfo*))List_1_ToArray_m1046025517_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::TrimExcess()
#define List_1_TrimExcess_m789389656(__this, method) ((  void (*) (List_1_t1813872974 *, const MethodInfo*))List_1_TrimExcess_m2451380967_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::get_Capacity()
#define List_1_get_Capacity_m2224736192(__this, method) ((  int32_t (*) (List_1_t1813872974 *, const MethodInfo*))List_1_get_Capacity_m543520655_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m1454911081(__this, ___value0, method) ((  void (*) (List_1_t1813872974 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1332789688_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::get_Count()
#define List_1_get_Count_m3662557163(__this, method) ((  int32_t (*) (List_1_t1813872974 *, const MethodInfo*))List_1_get_Count_m2599103100_gshared)(__this, method)
// T System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::get_Item(System.Int32)
#define List_1_get_Item_m2395010125(__this, ___index0, method) ((  TaskInfo_t1016914005 * (*) (List_1_t1813872974 *, int32_t, const MethodInfo*))List_1_get_Item_m2771401372_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>>::set_Item(System.Int32,T)
#define List_1_set_Item_m1324677274(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1813872974 *, int32_t, TaskInfo_t1016914005 *, const MethodInfo*))List_1_set_Item_m1074271145_gshared)(__this, ___index0, ___value1, method)
