﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.TakeUntilObservable`2/TakeUntil<UniRx.Unit,UniRx.Unit>
struct TakeUntil_t1361590388;
// UniRx.Operators.TakeUntilObservable`2<UniRx.Unit,UniRx.Unit>
struct TakeUntilObservable_2_t2928839418;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.TakeUntilObservable`2/TakeUntil<UniRx.Unit,UniRx.Unit>::.ctor(UniRx.Operators.TakeUntilObservable`2<T,TOther>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void TakeUntil__ctor_m288231061_gshared (TakeUntil_t1361590388 * __this, TakeUntilObservable_2_t2928839418 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define TakeUntil__ctor_m288231061(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (TakeUntil_t1361590388 *, TakeUntilObservable_2_t2928839418 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))TakeUntil__ctor_m288231061_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.TakeUntilObservable`2/TakeUntil<UniRx.Unit,UniRx.Unit>::Run()
extern "C"  Il2CppObject * TakeUntil_Run_m847578668_gshared (TakeUntil_t1361590388 * __this, const MethodInfo* method);
#define TakeUntil_Run_m847578668(__this, method) ((  Il2CppObject * (*) (TakeUntil_t1361590388 *, const MethodInfo*))TakeUntil_Run_m847578668_gshared)(__this, method)
// System.Void UniRx.Operators.TakeUntilObservable`2/TakeUntil<UniRx.Unit,UniRx.Unit>::OnNext(T)
extern "C"  void TakeUntil_OnNext_m2567014662_gshared (TakeUntil_t1361590388 * __this, Unit_t2558286038  ___value0, const MethodInfo* method);
#define TakeUntil_OnNext_m2567014662(__this, ___value0, method) ((  void (*) (TakeUntil_t1361590388 *, Unit_t2558286038 , const MethodInfo*))TakeUntil_OnNext_m2567014662_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.TakeUntilObservable`2/TakeUntil<UniRx.Unit,UniRx.Unit>::OnError(System.Exception)
extern "C"  void TakeUntil_OnError_m772724757_gshared (TakeUntil_t1361590388 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define TakeUntil_OnError_m772724757(__this, ___error0, method) ((  void (*) (TakeUntil_t1361590388 *, Exception_t1967233988 *, const MethodInfo*))TakeUntil_OnError_m772724757_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.TakeUntilObservable`2/TakeUntil<UniRx.Unit,UniRx.Unit>::OnCompleted()
extern "C"  void TakeUntil_OnCompleted_m276382440_gshared (TakeUntil_t1361590388 * __this, const MethodInfo* method);
#define TakeUntil_OnCompleted_m276382440(__this, method) ((  void (*) (TakeUntil_t1361590388 *, const MethodInfo*))TakeUntil_OnCompleted_m276382440_gshared)(__this, method)
