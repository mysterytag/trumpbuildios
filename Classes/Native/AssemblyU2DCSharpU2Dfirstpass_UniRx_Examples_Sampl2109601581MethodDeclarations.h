﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Examples.Sample07_OrchestratIEnumerator/<AsyncB>c__IteratorB
struct U3CAsyncBU3Ec__IteratorB_t2109601581;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Examples.Sample07_OrchestratIEnumerator/<AsyncB>c__IteratorB::.ctor()
extern "C"  void U3CAsyncBU3Ec__IteratorB__ctor_m3873859128 (U3CAsyncBU3Ec__IteratorB_t2109601581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UniRx.Examples.Sample07_OrchestratIEnumerator/<AsyncB>c__IteratorB::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CAsyncBU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m539879012 (U3CAsyncBU3Ec__IteratorB_t2109601581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UniRx.Examples.Sample07_OrchestratIEnumerator/<AsyncB>c__IteratorB::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CAsyncBU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m857813496 (U3CAsyncBU3Ec__IteratorB_t2109601581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.Examples.Sample07_OrchestratIEnumerator/<AsyncB>c__IteratorB::MoveNext()
extern "C"  bool U3CAsyncBU3Ec__IteratorB_MoveNext_m3271614604 (U3CAsyncBU3Ec__IteratorB_t2109601581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample07_OrchestratIEnumerator/<AsyncB>c__IteratorB::Dispose()
extern "C"  void U3CAsyncBU3Ec__IteratorB_Dispose_m3235226741 (U3CAsyncBU3Ec__IteratorB_t2109601581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample07_OrchestratIEnumerator/<AsyncB>c__IteratorB::Reset()
extern "C"  void U3CAsyncBU3Ec__IteratorB_Reset_m1520292069 (U3CAsyncBU3Ec__IteratorB_t2109601581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
