﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_Collection2806094150MethodDeclarations.h"

// System.Void System.Collections.ObjectModel.Collection`1<SettingsButton>::.ctor()
#define Collection_1__ctor_m2536569756(__this, method) ((  void (*) (Collection_1_t2884244391 *, const MethodInfo*))Collection_1__ctor_m1690372513_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<SettingsButton>::.ctor(System.Collections.Generic.IList`1<T>)
#define Collection_1__ctor_m4010802425(__this, ___list0, method) ((  void (*) (Collection_1_t2884244391 *, Il2CppObject*, const MethodInfo*))Collection_1__ctor_m3333275156_gshared)(__this, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SettingsButton>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2604797467(__this, method) ((  bool (*) (Collection_1_t2884244391 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1624016570_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<SettingsButton>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Collection_1_System_Collections_ICollection_CopyTo_m3225590056(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t2884244391 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m1285013187_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<SettingsButton>::System.Collections.IEnumerable.GetEnumerator()
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m1088199991(__this, method) ((  Il2CppObject * (*) (Collection_1_t2884244391 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m2828471038_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<SettingsButton>::System.Collections.IList.Add(System.Object)
#define Collection_1_System_Collections_IList_Add_m2493026918(__this, ___value0, method) ((  int32_t (*) (Collection_1_t2884244391 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m1708617267_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SettingsButton>::System.Collections.IList.Contains(System.Object)
#define Collection_1_System_Collections_IList_Contains_m3123058330(__this, ___value0, method) ((  bool (*) (Collection_1_t2884244391 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m504494585_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<SettingsButton>::System.Collections.IList.IndexOf(System.Object)
#define Collection_1_System_Collections_IList_IndexOf_m1188898238(__this, ___value0, method) ((  int32_t (*) (Collection_1_t2884244391 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m1652511499_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SettingsButton>::System.Collections.IList.Insert(System.Int32,System.Object)
#define Collection_1_System_Collections_IList_Insert_m1693667889(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2884244391 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m2187188150_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<SettingsButton>::System.Collections.IList.Remove(System.Object)
#define Collection_1_System_Collections_IList_Remove_m894540631(__this, ___value0, method) ((  void (*) (Collection_1_t2884244391 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m2625864114_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SettingsButton>::System.Collections.ICollection.get_IsSynchronized()
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m1444119170(__this, method) ((  bool (*) (Collection_1_t2884244391 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m1302589123_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<SettingsButton>::System.Collections.ICollection.get_SyncRoot()
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m3575906164(__this, method) ((  Il2CppObject * (*) (Collection_1_t2884244391 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1873829551_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SettingsButton>::System.Collections.IList.get_IsFixedSize()
#define Collection_1_System_Collections_IList_get_IsFixedSize_m4190192009(__this, method) ((  bool (*) (Collection_1_t2884244391 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m2813777960_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SettingsButton>::System.Collections.IList.get_IsReadOnly()
#define Collection_1_System_Collections_IList_get_IsReadOnly_m3360122960(__this, method) ((  bool (*) (Collection_1_t2884244391 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m1376059857_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<SettingsButton>::System.Collections.IList.get_Item(System.Int32)
#define Collection_1_System_Collections_IList_get_Item_m1537598459(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t2884244391 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m2224513142_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SettingsButton>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define Collection_1_System_Collections_IList_set_Item_m436188616(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2884244391 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m2262756877_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<SettingsButton>::Add(T)
#define Collection_1_Add_m71967075(__this, ___item0, method) ((  void (*) (Collection_1_t2884244391 *, SettingsButton_t915256661 *, const MethodInfo*))Collection_1_Add_m321765054_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SettingsButton>::Clear()
#define Collection_1_Clear_m4237670343(__this, method) ((  void (*) (Collection_1_t2884244391 *, const MethodInfo*))Collection_1_Clear_m3391473100_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<SettingsButton>::ClearItems()
#define Collection_1_ClearItems_m2321853851(__this, method) ((  void (*) (Collection_1_t2884244391 *, const MethodInfo*))Collection_1_ClearItems_m2738199222_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SettingsButton>::Contains(T)
#define Collection_1_Contains_m1869121081(__this, ___item0, method) ((  bool (*) (Collection_1_t2884244391 *, SettingsButton_t915256661 *, const MethodInfo*))Collection_1_Contains_m1050871674_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SettingsButton>::CopyTo(T[],System.Int32)
#define Collection_1_CopyTo_m2456655507(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t2884244391 *, SettingsButtonU5BU5D_t3103517112*, int32_t, const MethodInfo*))Collection_1_CopyTo_m1746187054_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<SettingsButton>::GetEnumerator()
#define Collection_1_GetEnumerator_m144770576(__this, method) ((  Il2CppObject* (*) (Collection_1_t2884244391 *, const MethodInfo*))Collection_1_GetEnumerator_m625631581_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<SettingsButton>::IndexOf(T)
#define Collection_1_IndexOf_m1215648671(__this, ___item0, method) ((  int32_t (*) (Collection_1_t2884244391 *, SettingsButton_t915256661 *, const MethodInfo*))Collection_1_IndexOf_m3101447730_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SettingsButton>::Insert(System.Int32,T)
#define Collection_1_Insert_m2241213898(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2884244391 *, int32_t, SettingsButton_t915256661 *, const MethodInfo*))Collection_1_Insert_m1208073509_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<SettingsButton>::InsertItem(System.Int32,T)
#define Collection_1_InsertItem_m575237885(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2884244391 *, int32_t, SettingsButton_t915256661 *, const MethodInfo*))Collection_1_InsertItem_m714854616_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SettingsButton>::Remove(T)
#define Collection_1_Remove_m4081811572(__this, ___item0, method) ((  bool (*) (Collection_1_t2884244391 *, SettingsButton_t915256661 *, const MethodInfo*))Collection_1_Remove_m2181520885_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SettingsButton>::RemoveAt(System.Int32)
#define Collection_1_RemoveAt_m115066768(__this, ___index0, method) ((  void (*) (Collection_1_t2884244391 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m3376893675_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SettingsButton>::RemoveItem(System.Int32)
#define Collection_1_RemoveItem_m1809639344(__this, ___index0, method) ((  void (*) (Collection_1_t2884244391 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m1099170891_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<SettingsButton>::get_Count()
#define Collection_1_get_Count_m3142677948(__this, method) ((  int32_t (*) (Collection_1_t2884244391 *, const MethodInfo*))Collection_1_get_Count_m1472906633_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<SettingsButton>::get_Item(System.Int32)
#define Collection_1_get_Item_m505166902(__this, ___index0, method) ((  SettingsButton_t915256661 * (*) (Collection_1_t2884244391 *, int32_t, const MethodInfo*))Collection_1_get_Item_m2356360623_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SettingsButton>::set_Item(System.Int32,T)
#define Collection_1_set_Item_m3837537313(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2884244391 *, int32_t, SettingsButton_t915256661 *, const MethodInfo*))Collection_1_set_Item_m3127068860_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<SettingsButton>::SetItem(System.Int32,T)
#define Collection_1_SetItem_m2074743864(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2884244391 *, int32_t, SettingsButton_t915256661 *, const MethodInfo*))Collection_1_SetItem_m112162877_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SettingsButton>::IsValidItem(System.Object)
#define Collection_1_IsValidItem_m443912915(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m1993492338_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<SettingsButton>::ConvertItem(System.Object)
#define Collection_1_ConvertItem_m1406197781(__this /* static, unused */, ___item0, method) ((  SettingsButton_t915256661 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m1655469326_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SettingsButton>::CheckWritable(System.Collections.Generic.IList`1<T>)
#define Collection_1_CheckWritable_m21812691(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m651250670_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SettingsButton>::IsSynchronized(System.Collections.Generic.IList`1<T>)
#define Collection_1_IsSynchronized_m1041171473(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m2469749778_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SettingsButton>::IsFixedSize(System.Collections.Generic.IList`1<T>)
#define Collection_1_IsFixedSize_m3482788910(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m3893865421_gshared)(__this /* static, unused */, ___list0, method)
