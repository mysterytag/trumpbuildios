﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellUtils.SingleAssignmentDisposable
struct SingleAssignmentDisposable_t1832432170;
// System.IDisposable
struct IDisposable_t1628921374;

#include "codegen/il2cpp-codegen.h"

// System.Void CellUtils.SingleAssignmentDisposable::.ctor()
extern "C"  void SingleAssignmentDisposable__ctor_m2805131143 (SingleAssignmentDisposable_t1832432170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CellUtils.SingleAssignmentDisposable::get_IsDisposed()
extern "C"  bool SingleAssignmentDisposable_get_IsDisposed_m2877825417 (SingleAssignmentDisposable_t1832432170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable CellUtils.SingleAssignmentDisposable::get_Disposable()
extern "C"  Il2CppObject * SingleAssignmentDisposable_get_Disposable_m4063720345 (SingleAssignmentDisposable_t1832432170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CellUtils.SingleAssignmentDisposable::set_Disposable(System.IDisposable)
extern "C"  void SingleAssignmentDisposable_set_Disposable_m1918433680 (SingleAssignmentDisposable_t1832432170 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CellUtils.SingleAssignmentDisposable::Dispose()
extern "C"  void SingleAssignmentDisposable_Dispose_m2684816900 (SingleAssignmentDisposable_t1832432170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
