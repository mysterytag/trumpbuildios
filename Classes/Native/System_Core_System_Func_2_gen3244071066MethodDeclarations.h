﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen1844407557MethodDeclarations.h"

// System.Void System.Func`2<System.Collections.Generic.KeyValuePair`2<System.IDisposable,System.Action`1<System.Object>>,System.Boolean>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m3213305948(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t3244071066 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m1217527044_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<System.Collections.Generic.KeyValuePair`2<System.IDisposable,System.Action`1<System.Object>>,System.Boolean>::Invoke(T)
#define Func_2_Invoke_m4102671366(__this, ___arg10, method) ((  bool (*) (Func_2_t3244071066 *, KeyValuePair_2_t3615068783 , const MethodInfo*))Func_2_Invoke_m3642283230_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<System.Collections.Generic.KeyValuePair`2<System.IDisposable,System.Action`1<System.Object>>,System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m3396811317(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t3244071066 *, KeyValuePair_2_t3615068783 , AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m2573919629_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<System.Collections.Generic.KeyValuePair`2<System.IDisposable,System.Action`1<System.Object>>,System.Boolean>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m1640257422(__this, ___result0, method) ((  bool (*) (Func_2_t3244071066 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m3946825142_gshared)(__this, ___result0, method)
