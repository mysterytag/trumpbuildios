﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct StringOptions_t2623536697;
struct StringOptions_t2623536697_marshaled_pinvoke;

extern "C" void StringOptions_t2623536697_marshal_pinvoke(const StringOptions_t2623536697& unmarshaled, StringOptions_t2623536697_marshaled_pinvoke& marshaled);
extern "C" void StringOptions_t2623536697_marshal_pinvoke_back(const StringOptions_t2623536697_marshaled_pinvoke& marshaled, StringOptions_t2623536697& unmarshaled);
extern "C" void StringOptions_t2623536697_marshal_pinvoke_cleanup(StringOptions_t2623536697_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct StringOptions_t2623536697;
struct StringOptions_t2623536697_marshaled_com;

extern "C" void StringOptions_t2623536697_marshal_com(const StringOptions_t2623536697& unmarshaled, StringOptions_t2623536697_marshaled_com& marshaled);
extern "C" void StringOptions_t2623536697_marshal_com_back(const StringOptions_t2623536697_marshaled_com& marshaled, StringOptions_t2623536697& unmarshaled);
extern "C" void StringOptions_t2623536697_marshal_com_cleanup(StringOptions_t2623536697_marshaled_com& marshaled);
