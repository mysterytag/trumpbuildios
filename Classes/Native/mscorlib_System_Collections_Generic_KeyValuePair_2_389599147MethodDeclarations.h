﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23312956448MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,PlayFab.ReflectionUtils/GetDelegate>>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3230350437(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t389599147 *, Type_t *, Il2CppObject*, const MethodInfo*))KeyValuePair_2__ctor_m4168265535_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,PlayFab.ReflectionUtils/GetDelegate>>::get_Key()
#define KeyValuePair_2_get_Key_m2603692387(__this, method) ((  Type_t * (*) (KeyValuePair_2_t389599147 *, const MethodInfo*))KeyValuePair_2_get_Key_m3256475977_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,PlayFab.ReflectionUtils/GetDelegate>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m525284004(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t389599147 *, Type_t *, const MethodInfo*))KeyValuePair_2_set_Key_m1278074762_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,PlayFab.ReflectionUtils/GetDelegate>>::get_Value()
#define KeyValuePair_2_get_Value_m3832599111(__this, method) ((  Il2CppObject* (*) (KeyValuePair_2_t389599147 *, const MethodInfo*))KeyValuePair_2_get_Value_m3899079597_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,PlayFab.ReflectionUtils/GetDelegate>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2647168164(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t389599147 *, Il2CppObject*, const MethodInfo*))KeyValuePair_2_set_Value_m2954518154_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,PlayFab.ReflectionUtils/GetDelegate>>::ToString()
#define KeyValuePair_2_ToString_m3561277028(__this, method) ((  String_t* (*) (KeyValuePair_2_t389599147 *, const MethodInfo*))KeyValuePair_2_ToString_m1313859518_gshared)(__this, method)
