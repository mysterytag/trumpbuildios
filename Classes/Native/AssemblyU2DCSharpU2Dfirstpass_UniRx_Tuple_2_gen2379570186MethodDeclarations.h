﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.Collections.IComparer
struct IComparer_t2207526184;
// System.Collections.IEqualityComparer
struct IEqualityComparer_t1661793090;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen2379570186.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.Tuple`2<System.Object,System.Int32>::.ctor(T1,T2)
extern "C"  void Tuple_2__ctor_m1925708277_gshared (Tuple_2_t2379570186 * __this, Il2CppObject * ___item10, int32_t ___item21, const MethodInfo* method);
#define Tuple_2__ctor_m1925708277(__this, ___item10, ___item21, method) ((  void (*) (Tuple_2_t2379570186 *, Il2CppObject *, int32_t, const MethodInfo*))Tuple_2__ctor_m1925708277_gshared)(__this, ___item10, ___item21, method)
// System.Int32 UniRx.Tuple`2<System.Object,System.Int32>::System.IComparable.CompareTo(System.Object)
extern "C"  int32_t Tuple_2_System_IComparable_CompareTo_m3860401504_gshared (Tuple_2_t2379570186 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define Tuple_2_System_IComparable_CompareTo_m3860401504(__this, ___obj0, method) ((  int32_t (*) (Tuple_2_t2379570186 *, Il2CppObject *, const MethodInfo*))Tuple_2_System_IComparable_CompareTo_m3860401504_gshared)(__this, ___obj0, method)
// System.Int32 UniRx.Tuple`2<System.Object,System.Int32>::UniRx.IStructuralComparable.CompareTo(System.Object,System.Collections.IComparer)
extern "C"  int32_t Tuple_2_UniRx_IStructuralComparable_CompareTo_m2475097650_gshared (Tuple_2_t2379570186 * __this, Il2CppObject * ___other0, Il2CppObject * ___comparer1, const MethodInfo* method);
#define Tuple_2_UniRx_IStructuralComparable_CompareTo_m2475097650(__this, ___other0, ___comparer1, method) ((  int32_t (*) (Tuple_2_t2379570186 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_2_UniRx_IStructuralComparable_CompareTo_m2475097650_gshared)(__this, ___other0, ___comparer1, method)
// System.Boolean UniRx.Tuple`2<System.Object,System.Int32>::UniRx.IStructuralEquatable.Equals(System.Object,System.Collections.IEqualityComparer)
extern "C"  bool Tuple_2_UniRx_IStructuralEquatable_Equals_m2785203497_gshared (Tuple_2_t2379570186 * __this, Il2CppObject * ___other0, Il2CppObject * ___comparer1, const MethodInfo* method);
#define Tuple_2_UniRx_IStructuralEquatable_Equals_m2785203497(__this, ___other0, ___comparer1, method) ((  bool (*) (Tuple_2_t2379570186 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_2_UniRx_IStructuralEquatable_Equals_m2785203497_gshared)(__this, ___other0, ___comparer1, method)
// System.Int32 UniRx.Tuple`2<System.Object,System.Int32>::UniRx.IStructuralEquatable.GetHashCode(System.Collections.IEqualityComparer)
extern "C"  int32_t Tuple_2_UniRx_IStructuralEquatable_GetHashCode_m4116156913_gshared (Tuple_2_t2379570186 * __this, Il2CppObject * ___comparer0, const MethodInfo* method);
#define Tuple_2_UniRx_IStructuralEquatable_GetHashCode_m4116156913(__this, ___comparer0, method) ((  int32_t (*) (Tuple_2_t2379570186 *, Il2CppObject *, const MethodInfo*))Tuple_2_UniRx_IStructuralEquatable_GetHashCode_m4116156913_gshared)(__this, ___comparer0, method)
// System.String UniRx.Tuple`2<System.Object,System.Int32>::UniRx.ITuple.ToString()
extern "C"  String_t* Tuple_2_UniRx_ITuple_ToString_m1025111316_gshared (Tuple_2_t2379570186 * __this, const MethodInfo* method);
#define Tuple_2_UniRx_ITuple_ToString_m1025111316(__this, method) ((  String_t* (*) (Tuple_2_t2379570186 *, const MethodInfo*))Tuple_2_UniRx_ITuple_ToString_m1025111316_gshared)(__this, method)
// T1 UniRx.Tuple`2<System.Object,System.Int32>::get_Item1()
extern "C"  Il2CppObject * Tuple_2_get_Item1_m3848247211_gshared (Tuple_2_t2379570186 * __this, const MethodInfo* method);
#define Tuple_2_get_Item1_m3848247211(__this, method) ((  Il2CppObject * (*) (Tuple_2_t2379570186 *, const MethodInfo*))Tuple_2_get_Item1_m3848247211_gshared)(__this, method)
// T2 UniRx.Tuple`2<System.Object,System.Int32>::get_Item2()
extern "C"  int32_t Tuple_2_get_Item2_m3868585547_gshared (Tuple_2_t2379570186 * __this, const MethodInfo* method);
#define Tuple_2_get_Item2_m3868585547(__this, method) ((  int32_t (*) (Tuple_2_t2379570186 *, const MethodInfo*))Tuple_2_get_Item2_m3868585547_gshared)(__this, method)
// System.Boolean UniRx.Tuple`2<System.Object,System.Int32>::Equals(System.Object)
extern "C"  bool Tuple_2_Equals_m201304029_gshared (Tuple_2_t2379570186 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define Tuple_2_Equals_m201304029(__this, ___obj0, method) ((  bool (*) (Tuple_2_t2379570186 *, Il2CppObject *, const MethodInfo*))Tuple_2_Equals_m201304029_gshared)(__this, ___obj0, method)
// System.Int32 UniRx.Tuple`2<System.Object,System.Int32>::GetHashCode()
extern "C"  int32_t Tuple_2_GetHashCode_m849819893_gshared (Tuple_2_t2379570186 * __this, const MethodInfo* method);
#define Tuple_2_GetHashCode_m849819893(__this, method) ((  int32_t (*) (Tuple_2_t2379570186 *, const MethodInfo*))Tuple_2_GetHashCode_m849819893_gshared)(__this, method)
// System.String UniRx.Tuple`2<System.Object,System.Int32>::ToString()
extern "C"  String_t* Tuple_2_ToString_m172897693_gshared (Tuple_2_t2379570186 * __this, const MethodInfo* method);
#define Tuple_2_ToString_m172897693(__this, method) ((  String_t* (*) (Tuple_2_t2379570186 *, const MethodInfo*))Tuple_2_ToString_m172897693_gshared)(__this, method)
// System.Boolean UniRx.Tuple`2<System.Object,System.Int32>::Equals(UniRx.Tuple`2<T1,T2>)
extern "C"  bool Tuple_2_Equals_m2628948332_gshared (Tuple_2_t2379570186 * __this, Tuple_2_t2379570186  ___other0, const MethodInfo* method);
#define Tuple_2_Equals_m2628948332(__this, ___other0, method) ((  bool (*) (Tuple_2_t2379570186 *, Tuple_2_t2379570186 , const MethodInfo*))Tuple_2_Equals_m2628948332_gshared)(__this, ___other0, method)
