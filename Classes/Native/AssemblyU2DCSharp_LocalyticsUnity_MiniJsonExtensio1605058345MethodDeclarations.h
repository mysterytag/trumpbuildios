﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Collections.Hashtable
struct Hashtable_t3875263730;
// System.Collections.ArrayList
struct ArrayList_t2121638921;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t2474804324;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t1634065389;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Hashtable3875263730.h"
#include "mscorlib_System_Collections_ArrayList2121638921.h"
#include "mscorlib_System_String968488902.h"

// System.String LocalyticsUnity.MiniJsonExtensions::toJson(System.Collections.Hashtable)
extern "C"  String_t* MiniJsonExtensions_toJson_m3446868454 (Il2CppObject * __this /* static, unused */, Hashtable_t3875263730 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LocalyticsUnity.MiniJsonExtensions::toJson(System.Collections.ArrayList)
extern "C"  String_t* MiniJsonExtensions_toJson_m624106927 (Il2CppObject * __this /* static, unused */, ArrayList_t2121638921 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LocalyticsUnity.MiniJsonExtensions::toJson(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  String_t* MiniJsonExtensions_toJson_m237076063 (Il2CppObject * __this /* static, unused */, Dictionary_2_t2606186806 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LocalyticsUnity.MiniJsonExtensions::toJson(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  String_t* MiniJsonExtensions_toJson_m2827529741 (Il2CppObject * __this /* static, unused */, Dictionary_2_t2474804324 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList LocalyticsUnity.MiniJsonExtensions::arrayListFromJson(System.String)
extern "C"  ArrayList_t2121638921 * MiniJsonExtensions_arrayListFromJson_m2274644125 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Object> LocalyticsUnity.MiniJsonExtensions::listFromJson(System.String)
extern "C"  List_1_t1634065389 * MiniJsonExtensions_listFromJson_m1921246219 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Hashtable LocalyticsUnity.MiniJsonExtensions::hashtableFromJson(System.String)
extern "C"  Hashtable_t3875263730 * MiniJsonExtensions_hashtableFromJson_m2450713163 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Object> LocalyticsUnity.MiniJsonExtensions::dictionaryFromJson(System.String)
extern "C"  Dictionary_2_t2474804324 * MiniJsonExtensions_dictionaryFromJson_m785051574 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
