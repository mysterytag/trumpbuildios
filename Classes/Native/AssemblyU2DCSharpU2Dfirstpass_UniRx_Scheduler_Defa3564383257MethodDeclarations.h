﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.IScheduler
struct IScheduler_t2938318244;

#include "codegen/il2cpp-codegen.h"

// UniRx.IScheduler UniRx.Scheduler/DefaultSchedulers::get_ConstantTimeOperations()
extern "C"  Il2CppObject * DefaultSchedulers_get_ConstantTimeOperations_m1411475237 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Scheduler/DefaultSchedulers::set_ConstantTimeOperations(UniRx.IScheduler)
extern "C"  void DefaultSchedulers_set_ConstantTimeOperations_m2911635658 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IScheduler UniRx.Scheduler/DefaultSchedulers::get_TailRecursion()
extern "C"  Il2CppObject * DefaultSchedulers_get_TailRecursion_m2924964748 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Scheduler/DefaultSchedulers::set_TailRecursion(UniRx.IScheduler)
extern "C"  void DefaultSchedulers_set_TailRecursion_m1463241011 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IScheduler UniRx.Scheduler/DefaultSchedulers::get_Iteration()
extern "C"  Il2CppObject * DefaultSchedulers_get_Iteration_m2327009079 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Scheduler/DefaultSchedulers::set_Iteration(UniRx.IScheduler)
extern "C"  void DefaultSchedulers_set_Iteration_m895774558 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IScheduler UniRx.Scheduler/DefaultSchedulers::get_TimeBasedOperations()
extern "C"  Il2CppObject * DefaultSchedulers_get_TimeBasedOperations_m2396324172 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Scheduler/DefaultSchedulers::set_TimeBasedOperations(UniRx.IScheduler)
extern "C"  void DefaultSchedulers_set_TimeBasedOperations_m2296016435 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IScheduler UniRx.Scheduler/DefaultSchedulers::get_AsyncConversions()
extern "C"  Il2CppObject * DefaultSchedulers_get_AsyncConversions_m4176597513 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Scheduler/DefaultSchedulers::set_AsyncConversions(UniRx.IScheduler)
extern "C"  void DefaultSchedulers_set_AsyncConversions_m1053590254 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Scheduler/DefaultSchedulers::SetDotNetCompatible()
extern "C"  void DefaultSchedulers_SetDotNetCompatible_m3360522066 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
