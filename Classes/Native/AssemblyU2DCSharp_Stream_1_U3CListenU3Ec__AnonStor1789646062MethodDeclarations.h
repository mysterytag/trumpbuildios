﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Stream`1/<Listen>c__AnonStorey128<UniRx.Unit>
struct U3CListenU3Ec__AnonStorey128_t1789646062;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

// System.Void Stream`1/<Listen>c__AnonStorey128<UniRx.Unit>::.ctor()
extern "C"  void U3CListenU3Ec__AnonStorey128__ctor_m858460540_gshared (U3CListenU3Ec__AnonStorey128_t1789646062 * __this, const MethodInfo* method);
#define U3CListenU3Ec__AnonStorey128__ctor_m858460540(__this, method) ((  void (*) (U3CListenU3Ec__AnonStorey128_t1789646062 *, const MethodInfo*))U3CListenU3Ec__AnonStorey128__ctor_m858460540_gshared)(__this, method)
// System.Void Stream`1/<Listen>c__AnonStorey128<UniRx.Unit>::<>m__1B9(T)
extern "C"  void U3CListenU3Ec__AnonStorey128_U3CU3Em__1B9_m163616409_gshared (U3CListenU3Ec__AnonStorey128_t1789646062 * __this, Unit_t2558286038  ____0, const MethodInfo* method);
#define U3CListenU3Ec__AnonStorey128_U3CU3Em__1B9_m163616409(__this, ____0, method) ((  void (*) (U3CListenU3Ec__AnonStorey128_t1789646062 *, Unit_t2558286038 , const MethodInfo*))U3CListenU3Ec__AnonStorey128_U3CU3Em__1B9_m163616409_gshared)(__this, ____0, method)
