﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.TimeoutFrameObservable`1/TimeoutFrame<System.Object>
struct TimeoutFrame_t2437429061;
// UniRx.Operators.TimeoutFrameObservable`1<System.Object>
struct TimeoutFrameObservable_1_t2457127902;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.TimeoutFrameObservable`1/TimeoutFrame<System.Object>::.ctor(UniRx.Operators.TimeoutFrameObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void TimeoutFrame__ctor_m3553775824_gshared (TimeoutFrame_t2437429061 * __this, TimeoutFrameObservable_1_t2457127902 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define TimeoutFrame__ctor_m3553775824(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (TimeoutFrame_t2437429061 *, TimeoutFrameObservable_1_t2457127902 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))TimeoutFrame__ctor_m3553775824_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.TimeoutFrameObservable`1/TimeoutFrame<System.Object>::Run()
extern "C"  Il2CppObject * TimeoutFrame_Run_m3224058181_gshared (TimeoutFrame_t2437429061 * __this, const MethodInfo* method);
#define TimeoutFrame_Run_m3224058181(__this, method) ((  Il2CppObject * (*) (TimeoutFrame_t2437429061 *, const MethodInfo*))TimeoutFrame_Run_m3224058181_gshared)(__this, method)
// System.IDisposable UniRx.Operators.TimeoutFrameObservable`1/TimeoutFrame<System.Object>::RunTimer(System.UInt64)
extern "C"  Il2CppObject * TimeoutFrame_RunTimer_m3926042265_gshared (TimeoutFrame_t2437429061 * __this, uint64_t ___timerId0, const MethodInfo* method);
#define TimeoutFrame_RunTimer_m3926042265(__this, ___timerId0, method) ((  Il2CppObject * (*) (TimeoutFrame_t2437429061 *, uint64_t, const MethodInfo*))TimeoutFrame_RunTimer_m3926042265_gshared)(__this, ___timerId0, method)
// System.Void UniRx.Operators.TimeoutFrameObservable`1/TimeoutFrame<System.Object>::OnNext(T)
extern "C"  void TimeoutFrame_OnNext_m2827895391_gshared (TimeoutFrame_t2437429061 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define TimeoutFrame_OnNext_m2827895391(__this, ___value0, method) ((  void (*) (TimeoutFrame_t2437429061 *, Il2CppObject *, const MethodInfo*))TimeoutFrame_OnNext_m2827895391_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.TimeoutFrameObservable`1/TimeoutFrame<System.Object>::OnError(System.Exception)
extern "C"  void TimeoutFrame_OnError_m1914158958_gshared (TimeoutFrame_t2437429061 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define TimeoutFrame_OnError_m1914158958(__this, ___error0, method) ((  void (*) (TimeoutFrame_t2437429061 *, Exception_t1967233988 *, const MethodInfo*))TimeoutFrame_OnError_m1914158958_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.TimeoutFrameObservable`1/TimeoutFrame<System.Object>::OnCompleted()
extern "C"  void TimeoutFrame_OnCompleted_m2917640129_gshared (TimeoutFrame_t2437429061 * __this, const MethodInfo* method);
#define TimeoutFrame_OnCompleted_m2917640129(__this, method) ((  void (*) (TimeoutFrame_t2437429061 *, const MethodInfo*))TimeoutFrame_OnCompleted_m2917640129_gshared)(__this, method)
