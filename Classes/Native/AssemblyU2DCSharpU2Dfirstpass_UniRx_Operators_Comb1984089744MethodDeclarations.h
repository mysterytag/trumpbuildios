﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.CombineLatestObservable`3/CombineLatest/RightObserver<System.Boolean,System.Boolean,System.Boolean>
struct RightObserver_t1984089744;
// UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Boolean,System.Boolean>
struct CombineLatest_t3519199716;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/RightObserver<System.Boolean,System.Boolean,System.Boolean>::.ctor(UniRx.Operators.CombineLatestObservable`3/CombineLatest<TLeft,TRight,TResult>)
extern "C"  void RightObserver__ctor_m3652537715_gshared (RightObserver_t1984089744 * __this, CombineLatest_t3519199716 * ___parent0, const MethodInfo* method);
#define RightObserver__ctor_m3652537715(__this, ___parent0, method) ((  void (*) (RightObserver_t1984089744 *, CombineLatest_t3519199716 *, const MethodInfo*))RightObserver__ctor_m3652537715_gshared)(__this, ___parent0, method)
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/RightObserver<System.Boolean,System.Boolean,System.Boolean>::OnNext(TRight)
extern "C"  void RightObserver_OnNext_m270056495_gshared (RightObserver_t1984089744 * __this, bool ___value0, const MethodInfo* method);
#define RightObserver_OnNext_m270056495(__this, ___value0, method) ((  void (*) (RightObserver_t1984089744 *, bool, const MethodInfo*))RightObserver_OnNext_m270056495_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/RightObserver<System.Boolean,System.Boolean,System.Boolean>::OnError(System.Exception)
extern "C"  void RightObserver_OnError_m2466157942_gshared (RightObserver_t1984089744 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define RightObserver_OnError_m2466157942(__this, ___error0, method) ((  void (*) (RightObserver_t1984089744 *, Exception_t1967233988 *, const MethodInfo*))RightObserver_OnError_m2466157942_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest/RightObserver<System.Boolean,System.Boolean,System.Boolean>::OnCompleted()
extern "C"  void RightObserver_OnCompleted_m840518089_gshared (RightObserver_t1984089744 * __this, const MethodInfo* method);
#define RightObserver_OnCompleted_m840518089(__this, method) ((  void (*) (RightObserver_t1984089744 *, const MethodInfo*))RightObserver_OnCompleted_m840518089_gshared)(__this, method)
