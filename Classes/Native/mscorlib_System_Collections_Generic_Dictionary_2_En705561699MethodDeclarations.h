﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>
struct Dictionary_2_t938533758;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En705561699.h"
#include "mscorlib_System_Collections_DictionaryEntry130027246.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_427065056.h"
#include "AssemblyU2DCSharp_GameAnalyticsSDK_GA_ServerFieldT3269179188.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3517588453_gshared (Enumerator_t705561700 * __this, Dictionary_2_t938533758 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m3517588453(__this, ___dictionary0, method) ((  void (*) (Enumerator_t705561700 *, Dictionary_2_t938533758 *, const MethodInfo*))Enumerator__ctor_m3517588453_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3376064358_gshared (Enumerator_t705561700 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3376064358(__this, method) ((  Il2CppObject * (*) (Enumerator_t705561700 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3376064358_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2654939056_gshared (Enumerator_t705561700 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2654939056(__this, method) ((  void (*) (Enumerator_t705561700 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2654939056_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t130027246  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1741064359_gshared (Enumerator_t705561700 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1741064359(__this, method) ((  DictionaryEntry_t130027246  (*) (Enumerator_t705561700 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1741064359_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2276212674_gshared (Enumerator_t705561700 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2276212674(__this, method) ((  Il2CppObject * (*) (Enumerator_t705561700 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2276212674_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2350462420_gshared (Enumerator_t705561700 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2350462420(__this, method) ((  Il2CppObject * (*) (Enumerator_t705561700 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2350462420_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m495617248_gshared (Enumerator_t705561700 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m495617248(__this, method) ((  bool (*) (Enumerator_t705561700 *, const MethodInfo*))Enumerator_MoveNext_m495617248_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t427065056  Enumerator_get_Current_m2814877276_gshared (Enumerator_t705561700 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2814877276(__this, method) ((  KeyValuePair_2_t427065056  (*) (Enumerator_t705561700 *, const MethodInfo*))Enumerator_get_Current_m2814877276_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m1612903401_gshared (Enumerator_t705561700 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m1612903401(__this, method) ((  int32_t (*) (Enumerator_t705561700 *, const MethodInfo*))Enumerator_get_CurrentKey_m1612903401_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m1701822569_gshared (Enumerator_t705561700 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m1701822569(__this, method) ((  Il2CppObject * (*) (Enumerator_t705561700 *, const MethodInfo*))Enumerator_get_CurrentValue_m1701822569_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m1068283575_gshared (Enumerator_t705561700 * __this, const MethodInfo* method);
#define Enumerator_Reset_m1068283575(__this, method) ((  void (*) (Enumerator_t705561700 *, const MethodInfo*))Enumerator_Reset_m1068283575_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2138300800_gshared (Enumerator_t705561700 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m2138300800(__this, method) ((  void (*) (Enumerator_t705561700 *, const MethodInfo*))Enumerator_VerifyState_m2138300800_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m362605800_gshared (Enumerator_t705561700 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m362605800(__this, method) ((  void (*) (Enumerator_t705561700 *, const MethodInfo*))Enumerator_VerifyCurrent_m362605800_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2646760903_gshared (Enumerator_t705561700 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2646760903(__this, method) ((  void (*) (Enumerator_t705561700 *, const MethodInfo*))Enumerator_Dispose_m2646760903_gshared)(__this, method)
