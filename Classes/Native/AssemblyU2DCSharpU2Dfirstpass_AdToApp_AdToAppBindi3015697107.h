﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3012272455;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdToApp.AdToAppBinding
struct  AdToAppBinding_t3015697107  : public Il2CppObject
{
public:

public:
};

struct AdToAppBinding_t3015697107_StaticFields
{
public:
	// UnityEngine.MonoBehaviour AdToApp.AdToAppBinding::_sdkDelegate
	MonoBehaviour_t3012272455 * ____sdkDelegate_0;

public:
	inline static int32_t get_offset_of__sdkDelegate_0() { return static_cast<int32_t>(offsetof(AdToAppBinding_t3015697107_StaticFields, ____sdkDelegate_0)); }
	inline MonoBehaviour_t3012272455 * get__sdkDelegate_0() const { return ____sdkDelegate_0; }
	inline MonoBehaviour_t3012272455 ** get_address_of__sdkDelegate_0() { return &____sdkDelegate_0; }
	inline void set__sdkDelegate_0(MonoBehaviour_t3012272455 * value)
	{
		____sdkDelegate_0 = value;
		Il2CppCodeGenWriteBarrier(&____sdkDelegate_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
