﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// Facebook.Unity.FacebookSettings/UrlSchemes
struct UrlSchemes_t3411608351;
// GameAnalyticsSDK.Studio
struct Studio_t2092322584;
// GameAnalyticsSDK.Game
struct Game_t3902568756;
// GooglePlayGames.BasicApi.Achievement
struct Achievement_t2886812913;
// GooglePlayGames.BasicApi.Events.IEvent
struct IEvent_t48279884;
// GooglePlayGames.BasicApi.Multiplayer.Participant
struct Participant_t2294414073;
// GooglePlayGames.BasicApi.Quests.IQuest
struct IQuest_t4055071484;
// GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata
struct ISavedGameMetadata_t2584424042;
// WindowBase
struct WindowBase_t3855465217;
// PlayFab.ReflectionUtils/ConstructorDelegate
struct ConstructorDelegate_t4072949631;
// PlayFab.ReflectionUtils/GetDelegate
struct GetDelegate_t270123739;
// PlayFab.UUnit.ObjNumFieldTest
struct ObjNumFieldTest_t1159807015;
// PlayFab.UUnit.ObjNumPropTest
struct ObjNumPropTest_t1151851726;
// PlayFab.UUnit.ObjOptNumFieldTest
struct ObjOptNumFieldTest_t3771869740;
// PlayFab.ClientModels.UserDataRecord
struct UserDataRecord_t163839734;
// PlayFab.ClientModels.CharacterResult
struct CharacterResult_t274624662;
// PlayFab.ClientModels.PlayerLeaderboardEntry
struct PlayerLeaderboardEntry_t2241891366;
// Ionic.Zlib.DeflateManager/Config
struct Config_t2024042338;
// Ionic.Zlib.WorkItem
struct WorkItem_t2657194833;
// PlayFab.ClientModels.ItemInstance
struct ItemInstance_t525144312;
// PlayFab.ClientModels.GameInfo
struct GameInfo_t1767394288;
// PlayFab.ClientModels.RegionInfo
struct RegionInfo_t3474869746;
// PlayFab.ClientModels.CatalogItem
struct CatalogItem_t4132589244;
// PlayFab.ClientModels.VirtualCurrencyRechargeTime
struct VirtualCurrencyRechargeTime_t1584308320;
// PlayFab.ClientModels.CharacterLeaderboardEntry
struct CharacterLeaderboardEntry_t3117535278;
// PlayFab.ClientModels.FriendInfo
struct FriendInfo_t3708701852;
// PlayFab.ClientModels.StatisticValue
struct StatisticValue_t3193655857;
// PlayFab.ClientModels.TradeInfo
struct TradeInfo_t1933812770;
// PlayFab.ClientModels.FacebookPlayFabIdPair
struct FacebookPlayFabIdPair_t2068825714;
// PlayFab.ClientModels.GameCenterPlayFabIdPair
struct GameCenterPlayFabIdPair_t3063128049;
// PlayFab.ClientModels.GooglePlayFabIdPair
struct GooglePlayFabIdPair_t663154143;
// PlayFab.ClientModels.KongregatePlayFabIdPair
struct KongregatePlayFabIdPair_t2985181565;
// PlayFab.ClientModels.PSNAccountPlayFabIdPair
struct PSNAccountPlayFabIdPair_t680553942;
// PlayFab.ClientModels.SteamPlayFabIdPair
struct SteamPlayFabIdPair_t2684316776;
// PlayFab.ClientModels.SharedGroupDataRecord
struct SharedGroupDataRecord_t3101610309;
// PlayFab.ClientModels.StoreItem
struct StoreItem_t2872884100;
// PlayFab.ClientModels.TitleNewsItem
struct TitleNewsItem_t2523316974;
// PlayFab.ClientModels.ItemPurchaseRequest
struct ItemPurchaseRequest_t731462315;
// PlayFab.ClientModels.CartItem
struct CartItem_t3543091843;
// PlayFab.ClientModels.PaymentOption
struct PaymentOption_t1289274891;
// PlayFab.ClientModels.StatisticUpdate
struct StatisticUpdate_t3411289641;
// PlayFab.UUnit.UUnitTestCase
struct UUnitTestCase_t14187365;
// AchievementDescription
struct AchievementDescription_t2323334509;
// FriendInfo
struct FriendInfo_t236470412;
// PlaneInfo
struct PlaneInfo_t4110763274;
// ZergRush.ImmutableList`1<System.Action`1<System.Int64>>
struct ImmutableList_1_t2499251769;
// ZergRush.ImmutableList`1<System.Action`1<System.Double>>
struct ImmutableList_1_t186353501;
// UpgradeDescriptor
struct UpgradeDescriptor_t2646513291;
// GlobalStat/ReceiptForVerify
struct ReceiptForVerify_t3763828586;
// ZergRush.ImmutableList`1<System.Action`1<System.Int32>>
struct ImmutableList_1_t2499251674;
// Cell`1<System.Int32>
struct Cell_1_t3029512419;
// DateTimeCell
struct DateTimeCell_t773798845;
// ZergRush.ImmutableList`1<System.Action`1<System.DateTime>>
struct ImmutableList_1_t4285838119;
// ZergRush.ImmutableList`1<System.Action`1<System.Single>>
struct ImmutableList_1_t610045908;
// ZergRush.ImmutableList`1<System.Action`1<System.Object>>
struct ImmutableList_1_t488943307;
// ZergRush.ImmutableList`1<System.Action`1<System.String>>
struct ImmutableList_1_t620325789;
// Cell`1<System.Single>
struct Cell_1_t1140306653;
// Tacticsoft.TableViewCell
struct TableViewCell_t776419755;
// ITableBlock
struct ITableBlock_t2428542600;
// TutorialTask
struct TutorialTask_t773278243;
// BottomButton
struct BottomButton_t1932555613;
// UIElemHideInfo
struct UIElemHideInfo_t26498675;
// ZergRush.ImmutableList`1<System.Action`1<UniRx.Unit>>
struct ImmutableList_1_t2210122925;
// ZergRush.ImmutableList`1<System.Action`1<UniRx.CollectionAddEvent`1<System.Object>>>
struct ImmutableList_1_t2068358874;
// ZergRush.ImmutableList`1<System.Action`1<CollectionMoveEvent`1<System.Object>>>
struct ImmutableList_1_t3223892268;
// CollectionMoveEvent`1<System.Object>
struct CollectionMoveEvent_1_t3572055381;
// ZergRush.ImmutableList`1<System.Action`1<CollectionRemoveEvent`1<System.Object>>>
struct ImmutableList_1_t550096145;
// CollectionRemoveEvent`1<System.Object>
struct CollectionRemoveEvent_1_t898259258;
// ZergRush.ImmutableList`1<System.Action`1<CollectionReplaceEvent`1<System.Object>>>
struct ImmutableList_1_t2149208957;
// CollectionReplaceEvent`1<System.Object>
struct CollectionReplaceEvent_1_t2497372070;
// ZergRush.ImmutableList`1<System.Action`1<System.Collections.Generic.ICollection`1<System.Object>>>
struct ImmutableList_1_t954774693;
// Cell`1<System.Object>
struct Cell_1_t1019204052;
// ICell`1<System.Object>
struct ICell_1_t2388737397;
// ICell
struct ICell_t69513547;
// ITransactionable
struct ITransactionable_t1652082095;
// ZergRush.ImmutableList`1<System.Action`1<System.Boolean>>
struct ImmutableList_1_t4157809524;
// IOrganism
struct IOrganism_t2580843579;
// IEmptyStream
struct IEmptyStream_t3684082468;
// IStream`1<System.Object>
struct IStream_1_t1389797411;
// BioProcessor`1/Instruction<System.Object>
struct Instruction_t1241108407;
// SubstanceBase`2/Intrusion<System.Object,System.Object>
struct Intrusion_t3044853612;
// SubstanceBase`2/Intrusion<System.Double,System.Double>
struct Intrusion_t3779021028;
// SubstanceBase`2/Intrusion<System.Single,System.Single>
struct Intrusion_t78896424;
// ZergRush.ImmutableList`1<System.Action`1<UniRx.InternalUtil.ImmutableList`1<System.Object>>>
struct ImmutableList_1_t1549147806;
// SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>
struct Intrusion_t1436088045;
// SubstanceBase`2/Intrusion<System.Object,System.Func`2<System.Object,System.Object>>
struct Intrusion_t48563248;
// SubstanceBase`2/Intrusion<System.Boolean,System.Boolean>
struct Intrusion_t1917188776;
// SubstanceBase`2/Intrusion<System.Single,System.Object>
struct Intrusion_t4252761119;
// SubstanceBase`2/Intrusion<System.Single,System.Func`1<System.Single>>
struct Intrusion_t1221677671;
// IProcess
struct IProcess_t4026237414;
// ZergRush.ImmutableList`1<System.Action`1<ICell`1<System.Int32>>>
struct ImmutableList_1_t4050882651;
// ICell`1<System.Int32>
struct ICell_1_t104078468;
// CoroMutex/LockProc
struct LockProc_t1973859137;
// Counter
struct Counter_t2622483932;
// LitJson.JsonData
struct JsonData_t2847671799;
// LitJson.ExporterFunc
struct ExporterFunc_t282942218;
// LitJson.ImporterFunc
struct ImporterFunc_t3385868859;
// LitJson.WriterContext
struct WriterContext_t3079472833;
// LitJson.Lexer/StateHandler
struct StateHandler_t1030384025;
// FontDef
struct FontDef_t983695638;
// GoMark
struct GoMark_t2137571317;
// ZergRush.ImmutableList`1<System.Action`1<UniRx.CollectionAddEvent`1<UpgradeButton>>>
struct ImmutableList_1_t2706719796;
// ZergRush.ImmutableList`1<System.Action`1<CollectionMoveEvent`1<UpgradeButton>>>
struct ImmutableList_1_t3862253190;
// CollectionMoveEvent`1<UpgradeButton>
struct CollectionMoveEvent_1_t4210416303;
// ZergRush.ImmutableList`1<System.Action`1<CollectionRemoveEvent`1<UpgradeButton>>>
struct ImmutableList_1_t1188457067;
// CollectionRemoveEvent`1<UpgradeButton>
struct CollectionRemoveEvent_1_t1536620180;
// ZergRush.ImmutableList`1<System.Action`1<CollectionReplaceEvent`1<UpgradeButton>>>
struct ImmutableList_1_t2787569879;
// CollectionReplaceEvent`1<UpgradeButton>
struct CollectionReplaceEvent_1_t3135732992;
// ZergRush.ImmutableList`1<System.Action`1<System.Collections.Generic.ICollection`1<UpgradeButton>>>
struct ImmutableList_1_t1593135615;
// UpgradeButton
struct UpgradeButton_t1475467342;
// ZergRush.ImmutableList`1<System.Action`1<UniRx.CollectionAddEvent`1<DonateButton>>>
struct ImmutableList_1_t1902005895;
// ZergRush.ImmutableList`1<System.Action`1<CollectionMoveEvent`1<DonateButton>>>
struct ImmutableList_1_t3057539289;
// CollectionMoveEvent`1<DonateButton>
struct CollectionMoveEvent_1_t3405702402;
// ZergRush.ImmutableList`1<System.Action`1<CollectionRemoveEvent`1<DonateButton>>>
struct ImmutableList_1_t383743166;
// CollectionRemoveEvent`1<DonateButton>
struct CollectionRemoveEvent_1_t731906279;
// ZergRush.ImmutableList`1<System.Action`1<CollectionReplaceEvent`1<DonateButton>>>
struct ImmutableList_1_t1982855978;
// CollectionReplaceEvent`1<DonateButton>
struct CollectionReplaceEvent_1_t2331019091;
// ZergRush.ImmutableList`1<System.Action`1<System.Collections.Generic.ICollection`1<DonateButton>>>
struct ImmutableList_1_t788421714;
// DonateButton
struct DonateButton_t670753441;
// ZergRush.ImmutableList`1<System.Action`1<UniRx.CollectionAddEvent`1<SettingsButton>>>
struct ImmutableList_1_t2146509115;
// ZergRush.ImmutableList`1<System.Action`1<CollectionMoveEvent`1<SettingsButton>>>
struct ImmutableList_1_t3302042509;
// CollectionMoveEvent`1<SettingsButton>
struct CollectionMoveEvent_1_t3650205622;
// ZergRush.ImmutableList`1<System.Action`1<CollectionRemoveEvent`1<SettingsButton>>>
struct ImmutableList_1_t628246386;
// CollectionRemoveEvent`1<SettingsButton>
struct CollectionRemoveEvent_1_t976409499;
// ZergRush.ImmutableList`1<System.Action`1<CollectionReplaceEvent`1<SettingsButton>>>
struct ImmutableList_1_t2227359198;
// CollectionReplaceEvent`1<SettingsButton>
struct CollectionReplaceEvent_1_t2575522311;
// ZergRush.ImmutableList`1<System.Action`1<System.Collections.Generic.ICollection`1<SettingsButton>>>
struct ImmutableList_1_t1032924934;
// SettingsButton
struct SettingsButton_t915256661;
// Zenject.ProviderBase
struct ProviderBase_t1627494391;
// Zenject.SingletonId
struct SingletonId_t1838183899;
// Zenject.SingletonLazyCreator
struct SingletonLazyCreator_t2762284194;
// Zenject.TypeValuePair
struct TypeValuePair_t620932390;
// ModestTree.Util.Tuple`2<System.Object,System.Type>
struct Tuple_2_t231167918;
// Zenject.ZenjectTypeInfo
struct ZenjectTypeInfo_t283213708;
// Zenject.PostInjectableInfo
struct PostInjectableInfo_t3080283662;
// Zenject.InjectableInfo
struct InjectableInfo_t1147709774;
// Zenject.InjectAttribute
struct InjectAttribute_t2791321440;
// Zenject.InjectOptionalAttribute
struct InjectOptionalAttribute_t899944672;
// Zenject.IInstaller
struct IInstaller_t2455223476;
// Zenject.BindingId
struct BindingId_t2965794261;
// Zenject.MonoInstaller
struct MonoInstaller_t3807575866;
// Zenject.DisposableManager/DisposableInfo
struct DisposableInfo_t1284166222;
// ModestTree.Util.Tuple`2<System.Type,System.Int32>
struct Tuple_2_t3719549051;
// Zenject.InitializableManager/InitializableInfo
struct InitializableInfo_t3830632317;
// Zenject.IInitializable
struct IInitializable_t617891835;
// Zenject.TaskUpdater`1/TaskInfo<System.Object>
struct TaskInfo_t1064508404;
// Zenject.ITickable
struct ITickable_t789512021;
// Zenject.IFixedTickable
struct IFixedTickable_t3328391607;
// Zenject.ILateTickable
struct ILateTickable_t3465810843;
// Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>
struct TaskInfo_t1016914005;
// Zenject.TaskUpdater`1/TaskInfo<Zenject.IFixedTickable>
struct TaskInfo_t3555793591;
// Zenject.TaskUpdater`1/TaskInfo<Zenject.ILateTickable>
struct TaskInfo_t3693212827;
// Zenject.PrefabSingletonId
struct PrefabSingletonId_t3643845047;
// Zenject.PrefabSingletonLazyCreator
struct PrefabSingletonLazyCreator_t3719004230;
// Zenject.DecoratorInstaller
struct DecoratorInstaller_t3984396578;

#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharp_Facebook_Unity_FacebookSettings_3411608351.h"
#include "AssemblyU2DCSharp_GameAnalyticsSDK_GA_ServerFieldT3269179188.h"
#include "AssemblyU2DCSharp_GameAnalyticsSDK_Studio2092322584.h"
#include "AssemblyU2DCSharp_GameAnalyticsSDK_Settings_HelpTy3291355544.h"
#include "AssemblyU2DCSharp_GameAnalyticsSDK_Game3902568756.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Achieve2886812913.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multipl2294414073.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Multipla597913872.h"
#include "AssemblyU2DCSharp_WindowBase3855465217.h"
#include "AssemblyU2DCSharp_PlayFab_ReflectionUtils_Construc4072949631.h"
#include "AssemblyU2DCSharp_PlayFab_ReflectionUtils_GetDelega270123739.h"
#include "AssemblyU2DCSharp_PlayFab_UUnit_Region3089759486.h"
#include "AssemblyU2DCSharp_PlayFab_UUnit_ObjNumFieldTest1159807015.h"
#include "AssemblyU2DCSharp_PlayFab_UUnit_ObjNumPropTest1151851726.h"
#include "AssemblyU2DCSharp_PlayFab_UUnit_StructNumFieldTest2313733701.h"
#include "AssemblyU2DCSharp_PlayFab_UUnit_ObjOptNumFieldTest3771869740.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UserDataReco163839734.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_CharacterRes274624662.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_PlayerLeade2241891366.h"
#include "AssemblyU2DCSharp_PlayFab_Internal_GMFB_327_testRe1684939302.h"
#include "AssemblyU2DCSharp_Ionic_Zlib_DeflateManager_Config2024042338.h"
#include "AssemblyU2DCSharp_Ionic_Zlib_WorkItem2657194833.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ItemInstance525144312.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GameInfo1767394288.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_RegionInfo3474869746.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_CatalogItem4132589244.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_VirtualCurr1584308320.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_CharacterLe3117535278.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_FriendInfo3708701852.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_StatisticVa3193655857.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_TradeInfo1933812770.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_FacebookPla2068825714.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GameCenterP3063128049.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GooglePlayFa663154143.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_KongregateP2985181565.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_PSNAccountPl680553942.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_SteamPlayFa2684316776.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_SharedGroup3101610309.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_StoreItem2872884100.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_TitleNewsIt2523316974.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ItemPurchase731462315.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_CartItem3543091843.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_PaymentOpti1289274891.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_StatisticUp3411289641.h"
#include "AssemblyU2DCSharp_PlayFab_UUnit_UUnitTestCase14187365.h"
#include "AssemblyU2DCSharp_AchievementDescription2323334509.h"
#include "AssemblyU2DCSharp_FriendInfo236470412.h"
#include "AssemblyU2DCSharp_PlaneInfo4110763274.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen2499251769.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen186353501.h"
#include "AssemblyU2DCSharp_UpgradeDescriptor2646513291.h"
#include "AssemblyU2DCSharp_GlobalStat_ReceiptForVerify3763828586.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen2499251674.h"
#include "AssemblyU2DCSharp_Cell_1_gen3029512419.h"
#include "AssemblyU2DCSharp_DateTimeCell773798845.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen4285838119.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen610045908.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen488943307.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen620325789.h"
#include "AssemblyU2DCSharp_Cell_1_gen1140306653.h"
#include "AssemblyU2DCSharp_Tacticsoft_TableViewCell776419755.h"
#include "AssemblyU2DCSharp_TutorialTask773278243.h"
#include "AssemblyU2DCSharp_BottomButton1932555613.h"
#include "AssemblyU2DCSharp_UIElemHideInfo26498675.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen2210122925.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen2068358874.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen3223892268.h"
#include "AssemblyU2DCSharp_CollectionMoveEvent_1_gen3572055381.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen550096145.h"
#include "AssemblyU2DCSharp_CollectionRemoveEvent_1_gen898259258.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen2149208957.h"
#include "AssemblyU2DCSharp_CollectionReplaceEvent_1_gen2497372070.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen954774693.h"
#include "AssemblyU2DCSharp_Cell_1_gen1019204052.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen4157809524.h"
#include "AssemblyU2DCSharp_BioProcessor_1_Instruction_gen1241108407.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_Intrusion_gen3044853612.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_Intrusion_gen3779021028.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_Intrusion_gen78896424.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen1549147806.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_Intrusion_gen1436088045.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_Intrusion_gen48563248.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_Intrusion_gen1917188776.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_Intrusion_gen4252761119.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_Intrusion_gen1221677671.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen4050882651.h"
#include "AssemblyU2DCSharp_CoroMutex_LockProc1973859137.h"
#include "AssemblyU2DCSharp_Counter2622483932.h"
#include "AssemblyU2DCSharp_SponsorPay_SPLogLevel220722135.h"
#include "AssemblyU2DCSharp_LitJson_JsonData2847671799.h"
#include "AssemblyU2DCSharp_LitJson_PropertyMetadata3942474089.h"
#include "AssemblyU2DCSharp_LitJson_ArrayMetadata4077657517.h"
#include "AssemblyU2DCSharp_LitJson_ObjectMetadata2608047315.h"
#include "AssemblyU2DCSharp_LitJson_ExporterFunc282942218.h"
#include "AssemblyU2DCSharp_LitJson_ImporterFunc3385868859.h"
#include "AssemblyU2DCSharp_LitJson_WriterContext3079472833.h"
#include "AssemblyU2DCSharp_LitJson_Lexer_StateHandler1030384025.h"
#include "AssemblyU2DCSharp_FontDef983695638.h"
#include "AssemblyU2DCSharp_GoMark2137571317.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen2706719796.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen3862253190.h"
#include "AssemblyU2DCSharp_CollectionMoveEvent_1_gen4210416303.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen1188457067.h"
#include "AssemblyU2DCSharp_CollectionRemoveEvent_1_gen1536620180.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen2787569879.h"
#include "AssemblyU2DCSharp_CollectionReplaceEvent_1_gen3135732992.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen1593135615.h"
#include "AssemblyU2DCSharp_UpgradeButton1475467342.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen1902005895.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen3057539289.h"
#include "AssemblyU2DCSharp_CollectionMoveEvent_1_gen3405702402.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen383743166.h"
#include "AssemblyU2DCSharp_CollectionRemoveEvent_1_gen731906279.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen1982855978.h"
#include "AssemblyU2DCSharp_CollectionReplaceEvent_1_gen2331019091.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen788421714.h"
#include "AssemblyU2DCSharp_DonateButton670753441.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen2146509115.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen3302042509.h"
#include "AssemblyU2DCSharp_CollectionMoveEvent_1_gen3650205622.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen628246386.h"
#include "AssemblyU2DCSharp_CollectionRemoveEvent_1_gen976409499.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen2227359198.h"
#include "AssemblyU2DCSharp_CollectionReplaceEvent_1_gen2575522311.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen1032924934.h"
#include "AssemblyU2DCSharp_SettingsButton915256661.h"
#include "AssemblyU2DCSharp_Zenject_ProviderBase1627494391.h"
#include "AssemblyU2DCSharp_Zenject_SingletonId1838183899.h"
#include "AssemblyU2DCSharp_Zenject_SingletonLazyCreator2762284194.h"
#include "AssemblyU2DCSharp_Zenject_TypeValuePair620932390.h"
#include "AssemblyU2DCSharp_ModestTree_Util_Tuple_2_gen231167918.h"
#include "AssemblyU2DCSharp_Zenject_ZenjectTypeInfo283213708.h"
#include "AssemblyU2DCSharp_Zenject_PostInjectableInfo3080283662.h"
#include "AssemblyU2DCSharp_Zenject_InjectableInfo1147709774.h"
#include "AssemblyU2DCSharp_Zenject_InjectAttribute2791321440.h"
#include "AssemblyU2DCSharp_Zenject_InjectOptionalAttribute899944672.h"
#include "AssemblyU2DCSharp_Zenject_BindingId2965794261.h"
#include "AssemblyU2DCSharp_Zenject_MonoInstaller3807575866.h"
#include "AssemblyU2DCSharp_Zenject_DisposableManager_Dispos1284166222.h"
#include "AssemblyU2DCSharp_ModestTree_Util_Tuple_2_gen3719549051.h"
#include "AssemblyU2DCSharp_Zenject_InitializableManager_Ini3830632317.h"
#include "AssemblyU2DCSharp_Zenject_TaskUpdater_1_TaskInfo_g1064508404.h"
#include "AssemblyU2DCSharp_Zenject_TaskUpdater_1_TaskInfo_g1016914005.h"
#include "AssemblyU2DCSharp_Zenject_TaskUpdater_1_TaskInfo_g3555793591.h"
#include "AssemblyU2DCSharp_Zenject_TaskUpdater_1_TaskInfo_g3693212827.h"
#include "AssemblyU2DCSharp_Zenject_PrefabSingletonId3643845047.h"
#include "AssemblyU2DCSharp_Zenject_PrefabSingletonLazyCreat3719004230.h"
#include "AssemblyU2DCSharp_Zenject_DecoratorInstaller3984396578.h"

#pragma once
// Facebook.Unity.FacebookSettings/UrlSchemes[]
struct UrlSchemesU5BU5D_t3110422918  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) UrlSchemes_t3411608351 * m_Items[1];

public:
	inline UrlSchemes_t3411608351 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UrlSchemes_t3411608351 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UrlSchemes_t3411608351 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GameAnalyticsSDK.GA_ServerFieldTypes/FieldType[]
struct FieldTypeU5BU5D_t1420599677  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// GameAnalyticsSDK.Studio[]
struct StudioU5BU5D_t3950172553  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Studio_t2092322584 * m_Items[1];

public:
	inline Studio_t2092322584 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Studio_t2092322584 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Studio_t2092322584 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GameAnalyticsSDK.Settings/HelpTypes[]
struct HelpTypesU5BU5D_t2651985161  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// GameAnalyticsSDK.Game[]
struct GameU5BU5D_t257333117  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Game_t3902568756 * m_Items[1];

public:
	inline Game_t3902568756 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Game_t3902568756 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Game_t3902568756 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GooglePlayGames.BasicApi.Achievement[]
struct AchievementU5BU5D_t3485498412  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Achievement_t2886812913 * m_Items[1];

public:
	inline Achievement_t2886812913 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Achievement_t2886812913 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Achievement_t2886812913 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GooglePlayGames.BasicApi.Events.IEvent[]
struct IEventU5BU5D_t446809093  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GooglePlayGames.BasicApi.Multiplayer.Participant[]
struct ParticipantU5BU5D_t300762372  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Participant_t2294414073 * m_Items[1];

public:
	inline Participant_t2294414073 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Participant_t2294414073 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Participant_t2294414073 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GooglePlayGames.BasicApi.Multiplayer.MatchOutcome/ParticipantResult[]
struct ParticipantResultU5BU5D_t335403185  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// GooglePlayGames.BasicApi.Quests.IQuest[]
struct IQuestU5BU5D_t41266581  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata[]
struct ISavedGameMetadataU5BU5D_t3329476783  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// WindowBase[]
struct WindowBaseU5BU5D_t2186255324  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) WindowBase_t3855465217 * m_Items[1];

public:
	inline WindowBase_t3855465217 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline WindowBase_t3855465217 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, WindowBase_t3855465217 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// PlayFab.ReflectionUtils/ConstructorDelegate[]
struct ConstructorDelegateU5BU5D_t2569897894  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) ConstructorDelegate_t4072949631 * m_Items[1];

public:
	inline ConstructorDelegate_t4072949631 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ConstructorDelegate_t4072949631 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ConstructorDelegate_t4072949631 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// PlayFab.ReflectionUtils/GetDelegate[]
struct GetDelegateU5BU5D_t1362511706  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) GetDelegate_t270123739 * m_Items[1];

public:
	inline GetDelegate_t270123739 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GetDelegate_t270123739 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GetDelegate_t270123739 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// PlayFab.UUnit.Region[]
struct RegionU5BU5D_t4128254283  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// PlayFab.UUnit.ObjNumFieldTest[]
struct ObjNumFieldTestU5BU5D_t404787038  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) ObjNumFieldTest_t1159807015 * m_Items[1];

public:
	inline ObjNumFieldTest_t1159807015 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ObjNumFieldTest_t1159807015 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ObjNumFieldTest_t1159807015 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// PlayFab.UUnit.ObjNumPropTest[]
struct ObjNumPropTestU5BU5D_t511721531  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) ObjNumPropTest_t1151851726 * m_Items[1];

public:
	inline ObjNumPropTest_t1151851726 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ObjNumPropTest_t1151851726 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ObjNumPropTest_t1151851726 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// PlayFab.UUnit.StructNumFieldTest[]
struct StructNumFieldTestU5BU5D_t1929070344  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) StructNumFieldTest_t2313733701  m_Items[1];

public:
	inline StructNumFieldTest_t2313733701  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline StructNumFieldTest_t2313733701 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, StructNumFieldTest_t2313733701  value)
	{
		m_Items[index] = value;
	}
};
// PlayFab.UUnit.ObjOptNumFieldTest[]
struct ObjOptNumFieldTestU5BU5D_t1366420389  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) ObjOptNumFieldTest_t3771869740 * m_Items[1];

public:
	inline ObjOptNumFieldTest_t3771869740 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ObjOptNumFieldTest_t3771869740 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ObjOptNumFieldTest_t3771869740 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// PlayFab.ClientModels.UserDataRecord[]
struct UserDataRecordU5BU5D_t1178519667  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) UserDataRecord_t163839734 * m_Items[1];

public:
	inline UserDataRecord_t163839734 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UserDataRecord_t163839734 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UserDataRecord_t163839734 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// PlayFab.ClientModels.CharacterResult[]
struct CharacterResultU5BU5D_t512048211  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) CharacterResult_t274624662 * m_Items[1];

public:
	inline CharacterResult_t274624662 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CharacterResult_t274624662 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CharacterResult_t274624662 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// PlayFab.ClientModels.PlayerLeaderboardEntry[]
struct PlayerLeaderboardEntryU5BU5D_t3995332739  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) PlayerLeaderboardEntry_t2241891366 * m_Items[1];

public:
	inline PlayerLeaderboardEntry_t2241891366 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline PlayerLeaderboardEntry_t2241891366 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, PlayerLeaderboardEntry_t2241891366 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// PlayFab.Internal.GMFB_327/testRegion[]
struct testRegionU5BU5D_t485442179  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// Ionic.Zlib.DeflateManager/Config[]
struct ConfigU5BU5D_t3235597527  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Config_t2024042338 * m_Items[1];

public:
	inline Config_t2024042338 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Config_t2024042338 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Config_t2024042338 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Ionic.Zlib.WorkItem[]
struct WorkItemU5BU5D_t4123805772  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) WorkItem_t2657194833 * m_Items[1];

public:
	inline WorkItem_t2657194833 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline WorkItem_t2657194833 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, WorkItem_t2657194833 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// PlayFab.ClientModels.ItemInstance[]
struct ItemInstanceU5BU5D_t1037726761  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) ItemInstance_t525144312 * m_Items[1];

public:
	inline ItemInstance_t525144312 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ItemInstance_t525144312 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ItemInstance_t525144312 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// PlayFab.ClientModels.GameInfo[]
struct GameInfoU5BU5D_t1727452753  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) GameInfo_t1767394288 * m_Items[1];

public:
	inline GameInfo_t1767394288 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GameInfo_t1767394288 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GameInfo_t1767394288 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// PlayFab.ClientModels.RegionInfo[]
struct RegionInfoU5BU5D_t4069773831  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) RegionInfo_t3474869746 * m_Items[1];

public:
	inline RegionInfo_t3474869746 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline RegionInfo_t3474869746 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, RegionInfo_t3474869746 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// PlayFab.ClientModels.CatalogItem[]
struct CatalogItemU5BU5D_t1104433877  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) CatalogItem_t4132589244 * m_Items[1];

public:
	inline CatalogItem_t4132589244 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CatalogItem_t4132589244 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CatalogItem_t4132589244 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// PlayFab.ClientModels.VirtualCurrencyRechargeTime[]
struct VirtualCurrencyRechargeTimeU5BU5D_t3890213409  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) VirtualCurrencyRechargeTime_t1584308320 * m_Items[1];

public:
	inline VirtualCurrencyRechargeTime_t1584308320 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline VirtualCurrencyRechargeTime_t1584308320 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, VirtualCurrencyRechargeTime_t1584308320 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// PlayFab.ClientModels.CharacterLeaderboardEntry[]
struct CharacterLeaderboardEntryU5BU5D_t287716187  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) CharacterLeaderboardEntry_t3117535278 * m_Items[1];

public:
	inline CharacterLeaderboardEntry_t3117535278 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CharacterLeaderboardEntry_t3117535278 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CharacterLeaderboardEntry_t3117535278 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// PlayFab.ClientModels.FriendInfo[]
struct FriendInfoU5BU5D_t135151989  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) FriendInfo_t3708701852 * m_Items[1];

public:
	inline FriendInfo_t3708701852 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FriendInfo_t3708701852 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FriendInfo_t3708701852 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// PlayFab.ClientModels.StatisticValue[]
struct StatisticValueU5BU5D_t1107055084  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) StatisticValue_t3193655857 * m_Items[1];

public:
	inline StatisticValue_t3193655857 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline StatisticValue_t3193655857 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, StatisticValue_t3193655857 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// PlayFab.ClientModels.TradeInfo[]
struct TradeInfoU5BU5D_t1777265943  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) TradeInfo_t1933812770 * m_Items[1];

public:
	inline TradeInfo_t1933812770 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TradeInfo_t1933812770 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TradeInfo_t1933812770 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// PlayFab.ClientModels.FacebookPlayFabIdPair[]
struct FacebookPlayFabIdPairU5BU5D_t1009182599  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) FacebookPlayFabIdPair_t2068825714 * m_Items[1];

public:
	inline FacebookPlayFabIdPair_t2068825714 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FacebookPlayFabIdPair_t2068825714 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FacebookPlayFabIdPair_t2068825714 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// PlayFab.ClientModels.GameCenterPlayFabIdPair[]
struct GameCenterPlayFabIdPairU5BU5D_t685397292  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) GameCenterPlayFabIdPair_t3063128049 * m_Items[1];

public:
	inline GameCenterPlayFabIdPair_t3063128049 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GameCenterPlayFabIdPair_t3063128049 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GameCenterPlayFabIdPair_t3063128049 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// PlayFab.ClientModels.GooglePlayFabIdPair[]
struct GooglePlayFabIdPairU5BU5D_t3803880390  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) GooglePlayFabIdPair_t663154143 * m_Items[1];

public:
	inline GooglePlayFabIdPair_t663154143 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GooglePlayFabIdPair_t663154143 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GooglePlayFabIdPair_t663154143 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// PlayFab.ClientModels.KongregatePlayFabIdPair[]
struct KongregatePlayFabIdPairU5BU5D_t2622824432  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) KongregatePlayFabIdPair_t2985181565 * m_Items[1];

public:
	inline KongregatePlayFabIdPair_t2985181565 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline KongregatePlayFabIdPair_t2985181565 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, KongregatePlayFabIdPair_t2985181565 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// PlayFab.ClientModels.PSNAccountPlayFabIdPair[]
struct PSNAccountPlayFabIdPairU5BU5D_t3289080339  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) PSNAccountPlayFabIdPair_t680553942 * m_Items[1];

public:
	inline PSNAccountPlayFabIdPair_t680553942 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline PSNAccountPlayFabIdPair_t680553942 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, PSNAccountPlayFabIdPair_t680553942 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// PlayFab.ClientModels.SteamPlayFabIdPair[]
struct SteamPlayFabIdPairU5BU5D_t3215691001  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) SteamPlayFabIdPair_t2684316776 * m_Items[1];

public:
	inline SteamPlayFabIdPair_t2684316776 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline SteamPlayFabIdPair_t2684316776 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, SteamPlayFabIdPair_t2684316776 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// PlayFab.ClientModels.SharedGroupDataRecord[]
struct SharedGroupDataRecordU5BU5D_t275558408  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) SharedGroupDataRecord_t3101610309 * m_Items[1];

public:
	inline SharedGroupDataRecord_t3101610309 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline SharedGroupDataRecord_t3101610309 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, SharedGroupDataRecord_t3101610309 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// PlayFab.ClientModels.StoreItem[]
struct StoreItemU5BU5D_t2055598573  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) StoreItem_t2872884100 * m_Items[1];

public:
	inline StoreItem_t2872884100 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline StoreItem_t2872884100 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, StoreItem_t2872884100 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// PlayFab.ClientModels.TitleNewsItem[]
struct TitleNewsItemU5BU5D_t363814299  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) TitleNewsItem_t2523316974 * m_Items[1];

public:
	inline TitleNewsItem_t2523316974 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TitleNewsItem_t2523316974 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TitleNewsItem_t2523316974 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// PlayFab.ClientModels.ItemPurchaseRequest[]
struct ItemPurchaseRequestU5BU5D_t1110664778  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) ItemPurchaseRequest_t731462315 * m_Items[1];

public:
	inline ItemPurchaseRequest_t731462315 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ItemPurchaseRequest_t731462315 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ItemPurchaseRequest_t731462315 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// PlayFab.ClientModels.CartItem[]
struct CartItemU5BU5D_t2928256018  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) CartItem_t3543091843 * m_Items[1];

public:
	inline CartItem_t3543091843 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CartItem_t3543091843 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CartItem_t3543091843 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// PlayFab.ClientModels.PaymentOption[]
struct PaymentOptionU5BU5D_t2239914346  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) PaymentOption_t1289274891 * m_Items[1];

public:
	inline PaymentOption_t1289274891 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline PaymentOption_t1289274891 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, PaymentOption_t1289274891 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// PlayFab.ClientModels.StatisticUpdate[]
struct StatisticUpdateU5BU5D_t4046231316  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) StatisticUpdate_t3411289641 * m_Items[1];

public:
	inline StatisticUpdate_t3411289641 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline StatisticUpdate_t3411289641 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, StatisticUpdate_t3411289641 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// PlayFab.UUnit.UUnitTestCase[]
struct UUnitTestCaseU5BU5D_t1139374440  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) UUnitTestCase_t14187365 * m_Items[1];

public:
	inline UUnitTestCase_t14187365 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UUnitTestCase_t14187365 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UUnitTestCase_t14187365 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// AchievementDescription[]
struct AchievementDescriptionU5BU5D_t836066880  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) AchievementDescription_t2323334509 * m_Items[1];

public:
	inline AchievementDescription_t2323334509 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline AchievementDescription_t2323334509 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, AchievementDescription_t2323334509 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// FriendInfo[]
struct FriendInfoU5BU5D_t3226456005  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) FriendInfo_t236470412 * m_Items[1];

public:
	inline FriendInfo_t236470412 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FriendInfo_t236470412 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FriendInfo_t236470412 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// PlaneInfo[]
struct PlaneInfoU5BU5D_t1962304399  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) PlaneInfo_t4110763274 * m_Items[1];

public:
	inline PlaneInfo_t4110763274 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline PlaneInfo_t4110763274 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, PlaneInfo_t4110763274 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZergRush.ImmutableList`1<System.Action`1<System.Int64>>[]
struct ImmutableList_1U5BU5D_t930135236  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) ImmutableList_1_t2499251769 * m_Items[1];

public:
	inline ImmutableList_1_t2499251769 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ImmutableList_1_t2499251769 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ImmutableList_1_t2499251769 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZergRush.ImmutableList`1<System.Action`1<System.Double>>[]
struct ImmutableList_1U5BU5D_t1225238160  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) ImmutableList_1_t186353501 * m_Items[1];

public:
	inline ImmutableList_1_t186353501 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ImmutableList_1_t186353501 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ImmutableList_1_t186353501 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UpgradeDescriptor[]
struct UpgradeDescriptorU5BU5D_t3305946858  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) UpgradeDescriptor_t2646513291 * m_Items[1];

public:
	inline UpgradeDescriptor_t2646513291 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UpgradeDescriptor_t2646513291 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UpgradeDescriptor_t2646513291 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GlobalStat/ReceiptForVerify[]
struct ReceiptForVerifyU5BU5D_t2045262767  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) ReceiptForVerify_t3763828586 * m_Items[1];

public:
	inline ReceiptForVerify_t3763828586 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ReceiptForVerify_t3763828586 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ReceiptForVerify_t3763828586 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZergRush.ImmutableList`1<System.Action`1<System.Int32>>[]
struct ImmutableList_1U5BU5D_t1986940287  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) ImmutableList_1_t2499251674 * m_Items[1];

public:
	inline ImmutableList_1_t2499251674 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ImmutableList_1_t2499251674 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ImmutableList_1_t2499251674 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Cell`1<System.Int32>[]
struct Cell_1U5BU5D_t3743086642  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Cell_1_t3029512419 * m_Items[1];

public:
	inline Cell_1_t3029512419 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Cell_1_t3029512419 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Cell_1_t3029512419 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// DateTimeCell[]
struct DateTimeCellU5BU5D_t991409328  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) DateTimeCell_t773798845 * m_Items[1];

public:
	inline DateTimeCell_t773798845 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DateTimeCell_t773798845 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DateTimeCell_t773798845 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZergRush.ImmutableList`1<System.Action`1<System.DateTime>>[]
struct ImmutableList_1U5BU5D_t2588536926  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) ImmutableList_1_t4285838119 * m_Items[1];

public:
	inline ImmutableList_1_t4285838119 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ImmutableList_1_t4285838119 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ImmutableList_1_t4285838119 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZergRush.ImmutableList`1<System.Action`1<System.Single>>[]
struct ImmutableList_1U5BU5D_t1396388445  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) ImmutableList_1_t610045908 * m_Items[1];

public:
	inline ImmutableList_1_t610045908 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ImmutableList_1_t610045908 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ImmutableList_1_t610045908 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZergRush.ImmutableList`1<System.Action`1<System.Object>>[]
struct ImmutableList_1U5BU5D_t188480938  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) ImmutableList_1_t488943307 * m_Items[1];

public:
	inline ImmutableList_1_t488943307 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ImmutableList_1_t488943307 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ImmutableList_1_t488943307 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZergRush.ImmutableList`1<System.Action`1<System.String>>[]
struct ImmutableList_1U5BU5D_t3133827408  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) ImmutableList_1_t620325789 * m_Items[1];

public:
	inline ImmutableList_1_t620325789 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ImmutableList_1_t620325789 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ImmutableList_1_t620325789 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Cell`1<System.Single>[]
struct Cell_1U5BU5D_t3152534800  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Cell_1_t1140306653 * m_Items[1];

public:
	inline Cell_1_t1140306653 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Cell_1_t1140306653 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Cell_1_t1140306653 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Tacticsoft.TableViewCell[]
struct TableViewCellU5BU5D_t2926218058  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) TableViewCell_t776419755 * m_Items[1];

public:
	inline TableViewCell_t776419755 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TableViewCell_t776419755 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TableViewCell_t776419755 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ITableBlock[]
struct ITableBlockU5BU5D_t1776922201  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// TutorialTask[]
struct TutorialTaskU5BU5D_t1694798322  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) TutorialTask_t773278243 * m_Items[1];

public:
	inline TutorialTask_t773278243 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TutorialTask_t773278243 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TutorialTask_t773278243 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// BottomButton[]
struct BottomButtonU5BU5D_t4253761680  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) BottomButton_t1932555613 * m_Items[1];

public:
	inline BottomButton_t1932555613 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline BottomButton_t1932555613 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, BottomButton_t1932555613 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UIElemHideInfo[]
struct UIElemHideInfoU5BU5D_t954776162  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) UIElemHideInfo_t26498675 * m_Items[1];

public:
	inline UIElemHideInfo_t26498675 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UIElemHideInfo_t26498675 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UIElemHideInfo_t26498675 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZergRush.ImmutableList`1<System.Action`1<UniRx.Unit>>[]
struct ImmutableList_1U5BU5D_t1444909568  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) ImmutableList_1_t2210122925 * m_Items[1];

public:
	inline ImmutableList_1_t2210122925 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ImmutableList_1_t2210122925 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ImmutableList_1_t2210122925 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZergRush.ImmutableList`1<System.Action`1<UniRx.CollectionAddEvent`1<System.Object>>>[]
struct ImmutableList_1U5BU5D_t966832255  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) ImmutableList_1_t2068358874 * m_Items[1];

public:
	inline ImmutableList_1_t2068358874 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ImmutableList_1_t2068358874 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ImmutableList_1_t2068358874 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZergRush.ImmutableList`1<System.Action`1<CollectionMoveEvent`1<System.Object>>>[]
struct ImmutableList_1U5BU5D_t4159006885  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) ImmutableList_1_t3223892268 * m_Items[1];

public:
	inline ImmutableList_1_t3223892268 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ImmutableList_1_t3223892268 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ImmutableList_1_t3223892268 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CollectionMoveEvent`1<System.Object>[]
struct CollectionMoveEvent_1U5BU5D_t3982049720  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) CollectionMoveEvent_1_t3572055381 * m_Items[1];

public:
	inline CollectionMoveEvent_1_t3572055381 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CollectionMoveEvent_1_t3572055381 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CollectionMoveEvent_1_t3572055381 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZergRush.ImmutableList`1<System.Action`1<CollectionRemoveEvent`1<System.Object>>>[]
struct ImmutableList_1U5BU5D_t3741396876  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) ImmutableList_1_t550096145 * m_Items[1];

public:
	inline ImmutableList_1_t550096145 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ImmutableList_1_t550096145 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ImmutableList_1_t550096145 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CollectionRemoveEvent`1<System.Object>[]
struct CollectionRemoveEvent_1U5BU5D_t3564439711  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) CollectionRemoveEvent_1_t898259258 * m_Items[1];

public:
	inline CollectionRemoveEvent_1_t898259258 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CollectionRemoveEvent_1_t898259258 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CollectionRemoveEvent_1_t898259258 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZergRush.ImmutableList`1<System.Action`1<CollectionReplaceEvent`1<System.Object>>>[]
struct ImmutableList_1U5BU5D_t1936492016  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) ImmutableList_1_t2149208957 * m_Items[1];

public:
	inline ImmutableList_1_t2149208957 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ImmutableList_1_t2149208957 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ImmutableList_1_t2149208957 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CollectionReplaceEvent`1<System.Object>[]
struct CollectionReplaceEvent_1U5BU5D_t1759534851  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) CollectionReplaceEvent_1_t2497372070 * m_Items[1];

public:
	inline CollectionReplaceEvent_1_t2497372070 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CollectionReplaceEvent_1_t2497372070 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CollectionReplaceEvent_1_t2497372070 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZergRush.ImmutableList`1<System.Action`1<System.Collections.Generic.ICollection`1<System.Object>>>[]
struct ImmutableList_1U5BU5D_t1719989032  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) ImmutableList_1_t954774693 * m_Items[1];

public:
	inline ImmutableList_1_t954774693 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ImmutableList_1_t954774693 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ImmutableList_1_t954774693 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Cell`1<System.Object>[]
struct Cell_1U5BU5D_t1944627293  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Cell_1_t1019204052 * m_Items[1];

public:
	inline Cell_1_t1019204052 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Cell_1_t1019204052 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Cell_1_t1019204052 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ICell`1<System.Object>[]
struct ICell_1U5BU5D_t643144984  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ICell[]
struct ICellU5BU5D_t450044202  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ITransactionable[]
struct ITransactionableU5BU5D_t3679443638  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZergRush.ImmutableList`1<System.Action`1<System.Boolean>>[]
struct ImmutableList_1U5BU5D_t3981884477  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) ImmutableList_1_t4157809524 * m_Items[1];

public:
	inline ImmutableList_1_t4157809524 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ImmutableList_1_t4157809524 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ImmutableList_1_t4157809524 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// IOrganism[]
struct IOrganismU5BU5D_t2133806202  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// IEmptyStream[]
struct IEmptyStreamU5BU5D_t1849841613  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// IStream`1<System.Object>[]
struct IStream_1U5BU5D_t3844023282  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// BioProcessor`1/Instruction<System.Object>[]
struct InstructionU5BU5D_t987529102  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Instruction_t1241108407 * m_Items[1];

public:
	inline Instruction_t1241108407 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Instruction_t1241108407 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Instruction_t1241108407 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SubstanceBase`2/Intrusion<System.Object,System.Object>[]
struct IntrusionU5BU5D_t3345543525  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Intrusion_t3044853612 * m_Items[1];

public:
	inline Intrusion_t3044853612 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Intrusion_t3044853612 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Intrusion_t3044853612 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SubstanceBase`2/Intrusion<System.Double,System.Double>[]
struct IntrusionU5BU5D_t3689653005  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Intrusion_t3779021028 * m_Items[1];

public:
	inline Intrusion_t3779021028 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Intrusion_t3779021028 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Intrusion_t3779021028 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SubstanceBase`2/Intrusion<System.Single,System.Single>[]
struct IntrusionU5BU5D_t820085049  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Intrusion_t78896424 * m_Items[1];

public:
	inline Intrusion_t78896424 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Intrusion_t78896424 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Intrusion_t78896424 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZergRush.ImmutableList`1<System.Action`1<UniRx.InternalUtil.ImmutableList`1<System.Object>>>[]
struct ImmutableList_1U5BU5D_t2874682667  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) ImmutableList_1_t1549147806 * m_Items[1];

public:
	inline ImmutableList_1_t1549147806 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ImmutableList_1_t1549147806 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ImmutableList_1_t1549147806 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>[]
struct IntrusionU5BU5D_t361707200  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Intrusion_t1436088045 * m_Items[1];

public:
	inline Intrusion_t1436088045 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Intrusion_t1436088045 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Intrusion_t1436088045 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SubstanceBase`2/Intrusion<System.Object,System.Func`2<System.Object,System.Object>>[]
struct IntrusionU5BU5D_t3178296593  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Intrusion_t48563248 * m_Items[1];

public:
	inline Intrusion_t48563248 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Intrusion_t48563248 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Intrusion_t48563248 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SubstanceBase`2/Intrusion<System.Boolean,System.Boolean>[]
struct IntrusionU5BU5D_t848018873  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Intrusion_t1917188776 * m_Items[1];

public:
	inline Intrusion_t1917188776 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Intrusion_t1917188776 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Intrusion_t1917188776 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SubstanceBase`2/Intrusion<System.Single,System.Object>[]
struct IntrusionU5BU5D_t3907144838  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Intrusion_t4252761119 * m_Items[1];

public:
	inline Intrusion_t4252761119 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Intrusion_t4252761119 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Intrusion_t4252761119 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SubstanceBase`2/Intrusion<System.Single,System.Func`1<System.Single>>[]
struct IntrusionU5BU5D_t2110565406  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Intrusion_t1221677671 * m_Items[1];

public:
	inline Intrusion_t1221677671 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Intrusion_t1221677671 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Intrusion_t1221677671 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// IProcess[]
struct IProcessU5BU5D_t1995942851  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZergRush.ImmutableList`1<System.Action`1<ICell`1<System.Int32>>>[]
struct ImmutableList_1U5BU5D_t2618561498  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) ImmutableList_1_t4050882651 * m_Items[1];

public:
	inline ImmutableList_1_t4050882651 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ImmutableList_1_t4050882651 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ImmutableList_1_t4050882651 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ICell`1<System.Int32>[]
struct ICell_1U5BU5D_t2441604333  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject* m_Items[1];

public:
	inline Il2CppObject* GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CoroMutex/LockProc[]
struct LockProcU5BU5D_t1283750556  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) LockProc_t1973859137 * m_Items[1];

public:
	inline LockProc_t1973859137 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline LockProc_t1973859137 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, LockProc_t1973859137 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Counter[]
struct CounterU5BU5D_t2780672821  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Counter_t2622483932 * m_Items[1];

public:
	inline Counter_t2622483932 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Counter_t2622483932 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Counter_t2622483932 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SponsorPay.SPLogLevel[]
struct SPLogLevelU5BU5D_t762423022  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// LitJson.JsonData[]
struct JsonDataU5BU5D_t114649166  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) JsonData_t2847671799 * m_Items[1];

public:
	inline JsonData_t2847671799 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline JsonData_t2847671799 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, JsonData_t2847671799 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// LitJson.PropertyMetadata[]
struct PropertyMetadataU5BU5D_t4210083540  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) PropertyMetadata_t3942474089  m_Items[1];

public:
	inline PropertyMetadata_t3942474089  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline PropertyMetadata_t3942474089 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, PropertyMetadata_t3942474089  value)
	{
		m_Items[index] = value;
	}
};
// LitJson.ArrayMetadata[]
struct ArrayMetadataU5BU5D_t2098650368  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) ArrayMetadata_t4077657517  m_Items[1];

public:
	inline ArrayMetadata_t4077657517  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ArrayMetadata_t4077657517 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ArrayMetadata_t4077657517  value)
	{
		m_Items[index] = value;
	}
};
// LitJson.ObjectMetadata[]
struct ObjectMetadataU5BU5D_t877233794  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) ObjectMetadata_t2608047315  m_Items[1];

public:
	inline ObjectMetadata_t2608047315  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ObjectMetadata_t2608047315 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ObjectMetadata_t2608047315  value)
	{
		m_Items[index] = value;
	}
};
// LitJson.ExporterFunc[]
struct ExporterFuncU5BU5D_t2159883663  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) ExporterFunc_t282942218 * m_Items[1];

public:
	inline ExporterFunc_t282942218 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ExporterFunc_t282942218 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ExporterFunc_t282942218 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// LitJson.ImporterFunc[]
struct ImporterFuncU5BU5D_t2427453050  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) ImporterFunc_t3385868859 * m_Items[1];

public:
	inline ImporterFunc_t3385868859 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ImporterFunc_t3385868859 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ImporterFunc_t3385868859 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// LitJson.WriterContext[]
struct WriterContextU5BU5D_t4167362844  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) WriterContext_t3079472833 * m_Items[1];

public:
	inline WriterContext_t3079472833 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline WriterContext_t3079472833 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, WriterContext_t3079472833 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// LitJson.Lexer/StateHandler[]
struct StateHandlerU5BU5D_t3158648804  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) StateHandler_t1030384025 * m_Items[1];

public:
	inline StateHandler_t1030384025 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline StateHandler_t1030384025 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, StateHandler_t1030384025 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// FontDef[]
struct FontDefU5BU5D_t616749523  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) FontDef_t983695638 * m_Items[1];

public:
	inline FontDef_t983695638 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FontDef_t983695638 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FontDef_t983695638 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// GoMark[]
struct GoMarkU5BU5D_t2304196760  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) GoMark_t2137571317 * m_Items[1];

public:
	inline GoMark_t2137571317 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline GoMark_t2137571317 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, GoMark_t2137571317 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZergRush.ImmutableList`1<System.Action`1<UniRx.CollectionAddEvent`1<UpgradeButton>>>[]
struct ImmutableList_1U5BU5D_t1138901117  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) ImmutableList_1_t2706719796 * m_Items[1];

public:
	inline ImmutableList_1_t2706719796 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ImmutableList_1_t2706719796 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ImmutableList_1_t2706719796 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZergRush.ImmutableList`1<System.Action`1<CollectionMoveEvent`1<UpgradeButton>>>[]
struct ImmutableList_1U5BU5D_t36108451  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) ImmutableList_1_t3862253190 * m_Items[1];

public:
	inline ImmutableList_1_t3862253190 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ImmutableList_1_t3862253190 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ImmutableList_1_t3862253190 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CollectionMoveEvent`1<UpgradeButton>[]
struct CollectionMoveEvent_1U5BU5D_t4154118582  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) CollectionMoveEvent_1_t4210416303 * m_Items[1];

public:
	inline CollectionMoveEvent_1_t4210416303 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CollectionMoveEvent_1_t4210416303 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CollectionMoveEvent_1_t4210416303 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZergRush.ImmutableList`1<System.Action`1<CollectionRemoveEvent`1<UpgradeButton>>>[]
struct ImmutableList_1U5BU5D_t3913465738  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) ImmutableList_1_t1188457067 * m_Items[1];

public:
	inline ImmutableList_1_t1188457067 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ImmutableList_1_t1188457067 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ImmutableList_1_t1188457067 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CollectionRemoveEvent`1<UpgradeButton>[]
struct CollectionRemoveEvent_1U5BU5D_t3736508573  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) CollectionRemoveEvent_1_t1536620180 * m_Items[1];

public:
	inline CollectionRemoveEvent_1_t1536620180 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CollectionRemoveEvent_1_t1536620180 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CollectionRemoveEvent_1_t1536620180 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZergRush.ImmutableList`1<System.Action`1<CollectionReplaceEvent`1<UpgradeButton>>>[]
struct ImmutableList_1U5BU5D_t2108560878  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) ImmutableList_1_t2787569879 * m_Items[1];

public:
	inline ImmutableList_1_t2787569879 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ImmutableList_1_t2787569879 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ImmutableList_1_t2787569879 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CollectionReplaceEvent`1<UpgradeButton>[]
struct CollectionReplaceEvent_1U5BU5D_t1931603713  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) CollectionReplaceEvent_1_t3135732992 * m_Items[1];

public:
	inline CollectionReplaceEvent_1_t3135732992 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CollectionReplaceEvent_1_t3135732992 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CollectionReplaceEvent_1_t3135732992 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZergRush.ImmutableList`1<System.Action`1<System.Collections.Generic.ICollection`1<UpgradeButton>>>[]
struct ImmutableList_1U5BU5D_t1892057894  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) ImmutableList_1_t1593135615 * m_Items[1];

public:
	inline ImmutableList_1_t1593135615 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ImmutableList_1_t1593135615 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ImmutableList_1_t1593135615 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UpgradeButton[]
struct UpgradeButtonU5BU5D_t183592635  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) UpgradeButton_t1475467342 * m_Items[1];

public:
	inline UpgradeButton_t1475467342 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UpgradeButton_t1475467342 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UpgradeButton_t1475467342 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZergRush.ImmutableList`1<System.Action`1<UniRx.CollectionAddEvent`1<DonateButton>>>[]
struct ImmutableList_1U5BU5D_t489986942  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) ImmutableList_1_t1902005895 * m_Items[1];

public:
	inline ImmutableList_1_t1902005895 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ImmutableList_1_t1902005895 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ImmutableList_1_t1902005895 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZergRush.ImmutableList`1<System.Action`1<CollectionMoveEvent`1<DonateButton>>>[]
struct ImmutableList_1U5BU5D_t3682161572  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) ImmutableList_1_t3057539289 * m_Items[1];

public:
	inline ImmutableList_1_t3057539289 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ImmutableList_1_t3057539289 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ImmutableList_1_t3057539289 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CollectionMoveEvent`1<DonateButton>[]
struct CollectionMoveEvent_1U5BU5D_t3505204407  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) CollectionMoveEvent_1_t3405702402 * m_Items[1];

public:
	inline CollectionMoveEvent_1_t3405702402 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CollectionMoveEvent_1_t3405702402 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CollectionMoveEvent_1_t3405702402 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZergRush.ImmutableList`1<System.Action`1<CollectionRemoveEvent`1<DonateButton>>>[]
struct ImmutableList_1U5BU5D_t3264551563  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) ImmutableList_1_t383743166 * m_Items[1];

public:
	inline ImmutableList_1_t383743166 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ImmutableList_1_t383743166 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ImmutableList_1_t383743166 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CollectionRemoveEvent`1<DonateButton>[]
struct CollectionRemoveEvent_1U5BU5D_t3087594398  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) CollectionRemoveEvent_1_t731906279 * m_Items[1];

public:
	inline CollectionRemoveEvent_1_t731906279 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CollectionRemoveEvent_1_t731906279 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CollectionRemoveEvent_1_t731906279 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZergRush.ImmutableList`1<System.Action`1<CollectionReplaceEvent`1<DonateButton>>>[]
struct ImmutableList_1U5BU5D_t1459646703  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) ImmutableList_1_t1982855978 * m_Items[1];

public:
	inline ImmutableList_1_t1982855978 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ImmutableList_1_t1982855978 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ImmutableList_1_t1982855978 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CollectionReplaceEvent`1<DonateButton>[]
struct CollectionReplaceEvent_1U5BU5D_t1282689538  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) CollectionReplaceEvent_1_t2331019091 * m_Items[1];

public:
	inline CollectionReplaceEvent_1_t2331019091 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CollectionReplaceEvent_1_t2331019091 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CollectionReplaceEvent_1_t2331019091 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZergRush.ImmutableList`1<System.Action`1<System.Collections.Generic.ICollection`1<DonateButton>>>[]
struct ImmutableList_1U5BU5D_t1243143719  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) ImmutableList_1_t788421714 * m_Items[1];

public:
	inline ImmutableList_1_t788421714 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ImmutableList_1_t788421714 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ImmutableList_1_t788421714 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// DonateButton[]
struct DonateButtonU5BU5D_t3829645756  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) DonateButton_t670753441 * m_Items[1];

public:
	inline DonateButton_t670753441 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DonateButton_t670753441 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DonateButton_t670753441 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZergRush.ImmutableList`1<System.Action`1<UniRx.CollectionAddEvent`1<SettingsButton>>>[]
struct ImmutableList_1U5BU5D_t4058825594  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) ImmutableList_1_t2146509115 * m_Items[1];

public:
	inline ImmutableList_1_t2146509115 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ImmutableList_1_t2146509115 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ImmutableList_1_t2146509115 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZergRush.ImmutableList`1<System.Action`1<CollectionMoveEvent`1<SettingsButton>>>[]
struct ImmutableList_1U5BU5D_t2956032928  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) ImmutableList_1_t3302042509 * m_Items[1];

public:
	inline ImmutableList_1_t3302042509 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ImmutableList_1_t3302042509 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ImmutableList_1_t3302042509 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CollectionMoveEvent`1<SettingsButton>[]
struct CollectionMoveEvent_1U5BU5D_t2779075763  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) CollectionMoveEvent_1_t3650205622 * m_Items[1];

public:
	inline CollectionMoveEvent_1_t3650205622 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CollectionMoveEvent_1_t3650205622 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CollectionMoveEvent_1_t3650205622 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZergRush.ImmutableList`1<System.Action`1<CollectionRemoveEvent`1<SettingsButton>>>[]
struct ImmutableList_1U5BU5D_t2538422919  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) ImmutableList_1_t628246386 * m_Items[1];

public:
	inline ImmutableList_1_t628246386 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ImmutableList_1_t628246386 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ImmutableList_1_t628246386 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CollectionRemoveEvent`1<SettingsButton>[]
struct CollectionRemoveEvent_1U5BU5D_t2361465754  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) CollectionRemoveEvent_1_t976409499 * m_Items[1];

public:
	inline CollectionRemoveEvent_1_t976409499 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CollectionRemoveEvent_1_t976409499 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CollectionRemoveEvent_1_t976409499 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZergRush.ImmutableList`1<System.Action`1<CollectionReplaceEvent`1<SettingsButton>>>[]
struct ImmutableList_1U5BU5D_t733518059  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) ImmutableList_1_t2227359198 * m_Items[1];

public:
	inline ImmutableList_1_t2227359198 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ImmutableList_1_t2227359198 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ImmutableList_1_t2227359198 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CollectionReplaceEvent`1<SettingsButton>[]
struct CollectionReplaceEvent_1U5BU5D_t556560894  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) CollectionReplaceEvent_1_t2575522311 * m_Items[1];

public:
	inline CollectionReplaceEvent_1_t2575522311 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CollectionReplaceEvent_1_t2575522311 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CollectionReplaceEvent_1_t2575522311 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ZergRush.ImmutableList`1<System.Action`1<System.Collections.Generic.ICollection`1<SettingsButton>>>[]
struct ImmutableList_1U5BU5D_t517015075  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) ImmutableList_1_t1032924934 * m_Items[1];

public:
	inline ImmutableList_1_t1032924934 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ImmutableList_1_t1032924934 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ImmutableList_1_t1032924934 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SettingsButton[]
struct SettingsButtonU5BU5D_t3103517112  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) SettingsButton_t915256661 * m_Items[1];

public:
	inline SettingsButton_t915256661 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline SettingsButton_t915256661 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, SettingsButton_t915256661 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Zenject.ProviderBase[]
struct ProviderBaseU5BU5D_t1774014030  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) ProviderBase_t1627494391 * m_Items[1];

public:
	inline ProviderBase_t1627494391 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ProviderBase_t1627494391 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ProviderBase_t1627494391 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Zenject.SingletonId[]
struct SingletonIdU5BU5D_t822569050  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) SingletonId_t1838183899 * m_Items[1];

public:
	inline SingletonId_t1838183899 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline SingletonId_t1838183899 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, SingletonId_t1838183899 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Zenject.SingletonLazyCreator[]
struct SingletonLazyCreatorU5BU5D_t59900567  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) SingletonLazyCreator_t2762284194 * m_Items[1];

public:
	inline SingletonLazyCreator_t2762284194 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline SingletonLazyCreator_t2762284194 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, SingletonLazyCreator_t2762284194 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Zenject.TypeValuePair[]
struct TypeValuePairU5BU5D_t2583178115  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) TypeValuePair_t620932390 * m_Items[1];

public:
	inline TypeValuePair_t620932390 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TypeValuePair_t620932390 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TypeValuePair_t620932390 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ModestTree.Util.Tuple`2<System.Object,System.Type>[]
struct Tuple_2U5BU5D_t2532318683  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Tuple_2_t231167918 * m_Items[1];

public:
	inline Tuple_2_t231167918 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Tuple_2_t231167918 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Tuple_2_t231167918 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Zenject.ZenjectTypeInfo[]
struct ZenjectTypeInfoU5BU5D_t39236805  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) ZenjectTypeInfo_t283213708 * m_Items[1];

public:
	inline ZenjectTypeInfo_t283213708 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ZenjectTypeInfo_t283213708 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ZenjectTypeInfo_t283213708 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Zenject.PostInjectableInfo[]
struct PostInjectableInfoU5BU5D_t1417357819  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) PostInjectableInfo_t3080283662 * m_Items[1];

public:
	inline PostInjectableInfo_t3080283662 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline PostInjectableInfo_t3080283662 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, PostInjectableInfo_t3080283662 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Zenject.InjectableInfo[]
struct InjectableInfoU5BU5D_t2664968635  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) InjectableInfo_t1147709774 * m_Items[1];

public:
	inline InjectableInfo_t1147709774 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline InjectableInfo_t1147709774 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, InjectableInfo_t1147709774 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Zenject.InjectAttribute[]
struct InjectAttributeU5BU5D_t4127409953  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) InjectAttribute_t2791321440 * m_Items[1];

public:
	inline InjectAttribute_t2791321440 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline InjectAttribute_t2791321440 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, InjectAttribute_t2791321440 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Zenject.InjectOptionalAttribute[]
struct InjectOptionalAttributeU5BU5D_t2965909409  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) InjectOptionalAttribute_t899944672 * m_Items[1];

public:
	inline InjectOptionalAttribute_t899944672 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline InjectOptionalAttribute_t899944672 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, InjectOptionalAttribute_t899944672 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Zenject.IInstaller[]
struct IInstallerU5BU5D_t2923451901  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Zenject.BindingId[]
struct BindingIdU5BU5D_t1497546552  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) BindingId_t2965794261 * m_Items[1];

public:
	inline BindingId_t2965794261 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline BindingId_t2965794261 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, BindingId_t2965794261 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Zenject.MonoInstaller[]
struct MonoInstallerU5BU5D_t2181499551  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) MonoInstaller_t3807575866 * m_Items[1];

public:
	inline MonoInstaller_t3807575866 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline MonoInstaller_t3807575866 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, MonoInstaller_t3807575866 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Zenject.DisposableManager/DisposableInfo[]
struct DisposableInfoU5BU5D_t1006847163  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) DisposableInfo_t1284166222 * m_Items[1];

public:
	inline DisposableInfo_t1284166222 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DisposableInfo_t1284166222 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DisposableInfo_t1284166222 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ModestTree.Util.Tuple`2<System.Type,System.Int32>[]
struct Tuple_2U5BU5D_t2128555834  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Tuple_2_t3719549051 * m_Items[1];

public:
	inline Tuple_2_t3719549051 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Tuple_2_t3719549051 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Tuple_2_t3719549051 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Zenject.InitializableManager/InitializableInfo[]
struct InitializableInfoU5BU5D_t104241648  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) InitializableInfo_t3830632317 * m_Items[1];

public:
	inline InitializableInfo_t3830632317 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline InitializableInfo_t3830632317 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, InitializableInfo_t3830632317 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Zenject.IInitializable[]
struct IInitializableU5BU5D_t2521065914  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Zenject.TaskUpdater`1/TaskInfo<System.Object>[]
struct TaskInfoU5BU5D_t460411325  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) TaskInfo_t1064508404 * m_Items[1];

public:
	inline TaskInfo_t1064508404 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TaskInfo_t1064508404 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TaskInfo_t1064508404 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Zenject.ITickable[]
struct ITickableU5BU5D_t1684682680  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Zenject.IFixedTickable[]
struct IFixedTickableU5BU5D_t3793628558  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Zenject.ILateTickable[]
struct ILateTickableU5BU5D_t2255674266  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Zenject.TaskUpdater`1/TaskInfo<Zenject.ITickable>[]
struct TaskInfoU5BU5D_t2133570232  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) TaskInfo_t1016914005 * m_Items[1];

public:
	inline TaskInfo_t1016914005 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TaskInfo_t1016914005 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TaskInfo_t1016914005 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Zenject.TaskUpdater`1/TaskInfo<Zenject.IFixedTickable>[]
struct TaskInfoU5BU5D_t4242516110  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) TaskInfo_t3555793591 * m_Items[1];

public:
	inline TaskInfo_t3555793591 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TaskInfo_t3555793591 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TaskInfo_t3555793591 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Zenject.TaskUpdater`1/TaskInfo<Zenject.ILateTickable>[]
struct TaskInfoU5BU5D_t2704561818  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) TaskInfo_t3693212827 * m_Items[1];

public:
	inline TaskInfo_t3693212827 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TaskInfo_t3693212827 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TaskInfo_t3693212827 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Zenject.PrefabSingletonId[]
struct PrefabSingletonIdU5BU5D_t1488780686  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) PrefabSingletonId_t3643845047 * m_Items[1];

public:
	inline PrefabSingletonId_t3643845047 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline PrefabSingletonId_t3643845047 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, PrefabSingletonId_t3643845047 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Zenject.PrefabSingletonLazyCreator[]
struct PrefabSingletonLazyCreatorU5BU5D_t356452323  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) PrefabSingletonLazyCreator_t3719004230 * m_Items[1];

public:
	inline PrefabSingletonLazyCreator_t3719004230 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline PrefabSingletonLazyCreator_t3719004230 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, PrefabSingletonLazyCreator_t3719004230 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Zenject.DecoratorInstaller[]
struct DecoratorInstallerU5BU5D_t3255381015  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) DecoratorInstaller_t3984396578 * m_Items[1];

public:
	inline DecoratorInstaller_t3984396578 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DecoratorInstaller_t3984396578 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DecoratorInstaller_t3984396578 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
