﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnonymousCell`1<System.Int32>
struct AnonymousCell_1_t2113901288;
// System.Func`3<System.Action`1<System.Int32>,Priority,System.IDisposable>
struct Func_3_t722536535;
// System.Func`1<System.Int32>
struct Func_1_t3990196034;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<System.Int32>
struct Action_1_t2995867492;
// UniRx.IObserver`1<System.Int32>
struct IObserver_1_t764446394;
// System.Action
struct Action_t437523947;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"
#include "System_Core_System_Action437523947.h"

// System.Void AnonymousCell`1<System.Int32>::.ctor()
extern "C"  void AnonymousCell_1__ctor_m3093625368_gshared (AnonymousCell_1_t2113901288 * __this, const MethodInfo* method);
#define AnonymousCell_1__ctor_m3093625368(__this, method) ((  void (*) (AnonymousCell_1_t2113901288 *, const MethodInfo*))AnonymousCell_1__ctor_m3093625368_gshared)(__this, method)
// System.Void AnonymousCell`1<System.Int32>::.ctor(System.Func`3<System.Action`1<T>,Priority,System.IDisposable>,System.Func`1<T>)
extern "C"  void AnonymousCell_1__ctor_m69632588_gshared (AnonymousCell_1_t2113901288 * __this, Func_3_t722536535 * ___subscribe0, Func_1_t3990196034 * ___current1, const MethodInfo* method);
#define AnonymousCell_1__ctor_m69632588(__this, ___subscribe0, ___current1, method) ((  void (*) (AnonymousCell_1_t2113901288 *, Func_3_t722536535 *, Func_1_t3990196034 *, const MethodInfo*))AnonymousCell_1__ctor_m69632588_gshared)(__this, ___subscribe0, ___current1, method)
// T AnonymousCell`1<System.Int32>::get_value()
extern "C"  int32_t AnonymousCell_1_get_value_m1324051165_gshared (AnonymousCell_1_t2113901288 * __this, const MethodInfo* method);
#define AnonymousCell_1_get_value_m1324051165(__this, method) ((  int32_t (*) (AnonymousCell_1_t2113901288 *, const MethodInfo*))AnonymousCell_1_get_value_m1324051165_gshared)(__this, method)
// System.IDisposable AnonymousCell`1<System.Int32>::ListenUpdates(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * AnonymousCell_1_ListenUpdates_m2071196534_gshared (AnonymousCell_1_t2113901288 * __this, Action_1_t2995867492 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define AnonymousCell_1_ListenUpdates_m2071196534(__this, ___reaction0, ___p1, method) ((  Il2CppObject * (*) (AnonymousCell_1_t2113901288 *, Action_1_t2995867492 *, int32_t, const MethodInfo*))AnonymousCell_1_ListenUpdates_m2071196534_gshared)(__this, ___reaction0, ___p1, method)
// System.IDisposable AnonymousCell`1<System.Int32>::Bind(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * AnonymousCell_1_Bind_m2557459980_gshared (AnonymousCell_1_t2113901288 * __this, Action_1_t2995867492 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define AnonymousCell_1_Bind_m2557459980(__this, ___reaction0, ___p1, method) ((  Il2CppObject * (*) (AnonymousCell_1_t2113901288 *, Action_1_t2995867492 *, int32_t, const MethodInfo*))AnonymousCell_1_Bind_m2557459980_gshared)(__this, ___reaction0, ___p1, method)
// System.IDisposable AnonymousCell`1<System.Int32>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * AnonymousCell_1_Subscribe_m2927049157_gshared (AnonymousCell_1_t2113901288 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define AnonymousCell_1_Subscribe_m2927049157(__this, ___observer0, method) ((  Il2CppObject * (*) (AnonymousCell_1_t2113901288 *, Il2CppObject*, const MethodInfo*))AnonymousCell_1_Subscribe_m2927049157_gshared)(__this, ___observer0, method)
// System.IDisposable AnonymousCell`1<System.Int32>::Listen(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * AnonymousCell_1_Listen_m1885669058_gshared (AnonymousCell_1_t2113901288 * __this, Action_1_t2995867492 * ___reaction0, int32_t ___priority1, const MethodInfo* method);
#define AnonymousCell_1_Listen_m1885669058(__this, ___reaction0, ___priority1, method) ((  Il2CppObject * (*) (AnonymousCell_1_t2113901288 *, Action_1_t2995867492 *, int32_t, const MethodInfo*))AnonymousCell_1_Listen_m1885669058_gshared)(__this, ___reaction0, ___priority1, method)
// System.IDisposable AnonymousCell`1<System.Int32>::Listen(System.Action,Priority)
extern "C"  Il2CppObject * AnonymousCell_1_Listen_m3131001125_gshared (AnonymousCell_1_t2113901288 * __this, Action_t437523947 * ___action0, int32_t ___priority1, const MethodInfo* method);
#define AnonymousCell_1_Listen_m3131001125(__this, ___action0, ___priority1, method) ((  Il2CppObject * (*) (AnonymousCell_1_t2113901288 *, Action_t437523947 *, int32_t, const MethodInfo*))AnonymousCell_1_Listen_m3131001125_gshared)(__this, ___action0, ___priority1, method)
// System.IDisposable AnonymousCell`1<System.Int32>::OnChanged(System.Action,Priority)
extern "C"  Il2CppObject * AnonymousCell_1_OnChanged_m3126362307_gshared (AnonymousCell_1_t2113901288 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method);
#define AnonymousCell_1_OnChanged_m3126362307(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (AnonymousCell_1_t2113901288 *, Action_t437523947 *, int32_t, const MethodInfo*))AnonymousCell_1_OnChanged_m3126362307_gshared)(__this, ___action0, ___p1, method)
// System.Object AnonymousCell`1<System.Int32>::get_valueObject()
extern "C"  Il2CppObject * AnonymousCell_1_get_valueObject_m142079666_gshared (AnonymousCell_1_t2113901288 * __this, const MethodInfo* method);
#define AnonymousCell_1_get_valueObject_m142079666(__this, method) ((  Il2CppObject * (*) (AnonymousCell_1_t2113901288 *, const MethodInfo*))AnonymousCell_1_get_valueObject_m142079666_gshared)(__this, method)
