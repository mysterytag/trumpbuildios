﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ObserveOnObservable`1/ObserveOn<System.Object>
struct ObserveOn_t3253138108;
// UniRx.Operators.ObserveOnObservable`1<System.Object>
struct ObserveOnObservable_1_t1522207957;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn<System.Object>::.ctor(UniRx.Operators.ObserveOnObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void ObserveOn__ctor_m3391667725_gshared (ObserveOn_t3253138108 * __this, ObserveOnObservable_1_t1522207957 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define ObserveOn__ctor_m3391667725(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (ObserveOn_t3253138108 *, ObserveOnObservable_1_t1522207957 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))ObserveOn__ctor_m3391667725_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.ObserveOnObservable`1/ObserveOn<System.Object>::Run()
extern "C"  Il2CppObject * ObserveOn_Run_m1320885093_gshared (ObserveOn_t3253138108 * __this, const MethodInfo* method);
#define ObserveOn_Run_m1320885093(__this, method) ((  Il2CppObject * (*) (ObserveOn_t3253138108 *, const MethodInfo*))ObserveOn_Run_m1320885093_gshared)(__this, method)
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn<System.Object>::OnNext(T)
extern "C"  void ObserveOn_OnNext_m1031103679_gshared (ObserveOn_t3253138108 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ObserveOn_OnNext_m1031103679(__this, ___value0, method) ((  void (*) (ObserveOn_t3253138108 *, Il2CppObject *, const MethodInfo*))ObserveOn_OnNext_m1031103679_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn<System.Object>::OnError(System.Exception)
extern "C"  void ObserveOn_OnError_m674111950_gshared (ObserveOn_t3253138108 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ObserveOn_OnError_m674111950(__this, ___error0, method) ((  void (*) (ObserveOn_t3253138108 *, Exception_t1967233988 *, const MethodInfo*))ObserveOn_OnError_m674111950_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn<System.Object>::OnCompleted()
extern "C"  void ObserveOn_OnCompleted_m1538693665_gshared (ObserveOn_t3253138108 * __this, const MethodInfo* method);
#define ObserveOn_OnCompleted_m1538693665(__this, method) ((  void (*) (ObserveOn_t3253138108 *, const MethodInfo*))ObserveOn_OnCompleted_m1538693665_gshared)(__this, method)
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn<System.Object>::<Run>m__7D()
extern "C"  void ObserveOn_U3CRunU3Em__7D_m4123616088_gshared (ObserveOn_t3253138108 * __this, const MethodInfo* method);
#define ObserveOn_U3CRunU3Em__7D_m4123616088(__this, method) ((  void (*) (ObserveOn_t3253138108 *, const MethodInfo*))ObserveOn_U3CRunU3Em__7D_m4123616088_gshared)(__this, method)
