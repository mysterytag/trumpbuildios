﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// <>__AnonType6`2<System.Double,System.Int32>
struct U3CU3E__AnonType6_2_t3631555212;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void <>__AnonType6`2<System.Double,System.Int32>::.ctor(<money>__T,<level>__T)
extern "C"  void U3CU3E__AnonType6_2__ctor_m870492846_gshared (U3CU3E__AnonType6_2_t3631555212 * __this, double ___money0, int32_t ___level1, const MethodInfo* method);
#define U3CU3E__AnonType6_2__ctor_m870492846(__this, ___money0, ___level1, method) ((  void (*) (U3CU3E__AnonType6_2_t3631555212 *, double, int32_t, const MethodInfo*))U3CU3E__AnonType6_2__ctor_m870492846_gshared)(__this, ___money0, ___level1, method)
// <money>__T <>__AnonType6`2<System.Double,System.Int32>::get_money()
extern "C"  double U3CU3E__AnonType6_2_get_money_m3007538860_gshared (U3CU3E__AnonType6_2_t3631555212 * __this, const MethodInfo* method);
#define U3CU3E__AnonType6_2_get_money_m3007538860(__this, method) ((  double (*) (U3CU3E__AnonType6_2_t3631555212 *, const MethodInfo*))U3CU3E__AnonType6_2_get_money_m3007538860_gshared)(__this, method)
// <level>__T <>__AnonType6`2<System.Double,System.Int32>::get_level()
extern "C"  int32_t U3CU3E__AnonType6_2_get_level_m1710193708_gshared (U3CU3E__AnonType6_2_t3631555212 * __this, const MethodInfo* method);
#define U3CU3E__AnonType6_2_get_level_m1710193708(__this, method) ((  int32_t (*) (U3CU3E__AnonType6_2_t3631555212 *, const MethodInfo*))U3CU3E__AnonType6_2_get_level_m1710193708_gshared)(__this, method)
// System.Boolean <>__AnonType6`2<System.Double,System.Int32>::Equals(System.Object)
extern "C"  bool U3CU3E__AnonType6_2_Equals_m3329063801_gshared (U3CU3E__AnonType6_2_t3631555212 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define U3CU3E__AnonType6_2_Equals_m3329063801(__this, ___obj0, method) ((  bool (*) (U3CU3E__AnonType6_2_t3631555212 *, Il2CppObject *, const MethodInfo*))U3CU3E__AnonType6_2_Equals_m3329063801_gshared)(__this, ___obj0, method)
// System.Int32 <>__AnonType6`2<System.Double,System.Int32>::GetHashCode()
extern "C"  int32_t U3CU3E__AnonType6_2_GetHashCode_m1637063953_gshared (U3CU3E__AnonType6_2_t3631555212 * __this, const MethodInfo* method);
#define U3CU3E__AnonType6_2_GetHashCode_m1637063953(__this, method) ((  int32_t (*) (U3CU3E__AnonType6_2_t3631555212 *, const MethodInfo*))U3CU3E__AnonType6_2_GetHashCode_m1637063953_gshared)(__this, method)
// System.String <>__AnonType6`2<System.Double,System.Int32>::ToString()
extern "C"  String_t* U3CU3E__AnonType6_2_ToString_m2164810329_gshared (U3CU3E__AnonType6_2_t3631555212 * __this, const MethodInfo* method);
#define U3CU3E__AnonType6_2_ToString_m2164810329(__this, method) ((  String_t* (*) (U3CU3E__AnonType6_2_t3631555212 *, const MethodInfo*))U3CU3E__AnonType6_2_ToString_m2164810329_gshared)(__this, method)
