﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<GetAllUsersCharacters>c__AnonStoreyC6
struct U3CGetAllUsersCharactersU3Ec__AnonStoreyC6_t1847748583;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<GetAllUsersCharacters>c__AnonStoreyC6::.ctor()
extern "C"  void U3CGetAllUsersCharactersU3Ec__AnonStoreyC6__ctor_m1064292172 (U3CGetAllUsersCharactersU3Ec__AnonStoreyC6_t1847748583 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<GetAllUsersCharacters>c__AnonStoreyC6::<>m__B3(PlayFab.CallRequestContainer)
extern "C"  void U3CGetAllUsersCharactersU3Ec__AnonStoreyC6_U3CU3Em__B3_m2655153563 (U3CGetAllUsersCharactersU3Ec__AnonStoreyC6_t1847748583 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
