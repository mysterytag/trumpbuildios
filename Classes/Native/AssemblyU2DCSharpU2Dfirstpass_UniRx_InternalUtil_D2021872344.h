﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.InternalUtil.DisposedObserver`1<System.Int64>
struct DisposedObserver_1_t2021872344;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.InternalUtil.DisposedObserver`1<System.Int64>
struct  DisposedObserver_1_t2021872344  : public Il2CppObject
{
public:

public:
};

struct DisposedObserver_1_t2021872344_StaticFields
{
public:
	// UniRx.InternalUtil.DisposedObserver`1<T> UniRx.InternalUtil.DisposedObserver`1::Instance
	DisposedObserver_1_t2021872344 * ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(DisposedObserver_1_t2021872344_StaticFields, ___Instance_0)); }
	inline DisposedObserver_1_t2021872344 * get_Instance_0() const { return ___Instance_0; }
	inline DisposedObserver_1_t2021872344 ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(DisposedObserver_1_t2021872344 * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier(&___Instance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
