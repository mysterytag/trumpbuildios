﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IScheduler
struct IScheduler_t2938318244;
// System.Action
struct Action_t437523947;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/<ToAsync>c__AnonStorey3A
struct  U3CToAsyncU3Ec__AnonStorey3A_t2315298076  : public Il2CppObject
{
public:
	// UniRx.IScheduler UniRx.Observable/<ToAsync>c__AnonStorey3A::scheduler
	Il2CppObject * ___scheduler_0;
	// System.Action UniRx.Observable/<ToAsync>c__AnonStorey3A::action
	Action_t437523947 * ___action_1;

public:
	inline static int32_t get_offset_of_scheduler_0() { return static_cast<int32_t>(offsetof(U3CToAsyncU3Ec__AnonStorey3A_t2315298076, ___scheduler_0)); }
	inline Il2CppObject * get_scheduler_0() const { return ___scheduler_0; }
	inline Il2CppObject ** get_address_of_scheduler_0() { return &___scheduler_0; }
	inline void set_scheduler_0(Il2CppObject * value)
	{
		___scheduler_0 = value;
		Il2CppCodeGenWriteBarrier(&___scheduler_0, value);
	}

	inline static int32_t get_offset_of_action_1() { return static_cast<int32_t>(offsetof(U3CToAsyncU3Ec__AnonStorey3A_t2315298076, ___action_1)); }
	inline Action_t437523947 * get_action_1() const { return ___action_1; }
	inline Action_t437523947 ** get_address_of_action_1() { return &___action_1; }
	inline void set_action_1(Action_t437523947 * value)
	{
		___action_1 = value;
		Il2CppCodeGenWriteBarrier(&___action_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
