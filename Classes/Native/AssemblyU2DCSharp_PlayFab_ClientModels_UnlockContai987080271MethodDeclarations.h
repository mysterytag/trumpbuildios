﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.UnlockContainerItemRequest
struct UnlockContainerItemRequest_t987080271;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.UnlockContainerItemRequest::.ctor()
extern "C"  void UnlockContainerItemRequest__ctor_m1836607054 (UnlockContainerItemRequest_t987080271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.UnlockContainerItemRequest::get_ContainerItemId()
extern "C"  String_t* UnlockContainerItemRequest_get_ContainerItemId_m2253319957 (UnlockContainerItemRequest_t987080271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UnlockContainerItemRequest::set_ContainerItemId(System.String)
extern "C"  void UnlockContainerItemRequest_set_ContainerItemId_m3694648964 (UnlockContainerItemRequest_t987080271 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.UnlockContainerItemRequest::get_CatalogVersion()
extern "C"  String_t* UnlockContainerItemRequest_get_CatalogVersion_m2286537915 (UnlockContainerItemRequest_t987080271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UnlockContainerItemRequest::set_CatalogVersion(System.String)
extern "C"  void UnlockContainerItemRequest_set_CatalogVersion_m1808296656 (UnlockContainerItemRequest_t987080271 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.UnlockContainerItemRequest::get_CharacterId()
extern "C"  String_t* UnlockContainerItemRequest_get_CharacterId_m429559562 (UnlockContainerItemRequest_t987080271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UnlockContainerItemRequest::set_CharacterId(System.String)
extern "C"  void UnlockContainerItemRequest_set_CharacterId_m3281217583 (UnlockContainerItemRequest_t987080271 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
