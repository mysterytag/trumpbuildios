﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ObserveOnObservable`1/ObserveOn/<OnError>c__AnonStorey62<UniRx.Unit>
struct U3COnErrorU3Ec__AnonStorey62_t2402998685;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn/<OnError>c__AnonStorey62<UniRx.Unit>::.ctor()
extern "C"  void U3COnErrorU3Ec__AnonStorey62__ctor_m4253313324_gshared (U3COnErrorU3Ec__AnonStorey62_t2402998685 * __this, const MethodInfo* method);
#define U3COnErrorU3Ec__AnonStorey62__ctor_m4253313324(__this, method) ((  void (*) (U3COnErrorU3Ec__AnonStorey62_t2402998685 *, const MethodInfo*))U3COnErrorU3Ec__AnonStorey62__ctor_m4253313324_gshared)(__this, method)
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn/<OnError>c__AnonStorey62<UniRx.Unit>::<>m__7F()
extern "C"  void U3COnErrorU3Ec__AnonStorey62_U3CU3Em__7F_m933478436_gshared (U3COnErrorU3Ec__AnonStorey62_t2402998685 * __this, const MethodInfo* method);
#define U3COnErrorU3Ec__AnonStorey62_U3CU3Em__7F_m933478436(__this, method) ((  void (*) (U3COnErrorU3Ec__AnonStorey62_t2402998685 *, const MethodInfo*))U3COnErrorU3Ec__AnonStorey62_U3CU3Em__7F_m933478436_gshared)(__this, method)
