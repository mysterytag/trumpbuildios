﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.UnlinkIOSDeviceIDResult
struct UnlinkIOSDeviceIDResult_t3277114616;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.UnlinkIOSDeviceIDResult::.ctor()
extern "C"  void UnlinkIOSDeviceIDResult__ctor_m301108145 (UnlinkIOSDeviceIDResult_t3277114616 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
