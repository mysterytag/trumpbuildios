﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ReactiveProperty`1/ReactivePropertyObserver<TableButtons/StatkaPoTableViev>
struct ReactivePropertyObserver_t3744378386;
// UniRx.ReactiveProperty`1<TableButtons/StatkaPoTableViev>
struct ReactiveProperty_1_t4116728855;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TableButtons_StatkaPoTableViev3508395817.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<TableButtons/StatkaPoTableViev>::.ctor(UniRx.ReactiveProperty`1<T>)
extern "C"  void ReactivePropertyObserver__ctor_m949315997_gshared (ReactivePropertyObserver_t3744378386 * __this, ReactiveProperty_1_t4116728855 * ___parent0, const MethodInfo* method);
#define ReactivePropertyObserver__ctor_m949315997(__this, ___parent0, method) ((  void (*) (ReactivePropertyObserver_t3744378386 *, ReactiveProperty_1_t4116728855 *, const MethodInfo*))ReactivePropertyObserver__ctor_m949315997_gshared)(__this, ___parent0, method)
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<TableButtons/StatkaPoTableViev>::OnNext(T)
extern "C"  void ReactivePropertyObserver_OnNext_m4031271278_gshared (ReactivePropertyObserver_t3744378386 * __this, int32_t ___value0, const MethodInfo* method);
#define ReactivePropertyObserver_OnNext_m4031271278(__this, ___value0, method) ((  void (*) (ReactivePropertyObserver_t3744378386 *, int32_t, const MethodInfo*))ReactivePropertyObserver_OnNext_m4031271278_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<TableButtons/StatkaPoTableViev>::OnError(System.Exception)
extern "C"  void ReactivePropertyObserver_OnError_m3016216701_gshared (ReactivePropertyObserver_t3744378386 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ReactivePropertyObserver_OnError_m3016216701(__this, ___error0, method) ((  void (*) (ReactivePropertyObserver_t3744378386 *, Exception_t1967233988 *, const MethodInfo*))ReactivePropertyObserver_OnError_m3016216701_gshared)(__this, ___error0, method)
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<TableButtons/StatkaPoTableViev>::OnCompleted()
extern "C"  void ReactivePropertyObserver_OnCompleted_m1557501776_gshared (ReactivePropertyObserver_t3744378386 * __this, const MethodInfo* method);
#define ReactivePropertyObserver_OnCompleted_m1557501776(__this, method) ((  void (*) (ReactivePropertyObserver_t3744378386 *, const MethodInfo*))ReactivePropertyObserver_OnCompleted_m1557501776_gshared)(__this, method)
