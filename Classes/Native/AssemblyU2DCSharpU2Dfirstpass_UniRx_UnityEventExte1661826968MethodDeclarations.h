﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey98`2<System.Object,System.Object>
struct U3CAsObservableU3Ec__AnonStorey98_2_t1661826968;
// UnityEngine.Events.UnityAction`2<System.Object,System.Object>
struct UnityAction_2_t2076416286;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey98`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CAsObservableU3Ec__AnonStorey98_2__ctor_m2197525914_gshared (U3CAsObservableU3Ec__AnonStorey98_2_t1661826968 * __this, const MethodInfo* method);
#define U3CAsObservableU3Ec__AnonStorey98_2__ctor_m2197525914(__this, method) ((  void (*) (U3CAsObservableU3Ec__AnonStorey98_2_t1661826968 *, const MethodInfo*))U3CAsObservableU3Ec__AnonStorey98_2__ctor_m2197525914_gshared)(__this, method)
// System.Void UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey98`2<System.Object,System.Object>::<>m__CD(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void U3CAsObservableU3Ec__AnonStorey98_2_U3CU3Em__CD_m967079360_gshared (U3CAsObservableU3Ec__AnonStorey98_2_t1661826968 * __this, UnityAction_2_t2076416286 * ___h0, const MethodInfo* method);
#define U3CAsObservableU3Ec__AnonStorey98_2_U3CU3Em__CD_m967079360(__this, ___h0, method) ((  void (*) (U3CAsObservableU3Ec__AnonStorey98_2_t1661826968 *, UnityAction_2_t2076416286 *, const MethodInfo*))U3CAsObservableU3Ec__AnonStorey98_2_U3CU3Em__CD_m967079360_gshared)(__this, ___h0, method)
// System.Void UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey98`2<System.Object,System.Object>::<>m__CE(UnityEngine.Events.UnityAction`2<T0,T1>)
extern "C"  void U3CAsObservableU3Ec__AnonStorey98_2_U3CU3Em__CE_m1091152607_gshared (U3CAsObservableU3Ec__AnonStorey98_2_t1661826968 * __this, UnityAction_2_t2076416286 * ___h0, const MethodInfo* method);
#define U3CAsObservableU3Ec__AnonStorey98_2_U3CU3Em__CE_m1091152607(__this, ___h0, method) ((  void (*) (U3CAsObservableU3Ec__AnonStorey98_2_t1661826968 *, UnityAction_2_t2076416286 *, const MethodInfo*))U3CAsObservableU3Ec__AnonStorey98_2_U3CU3Em__CE_m1091152607_gshared)(__this, ___h0, method)
