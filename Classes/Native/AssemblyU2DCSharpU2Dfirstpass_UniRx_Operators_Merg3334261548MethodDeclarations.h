﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.MergeObservable`1/MergeOuterObserver<UniRx.Unit>
struct MergeOuterObserver_t3334261548;
// UniRx.Operators.MergeObservable`1<UniRx.Unit>
struct MergeObservable_1_t1064204364;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.MergeObservable`1/MergeOuterObserver<UniRx.Unit>::.ctor(UniRx.Operators.MergeObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void MergeOuterObserver__ctor_m190944233_gshared (MergeOuterObserver_t3334261548 * __this, MergeObservable_1_t1064204364 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define MergeOuterObserver__ctor_m190944233(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (MergeOuterObserver_t3334261548 *, MergeObservable_1_t1064204364 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))MergeOuterObserver__ctor_m190944233_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.MergeObservable`1/MergeOuterObserver<UniRx.Unit>::Run()
extern "C"  Il2CppObject * MergeOuterObserver_Run_m3558962596_gshared (MergeOuterObserver_t3334261548 * __this, const MethodInfo* method);
#define MergeOuterObserver_Run_m3558962596(__this, method) ((  Il2CppObject * (*) (MergeOuterObserver_t3334261548 *, const MethodInfo*))MergeOuterObserver_Run_m3558962596_gshared)(__this, method)
// System.Void UniRx.Operators.MergeObservable`1/MergeOuterObserver<UniRx.Unit>::OnNext(UniRx.IObservable`1<T>)
extern "C"  void MergeOuterObserver_OnNext_m3240987263_gshared (MergeOuterObserver_t3334261548 * __this, Il2CppObject* ___value0, const MethodInfo* method);
#define MergeOuterObserver_OnNext_m3240987263(__this, ___value0, method) ((  void (*) (MergeOuterObserver_t3334261548 *, Il2CppObject*, const MethodInfo*))MergeOuterObserver_OnNext_m3240987263_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.MergeObservable`1/MergeOuterObserver<UniRx.Unit>::OnError(System.Exception)
extern "C"  void MergeOuterObserver_OnError_m4111113165_gshared (MergeOuterObserver_t3334261548 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define MergeOuterObserver_OnError_m4111113165(__this, ___error0, method) ((  void (*) (MergeOuterObserver_t3334261548 *, Exception_t1967233988 *, const MethodInfo*))MergeOuterObserver_OnError_m4111113165_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.MergeObservable`1/MergeOuterObserver<UniRx.Unit>::OnCompleted()
extern "C"  void MergeOuterObserver_OnCompleted_m2381503136_gshared (MergeOuterObserver_t3334261548 * __this, const MethodInfo* method);
#define MergeOuterObserver_OnCompleted_m2381503136(__this, method) ((  void (*) (MergeOuterObserver_t3334261548 *, const MethodInfo*))MergeOuterObserver_OnCompleted_m2381503136_gshared)(__this, method)
