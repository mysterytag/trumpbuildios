﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Int64>
struct List_1_t3644373851;
// System.Collections.Generic.IEnumerable`1<System.Int64>
struct IEnumerable_1_t1424601942;
// System.Collections.Generic.IEnumerator`1<System.Int64>
struct IEnumerator_1_t35554034;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<System.Int64>
struct ICollection_1_t3313246268;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int64>
struct ReadOnlyCollection_1_t1715592934;
// System.Int64[]
struct Int64U5BU5D_t753178071;
// System.Predicate`1<System.Int64>
struct Predicate_1_t3418378780;
// System.Action`1<System.Int64>
struct Action_1_t2995867587;
// System.Collections.Generic.IComparer`1<System.Int64>
struct IComparer_1_t1252154995;
// System.Comparison`1<System.Int64>
struct Comparison_1_t1256122462;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1730156843.h"

// System.Void System.Collections.Generic.List`1<System.Int64>::.ctor()
extern "C"  void List_1__ctor_m2722530713_gshared (List_1_t3644373851 * __this, const MethodInfo* method);
#define List_1__ctor_m2722530713(__this, method) ((  void (*) (List_1_t3644373851 *, const MethodInfo*))List_1__ctor_m2722530713_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m4154939677_gshared (List_1_t3644373851 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m4154939677(__this, ___collection0, method) ((  void (*) (List_1_t3644373851 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m4154939677_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m120513651_gshared (List_1_t3644373851 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m120513651(__this, ___capacity0, method) ((  void (*) (List_1_t3644373851 *, int32_t, const MethodInfo*))List_1__ctor_m120513651_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::.cctor()
extern "C"  void List_1__cctor_m491306443_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m491306443(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m491306443_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Int64>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2751001516_gshared (List_1_t3644373851 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2751001516(__this, method) ((  Il2CppObject* (*) (List_1_t3644373851 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2751001516_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m2167217506_gshared (List_1_t3644373851 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m2167217506(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3644373851 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m2167217506_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Int64>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m3786885873_gshared (List_1_t3644373851 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3786885873(__this, method) ((  Il2CppObject * (*) (List_1_t3644373851 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3786885873_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Int64>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m1407510508_gshared (List_1_t3644373851 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m1407510508(__this, ___item0, method) ((  int32_t (*) (List_1_t3644373851 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m1407510508_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<System.Int64>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m530987988_gshared (List_1_t3644373851 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m530987988(__this, ___item0, method) ((  bool (*) (List_1_t3644373851 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m530987988_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<System.Int64>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m894912580_gshared (List_1_t3644373851 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m894912580(__this, ___item0, method) ((  int32_t (*) (List_1_t3644373851 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m894912580_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m2059506103_gshared (List_1_t3644373851 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m2059506103(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3644373851 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2059506103_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m536123921_gshared (List_1_t3644373851 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m536123921(__this, ___item0, method) ((  void (*) (List_1_t3644373851 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m536123921_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<System.Int64>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3932713685_gshared (List_1_t3644373851 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3932713685(__this, method) ((  bool (*) (List_1_t3644373851 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3932713685_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Int64>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m293575432_gshared (List_1_t3644373851 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m293575432(__this, method) ((  bool (*) (List_1_t3644373851 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m293575432_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Int64>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m119941370_gshared (List_1_t3644373851 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m119941370(__this, method) ((  Il2CppObject * (*) (List_1_t3644373851 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m119941370_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Int64>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m4138490435_gshared (List_1_t3644373851 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m4138490435(__this, method) ((  bool (*) (List_1_t3644373851 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m4138490435_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Int64>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m1972981846_gshared (List_1_t3644373851 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m1972981846(__this, method) ((  bool (*) (List_1_t3644373851 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m1972981846_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Int64>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m365151233_gshared (List_1_t3644373851 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m365151233(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t3644373851 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m365151233_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m4114361294_gshared (List_1_t3644373851 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m4114361294(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3644373851 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m4114361294_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::Add(T)
extern "C"  void List_1_Add_m558752285_gshared (List_1_t3644373851 * __this, int64_t ___item0, const MethodInfo* method);
#define List_1_Add_m558752285(__this, ___item0, method) ((  void (*) (List_1_t3644373851 *, int64_t, const MethodInfo*))List_1_Add_m558752285_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m2593120216_gshared (List_1_t3644373851 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m2593120216(__this, ___newCount0, method) ((  void (*) (List_1_t3644373851 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m2593120216_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m3863664342_gshared (List_1_t3644373851 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m3863664342(__this, ___collection0, method) ((  void (*) (List_1_t3644373851 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m3863664342_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m3124636566_gshared (List_1_t3644373851 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m3124636566(__this, ___enumerable0, method) ((  void (*) (List_1_t3644373851 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m3124636566_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m2901368129_gshared (List_1_t3644373851 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m2901368129(__this, ___collection0, method) ((  void (*) (List_1_t3644373851 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2901368129_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Int64>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t1715592934 * List_1_AsReadOnly_m2857059748_gshared (List_1_t3644373851 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m2857059748(__this, method) ((  ReadOnlyCollection_1_t1715592934 * (*) (List_1_t3644373851 *, const MethodInfo*))List_1_AsReadOnly_m2857059748_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::Clear()
extern "C"  void List_1_Clear_m2148142669_gshared (List_1_t3644373851 * __this, const MethodInfo* method);
#define List_1_Clear_m2148142669(__this, method) ((  void (*) (List_1_t3644373851 *, const MethodInfo*))List_1_Clear_m2148142669_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Int64>::Contains(T)
extern "C"  bool List_1_Contains_m348921023_gshared (List_1_t3644373851 * __this, int64_t ___item0, const MethodInfo* method);
#define List_1_Contains_m348921023(__this, ___item0, method) ((  bool (*) (List_1_t3644373851 *, int64_t, const MethodInfo*))List_1_Contains_m348921023_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m3419167949_gshared (List_1_t3644373851 * __this, Int64U5BU5D_t753178071* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m3419167949(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3644373851 *, Int64U5BU5D_t753178071*, int32_t, const MethodInfo*))List_1_CopyTo_m3419167949_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<System.Int64>::Find(System.Predicate`1<T>)
extern "C"  int64_t List_1_Find_m3570887833_gshared (List_1_t3644373851 * __this, Predicate_1_t3418378780 * ___match0, const MethodInfo* method);
#define List_1_Find_m3570887833(__this, ___match0, method) ((  int64_t (*) (List_1_t3644373851 *, Predicate_1_t3418378780 *, const MethodInfo*))List_1_Find_m3570887833_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m728553654_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t3418378780 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m728553654(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t3418378780 *, const MethodInfo*))List_1_CheckMatch_m728553654_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<System.Int64>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m1911567507_gshared (List_1_t3644373851 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t3418378780 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m1911567507(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t3644373851 *, int32_t, int32_t, Predicate_1_t3418378780 *, const MethodInfo*))List_1_GetIndex_m1911567507_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::ForEach(System.Action`1<T>)
extern "C"  void List_1_ForEach_m1861088426_gshared (List_1_t3644373851 * __this, Action_1_t2995867587 * ___action0, const MethodInfo* method);
#define List_1_ForEach_m1861088426(__this, ___action0, method) ((  void (*) (List_1_t3644373851 *, Action_1_t2995867587 *, const MethodInfo*))List_1_ForEach_m1861088426_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Int64>::GetEnumerator()
extern "C"  Enumerator_t1730156843  List_1_GetEnumerator_m2263040038_gshared (List_1_t3644373851 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m2263040038(__this, method) ((  Enumerator_t1730156843  (*) (List_1_t3644373851 *, const MethodInfo*))List_1_GetEnumerator_m2263040038_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Int64>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m4291468633_gshared (List_1_t3644373851 * __this, int64_t ___item0, const MethodInfo* method);
#define List_1_IndexOf_m4291468633(__this, ___item0, method) ((  int32_t (*) (List_1_t3644373851 *, int64_t, const MethodInfo*))List_1_IndexOf_m4291468633_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m4275377700_gshared (List_1_t3644373851 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m4275377700(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t3644373851 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m4275377700_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m3165535133_gshared (List_1_t3644373851 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m3165535133(__this, ___index0, method) ((  void (*) (List_1_t3644373851 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3165535133_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m1048920708_gshared (List_1_t3644373851 * __this, int32_t ___index0, int64_t ___item1, const MethodInfo* method);
#define List_1_Insert_m1048920708(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3644373851 *, int32_t, int64_t, const MethodInfo*))List_1_Insert_m1048920708_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m2656357369_gshared (List_1_t3644373851 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m2656357369(__this, ___collection0, method) ((  void (*) (List_1_t3644373851 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2656357369_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<System.Int64>::Remove(T)
extern "C"  bool List_1_Remove_m2609840250_gshared (List_1_t3644373851 * __this, int64_t ___item0, const MethodInfo* method);
#define List_1_Remove_m2609840250(__this, ___item0, method) ((  bool (*) (List_1_t3644373851 *, int64_t, const MethodInfo*))List_1_Remove_m2609840250_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<System.Int64>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m631740550_gshared (List_1_t3644373851 * __this, Predicate_1_t3418378780 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m631740550(__this, ___match0, method) ((  int32_t (*) (List_1_t3644373851 *, Predicate_1_t3418378780 *, const MethodInfo*))List_1_RemoveAll_m631740550_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m3217740874_gshared (List_1_t3644373851 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m3217740874(__this, ___index0, method) ((  void (*) (List_1_t3644373851 *, int32_t, const MethodInfo*))List_1_RemoveAt_m3217740874_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::Reverse()
extern "C"  void List_1_Reverse_m148522466_gshared (List_1_t3644373851 * __this, const MethodInfo* method);
#define List_1_Reverse_m148522466(__this, method) ((  void (*) (List_1_t3644373851 *, const MethodInfo*))List_1_Reverse_m148522466_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::Sort()
extern "C"  void List_1_Sort_m1084724480_gshared (List_1_t3644373851 * __this, const MethodInfo* method);
#define List_1_Sort_m1084724480(__this, method) ((  void (*) (List_1_t3644373851 *, const MethodInfo*))List_1_Sort_m1084724480_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m3737735396_gshared (List_1_t3644373851 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m3737735396(__this, ___comparer0, method) ((  void (*) (List_1_t3644373851 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m3737735396_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m3240091987_gshared (List_1_t3644373851 * __this, Comparison_1_t1256122462 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m3240091987(__this, ___comparison0, method) ((  void (*) (List_1_t3644373851 *, Comparison_1_t1256122462 *, const MethodInfo*))List_1_Sort_m3240091987_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<System.Int64>::ToArray()
extern "C"  Int64U5BU5D_t753178071* List_1_ToArray_m3401137659_gshared (List_1_t3644373851 * __this, const MethodInfo* method);
#define List_1_ToArray_m3401137659(__this, method) ((  Int64U5BU5D_t753178071* (*) (List_1_t3644373851 *, const MethodInfo*))List_1_ToArray_m3401137659_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::TrimExcess()
extern "C"  void List_1_TrimExcess_m3490903193_gshared (List_1_t3644373851 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m3490903193(__this, method) ((  void (*) (List_1_t3644373851 *, const MethodInfo*))List_1_TrimExcess_m3490903193_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Int64>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m2486845705_gshared (List_1_t3644373851 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m2486845705(__this, method) ((  int32_t (*) (List_1_t3644373851 *, const MethodInfo*))List_1_get_Capacity_m2486845705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m4098276330_gshared (List_1_t3644373851 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m4098276330(__this, ___value0, method) ((  void (*) (List_1_t3644373851 *, int32_t, const MethodInfo*))List_1_set_Capacity_m4098276330_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<System.Int64>::get_Count()
extern "C"  int32_t List_1_get_Count_m4003816258_gshared (List_1_t3644373851 * __this, const MethodInfo* method);
#define List_1_get_Count_m4003816258(__this, method) ((  int32_t (*) (List_1_t3644373851 *, const MethodInfo*))List_1_get_Count_m4003816258_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Int64>::get_Item(System.Int32)
extern "C"  int64_t List_1_get_Item_m846582384_gshared (List_1_t3644373851 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m846582384(__this, ___index0, method) ((  int64_t (*) (List_1_t3644373851 *, int32_t, const MethodInfo*))List_1_get_Item_m846582384_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Int64>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m505082459_gshared (List_1_t3644373851 * __this, int32_t ___index0, int64_t ___value1, const MethodInfo* method);
#define List_1_set_Item_m505082459(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3644373851 *, int32_t, int64_t, const MethodInfo*))List_1_set_Item_m505082459_gshared)(__this, ___index0, ___value1, method)
