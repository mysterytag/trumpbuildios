﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Executer
struct Executer_t2107661245;
// IProcessExecution
struct IProcessExecution_t2362207186;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// ILivingState
struct ILivingState_t4195066719;
// System.Action
struct Action_t437523947;
// IProcess
struct IProcess_t4026237414;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"

// System.Void Executer::.ctor()
extern "C"  void Executer__ctor_m1414481790 (Executer_t2107661245 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// IProcessExecution Executer::Execute(System.Collections.IEnumerator,ILivingState,System.Action)
extern "C"  Il2CppObject * Executer_Execute_m2438425955 (Executer_t2107661245 * __this, Il2CppObject * ___bearutine0, Il2CppObject * ___owner1, Action_t437523947 * ___dispose2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// IProcessExecution Executer::Execute(IProcess)
extern "C"  Il2CppObject * Executer_Execute_m4215064714 (Executer_t2107661245 * __this, Il2CppObject * ___bearutine0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// IProcessExecution Executer::Delay(System.Action,System.Single,ILivingState)
extern "C"  Il2CppObject * Executer_Delay_m2003396323 (Executer_t2107661245 * __this, Action_t437523947 * ___act0, float ___time1, Il2CppObject * ___owner2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Executer::DelayCoro(System.Action,System.Single)
extern "C"  Il2CppObject * Executer_DelayCoro_m3741452034 (Executer_t2107661245 * __this, Action_t437523947 * ___act0, float ___time1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Executer::Terminate(IProcess)
extern "C"  void Executer_Terminate_m4182800407 (Executer_t2107661245 * __this, Il2CppObject * ___coro0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Executer::Tick(System.Single)
extern "C"  void Executer_Tick_m3407388200 (Executer_t2107661245 * __this, float ___dt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Executer::get_empty()
extern "C"  bool Executer_get_empty_m642887468 (Executer_t2107661245 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Executer::Dispose()
extern "C"  void Executer_Dispose_m2005617723 (Executer_t2107661245 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
