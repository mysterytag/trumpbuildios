﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZergEngineTools/UnityActionDisposable`1<System.Object>
struct UnityActionDisposable_1_t3710771925;

#include "codegen/il2cpp-codegen.h"

// System.Void ZergEngineTools/UnityActionDisposable`1<System.Object>::.ctor()
extern "C"  void UnityActionDisposable_1__ctor_m3503343429_gshared (UnityActionDisposable_1_t3710771925 * __this, const MethodInfo* method);
#define UnityActionDisposable_1__ctor_m3503343429(__this, method) ((  void (*) (UnityActionDisposable_1_t3710771925 *, const MethodInfo*))UnityActionDisposable_1__ctor_m3503343429_gshared)(__this, method)
// System.Void ZergEngineTools/UnityActionDisposable`1<System.Object>::Dispose()
extern "C"  void UnityActionDisposable_1_Dispose_m3651925570_gshared (UnityActionDisposable_1_t3710771925 * __this, const MethodInfo* method);
#define UnityActionDisposable_1_Dispose_m3651925570(__this, method) ((  void (*) (UnityActionDisposable_1_t3710771925 *, const MethodInfo*))UnityActionDisposable_1_Dispose_m3651925570_gshared)(__this, method)
