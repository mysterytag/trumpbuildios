﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DG.Tweening.Tweener
struct Tweener_t1766303790;
// UnityEngine.AudioSource
struct AudioSource_t3628549054;
// UnityEngine.Camera
struct Camera_t3533968274;
// UnityEngine.Light
struct Light_t1596303683;
// UnityEngine.LineRenderer
struct LineRenderer_t305781060;
// UnityEngine.Material
struct Material_t1886596500;
// System.String
struct String_t;
// UnityEngine.Rigidbody
struct Rigidbody_t1972007546;
// DG.Tweening.Sequence
struct Sequence_t2436335575;
// UnityEngine.TrailRenderer
struct TrailRenderer_t450938134;
// UnityEngine.Transform
struct Transform_t284553113;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>
struct TweenerCore_3_t3204927250;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t3227571696;
// DG.Tweening.Plugins.Core.PathCore.Path
struct Path_t3978043016;
// UnityEngine.Component
struct Component_t2126946602;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_AudioSource3628549054.h"
#include "UnityEngine_UnityEngine_Camera3533968274.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"
#include "UnityEngine_UnityEngine_Rect1525428817.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"
#include "UnityEngine_UnityEngine_Light1596303683.h"
#include "UnityEngine_UnityEngine_LineRenderer305781060.h"
#include "DOTween_DG_Tweening_Color23046135109.h"
#include "UnityEngine_UnityEngine_Material1886596500.h"
#include "mscorlib_System_String968488902.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"
#include "UnityEngine_UnityEngine_Vector43525329790.h"
#include "UnityEngine_UnityEngine_Rigidbody1972007546.h"
#include "DOTween_DG_Tweening_RotateMode2936663476.h"
#include "DOTween_DG_Tweening_AxisConstraint3652844660.h"
#include "mscorlib_System_Nullable_1_gen2116400401.h"
#include "UnityEngine_UnityEngine_TrailRenderer450938134.h"
#include "UnityEngine_UnityEngine_Transform284553113.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979.h"
#include "DOTween_DG_Tweening_PathType2321210645.h"
#include "DOTween_DG_Tweening_PathMode2320992126.h"
#include "mscorlib_System_Nullable_1_gen179246372.h"
#include "DOTween_DG_Tweening_Plugins_Core_PathCore_Path3978043016.h"
#include "UnityEngine_UnityEngine_Component2126946602.h"

// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOFade(UnityEngine.AudioSource,System.Single,System.Single)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOFade_m1944991959 (Il2CppObject * __this /* static, unused */, AudioSource_t3628549054 * ___target0, float ___endValue1, float ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOPitch(UnityEngine.AudioSource,System.Single,System.Single)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOPitch_m2219177327 (Il2CppObject * __this /* static, unused */, AudioSource_t3628549054 * ___target0, float ___endValue1, float ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOAspect(UnityEngine.Camera,System.Single,System.Single)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOAspect_m1343288533 (Il2CppObject * __this /* static, unused */, Camera_t3533968274 * ___target0, float ___endValue1, float ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOColor(UnityEngine.Camera,UnityEngine.Color,System.Single)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOColor_m2493006911 (Il2CppObject * __this /* static, unused */, Camera_t3533968274 * ___target0, Color_t1588175760  ___endValue1, float ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOFarClipPlane(UnityEngine.Camera,System.Single,System.Single)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOFarClipPlane_m3296962450 (Il2CppObject * __this /* static, unused */, Camera_t3533968274 * ___target0, float ___endValue1, float ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOFieldOfView(UnityEngine.Camera,System.Single,System.Single)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOFieldOfView_m625212631 (Il2CppObject * __this /* static, unused */, Camera_t3533968274 * ___target0, float ___endValue1, float ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DONearClipPlane(UnityEngine.Camera,System.Single,System.Single)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DONearClipPlane_m3896500037 (Il2CppObject * __this /* static, unused */, Camera_t3533968274 * ___target0, float ___endValue1, float ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOOrthoSize(UnityEngine.Camera,System.Single,System.Single)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOOrthoSize_m502679674 (Il2CppObject * __this /* static, unused */, Camera_t3533968274 * ___target0, float ___endValue1, float ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOPixelRect(UnityEngine.Camera,UnityEngine.Rect,System.Single)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOPixelRect_m2480562991 (Il2CppObject * __this /* static, unused */, Camera_t3533968274 * ___target0, Rect_t1525428817  ___endValue1, float ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DORect(UnityEngine.Camera,UnityEngine.Rect,System.Single)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DORect_m3103667641 (Il2CppObject * __this /* static, unused */, Camera_t3533968274 * ___target0, Rect_t1525428817  ___endValue1, float ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOShakePosition(UnityEngine.Camera,System.Single,System.Single,System.Int32,System.Single)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOShakePosition_m3119851404 (Il2CppObject * __this /* static, unused */, Camera_t3533968274 * ___target0, float ___duration1, float ___strength2, int32_t ___vibrato3, float ___randomness4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOShakePosition(UnityEngine.Camera,System.Single,UnityEngine.Vector3,System.Int32,System.Single)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOShakePosition_m1265593438 (Il2CppObject * __this /* static, unused */, Camera_t3533968274 * ___target0, float ___duration1, Vector3_t3525329789  ___strength2, int32_t ___vibrato3, float ___randomness4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOShakeRotation(UnityEngine.Camera,System.Single,System.Single,System.Int32,System.Single)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOShakeRotation_m4190831127 (Il2CppObject * __this /* static, unused */, Camera_t3533968274 * ___target0, float ___duration1, float ___strength2, int32_t ___vibrato3, float ___randomness4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOShakeRotation(UnityEngine.Camera,System.Single,UnityEngine.Vector3,System.Int32,System.Single)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOShakeRotation_m1593569961 (Il2CppObject * __this /* static, unused */, Camera_t3533968274 * ___target0, float ___duration1, Vector3_t3525329789  ___strength2, int32_t ___vibrato3, float ___randomness4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOColor(UnityEngine.Light,UnityEngine.Color,System.Single)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOColor_m2210283266 (Il2CppObject * __this /* static, unused */, Light_t1596303683 * ___target0, Color_t1588175760  ___endValue1, float ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOIntensity(UnityEngine.Light,System.Single,System.Single)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOIntensity_m1893393271 (Il2CppObject * __this /* static, unused */, Light_t1596303683 * ___target0, float ___endValue1, float ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOShadowStrength(UnityEngine.Light,System.Single,System.Single)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOShadowStrength_m3726046477 (Il2CppObject * __this /* static, unused */, Light_t1596303683 * ___target0, float ___endValue1, float ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOColor(UnityEngine.LineRenderer,DG.Tweening.Color2,DG.Tweening.Color2,System.Single)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOColor_m2234840077 (Il2CppObject * __this /* static, unused */, LineRenderer_t305781060 * ___target0, Color2_t3046135109  ___startValue1, Color2_t3046135109  ___endValue2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOColor(UnityEngine.Material,UnityEngine.Color,System.Single)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOColor_m616918269 (Il2CppObject * __this /* static, unused */, Material_t1886596500 * ___target0, Color_t1588175760  ___endValue1, float ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOColor(UnityEngine.Material,UnityEngine.Color,System.String,System.Single)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOColor_m1550053945 (Il2CppObject * __this /* static, unused */, Material_t1886596500 * ___target0, Color_t1588175760  ___endValue1, String_t* ___property2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOFade(UnityEngine.Material,System.Single,System.Single)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOFade_m3378649335 (Il2CppObject * __this /* static, unused */, Material_t1886596500 * ___target0, float ___endValue1, float ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOFade(UnityEngine.Material,System.Single,System.String,System.Single)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOFade_m272572595 (Il2CppObject * __this /* static, unused */, Material_t1886596500 * ___target0, float ___endValue1, String_t* ___property2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOFloat(UnityEngine.Material,System.Single,System.String,System.Single)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOFloat_m677483575 (Il2CppObject * __this /* static, unused */, Material_t1886596500 * ___target0, float ___endValue1, String_t* ___property2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOOffset(UnityEngine.Material,UnityEngine.Vector2,System.Single)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOOffset_m3676518685 (Il2CppObject * __this /* static, unused */, Material_t1886596500 * ___target0, Vector2_t3525329788  ___endValue1, float ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOOffset(UnityEngine.Material,UnityEngine.Vector2,System.String,System.Single)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOOffset_m1075447385 (Il2CppObject * __this /* static, unused */, Material_t1886596500 * ___target0, Vector2_t3525329788  ___endValue1, String_t* ___property2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOTiling(UnityEngine.Material,UnityEngine.Vector2,System.Single)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOTiling_m2022336629 (Il2CppObject * __this /* static, unused */, Material_t1886596500 * ___target0, Vector2_t3525329788  ___endValue1, float ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOTiling(UnityEngine.Material,UnityEngine.Vector2,System.String,System.Single)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOTiling_m154106801 (Il2CppObject * __this /* static, unused */, Material_t1886596500 * ___target0, Vector2_t3525329788  ___endValue1, String_t* ___property2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOVector(UnityEngine.Material,UnityEngine.Vector4,System.String,System.Single)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOVector_m2763265607 (Il2CppObject * __this /* static, unused */, Material_t1886596500 * ___target0, Vector4_t3525329790  ___endValue1, String_t* ___property2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOMove(UnityEngine.Rigidbody,UnityEngine.Vector3,System.Single,System.Boolean)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOMove_m1831907945 (Il2CppObject * __this /* static, unused */, Rigidbody_t1972007546 * ___target0, Vector3_t3525329789  ___endValue1, float ___duration2, bool ___snapping3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOMoveX(UnityEngine.Rigidbody,System.Single,System.Single,System.Boolean)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOMoveX_m4170913105 (Il2CppObject * __this /* static, unused */, Rigidbody_t1972007546 * ___target0, float ___endValue1, float ___duration2, bool ___snapping3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOMoveY(UnityEngine.Rigidbody,System.Single,System.Single,System.Boolean)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOMoveY_m3562836242 (Il2CppObject * __this /* static, unused */, Rigidbody_t1972007546 * ___target0, float ___endValue1, float ___duration2, bool ___snapping3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOMoveZ(UnityEngine.Rigidbody,System.Single,System.Single,System.Boolean)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOMoveZ_m2954759379 (Il2CppObject * __this /* static, unused */, Rigidbody_t1972007546 * ___target0, float ___endValue1, float ___duration2, bool ___snapping3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DORotate(UnityEngine.Rigidbody,UnityEngine.Vector3,System.Single,DG.Tweening.RotateMode)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DORotate_m2053526162 (Il2CppObject * __this /* static, unused */, Rigidbody_t1972007546 * ___target0, Vector3_t3525329789  ___endValue1, float ___duration2, int32_t ___mode3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOLookAt(UnityEngine.Rigidbody,UnityEngine.Vector3,System.Single,DG.Tweening.AxisConstraint,System.Nullable`1<UnityEngine.Vector3>)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOLookAt_m3411111949 (Il2CppObject * __this /* static, unused */, Rigidbody_t1972007546 * ___target0, Vector3_t3525329789  ___towards1, float ___duration2, int32_t ___axisConstraint3, Nullable_1_t2116400401  ___up4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Sequence DG.Tweening.ShortcutExtensions::DOJump(UnityEngine.Rigidbody,UnityEngine.Vector3,System.Single,System.Int32,System.Single,System.Boolean)
extern "C"  Sequence_t2436335575 * ShortcutExtensions_DOJump_m3734533771 (Il2CppObject * __this /* static, unused */, Rigidbody_t1972007546 * ___target0, Vector3_t3525329789  ___endValue1, float ___jumpPower2, int32_t ___numJumps3, float ___duration4, bool ___snapping5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOResize(UnityEngine.TrailRenderer,System.Single,System.Single,System.Single)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOResize_m179951148 (Il2CppObject * __this /* static, unused */, TrailRenderer_t450938134 * ___target0, float ___toStartWidth1, float ___toEndWidth2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOTime(UnityEngine.TrailRenderer,System.Single,System.Single)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOTime_m4255001038 (Il2CppObject * __this /* static, unused */, TrailRenderer_t450938134 * ___target0, float ___endValue1, float ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOMove(UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Boolean)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOMove_m3543894472 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___target0, Vector3_t3525329789  ___endValue1, float ___duration2, bool ___snapping3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOMoveX(UnityEngine.Transform,System.Single,System.Single,System.Boolean)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOMoveX_m2549522416 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___target0, float ___endValue1, float ___duration2, bool ___snapping3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOMoveY(UnityEngine.Transform,System.Single,System.Single,System.Boolean)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOMoveY_m1941445553 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___target0, float ___endValue1, float ___duration2, bool ___snapping3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOMoveZ(UnityEngine.Transform,System.Single,System.Single,System.Boolean)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOMoveZ_m1333368690 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___target0, float ___endValue1, float ___duration2, bool ___snapping3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOLocalMove(UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Boolean)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOLocalMove_m3569399671 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___target0, Vector3_t3525329789  ___endValue1, float ___duration2, bool ___snapping3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOLocalMoveX(UnityEngine.Transform,System.Single,System.Single,System.Boolean)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOLocalMoveX_m728326369 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___target0, float ___endValue1, float ___duration2, bool ___snapping3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOLocalMoveY(UnityEngine.Transform,System.Single,System.Single,System.Boolean)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOLocalMoveY_m120249506 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___target0, float ___endValue1, float ___duration2, bool ___snapping3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOLocalMoveZ(UnityEngine.Transform,System.Single,System.Single,System.Boolean)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOLocalMoveZ_m3807139939 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___target0, float ___endValue1, float ___duration2, bool ___snapping3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DORotate(UnityEngine.Transform,UnityEngine.Vector3,System.Single,DG.Tweening.RotateMode)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DORotate_m3037682417 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___target0, Vector3_t3525329789  ___endValue1, float ___duration2, int32_t ___mode3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DORotateQuaternion(UnityEngine.Transform,UnityEngine.Quaternion,System.Single)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DORotateQuaternion_m1700613121 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___target0, Quaternion_t1891715979  ___endValue1, float ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOLocalRotate(UnityEngine.Transform,UnityEngine.Vector3,System.Single,DG.Tweening.RotateMode)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOLocalRotate_m1343650272 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___target0, Vector3_t3525329789  ___endValue1, float ___duration2, int32_t ___mode3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOLocalRotateQuaternion(UnityEngine.Transform,UnityEngine.Quaternion,System.Single)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOLocalRotateQuaternion_m1726118320 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___target0, Quaternion_t1891715979  ___endValue1, float ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOScale(UnityEngine.Transform,UnityEngine.Vector3,System.Single)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOScale_m4284974744 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___target0, Vector3_t3525329789  ___endValue1, float ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOScale(UnityEngine.Transform,System.Single,System.Single)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOScale_m2001861738 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___target0, float ___endValue1, float ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOScaleX(UnityEngine.Transform,System.Single,System.Single)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOScaleX_m2081189002 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___target0, float ___endValue1, float ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOScaleY(UnityEngine.Transform,System.Single,System.Single)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOScaleY_m2850359017 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___target0, float ___endValue1, float ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOScaleZ(UnityEngine.Transform,System.Single,System.Single)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOScaleZ_m3619529032 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___target0, float ___endValue1, float ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOLookAt(UnityEngine.Transform,UnityEngine.Vector3,System.Single,DG.Tweening.AxisConstraint,System.Nullable`1<UnityEngine.Vector3>)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOLookAt_m2584642126 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___target0, Vector3_t3525329789  ___towards1, float ___duration2, int32_t ___axisConstraint3, Nullable_1_t2116400401  ___up4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOPunchPosition(UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Int32,System.Single,System.Boolean)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOPunchPosition_m512150732 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___target0, Vector3_t3525329789  ___punch1, float ___duration2, int32_t ___vibrato3, float ___elasticity4, bool ___snapping5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOPunchScale(UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Int32,System.Single)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOPunchScale_m2925235250 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___target0, Vector3_t3525329789  ___punch1, float ___duration2, int32_t ___vibrato3, float ___elasticity4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOPunchRotation(UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Int32,System.Single)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOPunchRotation_m2306196582 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___target0, Vector3_t3525329789  ___punch1, float ___duration2, int32_t ___vibrato3, float ___elasticity4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOShakePosition(UnityEngine.Transform,System.Single,System.Single,System.Int32,System.Single,System.Boolean)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOShakePosition_m3172874118 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___target0, float ___duration1, float ___strength2, int32_t ___vibrato3, float ___randomness4, bool ___snapping5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOShakePosition(UnityEngine.Transform,System.Single,UnityEngine.Vector3,System.Int32,System.Single,System.Boolean)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOShakePosition_m2153418932 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___target0, float ___duration1, Vector3_t3525329789  ___strength2, int32_t ___vibrato3, float ___randomness4, bool ___snapping5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOShakeRotation(UnityEngine.Transform,System.Single,System.Single,System.Int32,System.Single)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOShakeRotation_m750043308 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___target0, float ___duration1, float ___strength2, int32_t ___vibrato3, float ___randomness4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOShakeRotation(UnityEngine.Transform,System.Single,UnityEngine.Vector3,System.Int32,System.Single)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOShakeRotation_m2837425534 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___target0, float ___duration1, Vector3_t3525329789  ___strength2, int32_t ___vibrato3, float ___randomness4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOShakeScale(UnityEngine.Transform,System.Single,System.Single,System.Int32,System.Single)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOShakeScale_m3214321864 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___target0, float ___duration1, float ___strength2, int32_t ___vibrato3, float ___randomness4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOShakeScale(UnityEngine.Transform,System.Single,UnityEngine.Vector3,System.Int32,System.Single)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOShakeScale_m1042419866 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___target0, float ___duration1, Vector3_t3525329789  ___strength2, int32_t ___vibrato3, float ___randomness4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Sequence DG.Tweening.ShortcutExtensions::DOJump(UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Int32,System.Single,System.Boolean)
extern "C"  Sequence_t2436335575 * ShortcutExtensions_DOJump_m3977313228 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___target0, Vector3_t3525329789  ___endValue1, float ___jumpPower2, int32_t ___numJumps3, float ___duration4, bool ___snapping5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Sequence DG.Tweening.ShortcutExtensions::DOLocalJump(UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Int32,System.Single,System.Boolean)
extern "C"  Sequence_t2436335575 * ShortcutExtensions_DOLocalJump_m891178167 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___target0, Vector3_t3525329789  ___endValue1, float ___jumpPower2, int32_t ___numJumps3, float ___duration4, bool ___snapping5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.ShortcutExtensions::DOPath(UnityEngine.Transform,UnityEngine.Vector3[],System.Single,DG.Tweening.PathType,DG.Tweening.PathMode,System.Int32,System.Nullable`1<UnityEngine.Color>)
extern "C"  TweenerCore_3_t3204927250 * ShortcutExtensions_DOPath_m1375070599 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___target0, Vector3U5BU5D_t3227571696* ___path1, float ___duration2, int32_t ___pathType3, int32_t ___pathMode4, int32_t ___resolution5, Nullable_1_t179246372  ___gizmoColor6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.ShortcutExtensions::DOLocalPath(UnityEngine.Transform,UnityEngine.Vector3[],System.Single,DG.Tweening.PathType,DG.Tweening.PathMode,System.Int32,System.Nullable`1<UnityEngine.Color>)
extern "C"  TweenerCore_3_t3204927250 * ShortcutExtensions_DOLocalPath_m650674586 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___target0, Vector3U5BU5D_t3227571696* ___path1, float ___duration2, int32_t ___pathType3, int32_t ___pathMode4, int32_t ___resolution5, Nullable_1_t179246372  ___gizmoColor6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.ShortcutExtensions::DOPath(UnityEngine.Transform,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
extern "C"  TweenerCore_3_t3204927250 * ShortcutExtensions_DOPath_m2484533641 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___target0, Path_t3978043016 * ___path1, float ___duration2, int32_t ___pathMode3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.ShortcutExtensions::DOLocalPath(UnityEngine.Transform,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
extern "C"  TweenerCore_3_t3204927250 * ShortcutExtensions_DOLocalPath_m1980820956 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___target0, Path_t3978043016 * ___path1, float ___duration2, int32_t ___pathMode3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOBlendableColor(UnityEngine.Light,UnityEngine.Color,System.Single)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOBlendableColor_m92518129 (Il2CppObject * __this /* static, unused */, Light_t1596303683 * ___target0, Color_t1588175760  ___endValue1, float ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOBlendableColor(UnityEngine.Material,UnityEngine.Color,System.Single)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOBlendableColor_m3345300142 (Il2CppObject * __this /* static, unused */, Material_t1886596500 * ___target0, Color_t1588175760  ___endValue1, float ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOBlendableColor(UnityEngine.Material,UnityEngine.Color,System.String,System.Single)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOBlendableColor_m1368225322 (Il2CppObject * __this /* static, unused */, Material_t1886596500 * ___target0, Color_t1588175760  ___endValue1, String_t* ___property2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOBlendableMoveBy(UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Boolean)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOBlendableMoveBy_m2735424046 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___target0, Vector3_t3525329789  ___byValue1, float ___duration2, bool ___snapping3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOBlendableLocalMoveBy(UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Boolean)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOBlendableLocalMoveBy_m3726213151 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___target0, Vector3_t3525329789  ___byValue1, float ___duration2, bool ___snapping3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOBlendableRotateBy(UnityEngine.Transform,UnityEngine.Vector3,System.Single,DG.Tweening.RotateMode)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOBlendableRotateBy_m3109733143 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___target0, Vector3_t3525329789  ___byValue1, float ___duration2, int32_t ___mode3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOBlendableLocalRotateBy(UnityEngine.Transform,UnityEngine.Vector3,System.Single,DG.Tweening.RotateMode)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOBlendableLocalRotateBy_m2764900296 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___target0, Vector3_t3525329789  ___byValue1, float ___duration2, int32_t ___mode3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOBlendableScaleBy(UnityEngine.Transform,UnityEngine.Vector3,System.Single)
extern "C"  Tweener_t1766303790 * ShortcutExtensions_DOBlendableScaleBy_m3763668464 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___target0, Vector3_t3525329789  ___byValue1, float ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.ShortcutExtensions::DOComplete(UnityEngine.Component,System.Boolean)
extern "C"  int32_t ShortcutExtensions_DOComplete_m2756368932 (Il2CppObject * __this /* static, unused */, Component_t2126946602 * ___target0, bool ___withCallbacks1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.ShortcutExtensions::DOComplete(UnityEngine.Material,System.Boolean)
extern "C"  int32_t ShortcutExtensions_DOComplete_m3700157960 (Il2CppObject * __this /* static, unused */, Material_t1886596500 * ___target0, bool ___withCallbacks1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.ShortcutExtensions::DOKill(UnityEngine.Component,System.Boolean)
extern "C"  int32_t ShortcutExtensions_DOKill_m2275247369 (Il2CppObject * __this /* static, unused */, Component_t2126946602 * ___target0, bool ___complete1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.ShortcutExtensions::DOKill(UnityEngine.Material,System.Boolean)
extern "C"  int32_t ShortcutExtensions_DOKill_m913691267 (Il2CppObject * __this /* static, unused */, Material_t1886596500 * ___target0, bool ___complete1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.ShortcutExtensions::DOFlip(UnityEngine.Component)
extern "C"  int32_t ShortcutExtensions_DOFlip_m494625285 (Il2CppObject * __this /* static, unused */, Component_t2126946602 * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.ShortcutExtensions::DOFlip(UnityEngine.Material)
extern "C"  int32_t ShortcutExtensions_DOFlip_m988616073 (Il2CppObject * __this /* static, unused */, Material_t1886596500 * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.ShortcutExtensions::DOGoto(UnityEngine.Component,System.Single,System.Boolean)
extern "C"  int32_t ShortcutExtensions_DOGoto_m943344649 (Il2CppObject * __this /* static, unused */, Component_t2126946602 * ___target0, float ___to1, bool ___andPlay2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.ShortcutExtensions::DOGoto(UnityEngine.Material,System.Single,System.Boolean)
extern "C"  int32_t ShortcutExtensions_DOGoto_m1570683993 (Il2CppObject * __this /* static, unused */, Material_t1886596500 * ___target0, float ___to1, bool ___andPlay2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.ShortcutExtensions::DOPause(UnityEngine.Component)
extern "C"  int32_t ShortcutExtensions_DOPause_m1516910574 (Il2CppObject * __this /* static, unused */, Component_t2126946602 * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.ShortcutExtensions::DOPause(UnityEngine.Material)
extern "C"  int32_t ShortcutExtensions_DOPause_m3376897664 (Il2CppObject * __this /* static, unused */, Material_t1886596500 * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.ShortcutExtensions::DOPlay(UnityEngine.Component)
extern "C"  int32_t ShortcutExtensions_DOPlay_m355575646 (Il2CppObject * __this /* static, unused */, Component_t2126946602 * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.ShortcutExtensions::DOPlay(UnityEngine.Material)
extern "C"  int32_t ShortcutExtensions_DOPlay_m152846608 (Il2CppObject * __this /* static, unused */, Material_t1886596500 * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.ShortcutExtensions::DOPlayBackwards(UnityEngine.Component)
extern "C"  int32_t ShortcutExtensions_DOPlayBackwards_m1201695240 (Il2CppObject * __this /* static, unused */, Component_t2126946602 * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.ShortcutExtensions::DOPlayBackwards(UnityEngine.Material)
extern "C"  int32_t ShortcutExtensions_DOPlayBackwards_m1842708774 (Il2CppObject * __this /* static, unused */, Material_t1886596500 * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.ShortcutExtensions::DOPlayForward(UnityEngine.Component)
extern "C"  int32_t ShortcutExtensions_DOPlayForward_m1362085843 (Il2CppObject * __this /* static, unused */, Component_t2126946602 * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.ShortcutExtensions::DOPlayForward(UnityEngine.Material)
extern "C"  int32_t ShortcutExtensions_DOPlayForward_m323862011 (Il2CppObject * __this /* static, unused */, Material_t1886596500 * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.ShortcutExtensions::DORestart(UnityEngine.Component,System.Boolean)
extern "C"  int32_t ShortcutExtensions_DORestart_m10899816 (Il2CppObject * __this /* static, unused */, Component_t2126946602 * ___target0, bool ___includeDelay1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.ShortcutExtensions::DORestart(UnityEngine.Material,System.Boolean)
extern "C"  int32_t ShortcutExtensions_DORestart_m3473047108 (Il2CppObject * __this /* static, unused */, Material_t1886596500 * ___target0, bool ___includeDelay1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.ShortcutExtensions::DORewind(UnityEngine.Component,System.Boolean)
extern "C"  int32_t ShortcutExtensions_DORewind_m1870679654 (Il2CppObject * __this /* static, unused */, Component_t2126946602 * ___target0, bool ___includeDelay1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.ShortcutExtensions::DORewind(UnityEngine.Material,System.Boolean)
extern "C"  int32_t ShortcutExtensions_DORewind_m3533040006 (Il2CppObject * __this /* static, unused */, Material_t1886596500 * ___target0, bool ___includeDelay1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.ShortcutExtensions::DOSmoothRewind(UnityEngine.Component)
extern "C"  int32_t ShortcutExtensions_DOSmoothRewind_m3104319465 (Il2CppObject * __this /* static, unused */, Component_t2126946602 * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.ShortcutExtensions::DOSmoothRewind(UnityEngine.Material)
extern "C"  int32_t ShortcutExtensions_DOSmoothRewind_m1904083749 (Il2CppObject * __this /* static, unused */, Material_t1886596500 * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.ShortcutExtensions::DOTogglePause(UnityEngine.Component)
extern "C"  int32_t ShortcutExtensions_DOTogglePause_m2968384162 (Il2CppObject * __this /* static, unused */, Component_t2126946602 * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.ShortcutExtensions::DOTogglePause(UnityEngine.Material)
extern "C"  int32_t ShortcutExtensions_DOTogglePause_m2730982732 (Il2CppObject * __this /* static, unused */, Material_t1886596500 * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
