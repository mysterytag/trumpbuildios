﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper1445439458MethodDeclarations.h"

// System.Void UniRx.ReactiveProperty`1<UnityEngine.GameObject>::.ctor()
#define ReactiveProperty_1__ctor_m1353912682(__this, method) ((  void (*) (ReactiveProperty_1_t326060844 *, const MethodInfo*))ReactiveProperty_1__ctor_m2182085490_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.GameObject>::.ctor(T)
#define ReactiveProperty_1__ctor_m626024704(__this, ___initialValue0, method) ((  void (*) (ReactiveProperty_1_t326060844 *, GameObject_t4012695102 *, const MethodInfo*))ReactiveProperty_1__ctor_m3220142124_gshared)(__this, ___initialValue0, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.GameObject>::.ctor(UniRx.IObservable`1<T>)
#define ReactiveProperty_1__ctor_m3876684669(__this, ___source0, method) ((  void (*) (ReactiveProperty_1_t326060844 *, Il2CppObject*, const MethodInfo*))ReactiveProperty_1__ctor_m2559762385_gshared)(__this, ___source0, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.GameObject>::.ctor(UniRx.IObservable`1<T>,T)
#define ReactiveProperty_1__ctor_m1757326805(__this, ___source0, ___initialValue1, method) ((  void (*) (ReactiveProperty_1_t326060844 *, Il2CppObject*, GameObject_t4012695102 *, const MethodInfo*))ReactiveProperty_1__ctor_m3210364201_gshared)(__this, ___source0, ___initialValue1, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.GameObject>::.cctor()
#define ReactiveProperty_1__cctor_m143927119(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ReactiveProperty_1__cctor_m2738044539_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1<UnityEngine.GameObject>::get_EqualityComparer()
#define ReactiveProperty_1_get_EqualityComparer_m2667521301(__this, method) ((  Il2CppObject* (*) (ReactiveProperty_1_t326060844 *, const MethodInfo*))ReactiveProperty_1_get_EqualityComparer_m3290095395_gshared)(__this, method)
// T UniRx.ReactiveProperty`1<UnityEngine.GameObject>::get_Value()
#define ReactiveProperty_1_get_Value_m1233028837(__this, method) ((  GameObject_t4012695102 * (*) (ReactiveProperty_1_t326060844 *, const MethodInfo*))ReactiveProperty_1_get_Value_m2793108311_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.GameObject>::set_Value(T)
#define ReactiveProperty_1_set_Value_m3834643790(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t326060844 *, GameObject_t4012695102 *, const MethodInfo*))ReactiveProperty_1_set_Value_m1580705402_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.GameObject>::SetValue(T)
#define ReactiveProperty_1_SetValue_m2149047977(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t326060844 *, GameObject_t4012695102 *, const MethodInfo*))ReactiveProperty_1_SetValue_m4154550269_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.GameObject>::SetValueAndForceNotify(T)
#define ReactiveProperty_1_SetValueAndForceNotify_m2610530476(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t326060844 *, GameObject_t4012695102 *, const MethodInfo*))ReactiveProperty_1_SetValueAndForceNotify_m2537434880_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.ReactiveProperty`1<UnityEngine.GameObject>::Subscribe(UniRx.IObserver`1<T>)
#define ReactiveProperty_1_Subscribe_m3090479325(__this, ___observer0, method) ((  Il2CppObject * (*) (ReactiveProperty_1_t326060844 *, Il2CppObject*, const MethodInfo*))ReactiveProperty_1_Subscribe_m958004807_gshared)(__this, ___observer0, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.GameObject>::Dispose()
#define ReactiveProperty_1_Dispose_m2125137115(__this, method) ((  void (*) (ReactiveProperty_1_t326060844 *, const MethodInfo*))ReactiveProperty_1_Dispose_m938398511_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.GameObject>::Dispose(System.Boolean)
#define ReactiveProperty_1_Dispose_m1177460498(__this, ___disposing0, method) ((  void (*) (ReactiveProperty_1_t326060844 *, bool, const MethodInfo*))ReactiveProperty_1_Dispose_m431016550_gshared)(__this, ___disposing0, method)
// System.String UniRx.ReactiveProperty`1<UnityEngine.GameObject>::ToString()
#define ReactiveProperty_1_ToString_m1338826485(__this, method) ((  String_t* (*) (ReactiveProperty_1_t326060844 *, const MethodInfo*))ReactiveProperty_1_ToString_m2722346619_gshared)(__this, method)
// System.Boolean UniRx.ReactiveProperty`1<UnityEngine.GameObject>::IsRequiredSubscribeOnCurrentThread()
#define ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m3555669285(__this, method) ((  bool (*) (ReactiveProperty_1_t326060844 *, const MethodInfo*))ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m3797922201_gshared)(__this, method)
