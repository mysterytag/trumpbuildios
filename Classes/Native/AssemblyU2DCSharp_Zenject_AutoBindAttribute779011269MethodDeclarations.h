﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.AutoBindAttribute
struct AutoBindAttribute_t779011269;

#include "codegen/il2cpp-codegen.h"

// System.Void Zenject.AutoBindAttribute::.ctor()
extern "C"  void AutoBindAttribute__ctor_m178980730 (AutoBindAttribute_t779011269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
