﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.Rect>
struct List_1_t2322387786;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Rect>
struct IEnumerable_1_t102615877;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Rect>
struct IEnumerator_1_t3008535265;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<UnityEngine.Rect>
struct ICollection_1_t1991260203;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rect>
struct ReadOnlyCollection_1_t393606869;
// UnityEngine.Rect[]
struct RectU5BU5D_t1056984396;
// System.Predicate`1<UnityEngine.Rect>
struct Predicate_1_t2096392715;
// System.Action`1<UnityEngine.Rect>
struct Action_1_t1673881522;
// System.Collections.Generic.IComparer`1<UnityEngine.Rect>
struct IComparer_1_t4225136226;
// System.Comparison`1<UnityEngine.Rect>
struct Comparison_1_t4229103693;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Object837106420.h"
#include "UnityEngine_UnityEngine_Rect1525428817.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat408170778.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Rect>::.ctor()
extern "C"  void List_1__ctor_m4283677583_gshared (List_1_t2322387786 * __this, const MethodInfo* method);
#define List_1__ctor_m4283677583(__this, method) ((  void (*) (List_1_t2322387786 *, const MethodInfo*))List_1__ctor_m4283677583_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rect>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m672804240_gshared (List_1_t2322387786 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m672804240(__this, ___collection0, method) ((  void (*) (List_1_t2322387786 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m672804240_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rect>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m2723254880_gshared (List_1_t2322387786 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m2723254880(__this, ___capacity0, method) ((  void (*) (List_1_t2322387786 *, int32_t, const MethodInfo*))List_1__ctor_m2723254880_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rect>::.cctor()
extern "C"  void List_1__cctor_m3462889982_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m3462889982(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3462889982_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Rect>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m498817497_gshared (List_1_t2322387786 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m498817497(__this, method) ((  Il2CppObject* (*) (List_1_t2322387786 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m498817497_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rect>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m330149013_gshared (List_1_t2322387786 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m330149013(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2322387786 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m330149013_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Rect>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m829549540_gshared (List_1_t2322387786 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m829549540(__this, method) ((  Il2CppObject * (*) (List_1_t2322387786 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m829549540_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rect>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m1080389849_gshared (List_1_t2322387786 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m1080389849(__this, ___item0, method) ((  int32_t (*) (List_1_t2322387786 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m1080389849_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Rect>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m1448002055_gshared (List_1_t2322387786 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1448002055(__this, ___item0, method) ((  bool (*) (List_1_t2322387786 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1448002055_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rect>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m1801425585_gshared (List_1_t2322387786 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m1801425585(__this, ___item0, method) ((  int32_t (*) (List_1_t2322387786 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1801425585_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rect>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m960171684_gshared (List_1_t2322387786 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m960171684(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2322387786 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m960171684_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rect>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m3541147652_gshared (List_1_t2322387786 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m3541147652(__this, ___item0, method) ((  void (*) (List_1_t2322387786 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3541147652_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Rect>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1311839560_gshared (List_1_t2322387786 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1311839560(__this, method) ((  bool (*) (List_1_t2322387786 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1311839560_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Rect>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m3067642869_gshared (List_1_t2322387786 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m3067642869(__this, method) ((  bool (*) (List_1_t2322387786 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3067642869_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Rect>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m4247270951_gshared (List_1_t2322387786 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m4247270951(__this, method) ((  Il2CppObject * (*) (List_1_t2322387786 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m4247270951_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Rect>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m3740680886_gshared (List_1_t2322387786 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m3740680886(__this, method) ((  bool (*) (List_1_t2322387786 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m3740680886_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Rect>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m990317955_gshared (List_1_t2322387786 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m990317955(__this, method) ((  bool (*) (List_1_t2322387786 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m990317955_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Rect>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m2474064366_gshared (List_1_t2322387786 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m2474064366(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t2322387786 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m2474064366_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rect>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m4215939451_gshared (List_1_t2322387786 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m4215939451(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2322387786 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m4215939451_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rect>::Add(T)
extern "C"  void List_1_Add_m128325392_gshared (List_1_t2322387786 * __this, Rect_t1525428817  ___item0, const MethodInfo* method);
#define List_1_Add_m128325392(__this, ___item0, method) ((  void (*) (List_1_t2322387786 *, Rect_t1525428817 , const MethodInfo*))List_1_Add_m128325392_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rect>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m3958840651_gshared (List_1_t2322387786 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m3958840651(__this, ___newCount0, method) ((  void (*) (List_1_t2322387786 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m3958840651_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rect>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m3412559945_gshared (List_1_t2322387786 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m3412559945(__this, ___collection0, method) ((  void (*) (List_1_t2322387786 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m3412559945_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rect>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m2673532169_gshared (List_1_t2322387786 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m2673532169(__this, ___enumerable0, method) ((  void (*) (List_1_t2322387786 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2673532169_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rect>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m2949664750_gshared (List_1_t2322387786 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m2949664750(__this, ___collection0, method) ((  void (*) (List_1_t2322387786 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2949664750_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Rect>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t393606869 * List_1_AsReadOnly_m4038992215_gshared (List_1_t2322387786 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m4038992215(__this, method) ((  ReadOnlyCollection_1_t393606869 * (*) (List_1_t2322387786 *, const MethodInfo*))List_1_AsReadOnly_m4038992215_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rect>::Clear()
extern "C"  void List_1_Clear_m1689810874_gshared (List_1_t2322387786 * __this, const MethodInfo* method);
#define List_1_Clear_m1689810874(__this, method) ((  void (*) (List_1_t2322387786 *, const MethodInfo*))List_1_Clear_m1689810874_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Rect>::Contains(T)
extern "C"  bool List_1_Contains_m2611623596_gshared (List_1_t2322387786 * __this, Rect_t1525428817  ___item0, const MethodInfo* method);
#define List_1_Contains_m2611623596(__this, ___item0, method) ((  bool (*) (List_1_t2322387786 *, Rect_t1525428817 , const MethodInfo*))List_1_Contains_m2611623596_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rect>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m2696567552_gshared (List_1_t2322387786 * __this, RectU5BU5D_t1056984396* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m2696567552(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2322387786 *, RectU5BU5D_t1056984396*, int32_t, const MethodInfo*))List_1_CopyTo_m2696567552_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<UnityEngine.Rect>::Find(System.Predicate`1<T>)
extern "C"  Rect_t1525428817  List_1_Find_m3092620806_gshared (List_1_t2322387786 * __this, Predicate_1_t2096392715 * ___match0, const MethodInfo* method);
#define List_1_Find_m3092620806(__this, ___match0, method) ((  Rect_t1525428817  (*) (List_1_t2322387786 *, Predicate_1_t2096392715 *, const MethodInfo*))List_1_Find_m3092620806_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rect>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m436085731_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t2096392715 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m436085731(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t2096392715 *, const MethodInfo*))List_1_CheckMatch_m436085731_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rect>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m1057233600_gshared (List_1_t2322387786 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t2096392715 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m1057233600(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t2322387786 *, int32_t, int32_t, Predicate_1_t2096392715 *, const MethodInfo*))List_1_GetIndex_m1057233600_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rect>::ForEach(System.Action`1<T>)
extern "C"  void List_1_ForEach_m1248748951_gshared (List_1_t2322387786 * __this, Action_1_t1673881522 * ___action0, const MethodInfo* method);
#define List_1_ForEach_m1248748951(__this, ___action0, method) ((  void (*) (List_1_t2322387786 *, Action_1_t1673881522 *, const MethodInfo*))List_1_ForEach_m1248748951_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Rect>::GetEnumerator()
extern "C"  Enumerator_t408170778  List_1_GetEnumerator_m3783573993_gshared (List_1_t2322387786 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m3783573993(__this, method) ((  Enumerator_t408170778  (*) (List_1_t2322387786 *, const MethodInfo*))List_1_GetEnumerator_m3783573993_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rect>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m3672723148_gshared (List_1_t2322387786 * __this, Rect_t1525428817  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m3672723148(__this, ___item0, method) ((  int32_t (*) (List_1_t2322387786 *, Rect_t1525428817 , const MethodInfo*))List_1_IndexOf_m3672723148_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rect>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m109523287_gshared (List_1_t2322387786 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m109523287(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t2322387786 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m109523287_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rect>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m2442934736_gshared (List_1_t2322387786 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m2442934736(__this, ___index0, method) ((  void (*) (List_1_t2322387786 *, int32_t, const MethodInfo*))List_1_CheckIndex_m2442934736_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rect>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m2268279159_gshared (List_1_t2322387786 * __this, int32_t ___index0, Rect_t1525428817  ___item1, const MethodInfo* method);
#define List_1_Insert_m2268279159(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2322387786 *, int32_t, Rect_t1525428817 , const MethodInfo*))List_1_Insert_m2268279159_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rect>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m2936728748_gshared (List_1_t2322387786 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m2936728748(__this, ___collection0, method) ((  void (*) (List_1_t2322387786 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2936728748_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Rect>::Remove(T)
extern "C"  bool List_1_Remove_m1830072743_gshared (List_1_t2322387786 * __this, Rect_t1525428817  ___item0, const MethodInfo* method);
#define List_1_Remove_m1830072743(__this, ___item0, method) ((  bool (*) (List_1_t2322387786 *, Rect_t1525428817 , const MethodInfo*))List_1_Remove_m1830072743_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rect>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m1333507471_gshared (List_1_t2322387786 * __this, Predicate_1_t2096392715 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m1333507471(__this, ___match0, method) ((  int32_t (*) (List_1_t2322387786 *, Predicate_1_t2096392715 *, const MethodInfo*))List_1_RemoveAll_m1333507471_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rect>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m142132029_gshared (List_1_t2322387786 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m142132029(__this, ___index0, method) ((  void (*) (List_1_t2322387786 *, int32_t, const MethodInfo*))List_1_RemoveAt_m142132029_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rect>::Reverse()
extern "C"  void List_1_Reverse_m2073298959_gshared (List_1_t2322387786 * __this, const MethodInfo* method);
#define List_1_Reverse_m2073298959(__this, method) ((  void (*) (List_1_t2322387786 *, const MethodInfo*))List_1_Reverse_m2073298959_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rect>::Sort()
extern "C"  void List_1_Sort_m654297587_gshared (List_1_t2322387786 * __this, const MethodInfo* method);
#define List_1_Sort_m654297587(__this, method) ((  void (*) (List_1_t2322387786 *, const MethodInfo*))List_1_Sort_m654297587_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rect>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m2404190545_gshared (List_1_t2322387786 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m2404190545(__this, ___comparer0, method) ((  void (*) (List_1_t2322387786 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m2404190545_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rect>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m1437437446_gshared (List_1_t2322387786 * __this, Comparison_1_t4229103693 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m1437437446(__this, ___comparison0, method) ((  void (*) (List_1_t2322387786 *, Comparison_1_t4229103693 *, const MethodInfo*))List_1_Sort_m1437437446_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Rect>::ToArray()
extern "C"  RectU5BU5D_t1056984396* List_1_ToArray_m2380859816_gshared (List_1_t2322387786 * __this, const MethodInfo* method);
#define List_1_ToArray_m2380859816(__this, method) ((  RectU5BU5D_t1056984396* (*) (List_1_t2322387786 *, const MethodInfo*))List_1_ToArray_m2380859816_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rect>::TrimExcess()
extern "C"  void List_1_TrimExcess_m2399037260_gshared (List_1_t2322387786 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m2399037260(__this, method) ((  void (*) (List_1_t2322387786 *, const MethodInfo*))List_1_TrimExcess_m2399037260_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rect>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m1961689340_gshared (List_1_t2322387786 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1961689340(__this, method) ((  int32_t (*) (List_1_t2322387786 *, const MethodInfo*))List_1_get_Capacity_m1961689340_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rect>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m1169029469_gshared (List_1_t2322387786 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m1169029469(__this, ___value0, method) ((  void (*) (List_1_t2322387786 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1169029469_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rect>::get_Count()
extern "C"  int32_t List_1_get_Count_m2002575407_gshared (List_1_t2322387786 * __this, const MethodInfo* method);
#define List_1_get_Count_m2002575407(__this, method) ((  int32_t (*) (List_1_t2322387786 *, const MethodInfo*))List_1_get_Count_m2002575407_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Rect>::get_Item(System.Int32)
extern "C"  Rect_t1525428817  List_1_get_Item_m2496309475_gshared (List_1_t2322387786 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m2496309475(__this, ___index0, method) ((  Rect_t1525428817  (*) (List_1_t2322387786 *, int32_t, const MethodInfo*))List_1_get_Item_m2496309475_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rect>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m4077449358_gshared (List_1_t2322387786 * __this, int32_t ___index0, Rect_t1525428817  ___value1, const MethodInfo* method);
#define List_1_set_Item_m4077449358(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2322387786 *, int32_t, Rect_t1525428817 , const MethodInfo*))List_1_set_Item_m4077449358_gshared)(__this, ___index0, ___value1, method)
