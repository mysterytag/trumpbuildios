﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/AttributeInstallResponseCallback
struct AttributeInstallResponseCallback_t598515013;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.AttributeInstallRequest
struct AttributeInstallRequest_t4240476704;
// PlayFab.ClientModels.AttributeInstallResult
struct AttributeInstallResult_t1834505356;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_AttributeIn4240476704.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_AttributeIn1834505356.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/AttributeInstallResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void AttributeInstallResponseCallback__ctor_m2115477428 (AttributeInstallResponseCallback_t598515013 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/AttributeInstallResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.AttributeInstallRequest,PlayFab.ClientModels.AttributeInstallResult,PlayFab.PlayFabError,System.Object)
extern "C"  void AttributeInstallResponseCallback_Invoke_m3837496097 (AttributeInstallResponseCallback_t598515013 * __this, String_t* ___urlPath0, int32_t ___callId1, AttributeInstallRequest_t4240476704 * ___request2, AttributeInstallResult_t1834505356 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_AttributeInstallResponseCallback_t598515013(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, AttributeInstallRequest_t4240476704 * ___request2, AttributeInstallResult_t1834505356 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/AttributeInstallResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.AttributeInstallRequest,PlayFab.ClientModels.AttributeInstallResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * AttributeInstallResponseCallback_BeginInvoke_m614617102 (AttributeInstallResponseCallback_t598515013 * __this, String_t* ___urlPath0, int32_t ___callId1, AttributeInstallRequest_t4240476704 * ___request2, AttributeInstallResult_t1834505356 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/AttributeInstallResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void AttributeInstallResponseCallback_EndInvoke_m2046233540 (AttributeInstallResponseCallback_t598515013 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
