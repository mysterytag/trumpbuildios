﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.StableCompositeDisposable/NAryUnsafe
struct NAryUnsafe_t3995625856;
// System.IDisposable[]
struct IDisposableU5BU5D_t165183403;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.StableCompositeDisposable/NAryUnsafe::.ctor(System.IDisposable[])
extern "C"  void NAryUnsafe__ctor_m939567400 (NAryUnsafe_t3995625856 * __this, IDisposableU5BU5D_t165183403* ___disposables0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.StableCompositeDisposable/NAryUnsafe::get_IsDisposed()
extern "C"  bool NAryUnsafe_get_IsDisposed_m2789276196 (NAryUnsafe_t3995625856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.StableCompositeDisposable/NAryUnsafe::Dispose()
extern "C"  void NAryUnsafe_Dispose_m529757361 (NAryUnsafe_t3995625856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
