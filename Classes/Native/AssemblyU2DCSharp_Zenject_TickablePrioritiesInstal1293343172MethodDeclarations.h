﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.TickablePrioritiesInstaller
struct TickablePrioritiesInstaller_t1293343172;
// System.Collections.Generic.List`1<System.Type>
struct List_1_t3576188904;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449.h"
#include "mscorlib_System_Type2779229935.h"

// System.Void Zenject.TickablePrioritiesInstaller::.ctor(System.Collections.Generic.List`1<System.Type>)
extern "C"  void TickablePrioritiesInstaller__ctor_m1580647544 (TickablePrioritiesInstaller_t1293343172 * __this, List_1_t3576188904 * ___tickables0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.TickablePrioritiesInstaller::InstallBindings()
extern "C"  void TickablePrioritiesInstaller_InstallBindings_m4097492674 (TickablePrioritiesInstaller_t1293343172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.TickablePrioritiesInstaller::BindPriority(Zenject.DiContainer,System.Type,System.Int32)
extern "C"  void TickablePrioritiesInstaller_BindPriority_m1294937209 (Il2CppObject * __this /* static, unused */, DiContainer_t2383114449 * ___container0, Type_t * ___tickableType1, int32_t ___priorityCount2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
