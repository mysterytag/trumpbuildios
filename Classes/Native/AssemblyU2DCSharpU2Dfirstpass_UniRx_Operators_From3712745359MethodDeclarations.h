﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.FromCoroutine`1/FromCoroutineObserver<System.Double>
struct FromCoroutineObserver_t3712745359;
// UniRx.IObserver`1<System.Double>
struct IObserver_1_t2746515517;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.FromCoroutine`1/FromCoroutineObserver<System.Double>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void FromCoroutineObserver__ctor_m3431534472_gshared (FromCoroutineObserver_t3712745359 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define FromCoroutineObserver__ctor_m3431534472(__this, ___observer0, ___cancel1, method) ((  void (*) (FromCoroutineObserver_t3712745359 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))FromCoroutineObserver__ctor_m3431534472_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.FromCoroutine`1/FromCoroutineObserver<System.Double>::OnNext(T)
extern "C"  void FromCoroutineObserver_OnNext_m2249157386_gshared (FromCoroutineObserver_t3712745359 * __this, double ___value0, const MethodInfo* method);
#define FromCoroutineObserver_OnNext_m2249157386(__this, ___value0, method) ((  void (*) (FromCoroutineObserver_t3712745359 *, double, const MethodInfo*))FromCoroutineObserver_OnNext_m2249157386_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.FromCoroutine`1/FromCoroutineObserver<System.Double>::OnError(System.Exception)
extern "C"  void FromCoroutineObserver_OnError_m2579509785_gshared (FromCoroutineObserver_t3712745359 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define FromCoroutineObserver_OnError_m2579509785(__this, ___error0, method) ((  void (*) (FromCoroutineObserver_t3712745359 *, Exception_t1967233988 *, const MethodInfo*))FromCoroutineObserver_OnError_m2579509785_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.FromCoroutine`1/FromCoroutineObserver<System.Double>::OnCompleted()
extern "C"  void FromCoroutineObserver_OnCompleted_m536773356_gshared (FromCoroutineObserver_t3712745359 * __this, const MethodInfo* method);
#define FromCoroutineObserver_OnCompleted_m536773356(__this, method) ((  void (*) (FromCoroutineObserver_t3712745359 *, const MethodInfo*))FromCoroutineObserver_OnCompleted_m536773356_gshared)(__this, method)
