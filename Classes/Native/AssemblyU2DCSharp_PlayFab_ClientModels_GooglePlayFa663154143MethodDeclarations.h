﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GooglePlayFabIdPair
struct GooglePlayFabIdPair_t663154143;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.GooglePlayFabIdPair::.ctor()
extern "C"  void GooglePlayFabIdPair__ctor_m3814750634 (GooglePlayFabIdPair_t663154143 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GooglePlayFabIdPair::get_GoogleId()
extern "C"  String_t* GooglePlayFabIdPair_get_GoogleId_m3102549818 (GooglePlayFabIdPair_t663154143 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GooglePlayFabIdPair::set_GoogleId(System.String)
extern "C"  void GooglePlayFabIdPair_set_GoogleId_m3425473367 (GooglePlayFabIdPair_t663154143 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GooglePlayFabIdPair::get_PlayFabId()
extern "C"  String_t* GooglePlayFabIdPair_get_PlayFabId_m374440906 (GooglePlayFabIdPair_t663154143 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GooglePlayFabIdPair::set_PlayFabId(System.String)
extern "C"  void GooglePlayFabIdPair_set_PlayFabId_m3881742345 (GooglePlayFabIdPair_t663154143 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
