﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen2958521481.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_MatchmakeStat72483573.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Nullable`1<PlayFab.ClientModels.MatchmakeStatus>::.ctor(T)
extern "C"  void Nullable_1__ctor_m1412580726_gshared (Nullable_1_t2958521481 * __this, int32_t ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m1412580726(__this, ___value0, method) ((  void (*) (Nullable_1_t2958521481 *, int32_t, const MethodInfo*))Nullable_1__ctor_m1412580726_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<PlayFab.ClientModels.MatchmakeStatus>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m512914650_gshared (Nullable_1_t2958521481 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m512914650(__this, method) ((  bool (*) (Nullable_1_t2958521481 *, const MethodInfo*))Nullable_1_get_HasValue_m512914650_gshared)(__this, method)
// T System.Nullable`1<PlayFab.ClientModels.MatchmakeStatus>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m2157806605_gshared (Nullable_1_t2958521481 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m2157806605(__this, method) ((  int32_t (*) (Nullable_1_t2958521481 *, const MethodInfo*))Nullable_1_get_Value_m2157806605_gshared)(__this, method)
// System.Boolean System.Nullable`1<PlayFab.ClientModels.MatchmakeStatus>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m4093494363_gshared (Nullable_1_t2958521481 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m4093494363(__this, ___other0, method) ((  bool (*) (Nullable_1_t2958521481 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m4093494363_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<PlayFab.ClientModels.MatchmakeStatus>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2890547844_gshared (Nullable_1_t2958521481 * __this, Nullable_1_t2958521481  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m2890547844(__this, ___other0, method) ((  bool (*) (Nullable_1_t2958521481 *, Nullable_1_t2958521481 , const MethodInfo*))Nullable_1_Equals_m2890547844_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<PlayFab.ClientModels.MatchmakeStatus>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1302145715_gshared (Nullable_1_t2958521481 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m1302145715(__this, method) ((  int32_t (*) (Nullable_1_t2958521481 *, const MethodInfo*))Nullable_1_GetHashCode_m1302145715_gshared)(__this, method)
// T System.Nullable`1<PlayFab.ClientModels.MatchmakeStatus>::GetValueOrDefault()
extern "C"  int32_t Nullable_1_GetValueOrDefault_m1776500584_gshared (Nullable_1_t2958521481 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m1776500584(__this, method) ((  int32_t (*) (Nullable_1_t2958521481 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m1776500584_gshared)(__this, method)
// System.String System.Nullable`1<PlayFab.ClientModels.MatchmakeStatus>::ToString()
extern "C"  String_t* Nullable_1_ToString_m2740961925_gshared (Nullable_1_t2958521481 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m2740961925(__this, method) ((  String_t* (*) (Nullable_1_t2958521481 *, const MethodInfo*))Nullable_1_ToString_m2740961925_gshared)(__this, method)
// System.Nullable`1<T> System.Nullable`1<PlayFab.ClientModels.MatchmakeStatus>::op_Implicit(T)
extern "C"  Nullable_1_t2958521481  Nullable_1_op_Implicit_m3043354751_gshared (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method);
#define Nullable_1_op_Implicit_m3043354751(__this /* static, unused */, ___value0, method) ((  Nullable_1_t2958521481  (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))Nullable_1_op_Implicit_m3043354751_gshared)(__this /* static, unused */, ___value0, method)
