﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Action>>
struct List_1_t2031441885;
// System.Collections.Generic.List`1<ITransactionable>
struct List_1_t2449041064;
// System.Collections.Generic.List`1<ICell>
struct List_1_t866472516;
// System.Action`1<System.Int32>
struct Action_1_t2995867492;
// System.Func`3<System.Int32,System.Int32,System.Int32>
struct Func_3_t543102568;
// System.Action`1<System.String>
struct Action_1_t1116941607;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Transaction
struct  Transaction_t3809114814  : public Il2CppObject
{
public:

public:
};

struct Transaction_t3809114814_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Action>> Transaction::actionQueue
	List_1_t2031441885 * ___actionQueue_1;
	// System.Collections.Generic.List`1<ITransactionable> Transaction::registered
	List_1_t2449041064 * ___registered_2;
	// System.Boolean Transaction::hold
	bool ___hold_3;
	// System.Boolean Transaction::actionQueueDirty
	bool ___actionQueueDirty_4;
	// System.Boolean Transaction::performing
	bool ___performing_5;
	// System.Int32 Transaction::currentPriorityUnpack
	int32_t ___currentPriorityUnpack_6;
	// System.Collections.Generic.List`1<ICell> Transaction::touched
	List_1_t866472516 * ___touched_7;
	// System.Boolean Transaction::calculationMode
	bool ___calculationMode_8;
	// System.Action`1<System.Int32> Transaction::<>f__am$cache8
	Action_1_t2995867492 * ___U3CU3Ef__amU24cache8_9;
	// System.Action`1<System.Int32> Transaction::<>f__am$cache9
	Action_1_t2995867492 * ___U3CU3Ef__amU24cache9_10;
	// System.Action`1<System.Int32> Transaction::<>f__am$cacheA
	Action_1_t2995867492 * ___U3CU3Ef__amU24cacheA_11;
	// System.Action`1<System.Int32> Transaction::<>f__am$cacheB
	Action_1_t2995867492 * ___U3CU3Ef__amU24cacheB_12;
	// System.Action`1<System.Int32> Transaction::<>f__am$cacheC
	Action_1_t2995867492 * ___U3CU3Ef__amU24cacheC_13;
	// System.Action`1<System.Int32> Transaction::<>f__am$cacheD
	Action_1_t2995867492 * ___U3CU3Ef__amU24cacheD_14;
	// System.Func`3<System.Int32,System.Int32,System.Int32> Transaction::<>f__am$cacheE
	Func_3_t543102568 * ___U3CU3Ef__amU24cacheE_15;
	// System.Action`1<System.Int32> Transaction::<>f__am$cacheF
	Action_1_t2995867492 * ___U3CU3Ef__amU24cacheF_16;
	// System.Action`1<System.String> Transaction::<>f__am$cache10
	Action_1_t1116941607 * ___U3CU3Ef__amU24cache10_17;

public:
	inline static int32_t get_offset_of_actionQueue_1() { return static_cast<int32_t>(offsetof(Transaction_t3809114814_StaticFields, ___actionQueue_1)); }
	inline List_1_t2031441885 * get_actionQueue_1() const { return ___actionQueue_1; }
	inline List_1_t2031441885 ** get_address_of_actionQueue_1() { return &___actionQueue_1; }
	inline void set_actionQueue_1(List_1_t2031441885 * value)
	{
		___actionQueue_1 = value;
		Il2CppCodeGenWriteBarrier(&___actionQueue_1, value);
	}

	inline static int32_t get_offset_of_registered_2() { return static_cast<int32_t>(offsetof(Transaction_t3809114814_StaticFields, ___registered_2)); }
	inline List_1_t2449041064 * get_registered_2() const { return ___registered_2; }
	inline List_1_t2449041064 ** get_address_of_registered_2() { return &___registered_2; }
	inline void set_registered_2(List_1_t2449041064 * value)
	{
		___registered_2 = value;
		Il2CppCodeGenWriteBarrier(&___registered_2, value);
	}

	inline static int32_t get_offset_of_hold_3() { return static_cast<int32_t>(offsetof(Transaction_t3809114814_StaticFields, ___hold_3)); }
	inline bool get_hold_3() const { return ___hold_3; }
	inline bool* get_address_of_hold_3() { return &___hold_3; }
	inline void set_hold_3(bool value)
	{
		___hold_3 = value;
	}

	inline static int32_t get_offset_of_actionQueueDirty_4() { return static_cast<int32_t>(offsetof(Transaction_t3809114814_StaticFields, ___actionQueueDirty_4)); }
	inline bool get_actionQueueDirty_4() const { return ___actionQueueDirty_4; }
	inline bool* get_address_of_actionQueueDirty_4() { return &___actionQueueDirty_4; }
	inline void set_actionQueueDirty_4(bool value)
	{
		___actionQueueDirty_4 = value;
	}

	inline static int32_t get_offset_of_performing_5() { return static_cast<int32_t>(offsetof(Transaction_t3809114814_StaticFields, ___performing_5)); }
	inline bool get_performing_5() const { return ___performing_5; }
	inline bool* get_address_of_performing_5() { return &___performing_5; }
	inline void set_performing_5(bool value)
	{
		___performing_5 = value;
	}

	inline static int32_t get_offset_of_currentPriorityUnpack_6() { return static_cast<int32_t>(offsetof(Transaction_t3809114814_StaticFields, ___currentPriorityUnpack_6)); }
	inline int32_t get_currentPriorityUnpack_6() const { return ___currentPriorityUnpack_6; }
	inline int32_t* get_address_of_currentPriorityUnpack_6() { return &___currentPriorityUnpack_6; }
	inline void set_currentPriorityUnpack_6(int32_t value)
	{
		___currentPriorityUnpack_6 = value;
	}

	inline static int32_t get_offset_of_touched_7() { return static_cast<int32_t>(offsetof(Transaction_t3809114814_StaticFields, ___touched_7)); }
	inline List_1_t866472516 * get_touched_7() const { return ___touched_7; }
	inline List_1_t866472516 ** get_address_of_touched_7() { return &___touched_7; }
	inline void set_touched_7(List_1_t866472516 * value)
	{
		___touched_7 = value;
		Il2CppCodeGenWriteBarrier(&___touched_7, value);
	}

	inline static int32_t get_offset_of_calculationMode_8() { return static_cast<int32_t>(offsetof(Transaction_t3809114814_StaticFields, ___calculationMode_8)); }
	inline bool get_calculationMode_8() const { return ___calculationMode_8; }
	inline bool* get_address_of_calculationMode_8() { return &___calculationMode_8; }
	inline void set_calculationMode_8(bool value)
	{
		___calculationMode_8 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache8_9() { return static_cast<int32_t>(offsetof(Transaction_t3809114814_StaticFields, ___U3CU3Ef__amU24cache8_9)); }
	inline Action_1_t2995867492 * get_U3CU3Ef__amU24cache8_9() const { return ___U3CU3Ef__amU24cache8_9; }
	inline Action_1_t2995867492 ** get_address_of_U3CU3Ef__amU24cache8_9() { return &___U3CU3Ef__amU24cache8_9; }
	inline void set_U3CU3Ef__amU24cache8_9(Action_1_t2995867492 * value)
	{
		___U3CU3Ef__amU24cache8_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache8_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache9_10() { return static_cast<int32_t>(offsetof(Transaction_t3809114814_StaticFields, ___U3CU3Ef__amU24cache9_10)); }
	inline Action_1_t2995867492 * get_U3CU3Ef__amU24cache9_10() const { return ___U3CU3Ef__amU24cache9_10; }
	inline Action_1_t2995867492 ** get_address_of_U3CU3Ef__amU24cache9_10() { return &___U3CU3Ef__amU24cache9_10; }
	inline void set_U3CU3Ef__amU24cache9_10(Action_1_t2995867492 * value)
	{
		___U3CU3Ef__amU24cache9_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache9_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheA_11() { return static_cast<int32_t>(offsetof(Transaction_t3809114814_StaticFields, ___U3CU3Ef__amU24cacheA_11)); }
	inline Action_1_t2995867492 * get_U3CU3Ef__amU24cacheA_11() const { return ___U3CU3Ef__amU24cacheA_11; }
	inline Action_1_t2995867492 ** get_address_of_U3CU3Ef__amU24cacheA_11() { return &___U3CU3Ef__amU24cacheA_11; }
	inline void set_U3CU3Ef__amU24cacheA_11(Action_1_t2995867492 * value)
	{
		___U3CU3Ef__amU24cacheA_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheA_11, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_12() { return static_cast<int32_t>(offsetof(Transaction_t3809114814_StaticFields, ___U3CU3Ef__amU24cacheB_12)); }
	inline Action_1_t2995867492 * get_U3CU3Ef__amU24cacheB_12() const { return ___U3CU3Ef__amU24cacheB_12; }
	inline Action_1_t2995867492 ** get_address_of_U3CU3Ef__amU24cacheB_12() { return &___U3CU3Ef__amU24cacheB_12; }
	inline void set_U3CU3Ef__amU24cacheB_12(Action_1_t2995867492 * value)
	{
		___U3CU3Ef__amU24cacheB_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheB_12, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheC_13() { return static_cast<int32_t>(offsetof(Transaction_t3809114814_StaticFields, ___U3CU3Ef__amU24cacheC_13)); }
	inline Action_1_t2995867492 * get_U3CU3Ef__amU24cacheC_13() const { return ___U3CU3Ef__amU24cacheC_13; }
	inline Action_1_t2995867492 ** get_address_of_U3CU3Ef__amU24cacheC_13() { return &___U3CU3Ef__amU24cacheC_13; }
	inline void set_U3CU3Ef__amU24cacheC_13(Action_1_t2995867492 * value)
	{
		___U3CU3Ef__amU24cacheC_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheC_13, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheD_14() { return static_cast<int32_t>(offsetof(Transaction_t3809114814_StaticFields, ___U3CU3Ef__amU24cacheD_14)); }
	inline Action_1_t2995867492 * get_U3CU3Ef__amU24cacheD_14() const { return ___U3CU3Ef__amU24cacheD_14; }
	inline Action_1_t2995867492 ** get_address_of_U3CU3Ef__amU24cacheD_14() { return &___U3CU3Ef__amU24cacheD_14; }
	inline void set_U3CU3Ef__amU24cacheD_14(Action_1_t2995867492 * value)
	{
		___U3CU3Ef__amU24cacheD_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheD_14, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheE_15() { return static_cast<int32_t>(offsetof(Transaction_t3809114814_StaticFields, ___U3CU3Ef__amU24cacheE_15)); }
	inline Func_3_t543102568 * get_U3CU3Ef__amU24cacheE_15() const { return ___U3CU3Ef__amU24cacheE_15; }
	inline Func_3_t543102568 ** get_address_of_U3CU3Ef__amU24cacheE_15() { return &___U3CU3Ef__amU24cacheE_15; }
	inline void set_U3CU3Ef__amU24cacheE_15(Func_3_t543102568 * value)
	{
		___U3CU3Ef__amU24cacheE_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheE_15, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheF_16() { return static_cast<int32_t>(offsetof(Transaction_t3809114814_StaticFields, ___U3CU3Ef__amU24cacheF_16)); }
	inline Action_1_t2995867492 * get_U3CU3Ef__amU24cacheF_16() const { return ___U3CU3Ef__amU24cacheF_16; }
	inline Action_1_t2995867492 ** get_address_of_U3CU3Ef__amU24cacheF_16() { return &___U3CU3Ef__amU24cacheF_16; }
	inline void set_U3CU3Ef__amU24cacheF_16(Action_1_t2995867492 * value)
	{
		___U3CU3Ef__amU24cacheF_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheF_16, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache10_17() { return static_cast<int32_t>(offsetof(Transaction_t3809114814_StaticFields, ___U3CU3Ef__amU24cache10_17)); }
	inline Action_1_t1116941607 * get_U3CU3Ef__amU24cache10_17() const { return ___U3CU3Ef__amU24cache10_17; }
	inline Action_1_t1116941607 ** get_address_of_U3CU3Ef__amU24cache10_17() { return &___U3CU3Ef__amU24cache10_17; }
	inline void set_U3CU3Ef__amU24cache10_17(Action_1_t1116941607 * value)
	{
		___U3CU3Ef__amU24cache10_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache10_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
