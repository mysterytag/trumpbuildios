﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WindowManager/<recentCooldown>c__Iterator2C
struct U3CrecentCooldownU3Ec__Iterator2C_t2159820358;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void WindowManager/<recentCooldown>c__Iterator2C::.ctor()
extern "C"  void U3CrecentCooldownU3Ec__Iterator2C__ctor_m960823479 (U3CrecentCooldownU3Ec__Iterator2C_t2159820358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object WindowManager/<recentCooldown>c__Iterator2C::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CrecentCooldownU3Ec__Iterator2C_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4252872315 (U3CrecentCooldownU3Ec__Iterator2C_t2159820358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object WindowManager/<recentCooldown>c__Iterator2C::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CrecentCooldownU3Ec__Iterator2C_System_Collections_IEnumerator_get_Current_m3248097295 (U3CrecentCooldownU3Ec__Iterator2C_t2159820358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WindowManager/<recentCooldown>c__Iterator2C::MoveNext()
extern "C"  bool U3CrecentCooldownU3Ec__Iterator2C_MoveNext_m1313569757 (U3CrecentCooldownU3Ec__Iterator2C_t2159820358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WindowManager/<recentCooldown>c__Iterator2C::Dispose()
extern "C"  void U3CrecentCooldownU3Ec__Iterator2C_Dispose_m4126645044 (U3CrecentCooldownU3Ec__Iterator2C_t2159820358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WindowManager/<recentCooldown>c__Iterator2C::Reset()
extern "C"  void U3CrecentCooldownU3Ec__Iterator2C_Reset_m2902223716 (U3CrecentCooldownU3Ec__Iterator2C_t2159820358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
