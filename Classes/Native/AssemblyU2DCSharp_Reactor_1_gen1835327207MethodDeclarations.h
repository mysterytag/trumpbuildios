﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Reactor_1_gen96911316MethodDeclarations.h"

// System.Void Reactor`1<CollectionReplaceEvent`1<SettingsButton>>::.ctor()
#define Reactor_1__ctor_m2829780664(__this, method) ((  void (*) (Reactor_1_t1835327207 *, const MethodInfo*))Reactor_1__ctor_m1161914514_gshared)(__this, method)
// System.Void Reactor`1<CollectionReplaceEvent`1<SettingsButton>>::React(T)
#define Reactor_1_React_m1360549481(__this, ___t0, method) ((  void (*) (Reactor_1_t1835327207 *, CollectionReplaceEvent_1_t2575522311 *, const MethodInfo*))Reactor_1_React_m1196306383_gshared)(__this, ___t0, method)
// System.Void Reactor`1<CollectionReplaceEvent`1<SettingsButton>>::TransactionUnpackReact(T,System.Int32)
#define Reactor_1_TransactionUnpackReact_m2849958580(__this, ___t0, ___i1, method) ((  void (*) (Reactor_1_t1835327207 *, CollectionReplaceEvent_1_t2575522311 *, int32_t, const MethodInfo*))Reactor_1_TransactionUnpackReact_m1003670938_gshared)(__this, ___t0, ___i1, method)
// System.Boolean Reactor`1<CollectionReplaceEvent`1<SettingsButton>>::Empty()
#define Reactor_1_Empty_m4099054231(__this, method) ((  bool (*) (Reactor_1_t1835327207 *, const MethodInfo*))Reactor_1_Empty_m132443017_gshared)(__this, method)
// System.IDisposable Reactor`1<CollectionReplaceEvent`1<SettingsButton>>::AddReaction(System.Action`1<T>,Priority)
#define Reactor_1_AddReaction_m72902031(__this, ___reaction0, ___priority1, method) ((  Il2CppObject * (*) (Reactor_1_t1835327207 *, Action_1_t2723975016 *, int32_t, const MethodInfo*))Reactor_1_AddReaction_m1495770619_gshared)(__this, ___reaction0, ___priority1, method)
// System.Void Reactor`1<CollectionReplaceEvent`1<SettingsButton>>::UpgradeToMultyPriority()
#define Reactor_1_UpgradeToMultyPriority_m693613282(__this, method) ((  void (*) (Reactor_1_t1835327207 *, const MethodInfo*))Reactor_1_UpgradeToMultyPriority_m912916552_gshared)(__this, method)
// System.Void Reactor`1<CollectionReplaceEvent`1<SettingsButton>>::AddToMultipriority(System.Action`1<T>,Priority)
#define Reactor_1_AddToMultipriority_m2319730197(__this, ___reaction0, ___p1, method) ((  void (*) (Reactor_1_t1835327207 *, Action_1_t2723975016 *, int32_t, const MethodInfo*))Reactor_1_AddToMultipriority_m3381282287_gshared)(__this, ___reaction0, ___p1, method)
// System.Void Reactor`1<CollectionReplaceEvent`1<SettingsButton>>::RemoveReaction(System.Action`1<T>,Priority)
#define Reactor_1_RemoveReaction_m508670345(__this, ___reaction0, ___p1, method) ((  void (*) (Reactor_1_t1835327207 *, Action_1_t2723975016 *, int32_t, const MethodInfo*))Reactor_1_RemoveReaction_m4227182179_gshared)(__this, ___reaction0, ___p1, method)
// System.Void Reactor`1<CollectionReplaceEvent`1<SettingsButton>>::Clear()
#define Reactor_1_Clear_m235913955(__this, method) ((  void (*) (Reactor_1_t1835327207 *, const MethodInfo*))Reactor_1_Clear_m2863015101_gshared)(__this, method)
