﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ReactiveUI/<SetContent>c__AnonStorey122`1/<SetContent>c__AnonStorey123`1<System.Object>
struct U3CSetContentU3Ec__AnonStorey123_1_t573633800;
// System.Object
struct Il2CppObject;
// System.Func`2<System.Object,System.Object>
struct Func_2_t2135783352;

#include "codegen/il2cpp-codegen.h"

// System.Void ReactiveUI/<SetContent>c__AnonStorey122`1/<SetContent>c__AnonStorey123`1<System.Object>::.ctor()
extern "C"  void U3CSetContentU3Ec__AnonStorey123_1__ctor_m2136602973_gshared (U3CSetContentU3Ec__AnonStorey123_1_t573633800 * __this, const MethodInfo* method);
#define U3CSetContentU3Ec__AnonStorey123_1__ctor_m2136602973(__this, method) ((  void (*) (U3CSetContentU3Ec__AnonStorey123_1_t573633800 *, const MethodInfo*))U3CSetContentU3Ec__AnonStorey123_1__ctor_m2136602973_gshared)(__this, method)
// System.Object ReactiveUI/<SetContent>c__AnonStorey122`1/<SetContent>c__AnonStorey123`1<System.Object>::<>m__1B8(System.Func`2<T,System.Object>)
extern "C"  Il2CppObject * U3CSetContentU3Ec__AnonStorey123_1_U3CU3Em__1B8_m646068559_gshared (U3CSetContentU3Ec__AnonStorey123_1_t573633800 * __this, Func_2_t2135783352 * ___func0, const MethodInfo* method);
#define U3CSetContentU3Ec__AnonStorey123_1_U3CU3Em__1B8_m646068559(__this, ___func0, method) ((  Il2CppObject * (*) (U3CSetContentU3Ec__AnonStorey123_1_t573633800 *, Func_2_t2135783352 *, const MethodInfo*))U3CSetContentU3Ec__AnonStorey123_1_U3CU3Em__1B8_m646068559_gshared)(__this, ___func0, method)
