﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Queue`1<System.Action>
struct Queue_1_t2145611487;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.InternalUtil.AsyncLock
struct  AsyncLock_t4023816884  : public Il2CppObject
{
public:
	// System.Collections.Generic.Queue`1<System.Action> UniRx.InternalUtil.AsyncLock::queue
	Queue_1_t2145611487 * ___queue_0;
	// System.Boolean UniRx.InternalUtil.AsyncLock::isAcquired
	bool ___isAcquired_1;
	// System.Boolean UniRx.InternalUtil.AsyncLock::hasFaulted
	bool ___hasFaulted_2;

public:
	inline static int32_t get_offset_of_queue_0() { return static_cast<int32_t>(offsetof(AsyncLock_t4023816884, ___queue_0)); }
	inline Queue_1_t2145611487 * get_queue_0() const { return ___queue_0; }
	inline Queue_1_t2145611487 ** get_address_of_queue_0() { return &___queue_0; }
	inline void set_queue_0(Queue_1_t2145611487 * value)
	{
		___queue_0 = value;
		Il2CppCodeGenWriteBarrier(&___queue_0, value);
	}

	inline static int32_t get_offset_of_isAcquired_1() { return static_cast<int32_t>(offsetof(AsyncLock_t4023816884, ___isAcquired_1)); }
	inline bool get_isAcquired_1() const { return ___isAcquired_1; }
	inline bool* get_address_of_isAcquired_1() { return &___isAcquired_1; }
	inline void set_isAcquired_1(bool value)
	{
		___isAcquired_1 = value;
	}

	inline static int32_t get_offset_of_hasFaulted_2() { return static_cast<int32_t>(offsetof(AsyncLock_t4023816884, ___hasFaulted_2)); }
	inline bool get_hasFaulted_2() const { return ___hasFaulted_2; }
	inline bool* get_address_of_hasFaulted_2() { return &___hasFaulted_2; }
	inline void set_hasFaulted_2(bool value)
	{
		___hasFaulted_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
