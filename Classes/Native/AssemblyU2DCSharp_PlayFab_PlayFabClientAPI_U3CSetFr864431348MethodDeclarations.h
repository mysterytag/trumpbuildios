﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<SetFriendTags>c__AnonStoreyB0
struct U3CSetFriendTagsU3Ec__AnonStoreyB0_t864431348;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<SetFriendTags>c__AnonStoreyB0::.ctor()
extern "C"  void U3CSetFriendTagsU3Ec__AnonStoreyB0__ctor_m4156831967 (U3CSetFriendTagsU3Ec__AnonStoreyB0_t864431348 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<SetFriendTags>c__AnonStoreyB0::<>m__9D(PlayFab.CallRequestContainer)
extern "C"  void U3CSetFriendTagsU3Ec__AnonStoreyB0_U3CU3Em__9D_m556212584 (U3CSetFriendTagsU3Ec__AnonStoreyB0_t864431348 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
