﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ContinueWithObservable`2<System.Object,System.Object>
struct ContinueWithObservable_2_t4007622692;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Func`2<System.Object,UniRx.IObservable`1<System.Object>>
struct Func_2_t1894581716;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.ContinueWithObservable`2<System.Object,System.Object>::.ctor(UniRx.IObservable`1<TSource>,System.Func`2<TSource,UniRx.IObservable`1<TResult>>)
extern "C"  void ContinueWithObservable_2__ctor_m3943250262_gshared (ContinueWithObservable_2_t4007622692 * __this, Il2CppObject* ___source0, Func_2_t1894581716 * ___selector1, const MethodInfo* method);
#define ContinueWithObservable_2__ctor_m3943250262(__this, ___source0, ___selector1, method) ((  void (*) (ContinueWithObservable_2_t4007622692 *, Il2CppObject*, Func_2_t1894581716 *, const MethodInfo*))ContinueWithObservable_2__ctor_m3943250262_gshared)(__this, ___source0, ___selector1, method)
// System.IDisposable UniRx.Operators.ContinueWithObservable`2<System.Object,System.Object>::SubscribeCore(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  Il2CppObject * ContinueWithObservable_2_SubscribeCore_m2919942245_gshared (ContinueWithObservable_2_t4007622692 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define ContinueWithObservable_2_SubscribeCore_m2919942245(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (ContinueWithObservable_2_t4007622692 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))ContinueWithObservable_2_SubscribeCore_m2919942245_gshared)(__this, ___observer0, ___cancel1, method)
