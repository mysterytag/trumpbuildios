﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetTitleDataResponseCallback
struct GetTitleDataResponseCallback_t1917440850;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetTitleDataRequest
struct GetTitleDataRequest_t535093619;
// PlayFab.ClientModels.GetTitleDataResult
struct GetTitleDataResult_t1299334873;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetTitleData535093619.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetTitleDat1299334873.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetTitleDataResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetTitleDataResponseCallback__ctor_m1206561601 (GetTitleDataResponseCallback_t1917440850 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetTitleDataResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetTitleDataRequest,PlayFab.ClientModels.GetTitleDataResult,PlayFab.PlayFabError,System.Object)
extern "C"  void GetTitleDataResponseCallback_Invoke_m3970762190 (GetTitleDataResponseCallback_t1917440850 * __this, String_t* ___urlPath0, int32_t ___callId1, GetTitleDataRequest_t535093619 * ___request2, GetTitleDataResult_t1299334873 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetTitleDataResponseCallback_t1917440850(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetTitleDataRequest_t535093619 * ___request2, GetTitleDataResult_t1299334873 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetTitleDataResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetTitleDataRequest,PlayFab.ClientModels.GetTitleDataResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetTitleDataResponseCallback_BeginInvoke_m2656663931 (GetTitleDataResponseCallback_t1917440850 * __this, String_t* ___urlPath0, int32_t ___callId1, GetTitleDataRequest_t535093619 * ___request2, GetTitleDataResult_t1299334873 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetTitleDataResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetTitleDataResponseCallback_EndInvoke_m2400183761 (GetTitleDataResponseCallback_t1917440850 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
