﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.FirstObservable`1<System.Boolean>
struct FirstObservable_1_t2233559755;
// UniRx.IObservable`1<System.Boolean>
struct IObservable_1_t4264771001;
// System.Func`2<System.Boolean,System.Boolean>
struct Func_2_t1008118516;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Boolean>
struct IObserver_1_t2423004244;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.FirstObservable`1<System.Boolean>::.ctor(UniRx.IObservable`1<T>,System.Boolean)
extern "C"  void FirstObservable_1__ctor_m2155067117_gshared (FirstObservable_1_t2233559755 * __this, Il2CppObject* ___source0, bool ___useDefault1, const MethodInfo* method);
#define FirstObservable_1__ctor_m2155067117(__this, ___source0, ___useDefault1, method) ((  void (*) (FirstObservable_1_t2233559755 *, Il2CppObject*, bool, const MethodInfo*))FirstObservable_1__ctor_m2155067117_gshared)(__this, ___source0, ___useDefault1, method)
// System.Void UniRx.Operators.FirstObservable`1<System.Boolean>::.ctor(UniRx.IObservable`1<T>,System.Func`2<T,System.Boolean>,System.Boolean)
extern "C"  void FirstObservable_1__ctor_m2072790039_gshared (FirstObservable_1_t2233559755 * __this, Il2CppObject* ___source0, Func_2_t1008118516 * ___predicate1, bool ___useDefault2, const MethodInfo* method);
#define FirstObservable_1__ctor_m2072790039(__this, ___source0, ___predicate1, ___useDefault2, method) ((  void (*) (FirstObservable_1_t2233559755 *, Il2CppObject*, Func_2_t1008118516 *, bool, const MethodInfo*))FirstObservable_1__ctor_m2072790039_gshared)(__this, ___source0, ___predicate1, ___useDefault2, method)
// System.IDisposable UniRx.Operators.FirstObservable`1<System.Boolean>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * FirstObservable_1_SubscribeCore_m2141808247_gshared (FirstObservable_1_t2233559755 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define FirstObservable_1_SubscribeCore_m2141808247(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (FirstObservable_1_t2233559755 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))FirstObservable_1_SubscribeCore_m2141808247_gshared)(__this, ___observer0, ___cancel1, method)
