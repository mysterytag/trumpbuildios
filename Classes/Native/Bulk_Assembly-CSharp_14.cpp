﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// Zenject.SingletonInstanceHelper
struct SingletonInstanceHelper_t833281251;
// Zenject.SingletonLazyCreator
struct SingletonLazyCreator_t2762284194;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// Zenject.SingletonProviderMap
struct SingletonProviderMap_t1557411893;
// Zenject.SingletonId
struct SingletonId_t1838183899;
// System.Func`2<Zenject.InjectContext,System.Object>
struct Func_2_t2621245597;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// Zenject.InjectContext
struct InjectContext_t3456483891;
// Zenject.SingletonProvider
struct SingletonProvider_t585108081;
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException>
struct IEnumerable_1_t4073207355;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t3709260776;
// System.Collections.Generic.IEnumerable`1<Zenject.SingletonLazyCreator>
struct IEnumerable_1_t1339471254;
// Zenject.ProviderBase
struct ProviderBase_t1627494391;
// System.String
struct String_t;
// Zenject.StandardUnityInstaller
struct StandardUnityInstaller_t3603054405;
// Zenject.BinderGeneric`1<Zenject.IDependencyRoot>
struct BinderGeneric_1_t2051703467;
// Zenject.BinderGeneric`1<System.Object>
struct BinderGeneric_1_t520705716;
// Zenject.BindingConditionSetter
struct BindingConditionSetter_t259147722;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// Zenject.BinderGeneric`1<Zenject.TickableManager>
struct BinderGeneric_1_t4256934475;
// Zenject.BinderGeneric`1<Zenject.InitializableManager>
struct BinderGeneric_1_t1542634931;
// Zenject.BinderGeneric`1<Zenject.DisposableManager>
struct BinderGeneric_1_t1778548034;
// Zenject.BinderGeneric`1<Zenject.UnityEventManager>
struct BinderGeneric_1_t1939117789;
// Zenject.BinderGeneric`1<Zenject.ITickable>
struct BinderGeneric_1_t473111317;
// Zenject.TickableManager
struct TickableManager_t278367883;
// System.Collections.Generic.IEnumerable`1<System.Type>
struct IEnumerable_1_t1356416995;
// System.Collections.Generic.IEnumerable`1<Zenject.ITickable>
struct IEnumerable_1_t3661666377;
// System.Func`2<Zenject.ITickable,System.Type>
struct Func_2_t1456098478;
// System.Func`2<System.Object,System.Object>
struct Func_2_t2135783352;
// System.Collections.Generic.IEnumerable`1<ModestTree.Util.Tuple`2<System.Type,System.Int32>>
struct IEnumerable_1_t2296736111;
// System.Func`2<ModestTree.Util.Tuple`2<System.Type,System.Int32>,System.Type>
struct Func_2_t1899971632;
// System.Func`2<ModestTree.Util.Tuple`2<System.Type,System.Int32>,System.Boolean>
struct Func_2_t3626714334;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t1509682273;
// System.Collections.Generic.IEnumerable`1<System.Int32>
struct IEnumerable_1_t1424601847;
// System.Func`2<ModestTree.Util.Tuple`2<System.Type,System.Int32>,System.Int32>
struct Func_2_t1968156484;
// System.Func`2<System.Object,System.Int32>
struct Func_2_t4146091719;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t3644373756;
// Zenject.ILateTickable
struct ILateTickable_t3465810843;
// Zenject.IFixedTickable
struct IFixedTickable_t3328391607;
// Zenject.ITickable
struct ITickable_t789512021;
// ModestTree.Util.Tuple`2<System.Type,System.Int32>
struct Tuple_2_t3719549051;
// Zenject.TickableManager/<InitFixedTickables>c__AnonStorey192
struct U3CInitFixedTickablesU3Ec__AnonStorey192_t1515684493;
// Zenject.TickableManager/<InitLateTickables>c__AnonStorey194
struct U3CInitLateTickablesU3Ec__AnonStorey194_t4122519257;
// Zenject.TickableManager/<InitTickables>c__AnonStorey193
struct U3CInitTickablesU3Ec__AnonStorey193_t88162706;
// Zenject.TickablePrioritiesInstaller
struct TickablePrioritiesInstaller_t1293343172;
// System.Collections.Generic.List`1<System.Type>
struct List_1_t3576188904;
// Zenject.BinderGeneric`1<ModestTree.Util.Tuple`2<System.Type,System.Int32>>
struct BinderGeneric_1_t3403148347;
// ModestTree.Util.Tuple`2<System.Object,System.Int32>
struct Tuple_2_t299352770;
// Zenject.TransientProvider
struct TransientProvider_t1859995120;
// Zenject.ZenjectTypeInfo
struct ZenjectTypeInfo_t283213708;
// System.Collections.Generic.List`1<Zenject.InjectableInfo>
struct List_1_t1944668743;
// System.Collections.Generic.IEnumerable`1<Zenject.InjectableInfo>
struct IEnumerable_1_t4019864130;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t1634065389;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t3542137334;
// System.Collections.Generic.IEnumerable`1<System.Reflection.ParameterInfo>
struct IEnumerable_1_t1187460889;
// System.Func`2<System.Reflection.ParameterInfo,Zenject.InjectableInfo>
struct Func_2_t3562324733;
// Zenject.InjectableInfo
struct InjectableInfo_t1147709774;
// System.Reflection.ParameterInfo
struct ParameterInfo_t2610273829;
// System.Collections.Generic.IEnumerable`1<Zenject.InjectAttribute>
struct IEnumerable_1_t1368508500;
// System.Reflection.ICustomAttributeProvider
struct ICustomAttributeProvider_t2334200065;
// System.Collections.Generic.List`1<Zenject.InjectAttribute>
struct List_1_t3588280409;
// System.Collections.Generic.IEnumerable`1<Zenject.InjectOptionalAttribute>
struct IEnumerable_1_t3772099028;
// System.Collections.Generic.List`1<Zenject.InjectOptionalAttribute>
struct List_1_t1696903641;
// Zenject.InjectAttribute
struct InjectAttribute_t2791321440;
// Zenject.InjectOptionalAttribute
struct InjectOptionalAttribute_t899944672;
// System.Collections.Generic.List`1<Zenject.PostInjectableInfo>
struct List_1_t3877242631;
// System.Collections.Generic.IEnumerable`1<System.Reflection.MethodInfo>
struct IEnumerable_1_t2038408337;
// System.Func`2<System.Reflection.MethodInfo,System.Boolean>
struct Func_2_t3166396148;
// System.Collections.Generic.List`1<System.Reflection.MethodInfo>
struct List_1_t4258180246;
// System.Linq.IOrderedEnumerable`1<System.Reflection.MethodInfo>
struct IOrderedEnumerable_1_t3917975220;
// System.Func`2<System.Reflection.MethodInfo,System.Int32>
struct Func_2_t1507838298;
// System.Linq.IOrderedEnumerable`1<System.Object>
struct IOrderedEnumerable_1_t1293860363;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Collections.Generic.IEnumerable`1<System.Reflection.ConstructorInfo>
struct IEnumerable_1_t2119324394;
// System.Func`2<System.Reflection.ConstructorInfo,System.Boolean>
struct Func_2_t775214871;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// Zenject.TypeAnalyzer/<CreateForMember>c__AnonStorey183
struct U3CCreateForMemberU3Ec__AnonStorey183_t1843898456;
// Zenject.TypeAnalyzer/<CreateForMember>c__AnonStorey184
struct U3CCreateForMemberU3Ec__AnonStorey184_t1843898457;
// Zenject.TypeAnalyzer/<GetConstructorInjectables>c__AnonStorey181
struct U3CGetConstructorInjectablesU3Ec__AnonStorey181_t3473840153;
// Zenject.TypeAnalyzer/<GetFieldInjectables>c__Iterator48
struct U3CGetFieldInjectablesU3Ec__Iterator48_t3210264959;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<Zenject.InjectableInfo>
struct IEnumerator_1_t2630816222;
// System.Collections.Generic.IEnumerable`1<System.Reflection.FieldInfo>
struct IEnumerable_1_t4037084138;
// System.Func`2<System.Reflection.FieldInfo,System.Boolean>
struct Func_2_t2642952727;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// Zenject.TypeAnalyzer/<GetPostInjectMethods>c__AnonStorey182
struct U3CGetPostInjectMethodsU3Ec__AnonStorey182_t1421092187;
// Zenject.TypeAnalyzer/<GetPropertyInjectables>c__Iterator47
struct U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871;
// System.Collections.Generic.IEnumerable`1<System.Reflection.PropertyInfo>
struct IEnumerable_1_t67735429;
// System.Func`2<System.Reflection.PropertyInfo,System.Boolean>
struct Func_2_t2846737840;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// Zenject.TypeValuePair
struct TypeValuePair_t620932390;
// Zenject.UnityDependencyRoot
struct UnityDependencyRoot_t1917880567;
// Zenject.UnityEventManager
struct UnityEventManager_t2255518493;
// System.Action
struct Action_t437523947;
// System.Action`1<System.Boolean>
struct Action_1_t359458046;
// System.Action`1<System.Int32>
struct Action_1_t2995867492;
// System.Action`1<ModestTree.Util.MouseWheelScrollDirections>
struct Action_1_t3075561458;
// Zenject.ZenjectBindException
struct ZenjectBindException_t3550319704;
// System.Exception
struct Exception_t1967233988;
// Zenject.ZenjectException
struct ZenjectException_t523236501;
// Zenject.ZenjectResolveException
struct ZenjectResolveException_t1201052999;
// System.Collections.Generic.IEnumerable`1<Zenject.PostInjectableInfo>
struct IEnumerable_1_t1657470722;
// System.Func`2<Zenject.PostInjectableInfo,System.Collections.Generic.IEnumerable`1<Zenject.InjectableInfo>>
struct Func_2_t2429407812;
// System.Func`2<System.Object,System.Collections.Generic.IEnumerable`1<System.Object>>
struct Func_2_t712970412;
// Zenject.PostInjectableInfo
struct PostInjectableInfo_t3080283662;
// Zenject.ZenUtil
struct ZenUtil_t3149524122;
// System.Action`1<Zenject.DiContainer>
struct Action_1_t2531567154;
// Zenject.ZenUtil/<LoadSceneAdditiveWithContainer>c__Iterator4D
struct U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D_t2977085640;
// System.Collections.Generic.IEnumerable`1<UnityEngine.GameObject>
struct IEnumerable_1_t2589882162;
// Zenject.SceneCompositionRoot
struct SceneCompositionRoot_t1259693845;
// IStream`1<UnityEngine.EventSystems.BaseEventData>
struct IStream_1_t4099794717;
// UnityEngine.EventSystems.EventTrigger
struct EventTrigger_t2937500249;
// IEmptyStream
struct IEmptyStream_t3684082468;
// UnityEngine.UI.Button
struct Button_t990034267;
// ZergEngineTools/<ClickStream>c__AnonStorey14F
struct U3CClickStreamU3Ec__AnonStorey14F_t3915235342;
// System.IDisposable
struct IDisposable_t1628921374;
// ZergEngineTools/<HoldStream>c__AnonStorey150
struct U3CHoldStreamU3Ec__AnonStorey150_t1648779976;
// ZergEngineTools/<PressStream>c__AnonStorey14E
struct U3CPressStreamU3Ec__AnonStorey14E_t2542632690;
// System.Action`1<UnityEngine.EventSystems.BaseEventData>
struct Action_1_t3695556431;
// ZergEngineTools/<PressStream>c__AnonStorey14E/<PressStream>c__AnonStorey14D
struct U3CPressStreamU3Ec__AnonStorey14D_t2542632689;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t3547103726;
// ZergEngineTools/UnityActionDisposable
struct UnityActionDisposable_t208063915;
// UniRx.IObservable`1<UnityEngine.Vector2>
struct IObservable_1_t3284128152;
// UnityEngine.RectTransform
struct RectTransform_t3317474837;
// System.Func`2<UnityEngine.Vector2,System.Boolean>
struct Func_2_t4239542457;
// UniRx.IObservable`1<System.Int64>
struct IObservable_1_t2606213246;
// System.Func`2<System.Int64,System.Boolean>
struct Func_2_t2251336571;
// System.Func`2<System.Int64,UnityEngine.Vector2>
struct Func_2_t1270693722;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Touch>
struct IEnumerable_1_t181070944;
// System.Func`2<UnityEngine.Touch,System.Boolean>
struct Func_2_t1874381577;
// ZergRush.ObservableExtension/<EveryTapOutsideRectTransform>c__AnonStorey11B
struct U3CEveryTapOutsideRectTransformU3Ec__AnonStorey11B_t2782190866;
// UnityEngine.Canvas
struct Canvas_t3534013893;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharp_Zenject_SingletonInstanceHelper833281251.h"
#include "AssemblyU2DCSharp_Zenject_SingletonInstanceHelper833281251MethodDeclarations.h"
#include "mscorlib_System_Void2779279689.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_SingletonLazyCreator2762284194.h"
#include "AssemblyU2DCSharp_Zenject_SingletonLazyCreator2762284194MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449.h"
#include "AssemblyU2DCSharp_Zenject_SingletonProviderMap1557411893.h"
#include "AssemblyU2DCSharp_Zenject_SingletonId1838183899.h"
#include "System_Core_System_Func_2_gen2621245597.h"
#include "mscorlib_System_Boolean211005341.h"
#include "mscorlib_System_Int322847414787.h"
#include "AssemblyU2DCSharp_Zenject_SingletonProviderMap1557411893MethodDeclarations.h"
#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharp_ModestTree_Assert1161478012MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449MethodDeclarations.h"
#include "mscorlib_System_Type2779229935.h"
#include "AssemblyU2DCSharp_Zenject_InjectContext3456483891.h"
#include "System_Core_System_Func_2_gen2621245597MethodDeclarations.h"
#include "AssemblyU2DCSharp_ModestTree_MiscExtensions2809094742MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_ZenjectResolveException1201052999MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1417891359MethodDeclarations.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_ArrayTypes.h"
#include "AssemblyU2DCSharp_Zenject_ZenjectResolveException1201052999.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1417891359.h"
#include "AssemblyU2DCSharp_ModestTree_TypeExtensions3479487268MethodDeclarations.h"
#include "mscorlib_System_Type2779229935MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_SingletonProvider585108081.h"
#include "AssemblyU2DCSharp_Zenject_SingletonProvider585108081MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_ProviderBase1627494391MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable4285654829MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_BindingValidator1279814114MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable4285654829.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2265680905MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2265680905.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V4187817999.h"
#include "AssemblyU2DCSharp_Zenject_ProviderBase1627494391.h"
#include "AssemblyU2DCSharp_Zenject_SingletonId1838183899MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_ZenjectBindException3550319704MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_ZenjectBindException3550319704.h"
#include "AssemblyU2DCSharp_Zenject_StandardUnityInstaller3603054405.h"
#include "AssemblyU2DCSharp_Zenject_StandardUnityInstaller3603054405MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_Installer3915807453MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3878351788MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component2126946602MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_BinderGeneric_1_gen2051703467MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_Binder3872662847MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_BinderGeneric_1_gen473111317MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_BinderGeneric_1_gen2051703467.h"
#include "AssemblyU2DCSharp_Zenject_CompositionRoot1939928321.h"
#include "UnityEngine_UnityEngine_Object3878351788.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"
#include "AssemblyU2DCSharp_Zenject_BindingConditionSetter259147722.h"
#include "AssemblyU2DCSharp_Zenject_BinderGeneric_1_gen520705716.h"
#include "AssemblyU2DCSharp_Zenject_BinderGeneric_1_gen4256934475.h"
#include "AssemblyU2DCSharp_Zenject_BinderGeneric_1_gen1542634931.h"
#include "AssemblyU2DCSharp_Zenject_BinderGeneric_1_gen1778548034.h"
#include "AssemblyU2DCSharp_Zenject_BinderGeneric_1_gen1939117789.h"
#include "AssemblyU2DCSharp_Zenject_BinderGeneric_1_gen473111317.h"
#include "AssemblyU2DCSharp_Zenject_TickableManager278367883.h"
#include "AssemblyU2DCSharp_Zenject_TickableManager278367883MethodDeclarations.h"
#include "System_Core_System_Func_2_gen1456098478MethodDeclarations.h"
#include "mscorlib_System_String968488902MethodDeclarations.h"
#include "AssemblyU2DCSharp_ModestTree_Log3485895482MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1586470990.h"
#include "System_Core_System_Func_2_gen1456098478.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_Action_1_gen3476844312MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_TaskUpdater_1_gen4197280854MethodDeclarations.h"
#include "System_Core_System_Func_2_gen1899971632MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_TickableManager_U3CInitF1515684493MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4125350576MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2211133568MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3626714334MethodDeclarations.h"
#include "System_Core_System_Func_2_gen1968156484MethodDeclarations.h"
#include "AssemblyU2DCSharp_ModestTree_LinqExtensions3281724522MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2211133568.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3644373756.h"
#include "AssemblyU2DCSharp_Zenject_TickableManager_U3CInitF1515684493.h"
#include "mscorlib_System_Action_1_gen3476844312.h"
#include "AssemblyU2DCSharp_Zenject_TaskUpdater_1_gen4197280854.h"
#include "mscorlib_System_Collections_Generic_List_1_gen221540724.h"
#include "System_Core_System_Func_2_gen1899971632.h"
#include "AssemblyU2DCSharp_ModestTree_Util_Tuple_2_gen3719549051.h"
#include "AssemblyU2DCSharp_ModestTree_TypeExtensions3479487268.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4125350576.h"
#include "System_Core_System_Func_2_gen3626714334.h"
#include "System_Core_System_Func_2_gen1968156484.h"
#include "AssemblyU2DCSharp_ModestTree_LinqExtensions3281724522.h"
#include "mscorlib_System_Action_1_gen937964726MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_TaskUpdater_1_gen1658401268MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_TickableManager_U3CInitTic88162706MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1586470990MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3967221278MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3967221278.h"
#include "AssemblyU2DCSharp_Zenject_TickableManager_U3CInitTic88162706.h"
#include "mscorlib_System_Action_1_gen937964726.h"
#include "AssemblyU2DCSharp_Zenject_TaskUpdater_1_gen1658401268.h"
#include "mscorlib_System_Action_1_gen3614263548MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_TaskUpdater_1_gen39732794MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_TickableManager_U3CInitL4122519257MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4262769812MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2348552804MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2348552804.h"
#include "AssemblyU2DCSharp_Zenject_TickableManager_U3CInitL4122519257.h"
#include "mscorlib_System_Action_1_gen3614263548.h"
#include "AssemblyU2DCSharp_Zenject_TaskUpdater_1_gen39732794.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4262769812.h"
#include "AssemblyU2DCSharp_Zenject_TickablePrioritiesInstal1293343172.h"
#include "AssemblyU2DCSharp_Zenject_TickablePrioritiesInstal1293343172MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3576188904.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3576188904MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1661971896MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1661971896.h"
#include "AssemblyU2DCSharp_ModestTree_Util_Tuple2948270274MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_BinderGeneric_1_gen3403148347MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_BindingConditionSetter259147722MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_BinderGeneric_1_gen3403148347.h"
#include "AssemblyU2DCSharp_ModestTree_Util_Tuple2948270274.h"
#include "AssemblyU2DCSharp_Zenject_TransientProvider1859995120.h"
#include "AssemblyU2DCSharp_Zenject_TransientProvider1859995120MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_TypeAnalyzer2089278869.h"
#include "AssemblyU2DCSharp_Zenject_TypeAnalyzer2089278869MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2395761423MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2395761423.h"
#include "AssemblyU2DCSharp_Zenject_ZenjectTypeInfo283213708.h"
#include "AssemblyU2DCSharp_Zenject_ZenjectTypeInfo283213708MethodDeclarations.h"
#include "mscorlib_System_Reflection_ConstructorInfo3542137334.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3877242631.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1944668743.h"
#include "AssemblyU2DCSharp_Zenject_TypeAnalyzer_U3CGetConst3473840153MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3562324733MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_TypeAnalyzer_U3CGetConst3473840153.h"
#include "mscorlib_System_Reflection_MethodBase3461000640MethodDeclarations.h"
#include "mscorlib_System_Reflection_MethodBase3461000640.h"
#include "mscorlib_System_Reflection_ParameterInfo2610273829.h"
#include "AssemblyU2DCSharp_Zenject_InjectableInfo1147709774.h"
#include "System_Core_System_Func_2_gen3562324733.h"
#include "AssemblyU2DCSharp_Zenject_InjectAttribute2791321440MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_InjectOptionalAttribute899944672MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_InjectableInfo1147709774MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3588280409.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1696903641.h"
#include "mscorlib_System_Reflection_ParameterInfo2610273829MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_InjectAttribute2791321440.h"
#include "AssemblyU2DCSharp_Zenject_InjectOptionalAttribute899944672.h"
#include "mscorlib_System_Reflection_ParameterAttributes4282458126.h"
#include "System_Core_System_Action_2_gen4105459918.h"
#include "AssemblyU2DCSharp_Zenject_TypeAnalyzer_U3CGetPostI1421092187MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3166396148MethodDeclarations.h"
#include "System_Core_System_Func_2_gen1507838298MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3877242631MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_PostInjectableInfo3080283662MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4258180246.h"
#include "mscorlib_System_Reflection_MethodInfo3461221277.h"
#include "AssemblyU2DCSharp_Zenject_TypeAnalyzer_U3CGetPostI1421092187.h"
#include "mscorlib_System_Reflection_BindingFlags2090192240.h"
#include "System_Core_System_Func_2_gen3166396148.h"
#include "AssemblyU2DCSharp_ModestTree_MiscExtensions2809094742.h"
#include "System_Core_System_Func_2_gen1507838298.h"
#include "AssemblyU2DCSharp_Zenject_PostInjectableInfo3080283662.h"
#include "AssemblyU2DCSharp_Zenject_TypeAnalyzer_U3CGetProper473481871MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_TypeAnalyzer_U3CGetProper473481871.h"
#include "AssemblyU2DCSharp_Zenject_TypeAnalyzer_U3CGetField3210264959MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_TypeAnalyzer_U3CGetField3210264959.h"
#include "mscorlib_System_Reflection_MemberInfo2843033814.h"
#include "AssemblyU2DCSharp_Zenject_TypeAnalyzer_U3CCreateFo1843898456MethodDeclarations.h"
#include "System_Core_System_Action_2_gen4105459918MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_TypeAnalyzer_U3CCreateFo1843898457MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_TypeAnalyzer_U3CCreateFo1843898456.h"
#include "AssemblyU2DCSharp_Zenject_TypeAnalyzer_U3CCreateFo1843898457.h"
#include "mscorlib_System_Reflection_MemberInfo2843033814MethodDeclarations.h"
#include "mscorlib_System_Reflection_FieldInfo1164929782.h"
#include "mscorlib_System_Reflection_FieldInfo1164929782MethodDeclarations.h"
#include "mscorlib_System_Reflection_PropertyInfo1490548369.h"
#include "mscorlib_System_Reflection_PropertyInfo1490548369MethodDeclarations.h"
#include "System_Core_System_Func_2_gen775214871MethodDeclarations.h"
#include "System_Core_System_Func_2_gen775214871.h"
#include "mscorlib_System_RuntimeTypeHandle1864875887.h"
#include "mscorlib_System_Threading_Interlocked3007803305MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2642952727MethodDeclarations.h"
#include "mscorlib_System_UInt32985925326.h"
#include "System_Core_System_Func_2_gen2642952727.h"
#include "mscorlib_System_NotSupportedException1374155497MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1374155497.h"
#include "System_Core_System_Func_2_gen2846737840MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2846737840.h"
#include "AssemblyU2DCSharp_Zenject_TypeValuePair620932390.h"
#include "AssemblyU2DCSharp_Zenject_TypeValuePair620932390MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_UnityDependencyRoot1917880567.h"
#include "AssemblyU2DCSharp_Zenject_UnityDependencyRoot1917880567MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour3012272455MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_InitializableManager1859035635MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_InitializableManager1859035635.h"
#include "AssemblyU2DCSharp_Zenject_DisposableManager2094948738.h"
#include "AssemblyU2DCSharp_Zenject_DisposableManager2094948738MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_UnityEventManager2255518493.h"
#include "AssemblyU2DCSharp_Zenject_UnityEventManager2255518493MethodDeclarations.h"
#include "System_Core_System_Action437523947MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen359458046MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2995867492MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3075561458MethodDeclarations.h"
#include "System_Core_System_Action437523947.h"
#include "mscorlib_System_Action_1_gen359458046.h"
#include "mscorlib_System_Action_1_gen2995867492.h"
#include "mscorlib_System_Action_1_gen3075561458.h"
#include "AssemblyU2DCSharp_ModestTree_Util_MouseWheelScroll2927108753.h"
#include "mscorlib_System_Delegate3660574010MethodDeclarations.h"
#include "mscorlib_System_Delegate3660574010.h"
#include "UnityEngine_UnityEngine_Screen3994030297MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Input1593691127MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector33525329789MethodDeclarations.h"
#include "AssemblyU2DCSharp_ModestTree_Util_UnityUtil126913041MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"
#include "AssemblyU2DCSharp_Zenject_ZenjectException523236501MethodDeclarations.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharp_Zenject_ZenjectException523236501.h"
#include "mscorlib_System_Exception1967233988MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2429407812MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2429407812.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyColl15887826.h"
#include "AssemblyU2DCSharp_Zenject_ZenUtil3149524122.h"
#include "AssemblyU2DCSharp_Zenject_ZenUtil3149524122MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2531567154.h"
#include "UnityEngine_UnityEngine_Application450040189MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_SceneCompositionRoot1259693845.h"
#include "AssemblyU2DCSharp_Zenject_SceneCompositionRoot1259693845MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_ZenUtil_U3CLoadSceneAddi2977085640MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_ZenUtil_U3CLoadSceneAddi2977085640.h"
#include "UnityEngine_UnityEngine_GameObject4012695102MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen514686775.h"
#include "AssemblyU2DCSharp_ZergEngineTools3400005081.h"
#include "AssemblyU2DCSharp_ZergEngineTools3400005081MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg2937500249.h"
#include "AssemblyU2DCSharp_ZergEngineTools_U3CPressStreamU32542632690MethodDeclarations.h"
#include "System_Core_System_Func_3_gen3600102922MethodDeclarations.h"
#include "AssemblyU2DCSharp_AnonymousStream_1_gen2804241537MethodDeclarations.h"
#include "AssemblyU2DCSharp_ZergEngineTools_U3CPressStreamU32542632690.h"
#include "mscorlib_System_Action_1_gen3695556431.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"
#include "System_Core_System_Func_3_gen3600102922.h"
#include "AssemblyU2DCSharp_AnonymousStream_1_gen2804241537.h"
#include "UnityEngine_UI_UnityEngine_UI_Button990034267.h"
#include "AssemblyU2DCSharp_ZergEngineTools_U3CClickStreamU33915235342MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug1588791936MethodDeclarations.h"
#include "AssemblyU2DCSharp_AbandonedStream4197275220MethodDeclarations.h"
#include "System_Core_System_Func_3_gen1216273990MethodDeclarations.h"
#include "AssemblyU2DCSharp_AnonymousEmptyStream62006432MethodDeclarations.h"
#include "AssemblyU2DCSharp_ZergEngineTools_U3CClickStreamU33915235342.h"
#include "AssemblyU2DCSharp_AbandonedStream4197275220.h"
#include "System_Core_System_Func_3_gen1216273990.h"
#include "AssemblyU2DCSharp_AnonymousEmptyStream62006432.h"
#include "AssemblyU2DCSharp_ZergEngineTools_U3CHoldStreamU3E1648779976MethodDeclarations.h"
#include "AssemblyU2DCSharp_ZergEngineTools_U3CHoldStreamU3E1648779976.h"
#include "UnityEngine_UnityEngine_Events_UnityAction909267611MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Button990034267MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent2938797301MethodDeclarations.h"
#include "AssemblyU2DCSharp_ZergEngineTools_UnityActionDispos208063915MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction909267611.h"
#include "AssemblyU2DCSharp_ZergEngineTools_UnityActionDispos208063915.h"
#include "UnityEngine_UI_UnityEngine_UI_Button_ButtonClickedE962981669.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent2938797301.h"
#include "AssemblyU2DCSharp_ZergEngineTools_U3CPressStreamU32542632689MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3527565631MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigge516183010MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen2489558537MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg2937500249MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen864074059MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigger67115090MethodDeclarations.h"
#include "AssemblyU2DCSharp_ZergEngineTools_UnityActionDispo2125801935MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3527565631.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigge516183010.h"
#include "AssemblyU2DCSharp_ZergEngineTools_U3CPressStreamU32542632689.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigger67115090.h"
#include "AssemblyU2DCSharp_ZergEngineTools_UnityActionDispo2125801935.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseEventD3547103726.h"
#include "mscorlib_System_Collections_Generic_List_1_gen864074059.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigge904435379.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen2489558537.h"
#include "mscorlib_System_Action_1_gen3695556431MethodDeclarations.h"
#include "AssemblyU2DCSharp_ZergRush_ObservableExtension8126852.h"
#include "AssemblyU2DCSharp_ZergRush_ObservableExtension8126852MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform3317474837.h"
#include "AssemblyU2DCSharp_ZergRush_ObservableExtension_U3C2782190866MethodDeclarations.h"
#include "System_Core_System_Func_2_gen4239542457MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable258071701MethodDeclarations.h"
#include "AssemblyU2DCSharp_ZergRush_ObservableExtension_U3C2782190866.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"
#include "System_Core_System_Func_2_gen4239542457.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable258071701.h"
#include "System_Core_System_Func_2_gen2251336571MethodDeclarations.h"
#include "System_Core_System_Func_2_gen1270693722MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2251336571.h"
#include "mscorlib_System_Int642847414882.h"
#include "System_Core_System_Func_2_gen1270693722.h"
#include "System_Core_System_Func_2_gen1874381577MethodDeclarations.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Touch1603883884.h"
#include "System_Core_System_Func_2_gen1874381577.h"
#include "UnityEngine_UnityEngine_Vector23525329788MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Touch1603883884MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TouchPhase1905076713.h"
#include "UnityEngine_UnityEngine_Canvas3534013893MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransformUtility2895919825MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform3317474837MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect1525428817MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera3533968274.h"
#include "UnityEngine_UnityEngine_Rect1525428817.h"
#include "UnityEngine_UnityEngine_Canvas3534013893.h"
#include "UnityEngine_UnityEngine_Component2126946602.h"
#include "AssemblyU2DCSharp_ZergRush_ReactiveCollectionExten3620204240.h"
#include "AssemblyU2DCSharp_ZergRush_ReactiveCollectionExten3620204240MethodDeclarations.h"
#include "AssemblyU2DCSharp_ZergRush_ReactiveCollectionExten1250979187.h"
#include "AssemblyU2DCSharp_ZergRush_ReactiveCollectionExten1250979187MethodDeclarations.h"

// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Empty<System.Object>()
extern "C"  Il2CppObject* Enumerable_Empty_TisIl2CppObject_m301282091_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Enumerable_Empty_TisIl2CppObject_m301282091(__this /* static, unused */, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Enumerable_Empty_TisIl2CppObject_m301282091_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Empty<Zenject.ZenjectResolveException>()
#define Enumerable_Empty_TisZenjectResolveException_t1201052999_m2785882006(__this /* static, unused */, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Enumerable_Empty_TisIl2CppObject_m301282091_gshared)(__this /* static, unused */, method)
// Zenject.BinderGeneric`1<!!0> Zenject.DiContainer::Bind<System.Object>()
extern "C"  BinderGeneric_1_t520705716 * DiContainer_Bind_TisIl2CppObject_m3651981917_gshared (DiContainer_t2383114449 * __this, const MethodInfo* method);
#define DiContainer_Bind_TisIl2CppObject_m3651981917(__this, method) ((  BinderGeneric_1_t520705716 * (*) (DiContainer_t2383114449 *, const MethodInfo*))DiContainer_Bind_TisIl2CppObject_m3651981917_gshared)(__this, method)
// Zenject.BinderGeneric`1<!!0> Zenject.DiContainer::Bind<Zenject.IDependencyRoot>()
#define DiContainer_Bind_TisIDependencyRoot_t2368104171_m1381176612(__this, method) ((  BinderGeneric_1_t2051703467 * (*) (DiContainer_t2383114449 *, const MethodInfo*))DiContainer_Bind_TisIl2CppObject_m3651981917_gshared)(__this, method)
// Zenject.BindingConditionSetter Zenject.BinderGeneric`1<System.Object>::ToSingleMonoBehaviour<System.Object>(UnityEngine.GameObject)
extern "C"  BindingConditionSetter_t259147722 * BinderGeneric_1_ToSingleMonoBehaviour_TisIl2CppObject_m136241036_gshared (BinderGeneric_1_t520705716 * __this, GameObject_t4012695102 * p0, const MethodInfo* method);
#define BinderGeneric_1_ToSingleMonoBehaviour_TisIl2CppObject_m136241036(__this, p0, method) ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t520705716 *, GameObject_t4012695102 *, const MethodInfo*))BinderGeneric_1_ToSingleMonoBehaviour_TisIl2CppObject_m136241036_gshared)(__this, p0, method)
// Zenject.BindingConditionSetter Zenject.BinderGeneric`1<Zenject.IDependencyRoot>::ToSingleMonoBehaviour<Zenject.UnityDependencyRoot>(UnityEngine.GameObject)
#define BinderGeneric_1_ToSingleMonoBehaviour_TisUnityDependencyRoot_t1917880567_m890979406(__this, p0, method) ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t2051703467 *, GameObject_t4012695102 *, const MethodInfo*))BinderGeneric_1_ToSingleMonoBehaviour_TisIl2CppObject_m136241036_gshared)(__this, p0, method)
// Zenject.BinderGeneric`1<!!0> Zenject.DiContainer::Bind<Zenject.TickableManager>()
#define DiContainer_Bind_TisTickableManager_t278367883_m1598376324(__this, method) ((  BinderGeneric_1_t4256934475 * (*) (DiContainer_t2383114449 *, const MethodInfo*))DiContainer_Bind_TisIl2CppObject_m3651981917_gshared)(__this, method)
// Zenject.BinderGeneric`1<!!0> Zenject.DiContainer::Bind<Zenject.InitializableManager>()
#define DiContainer_Bind_TisInitializableManager_t1859035635_m2693675206(__this, method) ((  BinderGeneric_1_t1542634931 * (*) (DiContainer_t2383114449 *, const MethodInfo*))DiContainer_Bind_TisIl2CppObject_m3651981917_gshared)(__this, method)
// Zenject.BinderGeneric`1<!!0> Zenject.DiContainer::Bind<Zenject.DisposableManager>()
#define DiContainer_Bind_TisDisposableManager_t2094948738_m2325103725(__this, method) ((  BinderGeneric_1_t1778548034 * (*) (DiContainer_t2383114449 *, const MethodInfo*))DiContainer_Bind_TisIl2CppObject_m3651981917_gshared)(__this, method)
// Zenject.BinderGeneric`1<!!0> Zenject.DiContainer::Bind<Zenject.UnityEventManager>()
#define DiContainer_Bind_TisUnityEventManager_t2255518493_m1265107186(__this, method) ((  BinderGeneric_1_t1939117789 * (*) (DiContainer_t2383114449 *, const MethodInfo*))DiContainer_Bind_TisIl2CppObject_m3651981917_gshared)(__this, method)
// Zenject.BinderGeneric`1<!!0> Zenject.DiContainer::Bind<Zenject.ITickable>()
#define DiContainer_Bind_TisITickable_t789512021_m3228531898(__this, method) ((  BinderGeneric_1_t473111317 * (*) (DiContainer_t2383114449 *, const MethodInfo*))DiContainer_Bind_TisIl2CppObject_m3651981917_gshared)(__this, method)
// Zenject.BindingConditionSetter Zenject.BinderGeneric`1<System.Object>::ToLookup<System.Object>()
extern "C"  BindingConditionSetter_t259147722 * BinderGeneric_1_ToLookup_TisIl2CppObject_m787997530_gshared (BinderGeneric_1_t520705716 * __this, const MethodInfo* method);
#define BinderGeneric_1_ToLookup_TisIl2CppObject_m787997530(__this, method) ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t520705716 *, const MethodInfo*))BinderGeneric_1_ToLookup_TisIl2CppObject_m787997530_gshared)(__this, method)
// Zenject.BindingConditionSetter Zenject.BinderGeneric`1<Zenject.ITickable>::ToLookup<Zenject.UnityEventManager>()
#define BinderGeneric_1_ToLookup_TisUnityEventManager_t2255518493_m2801832754(__this, method) ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t473111317 *, const MethodInfo*))BinderGeneric_1_ToLookup_TisIl2CppObject_m787997530_gshared)(__this, method)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.Object,System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
extern "C"  Il2CppObject* Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m2885164610_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t2135783352 * p1, const MethodInfo* method);
#define Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m2885164610(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t2135783352 *, const MethodInfo*))Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m2885164610_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<Zenject.ITickable,System.Type>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
#define Enumerable_Select_TisITickable_t789512021_TisType_t_m1604350288(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1456098478 *, const MethodInfo*))Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m2885164610_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Distinct<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  Il2CppObject* Enumerable_Distinct_TisIl2CppObject_m204074613_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_Distinct_TisIl2CppObject_m204074613(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_Distinct_TisIl2CppObject_m204074613_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Distinct<System.Type>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_Distinct_TisType_t_m1138943728(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_Distinct_TisIl2CppObject_m204074613_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.IEnumerable`1<System.Type> Zenject.SingletonInstanceHelper::GetActiveSingletonTypesDerivingFrom<System.Object>(System.Collections.Generic.IEnumerable`1<System.Type>)
extern "C"  Il2CppObject* SingletonInstanceHelper_GetActiveSingletonTypesDerivingFrom_TisIl2CppObject_m3178924989_gshared (SingletonInstanceHelper_t833281251 * __this, Il2CppObject* p0, const MethodInfo* method);
#define SingletonInstanceHelper_GetActiveSingletonTypesDerivingFrom_TisIl2CppObject_m3178924989(__this, p0, method) ((  Il2CppObject* (*) (SingletonInstanceHelper_t833281251 *, Il2CppObject*, const MethodInfo*))SingletonInstanceHelper_GetActiveSingletonTypesDerivingFrom_TisIl2CppObject_m3178924989_gshared)(__this, p0, method)
// System.Collections.Generic.IEnumerable`1<System.Type> Zenject.SingletonInstanceHelper::GetActiveSingletonTypesDerivingFrom<Zenject.ITickable>(System.Collections.Generic.IEnumerable`1<System.Type>)
#define SingletonInstanceHelper_GetActiveSingletonTypesDerivingFrom_TisITickable_t789512021_m3238426240(__this, p0, method) ((  Il2CppObject* (*) (SingletonInstanceHelper_t833281251 *, Il2CppObject*, const MethodInfo*))SingletonInstanceHelper_GetActiveSingletonTypesDerivingFrom_TisIl2CppObject_m3178924989_gshared)(__this, p0, method)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<ModestTree.Util.Tuple`2<System.Type,System.Int32>,System.Type>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
#define Enumerable_Select_TisTuple_2_t3719549051_TisType_t_m1035253335(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1899971632 *, const MethodInfo*))Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m2885164610_gshared)(__this /* static, unused */, p0, p1, method)
// System.Boolean ModestTree.TypeExtensions::DerivesFrom<System.Object>(System.Type)
extern "C"  bool TypeExtensions_DerivesFrom_TisIl2CppObject_m3659857940_gshared (Il2CppObject * __this /* static, unused */, Type_t * p0, const MethodInfo* method);
#define TypeExtensions_DerivesFrom_TisIl2CppObject_m3659857940(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Type_t *, const MethodInfo*))TypeExtensions_DerivesFrom_TisIl2CppObject_m3659857940_gshared)(__this /* static, unused */, p0, method)
// System.Boolean ModestTree.TypeExtensions::DerivesFrom<Zenject.IFixedTickable>(System.Type)
#define TypeExtensions_DerivesFrom_TisIFixedTickable_t3328391607_m3119328223(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Type_t *, const MethodInfo*))TypeExtensions_DerivesFrom_TisIl2CppObject_m3659857940_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C"  Il2CppObject* Enumerable_Where_TisIl2CppObject_m3480373697_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t1509682273 * p1, const MethodInfo* method);
#define Enumerable_Where_TisIl2CppObject_m3480373697(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1509682273 *, const MethodInfo*))Enumerable_Where_TisIl2CppObject_m3480373697_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<ModestTree.Util.Tuple`2<System.Type,System.Int32>>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Enumerable_Where_TisTuple_2_t3719549051_m3323201792(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t3626714334 *, const MethodInfo*))Enumerable_Where_TisIl2CppObject_m3480373697_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.Object,System.Int32>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
extern "C"  Il2CppObject* Enumerable_Select_TisIl2CppObject_TisInt32_t2847414787_m2188982617_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t4146091719 * p1, const MethodInfo* method);
#define Enumerable_Select_TisIl2CppObject_TisInt32_t2847414787_m2188982617(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t4146091719 *, const MethodInfo*))Enumerable_Select_TisIl2CppObject_TisInt32_t2847414787_m2188982617_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<ModestTree.Util.Tuple`2<System.Type,System.Int32>,System.Int32>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
#define Enumerable_Select_TisTuple_2_t3719549051_TisInt32_t2847414787_m2850307189(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1968156484 *, const MethodInfo*))Enumerable_Select_TisIl2CppObject_TisInt32_t2847414787_m2188982617_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Int32>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  List_1_t3644373756 * Enumerable_ToList_TisInt32_t2847414787_m219610782_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_ToList_TisInt32_t2847414787_m219610782(__this /* static, unused */, p0, method) ((  List_1_t3644373756 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToList_TisInt32_t2847414787_m219610782_gshared)(__this /* static, unused */, p0, method)
// System.Boolean ModestTree.LinqExtensions::IsEmpty<System.Int32>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  bool LinqExtensions_IsEmpty_TisInt32_t2847414787_m576996776_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define LinqExtensions_IsEmpty_TisInt32_t2847414787_m576996776(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))LinqExtensions_IsEmpty_TisInt32_t2847414787_m576996776_gshared)(__this /* static, unused */, p0, method)
// !!0 System.Linq.Enumerable::Single<System.Int32>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  int32_t Enumerable_Single_TisInt32_t2847414787_m4182051005_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_Single_TisInt32_t2847414787_m4182051005(__this /* static, unused */, p0, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_Single_TisInt32_t2847414787_m4182051005_gshared)(__this /* static, unused */, p0, method)
// System.Boolean ModestTree.TypeExtensions::DerivesFrom<Zenject.ITickable>(System.Type)
#define TypeExtensions_DerivesFrom_TisITickable_t789512021_m2654461335(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Type_t *, const MethodInfo*))TypeExtensions_DerivesFrom_TisIl2CppObject_m3659857940_gshared)(__this /* static, unused */, p0, method)
// System.Boolean ModestTree.TypeExtensions::DerivesFrom<Zenject.ILateTickable>(System.Type)
#define TypeExtensions_DerivesFrom_TisILateTickable_t3465810843_m744818909(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Type_t *, const MethodInfo*))TypeExtensions_DerivesFrom_TisIl2CppObject_m3659857940_gshared)(__this /* static, unused */, p0, method)
// Zenject.BinderGeneric`1<!!0> Zenject.DiContainer::Bind<ModestTree.Util.Tuple`2<System.Type,System.Int32>>()
#define DiContainer_Bind_TisTuple_2_t3719549051_m656443329(__this, method) ((  BinderGeneric_1_t3403148347 * (*) (DiContainer_t2383114449 *, const MethodInfo*))DiContainer_Bind_TisIl2CppObject_m3651981917_gshared)(__this, method)
// ModestTree.Util.Tuple`2<!!0,!!1> ModestTree.Util.Tuple::New<System.Object,System.Int32>(!!0,!!1)
extern "C"  Tuple_2_t299352770 * Tuple_New_TisIl2CppObject_TisInt32_t2847414787_m408950693_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, int32_t p1, const MethodInfo* method);
#define Tuple_New_TisIl2CppObject_TisInt32_t2847414787_m408950693(__this /* static, unused */, p0, p1, method) ((  Tuple_2_t299352770 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, int32_t, const MethodInfo*))Tuple_New_TisIl2CppObject_TisInt32_t2847414787_m408950693_gshared)(__this /* static, unused */, p0, p1, method)
// ModestTree.Util.Tuple`2<!!0,!!1> ModestTree.Util.Tuple::New<System.Type,System.Int32>(!!0,!!1)
#define Tuple_New_TisType_t_TisInt32_t2847414787_m1765357578(__this /* static, unused */, p0, p1, method) ((  Tuple_2_t3719549051 * (*) (Il2CppObject * /* static, unused */, Type_t *, int32_t, const MethodInfo*))Tuple_New_TisIl2CppObject_TisInt32_t2847414787_m408950693_gshared)(__this /* static, unused */, p0, p1, method)
// Zenject.BindingConditionSetter Zenject.BinderGeneric`1<System.Object>::ToInstance<System.Object>(!!0)
extern "C"  BindingConditionSetter_t259147722 * BinderGeneric_1_ToInstance_TisIl2CppObject_m1818222243_gshared (BinderGeneric_1_t520705716 * __this, Il2CppObject * p0, const MethodInfo* method);
#define BinderGeneric_1_ToInstance_TisIl2CppObject_m1818222243(__this, p0, method) ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t520705716 *, Il2CppObject *, const MethodInfo*))BinderGeneric_1_ToInstance_TisIl2CppObject_m1818222243_gshared)(__this, p0, method)
// Zenject.BindingConditionSetter Zenject.BinderGeneric`1<ModestTree.Util.Tuple`2<System.Type,System.Int32>>::ToInstance<ModestTree.Util.Tuple`2<System.Type,System.Int32>>(!!0)
#define BinderGeneric_1_ToInstance_TisTuple_2_t3719549051_m259736355(__this, p0, method) ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t3403148347 *, Tuple_2_t3719549051 *, const MethodInfo*))BinderGeneric_1_ToInstance_TisIl2CppObject_m1818222243_gshared)(__this, p0, method)
// System.Void Zenject.BindingConditionSetter::WhenInjectedInto<System.Object>()
extern "C"  void BindingConditionSetter_WhenInjectedInto_TisIl2CppObject_m2845496023_gshared (BindingConditionSetter_t259147722 * __this, const MethodInfo* method);
#define BindingConditionSetter_WhenInjectedInto_TisIl2CppObject_m2845496023(__this, method) ((  void (*) (BindingConditionSetter_t259147722 *, const MethodInfo*))BindingConditionSetter_WhenInjectedInto_TisIl2CppObject_m2845496023_gshared)(__this, method)
// System.Void Zenject.BindingConditionSetter::WhenInjectedInto<Zenject.TickableManager>()
#define BindingConditionSetter_WhenInjectedInto_TisTickableManager_t278367883_m2461038974(__this, method) ((  void (*) (BindingConditionSetter_t259147722 *, const MethodInfo*))BindingConditionSetter_WhenInjectedInto_TisIl2CppObject_m2845496023_gshared)(__this, method)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  List_1_t1634065389 * Enumerable_ToList_TisIl2CppObject_m4082139931_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_ToList_TisIl2CppObject_m4082139931(__this /* static, unused */, p0, method) ((  List_1_t1634065389 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToList_TisIl2CppObject_m4082139931_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<Zenject.InjectableInfo>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToList_TisInjectableInfo_t1147709774_m755394695(__this /* static, unused */, p0, method) ((  List_1_t1944668743 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToList_TisIl2CppObject_m4082139931_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Empty<Zenject.InjectableInfo>()
#define Enumerable_Empty_TisInjectableInfo_t1147709774_m3970248253(__this /* static, unused */, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Enumerable_Empty_TisIl2CppObject_m301282091_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.Reflection.ParameterInfo,Zenject.InjectableInfo>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
#define Enumerable_Select_TisParameterInfo_t2610273829_TisInjectableInfo_t1147709774_m2840075429(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t3562324733 *, const MethodInfo*))Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m2885164610_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!0> ModestTree.TypeExtensions::AllAttributes<System.Object>(System.Reflection.ICustomAttributeProvider)
extern "C"  Il2CppObject* TypeExtensions_AllAttributes_TisIl2CppObject_m535975428_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define TypeExtensions_AllAttributes_TisIl2CppObject_m535975428(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))TypeExtensions_AllAttributes_TisIl2CppObject_m535975428_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.IEnumerable`1<!!0> ModestTree.TypeExtensions::AllAttributes<Zenject.InjectAttribute>(System.Reflection.ICustomAttributeProvider)
#define TypeExtensions_AllAttributes_TisInjectAttribute_t2791321440_m1093563446(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))TypeExtensions_AllAttributes_TisIl2CppObject_m535975428_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<Zenject.InjectAttribute>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToList_TisInjectAttribute_t2791321440_m2935078709(__this /* static, unused */, p0, method) ((  List_1_t3588280409 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToList_TisIl2CppObject_m4082139931_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.IEnumerable`1<!!0> ModestTree.TypeExtensions::AllAttributes<Zenject.InjectOptionalAttribute>(System.Reflection.ICustomAttributeProvider)
#define TypeExtensions_AllAttributes_TisInjectOptionalAttribute_t899944672_m2359032246(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))TypeExtensions_AllAttributes_TisIl2CppObject_m535975428_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<Zenject.InjectOptionalAttribute>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToList_TisInjectOptionalAttribute_t899944672_m3844690613(__this /* static, unused */, p0, method) ((  List_1_t1696903641 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToList_TisIl2CppObject_m4082139931_gshared)(__this /* static, unused */, p0, method)
// System.Boolean ModestTree.LinqExtensions::IsEmpty<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  bool LinqExtensions_IsEmpty_TisIl2CppObject_m3564374429_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define LinqExtensions_IsEmpty_TisIl2CppObject_m3564374429(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))LinqExtensions_IsEmpty_TisIl2CppObject_m3564374429_gshared)(__this /* static, unused */, p0, method)
// System.Boolean ModestTree.LinqExtensions::IsEmpty<Zenject.InjectAttribute>(System.Collections.Generic.IEnumerable`1<!!0>)
#define LinqExtensions_IsEmpty_TisInjectAttribute_t2791321440_m3377630699(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))LinqExtensions_IsEmpty_TisIl2CppObject_m3564374429_gshared)(__this /* static, unused */, p0, method)
// System.Boolean ModestTree.LinqExtensions::IsEmpty<Zenject.InjectOptionalAttribute>(System.Collections.Generic.IEnumerable`1<!!0>)
#define LinqExtensions_IsEmpty_TisInjectOptionalAttribute_t899944672_m428018539(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))LinqExtensions_IsEmpty_TisIl2CppObject_m3564374429_gshared)(__this /* static, unused */, p0, method)
// System.Boolean System.Linq.Enumerable::Any<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  bool Enumerable_Any_TisIl2CppObject_m3411867191_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_Any_TisIl2CppObject_m3411867191(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_Any_TisIl2CppObject_m3411867191_gshared)(__this /* static, unused */, p0, method)
// System.Boolean System.Linq.Enumerable::Any<Zenject.InjectAttribute>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_Any_TisInjectAttribute_t2791321440_m2761425413(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_Any_TisIl2CppObject_m3411867191_gshared)(__this /* static, unused */, p0, method)
// !!0 System.Linq.Enumerable::Single<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  Il2CppObject * Enumerable_Single_TisIl2CppObject_m3651905832_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_Single_TisIl2CppObject_m3651905832(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_Single_TisIl2CppObject_m3651905832_gshared)(__this /* static, unused */, p0, method)
// !!0 System.Linq.Enumerable::Single<Zenject.InjectAttribute>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_Single_TisInjectAttribute_t2791321440_m3640425398(__this /* static, unused */, p0, method) ((  InjectAttribute_t2791321440 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_Single_TisIl2CppObject_m3651905832_gshared)(__this /* static, unused */, p0, method)
// System.Boolean System.Linq.Enumerable::Any<Zenject.InjectOptionalAttribute>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_Any_TisInjectOptionalAttribute_t899944672_m4043818373(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_Any_TisIl2CppObject_m3411867191_gshared)(__this /* static, unused */, p0, method)
// !!0 System.Linq.Enumerable::Single<Zenject.InjectOptionalAttribute>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_Single_TisInjectOptionalAttribute_t899944672_m1563688502(__this /* static, unused */, p0, method) ((  InjectOptionalAttribute_t899944672 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_Single_TisIl2CppObject_m3651905832_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<System.Reflection.MethodInfo>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Enumerable_Where_TisMethodInfo_t_m4151038873(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t3166396148 *, const MethodInfo*))Enumerable_Where_TisIl2CppObject_m3480373697_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Reflection.MethodInfo>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToList_TisMethodInfo_t_m285312770(__this /* static, unused */, p0, method) ((  List_1_t4258180246 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToList_TisIl2CppObject_m4082139931_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.IEnumerable`1<!!0> ModestTree.MiscExtensions::Yield<System.Object>(!!0)
extern "C"  Il2CppObject* MiscExtensions_Yield_TisIl2CppObject_m2148863306_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define MiscExtensions_Yield_TisIl2CppObject_m2148863306(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))MiscExtensions_Yield_TisIl2CppObject_m2148863306_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.IEnumerable`1<!!0> ModestTree.MiscExtensions::Yield<System.Type>(!!0)
#define MiscExtensions_Yield_TisType_t_m2837271301(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Type_t *, const MethodInfo*))MiscExtensions_Yield_TisIl2CppObject_m2148863306_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Concat<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  Il2CppObject* Enumerable_Concat_TisIl2CppObject_m3543084802_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Il2CppObject* p1, const MethodInfo* method);
#define Enumerable_Concat_TisIl2CppObject_m3543084802(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Il2CppObject*, const MethodInfo*))Enumerable_Concat_TisIl2CppObject_m3543084802_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Concat<System.Type>(System.Collections.Generic.IEnumerable`1<!!0>,System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_Concat_TisType_t_m2838388285(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Il2CppObject*, const MethodInfo*))Enumerable_Concat_TisIl2CppObject_m3543084802_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Reverse<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  Il2CppObject* Enumerable_Reverse_TisIl2CppObject_m433718661_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_Reverse_TisIl2CppObject_m433718661(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_Reverse_TisIl2CppObject_m433718661_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Reverse<System.Type>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_Reverse_TisType_t_m567116288(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_Reverse_TisIl2CppObject_m433718661_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Type>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToList_TisType_t_m3338730594(__this /* static, unused */, p0, method) ((  List_1_t3576188904 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToList_TisIl2CppObject_m4082139931_gshared)(__this /* static, unused */, p0, method)
// System.Linq.IOrderedEnumerable`1<!!0> System.Linq.Enumerable::OrderBy<System.Object,System.Int32>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
extern "C"  Il2CppObject* Enumerable_OrderBy_TisIl2CppObject_TisInt32_t2847414787_m4229321362_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t4146091719 * p1, const MethodInfo* method);
#define Enumerable_OrderBy_TisIl2CppObject_TisInt32_t2847414787_m4229321362(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t4146091719 *, const MethodInfo*))Enumerable_OrderBy_TisIl2CppObject_TisInt32_t2847414787_m4229321362_gshared)(__this /* static, unused */, p0, p1, method)
// System.Linq.IOrderedEnumerable`1<!!0> System.Linq.Enumerable::OrderBy<System.Reflection.MethodInfo,System.Int32>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
#define Enumerable_OrderBy_TisMethodInfo_t_TisInt32_t2847414787_m4018660381(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1507838298 *, const MethodInfo*))Enumerable_OrderBy_TisIl2CppObject_TisInt32_t2847414787_m4229321362_gshared)(__this /* static, unused */, p0, p1, method)
// System.Boolean ModestTree.LinqExtensions::IsEmpty<System.Reflection.ConstructorInfo>(System.Collections.Generic.IEnumerable`1<!!0>)
#define LinqExtensions_IsEmpty_TisConstructorInfo_t3542137334_m3362315587(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))LinqExtensions_IsEmpty_TisIl2CppObject_m3564374429_gshared)(__this /* static, unused */, p0, method)
// System.Boolean ModestTree.LinqExtensions::HasMoreThan<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Int32)
extern "C"  bool LinqExtensions_HasMoreThan_TisIl2CppObject_m1282941229_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, int32_t p1, const MethodInfo* method);
#define LinqExtensions_HasMoreThan_TisIl2CppObject_m1282941229(__this /* static, unused */, p0, p1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, int32_t, const MethodInfo*))LinqExtensions_HasMoreThan_TisIl2CppObject_m1282941229_gshared)(__this /* static, unused */, p0, p1, method)
// System.Boolean ModestTree.LinqExtensions::HasMoreThan<System.Reflection.ConstructorInfo>(System.Collections.Generic.IEnumerable`1<!!0>,System.Int32)
#define LinqExtensions_HasMoreThan_TisConstructorInfo_t3542137334_m2138457031(__this /* static, unused */, p0, p1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, int32_t, const MethodInfo*))LinqExtensions_HasMoreThan_TisIl2CppObject_m1282941229_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<System.Reflection.ConstructorInfo>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Enumerable_Where_TisConstructorInfo_t3542137334_m963787402(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t775214871 *, const MethodInfo*))Enumerable_Where_TisIl2CppObject_m3480373697_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 System.Linq.Enumerable::SingleOrDefault<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  Il2CppObject * Enumerable_SingleOrDefault_TisIl2CppObject_m3274741330_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_SingleOrDefault_TisIl2CppObject_m3274741330(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_SingleOrDefault_TisIl2CppObject_m3274741330_gshared)(__this /* static, unused */, p0, method)
// !!0 System.Linq.Enumerable::SingleOrDefault<System.Reflection.ConstructorInfo>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_SingleOrDefault_TisConstructorInfo_t3542137334_m2408381816(__this /* static, unused */, p0, method) ((  ConstructorInfo_t3542137334 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_SingleOrDefault_TisIl2CppObject_m3274741330_gshared)(__this /* static, unused */, p0, method)
// System.Boolean ModestTree.TypeExtensions::HasAttribute<System.Object>(System.Reflection.ICustomAttributeProvider)
extern "C"  bool TypeExtensions_HasAttribute_TisIl2CppObject_m1870692646_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define TypeExtensions_HasAttribute_TisIl2CppObject_m1870692646(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))TypeExtensions_HasAttribute_TisIl2CppObject_m1870692646_gshared)(__this /* static, unused */, p0, method)
// System.Boolean ModestTree.TypeExtensions::HasAttribute<Zenject.InjectAttribute>(System.Reflection.ICustomAttributeProvider)
#define TypeExtensions_HasAttribute_TisInjectAttribute_t2791321440_m664968408(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))TypeExtensions_HasAttribute_TisIl2CppObject_m1870692646_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<System.Reflection.FieldInfo>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Enumerable_Where_TisFieldInfo_t_m107125898(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t2642952727 *, const MethodInfo*))Enumerable_Where_TisIl2CppObject_m3480373697_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<System.Reflection.PropertyInfo>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Enumerable_Where_TisPropertyInfo_t_m1174545549(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t2846737840 *, const MethodInfo*))Enumerable_Where_TisIl2CppObject_m3480373697_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Concat<Zenject.InjectableInfo>(System.Collections.Generic.IEnumerable`1<!!0>,System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_Concat_TisInjectableInfo_t1147709774_m1287063574(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Il2CppObject*, const MethodInfo*))Enumerable_Concat_TisIl2CppObject_m3543084802_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::SelectMany<System.Object,System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Collections.Generic.IEnumerable`1<!!1>>)
extern "C"  Il2CppObject* Enumerable_SelectMany_TisIl2CppObject_TisIl2CppObject_m2900030484_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t712970412 * p1, const MethodInfo* method);
#define Enumerable_SelectMany_TisIl2CppObject_TisIl2CppObject_m2900030484(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t712970412 *, const MethodInfo*))Enumerable_SelectMany_TisIl2CppObject_TisIl2CppObject_m2900030484_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::SelectMany<Zenject.PostInjectableInfo,Zenject.InjectableInfo>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Collections.Generic.IEnumerable`1<!!1>>)
#define Enumerable_SelectMany_TisPostInjectableInfo_t3080283662_TisInjectableInfo_t1147709774_m2380885368(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t2429407812 *, const MethodInfo*))Enumerable_SelectMany_TisIl2CppObject_TisIl2CppObject_m2900030484_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Except<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  Il2CppObject* Enumerable_Except_TisIl2CppObject_m1682104711_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Il2CppObject* p1, const MethodInfo* method);
#define Enumerable_Except_TisIl2CppObject_m1682104711(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Il2CppObject*, const MethodInfo*))Enumerable_Except_TisIl2CppObject_m1682104711_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Except<UnityEngine.GameObject>(System.Collections.Generic.IEnumerable`1<!!0>,System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_Except_TisGameObject_t4012695102_m3386116033(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Il2CppObject*, const MethodInfo*))Enumerable_Except_TisIl2CppObject_m1682104711_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared (GameObject_t4012695102 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2447772384(__this, method) ((  Il2CppObject * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<Zenject.SceneCompositionRoot>()
#define GameObject_GetComponent_TisSceneCompositionRoot_t1259693845_m758702398(__this, method) ((  SceneCompositionRoot_t1259693845 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// UniRx.IObservable`1<!!0> UniRx.Observable::Where<UnityEngine.Vector2>(UniRx.IObservable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C"  Il2CppObject* Observable_Where_TisVector2_t3525329788_m902023148_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t4239542457 * p1, const MethodInfo* method);
#define Observable_Where_TisVector2_t3525329788_m902023148(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t4239542457 *, const MethodInfo*))Observable_Where_TisVector2_t3525329788_m902023148_gshared)(__this /* static, unused */, p0, p1, method)
// UniRx.IObservable`1<!!0> UniRx.Observable::Where<System.Int64>(UniRx.IObservable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C"  Il2CppObject* Observable_Where_TisInt64_t2847414882_m2960877922_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t2251336571 * p1, const MethodInfo* method);
#define Observable_Where_TisInt64_t2847414882_m2960877922(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t2251336571 *, const MethodInfo*))Observable_Where_TisInt64_t2847414882_m2960877922_gshared)(__this /* static, unused */, p0, p1, method)
// UniRx.IObservable`1<!!1> UniRx.Observable::Select<System.Int64,UnityEngine.Vector2>(UniRx.IObservable`1<!!0>,System.Func`2<!!0,!!1>)
extern "C"  Il2CppObject* Observable_Select_TisInt64_t2847414882_TisVector2_t3525329788_m584888666_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t1270693722 * p1, const MethodInfo* method);
#define Observable_Select_TisInt64_t2847414882_TisVector2_t3525329788_m584888666(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1270693722 *, const MethodInfo*))Observable_Select_TisInt64_t2847414882_TisVector2_t3525329788_m584888666_gshared)(__this /* static, unused */, p0, p1, method)
// System.Boolean System.Linq.Enumerable::Any<UnityEngine.Touch>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C"  bool Enumerable_Any_TisTouch_t1603883884_m4047459675_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t1874381577 * p1, const MethodInfo* method);
#define Enumerable_Any_TisTouch_t1603883884_m4047459675(__this /* static, unused */, p0, p1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1874381577 *, const MethodInfo*))Enumerable_Any_TisTouch_t1603883884_m4047459675_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 System.Linq.Enumerable::First<UnityEngine.Touch>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C"  Touch_t1603883884  Enumerable_First_TisTouch_t1603883884_m2423823600_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t1874381577 * p1, const MethodInfo* method);
#define Enumerable_First_TisTouch_t1603883884_m2423823600(__this /* static, unused */, p0, p1, method) ((  Touch_t1603883884  (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1874381577 *, const MethodInfo*))Enumerable_First_TisTouch_t1603883884_m2423823600_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 UnityEngine.Component::GetComponentInParent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponentInParent_TisIl2CppObject_m1297875695_gshared (Component_t2126946602 * __this, const MethodInfo* method);
#define Component_GetComponentInParent_TisIl2CppObject_m1297875695(__this, method) ((  Il2CppObject * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponentInParent_TisIl2CppObject_m1297875695_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponentInParent<UnityEngine.Canvas>()
#define Component_GetComponentInParent_TisCanvas_t3534013893_m1800982117(__this, method) ((  Canvas_t3534013893 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponentInParent_TisIl2CppObject_m1297875695_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.SingletonInstanceHelper::.ctor()
extern "C"  void SingletonInstanceHelper__ctor_m581640156 (SingletonInstanceHelper_t833281251 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Zenject.SingletonLazyCreator::.ctor(Zenject.DiContainer,Zenject.SingletonProviderMap,Zenject.SingletonId,System.Func`2<Zenject.InjectContext,System.Object>)
extern "C"  void SingletonLazyCreator__ctor_m2760220428 (SingletonLazyCreator_t2762284194 * __this, DiContainer_t2383114449 * ___container0, SingletonProviderMap_t1557411893 * ___owner1, SingletonId_t1838183899 * ___id2, Func_2_t2621245597 * ___createMethod3, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		DiContainer_t2383114449 * L_0 = ___container0;
		__this->set__container_0(L_0);
		SingletonProviderMap_t1557411893 * L_1 = ___owner1;
		__this->set__owner_1(L_1);
		SingletonId_t1838183899 * L_2 = ___id2;
		__this->set__id_3(L_2);
		Func_2_t2621245597 * L_3 = ___createMethod3;
		__this->set__createMethod_2(L_3);
		return;
	}
}
// System.Void Zenject.SingletonLazyCreator::.ctor(Zenject.DiContainer,Zenject.SingletonProviderMap,Zenject.SingletonId)
extern "C"  void SingletonLazyCreator__ctor_m1748858214 (SingletonLazyCreator_t2762284194 * __this, DiContainer_t2383114449 * ___container0, SingletonProviderMap_t1557411893 * ___owner1, SingletonId_t1838183899 * ___id2, const MethodInfo* method)
{
	{
		DiContainer_t2383114449 * L_0 = ___container0;
		SingletonProviderMap_t1557411893 * L_1 = ___owner1;
		SingletonId_t1838183899 * L_2 = ___id2;
		SingletonLazyCreator__ctor_m2760220428(__this, L_0, L_1, L_2, (Func_2_t2621245597 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// Zenject.SingletonId Zenject.SingletonLazyCreator::get_Id()
extern "C"  SingletonId_t1838183899 * SingletonLazyCreator_get_Id_m617832261 (SingletonLazyCreator_t2762284194 * __this, const MethodInfo* method)
{
	{
		SingletonId_t1838183899 * L_0 = __this->get__id_3();
		return L_0;
	}
}
// System.Boolean Zenject.SingletonLazyCreator::get_HasCustomCreateMethod()
extern "C"  bool SingletonLazyCreator_get_HasCustomCreateMethod_m2482092184 (SingletonLazyCreator_t2762284194 * __this, const MethodInfo* method)
{
	{
		Func_2_t2621245597 * L_0 = __this->get__createMethod_2();
		return (bool)((((int32_t)((((Il2CppObject*)(Func_2_t2621245597 *)L_0) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void Zenject.SingletonLazyCreator::IncRefCount()
extern "C"  void SingletonLazyCreator_IncRefCount_m1541995047 (SingletonLazyCreator_t2762284194 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get__referenceCount_4();
		__this->set__referenceCount_4(((int32_t)((int32_t)L_0+(int32_t)1)));
		return;
	}
}
// System.Void Zenject.SingletonLazyCreator::DecRefCount()
extern "C"  void SingletonLazyCreator_DecRefCount_m1847265611 (SingletonLazyCreator_t2762284194 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get__referenceCount_4();
		__this->set__referenceCount_4(((int32_t)((int32_t)L_0-(int32_t)1)));
		int32_t L_1 = __this->get__referenceCount_4();
		if ((((int32_t)L_1) > ((int32_t)0)))
		{
			goto IL_002b;
		}
	}
	{
		SingletonProviderMap_t1557411893 * L_2 = __this->get__owner_1();
		SingletonId_t1838183899 * L_3 = __this->get__id_3();
		NullCheck(L_2);
		SingletonProviderMap_RemoveCreator_m1625230441(L_2, L_3, /*hidden argument*/NULL);
	}

IL_002b:
	{
		return;
	}
}
// System.Void Zenject.SingletonLazyCreator::SetInstance(System.Object)
extern "C"  void SingletonLazyCreator_SetInstance_m332882000 (SingletonLazyCreator_t2762284194 * __this, Il2CppObject * ___instance0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Il2CppObject * L_0 = __this->get__instance_5();
		Assert_IsNull_m3103527384(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___instance0;
		if (L_1)
		{
			goto IL_001e;
		}
	}
	{
		DiContainer_t2383114449 * L_2 = __this->get__container_0();
		NullCheck(L_2);
		bool L_3 = DiContainer_get_AllowNullBindings_m3648988981(L_2, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_001f;
	}

IL_001e:
	{
		G_B3_0 = 1;
	}

IL_001f:
	{
		Assert_That_m1320742921(NULL /*static, unused*/, (bool)G_B3_0, /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___instance0;
		__this->set__instance_5(L_4);
		__this->set__hasInstance_6((bool)1);
		return;
	}
}
// System.Boolean Zenject.SingletonLazyCreator::HasInstance()
extern "C"  bool SingletonLazyCreator_HasInstance_m2375219400 (SingletonLazyCreator_t2762284194 * __this, const MethodInfo* method)
{
	int32_t G_B4_0 = 0;
	{
		bool L_0 = __this->get__hasInstance_6();
		if (!L_0)
		{
			goto IL_002f;
		}
	}
	{
		DiContainer_t2383114449 * L_1 = __this->get__container_0();
		NullCheck(L_1);
		bool L_2 = DiContainer_get_AllowNullBindings_m3648988981(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0029;
		}
	}
	{
		Il2CppObject * L_3 = __this->get__instance_5();
		G_B4_0 = ((((int32_t)((((Il2CppObject*)(Il2CppObject *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_002a;
	}

IL_0029:
	{
		G_B4_0 = 1;
	}

IL_002a:
	{
		Assert_That_m1320742921(NULL /*static, unused*/, (bool)G_B4_0, /*hidden argument*/NULL);
	}

IL_002f:
	{
		bool L_4 = __this->get__hasInstance_6();
		return L_4;
	}
}
// System.Type Zenject.SingletonLazyCreator::GetInstanceType()
extern "C"  Type_t * SingletonLazyCreator_GetInstanceType_m3813166264 (SingletonLazyCreator_t2762284194 * __this, const MethodInfo* method)
{
	{
		SingletonId_t1838183899 * L_0 = __this->get__id_3();
		NullCheck(L_0);
		Type_t * L_1 = L_0->get_Type_0();
		return L_1;
	}
}
// System.Object Zenject.SingletonLazyCreator::GetInstance(Zenject.InjectContext)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* ZenjectResolveException_t1201052999_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1417891359_il2cpp_TypeInfo_var;
extern const MethodInfo* Func_2_Invoke_m1394846867_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m1502268817_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3628971126;
extern const uint32_t SingletonLazyCreator_GetInstance_m3399859088_MetadataUsageId;
extern "C"  Il2CppObject * SingletonLazyCreator_GetInstance_m3399859088 (SingletonLazyCreator_t2762284194 * __this, InjectContext_t3456483891 * ___context0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SingletonLazyCreator_GetInstance_m3399859088_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	bool V_1 = false;
	{
		bool L_0 = __this->get__hasInstance_6();
		if (L_0)
		{
			goto IL_00b4;
		}
	}
	{
		Func_2_t2621245597 * L_1 = __this->get__createMethod_2();
		if (!L_1)
		{
			goto IL_005e;
		}
	}
	{
		Func_2_t2621245597 * L_2 = __this->get__createMethod_2();
		InjectContext_t3456483891 * L_3 = ___context0;
		NullCheck(L_2);
		Il2CppObject * L_4 = Func_2_Invoke_m1394846867(L_2, L_3, /*hidden argument*/Func_2_Invoke_m1394846867_MethodInfo_var);
		__this->set__instance_5(L_4);
		Il2CppObject * L_5 = __this->get__instance_5();
		if (L_5)
		{
			goto IL_0052;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_6 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		InjectContext_t3456483891 * L_7 = ___context0;
		NullCheck(L_7);
		Type_t * L_8 = L_7->get_MemberType_6();
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		ArrayElementTypeCheck (L_6, L_8);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_8);
		String_t* L_9 = MiscExtensions_Fmt_m1348433569(NULL /*static, unused*/, _stringLiteral3628971126, L_6, /*hidden argument*/NULL);
		ZenjectResolveException_t1201052999 * L_10 = (ZenjectResolveException_t1201052999 *)il2cpp_codegen_object_new(ZenjectResolveException_t1201052999_il2cpp_TypeInfo_var);
		ZenjectResolveException__ctor_m3382034954(L_10, L_9, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
	}

IL_0052:
	{
		__this->set__hasInstance_6((bool)1);
		goto IL_00b4;
	}

IL_005e:
	{
		InjectContext_t3456483891 * L_11 = ___context0;
		NullCheck(L_11);
		Type_t * L_12 = L_11->get_MemberType_6();
		Type_t * L_13 = SingletonLazyCreator_GetTypeToInstantiate_m884638635(__this, L_12, /*hidden argument*/NULL);
		V_0 = L_13;
		V_1 = (bool)0;
		DiContainer_t2383114449 * L_14 = __this->get__container_0();
		Type_t * L_15 = V_0;
		List_1_t1417891359 * L_16 = (List_1_t1417891359 *)il2cpp_codegen_object_new(List_1_t1417891359_il2cpp_TypeInfo_var);
		List_1__ctor_m1502268817(L_16, /*hidden argument*/List_1__ctor_m1502268817_MethodInfo_var);
		InjectContext_t3456483891 * L_17 = ___context0;
		SingletonId_t1838183899 * L_18 = __this->get__id_3();
		NullCheck(L_18);
		String_t* L_19 = L_18->get_Identifier_1();
		bool L_20 = V_1;
		NullCheck(L_14);
		Il2CppObject * L_21 = VirtFuncInvoker5< Il2CppObject *, Type_t *, List_1_t1417891359 *, InjectContext_t3456483891 *, String_t*, bool >::Invoke(35 /* System.Object Zenject.DiContainer::InstantiateExplicit(System.Type,System.Collections.Generic.List`1<Zenject.TypeValuePair>,Zenject.InjectContext,System.String,System.Boolean) */, L_14, L_15, L_16, L_17, L_19, L_20);
		__this->set__instance_5(L_21);
		Il2CppObject * L_22 = __this->get__instance_5();
		Assert_IsNotNull_m3452793965(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		__this->set__hasInstance_6((bool)1);
		DiContainer_t2383114449 * L_23 = __this->get__container_0();
		Il2CppObject * L_24 = __this->get__instance_5();
		NullCheck(L_23);
		VirtActionInvoker1< Il2CppObject * >::Invoke(65 /* System.Void Zenject.DiContainer::Inject(System.Object) */, L_23, L_24);
	}

IL_00b4:
	{
		Il2CppObject * L_25 = __this->get__instance_5();
		return L_25;
	}
}
// System.Type Zenject.SingletonLazyCreator::GetTypeToInstantiate(System.Type)
extern "C"  Type_t * SingletonLazyCreator_GetTypeToInstantiate_m884638635 (SingletonLazyCreator_t2762284194 * __this, Type_t * ___contractType0, const MethodInfo* method)
{
	{
		SingletonId_t1838183899 * L_0 = __this->get__id_3();
		NullCheck(L_0);
		Type_t * L_1 = L_0->get_Type_0();
		bool L_2 = TypeExtensions_IsOpenGenericType_m3853458201(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_003d;
		}
	}
	{
		Type_t * L_3 = ___contractType0;
		NullCheck(L_3);
		bool L_4 = VirtFuncInvoker0< bool >::Invoke(21 /* System.Boolean System.Type::get_IsAbstract() */, L_3);
		Assert_That_m1320742921(NULL /*static, unused*/, (bool)((((int32_t)L_4) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		Type_t * L_5 = ___contractType0;
		NullCheck(L_5);
		Type_t * L_6 = VirtFuncInvoker0< Type_t * >::Invoke(96 /* System.Type System.Type::GetGenericTypeDefinition() */, L_5);
		SingletonId_t1838183899 * L_7 = __this->get__id_3();
		NullCheck(L_7);
		Type_t * L_8 = L_7->get_Type_0();
		Assert_That_m1320742921(NULL /*static, unused*/, (bool)((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_8))? 1 : 0), /*hidden argument*/NULL);
		Type_t * L_9 = ___contractType0;
		return L_9;
	}

IL_003d:
	{
		SingletonId_t1838183899 * L_10 = __this->get__id_3();
		NullCheck(L_10);
		Type_t * L_11 = L_10->get_Type_0();
		Type_t * L_12 = ___contractType0;
		bool L_13 = TypeExtensions_DerivesFromOrEqual_m3526502226(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		Assert_That_m1320742921(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		SingletonId_t1838183899 * L_14 = __this->get__id_3();
		NullCheck(L_14);
		Type_t * L_15 = L_14->get_Type_0();
		return L_15;
	}
}
// System.Void Zenject.SingletonProvider::.ctor(Zenject.DiContainer,Zenject.SingletonLazyCreator)
extern "C"  void SingletonProvider__ctor_m1411291507 (SingletonProvider_t585108081 * __this, DiContainer_t2383114449 * ___container0, SingletonLazyCreator_t2762284194 * ___creator1, const MethodInfo* method)
{
	{
		ProviderBase__ctor_m796348314(__this, /*hidden argument*/NULL);
		SingletonLazyCreator_t2762284194 * L_0 = ___creator1;
		__this->set__creator_2(L_0);
		DiContainer_t2383114449 * L_1 = ___container0;
		__this->set__container_3(L_1);
		return;
	}
}
// System.Void Zenject.SingletonProvider::Dispose()
extern "C"  void SingletonProvider_Dispose_m2647897099 (SingletonProvider_t585108081 * __this, const MethodInfo* method)
{
	{
		SingletonLazyCreator_t2762284194 * L_0 = __this->get__creator_2();
		NullCheck(L_0);
		SingletonLazyCreator_DecRefCount_m1847265611(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Type Zenject.SingletonProvider::GetInstanceType()
extern "C"  Type_t * SingletonProvider_GetInstanceType_m3993758059 (SingletonProvider_t585108081 * __this, const MethodInfo* method)
{
	{
		SingletonLazyCreator_t2762284194 * L_0 = __this->get__creator_2();
		NullCheck(L_0);
		Type_t * L_1 = SingletonLazyCreator_GetInstanceType_m3813166264(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Object Zenject.SingletonProvider::GetInstance(Zenject.InjectContext)
extern "C"  Il2CppObject * SingletonProvider_GetInstance_m3733716455 (SingletonProvider_t585108081 * __this, InjectContext_t3456483891 * ___context0, const MethodInfo* method)
{
	{
		SingletonLazyCreator_t2762284194 * L_0 = __this->get__creator_2();
		InjectContext_t3456483891 * L_1 = ___context0;
		NullCheck(L_0);
		Il2CppObject * L_2 = SingletonLazyCreator_GetInstance_m3399859088(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.SingletonProvider::ValidateBinding(Zenject.InjectContext)
extern Il2CppClass* TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var;
extern const MethodInfo* Enumerable_Empty_TisZenjectResolveException_t1201052999_m2785882006_MethodInfo_var;
extern const uint32_t SingletonProvider_ValidateBinding_m394548089_MetadataUsageId;
extern "C"  Il2CppObject* SingletonProvider_ValidateBinding_m394548089 (SingletonProvider_t585108081 * __this, InjectContext_t3456483891 * ___context0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SingletonProvider_ValidateBinding_m394548089_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SingletonLazyCreator_t2762284194 * L_0 = __this->get__creator_2();
		NullCheck(L_0);
		bool L_1 = SingletonLazyCreator_HasInstance_m2375219400(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0020;
		}
	}
	{
		SingletonLazyCreator_t2762284194 * L_2 = __this->get__creator_2();
		NullCheck(L_2);
		bool L_3 = SingletonLazyCreator_get_HasCustomCreateMethod_m2482092184(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0026;
		}
	}

IL_0020:
	{
		Il2CppObject* L_4 = Enumerable_Empty_TisZenjectResolveException_t1201052999_m2785882006(NULL /*static, unused*/, /*hidden argument*/Enumerable_Empty_TisZenjectResolveException_t1201052999_m2785882006_MethodInfo_var);
		return L_4;
	}

IL_0026:
	{
		DiContainer_t2383114449 * L_5 = __this->get__container_3();
		Type_t * L_6 = VirtFuncInvoker0< Type_t * >::Invoke(5 /* System.Type Zenject.SingletonProvider::GetInstanceType() */, __this);
		InjectContext_t3456483891 * L_7 = ___context0;
		SingletonLazyCreator_t2762284194 * L_8 = __this->get__creator_2();
		NullCheck(L_8);
		SingletonId_t1838183899 * L_9 = SingletonLazyCreator_get_Id_m617832261(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		String_t* L_10 = L_9->get_Identifier_1();
		Il2CppObject* L_11 = BindingValidator_ValidateObjectGraph_m65278683(NULL /*static, unused*/, L_5, L_6, L_7, L_10, ((TypeU5BU5D_t3431720054*)SZArrayNew(TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		return L_11;
	}
}
// System.Void Zenject.SingletonProviderMap::.ctor(Zenject.DiContainer)
extern Il2CppClass* Dictionary_2_t2265680905_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m3195789955_MethodInfo_var;
extern const uint32_t SingletonProviderMap__ctor_m1613265689_MetadataUsageId;
extern "C"  void SingletonProviderMap__ctor_m1613265689 (SingletonProviderMap_t1557411893 * __this, DiContainer_t2383114449 * ___container0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SingletonProviderMap__ctor_m1613265689_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2265680905 * L_0 = (Dictionary_2_t2265680905 *)il2cpp_codegen_object_new(Dictionary_2_t2265680905_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3195789955(L_0, /*hidden argument*/Dictionary_2__ctor_m3195789955_MethodInfo_var);
		__this->set__creators_0(L_0);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		DiContainer_t2383114449 * L_1 = ___container0;
		__this->set__container_1(L_1);
		return;
	}
}
// System.Collections.Generic.IEnumerable`1<Zenject.SingletonLazyCreator> Zenject.SingletonProviderMap::get_Creators()
extern const MethodInfo* Dictionary_2_get_Values_m3404878693_MethodInfo_var;
extern const uint32_t SingletonProviderMap_get_Creators_m919684460_MetadataUsageId;
extern "C"  Il2CppObject* SingletonProviderMap_get_Creators_m919684460 (SingletonProviderMap_t1557411893 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SingletonProviderMap_get_Creators_m919684460_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2265680905 * L_0 = __this->get__creators_0();
		NullCheck(L_0);
		ValueCollection_t4187817999 * L_1 = Dictionary_2_get_Values_m3404878693(L_0, /*hidden argument*/Dictionary_2_get_Values_m3404878693_MethodInfo_var);
		return L_1;
	}
}
// System.Void Zenject.SingletonProviderMap::RemoveCreator(Zenject.SingletonId)
extern "C"  void SingletonProviderMap_RemoveCreator_m1625230441 (SingletonProviderMap_t1557411893 * __this, SingletonId_t1838183899 * ___id0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Dictionary_2_t2265680905 * L_0 = __this->get__creators_0();
		SingletonId_t1838183899 * L_1 = ___id0;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, SingletonId_t1838183899 * >::Invoke(20 /* System.Boolean System.Collections.Generic.Dictionary`2<Zenject.SingletonId,Zenject.SingletonLazyCreator>::Remove(!0) */, L_0, L_1);
		V_0 = L_2;
		bool L_3 = V_0;
		Assert_That_m1320742921(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// Zenject.SingletonLazyCreator Zenject.SingletonProviderMap::AddCreator(Zenject.SingletonId)
extern Il2CppClass* SingletonLazyCreator_t2762284194_il2cpp_TypeInfo_var;
extern const uint32_t SingletonProviderMap_AddCreator_m3456770503_MetadataUsageId;
extern "C"  SingletonLazyCreator_t2762284194 * SingletonProviderMap_AddCreator_m3456770503 (SingletonProviderMap_t1557411893 * __this, SingletonId_t1838183899 * ___id0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SingletonProviderMap_AddCreator_m3456770503_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SingletonLazyCreator_t2762284194 * V_0 = NULL;
	{
		Dictionary_2_t2265680905 * L_0 = __this->get__creators_0();
		SingletonId_t1838183899 * L_1 = ___id0;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker2< bool, SingletonId_t1838183899 *, SingletonLazyCreator_t2762284194 ** >::Invoke(21 /* System.Boolean System.Collections.Generic.Dictionary`2<Zenject.SingletonId,Zenject.SingletonLazyCreator>::TryGetValue(!0,!1&) */, L_0, L_1, (&V_0));
		if (L_2)
		{
			goto IL_002e;
		}
	}
	{
		DiContainer_t2383114449 * L_3 = __this->get__container_1();
		SingletonId_t1838183899 * L_4 = ___id0;
		SingletonLazyCreator_t2762284194 * L_5 = (SingletonLazyCreator_t2762284194 *)il2cpp_codegen_object_new(SingletonLazyCreator_t2762284194_il2cpp_TypeInfo_var);
		SingletonLazyCreator__ctor_m1748858214(L_5, L_3, __this, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		Dictionary_2_t2265680905 * L_6 = __this->get__creators_0();
		SingletonId_t1838183899 * L_7 = ___id0;
		SingletonLazyCreator_t2762284194 * L_8 = V_0;
		NullCheck(L_6);
		VirtActionInvoker2< SingletonId_t1838183899 *, SingletonLazyCreator_t2762284194 * >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<Zenject.SingletonId,Zenject.SingletonLazyCreator>::Add(!0,!1) */, L_6, L_7, L_8);
	}

IL_002e:
	{
		SingletonLazyCreator_t2762284194 * L_9 = V_0;
		NullCheck(L_9);
		SingletonLazyCreator_IncRefCount_m1541995047(L_9, /*hidden argument*/NULL);
		SingletonLazyCreator_t2762284194 * L_10 = V_0;
		return L_10;
	}
}
// Zenject.ProviderBase Zenject.SingletonProviderMap::CreateProviderFromType(System.String,System.Type)
extern Il2CppClass* SingletonId_t1838183899_il2cpp_TypeInfo_var;
extern Il2CppClass* SingletonProvider_t585108081_il2cpp_TypeInfo_var;
extern const uint32_t SingletonProviderMap_CreateProviderFromType_m1979550400_MetadataUsageId;
extern "C"  ProviderBase_t1627494391 * SingletonProviderMap_CreateProviderFromType_m1979550400 (SingletonProviderMap_t1557411893 * __this, String_t* ___identifier0, Type_t * ___concreteType1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SingletonProviderMap_CreateProviderFromType_m1979550400_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DiContainer_t2383114449 * L_0 = __this->get__container_1();
		String_t* L_1 = ___identifier0;
		Type_t * L_2 = ___concreteType1;
		SingletonId_t1838183899 * L_3 = (SingletonId_t1838183899 *)il2cpp_codegen_object_new(SingletonId_t1838183899_il2cpp_TypeInfo_var);
		SingletonId__ctor_m351994705(L_3, L_1, L_2, /*hidden argument*/NULL);
		SingletonLazyCreator_t2762284194 * L_4 = SingletonProviderMap_AddCreator_m3456770503(__this, L_3, /*hidden argument*/NULL);
		SingletonProvider_t585108081 * L_5 = (SingletonProvider_t585108081 *)il2cpp_codegen_object_new(SingletonProvider_t585108081_il2cpp_TypeInfo_var);
		SingletonProvider__ctor_m1411291507(L_5, L_0, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// Zenject.ProviderBase Zenject.SingletonProviderMap::CreateProviderFromInstance(System.String,System.Type,System.Object)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* SingletonId_t1838183899_il2cpp_TypeInfo_var;
extern Il2CppClass* ZenjectBindException_t3550319704_il2cpp_TypeInfo_var;
extern Il2CppClass* SingletonProvider_t585108081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral929144291;
extern Il2CppCodeGenString* _stringLiteral545025033;
extern const uint32_t SingletonProviderMap_CreateProviderFromInstance_m1303240595_MetadataUsageId;
extern "C"  ProviderBase_t1627494391 * SingletonProviderMap_CreateProviderFromInstance_m1303240595 (SingletonProviderMap_t1557411893 * __this, String_t* ___identifier0, Type_t * ___concreteType1, Il2CppObject * ___instance2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SingletonProviderMap_CreateProviderFromInstance_m1303240595_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SingletonLazyCreator_t2762284194 * V_0 = NULL;
	int32_t G_B3_0 = 0;
	{
		Il2CppObject * L_0 = ___instance2;
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		DiContainer_t2383114449 * L_1 = __this->get__container_1();
		NullCheck(L_1);
		bool L_2 = DiContainer_get_AllowNullBindings_m3648988981(L_1, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_0014;
	}

IL_0013:
	{
		G_B3_0 = 1;
	}

IL_0014:
	{
		Assert_That_m1320742921(NULL /*static, unused*/, (bool)G_B3_0, /*hidden argument*/NULL);
		Il2CppObject * L_3 = ___instance2;
		if (!L_3)
		{
			goto IL_0048;
		}
	}
	{
		Il2CppObject * L_4 = ___instance2;
		NullCheck(L_4);
		Type_t * L_5 = Object_GetType_m2022236990(L_4, /*hidden argument*/NULL);
		Type_t * L_6 = ___concreteType1;
		bool L_7 = TypeExtensions_DerivesFromOrEqual_m3526502226(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_8 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)2));
		Type_t * L_9 = ___concreteType1;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_9);
		ObjectU5BU5D_t11523773* L_10 = L_8;
		Il2CppObject * L_11 = ___instance2;
		NullCheck(L_11);
		Type_t * L_12 = Object_GetType_m2022236990(L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 1);
		ArrayElementTypeCheck (L_10, L_12);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_12);
		Assert_That_m2934751473(NULL /*static, unused*/, L_7, _stringLiteral929144291, L_10, /*hidden argument*/NULL);
	}

IL_0048:
	{
		String_t* L_13 = ___identifier0;
		Type_t * L_14 = ___concreteType1;
		SingletonId_t1838183899 * L_15 = (SingletonId_t1838183899 *)il2cpp_codegen_object_new(SingletonId_t1838183899_il2cpp_TypeInfo_var);
		SingletonId__ctor_m351994705(L_15, L_13, L_14, /*hidden argument*/NULL);
		SingletonLazyCreator_t2762284194 * L_16 = SingletonProviderMap_AddCreator_m3456770503(__this, L_15, /*hidden argument*/NULL);
		V_0 = L_16;
		SingletonLazyCreator_t2762284194 * L_17 = V_0;
		NullCheck(L_17);
		bool L_18 = SingletonLazyCreator_HasInstance_m2375219400(L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_0080;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_19 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_20 = ___concreteType1;
		String_t* L_21 = TypeExtensions_Name_m3754378430(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 0);
		ArrayElementTypeCheck (L_19, L_21);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_21);
		String_t* L_22 = MiscExtensions_Fmt_m1348433569(NULL /*static, unused*/, _stringLiteral545025033, L_19, /*hidden argument*/NULL);
		ZenjectBindException_t3550319704 * L_23 = (ZenjectBindException_t3550319704 *)il2cpp_codegen_object_new(ZenjectBindException_t3550319704_il2cpp_TypeInfo_var);
		ZenjectBindException__ctor_m3054227529(L_23, L_22, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_23);
	}

IL_0080:
	{
		SingletonLazyCreator_t2762284194 * L_24 = V_0;
		Il2CppObject * L_25 = ___instance2;
		NullCheck(L_24);
		SingletonLazyCreator_SetInstance_m332882000(L_24, L_25, /*hidden argument*/NULL);
		DiContainer_t2383114449 * L_26 = __this->get__container_1();
		SingletonLazyCreator_t2762284194 * L_27 = V_0;
		SingletonProvider_t585108081 * L_28 = (SingletonProvider_t585108081 *)il2cpp_codegen_object_new(SingletonProvider_t585108081_il2cpp_TypeInfo_var);
		SingletonProvider__ctor_m1411291507(L_28, L_26, L_27, /*hidden argument*/NULL);
		return L_28;
	}
}
// System.Void Zenject.StandardUnityInstaller::.ctor()
extern "C"  void StandardUnityInstaller__ctor_m3600671948 (StandardUnityInstaller_t3603054405 * __this, const MethodInfo* method)
{
	{
		Installer__ctor_m4279559010(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Zenject.StandardUnityInstaller::InstallBindings()
extern const MethodInfo* DiContainer_Bind_TisIDependencyRoot_t2368104171_m1381176612_MethodInfo_var;
extern const MethodInfo* BinderGeneric_1_ToSingleMonoBehaviour_TisUnityDependencyRoot_t1917880567_m890979406_MethodInfo_var;
extern const MethodInfo* DiContainer_Bind_TisTickableManager_t278367883_m1598376324_MethodInfo_var;
extern const MethodInfo* DiContainer_Bind_TisInitializableManager_t1859035635_m2693675206_MethodInfo_var;
extern const MethodInfo* DiContainer_Bind_TisDisposableManager_t2094948738_m2325103725_MethodInfo_var;
extern const MethodInfo* DiContainer_Bind_TisUnityEventManager_t2255518493_m1265107186_MethodInfo_var;
extern const MethodInfo* DiContainer_Bind_TisITickable_t789512021_m3228531898_MethodInfo_var;
extern const MethodInfo* BinderGeneric_1_ToLookup_TisUnityEventManager_t2255518493_m2801832754_MethodInfo_var;
extern const uint32_t StandardUnityInstaller_InstallBindings_m2347478451_MetadataUsageId;
extern "C"  void StandardUnityInstaller_InstallBindings_m2347478451 (StandardUnityInstaller_t3603054405 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StandardUnityInstaller_InstallBindings_m2347478451_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	BinderGeneric_1_t2051703467 * G_B2_0 = NULL;
	BinderGeneric_1_t2051703467 * G_B1_0 = NULL;
	GameObject_t4012695102 * G_B3_0 = NULL;
	BinderGeneric_1_t2051703467 * G_B3_1 = NULL;
	{
		DiContainer_t2383114449 * L_0 = Installer_get_Container_m3289643822(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		BinderGeneric_1_t2051703467 * L_1 = GenericVirtFuncInvoker0< BinderGeneric_1_t2051703467 * >::Invoke(DiContainer_Bind_TisIDependencyRoot_t2368104171_m1381176612_MethodInfo_var, L_0);
		CompositionRoot_t1939928321 * L_2 = __this->get__root_1();
		bool L_3 = Object_op_Equality_m1690293457(NULL /*static, unused*/, L_2, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		G_B1_0 = L_1;
		if (!L_3)
		{
			G_B2_0 = L_1;
			goto IL_0022;
		}
	}
	{
		G_B3_0 = ((GameObject_t4012695102 *)(NULL));
		G_B3_1 = G_B1_0;
		goto IL_002d;
	}

IL_0022:
	{
		CompositionRoot_t1939928321 * L_4 = __this->get__root_1();
		NullCheck(L_4);
		GameObject_t4012695102 * L_5 = Component_get_gameObject_m2112202034(L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
	}

IL_002d:
	{
		NullCheck(G_B3_1);
		BinderGeneric_1_ToSingleMonoBehaviour_TisUnityDependencyRoot_t1917880567_m890979406(G_B3_1, G_B3_0, /*hidden argument*/BinderGeneric_1_ToSingleMonoBehaviour_TisUnityDependencyRoot_t1917880567_m890979406_MethodInfo_var);
		DiContainer_t2383114449 * L_6 = Installer_get_Container_m3289643822(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		BinderGeneric_1_t4256934475 * L_7 = GenericVirtFuncInvoker0< BinderGeneric_1_t4256934475 * >::Invoke(DiContainer_Bind_TisTickableManager_t278367883_m1598376324_MethodInfo_var, L_6);
		NullCheck(L_7);
		Binder_ToSingle_m85759326(L_7, /*hidden argument*/NULL);
		DiContainer_t2383114449 * L_8 = Installer_get_Container_m3289643822(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		BinderGeneric_1_t1542634931 * L_9 = GenericVirtFuncInvoker0< BinderGeneric_1_t1542634931 * >::Invoke(DiContainer_Bind_TisInitializableManager_t1859035635_m2693675206_MethodInfo_var, L_8);
		NullCheck(L_9);
		Binder_ToSingle_m85759326(L_9, /*hidden argument*/NULL);
		DiContainer_t2383114449 * L_10 = Installer_get_Container_m3289643822(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		BinderGeneric_1_t1778548034 * L_11 = GenericVirtFuncInvoker0< BinderGeneric_1_t1778548034 * >::Invoke(DiContainer_Bind_TisDisposableManager_t2094948738_m2325103725_MethodInfo_var, L_10);
		NullCheck(L_11);
		Binder_ToSingle_m85759326(L_11, /*hidden argument*/NULL);
		DiContainer_t2383114449 * L_12 = Installer_get_Container_m3289643822(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		BinderGeneric_1_t1939117789 * L_13 = GenericVirtFuncInvoker0< BinderGeneric_1_t1939117789 * >::Invoke(DiContainer_Bind_TisUnityEventManager_t2255518493_m1265107186_MethodInfo_var, L_12);
		NullCheck(L_13);
		Binder_ToSingleGameObject_m640566991(L_13, /*hidden argument*/NULL);
		DiContainer_t2383114449 * L_14 = Installer_get_Container_m3289643822(__this, /*hidden argument*/NULL);
		NullCheck(L_14);
		BinderGeneric_1_t473111317 * L_15 = GenericVirtFuncInvoker0< BinderGeneric_1_t473111317 * >::Invoke(DiContainer_Bind_TisITickable_t789512021_m3228531898_MethodInfo_var, L_14);
		NullCheck(L_15);
		BinderGeneric_1_ToLookup_TisUnityEventManager_t2255518493_m2801832754(L_15, /*hidden argument*/BinderGeneric_1_ToLookup_TisUnityEventManager_t2255518493_m2801832754_MethodInfo_var);
		return;
	}
}
// System.Void Zenject.TickableManager::.ctor()
extern "C"  void TickableManager__ctor_m2585051700 (TickableManager_t278367883 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Zenject.TickableManager::Initialize()
extern "C"  void TickableManager_Initialize_m733795552 (TickableManager_t278367883 * __this, const MethodInfo* method)
{
	{
		TickableManager_InitTickables_m2762782462(__this, /*hidden argument*/NULL);
		TickableManager_InitFixedTickables_m557236040(__this, /*hidden argument*/NULL);
		TickableManager_InitLateTickables_m1376374776(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get__warnForMissing_10();
		if (!L_0)
		{
			goto IL_0023;
		}
	}
	{
		TickableManager_WarnForMissingBindings_m956088769(__this, /*hidden argument*/NULL);
	}

IL_0023:
	{
		return;
	}
}
// System.Void Zenject.TickableManager::WarnForMissingBindings()
extern Il2CppClass* TickableManager_t278367883_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t1456098478_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_1_t1356416995_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t4262336383_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* TickableManager_U3CWarnForMissingBindingsU3Em__2E7_m2179551731_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m26258046_MethodInfo_var;
extern const MethodInfo* Enumerable_Select_TisITickable_t789512021_TisType_t_m1604350288_MethodInfo_var;
extern const MethodInfo* Enumerable_Distinct_TisType_t_m1138943728_MethodInfo_var;
extern const MethodInfo* SingletonInstanceHelper_GetActiveSingletonTypesDerivingFrom_TisITickable_t789512021_m3238426240_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2626824578;
extern Il2CppCodeGenString* _stringLiteral39;
extern const uint32_t TickableManager_WarnForMissingBindings_m956088769_MetadataUsageId;
extern "C"  void TickableManager_WarnForMissingBindings_m956088769 (TickableManager_t278367883 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TickableManager_WarnForMissingBindings_m956088769_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Type_t * V_2 = NULL;
	Il2CppObject* V_3 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	List_1_t1586470990 * G_B2_0 = NULL;
	List_1_t1586470990 * G_B1_0 = NULL;
	{
		List_1_t1586470990 * L_0 = __this->get__tickables_0();
		Func_2_t1456098478 * L_1 = ((TickableManager_t278367883_StaticFields*)TickableManager_t278367883_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheB_11();
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_001e;
		}
	}
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)TickableManager_U3CWarnForMissingBindingsU3Em__2E7_m2179551731_MethodInfo_var);
		Func_2_t1456098478 * L_3 = (Func_2_t1456098478 *)il2cpp_codegen_object_new(Func_2_t1456098478_il2cpp_TypeInfo_var);
		Func_2__ctor_m26258046(L_3, NULL, L_2, /*hidden argument*/Func_2__ctor_m26258046_MethodInfo_var);
		((TickableManager_t278367883_StaticFields*)TickableManager_t278367883_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cacheB_11(L_3);
		G_B2_0 = G_B1_0;
	}

IL_001e:
	{
		Func_2_t1456098478 * L_4 = ((TickableManager_t278367883_StaticFields*)TickableManager_t278367883_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheB_11();
		Il2CppObject* L_5 = Enumerable_Select_TisITickable_t789512021_TisType_t_m1604350288(NULL /*static, unused*/, G_B2_0, L_4, /*hidden argument*/Enumerable_Select_TisITickable_t789512021_TisType_t_m1604350288_MethodInfo_var);
		Il2CppObject* L_6 = Enumerable_Distinct_TisType_t_m1138943728(NULL /*static, unused*/, L_5, /*hidden argument*/Enumerable_Distinct_TisType_t_m1138943728_MethodInfo_var);
		V_0 = L_6;
		SingletonInstanceHelper_t833281251 * L_7 = __this->get__singletonInstanceHelper_6();
		Il2CppObject* L_8 = V_0;
		NullCheck(L_7);
		Il2CppObject* L_9 = SingletonInstanceHelper_GetActiveSingletonTypesDerivingFrom_TisITickable_t789512021_m3238426240(L_7, L_8, /*hidden argument*/SingletonInstanceHelper_GetActiveSingletonTypesDerivingFrom_TisITickable_t789512021_m3238426240_MethodInfo_var);
		V_1 = L_9;
		Il2CppObject* L_10 = V_1;
		NullCheck(L_10);
		Il2CppObject* L_11 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Type>::GetEnumerator() */, IEnumerable_1_t1356416995_il2cpp_TypeInfo_var, L_10);
		V_3 = L_11;
	}

IL_0042:
	try
	{ // begin try (depth: 1)
		{
			goto IL_006e;
		}

IL_0047:
		{
			Il2CppObject* L_12 = V_3;
			NullCheck(L_12);
			Type_t * L_13 = InterfaceFuncInvoker0< Type_t * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Type>::get_Current() */, IEnumerator_1_t4262336383_il2cpp_TypeInfo_var, L_12);
			V_2 = L_13;
			Type_t * L_14 = V_2;
			String_t* L_15 = TypeExtensions_Name_m3754378430(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_16 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral2626824578, L_15, _stringLiteral39, /*hidden argument*/NULL);
			Log_Warn_m626914343(NULL /*static, unused*/, L_16, ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		}

IL_006e:
		{
			Il2CppObject* L_17 = V_3;
			NullCheck(L_17);
			bool L_18 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_17);
			if (L_18)
			{
				goto IL_0047;
			}
		}

IL_0079:
		{
			IL2CPP_LEAVE(0x89, FINALLY_007e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_007e;
	}

FINALLY_007e:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_19 = V_3;
			if (L_19)
			{
				goto IL_0082;
			}
		}

IL_0081:
		{
			IL2CPP_END_FINALLY(126)
		}

IL_0082:
		{
			Il2CppObject* L_20 = V_3;
			NullCheck(L_20);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_20);
			IL2CPP_END_FINALLY(126)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(126)
	{
		IL2CPP_JUMP_TBL(0x89, IL_0089)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0089:
	{
		return;
	}
}
// System.Void Zenject.TickableManager::InitFixedTickables()
extern Il2CppClass* Action_1_t3476844312_il2cpp_TypeInfo_var;
extern Il2CppClass* TaskUpdater_1_t4197280854_il2cpp_TypeInfo_var;
extern Il2CppClass* TickableManager_t278367883_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t1899971632_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_1_t1356416995_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t4262336383_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppClass* U3CInitFixedTickablesU3Ec__AnonStorey192_t1515684493_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t3626714334_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t1968156484_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t2211133568_il2cpp_TypeInfo_var;
extern const MethodInfo* TickableManager_UpdateFixedTickable_m2742900371_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m4223608894_MethodInfo_var;
extern const MethodInfo* TaskUpdater_1__ctor_m2133350137_MethodInfo_var;
extern const MethodInfo* TickableManager_U3CInitFixedTickablesU3Em__2E8_m3385276832_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m41055557_MethodInfo_var;
extern const MethodInfo* Enumerable_Select_TisTuple_2_t3719549051_TisType_t_m1035253335_MethodInfo_var;
extern const MethodInfo* TypeExtensions_DerivesFrom_TisIFixedTickable_t3328391607_m3119328223_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m4279539545_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1297884997_MethodInfo_var;
extern const MethodInfo* U3CInitFixedTickablesU3Ec__AnonStorey192_U3CU3Em__2E9_m2563333851_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m2850122579_MethodInfo_var;
extern const MethodInfo* Enumerable_Where_TisTuple_2_t3719549051_m3323201792_MethodInfo_var;
extern const MethodInfo* TickableManager_U3CInitFixedTickablesU3Em__2EA_m1451350763_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m1691609133_MethodInfo_var;
extern const MethodInfo* Enumerable_Select_TisTuple_2_t3719549051_TisInt32_t2847414787_m2850307189_MethodInfo_var;
extern const MethodInfo* Enumerable_ToList_TisInt32_t2847414787_m219610782_MethodInfo_var;
extern const MethodInfo* LinqExtensions_IsEmpty_TisInt32_t2847414787_m576996776_MethodInfo_var;
extern const MethodInfo* Enumerable_Single_TisInt32_t2847414787_m4182051005_MethodInfo_var;
extern const MethodInfo* TaskUpdater_1_AddTask_m1413840662_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m3740477111_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2561555882;
extern const uint32_t TickableManager_InitFixedTickables_m557236040_MetadataUsageId;
extern "C"  void TickableManager_InitFixedTickables_m557236040 (TickableManager_t278367883 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TickableManager_InitFixedTickables_m557236040_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Enumerator_t2211133568  V_2;
	memset(&V_2, 0, sizeof(V_2));
	List_1_t3644373756 * V_3 = NULL;
	int32_t V_4 = 0;
	U3CInitFixedTickablesU3Ec__AnonStorey192_t1515684493 * V_5 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	List_1_t221540724 * G_B2_0 = NULL;
	List_1_t221540724 * G_B1_0 = NULL;
	Il2CppObject* G_B14_0 = NULL;
	Il2CppObject* G_B13_0 = NULL;
	int32_t G_B17_0 = 0;
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)TickableManager_UpdateFixedTickable_m2742900371_MethodInfo_var);
		Action_1_t3476844312 * L_1 = (Action_1_t3476844312 *)il2cpp_codegen_object_new(Action_1_t3476844312_il2cpp_TypeInfo_var);
		Action_1__ctor_m4223608894(L_1, __this, L_0, /*hidden argument*/Action_1__ctor_m4223608894_MethodInfo_var);
		TaskUpdater_1_t4197280854 * L_2 = (TaskUpdater_1_t4197280854 *)il2cpp_codegen_object_new(TaskUpdater_1_t4197280854_il2cpp_TypeInfo_var);
		TaskUpdater_1__ctor_m2133350137(L_2, L_1, /*hidden argument*/TaskUpdater_1__ctor_m2133350137_MethodInfo_var);
		__this->set__fixedUpdater_8(L_2);
		List_1_t221540724 * L_3 = __this->get__fixedPriorities_4();
		Func_2_t1899971632 * L_4 = ((TickableManager_t278367883_StaticFields*)TickableManager_t278367883_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheC_12();
		G_B1_0 = L_3;
		if (L_4)
		{
			G_B2_0 = L_3;
			goto IL_0035;
		}
	}
	{
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)TickableManager_U3CInitFixedTickablesU3Em__2E8_m3385276832_MethodInfo_var);
		Func_2_t1899971632 * L_6 = (Func_2_t1899971632 *)il2cpp_codegen_object_new(Func_2_t1899971632_il2cpp_TypeInfo_var);
		Func_2__ctor_m41055557(L_6, NULL, L_5, /*hidden argument*/Func_2__ctor_m41055557_MethodInfo_var);
		((TickableManager_t278367883_StaticFields*)TickableManager_t278367883_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cacheC_12(L_6);
		G_B2_0 = G_B1_0;
	}

IL_0035:
	{
		Func_2_t1899971632 * L_7 = ((TickableManager_t278367883_StaticFields*)TickableManager_t278367883_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheC_12();
		Il2CppObject* L_8 = Enumerable_Select_TisTuple_2_t3719549051_TisType_t_m1035253335(NULL /*static, unused*/, G_B2_0, L_7, /*hidden argument*/Enumerable_Select_TisTuple_2_t3719549051_TisType_t_m1035253335_MethodInfo_var);
		NullCheck(L_8);
		Il2CppObject* L_9 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Type>::GetEnumerator() */, IEnumerable_1_t1356416995_il2cpp_TypeInfo_var, L_8);
		V_1 = L_9;
	}

IL_0045:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0070;
		}

IL_004a:
		{
			Il2CppObject* L_10 = V_1;
			NullCheck(L_10);
			Type_t * L_11 = InterfaceFuncInvoker0< Type_t * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Type>::get_Current() */, IEnumerator_1_t4262336383_il2cpp_TypeInfo_var, L_10);
			V_0 = L_11;
			Type_t * L_12 = V_0;
			bool L_13 = TypeExtensions_DerivesFrom_TisIFixedTickable_t3328391607_m3119328223(NULL /*static, unused*/, L_12, /*hidden argument*/TypeExtensions_DerivesFrom_TisIFixedTickable_t3328391607_m3119328223_MethodInfo_var);
			ObjectU5BU5D_t11523773* L_14 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
			Type_t * L_15 = V_0;
			String_t* L_16 = TypeExtensions_Name_m3754378430(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
			NullCheck(L_14);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 0);
			ArrayElementTypeCheck (L_14, L_16);
			(L_14)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_16);
			Assert_That_m2934751473(NULL /*static, unused*/, L_13, _stringLiteral2561555882, L_14, /*hidden argument*/NULL);
		}

IL_0070:
		{
			Il2CppObject* L_17 = V_1;
			NullCheck(L_17);
			bool L_18 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_17);
			if (L_18)
			{
				goto IL_004a;
			}
		}

IL_007b:
		{
			IL2CPP_LEAVE(0x8B, FINALLY_0080);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0080;
	}

FINALLY_0080:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_19 = V_1;
			if (L_19)
			{
				goto IL_0084;
			}
		}

IL_0083:
		{
			IL2CPP_END_FINALLY(128)
		}

IL_0084:
		{
			Il2CppObject* L_20 = V_1;
			NullCheck(L_20);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_20);
			IL2CPP_END_FINALLY(128)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(128)
	{
		IL2CPP_JUMP_TBL(0x8B, IL_008b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_008b:
	{
		U3CInitFixedTickablesU3Ec__AnonStorey192_t1515684493 * L_21 = (U3CInitFixedTickablesU3Ec__AnonStorey192_t1515684493 *)il2cpp_codegen_object_new(U3CInitFixedTickablesU3Ec__AnonStorey192_t1515684493_il2cpp_TypeInfo_var);
		U3CInitFixedTickablesU3Ec__AnonStorey192__ctor_m4133438902(L_21, /*hidden argument*/NULL);
		V_5 = L_21;
		List_1_t4125350576 * L_22 = __this->get__fixedTickables_1();
		NullCheck(L_22);
		Enumerator_t2211133568  L_23 = List_1_GetEnumerator_m4279539545(L_22, /*hidden argument*/List_1_GetEnumerator_m4279539545_MethodInfo_var);
		V_2 = L_23;
	}

IL_009e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_011e;
		}

IL_00a3:
		{
			U3CInitFixedTickablesU3Ec__AnonStorey192_t1515684493 * L_24 = V_5;
			Il2CppObject * L_25 = Enumerator_get_Current_m1297884997((&V_2), /*hidden argument*/Enumerator_get_Current_m1297884997_MethodInfo_var);
			NullCheck(L_24);
			L_24->set_tickable_0(L_25);
			List_1_t221540724 * L_26 = __this->get__fixedPriorities_4();
			U3CInitFixedTickablesU3Ec__AnonStorey192_t1515684493 * L_27 = V_5;
			IntPtr_t L_28;
			L_28.set_m_value_0((void*)(void*)U3CInitFixedTickablesU3Ec__AnonStorey192_U3CU3Em__2E9_m2563333851_MethodInfo_var);
			Func_2_t3626714334 * L_29 = (Func_2_t3626714334 *)il2cpp_codegen_object_new(Func_2_t3626714334_il2cpp_TypeInfo_var);
			Func_2__ctor_m2850122579(L_29, L_27, L_28, /*hidden argument*/Func_2__ctor_m2850122579_MethodInfo_var);
			Il2CppObject* L_30 = Enumerable_Where_TisTuple_2_t3719549051_m3323201792(NULL /*static, unused*/, L_26, L_29, /*hidden argument*/Enumerable_Where_TisTuple_2_t3719549051_m3323201792_MethodInfo_var);
			Func_2_t1968156484 * L_31 = ((TickableManager_t278367883_StaticFields*)TickableManager_t278367883_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheD_13();
			G_B13_0 = L_30;
			if (L_31)
			{
				G_B14_0 = L_30;
				goto IL_00e1;
			}
		}

IL_00d0:
		{
			IntPtr_t L_32;
			L_32.set_m_value_0((void*)(void*)TickableManager_U3CInitFixedTickablesU3Em__2EA_m1451350763_MethodInfo_var);
			Func_2_t1968156484 * L_33 = (Func_2_t1968156484 *)il2cpp_codegen_object_new(Func_2_t1968156484_il2cpp_TypeInfo_var);
			Func_2__ctor_m1691609133(L_33, NULL, L_32, /*hidden argument*/Func_2__ctor_m1691609133_MethodInfo_var);
			((TickableManager_t278367883_StaticFields*)TickableManager_t278367883_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cacheD_13(L_33);
			G_B14_0 = G_B13_0;
		}

IL_00e1:
		{
			Func_2_t1968156484 * L_34 = ((TickableManager_t278367883_StaticFields*)TickableManager_t278367883_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheD_13();
			Il2CppObject* L_35 = Enumerable_Select_TisTuple_2_t3719549051_TisInt32_t2847414787_m2850307189(NULL /*static, unused*/, G_B14_0, L_34, /*hidden argument*/Enumerable_Select_TisTuple_2_t3719549051_TisInt32_t2847414787_m2850307189_MethodInfo_var);
			List_1_t3644373756 * L_36 = Enumerable_ToList_TisInt32_t2847414787_m219610782(NULL /*static, unused*/, L_35, /*hidden argument*/Enumerable_ToList_TisInt32_t2847414787_m219610782_MethodInfo_var);
			V_3 = L_36;
			List_1_t3644373756 * L_37 = V_3;
			bool L_38 = LinqExtensions_IsEmpty_TisInt32_t2847414787_m576996776(NULL /*static, unused*/, L_37, /*hidden argument*/LinqExtensions_IsEmpty_TisInt32_t2847414787_m576996776_MethodInfo_var);
			if (!L_38)
			{
				goto IL_0102;
			}
		}

IL_00fc:
		{
			G_B17_0 = 0;
			goto IL_0108;
		}

IL_0102:
		{
			List_1_t3644373756 * L_39 = V_3;
			int32_t L_40 = Enumerable_Single_TisInt32_t2847414787_m4182051005(NULL /*static, unused*/, L_39, /*hidden argument*/Enumerable_Single_TisInt32_t2847414787_m4182051005_MethodInfo_var);
			G_B17_0 = L_40;
		}

IL_0108:
		{
			V_4 = G_B17_0;
			TaskUpdater_1_t4197280854 * L_41 = __this->get__fixedUpdater_8();
			U3CInitFixedTickablesU3Ec__AnonStorey192_t1515684493 * L_42 = V_5;
			NullCheck(L_42);
			Il2CppObject * L_43 = L_42->get_tickable_0();
			int32_t L_44 = V_4;
			NullCheck(L_41);
			TaskUpdater_1_AddTask_m1413840662(L_41, L_43, L_44, /*hidden argument*/TaskUpdater_1_AddTask_m1413840662_MethodInfo_var);
		}

IL_011e:
		{
			bool L_45 = Enumerator_MoveNext_m3740477111((&V_2), /*hidden argument*/Enumerator_MoveNext_m3740477111_MethodInfo_var);
			if (L_45)
			{
				goto IL_00a3;
			}
		}

IL_012a:
		{
			IL2CPP_LEAVE(0x13B, FINALLY_012f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_012f;
	}

FINALLY_012f:
	{ // begin finally (depth: 1)
		Enumerator_t2211133568  L_46 = V_2;
		Enumerator_t2211133568  L_47 = L_46;
		Il2CppObject * L_48 = Box(Enumerator_t2211133568_il2cpp_TypeInfo_var, &L_47);
		NullCheck((Il2CppObject *)L_48);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_48);
		IL2CPP_END_FINALLY(303)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(303)
	{
		IL2CPP_JUMP_TBL(0x13B, IL_013b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_013b:
	{
		return;
	}
}
// System.Void Zenject.TickableManager::InitTickables()
extern Il2CppClass* Action_1_t937964726_il2cpp_TypeInfo_var;
extern Il2CppClass* TaskUpdater_1_t1658401268_il2cpp_TypeInfo_var;
extern Il2CppClass* TickableManager_t278367883_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t1899971632_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_1_t1356416995_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t4262336383_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppClass* U3CInitTickablesU3Ec__AnonStorey193_t88162706_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t3626714334_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t1968156484_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t3967221278_il2cpp_TypeInfo_var;
extern const MethodInfo* TickableManager_UpdateTickable_m4186942593_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m2769386234_MethodInfo_var;
extern const MethodInfo* TaskUpdater_1__ctor_m1974438621_MethodInfo_var;
extern const MethodInfo* TickableManager_U3CInitTickablesU3Em__2EB_m4229921116_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m41055557_MethodInfo_var;
extern const MethodInfo* Enumerable_Select_TisTuple_2_t3719549051_TisType_t_m1035253335_MethodInfo_var;
extern const MethodInfo* TypeExtensions_DerivesFrom_TisITickable_t789512021_m2654461335_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1775398595_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m3428178755_MethodInfo_var;
extern const MethodInfo* U3CInitTickablesU3Ec__AnonStorey193_U3CU3Em__2EC_m234334292_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m2850122579_MethodInfo_var;
extern const MethodInfo* Enumerable_Where_TisTuple_2_t3719549051_m3323201792_MethodInfo_var;
extern const MethodInfo* TickableManager_U3CInitTickablesU3Em__2ED_m4103590310_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m1691609133_MethodInfo_var;
extern const MethodInfo* Enumerable_Select_TisTuple_2_t3719549051_TisInt32_t2847414787_m2850307189_MethodInfo_var;
extern const MethodInfo* Enumerable_ToList_TisInt32_t2847414787_m219610782_MethodInfo_var;
extern const MethodInfo* LinqExtensions_IsEmpty_TisInt32_t2847414787_m576996776_MethodInfo_var;
extern const MethodInfo* Enumerable_Single_TisInt32_t2847414787_m4182051005_MethodInfo_var;
extern const MethodInfo* TaskUpdater_1_AddTask_m2450545658_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1452207789_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4144759378;
extern const uint32_t TickableManager_InitTickables_m2762782462_MetadataUsageId;
extern "C"  void TickableManager_InitTickables_m2762782462 (TickableManager_t278367883 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TickableManager_InitTickables_m2762782462_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Enumerator_t3967221278  V_2;
	memset(&V_2, 0, sizeof(V_2));
	List_1_t3644373756 * V_3 = NULL;
	int32_t V_4 = 0;
	U3CInitTickablesU3Ec__AnonStorey193_t88162706 * V_5 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	List_1_t221540724 * G_B2_0 = NULL;
	List_1_t221540724 * G_B1_0 = NULL;
	Il2CppObject* G_B14_0 = NULL;
	Il2CppObject* G_B13_0 = NULL;
	int32_t G_B17_0 = 0;
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)TickableManager_UpdateTickable_m4186942593_MethodInfo_var);
		Action_1_t937964726 * L_1 = (Action_1_t937964726 *)il2cpp_codegen_object_new(Action_1_t937964726_il2cpp_TypeInfo_var);
		Action_1__ctor_m2769386234(L_1, __this, L_0, /*hidden argument*/Action_1__ctor_m2769386234_MethodInfo_var);
		TaskUpdater_1_t1658401268 * L_2 = (TaskUpdater_1_t1658401268 *)il2cpp_codegen_object_new(TaskUpdater_1_t1658401268_il2cpp_TypeInfo_var);
		TaskUpdater_1__ctor_m1974438621(L_2, L_1, /*hidden argument*/TaskUpdater_1__ctor_m1974438621_MethodInfo_var);
		__this->set__updater_7(L_2);
		List_1_t221540724 * L_3 = __this->get__priorities_3();
		Func_2_t1899971632 * L_4 = ((TickableManager_t278367883_StaticFields*)TickableManager_t278367883_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheE_14();
		G_B1_0 = L_3;
		if (L_4)
		{
			G_B2_0 = L_3;
			goto IL_0035;
		}
	}
	{
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)TickableManager_U3CInitTickablesU3Em__2EB_m4229921116_MethodInfo_var);
		Func_2_t1899971632 * L_6 = (Func_2_t1899971632 *)il2cpp_codegen_object_new(Func_2_t1899971632_il2cpp_TypeInfo_var);
		Func_2__ctor_m41055557(L_6, NULL, L_5, /*hidden argument*/Func_2__ctor_m41055557_MethodInfo_var);
		((TickableManager_t278367883_StaticFields*)TickableManager_t278367883_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cacheE_14(L_6);
		G_B2_0 = G_B1_0;
	}

IL_0035:
	{
		Func_2_t1899971632 * L_7 = ((TickableManager_t278367883_StaticFields*)TickableManager_t278367883_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheE_14();
		Il2CppObject* L_8 = Enumerable_Select_TisTuple_2_t3719549051_TisType_t_m1035253335(NULL /*static, unused*/, G_B2_0, L_7, /*hidden argument*/Enumerable_Select_TisTuple_2_t3719549051_TisType_t_m1035253335_MethodInfo_var);
		NullCheck(L_8);
		Il2CppObject* L_9 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Type>::GetEnumerator() */, IEnumerable_1_t1356416995_il2cpp_TypeInfo_var, L_8);
		V_1 = L_9;
	}

IL_0045:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0070;
		}

IL_004a:
		{
			Il2CppObject* L_10 = V_1;
			NullCheck(L_10);
			Type_t * L_11 = InterfaceFuncInvoker0< Type_t * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Type>::get_Current() */, IEnumerator_1_t4262336383_il2cpp_TypeInfo_var, L_10);
			V_0 = L_11;
			Type_t * L_12 = V_0;
			bool L_13 = TypeExtensions_DerivesFrom_TisITickable_t789512021_m2654461335(NULL /*static, unused*/, L_12, /*hidden argument*/TypeExtensions_DerivesFrom_TisITickable_t789512021_m2654461335_MethodInfo_var);
			ObjectU5BU5D_t11523773* L_14 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
			Type_t * L_15 = V_0;
			String_t* L_16 = TypeExtensions_Name_m3754378430(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
			NullCheck(L_14);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 0);
			ArrayElementTypeCheck (L_14, L_16);
			(L_14)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_16);
			Assert_That_m2934751473(NULL /*static, unused*/, L_13, _stringLiteral4144759378, L_14, /*hidden argument*/NULL);
		}

IL_0070:
		{
			Il2CppObject* L_17 = V_1;
			NullCheck(L_17);
			bool L_18 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_17);
			if (L_18)
			{
				goto IL_004a;
			}
		}

IL_007b:
		{
			IL2CPP_LEAVE(0x8B, FINALLY_0080);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0080;
	}

FINALLY_0080:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_19 = V_1;
			if (L_19)
			{
				goto IL_0084;
			}
		}

IL_0083:
		{
			IL2CPP_END_FINALLY(128)
		}

IL_0084:
		{
			Il2CppObject* L_20 = V_1;
			NullCheck(L_20);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_20);
			IL2CPP_END_FINALLY(128)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(128)
	{
		IL2CPP_JUMP_TBL(0x8B, IL_008b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_008b:
	{
		U3CInitTickablesU3Ec__AnonStorey193_t88162706 * L_21 = (U3CInitTickablesU3Ec__AnonStorey193_t88162706 *)il2cpp_codegen_object_new(U3CInitTickablesU3Ec__AnonStorey193_t88162706_il2cpp_TypeInfo_var);
		U3CInitTickablesU3Ec__AnonStorey193__ctor_m2583430673(L_21, /*hidden argument*/NULL);
		V_5 = L_21;
		List_1_t1586470990 * L_22 = __this->get__tickables_0();
		NullCheck(L_22);
		Enumerator_t3967221278  L_23 = List_1_GetEnumerator_m1775398595(L_22, /*hidden argument*/List_1_GetEnumerator_m1775398595_MethodInfo_var);
		V_2 = L_23;
	}

IL_009e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_011e;
		}

IL_00a3:
		{
			U3CInitTickablesU3Ec__AnonStorey193_t88162706 * L_24 = V_5;
			Il2CppObject * L_25 = Enumerator_get_Current_m3428178755((&V_2), /*hidden argument*/Enumerator_get_Current_m3428178755_MethodInfo_var);
			NullCheck(L_24);
			L_24->set_tickable_0(L_25);
			List_1_t221540724 * L_26 = __this->get__priorities_3();
			U3CInitTickablesU3Ec__AnonStorey193_t88162706 * L_27 = V_5;
			IntPtr_t L_28;
			L_28.set_m_value_0((void*)(void*)U3CInitTickablesU3Ec__AnonStorey193_U3CU3Em__2EC_m234334292_MethodInfo_var);
			Func_2_t3626714334 * L_29 = (Func_2_t3626714334 *)il2cpp_codegen_object_new(Func_2_t3626714334_il2cpp_TypeInfo_var);
			Func_2__ctor_m2850122579(L_29, L_27, L_28, /*hidden argument*/Func_2__ctor_m2850122579_MethodInfo_var);
			Il2CppObject* L_30 = Enumerable_Where_TisTuple_2_t3719549051_m3323201792(NULL /*static, unused*/, L_26, L_29, /*hidden argument*/Enumerable_Where_TisTuple_2_t3719549051_m3323201792_MethodInfo_var);
			Func_2_t1968156484 * L_31 = ((TickableManager_t278367883_StaticFields*)TickableManager_t278367883_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheF_15();
			G_B13_0 = L_30;
			if (L_31)
			{
				G_B14_0 = L_30;
				goto IL_00e1;
			}
		}

IL_00d0:
		{
			IntPtr_t L_32;
			L_32.set_m_value_0((void*)(void*)TickableManager_U3CInitTickablesU3Em__2ED_m4103590310_MethodInfo_var);
			Func_2_t1968156484 * L_33 = (Func_2_t1968156484 *)il2cpp_codegen_object_new(Func_2_t1968156484_il2cpp_TypeInfo_var);
			Func_2__ctor_m1691609133(L_33, NULL, L_32, /*hidden argument*/Func_2__ctor_m1691609133_MethodInfo_var);
			((TickableManager_t278367883_StaticFields*)TickableManager_t278367883_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cacheF_15(L_33);
			G_B14_0 = G_B13_0;
		}

IL_00e1:
		{
			Func_2_t1968156484 * L_34 = ((TickableManager_t278367883_StaticFields*)TickableManager_t278367883_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheF_15();
			Il2CppObject* L_35 = Enumerable_Select_TisTuple_2_t3719549051_TisInt32_t2847414787_m2850307189(NULL /*static, unused*/, G_B14_0, L_34, /*hidden argument*/Enumerable_Select_TisTuple_2_t3719549051_TisInt32_t2847414787_m2850307189_MethodInfo_var);
			List_1_t3644373756 * L_36 = Enumerable_ToList_TisInt32_t2847414787_m219610782(NULL /*static, unused*/, L_35, /*hidden argument*/Enumerable_ToList_TisInt32_t2847414787_m219610782_MethodInfo_var);
			V_3 = L_36;
			List_1_t3644373756 * L_37 = V_3;
			bool L_38 = LinqExtensions_IsEmpty_TisInt32_t2847414787_m576996776(NULL /*static, unused*/, L_37, /*hidden argument*/LinqExtensions_IsEmpty_TisInt32_t2847414787_m576996776_MethodInfo_var);
			if (!L_38)
			{
				goto IL_0102;
			}
		}

IL_00fc:
		{
			G_B17_0 = 0;
			goto IL_0108;
		}

IL_0102:
		{
			List_1_t3644373756 * L_39 = V_3;
			int32_t L_40 = Enumerable_Single_TisInt32_t2847414787_m4182051005(NULL /*static, unused*/, L_39, /*hidden argument*/Enumerable_Single_TisInt32_t2847414787_m4182051005_MethodInfo_var);
			G_B17_0 = L_40;
		}

IL_0108:
		{
			V_4 = G_B17_0;
			TaskUpdater_1_t1658401268 * L_41 = __this->get__updater_7();
			U3CInitTickablesU3Ec__AnonStorey193_t88162706 * L_42 = V_5;
			NullCheck(L_42);
			Il2CppObject * L_43 = L_42->get_tickable_0();
			int32_t L_44 = V_4;
			NullCheck(L_41);
			TaskUpdater_1_AddTask_m2450545658(L_41, L_43, L_44, /*hidden argument*/TaskUpdater_1_AddTask_m2450545658_MethodInfo_var);
		}

IL_011e:
		{
			bool L_45 = Enumerator_MoveNext_m1452207789((&V_2), /*hidden argument*/Enumerator_MoveNext_m1452207789_MethodInfo_var);
			if (L_45)
			{
				goto IL_00a3;
			}
		}

IL_012a:
		{
			IL2CPP_LEAVE(0x13B, FINALLY_012f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_012f;
	}

FINALLY_012f:
	{ // begin finally (depth: 1)
		Enumerator_t3967221278  L_46 = V_2;
		Enumerator_t3967221278  L_47 = L_46;
		Il2CppObject * L_48 = Box(Enumerator_t3967221278_il2cpp_TypeInfo_var, &L_47);
		NullCheck((Il2CppObject *)L_48);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_48);
		IL2CPP_END_FINALLY(303)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(303)
	{
		IL2CPP_JUMP_TBL(0x13B, IL_013b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_013b:
	{
		return;
	}
}
// System.Void Zenject.TickableManager::InitLateTickables()
extern Il2CppClass* Action_1_t3614263548_il2cpp_TypeInfo_var;
extern Il2CppClass* TaskUpdater_1_t39732794_il2cpp_TypeInfo_var;
extern Il2CppClass* TickableManager_t278367883_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t1899971632_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_1_t1356416995_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t4262336383_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppClass* U3CInitLateTickablesU3Ec__AnonStorey194_t4122519257_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t3626714334_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t1968156484_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t2348552804_il2cpp_TypeInfo_var;
extern const MethodInfo* TickableManager_UpdateLateTickable_m2045923701_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3722871796_MethodInfo_var;
extern const MethodInfo* TaskUpdater_1__ctor_m3015656867_MethodInfo_var;
extern const MethodInfo* TickableManager_U3CInitLateTickablesU3Em__2EE_m2757629811_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m41055557_MethodInfo_var;
extern const MethodInfo* Enumerable_Select_TisTuple_2_t3719549051_TisType_t_m1035253335_MethodInfo_var;
extern const MethodInfo* TypeExtensions_DerivesFrom_TisILateTickable_t3465810843_m744818909_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m2618919561_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m3047689097_MethodInfo_var;
extern const MethodInfo* U3CInitLateTickablesU3Ec__AnonStorey194_U3CU3Em__2EF_m500120970_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m2850122579_MethodInfo_var;
extern const MethodInfo* Enumerable_Where_TisTuple_2_t3719549051_m3323201792_MethodInfo_var;
extern const MethodInfo* TickableManager_U3CInitLateTickablesU3Em__2F0_m1158061237_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m1691609133_MethodInfo_var;
extern const MethodInfo* Enumerable_Select_TisTuple_2_t3719549051_TisInt32_t2847414787_m2850307189_MethodInfo_var;
extern const MethodInfo* Enumerable_ToList_TisInt32_t2847414787_m219610782_MethodInfo_var;
extern const MethodInfo* LinqExtensions_IsEmpty_TisInt32_t2847414787_m576996776_MethodInfo_var;
extern const MethodInfo* Enumerable_Single_TisInt32_t2847414787_m4182051005_MethodInfo_var;
extern const MethodInfo* TaskUpdater_1_AddTask_m2889617472_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m329754407_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral631543372;
extern const uint32_t TickableManager_InitLateTickables_m1376374776_MetadataUsageId;
extern "C"  void TickableManager_InitLateTickables_m1376374776 (TickableManager_t278367883 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TickableManager_InitLateTickables_m1376374776_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Enumerator_t2348552804  V_2;
	memset(&V_2, 0, sizeof(V_2));
	List_1_t3644373756 * V_3 = NULL;
	int32_t V_4 = 0;
	U3CInitLateTickablesU3Ec__AnonStorey194_t4122519257 * V_5 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	List_1_t221540724 * G_B2_0 = NULL;
	List_1_t221540724 * G_B1_0 = NULL;
	Il2CppObject* G_B14_0 = NULL;
	Il2CppObject* G_B13_0 = NULL;
	int32_t G_B17_0 = 0;
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)TickableManager_UpdateLateTickable_m2045923701_MethodInfo_var);
		Action_1_t3614263548 * L_1 = (Action_1_t3614263548 *)il2cpp_codegen_object_new(Action_1_t3614263548_il2cpp_TypeInfo_var);
		Action_1__ctor_m3722871796(L_1, __this, L_0, /*hidden argument*/Action_1__ctor_m3722871796_MethodInfo_var);
		TaskUpdater_1_t39732794 * L_2 = (TaskUpdater_1_t39732794 *)il2cpp_codegen_object_new(TaskUpdater_1_t39732794_il2cpp_TypeInfo_var);
		TaskUpdater_1__ctor_m3015656867(L_2, L_1, /*hidden argument*/TaskUpdater_1__ctor_m3015656867_MethodInfo_var);
		__this->set__lateUpdater_9(L_2);
		List_1_t221540724 * L_3 = __this->get__latePriorities_5();
		Func_2_t1899971632 * L_4 = ((TickableManager_t278367883_StaticFields*)TickableManager_t278367883_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache10_16();
		G_B1_0 = L_3;
		if (L_4)
		{
			G_B2_0 = L_3;
			goto IL_0035;
		}
	}
	{
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)TickableManager_U3CInitLateTickablesU3Em__2EE_m2757629811_MethodInfo_var);
		Func_2_t1899971632 * L_6 = (Func_2_t1899971632 *)il2cpp_codegen_object_new(Func_2_t1899971632_il2cpp_TypeInfo_var);
		Func_2__ctor_m41055557(L_6, NULL, L_5, /*hidden argument*/Func_2__ctor_m41055557_MethodInfo_var);
		((TickableManager_t278367883_StaticFields*)TickableManager_t278367883_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache10_16(L_6);
		G_B2_0 = G_B1_0;
	}

IL_0035:
	{
		Func_2_t1899971632 * L_7 = ((TickableManager_t278367883_StaticFields*)TickableManager_t278367883_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache10_16();
		Il2CppObject* L_8 = Enumerable_Select_TisTuple_2_t3719549051_TisType_t_m1035253335(NULL /*static, unused*/, G_B2_0, L_7, /*hidden argument*/Enumerable_Select_TisTuple_2_t3719549051_TisType_t_m1035253335_MethodInfo_var);
		NullCheck(L_8);
		Il2CppObject* L_9 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Type>::GetEnumerator() */, IEnumerable_1_t1356416995_il2cpp_TypeInfo_var, L_8);
		V_1 = L_9;
	}

IL_0045:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0070;
		}

IL_004a:
		{
			Il2CppObject* L_10 = V_1;
			NullCheck(L_10);
			Type_t * L_11 = InterfaceFuncInvoker0< Type_t * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Type>::get_Current() */, IEnumerator_1_t4262336383_il2cpp_TypeInfo_var, L_10);
			V_0 = L_11;
			Type_t * L_12 = V_0;
			bool L_13 = TypeExtensions_DerivesFrom_TisILateTickable_t3465810843_m744818909(NULL /*static, unused*/, L_12, /*hidden argument*/TypeExtensions_DerivesFrom_TisILateTickable_t3465810843_m744818909_MethodInfo_var);
			ObjectU5BU5D_t11523773* L_14 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
			Type_t * L_15 = V_0;
			String_t* L_16 = TypeExtensions_Name_m3754378430(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
			NullCheck(L_14);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 0);
			ArrayElementTypeCheck (L_14, L_16);
			(L_14)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_16);
			Assert_That_m2934751473(NULL /*static, unused*/, L_13, _stringLiteral631543372, L_14, /*hidden argument*/NULL);
		}

IL_0070:
		{
			Il2CppObject* L_17 = V_1;
			NullCheck(L_17);
			bool L_18 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_17);
			if (L_18)
			{
				goto IL_004a;
			}
		}

IL_007b:
		{
			IL2CPP_LEAVE(0x8B, FINALLY_0080);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0080;
	}

FINALLY_0080:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_19 = V_1;
			if (L_19)
			{
				goto IL_0084;
			}
		}

IL_0083:
		{
			IL2CPP_END_FINALLY(128)
		}

IL_0084:
		{
			Il2CppObject* L_20 = V_1;
			NullCheck(L_20);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_20);
			IL2CPP_END_FINALLY(128)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(128)
	{
		IL2CPP_JUMP_TBL(0x8B, IL_008b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_008b:
	{
		U3CInitLateTickablesU3Ec__AnonStorey194_t4122519257 * L_21 = (U3CInitLateTickablesU3Ec__AnonStorey194_t4122519257 *)il2cpp_codegen_object_new(U3CInitLateTickablesU3Ec__AnonStorey194_t4122519257_il2cpp_TypeInfo_var);
		U3CInitLateTickablesU3Ec__AnonStorey194__ctor_m278671530(L_21, /*hidden argument*/NULL);
		V_5 = L_21;
		List_1_t4262769812 * L_22 = __this->get__lateTickables_2();
		NullCheck(L_22);
		Enumerator_t2348552804  L_23 = List_1_GetEnumerator_m2618919561(L_22, /*hidden argument*/List_1_GetEnumerator_m2618919561_MethodInfo_var);
		V_2 = L_23;
	}

IL_009e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_011e;
		}

IL_00a3:
		{
			U3CInitLateTickablesU3Ec__AnonStorey194_t4122519257 * L_24 = V_5;
			Il2CppObject * L_25 = Enumerator_get_Current_m3047689097((&V_2), /*hidden argument*/Enumerator_get_Current_m3047689097_MethodInfo_var);
			NullCheck(L_24);
			L_24->set_tickable_0(L_25);
			List_1_t221540724 * L_26 = __this->get__latePriorities_5();
			U3CInitLateTickablesU3Ec__AnonStorey194_t4122519257 * L_27 = V_5;
			IntPtr_t L_28;
			L_28.set_m_value_0((void*)(void*)U3CInitLateTickablesU3Ec__AnonStorey194_U3CU3Em__2EF_m500120970_MethodInfo_var);
			Func_2_t3626714334 * L_29 = (Func_2_t3626714334 *)il2cpp_codegen_object_new(Func_2_t3626714334_il2cpp_TypeInfo_var);
			Func_2__ctor_m2850122579(L_29, L_27, L_28, /*hidden argument*/Func_2__ctor_m2850122579_MethodInfo_var);
			Il2CppObject* L_30 = Enumerable_Where_TisTuple_2_t3719549051_m3323201792(NULL /*static, unused*/, L_26, L_29, /*hidden argument*/Enumerable_Where_TisTuple_2_t3719549051_m3323201792_MethodInfo_var);
			Func_2_t1968156484 * L_31 = ((TickableManager_t278367883_StaticFields*)TickableManager_t278367883_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache11_17();
			G_B13_0 = L_30;
			if (L_31)
			{
				G_B14_0 = L_30;
				goto IL_00e1;
			}
		}

IL_00d0:
		{
			IntPtr_t L_32;
			L_32.set_m_value_0((void*)(void*)TickableManager_U3CInitLateTickablesU3Em__2F0_m1158061237_MethodInfo_var);
			Func_2_t1968156484 * L_33 = (Func_2_t1968156484 *)il2cpp_codegen_object_new(Func_2_t1968156484_il2cpp_TypeInfo_var);
			Func_2__ctor_m1691609133(L_33, NULL, L_32, /*hidden argument*/Func_2__ctor_m1691609133_MethodInfo_var);
			((TickableManager_t278367883_StaticFields*)TickableManager_t278367883_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache11_17(L_33);
			G_B14_0 = G_B13_0;
		}

IL_00e1:
		{
			Func_2_t1968156484 * L_34 = ((TickableManager_t278367883_StaticFields*)TickableManager_t278367883_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache11_17();
			Il2CppObject* L_35 = Enumerable_Select_TisTuple_2_t3719549051_TisInt32_t2847414787_m2850307189(NULL /*static, unused*/, G_B14_0, L_34, /*hidden argument*/Enumerable_Select_TisTuple_2_t3719549051_TisInt32_t2847414787_m2850307189_MethodInfo_var);
			List_1_t3644373756 * L_36 = Enumerable_ToList_TisInt32_t2847414787_m219610782(NULL /*static, unused*/, L_35, /*hidden argument*/Enumerable_ToList_TisInt32_t2847414787_m219610782_MethodInfo_var);
			V_3 = L_36;
			List_1_t3644373756 * L_37 = V_3;
			bool L_38 = LinqExtensions_IsEmpty_TisInt32_t2847414787_m576996776(NULL /*static, unused*/, L_37, /*hidden argument*/LinqExtensions_IsEmpty_TisInt32_t2847414787_m576996776_MethodInfo_var);
			if (!L_38)
			{
				goto IL_0102;
			}
		}

IL_00fc:
		{
			G_B17_0 = 0;
			goto IL_0108;
		}

IL_0102:
		{
			List_1_t3644373756 * L_39 = V_3;
			int32_t L_40 = Enumerable_Single_TisInt32_t2847414787_m4182051005(NULL /*static, unused*/, L_39, /*hidden argument*/Enumerable_Single_TisInt32_t2847414787_m4182051005_MethodInfo_var);
			G_B17_0 = L_40;
		}

IL_0108:
		{
			V_4 = G_B17_0;
			TaskUpdater_1_t39732794 * L_41 = __this->get__lateUpdater_9();
			U3CInitLateTickablesU3Ec__AnonStorey194_t4122519257 * L_42 = V_5;
			NullCheck(L_42);
			Il2CppObject * L_43 = L_42->get_tickable_0();
			int32_t L_44 = V_4;
			NullCheck(L_41);
			TaskUpdater_1_AddTask_m2889617472(L_41, L_43, L_44, /*hidden argument*/TaskUpdater_1_AddTask_m2889617472_MethodInfo_var);
		}

IL_011e:
		{
			bool L_45 = Enumerator_MoveNext_m329754407((&V_2), /*hidden argument*/Enumerator_MoveNext_m329754407_MethodInfo_var);
			if (L_45)
			{
				goto IL_00a3;
			}
		}

IL_012a:
		{
			IL2CPP_LEAVE(0x13B, FINALLY_012f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_012f;
	}

FINALLY_012f:
	{ // begin finally (depth: 1)
		Enumerator_t2348552804  L_46 = V_2;
		Enumerator_t2348552804  L_47 = L_46;
		Il2CppObject * L_48 = Box(Enumerator_t2348552804_il2cpp_TypeInfo_var, &L_47);
		NullCheck((Il2CppObject *)L_48);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_48);
		IL2CPP_END_FINALLY(303)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(303)
	{
		IL2CPP_JUMP_TBL(0x13B, IL_013b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_013b:
	{
		return;
	}
}
// System.Void Zenject.TickableManager::UpdateLateTickable(Zenject.ILateTickable)
extern Il2CppClass* ILateTickable_t3465810843_il2cpp_TypeInfo_var;
extern const uint32_t TickableManager_UpdateLateTickable_m2045923701_MetadataUsageId;
extern "C"  void TickableManager_UpdateLateTickable_m2045923701 (TickableManager_t278367883 * __this, Il2CppObject * ___tickable0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TickableManager_UpdateLateTickable_m2045923701_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___tickable0;
		NullCheck(L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void Zenject.ILateTickable::LateTick() */, ILateTickable_t3465810843_il2cpp_TypeInfo_var, L_0);
		return;
	}
}
// System.Void Zenject.TickableManager::UpdateFixedTickable(Zenject.IFixedTickable)
extern Il2CppClass* IFixedTickable_t3328391607_il2cpp_TypeInfo_var;
extern const uint32_t TickableManager_UpdateFixedTickable_m2742900371_MetadataUsageId;
extern "C"  void TickableManager_UpdateFixedTickable_m2742900371 (TickableManager_t278367883 * __this, Il2CppObject * ___tickable0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TickableManager_UpdateFixedTickable_m2742900371_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___tickable0;
		NullCheck(L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void Zenject.IFixedTickable::FixedTick() */, IFixedTickable_t3328391607_il2cpp_TypeInfo_var, L_0);
		return;
	}
}
// System.Void Zenject.TickableManager::UpdateTickable(Zenject.ITickable)
extern Il2CppClass* ITickable_t789512021_il2cpp_TypeInfo_var;
extern const uint32_t TickableManager_UpdateTickable_m4186942593_MetadataUsageId;
extern "C"  void TickableManager_UpdateTickable_m4186942593 (TickableManager_t278367883 * __this, Il2CppObject * ___tickable0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TickableManager_UpdateTickable_m4186942593_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___tickable0;
		NullCheck(L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void Zenject.ITickable::Tick() */, ITickable_t789512021_il2cpp_TypeInfo_var, L_0);
		return;
	}
}
// System.Void Zenject.TickableManager::Add(Zenject.ITickable,System.Int32)
extern const MethodInfo* TaskUpdater_1_AddTask_m2450545658_MethodInfo_var;
extern const uint32_t TickableManager_Add_m1141490809_MetadataUsageId;
extern "C"  void TickableManager_Add_m1141490809 (TickableManager_t278367883 * __this, Il2CppObject * ___tickable0, int32_t ___priority1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TickableManager_Add_m1141490809_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TaskUpdater_1_t1658401268 * L_0 = __this->get__updater_7();
		Il2CppObject * L_1 = ___tickable0;
		int32_t L_2 = ___priority1;
		NullCheck(L_0);
		TaskUpdater_1_AddTask_m2450545658(L_0, L_1, L_2, /*hidden argument*/TaskUpdater_1_AddTask_m2450545658_MethodInfo_var);
		return;
	}
}
// System.Void Zenject.TickableManager::Add(Zenject.ITickable)
extern "C"  void TickableManager_Add_m3656882462 (TickableManager_t278367883 * __this, Il2CppObject * ___tickable0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___tickable0;
		TickableManager_Add_m1141490809(__this, L_0, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Zenject.TickableManager::AddLate(Zenject.ILateTickable,System.Int32)
extern const MethodInfo* TaskUpdater_1_AddTask_m2889617472_MethodInfo_var;
extern const uint32_t TickableManager_AddLate_m2428459909_MetadataUsageId;
extern "C"  void TickableManager_AddLate_m2428459909 (TickableManager_t278367883 * __this, Il2CppObject * ___tickable0, int32_t ___priority1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TickableManager_AddLate_m2428459909_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TaskUpdater_1_t39732794 * L_0 = __this->get__lateUpdater_9();
		Il2CppObject * L_1 = ___tickable0;
		int32_t L_2 = ___priority1;
		NullCheck(L_0);
		TaskUpdater_1_AddTask_m2889617472(L_0, L_1, L_2, /*hidden argument*/TaskUpdater_1_AddTask_m2889617472_MethodInfo_var);
		return;
	}
}
// System.Void Zenject.TickableManager::AddLate(Zenject.ILateTickable)
extern "C"  void TickableManager_AddLate_m1128103570 (TickableManager_t278367883 * __this, Il2CppObject * ___tickable0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___tickable0;
		TickableManager_AddLate_m2428459909(__this, L_0, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Zenject.TickableManager::AddFixed(Zenject.IFixedTickable,System.Int32)
extern const MethodInfo* TaskUpdater_1_AddTask_m1413840662_MethodInfo_var;
extern const uint32_t TickableManager_AddFixed_m4043982325_MetadataUsageId;
extern "C"  void TickableManager_AddFixed_m4043982325 (TickableManager_t278367883 * __this, Il2CppObject * ___tickable0, int32_t ___priority1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TickableManager_AddFixed_m4043982325_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TaskUpdater_1_t4197280854 * L_0 = __this->get__fixedUpdater_8();
		Il2CppObject * L_1 = ___tickable0;
		int32_t L_2 = ___priority1;
		NullCheck(L_0);
		TaskUpdater_1_AddTask_m1413840662(L_0, L_1, L_2, /*hidden argument*/TaskUpdater_1_AddTask_m1413840662_MethodInfo_var);
		return;
	}
}
// System.Void Zenject.TickableManager::AddFixed(Zenject.IFixedTickable)
extern const MethodInfo* TaskUpdater_1_AddTask_m1413840662_MethodInfo_var;
extern const uint32_t TickableManager_AddFixed_m3377880610_MetadataUsageId;
extern "C"  void TickableManager_AddFixed_m3377880610 (TickableManager_t278367883 * __this, Il2CppObject * ___tickable0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TickableManager_AddFixed_m3377880610_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TaskUpdater_1_t4197280854 * L_0 = __this->get__fixedUpdater_8();
		Il2CppObject * L_1 = ___tickable0;
		NullCheck(L_0);
		TaskUpdater_1_AddTask_m1413840662(L_0, L_1, 0, /*hidden argument*/TaskUpdater_1_AddTask_m1413840662_MethodInfo_var);
		return;
	}
}
// System.Void Zenject.TickableManager::Remove(Zenject.ITickable)
extern const MethodInfo* TaskUpdater_1_RemoveTask_m2480249412_MethodInfo_var;
extern const uint32_t TickableManager_Remove_m2180943581_MetadataUsageId;
extern "C"  void TickableManager_Remove_m2180943581 (TickableManager_t278367883 * __this, Il2CppObject * ___tickable0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TickableManager_Remove_m2180943581_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TaskUpdater_1_t1658401268 * L_0 = __this->get__updater_7();
		Il2CppObject * L_1 = ___tickable0;
		NullCheck(L_0);
		TaskUpdater_1_RemoveTask_m2480249412(L_0, L_1, /*hidden argument*/TaskUpdater_1_RemoveTask_m2480249412_MethodInfo_var);
		return;
	}
}
// System.Void Zenject.TickableManager::RemoveLate(Zenject.ILateTickable)
extern const MethodInfo* TaskUpdater_1_RemoveTask_m3116551690_MethodInfo_var;
extern const uint32_t TickableManager_RemoveLate_m2727232849_MetadataUsageId;
extern "C"  void TickableManager_RemoveLate_m2727232849 (TickableManager_t278367883 * __this, Il2CppObject * ___tickable0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TickableManager_RemoveLate_m2727232849_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TaskUpdater_1_t39732794 * L_0 = __this->get__lateUpdater_9();
		Il2CppObject * L_1 = ___tickable0;
		NullCheck(L_0);
		TaskUpdater_1_RemoveTask_m3116551690(L_0, L_1, /*hidden argument*/TaskUpdater_1_RemoveTask_m3116551690_MethodInfo_var);
		return;
	}
}
// System.Void Zenject.TickableManager::RemoveFixed(Zenject.IFixedTickable)
extern const MethodInfo* TaskUpdater_1_RemoveTask_m3152719968_MethodInfo_var;
extern const uint32_t TickableManager_RemoveFixed_m2542825761_MetadataUsageId;
extern "C"  void TickableManager_RemoveFixed_m2542825761 (TickableManager_t278367883 * __this, Il2CppObject * ___tickable0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TickableManager_RemoveFixed_m2542825761_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TaskUpdater_1_t4197280854 * L_0 = __this->get__fixedUpdater_8();
		Il2CppObject * L_1 = ___tickable0;
		NullCheck(L_0);
		TaskUpdater_1_RemoveTask_m3152719968(L_0, L_1, /*hidden argument*/TaskUpdater_1_RemoveTask_m3152719968_MethodInfo_var);
		return;
	}
}
// System.Void Zenject.TickableManager::Update()
extern const MethodInfo* TaskUpdater_1_OnFrameStart_m1028326720_MethodInfo_var;
extern const MethodInfo* TaskUpdater_1_UpdateAll_m3644818414_MethodInfo_var;
extern const uint32_t TickableManager_Update_m259086169_MetadataUsageId;
extern "C"  void TickableManager_Update_m259086169 (TickableManager_t278367883 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TickableManager_Update_m259086169_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TaskUpdater_1_t1658401268 * L_0 = __this->get__updater_7();
		NullCheck(L_0);
		TaskUpdater_1_OnFrameStart_m1028326720(L_0, /*hidden argument*/TaskUpdater_1_OnFrameStart_m1028326720_MethodInfo_var);
		TaskUpdater_1_t1658401268 * L_1 = __this->get__updater_7();
		NullCheck(L_1);
		TaskUpdater_1_UpdateAll_m3644818414(L_1, /*hidden argument*/TaskUpdater_1_UpdateAll_m3644818414_MethodInfo_var);
		return;
	}
}
// System.Void Zenject.TickableManager::FixedUpdate()
extern const MethodInfo* TaskUpdater_1_OnFrameStart_m1383007396_MethodInfo_var;
extern const MethodInfo* TaskUpdater_1_UpdateAll_m2750832394_MethodInfo_var;
extern const uint32_t TickableManager_FixedUpdate_m3368626351_MetadataUsageId;
extern "C"  void TickableManager_FixedUpdate_m3368626351 (TickableManager_t278367883 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TickableManager_FixedUpdate_m3368626351_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TaskUpdater_1_t4197280854 * L_0 = __this->get__fixedUpdater_8();
		NullCheck(L_0);
		TaskUpdater_1_OnFrameStart_m1383007396(L_0, /*hidden argument*/TaskUpdater_1_OnFrameStart_m1383007396_MethodInfo_var);
		TaskUpdater_1_t4197280854 * L_1 = __this->get__fixedUpdater_8();
		NullCheck(L_1);
		TaskUpdater_1_UpdateAll_m2750832394(L_1, /*hidden argument*/TaskUpdater_1_UpdateAll_m2750832394_MethodInfo_var);
		return;
	}
}
// System.Void Zenject.TickableManager::LateUpdate()
extern const MethodInfo* TaskUpdater_1_OnFrameStart_m2568083258_MethodInfo_var;
extern const MethodInfo* TaskUpdater_1_UpdateAll_m1995998260_MethodInfo_var;
extern const uint32_t TickableManager_LateUpdate_m1881524383_MetadataUsageId;
extern "C"  void TickableManager_LateUpdate_m1881524383 (TickableManager_t278367883 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TickableManager_LateUpdate_m1881524383_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TaskUpdater_1_t39732794 * L_0 = __this->get__lateUpdater_9();
		NullCheck(L_0);
		TaskUpdater_1_OnFrameStart_m2568083258(L_0, /*hidden argument*/TaskUpdater_1_OnFrameStart_m2568083258_MethodInfo_var);
		TaskUpdater_1_t39732794 * L_1 = __this->get__lateUpdater_9();
		NullCheck(L_1);
		TaskUpdater_1_UpdateAll_m1995998260(L_1, /*hidden argument*/TaskUpdater_1_UpdateAll_m1995998260_MethodInfo_var);
		return;
	}
}
// System.Type Zenject.TickableManager::<WarnForMissingBindings>m__2E7(Zenject.ITickable)
extern "C"  Type_t * TickableManager_U3CWarnForMissingBindingsU3Em__2E7_m2179551731 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___x0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___x0;
		NullCheck(L_0);
		Type_t * L_1 = Object_GetType_m2022236990(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Type Zenject.TickableManager::<InitFixedTickables>m__2E8(ModestTree.Util.Tuple`2<System.Type,System.Int32>)
extern "C"  Type_t * TickableManager_U3CInitFixedTickablesU3Em__2E8_m3385276832 (Il2CppObject * __this /* static, unused */, Tuple_2_t3719549051 * ___x0, const MethodInfo* method)
{
	{
		Tuple_2_t3719549051 * L_0 = ___x0;
		NullCheck(L_0);
		Type_t * L_1 = L_0->get_First_0();
		return L_1;
	}
}
// System.Int32 Zenject.TickableManager::<InitFixedTickables>m__2EA(ModestTree.Util.Tuple`2<System.Type,System.Int32>)
extern "C"  int32_t TickableManager_U3CInitFixedTickablesU3Em__2EA_m1451350763 (Il2CppObject * __this /* static, unused */, Tuple_2_t3719549051 * ___x0, const MethodInfo* method)
{
	{
		Tuple_2_t3719549051 * L_0 = ___x0;
		NullCheck(L_0);
		int32_t L_1 = L_0->get_Second_1();
		return L_1;
	}
}
// System.Type Zenject.TickableManager::<InitTickables>m__2EB(ModestTree.Util.Tuple`2<System.Type,System.Int32>)
extern "C"  Type_t * TickableManager_U3CInitTickablesU3Em__2EB_m4229921116 (Il2CppObject * __this /* static, unused */, Tuple_2_t3719549051 * ___x0, const MethodInfo* method)
{
	{
		Tuple_2_t3719549051 * L_0 = ___x0;
		NullCheck(L_0);
		Type_t * L_1 = L_0->get_First_0();
		return L_1;
	}
}
// System.Int32 Zenject.TickableManager::<InitTickables>m__2ED(ModestTree.Util.Tuple`2<System.Type,System.Int32>)
extern "C"  int32_t TickableManager_U3CInitTickablesU3Em__2ED_m4103590310 (Il2CppObject * __this /* static, unused */, Tuple_2_t3719549051 * ___x0, const MethodInfo* method)
{
	{
		Tuple_2_t3719549051 * L_0 = ___x0;
		NullCheck(L_0);
		int32_t L_1 = L_0->get_Second_1();
		return L_1;
	}
}
// System.Type Zenject.TickableManager::<InitLateTickables>m__2EE(ModestTree.Util.Tuple`2<System.Type,System.Int32>)
extern "C"  Type_t * TickableManager_U3CInitLateTickablesU3Em__2EE_m2757629811 (Il2CppObject * __this /* static, unused */, Tuple_2_t3719549051 * ___x0, const MethodInfo* method)
{
	{
		Tuple_2_t3719549051 * L_0 = ___x0;
		NullCheck(L_0);
		Type_t * L_1 = L_0->get_First_0();
		return L_1;
	}
}
// System.Int32 Zenject.TickableManager::<InitLateTickables>m__2F0(ModestTree.Util.Tuple`2<System.Type,System.Int32>)
extern "C"  int32_t TickableManager_U3CInitLateTickablesU3Em__2F0_m1158061237 (Il2CppObject * __this /* static, unused */, Tuple_2_t3719549051 * ___x0, const MethodInfo* method)
{
	{
		Tuple_2_t3719549051 * L_0 = ___x0;
		NullCheck(L_0);
		int32_t L_1 = L_0->get_Second_1();
		return L_1;
	}
}
// System.Void Zenject.TickableManager/<InitFixedTickables>c__AnonStorey192::.ctor()
extern "C"  void U3CInitFixedTickablesU3Ec__AnonStorey192__ctor_m4133438902 (U3CInitFixedTickablesU3Ec__AnonStorey192_t1515684493 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Zenject.TickableManager/<InitFixedTickables>c__AnonStorey192::<>m__2E9(ModestTree.Util.Tuple`2<System.Type,System.Int32>)
extern "C"  bool U3CInitFixedTickablesU3Ec__AnonStorey192_U3CU3Em__2E9_m2563333851 (U3CInitFixedTickablesU3Ec__AnonStorey192_t1515684493 * __this, Tuple_2_t3719549051 * ___x0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_tickable_0();
		NullCheck(L_0);
		Type_t * L_1 = Object_GetType_m2022236990(L_0, /*hidden argument*/NULL);
		Tuple_2_t3719549051 * L_2 = ___x0;
		NullCheck(L_2);
		Type_t * L_3 = L_2->get_First_0();
		bool L_4 = TypeExtensions_DerivesFromOrEqual_m3526502226(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void Zenject.TickableManager/<InitLateTickables>c__AnonStorey194::.ctor()
extern "C"  void U3CInitLateTickablesU3Ec__AnonStorey194__ctor_m278671530 (U3CInitLateTickablesU3Ec__AnonStorey194_t4122519257 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Zenject.TickableManager/<InitLateTickables>c__AnonStorey194::<>m__2EF(ModestTree.Util.Tuple`2<System.Type,System.Int32>)
extern "C"  bool U3CInitLateTickablesU3Ec__AnonStorey194_U3CU3Em__2EF_m500120970 (U3CInitLateTickablesU3Ec__AnonStorey194_t4122519257 * __this, Tuple_2_t3719549051 * ___x0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_tickable_0();
		NullCheck(L_0);
		Type_t * L_1 = Object_GetType_m2022236990(L_0, /*hidden argument*/NULL);
		Tuple_2_t3719549051 * L_2 = ___x0;
		NullCheck(L_2);
		Type_t * L_3 = L_2->get_First_0();
		bool L_4 = TypeExtensions_DerivesFromOrEqual_m3526502226(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void Zenject.TickableManager/<InitTickables>c__AnonStorey193::.ctor()
extern "C"  void U3CInitTickablesU3Ec__AnonStorey193__ctor_m2583430673 (U3CInitTickablesU3Ec__AnonStorey193_t88162706 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Zenject.TickableManager/<InitTickables>c__AnonStorey193::<>m__2EC(ModestTree.Util.Tuple`2<System.Type,System.Int32>)
extern "C"  bool U3CInitTickablesU3Ec__AnonStorey193_U3CU3Em__2EC_m234334292 (U3CInitTickablesU3Ec__AnonStorey193_t88162706 * __this, Tuple_2_t3719549051 * ___x0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_tickable_0();
		NullCheck(L_0);
		Type_t * L_1 = Object_GetType_m2022236990(L_0, /*hidden argument*/NULL);
		Tuple_2_t3719549051 * L_2 = ___x0;
		NullCheck(L_2);
		Type_t * L_3 = L_2->get_First_0();
		bool L_4 = TypeExtensions_DerivesFromOrEqual_m3526502226(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void Zenject.TickablePrioritiesInstaller::.ctor(System.Collections.Generic.List`1<System.Type>)
extern "C"  void TickablePrioritiesInstaller__ctor_m1580647544 (TickablePrioritiesInstaller_t1293343172 * __this, List_1_t3576188904 * ___tickables0, const MethodInfo* method)
{
	{
		Installer__ctor_m4279559010(__this, /*hidden argument*/NULL);
		List_1_t3576188904 * L_0 = ___tickables0;
		__this->set__tickables_1(L_0);
		return;
	}
}
// System.Void Zenject.TickablePrioritiesInstaller::InstallBindings()
extern Il2CppClass* Enumerator_t1661971896_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m3898856315_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1436972411_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1892553461_MethodInfo_var;
extern const uint32_t TickablePrioritiesInstaller_InstallBindings_m4097492674_MetadataUsageId;
extern "C"  void TickablePrioritiesInstaller_InstallBindings_m4097492674 (TickablePrioritiesInstaller_t1293343172 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TickablePrioritiesInstaller_InstallBindings_m4097492674_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Type_t * V_1 = NULL;
	Enumerator_t1661971896  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t3576188904 * L_0 = __this->get__tickables_1();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Type>::get_Count() */, L_0);
		V_0 = ((int32_t)((int32_t)(-1)*(int32_t)L_1));
		List_1_t3576188904 * L_2 = __this->get__tickables_1();
		NullCheck(L_2);
		Enumerator_t1661971896  L_3 = List_1_GetEnumerator_m3898856315(L_2, /*hidden argument*/List_1_GetEnumerator_m3898856315_MethodInfo_var);
		V_2 = L_3;
	}

IL_001a:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0038;
		}

IL_001f:
		{
			Type_t * L_4 = Enumerator_get_Current_m1436972411((&V_2), /*hidden argument*/Enumerator_get_Current_m1436972411_MethodInfo_var);
			V_1 = L_4;
			DiContainer_t2383114449 * L_5 = Installer_get_Container_m3289643822(__this, /*hidden argument*/NULL);
			Type_t * L_6 = V_1;
			int32_t L_7 = V_0;
			TickablePrioritiesInstaller_BindPriority_m1294937209(NULL /*static, unused*/, L_5, L_6, L_7, /*hidden argument*/NULL);
			int32_t L_8 = V_0;
			V_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
		}

IL_0038:
		{
			bool L_9 = Enumerator_MoveNext_m1892553461((&V_2), /*hidden argument*/Enumerator_MoveNext_m1892553461_MethodInfo_var);
			if (L_9)
			{
				goto IL_001f;
			}
		}

IL_0044:
		{
			IL2CPP_LEAVE(0x55, FINALLY_0049);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0049;
	}

FINALLY_0049:
	{ // begin finally (depth: 1)
		Enumerator_t1661971896  L_10 = V_2;
		Enumerator_t1661971896  L_11 = L_10;
		Il2CppObject * L_12 = Box(Enumerator_t1661971896_il2cpp_TypeInfo_var, &L_11);
		NullCheck((Il2CppObject *)L_12);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_12);
		IL2CPP_END_FINALLY(73)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(73)
	{
		IL2CPP_JUMP_TBL(0x55, IL_0055)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0055:
	{
		return;
	}
}
// System.Void Zenject.TickablePrioritiesInstaller::BindPriority(Zenject.DiContainer,System.Type,System.Int32)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern const MethodInfo* TypeExtensions_DerivesFrom_TisITickable_t789512021_m2654461335_MethodInfo_var;
extern const MethodInfo* DiContainer_Bind_TisTuple_2_t3719549051_m656443329_MethodInfo_var;
extern const MethodInfo* Tuple_New_TisType_t_TisInt32_t2847414787_m1765357578_MethodInfo_var;
extern const MethodInfo* BinderGeneric_1_ToInstance_TisTuple_2_t3719549051_m259736355_MethodInfo_var;
extern const MethodInfo* BindingConditionSetter_WhenInjectedInto_TisTickableManager_t278367883_m2461038974_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral871266966;
extern const uint32_t TickablePrioritiesInstaller_BindPriority_m1294937209_MetadataUsageId;
extern "C"  void TickablePrioritiesInstaller_BindPriority_m1294937209 (Il2CppObject * __this /* static, unused */, DiContainer_t2383114449 * ___container0, Type_t * ___tickableType1, int32_t ___priorityCount2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TickablePrioritiesInstaller_BindPriority_m1294937209_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = ___tickableType1;
		bool L_1 = TypeExtensions_DerivesFrom_TisITickable_t789512021_m2654461335(NULL /*static, unused*/, L_0, /*hidden argument*/TypeExtensions_DerivesFrom_TisITickable_t789512021_m2654461335_MethodInfo_var);
		ObjectU5BU5D_t11523773* L_2 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_3 = ___tickableType1;
		String_t* L_4 = TypeExtensions_Name_m3754378430(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_4);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_4);
		Assert_That_m2934751473(NULL /*static, unused*/, L_1, _stringLiteral871266966, L_2, /*hidden argument*/NULL);
		DiContainer_t2383114449 * L_5 = ___container0;
		NullCheck(L_5);
		BinderGeneric_1_t3403148347 * L_6 = GenericVirtFuncInvoker0< BinderGeneric_1_t3403148347 * >::Invoke(DiContainer_Bind_TisTuple_2_t3719549051_m656443329_MethodInfo_var, L_5);
		Type_t * L_7 = ___tickableType1;
		int32_t L_8 = ___priorityCount2;
		Tuple_2_t3719549051 * L_9 = Tuple_New_TisType_t_TisInt32_t2847414787_m1765357578(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/Tuple_New_TisType_t_TisInt32_t2847414787_m1765357578_MethodInfo_var);
		NullCheck(L_6);
		BindingConditionSetter_t259147722 * L_10 = BinderGeneric_1_ToInstance_TisTuple_2_t3719549051_m259736355(L_6, L_9, /*hidden argument*/BinderGeneric_1_ToInstance_TisTuple_2_t3719549051_m259736355_MethodInfo_var);
		NullCheck(L_10);
		BindingConditionSetter_WhenInjectedInto_TisTickableManager_t278367883_m2461038974(L_10, /*hidden argument*/BindingConditionSetter_WhenInjectedInto_TisTickableManager_t278367883_m2461038974_MethodInfo_var);
		return;
	}
}
// System.Void Zenject.TransientProvider::.ctor(Zenject.DiContainer,System.Type)
extern "C"  void TransientProvider__ctor_m154705881 (TransientProvider_t1859995120 * __this, DiContainer_t2383114449 * ___container0, Type_t * ___concreteType1, const MethodInfo* method)
{
	{
		ProviderBase__ctor_m796348314(__this, /*hidden argument*/NULL);
		DiContainer_t2383114449 * L_0 = ___container0;
		__this->set__container_3(L_0);
		Type_t * L_1 = ___concreteType1;
		__this->set__concreteType_2(L_1);
		return;
	}
}
// System.Type Zenject.TransientProvider::GetInstanceType()
extern "C"  Type_t * TransientProvider_GetInstanceType_m3567265676 (TransientProvider_t1859995120 * __this, const MethodInfo* method)
{
	{
		Type_t * L_0 = __this->get__concreteType_2();
		return L_0;
	}
}
// System.Object Zenject.TransientProvider::GetInstance(Zenject.InjectContext)
extern Il2CppClass* List_1_t1417891359_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1502268817_MethodInfo_var;
extern const uint32_t TransientProvider_GetInstance_m503948774_MetadataUsageId;
extern "C"  Il2CppObject * TransientProvider_GetInstance_m503948774 (TransientProvider_t1859995120 * __this, InjectContext_t3456483891 * ___context0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TransientProvider_GetInstance_m503948774_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		DiContainer_t2383114449 * L_0 = __this->get__container_3();
		InjectContext_t3456483891 * L_1 = ___context0;
		NullCheck(L_1);
		Type_t * L_2 = L_1->get_MemberType_6();
		Type_t * L_3 = TransientProvider_GetTypeToInstantiate_m197801599(__this, L_2, /*hidden argument*/NULL);
		List_1_t1417891359 * L_4 = (List_1_t1417891359 *)il2cpp_codegen_object_new(List_1_t1417891359_il2cpp_TypeInfo_var);
		List_1__ctor_m1502268817(L_4, /*hidden argument*/List_1__ctor_m1502268817_MethodInfo_var);
		InjectContext_t3456483891 * L_5 = ___context0;
		NullCheck(L_0);
		Il2CppObject * L_6 = VirtFuncInvoker5< Il2CppObject *, Type_t *, List_1_t1417891359 *, InjectContext_t3456483891 *, String_t*, bool >::Invoke(35 /* System.Object Zenject.DiContainer::InstantiateExplicit(System.Type,System.Collections.Generic.List`1<Zenject.TypeValuePair>,Zenject.InjectContext,System.String,System.Boolean) */, L_0, L_3, L_4, L_5, (String_t*)NULL, (bool)1);
		V_0 = L_6;
		Il2CppObject * L_7 = V_0;
		Assert_That_m1320742921(NULL /*static, unused*/, (bool)((((int32_t)((((Il2CppObject*)(Il2CppObject *)L_7) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		Il2CppObject * L_8 = V_0;
		return L_8;
	}
}
// System.Type Zenject.TransientProvider::GetTypeToInstantiate(System.Type)
extern "C"  Type_t * TransientProvider_GetTypeToInstantiate_m197801599 (TransientProvider_t1859995120 * __this, Type_t * ___contractType0, const MethodInfo* method)
{
	{
		Type_t * L_0 = __this->get__concreteType_2();
		bool L_1 = TypeExtensions_IsOpenGenericType_m3853458201(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0033;
		}
	}
	{
		Type_t * L_2 = ___contractType0;
		NullCheck(L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(21 /* System.Boolean System.Type::get_IsAbstract() */, L_2);
		Assert_That_m1320742921(NULL /*static, unused*/, (bool)((((int32_t)L_3) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		Type_t * L_4 = ___contractType0;
		NullCheck(L_4);
		Type_t * L_5 = VirtFuncInvoker0< Type_t * >::Invoke(96 /* System.Type System.Type::GetGenericTypeDefinition() */, L_4);
		Type_t * L_6 = __this->get__concreteType_2();
		Assert_That_m1320742921(NULL /*static, unused*/, (bool)((((Il2CppObject*)(Type_t *)L_5) == ((Il2CppObject*)(Type_t *)L_6))? 1 : 0), /*hidden argument*/NULL);
		Type_t * L_7 = ___contractType0;
		return L_7;
	}

IL_0033:
	{
		Type_t * L_8 = __this->get__concreteType_2();
		Type_t * L_9 = ___contractType0;
		bool L_10 = TypeExtensions_DerivesFromOrEqual_m3526502226(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		Assert_That_m1320742921(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		Type_t * L_11 = __this->get__concreteType_2();
		return L_11;
	}
}
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.TransientProvider::ValidateBinding(Zenject.InjectContext)
extern Il2CppClass* TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var;
extern const uint32_t TransientProvider_ValidateBinding_m2413814776_MetadataUsageId;
extern "C"  Il2CppObject* TransientProvider_ValidateBinding_m2413814776 (TransientProvider_t1859995120 * __this, InjectContext_t3456483891 * ___context0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TransientProvider_ValidateBinding_m2413814776_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DiContainer_t2383114449 * L_0 = __this->get__container_3();
		Type_t * L_1 = __this->get__concreteType_2();
		InjectContext_t3456483891 * L_2 = ___context0;
		Il2CppObject* L_3 = BindingValidator_ValidateObjectGraph_m65278683(NULL /*static, unused*/, L_0, L_1, L_2, (String_t*)NULL, ((TypeU5BU5D_t3431720054*)SZArrayNew(TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void Zenject.TypeAnalyzer::.cctor()
extern Il2CppClass* Dictionary_2_t2395761423_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeAnalyzer_t2089278869_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2573576797_MethodInfo_var;
extern const uint32_t TypeAnalyzer__cctor_m403752817_MetadataUsageId;
extern "C"  void TypeAnalyzer__cctor_m403752817 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeAnalyzer__cctor_m403752817_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2395761423 * L_0 = (Dictionary_2_t2395761423 *)il2cpp_codegen_object_new(Dictionary_2_t2395761423_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2573576797(L_0, /*hidden argument*/Dictionary_2__ctor_m2573576797_MethodInfo_var);
		((TypeAnalyzer_t2089278869_StaticFields*)TypeAnalyzer_t2089278869_il2cpp_TypeInfo_var->static_fields)->set__typeInfo_0(L_0);
		return;
	}
}
// Zenject.ZenjectTypeInfo Zenject.TypeAnalyzer::GetInfo(System.Type)
extern Il2CppClass* TypeAnalyzer_t2089278869_il2cpp_TypeInfo_var;
extern const uint32_t TypeAnalyzer_GetInfo_m224667190_MetadataUsageId;
extern "C"  ZenjectTypeInfo_t283213708 * TypeAnalyzer_GetInfo_m224667190 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeAnalyzer_GetInfo_m224667190_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ZenjectTypeInfo_t283213708 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(TypeAnalyzer_t2089278869_il2cpp_TypeInfo_var);
		Dictionary_2_t2395761423 * L_0 = ((TypeAnalyzer_t2089278869_StaticFields*)TypeAnalyzer_t2089278869_il2cpp_TypeInfo_var->static_fields)->get__typeInfo_0();
		Type_t * L_1 = ___type0;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker2< bool, Type_t *, ZenjectTypeInfo_t283213708 ** >::Invoke(21 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Type,Zenject.ZenjectTypeInfo>::TryGetValue(!0,!1&) */, L_0, L_1, (&V_0));
		if (L_2)
		{
			goto IL_0025;
		}
	}
	{
		Type_t * L_3 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(TypeAnalyzer_t2089278869_il2cpp_TypeInfo_var);
		ZenjectTypeInfo_t283213708 * L_4 = TypeAnalyzer_CreateTypeInfo_m444995314(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Dictionary_2_t2395761423 * L_5 = ((TypeAnalyzer_t2089278869_StaticFields*)TypeAnalyzer_t2089278869_il2cpp_TypeInfo_var->static_fields)->get__typeInfo_0();
		Type_t * L_6 = ___type0;
		ZenjectTypeInfo_t283213708 * L_7 = V_0;
		NullCheck(L_5);
		VirtActionInvoker2< Type_t *, ZenjectTypeInfo_t283213708 * >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.Type,Zenject.ZenjectTypeInfo>::Add(!0,!1) */, L_5, L_6, L_7);
	}

IL_0025:
	{
		ZenjectTypeInfo_t283213708 * L_8 = V_0;
		return L_8;
	}
}
// Zenject.ZenjectTypeInfo Zenject.TypeAnalyzer::CreateTypeInfo(System.Type)
extern Il2CppClass* TypeAnalyzer_t2089278869_il2cpp_TypeInfo_var;
extern Il2CppClass* ZenjectTypeInfo_t283213708_il2cpp_TypeInfo_var;
extern const MethodInfo* Enumerable_ToList_TisInjectableInfo_t1147709774_m755394695_MethodInfo_var;
extern const uint32_t TypeAnalyzer_CreateTypeInfo_m444995314_MetadataUsageId;
extern "C"  ZenjectTypeInfo_t283213708 * TypeAnalyzer_CreateTypeInfo_m444995314 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeAnalyzer_CreateTypeInfo_m444995314_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ConstructorInfo_t3542137334 * V_0 = NULL;
	{
		Type_t * L_0 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(TypeAnalyzer_t2089278869_il2cpp_TypeInfo_var);
		ConstructorInfo_t3542137334 * L_1 = TypeAnalyzer_GetInjectConstructor_m1524952239(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Type_t * L_2 = ___type0;
		Type_t * L_3 = ___type0;
		List_1_t3877242631 * L_4 = TypeAnalyzer_GetPostInjectMethods_m3454744571(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		ConstructorInfo_t3542137334 * L_5 = V_0;
		Type_t * L_6 = ___type0;
		Il2CppObject* L_7 = TypeAnalyzer_GetFieldInjectables_m2698805947(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		List_1_t1944668743 * L_8 = Enumerable_ToList_TisInjectableInfo_t1147709774_m755394695(NULL /*static, unused*/, L_7, /*hidden argument*/Enumerable_ToList_TisInjectableInfo_t1147709774_m755394695_MethodInfo_var);
		Type_t * L_9 = ___type0;
		Il2CppObject* L_10 = TypeAnalyzer_GetPropertyInjectables_m2870845588(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		List_1_t1944668743 * L_11 = Enumerable_ToList_TisInjectableInfo_t1147709774_m755394695(NULL /*static, unused*/, L_10, /*hidden argument*/Enumerable_ToList_TisInjectableInfo_t1147709774_m755394695_MethodInfo_var);
		Type_t * L_12 = ___type0;
		ConstructorInfo_t3542137334 * L_13 = V_0;
		Il2CppObject* L_14 = TypeAnalyzer_GetConstructorInjectables_m1689975587(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		List_1_t1944668743 * L_15 = Enumerable_ToList_TisInjectableInfo_t1147709774_m755394695(NULL /*static, unused*/, L_14, /*hidden argument*/Enumerable_ToList_TisInjectableInfo_t1147709774_m755394695_MethodInfo_var);
		ZenjectTypeInfo_t283213708 * L_16 = (ZenjectTypeInfo_t283213708 *)il2cpp_codegen_object_new(ZenjectTypeInfo_t283213708_il2cpp_TypeInfo_var);
		ZenjectTypeInfo__ctor_m4156201214(L_16, L_2, L_4, L_5, L_8, L_11, L_15, /*hidden argument*/NULL);
		return L_16;
	}
}
// System.Collections.Generic.IEnumerable`1<Zenject.InjectableInfo> Zenject.TypeAnalyzer::GetConstructorInjectables(System.Type,System.Reflection.ConstructorInfo)
extern Il2CppClass* U3CGetConstructorInjectablesU3Ec__AnonStorey181_t3473840153_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t3562324733_il2cpp_TypeInfo_var;
extern const MethodInfo* Enumerable_Empty_TisInjectableInfo_t1147709774_m3970248253_MethodInfo_var;
extern const MethodInfo* U3CGetConstructorInjectablesU3Ec__AnonStorey181_U3CU3Em__2B0_m3973830771_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m379451741_MethodInfo_var;
extern const MethodInfo* Enumerable_Select_TisParameterInfo_t2610273829_TisInjectableInfo_t1147709774_m2840075429_MethodInfo_var;
extern const uint32_t TypeAnalyzer_GetConstructorInjectables_m1689975587_MetadataUsageId;
extern "C"  Il2CppObject* TypeAnalyzer_GetConstructorInjectables_m1689975587 (Il2CppObject * __this /* static, unused */, Type_t * ___parentType0, ConstructorInfo_t3542137334 * ___constructorInfo1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeAnalyzer_GetConstructorInjectables_m1689975587_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetConstructorInjectablesU3Ec__AnonStorey181_t3473840153 * V_0 = NULL;
	{
		U3CGetConstructorInjectablesU3Ec__AnonStorey181_t3473840153 * L_0 = (U3CGetConstructorInjectablesU3Ec__AnonStorey181_t3473840153 *)il2cpp_codegen_object_new(U3CGetConstructorInjectablesU3Ec__AnonStorey181_t3473840153_il2cpp_TypeInfo_var);
		U3CGetConstructorInjectablesU3Ec__AnonStorey181__ctor_m1614566642(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetConstructorInjectablesU3Ec__AnonStorey181_t3473840153 * L_1 = V_0;
		Type_t * L_2 = ___parentType0;
		NullCheck(L_1);
		L_1->set_parentType_0(L_2);
		ConstructorInfo_t3542137334 * L_3 = ___constructorInfo1;
		if (L_3)
		{
			goto IL_0019;
		}
	}
	{
		Il2CppObject* L_4 = Enumerable_Empty_TisInjectableInfo_t1147709774_m3970248253(NULL /*static, unused*/, /*hidden argument*/Enumerable_Empty_TisInjectableInfo_t1147709774_m3970248253_MethodInfo_var);
		return L_4;
	}

IL_0019:
	{
		ConstructorInfo_t3542137334 * L_5 = ___constructorInfo1;
		NullCheck(L_5);
		ParameterInfoU5BU5D_t1127461800* L_6 = VirtFuncInvoker0< ParameterInfoU5BU5D_t1127461800* >::Invoke(15 /* System.Reflection.ParameterInfo[] System.Reflection.MethodBase::GetParameters() */, L_5);
		U3CGetConstructorInjectablesU3Ec__AnonStorey181_t3473840153 * L_7 = V_0;
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)U3CGetConstructorInjectablesU3Ec__AnonStorey181_U3CU3Em__2B0_m3973830771_MethodInfo_var);
		Func_2_t3562324733 * L_9 = (Func_2_t3562324733 *)il2cpp_codegen_object_new(Func_2_t3562324733_il2cpp_TypeInfo_var);
		Func_2__ctor_m379451741(L_9, L_7, L_8, /*hidden argument*/Func_2__ctor_m379451741_MethodInfo_var);
		Il2CppObject* L_10 = Enumerable_Select_TisParameterInfo_t2610273829_TisInjectableInfo_t1147709774_m2840075429(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)L_6, L_9, /*hidden argument*/Enumerable_Select_TisParameterInfo_t2610273829_TisInjectableInfo_t1147709774_m2840075429_MethodInfo_var);
		return L_10;
	}
}
// Zenject.InjectableInfo Zenject.TypeAnalyzer::CreateInjectableInfoForParam(System.Type,System.Reflection.ParameterInfo)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* InjectableInfo_t1147709774_il2cpp_TypeInfo_var;
extern const MethodInfo* TypeExtensions_AllAttributes_TisInjectAttribute_t2791321440_m1093563446_MethodInfo_var;
extern const MethodInfo* Enumerable_ToList_TisInjectAttribute_t2791321440_m2935078709_MethodInfo_var;
extern const MethodInfo* TypeExtensions_AllAttributes_TisInjectOptionalAttribute_t899944672_m2359032246_MethodInfo_var;
extern const MethodInfo* Enumerable_ToList_TisInjectOptionalAttribute_t899944672_m3844690613_MethodInfo_var;
extern const MethodInfo* LinqExtensions_IsEmpty_TisInjectAttribute_t2791321440_m3377630699_MethodInfo_var;
extern const MethodInfo* LinqExtensions_IsEmpty_TisInjectOptionalAttribute_t899944672_m428018539_MethodInfo_var;
extern const MethodInfo* Enumerable_Any_TisInjectAttribute_t2791321440_m2761425413_MethodInfo_var;
extern const MethodInfo* Enumerable_Single_TisInjectAttribute_t2791321440_m3640425398_MethodInfo_var;
extern const MethodInfo* Enumerable_Any_TisInjectOptionalAttribute_t899944672_m4043818373_MethodInfo_var;
extern const MethodInfo* Enumerable_Single_TisInjectOptionalAttribute_t899944672_m1563688502_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3546579937;
extern const uint32_t TypeAnalyzer_CreateInjectableInfoForParam_m3638764108_MetadataUsageId;
extern "C"  InjectableInfo_t1147709774 * TypeAnalyzer_CreateInjectableInfoForParam_m3638764108 (Il2CppObject * __this /* static, unused */, Type_t * ___parentType0, ParameterInfo_t2610273829 * ___paramInfo1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeAnalyzer_CreateInjectableInfoForParam_m3638764108_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t3588280409 * V_0 = NULL;
	List_1_t1696903641 * V_1 = NULL;
	String_t* V_2 = NULL;
	bool V_3 = false;
	int32_t G_B3_0 = 0;
	int32_t G_B10_0 = 0;
	Il2CppObject * G_B12_0 = NULL;
	Type_t * G_B12_1 = NULL;
	Type_t * G_B12_2 = NULL;
	String_t* G_B12_3 = NULL;
	String_t* G_B12_4 = NULL;
	int32_t G_B12_5 = 0;
	Il2CppObject * G_B11_0 = NULL;
	Type_t * G_B11_1 = NULL;
	Type_t * G_B11_2 = NULL;
	String_t* G_B11_3 = NULL;
	String_t* G_B11_4 = NULL;
	int32_t G_B11_5 = 0;
	Il2CppObject * G_B13_0 = NULL;
	Il2CppObject * G_B13_1 = NULL;
	Type_t * G_B13_2 = NULL;
	Type_t * G_B13_3 = NULL;
	String_t* G_B13_4 = NULL;
	String_t* G_B13_5 = NULL;
	int32_t G_B13_6 = 0;
	{
		ParameterInfo_t2610273829 * L_0 = ___paramInfo1;
		Il2CppObject* L_1 = TypeExtensions_AllAttributes_TisInjectAttribute_t2791321440_m1093563446(NULL /*static, unused*/, L_0, /*hidden argument*/TypeExtensions_AllAttributes_TisInjectAttribute_t2791321440_m1093563446_MethodInfo_var);
		List_1_t3588280409 * L_2 = Enumerable_ToList_TisInjectAttribute_t2791321440_m2935078709(NULL /*static, unused*/, L_1, /*hidden argument*/Enumerable_ToList_TisInjectAttribute_t2791321440_m2935078709_MethodInfo_var);
		V_0 = L_2;
		ParameterInfo_t2610273829 * L_3 = ___paramInfo1;
		Il2CppObject* L_4 = TypeExtensions_AllAttributes_TisInjectOptionalAttribute_t899944672_m2359032246(NULL /*static, unused*/, L_3, /*hidden argument*/TypeExtensions_AllAttributes_TisInjectOptionalAttribute_t899944672_m2359032246_MethodInfo_var);
		List_1_t1696903641 * L_5 = Enumerable_ToList_TisInjectOptionalAttribute_t899944672_m3844690613(NULL /*static, unused*/, L_4, /*hidden argument*/Enumerable_ToList_TisInjectOptionalAttribute_t899944672_m3844690613_MethodInfo_var);
		V_1 = L_5;
		V_2 = (String_t*)NULL;
		List_1_t3588280409 * L_6 = V_0;
		bool L_7 = LinqExtensions_IsEmpty_TisInjectAttribute_t2791321440_m3377630699(NULL /*static, unused*/, L_6, /*hidden argument*/LinqExtensions_IsEmpty_TisInjectAttribute_t2791321440_m3377630699_MethodInfo_var);
		if (L_7)
		{
			goto IL_002d;
		}
	}
	{
		List_1_t1696903641 * L_8 = V_1;
		bool L_9 = LinqExtensions_IsEmpty_TisInjectOptionalAttribute_t899944672_m428018539(NULL /*static, unused*/, L_8, /*hidden argument*/LinqExtensions_IsEmpty_TisInjectOptionalAttribute_t899944672_m428018539_MethodInfo_var);
		G_B3_0 = ((int32_t)(L_9));
		goto IL_002e;
	}

IL_002d:
	{
		G_B3_0 = 1;
	}

IL_002e:
	{
		ObjectU5BU5D_t11523773* L_10 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)2));
		ParameterInfo_t2610273829 * L_11 = ___paramInfo1;
		NullCheck(L_11);
		String_t* L_12 = VirtFuncInvoker0< String_t* >::Invoke(11 /* System.String System.Reflection.ParameterInfo::get_Name() */, L_11);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		ArrayElementTypeCheck (L_10, L_12);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_12);
		ObjectU5BU5D_t11523773* L_13 = L_10;
		Type_t * L_14 = ___parentType0;
		String_t* L_15 = TypeExtensions_Name_m3754378430(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 1);
		ArrayElementTypeCheck (L_13, L_15);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_15);
		Assert_That_m2934751473(NULL /*static, unused*/, (bool)G_B3_0, _stringLiteral3546579937, L_13, /*hidden argument*/NULL);
		List_1_t3588280409 * L_16 = V_0;
		bool L_17 = Enumerable_Any_TisInjectAttribute_t2791321440_m2761425413(NULL /*static, unused*/, L_16, /*hidden argument*/Enumerable_Any_TisInjectAttribute_t2791321440_m2761425413_MethodInfo_var);
		if (!L_17)
		{
			goto IL_006c;
		}
	}
	{
		List_1_t3588280409 * L_18 = V_0;
		InjectAttribute_t2791321440 * L_19 = Enumerable_Single_TisInjectAttribute_t2791321440_m3640425398(NULL /*static, unused*/, L_18, /*hidden argument*/Enumerable_Single_TisInjectAttribute_t2791321440_m3640425398_MethodInfo_var);
		NullCheck(L_19);
		String_t* L_20 = InjectAttribute_get_Identifier_m3727677780(L_19, /*hidden argument*/NULL);
		V_2 = L_20;
		goto IL_0083;
	}

IL_006c:
	{
		List_1_t1696903641 * L_21 = V_1;
		bool L_22 = Enumerable_Any_TisInjectOptionalAttribute_t899944672_m4043818373(NULL /*static, unused*/, L_21, /*hidden argument*/Enumerable_Any_TisInjectOptionalAttribute_t899944672_m4043818373_MethodInfo_var);
		if (!L_22)
		{
			goto IL_0083;
		}
	}
	{
		List_1_t1696903641 * L_23 = V_1;
		InjectOptionalAttribute_t899944672 * L_24 = Enumerable_Single_TisInjectOptionalAttribute_t899944672_m1563688502(NULL /*static, unused*/, L_23, /*hidden argument*/Enumerable_Single_TisInjectOptionalAttribute_t899944672_m1563688502_MethodInfo_var);
		NullCheck(L_24);
		String_t* L_25 = InjectOptionalAttribute_get_Identifier_m2651514068(L_24, /*hidden argument*/NULL);
		V_2 = L_25;
	}

IL_0083:
	{
		ParameterInfo_t2610273829 * L_26 = ___paramInfo1;
		NullCheck(L_26);
		int32_t L_27 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Reflection.ParameterAttributes System.Reflection.ParameterInfo::get_Attributes() */, L_26);
		V_3 = (bool)((((int32_t)((int32_t)((int32_t)L_27&(int32_t)((int32_t)4096)))) == ((int32_t)((int32_t)4096)))? 1 : 0);
		bool L_28 = V_3;
		if (L_28)
		{
			goto IL_00a5;
		}
	}
	{
		List_1_t1696903641 * L_29 = V_1;
		bool L_30 = Enumerable_Any_TisInjectOptionalAttribute_t899944672_m4043818373(NULL /*static, unused*/, L_29, /*hidden argument*/Enumerable_Any_TisInjectOptionalAttribute_t899944672_m4043818373_MethodInfo_var);
		G_B10_0 = ((int32_t)(L_30));
		goto IL_00a6;
	}

IL_00a5:
	{
		G_B10_0 = 1;
	}

IL_00a6:
	{
		String_t* L_31 = V_2;
		ParameterInfo_t2610273829 * L_32 = ___paramInfo1;
		NullCheck(L_32);
		String_t* L_33 = VirtFuncInvoker0< String_t* >::Invoke(11 /* System.String System.Reflection.ParameterInfo::get_Name() */, L_32);
		ParameterInfo_t2610273829 * L_34 = ___paramInfo1;
		NullCheck(L_34);
		Type_t * L_35 = VirtFuncInvoker0< Type_t * >::Invoke(7 /* System.Type System.Reflection.ParameterInfo::get_ParameterType() */, L_34);
		Type_t * L_36 = ___parentType0;
		bool L_37 = V_3;
		G_B11_0 = NULL;
		G_B11_1 = L_36;
		G_B11_2 = L_35;
		G_B11_3 = L_33;
		G_B11_4 = L_31;
		G_B11_5 = G_B10_0;
		if (!L_37)
		{
			G_B12_0 = NULL;
			G_B12_1 = L_36;
			G_B12_2 = L_35;
			G_B12_3 = L_33;
			G_B12_4 = L_31;
			G_B12_5 = G_B10_0;
			goto IL_00c6;
		}
	}
	{
		ParameterInfo_t2610273829 * L_38 = ___paramInfo1;
		NullCheck(L_38);
		Il2CppObject * L_39 = VirtFuncInvoker0< Il2CppObject * >::Invoke(9 /* System.Object System.Reflection.ParameterInfo::get_DefaultValue() */, L_38);
		G_B13_0 = L_39;
		G_B13_1 = G_B11_0;
		G_B13_2 = G_B11_1;
		G_B13_3 = G_B11_2;
		G_B13_4 = G_B11_3;
		G_B13_5 = G_B11_4;
		G_B13_6 = G_B11_5;
		goto IL_00c7;
	}

IL_00c6:
	{
		G_B13_0 = NULL;
		G_B13_1 = G_B12_0;
		G_B13_2 = G_B12_1;
		G_B13_3 = G_B12_2;
		G_B13_4 = G_B12_3;
		G_B13_5 = G_B12_4;
		G_B13_6 = G_B12_5;
	}

IL_00c7:
	{
		InjectableInfo_t1147709774 * L_40 = (InjectableInfo_t1147709774 *)il2cpp_codegen_object_new(InjectableInfo_t1147709774_il2cpp_TypeInfo_var);
		InjectableInfo__ctor_m1847028633(L_40, (bool)G_B13_6, G_B13_5, G_B13_4, G_B13_3, G_B13_2, (Action_2_t4105459918 *)G_B13_1, G_B13_0, /*hidden argument*/NULL);
		return L_40;
	}
}
// System.Collections.Generic.List`1<Zenject.PostInjectableInfo> Zenject.TypeAnalyzer::GetPostInjectMethods(System.Type)
extern Il2CppClass* U3CGetPostInjectMethodsU3Ec__AnonStorey182_t1421092187_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeAnalyzer_t2089278869_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t3166396148_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t1507838298_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t3877242631_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_1_t2038408337_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t649360429_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t3562324733_il2cpp_TypeInfo_var;
extern Il2CppClass* PostInjectableInfo_t3080283662_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* TypeAnalyzer_U3CGetPostInjectMethodsU3Em__2B1_m1279773076_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m2570232330_MethodInfo_var;
extern const MethodInfo* Enumerable_Where_TisMethodInfo_t_m4151038873_MethodInfo_var;
extern const MethodInfo* Enumerable_ToList_TisMethodInfo_t_m285312770_MethodInfo_var;
extern const MethodInfo* MiscExtensions_Yield_TisType_t_m2837271301_MethodInfo_var;
extern const MethodInfo* Enumerable_Concat_TisType_t_m2838388285_MethodInfo_var;
extern const MethodInfo* Enumerable_Reverse_TisType_t_m567116288_MethodInfo_var;
extern const MethodInfo* Enumerable_ToList_TisType_t_m3338730594_MethodInfo_var;
extern const MethodInfo* U3CGetPostInjectMethodsU3Ec__AnonStorey182_U3CU3Em__2B2_m3523916186_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m1369530532_MethodInfo_var;
extern const MethodInfo* Enumerable_OrderBy_TisMethodInfo_t_TisInt32_t2847414787_m4018660381_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m380485411_MethodInfo_var;
extern const MethodInfo* U3CGetPostInjectMethodsU3Ec__AnonStorey182_U3CU3Em__2B3_m461157480_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m379451741_MethodInfo_var;
extern const MethodInfo* Enumerable_Select_TisParameterInfo_t2610273829_TisInjectableInfo_t1147709774_m2840075429_MethodInfo_var;
extern const MethodInfo* Enumerable_ToList_TisInjectableInfo_t1147709774_m755394695_MethodInfo_var;
extern const uint32_t TypeAnalyzer_GetPostInjectMethods_m3454744571_MetadataUsageId;
extern "C"  List_1_t3877242631 * TypeAnalyzer_GetPostInjectMethods_m3454744571 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeAnalyzer_GetPostInjectMethods_m3454744571_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t4258180246 * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	List_1_t3877242631 * V_2 = NULL;
	MethodInfo_t * V_3 = NULL;
	Il2CppObject* V_4 = NULL;
	ParameterInfoU5BU5D_t1127461800* V_5 = NULL;
	U3CGetPostInjectMethodsU3Ec__AnonStorey182_t1421092187 * V_6 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	Il2CppObject* G_B2_0 = NULL;
	Il2CppObject* G_B1_0 = NULL;
	{
		U3CGetPostInjectMethodsU3Ec__AnonStorey182_t1421092187 * L_0 = (U3CGetPostInjectMethodsU3Ec__AnonStorey182_t1421092187 *)il2cpp_codegen_object_new(U3CGetPostInjectMethodsU3Ec__AnonStorey182_t1421092187_il2cpp_TypeInfo_var);
		U3CGetPostInjectMethodsU3Ec__AnonStorey182__ctor_m1647722528(L_0, /*hidden argument*/NULL);
		V_6 = L_0;
		U3CGetPostInjectMethodsU3Ec__AnonStorey182_t1421092187 * L_1 = V_6;
		Type_t * L_2 = ___type0;
		NullCheck(L_1);
		L_1->set_type_1(L_2);
		U3CGetPostInjectMethodsU3Ec__AnonStorey182_t1421092187 * L_3 = V_6;
		NullCheck(L_3);
		Type_t * L_4 = L_3->get_type_1();
		Il2CppObject* L_5 = TypeExtensions_GetAllMethods_m2967502904(NULL /*static, unused*/, L_4, ((int32_t)52), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TypeAnalyzer_t2089278869_il2cpp_TypeInfo_var);
		Func_2_t3166396148 * L_6 = ((TypeAnalyzer_t2089278869_StaticFields*)TypeAnalyzer_t2089278869_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_1();
		G_B1_0 = L_5;
		if (L_6)
		{
			G_B2_0 = L_5;
			goto IL_0035;
		}
	}
	{
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)TypeAnalyzer_U3CGetPostInjectMethodsU3Em__2B1_m1279773076_MethodInfo_var);
		Func_2_t3166396148 * L_8 = (Func_2_t3166396148 *)il2cpp_codegen_object_new(Func_2_t3166396148_il2cpp_TypeInfo_var);
		Func_2__ctor_m2570232330(L_8, NULL, L_7, /*hidden argument*/Func_2__ctor_m2570232330_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(TypeAnalyzer_t2089278869_il2cpp_TypeInfo_var);
		((TypeAnalyzer_t2089278869_StaticFields*)TypeAnalyzer_t2089278869_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache1_1(L_8);
		G_B2_0 = G_B1_0;
	}

IL_0035:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TypeAnalyzer_t2089278869_il2cpp_TypeInfo_var);
		Func_2_t3166396148 * L_9 = ((TypeAnalyzer_t2089278869_StaticFields*)TypeAnalyzer_t2089278869_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_1();
		Il2CppObject* L_10 = Enumerable_Where_TisMethodInfo_t_m4151038873(NULL /*static, unused*/, G_B2_0, L_9, /*hidden argument*/Enumerable_Where_TisMethodInfo_t_m4151038873_MethodInfo_var);
		List_1_t4258180246 * L_11 = Enumerable_ToList_TisMethodInfo_t_m285312770(NULL /*static, unused*/, L_10, /*hidden argument*/Enumerable_ToList_TisMethodInfo_t_m285312770_MethodInfo_var);
		V_0 = L_11;
		U3CGetPostInjectMethodsU3Ec__AnonStorey182_t1421092187 * L_12 = V_6;
		U3CGetPostInjectMethodsU3Ec__AnonStorey182_t1421092187 * L_13 = V_6;
		NullCheck(L_13);
		Type_t * L_14 = L_13->get_type_1();
		Il2CppObject* L_15 = MiscExtensions_Yield_TisType_t_m2837271301(NULL /*static, unused*/, L_14, /*hidden argument*/MiscExtensions_Yield_TisType_t_m2837271301_MethodInfo_var);
		U3CGetPostInjectMethodsU3Ec__AnonStorey182_t1421092187 * L_16 = V_6;
		NullCheck(L_16);
		Type_t * L_17 = L_16->get_type_1();
		Il2CppObject* L_18 = TypeExtensions_GetParentTypes_m3922980882(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		Il2CppObject* L_19 = Enumerable_Concat_TisType_t_m2838388285(NULL /*static, unused*/, L_15, L_18, /*hidden argument*/Enumerable_Concat_TisType_t_m2838388285_MethodInfo_var);
		Il2CppObject* L_20 = Enumerable_Reverse_TisType_t_m567116288(NULL /*static, unused*/, L_19, /*hidden argument*/Enumerable_Reverse_TisType_t_m567116288_MethodInfo_var);
		List_1_t3576188904 * L_21 = Enumerable_ToList_TisType_t_m3338730594(NULL /*static, unused*/, L_20, /*hidden argument*/Enumerable_ToList_TisType_t_m3338730594_MethodInfo_var);
		NullCheck(L_12);
		L_12->set_heirarchyList_0(L_21);
		List_1_t4258180246 * L_22 = V_0;
		U3CGetPostInjectMethodsU3Ec__AnonStorey182_t1421092187 * L_23 = V_6;
		IntPtr_t L_24;
		L_24.set_m_value_0((void*)(void*)U3CGetPostInjectMethodsU3Ec__AnonStorey182_U3CU3Em__2B2_m3523916186_MethodInfo_var);
		Func_2_t1507838298 * L_25 = (Func_2_t1507838298 *)il2cpp_codegen_object_new(Func_2_t1507838298_il2cpp_TypeInfo_var);
		Func_2__ctor_m1369530532(L_25, L_23, L_24, /*hidden argument*/Func_2__ctor_m1369530532_MethodInfo_var);
		Il2CppObject* L_26 = Enumerable_OrderBy_TisMethodInfo_t_TisInt32_t2847414787_m4018660381(NULL /*static, unused*/, L_22, L_25, /*hidden argument*/Enumerable_OrderBy_TisMethodInfo_t_TisInt32_t2847414787_m4018660381_MethodInfo_var);
		V_1 = L_26;
		List_1_t3877242631 * L_27 = (List_1_t3877242631 *)il2cpp_codegen_object_new(List_1_t3877242631_il2cpp_TypeInfo_var);
		List_1__ctor_m380485411(L_27, /*hidden argument*/List_1__ctor_m380485411_MethodInfo_var);
		V_2 = L_27;
		Il2CppObject* L_28 = V_1;
		NullCheck(L_28);
		Il2CppObject* L_29 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Reflection.MethodInfo>::GetEnumerator() */, IEnumerable_1_t2038408337_il2cpp_TypeInfo_var, L_28);
		V_4 = L_29;
	}

IL_0095:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00cf;
		}

IL_009a:
		{
			Il2CppObject* L_30 = V_4;
			NullCheck(L_30);
			MethodInfo_t * L_31 = InterfaceFuncInvoker0< MethodInfo_t * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Reflection.MethodInfo>::get_Current() */, IEnumerator_1_t649360429_il2cpp_TypeInfo_var, L_30);
			V_3 = L_31;
			MethodInfo_t * L_32 = V_3;
			NullCheck(L_32);
			ParameterInfoU5BU5D_t1127461800* L_33 = VirtFuncInvoker0< ParameterInfoU5BU5D_t1127461800* >::Invoke(15 /* System.Reflection.ParameterInfo[] System.Reflection.MethodBase::GetParameters() */, L_32);
			V_5 = L_33;
			List_1_t3877242631 * L_34 = V_2;
			MethodInfo_t * L_35 = V_3;
			ParameterInfoU5BU5D_t1127461800* L_36 = V_5;
			U3CGetPostInjectMethodsU3Ec__AnonStorey182_t1421092187 * L_37 = V_6;
			IntPtr_t L_38;
			L_38.set_m_value_0((void*)(void*)U3CGetPostInjectMethodsU3Ec__AnonStorey182_U3CU3Em__2B3_m461157480_MethodInfo_var);
			Func_2_t3562324733 * L_39 = (Func_2_t3562324733 *)il2cpp_codegen_object_new(Func_2_t3562324733_il2cpp_TypeInfo_var);
			Func_2__ctor_m379451741(L_39, L_37, L_38, /*hidden argument*/Func_2__ctor_m379451741_MethodInfo_var);
			Il2CppObject* L_40 = Enumerable_Select_TisParameterInfo_t2610273829_TisInjectableInfo_t1147709774_m2840075429(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)L_36, L_39, /*hidden argument*/Enumerable_Select_TisParameterInfo_t2610273829_TisInjectableInfo_t1147709774_m2840075429_MethodInfo_var);
			List_1_t1944668743 * L_41 = Enumerable_ToList_TisInjectableInfo_t1147709774_m755394695(NULL /*static, unused*/, L_40, /*hidden argument*/Enumerable_ToList_TisInjectableInfo_t1147709774_m755394695_MethodInfo_var);
			PostInjectableInfo_t3080283662 * L_42 = (PostInjectableInfo_t3080283662 *)il2cpp_codegen_object_new(PostInjectableInfo_t3080283662_il2cpp_TypeInfo_var);
			PostInjectableInfo__ctor_m595808628(L_42, L_35, L_41, /*hidden argument*/NULL);
			NullCheck(L_34);
			VirtActionInvoker1< PostInjectableInfo_t3080283662 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Zenject.PostInjectableInfo>::Add(!0) */, L_34, L_42);
		}

IL_00cf:
		{
			Il2CppObject* L_43 = V_4;
			NullCheck(L_43);
			bool L_44 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_43);
			if (L_44)
			{
				goto IL_009a;
			}
		}

IL_00db:
		{
			IL2CPP_LEAVE(0xED, FINALLY_00e0);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00e0;
	}

FINALLY_00e0:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_45 = V_4;
			if (L_45)
			{
				goto IL_00e5;
			}
		}

IL_00e4:
		{
			IL2CPP_END_FINALLY(224)
		}

IL_00e5:
		{
			Il2CppObject* L_46 = V_4;
			NullCheck(L_46);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_46);
			IL2CPP_END_FINALLY(224)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(224)
	{
		IL2CPP_JUMP_TBL(0xED, IL_00ed)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00ed:
	{
		List_1_t3877242631 * L_47 = V_2;
		return L_47;
	}
}
// System.Collections.Generic.IEnumerable`1<Zenject.InjectableInfo> Zenject.TypeAnalyzer::GetPropertyInjectables(System.Type)
extern Il2CppClass* U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871_il2cpp_TypeInfo_var;
extern const uint32_t TypeAnalyzer_GetPropertyInjectables_m2870845588_MetadataUsageId;
extern "C"  Il2CppObject* TypeAnalyzer_GetPropertyInjectables_m2870845588 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeAnalyzer_GetPropertyInjectables_m2870845588_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871 * V_0 = NULL;
	{
		U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871 * L_0 = (U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871 *)il2cpp_codegen_object_new(U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871_il2cpp_TypeInfo_var);
		U3CGetPropertyInjectablesU3Ec__Iterator47__ctor_m1864367420(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871 * L_1 = V_0;
		Type_t * L_2 = ___type0;
		NullCheck(L_1);
		L_1->set_type_0(L_2);
		U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871 * L_3 = V_0;
		Type_t * L_4 = ___type0;
		NullCheck(L_3);
		L_3->set_U3CU24U3Etype_6(L_4);
		U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871 * L_5 = V_0;
		U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871 * L_6 = L_5;
		NullCheck(L_6);
		L_6->set_U24PC_4(((int32_t)-2));
		return L_6;
	}
}
// System.Collections.Generic.IEnumerable`1<Zenject.InjectableInfo> Zenject.TypeAnalyzer::GetFieldInjectables(System.Type)
extern Il2CppClass* U3CGetFieldInjectablesU3Ec__Iterator48_t3210264959_il2cpp_TypeInfo_var;
extern const uint32_t TypeAnalyzer_GetFieldInjectables_m2698805947_MetadataUsageId;
extern "C"  Il2CppObject* TypeAnalyzer_GetFieldInjectables_m2698805947 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeAnalyzer_GetFieldInjectables_m2698805947_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetFieldInjectablesU3Ec__Iterator48_t3210264959 * V_0 = NULL;
	{
		U3CGetFieldInjectablesU3Ec__Iterator48_t3210264959 * L_0 = (U3CGetFieldInjectablesU3Ec__Iterator48_t3210264959 *)il2cpp_codegen_object_new(U3CGetFieldInjectablesU3Ec__Iterator48_t3210264959_il2cpp_TypeInfo_var);
		U3CGetFieldInjectablesU3Ec__Iterator48__ctor_m2018515068(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetFieldInjectablesU3Ec__Iterator48_t3210264959 * L_1 = V_0;
		Type_t * L_2 = ___type0;
		NullCheck(L_1);
		L_1->set_type_0(L_2);
		U3CGetFieldInjectablesU3Ec__Iterator48_t3210264959 * L_3 = V_0;
		Type_t * L_4 = ___type0;
		NullCheck(L_3);
		L_3->set_U3CU24U3Etype_6(L_4);
		U3CGetFieldInjectablesU3Ec__Iterator48_t3210264959 * L_5 = V_0;
		U3CGetFieldInjectablesU3Ec__Iterator48_t3210264959 * L_6 = L_5;
		NullCheck(L_6);
		L_6->set_U24PC_4(((int32_t)-2));
		return L_6;
	}
}
// Zenject.InjectableInfo Zenject.TypeAnalyzer::CreateForMember(System.Reflection.MemberInfo,System.Type)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* FieldInfo_t_il2cpp_TypeInfo_var;
extern Il2CppClass* U3CCreateForMemberU3Ec__AnonStorey183_t1843898456_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_2_t4105459918_il2cpp_TypeInfo_var;
extern Il2CppClass* U3CCreateForMemberU3Ec__AnonStorey184_t1843898457_il2cpp_TypeInfo_var;
extern Il2CppClass* PropertyInfo_t_il2cpp_TypeInfo_var;
extern Il2CppClass* InjectableInfo_t1147709774_il2cpp_TypeInfo_var;
extern const MethodInfo* TypeExtensions_AllAttributes_TisInjectAttribute_t2791321440_m1093563446_MethodInfo_var;
extern const MethodInfo* Enumerable_ToList_TisInjectAttribute_t2791321440_m2935078709_MethodInfo_var;
extern const MethodInfo* TypeExtensions_AllAttributes_TisInjectOptionalAttribute_t899944672_m2359032246_MethodInfo_var;
extern const MethodInfo* Enumerable_ToList_TisInjectOptionalAttribute_t899944672_m3844690613_MethodInfo_var;
extern const MethodInfo* LinqExtensions_IsEmpty_TisInjectAttribute_t2791321440_m3377630699_MethodInfo_var;
extern const MethodInfo* LinqExtensions_IsEmpty_TisInjectOptionalAttribute_t899944672_m428018539_MethodInfo_var;
extern const MethodInfo* Enumerable_Any_TisInjectAttribute_t2791321440_m2761425413_MethodInfo_var;
extern const MethodInfo* Enumerable_Single_TisInjectAttribute_t2791321440_m3640425398_MethodInfo_var;
extern const MethodInfo* Enumerable_Any_TisInjectOptionalAttribute_t899944672_m4043818373_MethodInfo_var;
extern const MethodInfo* Enumerable_Single_TisInjectOptionalAttribute_t899944672_m1563688502_MethodInfo_var;
extern const MethodInfo* U3CCreateForMemberU3Ec__AnonStorey183_U3CU3Em__2B4_m2405414520_MethodInfo_var;
extern const MethodInfo* Action_2__ctor_m3253598474_MethodInfo_var;
extern const MethodInfo* U3CCreateForMemberU3Ec__AnonStorey184_U3CU3Em__2B5_m3878310646_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1105121791;
extern const uint32_t TypeAnalyzer_CreateForMember_m3555310243_MetadataUsageId;
extern "C"  InjectableInfo_t1147709774 * TypeAnalyzer_CreateForMember_m3555310243 (Il2CppObject * __this /* static, unused */, MemberInfo_t * ___memInfo0, Type_t * ___parentType1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeAnalyzer_CreateForMember_m3555310243_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t3588280409 * V_0 = NULL;
	List_1_t1696903641 * V_1 = NULL;
	String_t* V_2 = NULL;
	Type_t * V_3 = NULL;
	Action_2_t4105459918 * V_4 = NULL;
	U3CCreateForMemberU3Ec__AnonStorey183_t1843898456 * V_5 = NULL;
	U3CCreateForMemberU3Ec__AnonStorey184_t1843898457 * V_6 = NULL;
	int32_t G_B3_0 = 0;
	{
		MemberInfo_t * L_0 = ___memInfo0;
		Il2CppObject* L_1 = TypeExtensions_AllAttributes_TisInjectAttribute_t2791321440_m1093563446(NULL /*static, unused*/, L_0, /*hidden argument*/TypeExtensions_AllAttributes_TisInjectAttribute_t2791321440_m1093563446_MethodInfo_var);
		List_1_t3588280409 * L_2 = Enumerable_ToList_TisInjectAttribute_t2791321440_m2935078709(NULL /*static, unused*/, L_1, /*hidden argument*/Enumerable_ToList_TisInjectAttribute_t2791321440_m2935078709_MethodInfo_var);
		V_0 = L_2;
		MemberInfo_t * L_3 = ___memInfo0;
		Il2CppObject* L_4 = TypeExtensions_AllAttributes_TisInjectOptionalAttribute_t899944672_m2359032246(NULL /*static, unused*/, L_3, /*hidden argument*/TypeExtensions_AllAttributes_TisInjectOptionalAttribute_t899944672_m2359032246_MethodInfo_var);
		List_1_t1696903641 * L_5 = Enumerable_ToList_TisInjectOptionalAttribute_t899944672_m3844690613(NULL /*static, unused*/, L_4, /*hidden argument*/Enumerable_ToList_TisInjectOptionalAttribute_t899944672_m3844690613_MethodInfo_var);
		V_1 = L_5;
		V_2 = (String_t*)NULL;
		List_1_t3588280409 * L_6 = V_0;
		bool L_7 = LinqExtensions_IsEmpty_TisInjectAttribute_t2791321440_m3377630699(NULL /*static, unused*/, L_6, /*hidden argument*/LinqExtensions_IsEmpty_TisInjectAttribute_t2791321440_m3377630699_MethodInfo_var);
		if (L_7)
		{
			goto IL_002d;
		}
	}
	{
		List_1_t1696903641 * L_8 = V_1;
		bool L_9 = LinqExtensions_IsEmpty_TisInjectOptionalAttribute_t899944672_m428018539(NULL /*static, unused*/, L_8, /*hidden argument*/LinqExtensions_IsEmpty_TisInjectOptionalAttribute_t899944672_m428018539_MethodInfo_var);
		G_B3_0 = ((int32_t)(L_9));
		goto IL_002e;
	}

IL_002d:
	{
		G_B3_0 = 1;
	}

IL_002e:
	{
		ObjectU5BU5D_t11523773* L_10 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)2));
		MemberInfo_t * L_11 = ___memInfo0;
		NullCheck(L_11);
		String_t* L_12 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Reflection.MemberInfo::get_Name() */, L_11);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		ArrayElementTypeCheck (L_10, L_12);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_12);
		ObjectU5BU5D_t11523773* L_13 = L_10;
		Type_t * L_14 = ___parentType1;
		String_t* L_15 = TypeExtensions_Name_m3754378430(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 1);
		ArrayElementTypeCheck (L_13, L_15);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_15);
		Assert_That_m2934751473(NULL /*static, unused*/, (bool)G_B3_0, _stringLiteral1105121791, L_13, /*hidden argument*/NULL);
		List_1_t3588280409 * L_16 = V_0;
		bool L_17 = Enumerable_Any_TisInjectAttribute_t2791321440_m2761425413(NULL /*static, unused*/, L_16, /*hidden argument*/Enumerable_Any_TisInjectAttribute_t2791321440_m2761425413_MethodInfo_var);
		if (!L_17)
		{
			goto IL_006c;
		}
	}
	{
		List_1_t3588280409 * L_18 = V_0;
		InjectAttribute_t2791321440 * L_19 = Enumerable_Single_TisInjectAttribute_t2791321440_m3640425398(NULL /*static, unused*/, L_18, /*hidden argument*/Enumerable_Single_TisInjectAttribute_t2791321440_m3640425398_MethodInfo_var);
		NullCheck(L_19);
		String_t* L_20 = InjectAttribute_get_Identifier_m3727677780(L_19, /*hidden argument*/NULL);
		V_2 = L_20;
		goto IL_0083;
	}

IL_006c:
	{
		List_1_t1696903641 * L_21 = V_1;
		bool L_22 = Enumerable_Any_TisInjectOptionalAttribute_t899944672_m4043818373(NULL /*static, unused*/, L_21, /*hidden argument*/Enumerable_Any_TisInjectOptionalAttribute_t899944672_m4043818373_MethodInfo_var);
		if (!L_22)
		{
			goto IL_0083;
		}
	}
	{
		List_1_t1696903641 * L_23 = V_1;
		InjectOptionalAttribute_t899944672 * L_24 = Enumerable_Single_TisInjectOptionalAttribute_t899944672_m1563688502(NULL /*static, unused*/, L_23, /*hidden argument*/Enumerable_Single_TisInjectOptionalAttribute_t899944672_m1563688502_MethodInfo_var);
		NullCheck(L_24);
		String_t* L_25 = InjectOptionalAttribute_get_Identifier_m2651514068(L_24, /*hidden argument*/NULL);
		V_2 = L_25;
	}

IL_0083:
	{
		MemberInfo_t * L_26 = ___memInfo0;
		if (!((FieldInfo_t *)IsInstClass(L_26, FieldInfo_t_il2cpp_TypeInfo_var)))
		{
			goto IL_00c3;
		}
	}
	{
		U3CCreateForMemberU3Ec__AnonStorey183_t1843898456 * L_27 = (U3CCreateForMemberU3Ec__AnonStorey183_t1843898456 *)il2cpp_codegen_object_new(U3CCreateForMemberU3Ec__AnonStorey183_t1843898456_il2cpp_TypeInfo_var);
		U3CCreateForMemberU3Ec__AnonStorey183__ctor_m578813331(L_27, /*hidden argument*/NULL);
		V_5 = L_27;
		U3CCreateForMemberU3Ec__AnonStorey183_t1843898456 * L_28 = V_5;
		MemberInfo_t * L_29 = ___memInfo0;
		NullCheck(L_28);
		L_28->set_fieldInfo_0(((FieldInfo_t *)CastclassClass(L_29, FieldInfo_t_il2cpp_TypeInfo_var)));
		U3CCreateForMemberU3Ec__AnonStorey183_t1843898456 * L_30 = V_5;
		IntPtr_t L_31;
		L_31.set_m_value_0((void*)(void*)U3CCreateForMemberU3Ec__AnonStorey183_U3CU3Em__2B4_m2405414520_MethodInfo_var);
		Action_2_t4105459918 * L_32 = (Action_2_t4105459918 *)il2cpp_codegen_object_new(Action_2_t4105459918_il2cpp_TypeInfo_var);
		Action_2__ctor_m3253598474(L_32, L_30, L_31, /*hidden argument*/Action_2__ctor_m3253598474_MethodInfo_var);
		V_4 = L_32;
		U3CCreateForMemberU3Ec__AnonStorey183_t1843898456 * L_33 = V_5;
		NullCheck(L_33);
		FieldInfo_t * L_34 = L_33->get_fieldInfo_0();
		NullCheck(L_34);
		Type_t * L_35 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Reflection.FieldInfo::get_FieldType() */, L_34);
		V_3 = L_35;
		goto IL_0101;
	}

IL_00c3:
	{
		U3CCreateForMemberU3Ec__AnonStorey184_t1843898457 * L_36 = (U3CCreateForMemberU3Ec__AnonStorey184_t1843898457 *)il2cpp_codegen_object_new(U3CCreateForMemberU3Ec__AnonStorey184_t1843898457_il2cpp_TypeInfo_var);
		U3CCreateForMemberU3Ec__AnonStorey184__ctor_m382299826(L_36, /*hidden argument*/NULL);
		V_6 = L_36;
		MemberInfo_t * L_37 = ___memInfo0;
		Assert_That_m1320742921(NULL /*static, unused*/, (bool)((!(((Il2CppObject*)(PropertyInfo_t *)((PropertyInfo_t *)IsInstClass(L_37, PropertyInfo_t_il2cpp_TypeInfo_var))) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0), /*hidden argument*/NULL);
		U3CCreateForMemberU3Ec__AnonStorey184_t1843898457 * L_38 = V_6;
		MemberInfo_t * L_39 = ___memInfo0;
		NullCheck(L_38);
		L_38->set_propInfo_0(((PropertyInfo_t *)CastclassClass(L_39, PropertyInfo_t_il2cpp_TypeInfo_var)));
		U3CCreateForMemberU3Ec__AnonStorey184_t1843898457 * L_40 = V_6;
		IntPtr_t L_41;
		L_41.set_m_value_0((void*)(void*)U3CCreateForMemberU3Ec__AnonStorey184_U3CU3Em__2B5_m3878310646_MethodInfo_var);
		Action_2_t4105459918 * L_42 = (Action_2_t4105459918 *)il2cpp_codegen_object_new(Action_2_t4105459918_il2cpp_TypeInfo_var);
		Action_2__ctor_m3253598474(L_42, L_40, L_41, /*hidden argument*/Action_2__ctor_m3253598474_MethodInfo_var);
		V_4 = L_42;
		U3CCreateForMemberU3Ec__AnonStorey184_t1843898457 * L_43 = V_6;
		NullCheck(L_43);
		PropertyInfo_t * L_44 = L_43->get_propInfo_0();
		NullCheck(L_44);
		Type_t * L_45 = VirtFuncInvoker0< Type_t * >::Invoke(18 /* System.Type System.Reflection.PropertyInfo::get_PropertyType() */, L_44);
		V_3 = L_45;
	}

IL_0101:
	{
		List_1_t1696903641 * L_46 = V_1;
		bool L_47 = Enumerable_Any_TisInjectOptionalAttribute_t899944672_m4043818373(NULL /*static, unused*/, L_46, /*hidden argument*/Enumerable_Any_TisInjectOptionalAttribute_t899944672_m4043818373_MethodInfo_var);
		String_t* L_48 = V_2;
		MemberInfo_t * L_49 = ___memInfo0;
		NullCheck(L_49);
		String_t* L_50 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Reflection.MemberInfo::get_Name() */, L_49);
		Type_t * L_51 = V_3;
		Type_t * L_52 = ___parentType1;
		Action_2_t4105459918 * L_53 = V_4;
		InjectableInfo_t1147709774 * L_54 = (InjectableInfo_t1147709774 *)il2cpp_codegen_object_new(InjectableInfo_t1147709774_il2cpp_TypeInfo_var);
		InjectableInfo__ctor_m1847028633(L_54, L_47, L_48, L_50, L_51, L_52, L_53, NULL, /*hidden argument*/NULL);
		return L_54;
	}
}
// System.Reflection.ConstructorInfo Zenject.TypeAnalyzer::GetInjectConstructor(System.Type)
extern Il2CppClass* TypeAnalyzer_t2089278869_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t775214871_il2cpp_TypeInfo_var;
extern const MethodInfo* LinqExtensions_IsEmpty_TisConstructorInfo_t3542137334_m3362315587_MethodInfo_var;
extern const MethodInfo* LinqExtensions_HasMoreThan_TisConstructorInfo_t3542137334_m2138457031_MethodInfo_var;
extern const MethodInfo* TypeAnalyzer_U3CGetInjectConstructorU3Em__2B6_m523941450_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3208195165_MethodInfo_var;
extern const MethodInfo* Enumerable_Where_TisConstructorInfo_t3542137334_m963787402_MethodInfo_var;
extern const MethodInfo* Enumerable_SingleOrDefault_TisConstructorInfo_t3542137334_m2408381816_MethodInfo_var;
extern const uint32_t TypeAnalyzer_GetInjectConstructor_m1524952239_MetadataUsageId;
extern "C"  ConstructorInfo_t3542137334 * TypeAnalyzer_GetInjectConstructor_m1524952239 (Il2CppObject * __this /* static, unused */, Type_t * ___parentType0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeAnalyzer_GetInjectConstructor_m1524952239_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ConstructorInfoU5BU5D_t3572023667* V_0 = NULL;
	ConstructorInfoU5BU5D_t3572023667* G_B5_0 = NULL;
	ConstructorInfoU5BU5D_t3572023667* G_B4_0 = NULL;
	{
		Type_t * L_0 = ___parentType0;
		NullCheck(L_0);
		ConstructorInfoU5BU5D_t3572023667* L_1 = VirtFuncInvoker1< ConstructorInfoU5BU5D_t3572023667*, int32_t >::Invoke(90 /* System.Reflection.ConstructorInfo[] System.Type::GetConstructors(System.Reflection.BindingFlags) */, L_0, ((int32_t)52));
		V_0 = L_1;
		ConstructorInfoU5BU5D_t3572023667* L_2 = V_0;
		bool L_3 = LinqExtensions_IsEmpty_TisConstructorInfo_t3542137334_m3362315587(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)L_2, /*hidden argument*/LinqExtensions_IsEmpty_TisConstructorInfo_t3542137334_m3362315587_MethodInfo_var);
		if (!L_3)
		{
			goto IL_0016;
		}
	}
	{
		return (ConstructorInfo_t3542137334 *)NULL;
	}

IL_0016:
	{
		ConstructorInfoU5BU5D_t3572023667* L_4 = V_0;
		bool L_5 = LinqExtensions_HasMoreThan_TisConstructorInfo_t3542137334_m2138457031(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)L_4, 1, /*hidden argument*/LinqExtensions_HasMoreThan_TisConstructorInfo_t3542137334_m2138457031_MethodInfo_var);
		if (!L_5)
		{
			goto IL_004b;
		}
	}
	{
		ConstructorInfoU5BU5D_t3572023667* L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(TypeAnalyzer_t2089278869_il2cpp_TypeInfo_var);
		Func_2_t775214871 * L_7 = ((TypeAnalyzer_t2089278869_StaticFields*)TypeAnalyzer_t2089278869_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache2_2();
		G_B4_0 = L_6;
		if (L_7)
		{
			G_B5_0 = L_6;
			goto IL_003b;
		}
	}
	{
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)TypeAnalyzer_U3CGetInjectConstructorU3Em__2B6_m523941450_MethodInfo_var);
		Func_2_t775214871 * L_9 = (Func_2_t775214871 *)il2cpp_codegen_object_new(Func_2_t775214871_il2cpp_TypeInfo_var);
		Func_2__ctor_m3208195165(L_9, NULL, L_8, /*hidden argument*/Func_2__ctor_m3208195165_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(TypeAnalyzer_t2089278869_il2cpp_TypeInfo_var);
		((TypeAnalyzer_t2089278869_StaticFields*)TypeAnalyzer_t2089278869_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache2_2(L_9);
		G_B5_0 = G_B4_0;
	}

IL_003b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TypeAnalyzer_t2089278869_il2cpp_TypeInfo_var);
		Func_2_t775214871 * L_10 = ((TypeAnalyzer_t2089278869_StaticFields*)TypeAnalyzer_t2089278869_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache2_2();
		Il2CppObject* L_11 = Enumerable_Where_TisConstructorInfo_t3542137334_m963787402(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)G_B5_0, L_10, /*hidden argument*/Enumerable_Where_TisConstructorInfo_t3542137334_m963787402_MethodInfo_var);
		ConstructorInfo_t3542137334 * L_12 = Enumerable_SingleOrDefault_TisConstructorInfo_t3542137334_m2408381816(NULL /*static, unused*/, L_11, /*hidden argument*/Enumerable_SingleOrDefault_TisConstructorInfo_t3542137334_m2408381816_MethodInfo_var);
		return L_12;
	}

IL_004b:
	{
		ConstructorInfoU5BU5D_t3572023667* L_13 = V_0;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 0);
		int32_t L_14 = 0;
		return ((L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_14)));
	}
}
// System.Boolean Zenject.TypeAnalyzer::<GetPostInjectMethods>m__2B1(System.Reflection.MethodInfo)
extern const Il2CppType* PostInjectAttribute_t2571569824_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Enumerable_Any_TisIl2CppObject_m3411867191_MethodInfo_var;
extern const uint32_t TypeAnalyzer_U3CGetPostInjectMethodsU3Em__2B1_m1279773076_MetadataUsageId;
extern "C"  bool TypeAnalyzer_U3CGetPostInjectMethodsU3Em__2B1_m1279773076 (Il2CppObject * __this /* static, unused */, MethodInfo_t * ___x0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeAnalyzer_U3CGetPostInjectMethodsU3Em__2B1_m1279773076_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		MethodInfo_t * L_0 = ___x0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(PostInjectAttribute_t2571569824_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		ObjectU5BU5D_t11523773* L_2 = VirtFuncInvoker2< ObjectU5BU5D_t11523773*, Type_t *, bool >::Invoke(14 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_0, L_1, (bool)0);
		bool L_3 = Enumerable_Any_TisIl2CppObject_m3411867191(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)L_2, /*hidden argument*/Enumerable_Any_TisIl2CppObject_m3411867191_MethodInfo_var);
		return L_3;
	}
}
// System.Boolean Zenject.TypeAnalyzer::<GetInjectConstructor>m__2B6(System.Reflection.ConstructorInfo)
extern const MethodInfo* TypeExtensions_HasAttribute_TisInjectAttribute_t2791321440_m664968408_MethodInfo_var;
extern const uint32_t TypeAnalyzer_U3CGetInjectConstructorU3Em__2B6_m523941450_MetadataUsageId;
extern "C"  bool TypeAnalyzer_U3CGetInjectConstructorU3Em__2B6_m523941450 (Il2CppObject * __this /* static, unused */, ConstructorInfo_t3542137334 * ___c0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeAnalyzer_U3CGetInjectConstructorU3Em__2B6_m523941450_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ConstructorInfo_t3542137334 * L_0 = ___c0;
		bool L_1 = TypeExtensions_HasAttribute_TisInjectAttribute_t2791321440_m664968408(NULL /*static, unused*/, L_0, /*hidden argument*/TypeExtensions_HasAttribute_TisInjectAttribute_t2791321440_m664968408_MethodInfo_var);
		return L_1;
	}
}
// System.Void Zenject.TypeAnalyzer/<CreateForMember>c__AnonStorey183::.ctor()
extern "C"  void U3CCreateForMemberU3Ec__AnonStorey183__ctor_m578813331 (U3CCreateForMemberU3Ec__AnonStorey183_t1843898456 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Zenject.TypeAnalyzer/<CreateForMember>c__AnonStorey183::<>m__2B4(System.Object,System.Object)
extern "C"  void U3CCreateForMemberU3Ec__AnonStorey183_U3CU3Em__2B4_m2405414520 (U3CCreateForMemberU3Ec__AnonStorey183_t1843898456 * __this, Il2CppObject * ___injectable0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		FieldInfo_t * L_0 = __this->get_fieldInfo_0();
		Il2CppObject * L_1 = ___injectable0;
		Il2CppObject * L_2 = ___value1;
		NullCheck(L_0);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(25 /* System.Void System.Reflection.FieldInfo::SetValue(System.Object,System.Object) */, L_0, L_1, L_2);
		return;
	}
}
// System.Void Zenject.TypeAnalyzer/<CreateForMember>c__AnonStorey184::.ctor()
extern "C"  void U3CCreateForMemberU3Ec__AnonStorey184__ctor_m382299826 (U3CCreateForMemberU3Ec__AnonStorey184_t1843898457 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Zenject.TypeAnalyzer/<CreateForMember>c__AnonStorey184::<>m__2B5(System.Object,System.Object)
extern "C"  void U3CCreateForMemberU3Ec__AnonStorey184_U3CU3Em__2B5_m3878310646 (U3CCreateForMemberU3Ec__AnonStorey184_t1843898457 * __this, Il2CppObject * ___injectable0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		PropertyInfo_t * L_0 = __this->get_propInfo_0();
		Il2CppObject * L_1 = ___injectable0;
		Il2CppObject * L_2 = ___value1;
		NullCheck(L_0);
		VirtActionInvoker3< Il2CppObject *, Il2CppObject *, ObjectU5BU5D_t11523773* >::Invoke(27 /* System.Void System.Reflection.PropertyInfo::SetValue(System.Object,System.Object,System.Object[]) */, L_0, L_1, L_2, (ObjectU5BU5D_t11523773*)(ObjectU5BU5D_t11523773*)NULL);
		return;
	}
}
// System.Void Zenject.TypeAnalyzer/<GetConstructorInjectables>c__AnonStorey181::.ctor()
extern "C"  void U3CGetConstructorInjectablesU3Ec__AnonStorey181__ctor_m1614566642 (U3CGetConstructorInjectablesU3Ec__AnonStorey181_t3473840153 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// Zenject.InjectableInfo Zenject.TypeAnalyzer/<GetConstructorInjectables>c__AnonStorey181::<>m__2B0(System.Reflection.ParameterInfo)
extern Il2CppClass* TypeAnalyzer_t2089278869_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetConstructorInjectablesU3Ec__AnonStorey181_U3CU3Em__2B0_m3973830771_MetadataUsageId;
extern "C"  InjectableInfo_t1147709774 * U3CGetConstructorInjectablesU3Ec__AnonStorey181_U3CU3Em__2B0_m3973830771 (U3CGetConstructorInjectablesU3Ec__AnonStorey181_t3473840153 * __this, ParameterInfo_t2610273829 * ___paramInfo0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetConstructorInjectablesU3Ec__AnonStorey181_U3CU3Em__2B0_m3973830771_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = __this->get_parentType_0();
		ParameterInfo_t2610273829 * L_1 = ___paramInfo0;
		IL2CPP_RUNTIME_CLASS_INIT(TypeAnalyzer_t2089278869_il2cpp_TypeInfo_var);
		InjectableInfo_t1147709774 * L_2 = TypeAnalyzer_CreateInjectableInfoForParam_m3638764108(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void Zenject.TypeAnalyzer/<GetFieldInjectables>c__Iterator48::.ctor()
extern "C"  void U3CGetFieldInjectablesU3Ec__Iterator48__ctor_m2018515068 (U3CGetFieldInjectablesU3Ec__Iterator48_t3210264959 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// Zenject.InjectableInfo Zenject.TypeAnalyzer/<GetFieldInjectables>c__Iterator48::System.Collections.Generic.IEnumerator<Zenject.InjectableInfo>.get_Current()
extern "C"  InjectableInfo_t1147709774 * U3CGetFieldInjectablesU3Ec__Iterator48_System_Collections_Generic_IEnumeratorU3CZenject_InjectableInfoU3E_get_Current_m2006423853 (U3CGetFieldInjectablesU3Ec__Iterator48_t3210264959 * __this, const MethodInfo* method)
{
	{
		InjectableInfo_t1147709774 * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Object Zenject.TypeAnalyzer/<GetFieldInjectables>c__Iterator48::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetFieldInjectablesU3Ec__Iterator48_System_Collections_IEnumerator_get_Current_m2709704426 (U3CGetFieldInjectablesU3Ec__Iterator48_t3210264959 * __this, const MethodInfo* method)
{
	{
		InjectableInfo_t1147709774 * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Collections.IEnumerator Zenject.TypeAnalyzer/<GetFieldInjectables>c__Iterator48::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CGetFieldInjectablesU3Ec__Iterator48_System_Collections_IEnumerable_GetEnumerator_m2027911013 (U3CGetFieldInjectablesU3Ec__Iterator48_t3210264959 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = U3CGetFieldInjectablesU3Ec__Iterator48_System_Collections_Generic_IEnumerableU3CZenject_InjectableInfoU3E_GetEnumerator_m1311321116(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<Zenject.InjectableInfo> Zenject.TypeAnalyzer/<GetFieldInjectables>c__Iterator48::System.Collections.Generic.IEnumerable<Zenject.InjectableInfo>.GetEnumerator()
extern Il2CppClass* U3CGetFieldInjectablesU3Ec__Iterator48_t3210264959_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetFieldInjectablesU3Ec__Iterator48_System_Collections_Generic_IEnumerableU3CZenject_InjectableInfoU3E_GetEnumerator_m1311321116_MetadataUsageId;
extern "C"  Il2CppObject* U3CGetFieldInjectablesU3Ec__Iterator48_System_Collections_Generic_IEnumerableU3CZenject_InjectableInfoU3E_GetEnumerator_m1311321116 (U3CGetFieldInjectablesU3Ec__Iterator48_t3210264959 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetFieldInjectablesU3Ec__Iterator48_System_Collections_Generic_IEnumerableU3CZenject_InjectableInfoU3E_GetEnumerator_m1311321116_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetFieldInjectablesU3Ec__Iterator48_t3210264959 * V_0 = NULL;
	{
		int32_t* L_0 = __this->get_address_of_U24PC_4();
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, L_0, 0, ((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CGetFieldInjectablesU3Ec__Iterator48_t3210264959 * L_2 = (U3CGetFieldInjectablesU3Ec__Iterator48_t3210264959 *)il2cpp_codegen_object_new(U3CGetFieldInjectablesU3Ec__Iterator48_t3210264959_il2cpp_TypeInfo_var);
		U3CGetFieldInjectablesU3Ec__Iterator48__ctor_m2018515068(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		U3CGetFieldInjectablesU3Ec__Iterator48_t3210264959 * L_3 = V_0;
		Type_t * L_4 = __this->get_U3CU24U3Etype_6();
		NullCheck(L_3);
		L_3->set_type_0(L_4);
		U3CGetFieldInjectablesU3Ec__Iterator48_t3210264959 * L_5 = V_0;
		return L_5;
	}
}
// System.Boolean Zenject.TypeAnalyzer/<GetFieldInjectables>c__Iterator48::MoveNext()
extern Il2CppClass* U3CGetFieldInjectablesU3Ec__Iterator48_t3210264959_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t2642952727_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_1_t4037084138_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t2648036230_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeAnalyzer_t2089278869_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CGetFieldInjectablesU3Ec__Iterator48_U3CU3Em__2B8_m2039708125_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3085140765_MethodInfo_var;
extern const MethodInfo* Enumerable_Where_TisFieldInfo_t_m107125898_MethodInfo_var;
extern const uint32_t U3CGetFieldInjectablesU3Ec__Iterator48_MoveNext_m1315356664_MetadataUsageId;
extern "C"  bool U3CGetFieldInjectablesU3Ec__Iterator48_MoveNext_m1315356664 (U3CGetFieldInjectablesU3Ec__Iterator48_t3210264959 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetFieldInjectablesU3Ec__Iterator48_MoveNext_m1315356664_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	Il2CppObject* G_B4_0 = NULL;
	U3CGetFieldInjectablesU3Ec__Iterator48_t3210264959 * G_B4_1 = NULL;
	Il2CppObject* G_B3_0 = NULL;
	U3CGetFieldInjectablesU3Ec__Iterator48_t3210264959 * G_B3_1 = NULL;
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_006c;
		}
	}
	{
		goto IL_00e8;
	}

IL_0023:
	{
		Type_t * L_2 = __this->get_type_0();
		Il2CppObject* L_3 = TypeExtensions_GetAllFields_m1768734574(NULL /*static, unused*/, L_2, ((int32_t)52), /*hidden argument*/NULL);
		Func_2_t2642952727 * L_4 = ((U3CGetFieldInjectablesU3Ec__Iterator48_t3210264959_StaticFields*)U3CGetFieldInjectablesU3Ec__Iterator48_t3210264959_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache7_7();
		G_B3_0 = L_3;
		G_B3_1 = __this;
		if (L_4)
		{
			G_B4_0 = L_3;
			G_B4_1 = __this;
			goto IL_0049;
		}
	}
	{
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)U3CGetFieldInjectablesU3Ec__Iterator48_U3CU3Em__2B8_m2039708125_MethodInfo_var);
		Func_2_t2642952727 * L_6 = (Func_2_t2642952727 *)il2cpp_codegen_object_new(Func_2_t2642952727_il2cpp_TypeInfo_var);
		Func_2__ctor_m3085140765(L_6, NULL, L_5, /*hidden argument*/Func_2__ctor_m3085140765_MethodInfo_var);
		((U3CGetFieldInjectablesU3Ec__Iterator48_t3210264959_StaticFields*)U3CGetFieldInjectablesU3Ec__Iterator48_t3210264959_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache7_7(L_6);
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
	}

IL_0049:
	{
		Func_2_t2642952727 * L_7 = ((U3CGetFieldInjectablesU3Ec__Iterator48_t3210264959_StaticFields*)U3CGetFieldInjectablesU3Ec__Iterator48_t3210264959_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache7_7();
		Il2CppObject* L_8 = Enumerable_Where_TisFieldInfo_t_m107125898(NULL /*static, unused*/, G_B4_0, L_7, /*hidden argument*/Enumerable_Where_TisFieldInfo_t_m107125898_MethodInfo_var);
		NullCheck(G_B4_1);
		G_B4_1->set_U3CfieldInfosU3E__0_1(L_8);
		Il2CppObject* L_9 = __this->get_U3CfieldInfosU3E__0_1();
		NullCheck(L_9);
		Il2CppObject* L_10 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Reflection.FieldInfo>::GetEnumerator() */, IEnumerable_1_t4037084138_il2cpp_TypeInfo_var, L_9);
		__this->set_U3CU24s_270U3E__1_2(L_10);
		V_0 = ((int32_t)-3);
	}

IL_006c:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_11 = V_0;
			if (((int32_t)((int32_t)L_11-(int32_t)1)) == 0)
			{
				goto IL_00b3;
			}
		}

IL_0078:
		{
			goto IL_00b3;
		}

IL_007d:
		{
			Il2CppObject* L_12 = __this->get_U3CU24s_270U3E__1_2();
			NullCheck(L_12);
			FieldInfo_t * L_13 = InterfaceFuncInvoker0< FieldInfo_t * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Reflection.FieldInfo>::get_Current() */, IEnumerator_1_t2648036230_il2cpp_TypeInfo_var, L_12);
			__this->set_U3CfieldInfoU3E__2_3(L_13);
			FieldInfo_t * L_14 = __this->get_U3CfieldInfoU3E__2_3();
			Type_t * L_15 = __this->get_type_0();
			IL2CPP_RUNTIME_CLASS_INIT(TypeAnalyzer_t2089278869_il2cpp_TypeInfo_var);
			InjectableInfo_t1147709774 * L_16 = TypeAnalyzer_CreateForMember_m3555310243(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
			__this->set_U24current_5(L_16);
			__this->set_U24PC_4(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xEA, FINALLY_00c8);
		}

IL_00b3:
		{
			Il2CppObject* L_17 = __this->get_U3CU24s_270U3E__1_2();
			NullCheck(L_17);
			bool L_18 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_17);
			if (L_18)
			{
				goto IL_007d;
			}
		}

IL_00c3:
		{
			IL2CPP_LEAVE(0xE1, FINALLY_00c8);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00c8;
	}

FINALLY_00c8:
	{ // begin finally (depth: 1)
		{
			bool L_19 = V_1;
			if (!L_19)
			{
				goto IL_00cc;
			}
		}

IL_00cb:
		{
			IL2CPP_END_FINALLY(200)
		}

IL_00cc:
		{
			Il2CppObject* L_20 = __this->get_U3CU24s_270U3E__1_2();
			if (L_20)
			{
				goto IL_00d5;
			}
		}

IL_00d4:
		{
			IL2CPP_END_FINALLY(200)
		}

IL_00d5:
		{
			Il2CppObject* L_21 = __this->get_U3CU24s_270U3E__1_2();
			NullCheck(L_21);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_21);
			IL2CPP_END_FINALLY(200)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(200)
	{
		IL2CPP_JUMP_TBL(0xEA, IL_00ea)
		IL2CPP_JUMP_TBL(0xE1, IL_00e1)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00e1:
	{
		__this->set_U24PC_4((-1));
	}

IL_00e8:
	{
		return (bool)0;
	}

IL_00ea:
	{
		return (bool)1;
	}
	// Dead block : IL_00ec: ldloc.2
}
// System.Void Zenject.TypeAnalyzer/<GetFieldInjectables>c__Iterator48::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetFieldInjectablesU3Ec__Iterator48_Dispose_m2661012921_MetadataUsageId;
extern "C"  void U3CGetFieldInjectablesU3Ec__Iterator48_Dispose_m2661012921 (U3CGetFieldInjectablesU3Ec__Iterator48_t3210264959 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetFieldInjectablesU3Ec__Iterator48_Dispose_m2661012921_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_003b;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_003b;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3B, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = __this->get_U3CU24s_270U3E__1_2();
			if (L_2)
			{
				goto IL_002f;
			}
		}

IL_002e:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_002f:
		{
			Il2CppObject* L_3 = __this->get_U3CU24s_270U3E__1_2();
			NullCheck(L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_3);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_003b:
	{
		return;
	}
}
// System.Void Zenject.TypeAnalyzer/<GetFieldInjectables>c__Iterator48::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetFieldInjectablesU3Ec__Iterator48_Reset_m3959915305_MetadataUsageId;
extern "C"  void U3CGetFieldInjectablesU3Ec__Iterator48_Reset_m3959915305 (U3CGetFieldInjectablesU3Ec__Iterator48_t3210264959 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetFieldInjectablesU3Ec__Iterator48_Reset_m3959915305_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean Zenject.TypeAnalyzer/<GetFieldInjectables>c__Iterator48::<>m__2B8(System.Reflection.FieldInfo)
extern const Il2CppType* InjectAttribute_t2791321440_0_0_0_var;
extern const Il2CppType* InjectOptionalAttribute_t899944672_0_0_0_var;
extern Il2CppClass* TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetFieldInjectablesU3Ec__Iterator48_U3CU3Em__2B8_m2039708125_MetadataUsageId;
extern "C"  bool U3CGetFieldInjectablesU3Ec__Iterator48_U3CU3Em__2B8_m2039708125 (Il2CppObject * __this /* static, unused */, FieldInfo_t * ___x0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetFieldInjectablesU3Ec__Iterator48_U3CU3Em__2B8_m2039708125_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FieldInfo_t * L_0 = ___x0;
		TypeU5BU5D_t3431720054* L_1 = ((TypeU5BU5D_t3431720054*)SZArrayNew(TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var, (uint32_t)2));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(InjectAttribute_t2791321440_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_2);
		TypeU5BU5D_t3431720054* L_3 = L_1;
		Type_t * L_4 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(InjectOptionalAttribute_t899944672_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 1);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_4);
		bool L_5 = TypeExtensions_HasAttribute_m3951583835(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void Zenject.TypeAnalyzer/<GetPostInjectMethods>c__AnonStorey182::.ctor()
extern "C"  void U3CGetPostInjectMethodsU3Ec__AnonStorey182__ctor_m1647722528 (U3CGetPostInjectMethodsU3Ec__AnonStorey182_t1421092187 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 Zenject.TypeAnalyzer/<GetPostInjectMethods>c__AnonStorey182::<>m__2B2(System.Reflection.MethodInfo)
extern "C"  int32_t U3CGetPostInjectMethodsU3Ec__AnonStorey182_U3CU3Em__2B2_m3523916186 (U3CGetPostInjectMethodsU3Ec__AnonStorey182_t1421092187 * __this, MethodInfo_t * ___x0, const MethodInfo* method)
{
	{
		List_1_t3576188904 * L_0 = __this->get_heirarchyList_0();
		MethodInfo_t * L_1 = ___x0;
		NullCheck(L_1);
		Type_t * L_2 = VirtFuncInvoker0< Type_t * >::Invoke(7 /* System.Type System.Reflection.MemberInfo::get_DeclaringType() */, L_1);
		NullCheck(L_0);
		int32_t L_3 = VirtFuncInvoker1< int32_t, Type_t * >::Invoke(28 /* System.Int32 System.Collections.Generic.List`1<System.Type>::IndexOf(!0) */, L_0, L_2);
		return L_3;
	}
}
// Zenject.InjectableInfo Zenject.TypeAnalyzer/<GetPostInjectMethods>c__AnonStorey182::<>m__2B3(System.Reflection.ParameterInfo)
extern Il2CppClass* TypeAnalyzer_t2089278869_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetPostInjectMethodsU3Ec__AnonStorey182_U3CU3Em__2B3_m461157480_MetadataUsageId;
extern "C"  InjectableInfo_t1147709774 * U3CGetPostInjectMethodsU3Ec__AnonStorey182_U3CU3Em__2B3_m461157480 (U3CGetPostInjectMethodsU3Ec__AnonStorey182_t1421092187 * __this, ParameterInfo_t2610273829 * ___paramInfo0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetPostInjectMethodsU3Ec__AnonStorey182_U3CU3Em__2B3_m461157480_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = __this->get_type_1();
		ParameterInfo_t2610273829 * L_1 = ___paramInfo0;
		IL2CPP_RUNTIME_CLASS_INIT(TypeAnalyzer_t2089278869_il2cpp_TypeInfo_var);
		InjectableInfo_t1147709774 * L_2 = TypeAnalyzer_CreateInjectableInfoForParam_m3638764108(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void Zenject.TypeAnalyzer/<GetPropertyInjectables>c__Iterator47::.ctor()
extern "C"  void U3CGetPropertyInjectablesU3Ec__Iterator47__ctor_m1864367420 (U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// Zenject.InjectableInfo Zenject.TypeAnalyzer/<GetPropertyInjectables>c__Iterator47::System.Collections.Generic.IEnumerator<Zenject.InjectableInfo>.get_Current()
extern "C"  InjectableInfo_t1147709774 * U3CGetPropertyInjectablesU3Ec__Iterator47_System_Collections_Generic_IEnumeratorU3CZenject_InjectableInfoU3E_get_Current_m1881937623 (U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871 * __this, const MethodInfo* method)
{
	{
		InjectableInfo_t1147709774 * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Object Zenject.TypeAnalyzer/<GetPropertyInjectables>c__Iterator47::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetPropertyInjectablesU3Ec__Iterator47_System_Collections_IEnumerator_get_Current_m526218612 (U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871 * __this, const MethodInfo* method)
{
	{
		InjectableInfo_t1147709774 * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Collections.IEnumerator Zenject.TypeAnalyzer/<GetPropertyInjectables>c__Iterator47::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CGetPropertyInjectablesU3Ec__Iterator47_System_Collections_IEnumerable_GetEnumerator_m567270965 (U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = U3CGetPropertyInjectablesU3Ec__Iterator47_System_Collections_Generic_IEnumerableU3CZenject_InjectableInfoU3E_GetEnumerator_m4015091492(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<Zenject.InjectableInfo> Zenject.TypeAnalyzer/<GetPropertyInjectables>c__Iterator47::System.Collections.Generic.IEnumerable<Zenject.InjectableInfo>.GetEnumerator()
extern Il2CppClass* U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetPropertyInjectablesU3Ec__Iterator47_System_Collections_Generic_IEnumerableU3CZenject_InjectableInfoU3E_GetEnumerator_m4015091492_MetadataUsageId;
extern "C"  Il2CppObject* U3CGetPropertyInjectablesU3Ec__Iterator47_System_Collections_Generic_IEnumerableU3CZenject_InjectableInfoU3E_GetEnumerator_m4015091492 (U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetPropertyInjectablesU3Ec__Iterator47_System_Collections_Generic_IEnumerableU3CZenject_InjectableInfoU3E_GetEnumerator_m4015091492_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871 * V_0 = NULL;
	{
		int32_t* L_0 = __this->get_address_of_U24PC_4();
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, L_0, 0, ((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871 * L_2 = (U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871 *)il2cpp_codegen_object_new(U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871_il2cpp_TypeInfo_var);
		U3CGetPropertyInjectablesU3Ec__Iterator47__ctor_m1864367420(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871 * L_3 = V_0;
		Type_t * L_4 = __this->get_U3CU24U3Etype_6();
		NullCheck(L_3);
		L_3->set_type_0(L_4);
		U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871 * L_5 = V_0;
		return L_5;
	}
}
// System.Boolean Zenject.TypeAnalyzer/<GetPropertyInjectables>c__Iterator47::MoveNext()
extern Il2CppClass* U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t2846737840_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_1_t67735429_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t2973654817_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeAnalyzer_t2089278869_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CGetPropertyInjectablesU3Ec__Iterator47_U3CU3Em__2B7_m2670351761_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3613743678_MethodInfo_var;
extern const MethodInfo* Enumerable_Where_TisPropertyInfo_t_m1174545549_MethodInfo_var;
extern const uint32_t U3CGetPropertyInjectablesU3Ec__Iterator47_MoveNext_m1591760416_MetadataUsageId;
extern "C"  bool U3CGetPropertyInjectablesU3Ec__Iterator47_MoveNext_m1591760416 (U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetPropertyInjectablesU3Ec__Iterator47_MoveNext_m1591760416_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	Il2CppObject* G_B4_0 = NULL;
	U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871 * G_B4_1 = NULL;
	Il2CppObject* G_B3_0 = NULL;
	U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871 * G_B3_1 = NULL;
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_006c;
		}
	}
	{
		goto IL_00e8;
	}

IL_0023:
	{
		Type_t * L_2 = __this->get_type_0();
		Il2CppObject* L_3 = TypeExtensions_GetAllProperties_m3364896073(NULL /*static, unused*/, L_2, ((int32_t)52), /*hidden argument*/NULL);
		Func_2_t2846737840 * L_4 = ((U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871_StaticFields*)U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache7_7();
		G_B3_0 = L_3;
		G_B3_1 = __this;
		if (L_4)
		{
			G_B4_0 = L_3;
			G_B4_1 = __this;
			goto IL_0049;
		}
	}
	{
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)U3CGetPropertyInjectablesU3Ec__Iterator47_U3CU3Em__2B7_m2670351761_MethodInfo_var);
		Func_2_t2846737840 * L_6 = (Func_2_t2846737840 *)il2cpp_codegen_object_new(Func_2_t2846737840_il2cpp_TypeInfo_var);
		Func_2__ctor_m3613743678(L_6, NULL, L_5, /*hidden argument*/Func_2__ctor_m3613743678_MethodInfo_var);
		((U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871_StaticFields*)U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache7_7(L_6);
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
	}

IL_0049:
	{
		Func_2_t2846737840 * L_7 = ((U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871_StaticFields*)U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache7_7();
		Il2CppObject* L_8 = Enumerable_Where_TisPropertyInfo_t_m1174545549(NULL /*static, unused*/, G_B4_0, L_7, /*hidden argument*/Enumerable_Where_TisPropertyInfo_t_m1174545549_MethodInfo_var);
		NullCheck(G_B4_1);
		G_B4_1->set_U3CpropInfosU3E__0_1(L_8);
		Il2CppObject* L_9 = __this->get_U3CpropInfosU3E__0_1();
		NullCheck(L_9);
		Il2CppObject* L_10 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Reflection.PropertyInfo>::GetEnumerator() */, IEnumerable_1_t67735429_il2cpp_TypeInfo_var, L_9);
		__this->set_U3CU24s_269U3E__1_2(L_10);
		V_0 = ((int32_t)-3);
	}

IL_006c:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_11 = V_0;
			if (((int32_t)((int32_t)L_11-(int32_t)1)) == 0)
			{
				goto IL_00b3;
			}
		}

IL_0078:
		{
			goto IL_00b3;
		}

IL_007d:
		{
			Il2CppObject* L_12 = __this->get_U3CU24s_269U3E__1_2();
			NullCheck(L_12);
			PropertyInfo_t * L_13 = InterfaceFuncInvoker0< PropertyInfo_t * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Reflection.PropertyInfo>::get_Current() */, IEnumerator_1_t2973654817_il2cpp_TypeInfo_var, L_12);
			__this->set_U3CpropInfoU3E__2_3(L_13);
			PropertyInfo_t * L_14 = __this->get_U3CpropInfoU3E__2_3();
			Type_t * L_15 = __this->get_type_0();
			IL2CPP_RUNTIME_CLASS_INIT(TypeAnalyzer_t2089278869_il2cpp_TypeInfo_var);
			InjectableInfo_t1147709774 * L_16 = TypeAnalyzer_CreateForMember_m3555310243(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
			__this->set_U24current_5(L_16);
			__this->set_U24PC_4(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xEA, FINALLY_00c8);
		}

IL_00b3:
		{
			Il2CppObject* L_17 = __this->get_U3CU24s_269U3E__1_2();
			NullCheck(L_17);
			bool L_18 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_17);
			if (L_18)
			{
				goto IL_007d;
			}
		}

IL_00c3:
		{
			IL2CPP_LEAVE(0xE1, FINALLY_00c8);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00c8;
	}

FINALLY_00c8:
	{ // begin finally (depth: 1)
		{
			bool L_19 = V_1;
			if (!L_19)
			{
				goto IL_00cc;
			}
		}

IL_00cb:
		{
			IL2CPP_END_FINALLY(200)
		}

IL_00cc:
		{
			Il2CppObject* L_20 = __this->get_U3CU24s_269U3E__1_2();
			if (L_20)
			{
				goto IL_00d5;
			}
		}

IL_00d4:
		{
			IL2CPP_END_FINALLY(200)
		}

IL_00d5:
		{
			Il2CppObject* L_21 = __this->get_U3CU24s_269U3E__1_2();
			NullCheck(L_21);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_21);
			IL2CPP_END_FINALLY(200)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(200)
	{
		IL2CPP_JUMP_TBL(0xEA, IL_00ea)
		IL2CPP_JUMP_TBL(0xE1, IL_00e1)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00e1:
	{
		__this->set_U24PC_4((-1));
	}

IL_00e8:
	{
		return (bool)0;
	}

IL_00ea:
	{
		return (bool)1;
	}
	// Dead block : IL_00ec: ldloc.2
}
// System.Void Zenject.TypeAnalyzer/<GetPropertyInjectables>c__Iterator47::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetPropertyInjectablesU3Ec__Iterator47_Dispose_m554011257_MetadataUsageId;
extern "C"  void U3CGetPropertyInjectablesU3Ec__Iterator47_Dispose_m554011257 (U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetPropertyInjectablesU3Ec__Iterator47_Dispose_m554011257_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_003b;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_003b;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3B, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = __this->get_U3CU24s_269U3E__1_2();
			if (L_2)
			{
				goto IL_002f;
			}
		}

IL_002e:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_002f:
		{
			Il2CppObject* L_3 = __this->get_U3CU24s_269U3E__1_2();
			NullCheck(L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_3);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_003b:
	{
		return;
	}
}
// System.Void Zenject.TypeAnalyzer/<GetPropertyInjectables>c__Iterator47::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetPropertyInjectablesU3Ec__Iterator47_Reset_m3805767657_MetadataUsageId;
extern "C"  void U3CGetPropertyInjectablesU3Ec__Iterator47_Reset_m3805767657 (U3CGetPropertyInjectablesU3Ec__Iterator47_t473481871 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetPropertyInjectablesU3Ec__Iterator47_Reset_m3805767657_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean Zenject.TypeAnalyzer/<GetPropertyInjectables>c__Iterator47::<>m__2B7(System.Reflection.PropertyInfo)
extern const Il2CppType* InjectAttribute_t2791321440_0_0_0_var;
extern const Il2CppType* InjectOptionalAttribute_t899944672_0_0_0_var;
extern Il2CppClass* TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetPropertyInjectablesU3Ec__Iterator47_U3CU3Em__2B7_m2670351761_MetadataUsageId;
extern "C"  bool U3CGetPropertyInjectablesU3Ec__Iterator47_U3CU3Em__2B7_m2670351761 (Il2CppObject * __this /* static, unused */, PropertyInfo_t * ___x0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetPropertyInjectablesU3Ec__Iterator47_U3CU3Em__2B7_m2670351761_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PropertyInfo_t * L_0 = ___x0;
		TypeU5BU5D_t3431720054* L_1 = ((TypeU5BU5D_t3431720054*)SZArrayNew(TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var, (uint32_t)2));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(InjectAttribute_t2791321440_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_2);
		TypeU5BU5D_t3431720054* L_3 = L_1;
		Type_t * L_4 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(InjectOptionalAttribute_t899944672_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 1);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_4);
		bool L_5 = TypeExtensions_HasAttribute_m3951583835(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void Zenject.TypeValuePair::.ctor(System.Type,System.Object)
extern "C"  void TypeValuePair__ctor_m1625881838 (TypeValuePair_t620932390 * __this, Type_t * ___type0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		Type_t * L_0 = ___type0;
		__this->set_Type_0(L_0);
		Il2CppObject * L_1 = ___value1;
		__this->set_Value_1(L_1);
		return;
	}
}
// System.Void Zenject.UnityDependencyRoot::.ctor()
extern "C"  void UnityDependencyRoot__ctor_m1275946440 (UnityDependencyRoot_t1917880567 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m350695606(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Zenject.UnityDependencyRoot::Initialize()
extern "C"  void UnityDependencyRoot_Initialize_m390024652 (UnityDependencyRoot_t1917880567 * __this, const MethodInfo* method)
{
	{
		InitializableManager_t1859035635 * L_0 = __this->get__initializableManager_3();
		NullCheck(L_0);
		InitializableManager_Initialize_m3971178806(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Zenject.UnityDependencyRoot::OnApplicationQuit()
extern "C"  void UnityDependencyRoot_OnApplicationQuit_m2508416838 (UnityDependencyRoot_t1917880567 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get__disposed_5();
		Assert_That_m1320742921(NULL /*static, unused*/, (bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		__this->set__disposed_5((bool)1);
		DisposableManager_t2094948738 * L_1 = __this->get__disposablesManager_4();
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(4 /* System.Void Zenject.DisposableManager::Dispose() */, L_1);
		return;
	}
}
// System.Void Zenject.UnityDependencyRoot::OnDestroy()
extern "C"  void UnityDependencyRoot_OnDestroy_m2812745409 (UnityDependencyRoot_t1917880567 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get__disposed_5();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		DisposableManager_t2094948738 * L_1 = __this->get__disposablesManager_4();
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(4 /* System.Void Zenject.DisposableManager::Dispose() */, L_1);
	}

IL_0016:
	{
		return;
	}
}
// System.Void Zenject.UnityDependencyRoot::Update()
extern "C"  void UnityDependencyRoot_Update_m2626496069 (UnityDependencyRoot_t1917880567 * __this, const MethodInfo* method)
{
	{
		TickableManager_t278367883 * L_0 = __this->get__tickableManager_2();
		NullCheck(L_0);
		TickableManager_Update_m259086169(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Zenject.UnityDependencyRoot::FixedUpdate()
extern "C"  void UnityDependencyRoot_FixedUpdate_m1301663043 (UnityDependencyRoot_t1917880567 * __this, const MethodInfo* method)
{
	{
		TickableManager_t278367883 * L_0 = __this->get__tickableManager_2();
		NullCheck(L_0);
		TickableManager_FixedUpdate_m3368626351(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Zenject.UnityDependencyRoot::LateUpdate()
extern "C"  void UnityDependencyRoot_LateUpdate_m1537753483 (UnityDependencyRoot_t1917880567 * __this, const MethodInfo* method)
{
	{
		TickableManager_t278367883 * L_0 = __this->get__tickableManager_2();
		NullCheck(L_0);
		TickableManager_LateUpdate_m1881524383(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Zenject.UnityEventManager::.ctor()
extern Il2CppClass* UnityEventManager_t2255518493_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t359458046_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t2995867492_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t3075561458_il2cpp_TypeInfo_var;
extern const MethodInfo* UnityEventManager_U3CApplicationGainedFocusU3Em__2F1_m2575721386_MethodInfo_var;
extern const MethodInfo* UnityEventManager_U3CApplicationLostFocusU3Em__2F2_m1691150577_MethodInfo_var;
extern const MethodInfo* UnityEventManager_U3CApplicationFocusChangedU3Em__2F3_m3170362807_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m2036539306_MethodInfo_var;
extern const MethodInfo* UnityEventManager_U3CApplicationQuitU3Em__2F4_m2326336718_MethodInfo_var;
extern const MethodInfo* UnityEventManager_U3CChangingScenesU3Em__2F5_m2194074212_MethodInfo_var;
extern const MethodInfo* UnityEventManager_U3CDrawGizmosU3Em__2F6_m2238731356_MethodInfo_var;
extern const MethodInfo* UnityEventManager_U3CMouseButtonDownU3Em__2F7_m2930362984_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m1091880516_MethodInfo_var;
extern const MethodInfo* UnityEventManager_U3CMouseButtonUpU3Em__2F8_m4087750992_MethodInfo_var;
extern const MethodInfo* UnityEventManager_U3CLeftMouseButtonDownU3Em__2F9_m148156608_MethodInfo_var;
extern const MethodInfo* UnityEventManager_U3CLeftMouseButtonUpU3Em__2FA_m2430237615_MethodInfo_var;
extern const MethodInfo* UnityEventManager_U3CMiddleMouseButtonDownU3Em__2FB_m2259636055_MethodInfo_var;
extern const MethodInfo* UnityEventManager_U3CMiddleMouseButtonUpU3Em__2FC_m3844725631_MethodInfo_var;
extern const MethodInfo* UnityEventManager_U3CRightMouseButtonDownU3Em__2FD_m1610318218_MethodInfo_var;
extern const MethodInfo* UnityEventManager_U3CRightMouseButtonUpU3Em__2FE_m1546847730_MethodInfo_var;
extern const MethodInfo* UnityEventManager_U3CMouseWheelMovedU3Em__2FF_m1664420075_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m1139518760_MethodInfo_var;
extern const MethodInfo* UnityEventManager_U3CMouseMoveU3Em__300_m3730852074_MethodInfo_var;
extern const MethodInfo* UnityEventManager_U3CScreenSizeChangedU3Em__301_m2782129018_MethodInfo_var;
extern const MethodInfo* UnityEventManager_U3CStartedU3Em__302_m1311423777_MethodInfo_var;
extern const uint32_t UnityEventManager__ctor_m1400173602_MetadataUsageId;
extern "C"  void UnityEventManager__ctor_m1400173602 (UnityEventManager_t2255518493 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEventManager__ctor_m1400173602_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UnityEventManager_t2255518493 * G_B2_0 = NULL;
	UnityEventManager_t2255518493 * G_B1_0 = NULL;
	UnityEventManager_t2255518493 * G_B4_0 = NULL;
	UnityEventManager_t2255518493 * G_B3_0 = NULL;
	UnityEventManager_t2255518493 * G_B6_0 = NULL;
	UnityEventManager_t2255518493 * G_B5_0 = NULL;
	UnityEventManager_t2255518493 * G_B8_0 = NULL;
	UnityEventManager_t2255518493 * G_B7_0 = NULL;
	UnityEventManager_t2255518493 * G_B10_0 = NULL;
	UnityEventManager_t2255518493 * G_B9_0 = NULL;
	UnityEventManager_t2255518493 * G_B12_0 = NULL;
	UnityEventManager_t2255518493 * G_B11_0 = NULL;
	UnityEventManager_t2255518493 * G_B14_0 = NULL;
	UnityEventManager_t2255518493 * G_B13_0 = NULL;
	UnityEventManager_t2255518493 * G_B16_0 = NULL;
	UnityEventManager_t2255518493 * G_B15_0 = NULL;
	UnityEventManager_t2255518493 * G_B18_0 = NULL;
	UnityEventManager_t2255518493 * G_B17_0 = NULL;
	UnityEventManager_t2255518493 * G_B20_0 = NULL;
	UnityEventManager_t2255518493 * G_B19_0 = NULL;
	UnityEventManager_t2255518493 * G_B22_0 = NULL;
	UnityEventManager_t2255518493 * G_B21_0 = NULL;
	UnityEventManager_t2255518493 * G_B24_0 = NULL;
	UnityEventManager_t2255518493 * G_B23_0 = NULL;
	UnityEventManager_t2255518493 * G_B26_0 = NULL;
	UnityEventManager_t2255518493 * G_B25_0 = NULL;
	UnityEventManager_t2255518493 * G_B28_0 = NULL;
	UnityEventManager_t2255518493 * G_B27_0 = NULL;
	UnityEventManager_t2255518493 * G_B30_0 = NULL;
	UnityEventManager_t2255518493 * G_B29_0 = NULL;
	UnityEventManager_t2255518493 * G_B32_0 = NULL;
	UnityEventManager_t2255518493 * G_B31_0 = NULL;
	UnityEventManager_t2255518493 * G_B34_0 = NULL;
	UnityEventManager_t2255518493 * G_B33_0 = NULL;
	UnityEventManager_t2255518493 * G_B36_0 = NULL;
	UnityEventManager_t2255518493 * G_B35_0 = NULL;
	{
		Action_t437523947 * L_0 = ((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache16_24();
		G_B1_0 = __this;
		if (L_0)
		{
			G_B2_0 = __this;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)UnityEventManager_U3CApplicationGainedFocusU3Em__2F1_m2575721386_MethodInfo_var);
		Action_t437523947 * L_2 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_2, NULL, L_1, /*hidden argument*/NULL);
		((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache16_24(L_2);
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		Action_t437523947 * L_3 = ((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache16_24();
		NullCheck(G_B2_0);
		G_B2_0->set_ApplicationGainedFocus_5(L_3);
		Action_t437523947 * L_4 = ((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache17_25();
		G_B3_0 = __this;
		if (L_4)
		{
			G_B4_0 = __this;
			goto IL_003c;
		}
	}
	{
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)UnityEventManager_U3CApplicationLostFocusU3Em__2F2_m1691150577_MethodInfo_var);
		Action_t437523947 * L_6 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_6, NULL, L_5, /*hidden argument*/NULL);
		((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache17_25(L_6);
		G_B4_0 = G_B3_0;
	}

IL_003c:
	{
		Action_t437523947 * L_7 = ((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache17_25();
		NullCheck(G_B4_0);
		G_B4_0->set_ApplicationLostFocus_6(L_7);
		Action_1_t359458046 * L_8 = ((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache18_26();
		G_B5_0 = __this;
		if (L_8)
		{
			G_B6_0 = __this;
			goto IL_005f;
		}
	}
	{
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)(void*)UnityEventManager_U3CApplicationFocusChangedU3Em__2F3_m3170362807_MethodInfo_var);
		Action_1_t359458046 * L_10 = (Action_1_t359458046 *)il2cpp_codegen_object_new(Action_1_t359458046_il2cpp_TypeInfo_var);
		Action_1__ctor_m2036539306(L_10, NULL, L_9, /*hidden argument*/Action_1__ctor_m2036539306_MethodInfo_var);
		((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache18_26(L_10);
		G_B6_0 = G_B5_0;
	}

IL_005f:
	{
		Action_1_t359458046 * L_11 = ((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache18_26();
		NullCheck(G_B6_0);
		G_B6_0->set_ApplicationFocusChanged_7(L_11);
		Action_t437523947 * L_12 = ((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache19_27();
		G_B7_0 = __this;
		if (L_12)
		{
			G_B8_0 = __this;
			goto IL_0082;
		}
	}
	{
		IntPtr_t L_13;
		L_13.set_m_value_0((void*)(void*)UnityEventManager_U3CApplicationQuitU3Em__2F4_m2326336718_MethodInfo_var);
		Action_t437523947 * L_14 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_14, NULL, L_13, /*hidden argument*/NULL);
		((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache19_27(L_14);
		G_B8_0 = G_B7_0;
	}

IL_0082:
	{
		Action_t437523947 * L_15 = ((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache19_27();
		NullCheck(G_B8_0);
		G_B8_0->set_ApplicationQuit_8(L_15);
		Action_t437523947 * L_16 = ((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1A_28();
		G_B9_0 = __this;
		if (L_16)
		{
			G_B10_0 = __this;
			goto IL_00a5;
		}
	}
	{
		IntPtr_t L_17;
		L_17.set_m_value_0((void*)(void*)UnityEventManager_U3CChangingScenesU3Em__2F5_m2194074212_MethodInfo_var);
		Action_t437523947 * L_18 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_18, NULL, L_17, /*hidden argument*/NULL);
		((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache1A_28(L_18);
		G_B10_0 = G_B9_0;
	}

IL_00a5:
	{
		Action_t437523947 * L_19 = ((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1A_28();
		NullCheck(G_B10_0);
		G_B10_0->set_ChangingScenes_9(L_19);
		Action_t437523947 * L_20 = ((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1B_29();
		G_B11_0 = __this;
		if (L_20)
		{
			G_B12_0 = __this;
			goto IL_00c8;
		}
	}
	{
		IntPtr_t L_21;
		L_21.set_m_value_0((void*)(void*)UnityEventManager_U3CDrawGizmosU3Em__2F6_m2238731356_MethodInfo_var);
		Action_t437523947 * L_22 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_22, NULL, L_21, /*hidden argument*/NULL);
		((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache1B_29(L_22);
		G_B12_0 = G_B11_0;
	}

IL_00c8:
	{
		Action_t437523947 * L_23 = ((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1B_29();
		NullCheck(G_B12_0);
		G_B12_0->set_DrawGizmos_10(L_23);
		Action_1_t2995867492 * L_24 = ((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1C_30();
		G_B13_0 = __this;
		if (L_24)
		{
			G_B14_0 = __this;
			goto IL_00eb;
		}
	}
	{
		IntPtr_t L_25;
		L_25.set_m_value_0((void*)(void*)UnityEventManager_U3CMouseButtonDownU3Em__2F7_m2930362984_MethodInfo_var);
		Action_1_t2995867492 * L_26 = (Action_1_t2995867492 *)il2cpp_codegen_object_new(Action_1_t2995867492_il2cpp_TypeInfo_var);
		Action_1__ctor_m1091880516(L_26, NULL, L_25, /*hidden argument*/Action_1__ctor_m1091880516_MethodInfo_var);
		((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache1C_30(L_26);
		G_B14_0 = G_B13_0;
	}

IL_00eb:
	{
		Action_1_t2995867492 * L_27 = ((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1C_30();
		NullCheck(G_B14_0);
		G_B14_0->set_MouseButtonDown_11(L_27);
		Action_1_t2995867492 * L_28 = ((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1D_31();
		G_B15_0 = __this;
		if (L_28)
		{
			G_B16_0 = __this;
			goto IL_010e;
		}
	}
	{
		IntPtr_t L_29;
		L_29.set_m_value_0((void*)(void*)UnityEventManager_U3CMouseButtonUpU3Em__2F8_m4087750992_MethodInfo_var);
		Action_1_t2995867492 * L_30 = (Action_1_t2995867492 *)il2cpp_codegen_object_new(Action_1_t2995867492_il2cpp_TypeInfo_var);
		Action_1__ctor_m1091880516(L_30, NULL, L_29, /*hidden argument*/Action_1__ctor_m1091880516_MethodInfo_var);
		((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache1D_31(L_30);
		G_B16_0 = G_B15_0;
	}

IL_010e:
	{
		Action_1_t2995867492 * L_31 = ((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1D_31();
		NullCheck(G_B16_0);
		G_B16_0->set_MouseButtonUp_12(L_31);
		Action_t437523947 * L_32 = ((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1E_32();
		G_B17_0 = __this;
		if (L_32)
		{
			G_B18_0 = __this;
			goto IL_0131;
		}
	}
	{
		IntPtr_t L_33;
		L_33.set_m_value_0((void*)(void*)UnityEventManager_U3CLeftMouseButtonDownU3Em__2F9_m148156608_MethodInfo_var);
		Action_t437523947 * L_34 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_34, NULL, L_33, /*hidden argument*/NULL);
		((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache1E_32(L_34);
		G_B18_0 = G_B17_0;
	}

IL_0131:
	{
		Action_t437523947 * L_35 = ((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1E_32();
		NullCheck(G_B18_0);
		G_B18_0->set_LeftMouseButtonDown_13(L_35);
		Action_t437523947 * L_36 = ((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1F_33();
		G_B19_0 = __this;
		if (L_36)
		{
			G_B20_0 = __this;
			goto IL_0154;
		}
	}
	{
		IntPtr_t L_37;
		L_37.set_m_value_0((void*)(void*)UnityEventManager_U3CLeftMouseButtonUpU3Em__2FA_m2430237615_MethodInfo_var);
		Action_t437523947 * L_38 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_38, NULL, L_37, /*hidden argument*/NULL);
		((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache1F_33(L_38);
		G_B20_0 = G_B19_0;
	}

IL_0154:
	{
		Action_t437523947 * L_39 = ((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1F_33();
		NullCheck(G_B20_0);
		G_B20_0->set_LeftMouseButtonUp_14(L_39);
		Action_t437523947 * L_40 = ((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache20_34();
		G_B21_0 = __this;
		if (L_40)
		{
			G_B22_0 = __this;
			goto IL_0177;
		}
	}
	{
		IntPtr_t L_41;
		L_41.set_m_value_0((void*)(void*)UnityEventManager_U3CMiddleMouseButtonDownU3Em__2FB_m2259636055_MethodInfo_var);
		Action_t437523947 * L_42 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_42, NULL, L_41, /*hidden argument*/NULL);
		((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache20_34(L_42);
		G_B22_0 = G_B21_0;
	}

IL_0177:
	{
		Action_t437523947 * L_43 = ((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache20_34();
		NullCheck(G_B22_0);
		G_B22_0->set_MiddleMouseButtonDown_15(L_43);
		Action_t437523947 * L_44 = ((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache21_35();
		G_B23_0 = __this;
		if (L_44)
		{
			G_B24_0 = __this;
			goto IL_019a;
		}
	}
	{
		IntPtr_t L_45;
		L_45.set_m_value_0((void*)(void*)UnityEventManager_U3CMiddleMouseButtonUpU3Em__2FC_m3844725631_MethodInfo_var);
		Action_t437523947 * L_46 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_46, NULL, L_45, /*hidden argument*/NULL);
		((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache21_35(L_46);
		G_B24_0 = G_B23_0;
	}

IL_019a:
	{
		Action_t437523947 * L_47 = ((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache21_35();
		NullCheck(G_B24_0);
		G_B24_0->set_MiddleMouseButtonUp_16(L_47);
		Action_t437523947 * L_48 = ((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache22_36();
		G_B25_0 = __this;
		if (L_48)
		{
			G_B26_0 = __this;
			goto IL_01bd;
		}
	}
	{
		IntPtr_t L_49;
		L_49.set_m_value_0((void*)(void*)UnityEventManager_U3CRightMouseButtonDownU3Em__2FD_m1610318218_MethodInfo_var);
		Action_t437523947 * L_50 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_50, NULL, L_49, /*hidden argument*/NULL);
		((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache22_36(L_50);
		G_B26_0 = G_B25_0;
	}

IL_01bd:
	{
		Action_t437523947 * L_51 = ((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache22_36();
		NullCheck(G_B26_0);
		G_B26_0->set_RightMouseButtonDown_17(L_51);
		Action_t437523947 * L_52 = ((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache23_37();
		G_B27_0 = __this;
		if (L_52)
		{
			G_B28_0 = __this;
			goto IL_01e0;
		}
	}
	{
		IntPtr_t L_53;
		L_53.set_m_value_0((void*)(void*)UnityEventManager_U3CRightMouseButtonUpU3Em__2FE_m1546847730_MethodInfo_var);
		Action_t437523947 * L_54 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_54, NULL, L_53, /*hidden argument*/NULL);
		((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache23_37(L_54);
		G_B28_0 = G_B27_0;
	}

IL_01e0:
	{
		Action_t437523947 * L_55 = ((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache23_37();
		NullCheck(G_B28_0);
		G_B28_0->set_RightMouseButtonUp_18(L_55);
		Action_1_t3075561458 * L_56 = ((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache24_38();
		G_B29_0 = __this;
		if (L_56)
		{
			G_B30_0 = __this;
			goto IL_0203;
		}
	}
	{
		IntPtr_t L_57;
		L_57.set_m_value_0((void*)(void*)UnityEventManager_U3CMouseWheelMovedU3Em__2FF_m1664420075_MethodInfo_var);
		Action_1_t3075561458 * L_58 = (Action_1_t3075561458 *)il2cpp_codegen_object_new(Action_1_t3075561458_il2cpp_TypeInfo_var);
		Action_1__ctor_m1139518760(L_58, NULL, L_57, /*hidden argument*/Action_1__ctor_m1139518760_MethodInfo_var);
		((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache24_38(L_58);
		G_B30_0 = G_B29_0;
	}

IL_0203:
	{
		Action_1_t3075561458 * L_59 = ((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache24_38();
		NullCheck(G_B30_0);
		G_B30_0->set_MouseWheelMoved_19(L_59);
		Action_t437523947 * L_60 = ((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache25_39();
		G_B31_0 = __this;
		if (L_60)
		{
			G_B32_0 = __this;
			goto IL_0226;
		}
	}
	{
		IntPtr_t L_61;
		L_61.set_m_value_0((void*)(void*)UnityEventManager_U3CMouseMoveU3Em__300_m3730852074_MethodInfo_var);
		Action_t437523947 * L_62 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_62, NULL, L_61, /*hidden argument*/NULL);
		((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache25_39(L_62);
		G_B32_0 = G_B31_0;
	}

IL_0226:
	{
		Action_t437523947 * L_63 = ((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache25_39();
		NullCheck(G_B32_0);
		G_B32_0->set_MouseMove_20(L_63);
		Action_t437523947 * L_64 = ((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache26_40();
		G_B33_0 = __this;
		if (L_64)
		{
			G_B34_0 = __this;
			goto IL_0249;
		}
	}
	{
		IntPtr_t L_65;
		L_65.set_m_value_0((void*)(void*)UnityEventManager_U3CScreenSizeChangedU3Em__301_m2782129018_MethodInfo_var);
		Action_t437523947 * L_66 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_66, NULL, L_65, /*hidden argument*/NULL);
		((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache26_40(L_66);
		G_B34_0 = G_B33_0;
	}

IL_0249:
	{
		Action_t437523947 * L_67 = ((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache26_40();
		NullCheck(G_B34_0);
		G_B34_0->set_ScreenSizeChanged_21(L_67);
		Action_t437523947 * L_68 = ((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache27_41();
		G_B35_0 = __this;
		if (L_68)
		{
			G_B36_0 = __this;
			goto IL_026c;
		}
	}
	{
		IntPtr_t L_69;
		L_69.set_m_value_0((void*)(void*)UnityEventManager_U3CStartedU3Em__302_m1311423777_MethodInfo_var);
		Action_t437523947 * L_70 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_70, NULL, L_69, /*hidden argument*/NULL);
		((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache27_41(L_70);
		G_B36_0 = G_B35_0;
	}

IL_026c:
	{
		Action_t437523947 * L_71 = ((UnityEventManager_t2255518493_StaticFields*)UnityEventManager_t2255518493_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache27_41();
		NullCheck(G_B36_0);
		G_B36_0->set_Started_22(L_71);
		MonoBehaviour__ctor_m350695606(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Zenject.UnityEventManager::add_ApplicationGainedFocus(System.Action)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t UnityEventManager_add_ApplicationGainedFocus_m4054089875_MetadataUsageId;
extern "C"  void UnityEventManager_add_ApplicationGainedFocus_m4054089875 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEventManager_add_ApplicationGainedFocus_m4054089875_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t437523947 * L_0 = __this->get_ApplicationGainedFocus_5();
		Action_t437523947 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_ApplicationGainedFocus_5(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void Zenject.UnityEventManager::remove_ApplicationGainedFocus(System.Action)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t UnityEventManager_remove_ApplicationGainedFocus_m1059060888_MetadataUsageId;
extern "C"  void UnityEventManager_remove_ApplicationGainedFocus_m1059060888 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEventManager_remove_ApplicationGainedFocus_m1059060888_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t437523947 * L_0 = __this->get_ApplicationGainedFocus_5();
		Action_t437523947 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_ApplicationGainedFocus_5(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void Zenject.UnityEventManager::add_ApplicationLostFocus(System.Action)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t UnityEventManager_add_ApplicationLostFocus_m1839379609_MetadataUsageId;
extern "C"  void UnityEventManager_add_ApplicationLostFocus_m1839379609 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEventManager_add_ApplicationLostFocus_m1839379609_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t437523947 * L_0 = __this->get_ApplicationLostFocus_6();
		Action_t437523947 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_ApplicationLostFocus_6(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void Zenject.UnityEventManager::remove_ApplicationLostFocus(System.Action)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t UnityEventManager_remove_ApplicationLostFocus_m2573692382_MetadataUsageId;
extern "C"  void UnityEventManager_remove_ApplicationLostFocus_m2573692382 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEventManager_remove_ApplicationLostFocus_m2573692382_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t437523947 * L_0 = __this->get_ApplicationLostFocus_6();
		Action_t437523947 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_ApplicationLostFocus_6(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void Zenject.UnityEventManager::add_ApplicationFocusChanged(System.Action`1<System.Boolean>)
extern Il2CppClass* Action_1_t359458046_il2cpp_TypeInfo_var;
extern const uint32_t UnityEventManager_add_ApplicationFocusChanged_m3839316197_MetadataUsageId;
extern "C"  void UnityEventManager_add_ApplicationFocusChanged_m3839316197 (UnityEventManager_t2255518493 * __this, Action_1_t359458046 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEventManager_add_ApplicationFocusChanged_m3839316197_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t359458046 * L_0 = __this->get_ApplicationFocusChanged_7();
		Action_1_t359458046 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_ApplicationFocusChanged_7(((Action_1_t359458046 *)CastclassSealed(L_2, Action_1_t359458046_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void Zenject.UnityEventManager::remove_ApplicationFocusChanged(System.Action`1<System.Boolean>)
extern Il2CppClass* Action_1_t359458046_il2cpp_TypeInfo_var;
extern const uint32_t UnityEventManager_remove_ApplicationFocusChanged_m1635484864_MetadataUsageId;
extern "C"  void UnityEventManager_remove_ApplicationFocusChanged_m1635484864 (UnityEventManager_t2255518493 * __this, Action_1_t359458046 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEventManager_remove_ApplicationFocusChanged_m1635484864_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t359458046 * L_0 = __this->get_ApplicationFocusChanged_7();
		Action_1_t359458046 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_ApplicationFocusChanged_7(((Action_1_t359458046 *)CastclassSealed(L_2, Action_1_t359458046_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void Zenject.UnityEventManager::add_ApplicationQuit(System.Action)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t UnityEventManager_add_ApplicationQuit_m1041428540_MetadataUsageId;
extern "C"  void UnityEventManager_add_ApplicationQuit_m1041428540 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEventManager_add_ApplicationQuit_m1041428540_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t437523947 * L_0 = __this->get_ApplicationQuit_8();
		Action_t437523947 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_ApplicationQuit_8(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void Zenject.UnityEventManager::remove_ApplicationQuit(System.Action)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t UnityEventManager_remove_ApplicationQuit_m4158046167_MetadataUsageId;
extern "C"  void UnityEventManager_remove_ApplicationQuit_m4158046167 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEventManager_remove_ApplicationQuit_m4158046167_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t437523947 * L_0 = __this->get_ApplicationQuit_8();
		Action_t437523947 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_ApplicationQuit_8(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void Zenject.UnityEventManager::add_ChangingScenes(System.Action)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t UnityEventManager_add_ChangingScenes_m1889670089_MetadataUsageId;
extern "C"  void UnityEventManager_add_ChangingScenes_m1889670089 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEventManager_add_ChangingScenes_m1889670089_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t437523947 * L_0 = __this->get_ChangingScenes_9();
		Action_t437523947 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_ChangingScenes_9(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void Zenject.UnityEventManager::remove_ChangingScenes(System.Action)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t UnityEventManager_remove_ChangingScenes_m2544395470_MetadataUsageId;
extern "C"  void UnityEventManager_remove_ChangingScenes_m2544395470 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEventManager_remove_ChangingScenes_m2544395470_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t437523947 * L_0 = __this->get_ChangingScenes_9();
		Action_t437523947 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_ChangingScenes_9(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void Zenject.UnityEventManager::add_DrawGizmos(System.Action)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t UnityEventManager_add_DrawGizmos_m3026080448_MetadataUsageId;
extern "C"  void UnityEventManager_add_DrawGizmos_m3026080448 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEventManager_add_DrawGizmos_m3026080448_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t437523947 * L_0 = __this->get_DrawGizmos_10();
		Action_t437523947 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_DrawGizmos_10(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void Zenject.UnityEventManager::remove_DrawGizmos(System.Action)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t UnityEventManager_remove_DrawGizmos_m4032294469_MetadataUsageId;
extern "C"  void UnityEventManager_remove_DrawGizmos_m4032294469 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEventManager_remove_DrawGizmos_m4032294469_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t437523947 * L_0 = __this->get_DrawGizmos_10();
		Action_t437523947 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_DrawGizmos_10(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void Zenject.UnityEventManager::add_MouseButtonDown(System.Action`1<System.Int32>)
extern Il2CppClass* Action_1_t2995867492_il2cpp_TypeInfo_var;
extern const uint32_t UnityEventManager_add_MouseButtonDown_m1820189278_MetadataUsageId;
extern "C"  void UnityEventManager_add_MouseButtonDown_m1820189278 (UnityEventManager_t2255518493 * __this, Action_1_t2995867492 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEventManager_add_MouseButtonDown_m1820189278_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t2995867492 * L_0 = __this->get_MouseButtonDown_11();
		Action_1_t2995867492 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_MouseButtonDown_11(((Action_1_t2995867492 *)CastclassSealed(L_2, Action_1_t2995867492_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void Zenject.UnityEventManager::remove_MouseButtonDown(System.Action`1<System.Int32>)
extern Il2CppClass* Action_1_t2995867492_il2cpp_TypeInfo_var;
extern const uint32_t UnityEventManager_remove_MouseButtonDown_m1906121721_MetadataUsageId;
extern "C"  void UnityEventManager_remove_MouseButtonDown_m1906121721 (UnityEventManager_t2255518493 * __this, Action_1_t2995867492 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEventManager_remove_MouseButtonDown_m1906121721_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t2995867492 * L_0 = __this->get_MouseButtonDown_11();
		Action_1_t2995867492 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_MouseButtonDown_11(((Action_1_t2995867492 *)CastclassSealed(L_2, Action_1_t2995867492_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void Zenject.UnityEventManager::add_MouseButtonUp(System.Action`1<System.Int32>)
extern Il2CppClass* Action_1_t2995867492_il2cpp_TypeInfo_var;
extern const uint32_t UnityEventManager_add_MouseButtonUp_m2565593925_MetadataUsageId;
extern "C"  void UnityEventManager_add_MouseButtonUp_m2565593925 (UnityEventManager_t2255518493 * __this, Action_1_t2995867492 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEventManager_add_MouseButtonUp_m2565593925_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t2995867492 * L_0 = __this->get_MouseButtonUp_12();
		Action_1_t2995867492 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_MouseButtonUp_12(((Action_1_t2995867492 *)CastclassSealed(L_2, Action_1_t2995867492_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void Zenject.UnityEventManager::remove_MouseButtonUp(System.Action`1<System.Int32>)
extern Il2CppClass* Action_1_t2995867492_il2cpp_TypeInfo_var;
extern const uint32_t UnityEventManager_remove_MouseButtonUp_m1412612000_MetadataUsageId;
extern "C"  void UnityEventManager_remove_MouseButtonUp_m1412612000 (UnityEventManager_t2255518493 * __this, Action_1_t2995867492 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEventManager_remove_MouseButtonUp_m1412612000_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t2995867492 * L_0 = __this->get_MouseButtonUp_12();
		Action_1_t2995867492 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_MouseButtonUp_12(((Action_1_t2995867492 *)CastclassSealed(L_2, Action_1_t2995867492_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void Zenject.UnityEventManager::add_LeftMouseButtonDown(System.Action)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t UnityEventManager_add_LeftMouseButtonDown_m3879338409_MetadataUsageId;
extern "C"  void UnityEventManager_add_LeftMouseButtonDown_m3879338409 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEventManager_add_LeftMouseButtonDown_m3879338409_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t437523947 * L_0 = __this->get_LeftMouseButtonDown_13();
		Action_t437523947 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_LeftMouseButtonDown_13(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void Zenject.UnityEventManager::remove_LeftMouseButtonDown(System.Action)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t UnityEventManager_remove_LeftMouseButtonDown_m1963363268_MetadataUsageId;
extern "C"  void UnityEventManager_remove_LeftMouseButtonDown_m1963363268 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEventManager_remove_LeftMouseButtonDown_m1963363268_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t437523947 * L_0 = __this->get_LeftMouseButtonDown_13();
		Action_t437523947 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_LeftMouseButtonDown_13(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void Zenject.UnityEventManager::add_LeftMouseButtonUp(System.Action)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t UnityEventManager_add_LeftMouseButtonUp_m188946000_MetadataUsageId;
extern "C"  void UnityEventManager_add_LeftMouseButtonUp_m188946000 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEventManager_add_LeftMouseButtonUp_m188946000_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t437523947 * L_0 = __this->get_LeftMouseButtonUp_14();
		Action_t437523947 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_LeftMouseButtonUp_14(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void Zenject.UnityEventManager::remove_LeftMouseButtonUp(System.Action)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t UnityEventManager_remove_LeftMouseButtonUp_m1666280235_MetadataUsageId;
extern "C"  void UnityEventManager_remove_LeftMouseButtonUp_m1666280235 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEventManager_remove_LeftMouseButtonUp_m1666280235_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t437523947 * L_0 = __this->get_LeftMouseButtonUp_14();
		Action_t437523947 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_LeftMouseButtonUp_14(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void Zenject.UnityEventManager::add_MiddleMouseButtonDown(System.Action)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t UnityEventManager_add_MiddleMouseButtonDown_m1624144631_MetadataUsageId;
extern "C"  void UnityEventManager_add_MiddleMouseButtonDown_m1624144631 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEventManager_add_MiddleMouseButtonDown_m1624144631_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t437523947 * L_0 = __this->get_MiddleMouseButtonDown_15();
		Action_t437523947 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_MiddleMouseButtonDown_15(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void Zenject.UnityEventManager::remove_MiddleMouseButtonDown(System.Action)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t UnityEventManager_remove_MiddleMouseButtonDown_m2913004114_MetadataUsageId;
extern "C"  void UnityEventManager_remove_MiddleMouseButtonDown_m2913004114 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEventManager_remove_MiddleMouseButtonDown_m2913004114_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t437523947 * L_0 = __this->get_MiddleMouseButtonDown_15();
		Action_t437523947 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_MiddleMouseButtonDown_15(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void Zenject.UnityEventManager::add_MiddleMouseButtonUp(System.Action)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t UnityEventManager_add_MiddleMouseButtonUp_m3802237726_MetadataUsageId;
extern "C"  void UnityEventManager_add_MiddleMouseButtonUp_m3802237726 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEventManager_add_MiddleMouseButtonUp_m3802237726_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t437523947 * L_0 = __this->get_MiddleMouseButtonUp_16();
		Action_t437523947 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_MiddleMouseButtonUp_16(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void Zenject.UnityEventManager::remove_MiddleMouseButtonUp(System.Action)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t UnityEventManager_remove_MiddleMouseButtonUp_m1886262585_MetadataUsageId;
extern "C"  void UnityEventManager_remove_MiddleMouseButtonUp_m1886262585 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEventManager_remove_MiddleMouseButtonUp_m1886262585_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t437523947 * L_0 = __this->get_MiddleMouseButtonUp_16();
		Action_t437523947 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_MiddleMouseButtonUp_16(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void Zenject.UnityEventManager::add_RightMouseButtonDown(System.Action)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t UnityEventManager_add_RightMouseButtonDown_m2991122528_MetadataUsageId;
extern "C"  void UnityEventManager_add_RightMouseButtonDown_m2991122528 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEventManager_add_RightMouseButtonDown_m2991122528_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t437523947 * L_0 = __this->get_RightMouseButtonDown_17();
		Action_t437523947 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_RightMouseButtonDown_17(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void Zenject.UnityEventManager::remove_RightMouseButtonDown(System.Action)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t UnityEventManager_remove_RightMouseButtonDown_m3725435301_MetadataUsageId;
extern "C"  void UnityEventManager_remove_RightMouseButtonDown_m3725435301 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEventManager_remove_RightMouseButtonDown_m3725435301_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t437523947 * L_0 = __this->get_RightMouseButtonDown_17();
		Action_t437523947 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_RightMouseButtonDown_17(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void Zenject.UnityEventManager::add_RightMouseButtonUp(System.Action)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t UnityEventManager_add_RightMouseButtonUp_m125451975_MetadataUsageId;
extern "C"  void UnityEventManager_add_RightMouseButtonUp_m125451975 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEventManager_add_RightMouseButtonUp_m125451975_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t437523947 * L_0 = __this->get_RightMouseButtonUp_18();
		Action_t437523947 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_RightMouseButtonUp_18(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void Zenject.UnityEventManager::remove_RightMouseButtonUp(System.Action)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t UnityEventManager_remove_RightMouseButtonUp_m2973140300_MetadataUsageId;
extern "C"  void UnityEventManager_remove_RightMouseButtonUp_m2973140300 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEventManager_remove_RightMouseButtonUp_m2973140300_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t437523947 * L_0 = __this->get_RightMouseButtonUp_18();
		Action_t437523947 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_RightMouseButtonUp_18(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void Zenject.UnityEventManager::add_MouseWheelMoved(System.Action`1<ModestTree.Util.MouseWheelScrollDirections>)
extern Il2CppClass* Action_1_t3075561458_il2cpp_TypeInfo_var;
extern const uint32_t UnityEventManager_add_MouseWheelMoved_m3459489442_MetadataUsageId;
extern "C"  void UnityEventManager_add_MouseWheelMoved_m3459489442 (UnityEventManager_t2255518493 * __this, Action_1_t3075561458 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEventManager_add_MouseWheelMoved_m3459489442_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t3075561458 * L_0 = __this->get_MouseWheelMoved_19();
		Action_1_t3075561458 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_MouseWheelMoved_19(((Action_1_t3075561458 *)CastclassSealed(L_2, Action_1_t3075561458_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void Zenject.UnityEventManager::remove_MouseWheelMoved(System.Action`1<ModestTree.Util.MouseWheelScrollDirections>)
extern Il2CppClass* Action_1_t3075561458_il2cpp_TypeInfo_var;
extern const uint32_t UnityEventManager_remove_MouseWheelMoved_m3499775741_MetadataUsageId;
extern "C"  void UnityEventManager_remove_MouseWheelMoved_m3499775741 (UnityEventManager_t2255518493 * __this, Action_1_t3075561458 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEventManager_remove_MouseWheelMoved_m3499775741_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t3075561458 * L_0 = __this->get_MouseWheelMoved_19();
		Action_1_t3075561458 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_MouseWheelMoved_19(((Action_1_t3075561458 *)CastclassSealed(L_2, Action_1_t3075561458_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void Zenject.UnityEventManager::add_MouseMove(System.Action)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t UnityEventManager_add_MouseMove_m3796333637_MetadataUsageId;
extern "C"  void UnityEventManager_add_MouseMove_m3796333637 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEventManager_add_MouseMove_m3796333637_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t437523947 * L_0 = __this->get_MouseMove_20();
		Action_t437523947 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_MouseMove_20(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void Zenject.UnityEventManager::remove_MouseMove(System.Action)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t UnityEventManager_remove_MouseMove_m1889129504_MetadataUsageId;
extern "C"  void UnityEventManager_remove_MouseMove_m1889129504 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEventManager_remove_MouseMove_m1889129504_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t437523947 * L_0 = __this->get_MouseMove_20();
		Action_t437523947 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_MouseMove_20(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void Zenject.UnityEventManager::add_ScreenSizeChanged(System.Action)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t UnityEventManager_add_ScreenSizeChanged_m896546772_MetadataUsageId;
extern "C"  void UnityEventManager_add_ScreenSizeChanged_m896546772 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEventManager_add_ScreenSizeChanged_m896546772_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t437523947 * L_0 = __this->get_ScreenSizeChanged_21();
		Action_t437523947 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_ScreenSizeChanged_21(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void Zenject.UnityEventManager::remove_ScreenSizeChanged(System.Action)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t UnityEventManager_remove_ScreenSizeChanged_m2373881007_MetadataUsageId;
extern "C"  void UnityEventManager_remove_ScreenSizeChanged_m2373881007 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEventManager_remove_ScreenSizeChanged_m2373881007_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t437523947 * L_0 = __this->get_ScreenSizeChanged_21();
		Action_t437523947 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_ScreenSizeChanged_21(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void Zenject.UnityEventManager::add_Started(System.Action)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t UnityEventManager_add_Started_m1641477690_MetadataUsageId;
extern "C"  void UnityEventManager_add_Started_m1641477690 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEventManager_add_Started_m1641477690_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t437523947 * L_0 = __this->get_Started_22();
		Action_t437523947 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_Started_22(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void Zenject.UnityEventManager::remove_Started(System.Action)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t UnityEventManager_remove_Started_m2573570261_MetadataUsageId;
extern "C"  void UnityEventManager_remove_Started_m2573570261 (UnityEventManager_t2255518493 * __this, Action_t437523947 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEventManager_remove_Started_m2573570261_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t437523947 * L_0 = __this->get_Started_22();
		Action_t437523947 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_Started_22(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Boolean Zenject.UnityEventManager::get_IsFocused()
extern "C"  bool UnityEventManager_get_IsFocused_m213405752 (UnityEventManager_t2255518493 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_U3CIsFocusedU3Ek__BackingField_23();
		return L_0;
	}
}
// System.Void Zenject.UnityEventManager::set_IsFocused(System.Boolean)
extern "C"  void UnityEventManager_set_IsFocused_m3303716615 (UnityEventManager_t2255518493 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CIsFocusedU3Ek__BackingField_23(L_0);
		return;
	}
}
// System.Void Zenject.UnityEventManager::Start()
extern "C"  void UnityEventManager_Start_m347311394 (UnityEventManager_t2255518493 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Screen_get_width_m1424489183(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__lastWidth_3(L_0);
		int32_t L_1 = Screen_get_height_m1713306064(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__lastHeight_4(L_1);
		Action_t437523947 * L_2 = __this->get_Started_22();
		NullCheck(L_2);
		Action_Invoke_m1445970038(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Zenject.UnityEventManager::Tick()
extern Il2CppClass* Input_t1593691127_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m4053252443_MethodInfo_var;
extern const MethodInfo* Action_1_Invoke_m2996669171_MethodInfo_var;
extern const uint32_t UnityEventManager_Tick_m3216313151_MetadataUsageId;
extern "C"  void UnityEventManager_Tick_m3216313151 (UnityEventManager_t2255518493 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEventManager_Tick_m3216313151_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		V_0 = 0;
		V_1 = 1;
		V_2 = 2;
		int32_t L_0 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetMouseButtonDown_m3552475078(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002d;
		}
	}
	{
		Action_t437523947 * L_2 = __this->get_LeftMouseButtonDown_13();
		NullCheck(L_2);
		Action_Invoke_m1445970038(L_2, /*hidden argument*/NULL);
		Action_1_t2995867492 * L_3 = __this->get_MouseButtonDown_11();
		NullCheck(L_3);
		Action_1_Invoke_m4053252443(L_3, 0, /*hidden argument*/Action_1_Invoke_m4053252443_MethodInfo_var);
		goto IL_004f;
	}

IL_002d:
	{
		int32_t L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_5 = Input_GetMouseButtonUp_m2588144188(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_004f;
		}
	}
	{
		Action_t437523947 * L_6 = __this->get_LeftMouseButtonUp_14();
		NullCheck(L_6);
		Action_Invoke_m1445970038(L_6, /*hidden argument*/NULL);
		Action_1_t2995867492 * L_7 = __this->get_MouseButtonUp_12();
		NullCheck(L_7);
		Action_1_Invoke_m4053252443(L_7, 0, /*hidden argument*/Action_1_Invoke_m4053252443_MethodInfo_var);
	}

IL_004f:
	{
		int32_t L_8 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_9 = Input_GetMouseButtonDown_m3552475078(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0076;
		}
	}
	{
		Action_t437523947 * L_10 = __this->get_RightMouseButtonDown_17();
		NullCheck(L_10);
		Action_Invoke_m1445970038(L_10, /*hidden argument*/NULL);
		Action_1_t2995867492 * L_11 = __this->get_MouseButtonDown_11();
		NullCheck(L_11);
		Action_1_Invoke_m4053252443(L_11, 1, /*hidden argument*/Action_1_Invoke_m4053252443_MethodInfo_var);
		goto IL_0098;
	}

IL_0076:
	{
		int32_t L_12 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_13 = Input_GetMouseButtonUp_m2588144188(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0098;
		}
	}
	{
		Action_t437523947 * L_14 = __this->get_RightMouseButtonUp_18();
		NullCheck(L_14);
		Action_Invoke_m1445970038(L_14, /*hidden argument*/NULL);
		Action_1_t2995867492 * L_15 = __this->get_MouseButtonUp_12();
		NullCheck(L_15);
		Action_1_Invoke_m4053252443(L_15, 1, /*hidden argument*/Action_1_Invoke_m4053252443_MethodInfo_var);
	}

IL_0098:
	{
		int32_t L_16 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_17 = Input_GetMouseButtonDown_m3552475078(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00bf;
		}
	}
	{
		Action_t437523947 * L_18 = __this->get_MiddleMouseButtonDown_15();
		NullCheck(L_18);
		Action_Invoke_m1445970038(L_18, /*hidden argument*/NULL);
		Action_1_t2995867492 * L_19 = __this->get_MouseButtonDown_11();
		NullCheck(L_19);
		Action_1_Invoke_m4053252443(L_19, 2, /*hidden argument*/Action_1_Invoke_m4053252443_MethodInfo_var);
		goto IL_00e1;
	}

IL_00bf:
	{
		int32_t L_20 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_21 = Input_GetMouseButtonUp_m2588144188(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_00e1;
		}
	}
	{
		Action_t437523947 * L_22 = __this->get_MiddleMouseButtonUp_16();
		NullCheck(L_22);
		Action_Invoke_m1445970038(L_22, /*hidden argument*/NULL);
		Action_1_t2995867492 * L_23 = __this->get_MouseButtonUp_12();
		NullCheck(L_23);
		Action_1_Invoke_m4053252443(L_23, 2, /*hidden argument*/Action_1_Invoke_m4053252443_MethodInfo_var);
	}

IL_00e1:
	{
		Vector3_t3525329789  L_24 = __this->get__lastMousePosition_2();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		Vector3_t3525329789  L_25 = Input_get_mousePosition_m4020233228(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_26 = Vector3_op_Inequality_m4114231449(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_010c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		Vector3_t3525329789  L_27 = Input_get_mousePosition_m4020233228(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__lastMousePosition_2(L_27);
		Action_t437523947 * L_28 = __this->get_MouseMove_20();
		NullCheck(L_28);
		Action_Invoke_m1445970038(L_28, /*hidden argument*/NULL);
	}

IL_010c:
	{
		int32_t L_29 = UnityUtil_CheckMouseScrollWheel_m3579850857(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_29;
		int32_t L_30 = V_3;
		if (!L_30)
		{
			goto IL_0124;
		}
	}
	{
		Action_1_t3075561458 * L_31 = __this->get_MouseWheelMoved_19();
		int32_t L_32 = V_3;
		NullCheck(L_31);
		Action_1_Invoke_m2996669171(L_31, L_32, /*hidden argument*/Action_1_Invoke_m2996669171_MethodInfo_var);
	}

IL_0124:
	{
		int32_t L_33 = __this->get__lastWidth_3();
		int32_t L_34 = Screen_get_width_m1424489183(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_33) == ((uint32_t)L_34))))
		{
			goto IL_0144;
		}
	}
	{
		int32_t L_35 = __this->get__lastHeight_4();
		int32_t L_36 = Screen_get_height_m1713306064(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_35) == ((int32_t)L_36)))
		{
			goto IL_0165;
		}
	}

IL_0144:
	{
		int32_t L_37 = Screen_get_width_m1424489183(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__lastWidth_3(L_37);
		int32_t L_38 = Screen_get_height_m1713306064(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__lastHeight_4(L_38);
		Action_t437523947 * L_39 = __this->get_ScreenSizeChanged_21();
		NullCheck(L_39);
		Action_Invoke_m1445970038(L_39, /*hidden argument*/NULL);
	}

IL_0165:
	{
		return;
	}
}
// System.Void Zenject.UnityEventManager::OnDestroy()
extern "C"  void UnityEventManager_OnDestroy_m2039212059 (UnityEventManager_t2255518493 * __this, const MethodInfo* method)
{
	{
		Action_t437523947 * L_0 = __this->get_ChangingScenes_9();
		NullCheck(L_0);
		Action_Invoke_m1445970038(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Zenject.UnityEventManager::OnApplicationQuit()
extern "C"  void UnityEventManager_OnApplicationQuit_m1108754080 (UnityEventManager_t2255518493 * __this, const MethodInfo* method)
{
	{
		Action_t437523947 * L_0 = __this->get_ApplicationQuit_8();
		NullCheck(L_0);
		Action_Invoke_m1445970038(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Zenject.UnityEventManager::OnDrawGizmos()
extern "C"  void UnityEventManager_OnDrawGizmos_m4106392254 (UnityEventManager_t2255518493 * __this, const MethodInfo* method)
{
	{
		Action_t437523947 * L_0 = __this->get_DrawGizmos_10();
		NullCheck(L_0);
		Action_Invoke_m1445970038(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Zenject.UnityEventManager::OnApplicationFocus(System.Boolean)
extern const MethodInfo* Action_1_Invoke_m510242183_MethodInfo_var;
extern const uint32_t UnityEventManager_OnApplicationFocus_m4278425728_MetadataUsageId;
extern "C"  void UnityEventManager_OnApplicationFocus_m4278425728 (UnityEventManager_t2255518493 * __this, bool ___newIsFocused0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEventManager_OnApplicationFocus_m4278425728_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___newIsFocused0;
		if (!L_0)
		{
			goto IL_002f;
		}
	}
	{
		bool L_1 = UnityEventManager_get_IsFocused_m213405752(__this, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_002f;
		}
	}
	{
		UnityEventManager_set_IsFocused_m3303716615(__this, (bool)1, /*hidden argument*/NULL);
		Action_t437523947 * L_2 = __this->get_ApplicationGainedFocus_5();
		NullCheck(L_2);
		Action_Invoke_m1445970038(L_2, /*hidden argument*/NULL);
		Action_1_t359458046 * L_3 = __this->get_ApplicationFocusChanged_7();
		NullCheck(L_3);
		Action_1_Invoke_m510242183(L_3, (bool)1, /*hidden argument*/Action_1_Invoke_m510242183_MethodInfo_var);
	}

IL_002f:
	{
		bool L_4 = ___newIsFocused0;
		if (L_4)
		{
			goto IL_005e;
		}
	}
	{
		bool L_5 = UnityEventManager_get_IsFocused_m213405752(__this, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_005e;
		}
	}
	{
		UnityEventManager_set_IsFocused_m3303716615(__this, (bool)0, /*hidden argument*/NULL);
		Action_t437523947 * L_6 = __this->get_ApplicationLostFocus_6();
		NullCheck(L_6);
		Action_Invoke_m1445970038(L_6, /*hidden argument*/NULL);
		Action_1_t359458046 * L_7 = __this->get_ApplicationFocusChanged_7();
		NullCheck(L_7);
		Action_1_Invoke_m510242183(L_7, (bool)0, /*hidden argument*/Action_1_Invoke_m510242183_MethodInfo_var);
	}

IL_005e:
	{
		return;
	}
}
// System.Void Zenject.UnityEventManager::<ApplicationGainedFocus>m__2F1()
extern "C"  void UnityEventManager_U3CApplicationGainedFocusU3Em__2F1_m2575721386 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Zenject.UnityEventManager::<ApplicationLostFocus>m__2F2()
extern "C"  void UnityEventManager_U3CApplicationLostFocusU3Em__2F2_m1691150577 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Zenject.UnityEventManager::<ApplicationFocusChanged>m__2F3(System.Boolean)
extern "C"  void UnityEventManager_U3CApplicationFocusChangedU3Em__2F3_m3170362807 (Il2CppObject * __this /* static, unused */, bool p0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Zenject.UnityEventManager::<ApplicationQuit>m__2F4()
extern "C"  void UnityEventManager_U3CApplicationQuitU3Em__2F4_m2326336718 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Zenject.UnityEventManager::<ChangingScenes>m__2F5()
extern "C"  void UnityEventManager_U3CChangingScenesU3Em__2F5_m2194074212 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Zenject.UnityEventManager::<DrawGizmos>m__2F6()
extern "C"  void UnityEventManager_U3CDrawGizmosU3Em__2F6_m2238731356 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Zenject.UnityEventManager::<MouseButtonDown>m__2F7(System.Int32)
extern "C"  void UnityEventManager_U3CMouseButtonDownU3Em__2F7_m2930362984 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Zenject.UnityEventManager::<MouseButtonUp>m__2F8(System.Int32)
extern "C"  void UnityEventManager_U3CMouseButtonUpU3Em__2F8_m4087750992 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Zenject.UnityEventManager::<LeftMouseButtonDown>m__2F9()
extern "C"  void UnityEventManager_U3CLeftMouseButtonDownU3Em__2F9_m148156608 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Zenject.UnityEventManager::<LeftMouseButtonUp>m__2FA()
extern "C"  void UnityEventManager_U3CLeftMouseButtonUpU3Em__2FA_m2430237615 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Zenject.UnityEventManager::<MiddleMouseButtonDown>m__2FB()
extern "C"  void UnityEventManager_U3CMiddleMouseButtonDownU3Em__2FB_m2259636055 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Zenject.UnityEventManager::<MiddleMouseButtonUp>m__2FC()
extern "C"  void UnityEventManager_U3CMiddleMouseButtonUpU3Em__2FC_m3844725631 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Zenject.UnityEventManager::<RightMouseButtonDown>m__2FD()
extern "C"  void UnityEventManager_U3CRightMouseButtonDownU3Em__2FD_m1610318218 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Zenject.UnityEventManager::<RightMouseButtonUp>m__2FE()
extern "C"  void UnityEventManager_U3CRightMouseButtonUpU3Em__2FE_m1546847730 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Zenject.UnityEventManager::<MouseWheelMoved>m__2FF(ModestTree.Util.MouseWheelScrollDirections)
extern "C"  void UnityEventManager_U3CMouseWheelMovedU3Em__2FF_m1664420075 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Zenject.UnityEventManager::<MouseMove>m__300()
extern "C"  void UnityEventManager_U3CMouseMoveU3Em__300_m3730852074 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Zenject.UnityEventManager::<ScreenSizeChanged>m__301()
extern "C"  void UnityEventManager_U3CScreenSizeChangedU3Em__301_m2782129018 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Zenject.UnityEventManager::<Started>m__302()
extern "C"  void UnityEventManager_U3CStartedU3Em__302_m1311423777 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Zenject.ZenjectBindException::.ctor(System.String)
extern "C"  void ZenjectBindException__ctor_m3054227529 (ZenjectBindException_t3550319704 * __this, String_t* ___message0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		ZenjectException__ctor_m3982815302(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Zenject.ZenjectBindException::.ctor(System.String,System.Exception)
extern "C"  void ZenjectBindException__ctor_m1767600141 (ZenjectBindException_t3550319704 * __this, String_t* ___message0, Exception_t1967233988 * ___innerException1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t1967233988 * L_1 = ___innerException1;
		ZenjectException__ctor_m2645600688(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Zenject.ZenjectException::.ctor(System.String)
extern "C"  void ZenjectException__ctor_m3982815302 (ZenjectException_t523236501 * __this, String_t* ___message0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		Exception__ctor_m3870771296(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Zenject.ZenjectException::.ctor(System.String,System.Exception)
extern "C"  void ZenjectException__ctor_m2645600688 (ZenjectException_t523236501 * __this, String_t* ___message0, Exception_t1967233988 * ___innerException1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t1967233988 * L_1 = ___innerException1;
		Exception__ctor_m1328171222(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Zenject.ZenjectResolveException::.ctor(System.String)
extern "C"  void ZenjectResolveException__ctor_m3382034954 (ZenjectResolveException_t1201052999 * __this, String_t* ___message0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		ZenjectException__ctor_m3982815302(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Zenject.ZenjectResolveException::.ctor(System.String,System.Exception)
extern "C"  void ZenjectResolveException__ctor_m1087182956 (ZenjectResolveException_t1201052999 * __this, String_t* ___message0, Exception_t1967233988 * ___innerException1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t1967233988 * L_1 = ___innerException1;
		ZenjectException__ctor_m2645600688(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Zenject.ZenjectTypeInfo::.ctor(System.Type,System.Collections.Generic.List`1<Zenject.PostInjectableInfo>,System.Reflection.ConstructorInfo,System.Collections.Generic.List`1<Zenject.InjectableInfo>,System.Collections.Generic.List`1<Zenject.InjectableInfo>,System.Collections.Generic.List`1<Zenject.InjectableInfo>)
extern "C"  void ZenjectTypeInfo__ctor_m4156201214 (ZenjectTypeInfo_t283213708 * __this, Type_t * ___typeAnalyzed0, List_1_t3877242631 * ___postInjectMethods1, ConstructorInfo_t3542137334 * ___injectConstructor2, List_1_t1944668743 * ___fieldInjectables3, List_1_t1944668743 * ___propertyInjectables4, List_1_t1944668743 * ___constructorInjectables5, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		List_1_t3877242631 * L_0 = ___postInjectMethods1;
		__this->set__postInjectMethods_0(L_0);
		List_1_t1944668743 * L_1 = ___fieldInjectables3;
		__this->set__fieldInjectables_2(L_1);
		List_1_t1944668743 * L_2 = ___propertyInjectables4;
		__this->set__propertyInjectables_3(L_2);
		List_1_t1944668743 * L_3 = ___constructorInjectables5;
		__this->set__constructorInjectables_1(L_3);
		ConstructorInfo_t3542137334 * L_4 = ___injectConstructor2;
		__this->set__injectConstructor_4(L_4);
		Type_t * L_5 = ___typeAnalyzed0;
		__this->set__typeAnalyzed_5(L_5);
		return;
	}
}
// System.Type Zenject.ZenjectTypeInfo::get_TypeAnalyzed()
extern "C"  Type_t * ZenjectTypeInfo_get_TypeAnalyzed_m89817778 (ZenjectTypeInfo_t283213708 * __this, const MethodInfo* method)
{
	{
		Type_t * L_0 = __this->get__typeAnalyzed_5();
		return L_0;
	}
}
// System.Collections.Generic.IEnumerable`1<Zenject.PostInjectableInfo> Zenject.ZenjectTypeInfo::get_PostInjectMethods()
extern "C"  Il2CppObject* ZenjectTypeInfo_get_PostInjectMethods_m2187525833 (ZenjectTypeInfo_t283213708 * __this, const MethodInfo* method)
{
	{
		List_1_t3877242631 * L_0 = __this->get__postInjectMethods_0();
		return L_0;
	}
}
// System.Collections.Generic.IEnumerable`1<Zenject.InjectableInfo> Zenject.ZenjectTypeInfo::get_AllInjectables()
extern Il2CppClass* ZenjectTypeInfo_t283213708_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t2429407812_il2cpp_TypeInfo_var;
extern const MethodInfo* Enumerable_Concat_TisInjectableInfo_t1147709774_m1287063574_MethodInfo_var;
extern const MethodInfo* ZenjectTypeInfo_U3Cget_AllInjectablesU3Em__2B9_m2318910823_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m1730094395_MethodInfo_var;
extern const MethodInfo* Enumerable_SelectMany_TisPostInjectableInfo_t3080283662_TisInjectableInfo_t1147709774_m2380885368_MethodInfo_var;
extern const uint32_t ZenjectTypeInfo_get_AllInjectables_m3519834113_MetadataUsageId;
extern "C"  Il2CppObject* ZenjectTypeInfo_get_AllInjectables_m3519834113 (ZenjectTypeInfo_t283213708 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ZenjectTypeInfo_get_AllInjectables_m3519834113_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t3877242631 * G_B2_0 = NULL;
	Il2CppObject* G_B2_1 = NULL;
	List_1_t3877242631 * G_B1_0 = NULL;
	Il2CppObject* G_B1_1 = NULL;
	{
		List_1_t1944668743 * L_0 = __this->get__constructorInjectables_1();
		List_1_t1944668743 * L_1 = __this->get__fieldInjectables_2();
		Il2CppObject* L_2 = Enumerable_Concat_TisInjectableInfo_t1147709774_m1287063574(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/Enumerable_Concat_TisInjectableInfo_t1147709774_m1287063574_MethodInfo_var);
		List_1_t1944668743 * L_3 = __this->get__propertyInjectables_3();
		Il2CppObject* L_4 = Enumerable_Concat_TisInjectableInfo_t1147709774_m1287063574(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/Enumerable_Concat_TisInjectableInfo_t1147709774_m1287063574_MethodInfo_var);
		List_1_t3877242631 * L_5 = __this->get__postInjectMethods_0();
		Func_2_t2429407812 * L_6 = ((ZenjectTypeInfo_t283213708_StaticFields*)ZenjectTypeInfo_t283213708_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache6_6();
		G_B1_0 = L_5;
		G_B1_1 = L_4;
		if (L_6)
		{
			G_B2_0 = L_5;
			G_B2_1 = L_4;
			goto IL_003a;
		}
	}
	{
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)ZenjectTypeInfo_U3Cget_AllInjectablesU3Em__2B9_m2318910823_MethodInfo_var);
		Func_2_t2429407812 * L_8 = (Func_2_t2429407812 *)il2cpp_codegen_object_new(Func_2_t2429407812_il2cpp_TypeInfo_var);
		Func_2__ctor_m1730094395(L_8, NULL, L_7, /*hidden argument*/Func_2__ctor_m1730094395_MethodInfo_var);
		((ZenjectTypeInfo_t283213708_StaticFields*)ZenjectTypeInfo_t283213708_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache6_6(L_8);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
	}

IL_003a:
	{
		Func_2_t2429407812 * L_9 = ((ZenjectTypeInfo_t283213708_StaticFields*)ZenjectTypeInfo_t283213708_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache6_6();
		Il2CppObject* L_10 = Enumerable_SelectMany_TisPostInjectableInfo_t3080283662_TisInjectableInfo_t1147709774_m2380885368(NULL /*static, unused*/, G_B2_0, L_9, /*hidden argument*/Enumerable_SelectMany_TisPostInjectableInfo_t3080283662_TisInjectableInfo_t1147709774_m2380885368_MethodInfo_var);
		Il2CppObject* L_11 = Enumerable_Concat_TisInjectableInfo_t1147709774_m1287063574(NULL /*static, unused*/, G_B2_1, L_10, /*hidden argument*/Enumerable_Concat_TisInjectableInfo_t1147709774_m1287063574_MethodInfo_var);
		return L_11;
	}
}
// System.Collections.Generic.IEnumerable`1<Zenject.InjectableInfo> Zenject.ZenjectTypeInfo::get_FieldInjectables()
extern "C"  Il2CppObject* ZenjectTypeInfo_get_FieldInjectables_m870985480 (ZenjectTypeInfo_t283213708 * __this, const MethodInfo* method)
{
	{
		List_1_t1944668743 * L_0 = __this->get__fieldInjectables_2();
		return L_0;
	}
}
// System.Collections.Generic.IEnumerable`1<Zenject.InjectableInfo> Zenject.ZenjectTypeInfo::get_PropertyInjectables()
extern "C"  Il2CppObject* ZenjectTypeInfo_get_PropertyInjectables_m202125435 (ZenjectTypeInfo_t283213708 * __this, const MethodInfo* method)
{
	{
		List_1_t1944668743 * L_0 = __this->get__propertyInjectables_3();
		return L_0;
	}
}
// System.Collections.Generic.IEnumerable`1<Zenject.InjectableInfo> Zenject.ZenjectTypeInfo::get_ConstructorInjectables()
extern "C"  Il2CppObject* ZenjectTypeInfo_get_ConstructorInjectables_m3437893960 (ZenjectTypeInfo_t283213708 * __this, const MethodInfo* method)
{
	{
		List_1_t1944668743 * L_0 = __this->get__constructorInjectables_1();
		return L_0;
	}
}
// System.Reflection.ConstructorInfo Zenject.ZenjectTypeInfo::get_InjectConstructor()
extern "C"  ConstructorInfo_t3542137334 * ZenjectTypeInfo_get_InjectConstructor_m103263552 (ZenjectTypeInfo_t283213708 * __this, const MethodInfo* method)
{
	{
		ConstructorInfo_t3542137334 * L_0 = __this->get__injectConstructor_4();
		return L_0;
	}
}
// System.Collections.Generic.IEnumerable`1<Zenject.InjectableInfo> Zenject.ZenjectTypeInfo::<get_AllInjectables>m__2B9(Zenject.PostInjectableInfo)
extern "C"  Il2CppObject* ZenjectTypeInfo_U3Cget_AllInjectablesU3Em__2B9_m2318910823 (Il2CppObject * __this /* static, unused */, PostInjectableInfo_t3080283662 * ___x0, const MethodInfo* method)
{
	{
		PostInjectableInfo_t3080283662 * L_0 = ___x0;
		NullCheck(L_0);
		ReadOnlyCollection_1_t15887826 * L_1 = L_0->get_InjectableInfo_1();
		return L_1;
	}
}
// System.Void Zenject.ZenUtil::.ctor()
extern "C"  void ZenUtil__ctor_m3462249477 (ZenUtil_t3149524122 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Zenject.ZenUtil::IsNull(System.Object)
extern "C"  bool ZenUtil_IsNull_m3822553240 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Il2CppObject * L_0 = ___obj0;
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		Il2CppObject * L_1 = ___obj0;
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_1, NULL);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_0010;
	}

IL_000f:
	{
		G_B3_0 = 1;
	}

IL_0010:
	{
		return (bool)G_B3_0;
	}
}
// System.Void Zenject.ZenUtil::LoadScene(System.String)
extern "C"  void ZenUtil_LoadScene_m2898507705 (Il2CppObject * __this /* static, unused */, String_t* ___levelName0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___levelName0;
		ZenUtil_LoadSceneInternal_m1988330721(NULL /*static, unused*/, L_0, (bool)0, (Action_1_t2531567154 *)NULL, (Action_1_t2531567154 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Zenject.ZenUtil::LoadScene(System.String,System.Action`1<Zenject.DiContainer>)
extern "C"  void ZenUtil_LoadScene_m3251234358 (Il2CppObject * __this /* static, unused */, String_t* ___levelName0, Action_1_t2531567154 * ___preBindings1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___levelName0;
		Action_1_t2531567154 * L_1 = ___preBindings1;
		ZenUtil_LoadSceneInternal_m1988330721(NULL /*static, unused*/, L_0, (bool)0, L_1, (Action_1_t2531567154 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Zenject.ZenUtil::LoadScene(System.String,System.Action`1<Zenject.DiContainer>,System.Action`1<Zenject.DiContainer>)
extern "C"  void ZenUtil_LoadScene_m3590255065 (Il2CppObject * __this /* static, unused */, String_t* ___levelName0, Action_1_t2531567154 * ___preBindings1, Action_1_t2531567154 * ___postBindings2, const MethodInfo* method)
{
	{
		String_t* L_0 = ___levelName0;
		Action_1_t2531567154 * L_1 = ___preBindings1;
		Action_1_t2531567154 * L_2 = ___postBindings2;
		ZenUtil_LoadSceneInternal_m1988330721(NULL /*static, unused*/, L_0, (bool)0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Zenject.ZenUtil::LoadSceneAdditive(System.String)
extern "C"  void ZenUtil_LoadSceneAdditive_m4086475341 (Il2CppObject * __this /* static, unused */, String_t* ___levelName0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___levelName0;
		ZenUtil_LoadSceneInternal_m1988330721(NULL /*static, unused*/, L_0, (bool)1, (Action_1_t2531567154 *)NULL, (Action_1_t2531567154 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Zenject.ZenUtil::LoadSceneAdditive(System.String,System.Action`1<Zenject.DiContainer>)
extern "C"  void ZenUtil_LoadSceneAdditive_m2676801570 (Il2CppObject * __this /* static, unused */, String_t* ___levelName0, Action_1_t2531567154 * ___preBindings1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___levelName0;
		Action_1_t2531567154 * L_1 = ___preBindings1;
		ZenUtil_LoadSceneInternal_m1988330721(NULL /*static, unused*/, L_0, (bool)1, L_1, (Action_1_t2531567154 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Zenject.ZenUtil::LoadSceneAdditive(System.String,System.Action`1<Zenject.DiContainer>,System.Action`1<Zenject.DiContainer>)
extern "C"  void ZenUtil_LoadSceneAdditive_m2288524141 (Il2CppObject * __this /* static, unused */, String_t* ___levelName0, Action_1_t2531567154 * ___preBindings1, Action_1_t2531567154 * ___postBindings2, const MethodInfo* method)
{
	{
		String_t* L_0 = ___levelName0;
		Action_1_t2531567154 * L_1 = ___preBindings1;
		Action_1_t2531567154 * L_2 = ___postBindings2;
		ZenUtil_LoadSceneInternal_m1988330721(NULL /*static, unused*/, L_0, (bool)1, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Zenject.ZenUtil::LoadSceneInternal(System.String,System.Boolean,System.Action`1<Zenject.DiContainer>,System.Action`1<Zenject.DiContainer>)
extern Il2CppClass* SceneCompositionRoot_t1259693845_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t2531567154_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1631026408;
extern Il2CppCodeGenString* _stringLiteral4230042685;
extern Il2CppCodeGenString* _stringLiteral3230948288;
extern const uint32_t ZenUtil_LoadSceneInternal_m1988330721_MetadataUsageId;
extern "C"  void ZenUtil_LoadSceneInternal_m1988330721 (Il2CppObject * __this /* static, unused */, String_t* ___levelName0, bool ___isAdditive1, Action_1_t2531567154 * ___preBindings2, Action_1_t2531567154 * ___postBindings3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ZenUtil_LoadSceneInternal_m1988330721_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t2531567154 * L_0 = ___preBindings2;
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SceneCompositionRoot_t1259693845_il2cpp_TypeInfo_var);
		Action_1_t2531567154 * L_1 = ((SceneCompositionRoot_t1259693845_StaticFields*)SceneCompositionRoot_t1259693845_il2cpp_TypeInfo_var->static_fields)->get_BeforeInstallHooks_2();
		Action_1_t2531567154 * L_2 = ___preBindings2;
		Delegate_t3660574010 * L_3 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		((SceneCompositionRoot_t1259693845_StaticFields*)SceneCompositionRoot_t1259693845_il2cpp_TypeInfo_var->static_fields)->set_BeforeInstallHooks_2(((Action_1_t2531567154 *)CastclassSealed(L_3, Action_1_t2531567154_il2cpp_TypeInfo_var)));
	}

IL_001b:
	{
		Action_1_t2531567154 * L_4 = ___postBindings3;
		if (!L_4)
		{
			goto IL_0036;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SceneCompositionRoot_t1259693845_il2cpp_TypeInfo_var);
		Action_1_t2531567154 * L_5 = ((SceneCompositionRoot_t1259693845_StaticFields*)SceneCompositionRoot_t1259693845_il2cpp_TypeInfo_var->static_fields)->get_AfterInstallHooks_3();
		Action_1_t2531567154 * L_6 = ___postBindings3;
		Delegate_t3660574010 * L_7 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		((SceneCompositionRoot_t1259693845_StaticFields*)SceneCompositionRoot_t1259693845_il2cpp_TypeInfo_var->static_fields)->set_AfterInstallHooks_3(((Action_1_t2531567154 *)CastclassSealed(L_7, Action_1_t2531567154_il2cpp_TypeInfo_var)));
	}

IL_0036:
	{
		String_t* L_8 = ___levelName0;
		bool L_9 = Application_CanStreamedLevelBeLoaded_m1206869776(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_10 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		String_t* L_11 = ___levelName0;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_11);
		Assert_That_m2934751473(NULL /*static, unused*/, L_9, _stringLiteral1631026408, L_10, /*hidden argument*/NULL);
		bool L_12 = ___isAdditive1;
		if (!L_12)
		{
			goto IL_0061;
		}
	}
	{
		String_t* L_13 = ___levelName0;
		Application_LoadLevelAdditive_m3028901073(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		goto IL_008f;
	}

IL_0061:
	{
		ObjectU5BU5D_t11523773* L_14 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		String_t* L_15 = ___levelName0;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 0);
		ArrayElementTypeCheck (L_14, L_15);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_15);
		Log_Debug_m3985269754(NULL /*static, unused*/, _stringLiteral4230042685, L_14, /*hidden argument*/NULL);
		String_t* L_16 = ___levelName0;
		Application_LoadLevel_m2722573885(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_17 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		String_t* L_18 = ___levelName0;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 0);
		ArrayElementTypeCheck (L_17, L_18);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_18);
		Log_Debug_m3985269754(NULL /*static, unused*/, _stringLiteral3230948288, L_17, /*hidden argument*/NULL);
	}

IL_008f:
	{
		return;
	}
}
// System.Collections.IEnumerator Zenject.ZenUtil::LoadSceneAdditiveWithContainer(System.String,Zenject.DiContainer)
extern Il2CppClass* U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D_t2977085640_il2cpp_TypeInfo_var;
extern const uint32_t ZenUtil_LoadSceneAdditiveWithContainer_m1312014993_MetadataUsageId;
extern "C"  Il2CppObject * ZenUtil_LoadSceneAdditiveWithContainer_m1312014993 (Il2CppObject * __this /* static, unused */, String_t* ___levelName0, DiContainer_t2383114449 * ___parentContainer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ZenUtil_LoadSceneAdditiveWithContainer_m1312014993_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D_t2977085640 * V_0 = NULL;
	{
		U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D_t2977085640 * L_0 = (U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D_t2977085640 *)il2cpp_codegen_object_new(U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D_t2977085640_il2cpp_TypeInfo_var);
		U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D__ctor_m582760364(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D_t2977085640 * L_1 = V_0;
		String_t* L_2 = ___levelName0;
		NullCheck(L_1);
		L_1->set_levelName_1(L_2);
		U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D_t2977085640 * L_3 = V_0;
		DiContainer_t2383114449 * L_4 = ___parentContainer1;
		NullCheck(L_3);
		L_3->set_parentContainer_5(L_4);
		U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D_t2977085640 * L_5 = V_0;
		String_t* L_6 = ___levelName0;
		NullCheck(L_5);
		L_5->set_U3CU24U3ElevelName_8(L_6);
		U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D_t2977085640 * L_7 = V_0;
		DiContainer_t2383114449 * L_8 = ___parentContainer1;
		NullCheck(L_7);
		L_7->set_U3CU24U3EparentContainer_9(L_8);
		U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D_t2977085640 * L_9 = V_0;
		return L_9;
	}
}
// System.Void Zenject.ZenUtil/<LoadSceneAdditiveWithContainer>c__Iterator4D::.ctor()
extern "C"  void U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D__ctor_m582760364 (U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D_t2977085640 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Zenject.ZenUtil/<LoadSceneAdditiveWithContainer>c__Iterator4D::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m296853990 (U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D_t2977085640 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_7();
		return L_0;
	}
}
// System.Object Zenject.ZenUtil/<LoadSceneAdditiveWithContainer>c__Iterator4D::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D_System_Collections_IEnumerator_get_Current_m4131402106 (U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D_t2977085640 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_7();
		return L_0;
	}
}
// System.Boolean Zenject.ZenUtil/<LoadSceneAdditiveWithContainer>c__Iterator4D::MoveNext()
extern Il2CppClass* IEnumerable_1_t2589882162_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t1200834254_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* Enumerable_Except_TisGameObject_t4012695102_m3386116033_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisSceneCompositionRoot_t1259693845_m758702398_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2206141365;
extern const uint32_t U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D_MoveNext_m3967568328_MetadataUsageId;
extern "C"  bool U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D_MoveNext_m3967568328 (U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D_t2977085640 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D_MoveNext_m3967568328_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_6();
		V_0 = L_0;
		__this->set_U24PC_6((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_004a;
		}
	}
	{
		goto IL_00ea;
	}

IL_0021:
	{
		List_1_t514686775 * L_2 = UnityUtil_GetRootGameObjects_m729052607(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3CrootObjectsBeforeLoadU3E__0_0(L_2);
		String_t* L_3 = __this->get_levelName_1();
		Application_LoadLevelAdditive_m3028901073(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		__this->set_U24current_7(NULL);
		__this->set_U24PC_6(1);
		goto IL_00ec;
	}

IL_004a:
	{
		List_1_t514686775 * L_4 = UnityUtil_GetRootGameObjects_m729052607(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3CrootObjectsAfterLoadU3E__1_2(L_4);
		List_1_t514686775 * L_5 = __this->get_U3CrootObjectsAfterLoadU3E__1_2();
		List_1_t514686775 * L_6 = __this->get_U3CrootObjectsBeforeLoadU3E__0_0();
		Il2CppObject* L_7 = Enumerable_Except_TisGameObject_t4012695102_m3386116033(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/Enumerable_Except_TisGameObject_t4012695102_m3386116033_MethodInfo_var);
		NullCheck(L_7);
		Il2CppObject* L_8 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.GameObject>::GetEnumerator() */, IEnumerable_1_t2589882162_il2cpp_TypeInfo_var, L_7);
		__this->set_U3CU24s_321U3E__2_3(L_8);
	}

IL_0071:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00b9;
		}

IL_0076:
		{
			Il2CppObject* L_9 = __this->get_U3CU24s_321U3E__2_3();
			NullCheck(L_9);
			GameObject_t4012695102 * L_10 = InterfaceFuncInvoker0< GameObject_t4012695102 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.GameObject>::get_Current() */, IEnumerator_1_t1200834254_il2cpp_TypeInfo_var, L_9);
			__this->set_U3CnewObjectU3E__3_4(L_10);
			GameObject_t4012695102 * L_11 = __this->get_U3CnewObjectU3E__3_4();
			NullCheck(L_11);
			SceneCompositionRoot_t1259693845 * L_12 = GameObject_GetComponent_TisSceneCompositionRoot_t1259693845_m758702398(L_11, /*hidden argument*/GameObject_GetComponent_TisSceneCompositionRoot_t1259693845_m758702398_MethodInfo_var);
			bool L_13 = Object_op_Equality_m1690293457(NULL /*static, unused*/, L_12, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
			Assert_That_m2934751473(NULL /*static, unused*/, L_13, _stringLiteral2206141365, ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
			DiContainer_t2383114449 * L_14 = __this->get_parentContainer_5();
			GameObject_t4012695102 * L_15 = __this->get_U3CnewObjectU3E__3_4();
			NullCheck(L_14);
			VirtActionInvoker1< GameObject_t4012695102 * >::Invoke(62 /* System.Void Zenject.DiContainer::InjectGameObject(UnityEngine.GameObject) */, L_14, L_15);
		}

IL_00b9:
		{
			Il2CppObject* L_16 = __this->get_U3CU24s_321U3E__2_3();
			NullCheck(L_16);
			bool L_17 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_16);
			if (L_17)
			{
				goto IL_0076;
			}
		}

IL_00c9:
		{
			IL2CPP_LEAVE(0xE3, FINALLY_00ce);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00ce;
	}

FINALLY_00ce:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_18 = __this->get_U3CU24s_321U3E__2_3();
			if (L_18)
			{
				goto IL_00d7;
			}
		}

IL_00d6:
		{
			IL2CPP_END_FINALLY(206)
		}

IL_00d7:
		{
			Il2CppObject* L_19 = __this->get_U3CU24s_321U3E__2_3();
			NullCheck(L_19);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_19);
			IL2CPP_END_FINALLY(206)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(206)
	{
		IL2CPP_JUMP_TBL(0xE3, IL_00e3)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00e3:
	{
		__this->set_U24PC_6((-1));
	}

IL_00ea:
	{
		return (bool)0;
	}

IL_00ec:
	{
		return (bool)1;
	}
	// Dead block : IL_00ee: ldloc.1
}
// System.Void Zenject.ZenUtil/<LoadSceneAdditiveWithContainer>c__Iterator4D::Dispose()
extern "C"  void U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D_Dispose_m1585244393 (U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D_t2977085640 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_6((-1));
		return;
	}
}
// System.Void Zenject.ZenUtil/<LoadSceneAdditiveWithContainer>c__Iterator4D::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D_Reset_m2524160601_MetadataUsageId;
extern "C"  void U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D_Reset_m2524160601 (U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D_t2977085640 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadSceneAdditiveWithContainerU3Ec__Iterator4D_Reset_m2524160601_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// IStream`1<UnityEngine.EventSystems.BaseEventData> ZergEngineTools::PressStream(UnityEngine.EventSystems.EventTrigger)
extern Il2CppClass* U3CPressStreamU3Ec__AnonStorey14E_t2542632690_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_3_t3600102922_il2cpp_TypeInfo_var;
extern Il2CppClass* AnonymousStream_1_t2804241537_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CPressStreamU3Ec__AnonStorey14E_U3CU3Em__1FB_m2964617654_MethodInfo_var;
extern const MethodInfo* Func_3__ctor_m2483892355_MethodInfo_var;
extern const MethodInfo* AnonymousStream_1__ctor_m52982127_MethodInfo_var;
extern const uint32_t ZergEngineTools_PressStream_m2131367255_MetadataUsageId;
extern "C"  Il2CppObject* ZergEngineTools_PressStream_m2131367255 (Il2CppObject * __this /* static, unused */, EventTrigger_t2937500249 * ___trigger0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ZergEngineTools_PressStream_m2131367255_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CPressStreamU3Ec__AnonStorey14E_t2542632690 * V_0 = NULL;
	{
		U3CPressStreamU3Ec__AnonStorey14E_t2542632690 * L_0 = (U3CPressStreamU3Ec__AnonStorey14E_t2542632690 *)il2cpp_codegen_object_new(U3CPressStreamU3Ec__AnonStorey14E_t2542632690_il2cpp_TypeInfo_var);
		U3CPressStreamU3Ec__AnonStorey14E__ctor_m630648751(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CPressStreamU3Ec__AnonStorey14E_t2542632690 * L_1 = V_0;
		EventTrigger_t2937500249 * L_2 = ___trigger0;
		NullCheck(L_1);
		L_1->set_trigger_0(L_2);
		U3CPressStreamU3Ec__AnonStorey14E_t2542632690 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CPressStreamU3Ec__AnonStorey14E_U3CU3Em__1FB_m2964617654_MethodInfo_var);
		Func_3_t3600102922 * L_5 = (Func_3_t3600102922 *)il2cpp_codegen_object_new(Func_3_t3600102922_il2cpp_TypeInfo_var);
		Func_3__ctor_m2483892355(L_5, L_3, L_4, /*hidden argument*/Func_3__ctor_m2483892355_MethodInfo_var);
		AnonymousStream_1_t2804241537 * L_6 = (AnonymousStream_1_t2804241537 *)il2cpp_codegen_object_new(AnonymousStream_1_t2804241537_il2cpp_TypeInfo_var);
		AnonymousStream_1__ctor_m52982127(L_6, L_5, /*hidden argument*/AnonymousStream_1__ctor_m52982127_MethodInfo_var);
		return L_6;
	}
}
// IEmptyStream ZergEngineTools::ClickStream(UnityEngine.UI.Button)
extern Il2CppClass* U3CClickStreamU3Ec__AnonStorey14F_t3915235342_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppClass* AbandonedStream_t4197275220_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_3_t1216273990_il2cpp_TypeInfo_var;
extern Il2CppClass* AnonymousEmptyStream_t62006432_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CClickStreamU3Ec__AnonStorey14F_U3CU3Em__1FC_m2237704742_MethodInfo_var;
extern const MethodInfo* Func_3__ctor_m1890144528_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2954032594;
extern const uint32_t ZergEngineTools_ClickStream_m2271378604_MetadataUsageId;
extern "C"  Il2CppObject * ZergEngineTools_ClickStream_m2271378604 (Il2CppObject * __this /* static, unused */, Button_t990034267 * ___button0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ZergEngineTools_ClickStream_m2271378604_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CClickStreamU3Ec__AnonStorey14F_t3915235342 * V_0 = NULL;
	{
		U3CClickStreamU3Ec__AnonStorey14F_t3915235342 * L_0 = (U3CClickStreamU3Ec__AnonStorey14F_t3915235342 *)il2cpp_codegen_object_new(U3CClickStreamU3Ec__AnonStorey14F_t3915235342_il2cpp_TypeInfo_var);
		U3CClickStreamU3Ec__AnonStorey14F__ctor_m998780435(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CClickStreamU3Ec__AnonStorey14F_t3915235342 * L_1 = V_0;
		Button_t990034267 * L_2 = ___button0;
		NullCheck(L_1);
		L_1->set_button_0(L_2);
		U3CClickStreamU3Ec__AnonStorey14F_t3915235342 * L_3 = V_0;
		NullCheck(L_3);
		Button_t990034267 * L_4 = L_3->get_button_0();
		bool L_5 = Object_op_Equality_m1690293457(NULL /*static, unused*/, L_4, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m1836388681(NULL /*static, unused*/, _stringLiteral2954032594, /*hidden argument*/NULL);
		AbandonedStream_t4197275220 * L_6 = (AbandonedStream_t4197275220 *)il2cpp_codegen_object_new(AbandonedStream_t4197275220_il2cpp_TypeInfo_var);
		AbandonedStream__ctor_m3467923351(L_6, /*hidden argument*/NULL);
		return L_6;
	}

IL_002e:
	{
		U3CClickStreamU3Ec__AnonStorey14F_t3915235342 * L_7 = V_0;
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)U3CClickStreamU3Ec__AnonStorey14F_U3CU3Em__1FC_m2237704742_MethodInfo_var);
		Func_3_t1216273990 * L_9 = (Func_3_t1216273990 *)il2cpp_codegen_object_new(Func_3_t1216273990_il2cpp_TypeInfo_var);
		Func_3__ctor_m1890144528(L_9, L_7, L_8, /*hidden argument*/Func_3__ctor_m1890144528_MethodInfo_var);
		AnonymousEmptyStream_t62006432 * L_10 = (AnonymousEmptyStream_t62006432 *)il2cpp_codegen_object_new(AnonymousEmptyStream_t62006432_il2cpp_TypeInfo_var);
		AnonymousEmptyStream__ctor_m3388750898(L_10, L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// IEmptyStream ZergEngineTools::HoldStream(UnityEngine.UI.Button)
extern Il2CppClass* U3CHoldStreamU3Ec__AnonStorey150_t1648779976_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppClass* AbandonedStream_t4197275220_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_3_t1216273990_il2cpp_TypeInfo_var;
extern Il2CppClass* AnonymousEmptyStream_t62006432_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CHoldStreamU3Ec__AnonStorey150_U3CU3Em__1FD_m1965807395_MethodInfo_var;
extern const MethodInfo* Func_3__ctor_m1890144528_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2954032594;
extern const uint32_t ZergEngineTools_HoldStream_m641685669_MetadataUsageId;
extern "C"  Il2CppObject * ZergEngineTools_HoldStream_m641685669 (Il2CppObject * __this /* static, unused */, Button_t990034267 * ___button0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ZergEngineTools_HoldStream_m641685669_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CHoldStreamU3Ec__AnonStorey150_t1648779976 * V_0 = NULL;
	{
		U3CHoldStreamU3Ec__AnonStorey150_t1648779976 * L_0 = (U3CHoldStreamU3Ec__AnonStorey150_t1648779976 *)il2cpp_codegen_object_new(U3CHoldStreamU3Ec__AnonStorey150_t1648779976_il2cpp_TypeInfo_var);
		U3CHoldStreamU3Ec__AnonStorey150__ctor_m2030460189(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CHoldStreamU3Ec__AnonStorey150_t1648779976 * L_1 = V_0;
		Button_t990034267 * L_2 = ___button0;
		NullCheck(L_1);
		L_1->set_button_0(L_2);
		U3CHoldStreamU3Ec__AnonStorey150_t1648779976 * L_3 = V_0;
		NullCheck(L_3);
		Button_t990034267 * L_4 = L_3->get_button_0();
		bool L_5 = Object_op_Equality_m1690293457(NULL /*static, unused*/, L_4, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m1836388681(NULL /*static, unused*/, _stringLiteral2954032594, /*hidden argument*/NULL);
		AbandonedStream_t4197275220 * L_6 = (AbandonedStream_t4197275220 *)il2cpp_codegen_object_new(AbandonedStream_t4197275220_il2cpp_TypeInfo_var);
		AbandonedStream__ctor_m3467923351(L_6, /*hidden argument*/NULL);
		return L_6;
	}

IL_002e:
	{
		U3CHoldStreamU3Ec__AnonStorey150_t1648779976 * L_7 = V_0;
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)U3CHoldStreamU3Ec__AnonStorey150_U3CU3Em__1FD_m1965807395_MethodInfo_var);
		Func_3_t1216273990 * L_9 = (Func_3_t1216273990 *)il2cpp_codegen_object_new(Func_3_t1216273990_il2cpp_TypeInfo_var);
		Func_3__ctor_m1890144528(L_9, L_7, L_8, /*hidden argument*/Func_3__ctor_m1890144528_MethodInfo_var);
		AnonymousEmptyStream_t62006432 * L_10 = (AnonymousEmptyStream_t62006432 *)il2cpp_codegen_object_new(AnonymousEmptyStream_t62006432_il2cpp_TypeInfo_var);
		AnonymousEmptyStream__ctor_m3388750898(L_10, L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.Void ZergEngineTools/<ClickStream>c__AnonStorey14F::.ctor()
extern "C"  void U3CClickStreamU3Ec__AnonStorey14F__ctor_m998780435 (U3CClickStreamU3Ec__AnonStorey14F_t3915235342 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.IDisposable ZergEngineTools/<ClickStream>c__AnonStorey14F::<>m__1FC(System.Action,Priority)
extern Il2CppClass* UnityAction_t909267611_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityActionDisposable_t208063915_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_Invoke_m1445970038_MethodInfo_var;
extern const uint32_t U3CClickStreamU3Ec__AnonStorey14F_U3CU3Em__1FC_m2237704742_MetadataUsageId;
extern "C"  Il2CppObject * U3CClickStreamU3Ec__AnonStorey14F_U3CU3Em__1FC_m2237704742 (U3CClickStreamU3Ec__AnonStorey14F_t3915235342 * __this, Action_t437523947 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CClickStreamU3Ec__AnonStorey14F_U3CU3Em__1FC_m2237704742_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UnityAction_t909267611 * V_0 = NULL;
	UnityActionDisposable_t208063915 * V_1 = NULL;
	{
		Action_t437523947 * L_0 = ___reaction0;
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)Action_Invoke_m1445970038_MethodInfo_var);
		UnityAction_t909267611 * L_2 = (UnityAction_t909267611 *)il2cpp_codegen_object_new(UnityAction_t909267611_il2cpp_TypeInfo_var);
		UnityAction__ctor_m4130179243(L_2, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Button_t990034267 * L_3 = __this->get_button_0();
		NullCheck(L_3);
		ButtonClickedEvent_t962981669 * L_4 = Button_get_onClick_m1145127631(L_3, /*hidden argument*/NULL);
		UnityAction_t909267611 * L_5 = V_0;
		NullCheck(L_4);
		UnityEvent_AddListener_m4099140869(L_4, L_5, /*hidden argument*/NULL);
		UnityActionDisposable_t208063915 * L_6 = (UnityActionDisposable_t208063915 *)il2cpp_codegen_object_new(UnityActionDisposable_t208063915_il2cpp_TypeInfo_var);
		UnityActionDisposable__ctor_m3467829014(L_6, /*hidden argument*/NULL);
		V_1 = L_6;
		UnityActionDisposable_t208063915 * L_7 = V_1;
		UnityAction_t909267611 * L_8 = V_0;
		NullCheck(L_7);
		L_7->set_action_1(L_8);
		UnityActionDisposable_t208063915 * L_9 = V_1;
		Button_t990034267 * L_10 = __this->get_button_0();
		NullCheck(L_10);
		ButtonClickedEvent_t962981669 * L_11 = Button_get_onClick_m1145127631(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		L_9->set_e_0(L_11);
		UnityActionDisposable_t208063915 * L_12 = V_1;
		return L_12;
	}
}
// System.Void ZergEngineTools/<HoldStream>c__AnonStorey150::.ctor()
extern "C"  void U3CHoldStreamU3Ec__AnonStorey150__ctor_m2030460189 (U3CHoldStreamU3Ec__AnonStorey150_t1648779976 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.IDisposable ZergEngineTools/<HoldStream>c__AnonStorey150::<>m__1FD(System.Action,Priority)
extern Il2CppClass* UnityAction_t909267611_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityActionDisposable_t208063915_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_Invoke_m1445970038_MethodInfo_var;
extern const uint32_t U3CHoldStreamU3Ec__AnonStorey150_U3CU3Em__1FD_m1965807395_MetadataUsageId;
extern "C"  Il2CppObject * U3CHoldStreamU3Ec__AnonStorey150_U3CU3Em__1FD_m1965807395 (U3CHoldStreamU3Ec__AnonStorey150_t1648779976 * __this, Action_t437523947 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CHoldStreamU3Ec__AnonStorey150_U3CU3Em__1FD_m1965807395_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UnityAction_t909267611 * V_0 = NULL;
	UnityActionDisposable_t208063915 * V_1 = NULL;
	{
		Action_t437523947 * L_0 = ___reaction0;
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)Action_Invoke_m1445970038_MethodInfo_var);
		UnityAction_t909267611 * L_2 = (UnityAction_t909267611 *)il2cpp_codegen_object_new(UnityAction_t909267611_il2cpp_TypeInfo_var);
		UnityAction__ctor_m4130179243(L_2, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Button_t990034267 * L_3 = __this->get_button_0();
		NullCheck(L_3);
		ButtonClickedEvent_t962981669 * L_4 = Button_get_onClick_m1145127631(L_3, /*hidden argument*/NULL);
		UnityAction_t909267611 * L_5 = V_0;
		NullCheck(L_4);
		UnityEvent_AddListener_m4099140869(L_4, L_5, /*hidden argument*/NULL);
		UnityActionDisposable_t208063915 * L_6 = (UnityActionDisposable_t208063915 *)il2cpp_codegen_object_new(UnityActionDisposable_t208063915_il2cpp_TypeInfo_var);
		UnityActionDisposable__ctor_m3467829014(L_6, /*hidden argument*/NULL);
		V_1 = L_6;
		UnityActionDisposable_t208063915 * L_7 = V_1;
		UnityAction_t909267611 * L_8 = V_0;
		NullCheck(L_7);
		L_7->set_action_1(L_8);
		UnityActionDisposable_t208063915 * L_9 = V_1;
		Button_t990034267 * L_10 = __this->get_button_0();
		NullCheck(L_10);
		ButtonClickedEvent_t962981669 * L_11 = Button_get_onClick_m1145127631(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		L_9->set_e_0(L_11);
		UnityActionDisposable_t208063915 * L_12 = V_1;
		return L_12;
	}
}
// System.Void ZergEngineTools/<PressStream>c__AnonStorey14E::.ctor()
extern "C"  void U3CPressStreamU3Ec__AnonStorey14E__ctor_m630648751 (U3CPressStreamU3Ec__AnonStorey14E_t2542632690 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.IDisposable ZergEngineTools/<PressStream>c__AnonStorey14E::<>m__1FB(System.Action`1<UnityEngine.EventSystems.BaseEventData>,Priority)
extern Il2CppClass* U3CPressStreamU3Ec__AnonStorey14D_t2542632689_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAction_1_t3527565631_il2cpp_TypeInfo_var;
extern Il2CppClass* TriggerEvent_t516183010_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t864074059_il2cpp_TypeInfo_var;
extern Il2CppClass* Entry_t67115091_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityActionDisposable_1_t2125801935_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CPressStreamU3Ec__AnonStorey14D_U3CU3Em__1FE_m3056454486_MethodInfo_var;
extern const MethodInfo* UnityAction_1__ctor_m3175874657_MethodInfo_var;
extern const MethodInfo* UnityEvent_1_AddListener_m568941693_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m1265020938_MethodInfo_var;
extern const MethodInfo* UnityActionDisposable_1__ctor_m3788165897_MethodInfo_var;
extern const uint32_t U3CPressStreamU3Ec__AnonStorey14E_U3CU3Em__1FB_m2964617654_MetadataUsageId;
extern "C"  Il2CppObject * U3CPressStreamU3Ec__AnonStorey14E_U3CU3Em__1FB_m2964617654 (U3CPressStreamU3Ec__AnonStorey14E_t2542632690 * __this, Action_1_t3695556431 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CPressStreamU3Ec__AnonStorey14E_U3CU3Em__1FB_m2964617654_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UnityAction_1_t3527565631 * V_0 = NULL;
	TriggerEvent_t516183010 * V_1 = NULL;
	U3CPressStreamU3Ec__AnonStorey14D_t2542632689 * V_2 = NULL;
	Entry_t67115091 * V_3 = NULL;
	UnityActionDisposable_1_t2125801935 * V_4 = NULL;
	{
		U3CPressStreamU3Ec__AnonStorey14D_t2542632689 * L_0 = (U3CPressStreamU3Ec__AnonStorey14D_t2542632689 *)il2cpp_codegen_object_new(U3CPressStreamU3Ec__AnonStorey14D_t2542632689_il2cpp_TypeInfo_var);
		U3CPressStreamU3Ec__AnonStorey14D__ctor_m1849442253(L_0, /*hidden argument*/NULL);
		V_2 = L_0;
		U3CPressStreamU3Ec__AnonStorey14D_t2542632689 * L_1 = V_2;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU24334_1(__this);
		U3CPressStreamU3Ec__AnonStorey14D_t2542632689 * L_2 = V_2;
		Action_1_t3695556431 * L_3 = ___reaction0;
		NullCheck(L_2);
		L_2->set_reaction_0(L_3);
		U3CPressStreamU3Ec__AnonStorey14D_t2542632689 * L_4 = V_2;
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)U3CPressStreamU3Ec__AnonStorey14D_U3CU3Em__1FE_m3056454486_MethodInfo_var);
		UnityAction_1_t3527565631 * L_6 = (UnityAction_1_t3527565631 *)il2cpp_codegen_object_new(UnityAction_1_t3527565631_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m3175874657(L_6, L_4, L_5, /*hidden argument*/UnityAction_1__ctor_m3175874657_MethodInfo_var);
		V_0 = L_6;
		TriggerEvent_t516183010 * L_7 = (TriggerEvent_t516183010 *)il2cpp_codegen_object_new(TriggerEvent_t516183010_il2cpp_TypeInfo_var);
		TriggerEvent__ctor_m1729711890(L_7, /*hidden argument*/NULL);
		V_1 = L_7;
		TriggerEvent_t516183010 * L_8 = V_1;
		UnityAction_1_t3527565631 * L_9 = V_0;
		NullCheck(L_8);
		UnityEvent_1_AddListener_m568941693(L_8, L_9, /*hidden argument*/UnityEvent_1_AddListener_m568941693_MethodInfo_var);
		EventTrigger_t2937500249 * L_10 = __this->get_trigger_0();
		NullCheck(L_10);
		List_1_t864074059 * L_11 = EventTrigger_get_triggers_m900961340(L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_004e;
		}
	}
	{
		EventTrigger_t2937500249 * L_12 = __this->get_trigger_0();
		List_1_t864074059 * L_13 = (List_1_t864074059 *)il2cpp_codegen_object_new(List_1_t864074059_il2cpp_TypeInfo_var);
		List_1__ctor_m1265020938(L_13, /*hidden argument*/List_1__ctor_m1265020938_MethodInfo_var);
		NullCheck(L_12);
		EventTrigger_set_triggers_m2956640203(L_12, L_13, /*hidden argument*/NULL);
	}

IL_004e:
	{
		EventTrigger_t2937500249 * L_14 = __this->get_trigger_0();
		NullCheck(L_14);
		List_1_t864074059 * L_15 = EventTrigger_get_triggers_m900961340(L_14, /*hidden argument*/NULL);
		Entry_t67115091 * L_16 = (Entry_t67115091 *)il2cpp_codegen_object_new(Entry_t67115091_il2cpp_TypeInfo_var);
		Entry__ctor_m515618144(L_16, /*hidden argument*/NULL);
		V_3 = L_16;
		Entry_t67115091 * L_17 = V_3;
		NullCheck(L_17);
		L_17->set_eventID_0(2);
		Entry_t67115091 * L_18 = V_3;
		TriggerEvent_t516183010 * L_19 = V_1;
		NullCheck(L_18);
		L_18->set_callback_1(L_19);
		Entry_t67115091 * L_20 = V_3;
		NullCheck(L_15);
		VirtActionInvoker1< Entry_t67115091 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::Add(!0) */, L_15, L_20);
		UnityActionDisposable_1_t2125801935 * L_21 = (UnityActionDisposable_1_t2125801935 *)il2cpp_codegen_object_new(UnityActionDisposable_1_t2125801935_il2cpp_TypeInfo_var);
		UnityActionDisposable_1__ctor_m3788165897(L_21, /*hidden argument*/UnityActionDisposable_1__ctor_m3788165897_MethodInfo_var);
		V_4 = L_21;
		UnityActionDisposable_1_t2125801935 * L_22 = V_4;
		UnityAction_1_t3527565631 * L_23 = V_0;
		NullCheck(L_22);
		L_22->set_action_1(L_23);
		UnityActionDisposable_1_t2125801935 * L_24 = V_4;
		TriggerEvent_t516183010 * L_25 = V_1;
		NullCheck(L_24);
		L_24->set_e_0(L_25);
		UnityActionDisposable_1_t2125801935 * L_26 = V_4;
		return L_26;
	}
}
// System.Void ZergEngineTools/<PressStream>c__AnonStorey14E/<PressStream>c__AnonStorey14D::.ctor()
extern "C"  void U3CPressStreamU3Ec__AnonStorey14D__ctor_m1849442253 (U3CPressStreamU3Ec__AnonStorey14D_t2542632689 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ZergEngineTools/<PressStream>c__AnonStorey14E/<PressStream>c__AnonStorey14D::<>m__1FE(UnityEngine.EventSystems.BaseEventData)
extern const MethodInfo* Action_1_Invoke_m2439915812_MethodInfo_var;
extern const uint32_t U3CPressStreamU3Ec__AnonStorey14D_U3CU3Em__1FE_m3056454486_MetadataUsageId;
extern "C"  void U3CPressStreamU3Ec__AnonStorey14D_U3CU3Em__1FE_m3056454486 (U3CPressStreamU3Ec__AnonStorey14D_t2542632689 * __this, BaseEventData_t3547103726 * ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CPressStreamU3Ec__AnonStorey14D_U3CU3Em__1FE_m3056454486_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t3695556431 * L_0 = __this->get_reaction_0();
		BaseEventData_t3547103726 * L_1 = ___data0;
		NullCheck(L_0);
		Action_1_Invoke_m2439915812(L_0, L_1, /*hidden argument*/Action_1_Invoke_m2439915812_MethodInfo_var);
		return;
	}
}
// System.Void ZergEngineTools/UnityActionDisposable::.ctor()
extern "C"  void UnityActionDisposable__ctor_m3467829014 (UnityActionDisposable_t208063915 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ZergEngineTools/UnityActionDisposable::Dispose()
extern "C"  void UnityActionDisposable_Dispose_m3882311123 (UnityActionDisposable_t208063915 * __this, const MethodInfo* method)
{
	{
		UnityEvent_t2938797301 * L_0 = __this->get_e_0();
		if (!L_0)
		{
			goto IL_002a;
		}
	}
	{
		UnityEvent_t2938797301 * L_1 = __this->get_e_0();
		UnityAction_t909267611 * L_2 = __this->get_action_1();
		NullCheck(L_1);
		UnityEvent_RemoveListener_m1947914352(L_1, L_2, /*hidden argument*/NULL);
		__this->set_e_0((UnityEvent_t2938797301 *)NULL);
		__this->set_action_1((UnityAction_t909267611 *)NULL);
	}

IL_002a:
	{
		return;
	}
}
// UniRx.IObservable`1<UnityEngine.Vector2> ZergRush.ObservableExtension::EveryTapOutsideRectTransform(UnityEngine.RectTransform)
extern Il2CppClass* U3CEveryTapOutsideRectTransformU3Ec__AnonStorey11B_t2782190866_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t4239542457_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CEveryTapOutsideRectTransformU3Ec__AnonStorey11B_U3CU3Em__1A0_m1024781829_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m647878481_MethodInfo_var;
extern const MethodInfo* Observable_Where_TisVector2_t3525329788_m902023148_MethodInfo_var;
extern const uint32_t ObservableExtension_EveryTapOutsideRectTransform_m3930871207_MetadataUsageId;
extern "C"  Il2CppObject* ObservableExtension_EveryTapOutsideRectTransform_m3930871207 (Il2CppObject * __this /* static, unused */, RectTransform_t3317474837 * ___rectTransform0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableExtension_EveryTapOutsideRectTransform_m3930871207_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CEveryTapOutsideRectTransformU3Ec__AnonStorey11B_t2782190866 * V_0 = NULL;
	{
		U3CEveryTapOutsideRectTransformU3Ec__AnonStorey11B_t2782190866 * L_0 = (U3CEveryTapOutsideRectTransformU3Ec__AnonStorey11B_t2782190866 *)il2cpp_codegen_object_new(U3CEveryTapOutsideRectTransformU3Ec__AnonStorey11B_t2782190866_il2cpp_TypeInfo_var);
		U3CEveryTapOutsideRectTransformU3Ec__AnonStorey11B__ctor_m3026690416(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CEveryTapOutsideRectTransformU3Ec__AnonStorey11B_t2782190866 * L_1 = V_0;
		RectTransform_t3317474837 * L_2 = ___rectTransform0;
		NullCheck(L_1);
		L_1->set_rectTransform_0(L_2);
		Il2CppObject* L_3 = ObservableExtension_EveryTap_m2592619319(NULL /*static, unused*/, /*hidden argument*/NULL);
		U3CEveryTapOutsideRectTransformU3Ec__AnonStorey11B_t2782190866 * L_4 = V_0;
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)U3CEveryTapOutsideRectTransformU3Ec__AnonStorey11B_U3CU3Em__1A0_m1024781829_MethodInfo_var);
		Func_2_t4239542457 * L_6 = (Func_2_t4239542457 *)il2cpp_codegen_object_new(Func_2_t4239542457_il2cpp_TypeInfo_var);
		Func_2__ctor_m647878481(L_6, L_4, L_5, /*hidden argument*/Func_2__ctor_m647878481_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_7 = Observable_Where_TisVector2_t3525329788_m902023148(NULL /*static, unused*/, L_3, L_6, /*hidden argument*/Observable_Where_TisVector2_t3525329788_m902023148_MethodInfo_var);
		return L_7;
	}
}
// UniRx.IObservable`1<UnityEngine.Vector2> ZergRush.ObservableExtension::EveryTap()
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern Il2CppClass* ObservableExtension_t8126852_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t2251336571_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t1270693722_il2cpp_TypeInfo_var;
extern const MethodInfo* ObservableExtension_U3CEveryTapU3Em__1A1_m3919383235_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m2519360593_MethodInfo_var;
extern const MethodInfo* Observable_Where_TisInt64_t2847414882_m2960877922_MethodInfo_var;
extern const MethodInfo* ObservableExtension_U3CEveryTapU3Em__1A2_m323694379_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m31193364_MethodInfo_var;
extern const MethodInfo* Observable_Select_TisInt64_t2847414882_TisVector2_t3525329788_m584888666_MethodInfo_var;
extern const uint32_t ObservableExtension_EveryTap_m2592619319_MetadataUsageId;
extern "C"  Il2CppObject* ObservableExtension_EveryTap_m2592619319 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableExtension_EveryTap_m2592619319_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* G_B2_0 = NULL;
	Il2CppObject* G_B1_0 = NULL;
	Il2CppObject* G_B4_0 = NULL;
	Il2CppObject* G_B3_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_0 = Observable_EveryUpdate_m1588154268(NULL /*static, unused*/, /*hidden argument*/NULL);
		Func_2_t2251336571 * L_1 = ((ObservableExtension_t8126852_StaticFields*)ObservableExtension_t8126852_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_0();
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_001d;
		}
	}
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)ObservableExtension_U3CEveryTapU3Em__1A1_m3919383235_MethodInfo_var);
		Func_2_t2251336571 * L_3 = (Func_2_t2251336571 *)il2cpp_codegen_object_new(Func_2_t2251336571_il2cpp_TypeInfo_var);
		Func_2__ctor_m2519360593(L_3, NULL, L_2, /*hidden argument*/Func_2__ctor_m2519360593_MethodInfo_var);
		((ObservableExtension_t8126852_StaticFields*)ObservableExtension_t8126852_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache0_0(L_3);
		G_B2_0 = G_B1_0;
	}

IL_001d:
	{
		Func_2_t2251336571 * L_4 = ((ObservableExtension_t8126852_StaticFields*)ObservableExtension_t8126852_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_0();
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_5 = Observable_Where_TisInt64_t2847414882_m2960877922(NULL /*static, unused*/, G_B2_0, L_4, /*hidden argument*/Observable_Where_TisInt64_t2847414882_m2960877922_MethodInfo_var);
		Func_2_t1270693722 * L_6 = ((ObservableExtension_t8126852_StaticFields*)ObservableExtension_t8126852_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_1();
		G_B3_0 = L_5;
		if (L_6)
		{
			G_B4_0 = L_5;
			goto IL_003f;
		}
	}
	{
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)ObservableExtension_U3CEveryTapU3Em__1A2_m323694379_MethodInfo_var);
		Func_2_t1270693722 * L_8 = (Func_2_t1270693722 *)il2cpp_codegen_object_new(Func_2_t1270693722_il2cpp_TypeInfo_var);
		Func_2__ctor_m31193364(L_8, NULL, L_7, /*hidden argument*/Func_2__ctor_m31193364_MethodInfo_var);
		((ObservableExtension_t8126852_StaticFields*)ObservableExtension_t8126852_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache1_1(L_8);
		G_B4_0 = G_B3_0;
	}

IL_003f:
	{
		Func_2_t1270693722 * L_9 = ((ObservableExtension_t8126852_StaticFields*)ObservableExtension_t8126852_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_1();
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_10 = Observable_Select_TisInt64_t2847414882_TisVector2_t3525329788_m584888666(NULL /*static, unused*/, G_B4_0, L_9, /*hidden argument*/Observable_Select_TisInt64_t2847414882_TisVector2_t3525329788_m584888666_MethodInfo_var);
		return L_10;
	}
}
// System.Boolean ZergRush.ObservableExtension::<EveryTap>m__1A1(System.Int64)
extern Il2CppClass* Input_t1593691127_il2cpp_TypeInfo_var;
extern Il2CppClass* ObservableExtension_t8126852_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t1874381577_il2cpp_TypeInfo_var;
extern const MethodInfo* ObservableExtension_U3CEveryTapU3Em__1A3_m2765014839_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m880807361_MethodInfo_var;
extern const MethodInfo* Enumerable_Any_TisTouch_t1603883884_m4047459675_MethodInfo_var;
extern const uint32_t ObservableExtension_U3CEveryTapU3Em__1A1_m3919383235_MetadataUsageId;
extern "C"  bool ObservableExtension_U3CEveryTapU3Em__1A1_m3919383235 (Il2CppObject * __this /* static, unused */, int64_t ___l0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableExtension_U3CEveryTapU3Em__1A1_m3919383235_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TouchU5BU5D_t376223077* G_B7_0 = NULL;
	TouchU5BU5D_t376223077* G_B6_0 = NULL;
	{
		bool L_0 = Application_get_isMobilePlatform_m1741446941(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetMouseButtonDown_m3552475078(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		return (bool)1;
	}

IL_0017:
	{
		bool L_2 = Application_get_isMobilePlatform_m1741446941(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_005a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		int32_t L_3 = Input_get_touchCount_m1430909390(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_3) <= ((int32_t)0)))
		{
			goto IL_005a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		TouchU5BU5D_t376223077* L_4 = Input_get_touches_m300368858(NULL /*static, unused*/, /*hidden argument*/NULL);
		Func_2_t1874381577 * L_5 = ((ObservableExtension_t8126852_StaticFields*)ObservableExtension_t8126852_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache2_2();
		G_B6_0 = L_4;
		if (L_5)
		{
			G_B7_0 = L_4;
			goto IL_0049;
		}
	}
	{
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)ObservableExtension_U3CEveryTapU3Em__1A3_m2765014839_MethodInfo_var);
		Func_2_t1874381577 * L_7 = (Func_2_t1874381577 *)il2cpp_codegen_object_new(Func_2_t1874381577_il2cpp_TypeInfo_var);
		Func_2__ctor_m880807361(L_7, NULL, L_6, /*hidden argument*/Func_2__ctor_m880807361_MethodInfo_var);
		((ObservableExtension_t8126852_StaticFields*)ObservableExtension_t8126852_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache2_2(L_7);
		G_B7_0 = G_B6_0;
	}

IL_0049:
	{
		Func_2_t1874381577 * L_8 = ((ObservableExtension_t8126852_StaticFields*)ObservableExtension_t8126852_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache2_2();
		bool L_9 = Enumerable_Any_TisTouch_t1603883884_m4047459675(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)G_B7_0, L_8, /*hidden argument*/Enumerable_Any_TisTouch_t1603883884_m4047459675_MethodInfo_var);
		if (!L_9)
		{
			goto IL_005a;
		}
	}
	{
		return (bool)1;
	}

IL_005a:
	{
		return (bool)0;
	}
}
// UnityEngine.Vector2 ZergRush.ObservableExtension::<EveryTap>m__1A2(System.Int64)
extern Il2CppClass* Input_t1593691127_il2cpp_TypeInfo_var;
extern Il2CppClass* ObservableExtension_t8126852_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t1874381577_il2cpp_TypeInfo_var;
extern const MethodInfo* ObservableExtension_U3CEveryTapU3Em__1A4_m2356190614_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m880807361_MethodInfo_var;
extern const MethodInfo* Enumerable_First_TisTouch_t1603883884_m2423823600_MethodInfo_var;
extern const uint32_t ObservableExtension_U3CEveryTapU3Em__1A2_m323694379_MetadataUsageId;
extern "C"  Vector2_t3525329788  ObservableExtension_U3CEveryTapU3Em__1A2_m323694379 (Il2CppObject * __this /* static, unused */, int64_t ___l0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableExtension_U3CEveryTapU3Em__1A2_m323694379_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Touch_t1603883884  V_0;
	memset(&V_0, 0, sizeof(V_0));
	TouchU5BU5D_t376223077* G_B5_0 = NULL;
	TouchU5BU5D_t376223077* G_B4_0 = NULL;
	{
		bool L_0 = Application_get_isMobilePlatform_m1741446941(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetMouseButtonDown_m3552475078(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		Vector3_t3525329789  L_2 = Input_get_mousePosition_m4020233228(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t3525329788  L_3 = Vector2_op_Implicit_m62903196(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0020:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		TouchU5BU5D_t376223077* L_4 = Input_get_touches_m300368858(NULL /*static, unused*/, /*hidden argument*/NULL);
		Func_2_t1874381577 * L_5 = ((ObservableExtension_t8126852_StaticFields*)ObservableExtension_t8126852_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache3_3();
		G_B4_0 = L_4;
		if (L_5)
		{
			G_B5_0 = L_4;
			goto IL_003d;
		}
	}
	{
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)ObservableExtension_U3CEveryTapU3Em__1A4_m2356190614_MethodInfo_var);
		Func_2_t1874381577 * L_7 = (Func_2_t1874381577 *)il2cpp_codegen_object_new(Func_2_t1874381577_il2cpp_TypeInfo_var);
		Func_2__ctor_m880807361(L_7, NULL, L_6, /*hidden argument*/Func_2__ctor_m880807361_MethodInfo_var);
		((ObservableExtension_t8126852_StaticFields*)ObservableExtension_t8126852_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache3_3(L_7);
		G_B5_0 = G_B4_0;
	}

IL_003d:
	{
		Func_2_t1874381577 * L_8 = ((ObservableExtension_t8126852_StaticFields*)ObservableExtension_t8126852_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache3_3();
		Touch_t1603883884  L_9 = Enumerable_First_TisTouch_t1603883884_m2423823600(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)G_B5_0, L_8, /*hidden argument*/Enumerable_First_TisTouch_t1603883884_m2423823600_MethodInfo_var);
		V_0 = L_9;
		Vector2_t3525329788  L_10 = Touch_get_position_m1943849441((&V_0), /*hidden argument*/NULL);
		return L_10;
	}
}
// System.Boolean ZergRush.ObservableExtension::<EveryTap>m__1A3(UnityEngine.Touch)
extern "C"  bool ObservableExtension_U3CEveryTapU3Em__1A3_m2765014839 (Il2CppObject * __this /* static, unused */, Touch_t1603883884  ___touch0, const MethodInfo* method)
{
	{
		int32_t L_0 = Touch_get_phase_m3314549414((&___touch0), /*hidden argument*/NULL);
		return (bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean ZergRush.ObservableExtension::<EveryTap>m__1A4(UnityEngine.Touch)
extern "C"  bool ObservableExtension_U3CEveryTapU3Em__1A4_m2356190614 (Il2CppObject * __this /* static, unused */, Touch_t1603883884  ___touch0, const MethodInfo* method)
{
	{
		int32_t L_0 = Touch_get_phase_m3314549414((&___touch0), /*hidden argument*/NULL);
		return (bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void ZergRush.ObservableExtension/<EveryTapOutsideRectTransform>c__AnonStorey11B::.ctor()
extern "C"  void U3CEveryTapOutsideRectTransformU3Ec__AnonStorey11B__ctor_m3026690416 (U3CEveryTapOutsideRectTransformU3Ec__AnonStorey11B_t2782190866 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean ZergRush.ObservableExtension/<EveryTapOutsideRectTransform>c__AnonStorey11B::<>m__1A0(UnityEngine.Vector2)
extern Il2CppClass* RectTransformUtility_t2895919825_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentInParent_TisCanvas_t3534013893_m1800982117_MethodInfo_var;
extern const uint32_t U3CEveryTapOutsideRectTransformU3Ec__AnonStorey11B_U3CU3Em__1A0_m1024781829_MetadataUsageId;
extern "C"  bool U3CEveryTapOutsideRectTransformU3Ec__AnonStorey11B_U3CU3Em__1A0_m1024781829 (U3CEveryTapOutsideRectTransformU3Ec__AnonStorey11B_t2782190866 * __this, Vector2_t3525329788  ___position0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CEveryTapOutsideRectTransformU3Ec__AnonStorey11B_U3CU3Em__1A0_m1024781829_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Camera_t3533968274 * V_0 = NULL;
	Vector2_t3525329788  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Rect_t1525428817  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		RectTransform_t3317474837 * L_0 = __this->get_rectTransform_0();
		NullCheck(L_0);
		Canvas_t3534013893 * L_1 = Component_GetComponentInParent_TisCanvas_t3534013893_m1800982117(L_0, /*hidden argument*/Component_GetComponentInParent_TisCanvas_t3534013893_m1800982117_MethodInfo_var);
		NullCheck(L_1);
		Camera_t3533968274 * L_2 = Canvas_get_worldCamera_m2621449230(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector2_t3525329788  L_3 = Vector2_get_zero_m199872368(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_3;
		RectTransform_t3317474837 * L_4 = __this->get_rectTransform_0();
		Vector2_t3525329788  L_5 = ___position0;
		Camera_t3533968274 * L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t2895919825_il2cpp_TypeInfo_var);
		RectTransformUtility_ScreenPointToLocalPointInRectangle_m666650172(NULL /*static, unused*/, L_4, L_5, L_6, (&V_1), /*hidden argument*/NULL);
		RectTransform_t3317474837 * L_7 = __this->get_rectTransform_0();
		NullCheck(L_7);
		Rect_t1525428817  L_8 = RectTransform_get_rect_m1566017036(L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		Vector2_t3525329788  L_9 = V_1;
		bool L_10 = Rect_Contains_m3556594010((&V_2), L_9, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_10) == ((int32_t)0))? 1 : 0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
