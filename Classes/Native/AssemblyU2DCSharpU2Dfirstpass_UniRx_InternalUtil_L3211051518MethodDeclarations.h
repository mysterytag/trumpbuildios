﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.ListObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>
struct ListObserver_1_t3211051518;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>>
struct ImmutableList_1_t1893669953;
// System.Exception
struct Exception_t1967233988;
// UniRx.IObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>
struct IObserver_1_t833465454;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionMove2916433847.h"

// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m4115048323_gshared (ListObserver_1_t3211051518 * __this, ImmutableList_1_t1893669953 * ___observers0, const MethodInfo* method);
#define ListObserver_1__ctor_m4115048323(__this, ___observers0, method) ((  void (*) (ListObserver_1_t3211051518 *, ImmutableList_1_t1893669953 *, const MethodInfo*))ListObserver_1__ctor_m4115048323_gshared)(__this, ___observers0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m797640827_gshared (ListObserver_1_t3211051518 * __this, const MethodInfo* method);
#define ListObserver_1_OnCompleted_m797640827(__this, method) ((  void (*) (ListObserver_1_t3211051518 *, const MethodInfo*))ListObserver_1_OnCompleted_m797640827_gshared)(__this, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m81647912_gshared (ListObserver_1_t3211051518 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ListObserver_1_OnError_m81647912(__this, ___error0, method) ((  void (*) (ListObserver_1_t3211051518 *, Exception_t1967233988 *, const MethodInfo*))ListObserver_1_OnError_m81647912_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m1092816921_gshared (ListObserver_1_t3211051518 * __this, CollectionMoveEvent_1_t2916433847  ___value0, const MethodInfo* method);
#define ListObserver_1_OnNext_m1092816921(__this, ___value0, method) ((  void (*) (ListObserver_1_t3211051518 *, CollectionMoveEvent_1_t2916433847 , const MethodInfo*))ListObserver_1_OnNext_m1092816921_gshared)(__this, ___value0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m2256284857_gshared (ListObserver_1_t3211051518 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Add_m2256284857(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t3211051518 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Add_m2256284857_gshared)(__this, ___observer0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m1177438194_gshared (ListObserver_1_t3211051518 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Remove_m1177438194(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t3211051518 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Remove_m1177438194_gshared)(__this, ___observer0, method)
