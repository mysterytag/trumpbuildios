﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1/Subscription<UniRx.CollectionAddEvent`1<System.Object>>
struct Subscription_t195768509;
// UniRx.Subject`1<UniRx.CollectionAddEvent`1<System.Object>>
struct Subject_1_t59589311;
// UniRx.IObserver`1<UniRx.CollectionAddEvent`1<System.Object>>
struct IObserver_1_t333553594;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Subject`1/Subscription<UniRx.CollectionAddEvent`1<System.Object>>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern "C"  void Subscription__ctor_m1851991161_gshared (Subscription_t195768509 * __this, Subject_1_t59589311 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method);
#define Subscription__ctor_m1851991161(__this, ___parent0, ___unsubscribeTarget1, method) ((  void (*) (Subscription_t195768509 *, Subject_1_t59589311 *, Il2CppObject*, const MethodInfo*))Subscription__ctor_m1851991161_gshared)(__this, ___parent0, ___unsubscribeTarget1, method)
// System.Void UniRx.Subject`1/Subscription<UniRx.CollectionAddEvent`1<System.Object>>::Dispose()
extern "C"  void Subscription_Dispose_m3028760029_gshared (Subscription_t195768509 * __this, const MethodInfo* method);
#define Subscription_Dispose_m3028760029(__this, method) ((  void (*) (Subscription_t195768509 *, const MethodInfo*))Subscription_Dispose_m3028760029_gshared)(__this, method)
