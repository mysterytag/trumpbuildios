﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Subject`1<UniRx.Unit>
struct Subject_1_t201353362;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableTriggerBase
struct  ObservableTriggerBase_t1883237895  : public MonoBehaviour_t3012272455
{
public:
	// System.Boolean UniRx.Triggers.ObservableTriggerBase::calledAwake
	bool ___calledAwake_2;
	// UniRx.Subject`1<UniRx.Unit> UniRx.Triggers.ObservableTriggerBase::awake
	Subject_1_t201353362 * ___awake_3;
	// System.Boolean UniRx.Triggers.ObservableTriggerBase::calledStart
	bool ___calledStart_4;
	// UniRx.Subject`1<UniRx.Unit> UniRx.Triggers.ObservableTriggerBase::start
	Subject_1_t201353362 * ___start_5;
	// System.Boolean UniRx.Triggers.ObservableTriggerBase::calledDestroy
	bool ___calledDestroy_6;
	// UniRx.Subject`1<UniRx.Unit> UniRx.Triggers.ObservableTriggerBase::onDestroy
	Subject_1_t201353362 * ___onDestroy_7;

public:
	inline static int32_t get_offset_of_calledAwake_2() { return static_cast<int32_t>(offsetof(ObservableTriggerBase_t1883237895, ___calledAwake_2)); }
	inline bool get_calledAwake_2() const { return ___calledAwake_2; }
	inline bool* get_address_of_calledAwake_2() { return &___calledAwake_2; }
	inline void set_calledAwake_2(bool value)
	{
		___calledAwake_2 = value;
	}

	inline static int32_t get_offset_of_awake_3() { return static_cast<int32_t>(offsetof(ObservableTriggerBase_t1883237895, ___awake_3)); }
	inline Subject_1_t201353362 * get_awake_3() const { return ___awake_3; }
	inline Subject_1_t201353362 ** get_address_of_awake_3() { return &___awake_3; }
	inline void set_awake_3(Subject_1_t201353362 * value)
	{
		___awake_3 = value;
		Il2CppCodeGenWriteBarrier(&___awake_3, value);
	}

	inline static int32_t get_offset_of_calledStart_4() { return static_cast<int32_t>(offsetof(ObservableTriggerBase_t1883237895, ___calledStart_4)); }
	inline bool get_calledStart_4() const { return ___calledStart_4; }
	inline bool* get_address_of_calledStart_4() { return &___calledStart_4; }
	inline void set_calledStart_4(bool value)
	{
		___calledStart_4 = value;
	}

	inline static int32_t get_offset_of_start_5() { return static_cast<int32_t>(offsetof(ObservableTriggerBase_t1883237895, ___start_5)); }
	inline Subject_1_t201353362 * get_start_5() const { return ___start_5; }
	inline Subject_1_t201353362 ** get_address_of_start_5() { return &___start_5; }
	inline void set_start_5(Subject_1_t201353362 * value)
	{
		___start_5 = value;
		Il2CppCodeGenWriteBarrier(&___start_5, value);
	}

	inline static int32_t get_offset_of_calledDestroy_6() { return static_cast<int32_t>(offsetof(ObservableTriggerBase_t1883237895, ___calledDestroy_6)); }
	inline bool get_calledDestroy_6() const { return ___calledDestroy_6; }
	inline bool* get_address_of_calledDestroy_6() { return &___calledDestroy_6; }
	inline void set_calledDestroy_6(bool value)
	{
		___calledDestroy_6 = value;
	}

	inline static int32_t get_offset_of_onDestroy_7() { return static_cast<int32_t>(offsetof(ObservableTriggerBase_t1883237895, ___onDestroy_7)); }
	inline Subject_1_t201353362 * get_onDestroy_7() const { return ___onDestroy_7; }
	inline Subject_1_t201353362 ** get_address_of_onDestroy_7() { return &___onDestroy_7; }
	inline void set_onDestroy_7(Subject_1_t201353362 * value)
	{
		___onDestroy_7 = value;
		Il2CppCodeGenWriteBarrier(&___onDestroy_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
