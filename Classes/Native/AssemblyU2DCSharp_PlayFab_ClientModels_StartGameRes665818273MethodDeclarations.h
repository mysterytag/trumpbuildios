﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.StartGameResult
struct StartGameResult_t665818273;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen1438485399.h"

// System.Void PlayFab.ClientModels.StartGameResult::.ctor()
extern "C"  void StartGameResult__ctor_m1088866344 (StartGameResult_t665818273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.StartGameResult::get_LobbyID()
extern "C"  String_t* StartGameResult_get_LobbyID_m201866251 (StartGameResult_t665818273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.StartGameResult::set_LobbyID(System.String)
extern "C"  void StartGameResult_set_LobbyID_m2501857320 (StartGameResult_t665818273 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.StartGameResult::get_ServerHostname()
extern "C"  String_t* StartGameResult_get_ServerHostname_m1882609182 (StartGameResult_t665818273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.StartGameResult::set_ServerHostname(System.String)
extern "C"  void StartGameResult_set_ServerHostname_m2308290867 (StartGameResult_t665818273 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> PlayFab.ClientModels.StartGameResult::get_ServerPort()
extern "C"  Nullable_1_t1438485399  StartGameResult_get_ServerPort_m3896089016 (StartGameResult_t665818273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.StartGameResult::set_ServerPort(System.Nullable`1<System.Int32>)
extern "C"  void StartGameResult_set_ServerPort_m4231781937 (StartGameResult_t665818273 * __this, Nullable_1_t1438485399  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.StartGameResult::get_Ticket()
extern "C"  String_t* StartGameResult_get_Ticket_m901707284 (StartGameResult_t665818273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.StartGameResult::set_Ticket(System.String)
extern "C"  void StartGameResult_set_Ticket_m2971351037 (StartGameResult_t665818273 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.StartGameResult::get_Expires()
extern "C"  String_t* StartGameResult_get_Expires_m2504505102 (StartGameResult_t665818273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.StartGameResult::set_Expires(System.String)
extern "C"  void StartGameResult_set_Expires_m2142835205 (StartGameResult_t665818273 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.StartGameResult::get_Password()
extern "C"  String_t* StartGameResult_get_Password_m2456320035 (StartGameResult_t665818273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.StartGameResult::set_Password(System.String)
extern "C"  void StartGameResult_set_Password_m3060424334 (StartGameResult_t665818273 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
