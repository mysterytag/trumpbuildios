﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.CombineLatestFunc`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct CombineLatestFunc_8_t584396980;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void UniRx.Operators.CombineLatestFunc`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void CombineLatestFunc_8__ctor_m2039772742_gshared (CombineLatestFunc_8_t584396980 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define CombineLatestFunc_8__ctor_m2039772742(__this, ___object0, ___method1, method) ((  void (*) (CombineLatestFunc_8_t584396980 *, Il2CppObject *, IntPtr_t, const MethodInfo*))CombineLatestFunc_8__ctor_m2039772742_gshared)(__this, ___object0, ___method1, method)
// TR UniRx.Operators.CombineLatestFunc`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Invoke(T1,T2,T3,T4,T5,T6,T7)
extern "C"  Il2CppObject * CombineLatestFunc_8_Invoke_m950119207_gshared (CombineLatestFunc_8_t584396980 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, Il2CppObject * ___arg65, Il2CppObject * ___arg76, const MethodInfo* method);
#define CombineLatestFunc_8_Invoke_m950119207(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76, method) ((  Il2CppObject * (*) (CombineLatestFunc_8_t584396980 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))CombineLatestFunc_8_Invoke_m950119207_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76, method)
// System.IAsyncResult UniRx.Operators.CombineLatestFunc`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,T3,T4,T5,T6,T7,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * CombineLatestFunc_8_BeginInvoke_m2895725499_gshared (CombineLatestFunc_8_t584396980 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, Il2CppObject * ___arg65, Il2CppObject * ___arg76, AsyncCallback_t1363551830 * ___callback7, Il2CppObject * ___object8, const MethodInfo* method);
#define CombineLatestFunc_8_BeginInvoke_m2895725499(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76, ___callback7, ___object8, method) ((  Il2CppObject * (*) (CombineLatestFunc_8_t584396980 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))CombineLatestFunc_8_BeginInvoke_m2895725499_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76, ___callback7, ___object8, method)
// TR UniRx.Operators.CombineLatestFunc`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * CombineLatestFunc_8_EndInvoke_m1310280829_gshared (CombineLatestFunc_8_t584396980 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define CombineLatestFunc_8_EndInvoke_m1310280829(__this, ___result0, method) ((  Il2CppObject * (*) (CombineLatestFunc_8_t584396980 *, Il2CppObject *, const MethodInfo*))CombineLatestFunc_8_EndInvoke_m1310280829_gshared)(__this, ___result0, method)
