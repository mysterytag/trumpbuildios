﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IProgress`1<System.Single>
struct IProgress_1_t4173802291;
// System.Object
struct Il2CppObject;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.Exception
struct Exception_t1967233988;

#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.AsyncOperationExtensions/<AsObservableCore>c__Iterator1E`1<System.Object>
struct  U3CAsObservableCoreU3Ec__Iterator1E_1_t2586538625  : public Il2CppObject
{
public:
	// UniRx.IProgress`1<System.Single> UniRx.AsyncOperationExtensions/<AsObservableCore>c__Iterator1E`1::reportProgress
	Il2CppObject* ___reportProgress_0;
	// T UniRx.AsyncOperationExtensions/<AsObservableCore>c__Iterator1E`1::asyncOperation
	Il2CppObject * ___asyncOperation_1;
	// UniRx.CancellationToken UniRx.AsyncOperationExtensions/<AsObservableCore>c__Iterator1E`1::cancel
	CancellationToken_t1439151560  ___cancel_2;
	// UniRx.IObserver`1<T> UniRx.AsyncOperationExtensions/<AsObservableCore>c__Iterator1E`1::observer
	Il2CppObject* ___observer_3;
	// System.Exception UniRx.AsyncOperationExtensions/<AsObservableCore>c__Iterator1E`1::<ex>__0
	Exception_t1967233988 * ___U3CexU3E__0_4;
	// System.Exception UniRx.AsyncOperationExtensions/<AsObservableCore>c__Iterator1E`1::<ex>__1
	Exception_t1967233988 * ___U3CexU3E__1_5;
	// System.Int32 UniRx.AsyncOperationExtensions/<AsObservableCore>c__Iterator1E`1::$PC
	int32_t ___U24PC_6;
	// System.Object UniRx.AsyncOperationExtensions/<AsObservableCore>c__Iterator1E`1::$current
	Il2CppObject * ___U24current_7;
	// UniRx.IProgress`1<System.Single> UniRx.AsyncOperationExtensions/<AsObservableCore>c__Iterator1E`1::<$>reportProgress
	Il2CppObject* ___U3CU24U3EreportProgress_8;
	// T UniRx.AsyncOperationExtensions/<AsObservableCore>c__Iterator1E`1::<$>asyncOperation
	Il2CppObject * ___U3CU24U3EasyncOperation_9;
	// UniRx.CancellationToken UniRx.AsyncOperationExtensions/<AsObservableCore>c__Iterator1E`1::<$>cancel
	CancellationToken_t1439151560  ___U3CU24U3Ecancel_10;
	// UniRx.IObserver`1<T> UniRx.AsyncOperationExtensions/<AsObservableCore>c__Iterator1E`1::<$>observer
	Il2CppObject* ___U3CU24U3Eobserver_11;

public:
	inline static int32_t get_offset_of_reportProgress_0() { return static_cast<int32_t>(offsetof(U3CAsObservableCoreU3Ec__Iterator1E_1_t2586538625, ___reportProgress_0)); }
	inline Il2CppObject* get_reportProgress_0() const { return ___reportProgress_0; }
	inline Il2CppObject** get_address_of_reportProgress_0() { return &___reportProgress_0; }
	inline void set_reportProgress_0(Il2CppObject* value)
	{
		___reportProgress_0 = value;
		Il2CppCodeGenWriteBarrier(&___reportProgress_0, value);
	}

	inline static int32_t get_offset_of_asyncOperation_1() { return static_cast<int32_t>(offsetof(U3CAsObservableCoreU3Ec__Iterator1E_1_t2586538625, ___asyncOperation_1)); }
	inline Il2CppObject * get_asyncOperation_1() const { return ___asyncOperation_1; }
	inline Il2CppObject ** get_address_of_asyncOperation_1() { return &___asyncOperation_1; }
	inline void set_asyncOperation_1(Il2CppObject * value)
	{
		___asyncOperation_1 = value;
		Il2CppCodeGenWriteBarrier(&___asyncOperation_1, value);
	}

	inline static int32_t get_offset_of_cancel_2() { return static_cast<int32_t>(offsetof(U3CAsObservableCoreU3Ec__Iterator1E_1_t2586538625, ___cancel_2)); }
	inline CancellationToken_t1439151560  get_cancel_2() const { return ___cancel_2; }
	inline CancellationToken_t1439151560 * get_address_of_cancel_2() { return &___cancel_2; }
	inline void set_cancel_2(CancellationToken_t1439151560  value)
	{
		___cancel_2 = value;
	}

	inline static int32_t get_offset_of_observer_3() { return static_cast<int32_t>(offsetof(U3CAsObservableCoreU3Ec__Iterator1E_1_t2586538625, ___observer_3)); }
	inline Il2CppObject* get_observer_3() const { return ___observer_3; }
	inline Il2CppObject** get_address_of_observer_3() { return &___observer_3; }
	inline void set_observer_3(Il2CppObject* value)
	{
		___observer_3 = value;
		Il2CppCodeGenWriteBarrier(&___observer_3, value);
	}

	inline static int32_t get_offset_of_U3CexU3E__0_4() { return static_cast<int32_t>(offsetof(U3CAsObservableCoreU3Ec__Iterator1E_1_t2586538625, ___U3CexU3E__0_4)); }
	inline Exception_t1967233988 * get_U3CexU3E__0_4() const { return ___U3CexU3E__0_4; }
	inline Exception_t1967233988 ** get_address_of_U3CexU3E__0_4() { return &___U3CexU3E__0_4; }
	inline void set_U3CexU3E__0_4(Exception_t1967233988 * value)
	{
		___U3CexU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CexU3E__0_4, value);
	}

	inline static int32_t get_offset_of_U3CexU3E__1_5() { return static_cast<int32_t>(offsetof(U3CAsObservableCoreU3Ec__Iterator1E_1_t2586538625, ___U3CexU3E__1_5)); }
	inline Exception_t1967233988 * get_U3CexU3E__1_5() const { return ___U3CexU3E__1_5; }
	inline Exception_t1967233988 ** get_address_of_U3CexU3E__1_5() { return &___U3CexU3E__1_5; }
	inline void set_U3CexU3E__1_5(Exception_t1967233988 * value)
	{
		___U3CexU3E__1_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CexU3E__1_5, value);
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CAsObservableCoreU3Ec__Iterator1E_1_t2586538625, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CAsObservableCoreU3Ec__Iterator1E_1_t2586538625, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EreportProgress_8() { return static_cast<int32_t>(offsetof(U3CAsObservableCoreU3Ec__Iterator1E_1_t2586538625, ___U3CU24U3EreportProgress_8)); }
	inline Il2CppObject* get_U3CU24U3EreportProgress_8() const { return ___U3CU24U3EreportProgress_8; }
	inline Il2CppObject** get_address_of_U3CU24U3EreportProgress_8() { return &___U3CU24U3EreportProgress_8; }
	inline void set_U3CU24U3EreportProgress_8(Il2CppObject* value)
	{
		___U3CU24U3EreportProgress_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3EreportProgress_8, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EasyncOperation_9() { return static_cast<int32_t>(offsetof(U3CAsObservableCoreU3Ec__Iterator1E_1_t2586538625, ___U3CU24U3EasyncOperation_9)); }
	inline Il2CppObject * get_U3CU24U3EasyncOperation_9() const { return ___U3CU24U3EasyncOperation_9; }
	inline Il2CppObject ** get_address_of_U3CU24U3EasyncOperation_9() { return &___U3CU24U3EasyncOperation_9; }
	inline void set_U3CU24U3EasyncOperation_9(Il2CppObject * value)
	{
		___U3CU24U3EasyncOperation_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3EasyncOperation_9, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Ecancel_10() { return static_cast<int32_t>(offsetof(U3CAsObservableCoreU3Ec__Iterator1E_1_t2586538625, ___U3CU24U3Ecancel_10)); }
	inline CancellationToken_t1439151560  get_U3CU24U3Ecancel_10() const { return ___U3CU24U3Ecancel_10; }
	inline CancellationToken_t1439151560 * get_address_of_U3CU24U3Ecancel_10() { return &___U3CU24U3Ecancel_10; }
	inline void set_U3CU24U3Ecancel_10(CancellationToken_t1439151560  value)
	{
		___U3CU24U3Ecancel_10 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Eobserver_11() { return static_cast<int32_t>(offsetof(U3CAsObservableCoreU3Ec__Iterator1E_1_t2586538625, ___U3CU24U3Eobserver_11)); }
	inline Il2CppObject* get_U3CU24U3Eobserver_11() const { return ___U3CU24U3Eobserver_11; }
	inline Il2CppObject** get_address_of_U3CU24U3Eobserver_11() { return &___U3CU24U3Eobserver_11; }
	inline void set_U3CU24U3Eobserver_11(Il2CppObject* value)
	{
		___U3CU24U3Eobserver_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eobserver_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
