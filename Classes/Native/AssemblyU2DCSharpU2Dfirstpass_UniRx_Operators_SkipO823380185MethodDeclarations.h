﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SkipObservable`1<System.Object>
struct SkipObservable_1_t823380185;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// UniRx.IScheduler
struct IScheduler_t2938318244;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_TimeSpan763862892.h"

// System.Void UniRx.Operators.SkipObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Int32)
extern "C"  void SkipObservable_1__ctor_m3269748147_gshared (SkipObservable_1_t823380185 * __this, Il2CppObject* ___source0, int32_t ___count1, const MethodInfo* method);
#define SkipObservable_1__ctor_m3269748147(__this, ___source0, ___count1, method) ((  void (*) (SkipObservable_1_t823380185 *, Il2CppObject*, int32_t, const MethodInfo*))SkipObservable_1__ctor_m3269748147_gshared)(__this, ___source0, ___count1, method)
// System.Void UniRx.Operators.SkipObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.TimeSpan,UniRx.IScheduler)
extern "C"  void SkipObservable_1__ctor_m1200294114_gshared (SkipObservable_1_t823380185 * __this, Il2CppObject* ___source0, TimeSpan_t763862892  ___duration1, Il2CppObject * ___scheduler2, const MethodInfo* method);
#define SkipObservable_1__ctor_m1200294114(__this, ___source0, ___duration1, ___scheduler2, method) ((  void (*) (SkipObservable_1_t823380185 *, Il2CppObject*, TimeSpan_t763862892 , Il2CppObject *, const MethodInfo*))SkipObservable_1__ctor_m1200294114_gshared)(__this, ___source0, ___duration1, ___scheduler2, method)
// UniRx.IObservable`1<T> UniRx.Operators.SkipObservable`1<System.Object>::Combine(System.Int32)
extern "C"  Il2CppObject* SkipObservable_1_Combine_m398784247_gshared (SkipObservable_1_t823380185 * __this, int32_t ___count0, const MethodInfo* method);
#define SkipObservable_1_Combine_m398784247(__this, ___count0, method) ((  Il2CppObject* (*) (SkipObservable_1_t823380185 *, int32_t, const MethodInfo*))SkipObservable_1_Combine_m398784247_gshared)(__this, ___count0, method)
// UniRx.IObservable`1<T> UniRx.Operators.SkipObservable`1<System.Object>::Combine(System.TimeSpan)
extern "C"  Il2CppObject* SkipObservable_1_Combine_m1317269046_gshared (SkipObservable_1_t823380185 * __this, TimeSpan_t763862892  ___duration0, const MethodInfo* method);
#define SkipObservable_1_Combine_m1317269046(__this, ___duration0, method) ((  Il2CppObject* (*) (SkipObservable_1_t823380185 *, TimeSpan_t763862892 , const MethodInfo*))SkipObservable_1_Combine_m1317269046_gshared)(__this, ___duration0, method)
// System.IDisposable UniRx.Operators.SkipObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * SkipObservable_1_SubscribeCore_m1236464419_gshared (SkipObservable_1_t823380185 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define SkipObservable_1_SubscribeCore_m1236464419(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (SkipObservable_1_t823380185 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SkipObservable_1_SubscribeCore_m1236464419_gshared)(__this, ___observer0, ___cancel1, method)
