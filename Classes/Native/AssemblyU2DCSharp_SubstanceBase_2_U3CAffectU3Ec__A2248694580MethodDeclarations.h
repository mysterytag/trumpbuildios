﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SubstanceBase`2/<Affect>c__AnonStorey144<System.Double,System.Double>
struct U3CAffectU3Ec__AnonStorey144_t2248694580;

#include "codegen/il2cpp-codegen.h"

// System.Void SubstanceBase`2/<Affect>c__AnonStorey144<System.Double,System.Double>::.ctor()
extern "C"  void U3CAffectU3Ec__AnonStorey144__ctor_m872203608_gshared (U3CAffectU3Ec__AnonStorey144_t2248694580 * __this, const MethodInfo* method);
#define U3CAffectU3Ec__AnonStorey144__ctor_m872203608(__this, method) ((  void (*) (U3CAffectU3Ec__AnonStorey144_t2248694580 *, const MethodInfo*))U3CAffectU3Ec__AnonStorey144__ctor_m872203608_gshared)(__this, method)
// System.Void SubstanceBase`2/<Affect>c__AnonStorey144<System.Double,System.Double>::<>m__1DA()
extern "C"  void U3CAffectU3Ec__AnonStorey144_U3CU3Em__1DA_m2095927503_gshared (U3CAffectU3Ec__AnonStorey144_t2248694580 * __this, const MethodInfo* method);
#define U3CAffectU3Ec__AnonStorey144_U3CU3Em__1DA_m2095927503(__this, method) ((  void (*) (U3CAffectU3Ec__AnonStorey144_t2248694580 *, const MethodInfo*))U3CAffectU3Ec__AnonStorey144_U3CU3Em__1DA_m2095927503_gshared)(__this, method)
