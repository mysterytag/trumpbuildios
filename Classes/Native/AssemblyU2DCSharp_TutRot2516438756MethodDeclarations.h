﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TutRot
struct TutRot_t2516438756;

#include "codegen/il2cpp-codegen.h"

// System.Void TutRot::.ctor()
extern "C"  void TutRot__ctor_m3445233207 (TutRot_t2516438756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutRot::Start()
extern "C"  void TutRot_Start_m2392370999 (TutRot_t2516438756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutRot::Update()
extern "C"  void TutRot_Update_m1154909110 (TutRot_t2516438756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
