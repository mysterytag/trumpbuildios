﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIHidder/<WatchMultiplier>c__Iterator27
struct U3CWatchMultiplierU3Ec__Iterator27_t471154860;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UIHidder/<WatchMultiplier>c__Iterator27::.ctor()
extern "C"  void U3CWatchMultiplierU3Ec__Iterator27__ctor_m3541259410 (U3CWatchMultiplierU3Ec__Iterator27_t471154860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UIHidder/<WatchMultiplier>c__Iterator27::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWatchMultiplierU3Ec__Iterator27_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2963454720 (U3CWatchMultiplierU3Ec__Iterator27_t471154860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UIHidder/<WatchMultiplier>c__Iterator27::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWatchMultiplierU3Ec__Iterator27_System_Collections_IEnumerator_get_Current_m3903504020 (U3CWatchMultiplierU3Ec__Iterator27_t471154860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIHidder/<WatchMultiplier>c__Iterator27::MoveNext()
extern "C"  bool U3CWatchMultiplierU3Ec__Iterator27_MoveNext_m267328546 (U3CWatchMultiplierU3Ec__Iterator27_t471154860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIHidder/<WatchMultiplier>c__Iterator27::Dispose()
extern "C"  void U3CWatchMultiplierU3Ec__Iterator27_Dispose_m1434477647 (U3CWatchMultiplierU3Ec__Iterator27_t471154860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIHidder/<WatchMultiplier>c__Iterator27::Reset()
extern "C"  void U3CWatchMultiplierU3Ec__Iterator27_Reset_m1187692351 (U3CWatchMultiplierU3Ec__Iterator27_t471154860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
