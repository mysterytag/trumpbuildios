﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionReplaceEvent`1<System.Object>>
struct OperatorObservableBase_1_t905895507;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.CollectionReplaceEvent`1<System.Object>>
struct IObserver_1_t4053749439;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionReplaceEvent`1<System.Object>>::.ctor(System.Boolean)
extern "C"  void OperatorObservableBase_1__ctor_m347555267_gshared (OperatorObservableBase_1_t905895507 * __this, bool ___isRequiredSubscribeOnCurrentThread0, const MethodInfo* method);
#define OperatorObservableBase_1__ctor_m347555267(__this, ___isRequiredSubscribeOnCurrentThread0, method) ((  void (*) (OperatorObservableBase_1_t905895507 *, bool, const MethodInfo*))OperatorObservableBase_1__ctor_m347555267_gshared)(__this, ___isRequiredSubscribeOnCurrentThread0, method)
// System.Boolean UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionReplaceEvent`1<System.Object>>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m2263078399_gshared (OperatorObservableBase_1_t905895507 * __this, const MethodInfo* method);
#define OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m2263078399(__this, method) ((  bool (*) (OperatorObservableBase_1_t905895507 *, const MethodInfo*))OperatorObservableBase_1_IsRequiredSubscribeOnCurrentThread_m2263078399_gshared)(__this, method)
// System.IDisposable UniRx.Operators.OperatorObservableBase`1<UniRx.CollectionReplaceEvent`1<System.Object>>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * OperatorObservableBase_1_Subscribe_m3540047905_gshared (OperatorObservableBase_1_t905895507 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define OperatorObservableBase_1_Subscribe_m3540047905(__this, ___observer0, method) ((  Il2CppObject * (*) (OperatorObservableBase_1_t905895507 *, Il2CppObject*, const MethodInfo*))OperatorObservableBase_1_Subscribe_m3540047905_gshared)(__this, ___observer0, method)
