﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Toggle
struct Toggle_t1499417981;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.UnityUIComponentExtensions/<OnValueChangedAsObservable>c__AnonStoreyA7
struct  U3COnValueChangedAsObservableU3Ec__AnonStoreyA7_t3117525746  : public Il2CppObject
{
public:
	// UnityEngine.UI.Toggle UniRx.UnityUIComponentExtensions/<OnValueChangedAsObservable>c__AnonStoreyA7::toggle
	Toggle_t1499417981 * ___toggle_0;

public:
	inline static int32_t get_offset_of_toggle_0() { return static_cast<int32_t>(offsetof(U3COnValueChangedAsObservableU3Ec__AnonStoreyA7_t3117525746, ___toggle_0)); }
	inline Toggle_t1499417981 * get_toggle_0() const { return ___toggle_0; }
	inline Toggle_t1499417981 ** get_address_of_toggle_0() { return &___toggle_0; }
	inline void set_toggle_0(Toggle_t1499417981 * value)
	{
		___toggle_0 = value;
		Il2CppCodeGenWriteBarrier(&___toggle_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
