﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GameAnalyticsSDK_GA_Resource_GAR3683864752.h"
#include "mscorlib_System_String968488902.h"

// System.Void GameAnalyticsSDK.GA_Resource::NewEvent(GameAnalyticsSDK.GA_Resource/GAResourceFlowType,System.String,System.Single,System.String,System.String)
extern "C"  void GA_Resource_NewEvent_m3244339177 (Il2CppObject * __this /* static, unused */, int32_t ___flowType0, String_t* ___currency1, float ___amount2, String_t* ___itemType3, String_t* ___itemId4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
