﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.FactoryNested`9<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct FactoryNested_9_t2228482800;
// Zenject.IFactory`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct IFactory_8_t3095311417;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void Zenject.FactoryNested`9<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(Zenject.IFactory`8<TParam1,TParam2,TParam3,TParam4,TParam5,TParam6,TParam7,TConcrete>)
extern "C"  void FactoryNested_9__ctor_m3137840232_gshared (FactoryNested_9_t2228482800 * __this, Il2CppObject* ___concreteFactory0, const MethodInfo* method);
#define FactoryNested_9__ctor_m3137840232(__this, ___concreteFactory0, method) ((  void (*) (FactoryNested_9_t2228482800 *, Il2CppObject*, const MethodInfo*))FactoryNested_9__ctor_m3137840232_gshared)(__this, ___concreteFactory0, method)
// TContract Zenject.FactoryNested`9<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Create(TParam1,TParam2,TParam3,TParam4,TParam5,TParam6,TParam7)
extern "C"  Il2CppObject * FactoryNested_9_Create_m2920034746_gshared (FactoryNested_9_t2228482800 * __this, Il2CppObject * ___param10, Il2CppObject * ___param21, Il2CppObject * ___param32, Il2CppObject * ___param43, Il2CppObject * ___param54, Il2CppObject * ___param65, Il2CppObject * ___param76, const MethodInfo* method);
#define FactoryNested_9_Create_m2920034746(__this, ___param10, ___param21, ___param32, ___param43, ___param54, ___param65, ___param76, method) ((  Il2CppObject * (*) (FactoryNested_9_t2228482800 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))FactoryNested_9_Create_m2920034746_gshared)(__this, ___param10, ___param21, ___param32, ___param43, ___param54, ___param65, ___param76, method)
