﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_Region1621303076.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.StartGameRequest
struct  StartGameRequest_t2370915499  : public Il2CppObject
{
public:
	// System.String PlayFab.ClientModels.StartGameRequest::<BuildVersion>k__BackingField
	String_t* ___U3CBuildVersionU3Ek__BackingField_0;
	// PlayFab.ClientModels.Region PlayFab.ClientModels.StartGameRequest::<Region>k__BackingField
	int32_t ___U3CRegionU3Ek__BackingField_1;
	// System.String PlayFab.ClientModels.StartGameRequest::<GameMode>k__BackingField
	String_t* ___U3CGameModeU3Ek__BackingField_2;
	// System.String PlayFab.ClientModels.StartGameRequest::<StatisticName>k__BackingField
	String_t* ___U3CStatisticNameU3Ek__BackingField_3;
	// System.String PlayFab.ClientModels.StartGameRequest::<CharacterId>k__BackingField
	String_t* ___U3CCharacterIdU3Ek__BackingField_4;
	// System.String PlayFab.ClientModels.StartGameRequest::<CustomCommandLineData>k__BackingField
	String_t* ___U3CCustomCommandLineDataU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CBuildVersionU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StartGameRequest_t2370915499, ___U3CBuildVersionU3Ek__BackingField_0)); }
	inline String_t* get_U3CBuildVersionU3Ek__BackingField_0() const { return ___U3CBuildVersionU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CBuildVersionU3Ek__BackingField_0() { return &___U3CBuildVersionU3Ek__BackingField_0; }
	inline void set_U3CBuildVersionU3Ek__BackingField_0(String_t* value)
	{
		___U3CBuildVersionU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CBuildVersionU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CRegionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(StartGameRequest_t2370915499, ___U3CRegionU3Ek__BackingField_1)); }
	inline int32_t get_U3CRegionU3Ek__BackingField_1() const { return ___U3CRegionU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CRegionU3Ek__BackingField_1() { return &___U3CRegionU3Ek__BackingField_1; }
	inline void set_U3CRegionU3Ek__BackingField_1(int32_t value)
	{
		___U3CRegionU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CGameModeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(StartGameRequest_t2370915499, ___U3CGameModeU3Ek__BackingField_2)); }
	inline String_t* get_U3CGameModeU3Ek__BackingField_2() const { return ___U3CGameModeU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CGameModeU3Ek__BackingField_2() { return &___U3CGameModeU3Ek__BackingField_2; }
	inline void set_U3CGameModeU3Ek__BackingField_2(String_t* value)
	{
		___U3CGameModeU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CGameModeU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CStatisticNameU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(StartGameRequest_t2370915499, ___U3CStatisticNameU3Ek__BackingField_3)); }
	inline String_t* get_U3CStatisticNameU3Ek__BackingField_3() const { return ___U3CStatisticNameU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CStatisticNameU3Ek__BackingField_3() { return &___U3CStatisticNameU3Ek__BackingField_3; }
	inline void set_U3CStatisticNameU3Ek__BackingField_3(String_t* value)
	{
		___U3CStatisticNameU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CStatisticNameU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CCharacterIdU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(StartGameRequest_t2370915499, ___U3CCharacterIdU3Ek__BackingField_4)); }
	inline String_t* get_U3CCharacterIdU3Ek__BackingField_4() const { return ___U3CCharacterIdU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CCharacterIdU3Ek__BackingField_4() { return &___U3CCharacterIdU3Ek__BackingField_4; }
	inline void set_U3CCharacterIdU3Ek__BackingField_4(String_t* value)
	{
		___U3CCharacterIdU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCharacterIdU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CCustomCommandLineDataU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(StartGameRequest_t2370915499, ___U3CCustomCommandLineDataU3Ek__BackingField_5)); }
	inline String_t* get_U3CCustomCommandLineDataU3Ek__BackingField_5() const { return ___U3CCustomCommandLineDataU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CCustomCommandLineDataU3Ek__BackingField_5() { return &___U3CCustomCommandLineDataU3Ek__BackingField_5; }
	inline void set_U3CCustomCommandLineDataU3Ek__BackingField_5(String_t* value)
	{
		___U3CCustomCommandLineDataU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCustomCommandLineDataU3Ek__BackingField_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
