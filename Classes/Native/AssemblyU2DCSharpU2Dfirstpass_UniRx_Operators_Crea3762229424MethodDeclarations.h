﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.CreateObservable`1<UniRx.Unit>
struct CreateObservable_1_t3762229424;
// System.Func`2<UniRx.IObserver`1<UniRx.Unit>,System.IDisposable>
struct Func_2_t3876191685;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.CreateObservable`1<UniRx.Unit>::.ctor(System.Func`2<UniRx.IObserver`1<T>,System.IDisposable>)
extern "C"  void CreateObservable_1__ctor_m1854901285_gshared (CreateObservable_1_t3762229424 * __this, Func_2_t3876191685 * ___subscribe0, const MethodInfo* method);
#define CreateObservable_1__ctor_m1854901285(__this, ___subscribe0, method) ((  void (*) (CreateObservable_1_t3762229424 *, Func_2_t3876191685 *, const MethodInfo*))CreateObservable_1__ctor_m1854901285_gshared)(__this, ___subscribe0, method)
// System.Void UniRx.Operators.CreateObservable`1<UniRx.Unit>::.ctor(System.Func`2<UniRx.IObserver`1<T>,System.IDisposable>,System.Boolean)
extern "C"  void CreateObservable_1__ctor_m742152376_gshared (CreateObservable_1_t3762229424 * __this, Func_2_t3876191685 * ___subscribe0, bool ___isRequiredSubscribeOnCurrentThread1, const MethodInfo* method);
#define CreateObservable_1__ctor_m742152376(__this, ___subscribe0, ___isRequiredSubscribeOnCurrentThread1, method) ((  void (*) (CreateObservable_1_t3762229424 *, Func_2_t3876191685 *, bool, const MethodInfo*))CreateObservable_1__ctor_m742152376_gshared)(__this, ___subscribe0, ___isRequiredSubscribeOnCurrentThread1, method)
// System.IDisposable UniRx.Operators.CreateObservable`1<UniRx.Unit>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * CreateObservable_1_SubscribeCore_m1750972476_gshared (CreateObservable_1_t3762229424 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define CreateObservable_1_SubscribeCore_m1750972476(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (CreateObservable_1_t3762229424 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))CreateObservable_1_SubscribeCore_m1750972476_gshared)(__this, ___observer0, ___cancel1, method)
