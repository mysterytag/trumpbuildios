﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Func`2<System.Object,System.Object>[]
struct Func_2U5BU5D_t4139244137;
// UnityEngine.UI.Text
struct Text_t3286458198;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReactiveUI/<SetContent>c__AnonStorey122`1<System.Object>
struct  U3CSetContentU3Ec__AnonStorey122_1_t1018658799  : public Il2CppObject
{
public:
	// System.String ReactiveUI/<SetContent>c__AnonStorey122`1::format
	String_t* ___format_0;
	// System.Func`2<T,System.Object>[] ReactiveUI/<SetContent>c__AnonStorey122`1::parameters
	Func_2U5BU5D_t4139244137* ___parameters_1;
	// UnityEngine.UI.Text ReactiveUI/<SetContent>c__AnonStorey122`1::text
	Text_t3286458198 * ___text_2;

public:
	inline static int32_t get_offset_of_format_0() { return static_cast<int32_t>(offsetof(U3CSetContentU3Ec__AnonStorey122_1_t1018658799, ___format_0)); }
	inline String_t* get_format_0() const { return ___format_0; }
	inline String_t** get_address_of_format_0() { return &___format_0; }
	inline void set_format_0(String_t* value)
	{
		___format_0 = value;
		Il2CppCodeGenWriteBarrier(&___format_0, value);
	}

	inline static int32_t get_offset_of_parameters_1() { return static_cast<int32_t>(offsetof(U3CSetContentU3Ec__AnonStorey122_1_t1018658799, ___parameters_1)); }
	inline Func_2U5BU5D_t4139244137* get_parameters_1() const { return ___parameters_1; }
	inline Func_2U5BU5D_t4139244137** get_address_of_parameters_1() { return &___parameters_1; }
	inline void set_parameters_1(Func_2U5BU5D_t4139244137* value)
	{
		___parameters_1 = value;
		Il2CppCodeGenWriteBarrier(&___parameters_1, value);
	}

	inline static int32_t get_offset_of_text_2() { return static_cast<int32_t>(offsetof(U3CSetContentU3Ec__AnonStorey122_1_t1018658799, ___text_2)); }
	inline Text_t3286458198 * get_text_2() const { return ___text_2; }
	inline Text_t3286458198 ** get_address_of_text_2() { return &___text_2; }
	inline void set_text_2(Text_t3286458198 * value)
	{
		___text_2 = value;
		Il2CppCodeGenWriteBarrier(&___text_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
