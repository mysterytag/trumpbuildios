﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.IObservable`1<UnityEngine.Vector2>
struct IObservable_1_t3284128152;
// UnityEngine.RectTransform
struct RectTransform_t3317474837;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RectTransform3317474837.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"
#include "UnityEngine_UnityEngine_Touch1603883884.h"

// UniRx.IObservable`1<UnityEngine.Vector2> ZergRush.ObservableExtension::EveryTapOutsideRectTransform(UnityEngine.RectTransform)
extern "C"  Il2CppObject* ObservableExtension_EveryTapOutsideRectTransform_m3930871207 (Il2CppObject * __this /* static, unused */, RectTransform_t3317474837 * ___rectTransform0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.Vector2> ZergRush.ObservableExtension::EveryTap()
extern "C"  Il2CppObject* ObservableExtension_EveryTap_m2592619319 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZergRush.ObservableExtension::<EveryTap>m__1A1(System.Int64)
extern "C"  bool ObservableExtension_U3CEveryTapU3Em__1A1_m3919383235 (Il2CppObject * __this /* static, unused */, int64_t ___l0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 ZergRush.ObservableExtension::<EveryTap>m__1A2(System.Int64)
extern "C"  Vector2_t3525329788  ObservableExtension_U3CEveryTapU3Em__1A2_m323694379 (Il2CppObject * __this /* static, unused */, int64_t ___l0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZergRush.ObservableExtension::<EveryTap>m__1A3(UnityEngine.Touch)
extern "C"  bool ObservableExtension_U3CEveryTapU3Em__1A3_m2765014839 (Il2CppObject * __this /* static, unused */, Touch_t1603883884  ___touch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZergRush.ObservableExtension::<EveryTap>m__1A4(UnityEngine.Touch)
extern "C"  bool ObservableExtension_U3CEveryTapU3Em__1A4_m2356190614 (Il2CppObject * __this /* static, unused */, Touch_t1603883884  ___touch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
