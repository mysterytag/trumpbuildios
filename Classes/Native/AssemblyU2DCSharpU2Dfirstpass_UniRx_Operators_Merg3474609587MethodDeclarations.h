﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.MergeObservable`1/MergeConcurrentObserver/Merge<UniRx.Unit>
struct Merge_t3474609587;
// UniRx.Operators.MergeObservable`1/MergeConcurrentObserver<UniRx.Unit>
struct MergeConcurrentObserver_t960805624;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.MergeObservable`1/MergeConcurrentObserver/Merge<UniRx.Unit>::.ctor(UniRx.Operators.MergeObservable`1/MergeConcurrentObserver<T>,System.IDisposable)
extern "C"  void Merge__ctor_m4162486394_gshared (Merge_t3474609587 * __this, MergeConcurrentObserver_t960805624 * ___parent0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define Merge__ctor_m4162486394(__this, ___parent0, ___cancel1, method) ((  void (*) (Merge_t3474609587 *, MergeConcurrentObserver_t960805624 *, Il2CppObject *, const MethodInfo*))Merge__ctor_m4162486394_gshared)(__this, ___parent0, ___cancel1, method)
// System.Void UniRx.Operators.MergeObservable`1/MergeConcurrentObserver/Merge<UniRx.Unit>::OnNext(T)
extern "C"  void Merge_OnNext_m1495671685_gshared (Merge_t3474609587 * __this, Unit_t2558286038  ___value0, const MethodInfo* method);
#define Merge_OnNext_m1495671685(__this, ___value0, method) ((  void (*) (Merge_t3474609587 *, Unit_t2558286038 , const MethodInfo*))Merge_OnNext_m1495671685_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.MergeObservable`1/MergeConcurrentObserver/Merge<UniRx.Unit>::OnError(System.Exception)
extern "C"  void Merge_OnError_m1510249620_gshared (Merge_t3474609587 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Merge_OnError_m1510249620(__this, ___error0, method) ((  void (*) (Merge_t3474609587 *, Exception_t1967233988 *, const MethodInfo*))Merge_OnError_m1510249620_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.MergeObservable`1/MergeConcurrentObserver/Merge<UniRx.Unit>::OnCompleted()
extern "C"  void Merge_OnCompleted_m2680063463_gshared (Merge_t3474609587 * __this, const MethodInfo* method);
#define Merge_OnCompleted_m2680063463(__this, method) ((  void (*) (Merge_t3474609587 *, const MethodInfo*))Merge_OnCompleted_m2680063463_gshared)(__this, method)
