﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`2<System.Int64,System.Boolean>
struct Func_2_t2251336571;
// UniRx.Operators.WhereObservable`1<System.Int64>
struct WhereObservable_1_t4045974831;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.WhereObservable`1/<CombinePredicate>c__AnonStorey72<System.Int64>
struct  U3CCombinePredicateU3Ec__AnonStorey72_t278143071  : public Il2CppObject
{
public:
	// System.Func`2<T,System.Boolean> UniRx.Operators.WhereObservable`1/<CombinePredicate>c__AnonStorey72::combinePredicate
	Func_2_t2251336571 * ___combinePredicate_0;
	// UniRx.Operators.WhereObservable`1<T> UniRx.Operators.WhereObservable`1/<CombinePredicate>c__AnonStorey72::<>f__this
	WhereObservable_1_t4045974831 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_combinePredicate_0() { return static_cast<int32_t>(offsetof(U3CCombinePredicateU3Ec__AnonStorey72_t278143071, ___combinePredicate_0)); }
	inline Func_2_t2251336571 * get_combinePredicate_0() const { return ___combinePredicate_0; }
	inline Func_2_t2251336571 ** get_address_of_combinePredicate_0() { return &___combinePredicate_0; }
	inline void set_combinePredicate_0(Func_2_t2251336571 * value)
	{
		___combinePredicate_0 = value;
		Il2CppCodeGenWriteBarrier(&___combinePredicate_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CCombinePredicateU3Ec__AnonStorey72_t278143071, ___U3CU3Ef__this_1)); }
	inline WhereObservable_1_t4045974831 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline WhereObservable_1_t4045974831 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(WhereObservable_1_t4045974831 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
