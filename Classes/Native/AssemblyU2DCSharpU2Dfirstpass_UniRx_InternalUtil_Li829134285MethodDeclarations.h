﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.ListObserver`1<System.Double>
struct ListObserver_1_t829134285;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<System.Double>>
struct ImmutableList_1_t3806720016;
// System.Exception
struct Exception_t1967233988;
// UniRx.IObserver`1<System.Double>
struct IObserver_1_t2746515517;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.InternalUtil.ListObserver`1<System.Double>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m1930397811_gshared (ListObserver_1_t829134285 * __this, ImmutableList_1_t3806720016 * ___observers0, const MethodInfo* method);
#define ListObserver_1__ctor_m1930397811(__this, ___observers0, method) ((  void (*) (ListObserver_1_t829134285 *, ImmutableList_1_t3806720016 *, const MethodInfo*))ListObserver_1__ctor_m1930397811_gshared)(__this, ___observers0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<System.Double>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m3831432555_gshared (ListObserver_1_t829134285 * __this, const MethodInfo* method);
#define ListObserver_1_OnCompleted_m3831432555(__this, method) ((  void (*) (ListObserver_1_t829134285 *, const MethodInfo*))ListObserver_1_OnCompleted_m3831432555_gshared)(__this, method)
// System.Void UniRx.InternalUtil.ListObserver`1<System.Double>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m2859331096_gshared (ListObserver_1_t829134285 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ListObserver_1_OnError_m2859331096(__this, ___error0, method) ((  void (*) (ListObserver_1_t829134285 *, Exception_t1967233988 *, const MethodInfo*))ListObserver_1_OnError_m2859331096_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<System.Double>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m2916812041_gshared (ListObserver_1_t829134285 * __this, double ___value0, const MethodInfo* method);
#define ListObserver_1_OnNext_m2916812041(__this, ___value0, method) ((  void (*) (ListObserver_1_t829134285 *, double, const MethodInfo*))ListObserver_1_OnNext_m2916812041_gshared)(__this, ___value0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<System.Double>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m1205449883_gshared (ListObserver_1_t829134285 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Add_m1205449883(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t829134285 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Add_m1205449883_gshared)(__this, ___observer0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<System.Double>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m1769348304_gshared (ListObserver_1_t829134285 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Remove_m1769348304(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t829134285 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Remove_m1769348304_gshared)(__this, ___observer0, method)
