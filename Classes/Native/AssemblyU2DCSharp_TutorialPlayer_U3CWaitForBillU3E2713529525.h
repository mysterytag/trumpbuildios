﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// TutorialPlayer
struct TutorialPlayer_t4281139199;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TutorialPlayer/<WaitForBill>c__Iterator25
struct  U3CWaitForBillU3Ec__Iterator25_t2713529525  : public Il2CppObject
{
public:
	// System.Int32 TutorialPlayer/<WaitForBill>c__Iterator25::money
	int32_t ___money_0;
	// System.Int32 TutorialPlayer/<WaitForBill>c__Iterator25::$PC
	int32_t ___U24PC_1;
	// System.Object TutorialPlayer/<WaitForBill>c__Iterator25::$current
	Il2CppObject * ___U24current_2;
	// System.Int32 TutorialPlayer/<WaitForBill>c__Iterator25::<$>money
	int32_t ___U3CU24U3Emoney_3;
	// TutorialPlayer TutorialPlayer/<WaitForBill>c__Iterator25::<>f__this
	TutorialPlayer_t4281139199 * ___U3CU3Ef__this_4;

public:
	inline static int32_t get_offset_of_money_0() { return static_cast<int32_t>(offsetof(U3CWaitForBillU3Ec__Iterator25_t2713529525, ___money_0)); }
	inline int32_t get_money_0() const { return ___money_0; }
	inline int32_t* get_address_of_money_0() { return &___money_0; }
	inline void set_money_0(int32_t value)
	{
		___money_0 = value;
	}

	inline static int32_t get_offset_of_U24PC_1() { return static_cast<int32_t>(offsetof(U3CWaitForBillU3Ec__Iterator25_t2713529525, ___U24PC_1)); }
	inline int32_t get_U24PC_1() const { return ___U24PC_1; }
	inline int32_t* get_address_of_U24PC_1() { return &___U24PC_1; }
	inline void set_U24PC_1(int32_t value)
	{
		___U24PC_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CWaitForBillU3Ec__Iterator25_t2713529525, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Emoney_3() { return static_cast<int32_t>(offsetof(U3CWaitForBillU3Ec__Iterator25_t2713529525, ___U3CU24U3Emoney_3)); }
	inline int32_t get_U3CU24U3Emoney_3() const { return ___U3CU24U3Emoney_3; }
	inline int32_t* get_address_of_U3CU24U3Emoney_3() { return &___U3CU24U3Emoney_3; }
	inline void set_U3CU24U3Emoney_3(int32_t value)
	{
		___U3CU24U3Emoney_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_4() { return static_cast<int32_t>(offsetof(U3CWaitForBillU3Ec__Iterator25_t2713529525, ___U3CU3Ef__this_4)); }
	inline TutorialPlayer_t4281139199 * get_U3CU3Ef__this_4() const { return ___U3CU3Ef__this_4; }
	inline TutorialPlayer_t4281139199 ** get_address_of_U3CU3Ef__this_4() { return &___U3CU3Ef__this_4; }
	inline void set_U3CU3Ef__this_4(TutorialPlayer_t4281139199 * value)
	{
		___U3CU3Ef__this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
