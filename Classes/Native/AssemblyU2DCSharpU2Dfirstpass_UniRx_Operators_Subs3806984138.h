﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// UniRx.SerialDisposable
struct SerialDisposable_t2547852742;
// UniRx.Operators.SubscribeOnObservable`1<System.Object>
struct SubscribeOnObservable_1_t2329329235;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.SubscribeOnObservable`1/<SubscribeCore>c__AnonStorey69<System.Object>
struct  U3CSubscribeCoreU3Ec__AnonStorey69_t3806984138  : public Il2CppObject
{
public:
	// UniRx.IObserver`1<T> UniRx.Operators.SubscribeOnObservable`1/<SubscribeCore>c__AnonStorey69::observer
	Il2CppObject* ___observer_0;
	// UniRx.SerialDisposable UniRx.Operators.SubscribeOnObservable`1/<SubscribeCore>c__AnonStorey69::d
	SerialDisposable_t2547852742 * ___d_1;
	// UniRx.Operators.SubscribeOnObservable`1<T> UniRx.Operators.SubscribeOnObservable`1/<SubscribeCore>c__AnonStorey69::<>f__this
	SubscribeOnObservable_1_t2329329235 * ___U3CU3Ef__this_2;

public:
	inline static int32_t get_offset_of_observer_0() { return static_cast<int32_t>(offsetof(U3CSubscribeCoreU3Ec__AnonStorey69_t3806984138, ___observer_0)); }
	inline Il2CppObject* get_observer_0() const { return ___observer_0; }
	inline Il2CppObject** get_address_of_observer_0() { return &___observer_0; }
	inline void set_observer_0(Il2CppObject* value)
	{
		___observer_0 = value;
		Il2CppCodeGenWriteBarrier(&___observer_0, value);
	}

	inline static int32_t get_offset_of_d_1() { return static_cast<int32_t>(offsetof(U3CSubscribeCoreU3Ec__AnonStorey69_t3806984138, ___d_1)); }
	inline SerialDisposable_t2547852742 * get_d_1() const { return ___d_1; }
	inline SerialDisposable_t2547852742 ** get_address_of_d_1() { return &___d_1; }
	inline void set_d_1(SerialDisposable_t2547852742 * value)
	{
		___d_1 = value;
		Il2CppCodeGenWriteBarrier(&___d_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_2() { return static_cast<int32_t>(offsetof(U3CSubscribeCoreU3Ec__AnonStorey69_t3806984138, ___U3CU3Ef__this_2)); }
	inline SubscribeOnObservable_1_t2329329235 * get_U3CU3Ef__this_2() const { return ___U3CU3Ef__this_2; }
	inline SubscribeOnObservable_1_t2329329235 ** get_address_of_U3CU3Ef__this_2() { return &___U3CU3Ef__this_2; }
	inline void set_U3CU3Ef__this_2(SubscribeOnObservable_1_t2329329235 * value)
	{
		___U3CU3Ef__this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
