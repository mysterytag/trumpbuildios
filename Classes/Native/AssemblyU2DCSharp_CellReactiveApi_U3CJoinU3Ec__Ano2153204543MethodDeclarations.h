﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/<Join>c__AnonStorey114`1<System.Object>
struct U3CJoinU3Ec__AnonStorey114_1_t2153204543;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"

// System.Void CellReactiveApi/<Join>c__AnonStorey114`1<System.Object>::.ctor()
extern "C"  void U3CJoinU3Ec__AnonStorey114_1__ctor_m3300018005_gshared (U3CJoinU3Ec__AnonStorey114_1_t2153204543 * __this, const MethodInfo* method);
#define U3CJoinU3Ec__AnonStorey114_1__ctor_m3300018005(__this, method) ((  void (*) (U3CJoinU3Ec__AnonStorey114_1_t2153204543 *, const MethodInfo*))U3CJoinU3Ec__AnonStorey114_1__ctor_m3300018005_gshared)(__this, method)
// System.IDisposable CellReactiveApi/<Join>c__AnonStorey114`1<System.Object>::<>m__188(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * U3CJoinU3Ec__AnonStorey114_1_U3CU3Em__188_m1965070592_gshared (U3CJoinU3Ec__AnonStorey114_1_t2153204543 * __this, Action_1_t985559125 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define U3CJoinU3Ec__AnonStorey114_1_U3CU3Em__188_m1965070592(__this, ___reaction0, ___p1, method) ((  Il2CppObject * (*) (U3CJoinU3Ec__AnonStorey114_1_t2153204543 *, Action_1_t985559125 *, int32_t, const MethodInfo*))U3CJoinU3Ec__AnonStorey114_1_U3CU3Em__188_m1965070592_gshared)(__this, ___reaction0, ___p1, method)
// T CellReactiveApi/<Join>c__AnonStorey114`1<System.Object>::<>m__189()
extern "C"  Il2CppObject * U3CJoinU3Ec__AnonStorey114_1_U3CU3Em__189_m3577530199_gshared (U3CJoinU3Ec__AnonStorey114_1_t2153204543 * __this, const MethodInfo* method);
#define U3CJoinU3Ec__AnonStorey114_1_U3CU3Em__189_m3577530199(__this, method) ((  Il2CppObject * (*) (U3CJoinU3Ec__AnonStorey114_1_t2153204543 *, const MethodInfo*))U3CJoinU3Ec__AnonStorey114_1_U3CU3Em__189_m3577530199_gshared)(__this, method)
