﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TestCoro/<Bearutine>c__Iterator2F
struct U3CBearutineU3Ec__Iterator2F_t3513655992;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void TestCoro/<Bearutine>c__Iterator2F::.ctor()
extern "C"  void U3CBearutineU3Ec__Iterator2F__ctor_m2512576543 (U3CBearutineU3Ec__Iterator2F_t3513655992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TestCoro/<Bearutine>c__Iterator2F::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CBearutineU3Ec__Iterator2F_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4164303315 (U3CBearutineU3Ec__Iterator2F_t3513655992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TestCoro/<Bearutine>c__Iterator2F::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CBearutineU3Ec__Iterator2F_System_Collections_IEnumerator_get_Current_m1875133287 (U3CBearutineU3Ec__Iterator2F_t3513655992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TestCoro/<Bearutine>c__Iterator2F::MoveNext()
extern "C"  bool U3CBearutineU3Ec__Iterator2F_MoveNext_m3145224821 (U3CBearutineU3Ec__Iterator2F_t3513655992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestCoro/<Bearutine>c__Iterator2F::Dispose()
extern "C"  void U3CBearutineU3Ec__Iterator2F_Dispose_m712720540 (U3CBearutineU3Ec__Iterator2F_t3513655992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestCoro/<Bearutine>c__Iterator2F::Reset()
extern "C"  void U3CBearutineU3Ec__Iterator2F_Reset_m159009484 (U3CBearutineU3Ec__Iterator2F_t3513655992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestCoro/<Bearutine>c__Iterator2F::<>m__1E3(System.Single)
extern "C"  void U3CBearutineU3Ec__Iterator2F_U3CU3Em__1E3_m1710957010 (Il2CppObject * __this /* static, unused */, float ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
