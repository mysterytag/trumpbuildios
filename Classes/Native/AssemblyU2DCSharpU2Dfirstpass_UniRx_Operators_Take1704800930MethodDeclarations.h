﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.TakeUntilObservable`2/TakeUntil/TakeUntilOther<System.Object,System.Object>
struct TakeUntilOther_t1704800930;
// UniRx.Operators.TakeUntilObservable`2/TakeUntil<System.Object,System.Object>
struct TakeUntil_t2678949436;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.TakeUntilObservable`2/TakeUntil/TakeUntilOther<System.Object,System.Object>::.ctor(UniRx.Operators.TakeUntilObservable`2/TakeUntil<T,TOther>,System.IDisposable)
extern "C"  void TakeUntilOther__ctor_m3699070061_gshared (TakeUntilOther_t1704800930 * __this, TakeUntil_t2678949436 * ___sourceObserver0, Il2CppObject * ___subscription1, const MethodInfo* method);
#define TakeUntilOther__ctor_m3699070061(__this, ___sourceObserver0, ___subscription1, method) ((  void (*) (TakeUntilOther_t1704800930 *, TakeUntil_t2678949436 *, Il2CppObject *, const MethodInfo*))TakeUntilOther__ctor_m3699070061_gshared)(__this, ___sourceObserver0, ___subscription1, method)
// System.Void UniRx.Operators.TakeUntilObservable`2/TakeUntil/TakeUntilOther<System.Object,System.Object>::OnNext(TOther)
extern "C"  void TakeUntilOther_OnNext_m3987641410_gshared (TakeUntilOther_t1704800930 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define TakeUntilOther_OnNext_m3987641410(__this, ___value0, method) ((  void (*) (TakeUntilOther_t1704800930 *, Il2CppObject *, const MethodInfo*))TakeUntilOther_OnNext_m3987641410_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.TakeUntilObservable`2/TakeUntil/TakeUntilOther<System.Object,System.Object>::OnError(System.Exception)
extern "C"  void TakeUntilOther_OnError_m3044938895_gshared (TakeUntilOther_t1704800930 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define TakeUntilOther_OnError_m3044938895(__this, ___error0, method) ((  void (*) (TakeUntilOther_t1704800930 *, Exception_t1967233988 *, const MethodInfo*))TakeUntilOther_OnError_m3044938895_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.TakeUntilObservable`2/TakeUntil/TakeUntilOther<System.Object,System.Object>::OnCompleted()
extern "C"  void TakeUntilOther_OnCompleted_m4149470818_gshared (TakeUntilOther_t1704800930 * __this, const MethodInfo* method);
#define TakeUntilOther_OnCompleted_m4149470818(__this, method) ((  void (*) (TakeUntilOther_t1704800930 *, const MethodInfo*))TakeUntilOther_OnCompleted_m4149470818_gshared)(__this, method)
