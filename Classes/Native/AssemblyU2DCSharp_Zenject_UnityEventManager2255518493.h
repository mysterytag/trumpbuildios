﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action
struct Action_t437523947;
// System.Action`1<System.Boolean>
struct Action_1_t359458046;
// System.Action`1<System.Int32>
struct Action_1_t2995867492;
// System.Action`1<ModestTree.Util.MouseWheelScrollDirections>
struct Action_1_t3075561458;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.UnityEventManager
struct  UnityEventManager_t2255518493  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.Vector3 Zenject.UnityEventManager::_lastMousePosition
	Vector3_t3525329789  ____lastMousePosition_2;
	// System.Int32 Zenject.UnityEventManager::_lastWidth
	int32_t ____lastWidth_3;
	// System.Int32 Zenject.UnityEventManager::_lastHeight
	int32_t ____lastHeight_4;
	// System.Action Zenject.UnityEventManager::ApplicationGainedFocus
	Action_t437523947 * ___ApplicationGainedFocus_5;
	// System.Action Zenject.UnityEventManager::ApplicationLostFocus
	Action_t437523947 * ___ApplicationLostFocus_6;
	// System.Action`1<System.Boolean> Zenject.UnityEventManager::ApplicationFocusChanged
	Action_1_t359458046 * ___ApplicationFocusChanged_7;
	// System.Action Zenject.UnityEventManager::ApplicationQuit
	Action_t437523947 * ___ApplicationQuit_8;
	// System.Action Zenject.UnityEventManager::ChangingScenes
	Action_t437523947 * ___ChangingScenes_9;
	// System.Action Zenject.UnityEventManager::DrawGizmos
	Action_t437523947 * ___DrawGizmos_10;
	// System.Action`1<System.Int32> Zenject.UnityEventManager::MouseButtonDown
	Action_1_t2995867492 * ___MouseButtonDown_11;
	// System.Action`1<System.Int32> Zenject.UnityEventManager::MouseButtonUp
	Action_1_t2995867492 * ___MouseButtonUp_12;
	// System.Action Zenject.UnityEventManager::LeftMouseButtonDown
	Action_t437523947 * ___LeftMouseButtonDown_13;
	// System.Action Zenject.UnityEventManager::LeftMouseButtonUp
	Action_t437523947 * ___LeftMouseButtonUp_14;
	// System.Action Zenject.UnityEventManager::MiddleMouseButtonDown
	Action_t437523947 * ___MiddleMouseButtonDown_15;
	// System.Action Zenject.UnityEventManager::MiddleMouseButtonUp
	Action_t437523947 * ___MiddleMouseButtonUp_16;
	// System.Action Zenject.UnityEventManager::RightMouseButtonDown
	Action_t437523947 * ___RightMouseButtonDown_17;
	// System.Action Zenject.UnityEventManager::RightMouseButtonUp
	Action_t437523947 * ___RightMouseButtonUp_18;
	// System.Action`1<ModestTree.Util.MouseWheelScrollDirections> Zenject.UnityEventManager::MouseWheelMoved
	Action_1_t3075561458 * ___MouseWheelMoved_19;
	// System.Action Zenject.UnityEventManager::MouseMove
	Action_t437523947 * ___MouseMove_20;
	// System.Action Zenject.UnityEventManager::ScreenSizeChanged
	Action_t437523947 * ___ScreenSizeChanged_21;
	// System.Action Zenject.UnityEventManager::Started
	Action_t437523947 * ___Started_22;
	// System.Boolean Zenject.UnityEventManager::<IsFocused>k__BackingField
	bool ___U3CIsFocusedU3Ek__BackingField_23;

public:
	inline static int32_t get_offset_of__lastMousePosition_2() { return static_cast<int32_t>(offsetof(UnityEventManager_t2255518493, ____lastMousePosition_2)); }
	inline Vector3_t3525329789  get__lastMousePosition_2() const { return ____lastMousePosition_2; }
	inline Vector3_t3525329789 * get_address_of__lastMousePosition_2() { return &____lastMousePosition_2; }
	inline void set__lastMousePosition_2(Vector3_t3525329789  value)
	{
		____lastMousePosition_2 = value;
	}

	inline static int32_t get_offset_of__lastWidth_3() { return static_cast<int32_t>(offsetof(UnityEventManager_t2255518493, ____lastWidth_3)); }
	inline int32_t get__lastWidth_3() const { return ____lastWidth_3; }
	inline int32_t* get_address_of__lastWidth_3() { return &____lastWidth_3; }
	inline void set__lastWidth_3(int32_t value)
	{
		____lastWidth_3 = value;
	}

	inline static int32_t get_offset_of__lastHeight_4() { return static_cast<int32_t>(offsetof(UnityEventManager_t2255518493, ____lastHeight_4)); }
	inline int32_t get__lastHeight_4() const { return ____lastHeight_4; }
	inline int32_t* get_address_of__lastHeight_4() { return &____lastHeight_4; }
	inline void set__lastHeight_4(int32_t value)
	{
		____lastHeight_4 = value;
	}

	inline static int32_t get_offset_of_ApplicationGainedFocus_5() { return static_cast<int32_t>(offsetof(UnityEventManager_t2255518493, ___ApplicationGainedFocus_5)); }
	inline Action_t437523947 * get_ApplicationGainedFocus_5() const { return ___ApplicationGainedFocus_5; }
	inline Action_t437523947 ** get_address_of_ApplicationGainedFocus_5() { return &___ApplicationGainedFocus_5; }
	inline void set_ApplicationGainedFocus_5(Action_t437523947 * value)
	{
		___ApplicationGainedFocus_5 = value;
		Il2CppCodeGenWriteBarrier(&___ApplicationGainedFocus_5, value);
	}

	inline static int32_t get_offset_of_ApplicationLostFocus_6() { return static_cast<int32_t>(offsetof(UnityEventManager_t2255518493, ___ApplicationLostFocus_6)); }
	inline Action_t437523947 * get_ApplicationLostFocus_6() const { return ___ApplicationLostFocus_6; }
	inline Action_t437523947 ** get_address_of_ApplicationLostFocus_6() { return &___ApplicationLostFocus_6; }
	inline void set_ApplicationLostFocus_6(Action_t437523947 * value)
	{
		___ApplicationLostFocus_6 = value;
		Il2CppCodeGenWriteBarrier(&___ApplicationLostFocus_6, value);
	}

	inline static int32_t get_offset_of_ApplicationFocusChanged_7() { return static_cast<int32_t>(offsetof(UnityEventManager_t2255518493, ___ApplicationFocusChanged_7)); }
	inline Action_1_t359458046 * get_ApplicationFocusChanged_7() const { return ___ApplicationFocusChanged_7; }
	inline Action_1_t359458046 ** get_address_of_ApplicationFocusChanged_7() { return &___ApplicationFocusChanged_7; }
	inline void set_ApplicationFocusChanged_7(Action_1_t359458046 * value)
	{
		___ApplicationFocusChanged_7 = value;
		Il2CppCodeGenWriteBarrier(&___ApplicationFocusChanged_7, value);
	}

	inline static int32_t get_offset_of_ApplicationQuit_8() { return static_cast<int32_t>(offsetof(UnityEventManager_t2255518493, ___ApplicationQuit_8)); }
	inline Action_t437523947 * get_ApplicationQuit_8() const { return ___ApplicationQuit_8; }
	inline Action_t437523947 ** get_address_of_ApplicationQuit_8() { return &___ApplicationQuit_8; }
	inline void set_ApplicationQuit_8(Action_t437523947 * value)
	{
		___ApplicationQuit_8 = value;
		Il2CppCodeGenWriteBarrier(&___ApplicationQuit_8, value);
	}

	inline static int32_t get_offset_of_ChangingScenes_9() { return static_cast<int32_t>(offsetof(UnityEventManager_t2255518493, ___ChangingScenes_9)); }
	inline Action_t437523947 * get_ChangingScenes_9() const { return ___ChangingScenes_9; }
	inline Action_t437523947 ** get_address_of_ChangingScenes_9() { return &___ChangingScenes_9; }
	inline void set_ChangingScenes_9(Action_t437523947 * value)
	{
		___ChangingScenes_9 = value;
		Il2CppCodeGenWriteBarrier(&___ChangingScenes_9, value);
	}

	inline static int32_t get_offset_of_DrawGizmos_10() { return static_cast<int32_t>(offsetof(UnityEventManager_t2255518493, ___DrawGizmos_10)); }
	inline Action_t437523947 * get_DrawGizmos_10() const { return ___DrawGizmos_10; }
	inline Action_t437523947 ** get_address_of_DrawGizmos_10() { return &___DrawGizmos_10; }
	inline void set_DrawGizmos_10(Action_t437523947 * value)
	{
		___DrawGizmos_10 = value;
		Il2CppCodeGenWriteBarrier(&___DrawGizmos_10, value);
	}

	inline static int32_t get_offset_of_MouseButtonDown_11() { return static_cast<int32_t>(offsetof(UnityEventManager_t2255518493, ___MouseButtonDown_11)); }
	inline Action_1_t2995867492 * get_MouseButtonDown_11() const { return ___MouseButtonDown_11; }
	inline Action_1_t2995867492 ** get_address_of_MouseButtonDown_11() { return &___MouseButtonDown_11; }
	inline void set_MouseButtonDown_11(Action_1_t2995867492 * value)
	{
		___MouseButtonDown_11 = value;
		Il2CppCodeGenWriteBarrier(&___MouseButtonDown_11, value);
	}

	inline static int32_t get_offset_of_MouseButtonUp_12() { return static_cast<int32_t>(offsetof(UnityEventManager_t2255518493, ___MouseButtonUp_12)); }
	inline Action_1_t2995867492 * get_MouseButtonUp_12() const { return ___MouseButtonUp_12; }
	inline Action_1_t2995867492 ** get_address_of_MouseButtonUp_12() { return &___MouseButtonUp_12; }
	inline void set_MouseButtonUp_12(Action_1_t2995867492 * value)
	{
		___MouseButtonUp_12 = value;
		Il2CppCodeGenWriteBarrier(&___MouseButtonUp_12, value);
	}

	inline static int32_t get_offset_of_LeftMouseButtonDown_13() { return static_cast<int32_t>(offsetof(UnityEventManager_t2255518493, ___LeftMouseButtonDown_13)); }
	inline Action_t437523947 * get_LeftMouseButtonDown_13() const { return ___LeftMouseButtonDown_13; }
	inline Action_t437523947 ** get_address_of_LeftMouseButtonDown_13() { return &___LeftMouseButtonDown_13; }
	inline void set_LeftMouseButtonDown_13(Action_t437523947 * value)
	{
		___LeftMouseButtonDown_13 = value;
		Il2CppCodeGenWriteBarrier(&___LeftMouseButtonDown_13, value);
	}

	inline static int32_t get_offset_of_LeftMouseButtonUp_14() { return static_cast<int32_t>(offsetof(UnityEventManager_t2255518493, ___LeftMouseButtonUp_14)); }
	inline Action_t437523947 * get_LeftMouseButtonUp_14() const { return ___LeftMouseButtonUp_14; }
	inline Action_t437523947 ** get_address_of_LeftMouseButtonUp_14() { return &___LeftMouseButtonUp_14; }
	inline void set_LeftMouseButtonUp_14(Action_t437523947 * value)
	{
		___LeftMouseButtonUp_14 = value;
		Il2CppCodeGenWriteBarrier(&___LeftMouseButtonUp_14, value);
	}

	inline static int32_t get_offset_of_MiddleMouseButtonDown_15() { return static_cast<int32_t>(offsetof(UnityEventManager_t2255518493, ___MiddleMouseButtonDown_15)); }
	inline Action_t437523947 * get_MiddleMouseButtonDown_15() const { return ___MiddleMouseButtonDown_15; }
	inline Action_t437523947 ** get_address_of_MiddleMouseButtonDown_15() { return &___MiddleMouseButtonDown_15; }
	inline void set_MiddleMouseButtonDown_15(Action_t437523947 * value)
	{
		___MiddleMouseButtonDown_15 = value;
		Il2CppCodeGenWriteBarrier(&___MiddleMouseButtonDown_15, value);
	}

	inline static int32_t get_offset_of_MiddleMouseButtonUp_16() { return static_cast<int32_t>(offsetof(UnityEventManager_t2255518493, ___MiddleMouseButtonUp_16)); }
	inline Action_t437523947 * get_MiddleMouseButtonUp_16() const { return ___MiddleMouseButtonUp_16; }
	inline Action_t437523947 ** get_address_of_MiddleMouseButtonUp_16() { return &___MiddleMouseButtonUp_16; }
	inline void set_MiddleMouseButtonUp_16(Action_t437523947 * value)
	{
		___MiddleMouseButtonUp_16 = value;
		Il2CppCodeGenWriteBarrier(&___MiddleMouseButtonUp_16, value);
	}

	inline static int32_t get_offset_of_RightMouseButtonDown_17() { return static_cast<int32_t>(offsetof(UnityEventManager_t2255518493, ___RightMouseButtonDown_17)); }
	inline Action_t437523947 * get_RightMouseButtonDown_17() const { return ___RightMouseButtonDown_17; }
	inline Action_t437523947 ** get_address_of_RightMouseButtonDown_17() { return &___RightMouseButtonDown_17; }
	inline void set_RightMouseButtonDown_17(Action_t437523947 * value)
	{
		___RightMouseButtonDown_17 = value;
		Il2CppCodeGenWriteBarrier(&___RightMouseButtonDown_17, value);
	}

	inline static int32_t get_offset_of_RightMouseButtonUp_18() { return static_cast<int32_t>(offsetof(UnityEventManager_t2255518493, ___RightMouseButtonUp_18)); }
	inline Action_t437523947 * get_RightMouseButtonUp_18() const { return ___RightMouseButtonUp_18; }
	inline Action_t437523947 ** get_address_of_RightMouseButtonUp_18() { return &___RightMouseButtonUp_18; }
	inline void set_RightMouseButtonUp_18(Action_t437523947 * value)
	{
		___RightMouseButtonUp_18 = value;
		Il2CppCodeGenWriteBarrier(&___RightMouseButtonUp_18, value);
	}

	inline static int32_t get_offset_of_MouseWheelMoved_19() { return static_cast<int32_t>(offsetof(UnityEventManager_t2255518493, ___MouseWheelMoved_19)); }
	inline Action_1_t3075561458 * get_MouseWheelMoved_19() const { return ___MouseWheelMoved_19; }
	inline Action_1_t3075561458 ** get_address_of_MouseWheelMoved_19() { return &___MouseWheelMoved_19; }
	inline void set_MouseWheelMoved_19(Action_1_t3075561458 * value)
	{
		___MouseWheelMoved_19 = value;
		Il2CppCodeGenWriteBarrier(&___MouseWheelMoved_19, value);
	}

	inline static int32_t get_offset_of_MouseMove_20() { return static_cast<int32_t>(offsetof(UnityEventManager_t2255518493, ___MouseMove_20)); }
	inline Action_t437523947 * get_MouseMove_20() const { return ___MouseMove_20; }
	inline Action_t437523947 ** get_address_of_MouseMove_20() { return &___MouseMove_20; }
	inline void set_MouseMove_20(Action_t437523947 * value)
	{
		___MouseMove_20 = value;
		Il2CppCodeGenWriteBarrier(&___MouseMove_20, value);
	}

	inline static int32_t get_offset_of_ScreenSizeChanged_21() { return static_cast<int32_t>(offsetof(UnityEventManager_t2255518493, ___ScreenSizeChanged_21)); }
	inline Action_t437523947 * get_ScreenSizeChanged_21() const { return ___ScreenSizeChanged_21; }
	inline Action_t437523947 ** get_address_of_ScreenSizeChanged_21() { return &___ScreenSizeChanged_21; }
	inline void set_ScreenSizeChanged_21(Action_t437523947 * value)
	{
		___ScreenSizeChanged_21 = value;
		Il2CppCodeGenWriteBarrier(&___ScreenSizeChanged_21, value);
	}

	inline static int32_t get_offset_of_Started_22() { return static_cast<int32_t>(offsetof(UnityEventManager_t2255518493, ___Started_22)); }
	inline Action_t437523947 * get_Started_22() const { return ___Started_22; }
	inline Action_t437523947 ** get_address_of_Started_22() { return &___Started_22; }
	inline void set_Started_22(Action_t437523947 * value)
	{
		___Started_22 = value;
		Il2CppCodeGenWriteBarrier(&___Started_22, value);
	}

	inline static int32_t get_offset_of_U3CIsFocusedU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(UnityEventManager_t2255518493, ___U3CIsFocusedU3Ek__BackingField_23)); }
	inline bool get_U3CIsFocusedU3Ek__BackingField_23() const { return ___U3CIsFocusedU3Ek__BackingField_23; }
	inline bool* get_address_of_U3CIsFocusedU3Ek__BackingField_23() { return &___U3CIsFocusedU3Ek__BackingField_23; }
	inline void set_U3CIsFocusedU3Ek__BackingField_23(bool value)
	{
		___U3CIsFocusedU3Ek__BackingField_23 = value;
	}
};

struct UnityEventManager_t2255518493_StaticFields
{
public:
	// System.Action Zenject.UnityEventManager::<>f__am$cache16
	Action_t437523947 * ___U3CU3Ef__amU24cache16_24;
	// System.Action Zenject.UnityEventManager::<>f__am$cache17
	Action_t437523947 * ___U3CU3Ef__amU24cache17_25;
	// System.Action`1<System.Boolean> Zenject.UnityEventManager::<>f__am$cache18
	Action_1_t359458046 * ___U3CU3Ef__amU24cache18_26;
	// System.Action Zenject.UnityEventManager::<>f__am$cache19
	Action_t437523947 * ___U3CU3Ef__amU24cache19_27;
	// System.Action Zenject.UnityEventManager::<>f__am$cache1A
	Action_t437523947 * ___U3CU3Ef__amU24cache1A_28;
	// System.Action Zenject.UnityEventManager::<>f__am$cache1B
	Action_t437523947 * ___U3CU3Ef__amU24cache1B_29;
	// System.Action`1<System.Int32> Zenject.UnityEventManager::<>f__am$cache1C
	Action_1_t2995867492 * ___U3CU3Ef__amU24cache1C_30;
	// System.Action`1<System.Int32> Zenject.UnityEventManager::<>f__am$cache1D
	Action_1_t2995867492 * ___U3CU3Ef__amU24cache1D_31;
	// System.Action Zenject.UnityEventManager::<>f__am$cache1E
	Action_t437523947 * ___U3CU3Ef__amU24cache1E_32;
	// System.Action Zenject.UnityEventManager::<>f__am$cache1F
	Action_t437523947 * ___U3CU3Ef__amU24cache1F_33;
	// System.Action Zenject.UnityEventManager::<>f__am$cache20
	Action_t437523947 * ___U3CU3Ef__amU24cache20_34;
	// System.Action Zenject.UnityEventManager::<>f__am$cache21
	Action_t437523947 * ___U3CU3Ef__amU24cache21_35;
	// System.Action Zenject.UnityEventManager::<>f__am$cache22
	Action_t437523947 * ___U3CU3Ef__amU24cache22_36;
	// System.Action Zenject.UnityEventManager::<>f__am$cache23
	Action_t437523947 * ___U3CU3Ef__amU24cache23_37;
	// System.Action`1<ModestTree.Util.MouseWheelScrollDirections> Zenject.UnityEventManager::<>f__am$cache24
	Action_1_t3075561458 * ___U3CU3Ef__amU24cache24_38;
	// System.Action Zenject.UnityEventManager::<>f__am$cache25
	Action_t437523947 * ___U3CU3Ef__amU24cache25_39;
	// System.Action Zenject.UnityEventManager::<>f__am$cache26
	Action_t437523947 * ___U3CU3Ef__amU24cache26_40;
	// System.Action Zenject.UnityEventManager::<>f__am$cache27
	Action_t437523947 * ___U3CU3Ef__amU24cache27_41;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache16_24() { return static_cast<int32_t>(offsetof(UnityEventManager_t2255518493_StaticFields, ___U3CU3Ef__amU24cache16_24)); }
	inline Action_t437523947 * get_U3CU3Ef__amU24cache16_24() const { return ___U3CU3Ef__amU24cache16_24; }
	inline Action_t437523947 ** get_address_of_U3CU3Ef__amU24cache16_24() { return &___U3CU3Ef__amU24cache16_24; }
	inline void set_U3CU3Ef__amU24cache16_24(Action_t437523947 * value)
	{
		___U3CU3Ef__amU24cache16_24 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache16_24, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache17_25() { return static_cast<int32_t>(offsetof(UnityEventManager_t2255518493_StaticFields, ___U3CU3Ef__amU24cache17_25)); }
	inline Action_t437523947 * get_U3CU3Ef__amU24cache17_25() const { return ___U3CU3Ef__amU24cache17_25; }
	inline Action_t437523947 ** get_address_of_U3CU3Ef__amU24cache17_25() { return &___U3CU3Ef__amU24cache17_25; }
	inline void set_U3CU3Ef__amU24cache17_25(Action_t437523947 * value)
	{
		___U3CU3Ef__amU24cache17_25 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache17_25, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache18_26() { return static_cast<int32_t>(offsetof(UnityEventManager_t2255518493_StaticFields, ___U3CU3Ef__amU24cache18_26)); }
	inline Action_1_t359458046 * get_U3CU3Ef__amU24cache18_26() const { return ___U3CU3Ef__amU24cache18_26; }
	inline Action_1_t359458046 ** get_address_of_U3CU3Ef__amU24cache18_26() { return &___U3CU3Ef__amU24cache18_26; }
	inline void set_U3CU3Ef__amU24cache18_26(Action_1_t359458046 * value)
	{
		___U3CU3Ef__amU24cache18_26 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache18_26, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache19_27() { return static_cast<int32_t>(offsetof(UnityEventManager_t2255518493_StaticFields, ___U3CU3Ef__amU24cache19_27)); }
	inline Action_t437523947 * get_U3CU3Ef__amU24cache19_27() const { return ___U3CU3Ef__amU24cache19_27; }
	inline Action_t437523947 ** get_address_of_U3CU3Ef__amU24cache19_27() { return &___U3CU3Ef__amU24cache19_27; }
	inline void set_U3CU3Ef__amU24cache19_27(Action_t437523947 * value)
	{
		___U3CU3Ef__amU24cache19_27 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache19_27, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1A_28() { return static_cast<int32_t>(offsetof(UnityEventManager_t2255518493_StaticFields, ___U3CU3Ef__amU24cache1A_28)); }
	inline Action_t437523947 * get_U3CU3Ef__amU24cache1A_28() const { return ___U3CU3Ef__amU24cache1A_28; }
	inline Action_t437523947 ** get_address_of_U3CU3Ef__amU24cache1A_28() { return &___U3CU3Ef__amU24cache1A_28; }
	inline void set_U3CU3Ef__amU24cache1A_28(Action_t437523947 * value)
	{
		___U3CU3Ef__amU24cache1A_28 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1A_28, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1B_29() { return static_cast<int32_t>(offsetof(UnityEventManager_t2255518493_StaticFields, ___U3CU3Ef__amU24cache1B_29)); }
	inline Action_t437523947 * get_U3CU3Ef__amU24cache1B_29() const { return ___U3CU3Ef__amU24cache1B_29; }
	inline Action_t437523947 ** get_address_of_U3CU3Ef__amU24cache1B_29() { return &___U3CU3Ef__amU24cache1B_29; }
	inline void set_U3CU3Ef__amU24cache1B_29(Action_t437523947 * value)
	{
		___U3CU3Ef__amU24cache1B_29 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1B_29, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1C_30() { return static_cast<int32_t>(offsetof(UnityEventManager_t2255518493_StaticFields, ___U3CU3Ef__amU24cache1C_30)); }
	inline Action_1_t2995867492 * get_U3CU3Ef__amU24cache1C_30() const { return ___U3CU3Ef__amU24cache1C_30; }
	inline Action_1_t2995867492 ** get_address_of_U3CU3Ef__amU24cache1C_30() { return &___U3CU3Ef__amU24cache1C_30; }
	inline void set_U3CU3Ef__amU24cache1C_30(Action_1_t2995867492 * value)
	{
		___U3CU3Ef__amU24cache1C_30 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1C_30, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1D_31() { return static_cast<int32_t>(offsetof(UnityEventManager_t2255518493_StaticFields, ___U3CU3Ef__amU24cache1D_31)); }
	inline Action_1_t2995867492 * get_U3CU3Ef__amU24cache1D_31() const { return ___U3CU3Ef__amU24cache1D_31; }
	inline Action_1_t2995867492 ** get_address_of_U3CU3Ef__amU24cache1D_31() { return &___U3CU3Ef__amU24cache1D_31; }
	inline void set_U3CU3Ef__amU24cache1D_31(Action_1_t2995867492 * value)
	{
		___U3CU3Ef__amU24cache1D_31 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1D_31, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1E_32() { return static_cast<int32_t>(offsetof(UnityEventManager_t2255518493_StaticFields, ___U3CU3Ef__amU24cache1E_32)); }
	inline Action_t437523947 * get_U3CU3Ef__amU24cache1E_32() const { return ___U3CU3Ef__amU24cache1E_32; }
	inline Action_t437523947 ** get_address_of_U3CU3Ef__amU24cache1E_32() { return &___U3CU3Ef__amU24cache1E_32; }
	inline void set_U3CU3Ef__amU24cache1E_32(Action_t437523947 * value)
	{
		___U3CU3Ef__amU24cache1E_32 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1E_32, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1F_33() { return static_cast<int32_t>(offsetof(UnityEventManager_t2255518493_StaticFields, ___U3CU3Ef__amU24cache1F_33)); }
	inline Action_t437523947 * get_U3CU3Ef__amU24cache1F_33() const { return ___U3CU3Ef__amU24cache1F_33; }
	inline Action_t437523947 ** get_address_of_U3CU3Ef__amU24cache1F_33() { return &___U3CU3Ef__amU24cache1F_33; }
	inline void set_U3CU3Ef__amU24cache1F_33(Action_t437523947 * value)
	{
		___U3CU3Ef__amU24cache1F_33 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1F_33, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache20_34() { return static_cast<int32_t>(offsetof(UnityEventManager_t2255518493_StaticFields, ___U3CU3Ef__amU24cache20_34)); }
	inline Action_t437523947 * get_U3CU3Ef__amU24cache20_34() const { return ___U3CU3Ef__amU24cache20_34; }
	inline Action_t437523947 ** get_address_of_U3CU3Ef__amU24cache20_34() { return &___U3CU3Ef__amU24cache20_34; }
	inline void set_U3CU3Ef__amU24cache20_34(Action_t437523947 * value)
	{
		___U3CU3Ef__amU24cache20_34 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache20_34, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache21_35() { return static_cast<int32_t>(offsetof(UnityEventManager_t2255518493_StaticFields, ___U3CU3Ef__amU24cache21_35)); }
	inline Action_t437523947 * get_U3CU3Ef__amU24cache21_35() const { return ___U3CU3Ef__amU24cache21_35; }
	inline Action_t437523947 ** get_address_of_U3CU3Ef__amU24cache21_35() { return &___U3CU3Ef__amU24cache21_35; }
	inline void set_U3CU3Ef__amU24cache21_35(Action_t437523947 * value)
	{
		___U3CU3Ef__amU24cache21_35 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache21_35, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache22_36() { return static_cast<int32_t>(offsetof(UnityEventManager_t2255518493_StaticFields, ___U3CU3Ef__amU24cache22_36)); }
	inline Action_t437523947 * get_U3CU3Ef__amU24cache22_36() const { return ___U3CU3Ef__amU24cache22_36; }
	inline Action_t437523947 ** get_address_of_U3CU3Ef__amU24cache22_36() { return &___U3CU3Ef__amU24cache22_36; }
	inline void set_U3CU3Ef__amU24cache22_36(Action_t437523947 * value)
	{
		___U3CU3Ef__amU24cache22_36 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache22_36, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache23_37() { return static_cast<int32_t>(offsetof(UnityEventManager_t2255518493_StaticFields, ___U3CU3Ef__amU24cache23_37)); }
	inline Action_t437523947 * get_U3CU3Ef__amU24cache23_37() const { return ___U3CU3Ef__amU24cache23_37; }
	inline Action_t437523947 ** get_address_of_U3CU3Ef__amU24cache23_37() { return &___U3CU3Ef__amU24cache23_37; }
	inline void set_U3CU3Ef__amU24cache23_37(Action_t437523947 * value)
	{
		___U3CU3Ef__amU24cache23_37 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache23_37, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache24_38() { return static_cast<int32_t>(offsetof(UnityEventManager_t2255518493_StaticFields, ___U3CU3Ef__amU24cache24_38)); }
	inline Action_1_t3075561458 * get_U3CU3Ef__amU24cache24_38() const { return ___U3CU3Ef__amU24cache24_38; }
	inline Action_1_t3075561458 ** get_address_of_U3CU3Ef__amU24cache24_38() { return &___U3CU3Ef__amU24cache24_38; }
	inline void set_U3CU3Ef__amU24cache24_38(Action_1_t3075561458 * value)
	{
		___U3CU3Ef__amU24cache24_38 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache24_38, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache25_39() { return static_cast<int32_t>(offsetof(UnityEventManager_t2255518493_StaticFields, ___U3CU3Ef__amU24cache25_39)); }
	inline Action_t437523947 * get_U3CU3Ef__amU24cache25_39() const { return ___U3CU3Ef__amU24cache25_39; }
	inline Action_t437523947 ** get_address_of_U3CU3Ef__amU24cache25_39() { return &___U3CU3Ef__amU24cache25_39; }
	inline void set_U3CU3Ef__amU24cache25_39(Action_t437523947 * value)
	{
		___U3CU3Ef__amU24cache25_39 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache25_39, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache26_40() { return static_cast<int32_t>(offsetof(UnityEventManager_t2255518493_StaticFields, ___U3CU3Ef__amU24cache26_40)); }
	inline Action_t437523947 * get_U3CU3Ef__amU24cache26_40() const { return ___U3CU3Ef__amU24cache26_40; }
	inline Action_t437523947 ** get_address_of_U3CU3Ef__amU24cache26_40() { return &___U3CU3Ef__amU24cache26_40; }
	inline void set_U3CU3Ef__amU24cache26_40(Action_t437523947 * value)
	{
		___U3CU3Ef__amU24cache26_40 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache26_40, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache27_41() { return static_cast<int32_t>(offsetof(UnityEventManager_t2255518493_StaticFields, ___U3CU3Ef__amU24cache27_41)); }
	inline Action_t437523947 * get_U3CU3Ef__amU24cache27_41() const { return ___U3CU3Ef__amU24cache27_41; }
	inline Action_t437523947 ** get_address_of_U3CU3Ef__amU24cache27_41() { return &___U3CU3Ef__amU24cache27_41; }
	inline void set_U3CU3Ef__amU24cache27_41(Action_t437523947 * value)
	{
		___U3CU3Ef__amU24cache27_41 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache27_41, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
