﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1166220788MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::.ctor()
#define List_1__ctor_m1557393666(__this, method) ((  void (*) (List_1_t2985533912 *, const MethodInfo*))List_1__ctor_m153366876_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m3701354045(__this, ___collection0, method) ((  void (*) (List_1_t2985533912 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1069742499_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::.ctor(System.Int32)
#define List_1__ctor_m84292435(__this, ___capacity0, method) ((  void (*) (List_1_t2985533912 *, int32_t, const MethodInfo*))List_1__ctor_m1426504877_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::.cctor()
#define List_1__cctor_m552467179(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m4272276945_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m701410516(__this, method) ((  Il2CppObject* (*) (List_1_t2985533912 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3791632230_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m1249409666(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2985533912 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m692738920_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3639486077(__this, method) ((  Il2CppObject * (*) (List_1_t2985533912 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3149404407_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m3819179284(__this, ___item0, method) ((  int32_t (*) (List_1_t2985533912 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m1637521702_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m2922163640(__this, ___item0, method) ((  bool (*) (List_1_t2985533912 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2274035034_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m3643775340(__this, ___item0, method) ((  int32_t (*) (List_1_t2985533912 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m570281086_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m1859682455(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2985533912 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3861587953_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m2598567729(__this, ___item0, method) ((  void (*) (List_1_t2985533912 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3511774615_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m402970553(__this, method) ((  bool (*) (List_1_t2985533912 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1163627739_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m3774820004(__this, method) ((  bool (*) (List_1_t2985533912 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m1253514178_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m3455427664(__this, method) ((  Il2CppObject * (*) (List_1_t2985533912 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1185554932_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m3218540327(__this, method) ((  bool (*) (List_1_t2985533912 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m909131849_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m4021516018(__this, method) ((  bool (*) (List_1_t2985533912 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m1453166992_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m1721876951(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t2985533912 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m2217567867_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m1062396590(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2985533912 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m748231560_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::Add(T)
#define List_1_Add_m3781158717(__this, ___item0, method) ((  void (*) (List_1_t2985533912 *, Tuple_2_t2188574943 , const MethodInfo*))List_1_Add_m272184227_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m764155128(__this, ___newCount0, method) ((  void (*) (List_1_t2985533912 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m3820565086_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m3995765750(__this, ___collection0, method) ((  void (*) (List_1_t2985533912 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m1150849372_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m3256737974(__this, ___enumerable0, method) ((  void (*) (List_1_t2985533912 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m411821596_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m2098918433(__this, ___collection0, method) ((  void (*) (List_1_t2985533912 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m4092372731_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::AsReadOnly()
#define List_1_AsReadOnly_m916263016(__this, method) ((  ReadOnlyCollection_1_t1056752995 * (*) (List_1_t2985533912 *, const MethodInfo*))List_1_AsReadOnly_m2556280362_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::Clear()
#define List_1_Clear_m3258494253(__this, method) ((  void (*) (List_1_t2985533912 *, const MethodInfo*))List_1_Clear_m1854467463_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::Contains(T)
#define List_1_Contains_m1924955227(__this, ___item0, method) ((  bool (*) (List_1_t2985533912 *, Tuple_2_t2188574943 , const MethodInfo*))List_1_Contains_m1083072377_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m1955813869(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2985533912 *, Tuple_2U5BU5D_t2629098694*, int32_t, const MethodInfo*))List_1_CopyTo_m2249496787_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::Find(System.Predicate`1<T>)
#define List_1_Find_m755159707(__this, ___match0, method) ((  Tuple_2_t2188574943  (*) (List_1_t2985533912 *, Predicate_1_t2759538841 *, const MethodInfo*))List_1_Find_m2498671251_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m526426006(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t2759538841 *, const MethodInfo*))List_1_CheckMatch_m1918944112_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m3356701627(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t2985533912 *, int32_t, int32_t, Predicate_1_t2759538841 *, const MethodInfo*))List_1_GetIndex_m4010948685_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m997745546(__this, ___action0, method) ((  void (*) (List_1_t2985533912 *, Action_1_t2337027648 *, const MethodInfo*))List_1_ForEach_m1257173732_gshared)(__this, ___action0, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::GetEnumerator()
#define List_1_GetEnumerator_m3764964376(__this, method) ((  Enumerator_t1071316904  (*) (List_1_t2985533912 *, const MethodInfo*))List_1_GetEnumerator_m1101151286_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::IndexOf(T)
#define List_1_IndexOf_m2710278961(__this, ___item0, method) ((  int32_t (*) (List_1_t2985533912 *, Tuple_2_t2188574943 , const MethodInfo*))List_1_IndexOf_m3129767135_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m2883384132(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t2985533912 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m1958472746_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m1702181053(__this, ___index0, method) ((  void (*) (List_1_t2985533912 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1995863971_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::Insert(System.Int32,T)
#define List_1_Insert_m19466148(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2985533912 *, int32_t, Tuple_2_t2188574943 , const MethodInfo*))List_1_Insert_m4019767306_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m756791577(__this, ___collection0, method) ((  void (*) (List_1_t2985533912 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2686319871_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::Remove(T)
#define List_1_Remove_m2852820758(__this, ___item0, method) ((  bool (*) (List_1_t2985533912 *, Tuple_2_t2188574943 , const MethodInfo*))List_1_Remove_m1788258740_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m48525364(__this, ___match0, method) ((  int32_t (*) (List_1_t2985533912 *, Predicate_1_t2759538841 *, const MethodInfo*))List_1_RemoveAll_m1632418658_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m2188286314(__this, ___index0, method) ((  void (*) (List_1_t2985533912 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1893620176_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::Reverse()
#define List_1_Reverse_m2044505282(__this, method) ((  void (*) (List_1_t2985533912 *, const MethodInfo*))List_1_Reverse_m1394491036_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::Sort()
#define List_1_Sort_m12163616(__this, method) ((  void (*) (List_1_t2985533912 *, const MethodInfo*))List_1_Sort_m798156422_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m3248984004(__this, ___comparer0, method) ((  void (*) (List_1_t2985533912 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m1493626398_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m2246266483(__this, ___comparison0, method) ((  void (*) (List_1_t2985533912 *, Comparison_1_t597282523 *, const MethodInfo*))List_1_Sort_m1698605657_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::ToArray()
#define List_1_ToArray_m1591770369(__this, method) ((  Tuple_2U5BU5D_t2629098694* (*) (List_1_t2985533912 *, const MethodInfo*))List_1_ToArray_m632698357_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::TrimExcess()
#define List_1_TrimExcess_m3600064953(__this, method) ((  void (*) (List_1_t2985533912 *, const MethodInfo*))List_1_TrimExcess_m738232735_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::get_Capacity()
#define List_1_get_Capacity_m3730606817(__this, method) ((  int32_t (*) (List_1_t2985533912 *, const MethodInfo*))List_1_get_Capacity_m3818448271_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m2269311242(__this, ___value0, method) ((  void (*) (List_1_t2985533912 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1030753904_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::get_Count()
#define List_1_get_Count_m2231576682(__this, method) ((  int32_t (*) (List_1_t2985533912 *, const MethodInfo*))List_1_get_Count_m2350808188_gshared)(__this, method)
// T System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::get_Item(System.Int32)
#define List_1_get_Item_m3646894894(__this, ___index0, method) ((  Tuple_2_t2188574943  (*) (List_1_t2985533912 *, int32_t, const MethodInfo*))List_1_get_Item_m1762998326_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::set_Item(System.Int32,T)
#define List_1_set_Item_m3336695675(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2985533912 *, int32_t, Tuple_2_t2188574943 , const MethodInfo*))List_1_set_Item_m3630378593_gshared)(__this, ___index0, ___value1, method)
