﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Bounds>
struct ReactivePropertyObserver_t3754497547;
// UniRx.ReactiveProperty`1<UnityEngine.Bounds>
struct ReactiveProperty_1_t4126848016;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Bounds3518514978.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Bounds>::.ctor(UniRx.ReactiveProperty`1<T>)
extern "C"  void ReactivePropertyObserver__ctor_m4114333790_gshared (ReactivePropertyObserver_t3754497547 * __this, ReactiveProperty_1_t4126848016 * ___parent0, const MethodInfo* method);
#define ReactivePropertyObserver__ctor_m4114333790(__this, ___parent0, method) ((  void (*) (ReactivePropertyObserver_t3754497547 *, ReactiveProperty_1_t4126848016 *, const MethodInfo*))ReactivePropertyObserver__ctor_m4114333790_gshared)(__this, ___parent0, method)
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Bounds>::OnNext(T)
extern "C"  void ReactivePropertyObserver_OnNext_m1946176397_gshared (ReactivePropertyObserver_t3754497547 * __this, Bounds_t3518514978  ___value0, const MethodInfo* method);
#define ReactivePropertyObserver_OnNext_m1946176397(__this, ___value0, method) ((  void (*) (ReactivePropertyObserver_t3754497547 *, Bounds_t3518514978 , const MethodInfo*))ReactivePropertyObserver_OnNext_m1946176397_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Bounds>::OnError(System.Exception)
extern "C"  void ReactivePropertyObserver_OnError_m2430446748_gshared (ReactivePropertyObserver_t3754497547 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ReactivePropertyObserver_OnError_m2430446748(__this, ___error0, method) ((  void (*) (ReactivePropertyObserver_t3754497547 *, Exception_t1967233988 *, const MethodInfo*))ReactivePropertyObserver_OnError_m2430446748_gshared)(__this, ___error0, method)
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Bounds>::OnCompleted()
extern "C"  void ReactivePropertyObserver_OnCompleted_m4055198191_gshared (ReactivePropertyObserver_t3754497547 * __this, const MethodInfo* method);
#define ReactivePropertyObserver_OnCompleted_m4055198191(__this, method) ((  void (*) (ReactivePropertyObserver_t3754497547 *, const MethodInfo*))ReactivePropertyObserver_OnCompleted_m4055198191_gshared)(__this, method)
