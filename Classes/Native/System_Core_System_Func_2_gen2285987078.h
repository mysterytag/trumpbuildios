﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_MulticastDelegate2585444626.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen3445990771.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`2<UniRx.Tuple`2<System.String,System.String>,System.Boolean>
struct  Func_2_t2285987078  : public MulticastDelegate_t2585444626
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
