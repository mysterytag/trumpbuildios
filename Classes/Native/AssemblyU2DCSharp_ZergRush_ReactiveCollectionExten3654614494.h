﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ZergRush.IReactiveCollection`1<System.Object>
struct IReactiveCollection_1_t4089814762;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t1509682273;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZergRush.ReactiveCollectionExtension/FilterCollectionRX`1<System.Object>
struct  FilterCollectionRX_1_t3654614494  : public Il2CppObject
{
public:
	// ZergRush.IReactiveCollection`1<T> ZergRush.ReactiveCollectionExtension/FilterCollectionRX`1::parent
	Il2CppObject* ___parent_0;
	// System.Func`2<T,System.Boolean> ZergRush.ReactiveCollectionExtension/FilterCollectionRX`1::predicate
	Func_2_t1509682273 * ___predicate_1;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(FilterCollectionRX_1_t3654614494, ___parent_0)); }
	inline Il2CppObject* get_parent_0() const { return ___parent_0; }
	inline Il2CppObject** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(Il2CppObject* value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier(&___parent_0, value);
	}

	inline static int32_t get_offset_of_predicate_1() { return static_cast<int32_t>(offsetof(FilterCollectionRX_1_t3654614494, ___predicate_1)); }
	inline Func_2_t1509682273 * get_predicate_1() const { return ___predicate_1; }
	inline Func_2_t1509682273 ** get_address_of_predicate_1() { return &___predicate_1; }
	inline void set_predicate_1(Func_2_t1509682273 * value)
	{
		___predicate_1 = value;
		Il2CppCodeGenWriteBarrier(&___predicate_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
