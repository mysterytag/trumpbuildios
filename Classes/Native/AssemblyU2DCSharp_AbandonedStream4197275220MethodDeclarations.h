﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AbandonedStream
struct AbandonedStream_t4197275220;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action
struct Action_t437523947;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"

// System.Void AbandonedStream::.ctor()
extern "C"  void AbandonedStream__ctor_m3467923351 (AbandonedStream_t4197275220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable AbandonedStream::Listen(System.Action,Priority)
extern "C"  Il2CppObject * AbandonedStream_Listen_m3609329542 (AbandonedStream_t4197275220 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
