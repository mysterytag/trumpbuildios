﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<GetCloudScriptUrl>c__AnonStoreyC3
struct U3CGetCloudScriptUrlU3Ec__AnonStoreyC3_t3889768482;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<GetCloudScriptUrl>c__AnonStoreyC3::.ctor()
extern "C"  void U3CGetCloudScriptUrlU3Ec__AnonStoreyC3__ctor_m65800561 (U3CGetCloudScriptUrlU3Ec__AnonStoreyC3_t3889768482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<GetCloudScriptUrl>c__AnonStoreyC3::<>m__B0(PlayFab.CallRequestContainer)
extern "C"  void U3CGetCloudScriptUrlU3Ec__AnonStoreyC3_U3CU3Em__B0_m3859267645 (U3CGetCloudScriptUrlU3Ec__AnonStoreyC3_t3889768482 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
