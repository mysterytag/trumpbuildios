﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GrapRotation
struct GrapRotation_t3746846744;

#include "codegen/il2cpp-codegen.h"

// System.Void GrapRotation::.ctor()
extern "C"  void GrapRotation__ctor_m3434689923 (GrapRotation_t3746846744 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GrapRotation::Start()
extern "C"  void GrapRotation_Start_m2381827715 (GrapRotation_t3746846744 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GrapRotation::Update()
extern "C"  void GrapRotation_Update_m828067306 (GrapRotation_t3746846744 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
