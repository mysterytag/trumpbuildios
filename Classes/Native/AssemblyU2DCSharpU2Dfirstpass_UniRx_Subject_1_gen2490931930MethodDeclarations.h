﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen2775141040MethodDeclarations.h"

// System.Void UniRx.Subject`1<UnityEngine.EventSystems.AxisEventData>::.ctor()
#define Subject_1__ctor_m2671033414(__this, method) ((  void (*) (Subject_1_t2490931930 *, const MethodInfo*))Subject_1__ctor_m3752525464_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UnityEngine.EventSystems.AxisEventData>::get_HasObservers()
#define Subject_1_get_HasObservers_m1269593038(__this, method) ((  bool (*) (Subject_1_t2490931930 *, const MethodInfo*))Subject_1_get_HasObservers_m2673131508_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.EventSystems.AxisEventData>::OnCompleted()
#define Subject_1_OnCompleted_m1670849360(__this, method) ((  void (*) (Subject_1_t2490931930 *, const MethodInfo*))Subject_1_OnCompleted_m1083367458_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.EventSystems.AxisEventData>::OnError(System.Exception)
#define Subject_1_OnError_m1217879165(__this, ___error0, method) ((  void (*) (Subject_1_t2490931930 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m1700032079_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<UnityEngine.EventSystems.AxisEventData>::OnNext(T)
#define Subject_1_OnNext_m1303371118(__this, ___value0, method) ((  void (*) (Subject_1_t2490931930 *, AxisEventData_t552897310 *, const MethodInfo*))Subject_1_OnNext_m1235145536_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<UnityEngine.EventSystems.AxisEventData>::Subscribe(UniRx.IObserver`1<T>)
#define Subject_1_Subscribe_m2896473371(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t2490931930 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m2428743831_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<UnityEngine.EventSystems.AxisEventData>::Dispose()
#define Subject_1_Dispose_m2665918211(__this, method) ((  void (*) (Subject_1_t2490931930 *, const MethodInfo*))Subject_1_Dispose_m2597692629_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.EventSystems.AxisEventData>::ThrowIfDisposed()
#define Subject_1_ThrowIfDisposed_m3971130764(__this, method) ((  void (*) (Subject_1_t2490931930 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m956279134_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UnityEngine.EventSystems.AxisEventData>::IsRequiredSubscribeOnCurrentThread()
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m379373893(__this, method) ((  bool (*) (Subject_1_t2490931930 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m3969161195_gshared)(__this, method)
