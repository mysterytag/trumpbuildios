﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameDataDescriptor
struct GameDataDescriptor_t594253675;

#include "codegen/il2cpp-codegen.h"

// System.Void GameDataDescriptor::.ctor()
extern "C"  void GameDataDescriptor__ctor_m3381039504 (GameDataDescriptor_t594253675 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
