﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// <>__AnonType2`2<System.Boolean,System.Boolean>
struct U3CU3E__AnonType2_2_t3656635463;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void <>__AnonType2`2<System.Boolean,System.Boolean>::.ctor(<hidden>__T,<showTable>__T)
extern "C"  void U3CU3E__AnonType2_2__ctor_m1934779086_gshared (U3CU3E__AnonType2_2_t3656635463 * __this, bool ___hidden0, bool ___showTable1, const MethodInfo* method);
#define U3CU3E__AnonType2_2__ctor_m1934779086(__this, ___hidden0, ___showTable1, method) ((  void (*) (U3CU3E__AnonType2_2_t3656635463 *, bool, bool, const MethodInfo*))U3CU3E__AnonType2_2__ctor_m1934779086_gshared)(__this, ___hidden0, ___showTable1, method)
// <hidden>__T <>__AnonType2`2<System.Boolean,System.Boolean>::get_hidden()
extern "C"  bool U3CU3E__AnonType2_2_get_hidden_m2280471269_gshared (U3CU3E__AnonType2_2_t3656635463 * __this, const MethodInfo* method);
#define U3CU3E__AnonType2_2_get_hidden_m2280471269(__this, method) ((  bool (*) (U3CU3E__AnonType2_2_t3656635463 *, const MethodInfo*))U3CU3E__AnonType2_2_get_hidden_m2280471269_gshared)(__this, method)
// <showTable>__T <>__AnonType2`2<System.Boolean,System.Boolean>::get_showTable()
extern "C"  bool U3CU3E__AnonType2_2_get_showTable_m1407594909_gshared (U3CU3E__AnonType2_2_t3656635463 * __this, const MethodInfo* method);
#define U3CU3E__AnonType2_2_get_showTable_m1407594909(__this, method) ((  bool (*) (U3CU3E__AnonType2_2_t3656635463 *, const MethodInfo*))U3CU3E__AnonType2_2_get_showTable_m1407594909_gshared)(__this, method)
// System.Boolean <>__AnonType2`2<System.Boolean,System.Boolean>::Equals(System.Object)
extern "C"  bool U3CU3E__AnonType2_2_Equals_m2755731654_gshared (U3CU3E__AnonType2_2_t3656635463 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define U3CU3E__AnonType2_2_Equals_m2755731654(__this, ___obj0, method) ((  bool (*) (U3CU3E__AnonType2_2_t3656635463 *, Il2CppObject *, const MethodInfo*))U3CU3E__AnonType2_2_Equals_m2755731654_gshared)(__this, ___obj0, method)
// System.Int32 <>__AnonType2`2<System.Boolean,System.Boolean>::GetHashCode()
extern "C"  int32_t U3CU3E__AnonType2_2_GetHashCode_m2028821354_gshared (U3CU3E__AnonType2_2_t3656635463 * __this, const MethodInfo* method);
#define U3CU3E__AnonType2_2_GetHashCode_m2028821354(__this, method) ((  int32_t (*) (U3CU3E__AnonType2_2_t3656635463 *, const MethodInfo*))U3CU3E__AnonType2_2_GetHashCode_m2028821354_gshared)(__this, method)
// System.String <>__AnonType2`2<System.Boolean,System.Boolean>::ToString()
extern "C"  String_t* U3CU3E__AnonType2_2_ToString_m3963165226_gshared (U3CU3E__AnonType2_2_t3656635463 * __this, const MethodInfo* method);
#define U3CU3E__AnonType2_2_ToString_m3963165226(__this, method) ((  String_t* (*) (U3CU3E__AnonType2_2_t3656635463 *, const MethodInfo*))U3CU3E__AnonType2_2_ToString_m3963165226_gshared)(__this, method)
