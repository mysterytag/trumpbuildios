﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ModestTree.TypeExtensions/<GetAllProperties>c__Iterator40
struct U3CGetAllPropertiesU3Ec__Iterator40_t3557131433;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<System.Reflection.PropertyInfo>
struct IEnumerator_1_t2973654817;

#include "codegen/il2cpp-codegen.h"

// System.Void ModestTree.TypeExtensions/<GetAllProperties>c__Iterator40::.ctor()
extern "C"  void U3CGetAllPropertiesU3Ec__Iterator40__ctor_m2990873591 (U3CGetAllPropertiesU3Ec__Iterator40_t3557131433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.PropertyInfo ModestTree.TypeExtensions/<GetAllProperties>c__Iterator40::System.Collections.Generic.IEnumerator<System.Reflection.PropertyInfo>.get_Current()
extern "C"  PropertyInfo_t * U3CGetAllPropertiesU3Ec__Iterator40_System_Collections_Generic_IEnumeratorU3CSystem_Reflection_PropertyInfoU3E_get_Current_m1430794738 (U3CGetAllPropertiesU3Ec__Iterator40_t3557131433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ModestTree.TypeExtensions/<GetAllProperties>c__Iterator40::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetAllPropertiesU3Ec__Iterator40_System_Collections_IEnumerator_get_Current_m4088692111 (U3CGetAllPropertiesU3Ec__Iterator40_t3557131433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ModestTree.TypeExtensions/<GetAllProperties>c__Iterator40::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CGetAllPropertiesU3Ec__Iterator40_System_Collections_IEnumerable_GetEnumerator_m1184006538 (U3CGetAllPropertiesU3Ec__Iterator40_t3557131433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Reflection.PropertyInfo> ModestTree.TypeExtensions/<GetAllProperties>c__Iterator40::System.Collections.Generic.IEnumerable<System.Reflection.PropertyInfo>.GetEnumerator()
extern "C"  Il2CppObject* U3CGetAllPropertiesU3Ec__Iterator40_System_Collections_Generic_IEnumerableU3CSystem_Reflection_PropertyInfoU3E_GetEnumerator_m3135099747 (U3CGetAllPropertiesU3Ec__Iterator40_t3557131433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ModestTree.TypeExtensions/<GetAllProperties>c__Iterator40::MoveNext()
extern "C"  bool U3CGetAllPropertiesU3Ec__Iterator40_MoveNext_m1070121885 (U3CGetAllPropertiesU3Ec__Iterator40_t3557131433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModestTree.TypeExtensions/<GetAllProperties>c__Iterator40::Dispose()
extern "C"  void U3CGetAllPropertiesU3Ec__Iterator40_Dispose_m794682996 (U3CGetAllPropertiesU3Ec__Iterator40_t3557131433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModestTree.TypeExtensions/<GetAllProperties>c__Iterator40::Reset()
extern "C"  void U3CGetAllPropertiesU3Ec__Iterator40_Reset_m637306532 (U3CGetAllPropertiesU3Ec__Iterator40_t3557131433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
