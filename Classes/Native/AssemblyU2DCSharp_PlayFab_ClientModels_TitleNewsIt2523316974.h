﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_DateTime339033936.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.TitleNewsItem
struct  TitleNewsItem_t2523316974  : public Il2CppObject
{
public:
	// System.DateTime PlayFab.ClientModels.TitleNewsItem::<Timestamp>k__BackingField
	DateTime_t339033936  ___U3CTimestampU3Ek__BackingField_0;
	// System.String PlayFab.ClientModels.TitleNewsItem::<NewsId>k__BackingField
	String_t* ___U3CNewsIdU3Ek__BackingField_1;
	// System.String PlayFab.ClientModels.TitleNewsItem::<Title>k__BackingField
	String_t* ___U3CTitleU3Ek__BackingField_2;
	// System.String PlayFab.ClientModels.TitleNewsItem::<Body>k__BackingField
	String_t* ___U3CBodyU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CTimestampU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TitleNewsItem_t2523316974, ___U3CTimestampU3Ek__BackingField_0)); }
	inline DateTime_t339033936  get_U3CTimestampU3Ek__BackingField_0() const { return ___U3CTimestampU3Ek__BackingField_0; }
	inline DateTime_t339033936 * get_address_of_U3CTimestampU3Ek__BackingField_0() { return &___U3CTimestampU3Ek__BackingField_0; }
	inline void set_U3CTimestampU3Ek__BackingField_0(DateTime_t339033936  value)
	{
		___U3CTimestampU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CNewsIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TitleNewsItem_t2523316974, ___U3CNewsIdU3Ek__BackingField_1)); }
	inline String_t* get_U3CNewsIdU3Ek__BackingField_1() const { return ___U3CNewsIdU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CNewsIdU3Ek__BackingField_1() { return &___U3CNewsIdU3Ek__BackingField_1; }
	inline void set_U3CNewsIdU3Ek__BackingField_1(String_t* value)
	{
		___U3CNewsIdU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CNewsIdU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CTitleU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TitleNewsItem_t2523316974, ___U3CTitleU3Ek__BackingField_2)); }
	inline String_t* get_U3CTitleU3Ek__BackingField_2() const { return ___U3CTitleU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CTitleU3Ek__BackingField_2() { return &___U3CTitleU3Ek__BackingField_2; }
	inline void set_U3CTitleU3Ek__BackingField_2(String_t* value)
	{
		___U3CTitleU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTitleU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CBodyU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(TitleNewsItem_t2523316974, ___U3CBodyU3Ek__BackingField_3)); }
	inline String_t* get_U3CBodyU3Ek__BackingField_3() const { return ___U3CBodyU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CBodyU3Ek__BackingField_3() { return &___U3CBodyU3Ek__BackingField_3; }
	inline void set_U3CBodyU3Ek__BackingField_3(String_t* value)
	{
		___U3CBodyU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CBodyU3Ek__BackingField_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
