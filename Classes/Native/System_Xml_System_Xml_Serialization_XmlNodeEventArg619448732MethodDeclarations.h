﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.Serialization.XmlNodeEventArgs
struct XmlNodeEventArgs_t619448732;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "System_Xml_System_Xml_XmlNodeType3966624571.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Xml.Serialization.XmlNodeEventArgs::.ctor(System.Int32,System.Int32,System.String,System.String,System.String,System.Xml.XmlNodeType,System.Object,System.String)
extern "C"  void XmlNodeEventArgs__ctor_m490603432 (XmlNodeEventArgs_t619448732 * __this, int32_t ___linenumber0, int32_t ___lineposition1, String_t* ___localname2, String_t* ___name3, String_t* ___nsuri4, int32_t ___nodetype5, Il2CppObject * ___source6, String_t* ___text7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
