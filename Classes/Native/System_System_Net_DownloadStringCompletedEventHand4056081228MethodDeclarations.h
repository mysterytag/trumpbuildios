﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.DownloadStringCompletedEventHandler
struct DownloadStringCompletedEventHandler_t4056081228;
// System.Object
struct Il2CppObject;
// System.Net.DownloadStringCompletedEventArgs
struct DownloadStringCompletedEventArgs_t4050086575;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "System_System_Net_DownloadStringCompletedEventArgs4050086575.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Net.DownloadStringCompletedEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void DownloadStringCompletedEventHandler__ctor_m1526234648 (DownloadStringCompletedEventHandler_t4056081228 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.DownloadStringCompletedEventHandler::Invoke(System.Object,System.Net.DownloadStringCompletedEventArgs)
extern "C"  void DownloadStringCompletedEventHandler_Invoke_m1773329369 (DownloadStringCompletedEventHandler_t4056081228 * __this, Il2CppObject * ___sender0, DownloadStringCompletedEventArgs_t4050086575 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_DownloadStringCompletedEventHandler_t4056081228(Il2CppObject* delegate, Il2CppObject * ___sender0, DownloadStringCompletedEventArgs_t4050086575 * ___e1);
// System.IAsyncResult System.Net.DownloadStringCompletedEventHandler::BeginInvoke(System.Object,System.Net.DownloadStringCompletedEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DownloadStringCompletedEventHandler_BeginInvoke_m433017132 (DownloadStringCompletedEventHandler_t4056081228 * __this, Il2CppObject * ___sender0, DownloadStringCompletedEventArgs_t4050086575 * ___e1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.DownloadStringCompletedEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void DownloadStringCompletedEventHandler_EndInvoke_m3568221224 (DownloadStringCompletedEventHandler_t4056081228 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
