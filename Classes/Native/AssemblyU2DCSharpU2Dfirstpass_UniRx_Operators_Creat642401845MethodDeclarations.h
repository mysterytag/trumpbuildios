﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.CreateObservable`1/Create<System.Object>
struct Create_t642401845;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.CreateObservable`1/Create<System.Object>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Create__ctor_m3386042515_gshared (Create_t642401845 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define Create__ctor_m3386042515(__this, ___observer0, ___cancel1, method) ((  void (*) (Create_t642401845 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Create__ctor_m3386042515_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.CreateObservable`1/Create<System.Object>::OnNext(T)
extern "C"  void Create_OnNext_m313289503_gshared (Create_t642401845 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Create_OnNext_m313289503(__this, ___value0, method) ((  void (*) (Create_t642401845 *, Il2CppObject *, const MethodInfo*))Create_OnNext_m313289503_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.CreateObservable`1/Create<System.Object>::OnError(System.Exception)
extern "C"  void Create_OnError_m3930875950_gshared (Create_t642401845 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Create_OnError_m3930875950(__this, ___error0, method) ((  void (*) (Create_t642401845 *, Exception_t1967233988 *, const MethodInfo*))Create_OnError_m3930875950_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.CreateObservable`1/Create<System.Object>::OnCompleted()
extern "C"  void Create_OnCompleted_m390295681_gshared (Create_t642401845 * __this, const MethodInfo* method);
#define Create_OnCompleted_m390295681(__this, method) ((  void (*) (Create_t642401845 *, const MethodInfo*))Create_OnCompleted_m390295681_gshared)(__this, method)
