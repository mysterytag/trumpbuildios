﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.Single>
struct Action_1_t1106661726;

#include "AssemblyU2DCSharp_FiniteTimeProc2933053874.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChronoLerp
struct  ChronoLerp_t521452250  : public FiniteTimeProc_t2933053874
{
public:
	// System.Single ChronoLerp::from
	float ___from_2;
	// System.Single ChronoLerp::to
	float ___to_3;
	// System.Action`1<System.Single> ChronoLerp::action
	Action_1_t1106661726 * ___action_4;

public:
	inline static int32_t get_offset_of_from_2() { return static_cast<int32_t>(offsetof(ChronoLerp_t521452250, ___from_2)); }
	inline float get_from_2() const { return ___from_2; }
	inline float* get_address_of_from_2() { return &___from_2; }
	inline void set_from_2(float value)
	{
		___from_2 = value;
	}

	inline static int32_t get_offset_of_to_3() { return static_cast<int32_t>(offsetof(ChronoLerp_t521452250, ___to_3)); }
	inline float get_to_3() const { return ___to_3; }
	inline float* get_address_of_to_3() { return &___to_3; }
	inline void set_to_3(float value)
	{
		___to_3 = value;
	}

	inline static int32_t get_offset_of_action_4() { return static_cast<int32_t>(offsetof(ChronoLerp_t521452250, ___action_4)); }
	inline Action_1_t1106661726 * get_action_4() const { return ___action_4; }
	inline Action_1_t1106661726 ** get_address_of_action_4() { return &___action_4; }
	inline void set_action_4(Action_1_t1106661726 * value)
	{
		___action_4 = value;
		Il2CppCodeGenWriteBarrier(&___action_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
