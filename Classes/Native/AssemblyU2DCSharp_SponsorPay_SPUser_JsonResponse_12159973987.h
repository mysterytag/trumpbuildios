﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "AssemblyU2DCSharp_SponsorPay_AbstractResponse2918001245.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SponsorPay.SPUser/JsonResponse`1<System.Object>
struct  JsonResponse_1_t2159973987  : public AbstractResponse_t2918001245
{
public:
	// System.String SponsorPay.SPUser/JsonResponse`1::<key>k__BackingField
	String_t* ___U3CkeyU3Ek__BackingField_1;
	// T SponsorPay.SPUser/JsonResponse`1::<value>k__BackingField
	Il2CppObject * ___U3CvalueU3Ek__BackingField_2;
	// System.String SponsorPay.SPUser/JsonResponse`1::<error>k__BackingField
	String_t* ___U3CerrorU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CkeyU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(JsonResponse_1_t2159973987, ___U3CkeyU3Ek__BackingField_1)); }
	inline String_t* get_U3CkeyU3Ek__BackingField_1() const { return ___U3CkeyU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CkeyU3Ek__BackingField_1() { return &___U3CkeyU3Ek__BackingField_1; }
	inline void set_U3CkeyU3Ek__BackingField_1(String_t* value)
	{
		___U3CkeyU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CkeyU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CvalueU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(JsonResponse_1_t2159973987, ___U3CvalueU3Ek__BackingField_2)); }
	inline Il2CppObject * get_U3CvalueU3Ek__BackingField_2() const { return ___U3CvalueU3Ek__BackingField_2; }
	inline Il2CppObject ** get_address_of_U3CvalueU3Ek__BackingField_2() { return &___U3CvalueU3Ek__BackingField_2; }
	inline void set_U3CvalueU3Ek__BackingField_2(Il2CppObject * value)
	{
		___U3CvalueU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CvalueU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CerrorU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(JsonResponse_1_t2159973987, ___U3CerrorU3Ek__BackingField_3)); }
	inline String_t* get_U3CerrorU3Ek__BackingField_3() const { return ___U3CerrorU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CerrorU3Ek__BackingField_3() { return &___U3CerrorU3Ek__BackingField_3; }
	inline void set_U3CerrorU3Ek__BackingField_3(String_t* value)
	{
		___U3CerrorU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CerrorU3Ek__BackingField_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
