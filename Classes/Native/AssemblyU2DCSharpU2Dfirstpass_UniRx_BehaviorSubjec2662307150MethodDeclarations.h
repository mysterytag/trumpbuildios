﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.BehaviorSubject`1<System.Object>
struct BehaviorSubject_1_t2662307150;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.BehaviorSubject`1<System.Object>::.ctor(T)
extern "C"  void BehaviorSubject_1__ctor_m4100827064_gshared (BehaviorSubject_1_t2662307150 * __this, Il2CppObject * ___defaultValue0, const MethodInfo* method);
#define BehaviorSubject_1__ctor_m4100827064(__this, ___defaultValue0, method) ((  void (*) (BehaviorSubject_1_t2662307150 *, Il2CppObject *, const MethodInfo*))BehaviorSubject_1__ctor_m4100827064_gshared)(__this, ___defaultValue0, method)
// T UniRx.BehaviorSubject`1<System.Object>::get_Value()
extern "C"  Il2CppObject * BehaviorSubject_1_get_Value_m3483490925_gshared (BehaviorSubject_1_t2662307150 * __this, const MethodInfo* method);
#define BehaviorSubject_1_get_Value_m3483490925(__this, method) ((  Il2CppObject * (*) (BehaviorSubject_1_t2662307150 *, const MethodInfo*))BehaviorSubject_1_get_Value_m3483490925_gshared)(__this, method)
// System.Boolean UniRx.BehaviorSubject`1<System.Object>::get_HasObservers()
extern "C"  bool BehaviorSubject_1_get_HasObservers_m3919047526_gshared (BehaviorSubject_1_t2662307150 * __this, const MethodInfo* method);
#define BehaviorSubject_1_get_HasObservers_m3919047526(__this, method) ((  bool (*) (BehaviorSubject_1_t2662307150 *, const MethodInfo*))BehaviorSubject_1_get_HasObservers_m3919047526_gshared)(__this, method)
// System.Void UniRx.BehaviorSubject`1<System.Object>::OnCompleted()
extern "C"  void BehaviorSubject_1_OnCompleted_m1128724848_gshared (BehaviorSubject_1_t2662307150 * __this, const MethodInfo* method);
#define BehaviorSubject_1_OnCompleted_m1128724848(__this, method) ((  void (*) (BehaviorSubject_1_t2662307150 *, const MethodInfo*))BehaviorSubject_1_OnCompleted_m1128724848_gshared)(__this, method)
// System.Void UniRx.BehaviorSubject`1<System.Object>::OnError(System.Exception)
extern "C"  void BehaviorSubject_1_OnError_m1905722013_gshared (BehaviorSubject_1_t2662307150 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define BehaviorSubject_1_OnError_m1905722013(__this, ___error0, method) ((  void (*) (BehaviorSubject_1_t2662307150 *, Exception_t1967233988 *, const MethodInfo*))BehaviorSubject_1_OnError_m1905722013_gshared)(__this, ___error0, method)
// System.Void UniRx.BehaviorSubject`1<System.Object>::OnNext(T)
extern "C"  void BehaviorSubject_1_OnNext_m1107280782_gshared (BehaviorSubject_1_t2662307150 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define BehaviorSubject_1_OnNext_m1107280782(__this, ___value0, method) ((  void (*) (BehaviorSubject_1_t2662307150 *, Il2CppObject *, const MethodInfo*))BehaviorSubject_1_OnNext_m1107280782_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.BehaviorSubject`1<System.Object>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * BehaviorSubject_1_Subscribe_m3151764837_gshared (BehaviorSubject_1_t2662307150 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define BehaviorSubject_1_Subscribe_m3151764837(__this, ___observer0, method) ((  Il2CppObject * (*) (BehaviorSubject_1_t2662307150 *, Il2CppObject*, const MethodInfo*))BehaviorSubject_1_Subscribe_m3151764837_gshared)(__this, ___observer0, method)
// System.Void UniRx.BehaviorSubject`1<System.Object>::Dispose()
extern "C"  void BehaviorSubject_1_Dispose_m2469827875_gshared (BehaviorSubject_1_t2662307150 * __this, const MethodInfo* method);
#define BehaviorSubject_1_Dispose_m2469827875(__this, method) ((  void (*) (BehaviorSubject_1_t2662307150 *, const MethodInfo*))BehaviorSubject_1_Dispose_m2469827875_gshared)(__this, method)
// System.Void UniRx.BehaviorSubject`1<System.Object>::ThrowIfDisposed()
extern "C"  void BehaviorSubject_1_ThrowIfDisposed_m642411436_gshared (BehaviorSubject_1_t2662307150 * __this, const MethodInfo* method);
#define BehaviorSubject_1_ThrowIfDisposed_m642411436(__this, method) ((  void (*) (BehaviorSubject_1_t2662307150 *, const MethodInfo*))BehaviorSubject_1_ThrowIfDisposed_m642411436_gshared)(__this, method)
// System.Boolean UniRx.BehaviorSubject`1<System.Object>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool BehaviorSubject_1_IsRequiredSubscribeOnCurrentThread_m2153941725_gshared (BehaviorSubject_1_t2662307150 * __this, const MethodInfo* method);
#define BehaviorSubject_1_IsRequiredSubscribeOnCurrentThread_m2153941725(__this, method) ((  bool (*) (BehaviorSubject_1_t2662307150 *, const MethodInfo*))BehaviorSubject_1_IsRequiredSubscribeOnCurrentThread_m2153941725_gshared)(__this, method)
