﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Scheduler/<Schedule>c__AnonStorey73
struct U3CScheduleU3Ec__AnonStorey73_t3913535384;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Scheduler/<Schedule>c__AnonStorey73::.ctor()
extern "C"  void U3CScheduleU3Ec__AnonStorey73__ctor_m2260222539 (U3CScheduleU3Ec__AnonStorey73_t3913535384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Scheduler/<Schedule>c__AnonStorey73::<>m__97()
extern "C"  void U3CScheduleU3Ec__AnonStorey73_U3CU3Em__97_m1128693234 (U3CScheduleU3Ec__AnonStorey73_t3913535384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Scheduler/<Schedule>c__AnonStorey73::<>m__9A()
extern "C"  void U3CScheduleU3Ec__AnonStorey73_U3CU3Em__9A_m1128702844 (U3CScheduleU3Ec__AnonStorey73_t3913535384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
