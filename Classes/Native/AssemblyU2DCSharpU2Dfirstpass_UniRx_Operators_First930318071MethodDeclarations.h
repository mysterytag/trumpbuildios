﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.FirstObservable`1/First<System.Int64>
struct First_t930318071;
// UniRx.Operators.FirstObservable`1<System.Int64>
struct FirstObservable_1_t575002000;
// UniRx.IObserver`1<System.Int64>
struct IObserver_1_t764446489;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.FirstObservable`1/First<System.Int64>::.ctor(UniRx.Operators.FirstObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void First__ctor_m2704542496_gshared (First_t930318071 * __this, FirstObservable_1_t575002000 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define First__ctor_m2704542496(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (First_t930318071 *, FirstObservable_1_t575002000 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))First__ctor_m2704542496_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.FirstObservable`1/First<System.Int64>::OnNext(T)
extern "C"  void First_OnNext_m810768445_gshared (First_t930318071 * __this, int64_t ___value0, const MethodInfo* method);
#define First_OnNext_m810768445(__this, ___value0, method) ((  void (*) (First_t930318071 *, int64_t, const MethodInfo*))First_OnNext_m810768445_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.FirstObservable`1/First<System.Int64>::OnError(System.Exception)
extern "C"  void First_OnError_m2992724300_gshared (First_t930318071 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define First_OnError_m2992724300(__this, ___error0, method) ((  void (*) (First_t930318071 *, Exception_t1967233988 *, const MethodInfo*))First_OnError_m2992724300_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.FirstObservable`1/First<System.Int64>::OnCompleted()
extern "C"  void First_OnCompleted_m4283604639_gshared (First_t930318071 * __this, const MethodInfo* method);
#define First_OnCompleted_m4283604639(__this, method) ((  void (*) (First_t930318071 *, const MethodInfo*))First_OnCompleted_m4283604639_gshared)(__this, method)
