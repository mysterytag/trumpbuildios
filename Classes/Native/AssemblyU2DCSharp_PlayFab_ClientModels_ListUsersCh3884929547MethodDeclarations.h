﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.ListUsersCharactersRequest
struct ListUsersCharactersRequest_t3884929547;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.ListUsersCharactersRequest::.ctor()
extern "C"  void ListUsersCharactersRequest__ctor_m1969106450 (ListUsersCharactersRequest_t3884929547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.ListUsersCharactersRequest::get_PlayFabId()
extern "C"  String_t* ListUsersCharactersRequest_get_PlayFabId_m3394880280 (ListUsersCharactersRequest_t3884929547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ListUsersCharactersRequest::set_PlayFabId(System.String)
extern "C"  void ListUsersCharactersRequest_set_PlayFabId_m319638689 (ListUsersCharactersRequest_t3884929547 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
