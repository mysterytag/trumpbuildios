﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>>
struct ImmutableList_1_t1356806292;
// UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>[]
struct IObserver_1U5BU5D_t1093590812;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>>
struct  ImmutableList_1_t1356806292  : public Il2CppObject
{
public:
	// T[] UniRx.InternalUtil.ImmutableList`1::data
	IObserver_1U5BU5D_t1093590812* ___data_1;

public:
	inline static int32_t get_offset_of_data_1() { return static_cast<int32_t>(offsetof(ImmutableList_1_t1356806292, ___data_1)); }
	inline IObserver_1U5BU5D_t1093590812* get_data_1() const { return ___data_1; }
	inline IObserver_1U5BU5D_t1093590812** get_address_of_data_1() { return &___data_1; }
	inline void set_data_1(IObserver_1U5BU5D_t1093590812* value)
	{
		___data_1 = value;
		Il2CppCodeGenWriteBarrier(&___data_1, value);
	}
};

struct ImmutableList_1_t1356806292_StaticFields
{
public:
	// UniRx.InternalUtil.ImmutableList`1<T> UniRx.InternalUtil.ImmutableList`1::Empty
	ImmutableList_1_t1356806292 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(ImmutableList_1_t1356806292_StaticFields, ___Empty_0)); }
	inline ImmutableList_1_t1356806292 * get_Empty_0() const { return ___Empty_0; }
	inline ImmutableList_1_t1356806292 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(ImmutableList_1_t1356806292 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier(&___Empty_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
