﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.AmbObservable`1/AmbOuterObserver<System.Object>
struct AmbOuterObserver_t2827192636;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.Operators.AmbObservable`1/AmbOuterObserver/Amb<System.Object>
struct Amb_t634706591;

#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_AmbO2663999068.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.AmbObservable`1/AmbOuterObserver/AmbDecisionObserver<System.Object>
struct  AmbDecisionObserver_t973332161  : public Il2CppObject
{
public:
	// UniRx.Operators.AmbObservable`1/AmbOuterObserver<T> UniRx.Operators.AmbObservable`1/AmbOuterObserver/AmbDecisionObserver::parent
	AmbOuterObserver_t2827192636 * ___parent_0;
	// UniRx.Operators.AmbObservable`1/AmbOuterObserver/AmbState<T> UniRx.Operators.AmbObservable`1/AmbOuterObserver/AmbDecisionObserver::me
	int32_t ___me_1;
	// System.IDisposable UniRx.Operators.AmbObservable`1/AmbOuterObserver/AmbDecisionObserver::otherSubscription
	Il2CppObject * ___otherSubscription_2;
	// UniRx.Operators.AmbObservable`1/AmbOuterObserver/Amb<T> UniRx.Operators.AmbObservable`1/AmbOuterObserver/AmbDecisionObserver::self
	Amb_t634706591 * ___self_3;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(AmbDecisionObserver_t973332161, ___parent_0)); }
	inline AmbOuterObserver_t2827192636 * get_parent_0() const { return ___parent_0; }
	inline AmbOuterObserver_t2827192636 ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(AmbOuterObserver_t2827192636 * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier(&___parent_0, value);
	}

	inline static int32_t get_offset_of_me_1() { return static_cast<int32_t>(offsetof(AmbDecisionObserver_t973332161, ___me_1)); }
	inline int32_t get_me_1() const { return ___me_1; }
	inline int32_t* get_address_of_me_1() { return &___me_1; }
	inline void set_me_1(int32_t value)
	{
		___me_1 = value;
	}

	inline static int32_t get_offset_of_otherSubscription_2() { return static_cast<int32_t>(offsetof(AmbDecisionObserver_t973332161, ___otherSubscription_2)); }
	inline Il2CppObject * get_otherSubscription_2() const { return ___otherSubscription_2; }
	inline Il2CppObject ** get_address_of_otherSubscription_2() { return &___otherSubscription_2; }
	inline void set_otherSubscription_2(Il2CppObject * value)
	{
		___otherSubscription_2 = value;
		Il2CppCodeGenWriteBarrier(&___otherSubscription_2, value);
	}

	inline static int32_t get_offset_of_self_3() { return static_cast<int32_t>(offsetof(AmbDecisionObserver_t973332161, ___self_3)); }
	inline Amb_t634706591 * get_self_3() const { return ___self_3; }
	inline Amb_t634706591 ** get_address_of_self_3() { return &___self_3; }
	inline void set_self_3(Amb_t634706591 * value)
	{
		___self_3 = value;
		Il2CppCodeGenWriteBarrier(&___self_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
