﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t1809983122;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ionic.Zlib.InflateCodes
struct  InflateCodes_t2875832300  : public Il2CppObject
{
public:
	// System.Int32 Ionic.Zlib.InflateCodes::mode
	int32_t ___mode_10;
	// System.Int32 Ionic.Zlib.InflateCodes::len
	int32_t ___len_11;
	// System.Int32[] Ionic.Zlib.InflateCodes::tree
	Int32U5BU5D_t1809983122* ___tree_12;
	// System.Int32 Ionic.Zlib.InflateCodes::tree_index
	int32_t ___tree_index_13;
	// System.Int32 Ionic.Zlib.InflateCodes::need
	int32_t ___need_14;
	// System.Int32 Ionic.Zlib.InflateCodes::lit
	int32_t ___lit_15;
	// System.Int32 Ionic.Zlib.InflateCodes::bitsToGet
	int32_t ___bitsToGet_16;
	// System.Int32 Ionic.Zlib.InflateCodes::dist
	int32_t ___dist_17;
	// System.Byte Ionic.Zlib.InflateCodes::lbits
	uint8_t ___lbits_18;
	// System.Byte Ionic.Zlib.InflateCodes::dbits
	uint8_t ___dbits_19;
	// System.Int32[] Ionic.Zlib.InflateCodes::ltree
	Int32U5BU5D_t1809983122* ___ltree_20;
	// System.Int32 Ionic.Zlib.InflateCodes::ltree_index
	int32_t ___ltree_index_21;
	// System.Int32[] Ionic.Zlib.InflateCodes::dtree
	Int32U5BU5D_t1809983122* ___dtree_22;
	// System.Int32 Ionic.Zlib.InflateCodes::dtree_index
	int32_t ___dtree_index_23;

public:
	inline static int32_t get_offset_of_mode_10() { return static_cast<int32_t>(offsetof(InflateCodes_t2875832300, ___mode_10)); }
	inline int32_t get_mode_10() const { return ___mode_10; }
	inline int32_t* get_address_of_mode_10() { return &___mode_10; }
	inline void set_mode_10(int32_t value)
	{
		___mode_10 = value;
	}

	inline static int32_t get_offset_of_len_11() { return static_cast<int32_t>(offsetof(InflateCodes_t2875832300, ___len_11)); }
	inline int32_t get_len_11() const { return ___len_11; }
	inline int32_t* get_address_of_len_11() { return &___len_11; }
	inline void set_len_11(int32_t value)
	{
		___len_11 = value;
	}

	inline static int32_t get_offset_of_tree_12() { return static_cast<int32_t>(offsetof(InflateCodes_t2875832300, ___tree_12)); }
	inline Int32U5BU5D_t1809983122* get_tree_12() const { return ___tree_12; }
	inline Int32U5BU5D_t1809983122** get_address_of_tree_12() { return &___tree_12; }
	inline void set_tree_12(Int32U5BU5D_t1809983122* value)
	{
		___tree_12 = value;
		Il2CppCodeGenWriteBarrier(&___tree_12, value);
	}

	inline static int32_t get_offset_of_tree_index_13() { return static_cast<int32_t>(offsetof(InflateCodes_t2875832300, ___tree_index_13)); }
	inline int32_t get_tree_index_13() const { return ___tree_index_13; }
	inline int32_t* get_address_of_tree_index_13() { return &___tree_index_13; }
	inline void set_tree_index_13(int32_t value)
	{
		___tree_index_13 = value;
	}

	inline static int32_t get_offset_of_need_14() { return static_cast<int32_t>(offsetof(InflateCodes_t2875832300, ___need_14)); }
	inline int32_t get_need_14() const { return ___need_14; }
	inline int32_t* get_address_of_need_14() { return &___need_14; }
	inline void set_need_14(int32_t value)
	{
		___need_14 = value;
	}

	inline static int32_t get_offset_of_lit_15() { return static_cast<int32_t>(offsetof(InflateCodes_t2875832300, ___lit_15)); }
	inline int32_t get_lit_15() const { return ___lit_15; }
	inline int32_t* get_address_of_lit_15() { return &___lit_15; }
	inline void set_lit_15(int32_t value)
	{
		___lit_15 = value;
	}

	inline static int32_t get_offset_of_bitsToGet_16() { return static_cast<int32_t>(offsetof(InflateCodes_t2875832300, ___bitsToGet_16)); }
	inline int32_t get_bitsToGet_16() const { return ___bitsToGet_16; }
	inline int32_t* get_address_of_bitsToGet_16() { return &___bitsToGet_16; }
	inline void set_bitsToGet_16(int32_t value)
	{
		___bitsToGet_16 = value;
	}

	inline static int32_t get_offset_of_dist_17() { return static_cast<int32_t>(offsetof(InflateCodes_t2875832300, ___dist_17)); }
	inline int32_t get_dist_17() const { return ___dist_17; }
	inline int32_t* get_address_of_dist_17() { return &___dist_17; }
	inline void set_dist_17(int32_t value)
	{
		___dist_17 = value;
	}

	inline static int32_t get_offset_of_lbits_18() { return static_cast<int32_t>(offsetof(InflateCodes_t2875832300, ___lbits_18)); }
	inline uint8_t get_lbits_18() const { return ___lbits_18; }
	inline uint8_t* get_address_of_lbits_18() { return &___lbits_18; }
	inline void set_lbits_18(uint8_t value)
	{
		___lbits_18 = value;
	}

	inline static int32_t get_offset_of_dbits_19() { return static_cast<int32_t>(offsetof(InflateCodes_t2875832300, ___dbits_19)); }
	inline uint8_t get_dbits_19() const { return ___dbits_19; }
	inline uint8_t* get_address_of_dbits_19() { return &___dbits_19; }
	inline void set_dbits_19(uint8_t value)
	{
		___dbits_19 = value;
	}

	inline static int32_t get_offset_of_ltree_20() { return static_cast<int32_t>(offsetof(InflateCodes_t2875832300, ___ltree_20)); }
	inline Int32U5BU5D_t1809983122* get_ltree_20() const { return ___ltree_20; }
	inline Int32U5BU5D_t1809983122** get_address_of_ltree_20() { return &___ltree_20; }
	inline void set_ltree_20(Int32U5BU5D_t1809983122* value)
	{
		___ltree_20 = value;
		Il2CppCodeGenWriteBarrier(&___ltree_20, value);
	}

	inline static int32_t get_offset_of_ltree_index_21() { return static_cast<int32_t>(offsetof(InflateCodes_t2875832300, ___ltree_index_21)); }
	inline int32_t get_ltree_index_21() const { return ___ltree_index_21; }
	inline int32_t* get_address_of_ltree_index_21() { return &___ltree_index_21; }
	inline void set_ltree_index_21(int32_t value)
	{
		___ltree_index_21 = value;
	}

	inline static int32_t get_offset_of_dtree_22() { return static_cast<int32_t>(offsetof(InflateCodes_t2875832300, ___dtree_22)); }
	inline Int32U5BU5D_t1809983122* get_dtree_22() const { return ___dtree_22; }
	inline Int32U5BU5D_t1809983122** get_address_of_dtree_22() { return &___dtree_22; }
	inline void set_dtree_22(Int32U5BU5D_t1809983122* value)
	{
		___dtree_22 = value;
		Il2CppCodeGenWriteBarrier(&___dtree_22, value);
	}

	inline static int32_t get_offset_of_dtree_index_23() { return static_cast<int32_t>(offsetof(InflateCodes_t2875832300, ___dtree_index_23)); }
	inline int32_t get_dtree_index_23() const { return ___dtree_index_23; }
	inline int32_t* get_address_of_dtree_index_23() { return &___dtree_index_23; }
	inline void set_dtree_index_23(int32_t value)
	{
		___dtree_index_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
