﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Func`3<System.Double,System.Double,System.Double>
struct Func_3_t1933728903;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Func`3<System.Double,System.Double,System.Double>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_3__ctor_m2706065971_gshared (Func_3_t1933728903 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Func_3__ctor_m2706065971(__this, ___object0, ___method1, method) ((  void (*) (Func_3_t1933728903 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_3__ctor_m2706065971_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`3<System.Double,System.Double,System.Double>::Invoke(T1,T2)
extern "C"  double Func_3_Invoke_m2760899878_gshared (Func_3_t1933728903 * __this, double ___arg10, double ___arg21, const MethodInfo* method);
#define Func_3_Invoke_m2760899878(__this, ___arg10, ___arg21, method) ((  double (*) (Func_3_t1933728903 *, double, double, const MethodInfo*))Func_3_Invoke_m2760899878_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Func`3<System.Double,System.Double,System.Double>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_3_BeginInvoke_m3522538023_gshared (Func_3_t1933728903 * __this, double ___arg10, double ___arg21, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Func_3_BeginInvoke_m3522538023(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Func_3_t1933728903 *, double, double, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_3_BeginInvoke_m3522538023_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// TResult System.Func`3<System.Double,System.Double,System.Double>::EndInvoke(System.IAsyncResult)
extern "C"  double Func_3_EndInvoke_m3188640373_gshared (Func_3_t1933728903 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Func_3_EndInvoke_m3188640373(__this, ___result0, method) ((  double (*) (Func_3_t1933728903 *, Il2CppObject *, const MethodInfo*))Func_3_EndInvoke_m3188640373_gshared)(__this, ___result0, method)
