﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.CompositionRoot
struct CompositionRoot_t1939928321;

#include "codegen/il2cpp-codegen.h"

// System.Void Zenject.CompositionRoot::.ctor()
extern "C"  void CompositionRoot__ctor_m1847083134 (CompositionRoot_t1939928321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
