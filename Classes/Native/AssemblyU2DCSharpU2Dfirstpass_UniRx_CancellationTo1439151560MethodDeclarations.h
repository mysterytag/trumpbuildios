﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ICancelable
struct ICancelable_t4109686575;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560.h"

// System.Void UniRx.CancellationToken::.ctor(UniRx.ICancelable)
extern "C"  void CancellationToken__ctor_m1453920148 (CancellationToken_t1439151560 * __this, Il2CppObject * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.CancellationToken::.cctor()
extern "C"  void CancellationToken__cctor_m2984451252 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.CancellationToken::get_IsCancellationRequested()
extern "C"  bool CancellationToken_get_IsCancellationRequested_m1021497867 (CancellationToken_t1439151560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.CancellationToken::ThrowIfCancellationRequested()
extern "C"  void CancellationToken_ThrowIfCancellationRequested_m198048467 (CancellationToken_t1439151560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
