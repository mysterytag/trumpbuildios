﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.BufferObservable`1/BufferT<System.Object>
struct BufferT_t129222477;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.BufferObservable`1/BufferT/Buffer<System.Object>
struct  Buffer_t492975322  : public Il2CppObject
{
public:
	// UniRx.Operators.BufferObservable`1/BufferT<T> UniRx.Operators.BufferObservable`1/BufferT/Buffer::parent
	BufferT_t129222477 * ___parent_0;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(Buffer_t492975322, ___parent_0)); }
	inline BufferT_t129222477 * get_parent_0() const { return ___parent_0; }
	inline BufferT_t129222477 ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(BufferT_t129222477 * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier(&___parent_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
