﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Stubs`1<System.Single>::.cctor()
extern "C"  void Stubs_1__cctor_m2167028339_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Stubs_1__cctor_m2167028339(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Stubs_1__cctor_m2167028339_gshared)(__this /* static, unused */, method)
// System.Void UniRx.Stubs`1<System.Single>::<Ignore>m__71(T)
extern "C"  void Stubs_1_U3CIgnoreU3Em__71_m3654020207_gshared (Il2CppObject * __this /* static, unused */, float ___t0, const MethodInfo* method);
#define Stubs_1_U3CIgnoreU3Em__71_m3654020207(__this /* static, unused */, ___t0, method) ((  void (*) (Il2CppObject * /* static, unused */, float, const MethodInfo*))Stubs_1_U3CIgnoreU3Em__71_m3654020207_gshared)(__this /* static, unused */, ___t0, method)
// T UniRx.Stubs`1<System.Single>::<Identity>m__72(T)
extern "C"  float Stubs_1_U3CIdentityU3Em__72_m2765273249_gshared (Il2CppObject * __this /* static, unused */, float ___t0, const MethodInfo* method);
#define Stubs_1_U3CIdentityU3Em__72_m2765273249(__this /* static, unused */, ___t0, method) ((  float (*) (Il2CppObject * /* static, unused */, float, const MethodInfo*))Stubs_1_U3CIdentityU3Em__72_m2765273249_gshared)(__this /* static, unused */, ___t0, method)
