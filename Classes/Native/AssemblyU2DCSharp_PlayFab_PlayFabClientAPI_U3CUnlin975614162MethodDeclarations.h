﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<UnlinkAndroidDeviceID>c__AnonStorey80
struct U3CUnlinkAndroidDeviceIDU3Ec__AnonStorey80_t975614162;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<UnlinkAndroidDeviceID>c__AnonStorey80::.ctor()
extern "C"  void U3CUnlinkAndroidDeviceIDU3Ec__AnonStorey80__ctor_m2670157505 (U3CUnlinkAndroidDeviceIDU3Ec__AnonStorey80_t975614162 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<UnlinkAndroidDeviceID>c__AnonStorey80::<>m__6D(PlayFab.CallRequestContainer)
extern "C"  void U3CUnlinkAndroidDeviceIDU3Ec__AnonStorey80_U3CU3Em__6D_m2066452525 (U3CUnlinkAndroidDeviceIDU3Ec__AnonStorey80_t975614162 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
