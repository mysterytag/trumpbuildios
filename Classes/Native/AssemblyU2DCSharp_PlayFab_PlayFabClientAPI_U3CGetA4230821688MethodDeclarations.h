﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<GetAccountInfo>c__AnonStorey6D
struct U3CGetAccountInfoU3Ec__AnonStorey6D_t4230821688;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<GetAccountInfo>c__AnonStorey6D::.ctor()
extern "C"  void U3CGetAccountInfoU3Ec__AnonStorey6D__ctor_m3613310843 (U3CGetAccountInfoU3Ec__AnonStorey6D_t4230821688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<GetAccountInfo>c__AnonStorey6D::<>m__5A(PlayFab.CallRequestContainer)
extern "C"  void U3CGetAccountInfoU3Ec__AnonStorey6D_U3CU3Em__5A_m1852954309 (U3CGetAccountInfoU3Ec__AnonStorey6D_t4230821688 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
