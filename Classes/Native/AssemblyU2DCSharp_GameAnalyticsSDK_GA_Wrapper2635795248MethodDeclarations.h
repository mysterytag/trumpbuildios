﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameAnalyticsSDK.GA_Wrapper
struct GA_Wrapper_t2635795248;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_GameAnalyticsSDK_GA_Resource_GAR3683864752.h"
#include "AssemblyU2DCSharp_GameAnalyticsSDK_GA_Progression_2058507475.h"
#include "AssemblyU2DCSharp_GameAnalyticsSDK_GA_Error_GAErro2545392235.h"

// System.Void GameAnalyticsSDK.GA_Wrapper::.ctor()
extern "C"  void GA_Wrapper__ctor_m4275586869 (GA_Wrapper_t2635795248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Wrapper::SetAvailableCustomDimensions01(System.String)
extern "C"  void GA_Wrapper_SetAvailableCustomDimensions01_m3629799213 (Il2CppObject * __this /* static, unused */, String_t* ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Wrapper::SetAvailableCustomDimensions02(System.String)
extern "C"  void GA_Wrapper_SetAvailableCustomDimensions02_m3119265036 (Il2CppObject * __this /* static, unused */, String_t* ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Wrapper::SetAvailableCustomDimensions03(System.String)
extern "C"  void GA_Wrapper_SetAvailableCustomDimensions03_m2608730859 (Il2CppObject * __this /* static, unused */, String_t* ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Wrapper::SetAvailableResourceCurrencies(System.String)
extern "C"  void GA_Wrapper_SetAvailableResourceCurrencies_m908306671 (Il2CppObject * __this /* static, unused */, String_t* ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Wrapper::SetAvailableResourceItemTypes(System.String)
extern "C"  void GA_Wrapper_SetAvailableResourceItemTypes_m560486526 (Il2CppObject * __this /* static, unused */, String_t* ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Wrapper::SetUnitySdkVersion(System.String)
extern "C"  void GA_Wrapper_SetUnitySdkVersion_m3906033794 (Il2CppObject * __this /* static, unused */, String_t* ___unitySdkVersion0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Wrapper::SetUnityEngineVersion(System.String)
extern "C"  void GA_Wrapper_SetUnityEngineVersion_m2785224812 (Il2CppObject * __this /* static, unused */, String_t* ___unityEngineVersion0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Wrapper::SetBuild(System.String)
extern "C"  void GA_Wrapper_SetBuild_m3322020423 (Il2CppObject * __this /* static, unused */, String_t* ___build0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Wrapper::Initialize(System.String,System.String)
extern "C"  void GA_Wrapper_Initialize_m4275813343 (Il2CppObject * __this /* static, unused */, String_t* ___gamekey0, String_t* ___gamesecret1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Wrapper::SetCustomDimension01(System.String)
extern "C"  void GA_Wrapper_SetCustomDimension01_m2327823359 (Il2CppObject * __this /* static, unused */, String_t* ___customDimension0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Wrapper::SetCustomDimension02(System.String)
extern "C"  void GA_Wrapper_SetCustomDimension02_m1817289182 (Il2CppObject * __this /* static, unused */, String_t* ___customDimension0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Wrapper::SetCustomDimension03(System.String)
extern "C"  void GA_Wrapper_SetCustomDimension03_m1306755005 (Il2CppObject * __this /* static, unused */, String_t* ___customDimension0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Wrapper::AddBusinessEvent(System.String,System.Int32,System.String,System.String,System.String,System.String)
extern "C"  void GA_Wrapper_AddBusinessEvent_m420358861 (Il2CppObject * __this /* static, unused */, String_t* ___currency0, int32_t ___amount1, String_t* ___itemType2, String_t* ___itemId3, String_t* ___cartType4, String_t* ___receipt5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Wrapper::AddBusinessEventAndAutoFetchReceipt(System.String,System.Int32,System.String,System.String,System.String)
extern "C"  void GA_Wrapper_AddBusinessEventAndAutoFetchReceipt_m2124978343 (Il2CppObject * __this /* static, unused */, String_t* ___currency0, int32_t ___amount1, String_t* ___itemType2, String_t* ___itemId3, String_t* ___cartType4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Wrapper::AddResourceEvent(GameAnalyticsSDK.GA_Resource/GAResourceFlowType,System.String,System.Single,System.String,System.String)
extern "C"  void GA_Wrapper_AddResourceEvent_m3790612445 (Il2CppObject * __this /* static, unused */, int32_t ___flowType0, String_t* ___currency1, float ___amount2, String_t* ___itemType3, String_t* ___itemId4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Wrapper::AddProgressionEvent(GameAnalyticsSDK.GA_Progression/GAProgressionStatus,System.String,System.String,System.String)
extern "C"  void GA_Wrapper_AddProgressionEvent_m4016971441 (Il2CppObject * __this /* static, unused */, int32_t ___progressionStatus0, String_t* ___progression011, String_t* ___progression022, String_t* ___progression033, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Wrapper::AddProgressionEventWithScore(GameAnalyticsSDK.GA_Progression/GAProgressionStatus,System.String,System.String,System.String,System.Int32)
extern "C"  void GA_Wrapper_AddProgressionEventWithScore_m1778549742 (Il2CppObject * __this /* static, unused */, int32_t ___progressionStatus0, String_t* ___progression011, String_t* ___progression022, String_t* ___progression033, int32_t ___score4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Wrapper::AddDesignEvent(System.String,System.Single)
extern "C"  void GA_Wrapper_AddDesignEvent_m4114106589 (Il2CppObject * __this /* static, unused */, String_t* ___eventID0, float ___eventValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Wrapper::AddDesignEvent(System.String)
extern "C"  void GA_Wrapper_AddDesignEvent_m4038397944 (Il2CppObject * __this /* static, unused */, String_t* ___eventID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Wrapper::AddErrorEvent(GameAnalyticsSDK.GA_Error/GAErrorSeverity,System.String)
extern "C"  void GA_Wrapper_AddErrorEvent_m1594697537 (Il2CppObject * __this /* static, unused */, int32_t ___severity0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Wrapper::SetInfoLog(System.Boolean)
extern "C"  void GA_Wrapper_SetInfoLog_m4014180634 (Il2CppObject * __this /* static, unused */, bool ___enabled0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Wrapper::SetVerboseLog(System.Boolean)
extern "C"  void GA_Wrapper_SetVerboseLog_m11324142 (Il2CppObject * __this /* static, unused */, bool ___enabled0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Wrapper::SetFacebookId(System.String)
extern "C"  void GA_Wrapper_SetFacebookId_m3834608812 (Il2CppObject * __this /* static, unused */, String_t* ___facebookId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Wrapper::SetGender(System.String)
extern "C"  void GA_Wrapper_SetGender_m3606037100 (Il2CppObject * __this /* static, unused */, String_t* ___gender0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Wrapper::SetBirthYear(System.Int32)
extern "C"  void GA_Wrapper_SetBirthYear_m3359386138 (Il2CppObject * __this /* static, unused */, int32_t ___birthYear0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Wrapper::configureAvailableCustomDimensions01(System.String)
extern "C"  void GA_Wrapper_configureAvailableCustomDimensions01_m1176316753 (Il2CppObject * __this /* static, unused */, String_t* ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Wrapper::configureAvailableCustomDimensions02(System.String)
extern "C"  void GA_Wrapper_configureAvailableCustomDimensions02_m665782576 (Il2CppObject * __this /* static, unused */, String_t* ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Wrapper::configureAvailableCustomDimensions03(System.String)
extern "C"  void GA_Wrapper_configureAvailableCustomDimensions03_m155248399 (Il2CppObject * __this /* static, unused */, String_t* ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Wrapper::configureAvailableResourceCurrencies(System.String)
extern "C"  void GA_Wrapper_configureAvailableResourceCurrencies_m2749791507 (Il2CppObject * __this /* static, unused */, String_t* ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Wrapper::configureAvailableResourceItemTypes(System.String)
extern "C"  void GA_Wrapper_configureAvailableResourceItemTypes_m4222119898 (Il2CppObject * __this /* static, unused */, String_t* ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Wrapper::configureSdkGameEngineVersion(System.String)
extern "C"  void GA_Wrapper_configureSdkGameEngineVersion_m1249253663 (Il2CppObject * __this /* static, unused */, String_t* ___unitySdkVersion0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Wrapper::configureGameEngineVersion(System.String)
extern "C"  void GA_Wrapper_configureGameEngineVersion_m2924521397 (Il2CppObject * __this /* static, unused */, String_t* ___unityEngineVersion0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Wrapper::configureBuild(System.String)
extern "C"  void GA_Wrapper_configureBuild_m653953387 (Il2CppObject * __this /* static, unused */, String_t* ___build0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Wrapper::initialize(System.String,System.String)
extern "C"  void GA_Wrapper_initialize_m519742463 (Il2CppObject * __this /* static, unused */, String_t* ___gamekey0, String_t* ___gamesecret1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Wrapper::setCustomDimension01(System.String)
extern "C"  void GA_Wrapper_setCustomDimension01_m1708637215 (Il2CppObject * __this /* static, unused */, String_t* ___customDimension0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Wrapper::setCustomDimension02(System.String)
extern "C"  void GA_Wrapper_setCustomDimension02_m1198103038 (Il2CppObject * __this /* static, unused */, String_t* ___customDimension0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Wrapper::setCustomDimension03(System.String)
extern "C"  void GA_Wrapper_setCustomDimension03_m687568861 (Il2CppObject * __this /* static, unused */, String_t* ___customDimension0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Wrapper::addBusinessEvent(System.String,System.Int32,System.String,System.String,System.String,System.String)
extern "C"  void GA_Wrapper_addBusinessEvent_m715536045 (Il2CppObject * __this /* static, unused */, String_t* ___currency0, int32_t ___amount1, String_t* ___itemType2, String_t* ___itemId3, String_t* ___cartType4, String_t* ___receipt5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Wrapper::addBusinessEventAndAutoFetchReceipt(System.String,System.Int32,System.String,System.String,System.String)
extern "C"  void GA_Wrapper_addBusinessEventAndAutoFetchReceipt_m4020041927 (Il2CppObject * __this /* static, unused */, String_t* ___currency0, int32_t ___amount1, String_t* ___itemType2, String_t* ___itemId3, String_t* ___cartType4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Wrapper::addResourceEvent(System.Int32,System.String,System.Single,System.String,System.String)
extern "C"  void GA_Wrapper_addResourceEvent_m3572279044 (Il2CppObject * __this /* static, unused */, int32_t ___flowType0, String_t* ___currency1, float ___amount2, String_t* ___itemType3, String_t* ___itemId4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Wrapper::addProgressionEvent(System.Int32,System.String,System.String,System.String)
extern "C"  void GA_Wrapper_addProgressionEvent_m2171743704 (Il2CppObject * __this /* static, unused */, int32_t ___progressionStatus0, String_t* ___progression011, String_t* ___progression022, String_t* ___progression033, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Wrapper::addProgressionEventWithScore(System.Int32,System.String,System.String,System.String,System.Int32)
extern "C"  void GA_Wrapper_addProgressionEventWithScore_m2513498391 (Il2CppObject * __this /* static, unused */, int32_t ___progressionStatus0, String_t* ___progression011, String_t* ___progression022, String_t* ___progression033, int32_t ___score4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Wrapper::addDesignEvent(System.String)
extern "C"  void GA_Wrapper_addDesignEvent_m447848472 (Il2CppObject * __this /* static, unused */, String_t* ___eventId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Wrapper::addDesignEventWithValue(System.String,System.Single)
extern "C"  void GA_Wrapper_addDesignEventWithValue_m3712165412 (Il2CppObject * __this /* static, unused */, String_t* ___eventId0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Wrapper::addErrorEvent(System.Int32,System.String)
extern "C"  void GA_Wrapper_addErrorEvent_m4111900403 (Il2CppObject * __this /* static, unused */, int32_t ___severity0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Wrapper::setEnabledInfoLog(System.Boolean)
extern "C"  void GA_Wrapper_setEnabledInfoLog_m3261432289 (Il2CppObject * __this /* static, unused */, bool ___enabled0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Wrapper::setEnabledVerboseLog(System.Boolean)
extern "C"  void GA_Wrapper_setEnabledVerboseLog_m3204597959 (Il2CppObject * __this /* static, unused */, bool ___enabled0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Wrapper::setFacebookId(System.String)
extern "C"  void GA_Wrapper_setFacebookId_m4272973964 (Il2CppObject * __this /* static, unused */, String_t* ___facebookId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Wrapper::setGender(System.String)
extern "C"  void GA_Wrapper_setGender_m1461821004 (Il2CppObject * __this /* static, unused */, String_t* ___gender0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Wrapper::setBirthYear(System.Int32)
extern "C"  void GA_Wrapper_setBirthYear_m57052666 (Il2CppObject * __this /* static, unused */, int32_t ___birthYear0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
