﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.InternalUtil.ThrowObserver`1<System.Object>
struct ThrowObserver_1_t1489068035;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.InternalUtil.ThrowObserver`1<System.Object>
struct  ThrowObserver_1_t1489068035  : public Il2CppObject
{
public:

public:
};

struct ThrowObserver_1_t1489068035_StaticFields
{
public:
	// UniRx.InternalUtil.ThrowObserver`1<T> UniRx.InternalUtil.ThrowObserver`1::Instance
	ThrowObserver_1_t1489068035 * ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(ThrowObserver_1_t1489068035_StaticFields, ___Instance_0)); }
	inline ThrowObserver_1_t1489068035 * get_Instance_0() const { return ___Instance_0; }
	inline ThrowObserver_1_t1489068035 ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(ThrowObserver_1_t1489068035 * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier(&___Instance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
