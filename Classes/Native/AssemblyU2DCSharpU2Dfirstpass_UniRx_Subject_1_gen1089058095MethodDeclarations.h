﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen2307296439MethodDeclarations.h"

// System.Void UniRx.Subject`1<UniRx.Tuple`2<System.String,System.String>>::.ctor()
#define Subject_1__ctor_m4281135848(__this, method) ((  void (*) (Subject_1_t1089058095 *, const MethodInfo*))Subject_1__ctor_m3673523288_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.Tuple`2<System.String,System.String>>::get_HasObservers()
#define Subject_1_get_HasObservers_m1340533152(__this, method) ((  bool (*) (Subject_1_t1089058095 *, const MethodInfo*))Subject_1_get_HasObservers_m1225079420_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.Tuple`2<System.String,System.String>>::OnCompleted()
#define Subject_1_OnCompleted_m106987582(__this, method) ((  void (*) (Subject_1_t1089058095 *, const MethodInfo*))Subject_1_OnCompleted_m1708266978_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.Tuple`2<System.String,System.String>>::OnError(System.Exception)
#define Subject_1_OnError_m3657876587(__this, ___error0, method) ((  void (*) (Subject_1_t1089058095 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m2796618767_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<UniRx.Tuple`2<System.String,System.String>>::OnNext(T)
#define Subject_1_OnNext_m2116599807(__this, ___value0, method) ((  void (*) (Subject_1_t1089058095 *, Tuple_2_t3445990771 , const MethodInfo*))Subject_1_OnNext_m2623465728_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<UniRx.Tuple`2<System.String,System.String>>::Subscribe(UniRx.IObserver`1<T>)
#define Subject_1_Subscribe_m1697666313(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t1089058095 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m376261037_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<UniRx.Tuple`2<System.String,System.String>>::Dispose()
#define Subject_1_Dispose_m651145969(__this, method) ((  void (*) (Subject_1_t1089058095 *, const MethodInfo*))Subject_1_Dispose_m3986012821_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.Tuple`2<System.String,System.String>>::ThrowIfDisposed()
#define Subject_1_ThrowIfDisposed_m545774458(__this, method) ((  void (*) (Subject_1_t1089058095 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m2620260126_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.Tuple`2<System.String,System.String>>::IsRequiredSubscribeOnCurrentThread()
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m3569888919(__this, method) ((  bool (*) (Subject_1_t1089058095 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m4284383347_gshared)(__this, method)
