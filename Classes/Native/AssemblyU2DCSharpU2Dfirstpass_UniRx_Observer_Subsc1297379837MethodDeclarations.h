﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observer/Subscribe_`1<System.Int64>
struct Subscribe__1_t1297379837;
// System.Action`1<System.Exception>
struct Action_1_t2115686693;
// System.Action
struct Action_t437523947;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Observer/Subscribe_`1<System.Int64>::.ctor(System.Action`1<System.Exception>,System.Action)
extern "C"  void Subscribe__1__ctor_m3387838357_gshared (Subscribe__1_t1297379837 * __this, Action_1_t2115686693 * ___onError0, Action_t437523947 * ___onCompleted1, const MethodInfo* method);
#define Subscribe__1__ctor_m3387838357(__this, ___onError0, ___onCompleted1, method) ((  void (*) (Subscribe__1_t1297379837 *, Action_1_t2115686693 *, Action_t437523947 *, const MethodInfo*))Subscribe__1__ctor_m3387838357_gshared)(__this, ___onError0, ___onCompleted1, method)
// System.Void UniRx.Observer/Subscribe_`1<System.Int64>::OnNext(T)
extern "C"  void Subscribe__1_OnNext_m3243444708_gshared (Subscribe__1_t1297379837 * __this, int64_t ___value0, const MethodInfo* method);
#define Subscribe__1_OnNext_m3243444708(__this, ___value0, method) ((  void (*) (Subscribe__1_t1297379837 *, int64_t, const MethodInfo*))Subscribe__1_OnNext_m3243444708_gshared)(__this, ___value0, method)
// System.Void UniRx.Observer/Subscribe_`1<System.Int64>::OnError(System.Exception)
extern "C"  void Subscribe__1_OnError_m1991171315_gshared (Subscribe__1_t1297379837 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Subscribe__1_OnError_m1991171315(__this, ___error0, method) ((  void (*) (Subscribe__1_t1297379837 *, Exception_t1967233988 *, const MethodInfo*))Subscribe__1_OnError_m1991171315_gshared)(__this, ___error0, method)
// System.Void UniRx.Observer/Subscribe_`1<System.Int64>::OnCompleted()
extern "C"  void Subscribe__1_OnCompleted_m3225625798_gshared (Subscribe__1_t1297379837 * __this, const MethodInfo* method);
#define Subscribe__1_OnCompleted_m3225625798(__this, method) ((  void (*) (Subscribe__1_t1297379837 *, const MethodInfo*))Subscribe__1_OnCompleted_m3225625798_gshared)(__this, method)
