﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Disposable/EmptyDisposable
struct EmptyDisposable_t3755833965;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Disposable/EmptyDisposable
struct  EmptyDisposable_t3755833965  : public Il2CppObject
{
public:

public:
};

struct EmptyDisposable_t3755833965_StaticFields
{
public:
	// UniRx.Disposable/EmptyDisposable UniRx.Disposable/EmptyDisposable::Singleton
	EmptyDisposable_t3755833965 * ___Singleton_0;

public:
	inline static int32_t get_offset_of_Singleton_0() { return static_cast<int32_t>(offsetof(EmptyDisposable_t3755833965_StaticFields, ___Singleton_0)); }
	inline EmptyDisposable_t3755833965 * get_Singleton_0() const { return ___Singleton_0; }
	inline EmptyDisposable_t3755833965 ** get_address_of_Singleton_0() { return &___Singleton_0; }
	inline void set_Singleton_0(EmptyDisposable_t3755833965 * value)
	{
		___Singleton_0 = value;
		Il2CppCodeGenWriteBarrier(&___Singleton_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
