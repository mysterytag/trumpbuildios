﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I1897310919MethodDeclarations.h"

// System.Void UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.NetworkDisconnection>>::.ctor()
#define ImmutableList_1__ctor_m3277782044(__this, method) ((  void (*) (ImmutableList_1_t3610963797 *, const MethodInfo*))ImmutableList_1__ctor_m2467204245_gshared)(__this, method)
// System.Void UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.NetworkDisconnection>>::.ctor(T[])
#define ImmutableList_1__ctor_m2324769632(__this, ___data0, method) ((  void (*) (ImmutableList_1_t3610963797 *, IObserver_1U5BU5D_t3485751351*, const MethodInfo*))ImmutableList_1__ctor_m707697735_gshared)(__this, ___data0, method)
// System.Void UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.NetworkDisconnection>>::.cctor()
#define ImmutableList_1__cctor_m2344899345(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ImmutableList_1__cctor_m2986791352_gshared)(__this /* static, unused */, method)
// T[] UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.NetworkDisconnection>>::get_Data()
#define ImmutableList_1_get_Data_m1014335646(__this, method) ((  IObserver_1U5BU5D_t3485751351* (*) (ImmutableList_1_t3610963797 *, const MethodInfo*))ImmutableList_1_get_Data_m1676800965_gshared)(__this, method)
// UniRx.InternalUtil.ImmutableList`1<T> UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.NetworkDisconnection>>::Add(T)
#define ImmutableList_1_Add_m2752275932(__this, ___value0, method) ((  ImmutableList_1_t3610963797 * (*) (ImmutableList_1_t3610963797 *, Il2CppObject*, const MethodInfo*))ImmutableList_1_Add_m948892931_gshared)(__this, ___value0, method)
// UniRx.InternalUtil.ImmutableList`1<T> UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.NetworkDisconnection>>::Remove(T)
#define ImmutableList_1_Remove_m375994329(__this, ___value0, method) ((  ImmutableList_1_t3610963797 * (*) (ImmutableList_1_t3610963797 *, Il2CppObject*, const MethodInfo*))ImmutableList_1_Remove_m1538917202_gshared)(__this, ___value0, method)
// System.Int32 UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.NetworkDisconnection>>::IndexOf(T)
#define ImmutableList_1_IndexOf_m797230341(__this, ___value0, method) ((  int32_t (*) (ImmutableList_1_t3610963797 *, Il2CppObject*, const MethodInfo*))ImmutableList_1_IndexOf_m3149323628_gshared)(__this, ___value0, method)
