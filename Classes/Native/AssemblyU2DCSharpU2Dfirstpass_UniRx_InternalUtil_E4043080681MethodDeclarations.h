﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.EmptyObserver`1<UnityEngine.NetworkDisconnection>
struct EmptyObserver_1_t4043080681;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_NetworkDisconnection338760395.h"

// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.NetworkDisconnection>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m2482815813_gshared (EmptyObserver_1_t4043080681 * __this, const MethodInfo* method);
#define EmptyObserver_1__ctor_m2482815813(__this, method) ((  void (*) (EmptyObserver_1_t4043080681 *, const MethodInfo*))EmptyObserver_1__ctor_m2482815813_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.NetworkDisconnection>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m3470749960_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EmptyObserver_1__cctor_m3470749960(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EmptyObserver_1__cctor_m3470749960_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.NetworkDisconnection>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m3105337103_gshared (EmptyObserver_1_t4043080681 * __this, const MethodInfo* method);
#define EmptyObserver_1_OnCompleted_m3105337103(__this, method) ((  void (*) (EmptyObserver_1_t4043080681 *, const MethodInfo*))EmptyObserver_1_OnCompleted_m3105337103_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.NetworkDisconnection>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m1986085308_gshared (EmptyObserver_1_t4043080681 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define EmptyObserver_1_OnError_m1986085308(__this, ___error0, method) ((  void (*) (EmptyObserver_1_t4043080681 *, Exception_t1967233988 *, const MethodInfo*))EmptyObserver_1_OnError_m1986085308_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UnityEngine.NetworkDisconnection>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m814882989_gshared (EmptyObserver_1_t4043080681 * __this, int32_t ___value0, const MethodInfo* method);
#define EmptyObserver_1_OnNext_m814882989(__this, ___value0, method) ((  void (*) (EmptyObserver_1_t4043080681 *, int32_t, const MethodInfo*))EmptyObserver_1_OnNext_m814882989_gshared)(__this, ___value0, method)
