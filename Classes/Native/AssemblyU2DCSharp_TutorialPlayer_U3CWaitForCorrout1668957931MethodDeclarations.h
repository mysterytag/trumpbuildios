﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TutorialPlayer/<WaitForCorroutine>c__Iterator24
struct U3CWaitForCorroutineU3Ec__Iterator24_t1668957931;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void TutorialPlayer/<WaitForCorroutine>c__Iterator24::.ctor()
extern "C"  void U3CWaitForCorroutineU3Ec__Iterator24__ctor_m1351731920 (U3CWaitForCorroutineU3Ec__Iterator24_t1668957931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TutorialPlayer/<WaitForCorroutine>c__Iterator24::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitForCorroutineU3Ec__Iterator24_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1652364418 (U3CWaitForCorroutineU3Ec__Iterator24_t1668957931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TutorialPlayer/<WaitForCorroutine>c__Iterator24::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitForCorroutineU3Ec__Iterator24_System_Collections_IEnumerator_get_Current_m2031901718 (U3CWaitForCorroutineU3Ec__Iterator24_t1668957931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TutorialPlayer/<WaitForCorroutine>c__Iterator24::MoveNext()
extern "C"  bool U3CWaitForCorroutineU3Ec__Iterator24_MoveNext_m4133325092 (U3CWaitForCorroutineU3Ec__Iterator24_t1668957931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialPlayer/<WaitForCorroutine>c__Iterator24::Dispose()
extern "C"  void U3CWaitForCorroutineU3Ec__Iterator24_Dispose_m1832534797 (U3CWaitForCorroutineU3Ec__Iterator24_t1668957931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialPlayer/<WaitForCorroutine>c__Iterator24::Reset()
extern "C"  void U3CWaitForCorroutineU3Ec__Iterator24_Reset_m3293132157 (U3CWaitForCorroutineU3Ec__Iterator24_t1668957931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
