﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.UUnit.UUnitTestSuite
struct UUnitTestSuite_t2558391287;
// PlayFab.UUnit.UUnitTestCase
struct UUnitTestCase_t14187365;
// PlayFab.UUnit.UUnitTestResult
struct UUnitTestResult_t4060832146;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PlayFab_UUnit_UUnitTestCase14187365.h"
#include "mscorlib_System_Type2779229935.h"

// System.Void PlayFab.UUnit.UUnitTestSuite::.ctor()
extern "C"  void UUnitTestSuite__ctor_m94517790 (UUnitTestSuite_t2558391287 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.UUnitTestSuite::Add(PlayFab.UUnit.UUnitTestCase)
extern "C"  void UUnitTestSuite_Add_m3435464298 (UUnitTestSuite_t2558391287 * __this, UUnitTestCase_t14187365 * ___testCase0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.UUnitTestSuite::RunAllTests()
extern "C"  void UUnitTestSuite_RunAllTests_m2259381479 (UUnitTestSuite_t2558391287 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayFab.UUnit.UUnitTestSuite::RunOneTest()
extern "C"  bool UUnitTestSuite_RunOneTest_m1452545319 (UUnitTestSuite_t2558391287 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayFab.UUnit.UUnitTestResult PlayFab.UUnit.UUnitTestSuite::GetResults()
extern "C"  UUnitTestResult_t4060832146 * UUnitTestSuite_GetResults_m3967650017 (UUnitTestSuite_t2558391287 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.UUnitTestSuite::FindAndAddAllTestCases(System.Type)
extern "C"  void UUnitTestSuite_FindAndAddAllTestCases_m4194677568 (UUnitTestSuite_t2558391287 * __this, Type_t * ___parent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.UUnitTestSuite::AddAll(System.Type)
extern "C"  void UUnitTestSuite_AddAll_m1342302291 (UUnitTestSuite_t2558391287 * __this, Type_t * ___testCaseType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayFab.UUnit.UUnitTestSuite::AllTestsPassed()
extern "C"  bool UUnitTestSuite_AllTestsPassed_m3215034986 (UUnitTestSuite_t2558391287 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
