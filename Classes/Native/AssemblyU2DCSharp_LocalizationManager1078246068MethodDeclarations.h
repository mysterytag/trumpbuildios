﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void LocalizationManager::Initialize()
extern "C"  void LocalizationManager_Initialize_m1786158525 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LocalizationManager::PathForLocalization(System.String)
extern "C"  String_t* LocalizationManager_PathForLocalization_m2659746189 (Il2CppObject * __this /* static, unused */, String_t* ___langId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LocalizationManager::GetLanguage()
extern "C"  String_t* LocalizationManager_GetLanguage_m3001286662 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LocalizationManager::UseVk()
extern "C"  bool LocalizationManager_UseVk_m310211749 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LocalizationManager::GetString(System.String)
extern "C"  String_t* LocalizationManager_GetString_m738567075 (Il2CppObject * __this /* static, unused */, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LocalizationManager::GetEngLocString(System.String)
extern "C"  String_t* LocalizationManager_GetEngLocString_m3629979905 (Il2CppObject * __this /* static, unused */, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalizationManager::InitializeOnlyEng()
extern "C"  void LocalizationManager_InitializeOnlyEng_m2286298551 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
