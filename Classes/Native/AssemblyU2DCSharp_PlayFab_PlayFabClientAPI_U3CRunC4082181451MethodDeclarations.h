﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<RunCloudScript>c__AnonStoreyC4
struct U3CRunCloudScriptU3Ec__AnonStoreyC4_t4082181451;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<RunCloudScript>c__AnonStoreyC4::.ctor()
extern "C"  void U3CRunCloudScriptU3Ec__AnonStoreyC4__ctor_m2688153288 (U3CRunCloudScriptU3Ec__AnonStoreyC4_t4082181451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<RunCloudScript>c__AnonStoreyC4::<>m__B1(PlayFab.CallRequestContainer)
extern "C"  void U3CRunCloudScriptU3Ec__AnonStoreyC4_U3CU3Em__B1_m1862124437 (U3CRunCloudScriptU3Ec__AnonStoreyC4_t4082181451 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
