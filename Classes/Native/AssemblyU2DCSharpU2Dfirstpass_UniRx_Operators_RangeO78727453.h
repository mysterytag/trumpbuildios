﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera701568569.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.RangeObservable/Range
struct  Range_t78727453  : public OperatorObserverBase_2_t701568569
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
