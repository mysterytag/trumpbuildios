﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZergRush.ReactiveCollectionExtensions/<Filter>c__AnonStorey11D`1<System.Object>
struct U3CFilterU3Ec__AnonStorey11D_1_t441028593;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void ZergRush.ReactiveCollectionExtensions/<Filter>c__AnonStorey11D`1<System.Object>::.ctor()
extern "C"  void U3CFilterU3Ec__AnonStorey11D_1__ctor_m1074412565_gshared (U3CFilterU3Ec__AnonStorey11D_1_t441028593 * __this, const MethodInfo* method);
#define U3CFilterU3Ec__AnonStorey11D_1__ctor_m1074412565(__this, method) ((  void (*) (U3CFilterU3Ec__AnonStorey11D_1_t441028593 *, const MethodInfo*))U3CFilterU3Ec__AnonStorey11D_1__ctor_m1074412565_gshared)(__this, method)
// System.Void ZergRush.ReactiveCollectionExtensions/<Filter>c__AnonStorey11D`1<System.Object>::<>m__1A9(T)
extern "C"  void U3CFilterU3Ec__AnonStorey11D_1_U3CU3Em__1A9_m3881350449_gshared (U3CFilterU3Ec__AnonStorey11D_1_t441028593 * __this, Il2CppObject * ___objAdd0, const MethodInfo* method);
#define U3CFilterU3Ec__AnonStorey11D_1_U3CU3Em__1A9_m3881350449(__this, ___objAdd0, method) ((  void (*) (U3CFilterU3Ec__AnonStorey11D_1_t441028593 *, Il2CppObject *, const MethodInfo*))U3CFilterU3Ec__AnonStorey11D_1_U3CU3Em__1A9_m3881350449_gshared)(__this, ___objAdd0, method)
// System.Void ZergRush.ReactiveCollectionExtensions/<Filter>c__AnonStorey11D`1<System.Object>::<>m__1AA(T)
extern "C"  void U3CFilterU3Ec__AnonStorey11D_1_U3CU3Em__1AA_m3881588777_gshared (U3CFilterU3Ec__AnonStorey11D_1_t441028593 * __this, Il2CppObject * ___objRemove0, const MethodInfo* method);
#define U3CFilterU3Ec__AnonStorey11D_1_U3CU3Em__1AA_m3881588777(__this, ___objRemove0, method) ((  void (*) (U3CFilterU3Ec__AnonStorey11D_1_t441028593 *, Il2CppObject *, const MethodInfo*))U3CFilterU3Ec__AnonStorey11D_1_U3CU3Em__1AA_m3881588777_gshared)(__this, ___objRemove0, method)
