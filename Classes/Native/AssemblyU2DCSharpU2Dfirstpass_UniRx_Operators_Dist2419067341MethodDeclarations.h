﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.DistinctUntilChangedObservable`2/DistinctUntilChanged<System.Object,System.Object>
struct DistinctUntilChanged_t2419067341;
// UniRx.Operators.DistinctUntilChangedObservable`2<System.Object,System.Object>
struct DistinctUntilChangedObservable_2_t1596230355;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.DistinctUntilChangedObservable`2/DistinctUntilChanged<System.Object,System.Object>::.ctor(UniRx.Operators.DistinctUntilChangedObservable`2<T,TKey>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void DistinctUntilChanged__ctor_m309756399_gshared (DistinctUntilChanged_t2419067341 * __this, DistinctUntilChangedObservable_2_t1596230355 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define DistinctUntilChanged__ctor_m309756399(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (DistinctUntilChanged_t2419067341 *, DistinctUntilChangedObservable_2_t1596230355 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))DistinctUntilChanged__ctor_m309756399_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.DistinctUntilChangedObservable`2/DistinctUntilChanged<System.Object,System.Object>::OnNext(T)
extern "C"  void DistinctUntilChanged_OnNext_m3817983600_gshared (DistinctUntilChanged_t2419067341 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define DistinctUntilChanged_OnNext_m3817983600(__this, ___value0, method) ((  void (*) (DistinctUntilChanged_t2419067341 *, Il2CppObject *, const MethodInfo*))DistinctUntilChanged_OnNext_m3817983600_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.DistinctUntilChangedObservable`2/DistinctUntilChanged<System.Object,System.Object>::OnError(System.Exception)
extern "C"  void DistinctUntilChanged_OnError_m2278230399_gshared (DistinctUntilChanged_t2419067341 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define DistinctUntilChanged_OnError_m2278230399(__this, ___error0, method) ((  void (*) (DistinctUntilChanged_t2419067341 *, Exception_t1967233988 *, const MethodInfo*))DistinctUntilChanged_OnError_m2278230399_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.DistinctUntilChangedObservable`2/DistinctUntilChanged<System.Object,System.Object>::OnCompleted()
extern "C"  void DistinctUntilChanged_OnCompleted_m1697956690_gshared (DistinctUntilChanged_t2419067341 * __this, const MethodInfo* method);
#define DistinctUntilChanged_OnCompleted_m1697956690(__this, method) ((  void (*) (DistinctUntilChanged_t2419067341 *, const MethodInfo*))DistinctUntilChanged_OnCompleted_m1697956690_gshared)(__this, method)
