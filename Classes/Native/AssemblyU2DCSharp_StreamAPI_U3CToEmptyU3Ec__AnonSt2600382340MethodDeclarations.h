﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StreamAPI/<ToEmpty>c__AnonStorey132`1/<ToEmpty>c__AnonStorey133`1<System.Object>
struct U3CToEmptyU3Ec__AnonStorey133_1_t2600382340;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void StreamAPI/<ToEmpty>c__AnonStorey132`1/<ToEmpty>c__AnonStorey133`1<System.Object>::.ctor()
extern "C"  void U3CToEmptyU3Ec__AnonStorey133_1__ctor_m1027732160_gshared (U3CToEmptyU3Ec__AnonStorey133_1_t2600382340 * __this, const MethodInfo* method);
#define U3CToEmptyU3Ec__AnonStorey133_1__ctor_m1027732160(__this, method) ((  void (*) (U3CToEmptyU3Ec__AnonStorey133_1_t2600382340 *, const MethodInfo*))U3CToEmptyU3Ec__AnonStorey133_1__ctor_m1027732160_gshared)(__this, method)
// System.Void StreamAPI/<ToEmpty>c__AnonStorey132`1/<ToEmpty>c__AnonStorey133`1<System.Object>::<>m__1CC(T)
extern "C"  void U3CToEmptyU3Ec__AnonStorey133_1_U3CU3Em__1CC_m2135939348_gshared (U3CToEmptyU3Ec__AnonStorey133_1_t2600382340 * __this, Il2CppObject * ____0, const MethodInfo* method);
#define U3CToEmptyU3Ec__AnonStorey133_1_U3CU3Em__1CC_m2135939348(__this, ____0, method) ((  void (*) (U3CToEmptyU3Ec__AnonStorey133_1_t2600382340 *, Il2CppObject *, const MethodInfo*))U3CToEmptyU3Ec__AnonStorey133_1_U3CU3Em__1CC_m2135939348_gshared)(__this, ____0, method)
