﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// IStream`1<System.Object>
struct IStream_1_t1389797411;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t1509682273;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StreamAPI/<Filter>c__AnonStorey134`1<System.Object>
struct  U3CFilterU3Ec__AnonStorey134_1_t1444715119  : public Il2CppObject
{
public:
	// IStream`1<T> StreamAPI/<Filter>c__AnonStorey134`1::stream
	Il2CppObject* ___stream_0;
	// System.Func`2<T,System.Boolean> StreamAPI/<Filter>c__AnonStorey134`1::filter
	Func_2_t1509682273 * ___filter_1;

public:
	inline static int32_t get_offset_of_stream_0() { return static_cast<int32_t>(offsetof(U3CFilterU3Ec__AnonStorey134_1_t1444715119, ___stream_0)); }
	inline Il2CppObject* get_stream_0() const { return ___stream_0; }
	inline Il2CppObject** get_address_of_stream_0() { return &___stream_0; }
	inline void set_stream_0(Il2CppObject* value)
	{
		___stream_0 = value;
		Il2CppCodeGenWriteBarrier(&___stream_0, value);
	}

	inline static int32_t get_offset_of_filter_1() { return static_cast<int32_t>(offsetof(U3CFilterU3Ec__AnonStorey134_1_t1444715119, ___filter_1)); }
	inline Func_2_t1509682273 * get_filter_1() const { return ___filter_1; }
	inline Func_2_t1509682273 ** get_address_of_filter_1() { return &___filter_1; }
	inline void set_filter_1(Func_2_t1509682273 * value)
	{
		___filter_1 = value;
		Il2CppCodeGenWriteBarrier(&___filter_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
