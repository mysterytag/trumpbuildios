﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.SchedulerQueue
struct SchedulerQueue_t3710535235;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_S3710535235.h"

// System.Void UniRx.Scheduler/CurrentThreadScheduler/Trampoline::Run(UniRx.InternalUtil.SchedulerQueue)
extern "C"  void Trampoline_Run_m2239505453 (Il2CppObject * __this /* static, unused */, SchedulerQueue_t3710535235 * ___queue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
