﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ScanObservable`1/Scan<System.Object>
struct Scan_t1424424718;
// UniRx.Operators.ScanObservable`1<System.Object>
struct ScanObservable_1_t4160826919;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.ScanObservable`1/Scan<System.Object>::.ctor(UniRx.Operators.ScanObservable`1<TSource>,UniRx.IObserver`1<TSource>,System.IDisposable)
extern "C"  void Scan__ctor_m3700370177_gshared (Scan_t1424424718 * __this, ScanObservable_1_t4160826919 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Scan__ctor_m3700370177(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Scan_t1424424718 *, ScanObservable_1_t4160826919 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Scan__ctor_m3700370177_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.ScanObservable`1/Scan<System.Object>::OnNext(TSource)
extern "C"  void Scan_OnNext_m1076706852_gshared (Scan_t1424424718 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Scan_OnNext_m1076706852(__this, ___value0, method) ((  void (*) (Scan_t1424424718 *, Il2CppObject *, const MethodInfo*))Scan_OnNext_m1076706852_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.ScanObservable`1/Scan<System.Object>::OnError(System.Exception)
extern "C"  void Scan_OnError_m471441550_gshared (Scan_t1424424718 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Scan_OnError_m471441550(__this, ___error0, method) ((  void (*) (Scan_t1424424718 *, Exception_t1967233988 *, const MethodInfo*))Scan_OnError_m471441550_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.ScanObservable`1/Scan<System.Object>::OnCompleted()
extern "C"  void Scan_OnCompleted_m2883205345_gshared (Scan_t1424424718 * __this, const MethodInfo* method);
#define Scan_OnCompleted_m2883205345(__this, method) ((  void (*) (Scan_t1424424718 *, const MethodInfo*))Scan_OnCompleted_m2883205345_gshared)(__this, method)
