﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.UnlinkIOSDeviceIDRequest
struct UnlinkIOSDeviceIDRequest_t1716723508;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.UnlinkIOSDeviceIDRequest::.ctor()
extern "C"  void UnlinkIOSDeviceIDRequest__ctor_m3299762569 (UnlinkIOSDeviceIDRequest_t1716723508 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.UnlinkIOSDeviceIDRequest::get_DeviceId()
extern "C"  String_t* UnlinkIOSDeviceIDRequest_get_DeviceId_m4244943794 (UnlinkIOSDeviceIDRequest_t1716723508 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UnlinkIOSDeviceIDRequest::set_DeviceId(System.String)
extern "C"  void UnlinkIOSDeviceIDRequest_set_DeviceId_m3496325945 (UnlinkIOSDeviceIDRequest_t1716723508 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
