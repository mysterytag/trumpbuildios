﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/SubtractUserVirtualCurrencyRequestCallback
struct SubtractUserVirtualCurrencyRequestCallback_t1466902807;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.SubtractUserVirtualCurrencyRequest
struct SubtractUserVirtualCurrencyRequest_t3004138178;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_SubtractUse3004138178.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/SubtractUserVirtualCurrencyRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void SubtractUserVirtualCurrencyRequestCallback__ctor_m1429794438 (SubtractUserVirtualCurrencyRequestCallback_t1466902807 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/SubtractUserVirtualCurrencyRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.SubtractUserVirtualCurrencyRequest,System.Object)
extern "C"  void SubtractUserVirtualCurrencyRequestCallback_Invoke_m3364665983 (SubtractUserVirtualCurrencyRequestCallback_t1466902807 * __this, String_t* ___urlPath0, int32_t ___callId1, SubtractUserVirtualCurrencyRequest_t3004138178 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_SubtractUserVirtualCurrencyRequestCallback_t1466902807(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, SubtractUserVirtualCurrencyRequest_t3004138178 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/SubtractUserVirtualCurrencyRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.SubtractUserVirtualCurrencyRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * SubtractUserVirtualCurrencyRequestCallback_BeginInvoke_m827382420 (SubtractUserVirtualCurrencyRequestCallback_t1466902807 * __this, String_t* ___urlPath0, int32_t ___callId1, SubtractUserVirtualCurrencyRequest_t3004138178 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/SubtractUserVirtualCurrencyRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void SubtractUserVirtualCurrencyRequestCallback_EndInvoke_m1973719958 (SubtractUserVirtualCurrencyRequestCallback_t1466902807 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
