﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/<CatchIgnore>c__AnonStorey3C`2<System.Object,System.Object>
struct U3CCatchIgnoreU3Ec__AnonStorey3C_2_t339163467;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.Observable/<CatchIgnore>c__AnonStorey3C`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CCatchIgnoreU3Ec__AnonStorey3C_2__ctor_m380469_gshared (U3CCatchIgnoreU3Ec__AnonStorey3C_2_t339163467 * __this, const MethodInfo* method);
#define U3CCatchIgnoreU3Ec__AnonStorey3C_2__ctor_m380469(__this, method) ((  void (*) (U3CCatchIgnoreU3Ec__AnonStorey3C_2_t339163467 *, const MethodInfo*))U3CCatchIgnoreU3Ec__AnonStorey3C_2__ctor_m380469_gshared)(__this, method)
// UniRx.IObservable`1<TSource> UniRx.Observable/<CatchIgnore>c__AnonStorey3C`2<System.Object,System.Object>::<>m__3E(TException)
extern "C"  Il2CppObject* U3CCatchIgnoreU3Ec__AnonStorey3C_2_U3CU3Em__3E_m644321494_gshared (U3CCatchIgnoreU3Ec__AnonStorey3C_2_t339163467 * __this, Il2CppObject * ___ex0, const MethodInfo* method);
#define U3CCatchIgnoreU3Ec__AnonStorey3C_2_U3CU3Em__3E_m644321494(__this, ___ex0, method) ((  Il2CppObject* (*) (U3CCatchIgnoreU3Ec__AnonStorey3C_2_t339163467 *, Il2CppObject *, const MethodInfo*))U3CCatchIgnoreU3Ec__AnonStorey3C_2_U3CU3Em__3E_m644321494_gshared)(__this, ___ex0, method)
