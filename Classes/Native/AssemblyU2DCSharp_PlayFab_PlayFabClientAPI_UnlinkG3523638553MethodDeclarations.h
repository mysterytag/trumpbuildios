﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/UnlinkGameCenterAccountResponseCallback
struct UnlinkGameCenterAccountResponseCallback_t3523638553;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.UnlinkGameCenterAccountRequest
struct UnlinkGameCenterAccountRequest_t687920076;
// PlayFab.ClientModels.UnlinkGameCenterAccountResult
struct UnlinkGameCenterAccountResult_t2689738080;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkGameCe687920076.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlinkGameC2689738080.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/UnlinkGameCenterAccountResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void UnlinkGameCenterAccountResponseCallback__ctor_m3535362344 (UnlinkGameCenterAccountResponseCallback_t3523638553 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UnlinkGameCenterAccountResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.UnlinkGameCenterAccountRequest,PlayFab.ClientModels.UnlinkGameCenterAccountResult,PlayFab.PlayFabError,System.Object)
extern "C"  void UnlinkGameCenterAccountResponseCallback_Invoke_m3105935161 (UnlinkGameCenterAccountResponseCallback_t3523638553 * __this, String_t* ___urlPath0, int32_t ___callId1, UnlinkGameCenterAccountRequest_t687920076 * ___request2, UnlinkGameCenterAccountResult_t2689738080 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_UnlinkGameCenterAccountResponseCallback_t3523638553(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, UnlinkGameCenterAccountRequest_t687920076 * ___request2, UnlinkGameCenterAccountResult_t2689738080 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/UnlinkGameCenterAccountResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.UnlinkGameCenterAccountRequest,PlayFab.ClientModels.UnlinkGameCenterAccountResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnlinkGameCenterAccountResponseCallback_BeginInvoke_m2439453734 (UnlinkGameCenterAccountResponseCallback_t3523638553 * __this, String_t* ___urlPath0, int32_t ___callId1, UnlinkGameCenterAccountRequest_t687920076 * ___request2, UnlinkGameCenterAccountResult_t2689738080 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UnlinkGameCenterAccountResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void UnlinkGameCenterAccountResponseCallback_EndInvoke_m280469304 (UnlinkGameCenterAccountResponseCallback_t3523638553 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
