﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.SelectManyObservable`3<System.Double,System.Double,System.Double>
struct SelectManyObservable_3_t44588665;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1921935565.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.SelectManyObservable`3/SelectManyEnumerableObserver<System.Double,System.Double,System.Double>
struct  SelectManyEnumerableObserver_t1450222884  : public OperatorObserverBase_2_t1921935565
{
public:
	// UniRx.Operators.SelectManyObservable`3<TSource,TCollection,TResult> UniRx.Operators.SelectManyObservable`3/SelectManyEnumerableObserver::parent
	SelectManyObservable_3_t44588665 * ___parent_2;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(SelectManyEnumerableObserver_t1450222884, ___parent_2)); }
	inline SelectManyObservable_3_t44588665 * get_parent_2() const { return ___parent_2; }
	inline SelectManyObservable_3_t44588665 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(SelectManyObservable_3_t44588665 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
