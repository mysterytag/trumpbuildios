﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ReactiveProperty`1<TableButtons/StatkaPoTableViev>
struct ReactiveProperty_1_t4116728855;
// UniRx.IObservable`1<TableButtons/StatkaPoTableViev>
struct IObservable_1_t3267194181;
// System.Collections.Generic.IEqualityComparer`1<TableButtons/StatkaPoTableViev>
struct IEqualityComparer_1_t1537695172;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<TableButtons/StatkaPoTableViev>
struct IObserver_1_t1425427424;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TableButtons_StatkaPoTableViev3508395817.h"

// System.Void UniRx.ReactiveProperty`1<TableButtons/StatkaPoTableViev>::.ctor()
extern "C"  void ReactiveProperty_1__ctor_m1349893103_gshared (ReactiveProperty_1_t4116728855 * __this, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m1349893103(__this, method) ((  void (*) (ReactiveProperty_1_t4116728855 *, const MethodInfo*))ReactiveProperty_1__ctor_m1349893103_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<TableButtons/StatkaPoTableViev>::.ctor(T)
extern "C"  void ReactiveProperty_1__ctor_m3938220827_gshared (ReactiveProperty_1_t4116728855 * __this, int32_t ___initialValue0, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m3938220827(__this, ___initialValue0, method) ((  void (*) (ReactiveProperty_1_t4116728855 *, int32_t, const MethodInfo*))ReactiveProperty_1__ctor_m3938220827_gshared)(__this, ___initialValue0, method)
// System.Void UniRx.ReactiveProperty`1<TableButtons/StatkaPoTableViev>::.ctor(UniRx.IObservable`1<T>)
extern "C"  void ReactiveProperty_1__ctor_m3258210370_gshared (ReactiveProperty_1_t4116728855 * __this, Il2CppObject* ___source0, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m3258210370(__this, ___source0, method) ((  void (*) (ReactiveProperty_1_t4116728855 *, Il2CppObject*, const MethodInfo*))ReactiveProperty_1__ctor_m3258210370_gshared)(__this, ___source0, method)
// System.Void UniRx.ReactiveProperty`1<TableButtons/StatkaPoTableViev>::.ctor(UniRx.IObservable`1<T>,T)
extern "C"  void ReactiveProperty_1__ctor_m109012314_gshared (ReactiveProperty_1_t4116728855 * __this, Il2CppObject* ___source0, int32_t ___initialValue1, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m109012314(__this, ___source0, ___initialValue1, method) ((  void (*) (ReactiveProperty_1_t4116728855 *, Il2CppObject*, int32_t, const MethodInfo*))ReactiveProperty_1__ctor_m109012314_gshared)(__this, ___source0, ___initialValue1, method)
// System.Void UniRx.ReactiveProperty`1<TableButtons/StatkaPoTableViev>::.cctor()
extern "C"  void ReactiveProperty_1__cctor_m3456123242_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ReactiveProperty_1__cctor_m3456123242(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ReactiveProperty_1__cctor_m3456123242_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1<TableButtons/StatkaPoTableViev>::get_EqualityComparer()
extern "C"  Il2CppObject* ReactiveProperty_1_get_EqualityComparer_m1026221296_gshared (ReactiveProperty_1_t4116728855 * __this, const MethodInfo* method);
#define ReactiveProperty_1_get_EqualityComparer_m1026221296(__this, method) ((  Il2CppObject* (*) (ReactiveProperty_1_t4116728855 *, const MethodInfo*))ReactiveProperty_1_get_EqualityComparer_m1026221296_gshared)(__this, method)
// T UniRx.ReactiveProperty`1<TableButtons/StatkaPoTableViev>::get_Value()
extern "C"  int32_t ReactiveProperty_1_get_Value_m1445003754_gshared (ReactiveProperty_1_t4116728855 * __this, const MethodInfo* method);
#define ReactiveProperty_1_get_Value_m1445003754(__this, method) ((  int32_t (*) (ReactiveProperty_1_t4116728855 *, const MethodInfo*))ReactiveProperty_1_get_Value_m1445003754_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<TableButtons/StatkaPoTableViev>::set_Value(T)
extern "C"  void ReactiveProperty_1_set_Value_m2212207081_gshared (ReactiveProperty_1_t4116728855 * __this, int32_t ___value0, const MethodInfo* method);
#define ReactiveProperty_1_set_Value_m2212207081(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t4116728855 *, int32_t, const MethodInfo*))ReactiveProperty_1_set_Value_m2212207081_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1<TableButtons/StatkaPoTableViev>::SetValue(T)
extern "C"  void ReactiveProperty_1_SetValue_m3205089966_gshared (ReactiveProperty_1_t4116728855 * __this, int32_t ___value0, const MethodInfo* method);
#define ReactiveProperty_1_SetValue_m3205089966(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t4116728855 *, int32_t, const MethodInfo*))ReactiveProperty_1_SetValue_m3205089966_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1<TableButtons/StatkaPoTableViev>::SetValueAndForceNotify(T)
extern "C"  void ReactiveProperty_1_SetValueAndForceNotify_m1745324017_gshared (ReactiveProperty_1_t4116728855 * __this, int32_t ___value0, const MethodInfo* method);
#define ReactiveProperty_1_SetValueAndForceNotify_m1745324017(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t4116728855 *, int32_t, const MethodInfo*))ReactiveProperty_1_SetValueAndForceNotify_m1745324017_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.ReactiveProperty`1<TableButtons/StatkaPoTableViev>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * ReactiveProperty_1_Subscribe_m837270370_gshared (ReactiveProperty_1_t4116728855 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ReactiveProperty_1_Subscribe_m837270370(__this, ___observer0, method) ((  Il2CppObject * (*) (ReactiveProperty_1_t4116728855 *, Il2CppObject*, const MethodInfo*))ReactiveProperty_1_Subscribe_m837270370_gshared)(__this, ___observer0, method)
// System.Void UniRx.ReactiveProperty`1<TableButtons/StatkaPoTableViev>::Dispose()
extern "C"  void ReactiveProperty_1_Dispose_m1724001824_gshared (ReactiveProperty_1_t4116728855 * __this, const MethodInfo* method);
#define ReactiveProperty_1_Dispose_m1724001824(__this, method) ((  void (*) (ReactiveProperty_1_t4116728855 *, const MethodInfo*))ReactiveProperty_1_Dispose_m1724001824_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<TableButtons/StatkaPoTableViev>::Dispose(System.Boolean)
extern "C"  void ReactiveProperty_1_Dispose_m372091799_gshared (ReactiveProperty_1_t4116728855 * __this, bool ___disposing0, const MethodInfo* method);
#define ReactiveProperty_1_Dispose_m372091799(__this, ___disposing0, method) ((  void (*) (ReactiveProperty_1_t4116728855 *, bool, const MethodInfo*))ReactiveProperty_1_Dispose_m372091799_gshared)(__this, ___disposing0, method)
// System.String UniRx.ReactiveProperty`1<TableButtons/StatkaPoTableViev>::ToString()
extern "C"  String_t* ReactiveProperty_1_ToString_m2953153360_gshared (ReactiveProperty_1_t4116728855 * __this, const MethodInfo* method);
#define ReactiveProperty_1_ToString_m2953153360(__this, method) ((  String_t* (*) (ReactiveProperty_1_t4116728855 *, const MethodInfo*))ReactiveProperty_1_ToString_m2953153360_gshared)(__this, method)
// System.Boolean UniRx.ReactiveProperty`1<TableButtons/StatkaPoTableViev>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m3543207104_gshared (ReactiveProperty_1_t4116728855 * __this, const MethodInfo* method);
#define ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m3543207104(__this, method) ((  bool (*) (ReactiveProperty_1_t4116728855 *, const MethodInfo*))ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m3543207104_gshared)(__this, method)
