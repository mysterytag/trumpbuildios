﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XPath.XPathFunctionSubstring
struct XPathFunctionSubstring_t1649127225;
// System.Xml.XPath.FunctionArguments
struct FunctionArguments_t3402201403;
// System.Object
struct Il2CppObject;
// System.Xml.XPath.BaseIterator
struct BaseIterator_t3696600956;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XPath_FunctionArguments3402201403.h"
#include "System_Xml_System_Xml_XPath_XPathResultType370286193.h"
#include "System_Xml_System_Xml_XPath_BaseIterator3696600956.h"

// System.Void System.Xml.XPath.XPathFunctionSubstring::.ctor(System.Xml.XPath.FunctionArguments)
extern "C"  void XPathFunctionSubstring__ctor_m2565884615 (XPathFunctionSubstring_t1649127225 * __this, FunctionArguments_t3402201403 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionSubstring::get_ReturnType()
extern "C"  int32_t XPathFunctionSubstring_get_ReturnType_m3917299253 (XPathFunctionSubstring_t1649127225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.XPathFunctionSubstring::get_Peer()
extern "C"  bool XPathFunctionSubstring_get_Peer_m3084538793 (XPathFunctionSubstring_t1649127225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Xml.XPath.XPathFunctionSubstring::Evaluate(System.Xml.XPath.BaseIterator)
extern "C"  Il2CppObject * XPathFunctionSubstring_Evaluate_m2782613730 (XPathFunctionSubstring_t1649127225 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XPath.XPathFunctionSubstring::ToString()
extern "C"  String_t* XPathFunctionSubstring_ToString_m97244847 (XPathFunctionSubstring_t1649127225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
