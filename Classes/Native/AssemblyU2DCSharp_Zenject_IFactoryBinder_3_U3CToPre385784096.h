﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t4012695102;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.IFactoryBinder`3/<ToPrefab>c__AnonStorey179<System.Object,System.Object,System.Object>
struct  U3CToPrefabU3Ec__AnonStorey179_t385784096  : public Il2CppObject
{
public:
	// UnityEngine.GameObject Zenject.IFactoryBinder`3/<ToPrefab>c__AnonStorey179::prefab
	GameObject_t4012695102 * ___prefab_0;

public:
	inline static int32_t get_offset_of_prefab_0() { return static_cast<int32_t>(offsetof(U3CToPrefabU3Ec__AnonStorey179_t385784096, ___prefab_0)); }
	inline GameObject_t4012695102 * get_prefab_0() const { return ___prefab_0; }
	inline GameObject_t4012695102 ** get_address_of_prefab_0() { return &___prefab_0; }
	inline void set_prefab_0(GameObject_t4012695102 * value)
	{
		___prefab_0 = value;
		Il2CppCodeGenWriteBarrier(&___prefab_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
