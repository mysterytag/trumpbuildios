﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Cell`1/<OnChanged>c__AnonStoreyF3<System.Int32>
struct U3COnChangedU3Ec__AnonStoreyF3_t1928596255;

#include "codegen/il2cpp-codegen.h"

// System.Void Cell`1/<OnChanged>c__AnonStoreyF3<System.Int32>::.ctor()
extern "C"  void U3COnChangedU3Ec__AnonStoreyF3__ctor_m2048617197_gshared (U3COnChangedU3Ec__AnonStoreyF3_t1928596255 * __this, const MethodInfo* method);
#define U3COnChangedU3Ec__AnonStoreyF3__ctor_m2048617197(__this, method) ((  void (*) (U3COnChangedU3Ec__AnonStoreyF3_t1928596255 *, const MethodInfo*))U3COnChangedU3Ec__AnonStoreyF3__ctor_m2048617197_gshared)(__this, method)
// System.Void Cell`1/<OnChanged>c__AnonStoreyF3<System.Int32>::<>m__162(T)
extern "C"  void U3COnChangedU3Ec__AnonStoreyF3_U3CU3Em__162_m1147700965_gshared (U3COnChangedU3Ec__AnonStoreyF3_t1928596255 * __this, int32_t ____0, const MethodInfo* method);
#define U3COnChangedU3Ec__AnonStoreyF3_U3CU3Em__162_m1147700965(__this, ____0, method) ((  void (*) (U3COnChangedU3Ec__AnonStoreyF3_t1928596255 *, int32_t, const MethodInfo*))U3COnChangedU3Ec__AnonStoreyF3_U3CU3Em__162_m1147700965_gshared)(__this, ____0, method)
