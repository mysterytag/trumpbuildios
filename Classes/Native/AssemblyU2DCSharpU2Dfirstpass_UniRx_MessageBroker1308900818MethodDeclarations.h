﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.MessageBroker
struct MessageBroker_t1308900818;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.MessageBroker::.ctor()
extern "C"  void MessageBroker__ctor_m402680655 (MessageBroker_t1308900818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.MessageBroker::.cctor()
extern "C"  void MessageBroker__cctor_m3411069502 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.MessageBroker::Dispose()
extern "C"  void MessageBroker_Dispose_m327335884 (MessageBroker_t1308900818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
