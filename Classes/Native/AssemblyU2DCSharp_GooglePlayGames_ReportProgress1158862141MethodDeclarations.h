﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.ReportProgress
struct ReportProgress_t1158862141;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Action`1<System.Boolean>
struct Action_1_t359458046;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void GooglePlayGames.ReportProgress::.ctor(System.Object,System.IntPtr)
extern "C"  void ReportProgress__ctor_m230147170 (ReportProgress_t1158862141 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.ReportProgress::Invoke(System.String,System.Double,System.Action`1<System.Boolean>)
extern "C"  void ReportProgress_Invoke_m485975983 (ReportProgress_t1158862141 * __this, String_t* ___id0, double ___progress1, Action_1_t359458046 * ___callback2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult GooglePlayGames.ReportProgress::BeginInvoke(System.String,System.Double,System.Action`1<System.Boolean>,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ReportProgress_BeginInvoke_m1945770370 (ReportProgress_t1158862141 * __this, String_t* ___id0, double ___progress1, Action_1_t359458046 * ___callback2, AsyncCallback_t1363551830 * ____callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GooglePlayGames.ReportProgress::EndInvoke(System.IAsyncResult)
extern "C"  void ReportProgress_EndInvoke_m2865805170 (ReportProgress_t1158862141 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
