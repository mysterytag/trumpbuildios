﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Examples.Sample08_DetectDoubleClick
struct Sample08_DetectDoubleClick_t1711438338;
// System.Collections.Generic.IList`1<System.Int64>
struct IList_1_t718939900;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Examples.Sample08_DetectDoubleClick::.ctor()
extern "C"  void Sample08_DetectDoubleClick__ctor_m778062339 (Sample08_DetectDoubleClick_t1711438338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample08_DetectDoubleClick::Start()
extern "C"  void Sample08_DetectDoubleClick_Start_m4020167427 (Sample08_DetectDoubleClick_t1711438338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.Examples.Sample08_DetectDoubleClick::<Start>m__21(System.Int64)
extern "C"  bool Sample08_DetectDoubleClick_U3CStartU3Em__21_m3995005579 (Il2CppObject * __this /* static, unused */, int64_t ____0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.Examples.Sample08_DetectDoubleClick::<Start>m__22(System.Collections.Generic.IList`1<System.Int64>)
extern "C"  bool Sample08_DetectDoubleClick_U3CStartU3Em__22_m691380157 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___xs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample08_DetectDoubleClick::<Start>m__23(System.Collections.Generic.IList`1<System.Int64>)
extern "C"  void Sample08_DetectDoubleClick_U3CStartU3Em__23_m3792214938 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___xs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
