﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ObserveOnObservable`1/ObserveOn/<OnNext>c__AnonStorey61<System.Object>
struct U3COnNextU3Ec__AnonStorey61_t1362241787;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn/<OnNext>c__AnonStorey61<System.Object>::.ctor()
extern "C"  void U3COnNextU3Ec__AnonStorey61__ctor_m509757722_gshared (U3COnNextU3Ec__AnonStorey61_t1362241787 * __this, const MethodInfo* method);
#define U3COnNextU3Ec__AnonStorey61__ctor_m509757722(__this, method) ((  void (*) (U3COnNextU3Ec__AnonStorey61_t1362241787 *, const MethodInfo*))U3COnNextU3Ec__AnonStorey61__ctor_m509757722_gshared)(__this, method)
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn/<OnNext>c__AnonStorey61<System.Object>::<>m__7E()
extern "C"  void U3COnNextU3Ec__AnonStorey61_U3CU3Em__7E_m2559138001_gshared (U3COnNextU3Ec__AnonStorey61_t1362241787 * __this, const MethodInfo* method);
#define U3COnNextU3Ec__AnonStorey61_U3CU3Em__7E_m2559138001(__this, method) ((  void (*) (U3COnNextU3Ec__AnonStorey61_t1362241787 *, const MethodInfo*))U3COnNextU3Ec__AnonStorey61_U3CU3Em__7E_m2559138001_gshared)(__this, method)
