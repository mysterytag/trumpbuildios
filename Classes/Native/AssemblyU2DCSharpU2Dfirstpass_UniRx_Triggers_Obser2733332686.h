﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Subject`1<UniRx.Unit>
struct Subject_1_t201353362;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Triggers_Obser1883237895.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableTransformChangedTrigger
struct  ObservableTransformChangedTrigger_t2733332686  : public ObservableTriggerBase_t1883237895
{
public:
	// UniRx.Subject`1<UniRx.Unit> UniRx.Triggers.ObservableTransformChangedTrigger::onBeforeTransformParentChanged
	Subject_1_t201353362 * ___onBeforeTransformParentChanged_8;
	// UniRx.Subject`1<UniRx.Unit> UniRx.Triggers.ObservableTransformChangedTrigger::onTransformParentChanged
	Subject_1_t201353362 * ___onTransformParentChanged_9;
	// UniRx.Subject`1<UniRx.Unit> UniRx.Triggers.ObservableTransformChangedTrigger::onTransformChildrenChanged
	Subject_1_t201353362 * ___onTransformChildrenChanged_10;

public:
	inline static int32_t get_offset_of_onBeforeTransformParentChanged_8() { return static_cast<int32_t>(offsetof(ObservableTransformChangedTrigger_t2733332686, ___onBeforeTransformParentChanged_8)); }
	inline Subject_1_t201353362 * get_onBeforeTransformParentChanged_8() const { return ___onBeforeTransformParentChanged_8; }
	inline Subject_1_t201353362 ** get_address_of_onBeforeTransformParentChanged_8() { return &___onBeforeTransformParentChanged_8; }
	inline void set_onBeforeTransformParentChanged_8(Subject_1_t201353362 * value)
	{
		___onBeforeTransformParentChanged_8 = value;
		Il2CppCodeGenWriteBarrier(&___onBeforeTransformParentChanged_8, value);
	}

	inline static int32_t get_offset_of_onTransformParentChanged_9() { return static_cast<int32_t>(offsetof(ObservableTransformChangedTrigger_t2733332686, ___onTransformParentChanged_9)); }
	inline Subject_1_t201353362 * get_onTransformParentChanged_9() const { return ___onTransformParentChanged_9; }
	inline Subject_1_t201353362 ** get_address_of_onTransformParentChanged_9() { return &___onTransformParentChanged_9; }
	inline void set_onTransformParentChanged_9(Subject_1_t201353362 * value)
	{
		___onTransformParentChanged_9 = value;
		Il2CppCodeGenWriteBarrier(&___onTransformParentChanged_9, value);
	}

	inline static int32_t get_offset_of_onTransformChildrenChanged_10() { return static_cast<int32_t>(offsetof(ObservableTransformChangedTrigger_t2733332686, ___onTransformChildrenChanged_10)); }
	inline Subject_1_t201353362 * get_onTransformChildrenChanged_10() const { return ___onTransformChildrenChanged_10; }
	inline Subject_1_t201353362 ** get_address_of_onTransformChildrenChanged_10() { return &___onTransformChildrenChanged_10; }
	inline void set_onTransformChildrenChanged_10(Subject_1_t201353362 * value)
	{
		___onTransformChildrenChanged_10 = value;
		Il2CppCodeGenWriteBarrier(&___onTransformChildrenChanged_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
