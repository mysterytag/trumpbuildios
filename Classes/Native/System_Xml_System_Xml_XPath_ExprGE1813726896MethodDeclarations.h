﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XPath.ExprGE
struct ExprGE_t1813726896;
// System.Xml.XPath.Expression
struct Expression_t4217024437;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XPath_Expression4217024437.h"

// System.Void System.Xml.XPath.ExprGE::.ctor(System.Xml.XPath.Expression,System.Xml.XPath.Expression)
extern "C"  void ExprGE__ctor_m4218213977 (ExprGE_t1813726896 * __this, Expression_t4217024437 * ___left0, Expression_t4217024437 * ___right1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XPath.ExprGE::get_Operator()
extern "C"  String_t* ExprGE_get_Operator_m3778930663 (ExprGE_t1813726896 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.ExprGE::Compare(System.Double,System.Double)
extern "C"  bool ExprGE_Compare_m601870988 (ExprGE_t1813726896 * __this, double ___arg10, double ___arg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
