﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.UnityEqualityComparer/ColorEqualityComparer
struct ColorEqualityComparer_t2896313514;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"

// System.Void UniRx.UnityEqualityComparer/ColorEqualityComparer::.ctor()
extern "C"  void ColorEqualityComparer__ctor_m539456312 (ColorEqualityComparer_t2896313514 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.UnityEqualityComparer/ColorEqualityComparer::Equals(UnityEngine.Color,UnityEngine.Color)
extern "C"  bool ColorEqualityComparer_Equals_m3761042295 (ColorEqualityComparer_t2896313514 * __this, Color_t1588175760  ___self0, Color_t1588175760  ___other1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UniRx.UnityEqualityComparer/ColorEqualityComparer::GetHashCode(UnityEngine.Color)
extern "C"  int32_t ColorEqualityComparer_GetHashCode_m1795945449 (ColorEqualityComparer_t2896313514 * __this, Color_t1588175760  ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
