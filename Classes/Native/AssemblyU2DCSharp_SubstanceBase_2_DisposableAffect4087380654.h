﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SubstanceBase`2/Intrusion<System.Double,System.Double>
struct Intrusion_t3779021028;
// SubstanceBase`2<System.Double,System.Double>
struct SubstanceBase_2_t1274403272;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SubstanceBase`2/DisposableAffect<System.Double,System.Double>
struct  DisposableAffect_t4087380654  : public Il2CppObject
{
public:
	// SubstanceBase`2/Intrusion<T,InfT> SubstanceBase`2/DisposableAffect::intrusion
	Intrusion_t3779021028 * ___intrusion_0;
	// SubstanceBase`2<T,InfT> SubstanceBase`2/DisposableAffect::substance
	SubstanceBase_2_t1274403272 * ___substance_1;

public:
	inline static int32_t get_offset_of_intrusion_0() { return static_cast<int32_t>(offsetof(DisposableAffect_t4087380654, ___intrusion_0)); }
	inline Intrusion_t3779021028 * get_intrusion_0() const { return ___intrusion_0; }
	inline Intrusion_t3779021028 ** get_address_of_intrusion_0() { return &___intrusion_0; }
	inline void set_intrusion_0(Intrusion_t3779021028 * value)
	{
		___intrusion_0 = value;
		Il2CppCodeGenWriteBarrier(&___intrusion_0, value);
	}

	inline static int32_t get_offset_of_substance_1() { return static_cast<int32_t>(offsetof(DisposableAffect_t4087380654, ___substance_1)); }
	inline SubstanceBase_2_t1274403272 * get_substance_1() const { return ___substance_1; }
	inline SubstanceBase_2_t1274403272 ** get_address_of_substance_1() { return &___substance_1; }
	inline void set_substance_1(SubstanceBase_2_t1274403272 * value)
	{
		___substance_1 = value;
		Il2CppCodeGenWriteBarrier(&___substance_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
