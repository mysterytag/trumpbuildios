﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Vector2>
struct DisposedObserver_1_t2699787250;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"

// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Vector2>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m3697696368_gshared (DisposedObserver_1_t2699787250 * __this, const MethodInfo* method);
#define DisposedObserver_1__ctor_m3697696368(__this, method) ((  void (*) (DisposedObserver_1_t2699787250 *, const MethodInfo*))DisposedObserver_1__ctor_m3697696368_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Vector2>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m2477341501_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define DisposedObserver_1__cctor_m2477341501(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))DisposedObserver_1__cctor_m2477341501_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Vector2>::OnCompleted()
extern "C"  void DisposedObserver_1_OnCompleted_m2538405370_gshared (DisposedObserver_1_t2699787250 * __this, const MethodInfo* method);
#define DisposedObserver_1_OnCompleted_m2538405370(__this, method) ((  void (*) (DisposedObserver_1_t2699787250 *, const MethodInfo*))DisposedObserver_1_OnCompleted_m2538405370_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Vector2>::OnError(System.Exception)
extern "C"  void DisposedObserver_1_OnError_m2337835047_gshared (DisposedObserver_1_t2699787250 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define DisposedObserver_1_OnError_m2337835047(__this, ___error0, method) ((  void (*) (DisposedObserver_1_t2699787250 *, Exception_t1967233988 *, const MethodInfo*))DisposedObserver_1_OnError_m2337835047_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Vector2>::OnNext(T)
extern "C"  void DisposedObserver_1_OnNext_m83991832_gshared (DisposedObserver_1_t2699787250 * __this, Vector2_t3525329788  ___value0, const MethodInfo* method);
#define DisposedObserver_1_OnNext_m83991832(__this, ___value0, method) ((  void (*) (DisposedObserver_1_t2699787250 *, Vector2_t3525329788 , const MethodInfo*))DisposedObserver_1_OnNext_m83991832_gshared)(__this, ___value0, method)
