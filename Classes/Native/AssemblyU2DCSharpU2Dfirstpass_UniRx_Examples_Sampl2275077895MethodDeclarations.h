﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Examples.Sample05_ConvertFromCoroutine/<GetWWWCore>c__Iterator6
struct U3CGetWWWCoreU3Ec__Iterator6_t2275077895;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Examples.Sample05_ConvertFromCoroutine/<GetWWWCore>c__Iterator6::.ctor()
extern "C"  void U3CGetWWWCoreU3Ec__Iterator6__ctor_m350843295 (U3CGetWWWCoreU3Ec__Iterator6_t2275077895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UniRx.Examples.Sample05_ConvertFromCoroutine/<GetWWWCore>c__Iterator6::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGetWWWCoreU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2584035795 (U3CGetWWWCoreU3Ec__Iterator6_t2275077895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UniRx.Examples.Sample05_ConvertFromCoroutine/<GetWWWCore>c__Iterator6::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetWWWCoreU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m1621314407 (U3CGetWWWCoreU3Ec__Iterator6_t2275077895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.Examples.Sample05_ConvertFromCoroutine/<GetWWWCore>c__Iterator6::MoveNext()
extern "C"  bool U3CGetWWWCoreU3Ec__Iterator6_MoveNext_m2321053965 (U3CGetWWWCoreU3Ec__Iterator6_t2275077895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample05_ConvertFromCoroutine/<GetWWWCore>c__Iterator6::Dispose()
extern "C"  void U3CGetWWWCoreU3Ec__Iterator6_Dispose_m2051240476 (U3CGetWWWCoreU3Ec__Iterator6_t2275077895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample05_ConvertFromCoroutine/<GetWWWCore>c__Iterator6::Reset()
extern "C"  void U3CGetWWWCoreU3Ec__Iterator6_Reset_m2292243532 (U3CGetWWWCoreU3Ec__Iterator6_t2275077895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
