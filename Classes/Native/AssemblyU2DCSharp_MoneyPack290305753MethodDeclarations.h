﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MoneyPack
struct MoneyPack_t290305753;
// UnityEngine.Sprite
struct Sprite_t4006040370;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Sprite4006040370.h"

// System.Void MoneyPack::.ctor()
extern "C"  void MoneyPack__ctor_m3675980466 (MoneyPack_t290305753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoneyPack::Awake()
extern "C"  void MoneyPack_Awake_m3913585685 (MoneyPack_t290305753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoneyPack::Start()
extern "C"  void MoneyPack_Start_m2623118258 (MoneyPack_t290305753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoneyPack::UpdateSprite(UnityEngine.Sprite,UnityEngine.Sprite,UnityEngine.Sprite)
extern "C"  void MoneyPack_UpdateSprite_m367442564 (MoneyPack_t290305753 * __this, Sprite_t4006040370 * ___spr0, Sprite_t4006040370 * ___spriteMini1, Sprite_t4006040370 * ___spriteMini22, const MethodInfo* method) IL2CPP_METHOD_ATTR;
