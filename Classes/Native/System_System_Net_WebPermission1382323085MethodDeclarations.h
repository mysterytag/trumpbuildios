﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.WebPermission
struct WebPermission_t1382323085;
// System.String
struct String_t;
// System.Text.RegularExpressions.Regex
struct Regex_t3802381858;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Net.WebPermissionInfo
struct WebPermissionInfo_t538092251;
// System.Security.IPermission
struct IPermission_t2562055037;
// System.Collections.ArrayList
struct ArrayList_t2121638921;
// System.Security.SecurityElement
struct SecurityElement_t2475331585;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_PermissionSta3569926873.h"
#include "System_System_Net_NetworkAccess3133366396.h"
#include "mscorlib_System_String968488902.h"
#include "System_System_Text_RegularExpressions_Regex3802381858.h"
#include "System_System_Net_WebPermissionInfo538092251.h"
#include "System_System_Net_WebPermission1382323085.h"
#include "mscorlib_System_Collections_ArrayList2121638921.h"
#include "mscorlib_System_Security_SecurityElement2475331585.h"

// System.Void System.Net.WebPermission::.ctor()
extern "C"  void WebPermission__ctor_m1549643113 (WebPermission_t1382323085 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebPermission::.ctor(System.Security.Permissions.PermissionState)
extern "C"  void WebPermission__ctor_m3866252032 (WebPermission_t1382323085 * __this, int32_t ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebPermission::.ctor(System.Net.NetworkAccess,System.String)
extern "C"  void WebPermission__ctor_m584781091 (WebPermission_t1382323085 * __this, int32_t ___access0, String_t* ___uriString1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebPermission::.ctor(System.Net.NetworkAccess,System.Text.RegularExpressions.Regex)
extern "C"  void WebPermission__ctor_m1985854757 (WebPermission_t1382323085 * __this, int32_t ___access0, Regex_t3802381858 * ___uriRegex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Net.WebPermission::get_AcceptList()
extern "C"  Il2CppObject * WebPermission_get_AcceptList_m696783476 (WebPermission_t1382323085 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Net.WebPermission::get_ConnectList()
extern "C"  Il2CppObject * WebPermission_get_ConnectList_m172517404 (WebPermission_t1382323085 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebPermission::AddPermission(System.Net.NetworkAccess,System.String)
extern "C"  void WebPermission_AddPermission_m2217032465 (WebPermission_t1382323085 * __this, int32_t ___access0, String_t* ___uriString1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebPermission::AddPermission(System.Net.NetworkAccess,System.Text.RegularExpressions.Regex)
extern "C"  void WebPermission_AddPermission_m3512976247 (WebPermission_t1382323085 * __this, int32_t ___access0, Regex_t3802381858 * ___uriRegex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebPermission::AddPermission(System.Net.NetworkAccess,System.Net.WebPermissionInfo)
extern "C"  void WebPermission_AddPermission_m54006992 (WebPermission_t1382323085 * __this, int32_t ___access0, WebPermissionInfo_t538092251 * ___info1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Net.WebPermission::Copy()
extern "C"  Il2CppObject * WebPermission_Copy_m3594106362 (WebPermission_t1382323085 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Net.WebPermission::Intersect(System.Security.IPermission)
extern "C"  Il2CppObject * WebPermission_Intersect_m1170437069 (WebPermission_t1382323085 * __this, Il2CppObject * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.WebPermission::IntersectEmpty(System.Net.WebPermission)
extern "C"  bool WebPermission_IntersectEmpty_m3541129228 (WebPermission_t1382323085 * __this, WebPermission_t1382323085 * ___permission0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebPermission::Intersect(System.Collections.ArrayList,System.Collections.ArrayList,System.Collections.ArrayList)
extern "C"  void WebPermission_Intersect_m966062855 (WebPermission_t1382323085 * __this, ArrayList_t2121638921 * ___list10, ArrayList_t2121638921 * ___list21, ArrayList_t2121638921 * ___result2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.WebPermission::IsSubsetOf(System.Security.IPermission)
extern "C"  bool WebPermission_IsSubsetOf_m874196341 (WebPermission_t1382323085 * __this, Il2CppObject * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.WebPermission::IsSubsetOf(System.Collections.ArrayList,System.Collections.ArrayList)
extern "C"  bool WebPermission_IsSubsetOf_m34051122 (WebPermission_t1382323085 * __this, ArrayList_t2121638921 * ___list10, ArrayList_t2121638921 * ___list21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.WebPermission::IsUnrestricted()
extern "C"  bool WebPermission_IsUnrestricted_m2192077263 (WebPermission_t1382323085 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Net.WebPermission::ToXml()
extern "C"  SecurityElement_t2475331585 * WebPermission_ToXml_m4288859997 (WebPermission_t1382323085 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebPermission::ToXml(System.Security.SecurityElement,System.String,System.Collections.IEnumerator)
extern "C"  void WebPermission_ToXml_m2312899251 (WebPermission_t1382323085 * __this, SecurityElement_t2475331585 * ___root0, String_t* ___childName1, Il2CppObject * ___enumerator2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebPermission::FromXml(System.Security.SecurityElement)
extern "C"  void WebPermission_FromXml_m359151377 (WebPermission_t1382323085 * __this, SecurityElement_t2475331585 * ___securityElement0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebPermission::FromXml(System.Collections.ArrayList,System.Net.NetworkAccess)
extern "C"  void WebPermission_FromXml_m415572623 (WebPermission_t1382323085 * __this, ArrayList_t2121638921 * ___endpoints0, int32_t ___access1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Net.WebPermission::Union(System.Security.IPermission)
extern "C"  Il2CppObject * WebPermission_Union_m1618354717 (WebPermission_t1382323085 * __this, Il2CppObject * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
