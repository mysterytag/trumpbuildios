﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.AsyncSubject`1<UniRx.Unit>
struct AsyncSubject_1_t3257925142;
// System.Exception
struct Exception_t1967233988;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.AsyncSubject`1<UniRx.Unit>::.ctor()
extern "C"  void AsyncSubject_1__ctor_m781162576_gshared (AsyncSubject_1_t3257925142 * __this, const MethodInfo* method);
#define AsyncSubject_1__ctor_m781162576(__this, method) ((  void (*) (AsyncSubject_1_t3257925142 *, const MethodInfo*))AsyncSubject_1__ctor_m781162576_gshared)(__this, method)
// T UniRx.AsyncSubject`1<UniRx.Unit>::get_Value()
extern "C"  Unit_t2558286038  AsyncSubject_1_get_Value_m4281593879_gshared (AsyncSubject_1_t3257925142 * __this, const MethodInfo* method);
#define AsyncSubject_1_get_Value_m4281593879(__this, method) ((  Unit_t2558286038  (*) (AsyncSubject_1_t3257925142 *, const MethodInfo*))AsyncSubject_1_get_Value_m4281593879_gshared)(__this, method)
// System.Boolean UniRx.AsyncSubject`1<UniRx.Unit>::get_HasObservers()
extern "C"  bool AsyncSubject_1_get_HasObservers_m2378705980_gshared (AsyncSubject_1_t3257925142 * __this, const MethodInfo* method);
#define AsyncSubject_1_get_HasObservers_m2378705980(__this, method) ((  bool (*) (AsyncSubject_1_t3257925142 *, const MethodInfo*))AsyncSubject_1_get_HasObservers_m2378705980_gshared)(__this, method)
// System.Boolean UniRx.AsyncSubject`1<UniRx.Unit>::get_IsCompleted()
extern "C"  bool AsyncSubject_1_get_IsCompleted_m1977826538_gshared (AsyncSubject_1_t3257925142 * __this, const MethodInfo* method);
#define AsyncSubject_1_get_IsCompleted_m1977826538(__this, method) ((  bool (*) (AsyncSubject_1_t3257925142 *, const MethodInfo*))AsyncSubject_1_get_IsCompleted_m1977826538_gshared)(__this, method)
// System.Void UniRx.AsyncSubject`1<UniRx.Unit>::OnCompleted()
extern "C"  void AsyncSubject_1_OnCompleted_m1935364058_gshared (AsyncSubject_1_t3257925142 * __this, const MethodInfo* method);
#define AsyncSubject_1_OnCompleted_m1935364058(__this, method) ((  void (*) (AsyncSubject_1_t3257925142 *, const MethodInfo*))AsyncSubject_1_OnCompleted_m1935364058_gshared)(__this, method)
// System.Void UniRx.AsyncSubject`1<UniRx.Unit>::OnError(System.Exception)
extern "C"  void AsyncSubject_1_OnError_m3927640583_gshared (AsyncSubject_1_t3257925142 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define AsyncSubject_1_OnError_m3927640583(__this, ___error0, method) ((  void (*) (AsyncSubject_1_t3257925142 *, Exception_t1967233988 *, const MethodInfo*))AsyncSubject_1_OnError_m3927640583_gshared)(__this, ___error0, method)
// System.Void UniRx.AsyncSubject`1<UniRx.Unit>::OnNext(T)
extern "C"  void AsyncSubject_1_OnNext_m1908662008_gshared (AsyncSubject_1_t3257925142 * __this, Unit_t2558286038  ___value0, const MethodInfo* method);
#define AsyncSubject_1_OnNext_m1908662008(__this, ___value0, method) ((  void (*) (AsyncSubject_1_t3257925142 *, Unit_t2558286038 , const MethodInfo*))AsyncSubject_1_OnNext_m1908662008_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.AsyncSubject`1<UniRx.Unit>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * AsyncSubject_1_Subscribe_m931781903_gshared (AsyncSubject_1_t3257925142 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define AsyncSubject_1_Subscribe_m931781903(__this, ___observer0, method) ((  Il2CppObject * (*) (AsyncSubject_1_t3257925142 *, Il2CppObject*, const MethodInfo*))AsyncSubject_1_Subscribe_m931781903_gshared)(__this, ___observer0, method)
// System.Void UniRx.AsyncSubject`1<UniRx.Unit>::Dispose()
extern "C"  void AsyncSubject_1_Dispose_m3271209101_gshared (AsyncSubject_1_t3257925142 * __this, const MethodInfo* method);
#define AsyncSubject_1_Dispose_m3271209101(__this, method) ((  void (*) (AsyncSubject_1_t3257925142 *, const MethodInfo*))AsyncSubject_1_Dispose_m3271209101_gshared)(__this, method)
// System.Void UniRx.AsyncSubject`1<UniRx.Unit>::ThrowIfDisposed()
extern "C"  void AsyncSubject_1_ThrowIfDisposed_m3994647830_gshared (AsyncSubject_1_t3257925142 * __this, const MethodInfo* method);
#define AsyncSubject_1_ThrowIfDisposed_m3994647830(__this, method) ((  void (*) (AsyncSubject_1_t3257925142 *, const MethodInfo*))AsyncSubject_1_ThrowIfDisposed_m3994647830_gshared)(__this, method)
// System.Boolean UniRx.AsyncSubject`1<UniRx.Unit>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool AsyncSubject_1_IsRequiredSubscribeOnCurrentThread_m4252009011_gshared (AsyncSubject_1_t3257925142 * __this, const MethodInfo* method);
#define AsyncSubject_1_IsRequiredSubscribeOnCurrentThread_m4252009011(__this, method) ((  bool (*) (AsyncSubject_1_t3257925142 *, const MethodInfo*))AsyncSubject_1_IsRequiredSubscribeOnCurrentThread_m4252009011_gshared)(__this, method)
