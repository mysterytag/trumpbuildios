﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// UniRx.IObservable`1<System.Int64>
struct IObservable_1_t2606213246;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4196218687.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.SubscribeOnMainThreadObservable`1<System.Object>
struct  SubscribeOnMainThreadObservable_1_t1377860062  : public OperatorObservableBase_1_t4196218687
{
public:
	// UniRx.IObservable`1<T> UniRx.Operators.SubscribeOnMainThreadObservable`1::source
	Il2CppObject* ___source_1;
	// UniRx.IObservable`1<System.Int64> UniRx.Operators.SubscribeOnMainThreadObservable`1::subscribeTrigger
	Il2CppObject* ___subscribeTrigger_2;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(SubscribeOnMainThreadObservable_1_t1377860062, ___source_1)); }
	inline Il2CppObject* get_source_1() const { return ___source_1; }
	inline Il2CppObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(Il2CppObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier(&___source_1, value);
	}

	inline static int32_t get_offset_of_subscribeTrigger_2() { return static_cast<int32_t>(offsetof(SubscribeOnMainThreadObservable_1_t1377860062, ___subscribeTrigger_2)); }
	inline Il2CppObject* get_subscribeTrigger_2() const { return ___subscribeTrigger_2; }
	inline Il2CppObject** get_address_of_subscribeTrigger_2() { return &___subscribeTrigger_2; }
	inline void set_subscribeTrigger_2(Il2CppObject* value)
	{
		___subscribeTrigger_2 = value;
		Il2CppCodeGenWriteBarrier(&___subscribeTrigger_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
