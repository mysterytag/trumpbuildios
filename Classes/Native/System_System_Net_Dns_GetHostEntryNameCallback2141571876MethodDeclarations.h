﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.Dns/GetHostEntryNameCallback
struct GetHostEntryNameCallback_t2141571876;
// System.Object
struct Il2CppObject;
// System.Net.IPHostEntry
struct IPHostEntry_t4205702573;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Net.Dns/GetHostEntryNameCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetHostEntryNameCallback__ctor_m4030556306 (GetHostEntryNameCallback_t2141571876 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IPHostEntry System.Net.Dns/GetHostEntryNameCallback::Invoke(System.String)
extern "C"  IPHostEntry_t4205702573 * GetHostEntryNameCallback_Invoke_m1505249186 (GetHostEntryNameCallback_t2141571876 * __this, String_t* ___hostName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" IPHostEntry_t4205702573 * pinvoke_delegate_wrapper_GetHostEntryNameCallback_t2141571876(Il2CppObject* delegate, String_t* ___hostName0);
// System.IAsyncResult System.Net.Dns/GetHostEntryNameCallback::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetHostEntryNameCallback_BeginInvoke_m335844131 (GetHostEntryNameCallback_t2141571876 * __this, String_t* ___hostName0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IPHostEntry System.Net.Dns/GetHostEntryNameCallback::EndInvoke(System.IAsyncResult)
extern "C"  IPHostEntry_t4205702573 * GetHostEntryNameCallback_EndInvoke_m4263571606 (GetHostEntryNameCallback_t2141571876 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
