﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/<TimerFrameCore>c__Iterator15
struct U3CTimerFrameCoreU3Ec__Iterator15_t2264980090;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Observable/<TimerFrameCore>c__Iterator15::.ctor()
extern "C"  void U3CTimerFrameCoreU3Ec__Iterator15__ctor_m318033033 (U3CTimerFrameCoreU3Ec__Iterator15_t2264980090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UniRx.Observable/<TimerFrameCore>c__Iterator15::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTimerFrameCoreU3Ec__Iterator15_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1653767475 (U3CTimerFrameCoreU3Ec__Iterator15_t2264980090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UniRx.Observable/<TimerFrameCore>c__Iterator15::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTimerFrameCoreU3Ec__Iterator15_System_Collections_IEnumerator_get_Current_m2597537479 (U3CTimerFrameCoreU3Ec__Iterator15_t2264980090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.Observable/<TimerFrameCore>c__Iterator15::MoveNext()
extern "C"  bool U3CTimerFrameCoreU3Ec__Iterator15_MoveNext_m1362424475 (U3CTimerFrameCoreU3Ec__Iterator15_t2264980090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Observable/<TimerFrameCore>c__Iterator15::Dispose()
extern "C"  void U3CTimerFrameCoreU3Ec__Iterator15_Dispose_m585349766 (U3CTimerFrameCoreU3Ec__Iterator15_t2264980090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Observable/<TimerFrameCore>c__Iterator15::Reset()
extern "C"  void U3CTimerFrameCoreU3Ec__Iterator15_Reset_m2259433270 (U3CTimerFrameCoreU3Ec__Iterator15_t2264980090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
