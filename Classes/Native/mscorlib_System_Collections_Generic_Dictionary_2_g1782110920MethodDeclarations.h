﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>
struct Dictionary_2_t1782110920;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t3161373071;
// System.Collections.Generic.IDictionary`2<System.Object,PlayFab.UUnit.Region>
struct IDictionary_2_t2957776707;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2995724695;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1302937806;
// System.Collections.Generic.ICollection`1<PlayFab.UUnit.Region>
struct ICollection_1_t3555590872;
// System.Collections.ICollection
struct ICollection_t3761522009;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<System.Object,PlayFab.UUnit.Region>[]
struct KeyValuePair_2U5BU5D_t168012271;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,PlayFab.UUnit.Region>>
struct IEnumerator_1_t2753748666;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1541724277;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,PlayFab.UUnit.Region>
struct KeyCollection_t4105386200;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,PlayFab.UUnit.Region>
struct ValueCollection_t3704248014;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2995724695.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCont986364934.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21270642218.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharp_PlayFab_UUnit_Region3089759486.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1549138861.h"
#include "mscorlib_System_Collections_DictionaryEntry130027246.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::.ctor()
extern "C"  void Dictionary_2__ctor_m3794050509_gshared (Dictionary_2_t1782110920 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m3794050509(__this, method) ((  void (*) (Dictionary_2_t1782110920 *, const MethodInfo*))Dictionary_2__ctor_m3794050509_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m3291699140_gshared (Dictionary_2_t1782110920 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m3291699140(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t1782110920 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3291699140_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m3274151403_gshared (Dictionary_2_t1782110920 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m3274151403(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t1782110920 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3274151403_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m1016792990_gshared (Dictionary_2_t1782110920 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m1016792990(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t1782110920 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m1016792990_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m555009778_gshared (Dictionary_2_t1782110920 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m555009778(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t1782110920 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m555009778_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m2376040223_gshared (Dictionary_2_t1782110920 * __this, int32_t ___capacity0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m2376040223(__this, ___capacity0, ___comparer1, method) ((  void (*) (Dictionary_2_t1782110920 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2376040223_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m3424645518_gshared (Dictionary_2_t1782110920 * __this, SerializationInfo_t2995724695 * ___info0, StreamingContext_t986364934  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m3424645518(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t1782110920 *, SerializationInfo_t2995724695 *, StreamingContext_t986364934 , const MethodInfo*))Dictionary_2__ctor_m3424645518_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3455450859_gshared (Dictionary_2_t1782110920 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3455450859(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1782110920 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3455450859_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m4211414663_gshared (Dictionary_2_t1782110920 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m4211414663(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1782110920 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m4211414663_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m4011665099_gshared (Dictionary_2_t1782110920 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m4011665099(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1782110920 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m4011665099_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m697083129_gshared (Dictionary_2_t1782110920 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m697083129(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1782110920 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m697083129_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m3446269936_gshared (Dictionary_2_t1782110920 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m3446269936(__this, method) ((  bool (*) (Dictionary_2_t1782110920 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m3446269936_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m2504841481_gshared (Dictionary_2_t1782110920 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m2504841481(__this, method) ((  bool (*) (Dictionary_2_t1782110920 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m2504841481_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m4013254033_gshared (Dictionary_2_t1782110920 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m4013254033(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t1782110920 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m4013254033_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m1100988032_gshared (Dictionary_2_t1782110920 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m1100988032(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1782110920 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m1100988032_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m3253301201_gshared (Dictionary_2_t1782110920 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m3253301201(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1782110920 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m3253301201_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m3269202881_gshared (Dictionary_2_t1782110920 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m3269202881(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1782110920 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m3269202881_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m2929670334_gshared (Dictionary_2_t1782110920 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m2929670334(__this, ___key0, method) ((  void (*) (Dictionary_2_t1782110920 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m2929670334_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m441391923_gshared (Dictionary_2_t1782110920 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m441391923(__this, method) ((  bool (*) (Dictionary_2_t1782110920 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m441391923_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2415946469_gshared (Dictionary_2_t1782110920 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2415946469(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1782110920 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2415946469_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m289611191_gshared (Dictionary_2_t1782110920 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m289611191(__this, method) ((  bool (*) (Dictionary_2_t1782110920 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m289611191_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1443950164_gshared (Dictionary_2_t1782110920 * __this, KeyValuePair_2_t1270642218  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1443950164(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t1782110920 *, KeyValuePair_2_t1270642218 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1443950164_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1689072690_gshared (Dictionary_2_t1782110920 * __this, KeyValuePair_2_t1270642218  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1689072690(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t1782110920 *, KeyValuePair_2_t1270642218 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1689072690_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2717282616_gshared (Dictionary_2_t1782110920 * __this, KeyValuePair_2U5BU5D_t168012271* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2717282616(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1782110920 *, KeyValuePair_2U5BU5D_t168012271*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2717282616_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m838778839_gshared (Dictionary_2_t1782110920 * __this, KeyValuePair_2_t1270642218  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m838778839(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t1782110920 *, KeyValuePair_2_t1270642218 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m838778839_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m2863834135_gshared (Dictionary_2_t1782110920 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m2863834135(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1782110920 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m2863834135_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1623540198_gshared (Dictionary_2_t1782110920 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1623540198(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1782110920 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1623540198_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3262507549_gshared (Dictionary_2_t1782110920 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3262507549(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1782110920 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3262507549_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m185403562_gshared (Dictionary_2_t1782110920 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m185403562(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1782110920 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m185403562_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m2099961709_gshared (Dictionary_2_t1782110920 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m2099961709(__this, method) ((  int32_t (*) (Dictionary_2_t1782110920 *, const MethodInfo*))Dictionary_2_get_Count_m2099961709_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::get_Item(TKey)
extern "C"  int32_t Dictionary_2_get_Item_m204799226_gshared (Dictionary_2_t1782110920 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m204799226(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t1782110920 *, Il2CppObject *, const MethodInfo*))Dictionary_2_get_Item_m204799226_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m3086362829_gshared (Dictionary_2_t1782110920 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m3086362829(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1782110920 *, Il2CppObject *, int32_t, const MethodInfo*))Dictionary_2_set_Item_m3086362829_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m3578039621_gshared (Dictionary_2_t1782110920 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m3578039621(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t1782110920 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m3578039621_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m2438279442_gshared (Dictionary_2_t1782110920 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m2438279442(__this, ___size0, method) ((  void (*) (Dictionary_2_t1782110920 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m2438279442_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m886818190_gshared (Dictionary_2_t1782110920 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m886818190(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1782110920 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m886818190_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t1270642218  Dictionary_2_make_pair_m2596498978_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m2596498978(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t1270642218  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, int32_t, const MethodInfo*))Dictionary_2_make_pair_m2596498978_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::pick_key(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_key_m4119656540_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m4119656540(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, int32_t, const MethodInfo*))Dictionary_2_pick_key_m4119656540_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::pick_value(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_value_m1630856376_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m1630856376(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject *, int32_t, const MethodInfo*))Dictionary_2_pick_value_m1630856376_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m2979400321_gshared (Dictionary_2_t1782110920 * __this, KeyValuePair_2U5BU5D_t168012271* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m2979400321(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1782110920 *, KeyValuePair_2U5BU5D_t168012271*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m2979400321_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::Resize()
extern "C"  void Dictionary_2_Resize_m1709245451_gshared (Dictionary_2_t1782110920 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m1709245451(__this, method) ((  void (*) (Dictionary_2_t1782110920 *, const MethodInfo*))Dictionary_2_Resize_m1709245451_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m1536347528_gshared (Dictionary_2_t1782110920 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m1536347528(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1782110920 *, Il2CppObject *, int32_t, const MethodInfo*))Dictionary_2_Add_m1536347528_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::Clear()
extern "C"  void Dictionary_2_Clear_m1200183800_gshared (Dictionary_2_t1782110920 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m1200183800(__this, method) ((  void (*) (Dictionary_2_t1782110920 *, const MethodInfo*))Dictionary_2_Clear_m1200183800_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m2552900002_gshared (Dictionary_2_t1782110920 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m2552900002(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1782110920 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsKey_m2552900002_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m2989743266_gshared (Dictionary_2_t1782110920 * __this, int32_t ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m2989743266(__this, ___value0, method) ((  bool (*) (Dictionary_2_t1782110920 *, int32_t, const MethodInfo*))Dictionary_2_ContainsValue_m2989743266_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m1742064363_gshared (Dictionary_2_t1782110920 * __this, SerializationInfo_t2995724695 * ___info0, StreamingContext_t986364934  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m1742064363(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t1782110920 *, SerializationInfo_t2995724695 *, StreamingContext_t986364934 , const MethodInfo*))Dictionary_2_GetObjectData_m1742064363_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m1835103321_gshared (Dictionary_2_t1782110920 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m1835103321(__this, ___sender0, method) ((  void (*) (Dictionary_2_t1782110920 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m1835103321_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m1355223566_gshared (Dictionary_2_t1782110920 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m1355223566(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1782110920 *, Il2CppObject *, const MethodInfo*))Dictionary_2_Remove_m1355223566_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m2798370811_gshared (Dictionary_2_t1782110920 * __this, Il2CppObject * ___key0, int32_t* ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m2798370811(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t1782110920 *, Il2CppObject *, int32_t*, const MethodInfo*))Dictionary_2_TryGetValue_m2798370811_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::get_Keys()
extern "C"  KeyCollection_t4105386200 * Dictionary_2_get_Keys_m4173252352_gshared (Dictionary_2_t1782110920 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m4173252352(__this, method) ((  KeyCollection_t4105386200 * (*) (Dictionary_2_t1782110920 *, const MethodInfo*))Dictionary_2_get_Keys_m4173252352_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::get_Values()
extern "C"  ValueCollection_t3704248014 * Dictionary_2_get_Values_m3031891996_gshared (Dictionary_2_t1782110920 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m3031891996(__this, method) ((  ValueCollection_t3704248014 * (*) (Dictionary_2_t1782110920 *, const MethodInfo*))Dictionary_2_get_Values_m3031891996_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::ToTKey(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTKey_m3569515447_gshared (Dictionary_2_t1782110920 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m3569515447(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t1782110920 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m3569515447_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::ToTValue(System.Object)
extern "C"  int32_t Dictionary_2_ToTValue_m4238137811_gshared (Dictionary_2_t1782110920 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m4238137811(__this, ___value0, method) ((  int32_t (*) (Dictionary_2_t1782110920 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m4238137811_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m2446381041_gshared (Dictionary_2_t1782110920 * __this, KeyValuePair_2_t1270642218  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m2446381041(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t1782110920 *, KeyValuePair_2_t1270642218 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m2446381041_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::GetEnumerator()
extern "C"  Enumerator_t1549138862  Dictionary_2_GetEnumerator_m926605016_gshared (Dictionary_2_t1782110920 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m926605016(__this, method) ((  Enumerator_t1549138862  (*) (Dictionary_2_t1782110920 *, const MethodInfo*))Dictionary_2_GetEnumerator_m926605016_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,PlayFab.UUnit.Region>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t130027246  Dictionary_2_U3CCopyToU3Em__0_m3041390159_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m3041390159(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t130027246  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, int32_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m3041390159_gshared)(__this /* static, unused */, ___key0, ___value1, method)
