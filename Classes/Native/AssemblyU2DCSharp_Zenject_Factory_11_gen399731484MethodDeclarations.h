﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.Factory`11<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct Factory_11_t399731484;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t3431720054;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void Zenject.Factory`11<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C"  void Factory_11__ctor_m3374782008_gshared (Factory_11_t399731484 * __this, const MethodInfo* method);
#define Factory_11__ctor_m3374782008(__this, method) ((  void (*) (Factory_11_t399731484 *, const MethodInfo*))Factory_11__ctor_m3374782008_gshared)(__this, method)
// System.Type Zenject.Factory`11<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_ConstructedType()
extern "C"  Type_t * Factory_11_get_ConstructedType_m2790504215_gshared (Factory_11_t399731484 * __this, const MethodInfo* method);
#define Factory_11_get_ConstructedType_m2790504215(__this, method) ((  Type_t * (*) (Factory_11_t399731484 *, const MethodInfo*))Factory_11_get_ConstructedType_m2790504215_gshared)(__this, method)
// System.Type[] Zenject.Factory`11<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_ProvidedTypes()
extern "C"  TypeU5BU5D_t3431720054* Factory_11_get_ProvidedTypes_m847810875_gshared (Factory_11_t399731484 * __this, const MethodInfo* method);
#define Factory_11_get_ProvidedTypes_m847810875(__this, method) ((  TypeU5BU5D_t3431720054* (*) (Factory_11_t399731484 *, const MethodInfo*))Factory_11_get_ProvidedTypes_m847810875_gshared)(__this, method)
// Zenject.DiContainer Zenject.Factory`11<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_Container()
extern "C"  DiContainer_t2383114449 * Factory_11_get_Container_m125868804_gshared (Factory_11_t399731484 * __this, const MethodInfo* method);
#define Factory_11_get_Container_m125868804(__this, method) ((  DiContainer_t2383114449 * (*) (Factory_11_t399731484 *, const MethodInfo*))Factory_11_get_Container_m125868804_gshared)(__this, method)
// TValue Zenject.Factory`11<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Create(TParam1,TParam2,TParam3,TParam4,TParam5,TParam6,TParam7,TParam8,TParam9,TParam10)
extern "C"  Il2CppObject * Factory_11_Create_m2516102464_gshared (Factory_11_t399731484 * __this, Il2CppObject * ___param10, Il2CppObject * ___param21, Il2CppObject * ___param32, Il2CppObject * ___param43, Il2CppObject * ___param54, Il2CppObject * ___param65, Il2CppObject * ___param76, Il2CppObject * ___param87, Il2CppObject * ___param98, Il2CppObject * ___param109, const MethodInfo* method);
#define Factory_11_Create_m2516102464(__this, ___param10, ___param21, ___param32, ___param43, ___param54, ___param65, ___param76, ___param87, ___param98, ___param109, method) ((  Il2CppObject * (*) (Factory_11_t399731484 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Factory_11_Create_m2516102464_gshared)(__this, ___param10, ___param21, ___param32, ___param43, ___param54, ___param65, ___param76, ___param87, ___param98, ___param109, method)
