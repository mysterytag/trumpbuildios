﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XmlIteratorNodeList
struct XmlIteratorNodeList_t1260461101;
// System.Xml.XPath.XPathNodeIterator
struct XPathNodeIterator_t2394191562;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Xml.XmlNode
struct XmlNode_t3592213601;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XPath_XPathNodeIterator2394191562.h"

// System.Void System.Xml.XmlIteratorNodeList::.ctor(System.Xml.XPath.XPathNodeIterator)
extern "C"  void XmlIteratorNodeList__ctor_m3864291280 (XmlIteratorNodeList_t1260461101 * __this, XPathNodeIterator_t2394191562 * ___iter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Xml.XmlIteratorNodeList::get_Count()
extern "C"  int32_t XmlIteratorNodeList_get_Count_m1696300248 (XmlIteratorNodeList_t1260461101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Xml.XmlIteratorNodeList::GetEnumerator()
extern "C"  Il2CppObject * XmlIteratorNodeList_GetEnumerator_m596871986 (XmlIteratorNodeList_t1260461101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNode System.Xml.XmlIteratorNodeList::Item(System.Int32)
extern "C"  XmlNode_t3592213601 * XmlIteratorNodeList_Item_m1908195040 (XmlIteratorNodeList_t1260461101 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
