﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Scheduler/<Schedule>c__AnonStorey73/<Schedule>c__AnonStorey74
struct U3CScheduleU3Ec__AnonStorey74_t3913535385;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Scheduler/<Schedule>c__AnonStorey73/<Schedule>c__AnonStorey74::.ctor()
extern "C"  void U3CScheduleU3Ec__AnonStorey74__ctor_m3274504513 (U3CScheduleU3Ec__AnonStorey74_t3913535385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Scheduler/<Schedule>c__AnonStorey73/<Schedule>c__AnonStorey74::<>m__9B()
extern "C"  void U3CScheduleU3Ec__AnonStorey74_U3CU3Em__9B_m896104627 (U3CScheduleU3Ec__AnonStorey74_t3913535385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
