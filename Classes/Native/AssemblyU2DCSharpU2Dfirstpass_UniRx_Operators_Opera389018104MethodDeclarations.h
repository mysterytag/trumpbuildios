﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149MethodDeclarations.h"

// System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,UniRx.Notification`1<System.Object>>::.ctor(UniRx.IObserver`1<TResult>,System.IDisposable)
#define OperatorObserverBase_2__ctor_m859760098(__this, ___observer0, ___cancel1, method) ((  void (*) (OperatorObserverBase_2_t389018104 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))OperatorObserverBase_2__ctor_m1863043020_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,UniRx.Notification`1<System.Object>>::Dispose()
#define OperatorObserverBase_2_Dispose_m3722290504(__this, method) ((  void (*) (OperatorObserverBase_2_t389018104 *, const MethodInfo*))OperatorObserverBase_2_Dispose_m3164711326_gshared)(__this, method)
