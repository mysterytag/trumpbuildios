﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.SendAccountRecoveryEmailResult
struct SendAccountRecoveryEmailResult_t3793926767;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.SendAccountRecoveryEmailResult::.ctor()
extern "C"  void SendAccountRecoveryEmailResult__ctor_m226971950 (SendAccountRecoveryEmailResult_t3793926767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
