﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/<RepeatInfinite>c__IteratorE`1<System.Object>
struct U3CRepeatInfiniteU3Ec__IteratorE_1_t1925948945;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<UniRx.IObservable`1<System.Object>>
struct IEnumerator_1_t2079011232;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Observable/<RepeatInfinite>c__IteratorE`1<System.Object>::.ctor()
extern "C"  void U3CRepeatInfiniteU3Ec__IteratorE_1__ctor_m4193162815_gshared (U3CRepeatInfiniteU3Ec__IteratorE_1_t1925948945 * __this, const MethodInfo* method);
#define U3CRepeatInfiniteU3Ec__IteratorE_1__ctor_m4193162815(__this, method) ((  void (*) (U3CRepeatInfiniteU3Ec__IteratorE_1_t1925948945 *, const MethodInfo*))U3CRepeatInfiniteU3Ec__IteratorE_1__ctor_m4193162815_gshared)(__this, method)
// UniRx.IObservable`1<T> UniRx.Observable/<RepeatInfinite>c__IteratorE`1<System.Object>::System.Collections.Generic.IEnumerator<UniRx.IObservable<T>>.get_Current()
extern "C"  Il2CppObject* U3CRepeatInfiniteU3Ec__IteratorE_1_System_Collections_Generic_IEnumeratorU3CUniRx_IObservableU3CTU3EU3E_get_Current_m2072144633_gshared (U3CRepeatInfiniteU3Ec__IteratorE_1_t1925948945 * __this, const MethodInfo* method);
#define U3CRepeatInfiniteU3Ec__IteratorE_1_System_Collections_Generic_IEnumeratorU3CUniRx_IObservableU3CTU3EU3E_get_Current_m2072144633(__this, method) ((  Il2CppObject* (*) (U3CRepeatInfiniteU3Ec__IteratorE_1_t1925948945 *, const MethodInfo*))U3CRepeatInfiniteU3Ec__IteratorE_1_System_Collections_Generic_IEnumeratorU3CUniRx_IObservableU3CTU3EU3E_get_Current_m2072144633_gshared)(__this, method)
// System.Object UniRx.Observable/<RepeatInfinite>c__IteratorE`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CRepeatInfiniteU3Ec__IteratorE_1_System_Collections_IEnumerator_get_Current_m836332753_gshared (U3CRepeatInfiniteU3Ec__IteratorE_1_t1925948945 * __this, const MethodInfo* method);
#define U3CRepeatInfiniteU3Ec__IteratorE_1_System_Collections_IEnumerator_get_Current_m836332753(__this, method) ((  Il2CppObject * (*) (U3CRepeatInfiniteU3Ec__IteratorE_1_t1925948945 *, const MethodInfo*))U3CRepeatInfiniteU3Ec__IteratorE_1_System_Collections_IEnumerator_get_Current_m836332753_gshared)(__this, method)
// System.Collections.IEnumerator UniRx.Observable/<RepeatInfinite>c__IteratorE`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CRepeatInfiniteU3Ec__IteratorE_1_System_Collections_IEnumerable_GetEnumerator_m4037698746_gshared (U3CRepeatInfiniteU3Ec__IteratorE_1_t1925948945 * __this, const MethodInfo* method);
#define U3CRepeatInfiniteU3Ec__IteratorE_1_System_Collections_IEnumerable_GetEnumerator_m4037698746(__this, method) ((  Il2CppObject * (*) (U3CRepeatInfiniteU3Ec__IteratorE_1_t1925948945 *, const MethodInfo*))U3CRepeatInfiniteU3Ec__IteratorE_1_System_Collections_IEnumerable_GetEnumerator_m4037698746_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<UniRx.IObservable`1<T>> UniRx.Observable/<RepeatInfinite>c__IteratorE`1<System.Object>::System.Collections.Generic.IEnumerable<UniRx.IObservable<T>>.GetEnumerator()
extern "C"  Il2CppObject* U3CRepeatInfiniteU3Ec__IteratorE_1_System_Collections_Generic_IEnumerableU3CUniRx_IObservableU3CTU3EU3E_GetEnumerator_m3722258232_gshared (U3CRepeatInfiniteU3Ec__IteratorE_1_t1925948945 * __this, const MethodInfo* method);
#define U3CRepeatInfiniteU3Ec__IteratorE_1_System_Collections_Generic_IEnumerableU3CUniRx_IObservableU3CTU3EU3E_GetEnumerator_m3722258232(__this, method) ((  Il2CppObject* (*) (U3CRepeatInfiniteU3Ec__IteratorE_1_t1925948945 *, const MethodInfo*))U3CRepeatInfiniteU3Ec__IteratorE_1_System_Collections_Generic_IEnumerableU3CUniRx_IObservableU3CTU3EU3E_GetEnumerator_m3722258232_gshared)(__this, method)
// System.Boolean UniRx.Observable/<RepeatInfinite>c__IteratorE`1<System.Object>::MoveNext()
extern "C"  bool U3CRepeatInfiniteU3Ec__IteratorE_1_MoveNext_m3993799973_gshared (U3CRepeatInfiniteU3Ec__IteratorE_1_t1925948945 * __this, const MethodInfo* method);
#define U3CRepeatInfiniteU3Ec__IteratorE_1_MoveNext_m3993799973(__this, method) ((  bool (*) (U3CRepeatInfiniteU3Ec__IteratorE_1_t1925948945 *, const MethodInfo*))U3CRepeatInfiniteU3Ec__IteratorE_1_MoveNext_m3993799973_gshared)(__this, method)
// System.Void UniRx.Observable/<RepeatInfinite>c__IteratorE`1<System.Object>::Dispose()
extern "C"  void U3CRepeatInfiniteU3Ec__IteratorE_1_Dispose_m848424636_gshared (U3CRepeatInfiniteU3Ec__IteratorE_1_t1925948945 * __this, const MethodInfo* method);
#define U3CRepeatInfiniteU3Ec__IteratorE_1_Dispose_m848424636(__this, method) ((  void (*) (U3CRepeatInfiniteU3Ec__IteratorE_1_t1925948945 *, const MethodInfo*))U3CRepeatInfiniteU3Ec__IteratorE_1_Dispose_m848424636_gshared)(__this, method)
// System.Void UniRx.Observable/<RepeatInfinite>c__IteratorE`1<System.Object>::Reset()
extern "C"  void U3CRepeatInfiniteU3Ec__IteratorE_1_Reset_m1839595756_gshared (U3CRepeatInfiniteU3Ec__IteratorE_1_t1925948945 * __this, const MethodInfo* method);
#define U3CRepeatInfiniteU3Ec__IteratorE_1_Reset_m1839595756(__this, method) ((  void (*) (U3CRepeatInfiniteU3Ec__IteratorE_1_t1925948945 *, const MethodInfo*))U3CRepeatInfiniteU3Ec__IteratorE_1_Reset_m1839595756_gshared)(__this, method)
