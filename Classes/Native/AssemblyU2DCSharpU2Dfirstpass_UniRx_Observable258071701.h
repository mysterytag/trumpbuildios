﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.HashSet`1<System.Type>
struct HashSet_1_t1182951310;
// System.Func`3<UniRx.IObserver`1<System.Int64>,UniRx.CancellationToken,System.Collections.IEnumerator>
struct Func_3_t521084177;
// System.Func`3<System.Int64,UniRx.Unit,System.Int64>
struct Func_3_t3706795343;

#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_TimeSpan763862892.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable
struct  Observable_t258071701  : public Il2CppObject
{
public:

public:
};

struct Observable_t258071701_StaticFields
{
public:
	// System.TimeSpan UniRx.Observable::InfiniteTimeSpan
	TimeSpan_t763862892  ___InfiniteTimeSpan_0;
	// System.Collections.Generic.HashSet`1<System.Type> UniRx.Observable::YieldInstructionTypes
	HashSet_1_t1182951310 * ___YieldInstructionTypes_1;
	// System.Func`3<UniRx.IObserver`1<System.Int64>,UniRx.CancellationToken,System.Collections.IEnumerator> UniRx.Observable::<>f__am$cache2
	Func_3_t521084177 * ___U3CU3Ef__amU24cache2_2;
	// System.Func`3<UniRx.IObserver`1<System.Int64>,UniRx.CancellationToken,System.Collections.IEnumerator> UniRx.Observable::<>f__am$cache3
	Func_3_t521084177 * ___U3CU3Ef__amU24cache3_3;
	// System.Func`3<UniRx.IObserver`1<System.Int64>,UniRx.CancellationToken,System.Collections.IEnumerator> UniRx.Observable::<>f__am$cache4
	Func_3_t521084177 * ___U3CU3Ef__amU24cache4_4;
	// System.Func`3<System.Int64,UniRx.Unit,System.Int64> UniRx.Observable::<>f__am$cache5
	Func_3_t3706795343 * ___U3CU3Ef__amU24cache5_5;
	// System.Func`3<System.Int64,UniRx.Unit,System.Int64> UniRx.Observable::<>f__am$cache6
	Func_3_t3706795343 * ___U3CU3Ef__amU24cache6_6;
	// System.Func`3<UniRx.IObserver`1<System.Int64>,UniRx.CancellationToken,System.Collections.IEnumerator> UniRx.Observable::<>f__am$cache7
	Func_3_t521084177 * ___U3CU3Ef__amU24cache7_7;

public:
	inline static int32_t get_offset_of_InfiniteTimeSpan_0() { return static_cast<int32_t>(offsetof(Observable_t258071701_StaticFields, ___InfiniteTimeSpan_0)); }
	inline TimeSpan_t763862892  get_InfiniteTimeSpan_0() const { return ___InfiniteTimeSpan_0; }
	inline TimeSpan_t763862892 * get_address_of_InfiniteTimeSpan_0() { return &___InfiniteTimeSpan_0; }
	inline void set_InfiniteTimeSpan_0(TimeSpan_t763862892  value)
	{
		___InfiniteTimeSpan_0 = value;
	}

	inline static int32_t get_offset_of_YieldInstructionTypes_1() { return static_cast<int32_t>(offsetof(Observable_t258071701_StaticFields, ___YieldInstructionTypes_1)); }
	inline HashSet_1_t1182951310 * get_YieldInstructionTypes_1() const { return ___YieldInstructionTypes_1; }
	inline HashSet_1_t1182951310 ** get_address_of_YieldInstructionTypes_1() { return &___YieldInstructionTypes_1; }
	inline void set_YieldInstructionTypes_1(HashSet_1_t1182951310 * value)
	{
		___YieldInstructionTypes_1 = value;
		Il2CppCodeGenWriteBarrier(&___YieldInstructionTypes_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_2() { return static_cast<int32_t>(offsetof(Observable_t258071701_StaticFields, ___U3CU3Ef__amU24cache2_2)); }
	inline Func_3_t521084177 * get_U3CU3Ef__amU24cache2_2() const { return ___U3CU3Ef__amU24cache2_2; }
	inline Func_3_t521084177 ** get_address_of_U3CU3Ef__amU24cache2_2() { return &___U3CU3Ef__amU24cache2_2; }
	inline void set_U3CU3Ef__amU24cache2_2(Func_3_t521084177 * value)
	{
		___U3CU3Ef__amU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_3() { return static_cast<int32_t>(offsetof(Observable_t258071701_StaticFields, ___U3CU3Ef__amU24cache3_3)); }
	inline Func_3_t521084177 * get_U3CU3Ef__amU24cache3_3() const { return ___U3CU3Ef__amU24cache3_3; }
	inline Func_3_t521084177 ** get_address_of_U3CU3Ef__amU24cache3_3() { return &___U3CU3Ef__amU24cache3_3; }
	inline void set_U3CU3Ef__amU24cache3_3(Func_3_t521084177 * value)
	{
		___U3CU3Ef__amU24cache3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_4() { return static_cast<int32_t>(offsetof(Observable_t258071701_StaticFields, ___U3CU3Ef__amU24cache4_4)); }
	inline Func_3_t521084177 * get_U3CU3Ef__amU24cache4_4() const { return ___U3CU3Ef__amU24cache4_4; }
	inline Func_3_t521084177 ** get_address_of_U3CU3Ef__amU24cache4_4() { return &___U3CU3Ef__amU24cache4_4; }
	inline void set_U3CU3Ef__amU24cache4_4(Func_3_t521084177 * value)
	{
		___U3CU3Ef__amU24cache4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_5() { return static_cast<int32_t>(offsetof(Observable_t258071701_StaticFields, ___U3CU3Ef__amU24cache5_5)); }
	inline Func_3_t3706795343 * get_U3CU3Ef__amU24cache5_5() const { return ___U3CU3Ef__amU24cache5_5; }
	inline Func_3_t3706795343 ** get_address_of_U3CU3Ef__amU24cache5_5() { return &___U3CU3Ef__amU24cache5_5; }
	inline void set_U3CU3Ef__amU24cache5_5(Func_3_t3706795343 * value)
	{
		___U3CU3Ef__amU24cache5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_6() { return static_cast<int32_t>(offsetof(Observable_t258071701_StaticFields, ___U3CU3Ef__amU24cache6_6)); }
	inline Func_3_t3706795343 * get_U3CU3Ef__amU24cache6_6() const { return ___U3CU3Ef__amU24cache6_6; }
	inline Func_3_t3706795343 ** get_address_of_U3CU3Ef__amU24cache6_6() { return &___U3CU3Ef__amU24cache6_6; }
	inline void set_U3CU3Ef__amU24cache6_6(Func_3_t3706795343 * value)
	{
		___U3CU3Ef__amU24cache6_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_7() { return static_cast<int32_t>(offsetof(Observable_t258071701_StaticFields, ___U3CU3Ef__amU24cache7_7)); }
	inline Func_3_t521084177 * get_U3CU3Ef__amU24cache7_7() const { return ___U3CU3Ef__amU24cache7_7; }
	inline Func_3_t521084177 ** get_address_of_U3CU3Ef__amU24cache7_7() { return &___U3CU3Ef__amU24cache7_7; }
	inline void set_U3CU3Ef__amU24cache7_7(Func_3_t521084177 * value)
	{
		___U3CU3Ef__amU24cache7_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
