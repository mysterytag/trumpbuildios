﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.RangeReactivePropertyAttribute
struct RangeReactivePropertyAttribute_t252243683;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.RangeReactivePropertyAttribute::.ctor(System.Single,System.Single)
extern "C"  void RangeReactivePropertyAttribute__ctor_m1724076426 (RangeReactivePropertyAttribute_t252243683 * __this, float ___min0, float ___max1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UniRx.RangeReactivePropertyAttribute::get_Min()
extern "C"  float RangeReactivePropertyAttribute_get_Min_m1545646273 (RangeReactivePropertyAttribute_t252243683 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.RangeReactivePropertyAttribute::set_Min(System.Single)
extern "C"  void RangeReactivePropertyAttribute_set_Min_m2699366962 (RangeReactivePropertyAttribute_t252243683 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UniRx.RangeReactivePropertyAttribute::get_Max()
extern "C"  float RangeReactivePropertyAttribute_get_Max_m1545417555 (RangeReactivePropertyAttribute_t252243683 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.RangeReactivePropertyAttribute::set_Max(System.Single)
extern "C"  void RangeReactivePropertyAttribute_set_Max_m3947416800 (RangeReactivePropertyAttribute_t252243683 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
