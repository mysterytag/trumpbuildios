﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ReadOnlyReactiveProperty`1<UnityEngine.Color>
struct ReadOnlyReactiveProperty_1_t82610480;
// UniRx.IObservable`1<UnityEngine.Color>
struct IObservable_1_t1346974124;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UnityEngine.Color>
struct IObserver_1_t3800174663;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"

// System.Void UniRx.ReadOnlyReactiveProperty`1<UnityEngine.Color>::.ctor(UniRx.IObservable`1<T>)
extern "C"  void ReadOnlyReactiveProperty_1__ctor_m556323457_gshared (ReadOnlyReactiveProperty_1_t82610480 * __this, Il2CppObject* ___source0, const MethodInfo* method);
#define ReadOnlyReactiveProperty_1__ctor_m556323457(__this, ___source0, method) ((  void (*) (ReadOnlyReactiveProperty_1_t82610480 *, Il2CppObject*, const MethodInfo*))ReadOnlyReactiveProperty_1__ctor_m556323457_gshared)(__this, ___source0, method)
// System.Void UniRx.ReadOnlyReactiveProperty`1<UnityEngine.Color>::.ctor(UniRx.IObservable`1<T>,T)
extern "C"  void ReadOnlyReactiveProperty_1__ctor_m2050903001_gshared (ReadOnlyReactiveProperty_1_t82610480 * __this, Il2CppObject* ___source0, Color_t1588175760  ___initialValue1, const MethodInfo* method);
#define ReadOnlyReactiveProperty_1__ctor_m2050903001(__this, ___source0, ___initialValue1, method) ((  void (*) (ReadOnlyReactiveProperty_1_t82610480 *, Il2CppObject*, Color_t1588175760 , const MethodInfo*))ReadOnlyReactiveProperty_1__ctor_m2050903001_gshared)(__this, ___source0, ___initialValue1, method)
// T UniRx.ReadOnlyReactiveProperty`1<UnityEngine.Color>::get_Value()
extern "C"  Color_t1588175760  ReadOnlyReactiveProperty_1_get_Value_m2105092999_gshared (ReadOnlyReactiveProperty_1_t82610480 * __this, const MethodInfo* method);
#define ReadOnlyReactiveProperty_1_get_Value_m2105092999(__this, method) ((  Color_t1588175760  (*) (ReadOnlyReactiveProperty_1_t82610480 *, const MethodInfo*))ReadOnlyReactiveProperty_1_get_Value_m2105092999_gshared)(__this, method)
// System.IDisposable UniRx.ReadOnlyReactiveProperty`1<UnityEngine.Color>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * ReadOnlyReactiveProperty_1_Subscribe_m3746595959_gshared (ReadOnlyReactiveProperty_1_t82610480 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ReadOnlyReactiveProperty_1_Subscribe_m3746595959(__this, ___observer0, method) ((  Il2CppObject * (*) (ReadOnlyReactiveProperty_1_t82610480 *, Il2CppObject*, const MethodInfo*))ReadOnlyReactiveProperty_1_Subscribe_m3746595959_gshared)(__this, ___observer0, method)
// System.Void UniRx.ReadOnlyReactiveProperty`1<UnityEngine.Color>::Dispose()
extern "C"  void ReadOnlyReactiveProperty_1_Dispose_m2383740383_gshared (ReadOnlyReactiveProperty_1_t82610480 * __this, const MethodInfo* method);
#define ReadOnlyReactiveProperty_1_Dispose_m2383740383(__this, method) ((  void (*) (ReadOnlyReactiveProperty_1_t82610480 *, const MethodInfo*))ReadOnlyReactiveProperty_1_Dispose_m2383740383_gshared)(__this, method)
// System.Void UniRx.ReadOnlyReactiveProperty`1<UnityEngine.Color>::Dispose(System.Boolean)
extern "C"  void ReadOnlyReactiveProperty_1_Dispose_m2073985302_gshared (ReadOnlyReactiveProperty_1_t82610480 * __this, bool ___disposing0, const MethodInfo* method);
#define ReadOnlyReactiveProperty_1_Dispose_m2073985302(__this, ___disposing0, method) ((  void (*) (ReadOnlyReactiveProperty_1_t82610480 *, bool, const MethodInfo*))ReadOnlyReactiveProperty_1_Dispose_m2073985302_gshared)(__this, ___disposing0, method)
// System.String UniRx.ReadOnlyReactiveProperty`1<UnityEngine.Color>::ToString()
extern "C"  String_t* ReadOnlyReactiveProperty_1_ToString_m2327039563_gshared (ReadOnlyReactiveProperty_1_t82610480 * __this, const MethodInfo* method);
#define ReadOnlyReactiveProperty_1_ToString_m2327039563(__this, method) ((  String_t* (*) (ReadOnlyReactiveProperty_1_t82610480 *, const MethodInfo*))ReadOnlyReactiveProperty_1_ToString_m2327039563_gshared)(__this, method)
// System.Boolean UniRx.ReadOnlyReactiveProperty`1<UnityEngine.Color>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool ReadOnlyReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m2589958889_gshared (ReadOnlyReactiveProperty_1_t82610480 * __this, const MethodInfo* method);
#define ReadOnlyReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m2589958889(__this, method) ((  bool (*) (ReadOnlyReactiveProperty_1_t82610480 *, const MethodInfo*))ReadOnlyReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m2589958889_gshared)(__this, method)
