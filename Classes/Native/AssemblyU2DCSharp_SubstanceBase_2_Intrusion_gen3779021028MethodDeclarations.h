﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SubstanceBase`2/Intrusion<System.Double,System.Double>
struct Intrusion_t3779021028;

#include "codegen/il2cpp-codegen.h"

// System.Void SubstanceBase`2/Intrusion<System.Double,System.Double>::.ctor()
extern "C"  void Intrusion__ctor_m250965792_gshared (Intrusion_t3779021028 * __this, const MethodInfo* method);
#define Intrusion__ctor_m250965792(__this, method) ((  void (*) (Intrusion_t3779021028 *, const MethodInfo*))Intrusion__ctor_m250965792_gshared)(__this, method)
