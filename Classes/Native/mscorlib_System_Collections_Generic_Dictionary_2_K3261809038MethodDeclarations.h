﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>
struct KeyCollection_t3261809038;
// System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>
struct Dictionary_2_t938533758;
// System.Collections.Generic.IEnumerator`1<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType>
struct IEnumerator_1_t457318340;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// GameAnalyticsSDK.GA_ServerFieldTypes/FieldType[]
struct FieldTypeU5BU5D_t1420599677;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GameAnalyticsSDK_GA_ServerFieldT3269179188.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke705561699.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m1969919838_gshared (KeyCollection_t3261809038 * __this, Dictionary_2_t938533758 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m1969919838(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t3261809038 *, Dictionary_2_t938533758 *, const MethodInfo*))KeyCollection__ctor_m1969919838_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3509919544_gshared (KeyCollection_t3261809038 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3509919544(__this, ___item0, method) ((  void (*) (KeyCollection_t3261809038 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3509919544_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m875147951_gshared (KeyCollection_t3261809038 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m875147951(__this, method) ((  void (*) (KeyCollection_t3261809038 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m875147951_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1927735574_gshared (KeyCollection_t3261809038 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1927735574(__this, ___item0, method) ((  bool (*) (KeyCollection_t3261809038 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1927735574_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3001460731_gshared (KeyCollection_t3261809038 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3001460731(__this, ___item0, method) ((  bool (*) (KeyCollection_t3261809038 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3001460731_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2952570433_gshared (KeyCollection_t3261809038 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2952570433(__this, method) ((  Il2CppObject* (*) (KeyCollection_t3261809038 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2952570433_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m622987425_gshared (KeyCollection_t3261809038 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m622987425(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3261809038 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m622987425_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3708494576_gshared (KeyCollection_t3261809038 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3708494576(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3261809038 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3708494576_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m9073399_gshared (KeyCollection_t3261809038 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m9073399(__this, method) ((  bool (*) (KeyCollection_t3261809038 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m9073399_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m395998313_gshared (KeyCollection_t3261809038 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m395998313(__this, method) ((  bool (*) (KeyCollection_t3261809038 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m395998313_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m1074099611_gshared (KeyCollection_t3261809038 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m1074099611(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3261809038 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m1074099611_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m2686252563_gshared (KeyCollection_t3261809038 * __this, FieldTypeU5BU5D_t1420599677* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m2686252563(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3261809038 *, FieldTypeU5BU5D_t1420599677*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m2686252563_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::GetEnumerator()
extern "C"  Enumerator_t705561699  KeyCollection_GetEnumerator_m2552633248_gshared (KeyCollection_t3261809038 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m2552633248(__this, method) ((  Enumerator_t705561699  (*) (KeyCollection_t3261809038 *, const MethodInfo*))KeyCollection_GetEnumerator_m2552633248_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m26838179_gshared (KeyCollection_t3261809038 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m26838179(__this, method) ((  int32_t (*) (KeyCollection_t3261809038 *, const MethodInfo*))KeyCollection_get_Count_m26838179_gshared)(__this, method)
