﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// NewTableView
struct NewTableView_t2775249395;
// InstantiationPool`1<System.Object>
struct InstantiationPool_1_t1380750307;
// ConnectionCollector
struct ConnectionCollector_t444796719;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t1634065389;
// ZergRush.IObservableCollection`1<System.Object>
struct IObservableCollection_1_t200574308;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// System.String
struct String_t;
// System.Action`2<System.Object,System.Object>
struct Action_2_t4105459918;
// System.Func`2<System.Object,System.Single>
struct Func_2_t2256885953;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t1509682273;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TableBlock`2<System.Object,System.Object>
struct  TableBlock_2_t3756399434  : public Il2CppObject
{
public:
	// NewTableView TableBlock`2::newTableView
	NewTableView_t2775249395 * ___newTableView_0;
	// InstantiationPool`1<TPrefab> TableBlock`2::pool
	InstantiationPool_1_t1380750307 * ___pool_1;
	// ConnectionCollector TableBlock`2::collector
	ConnectionCollector_t444796719 * ___collector_2;
	// System.Single TableBlock`2::defaultCellSize
	float ___defaultCellSize_3;
	// System.Collections.Generic.List`1<TPrefab> TableBlock`2::visibleList
	List_1_t1634065389 * ___visibleList_4;
	// ZergRush.IObservableCollection`1<TData> TableBlock`2::_observableData
	Il2CppObject* ____observableData_5;
	// UnityEngine.GameObject TableBlock`2::_prefab
	GameObject_t4012695102 * ____prefab_6;
	// System.String TableBlock`2::_prefabPath
	String_t* ____prefabPath_7;
	// System.Action`2<TPrefab,TData> TableBlock`2::_fillMethod
	Action_2_t4105459918 * ____fillMethod_8;

public:
	inline static int32_t get_offset_of_newTableView_0() { return static_cast<int32_t>(offsetof(TableBlock_2_t3756399434, ___newTableView_0)); }
	inline NewTableView_t2775249395 * get_newTableView_0() const { return ___newTableView_0; }
	inline NewTableView_t2775249395 ** get_address_of_newTableView_0() { return &___newTableView_0; }
	inline void set_newTableView_0(NewTableView_t2775249395 * value)
	{
		___newTableView_0 = value;
		Il2CppCodeGenWriteBarrier(&___newTableView_0, value);
	}

	inline static int32_t get_offset_of_pool_1() { return static_cast<int32_t>(offsetof(TableBlock_2_t3756399434, ___pool_1)); }
	inline InstantiationPool_1_t1380750307 * get_pool_1() const { return ___pool_1; }
	inline InstantiationPool_1_t1380750307 ** get_address_of_pool_1() { return &___pool_1; }
	inline void set_pool_1(InstantiationPool_1_t1380750307 * value)
	{
		___pool_1 = value;
		Il2CppCodeGenWriteBarrier(&___pool_1, value);
	}

	inline static int32_t get_offset_of_collector_2() { return static_cast<int32_t>(offsetof(TableBlock_2_t3756399434, ___collector_2)); }
	inline ConnectionCollector_t444796719 * get_collector_2() const { return ___collector_2; }
	inline ConnectionCollector_t444796719 ** get_address_of_collector_2() { return &___collector_2; }
	inline void set_collector_2(ConnectionCollector_t444796719 * value)
	{
		___collector_2 = value;
		Il2CppCodeGenWriteBarrier(&___collector_2, value);
	}

	inline static int32_t get_offset_of_defaultCellSize_3() { return static_cast<int32_t>(offsetof(TableBlock_2_t3756399434, ___defaultCellSize_3)); }
	inline float get_defaultCellSize_3() const { return ___defaultCellSize_3; }
	inline float* get_address_of_defaultCellSize_3() { return &___defaultCellSize_3; }
	inline void set_defaultCellSize_3(float value)
	{
		___defaultCellSize_3 = value;
	}

	inline static int32_t get_offset_of_visibleList_4() { return static_cast<int32_t>(offsetof(TableBlock_2_t3756399434, ___visibleList_4)); }
	inline List_1_t1634065389 * get_visibleList_4() const { return ___visibleList_4; }
	inline List_1_t1634065389 ** get_address_of_visibleList_4() { return &___visibleList_4; }
	inline void set_visibleList_4(List_1_t1634065389 * value)
	{
		___visibleList_4 = value;
		Il2CppCodeGenWriteBarrier(&___visibleList_4, value);
	}

	inline static int32_t get_offset_of__observableData_5() { return static_cast<int32_t>(offsetof(TableBlock_2_t3756399434, ____observableData_5)); }
	inline Il2CppObject* get__observableData_5() const { return ____observableData_5; }
	inline Il2CppObject** get_address_of__observableData_5() { return &____observableData_5; }
	inline void set__observableData_5(Il2CppObject* value)
	{
		____observableData_5 = value;
		Il2CppCodeGenWriteBarrier(&____observableData_5, value);
	}

	inline static int32_t get_offset_of__prefab_6() { return static_cast<int32_t>(offsetof(TableBlock_2_t3756399434, ____prefab_6)); }
	inline GameObject_t4012695102 * get__prefab_6() const { return ____prefab_6; }
	inline GameObject_t4012695102 ** get_address_of__prefab_6() { return &____prefab_6; }
	inline void set__prefab_6(GameObject_t4012695102 * value)
	{
		____prefab_6 = value;
		Il2CppCodeGenWriteBarrier(&____prefab_6, value);
	}

	inline static int32_t get_offset_of__prefabPath_7() { return static_cast<int32_t>(offsetof(TableBlock_2_t3756399434, ____prefabPath_7)); }
	inline String_t* get__prefabPath_7() const { return ____prefabPath_7; }
	inline String_t** get_address_of__prefabPath_7() { return &____prefabPath_7; }
	inline void set__prefabPath_7(String_t* value)
	{
		____prefabPath_7 = value;
		Il2CppCodeGenWriteBarrier(&____prefabPath_7, value);
	}

	inline static int32_t get_offset_of__fillMethod_8() { return static_cast<int32_t>(offsetof(TableBlock_2_t3756399434, ____fillMethod_8)); }
	inline Action_2_t4105459918 * get__fillMethod_8() const { return ____fillMethod_8; }
	inline Action_2_t4105459918 ** get_address_of__fillMethod_8() { return &____fillMethod_8; }
	inline void set__fillMethod_8(Action_2_t4105459918 * value)
	{
		____fillMethod_8 = value;
		Il2CppCodeGenWriteBarrier(&____fillMethod_8, value);
	}
};

struct TableBlock_2_t3756399434_StaticFields
{
public:
	// System.Func`2<TPrefab,System.Single> TableBlock`2::<>f__am$cache9
	Func_2_t2256885953 * ___U3CU3Ef__amU24cache9_9;
	// System.Func`2<TPrefab,System.Boolean> TableBlock`2::<>f__am$cacheA
	Func_2_t1509682273 * ___U3CU3Ef__amU24cacheA_10;
	// System.Func`2<TPrefab,System.Boolean> TableBlock`2::<>f__am$cacheB
	Func_2_t1509682273 * ___U3CU3Ef__amU24cacheB_11;
	// System.Func`2<TPrefab,System.Boolean> TableBlock`2::<>f__am$cacheC
	Func_2_t1509682273 * ___U3CU3Ef__amU24cacheC_12;
	// System.Func`2<TPrefab,System.Boolean> TableBlock`2::<>f__am$cacheD
	Func_2_t1509682273 * ___U3CU3Ef__amU24cacheD_13;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache9_9() { return static_cast<int32_t>(offsetof(TableBlock_2_t3756399434_StaticFields, ___U3CU3Ef__amU24cache9_9)); }
	inline Func_2_t2256885953 * get_U3CU3Ef__amU24cache9_9() const { return ___U3CU3Ef__amU24cache9_9; }
	inline Func_2_t2256885953 ** get_address_of_U3CU3Ef__amU24cache9_9() { return &___U3CU3Ef__amU24cache9_9; }
	inline void set_U3CU3Ef__amU24cache9_9(Func_2_t2256885953 * value)
	{
		___U3CU3Ef__amU24cache9_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache9_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheA_10() { return static_cast<int32_t>(offsetof(TableBlock_2_t3756399434_StaticFields, ___U3CU3Ef__amU24cacheA_10)); }
	inline Func_2_t1509682273 * get_U3CU3Ef__amU24cacheA_10() const { return ___U3CU3Ef__amU24cacheA_10; }
	inline Func_2_t1509682273 ** get_address_of_U3CU3Ef__amU24cacheA_10() { return &___U3CU3Ef__amU24cacheA_10; }
	inline void set_U3CU3Ef__amU24cacheA_10(Func_2_t1509682273 * value)
	{
		___U3CU3Ef__amU24cacheA_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheA_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_11() { return static_cast<int32_t>(offsetof(TableBlock_2_t3756399434_StaticFields, ___U3CU3Ef__amU24cacheB_11)); }
	inline Func_2_t1509682273 * get_U3CU3Ef__amU24cacheB_11() const { return ___U3CU3Ef__amU24cacheB_11; }
	inline Func_2_t1509682273 ** get_address_of_U3CU3Ef__amU24cacheB_11() { return &___U3CU3Ef__amU24cacheB_11; }
	inline void set_U3CU3Ef__amU24cacheB_11(Func_2_t1509682273 * value)
	{
		___U3CU3Ef__amU24cacheB_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheB_11, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheC_12() { return static_cast<int32_t>(offsetof(TableBlock_2_t3756399434_StaticFields, ___U3CU3Ef__amU24cacheC_12)); }
	inline Func_2_t1509682273 * get_U3CU3Ef__amU24cacheC_12() const { return ___U3CU3Ef__amU24cacheC_12; }
	inline Func_2_t1509682273 ** get_address_of_U3CU3Ef__amU24cacheC_12() { return &___U3CU3Ef__amU24cacheC_12; }
	inline void set_U3CU3Ef__amU24cacheC_12(Func_2_t1509682273 * value)
	{
		___U3CU3Ef__amU24cacheC_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheC_12, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheD_13() { return static_cast<int32_t>(offsetof(TableBlock_2_t3756399434_StaticFields, ___U3CU3Ef__amU24cacheD_13)); }
	inline Func_2_t1509682273 * get_U3CU3Ef__amU24cacheD_13() const { return ___U3CU3Ef__amU24cacheD_13; }
	inline Func_2_t1509682273 ** get_address_of_U3CU3Ef__amU24cacheD_13() { return &___U3CU3Ef__amU24cacheD_13; }
	inline void set_U3CU3Ef__amU24cacheD_13(Func_2_t1509682273 * value)
	{
		___U3CU3Ef__amU24cacheD_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheD_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
