﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Progress`1<System.Object>
struct Progress_1_t1807652617;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.Progress`1<System.Object>::.ctor(System.Action`1<T>)
extern "C"  void Progress_1__ctor_m933744241_gshared (Progress_1_t1807652617 * __this, Action_1_t985559125 * ___report0, const MethodInfo* method);
#define Progress_1__ctor_m933744241(__this, ___report0, method) ((  void (*) (Progress_1_t1807652617 *, Action_1_t985559125 *, const MethodInfo*))Progress_1__ctor_m933744241_gshared)(__this, ___report0, method)
// System.Void UniRx.Progress`1<System.Object>::Report(T)
extern "C"  void Progress_1_Report_m1709815895_gshared (Progress_1_t1807652617 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Progress_1_Report_m1709815895(__this, ___value0, method) ((  void (*) (Progress_1_t1807652617 *, Il2CppObject *, const MethodInfo*))Progress_1_Report_m1709815895_gshared)(__this, ___value0, method)
