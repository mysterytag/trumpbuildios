﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen2703348181.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_Currency4112277569.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Nullable`1<PlayFab.ClientModels.Currency>::.ctor(T)
extern "C"  void Nullable_1__ctor_m2655699942_gshared (Nullable_1_t2703348181 * __this, int32_t ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m2655699942(__this, ___value0, method) ((  void (*) (Nullable_1_t2703348181 *, int32_t, const MethodInfo*))Nullable_1__ctor_m2655699942_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<PlayFab.ClientModels.Currency>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m2091484046_gshared (Nullable_1_t2703348181 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m2091484046(__this, method) ((  bool (*) (Nullable_1_t2703348181 *, const MethodInfo*))Nullable_1_get_HasValue_m2091484046_gshared)(__this, method)
// T System.Nullable`1<PlayFab.ClientModels.Currency>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m3011379711_gshared (Nullable_1_t2703348181 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m3011379711(__this, method) ((  int32_t (*) (Nullable_1_t2703348181 *, const MethodInfo*))Nullable_1_get_Value_m3011379711_gshared)(__this, method)
// System.Boolean System.Nullable`1<PlayFab.ClientModels.Currency>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m953058087_gshared (Nullable_1_t2703348181 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m953058087(__this, ___other0, method) ((  bool (*) (Nullable_1_t2703348181 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m953058087_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<PlayFab.ClientModels.Currency>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1144489528_gshared (Nullable_1_t2703348181 * __this, Nullable_1_t2703348181  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m1144489528(__this, ___other0, method) ((  bool (*) (Nullable_1_t2703348181 *, Nullable_1_t2703348181 , const MethodInfo*))Nullable_1_Equals_m1144489528_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<PlayFab.ClientModels.Currency>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m2705279883_gshared (Nullable_1_t2703348181 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m2705279883(__this, method) ((  int32_t (*) (Nullable_1_t2703348181 *, const MethodInfo*))Nullable_1_GetHashCode_m2705279883_gshared)(__this, method)
// T System.Nullable`1<PlayFab.ClientModels.Currency>::GetValueOrDefault()
extern "C"  int32_t Nullable_1_GetValueOrDefault_m1336388442_gshared (Nullable_1_t2703348181 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m1336388442(__this, method) ((  int32_t (*) (Nullable_1_t2703348181 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m1336388442_gshared)(__this, method)
// System.String System.Nullable`1<PlayFab.ClientModels.Currency>::ToString()
extern "C"  String_t* Nullable_1_ToString_m3935192347_gshared (Nullable_1_t2703348181 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m3935192347(__this, method) ((  String_t* (*) (Nullable_1_t2703348181 *, const MethodInfo*))Nullable_1_ToString_m3935192347_gshared)(__this, method)
// System.Nullable`1<T> System.Nullable`1<PlayFab.ClientModels.Currency>::op_Implicit(T)
extern "C"  Nullable_1_t2703348181  Nullable_1_op_Implicit_m4116364699_gshared (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method);
#define Nullable_1_op_Implicit_m4116364699(__this /* static, unused */, ___value0, method) ((  Nullable_1_t2703348181  (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))Nullable_1_op_Implicit_m4116364699_gshared)(__this /* static, unused */, ___value0, method)
