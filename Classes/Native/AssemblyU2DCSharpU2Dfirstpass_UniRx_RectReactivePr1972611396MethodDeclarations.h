﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.RectReactiveProperty
struct RectReactiveProperty_t1972611396;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Rect>
struct IEqualityComparer_1_t3849695468;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Rect1525428817.h"

// System.Void UniRx.RectReactiveProperty::.ctor()
extern "C"  void RectReactiveProperty__ctor_m2812758981 (RectReactiveProperty_t1972611396 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.RectReactiveProperty::.ctor(UnityEngine.Rect)
extern "C"  void RectReactiveProperty__ctor_m88184714 (RectReactiveProperty_t1972611396 * __this, Rect_t1525428817  ___initialValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Rect> UniRx.RectReactiveProperty::get_EqualityComparer()
extern "C"  Il2CppObject* RectReactiveProperty_get_EqualityComparer_m3423873467 (RectReactiveProperty_t1972611396 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
