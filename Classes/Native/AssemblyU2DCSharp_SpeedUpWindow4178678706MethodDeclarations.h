﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SpeedUpWindow
struct SpeedUpWindow_t4178678706;
// Cell`1<System.DateTime>
struct Cell_1_t521131568;
// <>__AnonType5`2<System.DateTime,System.Int64>
struct U3CU3E__AnonType5_2_t3897323094;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime339033936.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

// System.Void SpeedUpWindow::.ctor()
extern "C"  void SpeedUpWindow__ctor_m2546914809 (SpeedUpWindow_t4178678706 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpeedUpWindow::PostInject()
extern "C"  void SpeedUpWindow_PostInject_m1946096316 (SpeedUpWindow_t4178678706 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpeedUpWindow::Init(Cell`1<System.DateTime>)
extern "C"  void SpeedUpWindow_Init_m2996844970 (SpeedUpWindow_t4178678706 * __this, Cell_1_t521131568 * ___deliveryTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SpeedUpWindow::CalculateCost(System.DateTime)
extern "C"  int32_t SpeedUpWindow_CalculateCost_m1564338432 (Il2CppObject * __this /* static, unused */, DateTime_t339033936  ___currentTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpeedUpWindow::Close()
extern "C"  void SpeedUpWindow_Close_m4257774351 (SpeedUpWindow_t4178678706 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpeedUpWindow::<PostInject>m__200(UniRx.Unit)
extern "C"  void SpeedUpWindow_U3CPostInjectU3Em__200_m1123257381 (SpeedUpWindow_t4178678706 * __this, Unit_t2558286038  ____0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpeedUpWindow::<PostInject>m__201(System.Single)
extern "C"  void SpeedUpWindow_U3CPostInjectU3Em__201_m2554373417 (SpeedUpWindow_t4178678706 * __this, float ___f0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// <>__AnonType5`2<System.DateTime,System.Int64> SpeedUpWindow::<Init>m__202(System.DateTime,System.Int64)
extern "C"  U3CU3E__AnonType5_2_t3897323094 * SpeedUpWindow_U3CInitU3Em__202_m3119409170 (Il2CppObject * __this /* static, unused */, DateTime_t339033936  ___time0, int64_t ____1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
