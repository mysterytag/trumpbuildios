﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Zenject.DiContainer
struct DiContainer_t2383114449;
// Zenject.PrefabSingletonProviderMap
struct PrefabSingletonProviderMap_t2514131929;
// Zenject.PrefabSingletonId
struct PrefabSingletonId_t3643845047;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// System.Func`2<UnityEngine.Component,System.Boolean>
struct Func_2_t2050525331;
// System.Func`2<UnityEngine.Component,System.Type>
struct Func_2_t323782629;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PrefabSingletonLazyCreator
struct  PrefabSingletonLazyCreator_t3719004230  : public Il2CppObject
{
public:
	// Zenject.DiContainer Zenject.PrefabSingletonLazyCreator::_container
	DiContainer_t2383114449 * ____container_0;
	// Zenject.PrefabSingletonProviderMap Zenject.PrefabSingletonLazyCreator::_owner
	PrefabSingletonProviderMap_t2514131929 * ____owner_1;
	// Zenject.PrefabSingletonId Zenject.PrefabSingletonLazyCreator::_id
	PrefabSingletonId_t3643845047 * ____id_2;
	// System.Int32 Zenject.PrefabSingletonLazyCreator::_referenceCount
	int32_t ____referenceCount_3;
	// UnityEngine.GameObject Zenject.PrefabSingletonLazyCreator::_rootObj
	GameObject_t4012695102 * ____rootObj_4;

public:
	inline static int32_t get_offset_of__container_0() { return static_cast<int32_t>(offsetof(PrefabSingletonLazyCreator_t3719004230, ____container_0)); }
	inline DiContainer_t2383114449 * get__container_0() const { return ____container_0; }
	inline DiContainer_t2383114449 ** get_address_of__container_0() { return &____container_0; }
	inline void set__container_0(DiContainer_t2383114449 * value)
	{
		____container_0 = value;
		Il2CppCodeGenWriteBarrier(&____container_0, value);
	}

	inline static int32_t get_offset_of__owner_1() { return static_cast<int32_t>(offsetof(PrefabSingletonLazyCreator_t3719004230, ____owner_1)); }
	inline PrefabSingletonProviderMap_t2514131929 * get__owner_1() const { return ____owner_1; }
	inline PrefabSingletonProviderMap_t2514131929 ** get_address_of__owner_1() { return &____owner_1; }
	inline void set__owner_1(PrefabSingletonProviderMap_t2514131929 * value)
	{
		____owner_1 = value;
		Il2CppCodeGenWriteBarrier(&____owner_1, value);
	}

	inline static int32_t get_offset_of__id_2() { return static_cast<int32_t>(offsetof(PrefabSingletonLazyCreator_t3719004230, ____id_2)); }
	inline PrefabSingletonId_t3643845047 * get__id_2() const { return ____id_2; }
	inline PrefabSingletonId_t3643845047 ** get_address_of__id_2() { return &____id_2; }
	inline void set__id_2(PrefabSingletonId_t3643845047 * value)
	{
		____id_2 = value;
		Il2CppCodeGenWriteBarrier(&____id_2, value);
	}

	inline static int32_t get_offset_of__referenceCount_3() { return static_cast<int32_t>(offsetof(PrefabSingletonLazyCreator_t3719004230, ____referenceCount_3)); }
	inline int32_t get__referenceCount_3() const { return ____referenceCount_3; }
	inline int32_t* get_address_of__referenceCount_3() { return &____referenceCount_3; }
	inline void set__referenceCount_3(int32_t value)
	{
		____referenceCount_3 = value;
	}

	inline static int32_t get_offset_of__rootObj_4() { return static_cast<int32_t>(offsetof(PrefabSingletonLazyCreator_t3719004230, ____rootObj_4)); }
	inline GameObject_t4012695102 * get__rootObj_4() const { return ____rootObj_4; }
	inline GameObject_t4012695102 ** get_address_of__rootObj_4() { return &____rootObj_4; }
	inline void set__rootObj_4(GameObject_t4012695102 * value)
	{
		____rootObj_4 = value;
		Il2CppCodeGenWriteBarrier(&____rootObj_4, value);
	}
};

struct PrefabSingletonLazyCreator_t3719004230_StaticFields
{
public:
	// System.Func`2<UnityEngine.Component,System.Boolean> Zenject.PrefabSingletonLazyCreator::<>f__am$cache5
	Func_2_t2050525331 * ___U3CU3Ef__amU24cache5_5;
	// System.Func`2<UnityEngine.Component,System.Type> Zenject.PrefabSingletonLazyCreator::<>f__am$cache6
	Func_2_t323782629 * ___U3CU3Ef__amU24cache6_6;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_5() { return static_cast<int32_t>(offsetof(PrefabSingletonLazyCreator_t3719004230_StaticFields, ___U3CU3Ef__amU24cache5_5)); }
	inline Func_2_t2050525331 * get_U3CU3Ef__amU24cache5_5() const { return ___U3CU3Ef__amU24cache5_5; }
	inline Func_2_t2050525331 ** get_address_of_U3CU3Ef__amU24cache5_5() { return &___U3CU3Ef__amU24cache5_5; }
	inline void set_U3CU3Ef__amU24cache5_5(Func_2_t2050525331 * value)
	{
		___U3CU3Ef__amU24cache5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_6() { return static_cast<int32_t>(offsetof(PrefabSingletonLazyCreator_t3719004230_StaticFields, ___U3CU3Ef__amU24cache6_6)); }
	inline Func_2_t323782629 * get_U3CU3Ef__amU24cache6_6() const { return ___U3CU3Ef__amU24cache6_6; }
	inline Func_2_t323782629 ** get_address_of_U3CU3Ef__amU24cache6_6() { return &___U3CU3Ef__amU24cache6_6; }
	inline void set_U3CU3Ef__amU24cache6_6(Func_2_t323782629 * value)
	{
		___U3CU3Ef__amU24cache6_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
