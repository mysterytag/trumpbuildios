﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;
// System.String[]
struct StringU5BU5D_t2956870243;

#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_OptionsVerifyM1652998574.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_SearchStrategy1294470191.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnePF.Options
struct  Options_t3062372690  : public Il2CppObject
{
public:
	// System.Int32 OnePF.Options::discoveryTimeoutMs
	int32_t ___discoveryTimeoutMs_2;
	// System.Boolean OnePF.Options::checkInventory
	bool ___checkInventory_3;
	// System.Int32 OnePF.Options::checkInventoryTimeoutMs
	int32_t ___checkInventoryTimeoutMs_4;
	// OnePF.OptionsVerifyMode OnePF.Options::verifyMode
	int32_t ___verifyMode_5;
	// OnePF.SearchStrategy OnePF.Options::storeSearchStrategy
	int32_t ___storeSearchStrategy_6;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> OnePF.Options::storeKeys
	Dictionary_2_t2606186806 * ___storeKeys_7;
	// System.String[] OnePF.Options::prefferedStoreNames
	StringU5BU5D_t2956870243* ___prefferedStoreNames_8;
	// System.String[] OnePF.Options::availableStoreNames
	StringU5BU5D_t2956870243* ___availableStoreNames_9;
	// System.Int32 OnePF.Options::samsungCertificationRequestCode
	int32_t ___samsungCertificationRequestCode_10;

public:
	inline static int32_t get_offset_of_discoveryTimeoutMs_2() { return static_cast<int32_t>(offsetof(Options_t3062372690, ___discoveryTimeoutMs_2)); }
	inline int32_t get_discoveryTimeoutMs_2() const { return ___discoveryTimeoutMs_2; }
	inline int32_t* get_address_of_discoveryTimeoutMs_2() { return &___discoveryTimeoutMs_2; }
	inline void set_discoveryTimeoutMs_2(int32_t value)
	{
		___discoveryTimeoutMs_2 = value;
	}

	inline static int32_t get_offset_of_checkInventory_3() { return static_cast<int32_t>(offsetof(Options_t3062372690, ___checkInventory_3)); }
	inline bool get_checkInventory_3() const { return ___checkInventory_3; }
	inline bool* get_address_of_checkInventory_3() { return &___checkInventory_3; }
	inline void set_checkInventory_3(bool value)
	{
		___checkInventory_3 = value;
	}

	inline static int32_t get_offset_of_checkInventoryTimeoutMs_4() { return static_cast<int32_t>(offsetof(Options_t3062372690, ___checkInventoryTimeoutMs_4)); }
	inline int32_t get_checkInventoryTimeoutMs_4() const { return ___checkInventoryTimeoutMs_4; }
	inline int32_t* get_address_of_checkInventoryTimeoutMs_4() { return &___checkInventoryTimeoutMs_4; }
	inline void set_checkInventoryTimeoutMs_4(int32_t value)
	{
		___checkInventoryTimeoutMs_4 = value;
	}

	inline static int32_t get_offset_of_verifyMode_5() { return static_cast<int32_t>(offsetof(Options_t3062372690, ___verifyMode_5)); }
	inline int32_t get_verifyMode_5() const { return ___verifyMode_5; }
	inline int32_t* get_address_of_verifyMode_5() { return &___verifyMode_5; }
	inline void set_verifyMode_5(int32_t value)
	{
		___verifyMode_5 = value;
	}

	inline static int32_t get_offset_of_storeSearchStrategy_6() { return static_cast<int32_t>(offsetof(Options_t3062372690, ___storeSearchStrategy_6)); }
	inline int32_t get_storeSearchStrategy_6() const { return ___storeSearchStrategy_6; }
	inline int32_t* get_address_of_storeSearchStrategy_6() { return &___storeSearchStrategy_6; }
	inline void set_storeSearchStrategy_6(int32_t value)
	{
		___storeSearchStrategy_6 = value;
	}

	inline static int32_t get_offset_of_storeKeys_7() { return static_cast<int32_t>(offsetof(Options_t3062372690, ___storeKeys_7)); }
	inline Dictionary_2_t2606186806 * get_storeKeys_7() const { return ___storeKeys_7; }
	inline Dictionary_2_t2606186806 ** get_address_of_storeKeys_7() { return &___storeKeys_7; }
	inline void set_storeKeys_7(Dictionary_2_t2606186806 * value)
	{
		___storeKeys_7 = value;
		Il2CppCodeGenWriteBarrier(&___storeKeys_7, value);
	}

	inline static int32_t get_offset_of_prefferedStoreNames_8() { return static_cast<int32_t>(offsetof(Options_t3062372690, ___prefferedStoreNames_8)); }
	inline StringU5BU5D_t2956870243* get_prefferedStoreNames_8() const { return ___prefferedStoreNames_8; }
	inline StringU5BU5D_t2956870243** get_address_of_prefferedStoreNames_8() { return &___prefferedStoreNames_8; }
	inline void set_prefferedStoreNames_8(StringU5BU5D_t2956870243* value)
	{
		___prefferedStoreNames_8 = value;
		Il2CppCodeGenWriteBarrier(&___prefferedStoreNames_8, value);
	}

	inline static int32_t get_offset_of_availableStoreNames_9() { return static_cast<int32_t>(offsetof(Options_t3062372690, ___availableStoreNames_9)); }
	inline StringU5BU5D_t2956870243* get_availableStoreNames_9() const { return ___availableStoreNames_9; }
	inline StringU5BU5D_t2956870243** get_address_of_availableStoreNames_9() { return &___availableStoreNames_9; }
	inline void set_availableStoreNames_9(StringU5BU5D_t2956870243* value)
	{
		___availableStoreNames_9 = value;
		Il2CppCodeGenWriteBarrier(&___availableStoreNames_9, value);
	}

	inline static int32_t get_offset_of_samsungCertificationRequestCode_10() { return static_cast<int32_t>(offsetof(Options_t3062372690, ___samsungCertificationRequestCode_10)); }
	inline int32_t get_samsungCertificationRequestCode_10() const { return ___samsungCertificationRequestCode_10; }
	inline int32_t* get_address_of_samsungCertificationRequestCode_10() { return &___samsungCertificationRequestCode_10; }
	inline void set_samsungCertificationRequestCode_10(int32_t value)
	{
		___samsungCertificationRequestCode_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
