﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;
// UniRx.IObserver`1<TableButtons/StatkaPoTableViev>
struct IObserver_1_t1425427424;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Subject`1<TableButtons/StatkaPoTableViev>
struct  Subject_1_t1151463141  : public Il2CppObject
{
public:
	// System.Object UniRx.Subject`1::observerLock
	Il2CppObject * ___observerLock_0;
	// System.Boolean UniRx.Subject`1::isStopped
	bool ___isStopped_1;
	// System.Boolean UniRx.Subject`1::isDisposed
	bool ___isDisposed_2;
	// System.Exception UniRx.Subject`1::lastError
	Exception_t1967233988 * ___lastError_3;
	// UniRx.IObserver`1<T> UniRx.Subject`1::outObserver
	Il2CppObject* ___outObserver_4;

public:
	inline static int32_t get_offset_of_observerLock_0() { return static_cast<int32_t>(offsetof(Subject_1_t1151463141, ___observerLock_0)); }
	inline Il2CppObject * get_observerLock_0() const { return ___observerLock_0; }
	inline Il2CppObject ** get_address_of_observerLock_0() { return &___observerLock_0; }
	inline void set_observerLock_0(Il2CppObject * value)
	{
		___observerLock_0 = value;
		Il2CppCodeGenWriteBarrier(&___observerLock_0, value);
	}

	inline static int32_t get_offset_of_isStopped_1() { return static_cast<int32_t>(offsetof(Subject_1_t1151463141, ___isStopped_1)); }
	inline bool get_isStopped_1() const { return ___isStopped_1; }
	inline bool* get_address_of_isStopped_1() { return &___isStopped_1; }
	inline void set_isStopped_1(bool value)
	{
		___isStopped_1 = value;
	}

	inline static int32_t get_offset_of_isDisposed_2() { return static_cast<int32_t>(offsetof(Subject_1_t1151463141, ___isDisposed_2)); }
	inline bool get_isDisposed_2() const { return ___isDisposed_2; }
	inline bool* get_address_of_isDisposed_2() { return &___isDisposed_2; }
	inline void set_isDisposed_2(bool value)
	{
		___isDisposed_2 = value;
	}

	inline static int32_t get_offset_of_lastError_3() { return static_cast<int32_t>(offsetof(Subject_1_t1151463141, ___lastError_3)); }
	inline Exception_t1967233988 * get_lastError_3() const { return ___lastError_3; }
	inline Exception_t1967233988 ** get_address_of_lastError_3() { return &___lastError_3; }
	inline void set_lastError_3(Exception_t1967233988 * value)
	{
		___lastError_3 = value;
		Il2CppCodeGenWriteBarrier(&___lastError_3, value);
	}

	inline static int32_t get_offset_of_outObserver_4() { return static_cast<int32_t>(offsetof(Subject_1_t1151463141, ___outObserver_4)); }
	inline Il2CppObject* get_outObserver_4() const { return ___outObserver_4; }
	inline Il2CppObject** get_address_of_outObserver_4() { return &___outObserver_4; }
	inline void set_outObserver_4(Il2CppObject* value)
	{
		___outObserver_4 = value;
		Il2CppCodeGenWriteBarrier(&___outObserver_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
