﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AdToApp.AdToAppSDKDelegate
struct AdToAppSDKDelegate_t2853395885;
// System.Action`2<System.String,System.String>
struct Action_2_t2887221574;
// System.Action`3<System.String,System.String,System.String>
struct Action_3_t2150340505;
// System.Action
struct Action_t437523947;
// System.Action`1<System.String>
struct Action_1_t1116941607;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3012272455;
// System.String
struct String_t;
// AdToApp.AndroidWrapper.ATAInterstitialAdListener
struct ATAInterstitialAdListener_t382012998;
// AdToApp.AndroidWrapper.ATABannerAdListener
struct ATABannerAdListener_t2854275270;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"
#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "mscorlib_System_String968488902.h"

// System.Void AdToApp.AdToAppSDKDelegate::.ctor()
extern "C"  void AdToAppSDKDelegate__ctor_m3716369608 (AdToAppSDKDelegate_t2853395885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppSDKDelegate::add_OnInterstitialStarted(System.Action`2<System.String,System.String>)
extern "C"  void AdToAppSDKDelegate_add_OnInterstitialStarted_m2918704575 (AdToAppSDKDelegate_t2853395885 * __this, Action_2_t2887221574 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppSDKDelegate::remove_OnInterstitialStarted(System.Action`2<System.String,System.String>)
extern "C"  void AdToAppSDKDelegate_remove_OnInterstitialStarted_m2274127864 (AdToAppSDKDelegate_t2853395885 * __this, Action_2_t2887221574 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppSDKDelegate::add_OnInterstitialClosed(System.Action`2<System.String,System.String>)
extern "C"  void AdToAppSDKDelegate_add_OnInterstitialClosed_m2977913650 (AdToAppSDKDelegate_t2853395885 * __this, Action_2_t2887221574 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppSDKDelegate::remove_OnInterstitialClosed(System.Action`2<System.String,System.String>)
extern "C"  void AdToAppSDKDelegate_remove_OnInterstitialClosed_m3095668185 (AdToAppSDKDelegate_t2853395885 * __this, Action_2_t2887221574 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppSDKDelegate::add_OnInterstitialFailedToAppear(System.Action`2<System.String,System.String>)
extern "C"  void AdToAppSDKDelegate_add_OnInterstitialFailedToAppear_m2222989139 (AdToAppSDKDelegate_t2853395885 * __this, Action_2_t2887221574 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppSDKDelegate::remove_OnInterstitialFailedToAppear(System.Action`2<System.String,System.String>)
extern "C"  void AdToAppSDKDelegate_remove_OnInterstitialFailedToAppear_m4290728698 (AdToAppSDKDelegate_t2853395885 * __this, Action_2_t2887221574 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppSDKDelegate::add_OnInterstitialClicked(System.Action`2<System.String,System.String>)
extern "C"  void AdToAppSDKDelegate_add_OnInterstitialClicked_m1662632677 (AdToAppSDKDelegate_t2853395885 * __this, Action_2_t2887221574 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppSDKDelegate::remove_OnInterstitialClicked(System.Action`2<System.String,System.String>)
extern "C"  void AdToAppSDKDelegate_remove_OnInterstitialClicked_m1018055966 (AdToAppSDKDelegate_t2853395885 * __this, Action_2_t2887221574 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppSDKDelegate::add_OnFirstInterstitialLoad(System.Action`2<System.String,System.String>)
extern "C"  void AdToAppSDKDelegate_add_OnFirstInterstitialLoad_m191452748 (AdToAppSDKDelegate_t2853395885 * __this, Action_2_t2887221574 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppSDKDelegate::remove_OnFirstInterstitialLoad(System.Action`2<System.String,System.String>)
extern "C"  void AdToAppSDKDelegate_remove_OnFirstInterstitialLoad_m3523491397 (AdToAppSDKDelegate_t2853395885 * __this, Action_2_t2887221574 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppSDKDelegate::add_OnRewardedCompleted(System.Action`3<System.String,System.String,System.String>)
extern "C"  void AdToAppSDKDelegate_add_OnRewardedCompleted_m3137603692 (AdToAppSDKDelegate_t2853395885 * __this, Action_3_t2150340505 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppSDKDelegate::remove_OnRewardedCompleted(System.Action`3<System.String,System.String,System.String>)
extern "C"  void AdToAppSDKDelegate_remove_OnRewardedCompleted_m2470839589 (AdToAppSDKDelegate_t2853395885 * __this, Action_3_t2150340505 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppSDKDelegate::add_OnBannerLoad(System.Action)
extern "C"  void AdToAppSDKDelegate_add_OnBannerLoad_m2000076754 (AdToAppSDKDelegate_t2853395885 * __this, Action_t437523947 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppSDKDelegate::remove_OnBannerLoad(System.Action)
extern "C"  void AdToAppSDKDelegate_remove_OnBannerLoad_m681883403 (AdToAppSDKDelegate_t2853395885 * __this, Action_t437523947 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppSDKDelegate::add_OnBannerFailedToLoad(System.Action`1<System.String>)
extern "C"  void AdToAppSDKDelegate_add_OnBannerFailedToLoad_m1386684831 (AdToAppSDKDelegate_t2853395885 * __this, Action_1_t1116941607 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppSDKDelegate::remove_OnBannerFailedToLoad(System.Action`1<System.String>)
extern "C"  void AdToAppSDKDelegate_remove_OnBannerFailedToLoad_m1047563398 (AdToAppSDKDelegate_t2853395885 * __this, Action_1_t1116941607 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppSDKDelegate::add_OnBannerClicked(System.Action)
extern "C"  void AdToAppSDKDelegate_add_OnBannerClicked_m3016283705 (AdToAppSDKDelegate_t2853395885 * __this, Action_t437523947 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppSDKDelegate::remove_OnBannerClicked(System.Action)
extern "C"  void AdToAppSDKDelegate_remove_OnBannerClicked_m1604151392 (AdToAppSDKDelegate_t2853395885 * __this, Action_t437523947 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AdToApp.AdToAppSDKDelegate AdToApp.AdToAppSDKDelegate::CreateInstance(UnityEngine.MonoBehaviour)
extern "C"  AdToAppSDKDelegate_t2853395885 * AdToAppSDKDelegate_CreateInstance_m1680219556 (Il2CppObject * __this /* static, unused */, MonoBehaviour_t3012272455 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppSDKDelegate::onInterstitialWillAppear(System.String)
extern "C"  void AdToAppSDKDelegate_onInterstitialWillAppear_m2297775444 (AdToAppSDKDelegate_t2853395885 * __this, String_t* ___adContentType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppSDKDelegate::onInterstitialDidDisappear(System.String)
extern "C"  void AdToAppSDKDelegate_onInterstitialDidDisappear_m1660603223 (AdToAppSDKDelegate_t2853395885 * __this, String_t* ___adContentType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppSDKDelegate::onInterstitialFailedToAppear(System.String)
extern "C"  void AdToAppSDKDelegate_onInterstitialFailedToAppear_m3939159118 (AdToAppSDKDelegate_t2853395885 * __this, String_t* ___adContentType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppSDKDelegate::onInterstitialClicked(System.String)
extern "C"  void AdToAppSDKDelegate_onInterstitialClicked_m2266473024 (AdToAppSDKDelegate_t2853395885 * __this, String_t* ___adContentType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppSDKDelegate::onInterstitialFirstLoaded(System.String)
extern "C"  void AdToAppSDKDelegate_onInterstitialFirstLoaded_m352211666 (AdToAppSDKDelegate_t2853395885 * __this, String_t* ___adContentType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppSDKDelegate::onRewardedCompleted(System.String)
extern "C"  void AdToAppSDKDelegate_onRewardedCompleted_m2022259198 (AdToAppSDKDelegate_t2853395885 * __this, String_t* ___reward0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppSDKDelegate::onBannerDidDisplayAd()
extern "C"  void AdToAppSDKDelegate_onBannerDidDisplayAd_m1161177453 (AdToAppSDKDelegate_t2853395885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppSDKDelegate::onBannerFailedToDisplayAd(System.String)
extern "C"  void AdToAppSDKDelegate_onBannerFailedToDisplayAd_m1512799706 (AdToAppSDKDelegate_t2853395885 * __this, String_t* ___errorDescription0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppSDKDelegate::onBannerClicked()
extern "C"  void AdToAppSDKDelegate_onBannerClicked_m2753084546 (AdToAppSDKDelegate_t2853395885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AdToApp.AndroidWrapper.ATAInterstitialAdListener AdToApp.AdToAppSDKDelegate::GetInterstitialAndroidAdListener()
extern "C"  ATAInterstitialAdListener_t382012998 * AdToAppSDKDelegate_GetInterstitialAndroidAdListener_m3429272147 (AdToAppSDKDelegate_t2853395885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AdToApp.AndroidWrapper.ATABannerAdListener AdToApp.AdToAppSDKDelegate::GetBannerAndroidAdListener()
extern "C"  ATABannerAdListener_t2854275270 * AdToAppSDKDelegate_GetBannerAndroidAdListener_m1240955859 (AdToAppSDKDelegate_t2853395885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
