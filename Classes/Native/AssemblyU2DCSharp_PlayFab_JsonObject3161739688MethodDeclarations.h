﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.JsonObject
struct JsonObject_t3161739688;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t3292755553;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IDictionary`2<System.String,System.Object>
struct IDictionary_2_t3650470111;
// System.String
struct String_t;
// System.Collections.Generic.ICollection`1<System.String>
struct ICollection_1_t1434320288;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1302937806;
// System.Collections.Generic.KeyValuePair`2<System.String,System.Object>[]
struct KeyValuePair_2U5BU5D_t2880731747;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>>
struct IEnumerator_1_t3446442070;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21963335622.h"

// System.Void PlayFab.JsonObject::.ctor()
extern "C"  void JsonObject__ctor_m1507580601 (JsonObject_t3161739688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.JsonObject::.ctor(System.Collections.Generic.IEqualityComparer`1<System.String>)
extern "C"  void JsonObject__ctor_m141067585 (JsonObject_t3161739688 * __this, Il2CppObject* ___comparer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PlayFab.JsonObject::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * JsonObject_System_Collections_IEnumerable_GetEnumerator_m687072792 (JsonObject_t3161739688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayFab.JsonObject::get_Item(System.Int32)
extern "C"  Il2CppObject * JsonObject_get_Item_m2270294797 (JsonObject_t3161739688 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayFab.JsonObject::GetAtIndex(System.Collections.Generic.IDictionary`2<System.String,System.Object>,System.Int32)
extern "C"  Il2CppObject * JsonObject_GetAtIndex_m3971159812 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___obj0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.JsonObject::Add(System.String,System.Object)
extern "C"  void JsonObject_Add_m3202177432 (JsonObject_t3161739688 * __this, String_t* ___key0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayFab.JsonObject::ContainsKey(System.String)
extern "C"  bool JsonObject_ContainsKey_m2503020799 (JsonObject_t3161739688 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.ICollection`1<System.String> PlayFab.JsonObject::get_Keys()
extern "C"  Il2CppObject* JsonObject_get_Keys_m1826451642 (JsonObject_t3161739688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayFab.JsonObject::Remove(System.String)
extern "C"  bool JsonObject_Remove_m3241388735 (JsonObject_t3161739688 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayFab.JsonObject::TryGetValue(System.String,System.Object&)
extern "C"  bool JsonObject_TryGetValue_m64639029 (JsonObject_t3161739688 * __this, String_t* ___key0, Il2CppObject ** ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.ICollection`1<System.Object> PlayFab.JsonObject::get_Values()
extern "C"  Il2CppObject* JsonObject_get_Values_m2599364310 (JsonObject_t3161739688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayFab.JsonObject::get_Item(System.String)
extern "C"  Il2CppObject * JsonObject_get_Item_m2116360390 (JsonObject_t3161739688 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.JsonObject::set_Item(System.String,System.Object)
extern "C"  void JsonObject_set_Item_m3488972341 (JsonObject_t3161739688 * __this, String_t* ___key0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.JsonObject::Add(System.Collections.Generic.KeyValuePair`2<System.String,System.Object>)
extern "C"  void JsonObject_Add_m3272852685 (JsonObject_t3161739688 * __this, KeyValuePair_2_t1963335622  ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.JsonObject::Clear()
extern "C"  void JsonObject_Clear_m3208681188 (JsonObject_t3161739688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayFab.JsonObject::Contains(System.Collections.Generic.KeyValuePair`2<System.String,System.Object>)
extern "C"  bool JsonObject_Contains_m3471609619 (JsonObject_t3161739688 * __this, KeyValuePair_2_t1963335622  ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.JsonObject::CopyTo(System.Collections.Generic.KeyValuePair`2<System.String,System.Object>[],System.Int32)
extern "C"  void JsonObject_CopyTo_m3827660265 (JsonObject_t3161739688 * __this, KeyValuePair_2U5BU5D_t2880731747* ___array0, int32_t ___arrayIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlayFab.JsonObject::get_Count()
extern "C"  int32_t JsonObject_get_Count_m2449193583 (JsonObject_t3161739688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayFab.JsonObject::get_IsReadOnly()
extern "C"  bool JsonObject_get_IsReadOnly_m1516332180 (JsonObject_t3161739688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayFab.JsonObject::Remove(System.Collections.Generic.KeyValuePair`2<System.String,System.Object>)
extern "C"  bool JsonObject_Remove_m3401073656 (JsonObject_t3161739688 * __this, KeyValuePair_2_t1963335622  ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>> PlayFab.JsonObject::GetEnumerator()
extern "C"  Il2CppObject* JsonObject_GetEnumerator_m3840333050 (JsonObject_t3161739688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.JsonObject::ToString()
extern "C"  String_t* JsonObject_ToString_m3798127802 (JsonObject_t3161739688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
