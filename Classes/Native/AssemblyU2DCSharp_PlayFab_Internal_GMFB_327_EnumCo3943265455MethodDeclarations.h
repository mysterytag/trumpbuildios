﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.Internal.GMFB_327/EnumConversionTestClass
struct EnumConversionTestClass_t3943265455;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void PlayFab.Internal.GMFB_327/EnumConversionTestClass::.ctor()
extern "C"  void EnumConversionTestClass__ctor_m1003452922 (EnumConversionTestClass_t3943265455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayFab.Internal.GMFB_327/EnumConversionTestClass::Equals(System.Object)
extern "C"  bool EnumConversionTestClass_Equals_m1697714463 (EnumConversionTestClass_t3943265455 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlayFab.Internal.GMFB_327/EnumConversionTestClass::GetHashCode()
extern "C"  int32_t EnumConversionTestClass_GetHashCode_m210119991 (EnumConversionTestClass_t3943265455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
