﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Func`3<System.Object,UniRx.CancellationToken,System.Object>
struct Func_3_t2554989993;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Func`3<System.Object,UniRx.CancellationToken,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_3__ctor_m2183027641_gshared (Func_3_t2554989993 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Func_3__ctor_m2183027641(__this, ___object0, ___method1, method) ((  void (*) (Func_3_t2554989993 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_3__ctor_m2183027641_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`3<System.Object,UniRx.CancellationToken,System.Object>::Invoke(T1,T2)
extern "C"  Il2CppObject * Func_3_Invoke_m1848481136_gshared (Func_3_t2554989993 * __this, Il2CppObject * ___arg10, CancellationToken_t1439151560  ___arg21, const MethodInfo* method);
#define Func_3_Invoke_m1848481136(__this, ___arg10, ___arg21, method) ((  Il2CppObject * (*) (Func_3_t2554989993 *, Il2CppObject *, CancellationToken_t1439151560 , const MethodInfo*))Func_3_Invoke_m1848481136_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Func`3<System.Object,UniRx.CancellationToken,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_3_BeginInvoke_m2647552753_gshared (Func_3_t2554989993 * __this, Il2CppObject * ___arg10, CancellationToken_t1439151560  ___arg21, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Func_3_BeginInvoke_m2647552753(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Func_3_t2554989993 *, Il2CppObject *, CancellationToken_t1439151560 , AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_3_BeginInvoke_m2647552753_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// TResult System.Func`3<System.Object,UniRx.CancellationToken,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Func_3_EndInvoke_m847220587_gshared (Func_3_t2554989993 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Func_3_EndInvoke_m847220587(__this, ___result0, method) ((  Il2CppObject * (*) (Func_3_t2554989993 *, Il2CppObject *, const MethodInfo*))Func_3_EndInvoke_m847220587_gshared)(__this, ___result0, method)
