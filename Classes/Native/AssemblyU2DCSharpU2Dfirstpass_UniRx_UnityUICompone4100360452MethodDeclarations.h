﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.UnityUIComponentExtensions/<SubscribeToText>c__AnonStoreyA4`1<System.Object>
struct U3CSubscribeToTextU3Ec__AnonStoreyA4_1_t4100360452;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.UnityUIComponentExtensions/<SubscribeToText>c__AnonStoreyA4`1<System.Object>::.ctor()
extern "C"  void U3CSubscribeToTextU3Ec__AnonStoreyA4_1__ctor_m1105422629_gshared (U3CSubscribeToTextU3Ec__AnonStoreyA4_1_t4100360452 * __this, const MethodInfo* method);
#define U3CSubscribeToTextU3Ec__AnonStoreyA4_1__ctor_m1105422629(__this, method) ((  void (*) (U3CSubscribeToTextU3Ec__AnonStoreyA4_1_t4100360452 *, const MethodInfo*))U3CSubscribeToTextU3Ec__AnonStoreyA4_1__ctor_m1105422629_gshared)(__this, method)
// System.Void UniRx.UnityUIComponentExtensions/<SubscribeToText>c__AnonStoreyA4`1<System.Object>::<>m__E2(T)
extern "C"  void U3CSubscribeToTextU3Ec__AnonStoreyA4_1_U3CU3Em__E2_m684608707_gshared (U3CSubscribeToTextU3Ec__AnonStoreyA4_1_t4100360452 * __this, Il2CppObject * ___x0, const MethodInfo* method);
#define U3CSubscribeToTextU3Ec__AnonStoreyA4_1_U3CU3Em__E2_m684608707(__this, ___x0, method) ((  void (*) (U3CSubscribeToTextU3Ec__AnonStoreyA4_1_t4100360452 *, Il2CppObject *, const MethodInfo*))U3CSubscribeToTextU3Ec__AnonStoreyA4_1_U3CU3Em__E2_m684608707_gshared)(__this, ___x0, method)
