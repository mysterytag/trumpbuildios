﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.UnityDependencyRoot
struct UnityDependencyRoot_t1917880567;

#include "codegen/il2cpp-codegen.h"

// System.Void Zenject.UnityDependencyRoot::.ctor()
extern "C"  void UnityDependencyRoot__ctor_m1275946440 (UnityDependencyRoot_t1917880567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityDependencyRoot::Initialize()
extern "C"  void UnityDependencyRoot_Initialize_m390024652 (UnityDependencyRoot_t1917880567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityDependencyRoot::OnApplicationQuit()
extern "C"  void UnityDependencyRoot_OnApplicationQuit_m2508416838 (UnityDependencyRoot_t1917880567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityDependencyRoot::OnDestroy()
extern "C"  void UnityDependencyRoot_OnDestroy_m2812745409 (UnityDependencyRoot_t1917880567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityDependencyRoot::Update()
extern "C"  void UnityDependencyRoot_Update_m2626496069 (UnityDependencyRoot_t1917880567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityDependencyRoot::FixedUpdate()
extern "C"  void UnityDependencyRoot_FixedUpdate_m1301663043 (UnityDependencyRoot_t1917880567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.UnityDependencyRoot::LateUpdate()
extern "C"  void UnityDependencyRoot_LateUpdate_m1537753483 (UnityDependencyRoot_t1917880567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
