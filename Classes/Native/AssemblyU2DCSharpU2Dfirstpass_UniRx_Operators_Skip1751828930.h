﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.SkipObservable`1<System.Boolean>
struct SkipObservable_1_t197279106;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Operat60103313.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.SkipObservable`1/Skip_<System.Boolean>
struct  Skip__t1751828930  : public OperatorObserverBase_2_t60103313
{
public:
	// UniRx.Operators.SkipObservable`1<T> UniRx.Operators.SkipObservable`1/Skip_::parent
	SkipObservable_1_t197279106 * ___parent_2;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) UniRx.Operators.SkipObservable`1/Skip_::open
	bool ___open_3;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(Skip__t1751828930, ___parent_2)); }
	inline SkipObservable_1_t197279106 * get_parent_2() const { return ___parent_2; }
	inline SkipObservable_1_t197279106 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(SkipObservable_1_t197279106 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_open_3() { return static_cast<int32_t>(offsetof(Skip__t1751828930, ___open_3)); }
	inline bool get_open_3() const { return ___open_3; }
	inline bool* get_address_of_open_3() { return &___open_3; }
	inline void set_open_3(bool value)
	{
		___open_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
