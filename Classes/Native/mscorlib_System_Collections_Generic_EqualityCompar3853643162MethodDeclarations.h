﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<SponsorPay.SPLogLevel>
struct DefaultComparer_t3853643162;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SponsorPay_SPLogLevel220722135.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<SponsorPay.SPLogLevel>::.ctor()
extern "C"  void DefaultComparer__ctor_m1803609851_gshared (DefaultComparer_t3853643162 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1803609851(__this, method) ((  void (*) (DefaultComparer_t3853643162 *, const MethodInfo*))DefaultComparer__ctor_m1803609851_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<SponsorPay.SPLogLevel>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1566240080_gshared (DefaultComparer_t3853643162 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m1566240080(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t3853643162 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m1566240080_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<SponsorPay.SPLogLevel>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3717996236_gshared (DefaultComparer_t3853643162 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m3717996236(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t3853643162 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m3717996236_gshared)(__this, ___x0, ___y1, method)
