﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.DiContainer/<GetDependencyContracts>c__Iterator4C
struct U3CGetDependencyContractsU3Ec__Iterator4C_t905474462;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<System.Type>
struct IEnumerator_1_t4262336383;

#include "codegen/il2cpp-codegen.h"

// System.Void Zenject.DiContainer/<GetDependencyContracts>c__Iterator4C::.ctor()
extern "C"  void U3CGetDependencyContractsU3Ec__Iterator4C__ctor_m856496127 (U3CGetDependencyContractsU3Ec__Iterator4C_t905474462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Zenject.DiContainer/<GetDependencyContracts>c__Iterator4C::System.Collections.Generic.IEnumerator<System.Type>.get_Current()
extern "C"  Type_t * U3CGetDependencyContractsU3Ec__Iterator4C_System_Collections_Generic_IEnumeratorU3CSystem_TypeU3E_get_Current_m1925518908 (U3CGetDependencyContractsU3Ec__Iterator4C_t905474462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Zenject.DiContainer/<GetDependencyContracts>c__Iterator4C::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetDependencyContractsU3Ec__Iterator4C_System_Collections_IEnumerator_get_Current_m2416895623 (U3CGetDependencyContractsU3Ec__Iterator4C_t905474462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Zenject.DiContainer/<GetDependencyContracts>c__Iterator4C::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CGetDependencyContractsU3Ec__Iterator4C_System_Collections_IEnumerable_GetEnumerator_m905350274 (U3CGetDependencyContractsU3Ec__Iterator4C_t905474462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Type> Zenject.DiContainer/<GetDependencyContracts>c__Iterator4C::System.Collections.Generic.IEnumerable<System.Type>.GetEnumerator()
extern "C"  Il2CppObject* U3CGetDependencyContractsU3Ec__Iterator4C_System_Collections_Generic_IEnumerableU3CSystem_TypeU3E_GetEnumerator_m2267679721 (U3CGetDependencyContractsU3Ec__Iterator4C_t905474462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.DiContainer/<GetDependencyContracts>c__Iterator4C::MoveNext()
extern "C"  bool U3CGetDependencyContractsU3Ec__Iterator4C_MoveNext_m2821909141 (U3CGetDependencyContractsU3Ec__Iterator4C_t905474462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.DiContainer/<GetDependencyContracts>c__Iterator4C::Dispose()
extern "C"  void U3CGetDependencyContractsU3Ec__Iterator4C_Dispose_m2652307580 (U3CGetDependencyContractsU3Ec__Iterator4C_t905474462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.DiContainer/<GetDependencyContracts>c__Iterator4C::Reset()
extern "C"  void U3CGetDependencyContractsU3Ec__Iterator4C_Reset_m2797896364 (U3CGetDependencyContractsU3Ec__Iterator4C_t905474462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
