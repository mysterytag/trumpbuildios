﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<System.Double,System.Object,System.Double>
struct Transform_1_t3903382114;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Double,System.Object,System.Double>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m914625311_gshared (Transform_1_t3903382114 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Transform_1__ctor_m914625311(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t3903382114 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m914625311_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Double,System.Object,System.Double>::Invoke(TKey,TValue)
extern "C"  double Transform_1_Invoke_m2733420029_gshared (Transform_1_t3903382114 * __this, double ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Transform_1_Invoke_m2733420029(__this, ___key0, ___value1, method) ((  double (*) (Transform_1_t3903382114 *, double, Il2CppObject *, const MethodInfo*))Transform_1_Invoke_m2733420029_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Double,System.Object,System.Double>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1878735196_gshared (Transform_1_t3903382114 * __this, double ___key0, Il2CppObject * ___value1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Transform_1_BeginInvoke_m1878735196(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t3903382114 *, double, Il2CppObject *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m1878735196_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Double,System.Object,System.Double>::EndInvoke(System.IAsyncResult)
extern "C"  double Transform_1_EndInvoke_m1133527661_gshared (Transform_1_t3903382114 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Transform_1_EndInvoke_m1133527661(__this, ___result0, method) ((  double (*) (Transform_1_t3903382114 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m1133527661_gshared)(__this, ___result0, method)
