﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AdToApp.AdToAppSDKDelegate/AndroidInterstitialListener
struct AndroidInterstitialListener_t3321194735;
// AdToApp.AdToAppSDKDelegate
struct AdToAppSDKDelegate_t2853395885;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AdToApp_AdToAppSDKDe2853395885.h"
#include "mscorlib_System_String968488902.h"

// System.Void AdToApp.AdToAppSDKDelegate/AndroidInterstitialListener::.ctor(AdToApp.AdToAppSDKDelegate)
extern "C"  void AndroidInterstitialListener__ctor_m1899201185 (AndroidInterstitialListener_t3321194735 * __this, AdToAppSDKDelegate_t2853395885 * ___sdkDelegate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppSDKDelegate/AndroidInterstitialListener::onInterstitialStarted(System.String,System.String)
extern "C"  void AndroidInterstitialListener_onInterstitialStarted_m1606331714 (AndroidInterstitialListener_t3321194735 * __this, String_t* ___adType0, String_t* ___provider1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppSDKDelegate/AndroidInterstitialListener::onInterstitialClosed(System.String,System.String)
extern "C"  void AndroidInterstitialListener_onInterstitialClosed_m3505100651 (AndroidInterstitialListener_t3321194735 * __this, String_t* ___adType0, String_t* ___provider1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppSDKDelegate/AndroidInterstitialListener::onInterstitialClicked(System.String,System.String)
extern "C"  void AndroidInterstitialListener_onInterstitialClicked_m2762803036 (AndroidInterstitialListener_t3321194735 * __this, String_t* ___adType0, String_t* ___provider1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppSDKDelegate/AndroidInterstitialListener::onFirstInterstitialLoad(System.String,System.String)
extern "C"  void AndroidInterstitialListener_onFirstInterstitialLoad_m915311765 (AndroidInterstitialListener_t3321194735 * __this, String_t* ___adType0, String_t* ___provider1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AdToApp.AdToAppSDKDelegate/AndroidInterstitialListener::onInterstitialFailedToShow(System.String)
extern "C"  bool AndroidInterstitialListener_onInterstitialFailedToShow_m1938861930 (AndroidInterstitialListener_t3321194735 * __this, String_t* ___adContentType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppSDKDelegate/AndroidInterstitialListener::onRewardedCompleted(System.String,System.String,System.String)
extern "C"  void AndroidInterstitialListener_onRewardedCompleted_m586188118 (AndroidInterstitialListener_t3321194735 * __this, String_t* ___adProvider0, String_t* ___currencyName1, String_t* ___currencyValue2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
