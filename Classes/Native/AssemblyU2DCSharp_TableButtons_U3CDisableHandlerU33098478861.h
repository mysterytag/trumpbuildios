﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UpgradeButton
struct UpgradeButton_t1475467342;
// UpgradeCell
struct UpgradeCell_t4117746046;
// TableButtons
struct TableButtons_t1868573683;

#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharp_GlobalStat_UpgradeType4118271830.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TableButtons/<DisableHandler>c__AnonStorey158
struct  U3CDisableHandlerU3Ec__AnonStorey158_t3098478861  : public Il2CppObject
{
public:
	// UpgradeButton TableButtons/<DisableHandler>c__AnonStorey158::button
	UpgradeButton_t1475467342 * ___button_0;
	// GlobalStat/UpgradeType TableButtons/<DisableHandler>c__AnonStorey158::upgradeType
	int32_t ___upgradeType_1;
	// UpgradeCell TableButtons/<DisableHandler>c__AnonStorey158::cell
	UpgradeCell_t4117746046 * ___cell_2;
	// TableButtons TableButtons/<DisableHandler>c__AnonStorey158::<>f__this
	TableButtons_t1868573683 * ___U3CU3Ef__this_3;

public:
	inline static int32_t get_offset_of_button_0() { return static_cast<int32_t>(offsetof(U3CDisableHandlerU3Ec__AnonStorey158_t3098478861, ___button_0)); }
	inline UpgradeButton_t1475467342 * get_button_0() const { return ___button_0; }
	inline UpgradeButton_t1475467342 ** get_address_of_button_0() { return &___button_0; }
	inline void set_button_0(UpgradeButton_t1475467342 * value)
	{
		___button_0 = value;
		Il2CppCodeGenWriteBarrier(&___button_0, value);
	}

	inline static int32_t get_offset_of_upgradeType_1() { return static_cast<int32_t>(offsetof(U3CDisableHandlerU3Ec__AnonStorey158_t3098478861, ___upgradeType_1)); }
	inline int32_t get_upgradeType_1() const { return ___upgradeType_1; }
	inline int32_t* get_address_of_upgradeType_1() { return &___upgradeType_1; }
	inline void set_upgradeType_1(int32_t value)
	{
		___upgradeType_1 = value;
	}

	inline static int32_t get_offset_of_cell_2() { return static_cast<int32_t>(offsetof(U3CDisableHandlerU3Ec__AnonStorey158_t3098478861, ___cell_2)); }
	inline UpgradeCell_t4117746046 * get_cell_2() const { return ___cell_2; }
	inline UpgradeCell_t4117746046 ** get_address_of_cell_2() { return &___cell_2; }
	inline void set_cell_2(UpgradeCell_t4117746046 * value)
	{
		___cell_2 = value;
		Il2CppCodeGenWriteBarrier(&___cell_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_3() { return static_cast<int32_t>(offsetof(U3CDisableHandlerU3Ec__AnonStorey158_t3098478861, ___U3CU3Ef__this_3)); }
	inline TableButtons_t1868573683 * get_U3CU3Ef__this_3() const { return ___U3CU3Ef__this_3; }
	inline TableButtons_t1868573683 ** get_address_of_U3CU3Ef__this_3() { return &___U3CU3Ef__this_3; }
	inline void set_U3CU3Ef__this_3(TableButtons_t1868573683 * value)
	{
		___U3CU3Ef__this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
