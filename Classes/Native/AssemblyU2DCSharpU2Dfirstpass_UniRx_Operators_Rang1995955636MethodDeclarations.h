﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.RangeObservable/<SubscribeCore>c__AnonStorey64
struct U3CSubscribeCoreU3Ec__AnonStorey64_t1995955636;
// System.Action
struct Action_t437523947;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"

// System.Void UniRx.Operators.RangeObservable/<SubscribeCore>c__AnonStorey64::.ctor()
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey64__ctor_m1501246165 (U3CSubscribeCoreU3Ec__AnonStorey64_t1995955636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Operators.RangeObservable/<SubscribeCore>c__AnonStorey64::<>m__81(System.Action)
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey64_U3CU3Em__81_m1584142790 (U3CSubscribeCoreU3Ec__AnonStorey64_t1995955636 * __this, Action_t437523947 * ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
