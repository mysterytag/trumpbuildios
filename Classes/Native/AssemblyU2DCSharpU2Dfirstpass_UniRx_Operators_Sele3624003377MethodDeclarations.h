﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver<System.Double,System.Double,System.Double>
struct SelectManyOuterObserver_t3624003377;
// UniRx.Operators.SelectManyObservable`3<System.Double,System.Double,System.Double>
struct SelectManyObservable_3_t44588665;
// UniRx.IObserver`1<System.Double>
struct IObserver_1_t2746515517;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver<System.Double,System.Double,System.Double>::.ctor(UniRx.Operators.SelectManyObservable`3<TSource,TCollection,TResult>,UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void SelectManyOuterObserver__ctor_m3400274333_gshared (SelectManyOuterObserver_t3624003377 * __this, SelectManyObservable_3_t44588665 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define SelectManyOuterObserver__ctor_m3400274333(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (SelectManyOuterObserver_t3624003377 *, SelectManyObservable_3_t44588665 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SelectManyOuterObserver__ctor_m3400274333_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver<System.Double,System.Double,System.Double>::Run()
extern "C"  Il2CppObject * SelectManyOuterObserver_Run_m3612630088_gshared (SelectManyOuterObserver_t3624003377 * __this, const MethodInfo* method);
#define SelectManyOuterObserver_Run_m3612630088(__this, method) ((  Il2CppObject * (*) (SelectManyOuterObserver_t3624003377 *, const MethodInfo*))SelectManyOuterObserver_Run_m3612630088_gshared)(__this, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver<System.Double,System.Double,System.Double>::OnNext(TSource)
extern "C"  void SelectManyOuterObserver_OnNext_m224988273_gshared (SelectManyOuterObserver_t3624003377 * __this, double ___value0, const MethodInfo* method);
#define SelectManyOuterObserver_OnNext_m224988273(__this, ___value0, method) ((  void (*) (SelectManyOuterObserver_t3624003377 *, double, const MethodInfo*))SelectManyOuterObserver_OnNext_m224988273_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver<System.Double,System.Double,System.Double>::OnError(System.Exception)
extern "C"  void SelectManyOuterObserver_OnError_m2889200283_gshared (SelectManyOuterObserver_t3624003377 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define SelectManyOuterObserver_OnError_m2889200283(__this, ___error0, method) ((  void (*) (SelectManyOuterObserver_t3624003377 *, Exception_t1967233988 *, const MethodInfo*))SelectManyOuterObserver_OnError_m2889200283_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver<System.Double,System.Double,System.Double>::OnCompleted()
extern "C"  void SelectManyOuterObserver_OnCompleted_m106903150_gshared (SelectManyOuterObserver_t3624003377 * __this, const MethodInfo* method);
#define SelectManyOuterObserver_OnCompleted_m106903150(__this, method) ((  void (*) (SelectManyOuterObserver_t3624003377 *, const MethodInfo*))SelectManyOuterObserver_OnCompleted_m106903150_gshared)(__this, method)
