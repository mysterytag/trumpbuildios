﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.DistinctObservable`1<System.Object>
struct DistinctObservable_1_t18876622;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t3161373071;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.DistinctObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
extern "C"  void DistinctObservable_1__ctor_m1443669977_gshared (DistinctObservable_1_t18876622 * __this, Il2CppObject* ___source0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define DistinctObservable_1__ctor_m1443669977(__this, ___source0, ___comparer1, method) ((  void (*) (DistinctObservable_1_t18876622 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))DistinctObservable_1__ctor_m1443669977_gshared)(__this, ___source0, ___comparer1, method)
// System.IDisposable UniRx.Operators.DistinctObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * DistinctObservable_1_SubscribeCore_m2138390918_gshared (DistinctObservable_1_t18876622 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define DistinctObservable_1_SubscribeCore_m2138390918(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (DistinctObservable_1_t18876622 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))DistinctObservable_1_SubscribeCore_m2138390918_gshared)(__this, ___observer0, ___cancel1, method)
