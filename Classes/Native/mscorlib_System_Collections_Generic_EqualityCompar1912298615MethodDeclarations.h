﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.NetworkMessageInfo>
struct DefaultComparer_t1912298615;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_NetworkMessageInfo2574344884.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.NetworkMessageInfo>::.ctor()
extern "C"  void DefaultComparer__ctor_m4137205226_gshared (DefaultComparer_t1912298615 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m4137205226(__this, method) ((  void (*) (DefaultComparer_t1912298615 *, const MethodInfo*))DefaultComparer__ctor_m4137205226_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.NetworkMessageInfo>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2961940169_gshared (DefaultComparer_t1912298615 * __this, NetworkMessageInfo_t2574344884  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m2961940169(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t1912298615 *, NetworkMessageInfo_t2574344884 , const MethodInfo*))DefaultComparer_GetHashCode_m2961940169_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.NetworkMessageInfo>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m488782207_gshared (DefaultComparer_t1912298615 * __this, NetworkMessageInfo_t2574344884  ___x0, NetworkMessageInfo_t2574344884  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m488782207(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t1912298615 *, NetworkMessageInfo_t2574344884 , NetworkMessageInfo_t2574344884 , const MethodInfo*))DefaultComparer_Equals_m488782207_gshared)(__this, ___x0, ___y1, method)
