﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.FirstObservable`1/First_<System.Boolean>
struct First__t3433238553;
// UniRx.Operators.FirstObservable`1<System.Boolean>
struct FirstObservable_1_t2233559755;
// UniRx.IObserver`1<System.Boolean>
struct IObserver_1_t2423004244;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.FirstObservable`1/First_<System.Boolean>::.ctor(UniRx.Operators.FirstObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void First___ctor_m3460627664_gshared (First__t3433238553 * __this, FirstObservable_1_t2233559755 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define First___ctor_m3460627664(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (First__t3433238553 *, FirstObservable_1_t2233559755 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))First___ctor_m3460627664_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.FirstObservable`1/First_<System.Boolean>::OnNext(T)
extern "C"  void First__OnNext_m3056281069_gshared (First__t3433238553 * __this, bool ___value0, const MethodInfo* method);
#define First__OnNext_m3056281069(__this, ___value0, method) ((  void (*) (First__t3433238553 *, bool, const MethodInfo*))First__OnNext_m3056281069_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.FirstObservable`1/First_<System.Boolean>::OnError(System.Exception)
extern "C"  void First__OnError_m633849596_gshared (First__t3433238553 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define First__OnError_m633849596(__this, ___error0, method) ((  void (*) (First__t3433238553 *, Exception_t1967233988 *, const MethodInfo*))First__OnError_m633849596_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.FirstObservable`1/First_<System.Boolean>::OnCompleted()
extern "C"  void First__OnCompleted_m338433103_gshared (First__t3433238553 * __this, const MethodInfo* method);
#define First__OnCompleted_m338433103(__this, method) ((  void (*) (First__t3433238553 *, const MethodInfo*))First__OnCompleted_m338433103_gshared)(__this, method)
