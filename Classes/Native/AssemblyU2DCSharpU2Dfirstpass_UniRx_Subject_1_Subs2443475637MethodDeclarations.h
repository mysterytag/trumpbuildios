﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1/Subscription<UniRx.Tuple`2<System.Object,System.Object>>
struct Subscription_t2443475637;
// UniRx.Subject`1<UniRx.Tuple`2<System.Object,System.Object>>
struct Subject_1_t2307296439;
// UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Object>>
struct IObserver_1_t2581260722;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Subject`1/Subscription<UniRx.Tuple`2<System.Object,System.Object>>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern "C"  void Subscription__ctor_m838108267_gshared (Subscription_t2443475637 * __this, Subject_1_t2307296439 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method);
#define Subscription__ctor_m838108267(__this, ___parent0, ___unsubscribeTarget1, method) ((  void (*) (Subscription_t2443475637 *, Subject_1_t2307296439 *, Il2CppObject*, const MethodInfo*))Subscription__ctor_m838108267_gshared)(__this, ___parent0, ___unsubscribeTarget1, method)
// System.Void UniRx.Subject`1/Subscription<UniRx.Tuple`2<System.Object,System.Object>>::Dispose()
extern "C"  void Subscription_Dispose_m1188617899_gshared (Subscription_t2443475637 * __this, const MethodInfo* method);
#define Subscription_Dispose_m1188617899(__this, method) ((  void (*) (Subscription_t2443475637 *, const MethodInfo*))Subscription_Dispose_m1188617899_gshared)(__this, method)
