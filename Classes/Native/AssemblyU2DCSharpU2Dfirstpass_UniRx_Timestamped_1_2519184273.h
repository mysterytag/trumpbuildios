﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;

#include "mscorlib_System_ValueType4014882752.h"
#include "mscorlib_System_DateTimeOffset3712260035.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Timestamped`1<System.Object>
struct  Timestamped_1_t2519184273 
{
public:
	// System.DateTimeOffset UniRx.Timestamped`1::_timestamp
	DateTimeOffset_t3712260035  ____timestamp_0;
	// T UniRx.Timestamped`1::_value
	Il2CppObject * ____value_1;

public:
	inline static int32_t get_offset_of__timestamp_0() { return static_cast<int32_t>(offsetof(Timestamped_1_t2519184273, ____timestamp_0)); }
	inline DateTimeOffset_t3712260035  get__timestamp_0() const { return ____timestamp_0; }
	inline DateTimeOffset_t3712260035 * get_address_of__timestamp_0() { return &____timestamp_0; }
	inline void set__timestamp_0(DateTimeOffset_t3712260035  value)
	{
		____timestamp_0 = value;
	}

	inline static int32_t get_offset_of__value_1() { return static_cast<int32_t>(offsetof(Timestamped_1_t2519184273, ____value_1)); }
	inline Il2CppObject * get__value_1() const { return ____value_1; }
	inline Il2CppObject ** get_address_of__value_1() { return &____value_1; }
	inline void set__value_1(Il2CppObject * value)
	{
		____value_1 = value;
		Il2CppCodeGenWriteBarrier(&____value_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
