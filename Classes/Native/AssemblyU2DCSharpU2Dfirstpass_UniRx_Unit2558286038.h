﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType4014882752.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Unit
struct  Unit_t2558286038 
{
public:

public:
};

struct Unit_t2558286038_StaticFields
{
public:
	// UniRx.Unit UniRx.Unit::default
	Unit_t2558286038  ___default_0;

public:
	inline static int32_t get_offset_of_default_0() { return static_cast<int32_t>(offsetof(Unit_t2558286038_StaticFields, ___default_0)); }
	inline Unit_t2558286038  get_default_0() const { return ___default_0; }
	inline Unit_t2558286038 * get_address_of_default_0() { return &___default_0; }
	inline void set_default_0(Unit_t2558286038  value)
	{
		___default_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: UniRx.Unit
struct Unit_t2558286038_marshaled_pinvoke
{
};
// Native definition for marshalling of: UniRx.Unit
struct Unit_t2558286038_marshaled_com
{
};
