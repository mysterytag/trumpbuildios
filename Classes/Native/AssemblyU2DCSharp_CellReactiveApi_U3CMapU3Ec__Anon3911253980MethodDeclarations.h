﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/<Map>c__AnonStorey10E`2/<Map>c__AnonStorey10F`2<System.Int32,System.Object>
struct U3CMapU3Ec__AnonStorey10F_2_t3911253980;

#include "codegen/il2cpp-codegen.h"

// System.Void CellReactiveApi/<Map>c__AnonStorey10E`2/<Map>c__AnonStorey10F`2<System.Int32,System.Object>::.ctor()
extern "C"  void U3CMapU3Ec__AnonStorey10F_2__ctor_m3611507434_gshared (U3CMapU3Ec__AnonStorey10F_2_t3911253980 * __this, const MethodInfo* method);
#define U3CMapU3Ec__AnonStorey10F_2__ctor_m3611507434(__this, method) ((  void (*) (U3CMapU3Ec__AnonStorey10F_2_t3911253980 *, const MethodInfo*))U3CMapU3Ec__AnonStorey10F_2__ctor_m3611507434_gshared)(__this, method)
// System.Void CellReactiveApi/<Map>c__AnonStorey10E`2/<Map>c__AnonStorey10F`2<System.Int32,System.Object>::<>m__197(T)
extern "C"  void U3CMapU3Ec__AnonStorey10F_2_U3CU3Em__197_m690658496_gshared (U3CMapU3Ec__AnonStorey10F_2_t3911253980 * __this, int32_t ___val0, const MethodInfo* method);
#define U3CMapU3Ec__AnonStorey10F_2_U3CU3Em__197_m690658496(__this, ___val0, method) ((  void (*) (U3CMapU3Ec__AnonStorey10F_2_t3911253980 *, int32_t, const MethodInfo*))U3CMapU3Ec__AnonStorey10F_2_U3CU3Em__197_m690658496_gshared)(__this, ___val0, method)
