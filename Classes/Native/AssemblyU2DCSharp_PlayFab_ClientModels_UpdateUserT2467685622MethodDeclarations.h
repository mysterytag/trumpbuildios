﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.UpdateUserTitleDisplayNameRequest
struct UpdateUserTitleDisplayNameRequest_t2467685622;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.UpdateUserTitleDisplayNameRequest::.ctor()
extern "C"  void UpdateUserTitleDisplayNameRequest__ctor_m3372101107 (UpdateUserTitleDisplayNameRequest_t2467685622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.UpdateUserTitleDisplayNameRequest::get_DisplayName()
extern "C"  String_t* UpdateUserTitleDisplayNameRequest_get_DisplayName_m2798290514 (UpdateUserTitleDisplayNameRequest_t2467685622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UpdateUserTitleDisplayNameRequest::set_DisplayName(System.String)
extern "C"  void UpdateUserTitleDisplayNameRequest_set_DisplayName_m1970135937 (UpdateUserTitleDisplayNameRequest_t2467685622 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
