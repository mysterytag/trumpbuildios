﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<AddUsernamePassword>c__AnonStorey6C
struct U3CAddUsernamePasswordU3Ec__AnonStorey6C_t1415463180;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<AddUsernamePassword>c__AnonStorey6C::.ctor()
extern "C"  void U3CAddUsernamePasswordU3Ec__AnonStorey6C__ctor_m2317738951 (U3CAddUsernamePasswordU3Ec__AnonStorey6C_t1415463180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<AddUsernamePassword>c__AnonStorey6C::<>m__59(PlayFab.CallRequestContainer)
extern "C"  void U3CAddUsernamePasswordU3Ec__AnonStorey6C_U3CU3Em__59_m2499226633 (U3CAddUsernamePasswordU3Ec__AnonStorey6C_t1415463180 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
