﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Material
struct Material_t1886596500;
// MaterialContainer
struct MaterialContainer_t3323339514;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MaterialContainer
struct  MaterialContainer_t3323339514  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.Material MaterialContainer::red_font
	Material_t1886596500 * ___red_font_2;
	// UnityEngine.Material MaterialContainer::white_font
	Material_t1886596500 * ___white_font_3;

public:
	inline static int32_t get_offset_of_red_font_2() { return static_cast<int32_t>(offsetof(MaterialContainer_t3323339514, ___red_font_2)); }
	inline Material_t1886596500 * get_red_font_2() const { return ___red_font_2; }
	inline Material_t1886596500 ** get_address_of_red_font_2() { return &___red_font_2; }
	inline void set_red_font_2(Material_t1886596500 * value)
	{
		___red_font_2 = value;
		Il2CppCodeGenWriteBarrier(&___red_font_2, value);
	}

	inline static int32_t get_offset_of_white_font_3() { return static_cast<int32_t>(offsetof(MaterialContainer_t3323339514, ___white_font_3)); }
	inline Material_t1886596500 * get_white_font_3() const { return ___white_font_3; }
	inline Material_t1886596500 ** get_address_of_white_font_3() { return &___white_font_3; }
	inline void set_white_font_3(Material_t1886596500 * value)
	{
		___white_font_3 = value;
		Il2CppCodeGenWriteBarrier(&___white_font_3, value);
	}
};

struct MaterialContainer_t3323339514_StaticFields
{
public:
	// MaterialContainer MaterialContainer::_instance
	MaterialContainer_t3323339514 * ____instance_4;

public:
	inline static int32_t get_offset_of__instance_4() { return static_cast<int32_t>(offsetof(MaterialContainer_t3323339514_StaticFields, ____instance_4)); }
	inline MaterialContainer_t3323339514 * get__instance_4() const { return ____instance_4; }
	inline MaterialContainer_t3323339514 ** get_address_of__instance_4() { return &____instance_4; }
	inline void set__instance_4(MaterialContainer_t3323339514 * value)
	{
		____instance_4 = value;
		Il2CppCodeGenWriteBarrier(&____instance_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
