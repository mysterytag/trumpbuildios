﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GlobalStat/<PostInject>c__AnonStoreyE2
struct U3CPostInjectU3Ec__AnonStoreyE2_t980490755;

#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_DateTime339033936.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GlobalStat/<PostInject>c__AnonStoreyE2/<PostInject>c__AnonStoreyE4
struct  U3CPostInjectU3Ec__AnonStoreyE4_t980490757  : public Il2CppObject
{
public:
	// System.DateTime GlobalStat/<PostInject>c__AnonStoreyE2/<PostInject>c__AnonStoreyE4::date
	DateTime_t339033936  ___date_0;
	// GlobalStat/<PostInject>c__AnonStoreyE2 GlobalStat/<PostInject>c__AnonStoreyE2/<PostInject>c__AnonStoreyE4::<>f__ref$226
	U3CPostInjectU3Ec__AnonStoreyE2_t980490755 * ___U3CU3Ef__refU24226_1;

public:
	inline static int32_t get_offset_of_date_0() { return static_cast<int32_t>(offsetof(U3CPostInjectU3Ec__AnonStoreyE4_t980490757, ___date_0)); }
	inline DateTime_t339033936  get_date_0() const { return ___date_0; }
	inline DateTime_t339033936 * get_address_of_date_0() { return &___date_0; }
	inline void set_date_0(DateTime_t339033936  value)
	{
		___date_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24226_1() { return static_cast<int32_t>(offsetof(U3CPostInjectU3Ec__AnonStoreyE4_t980490757, ___U3CU3Ef__refU24226_1)); }
	inline U3CPostInjectU3Ec__AnonStoreyE2_t980490755 * get_U3CU3Ef__refU24226_1() const { return ___U3CU3Ef__refU24226_1; }
	inline U3CPostInjectU3Ec__AnonStoreyE2_t980490755 ** get_address_of_U3CU3Ef__refU24226_1() { return &___U3CU3Ef__refU24226_1; }
	inline void set_U3CU3Ef__refU24226_1(U3CPostInjectU3Ec__AnonStoreyE2_t980490755 * value)
	{
		___U3CU3Ef__refU24226_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24226_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
