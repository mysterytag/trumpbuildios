﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/<ToAsync>c__AnonStorey38`1/<ToAsync>c__AnonStorey39`1<System.Object>
struct U3CToAsyncU3Ec__AnonStorey39_1_t776464662;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Observable/<ToAsync>c__AnonStorey38`1/<ToAsync>c__AnonStorey39`1<System.Object>::.ctor()
extern "C"  void U3CToAsyncU3Ec__AnonStorey39_1__ctor_m4082454551_gshared (U3CToAsyncU3Ec__AnonStorey39_1_t776464662 * __this, const MethodInfo* method);
#define U3CToAsyncU3Ec__AnonStorey39_1__ctor_m4082454551(__this, method) ((  void (*) (U3CToAsyncU3Ec__AnonStorey39_1_t776464662 *, const MethodInfo*))U3CToAsyncU3Ec__AnonStorey39_1__ctor_m4082454551_gshared)(__this, method)
// System.Void UniRx.Observable/<ToAsync>c__AnonStorey38`1/<ToAsync>c__AnonStorey39`1<System.Object>::<>m__62()
extern "C"  void U3CToAsyncU3Ec__AnonStorey39_1_U3CU3Em__62_m4241873116_gshared (U3CToAsyncU3Ec__AnonStorey39_1_t776464662 * __this, const MethodInfo* method);
#define U3CToAsyncU3Ec__AnonStorey39_1_U3CU3Em__62_m4241873116(__this, method) ((  void (*) (U3CToAsyncU3Ec__AnonStorey39_1_t776464662 *, const MethodInfo*))U3CToAsyncU3Ec__AnonStorey39_1_U3CU3Em__62_m4241873116_gshared)(__this, method)
