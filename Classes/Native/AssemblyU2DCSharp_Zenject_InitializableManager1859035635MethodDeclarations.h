﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.InitializableManager
struct InitializableManager_t1859035635;
// System.Collections.Generic.List`1<Zenject.IInitializable>
struct List_1_t1414850804;
// System.Collections.Generic.List`1<ModestTree.Util.Tuple`2<System.Type,System.Int32>>
struct List_1_t221540724;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// Zenject.SingletonInstanceHelper
struct SingletonInstanceHelper_t833281251;
// ModestTree.Util.Tuple`2<System.Type,System.Int32>
struct Tuple_2_t3719549051;
// System.Type
struct Type_t;
// Zenject.IInitializable
struct IInitializable_t617891835;
// Zenject.InitializableManager/InitializableInfo
struct InitializableInfo_t3830632317;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449.h"
#include "AssemblyU2DCSharp_Zenject_SingletonInstanceHelper833281251.h"
#include "AssemblyU2DCSharp_Zenject_InitializableManager_Ini3830632317.h"

// System.Void Zenject.InitializableManager::.ctor(System.Collections.Generic.List`1<Zenject.IInitializable>,System.Collections.Generic.List`1<ModestTree.Util.Tuple`2<System.Type,System.Int32>>,Zenject.DiContainer,Zenject.SingletonInstanceHelper)
extern "C"  void InitializableManager__ctor_m2396314231 (InitializableManager_t1859035635 * __this, List_1_t1414850804 * ___initializables0, List_1_t221540724 * ___priorities1, DiContainer_t2383114449 * ___container2, SingletonInstanceHelper_t833281251 * ___singletonInstanceHelper3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.InitializableManager::WarnForMissingBindings(System.Collections.Generic.List`1<Zenject.IInitializable>,Zenject.DiContainer)
extern "C"  void InitializableManager_WarnForMissingBindings_m1301992527 (InitializableManager_t1859035635 * __this, List_1_t1414850804 * ___initializables0, DiContainer_t2383114449 * ___container1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.InitializableManager::Initialize()
extern "C"  void InitializableManager_Initialize_m3971178806 (InitializableManager_t1859035635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Zenject.InitializableManager::<InitializableManager>m__2DB(ModestTree.Util.Tuple`2<System.Type,System.Int32>)
extern "C"  int32_t InitializableManager_U3CInitializableManagerU3Em__2DB_m3306730877 (Il2CppObject * __this /* static, unused */, Tuple_2_t3719549051 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Zenject.InitializableManager::<WarnForMissingBindings>m__2DC(Zenject.IInitializable)
extern "C"  Type_t * InitializableManager_U3CWarnForMissingBindingsU3Em__2DC_m1201165104 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Zenject.InitializableManager::<Initialize>m__2DD(Zenject.InitializableManager/InitializableInfo)
extern "C"  int32_t InitializableManager_U3CInitializeU3Em__2DD_m1187395968 (Il2CppObject * __this /* static, unused */, InitializableInfo_t3830632317 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.IInitializable Zenject.InitializableManager::<Initialize>m__2DE(Zenject.InitializableManager/InitializableInfo)
extern "C"  Il2CppObject * InitializableManager_U3CInitializeU3Em__2DE_m127041963 (Il2CppObject * __this /* static, unused */, InitializableInfo_t3830632317 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
