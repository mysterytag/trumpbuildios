﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1/Subscription<UnityEngine.Rect>
struct Subscription_t3599642635;
// UniRx.Subject`1<UnityEngine.Rect>
struct Subject_1_t3463463437;
// UniRx.IObserver`1<UnityEngine.Rect>
struct IObserver_1_t3737427720;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Subject`1/Subscription<UnityEngine.Rect>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern "C"  void Subscription__ctor_m2757842008_gshared (Subscription_t3599642635 * __this, Subject_1_t3463463437 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method);
#define Subscription__ctor_m2757842008(__this, ___parent0, ___unsubscribeTarget1, method) ((  void (*) (Subscription_t3599642635 *, Subject_1_t3463463437 *, Il2CppObject*, const MethodInfo*))Subscription__ctor_m2757842008_gshared)(__this, ___parent0, ___unsubscribeTarget1, method)
// System.Void UniRx.Subject`1/Subscription<UnityEngine.Rect>::Dispose()
extern "C"  void Subscription_Dispose_m771725982_gshared (Subscription_t3599642635 * __this, const MethodInfo* method);
#define Subscription_Dispose_m771725982(__this, method) ((  void (*) (Subscription_t3599642635 *, const MethodInfo*))Subscription_Dispose_m771725982_gshared)(__this, method)
