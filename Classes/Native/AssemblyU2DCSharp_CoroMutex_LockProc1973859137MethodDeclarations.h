﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CoroMutex/LockProc
struct LockProc_t1973859137;

#include "codegen/il2cpp-codegen.h"

// System.Void CoroMutex/LockProc::.ctor()
extern "C"  void LockProc__ctor_m276339681 (LockProc_t1973859137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CoroMutex/LockProc::get_finished()
extern "C"  bool LockProc_get_finished_m1426720082 (LockProc_t1973859137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CoroMutex/LockProc::Tick(System.Single)
extern "C"  void LockProc_Tick_m3431953931 (LockProc_t1973859137 * __this, float ___dt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CoroMutex/LockProc::Dispose()
extern "C"  void LockProc_Dispose_m3467711454 (LockProc_t1973859137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
