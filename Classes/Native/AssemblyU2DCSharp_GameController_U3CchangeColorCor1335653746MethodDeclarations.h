﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameController/<changeColorCoroutine>c__IteratorF
struct U3CchangeColorCoroutineU3Ec__IteratorF_t1335653746;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GameController/<changeColorCoroutine>c__IteratorF::.ctor()
extern "C"  void U3CchangeColorCoroutineU3Ec__IteratorF__ctor_m2692039928 (U3CchangeColorCoroutineU3Ec__IteratorF_t1335653746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameController/<changeColorCoroutine>c__IteratorF::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CchangeColorCoroutineU3Ec__IteratorF_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m446166170 (U3CchangeColorCoroutineU3Ec__IteratorF_t1335653746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameController/<changeColorCoroutine>c__IteratorF::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CchangeColorCoroutineU3Ec__IteratorF_System_Collections_IEnumerator_get_Current_m1600810030 (U3CchangeColorCoroutineU3Ec__IteratorF_t1335653746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameController/<changeColorCoroutine>c__IteratorF::MoveNext()
extern "C"  bool U3CchangeColorCoroutineU3Ec__IteratorF_MoveNext_m2236676860 (U3CchangeColorCoroutineU3Ec__IteratorF_t1335653746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController/<changeColorCoroutine>c__IteratorF::Dispose()
extern "C"  void U3CchangeColorCoroutineU3Ec__IteratorF_Dispose_m1378341685 (U3CchangeColorCoroutineU3Ec__IteratorF_t1335653746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController/<changeColorCoroutine>c__IteratorF::Reset()
extern "C"  void U3CchangeColorCoroutineU3Ec__IteratorF_Reset_m338472869 (U3CchangeColorCoroutineU3Ec__IteratorF_t1335653746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
