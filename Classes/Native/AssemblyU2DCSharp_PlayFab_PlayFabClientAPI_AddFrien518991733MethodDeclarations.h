﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/AddFriendRequestCallback
struct AddFriendRequestCallback_t518991733;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.AddFriendRequest
struct AddFriendRequest_t1861404448;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_AddFriendRe1861404448.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/AddFriendRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void AddFriendRequestCallback__ctor_m3672312292 (AddFriendRequestCallback_t518991733 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/AddFriendRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.AddFriendRequest,System.Object)
extern "C"  void AddFriendRequestCallback_Invoke_m1345761599 (AddFriendRequestCallback_t518991733 * __this, String_t* ___urlPath0, int32_t ___callId1, AddFriendRequest_t1861404448 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_AddFriendRequestCallback_t518991733(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, AddFriendRequest_t1861404448 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/AddFriendRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.AddFriendRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * AddFriendRequestCallback_BeginInvoke_m1906358416 (AddFriendRequestCallback_t518991733 * __this, String_t* ___urlPath0, int32_t ___callId1, AddFriendRequest_t1861404448 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/AddFriendRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void AddFriendRequestCallback_EndInvoke_m1945046516 (AddFriendRequestCallback_t518991733 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
