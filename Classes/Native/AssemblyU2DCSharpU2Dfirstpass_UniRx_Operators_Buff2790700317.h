﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.BufferObservable`1/BufferTS<System.Object>
struct BufferTS_t71212032;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.BufferObservable`1/BufferTS/<CreateTimer>c__AnonStorey5C<System.Object>
struct  U3CCreateTimerU3Ec__AnonStorey5C_t2790700317  : public Il2CppObject
{
public:
	// System.Boolean UniRx.Operators.BufferObservable`1/BufferTS/<CreateTimer>c__AnonStorey5C::isShift
	bool ___isShift_0;
	// System.Boolean UniRx.Operators.BufferObservable`1/BufferTS/<CreateTimer>c__AnonStorey5C::isSpan
	bool ___isSpan_1;
	// UniRx.Operators.BufferObservable`1/BufferTS<T> UniRx.Operators.BufferObservable`1/BufferTS/<CreateTimer>c__AnonStorey5C::<>f__this
	BufferTS_t71212032 * ___U3CU3Ef__this_2;

public:
	inline static int32_t get_offset_of_isShift_0() { return static_cast<int32_t>(offsetof(U3CCreateTimerU3Ec__AnonStorey5C_t2790700317, ___isShift_0)); }
	inline bool get_isShift_0() const { return ___isShift_0; }
	inline bool* get_address_of_isShift_0() { return &___isShift_0; }
	inline void set_isShift_0(bool value)
	{
		___isShift_0 = value;
	}

	inline static int32_t get_offset_of_isSpan_1() { return static_cast<int32_t>(offsetof(U3CCreateTimerU3Ec__AnonStorey5C_t2790700317, ___isSpan_1)); }
	inline bool get_isSpan_1() const { return ___isSpan_1; }
	inline bool* get_address_of_isSpan_1() { return &___isSpan_1; }
	inline void set_isSpan_1(bool value)
	{
		___isSpan_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_2() { return static_cast<int32_t>(offsetof(U3CCreateTimerU3Ec__AnonStorey5C_t2790700317, ___U3CU3Ef__this_2)); }
	inline BufferTS_t71212032 * get_U3CU3Ef__this_2() const { return ___U3CU3Ef__this_2; }
	inline BufferTS_t71212032 ** get_address_of_U3CU3Ef__this_2() { return &___U3CU3Ef__this_2; }
	inline void set_U3CU3Ef__this_2(BufferTS_t71212032 * value)
	{
		___U3CU3Ef__this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
