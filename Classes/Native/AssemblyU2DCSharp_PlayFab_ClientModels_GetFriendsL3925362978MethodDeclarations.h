﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetFriendsListRequest
struct GetFriendsListRequest_t3925362978;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen3097043249.h"

// System.Void PlayFab.ClientModels.GetFriendsListRequest::.ctor()
extern "C"  void GetFriendsListRequest__ctor_m160951367 (GetFriendsListRequest_t3925362978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.GetFriendsListRequest::get_IncludeSteamFriends()
extern "C"  Nullable_1_t3097043249  GetFriendsListRequest_get_IncludeSteamFriends_m2702834496 (GetFriendsListRequest_t3925362978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetFriendsListRequest::set_IncludeSteamFriends(System.Nullable`1<System.Boolean>)
extern "C"  void GetFriendsListRequest_set_IncludeSteamFriends_m3396397779 (GetFriendsListRequest_t3925362978 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.GetFriendsListRequest::get_IncludeFacebookFriends()
extern "C"  Nullable_1_t3097043249  GetFriendsListRequest_get_IncludeFacebookFriends_m1364088214 (GetFriendsListRequest_t3925362978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetFriendsListRequest::set_IncludeFacebookFriends(System.Nullable`1<System.Boolean>)
extern "C"  void GetFriendsListRequest_set_IncludeFacebookFriends_m1590002567 (GetFriendsListRequest_t3925362978 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
