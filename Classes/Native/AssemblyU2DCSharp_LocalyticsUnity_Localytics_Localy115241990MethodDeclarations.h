﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LocalyticsUnity.Localytics/LocalyticsSessionDidOpen
struct LocalyticsSessionDidOpen_t115241990;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void LocalyticsUnity.Localytics/LocalyticsSessionDidOpen::.ctor(System.Object,System.IntPtr)
extern "C"  void LocalyticsSessionDidOpen__ctor_m683175501 (LocalyticsSessionDidOpen_t115241990 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics/LocalyticsSessionDidOpen::Invoke(System.Boolean,System.Boolean,System.Boolean)
extern "C"  void LocalyticsSessionDidOpen_Invoke_m2858247166 (LocalyticsSessionDidOpen_t115241990 * __this, bool ___isFirst0, bool ___isUpgrade1, bool ___isResume2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_LocalyticsSessionDidOpen_t115241990(Il2CppObject* delegate, bool ___isFirst0, bool ___isUpgrade1, bool ___isResume2);
// System.IAsyncResult LocalyticsUnity.Localytics/LocalyticsSessionDidOpen::BeginInvoke(System.Boolean,System.Boolean,System.Boolean,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LocalyticsSessionDidOpen_BeginInvoke_m521734563 (LocalyticsSessionDidOpen_t115241990 * __this, bool ___isFirst0, bool ___isUpgrade1, bool ___isResume2, AsyncCallback_t1363551830 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics/LocalyticsSessionDidOpen::EndInvoke(System.IAsyncResult)
extern "C"  void LocalyticsSessionDidOpen_EndInvoke_m2247167709 (LocalyticsSessionDidOpen_t115241990 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
