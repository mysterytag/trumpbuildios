﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.PresenterBase`1<UniRx.Unit>
struct PresenterBase_1_t3949187127;
// UniRx.IPresenter
struct IPresenter_t2177085329;
// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;
// UnityEngine.GameObject
struct GameObject_t4012695102;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

// System.Void UniRx.PresenterBase`1<UniRx.Unit>::.ctor()
extern "C"  void PresenterBase_1__ctor_m304455231_gshared (PresenterBase_1_t3949187127 * __this, const MethodInfo* method);
#define PresenterBase_1__ctor_m304455231(__this, method) ((  void (*) (PresenterBase_1_t3949187127 *, const MethodInfo*))PresenterBase_1__ctor_m304455231_gshared)(__this, method)
// System.Void UniRx.PresenterBase`1<UniRx.Unit>::.cctor()
extern "C"  void PresenterBase_1__cctor_m366081358_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define PresenterBase_1__cctor_m366081358(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))PresenterBase_1__cctor_m366081358_gshared)(__this /* static, unused */, method)
// System.Void UniRx.PresenterBase`1<UniRx.Unit>::UniRx.IPresenter.Awake()
extern "C"  void PresenterBase_1_UniRx_IPresenter_Awake_m975050067_gshared (PresenterBase_1_t3949187127 * __this, const MethodInfo* method);
#define PresenterBase_1_UniRx_IPresenter_Awake_m975050067(__this, method) ((  void (*) (PresenterBase_1_t3949187127 *, const MethodInfo*))PresenterBase_1_UniRx_IPresenter_Awake_m975050067_gshared)(__this, method)
// System.Void UniRx.PresenterBase`1<UniRx.Unit>::UniRx.IPresenter.StartCapturePhase()
extern "C"  void PresenterBase_1_UniRx_IPresenter_StartCapturePhase_m1669220133_gshared (PresenterBase_1_t3949187127 * __this, const MethodInfo* method);
#define PresenterBase_1_UniRx_IPresenter_StartCapturePhase_m1669220133(__this, method) ((  void (*) (PresenterBase_1_t3949187127 *, const MethodInfo*))PresenterBase_1_UniRx_IPresenter_StartCapturePhase_m1669220133_gshared)(__this, method)
// System.Void UniRx.PresenterBase`1<UniRx.Unit>::UniRx.IPresenter.RegisterParent(UniRx.IPresenter)
extern "C"  void PresenterBase_1_UniRx_IPresenter_RegisterParent_m2060277322_gshared (PresenterBase_1_t3949187127 * __this, Il2CppObject * ___parent0, const MethodInfo* method);
#define PresenterBase_1_UniRx_IPresenter_RegisterParent_m2060277322(__this, ___parent0, method) ((  void (*) (PresenterBase_1_t3949187127 *, Il2CppObject *, const MethodInfo*))PresenterBase_1_UniRx_IPresenter_RegisterParent_m2060277322_gshared)(__this, ___parent0, method)
// System.Void UniRx.PresenterBase`1<UniRx.Unit>::UniRx.IPresenter.InitializeCore()
extern "C"  void PresenterBase_1_UniRx_IPresenter_InitializeCore_m3034738467_gshared (PresenterBase_1_t3949187127 * __this, const MethodInfo* method);
#define PresenterBase_1_UniRx_IPresenter_InitializeCore_m3034738467(__this, method) ((  void (*) (PresenterBase_1_t3949187127 *, const MethodInfo*))PresenterBase_1_UniRx_IPresenter_InitializeCore_m3034738467_gshared)(__this, method)
// UniRx.IPresenter UniRx.PresenterBase`1<UniRx.Unit>::get_Parent()
extern "C"  Il2CppObject * PresenterBase_1_get_Parent_m3916143850_gshared (PresenterBase_1_t3949187127 * __this, const MethodInfo* method);
#define PresenterBase_1_get_Parent_m3916143850(__this, method) ((  Il2CppObject * (*) (PresenterBase_1_t3949187127 *, const MethodInfo*))PresenterBase_1_get_Parent_m3916143850_gshared)(__this, method)
// UniRx.IObservable`1<UniRx.Unit> UniRx.PresenterBase`1<UniRx.Unit>::InitializeAsObservable()
extern "C"  Il2CppObject* PresenterBase_1_InitializeAsObservable_m3938054882_gshared (PresenterBase_1_t3949187127 * __this, const MethodInfo* method);
#define PresenterBase_1_InitializeAsObservable_m3938054882(__this, method) ((  Il2CppObject* (*) (PresenterBase_1_t3949187127 *, const MethodInfo*))PresenterBase_1_InitializeAsObservable_m3938054882_gshared)(__this, method)
// System.Void UniRx.PresenterBase`1<UniRx.Unit>::PropagateArgument(T)
extern "C"  void PresenterBase_1_PropagateArgument_m2892968155_gshared (PresenterBase_1_t3949187127 * __this, Unit_t2558286038  ___argument0, const MethodInfo* method);
#define PresenterBase_1_PropagateArgument_m2892968155(__this, ___argument0, method) ((  void (*) (PresenterBase_1_t3949187127 *, Unit_t2558286038 , const MethodInfo*))PresenterBase_1_PropagateArgument_m2892968155_gshared)(__this, ___argument0, method)
// System.Void UniRx.PresenterBase`1<UniRx.Unit>::ForceInitialize(T)
extern "C"  void PresenterBase_1_ForceInitialize_m2917668326_gshared (PresenterBase_1_t3949187127 * __this, Unit_t2558286038  ___argument0, const MethodInfo* method);
#define PresenterBase_1_ForceInitialize_m2917668326(__this, ___argument0, method) ((  void (*) (PresenterBase_1_t3949187127 *, Unit_t2558286038 , const MethodInfo*))PresenterBase_1_ForceInitialize_m2917668326_gshared)(__this, ___argument0, method)
// System.Void UniRx.PresenterBase`1<UniRx.Unit>::Awake()
extern "C"  void PresenterBase_1_Awake_m542060450_gshared (PresenterBase_1_t3949187127 * __this, const MethodInfo* method);
#define PresenterBase_1_Awake_m542060450(__this, method) ((  void (*) (PresenterBase_1_t3949187127 *, const MethodInfo*))PresenterBase_1_Awake_m542060450_gshared)(__this, method)
// System.Void UniRx.PresenterBase`1<UniRx.Unit>::OnAwake()
extern "C"  void PresenterBase_1_OnAwake_m900663747_gshared (PresenterBase_1_t3949187127 * __this, const MethodInfo* method);
#define PresenterBase_1_OnAwake_m900663747(__this, method) ((  void (*) (PresenterBase_1_t3949187127 *, const MethodInfo*))PresenterBase_1_OnAwake_m900663747_gshared)(__this, method)
// System.Void UniRx.PresenterBase`1<UniRx.Unit>::Start()
extern "C"  void PresenterBase_1_Start_m3546560319_gshared (PresenterBase_1_t3949187127 * __this, const MethodInfo* method);
#define PresenterBase_1_Start_m3546560319(__this, method) ((  void (*) (PresenterBase_1_t3949187127 *, const MethodInfo*))PresenterBase_1_Start_m3546560319_gshared)(__this, method)
// UnityEngine.GameObject UniRx.PresenterBase`1<UniRx.Unit>::UniRx.IPresenter.get_gameObject()
extern "C"  GameObject_t4012695102 * PresenterBase_1_UniRx_IPresenter_get_gameObject_m1235187307_gshared (PresenterBase_1_t3949187127 * __this, const MethodInfo* method);
#define PresenterBase_1_UniRx_IPresenter_get_gameObject_m1235187307(__this, method) ((  GameObject_t4012695102 * (*) (PresenterBase_1_t3949187127 *, const MethodInfo*))PresenterBase_1_UniRx_IPresenter_get_gameObject_m1235187307_gshared)(__this, method)
