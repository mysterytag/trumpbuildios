﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ForEachAsyncObservable`1/ForEachAsync<System.Object>
struct ForEachAsync_t4111581179;
// UniRx.Operators.ForEachAsyncObservable`1<System.Object>
struct ForEachAsyncObservable_1_t3176723348;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.ForEachAsyncObservable`1/ForEachAsync<System.Object>::.ctor(UniRx.Operators.ForEachAsyncObservable`1<T>,UniRx.IObserver`1<UniRx.Unit>,System.IDisposable)
extern "C"  void ForEachAsync__ctor_m2207845852_gshared (ForEachAsync_t4111581179 * __this, ForEachAsyncObservable_1_t3176723348 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define ForEachAsync__ctor_m2207845852(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (ForEachAsync_t4111581179 *, ForEachAsyncObservable_1_t3176723348 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))ForEachAsync__ctor_m2207845852_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.ForEachAsyncObservable`1/ForEachAsync<System.Object>::OnNext(T)
extern "C"  void ForEachAsync_OnNext_m3590576415_gshared (ForEachAsync_t4111581179 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ForEachAsync_OnNext_m3590576415(__this, ___value0, method) ((  void (*) (ForEachAsync_t4111581179 *, Il2CppObject *, const MethodInfo*))ForEachAsync_OnNext_m3590576415_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.ForEachAsyncObservable`1/ForEachAsync<System.Object>::OnError(System.Exception)
extern "C"  void ForEachAsync_OnError_m487052846_gshared (ForEachAsync_t4111581179 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ForEachAsync_OnError_m487052846(__this, ___error0, method) ((  void (*) (ForEachAsync_t4111581179 *, Exception_t1967233988 *, const MethodInfo*))ForEachAsync_OnError_m487052846_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.ForEachAsyncObservable`1/ForEachAsync<System.Object>::OnCompleted()
extern "C"  void ForEachAsync_OnCompleted_m1697898113_gshared (ForEachAsync_t4111581179 * __this, const MethodInfo* method);
#define ForEachAsync_OnCompleted_m1697898113(__this, method) ((  void (*) (ForEachAsync_t4111581179 *, const MethodInfo*))ForEachAsync_OnCompleted_m1697898113_gshared)(__this, method)
