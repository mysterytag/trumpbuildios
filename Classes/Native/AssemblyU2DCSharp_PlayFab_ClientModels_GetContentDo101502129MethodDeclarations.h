﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetContentDownloadUrlResult
struct GetContentDownloadUrlResult_t101502129;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.GetContentDownloadUrlResult::.ctor()
extern "C"  void GetContentDownloadUrlResult__ctor_m920301848 (GetContentDownloadUrlResult_t101502129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GetContentDownloadUrlResult::get_URL()
extern "C"  String_t* GetContentDownloadUrlResult_get_URL_m2591169657 (GetContentDownloadUrlResult_t101502129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetContentDownloadUrlResult::set_URL(System.String)
extern "C"  void GetContentDownloadUrlResult_set_URL_m1443479418 (GetContentDownloadUrlResult_t101502129 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
