﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XPath.XPathFunctionLast
struct XPathFunctionLast_t1855278280;
// System.Xml.XPath.FunctionArguments
struct FunctionArguments_t3402201403;
// System.Object
struct Il2CppObject;
// System.Xml.XPath.BaseIterator
struct BaseIterator_t3696600956;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XPath_FunctionArguments3402201403.h"
#include "System_Xml_System_Xml_XPath_XPathResultType370286193.h"
#include "System_Xml_System_Xml_XPath_BaseIterator3696600956.h"

// System.Void System.Xml.XPath.XPathFunctionLast::.ctor(System.Xml.XPath.FunctionArguments)
extern "C"  void XPathFunctionLast__ctor_m565310986 (XPathFunctionLast_t1855278280 * __this, FunctionArguments_t3402201403 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionLast::get_ReturnType()
extern "C"  int32_t XPathFunctionLast_get_ReturnType_m3983301210 (XPathFunctionLast_t1855278280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.XPathFunctionLast::get_Peer()
extern "C"  bool XPathFunctionLast_get_Peer_m1259485142 (XPathFunctionLast_t1855278280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Xml.XPath.XPathFunctionLast::Evaluate(System.Xml.XPath.BaseIterator)
extern "C"  Il2CppObject * XPathFunctionLast_Evaluate_m380434779 (XPathFunctionLast_t1855278280 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XPath.XPathFunctionLast::ToString()
extern "C"  String_t* XPathFunctionLast_ToString_m3968188594 (XPathFunctionLast_t1855278280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
