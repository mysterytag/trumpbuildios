﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.DisposableExtensions/<AddTo>c__AnonStorey36`1<System.Object>
struct U3CAddToU3Ec__AnonStorey36_1_t595347342;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

// System.Void UniRx.DisposableExtensions/<AddTo>c__AnonStorey36`1<System.Object>::.ctor()
extern "C"  void U3CAddToU3Ec__AnonStorey36_1__ctor_m3746277587_gshared (U3CAddToU3Ec__AnonStorey36_1_t595347342 * __this, const MethodInfo* method);
#define U3CAddToU3Ec__AnonStorey36_1__ctor_m3746277587(__this, method) ((  void (*) (U3CAddToU3Ec__AnonStorey36_1_t595347342 *, const MethodInfo*))U3CAddToU3Ec__AnonStorey36_1__ctor_m3746277587_gshared)(__this, method)
// System.Void UniRx.DisposableExtensions/<AddTo>c__AnonStorey36`1<System.Object>::<>m__3A(UniRx.Unit)
extern "C"  void U3CAddToU3Ec__AnonStorey36_1_U3CU3Em__3A_m2546724526_gshared (U3CAddToU3Ec__AnonStorey36_1_t595347342 * __this, Unit_t2558286038  ____0, const MethodInfo* method);
#define U3CAddToU3Ec__AnonStorey36_1_U3CU3Em__3A_m2546724526(__this, ____0, method) ((  void (*) (U3CAddToU3Ec__AnonStorey36_1_t595347342 *, Unit_t2558286038 , const MethodInfo*))U3CAddToU3Ec__AnonStorey36_1_U3CU3Em__3A_m2546724526_gshared)(__this, ____0, method)
