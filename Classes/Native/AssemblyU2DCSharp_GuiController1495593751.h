﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Subject`1<UniRx.Unit>
struct Subject_1_t201353362;
// BottomButton
struct BottomButton_t1932555613;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// Tacticsoft.TableView
struct TableView_t692333993;
// TableButtons
struct TableButtons_t1868573683;
// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;
// UniRx.ReactiveProperty`1<System.Boolean>
struct ReactiveProperty_1_t819338379;
// WindowManager
struct WindowManager_t3316821373;
// TutorialPlayer
struct TutorialPlayer_t4281139199;
// BottomPanel
struct BottomPanel_t1044499065;
// BarygaVkController
struct BarygaVkController_t3617163921;
// GameController
struct GameController_t2782302542;
// TopPanel
struct TopPanel_t3379186127;
// AchievementController
struct AchievementController_t3677499403;
// GlobalStat
struct GlobalStat_t1134678199;
// UnityEngine.UI.Image
struct Image_t3354615620;
// OligarchParameters
struct OligarchParameters_t821144443;
// UniRx.Operators.CombineLatestFunc`5<System.Boolean,System.Boolean,System.Boolean,System.Boolean,<>__AnonType0`4<System.Boolean,System.Boolean,System.Boolean,System.Boolean>>
struct CombineLatestFunc_5_t2127898724;
// System.Action`1<System.Boolean>
struct Action_1_t359458046;
// System.Func`3<System.Boolean,System.Boolean,<>__AnonType1`2<System.Boolean,System.Boolean>>
struct Func_3_t1116682641;
// System.Func`2<System.Int32,UnityEngine.Color>
struct Func_2_t390344745;
// System.Func`3<System.Boolean,System.Boolean,<>__AnonType2`2<System.Boolean,System.Boolean>>
struct Func_3_t2214213620;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GuiController
struct  GuiController_t1495593751  : public Il2CppObject
{
public:
	// UniRx.Subject`1<UniRx.Unit> GuiController::ToiletPressed
	Subject_1_t201353362 * ___ToiletPressed_0;
	// UniRx.Subject`1<UniRx.Unit> GuiController::CasePressed
	Subject_1_t201353362 * ___CasePressed_1;
	// UniRx.Subject`1<UniRx.Unit> GuiController::AbacusPressed
	Subject_1_t201353362 * ___AbacusPressed_2;
	// UniRx.Subject`1<UniRx.Unit> GuiController::SafePressed
	Subject_1_t201353362 * ___SafePressed_3;
	// UniRx.Subject`1<UniRx.Unit> GuiController::SettingsPressed
	Subject_1_t201353362 * ___SettingsPressed_4;
	// UniRx.Subject`1<UniRx.Unit> GuiController::BucketPressed
	Subject_1_t201353362 * ___BucketPressed_5;
	// UniRx.Subject`1<UniRx.Unit> GuiController::CloseMenu
	Subject_1_t201353362 * ___CloseMenu_6;
	// BottomButton GuiController::BucketButton
	BottomButton_t1932555613 * ___BucketButton_7;
	// UnityEngine.GameObject GuiController::moneyParent
	GameObject_t4012695102 * ___moneyParent_8;
	// Tacticsoft.TableView GuiController::TableView
	TableView_t692333993 * ___TableView_9;
	// TableButtons GuiController::TableButtons
	TableButtons_t1868573683 * ___TableButtons_10;
	// UniRx.IObservable`1<UniRx.Unit> GuiController::MenuButtonPressed
	Il2CppObject* ___MenuButtonPressed_11;
	// UniRx.ReactiveProperty`1<System.Boolean> GuiController::ShowTable
	ReactiveProperty_1_t819338379 * ___ShowTable_12;
	// UniRx.ReactiveProperty`1<System.Boolean> GuiController::ShowBottomButtons
	ReactiveProperty_1_t819338379 * ___ShowBottomButtons_13;
	// WindowManager GuiController::WindowManager
	WindowManager_t3316821373 * ___WindowManager_14;
	// TutorialPlayer GuiController::TutorialPlayer
	TutorialPlayer_t4281139199 * ___TutorialPlayer_15;
	// BottomPanel GuiController::BottomPanel
	BottomPanel_t1044499065 * ___BottomPanel_16;
	// BarygaVkController GuiController::BarygaVkController
	BarygaVkController_t3617163921 * ___BarygaVkController_17;
	// GameController GuiController::GameController
	GameController_t2782302542 * ___GameController_18;
	// TopPanel GuiController::TopPanel
	TopPanel_t3379186127 * ___TopPanel_19;
	// AchievementController GuiController::AchievementController
	AchievementController_t3677499403 * ___AchievementController_20;
	// GlobalStat GuiController::GlobalStat
	GlobalStat_t1134678199 * ___GlobalStat_21;
	// UnityEngine.GameObject GuiController::TableParent
	GameObject_t4012695102 * ___TableParent_22;
	// UnityEngine.UI.Image GuiController::CanvasFon
	Image_t3354615620 * ___CanvasFon_23;
	// OligarchParameters GuiController::OligarchParameters
	OligarchParameters_t821144443 * ___OligarchParameters_24;

public:
	inline static int32_t get_offset_of_ToiletPressed_0() { return static_cast<int32_t>(offsetof(GuiController_t1495593751, ___ToiletPressed_0)); }
	inline Subject_1_t201353362 * get_ToiletPressed_0() const { return ___ToiletPressed_0; }
	inline Subject_1_t201353362 ** get_address_of_ToiletPressed_0() { return &___ToiletPressed_0; }
	inline void set_ToiletPressed_0(Subject_1_t201353362 * value)
	{
		___ToiletPressed_0 = value;
		Il2CppCodeGenWriteBarrier(&___ToiletPressed_0, value);
	}

	inline static int32_t get_offset_of_CasePressed_1() { return static_cast<int32_t>(offsetof(GuiController_t1495593751, ___CasePressed_1)); }
	inline Subject_1_t201353362 * get_CasePressed_1() const { return ___CasePressed_1; }
	inline Subject_1_t201353362 ** get_address_of_CasePressed_1() { return &___CasePressed_1; }
	inline void set_CasePressed_1(Subject_1_t201353362 * value)
	{
		___CasePressed_1 = value;
		Il2CppCodeGenWriteBarrier(&___CasePressed_1, value);
	}

	inline static int32_t get_offset_of_AbacusPressed_2() { return static_cast<int32_t>(offsetof(GuiController_t1495593751, ___AbacusPressed_2)); }
	inline Subject_1_t201353362 * get_AbacusPressed_2() const { return ___AbacusPressed_2; }
	inline Subject_1_t201353362 ** get_address_of_AbacusPressed_2() { return &___AbacusPressed_2; }
	inline void set_AbacusPressed_2(Subject_1_t201353362 * value)
	{
		___AbacusPressed_2 = value;
		Il2CppCodeGenWriteBarrier(&___AbacusPressed_2, value);
	}

	inline static int32_t get_offset_of_SafePressed_3() { return static_cast<int32_t>(offsetof(GuiController_t1495593751, ___SafePressed_3)); }
	inline Subject_1_t201353362 * get_SafePressed_3() const { return ___SafePressed_3; }
	inline Subject_1_t201353362 ** get_address_of_SafePressed_3() { return &___SafePressed_3; }
	inline void set_SafePressed_3(Subject_1_t201353362 * value)
	{
		___SafePressed_3 = value;
		Il2CppCodeGenWriteBarrier(&___SafePressed_3, value);
	}

	inline static int32_t get_offset_of_SettingsPressed_4() { return static_cast<int32_t>(offsetof(GuiController_t1495593751, ___SettingsPressed_4)); }
	inline Subject_1_t201353362 * get_SettingsPressed_4() const { return ___SettingsPressed_4; }
	inline Subject_1_t201353362 ** get_address_of_SettingsPressed_4() { return &___SettingsPressed_4; }
	inline void set_SettingsPressed_4(Subject_1_t201353362 * value)
	{
		___SettingsPressed_4 = value;
		Il2CppCodeGenWriteBarrier(&___SettingsPressed_4, value);
	}

	inline static int32_t get_offset_of_BucketPressed_5() { return static_cast<int32_t>(offsetof(GuiController_t1495593751, ___BucketPressed_5)); }
	inline Subject_1_t201353362 * get_BucketPressed_5() const { return ___BucketPressed_5; }
	inline Subject_1_t201353362 ** get_address_of_BucketPressed_5() { return &___BucketPressed_5; }
	inline void set_BucketPressed_5(Subject_1_t201353362 * value)
	{
		___BucketPressed_5 = value;
		Il2CppCodeGenWriteBarrier(&___BucketPressed_5, value);
	}

	inline static int32_t get_offset_of_CloseMenu_6() { return static_cast<int32_t>(offsetof(GuiController_t1495593751, ___CloseMenu_6)); }
	inline Subject_1_t201353362 * get_CloseMenu_6() const { return ___CloseMenu_6; }
	inline Subject_1_t201353362 ** get_address_of_CloseMenu_6() { return &___CloseMenu_6; }
	inline void set_CloseMenu_6(Subject_1_t201353362 * value)
	{
		___CloseMenu_6 = value;
		Il2CppCodeGenWriteBarrier(&___CloseMenu_6, value);
	}

	inline static int32_t get_offset_of_BucketButton_7() { return static_cast<int32_t>(offsetof(GuiController_t1495593751, ___BucketButton_7)); }
	inline BottomButton_t1932555613 * get_BucketButton_7() const { return ___BucketButton_7; }
	inline BottomButton_t1932555613 ** get_address_of_BucketButton_7() { return &___BucketButton_7; }
	inline void set_BucketButton_7(BottomButton_t1932555613 * value)
	{
		___BucketButton_7 = value;
		Il2CppCodeGenWriteBarrier(&___BucketButton_7, value);
	}

	inline static int32_t get_offset_of_moneyParent_8() { return static_cast<int32_t>(offsetof(GuiController_t1495593751, ___moneyParent_8)); }
	inline GameObject_t4012695102 * get_moneyParent_8() const { return ___moneyParent_8; }
	inline GameObject_t4012695102 ** get_address_of_moneyParent_8() { return &___moneyParent_8; }
	inline void set_moneyParent_8(GameObject_t4012695102 * value)
	{
		___moneyParent_8 = value;
		Il2CppCodeGenWriteBarrier(&___moneyParent_8, value);
	}

	inline static int32_t get_offset_of_TableView_9() { return static_cast<int32_t>(offsetof(GuiController_t1495593751, ___TableView_9)); }
	inline TableView_t692333993 * get_TableView_9() const { return ___TableView_9; }
	inline TableView_t692333993 ** get_address_of_TableView_9() { return &___TableView_9; }
	inline void set_TableView_9(TableView_t692333993 * value)
	{
		___TableView_9 = value;
		Il2CppCodeGenWriteBarrier(&___TableView_9, value);
	}

	inline static int32_t get_offset_of_TableButtons_10() { return static_cast<int32_t>(offsetof(GuiController_t1495593751, ___TableButtons_10)); }
	inline TableButtons_t1868573683 * get_TableButtons_10() const { return ___TableButtons_10; }
	inline TableButtons_t1868573683 ** get_address_of_TableButtons_10() { return &___TableButtons_10; }
	inline void set_TableButtons_10(TableButtons_t1868573683 * value)
	{
		___TableButtons_10 = value;
		Il2CppCodeGenWriteBarrier(&___TableButtons_10, value);
	}

	inline static int32_t get_offset_of_MenuButtonPressed_11() { return static_cast<int32_t>(offsetof(GuiController_t1495593751, ___MenuButtonPressed_11)); }
	inline Il2CppObject* get_MenuButtonPressed_11() const { return ___MenuButtonPressed_11; }
	inline Il2CppObject** get_address_of_MenuButtonPressed_11() { return &___MenuButtonPressed_11; }
	inline void set_MenuButtonPressed_11(Il2CppObject* value)
	{
		___MenuButtonPressed_11 = value;
		Il2CppCodeGenWriteBarrier(&___MenuButtonPressed_11, value);
	}

	inline static int32_t get_offset_of_ShowTable_12() { return static_cast<int32_t>(offsetof(GuiController_t1495593751, ___ShowTable_12)); }
	inline ReactiveProperty_1_t819338379 * get_ShowTable_12() const { return ___ShowTable_12; }
	inline ReactiveProperty_1_t819338379 ** get_address_of_ShowTable_12() { return &___ShowTable_12; }
	inline void set_ShowTable_12(ReactiveProperty_1_t819338379 * value)
	{
		___ShowTable_12 = value;
		Il2CppCodeGenWriteBarrier(&___ShowTable_12, value);
	}

	inline static int32_t get_offset_of_ShowBottomButtons_13() { return static_cast<int32_t>(offsetof(GuiController_t1495593751, ___ShowBottomButtons_13)); }
	inline ReactiveProperty_1_t819338379 * get_ShowBottomButtons_13() const { return ___ShowBottomButtons_13; }
	inline ReactiveProperty_1_t819338379 ** get_address_of_ShowBottomButtons_13() { return &___ShowBottomButtons_13; }
	inline void set_ShowBottomButtons_13(ReactiveProperty_1_t819338379 * value)
	{
		___ShowBottomButtons_13 = value;
		Il2CppCodeGenWriteBarrier(&___ShowBottomButtons_13, value);
	}

	inline static int32_t get_offset_of_WindowManager_14() { return static_cast<int32_t>(offsetof(GuiController_t1495593751, ___WindowManager_14)); }
	inline WindowManager_t3316821373 * get_WindowManager_14() const { return ___WindowManager_14; }
	inline WindowManager_t3316821373 ** get_address_of_WindowManager_14() { return &___WindowManager_14; }
	inline void set_WindowManager_14(WindowManager_t3316821373 * value)
	{
		___WindowManager_14 = value;
		Il2CppCodeGenWriteBarrier(&___WindowManager_14, value);
	}

	inline static int32_t get_offset_of_TutorialPlayer_15() { return static_cast<int32_t>(offsetof(GuiController_t1495593751, ___TutorialPlayer_15)); }
	inline TutorialPlayer_t4281139199 * get_TutorialPlayer_15() const { return ___TutorialPlayer_15; }
	inline TutorialPlayer_t4281139199 ** get_address_of_TutorialPlayer_15() { return &___TutorialPlayer_15; }
	inline void set_TutorialPlayer_15(TutorialPlayer_t4281139199 * value)
	{
		___TutorialPlayer_15 = value;
		Il2CppCodeGenWriteBarrier(&___TutorialPlayer_15, value);
	}

	inline static int32_t get_offset_of_BottomPanel_16() { return static_cast<int32_t>(offsetof(GuiController_t1495593751, ___BottomPanel_16)); }
	inline BottomPanel_t1044499065 * get_BottomPanel_16() const { return ___BottomPanel_16; }
	inline BottomPanel_t1044499065 ** get_address_of_BottomPanel_16() { return &___BottomPanel_16; }
	inline void set_BottomPanel_16(BottomPanel_t1044499065 * value)
	{
		___BottomPanel_16 = value;
		Il2CppCodeGenWriteBarrier(&___BottomPanel_16, value);
	}

	inline static int32_t get_offset_of_BarygaVkController_17() { return static_cast<int32_t>(offsetof(GuiController_t1495593751, ___BarygaVkController_17)); }
	inline BarygaVkController_t3617163921 * get_BarygaVkController_17() const { return ___BarygaVkController_17; }
	inline BarygaVkController_t3617163921 ** get_address_of_BarygaVkController_17() { return &___BarygaVkController_17; }
	inline void set_BarygaVkController_17(BarygaVkController_t3617163921 * value)
	{
		___BarygaVkController_17 = value;
		Il2CppCodeGenWriteBarrier(&___BarygaVkController_17, value);
	}

	inline static int32_t get_offset_of_GameController_18() { return static_cast<int32_t>(offsetof(GuiController_t1495593751, ___GameController_18)); }
	inline GameController_t2782302542 * get_GameController_18() const { return ___GameController_18; }
	inline GameController_t2782302542 ** get_address_of_GameController_18() { return &___GameController_18; }
	inline void set_GameController_18(GameController_t2782302542 * value)
	{
		___GameController_18 = value;
		Il2CppCodeGenWriteBarrier(&___GameController_18, value);
	}

	inline static int32_t get_offset_of_TopPanel_19() { return static_cast<int32_t>(offsetof(GuiController_t1495593751, ___TopPanel_19)); }
	inline TopPanel_t3379186127 * get_TopPanel_19() const { return ___TopPanel_19; }
	inline TopPanel_t3379186127 ** get_address_of_TopPanel_19() { return &___TopPanel_19; }
	inline void set_TopPanel_19(TopPanel_t3379186127 * value)
	{
		___TopPanel_19 = value;
		Il2CppCodeGenWriteBarrier(&___TopPanel_19, value);
	}

	inline static int32_t get_offset_of_AchievementController_20() { return static_cast<int32_t>(offsetof(GuiController_t1495593751, ___AchievementController_20)); }
	inline AchievementController_t3677499403 * get_AchievementController_20() const { return ___AchievementController_20; }
	inline AchievementController_t3677499403 ** get_address_of_AchievementController_20() { return &___AchievementController_20; }
	inline void set_AchievementController_20(AchievementController_t3677499403 * value)
	{
		___AchievementController_20 = value;
		Il2CppCodeGenWriteBarrier(&___AchievementController_20, value);
	}

	inline static int32_t get_offset_of_GlobalStat_21() { return static_cast<int32_t>(offsetof(GuiController_t1495593751, ___GlobalStat_21)); }
	inline GlobalStat_t1134678199 * get_GlobalStat_21() const { return ___GlobalStat_21; }
	inline GlobalStat_t1134678199 ** get_address_of_GlobalStat_21() { return &___GlobalStat_21; }
	inline void set_GlobalStat_21(GlobalStat_t1134678199 * value)
	{
		___GlobalStat_21 = value;
		Il2CppCodeGenWriteBarrier(&___GlobalStat_21, value);
	}

	inline static int32_t get_offset_of_TableParent_22() { return static_cast<int32_t>(offsetof(GuiController_t1495593751, ___TableParent_22)); }
	inline GameObject_t4012695102 * get_TableParent_22() const { return ___TableParent_22; }
	inline GameObject_t4012695102 ** get_address_of_TableParent_22() { return &___TableParent_22; }
	inline void set_TableParent_22(GameObject_t4012695102 * value)
	{
		___TableParent_22 = value;
		Il2CppCodeGenWriteBarrier(&___TableParent_22, value);
	}

	inline static int32_t get_offset_of_CanvasFon_23() { return static_cast<int32_t>(offsetof(GuiController_t1495593751, ___CanvasFon_23)); }
	inline Image_t3354615620 * get_CanvasFon_23() const { return ___CanvasFon_23; }
	inline Image_t3354615620 ** get_address_of_CanvasFon_23() { return &___CanvasFon_23; }
	inline void set_CanvasFon_23(Image_t3354615620 * value)
	{
		___CanvasFon_23 = value;
		Il2CppCodeGenWriteBarrier(&___CanvasFon_23, value);
	}

	inline static int32_t get_offset_of_OligarchParameters_24() { return static_cast<int32_t>(offsetof(GuiController_t1495593751, ___OligarchParameters_24)); }
	inline OligarchParameters_t821144443 * get_OligarchParameters_24() const { return ___OligarchParameters_24; }
	inline OligarchParameters_t821144443 ** get_address_of_OligarchParameters_24() { return &___OligarchParameters_24; }
	inline void set_OligarchParameters_24(OligarchParameters_t821144443 * value)
	{
		___OligarchParameters_24 = value;
		Il2CppCodeGenWriteBarrier(&___OligarchParameters_24, value);
	}
};

struct GuiController_t1495593751_StaticFields
{
public:
	// UniRx.Operators.CombineLatestFunc`5<System.Boolean,System.Boolean,System.Boolean,System.Boolean,<>__AnonType0`4<System.Boolean,System.Boolean,System.Boolean,System.Boolean>> GuiController::<>f__am$cache19
	CombineLatestFunc_5_t2127898724 * ___U3CU3Ef__amU24cache19_25;
	// System.Action`1<System.Boolean> GuiController::<>f__am$cache1A
	Action_1_t359458046 * ___U3CU3Ef__amU24cache1A_26;
	// System.Func`3<System.Boolean,System.Boolean,<>__AnonType1`2<System.Boolean,System.Boolean>> GuiController::<>f__am$cache1B
	Func_3_t1116682641 * ___U3CU3Ef__amU24cache1B_27;
	// System.Func`2<System.Int32,UnityEngine.Color> GuiController::<>f__am$cache1C
	Func_2_t390344745 * ___U3CU3Ef__amU24cache1C_28;
	// System.Func`3<System.Boolean,System.Boolean,<>__AnonType2`2<System.Boolean,System.Boolean>> GuiController::<>f__am$cache1D
	Func_3_t2214213620 * ___U3CU3Ef__amU24cache1D_29;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache19_25() { return static_cast<int32_t>(offsetof(GuiController_t1495593751_StaticFields, ___U3CU3Ef__amU24cache19_25)); }
	inline CombineLatestFunc_5_t2127898724 * get_U3CU3Ef__amU24cache19_25() const { return ___U3CU3Ef__amU24cache19_25; }
	inline CombineLatestFunc_5_t2127898724 ** get_address_of_U3CU3Ef__amU24cache19_25() { return &___U3CU3Ef__amU24cache19_25; }
	inline void set_U3CU3Ef__amU24cache19_25(CombineLatestFunc_5_t2127898724 * value)
	{
		___U3CU3Ef__amU24cache19_25 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache19_25, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1A_26() { return static_cast<int32_t>(offsetof(GuiController_t1495593751_StaticFields, ___U3CU3Ef__amU24cache1A_26)); }
	inline Action_1_t359458046 * get_U3CU3Ef__amU24cache1A_26() const { return ___U3CU3Ef__amU24cache1A_26; }
	inline Action_1_t359458046 ** get_address_of_U3CU3Ef__amU24cache1A_26() { return &___U3CU3Ef__amU24cache1A_26; }
	inline void set_U3CU3Ef__amU24cache1A_26(Action_1_t359458046 * value)
	{
		___U3CU3Ef__amU24cache1A_26 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1A_26, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1B_27() { return static_cast<int32_t>(offsetof(GuiController_t1495593751_StaticFields, ___U3CU3Ef__amU24cache1B_27)); }
	inline Func_3_t1116682641 * get_U3CU3Ef__amU24cache1B_27() const { return ___U3CU3Ef__amU24cache1B_27; }
	inline Func_3_t1116682641 ** get_address_of_U3CU3Ef__amU24cache1B_27() { return &___U3CU3Ef__amU24cache1B_27; }
	inline void set_U3CU3Ef__amU24cache1B_27(Func_3_t1116682641 * value)
	{
		___U3CU3Ef__amU24cache1B_27 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1B_27, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1C_28() { return static_cast<int32_t>(offsetof(GuiController_t1495593751_StaticFields, ___U3CU3Ef__amU24cache1C_28)); }
	inline Func_2_t390344745 * get_U3CU3Ef__amU24cache1C_28() const { return ___U3CU3Ef__amU24cache1C_28; }
	inline Func_2_t390344745 ** get_address_of_U3CU3Ef__amU24cache1C_28() { return &___U3CU3Ef__amU24cache1C_28; }
	inline void set_U3CU3Ef__amU24cache1C_28(Func_2_t390344745 * value)
	{
		___U3CU3Ef__amU24cache1C_28 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1C_28, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1D_29() { return static_cast<int32_t>(offsetof(GuiController_t1495593751_StaticFields, ___U3CU3Ef__amU24cache1D_29)); }
	inline Func_3_t2214213620 * get_U3CU3Ef__amU24cache1D_29() const { return ___U3CU3Ef__amU24cache1D_29; }
	inline Func_3_t2214213620 ** get_address_of_U3CU3Ef__amU24cache1D_29() { return &___U3CU3Ef__amU24cache1D_29; }
	inline void set_U3CU3Ef__amU24cache1D_29(Func_3_t2214213620 * value)
	{
		___U3CU3Ef__amU24cache1D_29 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1D_29, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
