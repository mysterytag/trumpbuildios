﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.UnlinkPSNAccountRequest
struct UnlinkPSNAccountRequest_t26689898;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.UnlinkPSNAccountRequest::.ctor()
extern "C"  void UnlinkPSNAccountRequest__ctor_m3454465919 (UnlinkPSNAccountRequest_t26689898 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
