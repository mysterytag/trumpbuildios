﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;

#include "mscorlib_System_ValueType4014882752.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Tuple`1<System.Object>
struct  Tuple_1_t3656007660 
{
public:
	// T1 UniRx.Tuple`1::item1
	Il2CppObject * ___item1_0;

public:
	inline static int32_t get_offset_of_item1_0() { return static_cast<int32_t>(offsetof(Tuple_1_t3656007660, ___item1_0)); }
	inline Il2CppObject * get_item1_0() const { return ___item1_0; }
	inline Il2CppObject ** get_address_of_item1_0() { return &___item1_0; }
	inline void set_item1_0(Il2CppObject * value)
	{
		___item1_0 = value;
		Il2CppCodeGenWriteBarrier(&___item1_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
