﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.Action`1<System.TimeSpan>>
struct Action_1_t1060768302;
// UniRx.IScheduler
struct IScheduler_t2938318244;
// System.Object
struct Il2CppObject;
// UniRx.CompositeDisposable
struct CompositeDisposable_t1894629977;
// System.Action
struct Action_t437523947;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Scheduler/<Schedule>c__AnonStorey75
struct  U3CScheduleU3Ec__AnonStorey75_t3913535386  : public Il2CppObject
{
public:
	// System.Action`1<System.Action`1<System.TimeSpan>> UniRx.Scheduler/<Schedule>c__AnonStorey75::action
	Action_1_t1060768302 * ___action_0;
	// UniRx.IScheduler UniRx.Scheduler/<Schedule>c__AnonStorey75::scheduler
	Il2CppObject * ___scheduler_1;
	// System.Object UniRx.Scheduler/<Schedule>c__AnonStorey75::gate
	Il2CppObject * ___gate_2;
	// UniRx.CompositeDisposable UniRx.Scheduler/<Schedule>c__AnonStorey75::group
	CompositeDisposable_t1894629977 * ___group_3;
	// System.Action UniRx.Scheduler/<Schedule>c__AnonStorey75::recursiveAction
	Action_t437523947 * ___recursiveAction_4;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(U3CScheduleU3Ec__AnonStorey75_t3913535386, ___action_0)); }
	inline Action_1_t1060768302 * get_action_0() const { return ___action_0; }
	inline Action_1_t1060768302 ** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(Action_1_t1060768302 * value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier(&___action_0, value);
	}

	inline static int32_t get_offset_of_scheduler_1() { return static_cast<int32_t>(offsetof(U3CScheduleU3Ec__AnonStorey75_t3913535386, ___scheduler_1)); }
	inline Il2CppObject * get_scheduler_1() const { return ___scheduler_1; }
	inline Il2CppObject ** get_address_of_scheduler_1() { return &___scheduler_1; }
	inline void set_scheduler_1(Il2CppObject * value)
	{
		___scheduler_1 = value;
		Il2CppCodeGenWriteBarrier(&___scheduler_1, value);
	}

	inline static int32_t get_offset_of_gate_2() { return static_cast<int32_t>(offsetof(U3CScheduleU3Ec__AnonStorey75_t3913535386, ___gate_2)); }
	inline Il2CppObject * get_gate_2() const { return ___gate_2; }
	inline Il2CppObject ** get_address_of_gate_2() { return &___gate_2; }
	inline void set_gate_2(Il2CppObject * value)
	{
		___gate_2 = value;
		Il2CppCodeGenWriteBarrier(&___gate_2, value);
	}

	inline static int32_t get_offset_of_group_3() { return static_cast<int32_t>(offsetof(U3CScheduleU3Ec__AnonStorey75_t3913535386, ___group_3)); }
	inline CompositeDisposable_t1894629977 * get_group_3() const { return ___group_3; }
	inline CompositeDisposable_t1894629977 ** get_address_of_group_3() { return &___group_3; }
	inline void set_group_3(CompositeDisposable_t1894629977 * value)
	{
		___group_3 = value;
		Il2CppCodeGenWriteBarrier(&___group_3, value);
	}

	inline static int32_t get_offset_of_recursiveAction_4() { return static_cast<int32_t>(offsetof(U3CScheduleU3Ec__AnonStorey75_t3913535386, ___recursiveAction_4)); }
	inline Action_t437523947 * get_recursiveAction_4() const { return ___recursiveAction_4; }
	inline Action_t437523947 ** get_address_of_recursiveAction_4() { return &___recursiveAction_4; }
	inline void set_recursiveAction_4(Action_t437523947 * value)
	{
		___recursiveAction_4 = value;
		Il2CppCodeGenWriteBarrier(&___recursiveAction_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
