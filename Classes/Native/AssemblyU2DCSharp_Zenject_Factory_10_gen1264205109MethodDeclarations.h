﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.Factory`10<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct Factory_10_t1264205109;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t3431720054;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void Zenject.Factory`10<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C"  void Factory_10__ctor_m1091867141_gshared (Factory_10_t1264205109 * __this, const MethodInfo* method);
#define Factory_10__ctor_m1091867141(__this, method) ((  void (*) (Factory_10_t1264205109 *, const MethodInfo*))Factory_10__ctor_m1091867141_gshared)(__this, method)
// System.Type Zenject.Factory`10<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_ConstructedType()
extern "C"  Type_t * Factory_10_get_ConstructedType_m295591332_gshared (Factory_10_t1264205109 * __this, const MethodInfo* method);
#define Factory_10_get_ConstructedType_m295591332(__this, method) ((  Type_t * (*) (Factory_10_t1264205109 *, const MethodInfo*))Factory_10_get_ConstructedType_m295591332_gshared)(__this, method)
// System.Type[] Zenject.Factory`10<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_ProvidedTypes()
extern "C"  TypeU5BU5D_t3431720054* Factory_10_get_ProvidedTypes_m2918396808_gshared (Factory_10_t1264205109 * __this, const MethodInfo* method);
#define Factory_10_get_ProvidedTypes_m2918396808(__this, method) ((  TypeU5BU5D_t3431720054* (*) (Factory_10_t1264205109 *, const MethodInfo*))Factory_10_get_ProvidedTypes_m2918396808_gshared)(__this, method)
// Zenject.DiContainer Zenject.Factory`10<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::get_Container()
extern "C"  DiContainer_t2383114449 * Factory_10_get_Container_m3806906449_gshared (Factory_10_t1264205109 * __this, const MethodInfo* method);
#define Factory_10_get_Container_m3806906449(__this, method) ((  DiContainer_t2383114449 * (*) (Factory_10_t1264205109 *, const MethodInfo*))Factory_10_get_Container_m3806906449_gshared)(__this, method)
// TValue Zenject.Factory`10<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Create(TParam1,TParam2,TParam3,TParam4,TParam5,TParam6,TParam7,TParam8,TParam9)
extern "C"  Il2CppObject * Factory_10_Create_m633316187_gshared (Factory_10_t1264205109 * __this, Il2CppObject * ___param10, Il2CppObject * ___param21, Il2CppObject * ___param32, Il2CppObject * ___param43, Il2CppObject * ___param54, Il2CppObject * ___param65, Il2CppObject * ___param76, Il2CppObject * ___param87, Il2CppObject * ___param98, const MethodInfo* method);
#define Factory_10_Create_m633316187(__this, ___param10, ___param21, ___param32, ___param43, ___param54, ___param65, ___param76, ___param87, ___param98, method) ((  Il2CppObject * (*) (Factory_10_t1264205109 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Factory_10_Create_m633316187_gshared)(__this, ___param10, ___param21, ___param32, ___param43, ___param54, ___param65, ___param76, ___param87, ___param98, method)
