﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PlayFab_PlayFabSettings_Response3240762047MethodDeclarations.h"

// System.Void PlayFab.PlayFabSettings/ResponseCallback`2<System.Object,PlayFab.Internal.PlayFabResultCommon>::.ctor(System.Object,System.IntPtr)
#define ResponseCallback_2__ctor_m1677074594(__this, ___object0, ___method1, method) ((  void (*) (ResponseCallback_2_t2783168302 *, Il2CppObject *, IntPtr_t, const MethodInfo*))ResponseCallback_2__ctor_m3836631829_gshared)(__this, ___object0, ___method1, method)
// System.Void PlayFab.PlayFabSettings/ResponseCallback`2<System.Object,PlayFab.Internal.PlayFabResultCommon>::Invoke(System.String,System.Int32,TRequest,TResult,PlayFab.PlayFabError,System.Object)
#define ResponseCallback_2_Invoke_m2993138671(__this, ___urlPath0, ___callId1, ___request2, ___result3, ___error4, ___customData5, method) ((  void (*) (ResponseCallback_2_t2783168302 *, String_t*, int32_t, Il2CppObject *, PlayFabResultCommon_t379512675 *, PlayFabError_t750598646 *, Il2CppObject *, const MethodInfo*))ResponseCallback_2_Invoke_m2226430690_gshared)(__this, ___urlPath0, ___callId1, ___request2, ___result3, ___error4, ___customData5, method)
// System.IAsyncResult PlayFab.PlayFabSettings/ResponseCallback`2<System.Object,PlayFab.Internal.PlayFabResultCommon>::BeginInvoke(System.String,System.Int32,TRequest,TResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
#define ResponseCallback_2_BeginInvoke_m795759012(__this, ___urlPath0, ___callId1, ___request2, ___result3, ___error4, ___customData5, ___callback6, ___object7, method) ((  Il2CppObject * (*) (ResponseCallback_2_t2783168302 *, String_t*, int32_t, Il2CppObject *, PlayFabResultCommon_t379512675 *, PlayFabError_t750598646 *, Il2CppObject *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))ResponseCallback_2_BeginInvoke_m1492811023_gshared)(__this, ___urlPath0, ___callId1, ___request2, ___result3, ___error4, ___customData5, ___callback6, ___object7, method)
// System.Void PlayFab.PlayFabSettings/ResponseCallback`2<System.Object,PlayFab.Internal.PlayFabResultCommon>::EndInvoke(System.IAsyncResult)
#define ResponseCallback_2_EndInvoke_m1160197042(__this, ___result0, method) ((  void (*) (ResponseCallback_2_t2783168302 *, Il2CppObject *, const MethodInfo*))ResponseCallback_2_EndInvoke_m1596759973_gshared)(__this, ___result0, method)
