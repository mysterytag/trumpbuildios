﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.UnlinkKongregateAccountRequest
struct UnlinkKongregateAccountRequest_t2566546368;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.UnlinkKongregateAccountRequest::.ctor()
extern "C"  void UnlinkKongregateAccountRequest__ctor_m3749352189 (UnlinkKongregateAccountRequest_t2566546368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
