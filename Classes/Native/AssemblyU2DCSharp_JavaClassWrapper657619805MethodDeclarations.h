﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JavaClassWrapper
struct JavaClassWrapper_t657619805;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t2956870243;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void JavaClassWrapper::.ctor(System.String)
extern "C"  void JavaClassWrapper__ctor_m733754340 (JavaClassWrapper_t657619805 * __this, String_t* ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JavaClassWrapper::CallStatic(System.String)
extern "C"  void JavaClassWrapper_CallStatic_m2814935792 (JavaClassWrapper_t657619805 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JavaClassWrapper::CallStatic(System.String,System.String[])
extern "C"  void JavaClassWrapper_CallStatic_m3499455306 (JavaClassWrapper_t657619805 * __this, String_t* ___name0, StringU5BU5D_t2956870243* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
