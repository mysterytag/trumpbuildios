﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Nullable_1_gen1438485399.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.GetTitleNewsRequest
struct  GetTitleNewsRequest_t3432781354  : public Il2CppObject
{
public:
	// System.Nullable`1<System.Int32> PlayFab.ClientModels.GetTitleNewsRequest::<Count>k__BackingField
	Nullable_1_t1438485399  ___U3CCountU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CCountU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GetTitleNewsRequest_t3432781354, ___U3CCountU3Ek__BackingField_0)); }
	inline Nullable_1_t1438485399  get_U3CCountU3Ek__BackingField_0() const { return ___U3CCountU3Ek__BackingField_0; }
	inline Nullable_1_t1438485399 * get_address_of_U3CCountU3Ek__BackingField_0() { return &___U3CCountU3Ek__BackingField_0; }
	inline void set_U3CCountU3Ek__BackingField_0(Nullable_1_t1438485399  value)
	{
		___U3CCountU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
