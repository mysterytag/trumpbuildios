﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3051653477.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.EmptyObservable`1/Empty<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>
struct  Empty_t2209798842  : public OperatorObserverBase_2_t3051653477
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
