﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ReactiveProperty`1<System.Object>
struct ReactiveProperty_1_t1445439458;
// System.Object
struct Il2CppObject;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t3161373071;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.ReactiveProperty`1<System.Object>::.ctor()
extern "C"  void ReactiveProperty_1__ctor_m2182085490_gshared (ReactiveProperty_1_t1445439458 * __this, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m2182085490(__this, method) ((  void (*) (ReactiveProperty_1_t1445439458 *, const MethodInfo*))ReactiveProperty_1__ctor_m2182085490_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<System.Object>::.ctor(T)
extern "C"  void ReactiveProperty_1__ctor_m3220142124_gshared (ReactiveProperty_1_t1445439458 * __this, Il2CppObject * ___initialValue0, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m3220142124(__this, ___initialValue0, method) ((  void (*) (ReactiveProperty_1_t1445439458 *, Il2CppObject *, const MethodInfo*))ReactiveProperty_1__ctor_m3220142124_gshared)(__this, ___initialValue0, method)
// System.Void UniRx.ReactiveProperty`1<System.Object>::.ctor(UniRx.IObservable`1<T>)
extern "C"  void ReactiveProperty_1__ctor_m2559762385_gshared (ReactiveProperty_1_t1445439458 * __this, Il2CppObject* ___source0, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m2559762385(__this, ___source0, method) ((  void (*) (ReactiveProperty_1_t1445439458 *, Il2CppObject*, const MethodInfo*))ReactiveProperty_1__ctor_m2559762385_gshared)(__this, ___source0, method)
// System.Void UniRx.ReactiveProperty`1<System.Object>::.ctor(UniRx.IObservable`1<T>,T)
extern "C"  void ReactiveProperty_1__ctor_m3210364201_gshared (ReactiveProperty_1_t1445439458 * __this, Il2CppObject* ___source0, Il2CppObject * ___initialValue1, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m3210364201(__this, ___source0, ___initialValue1, method) ((  void (*) (ReactiveProperty_1_t1445439458 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))ReactiveProperty_1__ctor_m3210364201_gshared)(__this, ___source0, ___initialValue1, method)
// System.Void UniRx.ReactiveProperty`1<System.Object>::.cctor()
extern "C"  void ReactiveProperty_1__cctor_m2738044539_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ReactiveProperty_1__cctor_m2738044539(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ReactiveProperty_1__cctor_m2738044539_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1<System.Object>::get_EqualityComparer()
extern "C"  Il2CppObject* ReactiveProperty_1_get_EqualityComparer_m3290095395_gshared (ReactiveProperty_1_t1445439458 * __this, const MethodInfo* method);
#define ReactiveProperty_1_get_EqualityComparer_m3290095395(__this, method) ((  Il2CppObject* (*) (ReactiveProperty_1_t1445439458 *, const MethodInfo*))ReactiveProperty_1_get_EqualityComparer_m3290095395_gshared)(__this, method)
// T UniRx.ReactiveProperty`1<System.Object>::get_Value()
extern "C"  Il2CppObject * ReactiveProperty_1_get_Value_m2793108311_gshared (ReactiveProperty_1_t1445439458 * __this, const MethodInfo* method);
#define ReactiveProperty_1_get_Value_m2793108311(__this, method) ((  Il2CppObject * (*) (ReactiveProperty_1_t1445439458 *, const MethodInfo*))ReactiveProperty_1_get_Value_m2793108311_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<System.Object>::set_Value(T)
extern "C"  void ReactiveProperty_1_set_Value_m1580705402_gshared (ReactiveProperty_1_t1445439458 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReactiveProperty_1_set_Value_m1580705402(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t1445439458 *, Il2CppObject *, const MethodInfo*))ReactiveProperty_1_set_Value_m1580705402_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1<System.Object>::SetValue(T)
extern "C"  void ReactiveProperty_1_SetValue_m4154550269_gshared (ReactiveProperty_1_t1445439458 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReactiveProperty_1_SetValue_m4154550269(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t1445439458 *, Il2CppObject *, const MethodInfo*))ReactiveProperty_1_SetValue_m4154550269_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1<System.Object>::SetValueAndForceNotify(T)
extern "C"  void ReactiveProperty_1_SetValueAndForceNotify_m2537434880_gshared (ReactiveProperty_1_t1445439458 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReactiveProperty_1_SetValueAndForceNotify_m2537434880(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t1445439458 *, Il2CppObject *, const MethodInfo*))ReactiveProperty_1_SetValueAndForceNotify_m2537434880_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.ReactiveProperty`1<System.Object>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * ReactiveProperty_1_Subscribe_m958004807_gshared (ReactiveProperty_1_t1445439458 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ReactiveProperty_1_Subscribe_m958004807(__this, ___observer0, method) ((  Il2CppObject * (*) (ReactiveProperty_1_t1445439458 *, Il2CppObject*, const MethodInfo*))ReactiveProperty_1_Subscribe_m958004807_gshared)(__this, ___observer0, method)
// System.Void UniRx.ReactiveProperty`1<System.Object>::Dispose()
extern "C"  void ReactiveProperty_1_Dispose_m938398511_gshared (ReactiveProperty_1_t1445439458 * __this, const MethodInfo* method);
#define ReactiveProperty_1_Dispose_m938398511(__this, method) ((  void (*) (ReactiveProperty_1_t1445439458 *, const MethodInfo*))ReactiveProperty_1_Dispose_m938398511_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<System.Object>::Dispose(System.Boolean)
extern "C"  void ReactiveProperty_1_Dispose_m431016550_gshared (ReactiveProperty_1_t1445439458 * __this, bool ___disposing0, const MethodInfo* method);
#define ReactiveProperty_1_Dispose_m431016550(__this, ___disposing0, method) ((  void (*) (ReactiveProperty_1_t1445439458 *, bool, const MethodInfo*))ReactiveProperty_1_Dispose_m431016550_gshared)(__this, ___disposing0, method)
// System.String UniRx.ReactiveProperty`1<System.Object>::ToString()
extern "C"  String_t* ReactiveProperty_1_ToString_m2722346619_gshared (ReactiveProperty_1_t1445439458 * __this, const MethodInfo* method);
#define ReactiveProperty_1_ToString_m2722346619(__this, method) ((  String_t* (*) (ReactiveProperty_1_t1445439458 *, const MethodInfo*))ReactiveProperty_1_ToString_m2722346619_gshared)(__this, method)
// System.Boolean UniRx.ReactiveProperty`1<System.Object>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m3797922201_gshared (ReactiveProperty_1_t1445439458 * __this, const MethodInfo* method);
#define ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m3797922201(__this, method) ((  bool (*) (ReactiveProperty_1_t1445439458 *, const MethodInfo*))ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m3797922201_gshared)(__this, method)
