﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BottomButton
struct BottomButton_t1932555613;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t3205101634;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve3205101634.h"

// System.Void BottomButton::.ctor()
extern "C"  void BottomButton__ctor_m4193859038 (BottomButton_t1932555613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BottomButton::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern "C"  void BottomButton_OnPointerClick_m2998455246 (BottomButton_t1932555613 * __this, PointerEventData_t3205101634 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BottomButton::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern "C"  void BottomButton_OnPointerDown_m865674210 (BottomButton_t1932555613 * __this, PointerEventData_t3205101634 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BottomButton::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern "C"  void BottomButton_OnPointerUp_m1391457801 (BottomButton_t1932555613 * __this, PointerEventData_t3205101634 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
