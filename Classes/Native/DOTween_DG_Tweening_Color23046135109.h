﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType4014882752.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Color2
struct  Color2_t3046135109 
{
public:
	// UnityEngine.Color DG.Tweening.Color2::ca
	Color_t1588175760  ___ca_0;
	// UnityEngine.Color DG.Tweening.Color2::cb
	Color_t1588175760  ___cb_1;

public:
	inline static int32_t get_offset_of_ca_0() { return static_cast<int32_t>(offsetof(Color2_t3046135109, ___ca_0)); }
	inline Color_t1588175760  get_ca_0() const { return ___ca_0; }
	inline Color_t1588175760 * get_address_of_ca_0() { return &___ca_0; }
	inline void set_ca_0(Color_t1588175760  value)
	{
		___ca_0 = value;
	}

	inline static int32_t get_offset_of_cb_1() { return static_cast<int32_t>(offsetof(Color2_t3046135109, ___cb_1)); }
	inline Color_t1588175760  get_cb_1() const { return ___cb_1; }
	inline Color_t1588175760 * get_address_of_cb_1() { return &___cb_1; }
	inline void set_cb_1(Color_t1588175760  value)
	{
		___cb_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: DG.Tweening.Color2
struct Color2_t3046135109_marshaled_pinvoke
{
	Color_t1588175760_marshaled_pinvoke ___ca_0;
	Color_t1588175760_marshaled_pinvoke ___cb_1;
};
// Native definition for marshalling of: DG.Tweening.Color2
struct Color2_t3046135109_marshaled_com
{
	Color_t1588175760_marshaled_com ___ca_0;
	Color_t1588175760_marshaled_com ___cb_1;
};
