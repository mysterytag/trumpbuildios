﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Func`2<UniRx.Tuple`2<System.Object,System.Object>,System.Boolean>
struct Func_2_t1101125214;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen369261819.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Func`2<UniRx.Tuple`2<System.Object,System.Object>,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m1978381903_gshared (Func_2_t1101125214 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Func_2__ctor_m1978381903(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t1101125214 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m1978381903_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<UniRx.Tuple`2<System.Object,System.Object>,System.Boolean>::Invoke(T)
extern "C"  bool Func_2_Invoke_m621375027_gshared (Func_2_t1101125214 * __this, Tuple_2_t369261819  ___arg10, const MethodInfo* method);
#define Func_2_Invoke_m621375027(__this, ___arg10, method) ((  bool (*) (Func_2_t1101125214 *, Tuple_2_t369261819 , const MethodInfo*))Func_2_Invoke_m621375027_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<UniRx.Tuple`2<System.Object,System.Object>,System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_2_BeginInvoke_m2552012514_gshared (Func_2_t1101125214 * __this, Tuple_2_t369261819  ___arg10, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method);
#define Func_2_BeginInvoke_m2552012514(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t1101125214 *, Tuple_2_t369261819 , AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m2552012514_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<UniRx.Tuple`2<System.Object,System.Object>,System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  bool Func_2_EndInvoke_m205505409_gshared (Func_2_t1101125214 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Func_2_EndInvoke_m205505409(__this, ___result0, method) ((  bool (*) (Func_2_t1101125214 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m205505409_gshared)(__this, ___result0, method)
