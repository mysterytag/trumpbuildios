﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.WhereObservable`1/<CombinePredicate>c__AnonStorey72<System.Int64>
struct U3CCombinePredicateU3Ec__AnonStorey72_t278143071;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.WhereObservable`1/<CombinePredicate>c__AnonStorey72<System.Int64>::.ctor()
extern "C"  void U3CCombinePredicateU3Ec__AnonStorey72__ctor_m2118593878_gshared (U3CCombinePredicateU3Ec__AnonStorey72_t278143071 * __this, const MethodInfo* method);
#define U3CCombinePredicateU3Ec__AnonStorey72__ctor_m2118593878(__this, method) ((  void (*) (U3CCombinePredicateU3Ec__AnonStorey72_t278143071 *, const MethodInfo*))U3CCombinePredicateU3Ec__AnonStorey72__ctor_m2118593878_gshared)(__this, method)
// System.Boolean UniRx.Operators.WhereObservable`1/<CombinePredicate>c__AnonStorey72<System.Int64>::<>m__8F(T)
extern "C"  bool U3CCombinePredicateU3Ec__AnonStorey72_U3CU3Em__8F_m2979859637_gshared (U3CCombinePredicateU3Ec__AnonStorey72_t278143071 * __this, int64_t ___x0, const MethodInfo* method);
#define U3CCombinePredicateU3Ec__AnonStorey72_U3CU3Em__8F_m2979859637(__this, ___x0, method) ((  bool (*) (U3CCombinePredicateU3Ec__AnonStorey72_t278143071 *, int64_t, const MethodInfo*))U3CCombinePredicateU3Ec__AnonStorey72_U3CU3Em__8F_m2979859637_gshared)(__this, ___x0, method)
