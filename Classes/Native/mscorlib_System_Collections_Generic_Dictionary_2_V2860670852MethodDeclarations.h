﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>
struct ValueCollection_t2860670852;
// System.Collections.Generic.Dictionary`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>
struct Dictionary_2_t938533758;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2320212868;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object[]
struct ObjectU5BU5D_t11523773;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va705561699.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m1765980492_gshared (ValueCollection_t2860670852 * __this, Dictionary_2_t938533758 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m1765980492(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t2860670852 *, Dictionary_2_t938533758 *, const MethodInfo*))ValueCollection__ctor_m1765980492_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3944294758_gshared (ValueCollection_t2860670852 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3944294758(__this, ___item0, method) ((  void (*) (ValueCollection_t2860670852 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3944294758_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3484924207_gshared (ValueCollection_t2860670852 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3484924207(__this, method) ((  void (*) (ValueCollection_t2860670852 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3484924207_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2955240900_gshared (ValueCollection_t2860670852 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2955240900(__this, ___item0, method) ((  bool (*) (ValueCollection_t2860670852 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2955240900_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3454345065_gshared (ValueCollection_t2860670852 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3454345065(__this, ___item0, method) ((  bool (*) (ValueCollection_t2860670852 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3454345065_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3891928751_gshared (ValueCollection_t2860670852 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3891928751(__this, method) ((  Il2CppObject* (*) (ValueCollection_t2860670852 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3891928751_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m3193432947_gshared (ValueCollection_t2860670852 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m3193432947(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2860670852 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3193432947_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1817785282_gshared (ValueCollection_t2860670852 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1817785282(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2860670852 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1817785282_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1230416759_gshared (ValueCollection_t2860670852 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1230416759(__this, method) ((  bool (*) (ValueCollection_t2860670852 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1230416759_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1403071447_gshared (ValueCollection_t2860670852 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1403071447(__this, method) ((  bool (*) (ValueCollection_t2860670852 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1403071447_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m601618121_gshared (ValueCollection_t2860670852 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m601618121(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2860670852 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m601618121_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m4072966803_gshared (ValueCollection_t2860670852 * __this, ObjectU5BU5D_t11523773* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m4072966803(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2860670852 *, ObjectU5BU5D_t11523773*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m4072966803_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::GetEnumerator()
extern "C"  Enumerator_t705561701  ValueCollection_GetEnumerator_m3082107772_gshared (ValueCollection_t2860670852 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m3082107772(__this, method) ((  Enumerator_t705561701  (*) (ValueCollection_t2860670852 *, const MethodInfo*))ValueCollection_GetEnumerator_m3082107772_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m2799428497_gshared (ValueCollection_t2860670852 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m2799428497(__this, method) ((  int32_t (*) (ValueCollection_t2860670852 *, const MethodInfo*))ValueCollection_get_Count_m2799428497_gshared)(__this, method)
