﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Stream`1<UniRx.Unit>
struct Stream_1_t1249425060;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<UniRx.Unit>
struct Action_1_t2706738743;
// System.Action
struct Action_t437523947;
// IStream`1<UniRx.Unit>
struct IStream_1_t3110977029;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"
#include "System_Core_System_Action437523947.h"

// System.Void Stream`1<UniRx.Unit>::.ctor()
extern "C"  void Stream_1__ctor_m2832653796_gshared (Stream_1_t1249425060 * __this, const MethodInfo* method);
#define Stream_1__ctor_m2832653796(__this, method) ((  void (*) (Stream_1_t1249425060 *, const MethodInfo*))Stream_1__ctor_m2832653796_gshared)(__this, method)
// System.Void Stream`1<UniRx.Unit>::Send(T)
extern "C"  void Stream_1_Send_m1361871350_gshared (Stream_1_t1249425060 * __this, Unit_t2558286038  ___value0, const MethodInfo* method);
#define Stream_1_Send_m1361871350(__this, ___value0, method) ((  void (*) (Stream_1_t1249425060 *, Unit_t2558286038 , const MethodInfo*))Stream_1_Send_m1361871350_gshared)(__this, ___value0, method)
// System.Void Stream`1<UniRx.Unit>::SendInTransaction(T,System.Int32)
extern "C"  void Stream_1_SendInTransaction_m2976517644_gshared (Stream_1_t1249425060 * __this, Unit_t2558286038  ___value0, int32_t ___p1, const MethodInfo* method);
#define Stream_1_SendInTransaction_m2976517644(__this, ___value0, ___p1, method) ((  void (*) (Stream_1_t1249425060 *, Unit_t2558286038 , int32_t, const MethodInfo*))Stream_1_SendInTransaction_m2976517644_gshared)(__this, ___value0, ___p1, method)
// System.IDisposable Stream`1<UniRx.Unit>::Listen(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Stream_1_Listen_m2898576392_gshared (Stream_1_t1249425060 * __this, Action_1_t2706738743 * ___action0, int32_t ___priority1, const MethodInfo* method);
#define Stream_1_Listen_m2898576392(__this, ___action0, ___priority1, method) ((  Il2CppObject * (*) (Stream_1_t1249425060 *, Action_1_t2706738743 *, int32_t, const MethodInfo*))Stream_1_Listen_m2898576392_gshared)(__this, ___action0, ___priority1, method)
// System.IDisposable Stream`1<UniRx.Unit>::Listen(System.Action,Priority)
extern "C"  Il2CppObject * Stream_1_Listen_m3884177439_gshared (Stream_1_t1249425060 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method);
#define Stream_1_Listen_m3884177439(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (Stream_1_t1249425060 *, Action_t437523947 *, int32_t, const MethodInfo*))Stream_1_Listen_m3884177439_gshared)(__this, ___action0, ___p1, method)
// System.Void Stream`1<UniRx.Unit>::Dispose()
extern "C"  void Stream_1_Dispose_m3364282657_gshared (Stream_1_t1249425060 * __this, const MethodInfo* method);
#define Stream_1_Dispose_m3364282657(__this, method) ((  void (*) (Stream_1_t1249425060 *, const MethodInfo*))Stream_1_Dispose_m3364282657_gshared)(__this, method)
// System.Void Stream`1<UniRx.Unit>::SetInputStream(IStream`1<T>)
extern "C"  void Stream_1_SetInputStream_m1157679260_gshared (Stream_1_t1249425060 * __this, Il2CppObject* ___stream0, const MethodInfo* method);
#define Stream_1_SetInputStream_m1157679260(__this, ___stream0, method) ((  void (*) (Stream_1_t1249425060 *, Il2CppObject*, const MethodInfo*))Stream_1_SetInputStream_m1157679260_gshared)(__this, ___stream0, method)
// System.Void Stream`1<UniRx.Unit>::ClearInputStream()
extern "C"  void Stream_1_ClearInputStream_m3637070973_gshared (Stream_1_t1249425060 * __this, const MethodInfo* method);
#define Stream_1_ClearInputStream_m3637070973(__this, method) ((  void (*) (Stream_1_t1249425060 *, const MethodInfo*))Stream_1_ClearInputStream_m3637070973_gshared)(__this, method)
// System.Void Stream`1<UniRx.Unit>::TransactionIterationFinished()
extern "C"  void Stream_1_TransactionIterationFinished_m2425698513_gshared (Stream_1_t1249425060 * __this, const MethodInfo* method);
#define Stream_1_TransactionIterationFinished_m2425698513(__this, method) ((  void (*) (Stream_1_t1249425060 *, const MethodInfo*))Stream_1_TransactionIterationFinished_m2425698513_gshared)(__this, method)
// System.Void Stream`1<UniRx.Unit>::Unpack(System.Int32)
extern "C"  void Stream_1_Unpack_m3698641187_gshared (Stream_1_t1249425060 * __this, int32_t ___p0, const MethodInfo* method);
#define Stream_1_Unpack_m3698641187(__this, ___p0, method) ((  void (*) (Stream_1_t1249425060 *, int32_t, const MethodInfo*))Stream_1_Unpack_m3698641187_gshared)(__this, ___p0, method)
// System.Void Stream`1<UniRx.Unit>::<SetInputStream>m__1BA(T)
extern "C"  void Stream_1_U3CSetInputStreamU3Em__1BA_m3414567809_gshared (Stream_1_t1249425060 * __this, Unit_t2558286038  ___val0, const MethodInfo* method);
#define Stream_1_U3CSetInputStreamU3Em__1BA_m3414567809(__this, ___val0, method) ((  void (*) (Stream_1_t1249425060 *, Unit_t2558286038 , const MethodInfo*))Stream_1_U3CSetInputStreamU3Em__1BA_m3414567809_gshared)(__this, ___val0, method)
