﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type
struct Type_t;
// Zenject.ZenjectTypeInfo
struct ZenjectTypeInfo_t283213708;
// System.Type[]
struct TypeU5BU5D_t3431720054;
// System.Collections.Generic.List`1<System.Type>
struct List_1_t3576188904;
// System.Collections.Generic.IEnumerator`1<Zenject.InjectableInfo>
struct IEnumerator_1_t2630816222;
// Zenject.InjectableInfo
struct InjectableInfo_t1147709774;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// Zenject.InjectContext
struct InjectContext_t3456483891;
// System.String
struct String_t;
// System.Collections.Generic.IEnumerator`1<Zenject.ZenjectResolveException>
struct IEnumerator_1_t2684159447;
// Zenject.ZenjectResolveException
struct ZenjectResolveException_t1201052999;
// System.Func`2<System.Type,System.String>
struct Func_2_t1392394819;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.BindingValidator/<ValidateObjectGraph>c__Iterator4A
struct  U3CValidateObjectGraphU3Ec__Iterator4A_t3045653917  : public Il2CppObject
{
public:
	// System.Type Zenject.BindingValidator/<ValidateObjectGraph>c__Iterator4A::concreteType
	Type_t * ___concreteType_0;
	// Zenject.ZenjectTypeInfo Zenject.BindingValidator/<ValidateObjectGraph>c__Iterator4A::<typeInfo>__0
	ZenjectTypeInfo_t283213708 * ___U3CtypeInfoU3E__0_1;
	// System.Type[] Zenject.BindingValidator/<ValidateObjectGraph>c__Iterator4A::extras
	TypeU5BU5D_t3431720054* ___extras_2;
	// System.Collections.Generic.List`1<System.Type> Zenject.BindingValidator/<ValidateObjectGraph>c__Iterator4A::<extrasList>__1
	List_1_t3576188904 * ___U3CextrasListU3E__1_3;
	// System.Collections.Generic.IEnumerator`1<Zenject.InjectableInfo> Zenject.BindingValidator/<ValidateObjectGraph>c__Iterator4A::<$s_276>__2
	Il2CppObject* ___U3CU24s_276U3E__2_4;
	// Zenject.InjectableInfo Zenject.BindingValidator/<ValidateObjectGraph>c__Iterator4A::<dependInfo>__3
	InjectableInfo_t1147709774 * ___U3CdependInfoU3E__3_5;
	// Zenject.DiContainer Zenject.BindingValidator/<ValidateObjectGraph>c__Iterator4A::container
	DiContainer_t2383114449 * ___container_6;
	// Zenject.InjectContext Zenject.BindingValidator/<ValidateObjectGraph>c__Iterator4A::currentContext
	InjectContext_t3456483891 * ___currentContext_7;
	// System.String Zenject.BindingValidator/<ValidateObjectGraph>c__Iterator4A::concreteIdentifier
	String_t* ___concreteIdentifier_8;
	// Zenject.InjectContext Zenject.BindingValidator/<ValidateObjectGraph>c__Iterator4A::<context>__4
	InjectContext_t3456483891 * ___U3CcontextU3E__4_9;
	// System.Collections.Generic.IEnumerator`1<Zenject.ZenjectResolveException> Zenject.BindingValidator/<ValidateObjectGraph>c__Iterator4A::<$s_277>__5
	Il2CppObject* ___U3CU24s_277U3E__5_10;
	// Zenject.ZenjectResolveException Zenject.BindingValidator/<ValidateObjectGraph>c__Iterator4A::<error>__6
	ZenjectResolveException_t1201052999 * ___U3CerrorU3E__6_11;
	// System.Int32 Zenject.BindingValidator/<ValidateObjectGraph>c__Iterator4A::$PC
	int32_t ___U24PC_12;
	// Zenject.ZenjectResolveException Zenject.BindingValidator/<ValidateObjectGraph>c__Iterator4A::$current
	ZenjectResolveException_t1201052999 * ___U24current_13;
	// System.Type Zenject.BindingValidator/<ValidateObjectGraph>c__Iterator4A::<$>concreteType
	Type_t * ___U3CU24U3EconcreteType_14;
	// System.Type[] Zenject.BindingValidator/<ValidateObjectGraph>c__Iterator4A::<$>extras
	TypeU5BU5D_t3431720054* ___U3CU24U3Eextras_15;
	// Zenject.DiContainer Zenject.BindingValidator/<ValidateObjectGraph>c__Iterator4A::<$>container
	DiContainer_t2383114449 * ___U3CU24U3Econtainer_16;
	// Zenject.InjectContext Zenject.BindingValidator/<ValidateObjectGraph>c__Iterator4A::<$>currentContext
	InjectContext_t3456483891 * ___U3CU24U3EcurrentContext_17;
	// System.String Zenject.BindingValidator/<ValidateObjectGraph>c__Iterator4A::<$>concreteIdentifier
	String_t* ___U3CU24U3EconcreteIdentifier_18;

public:
	inline static int32_t get_offset_of_concreteType_0() { return static_cast<int32_t>(offsetof(U3CValidateObjectGraphU3Ec__Iterator4A_t3045653917, ___concreteType_0)); }
	inline Type_t * get_concreteType_0() const { return ___concreteType_0; }
	inline Type_t ** get_address_of_concreteType_0() { return &___concreteType_0; }
	inline void set_concreteType_0(Type_t * value)
	{
		___concreteType_0 = value;
		Il2CppCodeGenWriteBarrier(&___concreteType_0, value);
	}

	inline static int32_t get_offset_of_U3CtypeInfoU3E__0_1() { return static_cast<int32_t>(offsetof(U3CValidateObjectGraphU3Ec__Iterator4A_t3045653917, ___U3CtypeInfoU3E__0_1)); }
	inline ZenjectTypeInfo_t283213708 * get_U3CtypeInfoU3E__0_1() const { return ___U3CtypeInfoU3E__0_1; }
	inline ZenjectTypeInfo_t283213708 ** get_address_of_U3CtypeInfoU3E__0_1() { return &___U3CtypeInfoU3E__0_1; }
	inline void set_U3CtypeInfoU3E__0_1(ZenjectTypeInfo_t283213708 * value)
	{
		___U3CtypeInfoU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtypeInfoU3E__0_1, value);
	}

	inline static int32_t get_offset_of_extras_2() { return static_cast<int32_t>(offsetof(U3CValidateObjectGraphU3Ec__Iterator4A_t3045653917, ___extras_2)); }
	inline TypeU5BU5D_t3431720054* get_extras_2() const { return ___extras_2; }
	inline TypeU5BU5D_t3431720054** get_address_of_extras_2() { return &___extras_2; }
	inline void set_extras_2(TypeU5BU5D_t3431720054* value)
	{
		___extras_2 = value;
		Il2CppCodeGenWriteBarrier(&___extras_2, value);
	}

	inline static int32_t get_offset_of_U3CextrasListU3E__1_3() { return static_cast<int32_t>(offsetof(U3CValidateObjectGraphU3Ec__Iterator4A_t3045653917, ___U3CextrasListU3E__1_3)); }
	inline List_1_t3576188904 * get_U3CextrasListU3E__1_3() const { return ___U3CextrasListU3E__1_3; }
	inline List_1_t3576188904 ** get_address_of_U3CextrasListU3E__1_3() { return &___U3CextrasListU3E__1_3; }
	inline void set_U3CextrasListU3E__1_3(List_1_t3576188904 * value)
	{
		___U3CextrasListU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CextrasListU3E__1_3, value);
	}

	inline static int32_t get_offset_of_U3CU24s_276U3E__2_4() { return static_cast<int32_t>(offsetof(U3CValidateObjectGraphU3Ec__Iterator4A_t3045653917, ___U3CU24s_276U3E__2_4)); }
	inline Il2CppObject* get_U3CU24s_276U3E__2_4() const { return ___U3CU24s_276U3E__2_4; }
	inline Il2CppObject** get_address_of_U3CU24s_276U3E__2_4() { return &___U3CU24s_276U3E__2_4; }
	inline void set_U3CU24s_276U3E__2_4(Il2CppObject* value)
	{
		___U3CU24s_276U3E__2_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_276U3E__2_4, value);
	}

	inline static int32_t get_offset_of_U3CdependInfoU3E__3_5() { return static_cast<int32_t>(offsetof(U3CValidateObjectGraphU3Ec__Iterator4A_t3045653917, ___U3CdependInfoU3E__3_5)); }
	inline InjectableInfo_t1147709774 * get_U3CdependInfoU3E__3_5() const { return ___U3CdependInfoU3E__3_5; }
	inline InjectableInfo_t1147709774 ** get_address_of_U3CdependInfoU3E__3_5() { return &___U3CdependInfoU3E__3_5; }
	inline void set_U3CdependInfoU3E__3_5(InjectableInfo_t1147709774 * value)
	{
		___U3CdependInfoU3E__3_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CdependInfoU3E__3_5, value);
	}

	inline static int32_t get_offset_of_container_6() { return static_cast<int32_t>(offsetof(U3CValidateObjectGraphU3Ec__Iterator4A_t3045653917, ___container_6)); }
	inline DiContainer_t2383114449 * get_container_6() const { return ___container_6; }
	inline DiContainer_t2383114449 ** get_address_of_container_6() { return &___container_6; }
	inline void set_container_6(DiContainer_t2383114449 * value)
	{
		___container_6 = value;
		Il2CppCodeGenWriteBarrier(&___container_6, value);
	}

	inline static int32_t get_offset_of_currentContext_7() { return static_cast<int32_t>(offsetof(U3CValidateObjectGraphU3Ec__Iterator4A_t3045653917, ___currentContext_7)); }
	inline InjectContext_t3456483891 * get_currentContext_7() const { return ___currentContext_7; }
	inline InjectContext_t3456483891 ** get_address_of_currentContext_7() { return &___currentContext_7; }
	inline void set_currentContext_7(InjectContext_t3456483891 * value)
	{
		___currentContext_7 = value;
		Il2CppCodeGenWriteBarrier(&___currentContext_7, value);
	}

	inline static int32_t get_offset_of_concreteIdentifier_8() { return static_cast<int32_t>(offsetof(U3CValidateObjectGraphU3Ec__Iterator4A_t3045653917, ___concreteIdentifier_8)); }
	inline String_t* get_concreteIdentifier_8() const { return ___concreteIdentifier_8; }
	inline String_t** get_address_of_concreteIdentifier_8() { return &___concreteIdentifier_8; }
	inline void set_concreteIdentifier_8(String_t* value)
	{
		___concreteIdentifier_8 = value;
		Il2CppCodeGenWriteBarrier(&___concreteIdentifier_8, value);
	}

	inline static int32_t get_offset_of_U3CcontextU3E__4_9() { return static_cast<int32_t>(offsetof(U3CValidateObjectGraphU3Ec__Iterator4A_t3045653917, ___U3CcontextU3E__4_9)); }
	inline InjectContext_t3456483891 * get_U3CcontextU3E__4_9() const { return ___U3CcontextU3E__4_9; }
	inline InjectContext_t3456483891 ** get_address_of_U3CcontextU3E__4_9() { return &___U3CcontextU3E__4_9; }
	inline void set_U3CcontextU3E__4_9(InjectContext_t3456483891 * value)
	{
		___U3CcontextU3E__4_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcontextU3E__4_9, value);
	}

	inline static int32_t get_offset_of_U3CU24s_277U3E__5_10() { return static_cast<int32_t>(offsetof(U3CValidateObjectGraphU3Ec__Iterator4A_t3045653917, ___U3CU24s_277U3E__5_10)); }
	inline Il2CppObject* get_U3CU24s_277U3E__5_10() const { return ___U3CU24s_277U3E__5_10; }
	inline Il2CppObject** get_address_of_U3CU24s_277U3E__5_10() { return &___U3CU24s_277U3E__5_10; }
	inline void set_U3CU24s_277U3E__5_10(Il2CppObject* value)
	{
		___U3CU24s_277U3E__5_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_277U3E__5_10, value);
	}

	inline static int32_t get_offset_of_U3CerrorU3E__6_11() { return static_cast<int32_t>(offsetof(U3CValidateObjectGraphU3Ec__Iterator4A_t3045653917, ___U3CerrorU3E__6_11)); }
	inline ZenjectResolveException_t1201052999 * get_U3CerrorU3E__6_11() const { return ___U3CerrorU3E__6_11; }
	inline ZenjectResolveException_t1201052999 ** get_address_of_U3CerrorU3E__6_11() { return &___U3CerrorU3E__6_11; }
	inline void set_U3CerrorU3E__6_11(ZenjectResolveException_t1201052999 * value)
	{
		___U3CerrorU3E__6_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CerrorU3E__6_11, value);
	}

	inline static int32_t get_offset_of_U24PC_12() { return static_cast<int32_t>(offsetof(U3CValidateObjectGraphU3Ec__Iterator4A_t3045653917, ___U24PC_12)); }
	inline int32_t get_U24PC_12() const { return ___U24PC_12; }
	inline int32_t* get_address_of_U24PC_12() { return &___U24PC_12; }
	inline void set_U24PC_12(int32_t value)
	{
		___U24PC_12 = value;
	}

	inline static int32_t get_offset_of_U24current_13() { return static_cast<int32_t>(offsetof(U3CValidateObjectGraphU3Ec__Iterator4A_t3045653917, ___U24current_13)); }
	inline ZenjectResolveException_t1201052999 * get_U24current_13() const { return ___U24current_13; }
	inline ZenjectResolveException_t1201052999 ** get_address_of_U24current_13() { return &___U24current_13; }
	inline void set_U24current_13(ZenjectResolveException_t1201052999 * value)
	{
		___U24current_13 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_13, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EconcreteType_14() { return static_cast<int32_t>(offsetof(U3CValidateObjectGraphU3Ec__Iterator4A_t3045653917, ___U3CU24U3EconcreteType_14)); }
	inline Type_t * get_U3CU24U3EconcreteType_14() const { return ___U3CU24U3EconcreteType_14; }
	inline Type_t ** get_address_of_U3CU24U3EconcreteType_14() { return &___U3CU24U3EconcreteType_14; }
	inline void set_U3CU24U3EconcreteType_14(Type_t * value)
	{
		___U3CU24U3EconcreteType_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3EconcreteType_14, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eextras_15() { return static_cast<int32_t>(offsetof(U3CValidateObjectGraphU3Ec__Iterator4A_t3045653917, ___U3CU24U3Eextras_15)); }
	inline TypeU5BU5D_t3431720054* get_U3CU24U3Eextras_15() const { return ___U3CU24U3Eextras_15; }
	inline TypeU5BU5D_t3431720054** get_address_of_U3CU24U3Eextras_15() { return &___U3CU24U3Eextras_15; }
	inline void set_U3CU24U3Eextras_15(TypeU5BU5D_t3431720054* value)
	{
		___U3CU24U3Eextras_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eextras_15, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Econtainer_16() { return static_cast<int32_t>(offsetof(U3CValidateObjectGraphU3Ec__Iterator4A_t3045653917, ___U3CU24U3Econtainer_16)); }
	inline DiContainer_t2383114449 * get_U3CU24U3Econtainer_16() const { return ___U3CU24U3Econtainer_16; }
	inline DiContainer_t2383114449 ** get_address_of_U3CU24U3Econtainer_16() { return &___U3CU24U3Econtainer_16; }
	inline void set_U3CU24U3Econtainer_16(DiContainer_t2383114449 * value)
	{
		___U3CU24U3Econtainer_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Econtainer_16, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EcurrentContext_17() { return static_cast<int32_t>(offsetof(U3CValidateObjectGraphU3Ec__Iterator4A_t3045653917, ___U3CU24U3EcurrentContext_17)); }
	inline InjectContext_t3456483891 * get_U3CU24U3EcurrentContext_17() const { return ___U3CU24U3EcurrentContext_17; }
	inline InjectContext_t3456483891 ** get_address_of_U3CU24U3EcurrentContext_17() { return &___U3CU24U3EcurrentContext_17; }
	inline void set_U3CU24U3EcurrentContext_17(InjectContext_t3456483891 * value)
	{
		___U3CU24U3EcurrentContext_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3EcurrentContext_17, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EconcreteIdentifier_18() { return static_cast<int32_t>(offsetof(U3CValidateObjectGraphU3Ec__Iterator4A_t3045653917, ___U3CU24U3EconcreteIdentifier_18)); }
	inline String_t* get_U3CU24U3EconcreteIdentifier_18() const { return ___U3CU24U3EconcreteIdentifier_18; }
	inline String_t** get_address_of_U3CU24U3EconcreteIdentifier_18() { return &___U3CU24U3EconcreteIdentifier_18; }
	inline void set_U3CU24U3EconcreteIdentifier_18(String_t* value)
	{
		___U3CU24U3EconcreteIdentifier_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3EconcreteIdentifier_18, value);
	}
};

struct U3CValidateObjectGraphU3Ec__Iterator4A_t3045653917_StaticFields
{
public:
	// System.Func`2<System.Type,System.String> Zenject.BindingValidator/<ValidateObjectGraph>c__Iterator4A::<>f__am$cache13
	Func_2_t1392394819 * ___U3CU3Ef__amU24cache13_19;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache13_19() { return static_cast<int32_t>(offsetof(U3CValidateObjectGraphU3Ec__Iterator4A_t3045653917_StaticFields, ___U3CU3Ef__amU24cache13_19)); }
	inline Func_2_t1392394819 * get_U3CU3Ef__amU24cache13_19() const { return ___U3CU3Ef__amU24cache13_19; }
	inline Func_2_t1392394819 ** get_address_of_U3CU3Ef__amU24cache13_19() { return &___U3CU3Ef__amU24cache13_19; }
	inline void set_U3CU3Ef__amU24cache13_19(Func_2_t1392394819 * value)
	{
		___U3CU3Ef__amU24cache13_19 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache13_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
