﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.FactoryNested`2<System.Object,System.Object>
struct FactoryNested_2_t2234585871;
// Zenject.IFactory`1<System.Object>
struct IFactory_1_t2124284520;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Zenject.FactoryNested`2<System.Object,System.Object>::.ctor(Zenject.IFactory`1<TConcrete>)
extern "C"  void FactoryNested_2__ctor_m1422373693_gshared (FactoryNested_2_t2234585871 * __this, Il2CppObject* ___concreteFactory0, const MethodInfo* method);
#define FactoryNested_2__ctor_m1422373693(__this, ___concreteFactory0, method) ((  void (*) (FactoryNested_2_t2234585871 *, Il2CppObject*, const MethodInfo*))FactoryNested_2__ctor_m1422373693_gshared)(__this, ___concreteFactory0, method)
// TContract Zenject.FactoryNested`2<System.Object,System.Object>::Create()
extern "C"  Il2CppObject * FactoryNested_2_Create_m3988050152_gshared (FactoryNested_2_t2234585871 * __this, const MethodInfo* method);
#define FactoryNested_2_Create_m3988050152(__this, method) ((  Il2CppObject * (*) (FactoryNested_2_t2234585871 *, const MethodInfo*))FactoryNested_2_Create_m3988050152_gshared)(__this, method)
