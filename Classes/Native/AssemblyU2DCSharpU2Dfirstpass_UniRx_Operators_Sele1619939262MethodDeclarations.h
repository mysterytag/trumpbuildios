﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectManyObservable`3/SelectManyEnumerableObserverWithIndex<System.Double,System.Double,System.Double>
struct SelectManyEnumerableObserverWithIndex_t1619939262;
// UniRx.Operators.SelectManyObservable`3<System.Double,System.Double,System.Double>
struct SelectManyObservable_3_t44588665;
// UniRx.IObserver`1<System.Double>
struct IObserver_1_t2746515517;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyEnumerableObserverWithIndex<System.Double,System.Double,System.Double>::.ctor(UniRx.Operators.SelectManyObservable`3<TSource,TCollection,TResult>,UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void SelectManyEnumerableObserverWithIndex__ctor_m2849821984_gshared (SelectManyEnumerableObserverWithIndex_t1619939262 * __this, SelectManyObservable_3_t44588665 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define SelectManyEnumerableObserverWithIndex__ctor_m2849821984(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (SelectManyEnumerableObserverWithIndex_t1619939262 *, SelectManyObservable_3_t44588665 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SelectManyEnumerableObserverWithIndex__ctor_m2849821984_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.SelectManyObservable`3/SelectManyEnumerableObserverWithIndex<System.Double,System.Double,System.Double>::Run()
extern "C"  Il2CppObject * SelectManyEnumerableObserverWithIndex_Run_m2201956965_gshared (SelectManyEnumerableObserverWithIndex_t1619939262 * __this, const MethodInfo* method);
#define SelectManyEnumerableObserverWithIndex_Run_m2201956965(__this, method) ((  Il2CppObject * (*) (SelectManyEnumerableObserverWithIndex_t1619939262 *, const MethodInfo*))SelectManyEnumerableObserverWithIndex_Run_m2201956965_gshared)(__this, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyEnumerableObserverWithIndex<System.Double,System.Double,System.Double>::OnNext(TSource)
extern "C"  void SelectManyEnumerableObserverWithIndex_OnNext_m1707595150_gshared (SelectManyEnumerableObserverWithIndex_t1619939262 * __this, double ___value0, const MethodInfo* method);
#define SelectManyEnumerableObserverWithIndex_OnNext_m1707595150(__this, ___value0, method) ((  void (*) (SelectManyEnumerableObserverWithIndex_t1619939262 *, double, const MethodInfo*))SelectManyEnumerableObserverWithIndex_OnNext_m1707595150_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyEnumerableObserverWithIndex<System.Double,System.Double,System.Double>::OnError(System.Exception)
extern "C"  void SelectManyEnumerableObserverWithIndex_OnError_m357923704_gshared (SelectManyEnumerableObserverWithIndex_t1619939262 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define SelectManyEnumerableObserverWithIndex_OnError_m357923704(__this, ___error0, method) ((  void (*) (SelectManyEnumerableObserverWithIndex_t1619939262 *, Exception_t1967233988 *, const MethodInfo*))SelectManyEnumerableObserverWithIndex_OnError_m357923704_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyEnumerableObserverWithIndex<System.Double,System.Double,System.Double>::OnCompleted()
extern "C"  void SelectManyEnumerableObserverWithIndex_OnCompleted_m1864868555_gshared (SelectManyEnumerableObserverWithIndex_t1619939262 * __this, const MethodInfo* method);
#define SelectManyEnumerableObserverWithIndex_OnCompleted_m1864868555(__this, method) ((  void (*) (SelectManyEnumerableObserverWithIndex_t1619939262 *, const MethodInfo*))SelectManyEnumerableObserverWithIndex_OnCompleted_m1864868555_gshared)(__this, method)
