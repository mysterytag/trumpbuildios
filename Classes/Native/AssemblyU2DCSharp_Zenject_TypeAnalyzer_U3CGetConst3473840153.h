﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type
struct Type_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TypeAnalyzer/<GetConstructorInjectables>c__AnonStorey181
struct  U3CGetConstructorInjectablesU3Ec__AnonStorey181_t3473840153  : public Il2CppObject
{
public:
	// System.Type Zenject.TypeAnalyzer/<GetConstructorInjectables>c__AnonStorey181::parentType
	Type_t * ___parentType_0;

public:
	inline static int32_t get_offset_of_parentType_0() { return static_cast<int32_t>(offsetof(U3CGetConstructorInjectablesU3Ec__AnonStorey181_t3473840153, ___parentType_0)); }
	inline Type_t * get_parentType_0() const { return ___parentType_0; }
	inline Type_t ** get_address_of_parentType_0() { return &___parentType_0; }
	inline void set_parentType_0(Type_t * value)
	{
		___parentType_0 = value;
		Il2CppCodeGenWriteBarrier(&___parentType_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
