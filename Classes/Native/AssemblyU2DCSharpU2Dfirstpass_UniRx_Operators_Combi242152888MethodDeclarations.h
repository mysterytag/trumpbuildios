﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.CombineLatestObservable`1/CombineLatest<System.Boolean>
struct CombineLatest_t242152888;
// UniRx.Operators.CombineLatestObservable`1<System.Boolean>
struct CombineLatestObservable_1_t2883136337;
// UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Boolean>>
struct IObserver_1_t294529262;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Collections.Generic.IList`1<System.Boolean>
struct IList_1_t2377497655;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.CombineLatestObservable`1/CombineLatest<System.Boolean>::.ctor(UniRx.Operators.CombineLatestObservable`1<T>,UniRx.IObserver`1<System.Collections.Generic.IList`1<T>>,System.IDisposable)
extern "C"  void CombineLatest__ctor_m2966111004_gshared (CombineLatest_t242152888 * __this, CombineLatestObservable_1_t2883136337 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define CombineLatest__ctor_m2966111004(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (CombineLatest_t242152888 *, CombineLatestObservable_1_t2883136337 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))CombineLatest__ctor_m2966111004_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.CombineLatestObservable`1/CombineLatest<System.Boolean>::Run()
extern "C"  Il2CppObject * CombineLatest_Run_m1253482696_gshared (CombineLatest_t242152888 * __this, const MethodInfo* method);
#define CombineLatest_Run_m1253482696(__this, method) ((  Il2CppObject * (*) (CombineLatest_t242152888 *, const MethodInfo*))CombineLatest_Run_m1253482696_gshared)(__this, method)
// System.Void UniRx.Operators.CombineLatestObservable`1/CombineLatest<System.Boolean>::Publish(System.Int32)
extern "C"  void CombineLatest_Publish_m761439650_gshared (CombineLatest_t242152888 * __this, int32_t ___index0, const MethodInfo* method);
#define CombineLatest_Publish_m761439650(__this, ___index0, method) ((  void (*) (CombineLatest_t242152888 *, int32_t, const MethodInfo*))CombineLatest_Publish_m761439650_gshared)(__this, ___index0, method)
// System.Void UniRx.Operators.CombineLatestObservable`1/CombineLatest<System.Boolean>::OnNext(System.Collections.Generic.IList`1<T>)
extern "C"  void CombineLatest_OnNext_m4100036611_gshared (CombineLatest_t242152888 * __this, Il2CppObject* ___value0, const MethodInfo* method);
#define CombineLatest_OnNext_m4100036611(__this, ___value0, method) ((  void (*) (CombineLatest_t242152888 *, Il2CppObject*, const MethodInfo*))CombineLatest_OnNext_m4100036611_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.CombineLatestObservable`1/CombineLatest<System.Boolean>::OnError(System.Exception)
extern "C"  void CombineLatest_OnError_m3555055707_gshared (CombineLatest_t242152888 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define CombineLatest_OnError_m3555055707(__this, ___error0, method) ((  void (*) (CombineLatest_t242152888 *, Exception_t1967233988 *, const MethodInfo*))CombineLatest_OnError_m3555055707_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.CombineLatestObservable`1/CombineLatest<System.Boolean>::OnCompleted()
extern "C"  void CombineLatest_OnCompleted_m3335126062_gshared (CombineLatest_t242152888 * __this, const MethodInfo* method);
#define CombineLatest_OnCompleted_m3335126062(__this, method) ((  void (*) (CombineLatest_t242152888 *, const MethodInfo*))CombineLatest_OnCompleted_m3335126062_gshared)(__this, method)
