﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Zenject.DecoratorInstaller[]
struct DecoratorInstallerU5BU5D_t3255381015;
// Zenject.MonoInstaller[]
struct MonoInstallerU5BU5D_t2181499551;
// System.Action`1<Zenject.DiContainer>
struct Action_1_t2531567154;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SceneDecoratorCompositionRoot
struct  SceneDecoratorCompositionRoot_t3441797106  : public MonoBehaviour_t3012272455
{
public:
	// System.String Zenject.SceneDecoratorCompositionRoot::SceneName
	String_t* ___SceneName_2;
	// Zenject.DecoratorInstaller[] Zenject.SceneDecoratorCompositionRoot::DecoratorInstallers
	DecoratorInstallerU5BU5D_t3255381015* ___DecoratorInstallers_3;
	// Zenject.MonoInstaller[] Zenject.SceneDecoratorCompositionRoot::PreInstallers
	MonoInstallerU5BU5D_t2181499551* ___PreInstallers_4;
	// Zenject.MonoInstaller[] Zenject.SceneDecoratorCompositionRoot::PostInstallers
	MonoInstallerU5BU5D_t2181499551* ___PostInstallers_5;
	// System.Action`1<Zenject.DiContainer> Zenject.SceneDecoratorCompositionRoot::_beforeInstallHooks
	Action_1_t2531567154 * ____beforeInstallHooks_6;
	// System.Action`1<Zenject.DiContainer> Zenject.SceneDecoratorCompositionRoot::_afterInstallHooks
	Action_1_t2531567154 * ____afterInstallHooks_7;

public:
	inline static int32_t get_offset_of_SceneName_2() { return static_cast<int32_t>(offsetof(SceneDecoratorCompositionRoot_t3441797106, ___SceneName_2)); }
	inline String_t* get_SceneName_2() const { return ___SceneName_2; }
	inline String_t** get_address_of_SceneName_2() { return &___SceneName_2; }
	inline void set_SceneName_2(String_t* value)
	{
		___SceneName_2 = value;
		Il2CppCodeGenWriteBarrier(&___SceneName_2, value);
	}

	inline static int32_t get_offset_of_DecoratorInstallers_3() { return static_cast<int32_t>(offsetof(SceneDecoratorCompositionRoot_t3441797106, ___DecoratorInstallers_3)); }
	inline DecoratorInstallerU5BU5D_t3255381015* get_DecoratorInstallers_3() const { return ___DecoratorInstallers_3; }
	inline DecoratorInstallerU5BU5D_t3255381015** get_address_of_DecoratorInstallers_3() { return &___DecoratorInstallers_3; }
	inline void set_DecoratorInstallers_3(DecoratorInstallerU5BU5D_t3255381015* value)
	{
		___DecoratorInstallers_3 = value;
		Il2CppCodeGenWriteBarrier(&___DecoratorInstallers_3, value);
	}

	inline static int32_t get_offset_of_PreInstallers_4() { return static_cast<int32_t>(offsetof(SceneDecoratorCompositionRoot_t3441797106, ___PreInstallers_4)); }
	inline MonoInstallerU5BU5D_t2181499551* get_PreInstallers_4() const { return ___PreInstallers_4; }
	inline MonoInstallerU5BU5D_t2181499551** get_address_of_PreInstallers_4() { return &___PreInstallers_4; }
	inline void set_PreInstallers_4(MonoInstallerU5BU5D_t2181499551* value)
	{
		___PreInstallers_4 = value;
		Il2CppCodeGenWriteBarrier(&___PreInstallers_4, value);
	}

	inline static int32_t get_offset_of_PostInstallers_5() { return static_cast<int32_t>(offsetof(SceneDecoratorCompositionRoot_t3441797106, ___PostInstallers_5)); }
	inline MonoInstallerU5BU5D_t2181499551* get_PostInstallers_5() const { return ___PostInstallers_5; }
	inline MonoInstallerU5BU5D_t2181499551** get_address_of_PostInstallers_5() { return &___PostInstallers_5; }
	inline void set_PostInstallers_5(MonoInstallerU5BU5D_t2181499551* value)
	{
		___PostInstallers_5 = value;
		Il2CppCodeGenWriteBarrier(&___PostInstallers_5, value);
	}

	inline static int32_t get_offset_of__beforeInstallHooks_6() { return static_cast<int32_t>(offsetof(SceneDecoratorCompositionRoot_t3441797106, ____beforeInstallHooks_6)); }
	inline Action_1_t2531567154 * get__beforeInstallHooks_6() const { return ____beforeInstallHooks_6; }
	inline Action_1_t2531567154 ** get_address_of__beforeInstallHooks_6() { return &____beforeInstallHooks_6; }
	inline void set__beforeInstallHooks_6(Action_1_t2531567154 * value)
	{
		____beforeInstallHooks_6 = value;
		Il2CppCodeGenWriteBarrier(&____beforeInstallHooks_6, value);
	}

	inline static int32_t get_offset_of__afterInstallHooks_7() { return static_cast<int32_t>(offsetof(SceneDecoratorCompositionRoot_t3441797106, ____afterInstallHooks_7)); }
	inline Action_1_t2531567154 * get__afterInstallHooks_7() const { return ____afterInstallHooks_7; }
	inline Action_1_t2531567154 ** get_address_of__afterInstallHooks_7() { return &____afterInstallHooks_7; }
	inline void set__afterInstallHooks_7(Action_1_t2531567154 * value)
	{
		____afterInstallHooks_7 = value;
		Il2CppCodeGenWriteBarrier(&____afterInstallHooks_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
