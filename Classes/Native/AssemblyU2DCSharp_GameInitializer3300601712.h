﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GameController
struct GameController_t2782302542;
// UnityEngine.Canvas
struct Canvas_t3534013893;

#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_DateTime339033936.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameInitializer
struct  GameInitializer_t3300601712  : public Il2CppObject
{
public:
	// GameController GameInitializer::GameController
	GameController_t2782302542 * ___GameController_1;
	// UnityEngine.Canvas GameInitializer::SplashCanvas
	Canvas_t3534013893 * ___SplashCanvas_2;

public:
	inline static int32_t get_offset_of_GameController_1() { return static_cast<int32_t>(offsetof(GameInitializer_t3300601712, ___GameController_1)); }
	inline GameController_t2782302542 * get_GameController_1() const { return ___GameController_1; }
	inline GameController_t2782302542 ** get_address_of_GameController_1() { return &___GameController_1; }
	inline void set_GameController_1(GameController_t2782302542 * value)
	{
		___GameController_1 = value;
		Il2CppCodeGenWriteBarrier(&___GameController_1, value);
	}

	inline static int32_t get_offset_of_SplashCanvas_2() { return static_cast<int32_t>(offsetof(GameInitializer_t3300601712, ___SplashCanvas_2)); }
	inline Canvas_t3534013893 * get_SplashCanvas_2() const { return ___SplashCanvas_2; }
	inline Canvas_t3534013893 ** get_address_of_SplashCanvas_2() { return &___SplashCanvas_2; }
	inline void set_SplashCanvas_2(Canvas_t3534013893 * value)
	{
		___SplashCanvas_2 = value;
		Il2CppCodeGenWriteBarrier(&___SplashCanvas_2, value);
	}
};

struct GameInitializer_t3300601712_StaticFields
{
public:
	// System.DateTime GameInitializer::startLoading
	DateTime_t339033936  ___startLoading_0;

public:
	inline static int32_t get_offset_of_startLoading_0() { return static_cast<int32_t>(offsetof(GameInitializer_t3300601712_StaticFields, ___startLoading_0)); }
	inline DateTime_t339033936  get_startLoading_0() const { return ___startLoading_0; }
	inline DateTime_t339033936 * get_address_of_startLoading_0() { return &___startLoading_0; }
	inline void set_startLoading_0(DateTime_t339033936  value)
	{
		___startLoading_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
