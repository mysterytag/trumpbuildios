﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StreamAPI/<FirstOnly>c__AnonStorey136`1<System.Object>
struct U3CFirstOnlyU3Ec__AnonStorey136_1_t276735029;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<System.Object>
struct Action_1_t985559125;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"

// System.Void StreamAPI/<FirstOnly>c__AnonStorey136`1<System.Object>::.ctor()
extern "C"  void U3CFirstOnlyU3Ec__AnonStorey136_1__ctor_m1609024998_gshared (U3CFirstOnlyU3Ec__AnonStorey136_1_t276735029 * __this, const MethodInfo* method);
#define U3CFirstOnlyU3Ec__AnonStorey136_1__ctor_m1609024998(__this, method) ((  void (*) (U3CFirstOnlyU3Ec__AnonStorey136_1_t276735029 *, const MethodInfo*))U3CFirstOnlyU3Ec__AnonStorey136_1__ctor_m1609024998_gshared)(__this, method)
// System.IDisposable StreamAPI/<FirstOnly>c__AnonStorey136`1<System.Object>::<>m__1C6(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * U3CFirstOnlyU3Ec__AnonStorey136_1_U3CU3Em__1C6_m268636888_gshared (U3CFirstOnlyU3Ec__AnonStorey136_1_t276735029 * __this, Action_1_t985559125 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define U3CFirstOnlyU3Ec__AnonStorey136_1_U3CU3Em__1C6_m268636888(__this, ___reaction0, ___p1, method) ((  Il2CppObject * (*) (U3CFirstOnlyU3Ec__AnonStorey136_1_t276735029 *, Action_1_t985559125 *, int32_t, const MethodInfo*))U3CFirstOnlyU3Ec__AnonStorey136_1_U3CU3Em__1C6_m268636888_gshared)(__this, ___reaction0, ___p1, method)
