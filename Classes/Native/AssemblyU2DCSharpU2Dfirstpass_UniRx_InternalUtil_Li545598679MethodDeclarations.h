﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.ListObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>
struct ListObserver_1_t545598679;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>>
struct ImmutableList_1_t3523184410;
// System.Exception
struct Exception_t1967233988;
// UniRx.IObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>
struct IObserver_1_t2462979911;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_DictionaryAddEv250981008.h"

// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m707792948_gshared (ListObserver_1_t545598679 * __this, ImmutableList_1_t3523184410 * ___observers0, const MethodInfo* method);
#define ListObserver_1__ctor_m707792948(__this, ___observers0, method) ((  void (*) (ListObserver_1_t545598679 *, ImmutableList_1_t3523184410 *, const MethodInfo*))ListObserver_1__ctor_m707792948_gshared)(__this, ___observers0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m1568862060_gshared (ListObserver_1_t545598679 * __this, const MethodInfo* method);
#define ListObserver_1_OnCompleted_m1568862060(__this, method) ((  void (*) (ListObserver_1_t545598679 *, const MethodInfo*))ListObserver_1_OnCompleted_m1568862060_gshared)(__this, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m965074585_gshared (ListObserver_1_t545598679 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ListObserver_1_OnError_m965074585(__this, ___error0, method) ((  void (*) (ListObserver_1_t545598679 *, Exception_t1967233988 *, const MethodInfo*))ListObserver_1_OnError_m965074585_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m3418232714_gshared (ListObserver_1_t545598679 * __this, DictionaryAddEvent_2_t250981008  ___value0, const MethodInfo* method);
#define ListObserver_1_OnNext_m3418232714(__this, ___value0, method) ((  void (*) (ListObserver_1_t545598679 *, DictionaryAddEvent_2_t250981008 , const MethodInfo*))ListObserver_1_OnNext_m3418232714_gshared)(__this, ___value0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m1799248348_gshared (ListObserver_1_t545598679 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Add_m1799248348(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t545598679 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Add_m1799248348_gshared)(__this, ___observer0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m649126895_gshared (ListObserver_1_t545598679 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Remove_m649126895(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t545598679 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Remove_m649126895_gshared)(__this, ___observer0, method)
