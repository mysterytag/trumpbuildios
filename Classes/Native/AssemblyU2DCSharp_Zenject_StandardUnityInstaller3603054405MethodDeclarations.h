﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.StandardUnityInstaller
struct StandardUnityInstaller_t3603054405;

#include "codegen/il2cpp-codegen.h"

// System.Void Zenject.StandardUnityInstaller::.ctor()
extern "C"  void StandardUnityInstaller__ctor_m3600671948 (StandardUnityInstaller_t3603054405 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.StandardUnityInstaller::InstallBindings()
extern "C"  void StandardUnityInstaller_InstallBindings_m2347478451 (StandardUnityInstaller_t3603054405 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
