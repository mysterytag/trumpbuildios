﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SimpleJSON.JSONClass/<Remove>c__AnonStorey2E
struct U3CRemoveU3Ec__AnonStorey2E_t1556685308;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21706851834.h"

// System.Void SimpleJSON.JSONClass/<Remove>c__AnonStorey2E::.ctor()
extern "C"  void U3CRemoveU3Ec__AnonStorey2E__ctor_m2732807750 (U3CRemoveU3Ec__AnonStorey2E_t1556685308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SimpleJSON.JSONClass/<Remove>c__AnonStorey2E::<>m__8(System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode>)
extern "C"  bool U3CRemoveU3Ec__AnonStorey2E_U3CU3Em__8_m1921890812 (U3CRemoveU3Ec__AnonStorey2E_t1556685308 * __this, KeyValuePair_2_t1706851834  ___k0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
