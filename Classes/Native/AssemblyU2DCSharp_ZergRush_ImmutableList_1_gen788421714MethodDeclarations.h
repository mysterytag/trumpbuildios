﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen340490602MethodDeclarations.h"

// System.Void ZergRush.ImmutableList`1<System.Action`1<System.Collections.Generic.ICollection`1<DonateButton>>>::.ctor()
#define ImmutableList_1__ctor_m182348106(__this, method) ((  void (*) (ImmutableList_1_t788421714 *, const MethodInfo*))ImmutableList_1__ctor_m99192906_gshared)(__this, method)
// System.Void ZergRush.ImmutableList`1<System.Action`1<System.Collections.Generic.ICollection`1<DonateButton>>>::.ctor(T)
#define ImmutableList_1__ctor_m1357825364(__this, ___single0, method) ((  void (*) (ImmutableList_1_t788421714 *, Action_1_t1285037532 *, const MethodInfo*))ImmutableList_1__ctor_m3074981460_gshared)(__this, ___single0, method)
// System.Void ZergRush.ImmutableList`1<System.Action`1<System.Collections.Generic.ICollection`1<DonateButton>>>::.ctor(T[])
#define ImmutableList_1__ctor_m3495135090(__this, ___data0, method) ((  void (*) (ImmutableList_1_t788421714 *, Action_1U5BU5D_t31376181*, const MethodInfo*))ImmutableList_1__ctor_m119734386_gshared)(__this, ___data0, method)
// System.Collections.IEnumerator ZergRush.ImmutableList`1<System.Action`1<System.Collections.Generic.ICollection`1<DonateButton>>>::System.Collections.IEnumerable.GetEnumerator()
#define ImmutableList_1_System_Collections_IEnumerable_GetEnumerator_m2487653591(__this, method) ((  Il2CppObject * (*) (ImmutableList_1_t788421714 *, const MethodInfo*))ImmutableList_1_System_Collections_IEnumerable_GetEnumerator_m2591974359_gshared)(__this, method)
// ZergRush.ImmutableList`1<T> ZergRush.ImmutableList`1<System.Action`1<System.Collections.Generic.ICollection`1<DonateButton>>>::Add(T)
#define ImmutableList_1_Add_m544908301(__this, ___value0, method) ((  ImmutableList_1_t788421714 * (*) (ImmutableList_1_t788421714 *, Action_1_t1285037532 *, const MethodInfo*))ImmutableList_1_Add_m3924978445_gshared)(__this, ___value0, method)
// System.Collections.Generic.IEnumerator`1<T> ZergRush.ImmutableList`1<System.Action`1<System.Collections.Generic.ICollection`1<DonateButton>>>::GetEnumerator()
#define ImmutableList_1_GetEnumerator_m3620967614(__this, method) ((  Il2CppObject* (*) (ImmutableList_1_t788421714 *, const MethodInfo*))ImmutableList_1_GetEnumerator_m364779198_gshared)(__this, method)
// ZergRush.ImmutableList`1<T> ZergRush.ImmutableList`1<System.Action`1<System.Collections.Generic.ICollection`1<DonateButton>>>::Remove(T)
#define ImmutableList_1_Remove_m931168264(__this, ___value0, method) ((  ImmutableList_1_t788421714 * (*) (ImmutableList_1_t788421714 *, Action_1_t1285037532 *, const MethodInfo*))ImmutableList_1_Remove_m1092573448_gshared)(__this, ___value0, method)
// ZergRush.ImmutableList`1<T> ZergRush.ImmutableList`1<System.Action`1<System.Collections.Generic.ICollection`1<DonateButton>>>::Replace(System.Int32,T)
#define ImmutableList_1_Replace_m3214992621(__this, ___index0, ___value1, method) ((  ImmutableList_1_t788421714 * (*) (ImmutableList_1_t788421714 *, int32_t, Action_1_t1285037532 *, const MethodInfo*))ImmutableList_1_Replace_m2580545005_gshared)(__this, ___index0, ___value1, method)
// System.Int32 ZergRush.ImmutableList`1<System.Action`1<System.Collections.Generic.ICollection`1<DonateButton>>>::IndexOf(T)
#define ImmutableList_1_IndexOf_m819167103(__this, ___value0, method) ((  int32_t (*) (ImmutableList_1_t788421714 *, Action_1_t1285037532 *, const MethodInfo*))ImmutableList_1_IndexOf_m1554103039_gshared)(__this, ___value0, method)
// System.Int32 ZergRush.ImmutableList`1<System.Action`1<System.Collections.Generic.ICollection`1<DonateButton>>>::get_Count()
#define ImmutableList_1_get_Count_m3736651228(__this, method) ((  int32_t (*) (ImmutableList_1_t788421714 *, const MethodInfo*))ImmutableList_1_get_Count_m749861468_gshared)(__this, method)
