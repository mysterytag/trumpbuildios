﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameAnalyticsSDK.Game
struct Game_t3902568756;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void GameAnalyticsSDK.Game::.ctor(System.String,System.Int32,System.String,System.String)
extern "C"  void Game__ctor_m3774297598 (Game_t3902568756 * __this, String_t* ___name0, int32_t ___id1, String_t* ___gameKey2, String_t* ___secretKey3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GameAnalyticsSDK.Game::get_Name()
extern "C"  String_t* Game_get_Name_m3641710468 (Game_t3902568756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.Game::set_Name(System.String)
extern "C"  void Game_set_Name_m9082919 (Game_t3902568756 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GameAnalyticsSDK.Game::get_ID()
extern "C"  int32_t Game_get_ID_m3927340713 (Game_t3902568756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.Game::set_ID(System.Int32)
extern "C"  void Game_set_ID_m1504649660 (Game_t3902568756 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GameAnalyticsSDK.Game::get_GameKey()
extern "C"  String_t* Game_get_GameKey_m3419130134 (Game_t3902568756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.Game::set_GameKey(System.String)
extern "C"  void Game_set_GameKey_m4216615651 (Game_t3902568756 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GameAnalyticsSDK.Game::get_SecretKey()
extern "C"  String_t* Game_get_SecretKey_m3324926744 (Game_t3902568756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.Game::set_SecretKey(System.String)
extern "C"  void Game_set_SecretKey_m325403233 (Game_t3902568756 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
