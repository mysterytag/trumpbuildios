﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AdsManager/<InterstitialAdsCoroutine>c__IteratorB
struct U3CInterstitialAdsCoroutineU3Ec__IteratorB_t2771681503;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void AdsManager/<InterstitialAdsCoroutine>c__IteratorB::.ctor()
extern "C"  void U3CInterstitialAdsCoroutineU3Ec__IteratorB__ctor_m3732792922 (U3CInterstitialAdsCoroutineU3Ec__IteratorB_t2771681503 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AdsManager/<InterstitialAdsCoroutine>c__IteratorB::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CInterstitialAdsCoroutineU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3669519480 (U3CInterstitialAdsCoroutineU3Ec__IteratorB_t2771681503 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AdsManager/<InterstitialAdsCoroutine>c__IteratorB::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CInterstitialAdsCoroutineU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m4236402700 (U3CInterstitialAdsCoroutineU3Ec__IteratorB_t2771681503 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AdsManager/<InterstitialAdsCoroutine>c__IteratorB::MoveNext()
extern "C"  bool U3CInterstitialAdsCoroutineU3Ec__IteratorB_MoveNext_m1940211290 (U3CInterstitialAdsCoroutineU3Ec__IteratorB_t2771681503 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdsManager/<InterstitialAdsCoroutine>c__IteratorB::Dispose()
extern "C"  void U3CInterstitialAdsCoroutineU3Ec__IteratorB_Dispose_m814588951 (U3CInterstitialAdsCoroutineU3Ec__IteratorB_t2771681503 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdsManager/<InterstitialAdsCoroutine>c__IteratorB::Reset()
extern "C"  void U3CInterstitialAdsCoroutineU3Ec__IteratorB_Reset_m1379225863 (U3CInterstitialAdsCoroutineU3Ec__IteratorB_t2771681503 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
