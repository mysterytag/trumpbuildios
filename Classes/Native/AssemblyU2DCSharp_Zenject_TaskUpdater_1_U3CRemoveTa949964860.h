﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TaskUpdater`1/<RemoveTask>c__AnonStorey191<System.Object>
struct  U3CRemoveTaskU3Ec__AnonStorey191_t949964860  : public Il2CppObject
{
public:
	// TTask Zenject.TaskUpdater`1/<RemoveTask>c__AnonStorey191::task
	Il2CppObject * ___task_0;

public:
	inline static int32_t get_offset_of_task_0() { return static_cast<int32_t>(offsetof(U3CRemoveTaskU3Ec__AnonStorey191_t949964860, ___task_0)); }
	inline Il2CppObject * get_task_0() const { return ___task_0; }
	inline Il2CppObject ** get_address_of_task_0() { return &___task_0; }
	inline void set_task_0(Il2CppObject * value)
	{
		___task_0 = value;
		Il2CppCodeGenWriteBarrier(&___task_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
