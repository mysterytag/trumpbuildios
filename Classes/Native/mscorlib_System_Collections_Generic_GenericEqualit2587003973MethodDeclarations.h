﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.GenericEqualityComparer`1<UniRx.Tuple`1<System.Object>>
struct GenericEqualityComparer_1_t2587003973;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_1_gen3656007660.h"

// System.Void System.Collections.Generic.GenericEqualityComparer`1<UniRx.Tuple`1<System.Object>>::.ctor()
extern "C"  void GenericEqualityComparer_1__ctor_m844261003_gshared (GenericEqualityComparer_1_t2587003973 * __this, const MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m844261003(__this, method) ((  void (*) (GenericEqualityComparer_1_t2587003973 *, const MethodInfo*))GenericEqualityComparer_1__ctor_m844261003_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<UniRx.Tuple`1<System.Object>>::GetHashCode(T)
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m326017728_gshared (GenericEqualityComparer_1_t2587003973 * __this, Tuple_1_t3656007660  ___obj0, const MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m326017728(__this, ___obj0, method) ((  int32_t (*) (GenericEqualityComparer_1_t2587003973 *, Tuple_1_t3656007660 , const MethodInfo*))GenericEqualityComparer_1_GetHashCode_m326017728_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<UniRx.Tuple`1<System.Object>>::Equals(T,T)
extern "C"  bool GenericEqualityComparer_1_Equals_m2400488924_gshared (GenericEqualityComparer_1_t2587003973 * __this, Tuple_1_t3656007660  ___x0, Tuple_1_t3656007660  ___y1, const MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m2400488924(__this, ___x0, ___y1, method) ((  bool (*) (GenericEqualityComparer_1_t2587003973 *, Tuple_1_t3656007660 , Tuple_1_t3656007660 , const MethodInfo*))GenericEqualityComparer_1_Equals_m2400488924_gshared)(__this, ___x0, ___y1, method)
