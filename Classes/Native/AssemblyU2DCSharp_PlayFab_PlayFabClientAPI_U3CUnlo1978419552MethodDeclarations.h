﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<UnlockContainerInstance>c__AnonStoreyAB
struct U3CUnlockContainerInstanceU3Ec__AnonStoreyAB_t1978419552;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<UnlockContainerInstance>c__AnonStoreyAB::.ctor()
extern "C"  void U3CUnlockContainerInstanceU3Ec__AnonStoreyAB__ctor_m2686849523 (U3CUnlockContainerInstanceU3Ec__AnonStoreyAB_t1978419552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<UnlockContainerInstance>c__AnonStoreyAB::<>m__98(PlayFab.CallRequestContainer)
extern "C"  void U3CUnlockContainerInstanceU3Ec__AnonStoreyAB_U3CU3Em__98_m2731375728 (U3CUnlockContainerInstanceU3Ec__AnonStoreyAB_t1978419552 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
