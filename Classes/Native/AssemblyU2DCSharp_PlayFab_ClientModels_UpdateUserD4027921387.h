﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_PlayFab_Internal_PlayFabResultCom379512675.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.UpdateUserDataResult
struct  UpdateUserDataResult_t4027921387  : public PlayFabResultCommon_t379512675
{
public:
	// System.UInt32 PlayFab.ClientModels.UpdateUserDataResult::<DataVersion>k__BackingField
	uint32_t ___U3CDataVersionU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CDataVersionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(UpdateUserDataResult_t4027921387, ___U3CDataVersionU3Ek__BackingField_2)); }
	inline uint32_t get_U3CDataVersionU3Ek__BackingField_2() const { return ___U3CDataVersionU3Ek__BackingField_2; }
	inline uint32_t* get_address_of_U3CDataVersionU3Ek__BackingField_2() { return &___U3CDataVersionU3Ek__BackingField_2; }
	inline void set_U3CDataVersionU3Ek__BackingField_2(uint32_t value)
	{
		___U3CDataVersionU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
