﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/ConnectableObservable`1<System.Object>
struct ConnectableObservable_1_t608944977;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// UniRx.ISubject`1<System.Object>
struct ISubject_1_t353709935;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Observable/ConnectableObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,UniRx.ISubject`1<T>)
extern "C"  void ConnectableObservable_1__ctor_m4204051920_gshared (ConnectableObservable_1_t608944977 * __this, Il2CppObject* ___source0, Il2CppObject* ___subject1, const MethodInfo* method);
#define ConnectableObservable_1__ctor_m4204051920(__this, ___source0, ___subject1, method) ((  void (*) (ConnectableObservable_1_t608944977 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))ConnectableObservable_1__ctor_m4204051920_gshared)(__this, ___source0, ___subject1, method)
// System.IDisposable UniRx.Observable/ConnectableObservable`1<System.Object>::Connect()
extern "C"  Il2CppObject * ConnectableObservable_1_Connect_m4186812418_gshared (ConnectableObservable_1_t608944977 * __this, const MethodInfo* method);
#define ConnectableObservable_1_Connect_m4186812418(__this, method) ((  Il2CppObject * (*) (ConnectableObservable_1_t608944977 *, const MethodInfo*))ConnectableObservable_1_Connect_m4186812418_gshared)(__this, method)
// System.IDisposable UniRx.Observable/ConnectableObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * ConnectableObservable_1_Subscribe_m1737984212_gshared (ConnectableObservable_1_t608944977 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ConnectableObservable_1_Subscribe_m1737984212(__this, ___observer0, method) ((  Il2CppObject * (*) (ConnectableObservable_1_t608944977 *, Il2CppObject*, const MethodInfo*))ConnectableObservable_1_Subscribe_m1737984212_gshared)(__this, ___observer0, method)
