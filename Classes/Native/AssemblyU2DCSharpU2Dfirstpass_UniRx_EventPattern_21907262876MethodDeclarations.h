﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_EventPattern_23939142245MethodDeclarations.h"

// System.Void UniRx.EventPattern`2<System.Object,UniRx.Examples.Sample09_EventHandling/MyEventArgs>::.ctor(TSender,TEventArgs)
#define EventPattern_2__ctor_m2082322501(__this, ___sender0, ___e1, method) ((  void (*) (EventPattern_2_t1907262876 *, Il2CppObject *, MyEventArgs_t3100194347 *, const MethodInfo*))EventPattern_2__ctor_m3419812743_gshared)(__this, ___sender0, ___e1, method)
// TSender UniRx.EventPattern`2<System.Object,UniRx.Examples.Sample09_EventHandling/MyEventArgs>::get_Sender()
#define EventPattern_2_get_Sender_m992472653(__this, method) ((  Il2CppObject * (*) (EventPattern_2_t1907262876 *, const MethodInfo*))EventPattern_2_get_Sender_m2972234443_gshared)(__this, method)
// System.Void UniRx.EventPattern`2<System.Object,UniRx.Examples.Sample09_EventHandling/MyEventArgs>::set_Sender(TSender)
#define EventPattern_2_set_Sender_m288959486(__this, ___value0, method) ((  void (*) (EventPattern_2_t1907262876 *, Il2CppObject *, const MethodInfo*))EventPattern_2_set_Sender_m3333931200_gshared)(__this, ___value0, method)
// TEventArgs UniRx.EventPattern`2<System.Object,UniRx.Examples.Sample09_EventHandling/MyEventArgs>::get_EventArgs()
#define EventPattern_2_get_EventArgs_m742975417(__this, method) ((  MyEventArgs_t3100194347 * (*) (EventPattern_2_t1907262876 *, const MethodInfo*))EventPattern_2_get_EventArgs_m3420313211_gshared)(__this, method)
// System.Void UniRx.EventPattern`2<System.Object,UniRx.Examples.Sample09_EventHandling/MyEventArgs>::set_EventArgs(TEventArgs)
#define EventPattern_2_set_EventArgs_m4256252768(__this, ___value0, method) ((  void (*) (EventPattern_2_t1907262876 *, MyEventArgs_t3100194347 *, const MethodInfo*))EventPattern_2_set_EventArgs_m1298775714_gshared)(__this, ___value0, method)
// System.Boolean UniRx.EventPattern`2<System.Object,UniRx.Examples.Sample09_EventHandling/MyEventArgs>::Equals(UniRx.EventPattern`2<TSender,TEventArgs>)
#define EventPattern_2_Equals_m1993461312(__this, ___other0, method) ((  bool (*) (EventPattern_2_t1907262876 *, EventPattern_2_t1907262876 *, const MethodInfo*))EventPattern_2_Equals_m2393598654_gshared)(__this, ___other0, method)
// System.Boolean UniRx.EventPattern`2<System.Object,UniRx.Examples.Sample09_EventHandling/MyEventArgs>::Equals(System.Object)
#define EventPattern_2_Equals_m2387243224(__this, ___obj0, method) ((  bool (*) (EventPattern_2_t1907262876 *, Il2CppObject *, const MethodInfo*))EventPattern_2_Equals_m4062083866_gshared)(__this, ___obj0, method)
// System.Int32 UniRx.EventPattern`2<System.Object,UniRx.Examples.Sample09_EventHandling/MyEventArgs>::GetHashCode()
#define EventPattern_2_GetHashCode_m3121712624(__this, method) ((  int32_t (*) (EventPattern_2_t1907262876 *, const MethodInfo*))EventPattern_2_GetHashCode_m2584518450_gshared)(__this, method)
// System.Boolean UniRx.EventPattern`2<System.Object,UniRx.Examples.Sample09_EventHandling/MyEventArgs>::op_Equality(UniRx.EventPattern`2<TSender,TEventArgs>,UniRx.EventPattern`2<TSender,TEventArgs>)
#define EventPattern_2_op_Equality_m3961292681(__this /* static, unused */, ___first0, ___second1, method) ((  bool (*) (Il2CppObject * /* static, unused */, EventPattern_2_t1907262876 *, EventPattern_2_t1907262876 *, const MethodInfo*))EventPattern_2_op_Equality_m1906571143_gshared)(__this /* static, unused */, ___first0, ___second1, method)
// System.Boolean UniRx.EventPattern`2<System.Object,UniRx.Examples.Sample09_EventHandling/MyEventArgs>::op_Inequality(UniRx.EventPattern`2<TSender,TEventArgs>,UniRx.EventPattern`2<TSender,TEventArgs>)
#define EventPattern_2_op_Inequality_m2673075140(__this /* static, unused */, ___first0, ___second1, method) ((  bool (*) (Il2CppObject * /* static, unused */, EventPattern_2_t1907262876 *, EventPattern_2_t1907262876 *, const MethodInfo*))EventPattern_2_op_Inequality_m3770633282_gshared)(__this /* static, unused */, ___first0, ___second1, method)
