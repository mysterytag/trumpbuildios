﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/AddUserVirtualCurrencyResponseCallback
struct AddUserVirtualCurrencyResponseCallback_t2143751798;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.AddUserVirtualCurrencyRequest
struct AddUserVirtualCurrencyRequest_t2803224015;
// PlayFab.ClientModels.ModifyUserVirtualCurrencyResult
struct ModifyUserVirtualCurrencyResult_t448487620;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_AddUserVirt2803224015.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ModifyUserVi448487620.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/AddUserVirtualCurrencyResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void AddUserVirtualCurrencyResponseCallback__ctor_m2115954789 (AddUserVirtualCurrencyResponseCallback_t2143751798 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/AddUserVirtualCurrencyResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.AddUserVirtualCurrencyRequest,PlayFab.ClientModels.ModifyUserVirtualCurrencyResult,PlayFab.PlayFabError,System.Object)
extern "C"  void AddUserVirtualCurrencyResponseCallback_Invoke_m373866725 (AddUserVirtualCurrencyResponseCallback_t2143751798 * __this, String_t* ___urlPath0, int32_t ___callId1, AddUserVirtualCurrencyRequest_t2803224015 * ___request2, ModifyUserVirtualCurrencyResult_t448487620 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_AddUserVirtualCurrencyResponseCallback_t2143751798(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, AddUserVirtualCurrencyRequest_t2803224015 * ___request2, ModifyUserVirtualCurrencyResult_t448487620 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/AddUserVirtualCurrencyResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.AddUserVirtualCurrencyRequest,PlayFab.ClientModels.ModifyUserVirtualCurrencyResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * AddUserVirtualCurrencyResponseCallback_BeginInvoke_m3165508526 (AddUserVirtualCurrencyResponseCallback_t2143751798 * __this, String_t* ___urlPath0, int32_t ___callId1, AddUserVirtualCurrencyRequest_t2803224015 * ___request2, ModifyUserVirtualCurrencyResult_t448487620 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/AddUserVirtualCurrencyResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void AddUserVirtualCurrencyResponseCallback_EndInvoke_m3944240373 (AddUserVirtualCurrencyResponseCallback_t2143751798 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
