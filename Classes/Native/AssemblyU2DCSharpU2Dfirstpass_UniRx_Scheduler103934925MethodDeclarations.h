﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IScheduler
struct IScheduler_t2938318244;
// System.Action
struct Action_t437523947;
// System.Action`1<System.Action>
struct Action_1_t585976652;
// System.Action`1<System.Action`1<System.TimeSpan>>
struct Action_1_t1060768302;
// System.Action`1<System.Action`1<System.DateTimeOffset>>
struct Action_1_t4009165445;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTimeOffset3712260035.h"
#include "mscorlib_System_TimeSpan763862892.h"
#include "System_Core_System_Action437523947.h"

// System.Void UniRx.Scheduler::.cctor()
extern "C"  void Scheduler__cctor_m4113998969 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.Scheduler::get_IsCurrentThreadSchedulerScheduleRequired()
extern "C"  bool Scheduler_get_IsCurrentThreadSchedulerScheduleRequired_m2140071893 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTimeOffset UniRx.Scheduler::get_Now()
extern "C"  DateTimeOffset_t3712260035  Scheduler_get_Now_m959360261 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TimeSpan UniRx.Scheduler::Normalize(System.TimeSpan)
extern "C"  TimeSpan_t763862892  Scheduler_Normalize_m3379699520 (Il2CppObject * __this /* static, unused */, TimeSpan_t763862892  ___timeSpan0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable UniRx.Scheduler::Schedule(UniRx.IScheduler,System.DateTimeOffset,System.Action)
extern "C"  Il2CppObject * Scheduler_Schedule_m96532664 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___scheduler0, DateTimeOffset_t3712260035  ___dueTime1, Action_t437523947 * ___action2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable UniRx.Scheduler::Schedule(UniRx.IScheduler,System.Action`1<System.Action>)
extern "C"  Il2CppObject * Scheduler_Schedule_m2919836901 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___scheduler0, Action_1_t585976652 * ___action1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable UniRx.Scheduler::Schedule(UniRx.IScheduler,System.TimeSpan,System.Action`1<System.Action`1<System.TimeSpan>>)
extern "C"  Il2CppObject * Scheduler_Schedule_m279164218 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___scheduler0, TimeSpan_t763862892  ___dueTime1, Action_1_t1060768302 * ___action2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable UniRx.Scheduler::Schedule(UniRx.IScheduler,System.DateTimeOffset,System.Action`1<System.Action`1<System.DateTimeOffset>>)
extern "C"  Il2CppObject * Scheduler_Schedule_m3621390988 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___scheduler0, DateTimeOffset_t3712260035  ___dueTime1, Action_1_t4009165445 * ___action2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Scheduler::SetDefaultForUnity()
extern "C"  void Scheduler_SetDefaultForUnity_m4159226075 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IScheduler UniRx.Scheduler::get_MainThread()
extern "C"  Il2CppObject * Scheduler_get_MainThread_m2582475169 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IScheduler UniRx.Scheduler::get_MainThreadIgnoreTimeScale()
extern "C"  Il2CppObject * Scheduler_get_MainThreadIgnoreTimeScale_m3212577356 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
