﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ChronoLerp
struct ChronoLerp_t521452250;
// System.Action`1<System.Single>
struct Action_1_t1106661726;

#include "codegen/il2cpp-codegen.h"

// System.Void ChronoLerp::.ctor(System.Single,System.Single,System.Single,System.Action`1<System.Single>)
extern "C"  void ChronoLerp__ctor_m4144575301 (ChronoLerp_t521452250 * __this, float ___duration0, float ___inFrom1, float ___inTo2, Action_1_t1106661726 * ___inAction3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ChronoLerp::Update(System.Single)
extern "C"  void ChronoLerp_Update_m1007675647 (ChronoLerp_t521452250 * __this, float ___relativeTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
