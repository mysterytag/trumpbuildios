﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.FirstObservable`1/First<System.Boolean>
struct First_t2588875826;
// UniRx.Operators.FirstObservable`1<System.Boolean>
struct FirstObservable_1_t2233559755;
// UniRx.IObserver`1<System.Boolean>
struct IObserver_1_t2423004244;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.FirstObservable`1/First<System.Boolean>::.ctor(UniRx.Operators.FirstObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void First__ctor_m355693915_gshared (First_t2588875826 * __this, FirstObservable_1_t2233559755 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define First__ctor_m355693915(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (First_t2588875826 *, FirstObservable_1_t2233559755 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))First__ctor_m355693915_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.FirstObservable`1/First<System.Boolean>::OnNext(T)
extern "C"  void First_OnNext_m3616118840_gshared (First_t2588875826 * __this, bool ___value0, const MethodInfo* method);
#define First_OnNext_m3616118840(__this, ___value0, method) ((  void (*) (First_t2588875826 *, bool, const MethodInfo*))First_OnNext_m3616118840_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.FirstObservable`1/First<System.Boolean>::OnError(System.Exception)
extern "C"  void First_OnError_m298206023_gshared (First_t2588875826 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define First_OnError_m298206023(__this, ___error0, method) ((  void (*) (First_t2588875826 *, Exception_t1967233988 *, const MethodInfo*))First_OnError_m298206023_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.FirstObservable`1/First<System.Boolean>::OnCompleted()
extern "C"  void First_OnCompleted_m2703386906_gshared (First_t2588875826 * __this, const MethodInfo* method);
#define First_OnCompleted_m2703386906(__this, method) ((  void (*) (First_t2588875826 *, const MethodInfo*))First_OnCompleted_m2703386906_gshared)(__this, method)
