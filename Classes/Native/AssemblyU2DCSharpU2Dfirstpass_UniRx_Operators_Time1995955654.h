﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.SerialDisposable
struct SerialDisposable_t2547852742;
// UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey6D
struct U3CSubscribeCoreU3Ec__AnonStorey6D_t1995955652;
// UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey6E
struct U3CSubscribeCoreU3Ec__AnonStorey6E_t1995955653;
// UniRx.Operators.TimerObservable
struct TimerObservable_t1697791765;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey6F
struct  U3CSubscribeCoreU3Ec__AnonStorey6F_t1995955654  : public Il2CppObject
{
public:
	// UniRx.SerialDisposable UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey6F::disposable
	SerialDisposable_t2547852742 * ___disposable_0;
	// UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey6D UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey6F::<>f__ref$109
	U3CSubscribeCoreU3Ec__AnonStorey6D_t1995955652 * ___U3CU3Ef__refU24109_1;
	// UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey6E UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey6F::<>f__ref$110
	U3CSubscribeCoreU3Ec__AnonStorey6E_t1995955653 * ___U3CU3Ef__refU24110_2;
	// UniRx.Operators.TimerObservable UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey6F::<>f__this
	TimerObservable_t1697791765 * ___U3CU3Ef__this_3;

public:
	inline static int32_t get_offset_of_disposable_0() { return static_cast<int32_t>(offsetof(U3CSubscribeCoreU3Ec__AnonStorey6F_t1995955654, ___disposable_0)); }
	inline SerialDisposable_t2547852742 * get_disposable_0() const { return ___disposable_0; }
	inline SerialDisposable_t2547852742 ** get_address_of_disposable_0() { return &___disposable_0; }
	inline void set_disposable_0(SerialDisposable_t2547852742 * value)
	{
		___disposable_0 = value;
		Il2CppCodeGenWriteBarrier(&___disposable_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24109_1() { return static_cast<int32_t>(offsetof(U3CSubscribeCoreU3Ec__AnonStorey6F_t1995955654, ___U3CU3Ef__refU24109_1)); }
	inline U3CSubscribeCoreU3Ec__AnonStorey6D_t1995955652 * get_U3CU3Ef__refU24109_1() const { return ___U3CU3Ef__refU24109_1; }
	inline U3CSubscribeCoreU3Ec__AnonStorey6D_t1995955652 ** get_address_of_U3CU3Ef__refU24109_1() { return &___U3CU3Ef__refU24109_1; }
	inline void set_U3CU3Ef__refU24109_1(U3CSubscribeCoreU3Ec__AnonStorey6D_t1995955652 * value)
	{
		___U3CU3Ef__refU24109_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24109_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24110_2() { return static_cast<int32_t>(offsetof(U3CSubscribeCoreU3Ec__AnonStorey6F_t1995955654, ___U3CU3Ef__refU24110_2)); }
	inline U3CSubscribeCoreU3Ec__AnonStorey6E_t1995955653 * get_U3CU3Ef__refU24110_2() const { return ___U3CU3Ef__refU24110_2; }
	inline U3CSubscribeCoreU3Ec__AnonStorey6E_t1995955653 ** get_address_of_U3CU3Ef__refU24110_2() { return &___U3CU3Ef__refU24110_2; }
	inline void set_U3CU3Ef__refU24110_2(U3CSubscribeCoreU3Ec__AnonStorey6E_t1995955653 * value)
	{
		___U3CU3Ef__refU24110_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24110_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_3() { return static_cast<int32_t>(offsetof(U3CSubscribeCoreU3Ec__AnonStorey6F_t1995955654, ___U3CU3Ef__this_3)); }
	inline TimerObservable_t1697791765 * get_U3CU3Ef__this_3() const { return ___U3CU3Ef__this_3; }
	inline TimerObservable_t1697791765 ** get_address_of_U3CU3Ef__this_3() { return &___U3CU3Ef__this_3; }
	inline void set_U3CU3Ef__this_3(TimerObservable_t1697791765 * value)
	{
		___U3CU3Ef__this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
