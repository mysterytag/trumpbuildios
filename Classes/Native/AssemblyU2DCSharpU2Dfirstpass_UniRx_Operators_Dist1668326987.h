﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.DistinctObservable`2<System.Object,System.Object>
struct DistinctObservable_2_t2396543569;
// System.Collections.Generic.HashSet`1<System.Object>
struct HashSet_1_t3535795091;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.DistinctObservable`2/Distinct<System.Object,System.Object>
struct  Distinct_t1668326987  : public OperatorObserverBase_2_t1187768149
{
public:
	// UniRx.Operators.DistinctObservable`2<T,TKey> UniRx.Operators.DistinctObservable`2/Distinct::parent
	DistinctObservable_2_t2396543569 * ___parent_2;
	// System.Collections.Generic.HashSet`1<TKey> UniRx.Operators.DistinctObservable`2/Distinct::hashSet
	HashSet_1_t3535795091 * ___hashSet_3;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(Distinct_t1668326987, ___parent_2)); }
	inline DistinctObservable_2_t2396543569 * get_parent_2() const { return ___parent_2; }
	inline DistinctObservable_2_t2396543569 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(DistinctObservable_2_t2396543569 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_hashSet_3() { return static_cast<int32_t>(offsetof(Distinct_t1668326987, ___hashSet_3)); }
	inline HashSet_1_t3535795091 * get_hashSet_3() const { return ___hashSet_3; }
	inline HashSet_1_t3535795091 ** get_address_of_hashSet_3() { return &___hashSet_3; }
	inline void set_hashSet_3(HashSet_1_t3535795091 * value)
	{
		___hashSet_3 = value;
		Il2CppCodeGenWriteBarrier(&___hashSet_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
