﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ReactiveCollection`1<System.Object>
struct ReactiveCollection_1_t2041251907;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t3709260776;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t1634065389;
// System.Object
struct Il2CppObject;
// UniRx.IObservable`1<System.Int32>
struct IObservable_1_t2606213151;
// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;
// UniRx.IObservable`1<UniRx.CollectionAddEvent`1<System.Object>>
struct IObservable_1_t2175320351;
// UniRx.IObservable`1<UniRx.CollectionMoveEvent`1<System.Object>>
struct IObservable_1_t2675232211;
// UniRx.IObservable`1<UniRx.CollectionRemoveEvent`1<System.Object>>
struct IObservable_1_t1436088;
// UniRx.IObservable`1<UniRx.CollectionReplaceEvent`1<System.Object>>
struct IObservable_1_t1600548900;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.ReactiveCollection`1<System.Object>::.ctor()
extern "C"  void ReactiveCollection_1__ctor_m295792059_gshared (ReactiveCollection_1_t2041251907 * __this, const MethodInfo* method);
#define ReactiveCollection_1__ctor_m295792059(__this, method) ((  void (*) (ReactiveCollection_1_t2041251907 *, const MethodInfo*))ReactiveCollection_1__ctor_m295792059_gshared)(__this, method)
// System.Void UniRx.ReactiveCollection`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void ReactiveCollection_1__ctor_m1771263972_gshared (ReactiveCollection_1_t2041251907 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define ReactiveCollection_1__ctor_m1771263972(__this, ___collection0, method) ((  void (*) (ReactiveCollection_1_t2041251907 *, Il2CppObject*, const MethodInfo*))ReactiveCollection_1__ctor_m1771263972_gshared)(__this, ___collection0, method)
// System.Void UniRx.ReactiveCollection`1<System.Object>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void ReactiveCollection_1__ctor_m1797534867_gshared (ReactiveCollection_1_t2041251907 * __this, List_1_t1634065389 * ___list0, const MethodInfo* method);
#define ReactiveCollection_1__ctor_m1797534867(__this, ___list0, method) ((  void (*) (ReactiveCollection_1_t2041251907 *, List_1_t1634065389 *, const MethodInfo*))ReactiveCollection_1__ctor_m1797534867_gshared)(__this, ___list0, method)
// System.Void UniRx.ReactiveCollection`1<System.Object>::ClearItems()
extern "C"  void ReactiveCollection_1_ClearItems_m3545347804_gshared (ReactiveCollection_1_t2041251907 * __this, const MethodInfo* method);
#define ReactiveCollection_1_ClearItems_m3545347804(__this, method) ((  void (*) (ReactiveCollection_1_t2041251907 *, const MethodInfo*))ReactiveCollection_1_ClearItems_m3545347804_gshared)(__this, method)
// System.Void UniRx.ReactiveCollection`1<System.Object>::InsertItem(System.Int32,T)
extern "C"  void ReactiveCollection_1_InsertItem_m1340650110_gshared (ReactiveCollection_1_t2041251907 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define ReactiveCollection_1_InsertItem_m1340650110(__this, ___index0, ___item1, method) ((  void (*) (ReactiveCollection_1_t2041251907 *, int32_t, Il2CppObject *, const MethodInfo*))ReactiveCollection_1_InsertItem_m1340650110_gshared)(__this, ___index0, ___item1, method)
// System.Void UniRx.ReactiveCollection`1<System.Object>::Move(System.Int32,System.Int32)
extern "C"  void ReactiveCollection_1_Move_m1250881708_gshared (ReactiveCollection_1_t2041251907 * __this, int32_t ___oldIndex0, int32_t ___newIndex1, const MethodInfo* method);
#define ReactiveCollection_1_Move_m1250881708(__this, ___oldIndex0, ___newIndex1, method) ((  void (*) (ReactiveCollection_1_t2041251907 *, int32_t, int32_t, const MethodInfo*))ReactiveCollection_1_Move_m1250881708_gshared)(__this, ___oldIndex0, ___newIndex1, method)
// System.Void UniRx.ReactiveCollection`1<System.Object>::MoveItem(System.Int32,System.Int32)
extern "C"  void ReactiveCollection_1_MoveItem_m2985233945_gshared (ReactiveCollection_1_t2041251907 * __this, int32_t ___oldIndex0, int32_t ___newIndex1, const MethodInfo* method);
#define ReactiveCollection_1_MoveItem_m2985233945(__this, ___oldIndex0, ___newIndex1, method) ((  void (*) (ReactiveCollection_1_t2041251907 *, int32_t, int32_t, const MethodInfo*))ReactiveCollection_1_MoveItem_m2985233945_gshared)(__this, ___oldIndex0, ___newIndex1, method)
// System.Void UniRx.ReactiveCollection`1<System.Object>::RemoveItem(System.Int32)
extern "C"  void ReactiveCollection_1_RemoveItem_m3821606769_gshared (ReactiveCollection_1_t2041251907 * __this, int32_t ___index0, const MethodInfo* method);
#define ReactiveCollection_1_RemoveItem_m3821606769(__this, ___index0, method) ((  void (*) (ReactiveCollection_1_t2041251907 *, int32_t, const MethodInfo*))ReactiveCollection_1_RemoveItem_m3821606769_gshared)(__this, ___index0, method)
// System.Void UniRx.ReactiveCollection`1<System.Object>::SetItem(System.Int32,T)
extern "C"  void ReactiveCollection_1_SetItem_m2139646039_gshared (ReactiveCollection_1_t2041251907 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define ReactiveCollection_1_SetItem_m2139646039(__this, ___index0, ___item1, method) ((  void (*) (ReactiveCollection_1_t2041251907 *, int32_t, Il2CppObject *, const MethodInfo*))ReactiveCollection_1_SetItem_m2139646039_gshared)(__this, ___index0, ___item1, method)
// UniRx.IObservable`1<System.Int32> UniRx.ReactiveCollection`1<System.Object>::ObserveCountChanged(System.Boolean)
extern "C"  Il2CppObject* ReactiveCollection_1_ObserveCountChanged_m1653102444_gshared (ReactiveCollection_1_t2041251907 * __this, bool ___notifyCurrentCount0, const MethodInfo* method);
#define ReactiveCollection_1_ObserveCountChanged_m1653102444(__this, ___notifyCurrentCount0, method) ((  Il2CppObject* (*) (ReactiveCollection_1_t2041251907 *, bool, const MethodInfo*))ReactiveCollection_1_ObserveCountChanged_m1653102444_gshared)(__this, ___notifyCurrentCount0, method)
// UniRx.IObservable`1<UniRx.Unit> UniRx.ReactiveCollection`1<System.Object>::ObserveReset()
extern "C"  Il2CppObject* ReactiveCollection_1_ObserveReset_m129035828_gshared (ReactiveCollection_1_t2041251907 * __this, const MethodInfo* method);
#define ReactiveCollection_1_ObserveReset_m129035828(__this, method) ((  Il2CppObject* (*) (ReactiveCollection_1_t2041251907 *, const MethodInfo*))ReactiveCollection_1_ObserveReset_m129035828_gshared)(__this, method)
// UniRx.IObservable`1<UniRx.CollectionAddEvent`1<T>> UniRx.ReactiveCollection`1<System.Object>::ObserveAdd()
extern "C"  Il2CppObject* ReactiveCollection_1_ObserveAdd_m1265477464_gshared (ReactiveCollection_1_t2041251907 * __this, const MethodInfo* method);
#define ReactiveCollection_1_ObserveAdd_m1265477464(__this, method) ((  Il2CppObject* (*) (ReactiveCollection_1_t2041251907 *, const MethodInfo*))ReactiveCollection_1_ObserveAdd_m1265477464_gshared)(__this, method)
// UniRx.IObservable`1<UniRx.CollectionMoveEvent`1<T>> UniRx.ReactiveCollection`1<System.Object>::ObserveMove()
extern "C"  Il2CppObject* ReactiveCollection_1_ObserveMove_m1334704436_gshared (ReactiveCollection_1_t2041251907 * __this, const MethodInfo* method);
#define ReactiveCollection_1_ObserveMove_m1334704436(__this, method) ((  Il2CppObject* (*) (ReactiveCollection_1_t2041251907 *, const MethodInfo*))ReactiveCollection_1_ObserveMove_m1334704436_gshared)(__this, method)
// UniRx.IObservable`1<UniRx.CollectionRemoveEvent`1<T>> UniRx.ReactiveCollection`1<System.Object>::ObserveRemove()
extern "C"  Il2CppObject* ReactiveCollection_1_ObserveRemove_m2032996890_gshared (ReactiveCollection_1_t2041251907 * __this, const MethodInfo* method);
#define ReactiveCollection_1_ObserveRemove_m2032996890(__this, method) ((  Il2CppObject* (*) (ReactiveCollection_1_t2041251907 *, const MethodInfo*))ReactiveCollection_1_ObserveRemove_m2032996890_gshared)(__this, method)
// UniRx.IObservable`1<UniRx.CollectionReplaceEvent`1<T>> UniRx.ReactiveCollection`1<System.Object>::ObserveReplace()
extern "C"  Il2CppObject* ReactiveCollection_1_ObserveReplace_m1074234936_gshared (ReactiveCollection_1_t2041251907 * __this, const MethodInfo* method);
#define ReactiveCollection_1_ObserveReplace_m1074234936(__this, method) ((  Il2CppObject* (*) (ReactiveCollection_1_t2041251907 *, const MethodInfo*))ReactiveCollection_1_ObserveReplace_m1074234936_gshared)(__this, method)
// System.Void UniRx.ReactiveCollection`1<System.Object>::Dispose()
extern "C"  void ReactiveCollection_1_Dispose_m686610232_gshared (ReactiveCollection_1_t2041251907 * __this, const MethodInfo* method);
#define ReactiveCollection_1_Dispose_m686610232(__this, method) ((  void (*) (ReactiveCollection_1_t2041251907 *, const MethodInfo*))ReactiveCollection_1_Dispose_m686610232_gshared)(__this, method)
// System.Int32 UniRx.ReactiveCollection`1<System.Object>::<ObserveCountChanged>m__C3()
extern "C"  int32_t ReactiveCollection_1_U3CObserveCountChangedU3Em__C3_m3456580407_gshared (ReactiveCollection_1_t2041251907 * __this, const MethodInfo* method);
#define ReactiveCollection_1_U3CObserveCountChangedU3Em__C3_m3456580407(__this, method) ((  int32_t (*) (ReactiveCollection_1_t2041251907 *, const MethodInfo*))ReactiveCollection_1_U3CObserveCountChangedU3Em__C3_m3456580407_gshared)(__this, method)
