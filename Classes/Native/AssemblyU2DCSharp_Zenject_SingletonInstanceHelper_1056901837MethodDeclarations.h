﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.SingletonInstanceHelper/<GetActiveSingletonTypesDerivingFrom>c__AnonStorey190`1<System.Object>
struct U3CGetActiveSingletonTypesDerivingFromU3Ec__AnonStorey190_1_t1056901837;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2779229935.h"

// System.Void Zenject.SingletonInstanceHelper/<GetActiveSingletonTypesDerivingFrom>c__AnonStorey190`1<System.Object>::.ctor()
extern "C"  void U3CGetActiveSingletonTypesDerivingFromU3Ec__AnonStorey190_1__ctor_m3509688979_gshared (U3CGetActiveSingletonTypesDerivingFromU3Ec__AnonStorey190_1_t1056901837 * __this, const MethodInfo* method);
#define U3CGetActiveSingletonTypesDerivingFromU3Ec__AnonStorey190_1__ctor_m3509688979(__this, method) ((  void (*) (U3CGetActiveSingletonTypesDerivingFromU3Ec__AnonStorey190_1_t1056901837 *, const MethodInfo*))U3CGetActiveSingletonTypesDerivingFromU3Ec__AnonStorey190_1__ctor_m3509688979_gshared)(__this, method)
// System.Boolean Zenject.SingletonInstanceHelper/<GetActiveSingletonTypesDerivingFrom>c__AnonStorey190`1<System.Object>::<>m__2E1(System.Type)
extern "C"  bool U3CGetActiveSingletonTypesDerivingFromU3Ec__AnonStorey190_1_U3CU3Em__2E1_m264082753_gshared (U3CGetActiveSingletonTypesDerivingFromU3Ec__AnonStorey190_1_t1056901837 * __this, Type_t * ___x0, const MethodInfo* method);
#define U3CGetActiveSingletonTypesDerivingFromU3Ec__AnonStorey190_1_U3CU3Em__2E1_m264082753(__this, ___x0, method) ((  bool (*) (U3CGetActiveSingletonTypesDerivingFromU3Ec__AnonStorey190_1_t1056901837 *, Type_t *, const MethodInfo*))U3CGetActiveSingletonTypesDerivingFromU3Ec__AnonStorey190_1_U3CU3Em__2E1_m264082753_gshared)(__this, ___x0, method)
// System.Boolean Zenject.SingletonInstanceHelper/<GetActiveSingletonTypesDerivingFrom>c__AnonStorey190`1<System.Object>::<>m__2E4(System.Type)
extern "C"  bool U3CGetActiveSingletonTypesDerivingFromU3Ec__AnonStorey190_1_U3CU3Em__2E4_m3042374174_gshared (U3CGetActiveSingletonTypesDerivingFromU3Ec__AnonStorey190_1_t1056901837 * __this, Type_t * ___x0, const MethodInfo* method);
#define U3CGetActiveSingletonTypesDerivingFromU3Ec__AnonStorey190_1_U3CU3Em__2E4_m3042374174(__this, ___x0, method) ((  bool (*) (U3CGetActiveSingletonTypesDerivingFromU3Ec__AnonStorey190_1_t1056901837 *, Type_t *, const MethodInfo*))U3CGetActiveSingletonTypesDerivingFromU3Ec__AnonStorey190_1_U3CU3Em__2E4_m3042374174_gshared)(__this, ___x0, method)
