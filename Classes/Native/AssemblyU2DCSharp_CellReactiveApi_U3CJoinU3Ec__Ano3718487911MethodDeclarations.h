﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/<Join>c__AnonStorey114`1/<Join>c__AnonStorey115`1<System.Int32>
struct U3CJoinU3Ec__AnonStorey115_1_t3718487911;
// ICell`1<System.Int32>
struct ICell_1_t104078468;

#include "codegen/il2cpp-codegen.h"

// System.Void CellReactiveApi/<Join>c__AnonStorey114`1/<Join>c__AnonStorey115`1<System.Int32>::.ctor()
extern "C"  void U3CJoinU3Ec__AnonStorey115_1__ctor_m3137183832_gshared (U3CJoinU3Ec__AnonStorey115_1_t3718487911 * __this, const MethodInfo* method);
#define U3CJoinU3Ec__AnonStorey115_1__ctor_m3137183832(__this, method) ((  void (*) (U3CJoinU3Ec__AnonStorey115_1_t3718487911 *, const MethodInfo*))U3CJoinU3Ec__AnonStorey115_1__ctor_m3137183832_gshared)(__this, method)
// System.Void CellReactiveApi/<Join>c__AnonStorey114`1/<Join>c__AnonStorey115`1<System.Int32>::<>m__19C(ICell`1<T>)
extern "C"  void U3CJoinU3Ec__AnonStorey115_1_U3CU3Em__19C_m1744712434_gshared (U3CJoinU3Ec__AnonStorey115_1_t3718487911 * __this, Il2CppObject* ___innerCell0, const MethodInfo* method);
#define U3CJoinU3Ec__AnonStorey115_1_U3CU3Em__19C_m1744712434(__this, ___innerCell0, method) ((  void (*) (U3CJoinU3Ec__AnonStorey115_1_t3718487911 *, Il2CppObject*, const MethodInfo*))U3CJoinU3Ec__AnonStorey115_1_U3CU3Em__19C_m1744712434_gshared)(__this, ___innerCell0, method)
// System.Void CellReactiveApi/<Join>c__AnonStorey114`1/<Join>c__AnonStorey115`1<System.Int32>::<>m__19D(T)
extern "C"  void U3CJoinU3Ec__AnonStorey115_1_U3CU3Em__19D_m893289473_gshared (U3CJoinU3Ec__AnonStorey115_1_t3718487911 * __this, int32_t ___val0, const MethodInfo* method);
#define U3CJoinU3Ec__AnonStorey115_1_U3CU3Em__19D_m893289473(__this, ___val0, method) ((  void (*) (U3CJoinU3Ec__AnonStorey115_1_t3718487911 *, int32_t, const MethodInfo*))U3CJoinU3Ec__AnonStorey115_1_U3CU3Em__19D_m893289473_gshared)(__this, ___val0, method)
