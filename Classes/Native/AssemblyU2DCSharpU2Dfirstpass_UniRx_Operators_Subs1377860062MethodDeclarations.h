﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SubscribeOnMainThreadObservable`1<System.Object>
struct SubscribeOnMainThreadObservable_1_t1377860062;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// UniRx.IObservable`1<System.Int64>
struct IObservable_1_t2606213246;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.SubscribeOnMainThreadObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,UniRx.IObservable`1<System.Int64>)
extern "C"  void SubscribeOnMainThreadObservable_1__ctor_m2208469230_gshared (SubscribeOnMainThreadObservable_1_t1377860062 * __this, Il2CppObject* ___source0, Il2CppObject* ___subscribeTrigger1, const MethodInfo* method);
#define SubscribeOnMainThreadObservable_1__ctor_m2208469230(__this, ___source0, ___subscribeTrigger1, method) ((  void (*) (SubscribeOnMainThreadObservable_1_t1377860062 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))SubscribeOnMainThreadObservable_1__ctor_m2208469230_gshared)(__this, ___source0, ___subscribeTrigger1, method)
// System.IDisposable UniRx.Operators.SubscribeOnMainThreadObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * SubscribeOnMainThreadObservable_1_SubscribeCore_m561697474_gshared (SubscribeOnMainThreadObservable_1_t1377860062 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define SubscribeOnMainThreadObservable_1_SubscribeCore_m561697474(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (SubscribeOnMainThreadObservable_1_t1377860062 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SubscribeOnMainThreadObservable_1_SubscribeCore_m561697474_gshared)(__this, ___observer0, ___cancel1, method)
