﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver/SelectMany<System.Object,System.Object,System.Object>
struct SelectMany_t2449156268;
// UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver<System.Object,System.Object,System.Object>
struct SelectManyOuterObserver_t3582483703;
// System.Object
struct Il2CppObject;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver/SelectMany<System.Object,System.Object,System.Object>::.ctor(UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver<TSource,TCollection,TResult>,TSource,System.IDisposable)
extern "C"  void SelectMany__ctor_m3716214704_gshared (SelectMany_t2449156268 * __this, SelectManyOuterObserver_t3582483703 * ___parent0, Il2CppObject * ___value1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define SelectMany__ctor_m3716214704(__this, ___parent0, ___value1, ___cancel2, method) ((  void (*) (SelectMany_t2449156268 *, SelectManyOuterObserver_t3582483703 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))SelectMany__ctor_m3716214704_gshared)(__this, ___parent0, ___value1, ___cancel2, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver/SelectMany<System.Object,System.Object,System.Object>::OnNext(TCollection)
extern "C"  void SelectMany_OnNext_m240327932_gshared (SelectMany_t2449156268 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define SelectMany_OnNext_m240327932(__this, ___value0, method) ((  void (*) (SelectMany_t2449156268 *, Il2CppObject *, const MethodInfo*))SelectMany_OnNext_m240327932_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver/SelectMany<System.Object,System.Object,System.Object>::OnError(System.Exception)
extern "C"  void SelectMany_OnError_m2623608905_gshared (SelectMany_t2449156268 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define SelectMany_OnError_m2623608905(__this, ___error0, method) ((  void (*) (SelectMany_t2449156268 *, Exception_t1967233988 *, const MethodInfo*))SelectMany_OnError_m2623608905_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyOuterObserver/SelectMany<System.Object,System.Object,System.Object>::OnCompleted()
extern "C"  void SelectMany_OnCompleted_m2980262172_gshared (SelectMany_t2449156268 * __this, const MethodInfo* method);
#define SelectMany_OnCompleted_m2980262172(__this, method) ((  void (*) (SelectMany_t2449156268 *, const MethodInfo*))SelectMany_OnCompleted_m2980262172_gshared)(__this, method)
