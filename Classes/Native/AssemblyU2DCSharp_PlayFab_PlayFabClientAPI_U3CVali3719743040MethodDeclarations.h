﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<ValidateGooglePlayPurchase>c__AnonStoreyB9
struct U3CValidateGooglePlayPurchaseU3Ec__AnonStoreyB9_t3719743040;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<ValidateGooglePlayPurchase>c__AnonStoreyB9::.ctor()
extern "C"  void U3CValidateGooglePlayPurchaseU3Ec__AnonStoreyB9__ctor_m3494138227 (U3CValidateGooglePlayPurchaseU3Ec__AnonStoreyB9_t3719743040 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<ValidateGooglePlayPurchase>c__AnonStoreyB9::<>m__A6(PlayFab.CallRequestContainer)
extern "C"  void U3CValidateGooglePlayPurchaseU3Ec__AnonStoreyB9_U3CU3Em__A6_m533203558 (U3CValidateGooglePlayPurchaseU3Ec__AnonStoreyB9_t3719743040 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
