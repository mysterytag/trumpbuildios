﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/LoginWithPSNRequestCallback
struct LoginWithPSNRequestCallback_t1193662392;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.LoginWithPSNRequest
struct LoginWithPSNRequest_t4060832355;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginWithPS4060832355.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/LoginWithPSNRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void LoginWithPSNRequestCallback__ctor_m3991785543 (LoginWithPSNRequestCallback_t1193662392 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LoginWithPSNRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.LoginWithPSNRequest,System.Object)
extern "C"  void LoginWithPSNRequestCallback_Invoke_m2725484713 (LoginWithPSNRequestCallback_t1193662392 * __this, String_t* ___urlPath0, int32_t ___callId1, LoginWithPSNRequest_t4060832355 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_LoginWithPSNRequestCallback_t1193662392(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, LoginWithPSNRequest_t4060832355 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/LoginWithPSNRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.LoginWithPSNRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LoginWithPSNRequestCallback_BeginInvoke_m2103250910 (LoginWithPSNRequestCallback_t1193662392 * __this, String_t* ___urlPath0, int32_t ___callId1, LoginWithPSNRequest_t4060832355 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LoginWithPSNRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void LoginWithPSNRequestCallback_EndInvoke_m3284827607 (LoginWithPSNRequestCallback_t1193662392 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
