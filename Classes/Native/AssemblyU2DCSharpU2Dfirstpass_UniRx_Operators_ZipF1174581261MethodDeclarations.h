﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ZipFunc`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct ZipFunc_8_t1174581261;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void UniRx.Operators.ZipFunc`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void ZipFunc_8__ctor_m335315371_gshared (ZipFunc_8_t1174581261 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define ZipFunc_8__ctor_m335315371(__this, ___object0, ___method1, method) ((  void (*) (ZipFunc_8_t1174581261 *, Il2CppObject *, IntPtr_t, const MethodInfo*))ZipFunc_8__ctor_m335315371_gshared)(__this, ___object0, ___method1, method)
// TR UniRx.Operators.ZipFunc`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Invoke(T1,T2,T3,T4,T5,T6,T7)
extern "C"  Il2CppObject * ZipFunc_8_Invoke_m535202572_gshared (ZipFunc_8_t1174581261 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, Il2CppObject * ___arg65, Il2CppObject * ___arg76, const MethodInfo* method);
#define ZipFunc_8_Invoke_m535202572(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76, method) ((  Il2CppObject * (*) (ZipFunc_8_t1174581261 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))ZipFunc_8_Invoke_m535202572_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76, method)
// System.IAsyncResult UniRx.Operators.ZipFunc`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,T3,T4,T5,T6,T7,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ZipFunc_8_BeginInvoke_m1896598240_gshared (ZipFunc_8_t1174581261 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, Il2CppObject * ___arg65, Il2CppObject * ___arg76, AsyncCallback_t1363551830 * ___callback7, Il2CppObject * ___object8, const MethodInfo* method);
#define ZipFunc_8_BeginInvoke_m1896598240(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76, ___callback7, ___object8, method) ((  Il2CppObject * (*) (ZipFunc_8_t1174581261 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))ZipFunc_8_BeginInvoke_m1896598240_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76, ___callback7, ___object8, method)
// TR UniRx.Operators.ZipFunc`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * ZipFunc_8_EndInvoke_m2007353122_gshared (ZipFunc_8_t1174581261 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define ZipFunc_8_EndInvoke_m2007353122(__this, ___result0, method) ((  Il2CppObject * (*) (ZipFunc_8_t1174581261 *, Il2CppObject *, const MethodInfo*))ZipFunc_8_EndInvoke_m2007353122_gshared)(__this, ___result0, method)
