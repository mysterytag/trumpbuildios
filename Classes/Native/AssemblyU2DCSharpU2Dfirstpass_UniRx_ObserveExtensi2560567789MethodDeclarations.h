﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ObserveExtensions/<ObserveEveryValueChanged>c__AnonStorey92`2<System.Object,System.Object>
struct U3CObserveEveryValueChangedU3Ec__AnonStorey92_2_t2560567789;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560.h"

// System.Void UniRx.ObserveExtensions/<ObserveEveryValueChanged>c__AnonStorey92`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CObserveEveryValueChangedU3Ec__AnonStorey92_2__ctor_m1175585888_gshared (U3CObserveEveryValueChangedU3Ec__AnonStorey92_2_t2560567789 * __this, const MethodInfo* method);
#define U3CObserveEveryValueChangedU3Ec__AnonStorey92_2__ctor_m1175585888(__this, method) ((  void (*) (U3CObserveEveryValueChangedU3Ec__AnonStorey92_2_t2560567789 *, const MethodInfo*))U3CObserveEveryValueChangedU3Ec__AnonStorey92_2__ctor_m1175585888_gshared)(__this, method)
// System.Collections.IEnumerator UniRx.ObserveExtensions/<ObserveEveryValueChanged>c__AnonStorey92`2<System.Object,System.Object>::<>m__BE(UniRx.IObserver`1<TProperty>,UniRx.CancellationToken)
extern "C"  Il2CppObject * U3CObserveEveryValueChangedU3Ec__AnonStorey92_2_U3CU3Em__BE_m1766637169_gshared (U3CObserveEveryValueChangedU3Ec__AnonStorey92_2_t2560567789 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellationToken1, const MethodInfo* method);
#define U3CObserveEveryValueChangedU3Ec__AnonStorey92_2_U3CU3Em__BE_m1766637169(__this, ___observer0, ___cancellationToken1, method) ((  Il2CppObject * (*) (U3CObserveEveryValueChangedU3Ec__AnonStorey92_2_t2560567789 *, Il2CppObject*, CancellationToken_t1439151560 , const MethodInfo*))U3CObserveEveryValueChangedU3Ec__AnonStorey92_2_U3CU3Em__BE_m1766637169_gshared)(__this, ___observer0, ___cancellationToken1, method)
