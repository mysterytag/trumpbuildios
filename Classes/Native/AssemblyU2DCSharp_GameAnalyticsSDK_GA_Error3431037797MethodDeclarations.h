﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GameAnalyticsSDK_GA_Error_GAErro2545392235.h"
#include "mscorlib_System_String968488902.h"

// System.Void GameAnalyticsSDK.GA_Error::NewEvent(GameAnalyticsSDK.GA_Error/GAErrorSeverity,System.String)
extern "C"  void GA_Error_NewEvent_m4215362377 (Il2CppObject * __this /* static, unused */, int32_t ___severity0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameAnalyticsSDK.GA_Error::CreateNewEvent(GameAnalyticsSDK.GA_Error/GAErrorSeverity,System.String)
extern "C"  void GA_Error_CreateNewEvent_m1098686445 (Il2CppObject * __this /* static, unused */, int32_t ___severity0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
