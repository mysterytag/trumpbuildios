﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type
struct Type_t;
// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SingletonId
struct  SingletonId_t1838183899  : public Il2CppObject
{
public:
	// System.Type Zenject.SingletonId::Type
	Type_t * ___Type_0;
	// System.String Zenject.SingletonId::Identifier
	String_t* ___Identifier_1;

public:
	inline static int32_t get_offset_of_Type_0() { return static_cast<int32_t>(offsetof(SingletonId_t1838183899, ___Type_0)); }
	inline Type_t * get_Type_0() const { return ___Type_0; }
	inline Type_t ** get_address_of_Type_0() { return &___Type_0; }
	inline void set_Type_0(Type_t * value)
	{
		___Type_0 = value;
		Il2CppCodeGenWriteBarrier(&___Type_0, value);
	}

	inline static int32_t get_offset_of_Identifier_1() { return static_cast<int32_t>(offsetof(SingletonId_t1838183899, ___Identifier_1)); }
	inline String_t* get_Identifier_1() const { return ___Identifier_1; }
	inline String_t** get_address_of_Identifier_1() { return &___Identifier_1; }
	inline void set_Identifier_1(String_t* value)
	{
		___Identifier_1 = value;
		Il2CppCodeGenWriteBarrier(&___Identifier_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
