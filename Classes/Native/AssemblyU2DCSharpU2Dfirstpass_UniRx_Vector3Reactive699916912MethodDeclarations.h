﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Vector3ReactiveProperty
struct Vector3ReactiveProperty_t699916912;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Vector3>
struct IEqualityComparer_1_t1554629144;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

// System.Void UniRx.Vector3ReactiveProperty::.ctor()
extern "C"  void Vector3ReactiveProperty__ctor_m1715421553 (Vector3ReactiveProperty_t699916912 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Vector3ReactiveProperty::.ctor(UnityEngine.Vector3)
extern "C"  void Vector3ReactiveProperty__ctor_m3264605896 (Vector3ReactiveProperty_t699916912 * __this, Vector3_t3525329789  ___initialValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Vector3> UniRx.Vector3ReactiveProperty::get_EqualityComparer()
extern "C"  Il2CppObject* Vector3ReactiveProperty_get_EqualityComparer_m2607214143 (Vector3ReactiveProperty_t699916912 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
