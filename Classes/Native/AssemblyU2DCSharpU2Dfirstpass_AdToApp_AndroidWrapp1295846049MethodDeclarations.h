﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AdToApp.AndroidWrapper.AdToAppAndroidWrapper/<ShowBannerAtPositon>c__AnonStorey2D
struct U3CShowBannerAtPositonU3Ec__AnonStorey2D_t1295846049;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper/<ShowBannerAtPositon>c__AnonStorey2D::.ctor()
extern "C"  void U3CShowBannerAtPositonU3Ec__AnonStorey2D__ctor_m811311055 (U3CShowBannerAtPositonU3Ec__AnonStorey2D_t1295846049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper/<ShowBannerAtPositon>c__AnonStorey2D::<>m__4(System.Object)
extern "C"  void U3CShowBannerAtPositonU3Ec__AnonStorey2D_U3CU3Em__4_m2117761750 (U3CShowBannerAtPositonU3Ec__AnonStorey2D_t1295846049 * __this, Il2CppObject * ___wrapperContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
