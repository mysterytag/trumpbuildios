﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.CombineLatestObservable`1<System.Object>
struct CombineLatestObservable_1_t3509237416;
// UniRx.IObservable`1<System.Object>[]
struct IObservable_1U5BU5D_t2205425841;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Object>>
struct IObserver_1_t920630341;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.CombineLatestObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>[])
extern "C"  void CombineLatestObservable_1__ctor_m3644694771_gshared (CombineLatestObservable_1_t3509237416 * __this, IObservable_1U5BU5D_t2205425841* ___sources0, const MethodInfo* method);
#define CombineLatestObservable_1__ctor_m3644694771(__this, ___sources0, method) ((  void (*) (CombineLatestObservable_1_t3509237416 *, IObservable_1U5BU5D_t2205425841*, const MethodInfo*))CombineLatestObservable_1__ctor_m3644694771_gshared)(__this, ___sources0, method)
// System.IDisposable UniRx.Operators.CombineLatestObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<System.Collections.Generic.IList`1<T>>,System.IDisposable)
extern "C"  Il2CppObject * CombineLatestObservable_1_SubscribeCore_m3998794463_gshared (CombineLatestObservable_1_t3509237416 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define CombineLatestObservable_1_SubscribeCore_m3998794463(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (CombineLatestObservable_1_t3509237416 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))CombineLatestObservable_1_SubscribeCore_m3998794463_gshared)(__this, ___observer0, ___cancel1, method)
