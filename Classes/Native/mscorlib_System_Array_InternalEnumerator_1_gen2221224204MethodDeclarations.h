﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2221224204.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharp_PlayFab_UUnit_StructNumFieldTest2313733701.h"

// System.Void System.Array/InternalEnumerator`1<PlayFab.UUnit.StructNumFieldTest>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1941878332_gshared (InternalEnumerator_1_t2221224204 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1941878332(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2221224204 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1941878332_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<PlayFab.UUnit.StructNumFieldTest>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1277558244_gshared (InternalEnumerator_1_t2221224204 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1277558244(__this, method) ((  void (*) (InternalEnumerator_1_t2221224204 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1277558244_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<PlayFab.UUnit.StructNumFieldTest>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2939676944_gshared (InternalEnumerator_1_t2221224204 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2939676944(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2221224204 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2939676944_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<PlayFab.UUnit.StructNumFieldTest>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3215170323_gshared (InternalEnumerator_1_t2221224204 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3215170323(__this, method) ((  void (*) (InternalEnumerator_1_t2221224204 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3215170323_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<PlayFab.UUnit.StructNumFieldTest>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2781048272_gshared (InternalEnumerator_1_t2221224204 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2781048272(__this, method) ((  bool (*) (InternalEnumerator_1_t2221224204 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2781048272_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<PlayFab.UUnit.StructNumFieldTest>::get_Current()
extern "C"  StructNumFieldTest_t2313733701  InternalEnumerator_1_get_Current_m3986932163_gshared (InternalEnumerator_1_t2221224204 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3986932163(__this, method) ((  StructNumFieldTest_t2313733701  (*) (InternalEnumerator_1_t2221224204 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3986932163_gshared)(__this, method)
