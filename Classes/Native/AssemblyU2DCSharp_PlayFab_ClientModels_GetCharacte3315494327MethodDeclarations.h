﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetCharacterLeaderboardResult
struct GetCharacterLeaderboardResult_t3315494327;
// System.Collections.Generic.List`1<PlayFab.ClientModels.CharacterLeaderboardEntry>
struct List_1_t3914494247;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.GetCharacterLeaderboardResult::.ctor()
extern "C"  void GetCharacterLeaderboardResult__ctor_m530983250 (GetCharacterLeaderboardResult_t3315494327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PlayFab.ClientModels.CharacterLeaderboardEntry> PlayFab.ClientModels.GetCharacterLeaderboardResult::get_Leaderboard()
extern "C"  List_1_t3914494247 * GetCharacterLeaderboardResult_get_Leaderboard_m3630628017 (GetCharacterLeaderboardResult_t3315494327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetCharacterLeaderboardResult::set_Leaderboard(System.Collections.Generic.List`1<PlayFab.ClientModels.CharacterLeaderboardEntry>)
extern "C"  void GetCharacterLeaderboardResult_set_Leaderboard_m1342814978 (GetCharacterLeaderboardResult_t3315494327 * __this, List_1_t3914494247 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
