﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Comparison`1<System.DateTime>
struct Comparison_1_t3042708812;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_DateTime339033936.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Comparison`1<System.DateTime>::.ctor(System.Object,System.IntPtr)
extern "C"  void Comparison_1__ctor_m2850136343_gshared (Comparison_1_t3042708812 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Comparison_1__ctor_m2850136343(__this, ___object0, ___method1, method) ((  void (*) (Comparison_1_t3042708812 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m2850136343_gshared)(__this, ___object0, ___method1, method)
// System.Int32 System.Comparison`1<System.DateTime>::Invoke(T,T)
extern "C"  int32_t Comparison_1_Invoke_m1886131465_gshared (Comparison_1_t3042708812 * __this, DateTime_t339033936  ___x0, DateTime_t339033936  ___y1, const MethodInfo* method);
#define Comparison_1_Invoke_m1886131465(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t3042708812 *, DateTime_t339033936 , DateTime_t339033936 , const MethodInfo*))Comparison_1_Invoke_m1886131465_gshared)(__this, ___x0, ___y1, method)
// System.IAsyncResult System.Comparison`1<System.DateTime>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Comparison_1_BeginInvoke_m2860043138_gshared (Comparison_1_t3042708812 * __this, DateTime_t339033936  ___x0, DateTime_t339033936  ___y1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Comparison_1_BeginInvoke_m2860043138(__this, ___x0, ___y1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Comparison_1_t3042708812 *, DateTime_t339033936 , DateTime_t339033936 , AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Comparison_1_BeginInvoke_m2860043138_gshared)(__this, ___x0, ___y1, ___callback2, ___object3, method)
// System.Int32 System.Comparison`1<System.DateTime>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Comparison_1_EndInvoke_m3682698435_gshared (Comparison_1_t3042708812 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Comparison_1_EndInvoke_m3682698435(__this, ___result0, method) ((  int32_t (*) (Comparison_1_t3042708812 *, Il2CppObject *, const MethodInfo*))Comparison_1_EndInvoke_m3682698435_gshared)(__this, ___result0, method)
