﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CoroutineExtension/<DoAndWaitCallback>c__IteratorA
struct U3CDoAndWaitCallbackU3Ec__IteratorA_t829710692;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void CoroutineExtension/<DoAndWaitCallback>c__IteratorA::.ctor()
extern "C"  void U3CDoAndWaitCallbackU3Ec__IteratorA__ctor_m973728863 (U3CDoAndWaitCallbackU3Ec__IteratorA_t829710692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CoroutineExtension/<DoAndWaitCallback>c__IteratorA::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDoAndWaitCallbackU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m68089757 (U3CDoAndWaitCallbackU3Ec__IteratorA_t829710692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CoroutineExtension/<DoAndWaitCallback>c__IteratorA::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoAndWaitCallbackU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m3772110129 (U3CDoAndWaitCallbackU3Ec__IteratorA_t829710692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CoroutineExtension/<DoAndWaitCallback>c__IteratorA::MoveNext()
extern "C"  bool U3CDoAndWaitCallbackU3Ec__IteratorA_MoveNext_m2139677725 (U3CDoAndWaitCallbackU3Ec__IteratorA_t829710692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CoroutineExtension/<DoAndWaitCallback>c__IteratorA::Dispose()
extern "C"  void U3CDoAndWaitCallbackU3Ec__IteratorA_Dispose_m3643817180 (U3CDoAndWaitCallbackU3Ec__IteratorA_t829710692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CoroutineExtension/<DoAndWaitCallback>c__IteratorA::Reset()
extern "C"  void U3CDoAndWaitCallbackU3Ec__IteratorA_Reset_m2915129100 (U3CDoAndWaitCallbackU3Ec__IteratorA_t829710692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CoroutineExtension/<DoAndWaitCallback>c__IteratorA::<>m__CB()
extern "C"  void U3CDoAndWaitCallbackU3Ec__IteratorA_U3CU3Em__CB_m1759160327 (U3CDoAndWaitCallbackU3Ec__IteratorA_t829710692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
