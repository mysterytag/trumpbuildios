﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Stream`1/<Listen>c__AnonStorey128<System.Int64>
struct U3CListenU3Ec__AnonStorey128_t2078774906;

#include "codegen/il2cpp-codegen.h"

// System.Void Stream`1/<Listen>c__AnonStorey128<System.Int64>::.ctor()
extern "C"  void U3CListenU3Ec__AnonStorey128__ctor_m481776974_gshared (U3CListenU3Ec__AnonStorey128_t2078774906 * __this, const MethodInfo* method);
#define U3CListenU3Ec__AnonStorey128__ctor_m481776974(__this, method) ((  void (*) (U3CListenU3Ec__AnonStorey128_t2078774906 *, const MethodInfo*))U3CListenU3Ec__AnonStorey128__ctor_m481776974_gshared)(__this, method)
// System.Void Stream`1/<Listen>c__AnonStorey128<System.Int64>::<>m__1B9(T)
extern "C"  void U3CListenU3Ec__AnonStorey128_U3CU3Em__1B9_m151167339_gshared (U3CListenU3Ec__AnonStorey128_t2078774906 * __this, int64_t ____0, const MethodInfo* method);
#define U3CListenU3Ec__AnonStorey128_U3CU3Em__1B9_m151167339(__this, ____0, method) ((  void (*) (U3CListenU3Ec__AnonStorey128_t2078774906 *, int64_t, const MethodInfo*))U3CListenU3Ec__AnonStorey128_U3CU3Em__1B9_m151167339_gshared)(__this, ____0, method)
