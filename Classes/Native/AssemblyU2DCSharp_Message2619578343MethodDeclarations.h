﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Message
struct Message_t2619578343;
// System.String
struct String_t;
// UnityEngine.Sprite
struct Sprite_t4006040370;
// UnityEngine.RectTransform
struct RectTransform_t3317474837;
// <>__AnonType3`2<System.Boolean,System.Int32>
struct U3CU3E__AnonType3_2_t3095608592;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "UnityEngine_UnityEngine_Sprite4006040370.h"
#include "UnityEngine_UnityEngine_RectTransform3317474837.h"

// System.Void Message::.ctor()
extern "C"  void Message__ctor_m3445918564 (Message_t2619578343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Message::Show(System.String)
extern "C"  void Message_Show_m3165151493 (Message_t2619578343 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Message::Show(System.String,UnityEngine.Sprite)
extern "C"  void Message_Show_m452428517 (Message_t2619578343 * __this, String_t* ___text0, Sprite_t4006040370 * ___sprite1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Message::PostInject()
extern "C"  void Message_PostInject_m122226737 (Message_t2619578343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Message::SetAnchoredPositionX(UnityEngine.RectTransform,System.Int32)
extern "C"  void Message_SetAnchoredPositionX_m998189295 (Il2CppObject * __this /* static, unused */, RectTransform_t3317474837 * ___iconRectTransform0, int32_t ___x1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Message::ShowErrorMessage()
extern "C"  void Message_ShowErrorMessage_m1857742940 (Message_t2619578343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Message::ShowUpgrade(System.String,System.String)
extern "C"  void Message_ShowUpgrade_m380028605 (Message_t2619578343 * __this, String_t* ___lasUpgrade0, String_t* ___upgradeText1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// <>__AnonType3`2<System.Boolean,System.Int32> Message::<PostInject>m__3E(System.Boolean,System.Int32)
extern "C"  U3CU3E__AnonType3_2_t3095608592 * Message_U3CPostInjectU3Em__3E_m3718947806 (Il2CppObject * __this /* static, unused */, bool ___b0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
