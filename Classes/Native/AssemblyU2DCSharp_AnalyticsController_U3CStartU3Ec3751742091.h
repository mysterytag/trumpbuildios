﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;
// System.Exception
struct Exception_t1967233988;
// System.Object
struct Il2CppObject;
// AnalyticsController
struct AnalyticsController_t2265609122;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnalyticsController/<Start>c__Iterator1C
struct  U3CStartU3Ec__Iterator1C_t3751742091  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.String> AnalyticsController/<Start>c__Iterator1C::<dict>__0
	Dictionary_2_t2606186806 * ___U3CdictU3E__0_0;
	// System.Exception AnalyticsController/<Start>c__Iterator1C::<e>__1
	Exception_t1967233988 * ___U3CeU3E__1_1;
	// System.Int32 AnalyticsController/<Start>c__Iterator1C::$PC
	int32_t ___U24PC_2;
	// System.Object AnalyticsController/<Start>c__Iterator1C::$current
	Il2CppObject * ___U24current_3;
	// AnalyticsController AnalyticsController/<Start>c__Iterator1C::<>f__this
	AnalyticsController_t2265609122 * ___U3CU3Ef__this_4;

public:
	inline static int32_t get_offset_of_U3CdictU3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator1C_t3751742091, ___U3CdictU3E__0_0)); }
	inline Dictionary_2_t2606186806 * get_U3CdictU3E__0_0() const { return ___U3CdictU3E__0_0; }
	inline Dictionary_2_t2606186806 ** get_address_of_U3CdictU3E__0_0() { return &___U3CdictU3E__0_0; }
	inline void set_U3CdictU3E__0_0(Dictionary_2_t2606186806 * value)
	{
		___U3CdictU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CdictU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CeU3E__1_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator1C_t3751742091, ___U3CeU3E__1_1)); }
	inline Exception_t1967233988 * get_U3CeU3E__1_1() const { return ___U3CeU3E__1_1; }
	inline Exception_t1967233988 ** get_address_of_U3CeU3E__1_1() { return &___U3CeU3E__1_1; }
	inline void set_U3CeU3E__1_1(Exception_t1967233988 * value)
	{
		___U3CeU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CeU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator1C_t3751742091, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator1C_t3751742091, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator1C_t3751742091, ___U3CU3Ef__this_4)); }
	inline AnalyticsController_t2265609122 * get_U3CU3Ef__this_4() const { return ___U3CU3Ef__this_4; }
	inline AnalyticsController_t2265609122 ** get_address_of_U3CU3Ef__this_4() { return &___U3CU3Ef__this_4; }
	inline void set_U3CU3Ef__this_4(AnalyticsController_t2265609122 * value)
	{
		___U3CU3Ef__this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
