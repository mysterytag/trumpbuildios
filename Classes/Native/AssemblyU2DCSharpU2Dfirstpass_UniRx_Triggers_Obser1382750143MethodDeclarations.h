﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Triggers.ObservableBeginDragTrigger
struct ObservableBeginDragTrigger_t1382750143;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t3205101634;
// UniRx.IObservable`1<UnityEngine.EventSystems.PointerEventData>
struct IObservable_1_t2963899998;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve3205101634.h"

// System.Void UniRx.Triggers.ObservableBeginDragTrigger::.ctor()
extern "C"  void ObservableBeginDragTrigger__ctor_m2885262110 (ObservableBeginDragTrigger_t1382750143 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableBeginDragTrigger::UnityEngine.EventSystems.IBeginDragHandler.OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ObservableBeginDragTrigger_UnityEngine_EventSystems_IBeginDragHandler_OnBeginDrag_m2982735903 (ObservableBeginDragTrigger_t1382750143 * __this, PointerEventData_t3205101634 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableBeginDragTrigger::OnBeginDragAsObservable()
extern "C"  Il2CppObject* ObservableBeginDragTrigger_OnBeginDragAsObservable_m3521375795 (ObservableBeginDragTrigger_t1382750143 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Triggers.ObservableBeginDragTrigger::RaiseOnCompletedOnDestroy()
extern "C"  void ObservableBeginDragTrigger_RaiseOnCompletedOnDestroy_m706721623 (ObservableBeginDragTrigger_t1382750143 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
