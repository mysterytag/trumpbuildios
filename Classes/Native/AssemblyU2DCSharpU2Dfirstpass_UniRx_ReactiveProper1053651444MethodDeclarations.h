﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.IObservable`1<System.Boolean>
struct IObservable_1_t4264771001;
// System.Collections.Generic.IEnumerable`1<UniRx.IObservable`1<System.Boolean>>
struct IEnumerable_1_t2841958061;
// System.Collections.Generic.IList`1<System.Boolean>
struct IList_1_t2377497655;

#include "codegen/il2cpp-codegen.h"

// UniRx.IObservable`1<System.Boolean> UniRx.ReactivePropertyExtensions::CombineLatestValuesAreAllTrue(System.Collections.Generic.IEnumerable`1<UniRx.IObservable`1<System.Boolean>>)
extern "C"  Il2CppObject* ReactivePropertyExtensions_CombineLatestValuesAreAllTrue_m2288590888 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___sources0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.Boolean> UniRx.ReactivePropertyExtensions::CombineLatestValuesAreAllFalse(System.Collections.Generic.IEnumerable`1<UniRx.IObservable`1<System.Boolean>>)
extern "C"  Il2CppObject* ReactivePropertyExtensions_CombineLatestValuesAreAllFalse_m3124093143 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___sources0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.ReactivePropertyExtensions::<CombineLatestValuesAreAllTrue>m__C4(System.Collections.Generic.IList`1<System.Boolean>)
extern "C"  bool ReactivePropertyExtensions_U3CCombineLatestValuesAreAllTrueU3Em__C4_m2362911536 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___xs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.ReactivePropertyExtensions::<CombineLatestValuesAreAllFalse>m__C5(System.Collections.Generic.IList`1<System.Boolean>)
extern "C"  bool ReactivePropertyExtensions_U3CCombineLatestValuesAreAllFalseU3Em__C5_m2400503264 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___xs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
