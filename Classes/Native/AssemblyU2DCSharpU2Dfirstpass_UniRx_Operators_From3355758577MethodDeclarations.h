﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,UniRx.Tuple`3<System.Object,System.Object,System.Object>>
struct FromEvent_t3355758577;
// UniRx.Operators.FromEventObservable`2<System.Object,UniRx.Tuple`3<System.Object,System.Object,System.Object>>
struct FromEventObservable_2_t3065932279;
// UniRx.IObserver`1<UniRx.Tuple`3<System.Object,System.Object,System.Object>>
struct IObserver_1_t3862898005;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_3_gen1650899102.h"

// System.Void UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,UniRx.Tuple`3<System.Object,System.Object,System.Object>>::.ctor(UniRx.Operators.FromEventObservable`2<TDelegate,TEventArgs>,UniRx.IObserver`1<TEventArgs>)
extern "C"  void FromEvent__ctor_m367008356_gshared (FromEvent_t3355758577 * __this, FromEventObservable_2_t3065932279 * ___parent0, Il2CppObject* ___observer1, const MethodInfo* method);
#define FromEvent__ctor_m367008356(__this, ___parent0, ___observer1, method) ((  void (*) (FromEvent_t3355758577 *, FromEventObservable_2_t3065932279 *, Il2CppObject*, const MethodInfo*))FromEvent__ctor_m367008356_gshared)(__this, ___parent0, ___observer1, method)
// System.Boolean UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,UniRx.Tuple`3<System.Object,System.Object,System.Object>>::Register()
extern "C"  bool FromEvent_Register_m164332738_gshared (FromEvent_t3355758577 * __this, const MethodInfo* method);
#define FromEvent_Register_m164332738(__this, method) ((  bool (*) (FromEvent_t3355758577 *, const MethodInfo*))FromEvent_Register_m164332738_gshared)(__this, method)
// System.Void UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,UniRx.Tuple`3<System.Object,System.Object,System.Object>>::OnNext(TEventArgs)
extern "C"  void FromEvent_OnNext_m1301955626_gshared (FromEvent_t3355758577 * __this, Tuple_3_t1650899102  ___args0, const MethodInfo* method);
#define FromEvent_OnNext_m1301955626(__this, ___args0, method) ((  void (*) (FromEvent_t3355758577 *, Tuple_3_t1650899102 , const MethodInfo*))FromEvent_OnNext_m1301955626_gshared)(__this, ___args0, method)
// System.Void UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,UniRx.Tuple`3<System.Object,System.Object,System.Object>>::Dispose()
extern "C"  void FromEvent_Dispose_m4254234406_gshared (FromEvent_t3355758577 * __this, const MethodInfo* method);
#define FromEvent_Dispose_m4254234406(__this, method) ((  void (*) (FromEvent_t3355758577 *, const MethodInfo*))FromEvent_Dispose_m4254234406_gshared)(__this, method)
