﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex/SelectManyObserver<System.Object,UniRx.Unit,System.Object>
struct SelectManyObserver_t2520418200;
// UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex<System.Object,UniRx.Unit,System.Object>
struct SelectManyObserverWithIndex_t2099930242;
// System.Object
struct Il2CppObject;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex/SelectManyObserver<System.Object,UniRx.Unit,System.Object>::.ctor(UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex<TSource,TCollection,TResult>,TSource,System.Int32,System.IDisposable)
extern "C"  void SelectManyObserver__ctor_m4057789507_gshared (SelectManyObserver_t2520418200 * __this, SelectManyObserverWithIndex_t2099930242 * ___parent0, Il2CppObject * ___value1, int32_t ___index2, Il2CppObject * ___cancel3, const MethodInfo* method);
#define SelectManyObserver__ctor_m4057789507(__this, ___parent0, ___value1, ___index2, ___cancel3, method) ((  void (*) (SelectManyObserver_t2520418200 *, SelectManyObserverWithIndex_t2099930242 *, Il2CppObject *, int32_t, Il2CppObject *, const MethodInfo*))SelectManyObserver__ctor_m4057789507_gshared)(__this, ___parent0, ___value1, ___index2, ___cancel3, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex/SelectManyObserver<System.Object,UniRx.Unit,System.Object>::OnNext(TCollection)
extern "C"  void SelectManyObserver_OnNext_m1974656111_gshared (SelectManyObserver_t2520418200 * __this, Unit_t2558286038  ___value0, const MethodInfo* method);
#define SelectManyObserver_OnNext_m1974656111(__this, ___value0, method) ((  void (*) (SelectManyObserver_t2520418200 *, Unit_t2558286038 , const MethodInfo*))SelectManyObserver_OnNext_m1974656111_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex/SelectManyObserver<System.Object,UniRx.Unit,System.Object>::OnError(System.Exception)
extern "C"  void SelectManyObserver_OnError_m1243755900_gshared (SelectManyObserver_t2520418200 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define SelectManyObserver_OnError_m1243755900(__this, ___error0, method) ((  void (*) (SelectManyObserver_t2520418200 *, Exception_t1967233988 *, const MethodInfo*))SelectManyObserver_OnError_m1243755900_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex/SelectManyObserver<System.Object,UniRx.Unit,System.Object>::OnCompleted()
extern "C"  void SelectManyObserver_OnCompleted_m2379760335_gshared (SelectManyObserver_t2520418200 * __this, const MethodInfo* method);
#define SelectManyObserver_OnCompleted_m2379760335(__this, method) ((  void (*) (SelectManyObserver_t2520418200 *, const MethodInfo*))SelectManyObserver_OnCompleted_m2379760335_gshared)(__this, method)
