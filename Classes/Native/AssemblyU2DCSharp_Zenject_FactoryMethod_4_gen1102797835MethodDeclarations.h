﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.FactoryMethod`4<System.Object,System.Object,System.Object,System.Object>
struct FactoryMethod_4_t1102797835;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t3431720054;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void Zenject.FactoryMethod`4<System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C"  void FactoryMethod_4__ctor_m2895788885_gshared (FactoryMethod_4_t1102797835 * __this, const MethodInfo* method);
#define FactoryMethod_4__ctor_m2895788885(__this, method) ((  void (*) (FactoryMethod_4_t1102797835 *, const MethodInfo*))FactoryMethod_4__ctor_m2895788885_gshared)(__this, method)
// System.Type Zenject.FactoryMethod`4<System.Object,System.Object,System.Object,System.Object>::get_ConstructedType()
extern "C"  Type_t * FactoryMethod_4_get_ConstructedType_m719137536_gshared (FactoryMethod_4_t1102797835 * __this, const MethodInfo* method);
#define FactoryMethod_4_get_ConstructedType_m719137536(__this, method) ((  Type_t * (*) (FactoryMethod_4_t1102797835 *, const MethodInfo*))FactoryMethod_4_get_ConstructedType_m719137536_gshared)(__this, method)
// System.Type[] Zenject.FactoryMethod`4<System.Object,System.Object,System.Object,System.Object>::get_ProvidedTypes()
extern "C"  TypeU5BU5D_t3431720054* FactoryMethod_4_get_ProvidedTypes_m809037032_gshared (FactoryMethod_4_t1102797835 * __this, const MethodInfo* method);
#define FactoryMethod_4_get_ProvidedTypes_m809037032(__this, method) ((  TypeU5BU5D_t3431720054* (*) (FactoryMethod_4_t1102797835 *, const MethodInfo*))FactoryMethod_4_get_ProvidedTypes_m809037032_gshared)(__this, method)
// TValue Zenject.FactoryMethod`4<System.Object,System.Object,System.Object,System.Object>::Create(TParam1,TParam2,TParam3)
extern "C"  Il2CppObject * FactoryMethod_4_Create_m1378645684_gshared (FactoryMethod_4_t1102797835 * __this, Il2CppObject * ___param10, Il2CppObject * ___param21, Il2CppObject * ___param32, const MethodInfo* method);
#define FactoryMethod_4_Create_m1378645684(__this, ___param10, ___param21, ___param32, method) ((  Il2CppObject * (*) (FactoryMethod_4_t1102797835 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))FactoryMethod_4_Create_m1378645684_gshared)(__this, ___param10, ___param21, ___param32, method)
