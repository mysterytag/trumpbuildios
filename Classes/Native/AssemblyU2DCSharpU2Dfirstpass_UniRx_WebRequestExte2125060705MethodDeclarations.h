﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.IObservable`1<System.Net.WebResponse>
struct IObservable_1_t2170090779;
// System.Net.WebRequest
struct WebRequest_t3488810021;
// UniRx.IObservable`1<System.Net.HttpWebResponse>
struct IObservable_1_t2426765171;
// System.Net.HttpWebRequest
struct HttpWebRequest_t171953869;
// UniRx.IObservable`1<System.IO.Stream>
struct IObservable_1_t4272795235;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_WebRequest3488810021.h"
#include "System_System_Net_HttpWebRequest171953869.h"

// UniRx.IObservable`1<System.Net.WebResponse> UniRx.WebRequestExtensions::GetResponseAsObservable(System.Net.WebRequest)
extern "C"  Il2CppObject* WebRequestExtensions_GetResponseAsObservable_m313833062 (Il2CppObject * __this /* static, unused */, WebRequest_t3488810021 * ___request0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.Net.HttpWebResponse> UniRx.WebRequestExtensions::GetResponseAsObservable(System.Net.HttpWebRequest)
extern "C"  Il2CppObject* WebRequestExtensions_GetResponseAsObservable_m3915343894 (Il2CppObject * __this /* static, unused */, HttpWebRequest_t171953869 * ___request0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.IObservable`1<System.IO.Stream> UniRx.WebRequestExtensions::GetRequestStreamAsObservable(System.Net.WebRequest)
extern "C"  Il2CppObject* WebRequestExtensions_GetRequestStreamAsObservable_m1484252616 (Il2CppObject * __this /* static, unused */, WebRequest_t3488810021 * ___request0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
