﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Scheduler/ImmediateScheduler
struct ImmediateScheduler_t599915562;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action
struct Action_t437523947;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTimeOffset3712260035.h"
#include "System_Core_System_Action437523947.h"
#include "mscorlib_System_TimeSpan763862892.h"

// System.Void UniRx.Scheduler/ImmediateScheduler::.ctor()
extern "C"  void ImmediateScheduler__ctor_m4164585785 (ImmediateScheduler_t599915562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTimeOffset UniRx.Scheduler/ImmediateScheduler::get_Now()
extern "C"  DateTimeOffset_t3712260035  ImmediateScheduler_get_Now_m1550959358 (ImmediateScheduler_t599915562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable UniRx.Scheduler/ImmediateScheduler::Schedule(System.Action)
extern "C"  Il2CppObject * ImmediateScheduler_Schedule_m3476598752 (ImmediateScheduler_t599915562 * __this, Action_t437523947 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable UniRx.Scheduler/ImmediateScheduler::Schedule(System.TimeSpan,System.Action)
extern "C"  Il2CppObject * ImmediateScheduler_Schedule_m9792438 (ImmediateScheduler_t599915562 * __this, TimeSpan_t763862892  ___dueTime0, Action_t437523947 * ___action1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
