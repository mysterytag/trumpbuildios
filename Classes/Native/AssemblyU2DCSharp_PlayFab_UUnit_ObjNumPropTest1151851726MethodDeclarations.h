﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.UUnit.ObjNumPropTest
struct ObjNumPropTest_t1151851726;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.UUnit.ObjNumPropTest::.ctor()
extern "C"  void ObjNumPropTest__ctor_m2949337895 (ObjNumPropTest_t1151851726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.ObjNumPropTest::.cctor()
extern "C"  void ObjNumPropTest__cctor_m753065318 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.SByte PlayFab.UUnit.ObjNumPropTest::get_SbyteValue()
extern "C"  int8_t ObjNumPropTest_get_SbyteValue_m1967440285 (ObjNumPropTest_t1151851726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.ObjNumPropTest::set_SbyteValue(System.SByte)
extern "C"  void ObjNumPropTest_set_SbyteValue_m3559605524 (ObjNumPropTest_t1151851726 * __this, int8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte PlayFab.UUnit.ObjNumPropTest::get_ByteValue()
extern "C"  uint8_t ObjNumPropTest_get_ByteValue_m239361113 (ObjNumPropTest_t1151851726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.ObjNumPropTest::set_ByteValue(System.Byte)
extern "C"  void ObjNumPropTest_set_ByteValue_m761242586 (ObjNumPropTest_t1151851726 * __this, uint8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 PlayFab.UUnit.ObjNumPropTest::get_ShortValue()
extern "C"  int16_t ObjNumPropTest_get_ShortValue_m4049017507 (ObjNumPropTest_t1151851726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.ObjNumPropTest::set_ShortValue(System.Int16)
extern "C"  void ObjNumPropTest_set_ShortValue_m3132982234 (ObjNumPropTest_t1151851726 * __this, int16_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 PlayFab.UUnit.ObjNumPropTest::get_UshortValue()
extern "C"  uint16_t ObjNumPropTest_get_UshortValue_m500669041 (ObjNumPropTest_t1151851726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.ObjNumPropTest::set_UshortValue(System.UInt16)
extern "C"  void ObjNumPropTest_set_UshortValue_m2480139010 (ObjNumPropTest_t1151851726 * __this, uint16_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlayFab.UUnit.ObjNumPropTest::get_IntValue()
extern "C"  int32_t ObjNumPropTest_get_IntValue_m1960879382 (ObjNumPropTest_t1151851726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.ObjNumPropTest::set_IntValue(System.Int32)
extern "C"  void ObjNumPropTest_set_IntValue_m298036941 (ObjNumPropTest_t1151851726 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 PlayFab.UUnit.ObjNumPropTest::get_UintValue()
extern "C"  uint32_t ObjNumPropTest_get_UintValue_m2014401816 (ObjNumPropTest_t1151851726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.ObjNumPropTest::set_UintValue(System.UInt32)
extern "C"  void ObjNumPropTest_set_UintValue_m2512279291 (ObjNumPropTest_t1151851726 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 PlayFab.UUnit.ObjNumPropTest::get_LongValue()
extern "C"  int64_t ObjNumPropTest_get_LongValue_m2243466946 (ObjNumPropTest_t1151851726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.ObjNumPropTest::set_LongValue(System.Int64)
extern "C"  void ObjNumPropTest_set_LongValue_m2991566927 (ObjNumPropTest_t1151851726 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 PlayFab.UUnit.ObjNumPropTest::get_UlongValue()
extern "C"  uint64_t ObjNumPropTest_get_UlongValue_m3262891682 (ObjNumPropTest_t1151851726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.ObjNumPropTest::set_UlongValue(System.UInt64)
extern "C"  void ObjNumPropTest_set_UlongValue_m317705281 (ObjNumPropTest_t1151851726 * __this, uint64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single PlayFab.UUnit.ObjNumPropTest::get_FloatValue()
extern "C"  float ObjNumPropTest_get_FloatValue_m1608035207 (ObjNumPropTest_t1151851726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.ObjNumPropTest::set_FloatValue(System.Single)
extern "C"  void ObjNumPropTest_set_FloatValue_m4024355804 (ObjNumPropTest_t1151851726 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double PlayFab.UUnit.ObjNumPropTest::get_DoubleValue()
extern "C"  double ObjNumPropTest_get_DoubleValue_m416764281 (ObjNumPropTest_t1151851726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.ObjNumPropTest::set_DoubleValue(System.Double)
extern "C"  void ObjNumPropTest_set_DoubleValue_m1157921274 (ObjNumPropTest_t1151851726 * __this, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
