﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZergRush.ReactiveCollectionExtension/FilterCollectionRX`1<System.Object>
struct FilterCollectionRX_1_t3654614494;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<UniRx.CollectionAddEvent`1<System.Object>>
struct Action_1_t2564974692;
// System.Action`1<CollectionRemoveEvent`1<System.Object>>
struct Action_1_t1046711963;
// System.Action`1<CollectionMoveEvent`1<System.Object>>
struct Action_1_t3720508086;
// System.Action`1<CollectionReplaceEvent`1<System.Object>>
struct Action_1_t2645824775;
// System.Action
struct Action_t437523947;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2320212868;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"

// System.Void ZergRush.ReactiveCollectionExtension/FilterCollectionRX`1<System.Object>::.ctor()
extern "C"  void FilterCollectionRX_1__ctor_m1749687145_gshared (FilterCollectionRX_1_t3654614494 * __this, const MethodInfo* method);
#define FilterCollectionRX_1__ctor_m1749687145(__this, method) ((  void (*) (FilterCollectionRX_1_t3654614494 *, const MethodInfo*))FilterCollectionRX_1__ctor_m1749687145_gshared)(__this, method)
// System.Collections.IEnumerator ZergRush.ReactiveCollectionExtension/FilterCollectionRX`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * FilterCollectionRX_1_System_Collections_IEnumerable_GetEnumerator_m2985965928_gshared (FilterCollectionRX_1_t3654614494 * __this, const MethodInfo* method);
#define FilterCollectionRX_1_System_Collections_IEnumerable_GetEnumerator_m2985965928(__this, method) ((  Il2CppObject * (*) (FilterCollectionRX_1_t3654614494 *, const MethodInfo*))FilterCollectionRX_1_System_Collections_IEnumerable_GetEnumerator_m2985965928_gshared)(__this, method)
// System.IDisposable ZergRush.ReactiveCollectionExtension/FilterCollectionRX`1<System.Object>::Listen(System.Action`1<UniRx.CollectionAddEvent`1<T>>,System.Action`1<CollectionRemoveEvent`1<T>>,System.Action`1<CollectionMoveEvent`1<T>>,System.Action`1<CollectionReplaceEvent`1<T>>,System.Action)
extern "C"  Il2CppObject * FilterCollectionRX_1_Listen_m4051451108_gshared (FilterCollectionRX_1_t3654614494 * __this, Action_1_t2564974692 * ___onAdd0, Action_1_t1046711963 * ___onRemove1, Action_1_t3720508086 * ___onMove2, Action_1_t2645824775 * ___onReplace3, Action_t437523947 * ___onReset4, const MethodInfo* method);
#define FilterCollectionRX_1_Listen_m4051451108(__this, ___onAdd0, ___onRemove1, ___onMove2, ___onReplace3, ___onReset4, method) ((  Il2CppObject * (*) (FilterCollectionRX_1_t3654614494 *, Action_1_t2564974692 *, Action_1_t1046711963 *, Action_1_t3720508086 *, Action_1_t2645824775 *, Action_t437523947 *, const MethodInfo*))FilterCollectionRX_1_Listen_m4051451108_gshared)(__this, ___onAdd0, ___onRemove1, ___onMove2, ___onReplace3, ___onReset4, method)
// System.Collections.Generic.IEnumerator`1<T> ZergRush.ReactiveCollectionExtension/FilterCollectionRX`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* FilterCollectionRX_1_GetEnumerator_m4175857701_gshared (FilterCollectionRX_1_t3654614494 * __this, const MethodInfo* method);
#define FilterCollectionRX_1_GetEnumerator_m4175857701(__this, method) ((  Il2CppObject* (*) (FilterCollectionRX_1_t3654614494 *, const MethodInfo*))FilterCollectionRX_1_GetEnumerator_m4175857701_gshared)(__this, method)
// System.Int32 ZergRush.ReactiveCollectionExtension/FilterCollectionRX`1<System.Object>::get_Count()
extern "C"  int32_t FilterCollectionRX_1_get_Count_m2948975007_gshared (FilterCollectionRX_1_t3654614494 * __this, const MethodInfo* method);
#define FilterCollectionRX_1_get_Count_m2948975007(__this, method) ((  int32_t (*) (FilterCollectionRX_1_t3654614494 *, const MethodInfo*))FilterCollectionRX_1_get_Count_m2948975007_gshared)(__this, method)
// T ZergRush.ReactiveCollectionExtension/FilterCollectionRX`1<System.Object>::ElementAt(System.Int32)
extern "C"  Il2CppObject * FilterCollectionRX_1_ElementAt_m3272936136_gshared (FilterCollectionRX_1_t3654614494 * __this, int32_t ___index0, const MethodInfo* method);
#define FilterCollectionRX_1_ElementAt_m3272936136(__this, ___index0, method) ((  Il2CppObject * (*) (FilterCollectionRX_1_t3654614494 *, int32_t, const MethodInfo*))FilterCollectionRX_1_ElementAt_m3272936136_gshared)(__this, ___index0, method)
