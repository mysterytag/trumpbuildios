﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I1897310919MethodDeclarations.h"

// System.Void UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.Vector4>>::.ctor()
#define ImmutableList_1__ctor_m2645240571(__this, method) ((  void (*) (ImmutableList_1_t2502565896 *, const MethodInfo*))ImmutableList_1__ctor_m2467204245_gshared)(__this, method)
// System.Void UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.Vector4>>::.ctor(T[])
#define ImmutableList_1__ctor_m303275041(__this, ___data0, method) ((  void (*) (ImmutableList_1_t2502565896 *, IObserver_1U5BU5D_t3405924184*, const MethodInfo*))ImmutableList_1__ctor_m707697735_gshared)(__this, ___data0, method)
// System.Void UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.Vector4>>::.cctor()
#define ImmutableList_1__cctor_m4210950162(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ImmutableList_1__cctor_m2986791352_gshared)(__this /* static, unused */, method)
// T[] UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.Vector4>>::get_Data()
#define ImmutableList_1_get_Data_m3454179321(__this, method) ((  IObserver_1U5BU5D_t3405924184* (*) (ImmutableList_1_t2502565896 *, const MethodInfo*))ImmutableList_1_get_Data_m1676800965_gshared)(__this, method)
// UniRx.InternalUtil.ImmutableList`1<T> UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.Vector4>>::Add(T)
#define ImmutableList_1_Add_m781653131(__this, ___value0, method) ((  ImmutableList_1_t2502565896 * (*) (ImmutableList_1_t2502565896 *, Il2CppObject*, const MethodInfo*))ImmutableList_1_Add_m948892931_gshared)(__this, ___value0, method)
// UniRx.InternalUtil.ImmutableList`1<T> UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.Vector4>>::Remove(T)
#define ImmutableList_1_Remove_m1460098762(__this, ___value0, method) ((  ImmutableList_1_t2502565896 * (*) (ImmutableList_1_t2502565896 *, Il2CppObject*, const MethodInfo*))ImmutableList_1_Remove_m1538917202_gshared)(__this, ___value0, method)
// System.Int32 UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.Vector4>>::IndexOf(T)
#define ImmutableList_1_IndexOf_m274327794(__this, ___value0, method) ((  int32_t (*) (ImmutableList_1_t2502565896 *, Il2CppObject*, const MethodInfo*))ImmutableList_1_IndexOf_m3149323628_gshared)(__this, ___value0, method)
