﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.AppLinkResult
struct AppLinkResult_t1305020279;
// Facebook.Unity.ResultContainer
struct ResultContainer_t79372963;
// System.String
struct String_t;
// System.Collections.Generic.IDictionary`2<System.String,System.Object>
struct IDictionary_2_t3650470111;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Facebook_Unity_ResultContainer79372963.h"
#include "mscorlib_System_String968488902.h"

// System.Void Facebook.Unity.AppLinkResult::.ctor(Facebook.Unity.ResultContainer)
extern "C"  void AppLinkResult__ctor_m2318791873 (AppLinkResult_t1305020279 * __this, ResultContainer_t79372963 * ___resultContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.AppLinkResult::get_Url()
extern "C"  String_t* AppLinkResult_get_Url_m131073445 (AppLinkResult_t1305020279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.AppLinkResult::set_Url(System.String)
extern "C"  void AppLinkResult_set_Url_m3798363726 (AppLinkResult_t1305020279 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.AppLinkResult::get_TargetUrl()
extern "C"  String_t* AppLinkResult_get_TargetUrl_m4033293012 (AppLinkResult_t1305020279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.AppLinkResult::set_TargetUrl(System.String)
extern "C"  void AppLinkResult_set_TargetUrl_m280087743 (AppLinkResult_t1305020279 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.AppLinkResult::get_Ref()
extern "C"  String_t* AppLinkResult_get_Ref_m127909833 (AppLinkResult_t1305020279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.AppLinkResult::set_Ref(System.String)
extern "C"  void AppLinkResult_set_Ref_m849694378 (AppLinkResult_t1305020279 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IDictionary`2<System.String,System.Object> Facebook.Unity.AppLinkResult::get_Extras()
extern "C"  Il2CppObject* AppLinkResult_get_Extras_m4138232745 (AppLinkResult_t1305020279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.AppLinkResult::set_Extras(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern "C"  void AppLinkResult_set_Extras_m2658833404 (AppLinkResult_t1305020279 * __this, Il2CppObject* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.AppLinkResult::ToString()
extern "C"  String_t* AppLinkResult_ToString_m3208986383 (AppLinkResult_t1305020279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
