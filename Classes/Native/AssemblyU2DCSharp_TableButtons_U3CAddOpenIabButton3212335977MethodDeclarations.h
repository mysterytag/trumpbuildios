﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TableButtons/<AddOpenIabButton>c__AnonStorey15C
struct U3CAddOpenIabButtonU3Ec__AnonStorey15C_t3212335977;
// DonateCell
struct DonateCell_t2798474385;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_DonateCell2798474385.h"

// System.Void TableButtons/<AddOpenIabButton>c__AnonStorey15C::.ctor()
extern "C"  void U3CAddOpenIabButtonU3Ec__AnonStorey15C__ctor_m3457889798 (U3CAddOpenIabButtonU3Ec__AnonStorey15C_t3212335977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<AddOpenIabButton>c__AnonStorey15C::<>m__23A(DonateCell)
extern "C"  void U3CAddOpenIabButtonU3Ec__AnonStorey15C_U3CU3Em__23A_m1567233954 (U3CAddOpenIabButtonU3Ec__AnonStorey15C_t3212335977 * __this, DonateCell_t2798474385 * ___cell0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
