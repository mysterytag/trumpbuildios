﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.AuthenticationSchemeSelector
struct AuthenticationSchemeSelector_t1750022342;
// System.Object
struct Il2CppObject;
// System.Net.HttpListenerRequest
struct HttpListenerRequest_t404699357;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "System_System_Net_AuthenticationSchemes2907004192.h"
#include "System_System_Net_HttpListenerRequest404699357.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Net.AuthenticationSchemeSelector::.ctor(System.Object,System.IntPtr)
extern "C"  void AuthenticationSchemeSelector__ctor_m2582549380 (AuthenticationSchemeSelector_t1750022342 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.AuthenticationSchemes System.Net.AuthenticationSchemeSelector::Invoke(System.Net.HttpListenerRequest)
extern "C"  int32_t AuthenticationSchemeSelector_Invoke_m1251274626 (AuthenticationSchemeSelector_t1750022342 * __this, HttpListenerRequest_t404699357 * ___httpRequest0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" int32_t pinvoke_delegate_wrapper_AuthenticationSchemeSelector_t1750022342(Il2CppObject* delegate, HttpListenerRequest_t404699357 * ___httpRequest0);
// System.IAsyncResult System.Net.AuthenticationSchemeSelector::BeginInvoke(System.Net.HttpListenerRequest,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * AuthenticationSchemeSelector_BeginInvoke_m4017187316 (AuthenticationSchemeSelector_t1750022342 * __this, HttpListenerRequest_t404699357 * ___httpRequest0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.AuthenticationSchemes System.Net.AuthenticationSchemeSelector::EndInvoke(System.IAsyncResult)
extern "C"  int32_t AuthenticationSchemeSelector_EndInvoke_m3536481403 (AuthenticationSchemeSelector_t1750022342 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
