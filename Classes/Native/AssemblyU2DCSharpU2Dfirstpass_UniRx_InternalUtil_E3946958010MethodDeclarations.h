﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>
struct EmptyObserver_1_t3946958010;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionRemov242637724.h"

// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m1804469883_gshared (EmptyObserver_1_t3946958010 * __this, const MethodInfo* method);
#define EmptyObserver_1__ctor_m1804469883(__this, method) ((  void (*) (EmptyObserver_1_t3946958010 *, const MethodInfo*))EmptyObserver_1__ctor_m1804469883_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m3916862610_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EmptyObserver_1__cctor_m3916862610(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EmptyObserver_1__cctor_m3916862610_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m1241199045_gshared (EmptyObserver_1_t3946958010 * __this, const MethodInfo* method);
#define EmptyObserver_1_OnCompleted_m1241199045(__this, method) ((  void (*) (EmptyObserver_1_t3946958010 *, const MethodInfo*))EmptyObserver_1_OnCompleted_m1241199045_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m3845677938_gshared (EmptyObserver_1_t3946958010 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define EmptyObserver_1_OnError_m3845677938(__this, ___error0, method) ((  void (*) (EmptyObserver_1_t3946958010 *, Exception_t1967233988 *, const MethodInfo*))EmptyObserver_1_OnError_m3845677938_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m1759473251_gshared (EmptyObserver_1_t3946958010 * __this, CollectionRemoveEvent_1_t242637724  ___value0, const MethodInfo* method);
#define EmptyObserver_1_OnNext_m1759473251(__this, ___value0, method) ((  void (*) (EmptyObserver_1_t3946958010 *, CollectionRemoveEvent_1_t242637724 , const MethodInfo*))EmptyObserver_1_OnNext_m1759473251_gshared)(__this, ___value0, method)
