﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex<System.Double,System.Double,System.Double>
struct SelectManyObserverWithIndex_t885021286;
// UniRx.Operators.SelectManyObservable`3<System.Double,System.Double,System.Double>
struct SelectManyObservable_3_t44588665;
// UniRx.IObserver`1<System.Double>
struct IObserver_1_t2746515517;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex<System.Double,System.Double,System.Double>::.ctor(UniRx.Operators.SelectManyObservable`3<TSource,TCollection,TResult>,UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void SelectManyObserverWithIndex__ctor_m256152760_gshared (SelectManyObserverWithIndex_t885021286 * __this, SelectManyObservable_3_t44588665 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define SelectManyObserverWithIndex__ctor_m256152760(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (SelectManyObserverWithIndex_t885021286 *, SelectManyObservable_3_t44588665 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SelectManyObserverWithIndex__ctor_m256152760_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex<System.Double,System.Double,System.Double>::Run()
extern "C"  Il2CppObject * SelectManyObserverWithIndex_Run_m2548119437_gshared (SelectManyObserverWithIndex_t885021286 * __this, const MethodInfo* method);
#define SelectManyObserverWithIndex_Run_m2548119437(__this, method) ((  Il2CppObject * (*) (SelectManyObserverWithIndex_t885021286 *, const MethodInfo*))SelectManyObserverWithIndex_Run_m2548119437_gshared)(__this, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex<System.Double,System.Double,System.Double>::OnNext(TSource)
extern "C"  void SelectManyObserverWithIndex_OnNext_m3787754230_gshared (SelectManyObserverWithIndex_t885021286 * __this, double ___value0, const MethodInfo* method);
#define SelectManyObserverWithIndex_OnNext_m3787754230(__this, ___value0, method) ((  void (*) (SelectManyObserverWithIndex_t885021286 *, double, const MethodInfo*))SelectManyObserverWithIndex_OnNext_m3787754230_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex<System.Double,System.Double,System.Double>::OnError(System.Exception)
extern "C"  void SelectManyObserverWithIndex_OnError_m4120678112_gshared (SelectManyObserverWithIndex_t885021286 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define SelectManyObserverWithIndex_OnError_m4120678112(__this, ___error0, method) ((  void (*) (SelectManyObserverWithIndex_t885021286 *, Exception_t1967233988 *, const MethodInfo*))SelectManyObserverWithIndex_OnError_m4120678112_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SelectManyObservable`3/SelectManyObserverWithIndex<System.Double,System.Double,System.Double>::OnCompleted()
extern "C"  void SelectManyObserverWithIndex_OnCompleted_m3189936691_gshared (SelectManyObserverWithIndex_t885021286 * __this, const MethodInfo* method);
#define SelectManyObserverWithIndex_OnCompleted_m3189936691(__this, method) ((  void (*) (SelectManyObserverWithIndex_t885021286 *, const MethodInfo*))SelectManyObserverWithIndex_OnCompleted_m3189936691_gshared)(__this, method)
