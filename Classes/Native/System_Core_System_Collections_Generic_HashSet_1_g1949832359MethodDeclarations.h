﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Collections_Generic_HashSet_1_g3535795091MethodDeclarations.h"

// System.Void System.Collections.Generic.HashSet`1<System.Threading.Timer>::.ctor()
#define HashSet_1__ctor_m3424742727(__this, method) ((  void (*) (HashSet_1_t1949832359 *, const MethodInfo*))HashSet_1__ctor_m2123963813_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.Threading.Timer>::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
#define HashSet_1__ctor_m1951364955(__this, ___comparer0, method) ((  void (*) (HashSet_1_t1949832359 *, Il2CppObject*, const MethodInfo*))HashSet_1__ctor_m4289509481_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.HashSet`1<System.Threading.Timer>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define HashSet_1__ctor_m1847842588(__this, ___collection0, method) ((  void (*) (HashSet_1_t1949832359 *, Il2CppObject*, const MethodInfo*))HashSet_1__ctor_m345143978_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.HashSet`1<System.Threading.Timer>::.ctor(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
#define HashSet_1__ctor_m3191538388(__this, ___collection0, ___comparer1, method) ((  void (*) (HashSet_1_t1949832359 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))HashSet_1__ctor_m3091556066_gshared)(__this, ___collection0, ___comparer1, method)
// System.Void System.Collections.Generic.HashSet`1<System.Threading.Timer>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define HashSet_1__ctor_m1387132996(__this, ___info0, ___context1, method) ((  void (*) (HashSet_1_t1949832359 *, SerializationInfo_t2995724695 *, StreamingContext_t986364934 , const MethodInfo*))HashSet_1__ctor_m3739212406_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1<System.Threading.Timer>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3299891733(__this, method) ((  Il2CppObject* (*) (HashSet_1_t1949832359 *, const MethodInfo*))HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2351046527_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Threading.Timer>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m545844394(__this, method) ((  bool (*) (HashSet_1_t1949832359 *, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2501283472_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.Threading.Timer>::System.Collections.Generic.ICollection<T>.CopyTo(T[],System.Int32)
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m1289174348(__this, ___array0, ___index1, method) ((  void (*) (HashSet_1_t1949832359 *, TimerU5BU5D_t2766850777*, int32_t, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m1037632730_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.HashSet`1<System.Threading.Timer>::System.Collections.Generic.ICollection<T>.Add(T)
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m4217058396(__this, ___item0, method) ((  void (*) (HashSet_1_t1949832359 *, Timer_t3546110984 *, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m4170802026_gshared)(__this, ___item0, method)
// System.Collections.IEnumerator System.Collections.Generic.HashSet`1<System.Threading.Timer>::System.Collections.IEnumerable.GetEnumerator()
#define HashSet_1_System_Collections_IEnumerable_GetEnumerator_m1648407022(__this, method) ((  Il2CppObject * (*) (HashSet_1_t1949832359 *, const MethodInfo*))HashSet_1_System_Collections_IEnumerable_GetEnumerator_m1202823980_gshared)(__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.Threading.Timer>::get_Count()
#define HashSet_1_get_Count_m1331413465(__this, method) ((  int32_t (*) (HashSet_1_t1949832359 *, const MethodInfo*))HashSet_1_get_Count_m3501347367_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.Threading.Timer>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<T>)
#define HashSet_1_Init_m4069841530(__this, ___capacity0, ___comparer1, method) ((  void (*) (HashSet_1_t1949832359 *, int32_t, Il2CppObject*, const MethodInfo*))HashSet_1_Init_m2856515208_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.HashSet`1<System.Threading.Timer>::InitArrays(System.Int32)
#define HashSet_1_InitArrays_m2072350748(__this, ___size0, method) ((  void (*) (HashSet_1_t1949832359 *, int32_t, const MethodInfo*))HashSet_1_InitArrays_m523929898_gshared)(__this, ___size0, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Threading.Timer>::SlotsContainsAt(System.Int32,System.Int32,T)
#define HashSet_1_SlotsContainsAt_m3168845866(__this, ___index0, ___hash1, ___item2, method) ((  bool (*) (HashSet_1_t1949832359 *, int32_t, int32_t, Timer_t3546110984 *, const MethodInfo*))HashSet_1_SlotsContainsAt_m2666990736_gshared)(__this, ___index0, ___hash1, ___item2, method)
// System.Void System.Collections.Generic.HashSet`1<System.Threading.Timer>::CopyTo(T[],System.Int32)
#define HashSet_1_CopyTo_m118414220(__this, ___array0, ___index1, method) ((  void (*) (HashSet_1_t1949832359 *, TimerU5BU5D_t2766850777*, int32_t, const MethodInfo*))HashSet_1_CopyTo_m2864960666_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.HashSet`1<System.Threading.Timer>::CopyTo(T[],System.Int32,System.Int32)
#define HashSet_1_CopyTo_m2409260235(__this, ___array0, ___index1, ___count2, method) ((  void (*) (HashSet_1_t1949832359 *, TimerU5BU5D_t2766850777*, int32_t, int32_t, const MethodInfo*))HashSet_1_CopyTo_m2745373309_gshared)(__this, ___array0, ___index1, ___count2, method)
// System.Void System.Collections.Generic.HashSet`1<System.Threading.Timer>::Resize()
#define HashSet_1_Resize_m1765062421(__this, method) ((  void (*) (HashSet_1_t1949832359 *, const MethodInfo*))HashSet_1_Resize_m904498211_gshared)(__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.Threading.Timer>::GetLinkHashCode(System.Int32)
#define HashSet_1_GetLinkHashCode_m2861350191(__this, ___index0, method) ((  int32_t (*) (HashSet_1_t1949832359 *, int32_t, const MethodInfo*))HashSet_1_GetLinkHashCode_m1128863229_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.Threading.Timer>::GetItemHashCode(T)
#define HashSet_1_GetItemHashCode_m1460975495(__this, ___item0, method) ((  int32_t (*) (HashSet_1_t1949832359 *, Timer_t3546110984 *, const MethodInfo*))HashSet_1_GetItemHashCode_m416797561_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Threading.Timer>::Add(T)
#define HashSet_1_Add_m3847160347(__this, ___item0, method) ((  bool (*) (HashSet_1_t1949832359 *, Timer_t3546110984 *, const MethodInfo*))HashSet_1_Add_m3867096905_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.HashSet`1<System.Threading.Timer>::Clear()
#define HashSet_1_Clear_m3972930990(__this, method) ((  void (*) (HashSet_1_t1949832359 *, const MethodInfo*))HashSet_1_Clear_m1589866208_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Threading.Timer>::Contains(T)
#define HashSet_1_Contains_m4213817226(__this, ___item0, method) ((  bool (*) (HashSet_1_t1949832359 *, Timer_t3546110984 *, const MethodInfo*))HashSet_1_Contains_m3727111780_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Threading.Timer>::Remove(T)
#define HashSet_1_Remove_m1711069701(__this, ___item0, method) ((  bool (*) (HashSet_1_t1949832359 *, Timer_t3546110984 *, const MethodInfo*))HashSet_1_Remove_m3015589727_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.HashSet`1<System.Threading.Timer>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define HashSet_1_GetObjectData_m2208611233(__this, ___info0, ___context1, method) ((  void (*) (HashSet_1_t1949832359 *, SerializationInfo_t2995724695 *, StreamingContext_t986364934 , const MethodInfo*))HashSet_1_GetObjectData_m2922531795_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.HashSet`1<System.Threading.Timer>::OnDeserialization(System.Object)
#define HashSet_1_OnDeserialization_m1466902883(__this, ___sender0, method) ((  void (*) (HashSet_1_t1949832359 *, Il2CppObject *, const MethodInfo*))HashSet_1_OnDeserialization_m3000579185_gshared)(__this, ___sender0, method)
// System.Collections.Generic.HashSet`1/Enumerator<T> System.Collections.Generic.HashSet`1<System.Threading.Timer>::GetEnumerator()
#define HashSet_1_GetEnumerator_m2996607975(__this, method) ((  Enumerator_t2428852945  (*) (HashSet_1_t1949832359 *, const MethodInfo*))HashSet_1_GetEnumerator_m384339424_gshared)(__this, method)
