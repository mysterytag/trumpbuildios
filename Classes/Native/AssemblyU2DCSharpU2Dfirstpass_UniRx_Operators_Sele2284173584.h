﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Func`2<System.Object,UniRx.IObservable`1<UniRx.Unit>>
struct Func_2_t3615761334;
// System.Func`3<System.Object,System.Int32,UniRx.IObservable`1<UniRx.Unit>>
struct Func_3_t875679264;
// System.Func`2<System.Object,System.Collections.Generic.IEnumerable`1<UniRx.Unit>>
struct Func_2_t2434150030;
// System.Func`3<System.Object,System.Int32,System.Collections.Generic.IEnumerable`1<UniRx.Unit>>
struct Func_3_t3989035256;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1622431009.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.SelectManyObservable`2<System.Object,UniRx.Unit>
struct  SelectManyObservable_2_t2284173584  : public OperatorObservableBase_1_t1622431009
{
public:
	// UniRx.IObservable`1<TSource> UniRx.Operators.SelectManyObservable`2::source
	Il2CppObject* ___source_1;
	// System.Func`2<TSource,UniRx.IObservable`1<TResult>> UniRx.Operators.SelectManyObservable`2::selector
	Func_2_t3615761334 * ___selector_2;
	// System.Func`3<TSource,System.Int32,UniRx.IObservable`1<TResult>> UniRx.Operators.SelectManyObservable`2::selectorWithIndex
	Func_3_t875679264 * ___selectorWithIndex_3;
	// System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>> UniRx.Operators.SelectManyObservable`2::selectorEnumerable
	Func_2_t2434150030 * ___selectorEnumerable_4;
	// System.Func`3<TSource,System.Int32,System.Collections.Generic.IEnumerable`1<TResult>> UniRx.Operators.SelectManyObservable`2::selectorEnumerableWithIndex
	Func_3_t3989035256 * ___selectorEnumerableWithIndex_5;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(SelectManyObservable_2_t2284173584, ___source_1)); }
	inline Il2CppObject* get_source_1() const { return ___source_1; }
	inline Il2CppObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(Il2CppObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier(&___source_1, value);
	}

	inline static int32_t get_offset_of_selector_2() { return static_cast<int32_t>(offsetof(SelectManyObservable_2_t2284173584, ___selector_2)); }
	inline Func_2_t3615761334 * get_selector_2() const { return ___selector_2; }
	inline Func_2_t3615761334 ** get_address_of_selector_2() { return &___selector_2; }
	inline void set_selector_2(Func_2_t3615761334 * value)
	{
		___selector_2 = value;
		Il2CppCodeGenWriteBarrier(&___selector_2, value);
	}

	inline static int32_t get_offset_of_selectorWithIndex_3() { return static_cast<int32_t>(offsetof(SelectManyObservable_2_t2284173584, ___selectorWithIndex_3)); }
	inline Func_3_t875679264 * get_selectorWithIndex_3() const { return ___selectorWithIndex_3; }
	inline Func_3_t875679264 ** get_address_of_selectorWithIndex_3() { return &___selectorWithIndex_3; }
	inline void set_selectorWithIndex_3(Func_3_t875679264 * value)
	{
		___selectorWithIndex_3 = value;
		Il2CppCodeGenWriteBarrier(&___selectorWithIndex_3, value);
	}

	inline static int32_t get_offset_of_selectorEnumerable_4() { return static_cast<int32_t>(offsetof(SelectManyObservable_2_t2284173584, ___selectorEnumerable_4)); }
	inline Func_2_t2434150030 * get_selectorEnumerable_4() const { return ___selectorEnumerable_4; }
	inline Func_2_t2434150030 ** get_address_of_selectorEnumerable_4() { return &___selectorEnumerable_4; }
	inline void set_selectorEnumerable_4(Func_2_t2434150030 * value)
	{
		___selectorEnumerable_4 = value;
		Il2CppCodeGenWriteBarrier(&___selectorEnumerable_4, value);
	}

	inline static int32_t get_offset_of_selectorEnumerableWithIndex_5() { return static_cast<int32_t>(offsetof(SelectManyObservable_2_t2284173584, ___selectorEnumerableWithIndex_5)); }
	inline Func_3_t3989035256 * get_selectorEnumerableWithIndex_5() const { return ___selectorEnumerableWithIndex_5; }
	inline Func_3_t3989035256 ** get_address_of_selectorEnumerableWithIndex_5() { return &___selectorEnumerableWithIndex_5; }
	inline void set_selectorEnumerableWithIndex_5(Func_3_t3989035256 * value)
	{
		___selectorEnumerableWithIndex_5 = value;
		Il2CppCodeGenWriteBarrier(&___selectorEnumerableWithIndex_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
