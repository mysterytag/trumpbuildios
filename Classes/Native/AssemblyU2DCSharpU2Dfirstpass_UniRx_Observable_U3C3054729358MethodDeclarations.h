﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/<EveryEndOfFrameCore>c__Iterator13
struct U3CEveryEndOfFrameCoreU3Ec__Iterator13_t3054729358;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Observable/<EveryEndOfFrameCore>c__Iterator13::.ctor()
extern "C"  void U3CEveryEndOfFrameCoreU3Ec__Iterator13__ctor_m1961632373 (U3CEveryEndOfFrameCoreU3Ec__Iterator13_t3054729358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UniRx.Observable/<EveryEndOfFrameCore>c__Iterator13::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CEveryEndOfFrameCoreU3Ec__Iterator13_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3623432061 (U3CEveryEndOfFrameCoreU3Ec__Iterator13_t3054729358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UniRx.Observable/<EveryEndOfFrameCore>c__Iterator13::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CEveryEndOfFrameCoreU3Ec__Iterator13_System_Collections_IEnumerator_get_Current_m82562321 (U3CEveryEndOfFrameCoreU3Ec__Iterator13_t3054729358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.Observable/<EveryEndOfFrameCore>c__Iterator13::MoveNext()
extern "C"  bool U3CEveryEndOfFrameCoreU3Ec__Iterator13_MoveNext_m1379762551 (U3CEveryEndOfFrameCoreU3Ec__Iterator13_t3054729358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Observable/<EveryEndOfFrameCore>c__Iterator13::Dispose()
extern "C"  void U3CEveryEndOfFrameCoreU3Ec__Iterator13_Dispose_m3831317874 (U3CEveryEndOfFrameCoreU3Ec__Iterator13_t3054729358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Observable/<EveryEndOfFrameCore>c__Iterator13::Reset()
extern "C"  void U3CEveryEndOfFrameCoreU3Ec__Iterator13_Reset_m3903032610 (U3CEveryEndOfFrameCoreU3Ec__Iterator13_t3054729358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
