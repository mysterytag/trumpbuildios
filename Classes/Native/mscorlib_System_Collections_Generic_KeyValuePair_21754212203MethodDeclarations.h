﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23312956448MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Zenject.SingletonId,Zenject.SingletonLazyCreator>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3125627326(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1754212203 *, SingletonId_t1838183899 *, SingletonLazyCreator_t2762284194 *, const MethodInfo*))KeyValuePair_2__ctor_m4168265535_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Zenject.SingletonId,Zenject.SingletonLazyCreator>::get_Key()
#define KeyValuePair_2_get_Key_m930026602(__this, method) ((  SingletonId_t1838183899 * (*) (KeyValuePair_2_t1754212203 *, const MethodInfo*))KeyValuePair_2_get_Key_m3256475977_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Zenject.SingletonId,Zenject.SingletonLazyCreator>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m640577963(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1754212203 *, SingletonId_t1838183899 *, const MethodInfo*))KeyValuePair_2_set_Key_m1278074762_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<Zenject.SingletonId,Zenject.SingletonLazyCreator>::get_Value()
#define KeyValuePair_2_get_Value_m3226219946(__this, method) ((  SingletonLazyCreator_t2762284194 * (*) (KeyValuePair_2_t1754212203 *, const MethodInfo*))KeyValuePair_2_get_Value_m3899079597_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Zenject.SingletonId,Zenject.SingletonLazyCreator>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2505242667(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1754212203 *, SingletonLazyCreator_t2762284194 *, const MethodInfo*))KeyValuePair_2_set_Value_m2954518154_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<Zenject.SingletonId,Zenject.SingletonLazyCreator>::ToString()
#define KeyValuePair_2_ToString_m3862491863(__this, method) ((  String_t* (*) (KeyValuePair_2_t1754212203 *, const MethodInfo*))KeyValuePair_2_ToString_m1313859518_gshared)(__this, method)
