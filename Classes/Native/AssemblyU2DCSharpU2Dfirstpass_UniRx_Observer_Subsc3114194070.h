﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.Exception>
struct Action_1_t2115686693;
// System.Action
struct Action_t437523947;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observer/Subscribe_`1<UniRx.Tuple`2<System.Object,System.Object>>
struct  Subscribe__1_t3114194070  : public Il2CppObject
{
public:
	// System.Action`1<System.Exception> UniRx.Observer/Subscribe_`1::onError
	Action_1_t2115686693 * ___onError_0;
	// System.Action UniRx.Observer/Subscribe_`1::onCompleted
	Action_t437523947 * ___onCompleted_1;
	// System.Int32 UniRx.Observer/Subscribe_`1::isStopped
	int32_t ___isStopped_2;

public:
	inline static int32_t get_offset_of_onError_0() { return static_cast<int32_t>(offsetof(Subscribe__1_t3114194070, ___onError_0)); }
	inline Action_1_t2115686693 * get_onError_0() const { return ___onError_0; }
	inline Action_1_t2115686693 ** get_address_of_onError_0() { return &___onError_0; }
	inline void set_onError_0(Action_1_t2115686693 * value)
	{
		___onError_0 = value;
		Il2CppCodeGenWriteBarrier(&___onError_0, value);
	}

	inline static int32_t get_offset_of_onCompleted_1() { return static_cast<int32_t>(offsetof(Subscribe__1_t3114194070, ___onCompleted_1)); }
	inline Action_t437523947 * get_onCompleted_1() const { return ___onCompleted_1; }
	inline Action_t437523947 ** get_address_of_onCompleted_1() { return &___onCompleted_1; }
	inline void set_onCompleted_1(Action_t437523947 * value)
	{
		___onCompleted_1 = value;
		Il2CppCodeGenWriteBarrier(&___onCompleted_1, value);
	}

	inline static int32_t get_offset_of_isStopped_2() { return static_cast<int32_t>(offsetof(Subscribe__1_t3114194070, ___isStopped_2)); }
	inline int32_t get_isStopped_2() const { return ___isStopped_2; }
	inline int32_t* get_address_of_isStopped_2() { return &___isStopped_2; }
	inline void set_isStopped_2(int32_t value)
	{
		___isStopped_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
