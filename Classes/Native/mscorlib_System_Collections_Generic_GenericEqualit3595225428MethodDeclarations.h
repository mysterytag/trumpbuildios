﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.GenericEqualityComparer`1<UniRx.Tuple`2<System.Object,System.Object>>
struct GenericEqualityComparer_1_t3595225428;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen369261819.h"

// System.Void System.Collections.Generic.GenericEqualityComparer`1<UniRx.Tuple`2<System.Object,System.Object>>::.ctor()
extern "C"  void GenericEqualityComparer_1__ctor_m3696094968_gshared (GenericEqualityComparer_1_t3595225428 * __this, const MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m3696094968(__this, method) ((  void (*) (GenericEqualityComparer_1_t3595225428 *, const MethodInfo*))GenericEqualityComparer_1__ctor_m3696094968_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<UniRx.Tuple`2<System.Object,System.Object>>::GetHashCode(T)
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m3165197491_gshared (GenericEqualityComparer_1_t3595225428 * __this, Tuple_2_t369261819  ___obj0, const MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m3165197491(__this, ___obj0, method) ((  int32_t (*) (GenericEqualityComparer_1_t3595225428 *, Tuple_2_t369261819 , const MethodInfo*))GenericEqualityComparer_1_GetHashCode_m3165197491_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<UniRx.Tuple`2<System.Object,System.Object>>::Equals(T,T)
extern "C"  bool GenericEqualityComparer_1_Equals_m4174967625_gshared (GenericEqualityComparer_1_t3595225428 * __this, Tuple_2_t369261819  ___x0, Tuple_2_t369261819  ___y1, const MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m4174967625(__this, ___x0, ___y1, method) ((  bool (*) (GenericEqualityComparer_1_t3595225428 *, Tuple_2_t369261819 , Tuple_2_t369261819 , const MethodInfo*))GenericEqualityComparer_1_Equals_m4174967625_gshared)(__this, ___x0, ___y1, method)
