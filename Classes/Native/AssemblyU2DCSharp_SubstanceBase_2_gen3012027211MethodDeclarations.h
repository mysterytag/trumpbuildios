﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_gen1748143363MethodDeclarations.h"

// System.Void SubstanceBase`2<System.Single,System.Func`1<System.Single>>::.ctor()
#define SubstanceBase_2__ctor_m2557845914(__this, method) ((  void (*) (SubstanceBase_2_t3012027211 *, const MethodInfo*))SubstanceBase_2__ctor_m190675853_gshared)(__this, method)
// System.Void SubstanceBase`2<System.Single,System.Func`1<System.Single>>::.ctor(T)
#define SubstanceBase_2__ctor_m1983813380(__this, ___baseVal0, method) ((  void (*) (SubstanceBase_2_t3012027211 *, float, const MethodInfo*))SubstanceBase_2__ctor_m1615985521_gshared)(__this, ___baseVal0, method)
// T SubstanceBase`2<System.Single,System.Func`1<System.Single>>::get_baseValue()
#define SubstanceBase_2_get_baseValue_m3994099342(__this, method) ((  float (*) (SubstanceBase_2_t3012027211 *, const MethodInfo*))SubstanceBase_2_get_baseValue_m2947284323_gshared)(__this, method)
// System.Void SubstanceBase`2<System.Single,System.Func`1<System.Single>>::set_baseValue(T)
#define SubstanceBase_2_set_baseValue_m1328344099(__this, ___value0, method) ((  void (*) (SubstanceBase_2_t3012027211 *, float, const MethodInfo*))SubstanceBase_2_set_baseValue_m1388286864_gshared)(__this, ___value0, method)
// System.Void SubstanceBase`2<System.Single,System.Func`1<System.Single>>::set_baseValueReactive(ICell`1<T>)
#define SubstanceBase_2_set_baseValueReactive_m3458694954(__this, ___value0, method) ((  void (*) (SubstanceBase_2_t3012027211 *, Il2CppObject*, const MethodInfo*))SubstanceBase_2_set_baseValueReactive_m4252303197_gshared)(__this, ___value0, method)
// System.IDisposable SubstanceBase`2<System.Single,System.Func`1<System.Single>>::Bind(System.Action`1<T>,Priority)
#define SubstanceBase_2_Bind_m2152109006(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (SubstanceBase_2_t3012027211 *, Action_1_t1106661726 *, int32_t, const MethodInfo*))SubstanceBase_2_Bind_m1838262843_gshared)(__this, ___action0, ___p1, method)
// System.IDisposable SubstanceBase`2<System.Single,System.Func`1<System.Single>>::OnChanged(System.Action,Priority)
#define SubstanceBase_2_OnChanged_m2721011333(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (SubstanceBase_2_t3012027211 *, Action_t437523947 *, int32_t, const MethodInfo*))SubstanceBase_2_OnChanged_m2407165170_gshared)(__this, ___action0, ___p1, method)
// System.Object SubstanceBase`2<System.Single,System.Func`1<System.Single>>::get_valueObject()
#define SubstanceBase_2_get_valueObject_m4219189492(__this, method) ((  Il2CppObject * (*) (SubstanceBase_2_t3012027211 *, const MethodInfo*))SubstanceBase_2_get_valueObject_m820874205_gshared)(__this, method)
// System.IDisposable SubstanceBase`2<System.Single,System.Func`1<System.Single>>::ListenUpdates(System.Action`1<T>,Priority)
#define SubstanceBase_2_ListenUpdates_m1059354612(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (SubstanceBase_2_t3012027211 *, Action_1_t1106661726 *, int32_t, const MethodInfo*))SubstanceBase_2_ListenUpdates_m1765231655_gshared)(__this, ___action0, ___p1, method)
// IStream`1<T> SubstanceBase`2<System.Single,System.Func`1<System.Single>>::get_updates()
#define SubstanceBase_2_get_updates_m1560230410(__this, method) ((  Il2CppObject* (*) (SubstanceBase_2_t3012027211 *, const MethodInfo*))SubstanceBase_2_get_updates_m1848791611_gshared)(__this, method)
// T SubstanceBase`2<System.Single,System.Func`1<System.Single>>::get_value()
#define SubstanceBase_2_get_value_m3982289183(__this, method) ((  float (*) (SubstanceBase_2_t3012027211 *, const MethodInfo*))SubstanceBase_2_get_value_m91912820_gshared)(__this, method)
// SubstanceBase`2/Intrusion<T,InfT> SubstanceBase`2<System.Single,System.Func`1<System.Single>>::AffectInner(ILiving,InfT)
#define SubstanceBase_2_AffectInner_m3170973565(__this, ___intruder0, ___influence1, method) ((  Intrusion_t1221677671 * (*) (SubstanceBase_2_t3012027211 *, Il2CppObject *, Func_1_t2100990268 *, const MethodInfo*))SubstanceBase_2_AffectInner_m123032174_gshared)(__this, ___intruder0, ___influence1, method)
// System.IDisposable SubstanceBase`2<System.Single,System.Func`1<System.Single>>::AffectUnsafe(InfT)
#define SubstanceBase_2_AffectUnsafe_m448396991(__this, ___influence0, method) ((  Il2CppObject * (*) (SubstanceBase_2_t3012027211 *, Func_1_t2100990268 *, const MethodInfo*))SubstanceBase_2_AffectUnsafe_m3348397042_gshared)(__this, ___influence0, method)
// System.IDisposable SubstanceBase`2<System.Single,System.Func`1<System.Single>>::AffectUnsafe(ICell`1<InfT>)
#define SubstanceBase_2_AffectUnsafe_m938304053(__this, ___influence0, method) ((  Il2CppObject * (*) (SubstanceBase_2_t3012027211 *, Il2CppObject*, const MethodInfo*))SubstanceBase_2_AffectUnsafe_m3878751842_gshared)(__this, ___influence0, method)
// System.IDisposable SubstanceBase`2<System.Single,System.Func`1<System.Single>>::Affect(ILiving,InfT)
#define SubstanceBase_2_Affect_m1617465567(__this, ___intruder0, ___influence1, method) ((  Il2CppObject * (*) (SubstanceBase_2_t3012027211 *, Il2CppObject *, Func_1_t2100990268 *, const MethodInfo*))SubstanceBase_2_Affect_m1083739474_gshared)(__this, ___intruder0, ___influence1, method)
// System.IDisposable SubstanceBase`2<System.Single,System.Func`1<System.Single>>::Affect(ILiving,ICell`1<InfT>)
#define SubstanceBase_2_Affect_m3786486805(__this, ___intruder0, ___influence1, method) ((  Il2CppObject * (*) (SubstanceBase_2_t3012027211 *, Il2CppObject *, Il2CppObject*, const MethodInfo*))SubstanceBase_2_Affect_m3468331266_gshared)(__this, ___intruder0, ___influence1, method)
// System.IDisposable SubstanceBase`2<System.Single,System.Func`1<System.Single>>::Affect(ILiving,InfT,IEmptyStream)
#define SubstanceBase_2_Affect_m1191915491(__this, ___intruder0, ___influence1, ___update2, method) ((  Il2CppObject * (*) (SubstanceBase_2_t3012027211 *, Il2CppObject *, Func_1_t2100990268 *, Il2CppObject *, const MethodInfo*))SubstanceBase_2_Affect_m878069328_gshared)(__this, ___intruder0, ___influence1, ___update2, method)
// System.Void SubstanceBase`2<System.Single,System.Func`1<System.Single>>::OnAdd(SubstanceBase`2/Intrusion<T,InfT>)
#define SubstanceBase_2_OnAdd_m2525008278(__this, ___intrusion0, method) ((  void (*) (SubstanceBase_2_t3012027211 *, Intrusion_t1221677671 *, const MethodInfo*))SubstanceBase_2_OnAdd_m1323237891_gshared)(__this, ___intrusion0, method)
// System.Void SubstanceBase`2<System.Single,System.Func`1<System.Single>>::OnRemove(SubstanceBase`2/Intrusion<T,InfT>)
#define SubstanceBase_2_OnRemove_m2762370563(__this, ___intrusion0, method) ((  void (*) (SubstanceBase_2_t3012027211 *, Intrusion_t1221677671 *, const MethodInfo*))SubstanceBase_2_OnRemove_m3668150902_gshared)(__this, ___intrusion0, method)
// System.Void SubstanceBase`2<System.Single,System.Func`1<System.Single>>::OnUpdate(SubstanceBase`2/Intrusion<T,InfT>)
#define SubstanceBase_2_OnUpdate_m2113101726(__this, ___intrusion0, method) ((  void (*) (SubstanceBase_2_t3012027211 *, Intrusion_t1221677671 *, const MethodInfo*))SubstanceBase_2_OnUpdate_m3018882065_gshared)(__this, ___intrusion0, method)
// System.Void SubstanceBase`2<System.Single,System.Func`1<System.Single>>::UpdateAll()
#define SubstanceBase_2_UpdateAll_m849007952(__this, method) ((  void (*) (SubstanceBase_2_t3012027211 *, const MethodInfo*))SubstanceBase_2_UpdateAll_m3645799875_gshared)(__this, method)
// System.Void SubstanceBase`2<System.Single,System.Func`1<System.Single>>::ClearIntrusion(SubstanceBase`2/Intrusion<T,InfT>)
#define SubstanceBase_2_ClearIntrusion_m71007308(__this, ___intr0, method) ((  void (*) (SubstanceBase_2_t3012027211 *, Intrusion_t1221677671 *, const MethodInfo*))SubstanceBase_2_ClearIntrusion_m4200609407_gshared)(__this, ___intr0, method)
// System.Void SubstanceBase`2<System.Single,System.Func`1<System.Single>>::<set_baseValueReactive>m__1D7(T)
#define SubstanceBase_2_U3Cset_baseValueReactiveU3Em__1D7_m2267624273(__this, ___val0, method) ((  void (*) (SubstanceBase_2_t3012027211 *, float, const MethodInfo*))SubstanceBase_2_U3Cset_baseValueReactiveU3Em__1D7_m1323393214_gshared)(__this, ___val0, method)
