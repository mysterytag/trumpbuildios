﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Cell`1<System.Int32>
struct Cell_1_t3029512419;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tests/<TestReactive>c__AnonStorey147
struct  U3CTestReactiveU3Ec__AnonStorey147_t1969170084  : public Il2CppObject
{
public:
	// Cell`1<System.Int32> Tests/<TestReactive>c__AnonStorey147::c2
	Cell_1_t3029512419 * ___c2_0;

public:
	inline static int32_t get_offset_of_c2_0() { return static_cast<int32_t>(offsetof(U3CTestReactiveU3Ec__AnonStorey147_t1969170084, ___c2_0)); }
	inline Cell_1_t3029512419 * get_c2_0() const { return ___c2_0; }
	inline Cell_1_t3029512419 ** get_address_of_c2_0() { return &___c2_0; }
	inline void set_c2_0(Cell_1_t3029512419 * value)
	{
		___c2_0 = value;
		Il2CppCodeGenWriteBarrier(&___c2_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
