﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct Empty_t3907495220;
// UniRx.IObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct IObserver_1_t4160676289;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionAddE1948677386.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Empty__ctor_m2885220451_gshared (Empty_t3907495220 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define Empty__ctor_m2885220451(__this, ___observer0, ___cancel1, method) ((  void (*) (Empty_t3907495220 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Empty__ctor_m2885220451_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnNext(T)
extern "C"  void Empty_OnNext_m630487375_gshared (Empty_t3907495220 * __this, CollectionAddEvent_1_t1948677386  ___value0, const MethodInfo* method);
#define Empty_OnNext_m630487375(__this, ___value0, method) ((  void (*) (Empty_t3907495220 *, CollectionAddEvent_1_t1948677386 , const MethodInfo*))Empty_OnNext_m630487375_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnError(System.Exception)
extern "C"  void Empty_OnError_m2536642142_gshared (Empty_t3907495220 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Empty_OnError_m2536642142(__this, ___error0, method) ((  void (*) (Empty_t3907495220 *, Exception_t1967233988 *, const MethodInfo*))Empty_OnError_m2536642142_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnCompleted()
extern "C"  void Empty_OnCompleted_m1041819313_gshared (Empty_t3907495220 * __this, const MethodInfo* method);
#define Empty_OnCompleted_m1041819313(__this, method) ((  void (*) (Empty_t3907495220 *, const MethodInfo*))Empty_OnCompleted_m1041819313_gshared)(__this, method)
