﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen1935366568.h"
#include "DOTween_DG_Tweening_LoopType3344295956.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Nullable`1<DG.Tweening.LoopType>::.ctor(T)
extern "C"  void Nullable_1__ctor_m4064011763_gshared (Nullable_1_t1935366568 * __this, int32_t ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m4064011763(__this, ___value0, method) ((  void (*) (Nullable_1_t1935366568 *, int32_t, const MethodInfo*))Nullable_1__ctor_m4064011763_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<DG.Tweening.LoopType>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m1727196284_gshared (Nullable_1_t1935366568 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m1727196284(__this, method) ((  bool (*) (Nullable_1_t1935366568 *, const MethodInfo*))Nullable_1_get_HasValue_m1727196284_gshared)(__this, method)
// T System.Nullable`1<DG.Tweening.LoopType>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m3156116456_gshared (Nullable_1_t1935366568 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m3156116456(__this, method) ((  int32_t (*) (Nullable_1_t1935366568 *, const MethodInfo*))Nullable_1_get_Value_m3156116456_gshared)(__this, method)
// System.Boolean System.Nullable`1<DG.Tweening.LoopType>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3402554110_gshared (Nullable_1_t1935366568 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m3402554110(__this, ___other0, method) ((  bool (*) (Nullable_1_t1935366568 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m3402554110_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<DG.Tweening.LoopType>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m915668609_gshared (Nullable_1_t1935366568 * __this, Nullable_1_t1935366568  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m915668609(__this, ___other0, method) ((  bool (*) (Nullable_1_t1935366568 *, Nullable_1_t1935366568 , const MethodInfo*))Nullable_1_Equals_m915668609_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<DG.Tweening.LoopType>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1359939670_gshared (Nullable_1_t1935366568 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m1359939670(__this, method) ((  int32_t (*) (Nullable_1_t1935366568 *, const MethodInfo*))Nullable_1_GetHashCode_m1359939670_gshared)(__this, method)
// T System.Nullable`1<DG.Tweening.LoopType>::GetValueOrDefault()
extern "C"  int32_t Nullable_1_GetValueOrDefault_m2011806923_gshared (Nullable_1_t1935366568 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m2011806923(__this, method) ((  int32_t (*) (Nullable_1_t1935366568 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m2011806923_gshared)(__this, method)
// System.String System.Nullable`1<DG.Tweening.LoopType>::ToString()
extern "C"  String_t* Nullable_1_ToString_m3623309762_gshared (Nullable_1_t1935366568 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m3623309762(__this, method) ((  String_t* (*) (Nullable_1_t1935366568 *, const MethodInfo*))Nullable_1_ToString_m3623309762_gshared)(__this, method)
// System.Nullable`1<T> System.Nullable`1<DG.Tweening.LoopType>::op_Implicit(T)
extern "C"  Nullable_1_t1935366568  Nullable_1_op_Implicit_m3257158460_gshared (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method);
#define Nullable_1_op_Implicit_m3257158460(__this /* static, unused */, ___value0, method) ((  Nullable_1_t1935366568  (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))Nullable_1_op_Implicit_m3257158460_gshared)(__this /* static, unused */, ___value0, method)
