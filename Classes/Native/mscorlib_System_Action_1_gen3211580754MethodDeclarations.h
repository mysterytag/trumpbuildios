﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Action_1_gen985559125MethodDeclarations.h"

// System.Void System.Action`1<PlayFab.ClientModels.GameCenterPlayFabIdPair>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m1186574995(__this, ___object0, ___method1, method) ((  void (*) (Action_1_t3211580754 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_1__ctor_m1498188969_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`1<PlayFab.ClientModels.GameCenterPlayFabIdPair>::Invoke(T)
#define Action_1_Invoke_m3204917841(__this, ___obj0, method) ((  void (*) (Action_1_t3211580754 *, GameCenterPlayFabIdPair_t3063128049 *, const MethodInfo*))Action_1_Invoke_m2789055860_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Action`1<PlayFab.ClientModels.GameCenterPlayFabIdPair>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m3899634342(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Action_1_t3211580754 *, GameCenterPlayFabIdPair_t3063128049 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Action_1_BeginInvoke_m917692971_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Void System.Action`1<PlayFab.ClientModels.GameCenterPlayFabIdPair>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m2065393187(__this, ___result0, method) ((  void (*) (Action_1_t3211580754 *, Il2CppObject *, const MethodInfo*))Action_1_EndInvoke_m3562128182_gshared)(__this, ___result0, method)
