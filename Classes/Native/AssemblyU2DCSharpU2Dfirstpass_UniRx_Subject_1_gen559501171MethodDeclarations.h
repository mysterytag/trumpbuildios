﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1<UniRx.CollectionMoveEvent`1<System.Object>>
struct Subject_1_t559501171;
// System.Exception
struct Exception_t1967233988;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>
struct IObserver_1_t833465454;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionMove2916433847.h"

// System.Void UniRx.Subject`1<UniRx.CollectionMoveEvent`1<System.Object>>::.ctor()
extern "C"  void Subject_1__ctor_m1130900552_gshared (Subject_1_t559501171 * __this, const MethodInfo* method);
#define Subject_1__ctor_m1130900552(__this, method) ((  void (*) (Subject_1_t559501171 *, const MethodInfo*))Subject_1__ctor_m1130900552_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.CollectionMoveEvent`1<System.Object>>::get_HasObservers()
extern "C"  bool Subject_1_get_HasObservers_m2663333004_gshared (Subject_1_t559501171 * __this, const MethodInfo* method);
#define Subject_1_get_HasObservers_m2663333004(__this, method) ((  bool (*) (Subject_1_t559501171 *, const MethodInfo*))Subject_1_get_HasObservers_m2663333004_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.CollectionMoveEvent`1<System.Object>>::OnCompleted()
extern "C"  void Subject_1_OnCompleted_m4181920210_gshared (Subject_1_t559501171 * __this, const MethodInfo* method);
#define Subject_1_OnCompleted_m4181920210(__this, method) ((  void (*) (Subject_1_t559501171 *, const MethodInfo*))Subject_1_OnCompleted_m4181920210_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.CollectionMoveEvent`1<System.Object>>::OnError(System.Exception)
extern "C"  void Subject_1_OnError_m2351140863_gshared (Subject_1_t559501171 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Subject_1_OnError_m2351140863(__this, ___error0, method) ((  void (*) (Subject_1_t559501171 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m2351140863_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<UniRx.CollectionMoveEvent`1<System.Object>>::OnNext(T)
extern "C"  void Subject_1_OnNext_m2999407856_gshared (Subject_1_t559501171 * __this, CollectionMoveEvent_1_t2916433847  ___value0, const MethodInfo* method);
#define Subject_1_OnNext_m2999407856(__this, ___value0, method) ((  void (*) (Subject_1_t559501171 *, CollectionMoveEvent_1_t2916433847 , const MethodInfo*))Subject_1_OnNext_m2999407856_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<UniRx.CollectionMoveEvent`1<System.Object>>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * Subject_1_Subscribe_m946020765_gshared (Subject_1_t559501171 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define Subject_1_Subscribe_m946020765(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t559501171 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m946020765_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<UniRx.CollectionMoveEvent`1<System.Object>>::Dispose()
extern "C"  void Subject_1_Dispose_m66987653_gshared (Subject_1_t559501171 * __this, const MethodInfo* method);
#define Subject_1_Dispose_m66987653(__this, method) ((  void (*) (Subject_1_t559501171 *, const MethodInfo*))Subject_1_Dispose_m66987653_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.CollectionMoveEvent`1<System.Object>>::ThrowIfDisposed()
extern "C"  void Subject_1_ThrowIfDisposed_m1696824078_gshared (Subject_1_t559501171 * __this, const MethodInfo* method);
#define Subject_1_ThrowIfDisposed_m1696824078(__this, method) ((  void (*) (Subject_1_t559501171 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m1696824078_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.CollectionMoveEvent`1<System.Object>>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool Subject_1_IsRequiredSubscribeOnCurrentThread_m1241652867_gshared (Subject_1_t559501171 * __this, const MethodInfo* method);
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m1241652867(__this, method) ((  bool (*) (Subject_1_t559501171 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m1241652867_gshared)(__this, method)
