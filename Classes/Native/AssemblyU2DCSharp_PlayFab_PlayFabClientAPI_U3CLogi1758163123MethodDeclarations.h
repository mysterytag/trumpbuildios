﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<LoginWithIOSDeviceID>c__AnonStorey65
struct U3CLoginWithIOSDeviceIDU3Ec__AnonStorey65_t1758163123;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<LoginWithIOSDeviceID>c__AnonStorey65::.ctor()
extern "C"  void U3CLoginWithIOSDeviceIDU3Ec__AnonStorey65__ctor_m1770904672 (U3CLoginWithIOSDeviceIDU3Ec__AnonStorey65_t1758163123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<LoginWithIOSDeviceID>c__AnonStorey65::<>m__52(PlayFab.CallRequestContainer)
extern "C"  void U3CLoginWithIOSDeviceIDU3Ec__AnonStorey65_U3CU3Em__52_m887052571 (U3CLoginWithIOSDeviceIDU3Ec__AnonStorey65_t1758163123 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
