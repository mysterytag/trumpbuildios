﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1/Subscription<System.Boolean>
struct Subscription_t2285219159;
// UniRx.Subject`1<System.Boolean>
struct Subject_1_t2149039961;
// UniRx.IObserver`1<System.Boolean>
struct IObserver_1_t2423004244;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Subject`1/Subscription<System.Boolean>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern "C"  void Subscription__ctor_m1067686410_gshared (Subscription_t2285219159 * __this, Subject_1_t2149039961 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method);
#define Subscription__ctor_m1067686410(__this, ___parent0, ___unsubscribeTarget1, method) ((  void (*) (Subscription_t2285219159 *, Subject_1_t2149039961 *, Il2CppObject*, const MethodInfo*))Subscription__ctor_m1067686410_gshared)(__this, ___parent0, ___unsubscribeTarget1, method)
// System.Void UniRx.Subject`1/Subscription<System.Boolean>::Dispose()
extern "C"  void Subscription_Dispose_m3196332_gshared (Subscription_t2285219159 * __this, const MethodInfo* method);
#define Subscription_Dispose_m3196332(__this, method) ((  void (*) (Subscription_t2285219159 *, const MethodInfo*))Subscription_Dispose_m3196332_gshared)(__this, method)
