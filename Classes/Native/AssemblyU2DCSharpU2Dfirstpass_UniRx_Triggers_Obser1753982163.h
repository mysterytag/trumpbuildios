﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Subject`1<UniRx.Unit>
struct Subject_1_t201353362;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Triggers_Obser1883237895.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableEnableTrigger
struct  ObservableEnableTrigger_t1753982163  : public ObservableTriggerBase_t1883237895
{
public:
	// UniRx.Subject`1<UniRx.Unit> UniRx.Triggers.ObservableEnableTrigger::onEnable
	Subject_1_t201353362 * ___onEnable_8;
	// UniRx.Subject`1<UniRx.Unit> UniRx.Triggers.ObservableEnableTrigger::onDisable
	Subject_1_t201353362 * ___onDisable_9;

public:
	inline static int32_t get_offset_of_onEnable_8() { return static_cast<int32_t>(offsetof(ObservableEnableTrigger_t1753982163, ___onEnable_8)); }
	inline Subject_1_t201353362 * get_onEnable_8() const { return ___onEnable_8; }
	inline Subject_1_t201353362 ** get_address_of_onEnable_8() { return &___onEnable_8; }
	inline void set_onEnable_8(Subject_1_t201353362 * value)
	{
		___onEnable_8 = value;
		Il2CppCodeGenWriteBarrier(&___onEnable_8, value);
	}

	inline static int32_t get_offset_of_onDisable_9() { return static_cast<int32_t>(offsetof(ObservableEnableTrigger_t1753982163, ___onDisable_9)); }
	inline Subject_1_t201353362 * get_onDisable_9() const { return ___onDisable_9; }
	inline Subject_1_t201353362 ** get_address_of_onDisable_9() { return &___onDisable_9; }
	inline void set_onDisable_9(Subject_1_t201353362 * value)
	{
		___onDisable_9 = value;
		Il2CppCodeGenWriteBarrier(&___onDisable_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
