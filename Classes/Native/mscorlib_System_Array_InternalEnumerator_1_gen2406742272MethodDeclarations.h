﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen744596923MethodDeclarations.h"

// System.Void System.Array/InternalEnumerator`1<ZergRush.ImmutableList`1<System.Action`1<System.Int64>>>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m3249697578(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2406742272 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2616641763_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<ZergRush.ImmutableList`1<System.Action`1<System.Int64>>>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2369130038(__this, method) ((  void (*) (InternalEnumerator_1_t2406742272 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2224260061_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<ZergRush.ImmutableList`1<System.Action`1<System.Int64>>>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m541181740(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2406742272 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m390763987_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<ZergRush.ImmutableList`1<System.Action`1<System.Int64>>>::Dispose()
#define InternalEnumerator_1_Dispose_m3867412865(__this, method) ((  void (*) (InternalEnumerator_1_t2406742272 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2760671866_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<ZergRush.ImmutableList`1<System.Action`1<System.Int64>>>::MoveNext()
#define InternalEnumerator_1_MoveNext_m3692707302(__this, method) ((  bool (*) (InternalEnumerator_1_t2406742272 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3716548237_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<ZergRush.ImmutableList`1<System.Action`1<System.Int64>>>::get_Current()
#define InternalEnumerator_1_get_Current_m2717059155(__this, method) ((  ImmutableList_1_t2499251769 * (*) (InternalEnumerator_1_t2406742272 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2178852364_gshared)(__this, method)
