﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnonymousCell`1/<Listen>c__AnonStoreyF9<System.Boolean>
struct U3CListenU3Ec__AnonStoreyF9_t1928065897;

#include "codegen/il2cpp-codegen.h"

// System.Void AnonymousCell`1/<Listen>c__AnonStoreyF9<System.Boolean>::.ctor()
extern "C"  void U3CListenU3Ec__AnonStoreyF9__ctor_m2910158178_gshared (U3CListenU3Ec__AnonStoreyF9_t1928065897 * __this, const MethodInfo* method);
#define U3CListenU3Ec__AnonStoreyF9__ctor_m2910158178(__this, method) ((  void (*) (U3CListenU3Ec__AnonStoreyF9_t1928065897 *, const MethodInfo*))U3CListenU3Ec__AnonStoreyF9__ctor_m2910158178_gshared)(__this, method)
// System.Void AnonymousCell`1/<Listen>c__AnonStoreyF9<System.Boolean>::<>m__16F(T)
extern "C"  void U3CListenU3Ec__AnonStoreyF9_U3CU3Em__16F_m1055092294_gshared (U3CListenU3Ec__AnonStoreyF9_t1928065897 * __this, bool ____0, const MethodInfo* method);
#define U3CListenU3Ec__AnonStoreyF9_U3CU3Em__16F_m1055092294(__this, ____0, method) ((  void (*) (U3CListenU3Ec__AnonStoreyF9_t1928065897 *, bool, const MethodInfo*))U3CListenU3Ec__AnonStoreyF9_U3CU3Em__16F_m1055092294_gshared)(__this, ____0, method)
