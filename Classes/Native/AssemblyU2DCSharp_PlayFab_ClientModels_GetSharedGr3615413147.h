﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.SharedGroupDataRecord>
struct Dictionary_2_t444340917;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;

#include "AssemblyU2DCSharp_PlayFab_Internal_PlayFabResultCom379512675.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.GetSharedGroupDataResult
struct  GetSharedGroupDataResult_t3615413147  : public PlayFabResultCommon_t379512675
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.SharedGroupDataRecord> PlayFab.ClientModels.GetSharedGroupDataResult::<Data>k__BackingField
	Dictionary_2_t444340917 * ___U3CDataU3Ek__BackingField_2;
	// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.GetSharedGroupDataResult::<Members>k__BackingField
	List_1_t1765447871 * ___U3CMembersU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GetSharedGroupDataResult_t3615413147, ___U3CDataU3Ek__BackingField_2)); }
	inline Dictionary_2_t444340917 * get_U3CDataU3Ek__BackingField_2() const { return ___U3CDataU3Ek__BackingField_2; }
	inline Dictionary_2_t444340917 ** get_address_of_U3CDataU3Ek__BackingField_2() { return &___U3CDataU3Ek__BackingField_2; }
	inline void set_U3CDataU3Ek__BackingField_2(Dictionary_2_t444340917 * value)
	{
		___U3CDataU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CDataU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CMembersU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(GetSharedGroupDataResult_t3615413147, ___U3CMembersU3Ek__BackingField_3)); }
	inline List_1_t1765447871 * get_U3CMembersU3Ek__BackingField_3() const { return ___U3CMembersU3Ek__BackingField_3; }
	inline List_1_t1765447871 ** get_address_of_U3CMembersU3Ek__BackingField_3() { return &___U3CMembersU3Ek__BackingField_3; }
	inline void set_U3CMembersU3Ek__BackingField_3(List_1_t1765447871 * value)
	{
		___U3CMembersU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CMembersU3Ek__BackingField_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
