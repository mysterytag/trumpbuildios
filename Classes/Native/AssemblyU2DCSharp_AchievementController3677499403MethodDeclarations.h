﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AchievementController
struct AchievementController_t3677499403;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Action
struct Action_t437523947;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "System_Core_System_Action437523947.h"

// System.Void AchievementController::.ctor()
extern "C"  void AchievementController__ctor_m617784768 (AchievementController_t3677499403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AchievementController::Awake()
extern "C"  void AchievementController_Awake_m855389987 (AchievementController_t3677499403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AchievementController::PostInject()
extern "C"  void AchievementController_PostInject_m530122581 (AchievementController_t3677499403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AchievementController::get_savePath()
extern "C"  String_t* AchievementController_get_savePath_m3311468396 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AchievementController::ReadInfoFromDisk()
extern "C"  void AchievementController_ReadInfoFromDisk_m1096957999 (AchievementController_t3677499403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AchievementController::SaveInfoData()
extern "C"  void AchievementController_SaveInfoData_m3510437209 (AchievementController_t3677499403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AchievementController::OnApplicationPause(System.Boolean)
extern "C"  void AchievementController_OnApplicationPause_m3108940096 (AchievementController_t3677499403 * __this, bool ___pause0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AchievementController::OnApplicationQuit()
extern "C"  void AchievementController_OnApplicationQuit_m3159388990 (AchievementController_t3677499403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AchievementController::Start()
extern "C"  Il2CppObject * AchievementController_Start_m2334494472 (AchievementController_t3677499403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AchievementController::ReportAchievement(System.String)
extern "C"  void AchievementController_ReportAchievement_m3162260905 (Il2CppObject * __this /* static, unused */, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AchievementController::TryLoginOnce(System.Action)
extern "C"  void AchievementController_TryLoginOnce_m2415597962 (AchievementController_t3677499403 * __this, Action_t437523947 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AchievementController::ShowAchievements()
extern "C"  void AchievementController_ShowAchievements_m1282271621 (AchievementController_t3677499403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AchievementController::ShowLiderboards()
extern "C"  void AchievementController_ShowLiderboards_m3689740066 (AchievementController_t3677499403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AchievementController::ReportAchievementInner(System.String)
extern "C"  void AchievementController_ReportAchievementInner_m1345811875 (AchievementController_t3677499403 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AchievementController::ReportCoroutine()
extern "C"  Il2CppObject * AchievementController_ReportCoroutine_m2667309034 (AchievementController_t3677499403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AchievementController::Report(System.String)
extern "C"  Il2CppObject * AchievementController_Report_m617730386 (AchievementController_t3677499403 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AchievementController::<ReportAchievementInner>m__C7()
extern "C"  void AchievementController_U3CReportAchievementInnerU3Em__C7_m1522874872 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
