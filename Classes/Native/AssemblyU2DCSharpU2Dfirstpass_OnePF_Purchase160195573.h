﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnePF.Purchase
struct  Purchase_t160195573  : public Il2CppObject
{
public:
	// System.String OnePF.Purchase::<ItemType>k__BackingField
	String_t* ___U3CItemTypeU3Ek__BackingField_0;
	// System.String OnePF.Purchase::<OrderId>k__BackingField
	String_t* ___U3COrderIdU3Ek__BackingField_1;
	// System.String OnePF.Purchase::<PackageName>k__BackingField
	String_t* ___U3CPackageNameU3Ek__BackingField_2;
	// System.String OnePF.Purchase::<Sku>k__BackingField
	String_t* ___U3CSkuU3Ek__BackingField_3;
	// System.Int64 OnePF.Purchase::<PurchaseTime>k__BackingField
	int64_t ___U3CPurchaseTimeU3Ek__BackingField_4;
	// System.Int32 OnePF.Purchase::<PurchaseState>k__BackingField
	int32_t ___U3CPurchaseStateU3Ek__BackingField_5;
	// System.String OnePF.Purchase::<DeveloperPayload>k__BackingField
	String_t* ___U3CDeveloperPayloadU3Ek__BackingField_6;
	// System.String OnePF.Purchase::<Token>k__BackingField
	String_t* ___U3CTokenU3Ek__BackingField_7;
	// System.String OnePF.Purchase::<OriginalJson>k__BackingField
	String_t* ___U3COriginalJsonU3Ek__BackingField_8;
	// System.String OnePF.Purchase::<Signature>k__BackingField
	String_t* ___U3CSignatureU3Ek__BackingField_9;
	// System.String OnePF.Purchase::<AppstoreName>k__BackingField
	String_t* ___U3CAppstoreNameU3Ek__BackingField_10;
	// System.String OnePF.Purchase::<Receipt>k__BackingField
	String_t* ___U3CReceiptU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_U3CItemTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Purchase_t160195573, ___U3CItemTypeU3Ek__BackingField_0)); }
	inline String_t* get_U3CItemTypeU3Ek__BackingField_0() const { return ___U3CItemTypeU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CItemTypeU3Ek__BackingField_0() { return &___U3CItemTypeU3Ek__BackingField_0; }
	inline void set_U3CItemTypeU3Ek__BackingField_0(String_t* value)
	{
		___U3CItemTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CItemTypeU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3COrderIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Purchase_t160195573, ___U3COrderIdU3Ek__BackingField_1)); }
	inline String_t* get_U3COrderIdU3Ek__BackingField_1() const { return ___U3COrderIdU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3COrderIdU3Ek__BackingField_1() { return &___U3COrderIdU3Ek__BackingField_1; }
	inline void set_U3COrderIdU3Ek__BackingField_1(String_t* value)
	{
		___U3COrderIdU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3COrderIdU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CPackageNameU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Purchase_t160195573, ___U3CPackageNameU3Ek__BackingField_2)); }
	inline String_t* get_U3CPackageNameU3Ek__BackingField_2() const { return ___U3CPackageNameU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CPackageNameU3Ek__BackingField_2() { return &___U3CPackageNameU3Ek__BackingField_2; }
	inline void set_U3CPackageNameU3Ek__BackingField_2(String_t* value)
	{
		___U3CPackageNameU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPackageNameU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CSkuU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Purchase_t160195573, ___U3CSkuU3Ek__BackingField_3)); }
	inline String_t* get_U3CSkuU3Ek__BackingField_3() const { return ___U3CSkuU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CSkuU3Ek__BackingField_3() { return &___U3CSkuU3Ek__BackingField_3; }
	inline void set_U3CSkuU3Ek__BackingField_3(String_t* value)
	{
		___U3CSkuU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CSkuU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CPurchaseTimeU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Purchase_t160195573, ___U3CPurchaseTimeU3Ek__BackingField_4)); }
	inline int64_t get_U3CPurchaseTimeU3Ek__BackingField_4() const { return ___U3CPurchaseTimeU3Ek__BackingField_4; }
	inline int64_t* get_address_of_U3CPurchaseTimeU3Ek__BackingField_4() { return &___U3CPurchaseTimeU3Ek__BackingField_4; }
	inline void set_U3CPurchaseTimeU3Ek__BackingField_4(int64_t value)
	{
		___U3CPurchaseTimeU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CPurchaseStateU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Purchase_t160195573, ___U3CPurchaseStateU3Ek__BackingField_5)); }
	inline int32_t get_U3CPurchaseStateU3Ek__BackingField_5() const { return ___U3CPurchaseStateU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CPurchaseStateU3Ek__BackingField_5() { return &___U3CPurchaseStateU3Ek__BackingField_5; }
	inline void set_U3CPurchaseStateU3Ek__BackingField_5(int32_t value)
	{
		___U3CPurchaseStateU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CDeveloperPayloadU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Purchase_t160195573, ___U3CDeveloperPayloadU3Ek__BackingField_6)); }
	inline String_t* get_U3CDeveloperPayloadU3Ek__BackingField_6() const { return ___U3CDeveloperPayloadU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CDeveloperPayloadU3Ek__BackingField_6() { return &___U3CDeveloperPayloadU3Ek__BackingField_6; }
	inline void set_U3CDeveloperPayloadU3Ek__BackingField_6(String_t* value)
	{
		___U3CDeveloperPayloadU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CDeveloperPayloadU3Ek__BackingField_6, value);
	}

	inline static int32_t get_offset_of_U3CTokenU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Purchase_t160195573, ___U3CTokenU3Ek__BackingField_7)); }
	inline String_t* get_U3CTokenU3Ek__BackingField_7() const { return ___U3CTokenU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CTokenU3Ek__BackingField_7() { return &___U3CTokenU3Ek__BackingField_7; }
	inline void set_U3CTokenU3Ek__BackingField_7(String_t* value)
	{
		___U3CTokenU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTokenU3Ek__BackingField_7, value);
	}

	inline static int32_t get_offset_of_U3COriginalJsonU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Purchase_t160195573, ___U3COriginalJsonU3Ek__BackingField_8)); }
	inline String_t* get_U3COriginalJsonU3Ek__BackingField_8() const { return ___U3COriginalJsonU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3COriginalJsonU3Ek__BackingField_8() { return &___U3COriginalJsonU3Ek__BackingField_8; }
	inline void set_U3COriginalJsonU3Ek__BackingField_8(String_t* value)
	{
		___U3COriginalJsonU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3COriginalJsonU3Ek__BackingField_8, value);
	}

	inline static int32_t get_offset_of_U3CSignatureU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Purchase_t160195573, ___U3CSignatureU3Ek__BackingField_9)); }
	inline String_t* get_U3CSignatureU3Ek__BackingField_9() const { return ___U3CSignatureU3Ek__BackingField_9; }
	inline String_t** get_address_of_U3CSignatureU3Ek__BackingField_9() { return &___U3CSignatureU3Ek__BackingField_9; }
	inline void set_U3CSignatureU3Ek__BackingField_9(String_t* value)
	{
		___U3CSignatureU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CSignatureU3Ek__BackingField_9, value);
	}

	inline static int32_t get_offset_of_U3CAppstoreNameU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(Purchase_t160195573, ___U3CAppstoreNameU3Ek__BackingField_10)); }
	inline String_t* get_U3CAppstoreNameU3Ek__BackingField_10() const { return ___U3CAppstoreNameU3Ek__BackingField_10; }
	inline String_t** get_address_of_U3CAppstoreNameU3Ek__BackingField_10() { return &___U3CAppstoreNameU3Ek__BackingField_10; }
	inline void set_U3CAppstoreNameU3Ek__BackingField_10(String_t* value)
	{
		___U3CAppstoreNameU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAppstoreNameU3Ek__BackingField_10, value);
	}

	inline static int32_t get_offset_of_U3CReceiptU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(Purchase_t160195573, ___U3CReceiptU3Ek__BackingField_11)); }
	inline String_t* get_U3CReceiptU3Ek__BackingField_11() const { return ___U3CReceiptU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CReceiptU3Ek__BackingField_11() { return &___U3CReceiptU3Ek__BackingField_11; }
	inline void set_U3CReceiptU3Ek__BackingField_11(String_t* value)
	{
		___U3CReceiptU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CReceiptU3Ek__BackingField_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
