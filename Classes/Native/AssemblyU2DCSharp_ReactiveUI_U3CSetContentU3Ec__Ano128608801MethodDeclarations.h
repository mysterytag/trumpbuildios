﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ReactiveUI/<SetContent>c__AnonStorey124`1<System.Object>
struct U3CSetContentU3Ec__AnonStorey124_1_t128608801;
// System.String
struct String_t;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t3709260776;

#include "codegen/il2cpp-codegen.h"

// System.Void ReactiveUI/<SetContent>c__AnonStorey124`1<System.Object>::.ctor()
extern "C"  void U3CSetContentU3Ec__AnonStorey124_1__ctor_m1980478089_gshared (U3CSetContentU3Ec__AnonStorey124_1_t128608801 * __this, const MethodInfo* method);
#define U3CSetContentU3Ec__AnonStorey124_1__ctor_m1980478089(__this, method) ((  void (*) (U3CSetContentU3Ec__AnonStorey124_1_t128608801 *, const MethodInfo*))U3CSetContentU3Ec__AnonStorey124_1__ctor_m1980478089_gshared)(__this, method)
// System.String ReactiveUI/<SetContent>c__AnonStorey124`1<System.Object>::<>m__1B4(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  String_t* U3CSetContentU3Ec__AnonStorey124_1_U3CU3Em__1B4_m910397865_gshared (U3CSetContentU3Ec__AnonStorey124_1_t128608801 * __this, Il2CppObject* ___val0, const MethodInfo* method);
#define U3CSetContentU3Ec__AnonStorey124_1_U3CU3Em__1B4_m910397865(__this, ___val0, method) ((  String_t* (*) (U3CSetContentU3Ec__AnonStorey124_1_t128608801 *, Il2CppObject*, const MethodInfo*))U3CSetContentU3Ec__AnonStorey124_1_U3CU3Em__1B4_m910397865_gshared)(__this, ___val0, method)
