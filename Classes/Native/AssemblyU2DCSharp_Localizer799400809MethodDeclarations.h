﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Localizer
struct Localizer_t799400809;

#include "codegen/il2cpp-codegen.h"

// System.Void Localizer::.ctor()
extern "C"  void Localizer__ctor_m1753479202 (Localizer_t799400809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Localizer::Awake()
extern "C"  void Localizer_Awake_m1991084421 (Localizer_t799400809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Localizer::Update()
extern "C"  void Localizer_Update_m250142507 (Localizer_t799400809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
