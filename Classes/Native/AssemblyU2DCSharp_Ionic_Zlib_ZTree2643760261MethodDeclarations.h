﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ionic.Zlib.ZTree
struct ZTree_t2643760261;
// Ionic.Zlib.DeflateManager
struct DeflateManager_t3792001679;
// System.Int16[]
struct Int16U5BU5D_t3675865332;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Ionic_Zlib_DeflateManager3792001679.h"

// System.Void Ionic.Zlib.ZTree::.ctor()
extern "C"  void ZTree__ctor_m1926599162 (ZTree_t2643760261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.ZTree::.cctor()
extern "C"  void ZTree__cctor_m3407902963 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.ZTree::DistanceCode(System.Int32)
extern "C"  int32_t ZTree_DistanceCode_m1792112235 (Il2CppObject * __this /* static, unused */, int32_t ___dist0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.ZTree::gen_bitlen(Ionic.Zlib.DeflateManager)
extern "C"  void ZTree_gen_bitlen_m3111954616 (ZTree_t2643760261 * __this, DeflateManager_t3792001679 * ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.ZTree::build_tree(Ionic.Zlib.DeflateManager)
extern "C"  void ZTree_build_tree_m3926524064 (ZTree_t2643760261 * __this, DeflateManager_t3792001679 * ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ionic.Zlib.ZTree::gen_codes(System.Int16[],System.Int32,System.Int16[])
extern "C"  void ZTree_gen_codes_m2283226064 (Il2CppObject * __this /* static, unused */, Int16U5BU5D_t3675865332* ___tree0, int32_t ___max_code1, Int16U5BU5D_t3675865332* ___bl_count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Ionic.Zlib.ZTree::bi_reverse(System.Int32,System.Int32)
extern "C"  int32_t ZTree_bi_reverse_m3238884932 (Il2CppObject * __this /* static, unused */, int32_t ___code0, int32_t ___len1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
