﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// UniRx.Operators.ZipFunc`4<System.Object,System.Object,System.Object,System.Object>
struct ZipFunc_4_t799882833;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4196218687.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.ZipObservable`4<System.Object,System.Object,System.Object,System.Object>
struct  ZipObservable_4_t637587494  : public OperatorObservableBase_1_t4196218687
{
public:
	// UniRx.IObservable`1<T1> UniRx.Operators.ZipObservable`4::source1
	Il2CppObject* ___source1_1;
	// UniRx.IObservable`1<T2> UniRx.Operators.ZipObservable`4::source2
	Il2CppObject* ___source2_2;
	// UniRx.IObservable`1<T3> UniRx.Operators.ZipObservable`4::source3
	Il2CppObject* ___source3_3;
	// UniRx.Operators.ZipFunc`4<T1,T2,T3,TR> UniRx.Operators.ZipObservable`4::resultSelector
	ZipFunc_4_t799882833 * ___resultSelector_4;

public:
	inline static int32_t get_offset_of_source1_1() { return static_cast<int32_t>(offsetof(ZipObservable_4_t637587494, ___source1_1)); }
	inline Il2CppObject* get_source1_1() const { return ___source1_1; }
	inline Il2CppObject** get_address_of_source1_1() { return &___source1_1; }
	inline void set_source1_1(Il2CppObject* value)
	{
		___source1_1 = value;
		Il2CppCodeGenWriteBarrier(&___source1_1, value);
	}

	inline static int32_t get_offset_of_source2_2() { return static_cast<int32_t>(offsetof(ZipObservable_4_t637587494, ___source2_2)); }
	inline Il2CppObject* get_source2_2() const { return ___source2_2; }
	inline Il2CppObject** get_address_of_source2_2() { return &___source2_2; }
	inline void set_source2_2(Il2CppObject* value)
	{
		___source2_2 = value;
		Il2CppCodeGenWriteBarrier(&___source2_2, value);
	}

	inline static int32_t get_offset_of_source3_3() { return static_cast<int32_t>(offsetof(ZipObservable_4_t637587494, ___source3_3)); }
	inline Il2CppObject* get_source3_3() const { return ___source3_3; }
	inline Il2CppObject** get_address_of_source3_3() { return &___source3_3; }
	inline void set_source3_3(Il2CppObject* value)
	{
		___source3_3 = value;
		Il2CppCodeGenWriteBarrier(&___source3_3, value);
	}

	inline static int32_t get_offset_of_resultSelector_4() { return static_cast<int32_t>(offsetof(ZipObservable_4_t637587494, ___resultSelector_4)); }
	inline ZipFunc_4_t799882833 * get_resultSelector_4() const { return ___resultSelector_4; }
	inline ZipFunc_4_t799882833 ** get_address_of_resultSelector_4() { return &___resultSelector_4; }
	inline void set_resultSelector_4(ZipFunc_4_t799882833 * value)
	{
		___resultSelector_4 = value;
		Il2CppCodeGenWriteBarrier(&___resultSelector_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
