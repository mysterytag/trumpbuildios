﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CollectionMoveEvent_1_gen3572055381MethodDeclarations.h"

// System.Void CollectionMoveEvent`1<DonateButton>::.ctor(System.Int32,System.Int32,T)
#define CollectionMoveEvent_1__ctor_m2113036720(__this, ___oldIndex0, ___newIndex1, ___value2, method) ((  void (*) (CollectionMoveEvent_1_t3405702402 *, int32_t, int32_t, DonateButton_t670753441 *, const MethodInfo*))CollectionMoveEvent_1__ctor_m2094255715_gshared)(__this, ___oldIndex0, ___newIndex1, ___value2, method)
// System.Int32 CollectionMoveEvent`1<DonateButton>::get_OldIndex()
#define CollectionMoveEvent_1_get_OldIndex_m4066795932(__this, method) ((  int32_t (*) (CollectionMoveEvent_1_t3405702402 *, const MethodInfo*))CollectionMoveEvent_1_get_OldIndex_m1896787371_gshared)(__this, method)
// System.Void CollectionMoveEvent`1<DonateButton>::set_OldIndex(System.Int32)
#define CollectionMoveEvent_1_set_OldIndex_m1006870063(__this, ___value0, method) ((  void (*) (CollectionMoveEvent_1_t3405702402 *, int32_t, const MethodInfo*))CollectionMoveEvent_1_set_OldIndex_m3746196962_gshared)(__this, ___value0, method)
// System.Int32 CollectionMoveEvent`1<DonateButton>::get_NewIndex()
#define CollectionMoveEvent_1_get_NewIndex_m2784246787(__this, method) ((  int32_t (*) (CollectionMoveEvent_1_t3405702402 *, const MethodInfo*))CollectionMoveEvent_1_get_NewIndex_m614238226_gshared)(__this, method)
// System.Void CollectionMoveEvent`1<DonateButton>::set_NewIndex(System.Int32)
#define CollectionMoveEvent_1_set_NewIndex_m2387744278(__this, ___value0, method) ((  void (*) (CollectionMoveEvent_1_t3405702402 *, int32_t, const MethodInfo*))CollectionMoveEvent_1_set_NewIndex_m832103881_gshared)(__this, ___value0, method)
// T CollectionMoveEvent`1<DonateButton>::get_Value()
#define CollectionMoveEvent_1_get_Value_m3898554163(__this, method) ((  DonateButton_t670753441 * (*) (CollectionMoveEvent_1_t3405702402 *, const MethodInfo*))CollectionMoveEvent_1_get_Value_m3472123074_gshared)(__this, method)
// System.Void CollectionMoveEvent`1<DonateButton>::set_Value(T)
#define CollectionMoveEvent_1_set_Value_m837539486(__this, ___value0, method) ((  void (*) (CollectionMoveEvent_1_t3405702402 *, DonateButton_t670753441 *, const MethodInfo*))CollectionMoveEvent_1_set_Value_m4226272145_gshared)(__this, ___value0, method)
// System.String CollectionMoveEvent`1<DonateButton>::ToString()
#define CollectionMoveEvent_1_ToString_m747389727(__this, method) ((  String_t* (*) (CollectionMoveEvent_1_t3405702402 *, const MethodInfo*))CollectionMoveEvent_1_ToString_m512063864_gshared)(__this, method)
