﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.WhereObservable`1/Where<UniRx.Unit>
struct Where_t400675722;
// UniRx.Operators.WhereObservable`1<UniRx.Unit>
struct WhereObservable_1_t3756845987;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.WhereObservable`1/Where<UniRx.Unit>::.ctor(UniRx.Operators.WhereObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Where__ctor_m2768198231_gshared (Where_t400675722 * __this, WhereObservable_1_t3756845987 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Where__ctor_m2768198231(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Where_t400675722 *, WhereObservable_1_t3756845987 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Where__ctor_m2768198231_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.WhereObservable`1/Where<UniRx.Unit>::OnNext(T)
extern "C"  void Where_OnNext_m2681425469_gshared (Where_t400675722 * __this, Unit_t2558286038  ___value0, const MethodInfo* method);
#define Where_OnNext_m2681425469(__this, ___value0, method) ((  void (*) (Where_t400675722 *, Unit_t2558286038 , const MethodInfo*))Where_OnNext_m2681425469_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.WhereObservable`1/Where<UniRx.Unit>::OnError(System.Exception)
extern "C"  void Where_OnError_m2134200140_gshared (Where_t400675722 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Where_OnError_m2134200140(__this, ___error0, method) ((  void (*) (Where_t400675722 *, Exception_t1967233988 *, const MethodInfo*))Where_OnError_m2134200140_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.WhereObservable`1/Where<UniRx.Unit>::OnCompleted()
extern "C"  void Where_OnCompleted_m568824991_gshared (Where_t400675722 * __this, const MethodInfo* method);
#define Where_OnCompleted_m568824991(__this, method) ((  void (*) (Where_t400675722 *, const MethodInfo*))Where_OnCompleted_m568824991_gshared)(__this, method)
