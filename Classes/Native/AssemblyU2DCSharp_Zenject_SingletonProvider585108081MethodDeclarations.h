﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.SingletonProvider
struct SingletonProvider_t585108081;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// Zenject.SingletonLazyCreator
struct SingletonLazyCreator_t2762284194;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// Zenject.InjectContext
struct InjectContext_t3456483891;
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException>
struct IEnumerable_1_t4073207355;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449.h"
#include "AssemblyU2DCSharp_Zenject_SingletonLazyCreator2762284194.h"
#include "AssemblyU2DCSharp_Zenject_InjectContext3456483891.h"

// System.Void Zenject.SingletonProvider::.ctor(Zenject.DiContainer,Zenject.SingletonLazyCreator)
extern "C"  void SingletonProvider__ctor_m1411291507 (SingletonProvider_t585108081 * __this, DiContainer_t2383114449 * ___container0, SingletonLazyCreator_t2762284194 * ___creator1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.SingletonProvider::Dispose()
extern "C"  void SingletonProvider_Dispose_m2647897099 (SingletonProvider_t585108081 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Zenject.SingletonProvider::GetInstanceType()
extern "C"  Type_t * SingletonProvider_GetInstanceType_m3993758059 (SingletonProvider_t585108081 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Zenject.SingletonProvider::GetInstance(Zenject.InjectContext)
extern "C"  Il2CppObject * SingletonProvider_GetInstance_m3733716455 (SingletonProvider_t585108081 * __this, InjectContext_t3456483891 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.SingletonProvider::ValidateBinding(Zenject.InjectContext)
extern "C"  Il2CppObject* SingletonProvider_ValidateBinding_m394548089 (SingletonProvider_t585108081 * __this, InjectContext_t3456483891 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
