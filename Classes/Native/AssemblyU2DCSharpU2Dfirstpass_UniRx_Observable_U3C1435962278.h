﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`5<System.Object,System.Object,System.AsyncCallback,System.Object,System.IAsyncResult>
struct Func_5_t2364630122;
// System.Func`2<System.IAsyncResult,System.Object>
struct Func_2_t850390275;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/<FromAsyncPattern>c__AnonStorey43`3<System.Object,System.Object,System.Object>
struct  U3CFromAsyncPatternU3Ec__AnonStorey43_3_t1435962278  : public Il2CppObject
{
public:
	// System.Func`5<T1,T2,System.AsyncCallback,System.Object,System.IAsyncResult> UniRx.Observable/<FromAsyncPattern>c__AnonStorey43`3::begin
	Func_5_t2364630122 * ___begin_0;
	// System.Func`2<System.IAsyncResult,TResult> UniRx.Observable/<FromAsyncPattern>c__AnonStorey43`3::end
	Func_2_t850390275 * ___end_1;

public:
	inline static int32_t get_offset_of_begin_0() { return static_cast<int32_t>(offsetof(U3CFromAsyncPatternU3Ec__AnonStorey43_3_t1435962278, ___begin_0)); }
	inline Func_5_t2364630122 * get_begin_0() const { return ___begin_0; }
	inline Func_5_t2364630122 ** get_address_of_begin_0() { return &___begin_0; }
	inline void set_begin_0(Func_5_t2364630122 * value)
	{
		___begin_0 = value;
		Il2CppCodeGenWriteBarrier(&___begin_0, value);
	}

	inline static int32_t get_offset_of_end_1() { return static_cast<int32_t>(offsetof(U3CFromAsyncPatternU3Ec__AnonStorey43_3_t1435962278, ___end_1)); }
	inline Func_2_t850390275 * get_end_1() const { return ___end_1; }
	inline Func_2_t850390275 ** get_address_of_end_1() { return &___end_1; }
	inline void set_end_1(Func_2_t850390275 * value)
	{
		___end_1 = value;
		Il2CppCodeGenWriteBarrier(&___end_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
