﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Func`2<System.Int32,UnityEngine.Color>
struct Func_2_t390344745;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Func`2<System.Int32,UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m773707327_gshared (Func_2_t390344745 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Func_2__ctor_m773707327(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t390344745 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m773707327_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<System.Int32,UnityEngine.Color>::Invoke(T)
extern "C"  Color_t1588175760  Func_2_Invoke_m3571737047_gshared (Func_2_t390344745 * __this, int32_t ___arg10, const MethodInfo* method);
#define Func_2_Invoke_m3571737047(__this, ___arg10, method) ((  Color_t1588175760  (*) (Func_2_t390344745 *, int32_t, const MethodInfo*))Func_2_Invoke_m3571737047_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<System.Int32,UnityEngine.Color>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_2_BeginInvoke_m2721947530_gshared (Func_2_t390344745 * __this, int32_t ___arg10, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method);
#define Func_2_BeginInvoke_m2721947530(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t390344745 *, int32_t, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m2721947530_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<System.Int32,UnityEngine.Color>::EndInvoke(System.IAsyncResult)
extern "C"  Color_t1588175760  Func_2_EndInvoke_m3329637981_gshared (Func_2_t390344745 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Func_2_EndInvoke_m3329637981(__this, ___result0, method) ((  Color_t1588175760  (*) (Func_2_t390344745 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m3329637981_gshared)(__this, ___result0, method)
