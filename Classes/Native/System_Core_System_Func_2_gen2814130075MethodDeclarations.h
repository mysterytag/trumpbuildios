﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen2877437650MethodDeclarations.h"

// System.Void System.Func`2<System.Int64,DateTimeCell>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m1051393777(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t2814130075 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m165973854_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<System.Int64,DateTimeCell>::Invoke(T)
#define Func_2_Invoke_m2989683777(__this, ___arg10, method) ((  DateTimeCell_t773798845 * (*) (Func_2_t2814130075 *, int64_t, const MethodInfo*))Func_2_Invoke_m794738632_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<System.Int64,DateTimeCell>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m4111824624(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t2814130075 *, int64_t, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m4176984443_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<System.Int64,DateTimeCell>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m3719705139(__this, ___result0, method) ((  DateTimeCell_t773798845 * (*) (Func_2_t2814130075 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m3572281612_gshared)(__this, ___result0, method)
