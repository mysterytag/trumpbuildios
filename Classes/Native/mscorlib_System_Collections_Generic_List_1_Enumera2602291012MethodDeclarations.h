﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4014815677MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<ModestTree.Util.Tuple`2<System.Type,System.Int32>>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1173859169(__this, ___l0, method) ((  void (*) (Enumerator_t2602291012 *, List_1_t221540724 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ModestTree.Util.Tuple`2<System.Type,System.Int32>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2843742289(__this, method) ((  void (*) (Enumerator_t2602291012 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<ModestTree.Util.Tuple`2<System.Type,System.Int32>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1805784893(__this, method) ((  Il2CppObject * (*) (Enumerator_t2602291012 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ModestTree.Util.Tuple`2<System.Type,System.Int32>>::Dispose()
#define Enumerator_Dispose_m2702729606(__this, method) ((  void (*) (Enumerator_t2602291012 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ModestTree.Util.Tuple`2<System.Type,System.Int32>>::VerifyState()
#define Enumerator_VerifyState_m479456703(__this, method) ((  void (*) (Enumerator_t2602291012 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<ModestTree.Util.Tuple`2<System.Type,System.Int32>>::MoveNext()
#define Enumerator_MoveNext_m2502495421(__this, method) ((  bool (*) (Enumerator_t2602291012 *, const MethodInfo*))Enumerator_MoveNext_m302222300_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<ModestTree.Util.Tuple`2<System.Type,System.Int32>>::get_Current()
#define Enumerator_get_Current_m1970450998(__this, method) ((  Tuple_2_t3719549051 * (*) (Enumerator_t2602291012 *, const MethodInfo*))Enumerator_get_Current_m1113061044_gshared)(__this, method)
