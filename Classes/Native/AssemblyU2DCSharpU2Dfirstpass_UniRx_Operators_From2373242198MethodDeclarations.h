﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.FromEventObservable`2<System.Object,System.Single>
struct FromEventObservable_2_t2373242198;
// System.Func`2<System.Action`1<System.Single>,System.Object>
struct Func_2_t2308880486;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Single>
struct IObserver_1_t3170207924;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.FromEventObservable`2<System.Object,System.Single>::.ctor(System.Func`2<System.Action`1<TEventArgs>,TDelegate>,System.Action`1<TDelegate>,System.Action`1<TDelegate>)
extern "C"  void FromEventObservable_2__ctor_m1581643305_gshared (FromEventObservable_2_t2373242198 * __this, Func_2_t2308880486 * ___conversion0, Action_1_t985559125 * ___addHandler1, Action_1_t985559125 * ___removeHandler2, const MethodInfo* method);
#define FromEventObservable_2__ctor_m1581643305(__this, ___conversion0, ___addHandler1, ___removeHandler2, method) ((  void (*) (FromEventObservable_2_t2373242198 *, Func_2_t2308880486 *, Action_1_t985559125 *, Action_1_t985559125 *, const MethodInfo*))FromEventObservable_2__ctor_m1581643305_gshared)(__this, ___conversion0, ___addHandler1, ___removeHandler2, method)
// System.IDisposable UniRx.Operators.FromEventObservable`2<System.Object,System.Single>::SubscribeCore(UniRx.IObserver`1<TEventArgs>,System.IDisposable)
extern "C"  Il2CppObject * FromEventObservable_2_SubscribeCore_m3866424937_gshared (FromEventObservable_2_t2373242198 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define FromEventObservable_2_SubscribeCore_m3866424937(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (FromEventObservable_2_t2373242198 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))FromEventObservable_2_SubscribeCore_m3866424937_gshared)(__this, ___observer0, ___cancel1, method)
