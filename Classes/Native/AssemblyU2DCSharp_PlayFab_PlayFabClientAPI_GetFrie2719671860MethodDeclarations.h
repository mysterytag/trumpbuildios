﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetFriendLeaderboardAroundCurrentUserResponseCallback
struct GetFriendLeaderboardAroundCurrentUserResponseCallback_t2719671860;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetFriendLeaderboardAroundCurrentUserRequest
struct GetFriendLeaderboardAroundCurrentUserRequest_t2179115089;
// PlayFab.ClientModels.GetFriendLeaderboardAroundCurrentUserResult
struct GetFriendLeaderboardAroundCurrentUserResult_t659631163;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetFriendLe2179115089.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetFriendLea659631163.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetFriendLeaderboardAroundCurrentUserResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetFriendLeaderboardAroundCurrentUserResponseCallback__ctor_m1980722371 (GetFriendLeaderboardAroundCurrentUserResponseCallback_t2719671860 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetFriendLeaderboardAroundCurrentUserResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetFriendLeaderboardAroundCurrentUserRequest,PlayFab.ClientModels.GetFriendLeaderboardAroundCurrentUserResult,PlayFab.PlayFabError,System.Object)
extern "C"  void GetFriendLeaderboardAroundCurrentUserResponseCallback_Invoke_m3534245130 (GetFriendLeaderboardAroundCurrentUserResponseCallback_t2719671860 * __this, String_t* ___urlPath0, int32_t ___callId1, GetFriendLeaderboardAroundCurrentUserRequest_t2179115089 * ___request2, GetFriendLeaderboardAroundCurrentUserResult_t659631163 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetFriendLeaderboardAroundCurrentUserResponseCallback_t2719671860(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetFriendLeaderboardAroundCurrentUserRequest_t2179115089 * ___request2, GetFriendLeaderboardAroundCurrentUserResult_t659631163 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetFriendLeaderboardAroundCurrentUserResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetFriendLeaderboardAroundCurrentUserRequest,PlayFab.ClientModels.GetFriendLeaderboardAroundCurrentUserResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetFriendLeaderboardAroundCurrentUserResponseCallback_BeginInvoke_m3145553355 (GetFriendLeaderboardAroundCurrentUserResponseCallback_t2719671860 * __this, String_t* ___urlPath0, int32_t ___callId1, GetFriendLeaderboardAroundCurrentUserRequest_t2179115089 * ___request2, GetFriendLeaderboardAroundCurrentUserResult_t659631163 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetFriendLeaderboardAroundCurrentUserResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetFriendLeaderboardAroundCurrentUserResponseCallback_EndInvoke_m1229305939 (GetFriendLeaderboardAroundCurrentUserResponseCallback_t2719671860 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
