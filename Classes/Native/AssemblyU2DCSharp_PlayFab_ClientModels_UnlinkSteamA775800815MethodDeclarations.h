﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.UnlinkSteamAccountRequest
struct UnlinkSteamAccountRequest_t775800815;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.UnlinkSteamAccountRequest::.ctor()
extern "C"  void UnlinkSteamAccountRequest__ctor_m3897264410 (UnlinkSteamAccountRequest_t775800815 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
