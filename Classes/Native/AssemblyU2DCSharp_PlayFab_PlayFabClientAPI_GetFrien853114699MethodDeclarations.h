﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetFriendLeaderboardRequestCallback
struct GetFriendLeaderboardRequestCallback_t853114699;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetFriendLeaderboardRequest
struct GetFriendLeaderboardRequest_t2919058678;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetFriendLe2919058678.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetFriendLeaderboardRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetFriendLeaderboardRequestCallback__ctor_m2354616922 (GetFriendLeaderboardRequestCallback_t853114699 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetFriendLeaderboardRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetFriendLeaderboardRequest,System.Object)
extern "C"  void GetFriendLeaderboardRequestCallback_Invoke_m2969444419 (GetFriendLeaderboardRequestCallback_t853114699 * __this, String_t* ___urlPath0, int32_t ___callId1, GetFriendLeaderboardRequest_t2919058678 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetFriendLeaderboardRequestCallback_t853114699(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetFriendLeaderboardRequest_t2919058678 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetFriendLeaderboardRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetFriendLeaderboardRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetFriendLeaderboardRequestCallback_BeginInvoke_m4273836606 (GetFriendLeaderboardRequestCallback_t853114699 * __this, String_t* ___urlPath0, int32_t ___callId1, GetFriendLeaderboardRequest_t2919058678 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetFriendLeaderboardRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetFriendLeaderboardRequestCallback_EndInvoke_m2239449450 (GetFriendLeaderboardRequestCallback_t853114699 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
