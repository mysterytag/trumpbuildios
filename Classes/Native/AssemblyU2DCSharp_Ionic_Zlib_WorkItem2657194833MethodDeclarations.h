﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ionic.Zlib.WorkItem
struct WorkItem_t2657194833;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Ionic_Zlib_CompressionLevel3940478795.h"
#include "AssemblyU2DCSharp_Ionic_Zlib_CompressionStrategy2328611910.h"

// System.Void Ionic.Zlib.WorkItem::.ctor(System.Int32,Ionic.Zlib.CompressionLevel,Ionic.Zlib.CompressionStrategy,System.Int32)
extern "C"  void WorkItem__ctor_m981635787 (WorkItem_t2657194833 * __this, int32_t ___size0, int32_t ___compressLevel1, int32_t ___strategy2, int32_t ___ix3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
