﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.PayResult
struct PayResult_t3664338084;
// Facebook.Unity.ResultContainer
struct ResultContainer_t79372963;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Facebook_Unity_ResultContainer79372963.h"

// System.Void Facebook.Unity.PayResult::.ctor(Facebook.Unity.ResultContainer)
extern "C"  void PayResult__ctor_m2124401524 (PayResult_t3664338084 * __this, ResultContainer_t79372963 * ___resultContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Facebook.Unity.PayResult::get_ErrorCode()
extern "C"  int64_t PayResult_get_ErrorCode_m1019817618 (PayResult_t3664338084 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.PayResult::ToString()
extern "C"  String_t* PayResult_ToString_m3617955260 (PayResult_t3664338084 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
