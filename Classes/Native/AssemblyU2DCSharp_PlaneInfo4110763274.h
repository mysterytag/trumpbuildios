﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t284553113;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlaneInfo
struct  PlaneInfo_t4110763274  : public Il2CppObject
{
public:
	// System.Single PlaneInfo::speedCoeff
	float ___speedCoeff_0;
	// UnityEngine.Transform PlaneInfo::node
	Transform_t284553113 * ___node_1;
	// UnityEngine.Transform PlaneInfo::node2
	Transform_t284553113 * ___node2_2;

public:
	inline static int32_t get_offset_of_speedCoeff_0() { return static_cast<int32_t>(offsetof(PlaneInfo_t4110763274, ___speedCoeff_0)); }
	inline float get_speedCoeff_0() const { return ___speedCoeff_0; }
	inline float* get_address_of_speedCoeff_0() { return &___speedCoeff_0; }
	inline void set_speedCoeff_0(float value)
	{
		___speedCoeff_0 = value;
	}

	inline static int32_t get_offset_of_node_1() { return static_cast<int32_t>(offsetof(PlaneInfo_t4110763274, ___node_1)); }
	inline Transform_t284553113 * get_node_1() const { return ___node_1; }
	inline Transform_t284553113 ** get_address_of_node_1() { return &___node_1; }
	inline void set_node_1(Transform_t284553113 * value)
	{
		___node_1 = value;
		Il2CppCodeGenWriteBarrier(&___node_1, value);
	}

	inline static int32_t get_offset_of_node2_2() { return static_cast<int32_t>(offsetof(PlaneInfo_t4110763274, ___node2_2)); }
	inline Transform_t284553113 * get_node2_2() const { return ___node2_2; }
	inline Transform_t284553113 ** get_address_of_node2_2() { return &___node2_2; }
	inline void set_node2_2(Transform_t284553113 * value)
	{
		___node2_2 = value;
		Il2CppCodeGenWriteBarrier(&___node2_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
