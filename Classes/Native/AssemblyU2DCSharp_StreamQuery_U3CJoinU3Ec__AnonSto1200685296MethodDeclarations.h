﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StreamQuery/<Join>c__AnonStorey13F`1/<Join>c__AnonStorey140`1<System.Object>
struct U3CJoinU3Ec__AnonStorey140_1_t1200685296;
// IStream`1<System.Object>
struct IStream_1_t1389797411;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void StreamQuery/<Join>c__AnonStorey13F`1/<Join>c__AnonStorey140`1<System.Object>::.ctor()
extern "C"  void U3CJoinU3Ec__AnonStorey140_1__ctor_m2022263344_gshared (U3CJoinU3Ec__AnonStorey140_1_t1200685296 * __this, const MethodInfo* method);
#define U3CJoinU3Ec__AnonStorey140_1__ctor_m2022263344(__this, method) ((  void (*) (U3CJoinU3Ec__AnonStorey140_1_t1200685296 *, const MethodInfo*))U3CJoinU3Ec__AnonStorey140_1__ctor_m2022263344_gshared)(__this, method)
// System.Void StreamQuery/<Join>c__AnonStorey13F`1/<Join>c__AnonStorey140`1<System.Object>::<>m__1D4(IStream`1<T>)
extern "C"  void U3CJoinU3Ec__AnonStorey140_1_U3CU3Em__1D4_m1912173950_gshared (U3CJoinU3Ec__AnonStorey140_1_t1200685296 * __this, Il2CppObject* ___innerStream0, const MethodInfo* method);
#define U3CJoinU3Ec__AnonStorey140_1_U3CU3Em__1D4_m1912173950(__this, ___innerStream0, method) ((  void (*) (U3CJoinU3Ec__AnonStorey140_1_t1200685296 *, Il2CppObject*, const MethodInfo*))U3CJoinU3Ec__AnonStorey140_1_U3CU3Em__1D4_m1912173950_gshared)(__this, ___innerStream0, method)
// System.Void StreamQuery/<Join>c__AnonStorey13F`1/<Join>c__AnonStorey140`1<System.Object>::<>m__1D5(T)
extern "C"  void U3CJoinU3Ec__AnonStorey140_1_U3CU3Em__1D5_m2403709651_gshared (U3CJoinU3Ec__AnonStorey140_1_t1200685296 * __this, Il2CppObject * ___val0, const MethodInfo* method);
#define U3CJoinU3Ec__AnonStorey140_1_U3CU3Em__1D5_m2403709651(__this, ___val0, method) ((  void (*) (U3CJoinU3Ec__AnonStorey140_1_t1200685296 *, Il2CppObject *, const MethodInfo*))U3CJoinU3Ec__AnonStorey140_1_U3CU3Em__1D5_m2403709651_gshared)(__this, ___val0, method)
