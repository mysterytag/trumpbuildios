﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Subject`1<UniRx.Unit>
struct Subject_1_t201353362;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Triggers_Obser1883237895.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableFixedUpdateTrigger
struct  ObservableFixedUpdateTrigger_t3436683039  : public ObservableTriggerBase_t1883237895
{
public:
	// UniRx.Subject`1<UniRx.Unit> UniRx.Triggers.ObservableFixedUpdateTrigger::fixedUpdate
	Subject_1_t201353362 * ___fixedUpdate_8;

public:
	inline static int32_t get_offset_of_fixedUpdate_8() { return static_cast<int32_t>(offsetof(ObservableFixedUpdateTrigger_t3436683039, ___fixedUpdate_8)); }
	inline Subject_1_t201353362 * get_fixedUpdate_8() const { return ___fixedUpdate_8; }
	inline Subject_1_t201353362 ** get_address_of_fixedUpdate_8() { return &___fixedUpdate_8; }
	inline void set_fixedUpdate_8(Subject_1_t201353362 * value)
	{
		___fixedUpdate_8 = value;
		Il2CppCodeGenWriteBarrier(&___fixedUpdate_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
