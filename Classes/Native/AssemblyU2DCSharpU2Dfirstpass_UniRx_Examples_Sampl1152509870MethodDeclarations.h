﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Examples.Sample09_EventHandling/<Start>c__AnonStorey32
struct U3CStartU3Ec__AnonStorey32_t1152509870;
// System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>
struct EventHandler_1_t3942771990;
// System.Action`1<System.Int32>
struct Action_1_t2995867492;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Examples.Sample09_EventHandling/<Start>c__AnonStorey32::.ctor()
extern "C"  void U3CStartU3Ec__AnonStorey32__ctor_m2620545362 (U3CStartU3Ec__AnonStorey32_t1152509870 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample09_EventHandling/<Start>c__AnonStorey32::<>m__25(System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>)
extern "C"  void U3CStartU3Ec__AnonStorey32_U3CU3Em__25_m1219274304 (U3CStartU3Ec__AnonStorey32_t1152509870 * __this, EventHandler_1_t3942771990 * ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample09_EventHandling/<Start>c__AnonStorey32::<>m__26(System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>)
extern "C"  void U3CStartU3Ec__AnonStorey32_U3CU3Em__26_m2086326529 (U3CStartU3Ec__AnonStorey32_t1152509870 * __this, EventHandler_1_t3942771990 * ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample09_EventHandling/<Start>c__AnonStorey32::<>m__28(System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>)
extern "C"  void U3CStartU3Ec__AnonStorey32_U3CU3Em__28_m3820430979 (U3CStartU3Ec__AnonStorey32_t1152509870 * __this, EventHandler_1_t3942771990 * ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample09_EventHandling/<Start>c__AnonStorey32::<>m__29(System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>)
extern "C"  void U3CStartU3Ec__AnonStorey32_U3CU3Em__29_m392515908 (U3CStartU3Ec__AnonStorey32_t1152509870 * __this, EventHandler_1_t3942771990 * ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample09_EventHandling/<Start>c__AnonStorey32::<>m__2A(System.Action`1<System.Int32>)
extern "C"  void U3CStartU3Ec__AnonStorey32_U3CU3Em__2A_m3630825647 (U3CStartU3Ec__AnonStorey32_t1152509870 * __this, Action_1_t2995867492 * ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample09_EventHandling/<Start>c__AnonStorey32::<>m__2B(System.Action`1<System.Int32>)
extern "C"  void U3CStartU3Ec__AnonStorey32_U3CU3Em__2B_m1620721806 (U3CStartU3Ec__AnonStorey32_t1152509870 * __this, Action_1_t2995867492 * ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs> UniRx.Examples.Sample09_EventHandling/<Start>c__AnonStorey32::<>m__2C(System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>)
extern "C"  EventHandler_1_t3942771990 * U3CStartU3Ec__AnonStorey32_U3CU3Em__2C_m1001324839 (U3CStartU3Ec__AnonStorey32_t1152509870 * __this, EventHandler_1_t3942771990 * ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample09_EventHandling/<Start>c__AnonStorey32::<>m__2D(System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>)
extern "C"  void U3CStartU3Ec__AnonStorey32_U3CU3Em__2D_m1340155791 (U3CStartU3Ec__AnonStorey32_t1152509870 * __this, EventHandler_1_t3942771990 * ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample09_EventHandling/<Start>c__AnonStorey32::<>m__2E(System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>)
extern "C"  void U3CStartU3Ec__AnonStorey32_U3CU3Em__2E_m2207208016 (U3CStartU3Ec__AnonStorey32_t1152509870 * __this, EventHandler_1_t3942771990 * ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
