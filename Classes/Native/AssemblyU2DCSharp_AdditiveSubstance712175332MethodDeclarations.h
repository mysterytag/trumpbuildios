﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AdditiveSubstance
struct AdditiveSubstance_t712175332;

#include "codegen/il2cpp-codegen.h"

// System.Void AdditiveSubstance::.ctor()
extern "C"  void AdditiveSubstance__ctor_m3862249223 (AdditiveSubstance_t712175332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdditiveSubstance::.ctor(System.Single)
extern "C"  void AdditiveSubstance__ctor_m486319620 (AdditiveSubstance_t712175332 * __this, float ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AdditiveSubstance::Result()
extern "C"  float AdditiveSubstance_Result_m4205888238 (AdditiveSubstance_t712175332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
