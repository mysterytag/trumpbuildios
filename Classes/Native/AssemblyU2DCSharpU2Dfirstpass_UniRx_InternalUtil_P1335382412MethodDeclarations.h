﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.PriorityQueue`1<System.Object>
struct PriorityQueue_1_t1335382412;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.InternalUtil.PriorityQueue`1<System.Object>::.ctor()
extern "C"  void PriorityQueue_1__ctor_m698167010_gshared (PriorityQueue_1_t1335382412 * __this, const MethodInfo* method);
#define PriorityQueue_1__ctor_m698167010(__this, method) ((  void (*) (PriorityQueue_1_t1335382412 *, const MethodInfo*))PriorityQueue_1__ctor_m698167010_gshared)(__this, method)
// System.Void UniRx.InternalUtil.PriorityQueue`1<System.Object>::.ctor(System.Int32)
extern "C"  void PriorityQueue_1__ctor_m3615596851_gshared (PriorityQueue_1_t1335382412 * __this, int32_t ___capacity0, const MethodInfo* method);
#define PriorityQueue_1__ctor_m3615596851(__this, ___capacity0, method) ((  void (*) (PriorityQueue_1_t1335382412 *, int32_t, const MethodInfo*))PriorityQueue_1__ctor_m3615596851_gshared)(__this, ___capacity0, method)
// System.Void UniRx.InternalUtil.PriorityQueue`1<System.Object>::.cctor()
extern "C"  void PriorityQueue_1__cctor_m3981211915_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define PriorityQueue_1__cctor_m3981211915(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))PriorityQueue_1__cctor_m3981211915_gshared)(__this /* static, unused */, method)
// System.Boolean UniRx.InternalUtil.PriorityQueue`1<System.Object>::IsHigherPriority(System.Int32,System.Int32)
extern "C"  bool PriorityQueue_1_IsHigherPriority_m999759587_gshared (PriorityQueue_1_t1335382412 * __this, int32_t ___left0, int32_t ___right1, const MethodInfo* method);
#define PriorityQueue_1_IsHigherPriority_m999759587(__this, ___left0, ___right1, method) ((  bool (*) (PriorityQueue_1_t1335382412 *, int32_t, int32_t, const MethodInfo*))PriorityQueue_1_IsHigherPriority_m999759587_gshared)(__this, ___left0, ___right1, method)
// System.Void UniRx.InternalUtil.PriorityQueue`1<System.Object>::Percolate(System.Int32)
extern "C"  void PriorityQueue_1_Percolate_m2937402368_gshared (PriorityQueue_1_t1335382412 * __this, int32_t ___index0, const MethodInfo* method);
#define PriorityQueue_1_Percolate_m2937402368(__this, ___index0, method) ((  void (*) (PriorityQueue_1_t1335382412 *, int32_t, const MethodInfo*))PriorityQueue_1_Percolate_m2937402368_gshared)(__this, ___index0, method)
// System.Void UniRx.InternalUtil.PriorityQueue`1<System.Object>::Heapify()
extern "C"  void PriorityQueue_1_Heapify_m704617936_gshared (PriorityQueue_1_t1335382412 * __this, const MethodInfo* method);
#define PriorityQueue_1_Heapify_m704617936(__this, method) ((  void (*) (PriorityQueue_1_t1335382412 *, const MethodInfo*))PriorityQueue_1_Heapify_m704617936_gshared)(__this, method)
// System.Void UniRx.InternalUtil.PriorityQueue`1<System.Object>::Heapify(System.Int32)
extern "C"  void PriorityQueue_1_Heapify_m1602165537_gshared (PriorityQueue_1_t1335382412 * __this, int32_t ___index0, const MethodInfo* method);
#define PriorityQueue_1_Heapify_m1602165537(__this, ___index0, method) ((  void (*) (PriorityQueue_1_t1335382412 *, int32_t, const MethodInfo*))PriorityQueue_1_Heapify_m1602165537_gshared)(__this, ___index0, method)
// System.Int32 UniRx.InternalUtil.PriorityQueue`1<System.Object>::get_Count()
extern "C"  int32_t PriorityQueue_1_get_Count_m1055643996_gshared (PriorityQueue_1_t1335382412 * __this, const MethodInfo* method);
#define PriorityQueue_1_get_Count_m1055643996(__this, method) ((  int32_t (*) (PriorityQueue_1_t1335382412 *, const MethodInfo*))PriorityQueue_1_get_Count_m1055643996_gshared)(__this, method)
// T UniRx.InternalUtil.PriorityQueue`1<System.Object>::Peek()
extern "C"  Il2CppObject * PriorityQueue_1_Peek_m2099666718_gshared (PriorityQueue_1_t1335382412 * __this, const MethodInfo* method);
#define PriorityQueue_1_Peek_m2099666718(__this, method) ((  Il2CppObject * (*) (PriorityQueue_1_t1335382412 *, const MethodInfo*))PriorityQueue_1_Peek_m2099666718_gshared)(__this, method)
// System.Void UniRx.InternalUtil.PriorityQueue`1<System.Object>::RemoveAt(System.Int32)
extern "C"  void PriorityQueue_1_RemoveAt_m2349195146_gshared (PriorityQueue_1_t1335382412 * __this, int32_t ___index0, const MethodInfo* method);
#define PriorityQueue_1_RemoveAt_m2349195146(__this, ___index0, method) ((  void (*) (PriorityQueue_1_t1335382412 *, int32_t, const MethodInfo*))PriorityQueue_1_RemoveAt_m2349195146_gshared)(__this, ___index0, method)
// T UniRx.InternalUtil.PriorityQueue`1<System.Object>::Dequeue()
extern "C"  Il2CppObject * PriorityQueue_1_Dequeue_m1887212399_gshared (PriorityQueue_1_t1335382412 * __this, const MethodInfo* method);
#define PriorityQueue_1_Dequeue_m1887212399(__this, method) ((  Il2CppObject * (*) (PriorityQueue_1_t1335382412 *, const MethodInfo*))PriorityQueue_1_Dequeue_m1887212399_gshared)(__this, method)
// System.Void UniRx.InternalUtil.PriorityQueue`1<System.Object>::Enqueue(T)
extern "C"  void PriorityQueue_1_Enqueue_m4027093526_gshared (PriorityQueue_1_t1335382412 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define PriorityQueue_1_Enqueue_m4027093526(__this, ___item0, method) ((  void (*) (PriorityQueue_1_t1335382412 *, Il2CppObject *, const MethodInfo*))PriorityQueue_1_Enqueue_m4027093526_gshared)(__this, ___item0, method)
// System.Boolean UniRx.InternalUtil.PriorityQueue`1<System.Object>::Remove(T)
extern "C"  bool PriorityQueue_1_Remove_m112164948_gshared (PriorityQueue_1_t1335382412 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define PriorityQueue_1_Remove_m112164948(__this, ___item0, method) ((  bool (*) (PriorityQueue_1_t1335382412 *, Il2CppObject *, const MethodInfo*))PriorityQueue_1_Remove_m112164948_gshared)(__this, ___item0, method)
