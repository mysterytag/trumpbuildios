﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.CreateObservable`1/Create<System.Single>
struct Create_t763504446;
// UniRx.IObserver`1<System.Single>
struct IObserver_1_t3170207924;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.CreateObservable`1/Create<System.Single>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Create__ctor_m3983802026_gshared (Create_t763504446 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define Create__ctor_m3983802026(__this, ___observer0, ___cancel1, method) ((  void (*) (Create_t763504446 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Create__ctor_m3983802026_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.CreateObservable`1/Create<System.Single>::OnNext(T)
extern "C"  void Create_OnNext_m2327185064_gshared (Create_t763504446 * __this, float ___value0, const MethodInfo* method);
#define Create_OnNext_m2327185064(__this, ___value0, method) ((  void (*) (Create_t763504446 *, float, const MethodInfo*))Create_OnNext_m2327185064_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.CreateObservable`1/Create<System.Single>::OnError(System.Exception)
extern "C"  void Create_OnError_m883413431_gshared (Create_t763504446 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Create_OnError_m883413431(__this, ___error0, method) ((  void (*) (Create_t763504446 *, Exception_t1967233988 *, const MethodInfo*))Create_OnError_m883413431_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.CreateObservable`1/Create<System.Single>::OnCompleted()
extern "C"  void Create_OnCompleted_m4069662602_gshared (Create_t763504446 * __this, const MethodInfo* method);
#define Create_OnCompleted_m4069662602(__this, method) ((  void (*) (Create_t763504446 *, const MethodInfo*))Create_OnCompleted_m4069662602_gshared)(__this, method)
