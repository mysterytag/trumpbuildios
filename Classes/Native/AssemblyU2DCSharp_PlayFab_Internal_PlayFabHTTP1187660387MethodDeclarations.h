﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.Internal.PlayFabHTTP
struct PlayFabHTTP_t1187660387;
// System.String
struct String_t;
// System.Action`1<PlayFab.CallRequestContainer>
struct Action_1_t580771314;
// System.Object
struct Il2CppObject;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t3432067208;
// System.Security.Cryptography.X509Certificates.X509Chain
struct X509Chain_t2831591730;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"
#include "System_System_Net_HttpStatusCode2736938801.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError2114648451.h"
#include "mscorlib_System_Security_Cryptography_X509Certific3432067208.h"
#include "System_System_Security_Cryptography_X509Certificat2831591730.h"
#include "System_System_Net_Security_SslPolicyErrors3085256601.h"

// System.Void PlayFab.Internal.PlayFabHTTP::.ctor()
extern "C"  void PlayFabHTTP__ctor_m1045590966 (PlayFabHTTP_t1187660387 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.Internal.PlayFabHTTP::.cctor()
extern "C"  void PlayFabHTTP__cctor_m1866452663 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.Internal.PlayFabHTTP::Awake()
extern "C"  void PlayFabHTTP_Awake_m1283196185 (PlayFabHTTP_t1187660387 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.Internal.PlayFabHTTP::Post(System.String,System.String,System.String,System.String,System.Action`1<PlayFab.CallRequestContainer>,System.Object,System.Object,System.Boolean)
extern "C"  void PlayFabHTTP_Post_m4134733674 (Il2CppObject * __this /* static, unused */, String_t* ___urlPath0, String_t* ___data1, String_t* ___authType2, String_t* ___authKey3, Action_1_t580771314 * ___callback4, Il2CppObject * ___request5, Il2CppObject * ___customData6, bool ___isBlocking7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.Internal.PlayFabHTTP::_ActivateWorkerThread()
extern "C"  void PlayFabHTTP__ActivateWorkerThread_m2564362030 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.Internal.PlayFabHTTP::_WorkerThreadMainLoop()
extern "C"  void PlayFabHTTP__WorkerThreadMainLoop_m1676048440 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.Internal.PlayFabHTTP::StartHttpWebRequest(PlayFab.CallRequestContainer)
extern "C"  void PlayFabHTTP_StartHttpWebRequest_m3455363758 (Il2CppObject * __this /* static, unused */, CallRequestContainer_t432318609 * ___request0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayFab.Internal.PlayFabHTTP::ProcessHttpWebResult(PlayFab.CallRequestContainer,System.Boolean)
extern "C"  bool PlayFabHTTP_ProcessHttpWebResult_m1169872908 (Il2CppObject * __this /* static, unused */, CallRequestContainer_t432318609 * ___request0, bool ___syncronous1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PlayFab.Internal.PlayFabHTTP::MakeRequestViaUnity(PlayFab.CallRequestContainer)
extern "C"  Il2CppObject * PlayFabHTTP_MakeRequestViaUnity_m699550665 (PlayFabHTTP_t1187660387 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.Internal.PlayFabHTTP::GetResonseCodeResult(System.Net.HttpStatusCode)
extern "C"  String_t* PlayFabHTTP_GetResonseCodeResult_m801157029 (Il2CppObject * __this /* static, unused */, int32_t ___code0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayFab.PlayFabError PlayFab.Internal.PlayFabHTTP::GeneratePfError(System.Net.HttpStatusCode,PlayFab.PlayFabErrorCode,System.String)
extern "C"  PlayFabError_t750598646 * PlayFabHTTP_GeneratePfError_m617745390 (Il2CppObject * __this /* static, unused */, int32_t ___httpCode0, int32_t ___pfErrorCode1, String_t* ___errorMessage2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.Internal.PlayFabHTTP::Update()
extern "C"  void PlayFabHTTP_Update_m4075410967 (PlayFabHTTP_t1187660387 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayFab.Internal.PlayFabHTTP::AcceptAllCertifications(System.Object,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Net.Security.SslPolicyErrors)
extern "C"  bool PlayFabHTTP_AcceptAllCertifications_m3806116065 (PlayFabHTTP_t1187660387 * __this, Il2CppObject * ___sender0, X509Certificate_t3432067208 * ___certificate1, X509Chain_t2831591730 * ___chain2, int32_t ___sslPolicyErrors3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlayFab.Internal.PlayFabHTTP::GetPendingMessages()
extern "C"  int32_t PlayFabHTTP_GetPendingMessages_m3999759657 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
