﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver/SkipUntil<System.Object,System.Object>
struct SkipUntil_t2756386132;
// UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver<System.Object,System.Object>
struct SkipUntilOuterObserver_t2866202317;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver/SkipUntil<System.Object,System.Object>::.ctor(UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver<T,TOther>,System.IDisposable)
extern "C"  void SkipUntil__ctor_m4128809165_gshared (SkipUntil_t2756386132 * __this, SkipUntilOuterObserver_t2866202317 * ___parent0, Il2CppObject * ___subscription1, const MethodInfo* method);
#define SkipUntil__ctor_m4128809165(__this, ___parent0, ___subscription1, method) ((  void (*) (SkipUntil_t2756386132 *, SkipUntilOuterObserver_t2866202317 *, Il2CppObject *, const MethodInfo*))SkipUntil__ctor_m4128809165_gshared)(__this, ___parent0, ___subscription1, method)
// System.Void UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver/SkipUntil<System.Object,System.Object>::OnNext(T)
extern "C"  void SkipUntil_OnNext_m294762985_gshared (SkipUntil_t2756386132 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define SkipUntil_OnNext_m294762985(__this, ___value0, method) ((  void (*) (SkipUntil_t2756386132 *, Il2CppObject *, const MethodInfo*))SkipUntil_OnNext_m294762985_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver/SkipUntil<System.Object,System.Object>::OnError(System.Exception)
extern "C"  void SkipUntil_OnError_m3873366776_gshared (SkipUntil_t2756386132 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define SkipUntil_OnError_m3873366776(__this, ___error0, method) ((  void (*) (SkipUntil_t2756386132 *, Exception_t1967233988 *, const MethodInfo*))SkipUntil_OnError_m3873366776_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SkipUntilObservable`2/SkipUntilOuterObserver/SkipUntil<System.Object,System.Object>::OnCompleted()
extern "C"  void SkipUntil_OnCompleted_m1911573067_gshared (SkipUntil_t2756386132 * __this, const MethodInfo* method);
#define SkipUntil_OnCompleted_m1911573067(__this, method) ((  void (*) (SkipUntil_t2756386132 *, const MethodInfo*))SkipUntil_OnCompleted_m1911573067_gshared)(__this, method)
