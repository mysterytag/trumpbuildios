﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.FirstObservable`1<System.Int64>
struct FirstObservable_1_t575002000;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3939730909.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.FirstObservable`1/First<System.Int64>
struct  First_t930318071  : public OperatorObserverBase_2_t3939730909
{
public:
	// UniRx.Operators.FirstObservable`1<T> UniRx.Operators.FirstObservable`1/First::parent
	FirstObservable_1_t575002000 * ___parent_2;
	// System.Boolean UniRx.Operators.FirstObservable`1/First::notPublished
	bool ___notPublished_3;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(First_t930318071, ___parent_2)); }
	inline FirstObservable_1_t575002000 * get_parent_2() const { return ___parent_2; }
	inline FirstObservable_1_t575002000 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(FirstObservable_1_t575002000 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_notPublished_3() { return static_cast<int32_t>(offsetof(First_t930318071, ___notPublished_3)); }
	inline bool get_notPublished_3() const { return ___notPublished_3; }
	inline bool* get_address_of_notPublished_3() { return &___notPublished_3; }
	inline void set_notPublished_3(bool value)
	{
		___notPublished_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
