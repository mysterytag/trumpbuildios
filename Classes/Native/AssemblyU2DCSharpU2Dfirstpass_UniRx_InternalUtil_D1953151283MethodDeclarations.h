﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.DisposedObserver`1<System.Byte>
struct DisposedObserver_1_t1953151283;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Byte>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m1410718175_gshared (DisposedObserver_1_t1953151283 * __this, const MethodInfo* method);
#define DisposedObserver_1__ctor_m1410718175(__this, method) ((  void (*) (DisposedObserver_1_t1953151283 *, const MethodInfo*))DisposedObserver_1__ctor_m1410718175_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Byte>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m300494254_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define DisposedObserver_1__cctor_m300494254(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))DisposedObserver_1__cctor_m300494254_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Byte>::OnCompleted()
extern "C"  void DisposedObserver_1_OnCompleted_m3725060649_gshared (DisposedObserver_1_t1953151283 * __this, const MethodInfo* method);
#define DisposedObserver_1_OnCompleted_m3725060649(__this, method) ((  void (*) (DisposedObserver_1_t1953151283 *, const MethodInfo*))DisposedObserver_1_OnCompleted_m3725060649_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Byte>::OnError(System.Exception)
extern "C"  void DisposedObserver_1_OnError_m522167766_gshared (DisposedObserver_1_t1953151283 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define DisposedObserver_1_OnError_m522167766(__this, ___error0, method) ((  void (*) (DisposedObserver_1_t1953151283 *, Exception_t1967233988 *, const MethodInfo*))DisposedObserver_1_OnError_m522167766_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<System.Byte>::OnNext(T)
extern "C"  void DisposedObserver_1_OnNext_m1321203911_gshared (DisposedObserver_1_t1953151283 * __this, uint8_t ___value0, const MethodInfo* method);
#define DisposedObserver_1_OnNext_m1321203911(__this, ___value0, method) ((  void (*) (DisposedObserver_1_t1953151283 *, uint8_t, const MethodInfo*))DisposedObserver_1_OnNext_m1321203911_gshared)(__this, ___value0, method)
