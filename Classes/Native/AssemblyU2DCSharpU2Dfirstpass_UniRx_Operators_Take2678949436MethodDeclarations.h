﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.TakeUntilObservable`2/TakeUntil<System.Object,System.Object>
struct TakeUntil_t2678949436;
// UniRx.Operators.TakeUntilObservable`2<System.Object,System.Object>
struct TakeUntilObservable_2_t4246198466;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.TakeUntilObservable`2/TakeUntil<System.Object,System.Object>::.ctor(UniRx.Operators.TakeUntilObservable`2<T,TOther>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void TakeUntil__ctor_m3722184177_gshared (TakeUntil_t2678949436 * __this, TakeUntilObservable_2_t4246198466 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define TakeUntil__ctor_m3722184177(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (TakeUntil_t2678949436 *, TakeUntilObservable_2_t4246198466 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))TakeUntil__ctor_m3722184177_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.TakeUntilObservable`2/TakeUntil<System.Object,System.Object>::Run()
extern "C"  Il2CppObject * TakeUntil_Run_m1825255312_gshared (TakeUntil_t2678949436 * __this, const MethodInfo* method);
#define TakeUntil_Run_m1825255312(__this, method) ((  Il2CppObject * (*) (TakeUntil_t2678949436 *, const MethodInfo*))TakeUntil_Run_m1825255312_gshared)(__this, method)
// System.Void UniRx.Operators.TakeUntilObservable`2/TakeUntil<System.Object,System.Object>::OnNext(T)
extern "C"  void TakeUntil_OnNext_m3312144426_gshared (TakeUntil_t2678949436 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define TakeUntil_OnNext_m3312144426(__this, ___value0, method) ((  void (*) (TakeUntil_t2678949436 *, Il2CppObject *, const MethodInfo*))TakeUntil_OnNext_m3312144426_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.TakeUntilObservable`2/TakeUntil<System.Object,System.Object>::OnError(System.Exception)
extern "C"  void TakeUntil_OnError_m3421361977_gshared (TakeUntil_t2678949436 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define TakeUntil_OnError_m3421361977(__this, ___error0, method) ((  void (*) (TakeUntil_t2678949436 *, Exception_t1967233988 *, const MethodInfo*))TakeUntil_OnError_m3421361977_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.TakeUntilObservable`2/TakeUntil<System.Object,System.Object>::OnCompleted()
extern "C"  void TakeUntil_OnCompleted_m3600996364_gshared (TakeUntil_t2678949436 * __this, const MethodInfo* method);
#define TakeUntil_OnCompleted_m3600996364(__this, method) ((  void (*) (TakeUntil_t2678949436 *, const MethodInfo*))TakeUntil_OnCompleted_m3600996364_gshared)(__this, method)
