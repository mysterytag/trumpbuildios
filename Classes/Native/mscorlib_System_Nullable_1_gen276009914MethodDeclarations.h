﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen276009914.h"
#include "AssemblyU2DCSharp_PlayFab_Internal_GMFB_327_testRe1684939302.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Nullable`1<PlayFab.Internal.GMFB_327/testRegion>::.ctor(T)
extern "C"  void Nullable_1__ctor_m3810241773_gshared (Nullable_1_t276009914 * __this, int32_t ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m3810241773(__this, ___value0, method) ((  void (*) (Nullable_1_t276009914 *, int32_t, const MethodInfo*))Nullable_1__ctor_m3810241773_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<PlayFab.Internal.GMFB_327/testRegion>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m1844179796_gshared (Nullable_1_t276009914 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m1844179796(__this, method) ((  bool (*) (Nullable_1_t276009914 *, const MethodInfo*))Nullable_1_get_HasValue_m1844179796_gshared)(__this, method)
// T System.Nullable`1<PlayFab.Internal.GMFB_327/testRegion>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m4040595498_gshared (Nullable_1_t276009914 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m4040595498(__this, method) ((  int32_t (*) (Nullable_1_t276009914 *, const MethodInfo*))Nullable_1_get_Value_m4040595498_gshared)(__this, method)
// System.Boolean System.Nullable`1<PlayFab.Internal.GMFB_327/testRegion>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3973453880_gshared (Nullable_1_t276009914 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m3973453880(__this, ___other0, method) ((  bool (*) (Nullable_1_t276009914 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m3973453880_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<PlayFab.Internal.GMFB_327/testRegion>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1063406599_gshared (Nullable_1_t276009914 * __this, Nullable_1_t276009914  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m1063406599(__this, ___other0, method) ((  bool (*) (Nullable_1_t276009914 *, Nullable_1_t276009914 , const MethodInfo*))Nullable_1_Equals_m1063406599_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<PlayFab.Internal.GMFB_327/testRegion>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m2481040272_gshared (Nullable_1_t276009914 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m2481040272(__this, method) ((  int32_t (*) (Nullable_1_t276009914 *, const MethodInfo*))Nullable_1_GetHashCode_m2481040272_gshared)(__this, method)
// T System.Nullable`1<PlayFab.Internal.GMFB_327/testRegion>::GetValueOrDefault()
extern "C"  int32_t Nullable_1_GetValueOrDefault_m3631035755_gshared (Nullable_1_t276009914 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m3631035755(__this, method) ((  int32_t (*) (Nullable_1_t276009914 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m3631035755_gshared)(__this, method)
// System.String System.Nullable`1<PlayFab.Internal.GMFB_327/testRegion>::ToString()
extern "C"  String_t* Nullable_1_ToString_m30750408_gshared (Nullable_1_t276009914 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m30750408(__this, method) ((  String_t* (*) (Nullable_1_t276009914 *, const MethodInfo*))Nullable_1_ToString_m30750408_gshared)(__this, method)
// System.Nullable`1<T> System.Nullable`1<PlayFab.Internal.GMFB_327/testRegion>::op_Implicit(T)
extern "C"  Nullable_1_t276009914  Nullable_1_op_Implicit_m934380354_gshared (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method);
#define Nullable_1_op_Implicit_m934380354(__this /* static, unused */, ___value0, method) ((  Nullable_1_t276009914  (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))Nullable_1_op_Implicit_m934380354_gshared)(__this /* static, unused */, ___value0, method)
