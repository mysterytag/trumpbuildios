﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.LinqExtensions/<ToEnumerable>c__Iterator3A`1<System.Object>
struct  U3CToEnumerableU3Ec__Iterator3A_1_t3575002786  : public Il2CppObject
{
public:
	// System.Collections.IEnumerator ModestTree.LinqExtensions/<ToEnumerable>c__Iterator3A`1::enumerator
	Il2CppObject * ___enumerator_0;
	// System.Int32 ModestTree.LinqExtensions/<ToEnumerable>c__Iterator3A`1::$PC
	int32_t ___U24PC_1;
	// T ModestTree.LinqExtensions/<ToEnumerable>c__Iterator3A`1::$current
	Il2CppObject * ___U24current_2;
	// System.Collections.IEnumerator ModestTree.LinqExtensions/<ToEnumerable>c__Iterator3A`1::<$>enumerator
	Il2CppObject * ___U3CU24U3Eenumerator_3;

public:
	inline static int32_t get_offset_of_enumerator_0() { return static_cast<int32_t>(offsetof(U3CToEnumerableU3Ec__Iterator3A_1_t3575002786, ___enumerator_0)); }
	inline Il2CppObject * get_enumerator_0() const { return ___enumerator_0; }
	inline Il2CppObject ** get_address_of_enumerator_0() { return &___enumerator_0; }
	inline void set_enumerator_0(Il2CppObject * value)
	{
		___enumerator_0 = value;
		Il2CppCodeGenWriteBarrier(&___enumerator_0, value);
	}

	inline static int32_t get_offset_of_U24PC_1() { return static_cast<int32_t>(offsetof(U3CToEnumerableU3Ec__Iterator3A_1_t3575002786, ___U24PC_1)); }
	inline int32_t get_U24PC_1() const { return ___U24PC_1; }
	inline int32_t* get_address_of_U24PC_1() { return &___U24PC_1; }
	inline void set_U24PC_1(int32_t value)
	{
		___U24PC_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CToEnumerableU3Ec__Iterator3A_1_t3575002786, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eenumerator_3() { return static_cast<int32_t>(offsetof(U3CToEnumerableU3Ec__Iterator3A_1_t3575002786, ___U3CU24U3Eenumerator_3)); }
	inline Il2CppObject * get_U3CU24U3Eenumerator_3() const { return ___U3CU24U3Eenumerator_3; }
	inline Il2CppObject ** get_address_of_U3CU24U3Eenumerator_3() { return &___U3CU24U3Eenumerator_3; }
	inline void set_U3CU24U3Eenumerator_3(Il2CppObject * value)
	{
		___U3CU24U3Eenumerator_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eenumerator_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
