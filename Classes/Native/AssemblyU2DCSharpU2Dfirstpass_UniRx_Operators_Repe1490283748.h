﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.RepeatUntilObservable`1<System.Object>
struct RepeatUntilObservable_1_t679086845;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<UniRx.IObservable`1<System.Object>>
struct IEnumerator_1_t2079011232;
// UniRx.SerialDisposable
struct SerialDisposable_t2547852742;
// UniRx.SingleAssignmentDisposable
struct SingleAssignmentDisposable_t2336378823;
// System.Action
struct Action_t437523947;
// System.IDisposable
struct IDisposable_t1628921374;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.RepeatUntilObservable`1/RepeatUntil<System.Object>
struct  RepeatUntil_t1490283748  : public OperatorObserverBase_2_t1187768149
{
public:
	// UniRx.Operators.RepeatUntilObservable`1<T> UniRx.Operators.RepeatUntilObservable`1/RepeatUntil::parent
	RepeatUntilObservable_1_t679086845 * ___parent_2;
	// System.Object UniRx.Operators.RepeatUntilObservable`1/RepeatUntil::gate
	Il2CppObject * ___gate_3;
	// System.Collections.Generic.IEnumerator`1<UniRx.IObservable`1<T>> UniRx.Operators.RepeatUntilObservable`1/RepeatUntil::e
	Il2CppObject* ___e_4;
	// UniRx.SerialDisposable UniRx.Operators.RepeatUntilObservable`1/RepeatUntil::subscription
	SerialDisposable_t2547852742 * ___subscription_5;
	// UniRx.SingleAssignmentDisposable UniRx.Operators.RepeatUntilObservable`1/RepeatUntil::schedule
	SingleAssignmentDisposable_t2336378823 * ___schedule_6;
	// System.Action UniRx.Operators.RepeatUntilObservable`1/RepeatUntil::nextSelf
	Action_t437523947 * ___nextSelf_7;
	// System.Boolean UniRx.Operators.RepeatUntilObservable`1/RepeatUntil::isStopped
	bool ___isStopped_8;
	// System.Boolean UniRx.Operators.RepeatUntilObservable`1/RepeatUntil::isDisposed
	bool ___isDisposed_9;
	// System.Boolean UniRx.Operators.RepeatUntilObservable`1/RepeatUntil::isFirstSubscribe
	bool ___isFirstSubscribe_10;
	// System.IDisposable UniRx.Operators.RepeatUntilObservable`1/RepeatUntil::stopper
	Il2CppObject * ___stopper_11;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(RepeatUntil_t1490283748, ___parent_2)); }
	inline RepeatUntilObservable_1_t679086845 * get_parent_2() const { return ___parent_2; }
	inline RepeatUntilObservable_1_t679086845 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(RepeatUntilObservable_1_t679086845 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_gate_3() { return static_cast<int32_t>(offsetof(RepeatUntil_t1490283748, ___gate_3)); }
	inline Il2CppObject * get_gate_3() const { return ___gate_3; }
	inline Il2CppObject ** get_address_of_gate_3() { return &___gate_3; }
	inline void set_gate_3(Il2CppObject * value)
	{
		___gate_3 = value;
		Il2CppCodeGenWriteBarrier(&___gate_3, value);
	}

	inline static int32_t get_offset_of_e_4() { return static_cast<int32_t>(offsetof(RepeatUntil_t1490283748, ___e_4)); }
	inline Il2CppObject* get_e_4() const { return ___e_4; }
	inline Il2CppObject** get_address_of_e_4() { return &___e_4; }
	inline void set_e_4(Il2CppObject* value)
	{
		___e_4 = value;
		Il2CppCodeGenWriteBarrier(&___e_4, value);
	}

	inline static int32_t get_offset_of_subscription_5() { return static_cast<int32_t>(offsetof(RepeatUntil_t1490283748, ___subscription_5)); }
	inline SerialDisposable_t2547852742 * get_subscription_5() const { return ___subscription_5; }
	inline SerialDisposable_t2547852742 ** get_address_of_subscription_5() { return &___subscription_5; }
	inline void set_subscription_5(SerialDisposable_t2547852742 * value)
	{
		___subscription_5 = value;
		Il2CppCodeGenWriteBarrier(&___subscription_5, value);
	}

	inline static int32_t get_offset_of_schedule_6() { return static_cast<int32_t>(offsetof(RepeatUntil_t1490283748, ___schedule_6)); }
	inline SingleAssignmentDisposable_t2336378823 * get_schedule_6() const { return ___schedule_6; }
	inline SingleAssignmentDisposable_t2336378823 ** get_address_of_schedule_6() { return &___schedule_6; }
	inline void set_schedule_6(SingleAssignmentDisposable_t2336378823 * value)
	{
		___schedule_6 = value;
		Il2CppCodeGenWriteBarrier(&___schedule_6, value);
	}

	inline static int32_t get_offset_of_nextSelf_7() { return static_cast<int32_t>(offsetof(RepeatUntil_t1490283748, ___nextSelf_7)); }
	inline Action_t437523947 * get_nextSelf_7() const { return ___nextSelf_7; }
	inline Action_t437523947 ** get_address_of_nextSelf_7() { return &___nextSelf_7; }
	inline void set_nextSelf_7(Action_t437523947 * value)
	{
		___nextSelf_7 = value;
		Il2CppCodeGenWriteBarrier(&___nextSelf_7, value);
	}

	inline static int32_t get_offset_of_isStopped_8() { return static_cast<int32_t>(offsetof(RepeatUntil_t1490283748, ___isStopped_8)); }
	inline bool get_isStopped_8() const { return ___isStopped_8; }
	inline bool* get_address_of_isStopped_8() { return &___isStopped_8; }
	inline void set_isStopped_8(bool value)
	{
		___isStopped_8 = value;
	}

	inline static int32_t get_offset_of_isDisposed_9() { return static_cast<int32_t>(offsetof(RepeatUntil_t1490283748, ___isDisposed_9)); }
	inline bool get_isDisposed_9() const { return ___isDisposed_9; }
	inline bool* get_address_of_isDisposed_9() { return &___isDisposed_9; }
	inline void set_isDisposed_9(bool value)
	{
		___isDisposed_9 = value;
	}

	inline static int32_t get_offset_of_isFirstSubscribe_10() { return static_cast<int32_t>(offsetof(RepeatUntil_t1490283748, ___isFirstSubscribe_10)); }
	inline bool get_isFirstSubscribe_10() const { return ___isFirstSubscribe_10; }
	inline bool* get_address_of_isFirstSubscribe_10() { return &___isFirstSubscribe_10; }
	inline void set_isFirstSubscribe_10(bool value)
	{
		___isFirstSubscribe_10 = value;
	}

	inline static int32_t get_offset_of_stopper_11() { return static_cast<int32_t>(offsetof(RepeatUntil_t1490283748, ___stopper_11)); }
	inline Il2CppObject * get_stopper_11() const { return ___stopper_11; }
	inline Il2CppObject ** get_address_of_stopper_11() { return &___stopper_11; }
	inline void set_stopper_11(Il2CppObject * value)
	{
		___stopper_11 = value;
		Il2CppCodeGenWriteBarrier(&___stopper_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
