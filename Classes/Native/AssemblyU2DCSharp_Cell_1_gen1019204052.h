﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Stream`1<System.Object>
struct Stream_1_t3823212738;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cell`1<System.Object>
struct  Cell_1_t1019204052  : public Il2CppObject
{
public:
	// Stream`1<T> Cell`1::update
	Stream_1_t3823212738 * ___update_0;
	// System.IDisposable Cell`1::inputStreamConnection
	Il2CppObject * ___inputStreamConnection_1;
	// T Cell`1::Value
	Il2CppObject * ___Value_2;
	// System.Boolean Cell`1::holdedValueIsCurrent
	bool ___holdedValueIsCurrent_3;
	// T Cell`1::holdedValue
	Il2CppObject * ___holdedValue_4;

public:
	inline static int32_t get_offset_of_update_0() { return static_cast<int32_t>(offsetof(Cell_1_t1019204052, ___update_0)); }
	inline Stream_1_t3823212738 * get_update_0() const { return ___update_0; }
	inline Stream_1_t3823212738 ** get_address_of_update_0() { return &___update_0; }
	inline void set_update_0(Stream_1_t3823212738 * value)
	{
		___update_0 = value;
		Il2CppCodeGenWriteBarrier(&___update_0, value);
	}

	inline static int32_t get_offset_of_inputStreamConnection_1() { return static_cast<int32_t>(offsetof(Cell_1_t1019204052, ___inputStreamConnection_1)); }
	inline Il2CppObject * get_inputStreamConnection_1() const { return ___inputStreamConnection_1; }
	inline Il2CppObject ** get_address_of_inputStreamConnection_1() { return &___inputStreamConnection_1; }
	inline void set_inputStreamConnection_1(Il2CppObject * value)
	{
		___inputStreamConnection_1 = value;
		Il2CppCodeGenWriteBarrier(&___inputStreamConnection_1, value);
	}

	inline static int32_t get_offset_of_Value_2() { return static_cast<int32_t>(offsetof(Cell_1_t1019204052, ___Value_2)); }
	inline Il2CppObject * get_Value_2() const { return ___Value_2; }
	inline Il2CppObject ** get_address_of_Value_2() { return &___Value_2; }
	inline void set_Value_2(Il2CppObject * value)
	{
		___Value_2 = value;
		Il2CppCodeGenWriteBarrier(&___Value_2, value);
	}

	inline static int32_t get_offset_of_holdedValueIsCurrent_3() { return static_cast<int32_t>(offsetof(Cell_1_t1019204052, ___holdedValueIsCurrent_3)); }
	inline bool get_holdedValueIsCurrent_3() const { return ___holdedValueIsCurrent_3; }
	inline bool* get_address_of_holdedValueIsCurrent_3() { return &___holdedValueIsCurrent_3; }
	inline void set_holdedValueIsCurrent_3(bool value)
	{
		___holdedValueIsCurrent_3 = value;
	}

	inline static int32_t get_offset_of_holdedValue_4() { return static_cast<int32_t>(offsetof(Cell_1_t1019204052, ___holdedValue_4)); }
	inline Il2CppObject * get_holdedValue_4() const { return ___holdedValue_4; }
	inline Il2CppObject ** get_address_of_holdedValue_4() { return &___holdedValue_4; }
	inline void set_holdedValue_4(Il2CppObject * value)
	{
		___holdedValue_4 = value;
		Il2CppCodeGenWriteBarrier(&___holdedValue_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
