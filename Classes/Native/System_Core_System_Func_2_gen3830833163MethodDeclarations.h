﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen2135783352MethodDeclarations.h"

// System.Void System.Func`2<System.Action`1<UnityEngine.Vector2>,System.Object>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m2354685552(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t3830833163 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m3944524044_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<System.Action`1<UnityEngine.Vector2>,System.Object>::Invoke(T)
#define Func_2_Invoke_m266199670(__this, ___arg10, method) ((  Il2CppObject * (*) (Func_2_t3830833163 *, Action_1_t3673782493 *, const MethodInfo*))Func_2_Invoke_m1924616534_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<System.Action`1<UnityEngine.Vector2>,System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m1702367273(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t3830833163 *, Action_1_t3673782493 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m4281703301_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<System.Action`1<UnityEngine.Vector2>,System.Object>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m3145443102(__this, ___result0, method) ((  Il2CppObject * (*) (Func_2_t3830833163 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m4118168638_gshared)(__this, ___result0, method)
