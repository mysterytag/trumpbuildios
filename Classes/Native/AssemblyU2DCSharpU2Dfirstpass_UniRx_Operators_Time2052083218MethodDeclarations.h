﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.TimeoutObservable`1/Timeout<System.Object>
struct Timeout_t2052083218;
// UniRx.Operators.TimeoutObservable`1<System.Object>
struct TimeoutObservable_1_t1895307051;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.TimeoutObservable`1/Timeout<System.Object>::.ctor(UniRx.Operators.TimeoutObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Timeout__ctor_m3836546079_gshared (Timeout_t2052083218 * __this, TimeoutObservable_1_t1895307051 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Timeout__ctor_m3836546079(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Timeout_t2052083218 *, TimeoutObservable_1_t1895307051 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Timeout__ctor_m3836546079_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.TimeoutObservable`1/Timeout<System.Object>::Run()
extern "C"  Il2CppObject * Timeout_Run_m3464196849_gshared (Timeout_t2052083218 * __this, const MethodInfo* method);
#define Timeout_Run_m3464196849(__this, method) ((  Il2CppObject * (*) (Timeout_t2052083218 *, const MethodInfo*))Timeout_Run_m3464196849_gshared)(__this, method)
// System.IDisposable UniRx.Operators.TimeoutObservable`1/Timeout<System.Object>::RunTimer(System.UInt64)
extern "C"  Il2CppObject * Timeout_RunTimer_m579882309_gshared (Timeout_t2052083218 * __this, uint64_t ___timerId0, const MethodInfo* method);
#define Timeout_RunTimer_m579882309(__this, ___timerId0, method) ((  Il2CppObject * (*) (Timeout_t2052083218 *, uint64_t, const MethodInfo*))Timeout_RunTimer_m579882309_gshared)(__this, ___timerId0, method)
// System.Void UniRx.Operators.TimeoutObservable`1/Timeout<System.Object>::OnNext(T)
extern "C"  void Timeout_OnNext_m1526971339_gshared (Timeout_t2052083218 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Timeout_OnNext_m1526971339(__this, ___value0, method) ((  void (*) (Timeout_t2052083218 *, Il2CppObject *, const MethodInfo*))Timeout_OnNext_m1526971339_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.TimeoutObservable`1/Timeout<System.Object>::OnError(System.Exception)
extern "C"  void Timeout_OnError_m3961839834_gshared (Timeout_t2052083218 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Timeout_OnError_m3961839834(__this, ___error0, method) ((  void (*) (Timeout_t2052083218 *, Exception_t1967233988 *, const MethodInfo*))Timeout_OnError_m3961839834_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.TimeoutObservable`1/Timeout<System.Object>::OnCompleted()
extern "C"  void Timeout_OnCompleted_m3437923117_gshared (Timeout_t2052083218 * __this, const MethodInfo* method);
#define Timeout_OnCompleted_m3437923117(__this, method) ((  void (*) (Timeout_t2052083218 *, const MethodInfo*))Timeout_OnCompleted_m3437923117_gshared)(__this, method)
