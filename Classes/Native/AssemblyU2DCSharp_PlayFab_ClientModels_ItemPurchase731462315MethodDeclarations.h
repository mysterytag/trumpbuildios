﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.ItemPurchaseRequest
struct ItemPurchaseRequest_t731462315;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.ItemPurchaseRequest::.ctor()
extern "C"  void ItemPurchaseRequest__ctor_m3416904286 (ItemPurchaseRequest_t731462315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.ItemPurchaseRequest::get_ItemId()
extern "C"  String_t* ItemPurchaseRequest_get_ItemId_m1987968032 (ItemPurchaseRequest_t731462315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ItemPurchaseRequest::set_ItemId(System.String)
extern "C"  void ItemPurchaseRequest_set_ItemId_m3708316657 (ItemPurchaseRequest_t731462315 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 PlayFab.ClientModels.ItemPurchaseRequest::get_Quantity()
extern "C"  uint32_t ItemPurchaseRequest_get_Quantity_m916788117 (ItemPurchaseRequest_t731462315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ItemPurchaseRequest::set_Quantity(System.UInt32)
extern "C"  void ItemPurchaseRequest_set_Quantity_m2237283276 (ItemPurchaseRequest_t731462315 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.ItemPurchaseRequest::get_Annotation()
extern "C"  String_t* ItemPurchaseRequest_get_Annotation_m782337793 (ItemPurchaseRequest_t731462315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ItemPurchaseRequest::set_Annotation(System.String)
extern "C"  void ItemPurchaseRequest_set_Annotation_m3089381168 (ItemPurchaseRequest_t731462315 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.ItemPurchaseRequest::get_UpgradeFromItems()
extern "C"  List_1_t1765447871 * ItemPurchaseRequest_get_UpgradeFromItems_m3305117594 (ItemPurchaseRequest_t731462315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ItemPurchaseRequest::set_UpgradeFromItems(System.Collections.Generic.List`1<System.String>)
extern "C"  void ItemPurchaseRequest_set_UpgradeFromItems_m1540072273 (ItemPurchaseRequest_t731462315 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
