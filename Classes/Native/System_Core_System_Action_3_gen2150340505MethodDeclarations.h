﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_3_gen834096159MethodDeclarations.h"

// System.Void System.Action`3<System.String,System.String,System.String>::.ctor(System.Object,System.IntPtr)
#define Action_3__ctor_m578139649(__this, ___object0, ___method1, method) ((  void (*) (Action_3_t2150340505 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_3__ctor_m22279495_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`3<System.String,System.String,System.String>::Invoke(T1,T2,T3)
#define Action_3_Invoke_m1741884151(__this, ___arg10, ___arg21, ___arg32, method) ((  void (*) (Action_3_t2150340505 *, String_t*, String_t*, String_t*, const MethodInfo*))Action_3_Invoke_m3949892547_gshared)(__this, ___arg10, ___arg21, ___arg32, method)
// System.IAsyncResult System.Action`3<System.String,System.String,System.String>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
#define Action_3_BeginInvoke_m2051299140(__this, ___arg10, ___arg21, ___arg32, ___callback3, ___object4, method) ((  Il2CppObject * (*) (Action_3_t2150340505 *, String_t*, String_t*, String_t*, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Action_3_BeginInvoke_m1386617082_gshared)(__this, ___arg10, ___arg21, ___arg32, ___callback3, ___object4, method)
// System.Void System.Action`3<System.String,System.String,System.String>::EndInvoke(System.IAsyncResult)
#define Action_3_EndInvoke_m929933729(__this, ___result0, method) ((  void (*) (Action_3_t2150340505 *, Il2CppObject *, const MethodInfo*))Action_3_EndInvoke_m3792380631_gshared)(__this, ___result0, method)
