﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>
struct DOGetter_1_t219589798;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "UnityEngine_UnityEngine_Rect1525428817.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m3776501594_gshared (DOGetter_1_t219589798 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define DOGetter_1__ctor_m3776501594(__this, ___object0, ___method1, method) ((  void (*) (DOGetter_1_t219589798 *, Il2CppObject *, IntPtr_t, const MethodInfo*))DOGetter_1__ctor_m3776501594_gshared)(__this, ___object0, ___method1, method)
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>::Invoke()
extern "C"  Rect_t1525428817  DOGetter_1_Invoke_m2364178517_gshared (DOGetter_1_t219589798 * __this, const MethodInfo* method);
#define DOGetter_1_Invoke_m2364178517(__this, method) ((  Rect_t1525428817  (*) (DOGetter_1_t219589798 *, const MethodInfo*))DOGetter_1_Invoke_m2364178517_gshared)(__this, method)
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m343051223_gshared (DOGetter_1_t219589798 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method);
#define DOGetter_1_BeginInvoke_m343051223(__this, ___callback0, ___object1, method) ((  Il2CppObject * (*) (DOGetter_1_t219589798 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))DOGetter_1_BeginInvoke_m343051223_gshared)(__this, ___callback0, ___object1, method)
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>::EndInvoke(System.IAsyncResult)
extern "C"  Rect_t1525428817  DOGetter_1_EndInvoke_m1360414667_gshared (DOGetter_1_t219589798 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define DOGetter_1_EndInvoke_m1360414667(__this, ___result0, method) ((  Rect_t1525428817  (*) (DOGetter_1_t219589798 *, Il2CppObject *, const MethodInfo*))DOGetter_1_EndInvoke_m1360414667_gshared)(__this, ___result0, method)
