﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Reactor`1/Subscription<System.Single>
struct Subscription_t3032422840;

#include "codegen/il2cpp-codegen.h"

// System.Void Reactor`1/Subscription<System.Single>::.ctor()
extern "C"  void Subscription__ctor_m3711011665_gshared (Subscription_t3032422840 * __this, const MethodInfo* method);
#define Subscription__ctor_m3711011665(__this, method) ((  void (*) (Subscription_t3032422840 *, const MethodInfo*))Subscription__ctor_m3711011665_gshared)(__this, method)
// System.Void Reactor`1/Subscription<System.Single>::Dispose()
extern "C"  void Subscription_Dispose_m1357637454_gshared (Subscription_t3032422840 * __this, const MethodInfo* method);
#define Subscription_Dispose_m1357637454(__this, method) ((  void (*) (Subscription_t3032422840 *, const MethodInfo*))Subscription_Dispose_m1357637454_gshared)(__this, method)
