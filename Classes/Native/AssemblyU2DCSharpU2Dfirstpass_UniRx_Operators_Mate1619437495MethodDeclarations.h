﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.MaterializeObservable`1<System.Object>
struct MaterializeObservable_1_t1619437495;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.Notification`1<System.Object>>
struct IObserver_1_t2250355278;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.MaterializeObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>)
extern "C"  void MaterializeObservable_1__ctor_m2251900604_gshared (MaterializeObservable_1_t1619437495 * __this, Il2CppObject* ___source0, const MethodInfo* method);
#define MaterializeObservable_1__ctor_m2251900604(__this, ___source0, method) ((  void (*) (MaterializeObservable_1_t1619437495 *, Il2CppObject*, const MethodInfo*))MaterializeObservable_1__ctor_m2251900604_gshared)(__this, ___source0, method)
// System.IDisposable UniRx.Operators.MaterializeObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<UniRx.Notification`1<T>>,System.IDisposable)
extern "C"  Il2CppObject * MaterializeObservable_1_SubscribeCore_m3597847907_gshared (MaterializeObservable_1_t1619437495 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define MaterializeObservable_1_SubscribeCore_m3597847907(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (MaterializeObservable_1_t1619437495 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))MaterializeObservable_1_SubscribeCore_m3597847907_gshared)(__this, ___observer0, ___cancel1, method)
