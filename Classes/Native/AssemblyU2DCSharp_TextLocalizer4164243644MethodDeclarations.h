﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TextLocalizer
struct TextLocalizer_t4164243644;

#include "codegen/il2cpp-codegen.h"

// System.Void TextLocalizer::.ctor()
extern "C"  void TextLocalizer__ctor_m3010339887 (TextLocalizer_t4164243644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TextLocalizer::Awake()
extern "C"  void TextLocalizer_Awake_m3247945106 (TextLocalizer_t4164243644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
