﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AudioSource
struct AudioSource_t3628549054;
// UnityEngine.AudioClip[]
struct AudioClipU5BU5D_t2889538658;
// UnityEngine.AudioClip
struct AudioClip_t3714538611;
// GlobalStat
struct GlobalStat_t1134678199;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AudioController
struct  AudioController_t2316845042  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.AudioSource AudioController::BgMusicSource
	AudioSource_t3628549054 * ___BgMusicSource_2;
	// UnityEngine.AudioSource AudioController::BoostMusicSource
	AudioSource_t3628549054 * ___BoostMusicSource_3;
	// System.Single AudioController::speedLevel
	float ___speedLevel_4;
	// System.Single AudioController::duration
	float ___duration_5;
	// System.Single AudioController::targetVolumeLevel
	float ___targetVolumeLevel_6;
	// System.Single AudioController::start_change_time
	float ___start_change_time_7;
	// UnityEngine.AudioClip[] AudioController::wooshSounds
	AudioClipU5BU5D_t2889538658* ___wooshSounds_8;
	// UnityEngine.AudioClip AudioController::purchaseSound
	AudioClip_t3714538611 * ___purchaseSound_9;
	// UnityEngine.AudioClip AudioController::multiplierSound
	AudioClip_t3714538611 * ___multiplierSound_10;
	// UnityEngine.AudioClip AudioController::mmmSound
	AudioClip_t3714538611 * ___mmmSound_11;
	// UnityEngine.AudioClip AudioController::mmmCountdown60Sound
	AudioClip_t3714538611 * ___mmmCountdown60Sound_12;
	// UnityEngine.AudioClip AudioController::mmmCountdown5Sound
	AudioClip_t3714538611 * ___mmmCountdown5Sound_13;
	// GlobalStat AudioController::_globalStat
	GlobalStat_t1134678199 * ____globalStat_14;

public:
	inline static int32_t get_offset_of_BgMusicSource_2() { return static_cast<int32_t>(offsetof(AudioController_t2316845042, ___BgMusicSource_2)); }
	inline AudioSource_t3628549054 * get_BgMusicSource_2() const { return ___BgMusicSource_2; }
	inline AudioSource_t3628549054 ** get_address_of_BgMusicSource_2() { return &___BgMusicSource_2; }
	inline void set_BgMusicSource_2(AudioSource_t3628549054 * value)
	{
		___BgMusicSource_2 = value;
		Il2CppCodeGenWriteBarrier(&___BgMusicSource_2, value);
	}

	inline static int32_t get_offset_of_BoostMusicSource_3() { return static_cast<int32_t>(offsetof(AudioController_t2316845042, ___BoostMusicSource_3)); }
	inline AudioSource_t3628549054 * get_BoostMusicSource_3() const { return ___BoostMusicSource_3; }
	inline AudioSource_t3628549054 ** get_address_of_BoostMusicSource_3() { return &___BoostMusicSource_3; }
	inline void set_BoostMusicSource_3(AudioSource_t3628549054 * value)
	{
		___BoostMusicSource_3 = value;
		Il2CppCodeGenWriteBarrier(&___BoostMusicSource_3, value);
	}

	inline static int32_t get_offset_of_speedLevel_4() { return static_cast<int32_t>(offsetof(AudioController_t2316845042, ___speedLevel_4)); }
	inline float get_speedLevel_4() const { return ___speedLevel_4; }
	inline float* get_address_of_speedLevel_4() { return &___speedLevel_4; }
	inline void set_speedLevel_4(float value)
	{
		___speedLevel_4 = value;
	}

	inline static int32_t get_offset_of_duration_5() { return static_cast<int32_t>(offsetof(AudioController_t2316845042, ___duration_5)); }
	inline float get_duration_5() const { return ___duration_5; }
	inline float* get_address_of_duration_5() { return &___duration_5; }
	inline void set_duration_5(float value)
	{
		___duration_5 = value;
	}

	inline static int32_t get_offset_of_targetVolumeLevel_6() { return static_cast<int32_t>(offsetof(AudioController_t2316845042, ___targetVolumeLevel_6)); }
	inline float get_targetVolumeLevel_6() const { return ___targetVolumeLevel_6; }
	inline float* get_address_of_targetVolumeLevel_6() { return &___targetVolumeLevel_6; }
	inline void set_targetVolumeLevel_6(float value)
	{
		___targetVolumeLevel_6 = value;
	}

	inline static int32_t get_offset_of_start_change_time_7() { return static_cast<int32_t>(offsetof(AudioController_t2316845042, ___start_change_time_7)); }
	inline float get_start_change_time_7() const { return ___start_change_time_7; }
	inline float* get_address_of_start_change_time_7() { return &___start_change_time_7; }
	inline void set_start_change_time_7(float value)
	{
		___start_change_time_7 = value;
	}

	inline static int32_t get_offset_of_wooshSounds_8() { return static_cast<int32_t>(offsetof(AudioController_t2316845042, ___wooshSounds_8)); }
	inline AudioClipU5BU5D_t2889538658* get_wooshSounds_8() const { return ___wooshSounds_8; }
	inline AudioClipU5BU5D_t2889538658** get_address_of_wooshSounds_8() { return &___wooshSounds_8; }
	inline void set_wooshSounds_8(AudioClipU5BU5D_t2889538658* value)
	{
		___wooshSounds_8 = value;
		Il2CppCodeGenWriteBarrier(&___wooshSounds_8, value);
	}

	inline static int32_t get_offset_of_purchaseSound_9() { return static_cast<int32_t>(offsetof(AudioController_t2316845042, ___purchaseSound_9)); }
	inline AudioClip_t3714538611 * get_purchaseSound_9() const { return ___purchaseSound_9; }
	inline AudioClip_t3714538611 ** get_address_of_purchaseSound_9() { return &___purchaseSound_9; }
	inline void set_purchaseSound_9(AudioClip_t3714538611 * value)
	{
		___purchaseSound_9 = value;
		Il2CppCodeGenWriteBarrier(&___purchaseSound_9, value);
	}

	inline static int32_t get_offset_of_multiplierSound_10() { return static_cast<int32_t>(offsetof(AudioController_t2316845042, ___multiplierSound_10)); }
	inline AudioClip_t3714538611 * get_multiplierSound_10() const { return ___multiplierSound_10; }
	inline AudioClip_t3714538611 ** get_address_of_multiplierSound_10() { return &___multiplierSound_10; }
	inline void set_multiplierSound_10(AudioClip_t3714538611 * value)
	{
		___multiplierSound_10 = value;
		Il2CppCodeGenWriteBarrier(&___multiplierSound_10, value);
	}

	inline static int32_t get_offset_of_mmmSound_11() { return static_cast<int32_t>(offsetof(AudioController_t2316845042, ___mmmSound_11)); }
	inline AudioClip_t3714538611 * get_mmmSound_11() const { return ___mmmSound_11; }
	inline AudioClip_t3714538611 ** get_address_of_mmmSound_11() { return &___mmmSound_11; }
	inline void set_mmmSound_11(AudioClip_t3714538611 * value)
	{
		___mmmSound_11 = value;
		Il2CppCodeGenWriteBarrier(&___mmmSound_11, value);
	}

	inline static int32_t get_offset_of_mmmCountdown60Sound_12() { return static_cast<int32_t>(offsetof(AudioController_t2316845042, ___mmmCountdown60Sound_12)); }
	inline AudioClip_t3714538611 * get_mmmCountdown60Sound_12() const { return ___mmmCountdown60Sound_12; }
	inline AudioClip_t3714538611 ** get_address_of_mmmCountdown60Sound_12() { return &___mmmCountdown60Sound_12; }
	inline void set_mmmCountdown60Sound_12(AudioClip_t3714538611 * value)
	{
		___mmmCountdown60Sound_12 = value;
		Il2CppCodeGenWriteBarrier(&___mmmCountdown60Sound_12, value);
	}

	inline static int32_t get_offset_of_mmmCountdown5Sound_13() { return static_cast<int32_t>(offsetof(AudioController_t2316845042, ___mmmCountdown5Sound_13)); }
	inline AudioClip_t3714538611 * get_mmmCountdown5Sound_13() const { return ___mmmCountdown5Sound_13; }
	inline AudioClip_t3714538611 ** get_address_of_mmmCountdown5Sound_13() { return &___mmmCountdown5Sound_13; }
	inline void set_mmmCountdown5Sound_13(AudioClip_t3714538611 * value)
	{
		___mmmCountdown5Sound_13 = value;
		Il2CppCodeGenWriteBarrier(&___mmmCountdown5Sound_13, value);
	}

	inline static int32_t get_offset_of__globalStat_14() { return static_cast<int32_t>(offsetof(AudioController_t2316845042, ____globalStat_14)); }
	inline GlobalStat_t1134678199 * get__globalStat_14() const { return ____globalStat_14; }
	inline GlobalStat_t1134678199 ** get_address_of__globalStat_14() { return &____globalStat_14; }
	inline void set__globalStat_14(GlobalStat_t1134678199 * value)
	{
		____globalStat_14 = value;
		Il2CppCodeGenWriteBarrier(&____globalStat_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
