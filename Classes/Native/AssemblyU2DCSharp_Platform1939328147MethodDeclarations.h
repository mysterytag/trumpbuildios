﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RuntimePlatform1574985880.h"

// System.Boolean Platform::Is(UnityEngine.RuntimePlatform)
extern "C"  bool Platform_Is_m3845187556 (Il2CppObject * __this /* static, unused */, int32_t ___platform0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
