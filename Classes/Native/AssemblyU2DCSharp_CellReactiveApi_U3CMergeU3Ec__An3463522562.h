﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`3<System.Single,System.Object,System.Object>
struct Func_3_t2453810542;
// ICell`1<System.Single>
struct ICell_1_t2509839998;
// ICell`1<System.Object>
struct ICell_1_t2388737397;
// System.Func`1<System.Object>
struct Func_1_t1979887667;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CellReactiveApi/<Merge>c__AnonStorey112`3<System.Single,System.Object,System.Object>
struct  U3CMergeU3Ec__AnonStorey112_3_t3463522562  : public Il2CppObject
{
public:
	// System.Func`3<T,T2,T3> CellReactiveApi/<Merge>c__AnonStorey112`3::func
	Func_3_t2453810542 * ___func_0;
	// ICell`1<T> CellReactiveApi/<Merge>c__AnonStorey112`3::cell
	Il2CppObject* ___cell_1;
	// ICell`1<T2> CellReactiveApi/<Merge>c__AnonStorey112`3::cell2
	Il2CppObject* ___cell2_2;
	// System.Func`1<T3> CellReactiveApi/<Merge>c__AnonStorey112`3::curr
	Func_1_t1979887667 * ___curr_3;

public:
	inline static int32_t get_offset_of_func_0() { return static_cast<int32_t>(offsetof(U3CMergeU3Ec__AnonStorey112_3_t3463522562, ___func_0)); }
	inline Func_3_t2453810542 * get_func_0() const { return ___func_0; }
	inline Func_3_t2453810542 ** get_address_of_func_0() { return &___func_0; }
	inline void set_func_0(Func_3_t2453810542 * value)
	{
		___func_0 = value;
		Il2CppCodeGenWriteBarrier(&___func_0, value);
	}

	inline static int32_t get_offset_of_cell_1() { return static_cast<int32_t>(offsetof(U3CMergeU3Ec__AnonStorey112_3_t3463522562, ___cell_1)); }
	inline Il2CppObject* get_cell_1() const { return ___cell_1; }
	inline Il2CppObject** get_address_of_cell_1() { return &___cell_1; }
	inline void set_cell_1(Il2CppObject* value)
	{
		___cell_1 = value;
		Il2CppCodeGenWriteBarrier(&___cell_1, value);
	}

	inline static int32_t get_offset_of_cell2_2() { return static_cast<int32_t>(offsetof(U3CMergeU3Ec__AnonStorey112_3_t3463522562, ___cell2_2)); }
	inline Il2CppObject* get_cell2_2() const { return ___cell2_2; }
	inline Il2CppObject** get_address_of_cell2_2() { return &___cell2_2; }
	inline void set_cell2_2(Il2CppObject* value)
	{
		___cell2_2 = value;
		Il2CppCodeGenWriteBarrier(&___cell2_2, value);
	}

	inline static int32_t get_offset_of_curr_3() { return static_cast<int32_t>(offsetof(U3CMergeU3Ec__AnonStorey112_3_t3463522562, ___curr_3)); }
	inline Func_1_t1979887667 * get_curr_3() const { return ___curr_3; }
	inline Func_1_t1979887667 ** get_address_of_curr_3() { return &___curr_3; }
	inline void set_curr_3(Func_1_t1979887667 * value)
	{
		___curr_3 = value;
		Il2CppCodeGenWriteBarrier(&___curr_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
