﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen2106500283MethodDeclarations.h"

// System.Void UniRx.Tuple`2<UnityEngine.BitStream,UnityEngine.NetworkMessageInfo>::.ctor(T1,T2)
#define Tuple_2__ctor_m2382470942(__this, ___item10, ___item21, method) ((  void (*) (Tuple_2_t3276012861 *, BitStream_t3159523098 *, NetworkMessageInfo_t2574344884 , const MethodInfo*))Tuple_2__ctor_m771351398_gshared)(__this, ___item10, ___item21, method)
// System.Int32 UniRx.Tuple`2<UnityEngine.BitStream,UnityEngine.NetworkMessageInfo>::System.IComparable.CompareTo(System.Object)
#define Tuple_2_System_IComparable_CompareTo_m1900772247(__this, ___obj0, method) ((  int32_t (*) (Tuple_2_t3276012861 *, Il2CppObject *, const MethodInfo*))Tuple_2_System_IComparable_CompareTo_m3405317199_gshared)(__this, ___obj0, method)
// System.Int32 UniRx.Tuple`2<UnityEngine.BitStream,UnityEngine.NetworkMessageInfo>::UniRx.IStructuralComparable.CompareTo(System.Object,System.Collections.IComparer)
#define Tuple_2_UniRx_IStructuralComparable_CompareTo_m3266054953(__this, ___other0, ___comparer1, method) ((  int32_t (*) (Tuple_2_t3276012861 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_2_UniRx_IStructuralComparable_CompareTo_m1856326113_gshared)(__this, ___other0, ___comparer1, method)
// System.Boolean UniRx.Tuple`2<UnityEngine.BitStream,UnityEngine.NetworkMessageInfo>::UniRx.IStructuralEquatable.Equals(System.Object,System.Collections.IEqualityComparer)
#define Tuple_2_UniRx_IStructuralEquatable_Equals_m440838944(__this, ___other0, ___comparer1, method) ((  bool (*) (Tuple_2_t3276012861 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_2_UniRx_IStructuralEquatable_Equals_m1941656536_gshared)(__this, ___other0, ___comparer1, method)
// System.Int32 UniRx.Tuple`2<UnityEngine.BitStream,UnityEngine.NetworkMessageInfo>::UniRx.IStructuralEquatable.GetHashCode(System.Collections.IEqualityComparer)
#define Tuple_2_UniRx_IStructuralEquatable_GetHashCode_m3766355610(__this, ___comparer0, method) ((  int32_t (*) (Tuple_2_t3276012861 *, Il2CppObject *, const MethodInfo*))Tuple_2_UniRx_IStructuralEquatable_GetHashCode_m4285953250_gshared)(__this, ___comparer0, method)
// System.String UniRx.Tuple`2<UnityEngine.BitStream,UnityEngine.NetworkMessageInfo>::UniRx.ITuple.ToString()
#define Tuple_2_UniRx_ITuple_ToString_m1640867467(__this, method) ((  String_t* (*) (Tuple_2_t3276012861 *, const MethodInfo*))Tuple_2_UniRx_ITuple_ToString_m625326147_gshared)(__this, method)
// T1 UniRx.Tuple`2<UnityEngine.BitStream,UnityEngine.NetworkMessageInfo>::get_Item1()
#define Tuple_2_get_Item1_m1576413026(__this, method) ((  BitStream_t3159523098 * (*) (Tuple_2_t3276012861 *, const MethodInfo*))Tuple_2_get_Item1_m1367433754_gshared)(__this, method)
// T2 UniRx.Tuple`2<UnityEngine.BitStream,UnityEngine.NetworkMessageInfo>::get_Item2()
#define Tuple_2_get_Item2_m2359792962(__this, method) ((  NetworkMessageInfo_t2574344884  (*) (Tuple_2_t3276012861 *, const MethodInfo*))Tuple_2_get_Item2_m1811046138_gshared)(__this, method)
// System.Boolean UniRx.Tuple`2<UnityEngine.BitStream,UnityEngine.NetworkMessageInfo>::Equals(System.Object)
#define Tuple_2_Equals_m3144594900(__this, ___obj0, method) ((  bool (*) (Tuple_2_t3276012861 *, Il2CppObject *, const MethodInfo*))Tuple_2_Equals_m444931724_gshared)(__this, ___obj0, method)
// System.Int32 UniRx.Tuple`2<UnityEngine.BitStream,UnityEngine.NetworkMessageInfo>::GetHashCode()
#define Tuple_2_GetHashCode_m1863225708(__this, method) ((  int32_t (*) (Tuple_2_t3276012861 *, const MethodInfo*))Tuple_2_GetHashCode_m2471865380_gshared)(__this, method)
// System.String UniRx.Tuple`2<UnityEngine.BitStream,UnityEngine.NetworkMessageInfo>::ToString()
#define Tuple_2_ToString_m63205062(__this, method) ((  String_t* (*) (Tuple_2_t3276012861 *, const MethodInfo*))Tuple_2_ToString_m1660547598_gshared)(__this, method)
// System.Boolean UniRx.Tuple`2<UnityEngine.BitStream,UnityEngine.NetworkMessageInfo>::Equals(UniRx.Tuple`2<T1,T2>)
#define Tuple_2_Equals_m736964501(__this, ___other0, method) ((  bool (*) (Tuple_2_t3276012861 *, Tuple_2_t3276012861 , const MethodInfo*))Tuple_2_Equals_m3962177501_gshared)(__this, ___other0, method)
