﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// BarygaVkController/<InviteFriend>c__AnonStoreyE0
struct U3CInviteFriendU3Ec__AnonStoreyE0_t3048000439;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BarygaVkController/<InviteFriend>c__AnonStoreyE0/<InviteFriend>c__AnonStoreyE1
struct  U3CInviteFriendU3Ec__AnonStoreyE1_t3048000440  : public Il2CppObject
{
public:
	// System.Double BarygaVkController/<InviteFriend>c__AnonStoreyE0/<InviteFriend>c__AnonStoreyE1::reward
	double ___reward_0;
	// BarygaVkController/<InviteFriend>c__AnonStoreyE0 BarygaVkController/<InviteFriend>c__AnonStoreyE0/<InviteFriend>c__AnonStoreyE1::<>f__ref$224
	U3CInviteFriendU3Ec__AnonStoreyE0_t3048000439 * ___U3CU3Ef__refU24224_1;

public:
	inline static int32_t get_offset_of_reward_0() { return static_cast<int32_t>(offsetof(U3CInviteFriendU3Ec__AnonStoreyE1_t3048000440, ___reward_0)); }
	inline double get_reward_0() const { return ___reward_0; }
	inline double* get_address_of_reward_0() { return &___reward_0; }
	inline void set_reward_0(double value)
	{
		___reward_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24224_1() { return static_cast<int32_t>(offsetof(U3CInviteFriendU3Ec__AnonStoreyE1_t3048000440, ___U3CU3Ef__refU24224_1)); }
	inline U3CInviteFriendU3Ec__AnonStoreyE0_t3048000439 * get_U3CU3Ef__refU24224_1() const { return ___U3CU3Ef__refU24224_1; }
	inline U3CInviteFriendU3Ec__AnonStoreyE0_t3048000439 ** get_address_of_U3CU3Ef__refU24224_1() { return &___U3CU3Ef__refU24224_1; }
	inline void set_U3CU3Ef__refU24224_1(U3CInviteFriendU3Ec__AnonStoreyE0_t3048000439 * value)
	{
		___U3CU3Ef__refU24224_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24224_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
