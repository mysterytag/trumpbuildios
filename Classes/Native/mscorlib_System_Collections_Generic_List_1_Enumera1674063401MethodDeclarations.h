﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4014815677MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Zenject.InjectAttribute>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3947621715(__this, ___l0, method) ((  void (*) (Enumerator_t1674063401 *, List_1_t3588280409 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Zenject.InjectAttribute>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3007504159(__this, method) ((  void (*) (Enumerator_t1674063401 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Zenject.InjectAttribute>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1298322251(__this, method) ((  Il2CppObject * (*) (Enumerator_t1674063401 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Zenject.InjectAttribute>::Dispose()
#define Enumerator_Dispose_m1560199160(__this, method) ((  void (*) (Enumerator_t1674063401 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Zenject.InjectAttribute>::VerifyState()
#define Enumerator_VerifyState_m2824979249(__this, method) ((  void (*) (Enumerator_t1674063401 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Zenject.InjectAttribute>::MoveNext()
#define Enumerator_MoveNext_m4057771531(__this, method) ((  bool (*) (Enumerator_t1674063401 *, const MethodInfo*))Enumerator_MoveNext_m302222300_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Zenject.InjectAttribute>::get_Current()
#define Enumerator_get_Current_m4026116200(__this, method) ((  InjectAttribute_t2791321440 * (*) (Enumerator_t1674063401 *, const MethodInfo*))Enumerator_get_Current_m1113061044_gshared)(__this, method)
