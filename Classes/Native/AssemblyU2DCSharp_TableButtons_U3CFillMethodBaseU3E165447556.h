﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<DateTimeCell>
struct List_1_t1570757814;
// UpgradeButton
struct UpgradeButton_t1475467342;
// UpgradeCell
struct UpgradeCell_t4117746046;
// System.Action`3<UpgradeButton,UpgradeCell,System.Int32>
struct Action_3_t1751200134;
// TableButtons
struct TableButtons_t1868573683;

#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharp_GlobalStat_UpgradeType4118271830.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TableButtons/<FillMethodBase>c__AnonStorey15B
struct  U3CFillMethodBaseU3Ec__AnonStorey15B_t165447556  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<DateTimeCell> TableButtons/<FillMethodBase>c__AnonStorey15B::timerList
	List_1_t1570757814 * ___timerList_0;
	// UpgradeButton TableButtons/<FillMethodBase>c__AnonStorey15B::button
	UpgradeButton_t1475467342 * ___button_1;
	// UpgradeCell TableButtons/<FillMethodBase>c__AnonStorey15B::cell
	UpgradeCell_t4117746046 * ___cell_2;
	// GlobalStat/UpgradeType TableButtons/<FillMethodBase>c__AnonStorey15B::upgradeType
	int32_t ___upgradeType_3;
	// System.Action`3<UpgradeButton,UpgradeCell,System.Int32> TableButtons/<FillMethodBase>c__AnonStorey15B::fillWithlevel
	Action_3_t1751200134 * ___fillWithlevel_4;
	// TableButtons TableButtons/<FillMethodBase>c__AnonStorey15B::<>f__this
	TableButtons_t1868573683 * ___U3CU3Ef__this_5;

public:
	inline static int32_t get_offset_of_timerList_0() { return static_cast<int32_t>(offsetof(U3CFillMethodBaseU3Ec__AnonStorey15B_t165447556, ___timerList_0)); }
	inline List_1_t1570757814 * get_timerList_0() const { return ___timerList_0; }
	inline List_1_t1570757814 ** get_address_of_timerList_0() { return &___timerList_0; }
	inline void set_timerList_0(List_1_t1570757814 * value)
	{
		___timerList_0 = value;
		Il2CppCodeGenWriteBarrier(&___timerList_0, value);
	}

	inline static int32_t get_offset_of_button_1() { return static_cast<int32_t>(offsetof(U3CFillMethodBaseU3Ec__AnonStorey15B_t165447556, ___button_1)); }
	inline UpgradeButton_t1475467342 * get_button_1() const { return ___button_1; }
	inline UpgradeButton_t1475467342 ** get_address_of_button_1() { return &___button_1; }
	inline void set_button_1(UpgradeButton_t1475467342 * value)
	{
		___button_1 = value;
		Il2CppCodeGenWriteBarrier(&___button_1, value);
	}

	inline static int32_t get_offset_of_cell_2() { return static_cast<int32_t>(offsetof(U3CFillMethodBaseU3Ec__AnonStorey15B_t165447556, ___cell_2)); }
	inline UpgradeCell_t4117746046 * get_cell_2() const { return ___cell_2; }
	inline UpgradeCell_t4117746046 ** get_address_of_cell_2() { return &___cell_2; }
	inline void set_cell_2(UpgradeCell_t4117746046 * value)
	{
		___cell_2 = value;
		Il2CppCodeGenWriteBarrier(&___cell_2, value);
	}

	inline static int32_t get_offset_of_upgradeType_3() { return static_cast<int32_t>(offsetof(U3CFillMethodBaseU3Ec__AnonStorey15B_t165447556, ___upgradeType_3)); }
	inline int32_t get_upgradeType_3() const { return ___upgradeType_3; }
	inline int32_t* get_address_of_upgradeType_3() { return &___upgradeType_3; }
	inline void set_upgradeType_3(int32_t value)
	{
		___upgradeType_3 = value;
	}

	inline static int32_t get_offset_of_fillWithlevel_4() { return static_cast<int32_t>(offsetof(U3CFillMethodBaseU3Ec__AnonStorey15B_t165447556, ___fillWithlevel_4)); }
	inline Action_3_t1751200134 * get_fillWithlevel_4() const { return ___fillWithlevel_4; }
	inline Action_3_t1751200134 ** get_address_of_fillWithlevel_4() { return &___fillWithlevel_4; }
	inline void set_fillWithlevel_4(Action_3_t1751200134 * value)
	{
		___fillWithlevel_4 = value;
		Il2CppCodeGenWriteBarrier(&___fillWithlevel_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_5() { return static_cast<int32_t>(offsetof(U3CFillMethodBaseU3Ec__AnonStorey15B_t165447556, ___U3CU3Ef__this_5)); }
	inline TableButtons_t1868573683 * get_U3CU3Ef__this_5() const { return ___U3CU3Ef__this_5; }
	inline TableButtons_t1868573683 ** get_address_of_U3CU3Ef__this_5() { return &___U3CU3Ef__this_5; }
	inline void set_U3CU3Ef__this_5(TableButtons_t1868573683 * value)
	{
		___U3CU3Ef__this_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
