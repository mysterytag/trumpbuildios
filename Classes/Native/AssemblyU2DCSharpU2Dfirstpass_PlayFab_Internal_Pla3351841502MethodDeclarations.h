﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.Internal.PlayFabPluginEventHandler
struct PlayFabPluginEventHandler_t3351841502;
// System.String
struct String_t;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.Internal.PlayFabPluginEventHandler::.ctor()
extern "C"  void PlayFabPluginEventHandler__ctor_m1745091279 (PlayFabPluginEventHandler_t3351841502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.Internal.PlayFabPluginEventHandler::.cctor()
extern "C"  void PlayFabPluginEventHandler__cctor_m2076125886 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.Internal.PlayFabPluginEventHandler::Init()
extern "C"  void PlayFabPluginEventHandler_Init_m1116208549 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.Internal.PlayFabPluginEventHandler::GCMRegistrationReady(System.String)
extern "C"  void PlayFabPluginEventHandler_GCMRegistrationReady_m705651220 (PlayFabPluginEventHandler_t3351841502 * __this, String_t* ___status0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.Internal.PlayFabPluginEventHandler::GCMRegistered(System.String)
extern "C"  void PlayFabPluginEventHandler_GCMRegistered_m2666134370 (PlayFabPluginEventHandler_t3351841502 * __this, String_t* ___token0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.Internal.PlayFabPluginEventHandler::GCMRegisterError(System.String)
extern "C"  void PlayFabPluginEventHandler_GCMRegisterError_m3763254905 (PlayFabPluginEventHandler_t3351841502 * __this, String_t* ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.Internal.PlayFabPluginEventHandler::GCMMessageReceived(System.String)
extern "C"  void PlayFabPluginEventHandler_GCMMessageReceived_m2076682998 (PlayFabPluginEventHandler_t3351841502 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.Internal.PlayFabPluginEventHandler::AddHttpDelegate(PlayFab.CallRequestContainer)
extern "C"  void PlayFabPluginEventHandler_AddHttpDelegate_m591552496 (Il2CppObject * __this /* static, unused */, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.Internal.PlayFabPluginEventHandler::OnHttpError(System.String)
extern "C"  void PlayFabPluginEventHandler_OnHttpError_m109204436 (PlayFabPluginEventHandler_t3351841502 * __this, String_t* ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.Internal.PlayFabPluginEventHandler::OnHttpResponse(System.String)
extern "C"  void PlayFabPluginEventHandler_OnHttpResponse_m1170359333 (PlayFabPluginEventHandler_t3351841502 * __this, String_t* ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
