﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.ListObserver`1<UnityEngine.Color>
struct ListObserver_1_t1882793431;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UnityEngine.Color>>
struct ImmutableList_1_t565411866;
// System.Exception
struct Exception_t1967233988;
// UniRx.IObserver`1<UnityEngine.Color>
struct IObserver_1_t3800174663;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"

// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Color>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m200844175_gshared (ListObserver_1_t1882793431 * __this, ImmutableList_1_t565411866 * ___observers0, const MethodInfo* method);
#define ListObserver_1__ctor_m200844175(__this, ___observers0, method) ((  void (*) (ListObserver_1_t1882793431 *, ImmutableList_1_t565411866 *, const MethodInfo*))ListObserver_1__ctor_m200844175_gshared)(__this, ___observers0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Color>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m949706631_gshared (ListObserver_1_t1882793431 * __this, const MethodInfo* method);
#define ListObserver_1_OnCompleted_m949706631(__this, method) ((  void (*) (ListObserver_1_t1882793431 *, const MethodInfo*))ListObserver_1_OnCompleted_m949706631_gshared)(__this, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Color>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m2873622068_gshared (ListObserver_1_t1882793431 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ListObserver_1_OnError_m2873622068(__this, ___error0, method) ((  void (*) (ListObserver_1_t1882793431 *, Exception_t1967233988 *, const MethodInfo*))ListObserver_1_OnError_m2873622068_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UnityEngine.Color>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m1890635045_gshared (ListObserver_1_t1882793431 * __this, Color_t1588175760  ___value0, const MethodInfo* method);
#define ListObserver_1_OnNext_m1890635045(__this, ___value0, method) ((  void (*) (ListObserver_1_t1882793431 *, Color_t1588175760 , const MethodInfo*))ListObserver_1_OnNext_m1890635045_gshared)(__this, ___value0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UnityEngine.Color>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m174569527_gshared (ListObserver_1_t1882793431 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Add_m174569527(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t1882793431 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Add_m174569527_gshared)(__this, ___observer0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UnityEngine.Color>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m4123796404_gshared (ListObserver_1_t1882793431 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Remove_m4123796404(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t1882793431 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Remove_m4123796404_gshared)(__this, ___observer0, method)
