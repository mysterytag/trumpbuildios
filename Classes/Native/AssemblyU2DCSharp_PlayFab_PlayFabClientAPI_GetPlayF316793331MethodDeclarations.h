﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetPlayFabIDsFromFacebookIDsResponseCallback
struct GetPlayFabIDsFromFacebookIDsResponseCallback_t316793331;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetPlayFabIDsFromFacebookIDsRequest
struct GetPlayFabIDsFromFacebookIDsRequest_t270576050;
// PlayFab.ClientModels.GetPlayFabIDsFromFacebookIDsResult
struct GetPlayFabIDsFromFacebookIDsResult_t3230464698;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayFabID270576050.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetPlayFabI3230464698.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetPlayFabIDsFromFacebookIDsResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetPlayFabIDsFromFacebookIDsResponseCallback__ctor_m2350603618 (GetPlayFabIDsFromFacebookIDsResponseCallback_t316793331 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetPlayFabIDsFromFacebookIDsResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetPlayFabIDsFromFacebookIDsRequest,PlayFab.ClientModels.GetPlayFabIDsFromFacebookIDsResult,PlayFab.PlayFabError,System.Object)
extern "C"  void GetPlayFabIDsFromFacebookIDsResponseCallback_Invoke_m4171092623 (GetPlayFabIDsFromFacebookIDsResponseCallback_t316793331 * __this, String_t* ___urlPath0, int32_t ___callId1, GetPlayFabIDsFromFacebookIDsRequest_t270576050 * ___request2, GetPlayFabIDsFromFacebookIDsResult_t3230464698 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetPlayFabIDsFromFacebookIDsResponseCallback_t316793331(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetPlayFabIDsFromFacebookIDsRequest_t270576050 * ___request2, GetPlayFabIDsFromFacebookIDsResult_t3230464698 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetPlayFabIDsFromFacebookIDsResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetPlayFabIDsFromFacebookIDsRequest,PlayFab.ClientModels.GetPlayFabIDsFromFacebookIDsResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetPlayFabIDsFromFacebookIDsResponseCallback_BeginInvoke_m4099514108 (GetPlayFabIDsFromFacebookIDsResponseCallback_t316793331 * __this, String_t* ___urlPath0, int32_t ___callId1, GetPlayFabIDsFromFacebookIDsRequest_t270576050 * ___request2, GetPlayFabIDsFromFacebookIDsResult_t3230464698 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetPlayFabIDsFromFacebookIDsResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetPlayFabIDsFromFacebookIDsResponseCallback_EndInvoke_m2498285682 (GetPlayFabIDsFromFacebookIDsResponseCallback_t316793331 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
