﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass15_0
struct U3CU3Ec__DisplayClass15_0_t2750077581;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"

// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass15_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass15_0__ctor_m1196872138 (U3CU3Ec__DisplayClass15_0_t2750077581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 DG.Tweening.ShortcutExtensions46/<>c__DisplayClass15_0::<DOAnchorPosY>b__0()
extern "C"  Vector2_t3525329788  U3CU3Ec__DisplayClass15_0_U3CDOAnchorPosYU3Eb__0_m3638483200 (U3CU3Ec__DisplayClass15_0_t2750077581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass15_0::<DOAnchorPosY>b__1(UnityEngine.Vector2)
extern "C"  void U3CU3Ec__DisplayClass15_0_U3CDOAnchorPosYU3Eb__1_m480464404 (U3CU3Ec__DisplayClass15_0_t2750077581 * __this, Vector2_t3525329788  ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
