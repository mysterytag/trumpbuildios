﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Scheduler/<Schedule>c__AnonStorey77
struct U3CScheduleU3Ec__AnonStorey77_t3913535388;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTimeOffset3712260035.h"

// System.Void UniRx.Scheduler/<Schedule>c__AnonStorey77::.ctor()
extern "C"  void U3CScheduleU3Ec__AnonStorey77__ctor_m1474168519 (U3CScheduleU3Ec__AnonStorey77_t3913535388 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Scheduler/<Schedule>c__AnonStorey77::<>m__99()
extern "C"  void U3CScheduleU3Ec__AnonStorey77_U3CU3Em__99_m1645026032 (U3CScheduleU3Ec__AnonStorey77_t3913535388 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Scheduler/<Schedule>c__AnonStorey77::<>m__9E(System.DateTimeOffset)
extern "C"  void U3CScheduleU3Ec__AnonStorey77_U3CU3Em__9E_m1317880361 (U3CScheduleU3Ec__AnonStorey77_t3913535388 * __this, DateTimeOffset_t3712260035  ___dt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
