﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/<RepeatInfinite>c__IteratorE`1<System.Object>
struct  U3CRepeatInfiniteU3Ec__IteratorE_1_t1925948945  : public Il2CppObject
{
public:
	// UniRx.IObservable`1<T> UniRx.Observable/<RepeatInfinite>c__IteratorE`1::source
	Il2CppObject* ___source_0;
	// System.Int32 UniRx.Observable/<RepeatInfinite>c__IteratorE`1::$PC
	int32_t ___U24PC_1;
	// UniRx.IObservable`1<T> UniRx.Observable/<RepeatInfinite>c__IteratorE`1::$current
	Il2CppObject* ___U24current_2;
	// UniRx.IObservable`1<T> UniRx.Observable/<RepeatInfinite>c__IteratorE`1::<$>source
	Il2CppObject* ___U3CU24U3Esource_3;

public:
	inline static int32_t get_offset_of_source_0() { return static_cast<int32_t>(offsetof(U3CRepeatInfiniteU3Ec__IteratorE_1_t1925948945, ___source_0)); }
	inline Il2CppObject* get_source_0() const { return ___source_0; }
	inline Il2CppObject** get_address_of_source_0() { return &___source_0; }
	inline void set_source_0(Il2CppObject* value)
	{
		___source_0 = value;
		Il2CppCodeGenWriteBarrier(&___source_0, value);
	}

	inline static int32_t get_offset_of_U24PC_1() { return static_cast<int32_t>(offsetof(U3CRepeatInfiniteU3Ec__IteratorE_1_t1925948945, ___U24PC_1)); }
	inline int32_t get_U24PC_1() const { return ___U24PC_1; }
	inline int32_t* get_address_of_U24PC_1() { return &___U24PC_1; }
	inline void set_U24PC_1(int32_t value)
	{
		___U24PC_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CRepeatInfiniteU3Ec__IteratorE_1_t1925948945, ___U24current_2)); }
	inline Il2CppObject* get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject* value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Esource_3() { return static_cast<int32_t>(offsetof(U3CRepeatInfiniteU3Ec__IteratorE_1_t1925948945, ___U3CU24U3Esource_3)); }
	inline Il2CppObject* get_U3CU24U3Esource_3() const { return ___U3CU24U3Esource_3; }
	inline Il2CppObject** get_address_of_U3CU24U3Esource_3() { return &___U3CU24U3Esource_3; }
	inline void set_U3CU24U3Esource_3(Il2CppObject* value)
	{
		___U3CU24U3Esource_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Esource_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
