﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/<WhenSatisfy>c__AnonStorey10C`1<System.Object>
struct U3CWhenSatisfyU3Ec__AnonStorey10C_1_t4279471744;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action
struct Action_t437523947;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"

// System.Void CellReactiveApi/<WhenSatisfy>c__AnonStorey10C`1<System.Object>::.ctor()
extern "C"  void U3CWhenSatisfyU3Ec__AnonStorey10C_1__ctor_m3173308558_gshared (U3CWhenSatisfyU3Ec__AnonStorey10C_1_t4279471744 * __this, const MethodInfo* method);
#define U3CWhenSatisfyU3Ec__AnonStorey10C_1__ctor_m3173308558(__this, method) ((  void (*) (U3CWhenSatisfyU3Ec__AnonStorey10C_1_t4279471744 *, const MethodInfo*))U3CWhenSatisfyU3Ec__AnonStorey10C_1__ctor_m3173308558_gshared)(__this, method)
// System.IDisposable CellReactiveApi/<WhenSatisfy>c__AnonStorey10C`1<System.Object>::<>m__17F(System.Action,Priority)
extern "C"  Il2CppObject * U3CWhenSatisfyU3Ec__AnonStorey10C_1_U3CU3Em__17F_m2871339331_gshared (U3CWhenSatisfyU3Ec__AnonStorey10C_1_t4279471744 * __this, Action_t437523947 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define U3CWhenSatisfyU3Ec__AnonStorey10C_1_U3CU3Em__17F_m2871339331(__this, ___reaction0, ___p1, method) ((  Il2CppObject * (*) (U3CWhenSatisfyU3Ec__AnonStorey10C_1_t4279471744 *, Action_t437523947 *, int32_t, const MethodInfo*))U3CWhenSatisfyU3Ec__AnonStorey10C_1_U3CU3Em__17F_m2871339331_gshared)(__this, ___reaction0, ___p1, method)
