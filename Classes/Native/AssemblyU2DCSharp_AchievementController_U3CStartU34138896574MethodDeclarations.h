﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AchievementController/<Start>c__Iterator7
struct U3CStartU3Ec__Iterator7_t4138896574;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void AchievementController/<Start>c__Iterator7::.ctor()
extern "C"  void U3CStartU3Ec__Iterator7__ctor_m883720689 (U3CStartU3Ec__Iterator7_t4138896574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AchievementController/<Start>c__Iterator7::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1855929409 (U3CStartU3Ec__Iterator7_t4138896574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AchievementController/<Start>c__Iterator7::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m338420181 (U3CStartU3Ec__Iterator7_t4138896574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AchievementController/<Start>c__Iterator7::MoveNext()
extern "C"  bool U3CStartU3Ec__Iterator7_MoveNext_m2412903267 (U3CStartU3Ec__Iterator7_t4138896574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AchievementController/<Start>c__Iterator7::Dispose()
extern "C"  void U3CStartU3Ec__Iterator7_Dispose_m3045307886 (U3CStartU3Ec__Iterator7_t4138896574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AchievementController/<Start>c__Iterator7::Reset()
extern "C"  void U3CStartU3Ec__Iterator7_Reset_m2825120926 (U3CStartU3Ec__Iterator7_t4138896574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AchievementController/<Start>c__Iterator7::<>m__C8(System.Boolean)
extern "C"  void U3CStartU3Ec__Iterator7_U3CU3Em__C8_m2274478790 (U3CStartU3Ec__Iterator7_t4138896574 * __this, bool ___ok0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
