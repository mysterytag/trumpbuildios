﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MetaProcessBase
struct MetaProcessBase_t2426506875;
// System.Collections.Generic.IEnumerable`1<IProcess>
struct IEnumerable_1_t2603424474;
// IProcess
struct IProcess_t4026237414;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;

#include "codegen/il2cpp-codegen.h"

// System.Void MetaProcessBase::.ctor()
extern "C"  void MetaProcessBase__ctor_m2192528208 (MetaProcessBase_t2426506875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MetaProcessBase::.ctor(System.Collections.Generic.IEnumerable`1<IProcess>)
extern "C"  void MetaProcessBase__ctor_m3869388257 (MetaProcessBase_t2426506875 * __this, Il2CppObject* ___coroutines0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MetaProcessBase::Add(IProcess)
extern "C"  void MetaProcessBase_Add_m2990930153 (MetaProcessBase_t2426506875 * __this, Il2CppObject * ___proc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MetaProcessBase::get_finished()
extern "C"  bool MetaProcessBase_get_finished_m3307854875 (MetaProcessBase_t2426506875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MetaProcessBase::Tick(System.Single)
extern "C"  void MetaProcessBase_Tick_m2990243578 (MetaProcessBase_t2426506875 * __this, float ___dt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MetaProcessBase::Dispose()
extern "C"  void MetaProcessBase_Dispose_m2383915917 (MetaProcessBase_t2426506875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MetaProcessBase::GetEnumerator()
extern "C"  Il2CppObject * MetaProcessBase_GetEnumerator_m2692735728 (MetaProcessBase_t2426506875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MetaProcessBase::<get_finished>m__1E0(IProcess)
extern "C"  bool MetaProcessBase_U3Cget_finishedU3Em__1E0_m485819824 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MetaProcessBase::<Dispose>m__1E2(IProcess)
extern "C"  void MetaProcessBase_U3CDisposeU3Em__1E2_m3847224724 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
