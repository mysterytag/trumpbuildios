﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetUserInventoryRequest
struct GetUserInventoryRequest_t2337033188;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.GetUserInventoryRequest::.ctor()
extern "C"  void GetUserInventoryRequest__ctor_m2839352901 (GetUserInventoryRequest_t2337033188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
