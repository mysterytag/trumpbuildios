﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen2611574664.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UserDataPer4020504052.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Nullable`1<PlayFab.ClientModels.UserDataPermission>::.ctor(T)
extern "C"  void Nullable_1__ctor_m4243540691_gshared (Nullable_1_t2611574664 * __this, int32_t ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m4243540691(__this, ___value0, method) ((  void (*) (Nullable_1_t2611574664 *, int32_t, const MethodInfo*))Nullable_1__ctor_m4243540691_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<PlayFab.ClientModels.UserDataPermission>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3483961915_gshared (Nullable_1_t2611574664 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m3483961915(__this, method) ((  bool (*) (Nullable_1_t2611574664 *, const MethodInfo*))Nullable_1_get_HasValue_m3483961915_gshared)(__this, method)
// T System.Nullable`1<PlayFab.ClientModels.UserDataPermission>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m4106592946_gshared (Nullable_1_t2611574664 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m4106592946(__this, method) ((  int32_t (*) (Nullable_1_t2611574664 *, const MethodInfo*))Nullable_1_get_Value_m4106592946_gshared)(__this, method)
// System.Boolean System.Nullable`1<PlayFab.ClientModels.UserDataPermission>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1771540442_gshared (Nullable_1_t2611574664 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m1771540442(__this, ___other0, method) ((  bool (*) (Nullable_1_t2611574664 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m1771540442_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<PlayFab.ClientModels.UserDataPermission>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2145081893_gshared (Nullable_1_t2611574664 * __this, Nullable_1_t2611574664  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m2145081893(__this, ___other0, method) ((  bool (*) (Nullable_1_t2611574664 *, Nullable_1_t2611574664 , const MethodInfo*))Nullable_1_Equals_m2145081893_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<PlayFab.ClientModels.UserDataPermission>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m2904351678_gshared (Nullable_1_t2611574664 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m2904351678(__this, method) ((  int32_t (*) (Nullable_1_t2611574664 *, const MethodInfo*))Nullable_1_GetHashCode_m2904351678_gshared)(__this, method)
// T System.Nullable`1<PlayFab.ClientModels.UserDataPermission>::GetValueOrDefault()
extern "C"  int32_t Nullable_1_GetValueOrDefault_m3408455949_gshared (Nullable_1_t2611574664 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m3408455949(__this, method) ((  int32_t (*) (Nullable_1_t2611574664 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m3408455949_gshared)(__this, method)
// System.String System.Nullable`1<PlayFab.ClientModels.UserDataPermission>::ToString()
extern "C"  String_t* Nullable_1_ToString_m3674318088_gshared (Nullable_1_t2611574664 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m3674318088(__this, method) ((  String_t* (*) (Nullable_1_t2611574664 *, const MethodInfo*))Nullable_1_ToString_m3674318088_gshared)(__this, method)
// System.Nullable`1<T> System.Nullable`1<PlayFab.ClientModels.UserDataPermission>::op_Implicit(T)
extern "C"  Nullable_1_t2611574664  Nullable_1_op_Implicit_m3511051080_gshared (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method);
#define Nullable_1_op_Implicit_m3511051080(__this /* static, unused */, ___value0, method) ((  Nullable_1_t2611574664  (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))Nullable_1_op_Implicit_m3511051080_gshared)(__this /* static, unused */, ___value0, method)
