﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XPath.ExprLE
struct ExprLE_t1813727051;
// System.Xml.XPath.Expression
struct Expression_t4217024437;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XPath_Expression4217024437.h"

// System.Void System.Xml.XPath.ExprLE::.ctor(System.Xml.XPath.Expression,System.Xml.XPath.Expression)
extern "C"  void ExprLE__ctor_m240811764 (ExprLE_t1813727051 * __this, Expression_t4217024437 * ___left0, Expression_t4217024437 * ___right1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XPath.ExprLE::get_Operator()
extern "C"  String_t* ExprLE_get_Operator_m3088536194 (ExprLE_t1813727051 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.ExprLE::Compare(System.Double,System.Double)
extern "C"  bool ExprLE_Compare_m3614803687 (ExprLE_t1813727051 * __this, double ___arg10, double ___arg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
