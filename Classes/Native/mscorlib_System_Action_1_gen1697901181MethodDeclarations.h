﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Action`1<GooglePlayGames.BasicApi.Nearby.ConnectionResponse>
struct Action_1_t1697901181;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_Nearby_1549448476.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Action`1<GooglePlayGames.BasicApi.Nearby.ConnectionResponse>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m2084931440_gshared (Action_1_t1697901181 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Action_1__ctor_m2084931440(__this, ___object0, ___method1, method) ((  void (*) (Action_1_t1697901181 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_1__ctor_m2084931440_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`1<GooglePlayGames.BasicApi.Nearby.ConnectionResponse>::Invoke(T)
extern "C"  void Action_1_Invoke_m1137087460_gshared (Action_1_t1697901181 * __this, ConnectionResponse_t1549448476  ___obj0, const MethodInfo* method);
#define Action_1_Invoke_m1137087460(__this, ___obj0, method) ((  void (*) (Action_1_t1697901181 *, ConnectionResponse_t1549448476 , const MethodInfo*))Action_1_Invoke_m1137087460_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Action`1<GooglePlayGames.BasicApi.Nearby.ConnectionResponse>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m2383279401_gshared (Action_1_t1697901181 * __this, ConnectionResponse_t1549448476  ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method);
#define Action_1_BeginInvoke_m2383279401(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Action_1_t1697901181 *, ConnectionResponse_t1549448476 , AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Action_1_BeginInvoke_m2383279401_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Void System.Action`1<GooglePlayGames.BasicApi.Nearby.ConnectionResponse>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m4091949440_gshared (Action_1_t1697901181 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Action_1_EndInvoke_m4091949440(__this, ___result0, method) ((  void (*) (Action_1_t1697901181 *, Il2CppObject *, const MethodInfo*))Action_1_EndInvoke_m4091949440_gshared)(__this, ___result0, method)
