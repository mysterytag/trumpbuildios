﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabGoogleCloudMessaging/GCMRegisterComplete
struct GCMRegisterComplete_t1540081165;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabGoogleCloudMessaging/GCMRegisterComplete::.ctor(System.Object,System.IntPtr)
extern "C"  void GCMRegisterComplete__ctor_m1214277705 (GCMRegisterComplete_t1540081165 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabGoogleCloudMessaging/GCMRegisterComplete::Invoke(System.String,System.String)
extern "C"  void GCMRegisterComplete_Invoke_m429156347 (GCMRegisterComplete_t1540081165 * __this, String_t* ___id0, String_t* ___error1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GCMRegisterComplete_t1540081165(Il2CppObject* delegate, String_t* ___id0, String_t* ___error1);
// System.IAsyncResult PlayFab.PlayFabGoogleCloudMessaging/GCMRegisterComplete::BeginInvoke(System.String,System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GCMRegisterComplete_BeginInvoke_m3689487568 (GCMRegisterComplete_t1540081165 * __this, String_t* ___id0, String_t* ___error1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabGoogleCloudMessaging/GCMRegisterComplete::EndInvoke(System.IAsyncResult)
extern "C"  void GCMRegisterComplete_EndInvoke_m2478663385 (GCMRegisterComplete_t1540081165 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
