﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.LoginResult
struct LoginResult_t2117559510;
// System.String
struct String_t;
// PlayFab.ClientModels.UserSettings
struct UserSettings_t8286526;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UserSettings8286526.h"
#include "mscorlib_System_Nullable_1_gen3225071844.h"

// System.Void PlayFab.ClientModels.LoginResult::.ctor()
extern "C"  void LoginResult__ctor_m712990355 (LoginResult_t2117559510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LoginResult::get_SessionTicket()
extern "C"  String_t* LoginResult_get_SessionTicket_m2264351559 (LoginResult_t2117559510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LoginResult::set_SessionTicket(System.String)
extern "C"  void LoginResult_set_SessionTicket_m593625132 (LoginResult_t2117559510 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LoginResult::get_PlayFabId()
extern "C"  String_t* LoginResult_get_PlayFabId_m88276147 (LoginResult_t2117559510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LoginResult::set_PlayFabId(System.String)
extern "C"  void LoginResult_set_PlayFabId_m1760372416 (LoginResult_t2117559510 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayFab.ClientModels.LoginResult::get_NewlyCreated()
extern "C"  bool LoginResult_get_NewlyCreated_m4259775817 (LoginResult_t2117559510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LoginResult::set_NewlyCreated(System.Boolean)
extern "C"  void LoginResult_set_NewlyCreated_m2859193280 (LoginResult_t2117559510 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayFab.ClientModels.UserSettings PlayFab.ClientModels.LoginResult::get_SettingsForUser()
extern "C"  UserSettings_t8286526 * LoginResult_get_SettingsForUser_m3876956114 (LoginResult_t2117559510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LoginResult::set_SettingsForUser(PlayFab.ClientModels.UserSettings)
extern "C"  void LoginResult_set_SettingsForUser_m3923847361 (LoginResult_t2117559510 * __this, UserSettings_t8286526 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.DateTime> PlayFab.ClientModels.LoginResult::get_LastLoginTime()
extern "C"  Nullable_1_t3225071844  LoginResult_get_LastLoginTime_m1397509722 (LoginResult_t2117559510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LoginResult::set_LastLoginTime(System.Nullable`1<System.DateTime>)
extern "C"  void LoginResult_set_LastLoginTime_m1617922437 (LoginResult_t2117559510 * __this, Nullable_1_t3225071844  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
