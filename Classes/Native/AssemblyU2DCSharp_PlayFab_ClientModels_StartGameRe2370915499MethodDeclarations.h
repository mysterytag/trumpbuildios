﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.StartGameRequest
struct StartGameRequest_t2370915499;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_Region1621303076.h"

// System.Void PlayFab.ClientModels.StartGameRequest::.ctor()
extern "C"  void StartGameRequest__ctor_m1950462962 (StartGameRequest_t2370915499 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.StartGameRequest::get_BuildVersion()
extern "C"  String_t* StartGameRequest_get_BuildVersion_m2982438114 (StartGameRequest_t2370915499 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.StartGameRequest::set_BuildVersion(System.String)
extern "C"  void StartGameRequest_set_BuildVersion_m3242811529 (StartGameRequest_t2370915499 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayFab.ClientModels.Region PlayFab.ClientModels.StartGameRequest::get_Region()
extern "C"  int32_t StartGameRequest_get_Region_m4228436782 (StartGameRequest_t2370915499 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.StartGameRequest::set_Region(PlayFab.ClientModels.Region)
extern "C"  void StartGameRequest_set_Region_m2009805117 (StartGameRequest_t2370915499 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.StartGameRequest::get_GameMode()
extern "C"  String_t* StartGameRequest_get_GameMode_m1061241805 (StartGameRequest_t2370915499 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.StartGameRequest::set_GameMode(System.String)
extern "C"  void StartGameRequest_set_GameMode_m989418494 (StartGameRequest_t2370915499 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.StartGameRequest::get_StatisticName()
extern "C"  String_t* StartGameRequest_get_StatisticName_m2258061637 (StartGameRequest_t2370915499 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.StartGameRequest::set_StatisticName(System.String)
extern "C"  void StartGameRequest_set_StatisticName_m981961684 (StartGameRequest_t2370915499 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.StartGameRequest::get_CharacterId()
extern "C"  String_t* StartGameRequest_get_CharacterId_m4262719342 (StartGameRequest_t2370915499 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.StartGameRequest::set_CharacterId(System.String)
extern "C"  void StartGameRequest_set_CharacterId_m329449995 (StartGameRequest_t2370915499 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.StartGameRequest::get_CustomCommandLineData()
extern "C"  String_t* StartGameRequest_get_CustomCommandLineData_m87016322 (StartGameRequest_t2370915499 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.StartGameRequest::set_CustomCommandLineData(System.String)
extern "C"  void StartGameRequest_set_CustomCommandLineData_m207123895 (StartGameRequest_t2370915499 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
