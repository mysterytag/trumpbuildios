﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Organism`2<System.Object,System.Object>
struct Organism_2_t948289219;
// System.Object
struct Il2CppObject;
// IOnceEmptyStream
struct IOnceEmptyStream_t172682851;
// System.IDisposable
struct IDisposable_t1628921374;
// IOrganism
struct IOrganism_t2580843579;
// IRoot
struct IRoot_t69970123;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void Organism`2<System.Object,System.Object>::.ctor()
extern "C"  void Organism_2__ctor_m4082911749_gshared (Organism_2_t948289219 * __this, const MethodInfo* method);
#define Organism_2__ctor_m4082911749(__this, method) ((  void (*) (Organism_2_t948289219 *, const MethodInfo*))Organism_2__ctor_m4082911749_gshared)(__this, method)
// TCarrier Organism`2<System.Object,System.Object>::get_carrier()
extern "C"  Il2CppObject * Organism_2_get_carrier_m4029902219_gshared (Organism_2_t948289219 * __this, const MethodInfo* method);
#define Organism_2_get_carrier_m4029902219(__this, method) ((  Il2CppObject * (*) (Organism_2_t948289219 *, const MethodInfo*))Organism_2_get_carrier_m4029902219_gshared)(__this, method)
// System.Void Organism`2<System.Object,System.Object>::set_carrier(TCarrier)
extern "C"  void Organism_2_set_carrier_m2958823066_gshared (Organism_2_t948289219 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Organism_2_set_carrier_m2958823066(__this, ___value0, method) ((  void (*) (Organism_2_t948289219 *, Il2CppObject *, const MethodInfo*))Organism_2_set_carrier_m2958823066_gshared)(__this, ___value0, method)
// TRoot Organism`2<System.Object,System.Object>::get_root()
extern "C"  Il2CppObject * Organism_2_get_root_m2383393421_gshared (Organism_2_t948289219 * __this, const MethodInfo* method);
#define Organism_2_get_root_m2383393421(__this, method) ((  Il2CppObject * (*) (Organism_2_t948289219 *, const MethodInfo*))Organism_2_get_root_m2383393421_gshared)(__this, method)
// System.Void Organism`2<System.Object,System.Object>::set_root(TRoot)
extern "C"  void Organism_2_set_root_m984770174_gshared (Organism_2_t948289219 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Organism_2_set_root_m984770174(__this, ___value0, method) ((  void (*) (Organism_2_t948289219 *, Il2CppObject *, const MethodInfo*))Organism_2_set_root_m984770174_gshared)(__this, ___value0, method)
// IOnceEmptyStream Organism`2<System.Object,System.Object>::get_fellAsleep()
extern "C"  Il2CppObject * Organism_2_get_fellAsleep_m2050669571_gshared (Organism_2_t948289219 * __this, const MethodInfo* method);
#define Organism_2_get_fellAsleep_m2050669571(__this, method) ((  Il2CppObject * (*) (Organism_2_t948289219 *, const MethodInfo*))Organism_2_get_fellAsleep_m2050669571_gshared)(__this, method)
// System.Void Organism`2<System.Object,System.Object>::DisposeWhenAsleep(System.IDisposable)
extern "C"  void Organism_2_DisposeWhenAsleep_m153634408_gshared (Organism_2_t948289219 * __this, Il2CppObject * ___disposable0, const MethodInfo* method);
#define Organism_2_DisposeWhenAsleep_m153634408(__this, ___disposable0, method) ((  void (*) (Organism_2_t948289219 *, Il2CppObject *, const MethodInfo*))Organism_2_DisposeWhenAsleep_m153634408_gshared)(__this, ___disposable0, method)
// System.Boolean Organism`2<System.Object,System.Object>::get_isAlive()
extern "C"  bool Organism_2_get_isAlive_m434369745_gshared (Organism_2_t948289219 * __this, const MethodInfo* method);
#define Organism_2_get_isAlive_m434369745(__this, method) ((  bool (*) (Organism_2_t948289219 *, const MethodInfo*))Organism_2_get_isAlive_m434369745_gshared)(__this, method)
// System.Boolean Organism`2<System.Object,System.Object>::get_primal()
extern "C"  bool Organism_2_get_primal_m1803875749_gshared (Organism_2_t948289219 * __this, const MethodInfo* method);
#define Organism_2_get_primal_m1803875749(__this, method) ((  bool (*) (Organism_2_t948289219 *, const MethodInfo*))Organism_2_get_primal_m1803875749_gshared)(__this, method)
// System.Void Organism`2<System.Object,System.Object>::Setup(IOrganism,IRoot)
extern "C"  void Organism_2_Setup_m3424294968_gshared (Organism_2_t948289219 * __this, Il2CppObject * ___c0, Il2CppObject * ___r1, const MethodInfo* method);
#define Organism_2_Setup_m3424294968(__this, ___c0, ___r1, method) ((  void (*) (Organism_2_t948289219 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Organism_2_Setup_m3424294968_gshared)(__this, ___c0, ___r1, method)
// System.Void Organism`2<System.Object,System.Object>::WakeUp()
extern "C"  void Organism_2_WakeUp_m2709259966_gshared (Organism_2_t948289219 * __this, const MethodInfo* method);
#define Organism_2_WakeUp_m2709259966(__this, method) ((  void (*) (Organism_2_t948289219 *, const MethodInfo*))Organism_2_WakeUp_m2709259966_gshared)(__this, method)
// System.Void Organism`2<System.Object,System.Object>::Spread()
extern "C"  void Organism_2_Spread_m661389266_gshared (Organism_2_t948289219 * __this, const MethodInfo* method);
#define Organism_2_Spread_m661389266(__this, method) ((  void (*) (Organism_2_t948289219 *, const MethodInfo*))Organism_2_Spread_m661389266_gshared)(__this, method)
// System.Void Organism`2<System.Object,System.Object>::Collect(System.IDisposable)
extern "C"  void Organism_2_Collect_m831016995_gshared (Organism_2_t948289219 * __this, Il2CppObject * ___tail0, const MethodInfo* method);
#define Organism_2_Collect_m831016995(__this, ___tail0, method) ((  void (*) (Organism_2_t948289219 *, Il2CppObject *, const MethodInfo*))Organism_2_Collect_m831016995_gshared)(__this, ___tail0, method)
// System.Void Organism`2<System.Object,System.Object>::set_collect(System.IDisposable)
extern "C"  void Organism_2_set_collect_m1070393574_gshared (Organism_2_t948289219 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Organism_2_set_collect_m1070393574(__this, ___value0, method) ((  void (*) (Organism_2_t948289219 *, Il2CppObject *, const MethodInfo*))Organism_2_set_collect_m1070393574_gshared)(__this, ___value0, method)
// System.Void Organism`2<System.Object,System.Object>::ToSleep()
extern "C"  void Organism_2_ToSleep_m2529287359_gshared (Organism_2_t948289219 * __this, const MethodInfo* method);
#define Organism_2_ToSleep_m2529287359(__this, method) ((  void (*) (Organism_2_t948289219 *, const MethodInfo*))Organism_2_ToSleep_m2529287359_gshared)(__this, method)
// System.Void Organism`2<System.Object,System.Object>::Put(IOrganism)
extern "C"  void Organism_2_Put_m1862734501_gshared (Organism_2_t948289219 * __this, Il2CppObject * ___organism0, const MethodInfo* method);
#define Organism_2_Put_m1862734501(__this, ___organism0, method) ((  void (*) (Organism_2_t948289219 *, Il2CppObject *, const MethodInfo*))Organism_2_Put_m1862734501_gshared)(__this, ___organism0, method)
// System.Void Organism`2<System.Object,System.Object>::Take(IOrganism)
extern "C"  void Organism_2_Take_m340054705_gshared (Organism_2_t948289219 * __this, Il2CppObject * ___organism0, const MethodInfo* method);
#define Organism_2_Take_m340054705(__this, ___organism0, method) ((  void (*) (Organism_2_t948289219 *, Il2CppObject *, const MethodInfo*))Organism_2_Take_m340054705_gshared)(__this, ___organism0, method)
// IOrganism Organism`2<System.Object,System.Object>::ChangeRoot(TRoot)
extern "C"  Il2CppObject * Organism_2_ChangeRoot_m2085091299_gshared (Organism_2_t948289219 * __this, Il2CppObject * ___r0, const MethodInfo* method);
#define Organism_2_ChangeRoot_m2085091299(__this, ___r0, method) ((  Il2CppObject * (*) (Organism_2_t948289219 *, Il2CppObject *, const MethodInfo*))Organism_2_ChangeRoot_m2085091299_gshared)(__this, ___r0, method)
// System.Void Organism`2<System.Object,System.Object>::RegisterChildren()
extern "C"  void Organism_2_RegisterChildren_m1720665825_gshared (Organism_2_t948289219 * __this, const MethodInfo* method);
#define Organism_2_RegisterChildren_m1720665825(__this, method) ((  void (*) (Organism_2_t948289219 *, const MethodInfo*))Organism_2_RegisterChildren_m1720665825_gshared)(__this, method)
// System.Void Organism`2<System.Object,System.Object>::<WakeUp>m__1AD(IOrganism)
extern "C"  void Organism_2_U3CWakeUpU3Em__1AD_m1095000686_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___child0, const MethodInfo* method);
#define Organism_2_U3CWakeUpU3Em__1AD_m1095000686(__this /* static, unused */, ___child0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Organism_2_U3CWakeUpU3Em__1AD_m1095000686_gshared)(__this /* static, unused */, ___child0, method)
// System.Void Organism`2<System.Object,System.Object>::<Spread>m__1AE(IOrganism)
extern "C"  void Organism_2_U3CSpreadU3Em__1AE_m2535020769_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___child0, const MethodInfo* method);
#define Organism_2_U3CSpreadU3Em__1AE_m2535020769(__this /* static, unused */, ___child0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Organism_2_U3CSpreadU3Em__1AE_m2535020769_gshared)(__this /* static, unused */, ___child0, method)
// System.Void Organism`2<System.Object,System.Object>::<ToSleep>m__1AF(IOrganism)
extern "C"  void Organism_2_U3CToSleepU3Em__1AF_m3215044909_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___child0, const MethodInfo* method);
#define Organism_2_U3CToSleepU3Em__1AF_m3215044909(__this /* static, unused */, ___child0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Organism_2_U3CToSleepU3Em__1AF_m3215044909_gshared)(__this /* static, unused */, ___child0, method)
// System.Void Organism`2<System.Object,System.Object>::<ToSleep>m__1B0(System.IDisposable)
extern "C"  void Organism_2_U3CToSleepU3Em__1B0_m3346233993_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___d0, const MethodInfo* method);
#define Organism_2_U3CToSleepU3Em__1B0_m3346233993(__this /* static, unused */, ___d0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Organism_2_U3CToSleepU3Em__1B0_m3346233993_gshared)(__this /* static, unused */, ___d0, method)
