﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BarygaVkController
struct BarygaVkController_t3617163921;
// AchievementDescription
struct AchievementDescription_t2323334509;
// FriendInfo
struct FriendInfo_t236470412;
// System.Collections.Generic.List`1<FriendInfo>
struct List_1_t1033429381;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AchievementDescription2323334509.h"
#include "AssemblyU2DCSharp_FriendInfo236470412.h"

// System.Void BarygaVkController::.ctor()
extern "C"  void BarygaVkController__ctor_m1900071210 (BarygaVkController_t3617163921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarygaVkController::PostInject()
extern "C"  void BarygaVkController_PostInject_m3597301291 (BarygaVkController_t3617163921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarygaVkController::JoinGroup()
extern "C"  void BarygaVkController_JoinGroup_m2035481853 (BarygaVkController_t3617163921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarygaVkController::ReportAchievement(AchievementDescription)
extern "C"  void BarygaVkController_ReportAchievement_m508136214 (BarygaVkController_t3617163921 * __this, AchievementDescription_t2323334509 * ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarygaVkController::InviteFriends()
extern "C"  void BarygaVkController_InviteFriends_m4018076308 (BarygaVkController_t3617163921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarygaVkController::InviteFriend(FriendInfo)
extern "C"  void BarygaVkController_InviteFriend_m2710896149 (BarygaVkController_t3617163921 * __this, FriendInfo_t236470412 * ___friend0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BarygaVkController::<PostInject>m__EB(System.Int64)
extern "C"  bool BarygaVkController_U3CPostInjectU3Em__EB_m128326815 (BarygaVkController_t3617163921 * __this, int64_t ____0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BarygaVkController::<PostInject>m__EC(System.Int64)
extern "C"  bool BarygaVkController_U3CPostInjectU3Em__EC_m1635878624 (BarygaVkController_t3617163921 * __this, int64_t ____0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarygaVkController::<PostInject>m__ED(System.Int64)
extern "C"  void BarygaVkController_U3CPostInjectU3Em__ED_m3470595157 (BarygaVkController_t3617163921 * __this, int64_t ____0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BarygaVkController::<PostInject>m__EE(System.Boolean,System.Boolean)
extern "C"  bool BarygaVkController_U3CPostInjectU3Em__EE_m1802696854 (Il2CppObject * __this /* static, unused */, bool ___b0, bool ___b11, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BarygaVkController::<PostInject>m__EF(System.Boolean)
extern "C"  bool BarygaVkController_U3CPostInjectU3Em__EF_m1367263752 (BarygaVkController_t3617163921 * __this, bool ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarygaVkController::<PostInject>m__F0(System.Boolean)
extern "C"  void BarygaVkController_U3CPostInjectU3Em__F0_m1534836293 (BarygaVkController_t3617163921 * __this, bool ____0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarygaVkController::<JoinGroup>m__F1(System.Boolean)
extern "C"  void BarygaVkController_U3CJoinGroupU3Em__F1_m2310919172 (BarygaVkController_t3617163921 * __this, bool ___ok0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BarygaVkController::<ReportAchievement>m__F2(System.Boolean)
extern "C"  bool BarygaVkController_U3CReportAchievementU3Em__F2_m3192390431 (Il2CppObject * __this /* static, unused */, bool ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BarygaVkController::<InviteFriends>m__F4(System.Boolean)
extern "C"  bool BarygaVkController_U3CInviteFriendsU3Em__F4_m1854661074 (Il2CppObject * __this /* static, unused */, bool ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarygaVkController::<InviteFriends>m__F5(System.Boolean)
extern "C"  void BarygaVkController_U3CInviteFriendsU3Em__F5_m3993992479 (BarygaVkController_t3617163921 * __this, bool ____0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarygaVkController::<InviteFriends>m__F8(System.Collections.Generic.List`1<FriendInfo>)
extern "C"  void BarygaVkController_U3CInviteFriendsU3Em__F8_m1854139635 (BarygaVkController_t3617163921 * __this, List_1_t1033429381 * ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
