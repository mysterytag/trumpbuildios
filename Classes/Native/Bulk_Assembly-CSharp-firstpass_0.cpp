﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// AdToApp.AdToAppBinding
struct AdToAppBinding_t3015697107;
// System.String
struct String_t;
// AdToApp.AdToAppSDKDelegate
struct AdToAppSDKDelegate_t2853395885;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3012272455;
// System.Action`2<System.String,System.String>
struct Action_2_t2887221574;
// System.Action`3<System.String,System.String,System.String>
struct Action_3_t2150340505;
// System.Action
struct Action_t437523947;
// System.Action`1<System.String>
struct Action_1_t1116941607;
// System.Object
struct Il2CppObject;
// AdToApp.AndroidWrapper.ATAInterstitialAdListener
struct ATAInterstitialAdListener_t382012998;
// AdToApp.AndroidWrapper.ATABannerAdListener
struct ATABannerAdListener_t2854275270;
// AdToApp.AdToAppSDKDelegate/AndroidBannerListener
struct AndroidBannerListener_t3507331119;
// AdToApp.AdToAppSDKDelegate/AndroidInterstitialListener
struct AndroidInterstitialListener_t3321194735;
// AdToApp.AndroidWrapper.AdToAppAndroidWrapper
struct AdToAppAndroidWrapper_t877850908;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// System.IDisposable
struct IDisposable_t1628921374;
// AdToApp.AndroidWrapper.AdToAppAndroidWrapper/<InitializeSDK>c__AnonStorey29
struct U3CInitializeSDKU3Ec__AnonStorey29_t2322037182;
// AdToApp.AndroidWrapper.AdToAppAndroidWrapper/<ShowBanner>c__AnonStorey2A
struct U3CShowBannerU3Ec__AnonStorey2A_t18990589;
// AdToApp.AndroidWrapper.AdToAppAndroidWrapper/<ShowBanner>c__AnonStorey2B
struct U3CShowBannerU3Ec__AnonStorey2B_t18990590;
// AdToApp.AndroidWrapper.AdToAppAndroidWrapper/<ShowBannerAtPositon>c__AnonStorey2C
struct U3CShowBannerAtPositonU3Ec__AnonStorey2C_t1295846048;
// AdToApp.AndroidWrapper.AdToAppAndroidWrapper/<ShowBannerAtPositon>c__AnonStorey2D
struct U3CShowBannerAtPositonU3Ec__AnonStorey2D_t1295846049;
// AppsFlyer
struct AppsFlyer_t82339054;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;
// AppsFlyerTrackerCallbacks
struct AppsFlyerTrackerCallbacks_t1581047172;
// OnePF.Inventory
struct Inventory_t2630562832;
// OnePF.SkuDetails
struct SkuDetails_t70130009;
// OnePF.Purchase
struct Purchase_t160195573;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t3840643258;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t1634065389;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t3709260776;
// System.Collections.Generic.List`1<OnePF.Purchase>
struct List_1_t957154542;
// System.Collections.Generic.IEnumerable`1<OnePF.Purchase>
struct IEnumerable_1_t3032349929;
// System.Collections.Generic.List`1<OnePF.SkuDetails>
struct List_1_t867088978;
// System.Collections.Generic.IEnumerable`1<OnePF.SkuDetails>
struct IEnumerable_1_t2942284365;
// OnePF.JSON
struct JSON_t2649481148;
// OnePF.JSON/_JSON
struct _JSON_t90021319;
// OnePF.JSON/_JSON/Parser
struct Parser_t2383423553;
// OnePF.JSON/_JSON/Serializer
struct Serializer_t1395478963;
// System.Collections.IDictionary
struct IDictionary_t1654916945;
// System.Collections.IList
struct IList_t1612618265;
// OnePF.OpenIAB
struct OpenIAB_t3048630868;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// OnePF.Options
struct Options_t3062372690;
// System.String[]
struct StringU5BU5D_t2956870243;
// OnePF.OpenIAB_Android
struct OpenIAB_Android_t58679140;
// OnePF.OpenIAB_iOS
struct OpenIAB_iOS_t446655906;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>
struct IEnumerable_1_t671905164;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerable_1_t1890143508;
// OnePF.OpenIAB_WP8
struct OpenIAB_WP8_t446638612;
// OpenIABEventManager
struct OpenIABEventManager_t528054099;
// System.Action`1<OnePF.Inventory>
struct Action_1_t2779015537;
// System.Action`1<OnePF.Purchase>
struct Action_1_t308648278;
// System.Action`2<System.Int32,System.String>
struct Action_2_t1740334453;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;
// PlayFab.ErrorCallback
struct ErrorCallback_t582108558;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;
// PlayFab.Internal.PlayFabPluginEventHandler
struct PlayFabPluginEventHandler_t3351841502;
// PlayFab.PlayFabAndroidPlugin
struct PlayFabAndroidPlugin_t314518896;
// PlayFab.PlayFabGoogleCloudMessaging
struct PlayFabGoogleCloudMessaging_t2415988188;
// PlayFab.PlayFabGoogleCloudMessaging/GCMMessageReceived
struct GCMMessageReceived_t1036884951;
// PlayFab.PlayFabGoogleCloudMessaging/GCMRegisterComplete
struct GCMRegisterComplete_t1540081165;
// PlayFab.PlayFabGoogleCloudMessaging/GCMRegisterReady
struct GCMRegisterReady_t3751777615;
// SimpleJSON.JSONNode
struct JSONNode_t580622632;
// SimpleJSON.JSONArray
struct JSONArray_t3478762607;
// System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode>
struct IEnumerable_1_t3452776988;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.IO.BinaryWriter
struct BinaryWriter_t2314211483;
// SimpleJSON.JSONArray/<>c__Iterator2
struct U3CU3Ec__Iterator2_t2516598211;
// System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode>
struct IEnumerator_1_t2063729080;
// SimpleJSON.JSONArray/<GetEnumerator>c__Iterator3
struct U3CGetEnumeratorU3Ec__Iterator3_t490656866;
// SimpleJSON.JSONClass
struct JSONClass_t3480415118;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode>>
struct IEnumerable_1_t284038894;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode>,System.Boolean>
struct Func_2_t124259971;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Boolean>
struct Func_2_t1844407557;
// SimpleJSON.JSONClass/<>c__Iterator4
struct U3CU3Ec__Iterator4_t2516598213;
// SimpleJSON.JSONClass/<GetEnumerator>c__Iterator5
struct U3CGetEnumeratorU3Ec__Iterator5_t490656868;
// SimpleJSON.JSONClass/<Remove>c__AnonStorey2E
struct U3CRemoveU3Ec__AnonStorey2E_t1556685308;
// SimpleJSON.JSONData
struct JSONData_t580311760;
// SimpleJSON.JSONLazyCreator
struct JSONLazyCreator_t3067077166;
// System.IO.Stream
struct Stream_t219029575;
// System.IO.BinaryReader
struct BinaryReader_t2158806251;
// SimpleJSON.JSONNode/<>c__Iterator0
struct U3CU3Ec__Iterator0_t2516598209;
// SimpleJSON.JSONNode/<>c__Iterator1
struct U3CU3Ec__Iterator1_t2516598210;
// TrackEventTests
struct TrackEventTests_t1452154674;
// UniRx.AnimationCurveReactiveProperty
struct AnimationCurveReactiveProperty_t3280306475;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3342907448;
struct AnimationCurve_t3342907448_marshaled_pinvoke;
// UniRx.IObservable`1<UnityEngine.AsyncOperation>
struct IObservable_1_t3133193428;
// UnityEngine.AsyncOperation
struct AsyncOperation_t3374395064;
struct AsyncOperation_t3374395064_marshaled_pinvoke;
// UniRx.IProgress`1<System.Single>
struct IProgress_1_t4173802291;
// System.Func`3<UniRx.IObserver`1<UnityEngine.AsyncOperation>,UniRx.CancellationToken,System.Collections.IEnumerator>
struct Func_3_t3732850935;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Func`3<UniRx.IObserver`1<System.Object>,UniRx.CancellationToken,System.Collections.IEnumerator>
struct Func_3_t1973235155;
// UniRx.AsyncOperationExtensions/<AsObservable>c__AnonStorey7B
struct U3CAsObservableU3Ec__AnonStorey7B_t3743595045;
// UniRx.IObserver`1<UnityEngine.AsyncOperation>
struct IObserver_1_t1291426671;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// UniRx.BooleanDisposable
struct BooleanDisposable_t3065601722;
// UniRx.BooleanNotifier
struct BooleanNotifier_t3248530688;
// UniRx.IObserver`1<System.Boolean>
struct IObserver_1_t2423004244;
// UniRx.BoolReactiveProperty
struct BoolReactiveProperty_t3047538250;
// UniRx.BoundsReactiveProperty
struct BoundsReactiveProperty_t4100324373;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Bounds>
struct IEqualityComparer_1_t1547814333;
// UniRx.ByteReactiveProperty
struct ByteReactiveProperty_t1619126376;
// UniRx.ICancelable
struct ICancelable_t4109686575;
// UniRx.ColorReactiveProperty
struct ColorReactiveProperty_t874298499;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Color>
struct IEqualityComparer_1_t3912442411;
// UniRx.CompositeDisposable
struct CompositeDisposable_t1894629977;
// System.IDisposable[]
struct IDisposableU5BU5D_t165183403;
// System.Collections.Generic.IEnumerable`1<System.IDisposable>
struct IEnumerable_1_t206108434;
// System.Collections.Generic.IEnumerator`1<System.IDisposable>
struct IEnumerator_1_t3112027822;
// UniRx.CountNotifier
struct CountNotifier_t4027919079;
// UniRx.IObserver`1<UniRx.CountChangedStatus>
struct IObserver_1_t1688259328;
// UniRx.CountNotifier/<Increment>c__AnonStorey57
struct U3CIncrementU3Ec__AnonStorey57_t1898758910;
// UniRx.Diagnostics.LogEntry
struct LogEntry_t1891155722;
// UnityEngine.Object
struct Object_t3878351788;
struct Object_t3878351788_marshaled_pinvoke;
// System.Exception
struct Exception_t1967233988;
// UniRx.IObservable`1<UniRx.Diagnostics.LogEntry>
struct IObservable_1_t1649954086;
// UniRx.Diagnostics.Logger
struct Logger_t2118475020;
// UniRx.Diagnostics.ObservableLogger
struct ObservableLogger_t586897455;
// System.Action`1<UniRx.Diagnostics.LogEntry>
struct Action_1_t2039608427;
// UniRx.IObserver`1<UniRx.Diagnostics.LogEntry>
struct IObserver_1_t4103154625;
// UniRx.Diagnostics.UnityDebugSink
struct UnityDebugSink_t4077792013;
// UniRx.Disposable/AnonymousDisposable
struct AnonymousDisposable_t1367871885;
// UniRx.Disposable/EmptyDisposable
struct EmptyDisposable_t3755833965;
// UniRx.DoubleReactiveProperty
struct DoubleReactiveProperty_t414828849;
// UniRx.Examples.Sample01_ObservableWWW
struct Sample01_ObservableWWW_t1646932859;
// UniRx.IObservable`1<System.String>
struct IObservable_1_t727287266;
// System.Action`1<System.Exception>
struct Action_1_t2115686693;
// UniRx.IObservable`1<<>__AnonType0`2<System.String,System.String>>
struct IObservable_1_t1129798361;
// System.Func`2<System.String,UniRx.IObservable`1<System.String>>
struct Func_2_t676343372;
// System.Func`3<System.String,System.String,<>__AnonType0`2<System.String,System.String>>
struct Func_3_t3610964670;
// System.Func`2<System.Object,UniRx.IObservable`1<System.Object>>
struct Func_2_t1894581716;
// System.Func`3<System.Object,System.Object,System.Object>
struct Func_3_t1892209229;
// System.Action`1<<>__AnonType0`2<System.String,System.String>>
struct Action_1_t1519452702;
// UniRx.IObservable`1<System.String[]>
struct IObservable_1_t2715668607;
// UniRx.IObservable`1<System.String>[]
struct IObservable_1U5BU5D_t855805015;
// UniRx.IObservable`1<System.Object[]>
struct IObservable_1_t4065289433;
// UniRx.IObservable`1<System.Object>[]
struct IObservable_1U5BU5D_t2205425841;
// System.Action`1<System.String[]>
struct Action_1_t3105322948;
// UniRx.IObservable`1<System.Single>
struct IObservable_1_t717007385;
// System.Action`1<System.Single>
struct Action_1_t1106661726;
// System.Action`1<UniRx.WWWErrorException>
struct Action_1_t3907447057;
// <>__AnonType0`2<System.String,System.String>
struct U3CU3E__AnonType0_2_t1370999997;
// UniRx.WWWErrorException
struct WWWErrorException_t3758994352;
// UniRx.Examples.Sample02_ObservableTriggers
struct Sample02_ObservableTriggers_t2916496460;
// UniRx.Triggers.ObservableUpdateTrigger
struct ObservableUpdateTrigger_t3433526989;
// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;
// System.Action`1<UniRx.Unit>
struct Action_1_t2706738743;
// UniRx.Examples.Sample04_ConvertFromUnityCallback
struct Sample04_ConvertFromUnityCallback_t3574096393;
// UniRx.IObservable`1<UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback>
struct IObservable_1_t2994461093;
// System.Func`2<UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback,System.Boolean>
struct Func_2_t2102879704;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t1509682273;
// System.Action`1<UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback>
struct Action_1_t3384115434;
// UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback
struct LogCallback_t3235662730;
// System.Func`2<System.Action`1<UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback>,UnityEngine.Application/LogCallback>
struct Func_2_t4092726719;
// System.Action`1<UnityEngine.Application/LogCallback>
struct Action_1_t3384115435;
// System.Func`2<System.Action`1<System.Object>,System.Object>
struct Func_2_t1100972979;
// UnityEngine.Application/LogCallback
struct LogCallback_t3235662729;
// UniRx.Examples.Sample04_ConvertFromUnityCallback/LogHelper/<LogCallbackAsObservable>c__AnonStorey2F
struct U3CLogCallbackAsObservableU3Ec__AnonStorey2F_t1039971231;
// UniRx.Examples.Sample05_ConvertFromCoroutine
struct Sample05_ConvertFromCoroutine_t2367087270;
// System.Func`3<UniRx.IObserver`1<System.String>,UniRx.CancellationToken,System.Collections.IEnumerator>
struct Func_3_t212750549;
// UniRx.IObserver`1<System.String>
struct IObserver_1_t3180487805;
// UniRx.Examples.Sample05_ConvertFromCoroutine/<GetWWW>c__AnonStorey30
struct U3CGetWWWU3Ec__AnonStorey30_t1804159459;
// UniRx.Examples.Sample05_ConvertFromCoroutine/<GetWWWCore>c__Iterator6
struct U3CGetWWWCoreU3Ec__Iterator6_t2275077895;
// UniRx.Examples.Sample06_ConvertToCoroutine
struct Sample06_ConvertToCoroutine_t617332884;
// UniRx.Examples.Sample06_ConvertToCoroutine/<ComplexCoroutineTest>c__Iterator7
struct U3CComplexCoroutineTestU3Ec__Iterator7_t4070488366;
// UnityEngine.Coroutine
struct Coroutine_t2246592261;
struct Coroutine_t2246592261_marshaled_pinvoke;
// UniRx.IObservable`1<System.Int32>
struct IObservable_1_t2606213151;
// System.Action`1<System.Int32>
struct Action_1_t2995867492;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E86524790.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E86524790MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AdToApp_AdToAppBanne3244534633.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AdToApp_AdToAppBanne3244534633MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AdToApp_AdToAppBanne3012686273.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AdToApp_AdToAppBanne3012686273MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AdToApp_AdToAppBindi3015697107.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AdToApp_AdToAppBindi3015697107MethodDeclarations.h"
#include "mscorlib_System_Void2779279689.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AdToApp_AdToAppLogLev289278932.h"
#include "mscorlib_System_Int322847414787.h"
#include "mscorlib_System_Boolean211005341.h"
#include "mscorlib_System_Single958209021.h"
#include "mscorlib_System_Double534516614.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AdToApp_AdToAppSDKDe2853395885.h"
#include "UnityEngine_UnityEngine_Object3878351788MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AdToApp_AdToAppConten669421857.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AdToApp_AdToAppConten669421857MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AdToApp_AdToAppLogLev289278932MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AdToApp_AdToAppSDKDe2853395885MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour3012272455MethodDeclarations.h"
#include "System_Core_System_Action_2_gen2887221574.h"
#include "mscorlib_System_Delegate3660574010MethodDeclarations.h"
#include "mscorlib_System_Delegate3660574010.h"
#include "System_Core_System_Action_3_gen2150340505.h"
#include "System_Core_System_Action437523947.h"
#include "mscorlib_System_Action_1_gen1116941607.h"
#include "UnityEngine_UnityEngine_Component2126946602MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject4012695102MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"
#include "System_Core_System_Action_2_gen2887221574MethodDeclarations.h"
#include "System_Core_System_Action_3_gen2150340505MethodDeclarations.h"
#include "System_Core_System_Action437523947MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen1116941607MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AdToApp_AndroidWrappe382012998.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AdToApp_AdToAppSDKDe3321194735MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AdToApp_AdToAppSDKDe3321194735.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AdToApp_AndroidWrapp2854275270.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AdToApp_AdToAppSDKDe3507331119MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AdToApp_AdToAppSDKDe3507331119.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AdToApp_AndroidWrapp2854275270MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AdToApp_AndroidWrappe382012998MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AdToApp_AdToAppTarge2355315135.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AdToApp_AdToAppTarge2355315135MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AdToApp_AndroidWrappe877850908.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AdToApp_AndroidWrappe877850908MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AdToApp_AndroidWrapp2322037182MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen985559125MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AdToApp_AndroidWrapp2322037182.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Action_1_gen985559125.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_String968488902MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AdToApp_AndroidWrapper18990589MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AdToApp_AndroidWrapper18990589.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AdToApp_AndroidWrapper18990590MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AdToApp_AndroidWrapper18990590.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AdToApp_AndroidWrapp1295846048MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AdToApp_AndroidWrapp1295846048.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AdToApp_AndroidWrapp1295846049MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AdToApp_AndroidWrapp1295846049.h"
#include "mscorlib_System_ArgumentNullException3214793280MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException3214793280.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AppsFlyer82339054.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AppsFlyer82339054MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2606186806.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2606186806MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2373214747MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22094718104MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22094718104.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2373214747.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AppsFlyerTrackerCall1581047172.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AppsFlyerTrackerCall1581047172MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug1588791936MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_Inventory2630562832.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_Inventory2630562832MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1707827913MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1797893477MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_JSON2649481148MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1634065389MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4014815677MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_OpenIAB_iOS446655906MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_Purchase160195573MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_SkuDetails70130009MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_JSON2649481148.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4014815677.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1634065389.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_Purchase160195573.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_SkuDetails70130009.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1707827913.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1797893477.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2474804324.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2474804324MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder3822575854MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1564921418MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21286424775MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1474855854MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21196359211MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder3822575854.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21286424775.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1564921418.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21196359211.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1474855854.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1765447871.h"
#include "System_Core_System_Linq_Enumerable4285654829MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K4121168757.h"
#include "System_Core_System_Linq_Enumerable4285654829.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1765447871MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3720030571MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1564921418MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1564921418.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3720030571.h"
#include "mscorlib_System_Collections_Generic_List_1_gen957154542.h"
#include "mscorlib_System_Collections_Generic_List_1_gen867088978.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3629965007.h"
#include "mscorlib_System_Convert1097883944MethodDeclarations.h"
#include "mscorlib_System_Int642847414882.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_JSON__JSON90021319MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"
#include "UnityEngine_UnityEngine_Vector33525329789MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector23525329788MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"
#include "UnityEngine_UnityEngine_Color1588175760MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color324137084207.h"
#include "UnityEngine_UnityEngine_Color324137084207MethodDeclarations.h"
#include "mscorlib_System_Byte2778693821.h"
#include "UnityEngine_UnityEngine_Rect1525428817.h"
#include "UnityEngine_UnityEngine_Rect1525428817MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_JSON__JSON90021319.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_JSON__JSON_Par2383423551MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_JSON__JSON_Ser1395478962MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_JSON__JSON_Par2383423551.h"
#include "mscorlib_System_IO_StringReader2229325051MethodDeclarations.h"
#include "mscorlib_System_IO_StringReader2229325051.h"
#include "mscorlib_System_IO_TextReader1534522647MethodDeclarations.h"
#include "mscorlib_System_IO_TextReader1534522647.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_JSON__JSON_Parse80003545.h"
#include "mscorlib_System_Char2778706699.h"
#include "mscorlib_System_Int642847414882MethodDeclarations.h"
#include "mscorlib_System_Double534516614MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge190145395MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge190145395.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_JSON__JSON_Parse80003545MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_JSON__JSON_Ser1395478962.h"
#include "mscorlib_System_UInt32985925326.h"
#include "mscorlib_System_SByte2855346064.h"
#include "mscorlib_System_Int162847414729.h"
#include "mscorlib_System_UInt16985925268.h"
#include "mscorlib_System_UInt64985925421.h"
#include "mscorlib_System_Decimal1688557254.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_OpenIAB3048630868.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_OpenIAB3048630868MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_OpenIAB_iOS446655906.h"
#include "mscorlib_System_Type2779229935MethodDeclarations.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_System_RuntimeTypeHandle1864875887.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_Options3062372690.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_OpenIAB_Android58679140.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_OpenIAB_Android58679140MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application450040189MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimePlatform1574985880.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23312956448.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_OpenIAB_WP8446638612.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_OpenIAB_WP8446638612MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_Options3062372690MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_OptionsVerifyM1652998574.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_OptionsVerifyM1652998574MethodDeclarations.h"
#include "mscorlib_System_Guid2778838590MethodDeclarations.h"
#include "mscorlib_System_Guid2778838590.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_SearchStrategy1294470191.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OnePF_SearchStrategy1294470191MethodDeclarations.h"
#include "mscorlib_System_Single958209021MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OpenIABEventManager528054099.h"
#include "AssemblyU2DCSharpU2Dfirstpass_OpenIABEventManager528054099MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2779015537.h"
#include "mscorlib_System_Action_1_gen308648278.h"
#include "System_Core_System_Action_2_gen1740334453.h"
#include "UnityEngine_UnityEngine_Object3878351788.h"
#include "mscorlib_System_Action_1_gen2779015537MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen308648278MethodDeclarations.h"
#include "System_Core_System_Action_2_gen1740334453MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen580771314MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen580771314.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestC1456502626.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestC1456502626MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_ErrorCallback582108558.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_ErrorCallback582108558MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_Internal_Pla3351841502.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_Internal_Pla3351841502MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge923129392MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge923129392.h"
#include "mscorlib_System_Boolean211005341MethodDeclarations.h"
#include "mscorlib_System_Int322847414787MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646MethodDeclarations.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabAndroi314518896.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabAndroi314518896MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabGoogl2415988188MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError2114648451.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError2114648451MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabGoogl2415988188.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFabNotificationP2037342920.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabGoogl3751777615MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabGoogl3751777615.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabGoogl1540081165MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabGoogl1540081165.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabGoogl1036884951MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabGoogl1036884951.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_WebRequestTy2827971030.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_WebRequestTy2827971030MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFabNotificationP2037342920MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SimpleJSON_JSON1774398214.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SimpleJSON_JSON1774398214MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SimpleJSON_JSONNode580622632.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SimpleJSON_JSONNode580622632MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SimpleJSON_JSONArray3478762607.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SimpleJSON_JSONArray3478762607MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1377581601MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1377581601.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SimpleJSON_JSONLazyC3067077166MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SimpleJSON_JSONLazyC3067077166.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SimpleJSON_JSONArray2516598211MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SimpleJSON_JSONArray2516598211.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SimpleJSON_JSONArray_490656866MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SimpleJSON_JSONArray_490656866.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3758331889MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3758331889.h"
#include "mscorlib_System_IO_BinaryWriter2314211483.h"
#include "mscorlib_System_IO_BinaryWriter2314211483MethodDeclarations.h"
#include "mscorlib_System_Threading_Interlocked3007803305MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1374155497MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1374155497.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SimpleJSON_JSONBinar1996404207.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SimpleJSON_JSONBinar1996404207MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SimpleJSON_JSONClass3480415118.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SimpleJSON_JSONClass3480415118MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2218320536MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2218320536.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21706851834MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21706851834.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SimpleJSON_JSONClass1556685308MethodDeclarations.h"
#include "System_Core_System_Func_2_gen124259971MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SimpleJSON_JSONClass1556685308.h"
#include "System_Core_System_Func_2_gen124259971.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SimpleJSON_JSONClass2516598213MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SimpleJSON_JSONClass2516598213.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SimpleJSON_JSONClass_490656868MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SimpleJSON_JSONClass_490656868.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1985348477MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1985348477.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke246628520MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1985348477MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1985348477.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke246628520.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SimpleJSON_JSONData580311760.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SimpleJSON_JSONData580311760MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SimpleJSON_JSONNode_2516598209MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SimpleJSON_JSONNode_2516598209.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SimpleJSON_JSONNode_2516598210MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SimpleJSON_JSONNode_2516598210.h"
#include "System_System_Collections_Generic_Stack_1_gen3151028667MethodDeclarations.h"
#include "mscorlib_System_Exception1967233988MethodDeclarations.h"
#include "System_System_Collections_Generic_Stack_1_gen3151028667.h"
#include "mscorlib_System_Globalization_NumberStyles3988678145.h"
#include "mscorlib_System_IO_Stream219029575.h"
#include "mscorlib_System_IO_FileInfo1355938065MethodDeclarations.h"
#include "mscorlib_System_IO_Directory3068535540MethodDeclarations.h"
#include "mscorlib_System_IO_File2029342275MethodDeclarations.h"
#include "mscorlib_System_IO_FileStream1527309539.h"
#include "mscorlib_System_IO_FileInfo1355938065.h"
#include "mscorlib_System_IO_DirectoryInfo3421965634.h"
#include "mscorlib_System_IO_FileSystemInfo2843833056MethodDeclarations.h"
#include "mscorlib_System_IO_FileSystemInfo2843833056.h"
#include "mscorlib_System_IO_MemoryStream2881531048MethodDeclarations.h"
#include "mscorlib_System_IO_MemoryStream2881531048.h"
#include "mscorlib_System_IO_BinaryReader2158806251.h"
#include "mscorlib_System_IO_BinaryReader2158806251MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_TrackEventTests1452154674.h"
#include "AssemblyU2DCSharpU2Dfirstpass_TrackEventTests1452154674MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_AnimationCurve3280306475.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_AnimationCurve3280306475MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper3951240486MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationCurve3342907448.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_AotSafeExtensio229198393.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_AotSafeExtensio229198393MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_AsyncOperation3431617553.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_AsyncOperation3431617553MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AsyncOperation3374395064.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_AsyncOperation3743595045MethodDeclarations.h"
#include "System_Core_System_Func_3_gen3732850935MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable258071701MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_AsyncOperation3743595045.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560.h"
#include "System_Core_System_Func_3_gen3732850935.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Observable258071701.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_BooleanDisposa3065601722.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_BooleanDisposa3065601722MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_BooleanNotifie3248530688.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_BooleanNotifie3248530688MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen2149039961MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen2149039961.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_BoolReactivePr3047538250.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_BoolReactivePr3047538250MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactivePropert819338379MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_BoundsReactive4100324373.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_BoundsReactive4100324373MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper4126848016MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Bounds3518514978.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_UnityEqualityC3801418734.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_UnityEqualityC3801418734MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ByteReactivePr1619126376.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ByteReactivePr1619126376MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper3387026859MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560MethodDeclarations.h"
#include "mscorlib_System_OperationCanceledException3009926340MethodDeclarations.h"
#include "mscorlib_System_OperationCanceledException3009926340.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ColorReactivePr874298499.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ColorReactivePr874298499MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper2196508798MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CompositeDispo1894629977.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CompositeDispo1894629977MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2425880343MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2425880343.h"
#include "mscorlib_System_ArgumentOutOfRangeException3479058991MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException3479058991.h"
#include "mscorlib_System_Threading_Monitor2071304733MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat511663335MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat511663335.h"
#include "mscorlib_System_Array2840145358MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CountChangedSt3771227721.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CountChangedSt3771227721MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CountNotifier4027919079.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CountNotifier4027919079MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen1414295045MethodDeclarations.h"
#include "mscorlib_System_ArgumentException124305799MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen1414295045.h"
#include "mscorlib_System_ArgumentException124305799.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CountNotifier_1898758910MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Disposable3736388786MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CountNotifier_1898758910.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Disposable3736388786.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Diagnostics_Lo1891155722.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Diagnostics_Lo1891155722MethodDeclarations.h"
#include "UnityEngine_UnityEngine_LogType3529269451.h"
#include "mscorlib_System_DateTime339033936.h"
#include "mscorlib_System_Environment63604104MethodDeclarations.h"
#include "mscorlib_System_DateTime339033936MethodDeclarations.h"
#include "mscorlib_System_Enum2778772662MethodDeclarations.h"
#include "mscorlib_System_Enum2778772662.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Diagnostics_Lo1038796222.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Diagnostics_Lo1038796222MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Diagnostics_Un4077792013MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Diagnostics_Un4077792013.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Diagnostics_Lo2118475020.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Diagnostics_Lo2118475020MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Diagnostics_Obs586897455MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2039608427.h"
#include "mscorlib_System_Action_1_gen2039608427MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Diagnostics_Obs586897455.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen3829190342MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen3829190342.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Disposable_Emp3755833965.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Disposable_Emp3755833965MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Disposable_Ano1367871885MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Disposable_Ano1367871885.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_DisposableExten691219686.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_DisposableExten691219686MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_DoubleReactiveP414828849.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_DoubleReactiveP414828849MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper1142849652MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sampl1646932859.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sampl1646932859MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableWWW156691558MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2115686693MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableExte3095004425MethodDeclarations.h"
#include "System_Core_System_Func_2_gen676343372MethodDeclarations.h"
#include "System_Core_System_Func_3_gen3610964670MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen1519452702MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3105322948MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ScheduledNotif2478225832MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen1106661726MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3907447057MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ScheduledNotif2478225832.h"
#include "mscorlib_System_Action_1_gen2115686693.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ObservableExte3095004425.h"
#include "System_Core_System_Func_2_gen676343372.h"
#include "System_Core_System_Func_3_gen3610964670.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CU3E__AnonType0_2_1370999997.h"
#include "mscorlib_System_Action_1_gen1519452702.h"
#include "Assembly-CSharp-firstpass_ArrayTypes.h"
#include "mscorlib_System_Action_1_gen3105322948.h"
#include "mscorlib_System_Action_1_gen1106661726.h"
#include "mscorlib_System_Action_1_gen3907447057.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_WWWErrorExcept3758994352.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CU3E__AnonType0_2_1370999997MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_WWWErrorExcept3758994352MethodDeclarations.h"
#include "System_System_Net_HttpStatusCode2736938801.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sampl2916496460.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sampl2916496460MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Triggers_Obser3433526989MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2706738743MethodDeclarations.h"
#include "UnityEngine_UnityEngine_PrimitiveType3380664558.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Triggers_Obser3433526989.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_FrameCountType167759182.h"
#include "mscorlib_System_Action_1_gen2706738743.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sampl3574096393.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sampl3574096393MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sampl3627301874MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2102879704MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3384115434MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2102879704.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sampl3235662729.h"
#include "mscorlib_System_Action_1_gen3384115434.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sampl3235662729MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sampl3627301874.h"
#include "System_Core_System_Func_2_gen4092726719MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen_0MethodDeclarations.h"
#include "System_Core_System_Func_2_gen4092726719.h"
#include "UnityEngine_UnityEngine_Application_LogCallback3235662729.h"
#include "mscorlib_System_Action_1_gen_0.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sampl1039971231MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application_LogCallback3235662729MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sampl1039971231.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sampl2367087270.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sampl2367087270MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sampl1804159459MethodDeclarations.h"
#include "System_Core_System_Func_3_gen212750549MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sampl1804159459.h"
#include "System_Core_System_Func_3_gen212750549.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sampl2275077895MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sampl2275077895.h"
#include "UnityEngine_UnityEngine_WWW1522972100MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWW1522972100.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sample617332884.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sample617332884MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Coroutine2246592261.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sampl4070488366MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sampl4070488366.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sampl1612341822MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sampl1612341822.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sampl2930300994MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Examples_Sampl2930300994.h"
#include "UnityEngine_UnityEngine_WaitForSeconds1291133240MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2995867492MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForSeconds1291133240.h"
#include "mscorlib_System_Action_1_gen2995867492.h"

// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m3048268896_gshared (GameObject_t4012695102 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisIl2CppObject_m3048268896(__this, method) ((  Il2CppObject * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3048268896_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<AdToApp.AdToAppSDKDelegate>()
#define GameObject_AddComponent_TisAdToAppSDKDelegate_t2853395885_m1834791607(__this, method) ((  AdToAppSDKDelegate_t2853395885 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3048268896_gshared)(__this, method)
// !!0 AdToApp.AndroidWrapper.AdToAppAndroidWrapper::GetResult<System.Object>(System.String)
extern "C"  Il2CppObject * AdToAppAndroidWrapper_GetResult_TisIl2CppObject_m799584889_gshared (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method);
#define AdToAppAndroidWrapper_GetResult_TisIl2CppObject_m799584889(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))AdToAppAndroidWrapper_GetResult_TisIl2CppObject_m799584889_gshared)(__this /* static, unused */, p0, method)
// !!0 AdToApp.AndroidWrapper.AdToAppAndroidWrapper::GetResult<System.String>(System.String)
#define AdToAppAndroidWrapper_GetResult_TisString_t_m4192045899(__this /* static, unused */, p0, method) ((  String_t* (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))AdToAppAndroidWrapper_GetResult_TisIl2CppObject_m799584889_gshared)(__this /* static, unused */, p0, method)
// !!0 AdToApp.AndroidWrapper.AdToAppAndroidWrapper::CallWrapperMethod<System.Boolean>(System.String,System.Object[])
extern "C"  bool AdToAppAndroidWrapper_CallWrapperMethod_TisBoolean_t211005341_m2126928797_gshared (Il2CppObject * __this /* static, unused */, String_t* p0, ObjectU5BU5D_t11523773* p1, const MethodInfo* method);
#define AdToAppAndroidWrapper_CallWrapperMethod_TisBoolean_t211005341_m2126928797(__this /* static, unused */, p0, p1, method) ((  bool (*) (Il2CppObject * /* static, unused */, String_t*, ObjectU5BU5D_t11523773*, const MethodInfo*))AdToAppAndroidWrapper_CallWrapperMethod_TisBoolean_t211005341_m2126928797_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 AdToApp.AndroidWrapper.AdToAppAndroidWrapper::CallWrapperMethod<System.Object>(System.String,System.Object[])
extern "C"  Il2CppObject * AdToAppAndroidWrapper_CallWrapperMethod_TisIl2CppObject_m3095671592_gshared (Il2CppObject * __this /* static, unused */, String_t* p0, ObjectU5BU5D_t11523773* p1, const MethodInfo* method);
#define AdToAppAndroidWrapper_CallWrapperMethod_TisIl2CppObject_m3095671592(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, String_t*, ObjectU5BU5D_t11523773*, const MethodInfo*))AdToAppAndroidWrapper_CallWrapperMethod_TisIl2CppObject_m3095671592_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 AdToApp.AndroidWrapper.AdToAppAndroidWrapper::CallWrapperMethod<System.String>(System.String,System.Object[])
#define AdToAppAndroidWrapper_CallWrapperMethod_TisString_t_m425797626(__this /* static, unused */, p0, p1, method) ((  String_t* (*) (Il2CppObject * /* static, unused */, String_t*, ObjectU5BU5D_t11523773*, const MethodInfo*))AdToAppAndroidWrapper_CallWrapperMethod_TisIl2CppObject_m3095671592_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  List_1_t1634065389 * Enumerable_ToList_TisIl2CppObject_m4082139931_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_ToList_TisIl2CppObject_m4082139931(__this /* static, unused */, p0, method) ((  List_1_t1634065389 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToList_TisIl2CppObject_m4082139931_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.String>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToList_TisString_t_m790467565(__this /* static, unused */, p0, method) ((  List_1_t1765447871 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToList_TisIl2CppObject_m4082139931_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<OnePF.Purchase>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToList_TisPurchase_t160195573_m1369263790(__this /* static, unused */, p0, method) ((  List_1_t957154542 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToList_TisIl2CppObject_m4082139931_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<OnePF.SkuDetails>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToList_TisSkuDetails_t70130009_m2376041426(__this /* static, unused */, p0, method) ((  List_1_t867088978 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToList_TisIl2CppObject_m4082139931_gshared)(__this /* static, unused */, p0, method)
// !!0 System.Linq.Enumerable::ElementAt<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>(System.Collections.Generic.IEnumerable`1<!!0>,System.Int32)
extern "C"  KeyValuePair_2_t3312956448  Enumerable_ElementAt_TisKeyValuePair_2_t3312956448_m265861539_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, int32_t p1, const MethodInfo* method);
#define Enumerable_ElementAt_TisKeyValuePair_2_t3312956448_m265861539(__this /* static, unused */, p0, p1, method) ((  KeyValuePair_2_t3312956448  (*) (Il2CppObject * /* static, unused */, Il2CppObject*, int32_t, const MethodInfo*))Enumerable_ElementAt_TisKeyValuePair_2_t3312956448_m265861539_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 System.Linq.Enumerable::ElementAt<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>(System.Collections.Generic.IEnumerable`1<!!0>,System.Int32)
#define Enumerable_ElementAt_TisKeyValuePair_2_t2094718104_m631600327(__this /* static, unused */, p0, p1, method) ((  KeyValuePair_2_t2094718104  (*) (Il2CppObject * /* static, unused */, Il2CppObject*, int32_t, const MethodInfo*))Enumerable_ElementAt_TisKeyValuePair_2_t3312956448_m265861539_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared (GameObject_t4012695102 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2447772384(__this, method) ((  Il2CppObject * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<PlayFab.Internal.PlayFabPluginEventHandler>()
#define GameObject_GetComponent_TisPlayFabPluginEventHandler_t3351841502_m567520681(__this, method) ((  PlayFabPluginEventHandler_t3351841502 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<PlayFab.Internal.PlayFabPluginEventHandler>()
#define GameObject_AddComponent_TisPlayFabPluginEventHandler_t3351841502_m2605444094(__this, method) ((  PlayFabPluginEventHandler_t3351841502 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3048268896_gshared)(__this, method)
// !!0 System.Linq.Enumerable::ElementAt<System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode>>(System.Collections.Generic.IEnumerable`1<!!0>,System.Int32)
#define Enumerable_ElementAt_TisKeyValuePair_2_t1706851834_m2914948309(__this /* static, unused */, p0, p1, method) ((  KeyValuePair_2_t1706851834  (*) (Il2CppObject * /* static, unused */, Il2CppObject*, int32_t, const MethodInfo*))Enumerable_ElementAt_TisKeyValuePair_2_t3312956448_m265861539_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C"  Il2CppObject* Enumerable_Where_TisKeyValuePair_2_t3312956448_m1955579279_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t1844407557 * p1, const MethodInfo* method);
#define Enumerable_Where_TisKeyValuePair_2_t3312956448_m1955579279(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1844407557 *, const MethodInfo*))Enumerable_Where_TisKeyValuePair_2_t3312956448_m1955579279_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode>>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Enumerable_Where_TisKeyValuePair_2_t1706851834_m879253725(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t124259971 *, const MethodInfo*))Enumerable_Where_TisKeyValuePair_2_t3312956448_m1955579279_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 System.Linq.Enumerable::First<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  KeyValuePair_2_t3312956448  Enumerable_First_TisKeyValuePair_2_t3312956448_m1563542323_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_First_TisKeyValuePair_2_t3312956448_m1563542323(__this /* static, unused */, p0, method) ((  KeyValuePair_2_t3312956448  (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_First_TisKeyValuePair_2_t3312956448_m1563542323_gshared)(__this /* static, unused */, p0, method)
// !!0 System.Linq.Enumerable::First<System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode>>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_First_TisKeyValuePair_2_t1706851834_m4161586049(__this /* static, unused */, p0, method) ((  KeyValuePair_2_t1706851834  (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_First_TisKeyValuePair_2_t3312956448_m1563542323_gshared)(__this /* static, unused */, p0, method)
// UniRx.IObservable`1<!!0> UniRx.Observable::FromCoroutine<System.Object>(System.Func`3<UniRx.IObserver`1<!!0>,UniRx.CancellationToken,System.Collections.IEnumerator>)
extern "C"  Il2CppObject* Observable_FromCoroutine_TisIl2CppObject_m3257165431_gshared (Il2CppObject * __this /* static, unused */, Func_3_t1973235155 * p0, const MethodInfo* method);
#define Observable_FromCoroutine_TisIl2CppObject_m3257165431(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Func_3_t1973235155 *, const MethodInfo*))Observable_FromCoroutine_TisIl2CppObject_m3257165431_gshared)(__this /* static, unused */, p0, method)
// UniRx.IObservable`1<!!0> UniRx.Observable::FromCoroutine<UnityEngine.AsyncOperation>(System.Func`3<UniRx.IObserver`1<!!0>,UniRx.CancellationToken,System.Collections.IEnumerator>)
#define Observable_FromCoroutine_TisAsyncOperation_t3374395064_m2741885049(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Func_3_t3732850935 *, const MethodInfo*))Observable_FromCoroutine_TisIl2CppObject_m3257165431_gshared)(__this /* static, unused */, p0, method)
// System.Collections.IEnumerator UniRx.AsyncOperationExtensions::AsObservableCore<System.Object>(!!0,UniRx.IObserver`1<!!0>,UniRx.IProgress`1<System.Single>,UniRx.CancellationToken)
extern "C"  Il2CppObject * AsyncOperationExtensions_AsObservableCore_TisIl2CppObject_m703276880_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Il2CppObject* p1, Il2CppObject* p2, CancellationToken_t1439151560  p3, const MethodInfo* method);
#define AsyncOperationExtensions_AsObservableCore_TisIl2CppObject_m703276880(__this /* static, unused */, p0, p1, p2, p3, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Il2CppObject*, Il2CppObject*, CancellationToken_t1439151560 , const MethodInfo*))AsyncOperationExtensions_AsObservableCore_TisIl2CppObject_m703276880_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Collections.IEnumerator UniRx.AsyncOperationExtensions::AsObservableCore<UnityEngine.AsyncOperation>(!!0,UniRx.IObserver`1<!!0>,UniRx.IProgress`1<System.Single>,UniRx.CancellationToken)
#define AsyncOperationExtensions_AsObservableCore_TisAsyncOperation_t3374395064_m3718184494(__this /* static, unused */, p0, p1, p2, p3, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, AsyncOperation_t3374395064 *, Il2CppObject*, Il2CppObject*, CancellationToken_t1439151560 , const MethodInfo*))AsyncOperationExtensions_AsObservableCore_TisIl2CppObject_m703276880_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.IDisposable UniRx.ObservableExtensions::Subscribe<System.Object>(UniRx.IObservable`1<!!0>,System.Action`1<!!0>,System.Action`1<System.Exception>)
extern "C"  Il2CppObject * ObservableExtensions_Subscribe_TisIl2CppObject_m3586747146_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Action_1_t985559125 * p1, Action_1_t2115686693 * p2, const MethodInfo* method);
#define ObservableExtensions_Subscribe_TisIl2CppObject_m3586747146(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Action_1_t985559125 *, Action_1_t2115686693 *, const MethodInfo*))ObservableExtensions_Subscribe_TisIl2CppObject_m3586747146_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.IDisposable UniRx.ObservableExtensions::Subscribe<System.String>(UniRx.IObservable`1<!!0>,System.Action`1<!!0>,System.Action`1<System.Exception>)
#define ObservableExtensions_Subscribe_TisString_t_m3357525852(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Action_1_t1116941607 *, Action_1_t2115686693 *, const MethodInfo*))ObservableExtensions_Subscribe_TisIl2CppObject_m3586747146_gshared)(__this /* static, unused */, p0, p1, p2, method)
// UniRx.IObservable`1<!!2> UniRx.Observable::SelectMany<System.Object,System.Object,System.Object>(UniRx.IObservable`1<!!0>,System.Func`2<!!0,UniRx.IObservable`1<!!1>>,System.Func`3<!!0,!!1,!!2>)
extern "C"  Il2CppObject* Observable_SelectMany_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m2252928320_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t1894581716 * p1, Func_3_t1892209229 * p2, const MethodInfo* method);
#define Observable_SelectMany_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m2252928320(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1894581716 *, Func_3_t1892209229 *, const MethodInfo*))Observable_SelectMany_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m2252928320_gshared)(__this /* static, unused */, p0, p1, p2, method)
// UniRx.IObservable`1<!!2> UniRx.Observable::SelectMany<System.String,System.String,<>__AnonType0`2<System.String,System.String>>(UniRx.IObservable`1<!!0>,System.Func`2<!!0,UniRx.IObservable`1<!!1>>,System.Func`3<!!0,!!1,!!2>)
#define Observable_SelectMany_TisString_t_TisString_t_TisU3CU3E__AnonType0_2_t1370999997_m1859969858(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t676343372 *, Func_3_t3610964670 *, const MethodInfo*))Observable_SelectMany_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m2252928320_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.IDisposable UniRx.ObservableExtensions::Subscribe<System.Object>(UniRx.IObservable`1<!!0>,System.Action`1<!!0>)
extern "C"  Il2CppObject * ObservableExtensions_Subscribe_TisIl2CppObject_m2475506742_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Action_1_t985559125 * p1, const MethodInfo* method);
#define ObservableExtensions_Subscribe_TisIl2CppObject_m2475506742(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Action_1_t985559125 *, const MethodInfo*))ObservableExtensions_Subscribe_TisIl2CppObject_m2475506742_gshared)(__this /* static, unused */, p0, p1, method)
// System.IDisposable UniRx.ObservableExtensions::Subscribe<<>__AnonType0`2<System.String,System.String>>(UniRx.IObservable`1<!!0>,System.Action`1<!!0>)
#define ObservableExtensions_Subscribe_TisU3CU3E__AnonType0_2_t1370999997_m765500324(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Action_1_t1519452702 *, const MethodInfo*))ObservableExtensions_Subscribe_TisIl2CppObject_m2475506742_gshared)(__this /* static, unused */, p0, p1, method)
// UniRx.IObservable`1<!!0[]> UniRx.Observable::WhenAll<System.Object>(UniRx.IObservable`1<!!0>[])
extern "C"  Il2CppObject* Observable_WhenAll_TisIl2CppObject_m2216747008_gshared (Il2CppObject * __this /* static, unused */, IObservable_1U5BU5D_t2205425841* p0, const MethodInfo* method);
#define Observable_WhenAll_TisIl2CppObject_m2216747008(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, IObservable_1U5BU5D_t2205425841*, const MethodInfo*))Observable_WhenAll_TisIl2CppObject_m2216747008_gshared)(__this /* static, unused */, p0, method)
// UniRx.IObservable`1<!!0[]> UniRx.Observable::WhenAll<System.String>(UniRx.IObservable`1<!!0>[])
#define Observable_WhenAll_TisString_t_m1484418158(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, IObservable_1U5BU5D_t855805015*, const MethodInfo*))Observable_WhenAll_TisIl2CppObject_m2216747008_gshared)(__this /* static, unused */, p0, method)
// System.IDisposable UniRx.ObservableExtensions::Subscribe<System.String[]>(UniRx.IObservable`1<!!0>,System.Action`1<!!0>)
#define ObservableExtensions_Subscribe_TisStringU5BU5D_t2956870243_m2794009706(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Action_1_t3105322948 *, const MethodInfo*))ObservableExtensions_Subscribe_TisIl2CppObject_m2475506742_gshared)(__this /* static, unused */, p0, p1, method)
// System.IDisposable UniRx.ObservableExtensions::Subscribe<System.Single>(UniRx.IObservable`1<!!0>,System.Action`1<!!0>)
extern "C"  Il2CppObject * ObservableExtensions_Subscribe_TisSingle_t958209021_m2633336639_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Action_1_t1106661726 * p1, const MethodInfo* method);
#define ObservableExtensions_Subscribe_TisSingle_t958209021_m2633336639(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Action_1_t1106661726 *, const MethodInfo*))ObservableExtensions_Subscribe_TisSingle_t958209021_m2633336639_gshared)(__this /* static, unused */, p0, p1, method)
// System.IDisposable UniRx.ObservableExtensions::Subscribe<System.Object>(UniRx.IObservable`1<!!0>)
extern "C"  Il2CppObject * ObservableExtensions_Subscribe_TisIl2CppObject_m2698751244_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define ObservableExtensions_Subscribe_TisIl2CppObject_m2698751244(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))ObservableExtensions_Subscribe_TisIl2CppObject_m2698751244_gshared)(__this /* static, unused */, p0, method)
// System.IDisposable UniRx.ObservableExtensions::Subscribe<System.String>(UniRx.IObservable`1<!!0>)
#define ObservableExtensions_Subscribe_TisString_t_m1634303226(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))ObservableExtensions_Subscribe_TisIl2CppObject_m2698751244_gshared)(__this /* static, unused */, p0, method)
// UniRx.IObservable`1<!!0> UniRx.Observable::CatchIgnore<System.Object,System.Object>(UniRx.IObservable`1<!!0>,System.Action`1<!!1>)
extern "C"  Il2CppObject* Observable_CatchIgnore_TisIl2CppObject_TisIl2CppObject_m2128824987_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Action_1_t985559125 * p1, const MethodInfo* method);
#define Observable_CatchIgnore_TisIl2CppObject_TisIl2CppObject_m2128824987(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Action_1_t985559125 *, const MethodInfo*))Observable_CatchIgnore_TisIl2CppObject_TisIl2CppObject_m2128824987_gshared)(__this /* static, unused */, p0, p1, method)
// UniRx.IObservable`1<!!0> UniRx.Observable::CatchIgnore<System.String,UniRx.WWWErrorException>(UniRx.IObservable`1<!!0>,System.Action`1<!!1>)
#define Observable_CatchIgnore_TisString_t_TisWWWErrorException_t3758994352_m2738378837(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Action_1_t3907447057 *, const MethodInfo*))Observable_CatchIgnore_TisIl2CppObject_TisIl2CppObject_m2128824987_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 UnityEngine.GameObject::AddComponent<UniRx.Triggers.ObservableUpdateTrigger>()
#define GameObject_AddComponent_TisObservableUpdateTrigger_t3433526989_m342345901(__this, method) ((  ObservableUpdateTrigger_t3433526989 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3048268896_gshared)(__this, method)
// UniRx.IObservable`1<!!0> UniRx.Observable::SampleFrame<UniRx.Unit>(UniRx.IObservable`1<!!0>,System.Int32,UniRx.FrameCountType)
extern "C"  Il2CppObject* Observable_SampleFrame_TisUnit_t2558286038_m1174250833_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, int32_t p1, int32_t p2, const MethodInfo* method);
#define Observable_SampleFrame_TisUnit_t2558286038_m1174250833(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, int32_t, int32_t, const MethodInfo*))Observable_SampleFrame_TisUnit_t2558286038_m1174250833_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.IDisposable UniRx.ObservableExtensions::Subscribe<UniRx.Unit>(UniRx.IObservable`1<!!0>,System.Action`1<!!0>,System.Action)
extern "C"  Il2CppObject * ObservableExtensions_Subscribe_TisUnit_t2558286038_m2179646475_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Action_1_t2706738743 * p1, Action_t437523947 * p2, const MethodInfo* method);
#define ObservableExtensions_Subscribe_TisUnit_t2558286038_m2179646475(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Action_1_t2706738743 *, Action_t437523947 *, const MethodInfo*))ObservableExtensions_Subscribe_TisUnit_t2558286038_m2179646475_gshared)(__this /* static, unused */, p0, p1, p2, method)
// UniRx.IObservable`1<!!0> UniRx.Observable::Where<System.Object>(UniRx.IObservable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C"  Il2CppObject* Observable_Where_TisIl2CppObject_m1271830458_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t1509682273 * p1, const MethodInfo* method);
#define Observable_Where_TisIl2CppObject_m1271830458(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1509682273 *, const MethodInfo*))Observable_Where_TisIl2CppObject_m1271830458_gshared)(__this /* static, unused */, p0, p1, method)
// UniRx.IObservable`1<!!0> UniRx.Observable::Where<UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback>(UniRx.IObservable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Observable_Where_TisLogCallback_t3235662730_m1342342965(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t2102879704 *, const MethodInfo*))Observable_Where_TisIl2CppObject_m1271830458_gshared)(__this /* static, unused */, p0, p1, method)
// System.IDisposable UniRx.ObservableExtensions::Subscribe<UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback>(UniRx.IObservable`1<!!0>,System.Action`1<!!0>)
#define ObservableExtensions_Subscribe_TisLogCallback_t3235662730_m968574131(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Action_1_t3384115434 *, const MethodInfo*))ObservableExtensions_Subscribe_TisIl2CppObject_m2475506742_gshared)(__this /* static, unused */, p0, p1, method)
// UniRx.IObservable`1<!!1> UniRx.Observable::FromEvent<System.Object,System.Object>(System.Func`2<System.Action`1<!!1>,!!0>,System.Action`1<!!0>,System.Action`1<!!0>)
extern "C"  Il2CppObject* Observable_FromEvent_TisIl2CppObject_TisIl2CppObject_m2595454071_gshared (Il2CppObject * __this /* static, unused */, Func_2_t1100972979 * p0, Action_1_t985559125 * p1, Action_1_t985559125 * p2, const MethodInfo* method);
#define Observable_FromEvent_TisIl2CppObject_TisIl2CppObject_m2595454071(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Func_2_t1100972979 *, Action_1_t985559125 *, Action_1_t985559125 *, const MethodInfo*))Observable_FromEvent_TisIl2CppObject_TisIl2CppObject_m2595454071_gshared)(__this /* static, unused */, p0, p1, p2, method)
// UniRx.IObservable`1<!!1> UniRx.Observable::FromEvent<UnityEngine.Application/LogCallback,UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback>(System.Func`2<System.Action`1<!!1>,!!0>,System.Action`1<!!0>,System.Action`1<!!0>)
#define Observable_FromEvent_TisLogCallback_t3235662729_TisLogCallback_t3235662730_m2618871987(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Func_2_t4092726719 *, Action_1_t3384115435 *, Action_1_t3384115435 *, const MethodInfo*))Observable_FromEvent_TisIl2CppObject_TisIl2CppObject_m2595454071_gshared)(__this /* static, unused */, p0, p1, p2, method)
// UniRx.IObservable`1<!!0> UniRx.Observable::FromCoroutine<System.String>(System.Func`3<UniRx.IObserver`1<!!0>,UniRx.CancellationToken,System.Collections.IEnumerator>)
#define Observable_FromCoroutine_TisString_t_m3641581413(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Func_3_t212750549 *, const MethodInfo*))Observable_FromCoroutine_TisIl2CppObject_m3257165431_gshared)(__this /* static, unused */, p0, method)
// UnityEngine.Coroutine UniRx.Observable::StartAsCoroutine<System.Int32>(UniRx.IObservable`1<!!0>,System.Action`1<!!0>,UniRx.CancellationToken)
extern "C"  Coroutine_t2246592261 * Observable_StartAsCoroutine_TisInt32_t2847414787_m1205650364_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Action_1_t2995867492 * p1, CancellationToken_t1439151560  p2, const MethodInfo* method);
#define Observable_StartAsCoroutine_TisInt32_t2847414787_m1205650364(__this /* static, unused */, p0, p1, p2, method) ((  Coroutine_t2246592261 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Action_1_t2995867492 *, CancellationToken_t1439151560 , const MethodInfo*))Observable_StartAsCoroutine_TisInt32_t2847414787_m1205650364_gshared)(__this /* static, unused */, p0, p1, p2, method)
// UniRx.IObservable`1<!!0> UniRx.Observable::Return<System.Int32>(!!0)
extern "C"  Il2CppObject* Observable_Return_TisInt32_t2847414787_m3069752271_gshared (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method);
#define Observable_Return_TisInt32_t2847414787_m3069752271(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))Observable_Return_TisInt32_t2847414787_m3069752271_gshared)(__this /* static, unused */, p0, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AdToApp.AdToAppBinding::.ctor()
extern "C"  void AdToAppBinding__ctor_m914889442 (AdToAppBinding_t3015697107 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AdToApp.AdToAppBinding::.cctor()
extern "C"  void AdToAppBinding__cctor_m2109672715 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void AdToApp.AdToAppBinding::start(System.String,System.String)
extern Il2CppClass* AdToAppBinding_t3015697107_il2cpp_TypeInfo_var;
extern const uint32_t AdToAppBinding_start_m1693568636_MetadataUsageId;
extern "C"  void AdToAppBinding_start_m1693568636 (Il2CppObject * __this /* static, unused */, String_t* ___adContentType0, String_t* ___appId1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppBinding_start_m1693568636_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___appId1;
		String_t* L_1 = ___adContentType0;
		IL2CPP_RUNTIME_CLASS_INIT(AdToAppBinding_t3015697107_il2cpp_TypeInfo_var);
		AdToAppBinding_AdToApp_start_platform_m2824761840(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AdToApp.AdToAppBinding::setLogLevel(AdToApp.AdToAppLogLevel)
extern Il2CppClass* AdToAppBinding_t3015697107_il2cpp_TypeInfo_var;
extern const uint32_t AdToAppBinding_setLogLevel_m1570455448_MetadataUsageId;
extern "C"  void AdToAppBinding_setLogLevel_m1570455448 (Il2CppObject * __this /* static, unused */, int32_t ___logLevel0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppBinding_setLogLevel_m1570455448_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___logLevel0;
		IL2CPP_RUNTIME_CLASS_INIT(AdToAppBinding_t3015697107_il2cpp_TypeInfo_var);
		AdToAppBinding_AdToApp_setLogLevel_platform_m660456159(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean AdToApp.AdToAppBinding::showInterstitial(System.String)
extern Il2CppClass* AdToAppBinding_t3015697107_il2cpp_TypeInfo_var;
extern const uint32_t AdToAppBinding_showInterstitial_m1419824827_MetadataUsageId;
extern "C"  bool AdToAppBinding_showInterstitial_m1419824827 (Il2CppObject * __this /* static, unused */, String_t* ___adContentType0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppBinding_showInterstitial_m1419824827_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___adContentType0;
		IL2CPP_RUNTIME_CLASS_INIT(AdToAppBinding_t3015697107_il2cpp_TypeInfo_var);
		bool L_1 = AdToAppBinding_AdToApp_showInterstitial_platform_m1626468945(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean AdToApp.AdToAppBinding::hasInterstitial(System.String)
extern Il2CppClass* AdToAppBinding_t3015697107_il2cpp_TypeInfo_var;
extern const uint32_t AdToAppBinding_hasInterstitial_m4192788120_MetadataUsageId;
extern "C"  bool AdToAppBinding_hasInterstitial_m4192788120 (Il2CppObject * __this /* static, unused */, String_t* ___adContentType0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppBinding_hasInterstitial_m4192788120_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___adContentType0;
		IL2CPP_RUNTIME_CLASS_INIT(AdToAppBinding_t3015697107_il2cpp_TypeInfo_var);
		bool L_1 = AdToAppBinding_AdToApp_hasInterstitial_platform_m451624540(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean AdToApp.AdToAppBinding::isInterstitialDisplayed()
extern Il2CppClass* AdToAppBinding_t3015697107_il2cpp_TypeInfo_var;
extern const uint32_t AdToAppBinding_isInterstitialDisplayed_m1801670031_MetadataUsageId;
extern "C"  bool AdToAppBinding_isInterstitialDisplayed_m1801670031 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppBinding_isInterstitialDisplayed_m1801670031_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AdToAppBinding_t3015697107_il2cpp_TypeInfo_var);
		bool L_0 = AdToAppBinding_AdToApp_isInterstitialDisplayed_platform_m2005541665(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void AdToApp.AdToAppBinding::showBanner(System.String,System.Single,System.Single,System.Single,System.Single)
extern Il2CppClass* AdToAppBinding_t3015697107_il2cpp_TypeInfo_var;
extern const uint32_t AdToAppBinding_showBanner_m2188649323_MetadataUsageId;
extern "C"  void AdToAppBinding_showBanner_m2188649323 (Il2CppObject * __this /* static, unused */, String_t* ___bannerSize0, float ___x1, float ___y2, float ___width3, float ___height4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppBinding_showBanner_m2188649323_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___x1;
		float L_1 = ___y2;
		float L_2 = ___width3;
		float L_3 = ___height4;
		String_t* L_4 = ___bannerSize0;
		IL2CPP_RUNTIME_CLASS_INIT(AdToAppBinding_t3015697107_il2cpp_TypeInfo_var);
		AdToAppBinding_AdToApp_showBanner_platform_m119572809(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AdToApp.AdToAppBinding::showBannerAtPosition(System.String,System.String,System.Single,System.Single,System.Single,System.Single)
extern Il2CppClass* AdToAppBinding_t3015697107_il2cpp_TypeInfo_var;
extern const uint32_t AdToAppBinding_showBannerAtPosition_m617533131_MetadataUsageId;
extern "C"  void AdToAppBinding_showBannerAtPosition_m617533131 (Il2CppObject * __this /* static, unused */, String_t* ___position0, String_t* ___bannerSize1, float ___marginTop2, float ___marginLeft3, float ___marginBottom4, float ___marginRight5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppBinding_showBannerAtPosition_m617533131_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___position0;
		String_t* L_1 = ___bannerSize1;
		float L_2 = ___marginTop2;
		float L_3 = ___marginLeft3;
		float L_4 = ___marginBottom4;
		float L_5 = ___marginRight5;
		IL2CPP_RUNTIME_CLASS_INIT(AdToAppBinding_t3015697107_il2cpp_TypeInfo_var);
		AdToAppBinding_AdToApp_showBannerAtPosition_platform_m3180678625(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AdToApp.AdToAppBinding::loadNextBanner()
extern Il2CppClass* AdToAppBinding_t3015697107_il2cpp_TypeInfo_var;
extern const uint32_t AdToAppBinding_loadNextBanner_m566023431_MetadataUsageId;
extern "C"  void AdToAppBinding_loadNextBanner_m566023431 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppBinding_loadNextBanner_m566023431_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AdToAppBinding_t3015697107_il2cpp_TypeInfo_var);
		AdToAppBinding_AdToApp_loadNextBanner_platform_m2891887601(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AdToApp.AdToAppBinding::setBannerRefreshInterval(System.Double)
extern Il2CppClass* AdToAppBinding_t3015697107_il2cpp_TypeInfo_var;
extern const uint32_t AdToAppBinding_setBannerRefreshInterval_m3479436302_MetadataUsageId;
extern "C"  void AdToAppBinding_setBannerRefreshInterval_m3479436302 (Il2CppObject * __this /* static, unused */, double ___refreshInterval0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppBinding_setBannerRefreshInterval_m3479436302_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		double L_0 = ___refreshInterval0;
		IL2CPP_RUNTIME_CLASS_INIT(AdToAppBinding_t3015697107_il2cpp_TypeInfo_var);
		AdToAppBinding_AdToApp_setBannerRefreshInterval_platform_m278725342(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AdToApp.AdToAppBinding::removeAllBanners()
extern Il2CppClass* AdToAppBinding_t3015697107_il2cpp_TypeInfo_var;
extern const uint32_t AdToAppBinding_removeAllBanners_m1851435916_MetadataUsageId;
extern "C"  void AdToAppBinding_removeAllBanners_m1851435916 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppBinding_removeAllBanners_m1851435916_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AdToAppBinding_t3015697107_il2cpp_TypeInfo_var);
		AdToAppBinding_AdToApp_removeAllBanners_platform_m1600797196(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AdToApp.AdToAppBinding::setTestMode(System.Boolean)
extern "C"  void AdToAppBinding_setTestMode_m2621369390 (Il2CppObject * __this /* static, unused */, bool ___isEnabled0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.String AdToApp.AdToAppBinding::getRewardedCurrency()
extern Il2CppClass* AdToAppBinding_t3015697107_il2cpp_TypeInfo_var;
extern const uint32_t AdToAppBinding_getRewardedCurrency_m340757522_MetadataUsageId;
extern "C"  String_t* AdToAppBinding_getRewardedCurrency_m340757522 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppBinding_getRewardedCurrency_m340757522_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AdToAppBinding_t3015697107_il2cpp_TypeInfo_var);
		String_t* L_0 = AdToAppBinding_AdToApp_rewardedCurrency_platform_m2748434740(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String AdToApp.AdToAppBinding::getRewardedValue()
extern Il2CppClass* AdToAppBinding_t3015697107_il2cpp_TypeInfo_var;
extern const uint32_t AdToAppBinding_getRewardedValue_m3495948018_MetadataUsageId;
extern "C"  String_t* AdToAppBinding_getRewardedValue_m3495948018 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppBinding_getRewardedValue_m3495948018_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AdToAppBinding_t3015697107_il2cpp_TypeInfo_var);
		String_t* L_0 = AdToAppBinding_AdToApp_rewardedValue_platform_m991686128(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void AdToApp.AdToAppBinding::setTargetingParam(System.String,System.String)
extern Il2CppClass* AdToAppBinding_t3015697107_il2cpp_TypeInfo_var;
extern const uint32_t AdToAppBinding_setTargetingParam_m1383159680_MetadataUsageId;
extern "C"  void AdToAppBinding_setTargetingParam_m1383159680 (Il2CppObject * __this /* static, unused */, String_t* ___parameterName0, String_t* ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppBinding_setTargetingParam_m1383159680_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___parameterName0;
		String_t* L_1 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(AdToAppBinding_t3015697107_il2cpp_TypeInfo_var);
		AdToAppBinding_AdToApp_setTargetingParam_platform_m1257445228(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AdToApp.AdToAppBinding::setCallbacks(AdToApp.AdToAppSDKDelegate)
extern Il2CppClass* AdToAppBinding_t3015697107_il2cpp_TypeInfo_var;
extern const uint32_t AdToAppBinding_setCallbacks_m2190494663_MetadataUsageId;
extern "C"  void AdToAppBinding_setCallbacks_m2190494663 (Il2CppObject * __this /* static, unused */, AdToAppSDKDelegate_t2853395885 * ___sdkDelegate0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppBinding_setCallbacks_m2190494663_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AdToAppSDKDelegate_t2853395885 * L_0 = ___sdkDelegate0;
		NullCheck(L_0);
		String_t* L_1 = Object_get_name_m3709440845(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(AdToAppBinding_t3015697107_il2cpp_TypeInfo_var);
		AdToAppBinding_AdToApp_setUnityCallbackTargetName_platform_m3138860064(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AdToApp.AdToAppBinding::onPause(System.Boolean)
extern "C"  void AdToAppBinding_onPause_m1474729774 (Il2CppObject * __this /* static, unused */, bool ___pauseStatus0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void AdToApp.AdToAppBinding::setIOSCallback(UnityEngine.MonoBehaviour)
extern Il2CppClass* AdToAppBinding_t3015697107_il2cpp_TypeInfo_var;
extern const uint32_t AdToAppBinding_setIOSCallback_m795486493_MetadataUsageId;
extern "C"  void AdToAppBinding_setIOSCallback_m795486493 (Il2CppObject * __this /* static, unused */, MonoBehaviour_t3012272455 * ___sdkDelegate0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppBinding_setIOSCallback_m795486493_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		MonoBehaviour_t3012272455 * L_0 = ___sdkDelegate0;
		IL2CPP_RUNTIME_CLASS_INIT(AdToAppBinding_t3015697107_il2cpp_TypeInfo_var);
		((AdToAppBinding_t3015697107_StaticFields*)AdToAppBinding_t3015697107_il2cpp_TypeInfo_var->static_fields)->set__sdkDelegate_0(L_0);
		MonoBehaviour_t3012272455 * L_1 = ___sdkDelegate0;
		NullCheck(L_1);
		String_t* L_2 = Object_get_name_m3709440845(L_1, /*hidden argument*/NULL);
		AdToAppBinding_AdToApp_setUnityCallbackTargetName_platform_m3138860064(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AdToApp.AdToAppBinding::AdToApp_start_platform(System.String,System.String)
extern "C" void DEFAULT_CALL AdToApp_start_platform(char*, char*);
extern "C"  void AdToAppBinding_AdToApp_start_platform_m2824761840 (Il2CppObject * __this /* static, unused */, String_t* ___appId0, String_t* ___adContentType1, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*);

	// Marshaling of parameter '___appId0' to native representation
	char* ____appId0_marshaled = NULL;
	____appId0_marshaled = il2cpp_codegen_marshal_string(___appId0);

	// Marshaling of parameter '___adContentType1' to native representation
	char* ____adContentType1_marshaled = NULL;
	____adContentType1_marshaled = il2cpp_codegen_marshal_string(___adContentType1);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(AdToApp_start_platform)(____appId0_marshaled, ____adContentType1_marshaled);

	// Marshaling cleanup of parameter '___appId0' native representation
	il2cpp_codegen_marshal_free(____appId0_marshaled);
	____appId0_marshaled = NULL;

	// Marshaling cleanup of parameter '___adContentType1' native representation
	il2cpp_codegen_marshal_free(____adContentType1_marshaled);
	____adContentType1_marshaled = NULL;

}
// System.Void AdToApp.AdToAppBinding::AdToApp_setUnityCallbackTargetName_platform(System.String)
extern "C" void DEFAULT_CALL AdToApp_setUnityCallbackTargetName_platform(char*);
extern "C"  void AdToAppBinding_AdToApp_setUnityCallbackTargetName_platform_m3138860064 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___name0' to native representation
	char* ____name0_marshaled = NULL;
	____name0_marshaled = il2cpp_codegen_marshal_string(___name0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(AdToApp_setUnityCallbackTargetName_platform)(____name0_marshaled);

	// Marshaling cleanup of parameter '___name0' native representation
	il2cpp_codegen_marshal_free(____name0_marshaled);
	____name0_marshaled = NULL;

}
// System.Boolean AdToApp.AdToAppBinding::AdToApp_isInterstitialDisplayed_platform()
extern "C" int32_t DEFAULT_CALL AdToApp_isInterstitialDisplayed_platform();
extern "C"  bool AdToAppBinding_AdToApp_isInterstitialDisplayed_platform_m2005541665 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = reinterpret_cast<PInvokeFunc>(AdToApp_isInterstitialDisplayed_platform)();

	return _return_value;
}
// System.Boolean AdToApp.AdToAppBinding::AdToApp_hasInterstitial_platform(System.String)
extern "C" int32_t DEFAULT_CALL AdToApp_hasInterstitial_platform(char*);
extern "C"  bool AdToAppBinding_AdToApp_hasInterstitial_platform_m451624540 (Il2CppObject * __this /* static, unused */, String_t* ___adContentType0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___adContentType0' to native representation
	char* ____adContentType0_marshaled = NULL;
	____adContentType0_marshaled = il2cpp_codegen_marshal_string(___adContentType0);

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = reinterpret_cast<PInvokeFunc>(AdToApp_hasInterstitial_platform)(____adContentType0_marshaled);

	// Marshaling cleanup of parameter '___adContentType0' native representation
	il2cpp_codegen_marshal_free(____adContentType0_marshaled);
	____adContentType0_marshaled = NULL;

	return _return_value;
}
// System.Void AdToApp.AdToAppBinding::AdToApp_removeAllBanners_platform()
extern "C" void DEFAULT_CALL AdToApp_removeAllBanners_platform();
extern "C"  void AdToAppBinding_AdToApp_removeAllBanners_platform_m1600797196 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(AdToApp_removeAllBanners_platform)();

}
// System.Void AdToApp.AdToAppBinding::AdToApp_showBannerAtPosition_platform(System.String,System.String,System.Single,System.Single,System.Single,System.Single)
extern "C" void DEFAULT_CALL AdToApp_showBannerAtPosition_platform(char*, char*, float, float, float, float);
extern "C"  void AdToAppBinding_AdToApp_showBannerAtPosition_platform_m3180678625 (Il2CppObject * __this /* static, unused */, String_t* ___position0, String_t* ___bannerSize1, float ___marginTop2, float ___marginLeft3, float ___marginBottom4, float ___marginRight5, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*, float, float, float, float);

	// Marshaling of parameter '___position0' to native representation
	char* ____position0_marshaled = NULL;
	____position0_marshaled = il2cpp_codegen_marshal_string(___position0);

	// Marshaling of parameter '___bannerSize1' to native representation
	char* ____bannerSize1_marshaled = NULL;
	____bannerSize1_marshaled = il2cpp_codegen_marshal_string(___bannerSize1);

	// Marshaling of parameter '___marginTop2' to native representation

	// Marshaling of parameter '___marginLeft3' to native representation

	// Marshaling of parameter '___marginBottom4' to native representation

	// Marshaling of parameter '___marginRight5' to native representation

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(AdToApp_showBannerAtPosition_platform)(____position0_marshaled, ____bannerSize1_marshaled, ___marginTop2, ___marginLeft3, ___marginBottom4, ___marginRight5);

	// Marshaling cleanup of parameter '___position0' native representation
	il2cpp_codegen_marshal_free(____position0_marshaled);
	____position0_marshaled = NULL;

	// Marshaling cleanup of parameter '___bannerSize1' native representation
	il2cpp_codegen_marshal_free(____bannerSize1_marshaled);
	____bannerSize1_marshaled = NULL;

	// Marshaling cleanup of parameter '___marginTop2' native representation

	// Marshaling cleanup of parameter '___marginLeft3' native representation

	// Marshaling cleanup of parameter '___marginBottom4' native representation

	// Marshaling cleanup of parameter '___marginRight5' native representation

}
// System.Void AdToApp.AdToAppBinding::AdToApp_showBanner_platform(System.Single,System.Single,System.Single,System.Single,System.String)
extern "C" void DEFAULT_CALL AdToApp_showBanner_platform(float, float, float, float, char*);
extern "C"  void AdToAppBinding_AdToApp_showBanner_platform_m119572809 (Il2CppObject * __this /* static, unused */, float ___x0, float ___y1, float ___width2, float ___height3, String_t* ___bannerSize4, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (float, float, float, float, char*);

	// Marshaling of parameter '___x0' to native representation

	// Marshaling of parameter '___y1' to native representation

	// Marshaling of parameter '___width2' to native representation

	// Marshaling of parameter '___height3' to native representation

	// Marshaling of parameter '___bannerSize4' to native representation
	char* ____bannerSize4_marshaled = NULL;
	____bannerSize4_marshaled = il2cpp_codegen_marshal_string(___bannerSize4);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(AdToApp_showBanner_platform)(___x0, ___y1, ___width2, ___height3, ____bannerSize4_marshaled);

	// Marshaling cleanup of parameter '___x0' native representation

	// Marshaling cleanup of parameter '___y1' native representation

	// Marshaling cleanup of parameter '___width2' native representation

	// Marshaling cleanup of parameter '___height3' native representation

	// Marshaling cleanup of parameter '___bannerSize4' native representation
	il2cpp_codegen_marshal_free(____bannerSize4_marshaled);
	____bannerSize4_marshaled = NULL;

}
// System.Void AdToApp.AdToAppBinding::AdToApp_setBannerRefreshInterval_platform(System.Double)
extern "C" void DEFAULT_CALL AdToApp_setBannerRefreshInterval_platform(double);
extern "C"  void AdToAppBinding_AdToApp_setBannerRefreshInterval_platform_m278725342 (Il2CppObject * __this /* static, unused */, double ___refreshInterval0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (double);

	// Marshaling of parameter '___refreshInterval0' to native representation

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(AdToApp_setBannerRefreshInterval_platform)(___refreshInterval0);

	// Marshaling cleanup of parameter '___refreshInterval0' native representation

}
// System.Void AdToApp.AdToAppBinding::AdToApp_setLogLevel_platform(System.Int32)
extern "C" void DEFAULT_CALL AdToApp_setLogLevel_platform(int32_t);
extern "C"  void AdToAppBinding_AdToApp_setLogLevel_platform_m660456159 (Il2CppObject * __this /* static, unused */, int32_t ___logLevel0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Marshaling of parameter '___logLevel0' to native representation

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(AdToApp_setLogLevel_platform)(___logLevel0);

	// Marshaling cleanup of parameter '___logLevel0' native representation

}
// System.Boolean AdToApp.AdToAppBinding::AdToApp_showInterstitial_platform(System.String)
extern "C" int32_t DEFAULT_CALL AdToApp_showInterstitial_platform(char*);
extern "C"  bool AdToAppBinding_AdToApp_showInterstitial_platform_m1626468945 (Il2CppObject * __this /* static, unused */, String_t* ___adContentType0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___adContentType0' to native representation
	char* ____adContentType0_marshaled = NULL;
	____adContentType0_marshaled = il2cpp_codegen_marshal_string(___adContentType0);

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = reinterpret_cast<PInvokeFunc>(AdToApp_showInterstitial_platform)(____adContentType0_marshaled);

	// Marshaling cleanup of parameter '___adContentType0' native representation
	il2cpp_codegen_marshal_free(____adContentType0_marshaled);
	____adContentType0_marshaled = NULL;

	return _return_value;
}
// System.Void AdToApp.AdToAppBinding::AdToApp_loadNextBanner_platform()
extern "C" void DEFAULT_CALL AdToApp_loadNextBanner_platform();
extern "C"  void AdToAppBinding_AdToApp_loadNextBanner_platform_m2891887601 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(AdToApp_loadNextBanner_platform)();

}
// System.Void AdToApp.AdToAppBinding::AdToApp_setTargetingParam_platform(System.String,System.String)
extern "C" void DEFAULT_CALL AdToApp_setTargetingParam_platform(char*, char*);
extern "C"  void AdToAppBinding_AdToApp_setTargetingParam_platform_m1257445228 (Il2CppObject * __this /* static, unused */, String_t* ___parameterName0, String_t* ___value1, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*);

	// Marshaling of parameter '___parameterName0' to native representation
	char* ____parameterName0_marshaled = NULL;
	____parameterName0_marshaled = il2cpp_codegen_marshal_string(___parameterName0);

	// Marshaling of parameter '___value1' to native representation
	char* ____value1_marshaled = NULL;
	____value1_marshaled = il2cpp_codegen_marshal_string(___value1);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(AdToApp_setTargetingParam_platform)(____parameterName0_marshaled, ____value1_marshaled);

	// Marshaling cleanup of parameter '___parameterName0' native representation
	il2cpp_codegen_marshal_free(____parameterName0_marshaled);
	____parameterName0_marshaled = NULL;

	// Marshaling cleanup of parameter '___value1' native representation
	il2cpp_codegen_marshal_free(____value1_marshaled);
	____value1_marshaled = NULL;

}
// System.String AdToApp.AdToAppBinding::AdToApp_rewardedCurrency_platform()
extern "C" char* DEFAULT_CALL AdToApp_rewardedCurrency_platform();
extern "C"  String_t* AdToAppBinding_AdToApp_rewardedCurrency_platform_m2748434740 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation and marshaling of return value back from native representation
	char* _return_value = reinterpret_cast<PInvokeFunc>(AdToApp_rewardedCurrency_platform)();
	String_t* __return_value_unmarshaled = NULL;
	__return_value_unmarshaled = il2cpp_codegen_marshal_string_result(_return_value);
	il2cpp_codegen_marshal_free(_return_value);
	_return_value = NULL;

	return __return_value_unmarshaled;
}
// System.String AdToApp.AdToAppBinding::AdToApp_rewardedValue_platform()
extern "C" char* DEFAULT_CALL AdToApp_rewardedValue_platform();
extern "C"  String_t* AdToAppBinding_AdToApp_rewardedValue_platform_m991686128 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation and marshaling of return value back from native representation
	char* _return_value = reinterpret_cast<PInvokeFunc>(AdToApp_rewardedValue_platform)();
	String_t* __return_value_unmarshaled = NULL;
	__return_value_unmarshaled = il2cpp_codegen_marshal_string_result(_return_value);
	il2cpp_codegen_marshal_free(_return_value);
	_return_value = NULL;

	return __return_value_unmarshaled;
}
// System.Void AdToApp.AdToAppSDKDelegate::.ctor()
extern "C"  void AdToAppSDKDelegate__ctor_m3716369608 (AdToAppSDKDelegate_t2853395885 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m350695606(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AdToApp.AdToAppSDKDelegate::add_OnInterstitialStarted(System.Action`2<System.String,System.String>)
extern Il2CppClass* Action_2_t2887221574_il2cpp_TypeInfo_var;
extern const uint32_t AdToAppSDKDelegate_add_OnInterstitialStarted_m2918704575_MetadataUsageId;
extern "C"  void AdToAppSDKDelegate_add_OnInterstitialStarted_m2918704575 (AdToAppSDKDelegate_t2853395885 * __this, Action_2_t2887221574 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppSDKDelegate_add_OnInterstitialStarted_m2918704575_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_2_t2887221574 * L_0 = __this->get_OnInterstitialStarted_2();
		Action_2_t2887221574 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_OnInterstitialStarted_2(((Action_2_t2887221574 *)CastclassSealed(L_2, Action_2_t2887221574_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void AdToApp.AdToAppSDKDelegate::remove_OnInterstitialStarted(System.Action`2<System.String,System.String>)
extern Il2CppClass* Action_2_t2887221574_il2cpp_TypeInfo_var;
extern const uint32_t AdToAppSDKDelegate_remove_OnInterstitialStarted_m2274127864_MetadataUsageId;
extern "C"  void AdToAppSDKDelegate_remove_OnInterstitialStarted_m2274127864 (AdToAppSDKDelegate_t2853395885 * __this, Action_2_t2887221574 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppSDKDelegate_remove_OnInterstitialStarted_m2274127864_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_2_t2887221574 * L_0 = __this->get_OnInterstitialStarted_2();
		Action_2_t2887221574 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_OnInterstitialStarted_2(((Action_2_t2887221574 *)CastclassSealed(L_2, Action_2_t2887221574_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void AdToApp.AdToAppSDKDelegate::add_OnInterstitialClosed(System.Action`2<System.String,System.String>)
extern Il2CppClass* Action_2_t2887221574_il2cpp_TypeInfo_var;
extern const uint32_t AdToAppSDKDelegate_add_OnInterstitialClosed_m2977913650_MetadataUsageId;
extern "C"  void AdToAppSDKDelegate_add_OnInterstitialClosed_m2977913650 (AdToAppSDKDelegate_t2853395885 * __this, Action_2_t2887221574 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppSDKDelegate_add_OnInterstitialClosed_m2977913650_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_2_t2887221574 * L_0 = __this->get_OnInterstitialClosed_3();
		Action_2_t2887221574 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_OnInterstitialClosed_3(((Action_2_t2887221574 *)CastclassSealed(L_2, Action_2_t2887221574_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void AdToApp.AdToAppSDKDelegate::remove_OnInterstitialClosed(System.Action`2<System.String,System.String>)
extern Il2CppClass* Action_2_t2887221574_il2cpp_TypeInfo_var;
extern const uint32_t AdToAppSDKDelegate_remove_OnInterstitialClosed_m3095668185_MetadataUsageId;
extern "C"  void AdToAppSDKDelegate_remove_OnInterstitialClosed_m3095668185 (AdToAppSDKDelegate_t2853395885 * __this, Action_2_t2887221574 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppSDKDelegate_remove_OnInterstitialClosed_m3095668185_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_2_t2887221574 * L_0 = __this->get_OnInterstitialClosed_3();
		Action_2_t2887221574 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_OnInterstitialClosed_3(((Action_2_t2887221574 *)CastclassSealed(L_2, Action_2_t2887221574_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void AdToApp.AdToAppSDKDelegate::add_OnInterstitialFailedToAppear(System.Action`2<System.String,System.String>)
extern Il2CppClass* Action_2_t2887221574_il2cpp_TypeInfo_var;
extern const uint32_t AdToAppSDKDelegate_add_OnInterstitialFailedToAppear_m2222989139_MetadataUsageId;
extern "C"  void AdToAppSDKDelegate_add_OnInterstitialFailedToAppear_m2222989139 (AdToAppSDKDelegate_t2853395885 * __this, Action_2_t2887221574 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppSDKDelegate_add_OnInterstitialFailedToAppear_m2222989139_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_2_t2887221574 * L_0 = __this->get_OnInterstitialFailedToAppear_4();
		Action_2_t2887221574 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_OnInterstitialFailedToAppear_4(((Action_2_t2887221574 *)CastclassSealed(L_2, Action_2_t2887221574_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void AdToApp.AdToAppSDKDelegate::remove_OnInterstitialFailedToAppear(System.Action`2<System.String,System.String>)
extern Il2CppClass* Action_2_t2887221574_il2cpp_TypeInfo_var;
extern const uint32_t AdToAppSDKDelegate_remove_OnInterstitialFailedToAppear_m4290728698_MetadataUsageId;
extern "C"  void AdToAppSDKDelegate_remove_OnInterstitialFailedToAppear_m4290728698 (AdToAppSDKDelegate_t2853395885 * __this, Action_2_t2887221574 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppSDKDelegate_remove_OnInterstitialFailedToAppear_m4290728698_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_2_t2887221574 * L_0 = __this->get_OnInterstitialFailedToAppear_4();
		Action_2_t2887221574 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_OnInterstitialFailedToAppear_4(((Action_2_t2887221574 *)CastclassSealed(L_2, Action_2_t2887221574_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void AdToApp.AdToAppSDKDelegate::add_OnInterstitialClicked(System.Action`2<System.String,System.String>)
extern Il2CppClass* Action_2_t2887221574_il2cpp_TypeInfo_var;
extern const uint32_t AdToAppSDKDelegate_add_OnInterstitialClicked_m1662632677_MetadataUsageId;
extern "C"  void AdToAppSDKDelegate_add_OnInterstitialClicked_m1662632677 (AdToAppSDKDelegate_t2853395885 * __this, Action_2_t2887221574 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppSDKDelegate_add_OnInterstitialClicked_m1662632677_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_2_t2887221574 * L_0 = __this->get_OnInterstitialClicked_5();
		Action_2_t2887221574 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_OnInterstitialClicked_5(((Action_2_t2887221574 *)CastclassSealed(L_2, Action_2_t2887221574_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void AdToApp.AdToAppSDKDelegate::remove_OnInterstitialClicked(System.Action`2<System.String,System.String>)
extern Il2CppClass* Action_2_t2887221574_il2cpp_TypeInfo_var;
extern const uint32_t AdToAppSDKDelegate_remove_OnInterstitialClicked_m1018055966_MetadataUsageId;
extern "C"  void AdToAppSDKDelegate_remove_OnInterstitialClicked_m1018055966 (AdToAppSDKDelegate_t2853395885 * __this, Action_2_t2887221574 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppSDKDelegate_remove_OnInterstitialClicked_m1018055966_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_2_t2887221574 * L_0 = __this->get_OnInterstitialClicked_5();
		Action_2_t2887221574 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_OnInterstitialClicked_5(((Action_2_t2887221574 *)CastclassSealed(L_2, Action_2_t2887221574_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void AdToApp.AdToAppSDKDelegate::add_OnFirstInterstitialLoad(System.Action`2<System.String,System.String>)
extern Il2CppClass* Action_2_t2887221574_il2cpp_TypeInfo_var;
extern const uint32_t AdToAppSDKDelegate_add_OnFirstInterstitialLoad_m191452748_MetadataUsageId;
extern "C"  void AdToAppSDKDelegate_add_OnFirstInterstitialLoad_m191452748 (AdToAppSDKDelegate_t2853395885 * __this, Action_2_t2887221574 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppSDKDelegate_add_OnFirstInterstitialLoad_m191452748_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_2_t2887221574 * L_0 = __this->get_OnFirstInterstitialLoad_6();
		Action_2_t2887221574 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_OnFirstInterstitialLoad_6(((Action_2_t2887221574 *)CastclassSealed(L_2, Action_2_t2887221574_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void AdToApp.AdToAppSDKDelegate::remove_OnFirstInterstitialLoad(System.Action`2<System.String,System.String>)
extern Il2CppClass* Action_2_t2887221574_il2cpp_TypeInfo_var;
extern const uint32_t AdToAppSDKDelegate_remove_OnFirstInterstitialLoad_m3523491397_MetadataUsageId;
extern "C"  void AdToAppSDKDelegate_remove_OnFirstInterstitialLoad_m3523491397 (AdToAppSDKDelegate_t2853395885 * __this, Action_2_t2887221574 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppSDKDelegate_remove_OnFirstInterstitialLoad_m3523491397_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_2_t2887221574 * L_0 = __this->get_OnFirstInterstitialLoad_6();
		Action_2_t2887221574 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_OnFirstInterstitialLoad_6(((Action_2_t2887221574 *)CastclassSealed(L_2, Action_2_t2887221574_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void AdToApp.AdToAppSDKDelegate::add_OnRewardedCompleted(System.Action`3<System.String,System.String,System.String>)
extern Il2CppClass* Action_3_t2150340505_il2cpp_TypeInfo_var;
extern const uint32_t AdToAppSDKDelegate_add_OnRewardedCompleted_m3137603692_MetadataUsageId;
extern "C"  void AdToAppSDKDelegate_add_OnRewardedCompleted_m3137603692 (AdToAppSDKDelegate_t2853395885 * __this, Action_3_t2150340505 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppSDKDelegate_add_OnRewardedCompleted_m3137603692_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_3_t2150340505 * L_0 = __this->get_OnRewardedCompleted_7();
		Action_3_t2150340505 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_OnRewardedCompleted_7(((Action_3_t2150340505 *)CastclassSealed(L_2, Action_3_t2150340505_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void AdToApp.AdToAppSDKDelegate::remove_OnRewardedCompleted(System.Action`3<System.String,System.String,System.String>)
extern Il2CppClass* Action_3_t2150340505_il2cpp_TypeInfo_var;
extern const uint32_t AdToAppSDKDelegate_remove_OnRewardedCompleted_m2470839589_MetadataUsageId;
extern "C"  void AdToAppSDKDelegate_remove_OnRewardedCompleted_m2470839589 (AdToAppSDKDelegate_t2853395885 * __this, Action_3_t2150340505 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppSDKDelegate_remove_OnRewardedCompleted_m2470839589_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_3_t2150340505 * L_0 = __this->get_OnRewardedCompleted_7();
		Action_3_t2150340505 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_OnRewardedCompleted_7(((Action_3_t2150340505 *)CastclassSealed(L_2, Action_3_t2150340505_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void AdToApp.AdToAppSDKDelegate::add_OnBannerLoad(System.Action)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t AdToAppSDKDelegate_add_OnBannerLoad_m2000076754_MetadataUsageId;
extern "C"  void AdToAppSDKDelegate_add_OnBannerLoad_m2000076754 (AdToAppSDKDelegate_t2853395885 * __this, Action_t437523947 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppSDKDelegate_add_OnBannerLoad_m2000076754_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t437523947 * L_0 = __this->get_OnBannerLoad_8();
		Action_t437523947 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_OnBannerLoad_8(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void AdToApp.AdToAppSDKDelegate::remove_OnBannerLoad(System.Action)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t AdToAppSDKDelegate_remove_OnBannerLoad_m681883403_MetadataUsageId;
extern "C"  void AdToAppSDKDelegate_remove_OnBannerLoad_m681883403 (AdToAppSDKDelegate_t2853395885 * __this, Action_t437523947 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppSDKDelegate_remove_OnBannerLoad_m681883403_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t437523947 * L_0 = __this->get_OnBannerLoad_8();
		Action_t437523947 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_OnBannerLoad_8(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void AdToApp.AdToAppSDKDelegate::add_OnBannerFailedToLoad(System.Action`1<System.String>)
extern Il2CppClass* Action_1_t1116941607_il2cpp_TypeInfo_var;
extern const uint32_t AdToAppSDKDelegate_add_OnBannerFailedToLoad_m1386684831_MetadataUsageId;
extern "C"  void AdToAppSDKDelegate_add_OnBannerFailedToLoad_m1386684831 (AdToAppSDKDelegate_t2853395885 * __this, Action_1_t1116941607 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppSDKDelegate_add_OnBannerFailedToLoad_m1386684831_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t1116941607 * L_0 = __this->get_OnBannerFailedToLoad_9();
		Action_1_t1116941607 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_OnBannerFailedToLoad_9(((Action_1_t1116941607 *)CastclassSealed(L_2, Action_1_t1116941607_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void AdToApp.AdToAppSDKDelegate::remove_OnBannerFailedToLoad(System.Action`1<System.String>)
extern Il2CppClass* Action_1_t1116941607_il2cpp_TypeInfo_var;
extern const uint32_t AdToAppSDKDelegate_remove_OnBannerFailedToLoad_m1047563398_MetadataUsageId;
extern "C"  void AdToAppSDKDelegate_remove_OnBannerFailedToLoad_m1047563398 (AdToAppSDKDelegate_t2853395885 * __this, Action_1_t1116941607 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppSDKDelegate_remove_OnBannerFailedToLoad_m1047563398_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t1116941607 * L_0 = __this->get_OnBannerFailedToLoad_9();
		Action_1_t1116941607 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_OnBannerFailedToLoad_9(((Action_1_t1116941607 *)CastclassSealed(L_2, Action_1_t1116941607_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void AdToApp.AdToAppSDKDelegate::add_OnBannerClicked(System.Action)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t AdToAppSDKDelegate_add_OnBannerClicked_m3016283705_MetadataUsageId;
extern "C"  void AdToAppSDKDelegate_add_OnBannerClicked_m3016283705 (AdToAppSDKDelegate_t2853395885 * __this, Action_t437523947 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppSDKDelegate_add_OnBannerClicked_m3016283705_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t437523947 * L_0 = __this->get_OnBannerClicked_10();
		Action_t437523947 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_OnBannerClicked_10(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void AdToApp.AdToAppSDKDelegate::remove_OnBannerClicked(System.Action)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t AdToAppSDKDelegate_remove_OnBannerClicked_m1604151392_MetadataUsageId;
extern "C"  void AdToAppSDKDelegate_remove_OnBannerClicked_m1604151392 (AdToAppSDKDelegate_t2853395885 * __this, Action_t437523947 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppSDKDelegate_remove_OnBannerClicked_m1604151392_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t437523947 * L_0 = __this->get_OnBannerClicked_10();
		Action_t437523947 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_OnBannerClicked_10(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// AdToApp.AdToAppSDKDelegate AdToApp.AdToAppSDKDelegate::CreateInstance(UnityEngine.MonoBehaviour)
extern const MethodInfo* GameObject_AddComponent_TisAdToAppSDKDelegate_t2853395885_m1834791607_MethodInfo_var;
extern const uint32_t AdToAppSDKDelegate_CreateInstance_m1680219556_MetadataUsageId;
extern "C"  AdToAppSDKDelegate_t2853395885 * AdToAppSDKDelegate_CreateInstance_m1680219556 (Il2CppObject * __this /* static, unused */, MonoBehaviour_t3012272455 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppSDKDelegate_CreateInstance_m1680219556_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	AdToAppSDKDelegate_t2853395885 * V_0 = NULL;
	{
		MonoBehaviour_t3012272455 * L_0 = ___obj0;
		NullCheck(L_0);
		GameObject_t4012695102 * L_1 = Component_get_gameObject_m2112202034(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		AdToAppSDKDelegate_t2853395885 * L_2 = GameObject_AddComponent_TisAdToAppSDKDelegate_t2853395885_m1834791607(L_1, /*hidden argument*/GameObject_AddComponent_TisAdToAppSDKDelegate_t2853395885_m1834791607_MethodInfo_var);
		V_0 = L_2;
		AdToAppSDKDelegate_t2853395885 * L_3 = V_0;
		return L_3;
	}
}
// System.Void AdToApp.AdToAppSDKDelegate::onInterstitialWillAppear(System.String)
extern const MethodInfo* Action_2_Invoke_m1687808505_MethodInfo_var;
extern const uint32_t AdToAppSDKDelegate_onInterstitialWillAppear_m2297775444_MetadataUsageId;
extern "C"  void AdToAppSDKDelegate_onInterstitialWillAppear_m2297775444 (AdToAppSDKDelegate_t2853395885 * __this, String_t* ___adContentType0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppSDKDelegate_onInterstitialWillAppear_m2297775444_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_2_t2887221574 * L_0 = __this->get_OnInterstitialStarted_2();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		Action_2_t2887221574 * L_1 = __this->get_OnInterstitialStarted_2();
		String_t* L_2 = ___adContentType0;
		NullCheck(L_1);
		Action_2_Invoke_m1687808505(L_1, L_2, (String_t*)NULL, /*hidden argument*/Action_2_Invoke_m1687808505_MethodInfo_var);
	}

IL_0018:
	{
		return;
	}
}
// System.Void AdToApp.AdToAppSDKDelegate::onInterstitialDidDisappear(System.String)
extern const MethodInfo* Action_2_Invoke_m1687808505_MethodInfo_var;
extern const uint32_t AdToAppSDKDelegate_onInterstitialDidDisappear_m1660603223_MetadataUsageId;
extern "C"  void AdToAppSDKDelegate_onInterstitialDidDisappear_m1660603223 (AdToAppSDKDelegate_t2853395885 * __this, String_t* ___adContentType0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppSDKDelegate_onInterstitialDidDisappear_m1660603223_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_2_t2887221574 * L_0 = __this->get_OnInterstitialClosed_3();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		Action_2_t2887221574 * L_1 = __this->get_OnInterstitialClosed_3();
		String_t* L_2 = ___adContentType0;
		NullCheck(L_1);
		Action_2_Invoke_m1687808505(L_1, L_2, (String_t*)NULL, /*hidden argument*/Action_2_Invoke_m1687808505_MethodInfo_var);
	}

IL_0018:
	{
		return;
	}
}
// System.Void AdToApp.AdToAppSDKDelegate::onInterstitialFailedToAppear(System.String)
extern const MethodInfo* Action_2_Invoke_m1687808505_MethodInfo_var;
extern const uint32_t AdToAppSDKDelegate_onInterstitialFailedToAppear_m3939159118_MetadataUsageId;
extern "C"  void AdToAppSDKDelegate_onInterstitialFailedToAppear_m3939159118 (AdToAppSDKDelegate_t2853395885 * __this, String_t* ___adContentType0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppSDKDelegate_onInterstitialFailedToAppear_m3939159118_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_2_t2887221574 * L_0 = __this->get_OnInterstitialFailedToAppear_4();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		Action_2_t2887221574 * L_1 = __this->get_OnInterstitialFailedToAppear_4();
		String_t* L_2 = ___adContentType0;
		NullCheck(L_1);
		Action_2_Invoke_m1687808505(L_1, L_2, (String_t*)NULL, /*hidden argument*/Action_2_Invoke_m1687808505_MethodInfo_var);
	}

IL_0018:
	{
		return;
	}
}
// System.Void AdToApp.AdToAppSDKDelegate::onInterstitialClicked(System.String)
extern const MethodInfo* Action_2_Invoke_m1687808505_MethodInfo_var;
extern const uint32_t AdToAppSDKDelegate_onInterstitialClicked_m2266473024_MetadataUsageId;
extern "C"  void AdToAppSDKDelegate_onInterstitialClicked_m2266473024 (AdToAppSDKDelegate_t2853395885 * __this, String_t* ___adContentType0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppSDKDelegate_onInterstitialClicked_m2266473024_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_2_t2887221574 * L_0 = __this->get_OnInterstitialClicked_5();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		Action_2_t2887221574 * L_1 = __this->get_OnInterstitialClicked_5();
		String_t* L_2 = ___adContentType0;
		NullCheck(L_1);
		Action_2_Invoke_m1687808505(L_1, L_2, (String_t*)NULL, /*hidden argument*/Action_2_Invoke_m1687808505_MethodInfo_var);
	}

IL_0018:
	{
		return;
	}
}
// System.Void AdToApp.AdToAppSDKDelegate::onInterstitialFirstLoaded(System.String)
extern const MethodInfo* Action_2_Invoke_m1687808505_MethodInfo_var;
extern const uint32_t AdToAppSDKDelegate_onInterstitialFirstLoaded_m352211666_MetadataUsageId;
extern "C"  void AdToAppSDKDelegate_onInterstitialFirstLoaded_m352211666 (AdToAppSDKDelegate_t2853395885 * __this, String_t* ___adContentType0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppSDKDelegate_onInterstitialFirstLoaded_m352211666_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_2_t2887221574 * L_0 = __this->get_OnFirstInterstitialLoad_6();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		Action_2_t2887221574 * L_1 = __this->get_OnFirstInterstitialLoad_6();
		String_t* L_2 = ___adContentType0;
		NullCheck(L_1);
		Action_2_Invoke_m1687808505(L_1, L_2, (String_t*)NULL, /*hidden argument*/Action_2_Invoke_m1687808505_MethodInfo_var);
	}

IL_0018:
	{
		return;
	}
}
// System.Void AdToApp.AdToAppSDKDelegate::onRewardedCompleted(System.String)
extern const MethodInfo* Action_3_Invoke_m1741884151_MethodInfo_var;
extern const uint32_t AdToAppSDKDelegate_onRewardedCompleted_m2022259198_MetadataUsageId;
extern "C"  void AdToAppSDKDelegate_onRewardedCompleted_m2022259198 (AdToAppSDKDelegate_t2853395885 * __this, String_t* ___reward0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppSDKDelegate_onRewardedCompleted_m2022259198_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_3_t2150340505 * L_0 = __this->get_OnRewardedCompleted_7();
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		Action_3_t2150340505 * L_1 = __this->get_OnRewardedCompleted_7();
		String_t* L_2 = ___reward0;
		NullCheck(L_1);
		Action_3_Invoke_m1741884151(L_1, (String_t*)NULL, (String_t*)NULL, L_2, /*hidden argument*/Action_3_Invoke_m1741884151_MethodInfo_var);
	}

IL_0019:
	{
		return;
	}
}
// System.Void AdToApp.AdToAppSDKDelegate::onBannerDidDisplayAd()
extern "C"  void AdToAppSDKDelegate_onBannerDidDisplayAd_m1161177453 (AdToAppSDKDelegate_t2853395885 * __this, const MethodInfo* method)
{
	{
		Action_t437523947 * L_0 = __this->get_OnBannerLoad_8();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Action_t437523947 * L_1 = __this->get_OnBannerLoad_8();
		NullCheck(L_1);
		Action_Invoke_m1445970038(L_1, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void AdToApp.AdToAppSDKDelegate::onBannerFailedToDisplayAd(System.String)
extern const MethodInfo* Action_1_Invoke_m518637026_MethodInfo_var;
extern const uint32_t AdToAppSDKDelegate_onBannerFailedToDisplayAd_m1512799706_MetadataUsageId;
extern "C"  void AdToAppSDKDelegate_onBannerFailedToDisplayAd_m1512799706 (AdToAppSDKDelegate_t2853395885 * __this, String_t* ___errorDescription0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppSDKDelegate_onBannerFailedToDisplayAd_m1512799706_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t1116941607 * L_0 = __this->get_OnBannerFailedToLoad_9();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Action_1_t1116941607 * L_1 = __this->get_OnBannerFailedToLoad_9();
		String_t* L_2 = ___errorDescription0;
		NullCheck(L_1);
		Action_1_Invoke_m518637026(L_1, L_2, /*hidden argument*/Action_1_Invoke_m518637026_MethodInfo_var);
	}

IL_0017:
	{
		return;
	}
}
// System.Void AdToApp.AdToAppSDKDelegate::onBannerClicked()
extern "C"  void AdToAppSDKDelegate_onBannerClicked_m2753084546 (AdToAppSDKDelegate_t2853395885 * __this, const MethodInfo* method)
{
	{
		Action_t437523947 * L_0 = __this->get_OnBannerClicked_10();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Action_t437523947 * L_1 = __this->get_OnBannerClicked_10();
		NullCheck(L_1);
		Action_Invoke_m1445970038(L_1, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// AdToApp.AndroidWrapper.ATAInterstitialAdListener AdToApp.AdToAppSDKDelegate::GetInterstitialAndroidAdListener()
extern Il2CppClass* AndroidInterstitialListener_t3321194735_il2cpp_TypeInfo_var;
extern const uint32_t AdToAppSDKDelegate_GetInterstitialAndroidAdListener_m3429272147_MetadataUsageId;
extern "C"  ATAInterstitialAdListener_t382012998 * AdToAppSDKDelegate_GetInterstitialAndroidAdListener_m3429272147 (AdToAppSDKDelegate_t2853395885 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppSDKDelegate_GetInterstitialAndroidAdListener_m3429272147_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AndroidInterstitialListener_t3321194735 * L_0 = (AndroidInterstitialListener_t3321194735 *)il2cpp_codegen_object_new(AndroidInterstitialListener_t3321194735_il2cpp_TypeInfo_var);
		AndroidInterstitialListener__ctor_m1899201185(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// AdToApp.AndroidWrapper.ATABannerAdListener AdToApp.AdToAppSDKDelegate::GetBannerAndroidAdListener()
extern Il2CppClass* AndroidBannerListener_t3507331119_il2cpp_TypeInfo_var;
extern const uint32_t AdToAppSDKDelegate_GetBannerAndroidAdListener_m1240955859_MetadataUsageId;
extern "C"  ATABannerAdListener_t2854275270 * AdToAppSDKDelegate_GetBannerAndroidAdListener_m1240955859 (AdToAppSDKDelegate_t2853395885 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppSDKDelegate_GetBannerAndroidAdListener_m1240955859_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AndroidBannerListener_t3507331119 * L_0 = (AndroidBannerListener_t3507331119 *)il2cpp_codegen_object_new(AndroidBannerListener_t3507331119_il2cpp_TypeInfo_var);
		AndroidBannerListener__ctor_m2581129825(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void AdToApp.AdToAppSDKDelegate/AndroidBannerListener::.ctor(AdToApp.AdToAppSDKDelegate)
extern "C"  void AndroidBannerListener__ctor_m2581129825 (AndroidBannerListener_t3507331119 * __this, AdToAppSDKDelegate_t2853395885 * ___sdkDelegate0, const MethodInfo* method)
{
	{
		ATABannerAdListener__ctor_m1543430333(__this, /*hidden argument*/NULL);
		AdToAppSDKDelegate_t2853395885 * L_0 = ___sdkDelegate0;
		__this->set__sdkDelegate_0(L_0);
		return;
	}
}
// System.Void AdToApp.AdToAppSDKDelegate/AndroidBannerListener::onBannerLoad()
extern "C"  void AndroidBannerListener_onBannerLoad_m1677428397 (AndroidBannerListener_t3507331119 * __this, const MethodInfo* method)
{
	{
		AdToAppSDKDelegate_t2853395885 * L_0 = __this->get__sdkDelegate_0();
		NullCheck(L_0);
		Action_t437523947 * L_1 = L_0->get_OnBannerLoad_8();
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		AdToAppSDKDelegate_t2853395885 * L_2 = __this->get__sdkDelegate_0();
		NullCheck(L_2);
		Action_t437523947 * L_3 = L_2->get_OnBannerLoad_8();
		NullCheck(L_3);
		Action_Invoke_m1445970038(L_3, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Void AdToApp.AdToAppSDKDelegate/AndroidBannerListener::onBannerFailedToLoad()
extern const MethodInfo* Action_1_Invoke_m518637026_MethodInfo_var;
extern const uint32_t AndroidBannerListener_onBannerFailedToLoad_m3131063909_MetadataUsageId;
extern "C"  void AndroidBannerListener_onBannerFailedToLoad_m3131063909 (AndroidBannerListener_t3507331119 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AndroidBannerListener_onBannerFailedToLoad_m3131063909_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AdToAppSDKDelegate_t2853395885 * L_0 = __this->get__sdkDelegate_0();
		NullCheck(L_0);
		Action_1_t1116941607 * L_1 = L_0->get_OnBannerFailedToLoad_9();
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		AdToAppSDKDelegate_t2853395885 * L_2 = __this->get__sdkDelegate_0();
		NullCheck(L_2);
		Action_1_t1116941607 * L_3 = L_2->get_OnBannerFailedToLoad_9();
		NullCheck(L_3);
		Action_1_Invoke_m518637026(L_3, (String_t*)NULL, /*hidden argument*/Action_1_Invoke_m518637026_MethodInfo_var);
	}

IL_0021:
	{
		return;
	}
}
// System.Void AdToApp.AdToAppSDKDelegate/AndroidBannerListener::onBannerClicked()
extern "C"  void AndroidBannerListener_onBannerClicked_m1318837090 (AndroidBannerListener_t3507331119 * __this, const MethodInfo* method)
{
	{
		AdToAppSDKDelegate_t2853395885 * L_0 = __this->get__sdkDelegate_0();
		NullCheck(L_0);
		Action_t437523947 * L_1 = L_0->get_OnBannerClicked_10();
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		AdToAppSDKDelegate_t2853395885 * L_2 = __this->get__sdkDelegate_0();
		NullCheck(L_2);
		Action_t437523947 * L_3 = L_2->get_OnBannerClicked_10();
		NullCheck(L_3);
		Action_Invoke_m1445970038(L_3, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Void AdToApp.AdToAppSDKDelegate/AndroidInterstitialListener::.ctor(AdToApp.AdToAppSDKDelegate)
extern "C"  void AndroidInterstitialListener__ctor_m1899201185 (AndroidInterstitialListener_t3321194735 * __this, AdToAppSDKDelegate_t2853395885 * ___sdkDelegate0, const MethodInfo* method)
{
	{
		ATAInterstitialAdListener__ctor_m979529469(__this, /*hidden argument*/NULL);
		AdToAppSDKDelegate_t2853395885 * L_0 = ___sdkDelegate0;
		__this->set__sdkDelegate_0(L_0);
		return;
	}
}
// System.Void AdToApp.AdToAppSDKDelegate/AndroidInterstitialListener::onInterstitialStarted(System.String,System.String)
extern const MethodInfo* Action_2_Invoke_m1687808505_MethodInfo_var;
extern const uint32_t AndroidInterstitialListener_onInterstitialStarted_m1606331714_MetadataUsageId;
extern "C"  void AndroidInterstitialListener_onInterstitialStarted_m1606331714 (AndroidInterstitialListener_t3321194735 * __this, String_t* ___adType0, String_t* ___provider1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AndroidInterstitialListener_onInterstitialStarted_m1606331714_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AdToAppSDKDelegate_t2853395885 * L_0 = __this->get__sdkDelegate_0();
		NullCheck(L_0);
		Action_2_t2887221574 * L_1 = L_0->get_OnInterstitialStarted_2();
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		AdToAppSDKDelegate_t2853395885 * L_2 = __this->get__sdkDelegate_0();
		NullCheck(L_2);
		Action_2_t2887221574 * L_3 = L_2->get_OnInterstitialStarted_2();
		String_t* L_4 = ___adType0;
		String_t* L_5 = ___provider1;
		NullCheck(L_3);
		Action_2_Invoke_m1687808505(L_3, L_4, L_5, /*hidden argument*/Action_2_Invoke_m1687808505_MethodInfo_var);
	}

IL_0022:
	{
		return;
	}
}
// System.Void AdToApp.AdToAppSDKDelegate/AndroidInterstitialListener::onInterstitialClosed(System.String,System.String)
extern const MethodInfo* Action_2_Invoke_m1687808505_MethodInfo_var;
extern const uint32_t AndroidInterstitialListener_onInterstitialClosed_m3505100651_MetadataUsageId;
extern "C"  void AndroidInterstitialListener_onInterstitialClosed_m3505100651 (AndroidInterstitialListener_t3321194735 * __this, String_t* ___adType0, String_t* ___provider1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AndroidInterstitialListener_onInterstitialClosed_m3505100651_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AdToAppSDKDelegate_t2853395885 * L_0 = __this->get__sdkDelegate_0();
		NullCheck(L_0);
		Action_2_t2887221574 * L_1 = L_0->get_OnInterstitialClosed_3();
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		AdToAppSDKDelegate_t2853395885 * L_2 = __this->get__sdkDelegate_0();
		NullCheck(L_2);
		Action_2_t2887221574 * L_3 = L_2->get_OnInterstitialClosed_3();
		String_t* L_4 = ___adType0;
		String_t* L_5 = ___provider1;
		NullCheck(L_3);
		Action_2_Invoke_m1687808505(L_3, L_4, L_5, /*hidden argument*/Action_2_Invoke_m1687808505_MethodInfo_var);
	}

IL_0022:
	{
		return;
	}
}
// System.Void AdToApp.AdToAppSDKDelegate/AndroidInterstitialListener::onInterstitialClicked(System.String,System.String)
extern const MethodInfo* Action_2_Invoke_m1687808505_MethodInfo_var;
extern const uint32_t AndroidInterstitialListener_onInterstitialClicked_m2762803036_MetadataUsageId;
extern "C"  void AndroidInterstitialListener_onInterstitialClicked_m2762803036 (AndroidInterstitialListener_t3321194735 * __this, String_t* ___adType0, String_t* ___provider1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AndroidInterstitialListener_onInterstitialClicked_m2762803036_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AdToAppSDKDelegate_t2853395885 * L_0 = __this->get__sdkDelegate_0();
		NullCheck(L_0);
		Action_2_t2887221574 * L_1 = L_0->get_OnInterstitialClicked_5();
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		AdToAppSDKDelegate_t2853395885 * L_2 = __this->get__sdkDelegate_0();
		NullCheck(L_2);
		Action_2_t2887221574 * L_3 = L_2->get_OnInterstitialClicked_5();
		String_t* L_4 = ___adType0;
		String_t* L_5 = ___provider1;
		NullCheck(L_3);
		Action_2_Invoke_m1687808505(L_3, L_4, L_5, /*hidden argument*/Action_2_Invoke_m1687808505_MethodInfo_var);
	}

IL_0022:
	{
		return;
	}
}
// System.Void AdToApp.AdToAppSDKDelegate/AndroidInterstitialListener::onFirstInterstitialLoad(System.String,System.String)
extern const MethodInfo* Action_2_Invoke_m1687808505_MethodInfo_var;
extern const uint32_t AndroidInterstitialListener_onFirstInterstitialLoad_m915311765_MetadataUsageId;
extern "C"  void AndroidInterstitialListener_onFirstInterstitialLoad_m915311765 (AndroidInterstitialListener_t3321194735 * __this, String_t* ___adType0, String_t* ___provider1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AndroidInterstitialListener_onFirstInterstitialLoad_m915311765_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AdToAppSDKDelegate_t2853395885 * L_0 = __this->get__sdkDelegate_0();
		NullCheck(L_0);
		Action_2_t2887221574 * L_1 = L_0->get_OnFirstInterstitialLoad_6();
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		AdToAppSDKDelegate_t2853395885 * L_2 = __this->get__sdkDelegate_0();
		NullCheck(L_2);
		Action_2_t2887221574 * L_3 = L_2->get_OnFirstInterstitialLoad_6();
		String_t* L_4 = ___adType0;
		String_t* L_5 = ___provider1;
		NullCheck(L_3);
		Action_2_Invoke_m1687808505(L_3, L_4, L_5, /*hidden argument*/Action_2_Invoke_m1687808505_MethodInfo_var);
	}

IL_0022:
	{
		return;
	}
}
// System.Boolean AdToApp.AdToAppSDKDelegate/AndroidInterstitialListener::onInterstitialFailedToShow(System.String)
extern const MethodInfo* Action_2_Invoke_m1687808505_MethodInfo_var;
extern const uint32_t AndroidInterstitialListener_onInterstitialFailedToShow_m1938861930_MetadataUsageId;
extern "C"  bool AndroidInterstitialListener_onInterstitialFailedToShow_m1938861930 (AndroidInterstitialListener_t3321194735 * __this, String_t* ___adContentType0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AndroidInterstitialListener_onInterstitialFailedToShow_m1938861930_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AdToAppSDKDelegate_t2853395885 * L_0 = __this->get__sdkDelegate_0();
		NullCheck(L_0);
		Action_2_t2887221574 * L_1 = L_0->get_OnInterstitialFailedToAppear_4();
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		AdToAppSDKDelegate_t2853395885 * L_2 = __this->get__sdkDelegate_0();
		NullCheck(L_2);
		Action_2_t2887221574 * L_3 = L_2->get_OnInterstitialFailedToAppear_4();
		String_t* L_4 = ___adContentType0;
		NullCheck(L_3);
		Action_2_Invoke_m1687808505(L_3, L_4, (String_t*)NULL, /*hidden argument*/Action_2_Invoke_m1687808505_MethodInfo_var);
	}

IL_0022:
	{
		return (bool)0;
	}
}
// System.Void AdToApp.AdToAppSDKDelegate/AndroidInterstitialListener::onRewardedCompleted(System.String,System.String,System.String)
extern const MethodInfo* Action_3_Invoke_m1741884151_MethodInfo_var;
extern const uint32_t AndroidInterstitialListener_onRewardedCompleted_m586188118_MetadataUsageId;
extern "C"  void AndroidInterstitialListener_onRewardedCompleted_m586188118 (AndroidInterstitialListener_t3321194735 * __this, String_t* ___adProvider0, String_t* ___currencyName1, String_t* ___currencyValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AndroidInterstitialListener_onRewardedCompleted_m586188118_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AdToAppSDKDelegate_t2853395885 * L_0 = __this->get__sdkDelegate_0();
		NullCheck(L_0);
		Action_3_t2150340505 * L_1 = L_0->get_OnRewardedCompleted_7();
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		AdToAppSDKDelegate_t2853395885 * L_2 = __this->get__sdkDelegate_0();
		NullCheck(L_2);
		Action_3_t2150340505 * L_3 = L_2->get_OnRewardedCompleted_7();
		String_t* L_4 = ___adProvider0;
		String_t* L_5 = ___currencyName1;
		String_t* L_6 = ___currencyValue2;
		NullCheck(L_3);
		Action_3_Invoke_m1741884151(L_3, L_4, L_5, L_6, /*hidden argument*/Action_3_Invoke_m1741884151_MethodInfo_var);
	}

IL_0023:
	{
		return;
	}
}
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper::.ctor()
extern "C"  void AdToAppAndroidWrapper__ctor_m3587631719 (AdToAppAndroidWrapper_t877850908 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String AdToApp.AndroidWrapper.AdToAppAndroidWrapper::get_Version()
extern const MethodInfo* AdToAppAndroidWrapper_GetResult_TisString_t_m4192045899_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1388468386;
extern const uint32_t AdToAppAndroidWrapper_get_Version_m2578489521_MetadataUsageId;
extern "C"  String_t* AdToAppAndroidWrapper_get_Version_m2578489521 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppAndroidWrapper_get_Version_m2578489521_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = AdToAppAndroidWrapper_GetResult_TisString_t_m4192045899(NULL /*static, unused*/, _stringLiteral1388468386, /*hidden argument*/AdToAppAndroidWrapper_GetResult_TisString_t_m4192045899_MethodInfo_var);
		return L_0;
	}
}
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper::InitializeSDK(System.String,System.String)
extern Il2CppClass* U3CInitializeSDKU3Ec__AnonStorey29_t2322037182_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t985559125_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CInitializeSDKU3Ec__AnonStorey29_U3CU3Em__0_m2814292317_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m1498188969_MethodInfo_var;
extern const uint32_t AdToAppAndroidWrapper_InitializeSDK_m2745794255_MetadataUsageId;
extern "C"  void AdToAppAndroidWrapper_InitializeSDK_m2745794255 (Il2CppObject * __this /* static, unused */, String_t* ___adContentType0, String_t* ___sdkKey1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppAndroidWrapper_InitializeSDK_m2745794255_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CInitializeSDKU3Ec__AnonStorey29_t2322037182 * V_0 = NULL;
	{
		U3CInitializeSDKU3Ec__AnonStorey29_t2322037182 * L_0 = (U3CInitializeSDKU3Ec__AnonStorey29_t2322037182 *)il2cpp_codegen_object_new(U3CInitializeSDKU3Ec__AnonStorey29_t2322037182_il2cpp_TypeInfo_var);
		U3CInitializeSDKU3Ec__AnonStorey29__ctor_m261930770(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CInitializeSDKU3Ec__AnonStorey29_t2322037182 * L_1 = V_0;
		String_t* L_2 = ___adContentType0;
		NullCheck(L_1);
		L_1->set_adContentType_0(L_2);
		U3CInitializeSDKU3Ec__AnonStorey29_t2322037182 * L_3 = V_0;
		String_t* L_4 = ___sdkKey1;
		NullCheck(L_3);
		L_3->set_sdkKey_1(L_4);
		U3CInitializeSDKU3Ec__AnonStorey29_t2322037182 * L_5 = V_0;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)U3CInitializeSDKU3Ec__AnonStorey29_U3CU3Em__0_m2814292317_MethodInfo_var);
		Action_1_t985559125 * L_7 = (Action_1_t985559125 *)il2cpp_codegen_object_new(Action_1_t985559125_il2cpp_TypeInfo_var);
		Action_1__ctor_m1498188969(L_7, L_5, L_6, /*hidden argument*/Action_1__ctor_m1498188969_MethodInfo_var);
		AdToAppAndroidWrapper_CallWrapperMethodWithContext_m1648724762(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper::SetLogging(System.Boolean)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3315946653;
extern const uint32_t AdToAppAndroidWrapper_SetLogging_m285204049_MetadataUsageId;
extern "C"  void AdToAppAndroidWrapper_SetLogging_m285204049 (Il2CppObject * __this /* static, unused */, bool ___logging0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppAndroidWrapper_SetLogging_m285204049_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t11523773* L_0 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		bool L_1 = ___logging0;
		bool L_2 = L_1;
		Il2CppObject * L_3 = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		AdToAppAndroidWrapper_CallWrapperMethod_m2056165811(NULL /*static, unused*/, _stringLiteral3315946653, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper::ShowInterstitial(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral769047372;
extern Il2CppCodeGenString* _stringLiteral3101523148;
extern Il2CppCodeGenString* _stringLiteral3516072649;
extern const uint32_t AdToAppAndroidWrapper_ShowInterstitial_m2345890876_MetadataUsageId;
extern "C"  void AdToAppAndroidWrapper_ShowInterstitial_m2345890876 (Il2CppObject * __this /* static, unused */, String_t* ___adContentType0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppAndroidWrapper_ShowInterstitial_m2345890876_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___adContentType0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_0, _stringLiteral769047372, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		AdToAppAndroidWrapper_CallWrapperMethod_m2056165811(NULL /*static, unused*/, _stringLiteral3101523148, ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		return;
	}

IL_0021:
	{
		ObjectU5BU5D_t11523773* L_2 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		String_t* L_3 = ___adContentType0;
		String_t* L_4 = AdToAppAndroidWrapper_NormalizeAdContentType_m4118584223(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_4);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_4);
		AdToAppAndroidWrapper_CallWrapperMethod_m2056165811(NULL /*static, unused*/, _stringLiteral3516072649, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper::SetInterstitialAdListeners(AdToApp.AndroidWrapper.ATAInterstitialAdListener)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1772982146;
extern const uint32_t AdToAppAndroidWrapper_SetInterstitialAdListeners_m2032796921_MetadataUsageId;
extern "C"  void AdToAppAndroidWrapper_SetInterstitialAdListeners_m2032796921 (Il2CppObject * __this /* static, unused */, ATAInterstitialAdListener_t382012998 * ___listener0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppAndroidWrapper_SetInterstitialAdListeners_m2032796921_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t11523773* L_0 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		ATAInterstitialAdListener_t382012998 * L_1 = ___listener0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_1);
		AdToAppAndroidWrapper_CallWrapperMethod_m2056165811(NULL /*static, unused*/, _stringLiteral1772982146, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper::ShowBanner(System.Int32,System.Int32)
extern Il2CppClass* U3CShowBannerU3Ec__AnonStorey2A_t18990589_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t985559125_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CShowBannerU3Ec__AnonStorey2A_U3CU3Em__1_m940804003_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m1498188969_MethodInfo_var;
extern const uint32_t AdToAppAndroidWrapper_ShowBanner_m218655392_MetadataUsageId;
extern "C"  void AdToAppAndroidWrapper_ShowBanner_m218655392 (Il2CppObject * __this /* static, unused */, int32_t ___x0, int32_t ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppAndroidWrapper_ShowBanner_m218655392_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CShowBannerU3Ec__AnonStorey2A_t18990589 * V_0 = NULL;
	{
		U3CShowBannerU3Ec__AnonStorey2A_t18990589 * L_0 = (U3CShowBannerU3Ec__AnonStorey2A_t18990589 *)il2cpp_codegen_object_new(U3CShowBannerU3Ec__AnonStorey2A_t18990589_il2cpp_TypeInfo_var);
		U3CShowBannerU3Ec__AnonStorey2A__ctor_m2365810617(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CShowBannerU3Ec__AnonStorey2A_t18990589 * L_1 = V_0;
		int32_t L_2 = ___x0;
		NullCheck(L_1);
		L_1->set_x_0(L_2);
		U3CShowBannerU3Ec__AnonStorey2A_t18990589 * L_3 = V_0;
		int32_t L_4 = ___y1;
		NullCheck(L_3);
		L_3->set_y_1(L_4);
		U3CShowBannerU3Ec__AnonStorey2A_t18990589 * L_5 = V_0;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)U3CShowBannerU3Ec__AnonStorey2A_U3CU3Em__1_m940804003_MethodInfo_var);
		Action_1_t985559125 * L_7 = (Action_1_t985559125 *)il2cpp_codegen_object_new(Action_1_t985559125_il2cpp_TypeInfo_var);
		Action_1__ctor_m1498188969(L_7, L_5, L_6, /*hidden argument*/Action_1__ctor_m1498188969_MethodInfo_var);
		AdToAppAndroidWrapper_CallWrapperMethodWithContext_m1648724762(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper::ShowBanner(System.Int32,System.Int32,System.String)
extern Il2CppClass* U3CShowBannerU3Ec__AnonStorey2B_t18990590_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t985559125_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CShowBannerU3Ec__AnonStorey2B_U3CU3Em__2_m363263073_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m1498188969_MethodInfo_var;
extern const uint32_t AdToAppAndroidWrapper_ShowBanner_m2996992732_MetadataUsageId;
extern "C"  void AdToAppAndroidWrapper_ShowBanner_m2996992732 (Il2CppObject * __this /* static, unused */, int32_t ___x0, int32_t ___y1, String_t* ___bannerSize2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppAndroidWrapper_ShowBanner_m2996992732_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CShowBannerU3Ec__AnonStorey2B_t18990590 * V_0 = NULL;
	{
		U3CShowBannerU3Ec__AnonStorey2B_t18990590 * L_0 = (U3CShowBannerU3Ec__AnonStorey2B_t18990590 *)il2cpp_codegen_object_new(U3CShowBannerU3Ec__AnonStorey2B_t18990590_il2cpp_TypeInfo_var);
		U3CShowBannerU3Ec__AnonStorey2B__ctor_m2169297112(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CShowBannerU3Ec__AnonStorey2B_t18990590 * L_1 = V_0;
		int32_t L_2 = ___x0;
		NullCheck(L_1);
		L_1->set_x_0(L_2);
		U3CShowBannerU3Ec__AnonStorey2B_t18990590 * L_3 = V_0;
		int32_t L_4 = ___y1;
		NullCheck(L_3);
		L_3->set_y_1(L_4);
		U3CShowBannerU3Ec__AnonStorey2B_t18990590 * L_5 = V_0;
		String_t* L_6 = ___bannerSize2;
		NullCheck(L_5);
		L_5->set_bannerSize_2(L_6);
		U3CShowBannerU3Ec__AnonStorey2B_t18990590 * L_7 = V_0;
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)U3CShowBannerU3Ec__AnonStorey2B_U3CU3Em__2_m363263073_MethodInfo_var);
		Action_1_t985559125 * L_9 = (Action_1_t985559125 *)il2cpp_codegen_object_new(Action_1_t985559125_il2cpp_TypeInfo_var);
		Action_1__ctor_m1498188969(L_9, L_7, L_8, /*hidden argument*/Action_1__ctor_m1498188969_MethodInfo_var);
		AdToAppAndroidWrapper_CallWrapperMethodWithContext_m1648724762(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper::ShowBannerAtPositon(System.String)
extern Il2CppClass* U3CShowBannerAtPositonU3Ec__AnonStorey2C_t1295846048_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t985559125_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CShowBannerAtPositonU3Ec__AnonStorey2C_U3CU3Em__3_m2695302680_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m1498188969_MethodInfo_var;
extern const uint32_t AdToAppAndroidWrapper_ShowBannerAtPositon_m795426811_MetadataUsageId;
extern "C"  void AdToAppAndroidWrapper_ShowBannerAtPositon_m795426811 (Il2CppObject * __this /* static, unused */, String_t* ___bannerPositon0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppAndroidWrapper_ShowBannerAtPositon_m795426811_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CShowBannerAtPositonU3Ec__AnonStorey2C_t1295846048 * V_0 = NULL;
	{
		U3CShowBannerAtPositonU3Ec__AnonStorey2C_t1295846048 * L_0 = (U3CShowBannerAtPositonU3Ec__AnonStorey2C_t1295846048 *)il2cpp_codegen_object_new(U3CShowBannerAtPositonU3Ec__AnonStorey2C_t1295846048_il2cpp_TypeInfo_var);
		U3CShowBannerAtPositonU3Ec__AnonStorey2C__ctor_m1007824560(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CShowBannerAtPositonU3Ec__AnonStorey2C_t1295846048 * L_1 = V_0;
		String_t* L_2 = ___bannerPositon0;
		NullCheck(L_1);
		L_1->set_bannerPositon_0(L_2);
		U3CShowBannerAtPositonU3Ec__AnonStorey2C_t1295846048 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CShowBannerAtPositonU3Ec__AnonStorey2C_U3CU3Em__3_m2695302680_MethodInfo_var);
		Action_1_t985559125 * L_5 = (Action_1_t985559125 *)il2cpp_codegen_object_new(Action_1_t985559125_il2cpp_TypeInfo_var);
		Action_1__ctor_m1498188969(L_5, L_3, L_4, /*hidden argument*/Action_1__ctor_m1498188969_MethodInfo_var);
		AdToAppAndroidWrapper_CallWrapperMethodWithContext_m1648724762(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper::ShowBannerAtPositon(System.String,System.String)
extern Il2CppClass* U3CShowBannerAtPositonU3Ec__AnonStorey2D_t1295846049_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t985559125_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CShowBannerAtPositonU3Ec__AnonStorey2D_U3CU3Em__4_m2117761750_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m1498188969_MethodInfo_var;
extern const uint32_t AdToAppAndroidWrapper_ShowBannerAtPositon_m2909361399_MetadataUsageId;
extern "C"  void AdToAppAndroidWrapper_ShowBannerAtPositon_m2909361399 (Il2CppObject * __this /* static, unused */, String_t* ___bannerPositon0, String_t* ___bannerSize1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppAndroidWrapper_ShowBannerAtPositon_m2909361399_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CShowBannerAtPositonU3Ec__AnonStorey2D_t1295846049 * V_0 = NULL;
	{
		U3CShowBannerAtPositonU3Ec__AnonStorey2D_t1295846049 * L_0 = (U3CShowBannerAtPositonU3Ec__AnonStorey2D_t1295846049 *)il2cpp_codegen_object_new(U3CShowBannerAtPositonU3Ec__AnonStorey2D_t1295846049_il2cpp_TypeInfo_var);
		U3CShowBannerAtPositonU3Ec__AnonStorey2D__ctor_m811311055(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CShowBannerAtPositonU3Ec__AnonStorey2D_t1295846049 * L_1 = V_0;
		String_t* L_2 = ___bannerPositon0;
		NullCheck(L_1);
		L_1->set_bannerPositon_0(L_2);
		U3CShowBannerAtPositonU3Ec__AnonStorey2D_t1295846049 * L_3 = V_0;
		String_t* L_4 = ___bannerSize1;
		NullCheck(L_3);
		L_3->set_bannerSize_1(L_4);
		U3CShowBannerAtPositonU3Ec__AnonStorey2D_t1295846049 * L_5 = V_0;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)U3CShowBannerAtPositonU3Ec__AnonStorey2D_U3CU3Em__4_m2117761750_MethodInfo_var);
		Action_1_t985559125 * L_7 = (Action_1_t985559125 *)il2cpp_codegen_object_new(Action_1_t985559125_il2cpp_TypeInfo_var);
		Action_1__ctor_m1498188969(L_7, L_5, L_6, /*hidden argument*/Action_1__ctor_m1498188969_MethodInfo_var);
		AdToAppAndroidWrapper_CallWrapperMethodWithContext_m1648724762(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper::RemoveAllBanners()
extern Il2CppClass* AdToAppAndroidWrapper_t877850908_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t985559125_il2cpp_TypeInfo_var;
extern const MethodInfo* AdToAppAndroidWrapper_U3CRemoveAllBannersU3Em__5_m1653727735_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m1498188969_MethodInfo_var;
extern const uint32_t AdToAppAndroidWrapper_RemoveAllBanners_m3826459399_MetadataUsageId;
extern "C"  void AdToAppAndroidWrapper_RemoveAllBanners_m3826459399 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppAndroidWrapper_RemoveAllBanners_m3826459399_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t985559125 * L_0 = ((AdToAppAndroidWrapper_t877850908_StaticFields*)AdToAppAndroidWrapper_t877850908_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_1();
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)AdToAppAndroidWrapper_U3CRemoveAllBannersU3Em__5_m1653727735_MethodInfo_var);
		Action_1_t985559125 * L_2 = (Action_1_t985559125 *)il2cpp_codegen_object_new(Action_1_t985559125_il2cpp_TypeInfo_var);
		Action_1__ctor_m1498188969(L_2, NULL, L_1, /*hidden argument*/Action_1__ctor_m1498188969_MethodInfo_var);
		((AdToAppAndroidWrapper_t877850908_StaticFields*)AdToAppAndroidWrapper_t877850908_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache0_1(L_2);
	}

IL_0018:
	{
		Action_1_t985559125 * L_3 = ((AdToAppAndroidWrapper_t877850908_StaticFields*)AdToAppAndroidWrapper_t877850908_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_1();
		AdToAppAndroidWrapper_CallWrapperMethodWithContext_m1648724762(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper::SetBannerAdListeners(AdToApp.AndroidWrapper.ATABannerAdListener)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2527014833;
extern const uint32_t AdToAppAndroidWrapper_SetBannerAdListeners_m2865466041_MetadataUsageId;
extern "C"  void AdToAppAndroidWrapper_SetBannerAdListeners_m2865466041 (Il2CppObject * __this /* static, unused */, ATABannerAdListener_t2854275270 * ___listener0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppAndroidWrapper_SetBannerAdListeners_m2865466041_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t11523773* L_0 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		ATABannerAdListener_t2854275270 * L_1 = ___listener0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_1);
		AdToAppAndroidWrapper_CallWrapperMethod_m2056165811(NULL /*static, unused*/, _stringLiteral2527014833, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper::onPause(System.Boolean)
extern Il2CppClass* AdToAppAndroidWrapper_t877850908_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t985559125_il2cpp_TypeInfo_var;
extern const MethodInfo* AdToAppAndroidWrapper_U3ConPauseU3Em__6_m562301251_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m1498188969_MethodInfo_var;
extern const MethodInfo* AdToAppAndroidWrapper_U3ConPauseU3Em__7_m51767074_MethodInfo_var;
extern const uint32_t AdToAppAndroidWrapper_onPause_m3724000435_MetadataUsageId;
extern "C"  void AdToAppAndroidWrapper_onPause_m3724000435 (Il2CppObject * __this /* static, unused */, bool ___pauseStatus0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppAndroidWrapper_onPause_m3724000435_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___pauseStatus0;
		if (!L_0)
		{
			goto IL_002d;
		}
	}
	{
		Action_1_t985559125 * L_1 = ((AdToAppAndroidWrapper_t877850908_StaticFields*)AdToAppAndroidWrapper_t877850908_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_2();
		if (L_1)
		{
			goto IL_001e;
		}
	}
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)AdToAppAndroidWrapper_U3ConPauseU3Em__6_m562301251_MethodInfo_var);
		Action_1_t985559125 * L_3 = (Action_1_t985559125 *)il2cpp_codegen_object_new(Action_1_t985559125_il2cpp_TypeInfo_var);
		Action_1__ctor_m1498188969(L_3, NULL, L_2, /*hidden argument*/Action_1__ctor_m1498188969_MethodInfo_var);
		((AdToAppAndroidWrapper_t877850908_StaticFields*)AdToAppAndroidWrapper_t877850908_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache1_2(L_3);
	}

IL_001e:
	{
		Action_1_t985559125 * L_4 = ((AdToAppAndroidWrapper_t877850908_StaticFields*)AdToAppAndroidWrapper_t877850908_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_2();
		AdToAppAndroidWrapper_CallWrapperMethodWithContext_m1648724762(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		goto IL_004f;
	}

IL_002d:
	{
		Action_1_t985559125 * L_5 = ((AdToAppAndroidWrapper_t877850908_StaticFields*)AdToAppAndroidWrapper_t877850908_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache2_3();
		if (L_5)
		{
			goto IL_0045;
		}
	}
	{
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)AdToAppAndroidWrapper_U3ConPauseU3Em__7_m51767074_MethodInfo_var);
		Action_1_t985559125 * L_7 = (Action_1_t985559125 *)il2cpp_codegen_object_new(Action_1_t985559125_il2cpp_TypeInfo_var);
		Action_1__ctor_m1498188969(L_7, NULL, L_6, /*hidden argument*/Action_1__ctor_m1498188969_MethodInfo_var);
		((AdToAppAndroidWrapper_t877850908_StaticFields*)AdToAppAndroidWrapper_t877850908_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache2_3(L_7);
	}

IL_0045:
	{
		Action_1_t985559125 * L_8 = ((AdToAppAndroidWrapper_t877850908_StaticFields*)AdToAppAndroidWrapper_t877850908_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache2_3();
		AdToAppAndroidWrapper_CallWrapperMethodWithContext_m1648724762(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
	}

IL_004f:
	{
		return;
	}
}
// System.Boolean AdToApp.AndroidWrapper.AdToAppAndroidWrapper::HasInterstitial(System.String)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern const MethodInfo* AdToAppAndroidWrapper_CallWrapperMethod_TisBoolean_t211005341_m2126928797_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1759459046;
extern const uint32_t AdToAppAndroidWrapper_HasInterstitial_m3242219667_MetadataUsageId;
extern "C"  bool AdToAppAndroidWrapper_HasInterstitial_m3242219667 (Il2CppObject * __this /* static, unused */, String_t* ___adContentType0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppAndroidWrapper_HasInterstitial_m3242219667_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t11523773* L_0 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		String_t* L_1 = ___adContentType0;
		String_t* L_2 = AdToAppAndroidWrapper_NormalizeAdContentType_m4118584223(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_2);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_2);
		bool L_3 = AdToAppAndroidWrapper_CallWrapperMethod_TisBoolean_t211005341_m2126928797(NULL /*static, unused*/, _stringLiteral1759459046, L_0, /*hidden argument*/AdToAppAndroidWrapper_CallWrapperMethod_TisBoolean_t211005341_m2126928797_MethodInfo_var);
		return L_3;
	}
}
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper::LoadNextBanner()
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3580947845;
extern const uint32_t AdToAppAndroidWrapper_LoadNextBanner_m4103270210_MetadataUsageId;
extern "C"  void AdToAppAndroidWrapper_LoadNextBanner_m4103270210 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppAndroidWrapper_LoadNextBanner_m4103270210_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AdToAppAndroidWrapper_CallWrapperMethod_m2056165811(NULL /*static, unused*/, _stringLiteral3580947845, ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper::SetBannerRefreshInterval(System.Double)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3312832690;
extern const uint32_t AdToAppAndroidWrapper_SetBannerRefreshInterval_m1756139379_MetadataUsageId;
extern "C"  void AdToAppAndroidWrapper_SetBannerRefreshInterval_m1756139379 (Il2CppObject * __this /* static, unused */, double ___refreshInterval0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppAndroidWrapper_SetBannerRefreshInterval_m1756139379_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t11523773* L_0 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		double L_1 = ___refreshInterval0;
		double L_2 = L_1;
		Il2CppObject * L_3 = Box(Double_t534516614_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		AdToAppAndroidWrapper_CallWrapperMethod_m2056165811(NULL /*static, unused*/, _stringLiteral3312832690, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper::CallWrapperMethod(System.String,System.Object[])
extern "C"  void AdToAppAndroidWrapper_CallWrapperMethod_m2056165811 (Il2CppObject * __this /* static, unused */, String_t* ___methodName0, ObjectU5BU5D_t11523773* ___args1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper::CallWrapperMethodWithContext(System.Action`1<System.Object>)
extern "C"  void AdToAppAndroidWrapper_CallWrapperMethodWithContext_m1648724762 (Il2CppObject * __this /* static, unused */, Action_1_t985559125 * ___action0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.IDisposable AdToApp.AndroidWrapper.AdToAppAndroidWrapper::GetAndroidContext()
extern "C"  Il2CppObject * AdToAppAndroidWrapper_GetAndroidContext_m4026730848 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return (Il2CppObject *)NULL;
	}
}
// System.String AdToApp.AndroidWrapper.AdToAppAndroidWrapper::NormalizeAdContentType(System.String)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2906786608;
extern const uint32_t AdToAppAndroidWrapper_NormalizeAdContentType_m4118584223_MetadataUsageId;
extern "C"  String_t* AdToAppAndroidWrapper_NormalizeAdContentType_m4118584223 (Il2CppObject * __this /* static, unused */, String_t* ___adContentType0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppAndroidWrapper_NormalizeAdContentType_m4118584223_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___adContentType0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, _stringLiteral2906786608, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		String_t* L_2 = ___adContentType0;
		NullCheck(L_2);
		String_t* L_3 = String_ToLowerInvariant_m4111189975(L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper::setTestMode(System.Boolean)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral842332119;
extern const uint32_t AdToAppAndroidWrapper_setTestMode_m3263678259_MetadataUsageId;
extern "C"  void AdToAppAndroidWrapper_setTestMode_m3263678259 (Il2CppObject * __this /* static, unused */, bool ___isEnabled0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppAndroidWrapper_setTestMode_m3263678259_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t11523773* L_0 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		bool L_1 = ___isEnabled0;
		bool L_2 = L_1;
		Il2CppObject * L_3 = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		AdToAppAndroidWrapper_CallWrapperMethod_m2056165811(NULL /*static, unused*/, _stringLiteral842332119, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String AdToApp.AndroidWrapper.AdToAppAndroidWrapper::getRewardedCurrency()
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern const MethodInfo* AdToAppAndroidWrapper_CallWrapperMethod_TisString_t_m425797626_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4100528565;
extern const uint32_t AdToAppAndroidWrapper_getRewardedCurrency_m2026025751_MetadataUsageId;
extern "C"  String_t* AdToAppAndroidWrapper_getRewardedCurrency_m2026025751 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppAndroidWrapper_getRewardedCurrency_m2026025751_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = AdToAppAndroidWrapper_CallWrapperMethod_TisString_t_m425797626(NULL /*static, unused*/, _stringLiteral4100528565, ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/AdToAppAndroidWrapper_CallWrapperMethod_TisString_t_m425797626_MethodInfo_var);
		return L_0;
	}
}
// System.String AdToApp.AndroidWrapper.AdToAppAndroidWrapper::getRewardedValue()
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern const MethodInfo* AdToAppAndroidWrapper_CallWrapperMethod_TisString_t_m425797626_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3955518029;
extern const uint32_t AdToAppAndroidWrapper_getRewardedValue_m3410800141_MetadataUsageId;
extern "C"  String_t* AdToAppAndroidWrapper_getRewardedValue_m3410800141 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppAndroidWrapper_getRewardedValue_m3410800141_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = AdToAppAndroidWrapper_CallWrapperMethod_TisString_t_m425797626(NULL /*static, unused*/, _stringLiteral3955518029, ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/AdToAppAndroidWrapper_CallWrapperMethod_TisString_t_m425797626_MethodInfo_var);
		return L_0;
	}
}
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper::setTargetingParam(System.String,System.String)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1584378718;
extern const uint32_t AdToAppAndroidWrapper_setTargetingParam_m2947927131_MetadataUsageId;
extern "C"  void AdToAppAndroidWrapper_setTargetingParam_m2947927131 (Il2CppObject * __this /* static, unused */, String_t* ___parameterName0, String_t* ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppAndroidWrapper_setTargetingParam_m2947927131_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t11523773* L_0 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)2));
		String_t* L_1 = ___parameterName0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_1);
		ObjectU5BU5D_t11523773* L_2 = L_0;
		String_t* L_3 = ___value1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_3);
		AdToAppAndroidWrapper_CallWrapperMethod_m2056165811(NULL /*static, unused*/, _stringLiteral1584378718, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper::<RemoveAllBanners>m__5(System.Object)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2089262474;
extern const uint32_t AdToAppAndroidWrapper_U3CRemoveAllBannersU3Em__5_m1653727735_MetadataUsageId;
extern "C"  void AdToAppAndroidWrapper_U3CRemoveAllBannersU3Em__5_m1653727735 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___wrapperContext0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppAndroidWrapper_U3CRemoveAllBannersU3Em__5_m1653727735_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t11523773* L_0 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		Il2CppObject * L_1 = ___wrapperContext0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_1);
		AdToAppAndroidWrapper_CallWrapperMethod_m2056165811(NULL /*static, unused*/, _stringLiteral2089262474, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper::<onPause>m__6(System.Object)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2954754903;
extern const uint32_t AdToAppAndroidWrapper_U3ConPauseU3Em__6_m562301251_MetadataUsageId;
extern "C"  void AdToAppAndroidWrapper_U3ConPauseU3Em__6_m562301251 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___wrapperContext0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppAndroidWrapper_U3ConPauseU3Em__6_m562301251_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t11523773* L_0 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		Il2CppObject * L_1 = ___wrapperContext0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_1);
		AdToAppAndroidWrapper_CallWrapperMethod_m2056165811(NULL /*static, unused*/, _stringLiteral2954754903, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper::<onPause>m__7(System.Object)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1463983852;
extern const uint32_t AdToAppAndroidWrapper_U3ConPauseU3Em__7_m51767074_MetadataUsageId;
extern "C"  void AdToAppAndroidWrapper_U3ConPauseU3Em__7_m51767074 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___wrapperContext0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdToAppAndroidWrapper_U3ConPauseU3Em__7_m51767074_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t11523773* L_0 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		Il2CppObject * L_1 = ___wrapperContext0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_1);
		AdToAppAndroidWrapper_CallWrapperMethod_m2056165811(NULL /*static, unused*/, _stringLiteral1463983852, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper/<InitializeSDK>c__AnonStorey29::.ctor()
extern "C"  void U3CInitializeSDKU3Ec__AnonStorey29__ctor_m261930770 (U3CInitializeSDKU3Ec__AnonStorey29_t2322037182 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper/<InitializeSDK>c__AnonStorey29::<>m__0(System.Object)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral109757538;
extern const uint32_t U3CInitializeSDKU3Ec__AnonStorey29_U3CU3Em__0_m2814292317_MetadataUsageId;
extern "C"  void U3CInitializeSDKU3Ec__AnonStorey29_U3CU3Em__0_m2814292317 (U3CInitializeSDKU3Ec__AnonStorey29_t2322037182 * __this, Il2CppObject * ___wrapperContext0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CInitializeSDKU3Ec__AnonStorey29_U3CU3Em__0_m2814292317_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t11523773* L_0 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)3));
		Il2CppObject * L_1 = ___wrapperContext0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_1);
		ObjectU5BU5D_t11523773* L_2 = L_0;
		String_t* L_3 = __this->get_adContentType_0();
		String_t* L_4 = AdToAppAndroidWrapper_NormalizeAdContentType_m4118584223(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_4);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_4);
		ObjectU5BU5D_t11523773* L_5 = L_2;
		String_t* L_6 = __this->get_sdkKey_1();
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 2);
		ArrayElementTypeCheck (L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_6);
		AdToAppAndroidWrapper_CallWrapperMethod_m2056165811(NULL /*static, unused*/, _stringLiteral109757538, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper/<ShowBanner>c__AnonStorey2A::.ctor()
extern "C"  void U3CShowBannerU3Ec__AnonStorey2A__ctor_m2365810617 (U3CShowBannerU3Ec__AnonStorey2A_t18990589 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper/<ShowBanner>c__AnonStorey2A::<>m__1(System.Object)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral278746249;
extern const uint32_t U3CShowBannerU3Ec__AnonStorey2A_U3CU3Em__1_m940804003_MetadataUsageId;
extern "C"  void U3CShowBannerU3Ec__AnonStorey2A_U3CU3Em__1_m940804003 (U3CShowBannerU3Ec__AnonStorey2A_t18990589 * __this, Il2CppObject * ___wrapperContext0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CShowBannerU3Ec__AnonStorey2A_U3CU3Em__1_m940804003_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t11523773* L_0 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)3));
		Il2CppObject * L_1 = ___wrapperContext0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_1);
		ObjectU5BU5D_t11523773* L_2 = L_0;
		int32_t L_3 = __this->get_x_0();
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_5);
		ObjectU5BU5D_t11523773* L_6 = L_2;
		int32_t L_7 = __this->get_y_1();
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, L_9);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_9);
		AdToAppAndroidWrapper_CallWrapperMethod_m2056165811(NULL /*static, unused*/, _stringLiteral278746249, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper/<ShowBanner>c__AnonStorey2B::.ctor()
extern "C"  void U3CShowBannerU3Ec__AnonStorey2B__ctor_m2169297112 (U3CShowBannerU3Ec__AnonStorey2B_t18990590 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper/<ShowBanner>c__AnonStorey2B::<>m__2(System.Object)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral278746249;
extern const uint32_t U3CShowBannerU3Ec__AnonStorey2B_U3CU3Em__2_m363263073_MetadataUsageId;
extern "C"  void U3CShowBannerU3Ec__AnonStorey2B_U3CU3Em__2_m363263073 (U3CShowBannerU3Ec__AnonStorey2B_t18990590 * __this, Il2CppObject * ___wrapperContext0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CShowBannerU3Ec__AnonStorey2B_U3CU3Em__2_m363263073_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t11523773* L_0 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)4));
		Il2CppObject * L_1 = ___wrapperContext0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_1);
		ObjectU5BU5D_t11523773* L_2 = L_0;
		int32_t L_3 = __this->get_x_0();
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_5);
		ObjectU5BU5D_t11523773* L_6 = L_2;
		int32_t L_7 = __this->get_y_1();
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, L_9);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_9);
		ObjectU5BU5D_t11523773* L_10 = L_6;
		String_t* L_11 = __this->get_bannerSize_2();
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 3);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_11);
		AdToAppAndroidWrapper_CallWrapperMethod_m2056165811(NULL /*static, unused*/, _stringLiteral278746249, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper/<ShowBannerAtPositon>c__AnonStorey2C::.ctor()
extern "C"  void U3CShowBannerAtPositonU3Ec__AnonStorey2C__ctor_m1007824560 (U3CShowBannerAtPositonU3Ec__AnonStorey2C_t1295846048 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper/<ShowBannerAtPositon>c__AnonStorey2C::<>m__3(System.Object)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3481075685;
extern const uint32_t U3CShowBannerAtPositonU3Ec__AnonStorey2C_U3CU3Em__3_m2695302680_MetadataUsageId;
extern "C"  void U3CShowBannerAtPositonU3Ec__AnonStorey2C_U3CU3Em__3_m2695302680 (U3CShowBannerAtPositonU3Ec__AnonStorey2C_t1295846048 * __this, Il2CppObject * ___wrapperContext0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CShowBannerAtPositonU3Ec__AnonStorey2C_U3CU3Em__3_m2695302680_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t11523773* L_0 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)2));
		Il2CppObject * L_1 = ___wrapperContext0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_1);
		ObjectU5BU5D_t11523773* L_2 = L_0;
		String_t* L_3 = __this->get_bannerPositon_0();
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_3);
		AdToAppAndroidWrapper_CallWrapperMethod_m2056165811(NULL /*static, unused*/, _stringLiteral3481075685, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper/<ShowBannerAtPositon>c__AnonStorey2D::.ctor()
extern "C"  void U3CShowBannerAtPositonU3Ec__AnonStorey2D__ctor_m811311055 (U3CShowBannerAtPositonU3Ec__AnonStorey2D_t1295846049 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper/<ShowBannerAtPositon>c__AnonStorey2D::<>m__4(System.Object)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3481075685;
extern const uint32_t U3CShowBannerAtPositonU3Ec__AnonStorey2D_U3CU3Em__4_m2117761750_MetadataUsageId;
extern "C"  void U3CShowBannerAtPositonU3Ec__AnonStorey2D_U3CU3Em__4_m2117761750 (U3CShowBannerAtPositonU3Ec__AnonStorey2D_t1295846049 * __this, Il2CppObject * ___wrapperContext0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CShowBannerAtPositonU3Ec__AnonStorey2D_U3CU3Em__4_m2117761750_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t11523773* L_0 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)3));
		Il2CppObject * L_1 = ___wrapperContext0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_1);
		ObjectU5BU5D_t11523773* L_2 = L_0;
		String_t* L_3 = __this->get_bannerPositon_0();
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_3);
		ObjectU5BU5D_t11523773* L_4 = L_2;
		String_t* L_5 = __this->get_bannerSize_1();
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_5);
		AdToAppAndroidWrapper_CallWrapperMethod_m2056165811(NULL /*static, unused*/, _stringLiteral3481075685, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AdToApp.AndroidWrapper.ATABannerAdListener::.ctor()
extern "C"  void ATABannerAdListener__ctor_m1543430333 (ATABannerAdListener_t2854275270 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AdToApp.AndroidWrapper.ATABannerAdListener::onBannerLoad()
extern "C"  void ATABannerAdListener_onBannerLoad_m2673358840 (ATABannerAdListener_t2854275270 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void AdToApp.AndroidWrapper.ATABannerAdListener::onBannerFailedToLoad()
extern "C"  void ATABannerAdListener_onBannerFailedToLoad_m2596828336 (ATABannerAdListener_t2854275270 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void AdToApp.AndroidWrapper.ATABannerAdListener::onBannerClicked()
extern "C"  void ATABannerAdListener_onBannerClicked_m1448583735 (ATABannerAdListener_t2854275270 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void AdToApp.AndroidWrapper.ATAInterstitialAdListener::.ctor()
extern "C"  void ATAInterstitialAdListener__ctor_m979529469 (ATAInterstitialAdListener_t382012998 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AdToApp.AndroidWrapper.ATAInterstitialAdListener::onInterstitialStarted(System.String,System.String)
extern "C"  void ATAInterstitialAdListener_onInterstitialStarted_m2636836621 (ATAInterstitialAdListener_t382012998 * __this, String_t* ___adType0, String_t* ___provider1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void AdToApp.AndroidWrapper.ATAInterstitialAdListener::onInterstitialClosed(System.String,System.String)
extern "C"  void ATAInterstitialAdListener_onInterstitialClosed_m2707058752 (ATAInterstitialAdListener_t382012998 * __this, String_t* ___adType0, String_t* ___provider1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void AdToApp.AndroidWrapper.ATAInterstitialAdListener::onInterstitialClicked(System.String,System.String)
extern "C"  void ATAInterstitialAdListener_onInterstitialClicked_m3793307943 (ATAInterstitialAdListener_t382012998 * __this, String_t* ___adType0, String_t* ___provider1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void AdToApp.AndroidWrapper.ATAInterstitialAdListener::onFirstInterstitialLoad(System.String,System.String)
extern "C"  void ATAInterstitialAdListener_onFirstInterstitialLoad_m3388049312 (ATAInterstitialAdListener_t382012998 * __this, String_t* ___adType0, String_t* ___provider1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Boolean AdToApp.AndroidWrapper.ATAInterstitialAdListener::onInterstitialFailedToShow(System.String)
extern "C"  bool ATAInterstitialAdListener_onInterstitialFailedToShow_m3807340607 (ATAInterstitialAdListener_t382012998 * __this, String_t* ___adType0, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void AdToApp.AndroidWrapper.ATAInterstitialAdListener::onRewardedCompleted(System.String,System.String,System.String)
extern "C"  void ATAInterstitialAdListener_onRewardedCompleted_m82407073 (ATAInterstitialAdListener_t382012998 * __this, String_t* ___adProvider0, String_t* ___currencyName1, String_t* ___currencyValue2, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void AppsFlyer::.ctor()
extern "C"  void AppsFlyer__ctor_m3363854345 (AppsFlyer_t82339054 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m350695606(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AppsFlyer::mTrackEvent(System.String,System.String)
extern "C" void DEFAULT_CALL mTrackEvent(char*, char*);
extern "C"  void AppsFlyer_mTrackEvent_m2810694587 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, String_t* ___eventValue1, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*);

	// Marshaling of parameter '___eventName0' to native representation
	char* ____eventName0_marshaled = NULL;
	____eventName0_marshaled = il2cpp_codegen_marshal_string(___eventName0);

	// Marshaling of parameter '___eventValue1' to native representation
	char* ____eventValue1_marshaled = NULL;
	____eventValue1_marshaled = il2cpp_codegen_marshal_string(___eventValue1);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(mTrackEvent)(____eventName0_marshaled, ____eventValue1_marshaled);

	// Marshaling cleanup of parameter '___eventName0' native representation
	il2cpp_codegen_marshal_free(____eventName0_marshaled);
	____eventName0_marshaled = NULL;

	// Marshaling cleanup of parameter '___eventValue1' native representation
	il2cpp_codegen_marshal_free(____eventValue1_marshaled);
	____eventValue1_marshaled = NULL;

}
// System.Void AppsFlyer::mSetCurrencyCode(System.String)
extern "C" void DEFAULT_CALL mSetCurrencyCode(char*);
extern "C"  void AppsFlyer_mSetCurrencyCode_m3911200404 (Il2CppObject * __this /* static, unused */, String_t* ___currencyCode0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___currencyCode0' to native representation
	char* ____currencyCode0_marshaled = NULL;
	____currencyCode0_marshaled = il2cpp_codegen_marshal_string(___currencyCode0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(mSetCurrencyCode)(____currencyCode0_marshaled);

	// Marshaling cleanup of parameter '___currencyCode0' native representation
	il2cpp_codegen_marshal_free(____currencyCode0_marshaled);
	____currencyCode0_marshaled = NULL;

}
// System.Void AppsFlyer::mSetCustomerUserID(System.String)
extern "C" void DEFAULT_CALL mSetCustomerUserID(char*);
extern "C"  void AppsFlyer_mSetCustomerUserID_m919353230 (Il2CppObject * __this /* static, unused */, String_t* ___customerUserID0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___customerUserID0' to native representation
	char* ____customerUserID0_marshaled = NULL;
	____customerUserID0_marshaled = il2cpp_codegen_marshal_string(___customerUserID0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(mSetCustomerUserID)(____customerUserID0_marshaled);

	// Marshaling cleanup of parameter '___customerUserID0' native representation
	il2cpp_codegen_marshal_free(____customerUserID0_marshaled);
	____customerUserID0_marshaled = NULL;

}
// System.Void AppsFlyer::mSetAppsFlyerDevKey(System.String)
extern "C" void DEFAULT_CALL mSetAppsFlyerDevKey(char*);
extern "C"  void AppsFlyer_mSetAppsFlyerDevKey_m130938360 (Il2CppObject * __this /* static, unused */, String_t* ___devKey0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___devKey0' to native representation
	char* ____devKey0_marshaled = NULL;
	____devKey0_marshaled = il2cpp_codegen_marshal_string(___devKey0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(mSetAppsFlyerDevKey)(____devKey0_marshaled);

	// Marshaling cleanup of parameter '___devKey0' native representation
	il2cpp_codegen_marshal_free(____devKey0_marshaled);
	____devKey0_marshaled = NULL;

}
// System.Void AppsFlyer::mTrackAppLaunch()
extern "C" void DEFAULT_CALL mTrackAppLaunch();
extern "C"  void AppsFlyer_mTrackAppLaunch_m62097981 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(mTrackAppLaunch)();

}
// System.Void AppsFlyer::mSetAppID(System.String)
extern "C" void DEFAULT_CALL mSetAppID(char*);
extern "C"  void AppsFlyer_mSetAppID_m328268276 (Il2CppObject * __this /* static, unused */, String_t* ___appleAppId0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___appleAppId0' to native representation
	char* ____appleAppId0_marshaled = NULL;
	____appleAppId0_marshaled = il2cpp_codegen_marshal_string(___appleAppId0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(mSetAppID)(____appleAppId0_marshaled);

	// Marshaling cleanup of parameter '___appleAppId0' native representation
	il2cpp_codegen_marshal_free(____appleAppId0_marshaled);
	____appleAppId0_marshaled = NULL;

}
// System.Void AppsFlyer::mTrackRichEvent(System.String,System.String)
extern "C" void DEFAULT_CALL mTrackRichEvent(char*, char*);
extern "C"  void AppsFlyer_mTrackRichEvent_m808304055 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, String_t* ___eventValues1, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*);

	// Marshaling of parameter '___eventName0' to native representation
	char* ____eventName0_marshaled = NULL;
	____eventName0_marshaled = il2cpp_codegen_marshal_string(___eventName0);

	// Marshaling of parameter '___eventValues1' to native representation
	char* ____eventValues1_marshaled = NULL;
	____eventValues1_marshaled = il2cpp_codegen_marshal_string(___eventValues1);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(mTrackRichEvent)(____eventName0_marshaled, ____eventValues1_marshaled);

	// Marshaling cleanup of parameter '___eventName0' native representation
	il2cpp_codegen_marshal_free(____eventName0_marshaled);
	____eventName0_marshaled = NULL;

	// Marshaling cleanup of parameter '___eventValues1' native representation
	il2cpp_codegen_marshal_free(____eventValues1_marshaled);
	____eventValues1_marshaled = NULL;

}
// System.Void AppsFlyer::mValidateReceipt(System.String,System.String,System.String,System.String,System.Double,System.String)
extern "C" void DEFAULT_CALL mValidateReceipt(char*, char*, char*, char*, double, char*);
extern "C"  void AppsFlyer_mValidateReceipt_m4292413214 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, String_t* ___failedEventName1, String_t* ___eventValue2, String_t* ___productIdentifier3, double ___price4, String_t* ___currency5, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*, char*, char*, double, char*);

	// Marshaling of parameter '___eventName0' to native representation
	char* ____eventName0_marshaled = NULL;
	____eventName0_marshaled = il2cpp_codegen_marshal_string(___eventName0);

	// Marshaling of parameter '___failedEventName1' to native representation
	char* ____failedEventName1_marshaled = NULL;
	____failedEventName1_marshaled = il2cpp_codegen_marshal_string(___failedEventName1);

	// Marshaling of parameter '___eventValue2' to native representation
	char* ____eventValue2_marshaled = NULL;
	____eventValue2_marshaled = il2cpp_codegen_marshal_string(___eventValue2);

	// Marshaling of parameter '___productIdentifier3' to native representation
	char* ____productIdentifier3_marshaled = NULL;
	____productIdentifier3_marshaled = il2cpp_codegen_marshal_string(___productIdentifier3);

	// Marshaling of parameter '___price4' to native representation

	// Marshaling of parameter '___currency5' to native representation
	char* ____currency5_marshaled = NULL;
	____currency5_marshaled = il2cpp_codegen_marshal_string(___currency5);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(mValidateReceipt)(____eventName0_marshaled, ____failedEventName1_marshaled, ____eventValue2_marshaled, ____productIdentifier3_marshaled, ___price4, ____currency5_marshaled);

	// Marshaling cleanup of parameter '___eventName0' native representation
	il2cpp_codegen_marshal_free(____eventName0_marshaled);
	____eventName0_marshaled = NULL;

	// Marshaling cleanup of parameter '___failedEventName1' native representation
	il2cpp_codegen_marshal_free(____failedEventName1_marshaled);
	____failedEventName1_marshaled = NULL;

	// Marshaling cleanup of parameter '___eventValue2' native representation
	il2cpp_codegen_marshal_free(____eventValue2_marshaled);
	____eventValue2_marshaled = NULL;

	// Marshaling cleanup of parameter '___productIdentifier3' native representation
	il2cpp_codegen_marshal_free(____productIdentifier3_marshaled);
	____productIdentifier3_marshaled = NULL;

	// Marshaling cleanup of parameter '___price4' native representation

	// Marshaling cleanup of parameter '___currency5' native representation
	il2cpp_codegen_marshal_free(____currency5_marshaled);
	____currency5_marshaled = NULL;

}
// System.Void AppsFlyer::trackEvent(System.String,System.String)
extern "C"  void AppsFlyer_trackEvent_m3305581748 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, String_t* ___eventValue1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___eventName0;
		String_t* L_1 = ___eventValue1;
		AppsFlyer_mTrackEvent_m2810694587(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AppsFlyer::setCurrencyCode(System.String)
extern "C"  void AppsFlyer_setCurrencyCode_m3598066043 (Il2CppObject * __this /* static, unused */, String_t* ___currencyCode0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___currencyCode0;
		AppsFlyer_mSetCurrencyCode_m3911200404(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AppsFlyer::setCustomerUserID(System.String)
extern "C"  void AppsFlyer_setCustomerUserID_m644943029 (Il2CppObject * __this /* static, unused */, String_t* ___customerUserID0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___customerUserID0;
		AppsFlyer_mSetCustomerUserID_m919353230(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AppsFlyer::setAppsFlyerKey(System.String)
extern "C"  void AppsFlyer_setAppsFlyerKey_m1333593160 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___key0;
		AppsFlyer_mSetAppsFlyerDevKey_m130938360(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AppsFlyer::trackAppLaunch()
extern "C"  void AppsFlyer_trackAppLaunch_m1071484260 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		AppsFlyer_mTrackAppLaunch_m62097981(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AppsFlyer::setAppID(System.String)
extern "C"  void AppsFlyer_setAppID_m721120493 (Il2CppObject * __this /* static, unused */, String_t* ___appleAppId0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___appleAppId0;
		AppsFlyer_mSetAppID_m328268276(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AppsFlyer::trackRichEvent(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t2373214747_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m2759194411_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m2871721525_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m1739472607_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m730091314_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m2577713898_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral61;
extern Il2CppCodeGenString* _stringLiteral10;
extern const uint32_t AppsFlyer_trackRichEvent_m3321556035_MetadataUsageId;
extern "C"  void AppsFlyer_trackRichEvent_m3321556035 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, Dictionary_2_t2606186806 * ___eventValues1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AppsFlyer_trackRichEvent_m3321556035_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	KeyValuePair_2_t2094718104  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Enumerator_t2373214747  V_2;
	memset(&V_2, 0, sizeof(V_2));
	String_t* V_3 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_0;
		Dictionary_2_t2606186806 * L_1 = ___eventValues1;
		NullCheck(L_1);
		Enumerator_t2373214747  L_2 = Dictionary_2_GetEnumerator_m2759194411(L_1, /*hidden argument*/Dictionary_2_GetEnumerator_m2759194411_MethodInfo_var);
		V_2 = L_2;
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0050;
		}

IL_0012:
		{
			KeyValuePair_2_t2094718104  L_3 = Enumerator_get_Current_m2871721525((&V_2), /*hidden argument*/Enumerator_get_Current_m2871721525_MethodInfo_var);
			V_1 = L_3;
			String_t* L_4 = V_0;
			V_3 = L_4;
			StringU5BU5D_t2956870243* L_5 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)5));
			String_t* L_6 = V_3;
			NullCheck(L_5);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 0);
			ArrayElementTypeCheck (L_5, L_6);
			(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_6);
			StringU5BU5D_t2956870243* L_7 = L_5;
			String_t* L_8 = KeyValuePair_2_get_Key_m1739472607((&V_1), /*hidden argument*/KeyValuePair_2_get_Key_m1739472607_MethodInfo_var);
			NullCheck(L_7);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 1);
			ArrayElementTypeCheck (L_7, L_8);
			(L_7)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_8);
			StringU5BU5D_t2956870243* L_9 = L_7;
			NullCheck(L_9);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 2);
			ArrayElementTypeCheck (L_9, _stringLiteral61);
			(L_9)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral61);
			StringU5BU5D_t2956870243* L_10 = L_9;
			String_t* L_11 = KeyValuePair_2_get_Value_m730091314((&V_1), /*hidden argument*/KeyValuePair_2_get_Value_m730091314_MethodInfo_var);
			NullCheck(L_10);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 3);
			ArrayElementTypeCheck (L_10, L_11);
			(L_10)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_11);
			StringU5BU5D_t2956870243* L_12 = L_10;
			NullCheck(L_12);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
			ArrayElementTypeCheck (L_12, _stringLiteral10);
			(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral10);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_13 = String_Concat_m21867311(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
			V_0 = L_13;
		}

IL_0050:
		{
			bool L_14 = Enumerator_MoveNext_m2577713898((&V_2), /*hidden argument*/Enumerator_MoveNext_m2577713898_MethodInfo_var);
			if (L_14)
			{
				goto IL_0012;
			}
		}

IL_005c:
		{
			IL2CPP_LEAVE(0x6D, FINALLY_0061);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0061;
	}

FINALLY_0061:
	{ // begin finally (depth: 1)
		Enumerator_t2373214747  L_15 = V_2;
		Enumerator_t2373214747  L_16 = L_15;
		Il2CppObject * L_17 = Box(Enumerator_t2373214747_il2cpp_TypeInfo_var, &L_16);
		NullCheck((Il2CppObject *)L_17);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_17);
		IL2CPP_END_FINALLY(97)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(97)
	{
		IL2CPP_JUMP_TBL(0x6D, IL_006d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_006d:
	{
		String_t* L_18 = ___eventName0;
		String_t* L_19 = V_0;
		AppsFlyer_mTrackRichEvent_m808304055(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AppsFlyer::validateReceipt(System.String,System.String,System.String,System.String,System.Double,System.String)
extern "C"  void AppsFlyer_validateReceipt_m4249893061 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, String_t* ___failedEventName1, String_t* ___eventValue2, String_t* ___productIdentifier3, double ___price4, String_t* ___currency5, const MethodInfo* method)
{
	{
		String_t* L_0 = ___eventName0;
		String_t* L_1 = ___failedEventName1;
		String_t* L_2 = ___eventValue2;
		String_t* L_3 = ___productIdentifier3;
		double L_4 = ___price4;
		String_t* L_5 = ___currency5;
		AppsFlyer_mValidateReceipt_m4292413214(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AppsFlyerTrackerCallbacks::.ctor()
extern "C"  void AppsFlyerTrackerCallbacks__ctor_m1729839155 (AppsFlyerTrackerCallbacks_t1581047172 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m350695606(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AppsFlyerTrackerCallbacks::Start()
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3410676189;
extern const uint32_t AppsFlyerTrackerCallbacks_Start_m676976947_MetadataUsageId;
extern "C"  void AppsFlyerTrackerCallbacks_Start_m676976947 (AppsFlyerTrackerCallbacks_t1581047172 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AppsFlyerTrackerCallbacks_Start_m676976947_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m3349710197(NULL /*static, unused*/, _stringLiteral3410676189, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AppsFlyerTrackerCallbacks::Update()
extern "C"  void AppsFlyerTrackerCallbacks_Update_m3812268346 (AppsFlyerTrackerCallbacks_t1581047172 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void AppsFlyerTrackerCallbacks::didReceiveConversionData(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral588603039;
extern const uint32_t AppsFlyerTrackerCallbacks_didReceiveConversionData_m1726608173_MetadataUsageId;
extern "C"  void AppsFlyerTrackerCallbacks_didReceiveConversionData_m1726608173 (AppsFlyerTrackerCallbacks_t1581047172 * __this, String_t* ___conversionData0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AppsFlyerTrackerCallbacks_didReceiveConversionData_m1726608173_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___conversionData0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral588603039, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m3349710197(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AppsFlyerTrackerCallbacks::didReceiveConversionDataWithError(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral433889975;
extern const uint32_t AppsFlyerTrackerCallbacks_didReceiveConversionDataWithError_m2493028499_MetadataUsageId;
extern "C"  void AppsFlyerTrackerCallbacks_didReceiveConversionDataWithError_m2493028499 (AppsFlyerTrackerCallbacks_t1581047172 * __this, String_t* ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AppsFlyerTrackerCallbacks_didReceiveConversionDataWithError_m2493028499_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___error0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral433889975, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m3349710197(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AppsFlyerTrackerCallbacks::didFinishValidateReceipt(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral945718115;
extern const uint32_t AppsFlyerTrackerCallbacks_didFinishValidateReceipt_m2188660257_MetadataUsageId;
extern "C"  void AppsFlyerTrackerCallbacks_didFinishValidateReceipt_m2188660257 (AppsFlyerTrackerCallbacks_t1581047172 * __this, String_t* ___validateResult0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AppsFlyerTrackerCallbacks_didFinishValidateReceipt_m2188660257_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___validateResult0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral945718115, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m3349710197(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AppsFlyerTrackerCallbacks::didFinishValidateReceiptWithError(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3435800605;
extern const uint32_t AppsFlyerTrackerCallbacks_didFinishValidateReceiptWithError_m2081640479_MetadataUsageId;
extern "C"  void AppsFlyerTrackerCallbacks_didFinishValidateReceiptWithError_m2081640479 (AppsFlyerTrackerCallbacks_t1581047172 * __this, String_t* ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AppsFlyerTrackerCallbacks_didFinishValidateReceiptWithError_m2081640479_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___error0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3435800605, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m3349710197(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OnePF.Inventory::.ctor(System.String)
extern Il2CppClass* Dictionary_2_t1707827913_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t1797893477_il2cpp_TypeInfo_var;
extern Il2CppClass* JSON_t2649481148_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1634065389_il2cpp_TypeInfo_var;
extern Il2CppClass* OpenIAB_iOS_t446655906_il2cpp_TypeInfo_var;
extern Il2CppClass* Purchase_t160195573_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t4014815677_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppClass* SkuDetails_t70130009_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m3724077073_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m128083309_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1820263692_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1113061044_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m302222300_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral633240731;
extern Il2CppCodeGenString* _stringLiteral3394731775;
extern const uint32_t Inventory__ctor_m529083189_MetadataUsageId;
extern "C"  void Inventory__ctor_m529083189 (Inventory_t2630562832 * __this, String_t* ___json0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Inventory__ctor_m529083189_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JSON_t2649481148 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Enumerator_t4014815677  V_2;
	memset(&V_2, 0, sizeof(V_2));
	List_1_t1634065389 * V_3 = NULL;
	String_t* V_4 = NULL;
	Purchase_t160195573 * V_5 = NULL;
	Il2CppObject * V_6 = NULL;
	Enumerator_t4014815677  V_7;
	memset(&V_7, 0, sizeof(V_7));
	List_1_t1634065389 * V_8 = NULL;
	String_t* V_9 = NULL;
	SkuDetails_t70130009 * V_10 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t1707827913 * L_0 = (Dictionary_2_t1707827913 *)il2cpp_codegen_object_new(Dictionary_2_t1707827913_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3724077073(L_0, /*hidden argument*/Dictionary_2__ctor_m3724077073_MethodInfo_var);
		__this->set__skuMap_0(L_0);
		Dictionary_2_t1797893477 * L_1 = (Dictionary_2_t1797893477 *)il2cpp_codegen_object_new(Dictionary_2_t1797893477_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m128083309(L_1, /*hidden argument*/Dictionary_2__ctor_m128083309_MethodInfo_var);
		__this->set__purchaseMap_1(L_1);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		String_t* L_2 = ___json0;
		JSON_t2649481148 * L_3 = (JSON_t2649481148 *)il2cpp_codegen_object_new(JSON_t2649481148_il2cpp_TypeInfo_var);
		JSON__ctor_m2841521773(L_3, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		JSON_t2649481148 * L_4 = V_0;
		NullCheck(L_4);
		Dictionary_2_t2474804324 * L_5 = L_4->get_fields_0();
		NullCheck(L_5);
		Il2CppObject * L_6 = VirtFuncInvoker1< Il2CppObject *, String_t* >::Invoke(22 /* !1 System.Collections.Generic.Dictionary`2<System.String,System.Object>::get_Item(!0) */, L_5, _stringLiteral633240731);
		NullCheck(((List_1_t1634065389 *)CastclassClass(L_6, List_1_t1634065389_il2cpp_TypeInfo_var)));
		Enumerator_t4014815677  L_7 = List_1_GetEnumerator_m1820263692(((List_1_t1634065389 *)CastclassClass(L_6, List_1_t1634065389_il2cpp_TypeInfo_var)), /*hidden argument*/List_1_GetEnumerator_m1820263692_MethodInfo_var);
		V_2 = L_7;
	}

IL_003e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0087;
		}

IL_0043:
		{
			Il2CppObject * L_8 = Enumerator_get_Current_m1113061044((&V_2), /*hidden argument*/Enumerator_get_Current_m1113061044_MethodInfo_var);
			V_1 = L_8;
			Il2CppObject * L_9 = V_1;
			V_3 = ((List_1_t1634065389 *)CastclassClass(L_9, List_1_t1634065389_il2cpp_TypeInfo_var));
			List_1_t1634065389 * L_10 = V_3;
			NullCheck(L_10);
			Il2CppObject * L_11 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32) */, L_10, 0);
			NullCheck(L_11);
			String_t* L_12 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_11);
			IL2CPP_RUNTIME_CLASS_INIT(OpenIAB_iOS_t446655906_il2cpp_TypeInfo_var);
			String_t* L_13 = OpenIAB_iOS_StoreSku2Sku_m2417310421(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
			V_4 = L_13;
			List_1_t1634065389 * L_14 = V_3;
			NullCheck(L_14);
			Il2CppObject * L_15 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32) */, L_14, 1);
			Purchase_t160195573 * L_16 = (Purchase_t160195573 *)il2cpp_codegen_object_new(Purchase_t160195573_il2cpp_TypeInfo_var);
			Purchase__ctor_m2219165762(L_16, ((JSON_t2649481148 *)CastclassClass(L_15, JSON_t2649481148_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
			V_5 = L_16;
			Dictionary_2_t1797893477 * L_17 = __this->get__purchaseMap_1();
			String_t* L_18 = V_4;
			Purchase_t160195573 * L_19 = V_5;
			NullCheck(L_17);
			VirtActionInvoker2< String_t*, Purchase_t160195573 * >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,OnePF.Purchase>::Add(!0,!1) */, L_17, L_18, L_19);
		}

IL_0087:
		{
			bool L_20 = Enumerator_MoveNext_m302222300((&V_2), /*hidden argument*/Enumerator_MoveNext_m302222300_MethodInfo_var);
			if (L_20)
			{
				goto IL_0043;
			}
		}

IL_0093:
		{
			IL2CPP_LEAVE(0xA4, FINALLY_0098);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0098;
	}

FINALLY_0098:
	{ // begin finally (depth: 1)
		Enumerator_t4014815677  L_21 = V_2;
		Enumerator_t4014815677  L_22 = L_21;
		Il2CppObject * L_23 = Box(Enumerator_t4014815677_il2cpp_TypeInfo_var, &L_22);
		NullCheck((Il2CppObject *)L_23);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_23);
		IL2CPP_END_FINALLY(152)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(152)
	{
		IL2CPP_JUMP_TBL(0xA4, IL_00a4)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00a4:
	{
		JSON_t2649481148 * L_24 = V_0;
		NullCheck(L_24);
		Dictionary_2_t2474804324 * L_25 = L_24->get_fields_0();
		NullCheck(L_25);
		Il2CppObject * L_26 = VirtFuncInvoker1< Il2CppObject *, String_t* >::Invoke(22 /* !1 System.Collections.Generic.Dictionary`2<System.String,System.Object>::get_Item(!0) */, L_25, _stringLiteral3394731775);
		NullCheck(((List_1_t1634065389 *)CastclassClass(L_26, List_1_t1634065389_il2cpp_TypeInfo_var)));
		Enumerator_t4014815677  L_27 = List_1_GetEnumerator_m1820263692(((List_1_t1634065389 *)CastclassClass(L_26, List_1_t1634065389_il2cpp_TypeInfo_var)), /*hidden argument*/List_1_GetEnumerator_m1820263692_MethodInfo_var);
		V_7 = L_27;
	}

IL_00c0:
	try
	{ // begin try (depth: 1)
		{
			goto IL_010e;
		}

IL_00c5:
		{
			Il2CppObject * L_28 = Enumerator_get_Current_m1113061044((&V_7), /*hidden argument*/Enumerator_get_Current_m1113061044_MethodInfo_var);
			V_6 = L_28;
			Il2CppObject * L_29 = V_6;
			V_8 = ((List_1_t1634065389 *)CastclassClass(L_29, List_1_t1634065389_il2cpp_TypeInfo_var));
			List_1_t1634065389 * L_30 = V_8;
			NullCheck(L_30);
			Il2CppObject * L_31 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32) */, L_30, 0);
			NullCheck(L_31);
			String_t* L_32 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_31);
			IL2CPP_RUNTIME_CLASS_INIT(OpenIAB_iOS_t446655906_il2cpp_TypeInfo_var);
			String_t* L_33 = OpenIAB_iOS_StoreSku2Sku_m2417310421(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
			V_9 = L_33;
			List_1_t1634065389 * L_34 = V_8;
			NullCheck(L_34);
			Il2CppObject * L_35 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32) */, L_34, 1);
			SkuDetails_t70130009 * L_36 = (SkuDetails_t70130009 *)il2cpp_codegen_object_new(SkuDetails_t70130009_il2cpp_TypeInfo_var);
			SkuDetails__ctor_m108523998(L_36, ((JSON_t2649481148 *)CastclassClass(L_35, JSON_t2649481148_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
			V_10 = L_36;
			Dictionary_2_t1707827913 * L_37 = __this->get__skuMap_0();
			String_t* L_38 = V_9;
			SkuDetails_t70130009 * L_39 = V_10;
			NullCheck(L_37);
			VirtActionInvoker2< String_t*, SkuDetails_t70130009 * >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,OnePF.SkuDetails>::Add(!0,!1) */, L_37, L_38, L_39);
		}

IL_010e:
		{
			bool L_40 = Enumerator_MoveNext_m302222300((&V_7), /*hidden argument*/Enumerator_MoveNext_m302222300_MethodInfo_var);
			if (L_40)
			{
				goto IL_00c5;
			}
		}

IL_011a:
		{
			IL2CPP_LEAVE(0x12C, FINALLY_011f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_011f;
	}

FINALLY_011f:
	{ // begin finally (depth: 1)
		Enumerator_t4014815677  L_41 = V_7;
		Enumerator_t4014815677  L_42 = L_41;
		Il2CppObject * L_43 = Box(Enumerator_t4014815677_il2cpp_TypeInfo_var, &L_42);
		NullCheck((Il2CppObject *)L_43);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_43);
		IL2CPP_END_FINALLY(287)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(287)
	{
		IL2CPP_JUMP_TBL(0x12C, IL_012c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_012c:
	{
		return;
	}
}
// System.String OnePF.Inventory::ToString()
extern Il2CppClass* StringBuilder_t3822575854_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t1564921418_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t1474855854_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m3634053991_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4289358153_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m4008284001_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m1591893074_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m2289425636_MethodInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m717685131_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m902753645_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m3908616453_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m782600502_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1664858496_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral899786945;
extern Il2CppCodeGenString* _stringLiteral34;
extern Il2CppCodeGenString* _stringLiteral34595;
extern Il2CppCodeGenString* _stringLiteral3919;
extern Il2CppCodeGenString* _stringLiteral2457060032;
extern Il2CppCodeGenString* _stringLiteral4000;
extern const uint32_t Inventory_ToString_m517228064_MetadataUsageId;
extern "C"  String_t* Inventory_ToString_m517228064 (Inventory_t2630562832 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Inventory_ToString_m517228064_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t3822575854 * V_0 = NULL;
	KeyValuePair_2_t1286424775  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Enumerator_t1564921418  V_2;
	memset(&V_2, 0, sizeof(V_2));
	KeyValuePair_2_t1196359211  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Enumerator_t1474855854  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		StringBuilder_t3822575854 * L_0 = (StringBuilder_t3822575854 *)il2cpp_codegen_object_new(StringBuilder_t3822575854_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m135953004(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		StringBuilder_t3822575854 * L_1 = V_0;
		NullCheck(L_1);
		StringBuilder_Append_m3898090075(L_1, _stringLiteral899786945, /*hidden argument*/NULL);
		Dictionary_2_t1797893477 * L_2 = __this->get__purchaseMap_1();
		NullCheck(L_2);
		Enumerator_t1564921418  L_3 = Dictionary_2_GetEnumerator_m3634053991(L_2, /*hidden argument*/Dictionary_2_GetEnumerator_m3634053991_MethodInfo_var);
		V_2 = L_3;
	}

IL_001e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_006e;
		}

IL_0023:
		{
			KeyValuePair_2_t1286424775  L_4 = Enumerator_get_Current_m4289358153((&V_2), /*hidden argument*/Enumerator_get_Current_m4289358153_MethodInfo_var);
			V_1 = L_4;
			StringBuilder_t3822575854 * L_5 = V_0;
			StringU5BU5D_t2956870243* L_6 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)5));
			NullCheck(L_6);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
			ArrayElementTypeCheck (L_6, _stringLiteral34);
			(L_6)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral34);
			StringU5BU5D_t2956870243* L_7 = L_6;
			String_t* L_8 = KeyValuePair_2_get_Key_m4008284001((&V_1), /*hidden argument*/KeyValuePair_2_get_Key_m4008284001_MethodInfo_var);
			NullCheck(L_7);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 1);
			ArrayElementTypeCheck (L_7, L_8);
			(L_7)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_8);
			StringU5BU5D_t2956870243* L_9 = L_7;
			NullCheck(L_9);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 2);
			ArrayElementTypeCheck (L_9, _stringLiteral34595);
			(L_9)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral34595);
			StringU5BU5D_t2956870243* L_10 = L_9;
			Purchase_t160195573 * L_11 = KeyValuePair_2_get_Value_m1591893074((&V_1), /*hidden argument*/KeyValuePair_2_get_Value_m1591893074_MethodInfo_var);
			NullCheck(L_11);
			String_t* L_12 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String OnePF.Purchase::ToString() */, L_11);
			NullCheck(L_10);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 3);
			ArrayElementTypeCheck (L_10, L_12);
			(L_10)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_12);
			StringU5BU5D_t2956870243* L_13 = L_10;
			NullCheck(L_13);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
			ArrayElementTypeCheck (L_13, _stringLiteral3919);
			(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral3919);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
			NullCheck(L_5);
			StringBuilder_Append_m3898090075(L_5, L_14, /*hidden argument*/NULL);
		}

IL_006e:
		{
			bool L_15 = Enumerator_MoveNext_m2289425636((&V_2), /*hidden argument*/Enumerator_MoveNext_m2289425636_MethodInfo_var);
			if (L_15)
			{
				goto IL_0023;
			}
		}

IL_007a:
		{
			IL2CPP_LEAVE(0x8B, FINALLY_007f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_007f;
	}

FINALLY_007f:
	{ // begin finally (depth: 1)
		Enumerator_t1564921418  L_16 = V_2;
		Enumerator_t1564921418  L_17 = L_16;
		Il2CppObject * L_18 = Box(Enumerator_t1564921418_il2cpp_TypeInfo_var, &L_17);
		NullCheck((Il2CppObject *)L_18);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_18);
		IL2CPP_END_FINALLY(127)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(127)
	{
		IL2CPP_JUMP_TBL(0x8B, IL_008b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_008b:
	{
		StringBuilder_t3822575854 * L_19 = V_0;
		NullCheck(L_19);
		StringBuilder_Append_m3898090075(L_19, _stringLiteral3919, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_20 = V_0;
		NullCheck(L_20);
		StringBuilder_Append_m3898090075(L_20, _stringLiteral2457060032, /*hidden argument*/NULL);
		Dictionary_2_t1707827913 * L_21 = __this->get__skuMap_0();
		NullCheck(L_21);
		Enumerator_t1474855854  L_22 = Dictionary_2_GetEnumerator_m717685131(L_21, /*hidden argument*/Dictionary_2_GetEnumerator_m717685131_MethodInfo_var);
		V_4 = L_22;
	}

IL_00b0:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0100;
		}

IL_00b5:
		{
			KeyValuePair_2_t1196359211  L_23 = Enumerator_get_Current_m902753645((&V_4), /*hidden argument*/Enumerator_get_Current_m902753645_MethodInfo_var);
			V_3 = L_23;
			StringBuilder_t3822575854 * L_24 = V_0;
			StringU5BU5D_t2956870243* L_25 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)5));
			NullCheck(L_25);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 0);
			ArrayElementTypeCheck (L_25, _stringLiteral34);
			(L_25)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral34);
			StringU5BU5D_t2956870243* L_26 = L_25;
			String_t* L_27 = KeyValuePair_2_get_Key_m3908616453((&V_3), /*hidden argument*/KeyValuePair_2_get_Key_m3908616453_MethodInfo_var);
			NullCheck(L_26);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 1);
			ArrayElementTypeCheck (L_26, L_27);
			(L_26)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_27);
			StringU5BU5D_t2956870243* L_28 = L_26;
			NullCheck(L_28);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_28, 2);
			ArrayElementTypeCheck (L_28, _stringLiteral34595);
			(L_28)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral34595);
			StringU5BU5D_t2956870243* L_29 = L_28;
			SkuDetails_t70130009 * L_30 = KeyValuePair_2_get_Value_m782600502((&V_3), /*hidden argument*/KeyValuePair_2_get_Value_m782600502_MethodInfo_var);
			NullCheck(L_30);
			String_t* L_31 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String OnePF.SkuDetails::ToString() */, L_30);
			NullCheck(L_29);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_29, 3);
			ArrayElementTypeCheck (L_29, L_31);
			(L_29)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_31);
			StringU5BU5D_t2956870243* L_32 = L_29;
			NullCheck(L_32);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_32, 4);
			ArrayElementTypeCheck (L_32, _stringLiteral3919);
			(L_32)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral3919);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_33 = String_Concat_m21867311(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
			NullCheck(L_24);
			StringBuilder_Append_m3898090075(L_24, L_33, /*hidden argument*/NULL);
		}

IL_0100:
		{
			bool L_34 = Enumerator_MoveNext_m1664858496((&V_4), /*hidden argument*/Enumerator_MoveNext_m1664858496_MethodInfo_var);
			if (L_34)
			{
				goto IL_00b5;
			}
		}

IL_010c:
		{
			IL2CPP_LEAVE(0x11E, FINALLY_0111);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0111;
	}

FINALLY_0111:
	{ // begin finally (depth: 1)
		Enumerator_t1474855854  L_35 = V_4;
		Enumerator_t1474855854  L_36 = L_35;
		Il2CppObject * L_37 = Box(Enumerator_t1474855854_il2cpp_TypeInfo_var, &L_36);
		NullCheck((Il2CppObject *)L_37);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_37);
		IL2CPP_END_FINALLY(273)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(273)
	{
		IL2CPP_JUMP_TBL(0x11E, IL_011e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_011e:
	{
		StringBuilder_t3822575854 * L_38 = V_0;
		NullCheck(L_38);
		StringBuilder_Append_m3898090075(L_38, _stringLiteral4000, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_39 = V_0;
		NullCheck(L_39);
		String_t* L_40 = StringBuilder_ToString_m350379841(L_39, /*hidden argument*/NULL);
		return L_40;
	}
}
// OnePF.SkuDetails OnePF.Inventory::GetSkuDetails(System.String)
extern "C"  SkuDetails_t70130009 * Inventory_GetSkuDetails_m2242380878 (Inventory_t2630562832 * __this, String_t* ___sku0, const MethodInfo* method)
{
	{
		Dictionary_2_t1707827913 * L_0 = __this->get__skuMap_0();
		String_t* L_1 = ___sku0;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, String_t* >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,OnePF.SkuDetails>::ContainsKey(!0) */, L_0, L_1);
		if (L_2)
		{
			goto IL_0013;
		}
	}
	{
		return (SkuDetails_t70130009 *)NULL;
	}

IL_0013:
	{
		Dictionary_2_t1707827913 * L_3 = __this->get__skuMap_0();
		String_t* L_4 = ___sku0;
		NullCheck(L_3);
		SkuDetails_t70130009 * L_5 = VirtFuncInvoker1< SkuDetails_t70130009 *, String_t* >::Invoke(22 /* !1 System.Collections.Generic.Dictionary`2<System.String,OnePF.SkuDetails>::get_Item(!0) */, L_3, L_4);
		return L_5;
	}
}
// OnePF.Purchase OnePF.Inventory::GetPurchase(System.String)
extern "C"  Purchase_t160195573 * Inventory_GetPurchase_m339254670 (Inventory_t2630562832 * __this, String_t* ___sku0, const MethodInfo* method)
{
	{
		Dictionary_2_t1797893477 * L_0 = __this->get__purchaseMap_1();
		String_t* L_1 = ___sku0;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, String_t* >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,OnePF.Purchase>::ContainsKey(!0) */, L_0, L_1);
		if (L_2)
		{
			goto IL_0013;
		}
	}
	{
		return (Purchase_t160195573 *)NULL;
	}

IL_0013:
	{
		Dictionary_2_t1797893477 * L_3 = __this->get__purchaseMap_1();
		String_t* L_4 = ___sku0;
		NullCheck(L_3);
		Purchase_t160195573 * L_5 = VirtFuncInvoker1< Purchase_t160195573 *, String_t* >::Invoke(22 /* !1 System.Collections.Generic.Dictionary`2<System.String,OnePF.Purchase>::get_Item(!0) */, L_3, L_4);
		return L_5;
	}
}
// System.Boolean OnePF.Inventory::HasPurchase(System.String)
extern "C"  bool Inventory_HasPurchase_m1648840736 (Inventory_t2630562832 * __this, String_t* ___sku0, const MethodInfo* method)
{
	{
		Dictionary_2_t1797893477 * L_0 = __this->get__purchaseMap_1();
		String_t* L_1 = ___sku0;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, String_t* >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,OnePF.Purchase>::ContainsKey(!0) */, L_0, L_1);
		return L_2;
	}
}
// System.Boolean OnePF.Inventory::HasDetails(System.String)
extern "C"  bool Inventory_HasDetails_m774176127 (Inventory_t2630562832 * __this, String_t* ___sku0, const MethodInfo* method)
{
	{
		Dictionary_2_t1707827913 * L_0 = __this->get__skuMap_0();
		String_t* L_1 = ___sku0;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, String_t* >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,OnePF.SkuDetails>::ContainsKey(!0) */, L_0, L_1);
		return L_2;
	}
}
// System.Void OnePF.Inventory::ErasePurchase(System.String)
extern "C"  void Inventory_ErasePurchase_m1346425904 (Inventory_t2630562832 * __this, String_t* ___sku0, const MethodInfo* method)
{
	{
		Dictionary_2_t1797893477 * L_0 = __this->get__purchaseMap_1();
		String_t* L_1 = ___sku0;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, String_t* >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,OnePF.Purchase>::ContainsKey(!0) */, L_0, L_1);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		Dictionary_2_t1797893477 * L_3 = __this->get__purchaseMap_1();
		String_t* L_4 = ___sku0;
		NullCheck(L_3);
		VirtFuncInvoker1< bool, String_t* >::Invoke(20 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,OnePF.Purchase>::Remove(!0) */, L_3, L_4);
	}

IL_001e:
	{
		return;
	}
}
// System.Collections.Generic.List`1<System.String> OnePF.Inventory::GetAllOwnedSkus()
extern const MethodInfo* Dictionary_2_get_Keys_m1873637815_MethodInfo_var;
extern const MethodInfo* Enumerable_ToList_TisString_t_m790467565_MethodInfo_var;
extern const uint32_t Inventory_GetAllOwnedSkus_m2770015284_MetadataUsageId;
extern "C"  List_1_t1765447871 * Inventory_GetAllOwnedSkus_m2770015284 (Inventory_t2630562832 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Inventory_GetAllOwnedSkus_m2770015284_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t1797893477 * L_0 = __this->get__purchaseMap_1();
		NullCheck(L_0);
		KeyCollection_t4121168757 * L_1 = Dictionary_2_get_Keys_m1873637815(L_0, /*hidden argument*/Dictionary_2_get_Keys_m1873637815_MethodInfo_var);
		List_1_t1765447871 * L_2 = Enumerable_ToList_TisString_t_m790467565(NULL /*static, unused*/, L_1, /*hidden argument*/Enumerable_ToList_TisString_t_m790467565_MethodInfo_var);
		return L_2;
	}
}
// System.Collections.Generic.List`1<System.String> OnePF.Inventory::GetAllOwnedSkus(System.String)
extern Il2CppClass* List_1_t1765447871_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t1564921419_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m459821414_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Values_m4177709395_MethodInfo_var;
extern const MethodInfo* ValueCollection_GetEnumerator_m3487143911_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m780193209_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m47107108_MethodInfo_var;
extern const uint32_t Inventory_GetAllOwnedSkus_m3388211022_MetadataUsageId;
extern "C"  List_1_t1765447871 * Inventory_GetAllOwnedSkus_m3388211022 (Inventory_t2630562832 * __this, String_t* ___itemType0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Inventory_GetAllOwnedSkus_m3388211022_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t1765447871 * V_0 = NULL;
	Purchase_t160195573 * V_1 = NULL;
	Enumerator_t1564921419  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t1765447871 * L_0 = (List_1_t1765447871 *)il2cpp_codegen_object_new(List_1_t1765447871_il2cpp_TypeInfo_var);
		List_1__ctor_m459821414(L_0, /*hidden argument*/List_1__ctor_m459821414_MethodInfo_var);
		V_0 = L_0;
		Dictionary_2_t1797893477 * L_1 = __this->get__purchaseMap_1();
		NullCheck(L_1);
		ValueCollection_t3720030571 * L_2 = Dictionary_2_get_Values_m4177709395(L_1, /*hidden argument*/Dictionary_2_get_Values_m4177709395_MethodInfo_var);
		NullCheck(L_2);
		Enumerator_t1564921419  L_3 = ValueCollection_GetEnumerator_m3487143911(L_2, /*hidden argument*/ValueCollection_GetEnumerator_m3487143911_MethodInfo_var);
		V_2 = L_3;
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0041;
		}

IL_001c:
		{
			Purchase_t160195573 * L_4 = Enumerator_get_Current_m780193209((&V_2), /*hidden argument*/Enumerator_get_Current_m780193209_MethodInfo_var);
			V_1 = L_4;
			Purchase_t160195573 * L_5 = V_1;
			NullCheck(L_5);
			String_t* L_6 = Purchase_get_ItemType_m4191074113(L_5, /*hidden argument*/NULL);
			String_t* L_7 = ___itemType0;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_8 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
			if (!L_8)
			{
				goto IL_0041;
			}
		}

IL_0035:
		{
			List_1_t1765447871 * L_9 = V_0;
			Purchase_t160195573 * L_10 = V_1;
			NullCheck(L_10);
			String_t* L_11 = Purchase_get_Sku_m889168491(L_10, /*hidden argument*/NULL);
			NullCheck(L_9);
			VirtActionInvoker1< String_t* >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.String>::Add(!0) */, L_9, L_11);
		}

IL_0041:
		{
			bool L_12 = Enumerator_MoveNext_m47107108((&V_2), /*hidden argument*/Enumerator_MoveNext_m47107108_MethodInfo_var);
			if (L_12)
			{
				goto IL_001c;
			}
		}

IL_004d:
		{
			IL2CPP_LEAVE(0x5E, FINALLY_0052);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0052;
	}

FINALLY_0052:
	{ // begin finally (depth: 1)
		Enumerator_t1564921419  L_13 = V_2;
		Enumerator_t1564921419  L_14 = L_13;
		Il2CppObject * L_15 = Box(Enumerator_t1564921419_il2cpp_TypeInfo_var, &L_14);
		NullCheck((Il2CppObject *)L_15);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_15);
		IL2CPP_END_FINALLY(82)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(82)
	{
		IL2CPP_JUMP_TBL(0x5E, IL_005e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_005e:
	{
		List_1_t1765447871 * L_16 = V_0;
		return L_16;
	}
}
// System.Collections.Generic.List`1<OnePF.Purchase> OnePF.Inventory::GetAllPurchases()
extern const MethodInfo* Dictionary_2_get_Values_m4177709395_MethodInfo_var;
extern const MethodInfo* Enumerable_ToList_TisPurchase_t160195573_m1369263790_MethodInfo_var;
extern const uint32_t Inventory_GetAllPurchases_m2700192536_MetadataUsageId;
extern "C"  List_1_t957154542 * Inventory_GetAllPurchases_m2700192536 (Inventory_t2630562832 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Inventory_GetAllPurchases_m2700192536_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t1797893477 * L_0 = __this->get__purchaseMap_1();
		NullCheck(L_0);
		ValueCollection_t3720030571 * L_1 = Dictionary_2_get_Values_m4177709395(L_0, /*hidden argument*/Dictionary_2_get_Values_m4177709395_MethodInfo_var);
		List_1_t957154542 * L_2 = Enumerable_ToList_TisPurchase_t160195573_m1369263790(NULL /*static, unused*/, L_1, /*hidden argument*/Enumerable_ToList_TisPurchase_t160195573_m1369263790_MethodInfo_var);
		return L_2;
	}
}
// System.Collections.Generic.List`1<OnePF.SkuDetails> OnePF.Inventory::GetAllAvailableSkus()
extern const MethodInfo* Dictionary_2_get_Values_m2643199215_MethodInfo_var;
extern const MethodInfo* Enumerable_ToList_TisSkuDetails_t70130009_m2376041426_MethodInfo_var;
extern const uint32_t Inventory_GetAllAvailableSkus_m887039689_MetadataUsageId;
extern "C"  List_1_t867088978 * Inventory_GetAllAvailableSkus_m887039689 (Inventory_t2630562832 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Inventory_GetAllAvailableSkus_m887039689_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t1707827913 * L_0 = __this->get__skuMap_0();
		NullCheck(L_0);
		ValueCollection_t3629965007 * L_1 = Dictionary_2_get_Values_m2643199215(L_0, /*hidden argument*/Dictionary_2_get_Values_m2643199215_MethodInfo_var);
		List_1_t867088978 * L_2 = Enumerable_ToList_TisSkuDetails_t70130009_m2376041426(NULL /*static, unused*/, L_1, /*hidden argument*/Enumerable_ToList_TisSkuDetails_t70130009_m2376041426_MethodInfo_var);
		return L_2;
	}
}
// System.Void OnePF.Inventory::AddSkuDetails(OnePF.SkuDetails)
extern "C"  void Inventory_AddSkuDetails_m1656086106 (Inventory_t2630562832 * __this, SkuDetails_t70130009 * ___d0, const MethodInfo* method)
{
	{
		Dictionary_2_t1707827913 * L_0 = __this->get__skuMap_0();
		SkuDetails_t70130009 * L_1 = ___d0;
		NullCheck(L_1);
		String_t* L_2 = SkuDetails_get_Sku_m1697287367(L_1, /*hidden argument*/NULL);
		SkuDetails_t70130009 * L_3 = ___d0;
		NullCheck(L_0);
		VirtActionInvoker2< String_t*, SkuDetails_t70130009 * >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,OnePF.SkuDetails>::Add(!0,!1) */, L_0, L_2, L_3);
		return;
	}
}
// System.Void OnePF.Inventory::AddPurchase(OnePF.Purchase)
extern "C"  void Inventory_AddPurchase_m3006154266 (Inventory_t2630562832 * __this, Purchase_t160195573 * ___p0, const MethodInfo* method)
{
	{
		Dictionary_2_t1797893477 * L_0 = __this->get__purchaseMap_1();
		Purchase_t160195573 * L_1 = ___p0;
		NullCheck(L_1);
		String_t* L_2 = Purchase_get_Sku_m889168491(L_1, /*hidden argument*/NULL);
		Purchase_t160195573 * L_3 = ___p0;
		NullCheck(L_0);
		VirtActionInvoker2< String_t*, Purchase_t160195573 * >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,OnePF.Purchase>::Add(!0,!1) */, L_0, L_2, L_3);
		return;
	}
}
// System.Void OnePF.JSON::.ctor()
extern Il2CppClass* Dictionary_2_t2474804324_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2887377984_MethodInfo_var;
extern const uint32_t JSON__ctor_m802668213_MetadataUsageId;
extern "C"  void JSON__ctor_m802668213 (JSON_t2649481148 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSON__ctor_m802668213_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2474804324 * L_0 = (Dictionary_2_t2474804324 *)il2cpp_codegen_object_new(Dictionary_2_t2474804324_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2887377984(L_0, /*hidden argument*/Dictionary_2__ctor_m2887377984_MethodInfo_var);
		__this->set_fields_0(L_0);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OnePF.JSON::.ctor(System.String)
extern Il2CppClass* Dictionary_2_t2474804324_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2887377984_MethodInfo_var;
extern const uint32_t JSON__ctor_m2841521773_MetadataUsageId;
extern "C"  void JSON__ctor_m2841521773 (JSON_t2649481148 * __this, String_t* ___jsonString0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSON__ctor_m2841521773_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2474804324 * L_0 = (Dictionary_2_t2474804324 *)il2cpp_codegen_object_new(Dictionary_2_t2474804324_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2887377984(L_0, /*hidden argument*/Dictionary_2__ctor_m2887377984_MethodInfo_var);
		__this->set_fields_0(L_0);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		String_t* L_1 = ___jsonString0;
		JSON_set_serialized_m962312562(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Object OnePF.JSON::get_Item(System.String)
extern "C"  Il2CppObject * JSON_get_Item_m1796424386 (JSON_t2649481148 * __this, String_t* ___fieldName0, const MethodInfo* method)
{
	{
		Dictionary_2_t2474804324 * L_0 = __this->get_fields_0();
		String_t* L_1 = ___fieldName0;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, String_t* >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Object>::ContainsKey(!0) */, L_0, L_1);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		Dictionary_2_t2474804324 * L_3 = __this->get_fields_0();
		String_t* L_4 = ___fieldName0;
		NullCheck(L_3);
		Il2CppObject * L_5 = VirtFuncInvoker1< Il2CppObject *, String_t* >::Invoke(22 /* !1 System.Collections.Generic.Dictionary`2<System.String,System.Object>::get_Item(!0) */, L_3, L_4);
		return L_5;
	}

IL_001e:
	{
		return NULL;
	}
}
// System.Void OnePF.JSON::set_Item(System.String,System.Object)
extern "C"  void JSON_set_Item_m996622641 (JSON_t2649481148 * __this, String_t* ___fieldName0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Dictionary_2_t2474804324 * L_0 = __this->get_fields_0();
		String_t* L_1 = ___fieldName0;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, String_t* >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Object>::ContainsKey(!0) */, L_0, L_1);
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		Dictionary_2_t2474804324 * L_3 = __this->get_fields_0();
		String_t* L_4 = ___fieldName0;
		Il2CppObject * L_5 = ___value1;
		NullCheck(L_3);
		VirtActionInvoker2< String_t*, Il2CppObject * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::set_Item(!0,!1) */, L_3, L_4, L_5);
		goto IL_0030;
	}

IL_0023:
	{
		Dictionary_2_t2474804324 * L_6 = __this->get_fields_0();
		String_t* L_7 = ___fieldName0;
		Il2CppObject * L_8 = ___value1;
		NullCheck(L_6);
		VirtActionInvoker2< String_t*, Il2CppObject * >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::Add(!0,!1) */, L_6, L_7, L_8);
	}

IL_0030:
	{
		return;
	}
}
// System.String OnePF.JSON::ToString(System.String)
extern Il2CppClass* Convert_t1097883944_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t JSON_ToString_m2727470212_MetadataUsageId;
extern "C"  String_t* JSON_ToString_m2727470212 (JSON_t2649481148 * __this, String_t* ___fieldName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSON_ToString_m2727470212_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2474804324 * L_0 = __this->get_fields_0();
		String_t* L_1 = ___fieldName0;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, String_t* >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Object>::ContainsKey(!0) */, L_0, L_1);
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		Dictionary_2_t2474804324 * L_3 = __this->get_fields_0();
		String_t* L_4 = ___fieldName0;
		NullCheck(L_3);
		Il2CppObject * L_5 = VirtFuncInvoker1< Il2CppObject *, String_t* >::Invoke(22 /* !1 System.Collections.Generic.Dictionary`2<System.String,System.Object>::get_Item(!0) */, L_3, L_4);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		String_t* L_6 = Convert_ToString_m427788191(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return L_6;
	}

IL_0023:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_7;
	}
}
// System.Int32 OnePF.JSON::ToInt(System.String)
extern Il2CppClass* Convert_t1097883944_il2cpp_TypeInfo_var;
extern const uint32_t JSON_ToInt_m961998353_MetadataUsageId;
extern "C"  int32_t JSON_ToInt_m961998353 (JSON_t2649481148 * __this, String_t* ___fieldName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSON_ToInt_m961998353_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2474804324 * L_0 = __this->get_fields_0();
		String_t* L_1 = ___fieldName0;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, String_t* >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Object>::ContainsKey(!0) */, L_0, L_1);
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		Dictionary_2_t2474804324 * L_3 = __this->get_fields_0();
		String_t* L_4 = ___fieldName0;
		NullCheck(L_3);
		Il2CppObject * L_5 = VirtFuncInvoker1< Il2CppObject *, String_t* >::Invoke(22 /* !1 System.Collections.Generic.Dictionary`2<System.String,System.Object>::get_Item(!0) */, L_3, L_4);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		int32_t L_6 = Convert_ToInt32_m12524065(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return L_6;
	}

IL_0023:
	{
		return 0;
	}
}
// System.Int64 OnePF.JSON::ToLong(System.String)
extern Il2CppClass* Convert_t1097883944_il2cpp_TypeInfo_var;
extern const uint32_t JSON_ToLong_m2611652581_MetadataUsageId;
extern "C"  int64_t JSON_ToLong_m2611652581 (JSON_t2649481148 * __this, String_t* ___fieldName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSON_ToLong_m2611652581_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2474804324 * L_0 = __this->get_fields_0();
		String_t* L_1 = ___fieldName0;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, String_t* >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Object>::ContainsKey(!0) */, L_0, L_1);
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		Dictionary_2_t2474804324 * L_3 = __this->get_fields_0();
		String_t* L_4 = ___fieldName0;
		NullCheck(L_3);
		Il2CppObject * L_5 = VirtFuncInvoker1< Il2CppObject *, String_t* >::Invoke(22 /* !1 System.Collections.Generic.Dictionary`2<System.String,System.Object>::get_Item(!0) */, L_3, L_4);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		int64_t L_6 = Convert_ToInt64_m167563939(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return L_6;
	}

IL_0023:
	{
		return (((int64_t)((int64_t)0)));
	}
}
// System.Single OnePF.JSON::ToFloat(System.String)
extern Il2CppClass* Convert_t1097883944_il2cpp_TypeInfo_var;
extern const uint32_t JSON_ToFloat_m3878641306_MetadataUsageId;
extern "C"  float JSON_ToFloat_m3878641306 (JSON_t2649481148 * __this, String_t* ___fieldName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSON_ToFloat_m3878641306_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2474804324 * L_0 = __this->get_fields_0();
		String_t* L_1 = ___fieldName0;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, String_t* >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Object>::ContainsKey(!0) */, L_0, L_1);
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		Dictionary_2_t2474804324 * L_3 = __this->get_fields_0();
		String_t* L_4 = ___fieldName0;
		NullCheck(L_3);
		Il2CppObject * L_5 = VirtFuncInvoker1< Il2CppObject *, String_t* >::Invoke(22 /* !1 System.Collections.Generic.Dictionary`2<System.String,System.Object>::get_Item(!0) */, L_3, L_4);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		float L_6 = Convert_ToSingle_m1579085759(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return L_6;
	}

IL_0023:
	{
		return (0.0f);
	}
}
// System.Boolean OnePF.JSON::ToBoolean(System.String)
extern Il2CppClass* Convert_t1097883944_il2cpp_TypeInfo_var;
extern const uint32_t JSON_ToBoolean_m2964217310_MetadataUsageId;
extern "C"  bool JSON_ToBoolean_m2964217310 (JSON_t2649481148 * __this, String_t* ___fieldName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSON_ToBoolean_m2964217310_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2474804324 * L_0 = __this->get_fields_0();
		String_t* L_1 = ___fieldName0;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, String_t* >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Object>::ContainsKey(!0) */, L_0, L_1);
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		Dictionary_2_t2474804324 * L_3 = __this->get_fields_0();
		String_t* L_4 = ___fieldName0;
		NullCheck(L_3);
		Il2CppObject * L_5 = VirtFuncInvoker1< Il2CppObject *, String_t* >::Invoke(22 /* !1 System.Collections.Generic.Dictionary`2<System.String,System.Object>::get_Item(!0) */, L_3, L_4);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		bool L_6 = Convert_ToBoolean_m531114797(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return L_6;
	}

IL_0023:
	{
		return (bool)0;
	}
}
// System.String OnePF.JSON::get_serialized()
extern "C"  String_t* JSON_get_serialized_m2868133311 (JSON_t2649481148 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = _JSON_Serialize_m3685512094(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void OnePF.JSON::set_serialized(System.String)
extern "C"  void JSON_set_serialized_m962312562 (JSON_t2649481148 * __this, String_t* ___value0, const MethodInfo* method)
{
	JSON_t2649481148 * V_0 = NULL;
	{
		String_t* L_0 = ___value0;
		JSON_t2649481148 * L_1 = _JSON_Deserialize_m1156793587(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		JSON_t2649481148 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_0019;
		}
	}
	{
		JSON_t2649481148 * L_3 = V_0;
		NullCheck(L_3);
		Dictionary_2_t2474804324 * L_4 = L_3->get_fields_0();
		__this->set_fields_0(L_4);
	}

IL_0019:
	{
		return;
	}
}
// OnePF.JSON OnePF.JSON::ToJSON(System.String)
extern Il2CppClass* JSON_t2649481148_il2cpp_TypeInfo_var;
extern const uint32_t JSON_ToJSON_m1451252037_MetadataUsageId;
extern "C"  JSON_t2649481148 * JSON_ToJSON_m1451252037 (JSON_t2649481148 * __this, String_t* ___fieldName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSON_ToJSON_m1451252037_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2474804324 * L_0 = __this->get_fields_0();
		String_t* L_1 = ___fieldName0;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, String_t* >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Object>::ContainsKey(!0) */, L_0, L_1);
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		Dictionary_2_t2474804324 * L_3 = __this->get_fields_0();
		String_t* L_4 = ___fieldName0;
		JSON_t2649481148 * L_5 = (JSON_t2649481148 *)il2cpp_codegen_object_new(JSON_t2649481148_il2cpp_TypeInfo_var);
		JSON__ctor_m802668213(L_5, /*hidden argument*/NULL);
		NullCheck(L_3);
		VirtActionInvoker2< String_t*, Il2CppObject * >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::Add(!0,!1) */, L_3, L_4, L_5);
	}

IL_0022:
	{
		String_t* L_6 = ___fieldName0;
		Il2CppObject * L_7 = JSON_get_Item_m1796424386(__this, L_6, /*hidden argument*/NULL);
		return ((JSON_t2649481148 *)CastclassClass(L_7, JSON_t2649481148_il2cpp_TypeInfo_var));
	}
}
// UnityEngine.Vector2 OnePF.JSON::op_Implicit(OnePF.JSON)
extern Il2CppClass* Convert_t1097883944_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral120;
extern Il2CppCodeGenString* _stringLiteral121;
extern const uint32_t JSON_op_Implicit_m497151809_MetadataUsageId;
extern "C"  Vector2_t3525329788  JSON_op_Implicit_m497151809 (Il2CppObject * __this /* static, unused */, JSON_t2649481148 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSON_op_Implicit_m497151809_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		JSON_t2649481148 * L_0 = ___value0;
		NullCheck(L_0);
		Il2CppObject * L_1 = JSON_get_Item_m1796424386(L_0, _stringLiteral120, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		float L_2 = Convert_ToSingle_m1579085759(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		JSON_t2649481148 * L_3 = ___value0;
		NullCheck(L_3);
		Il2CppObject * L_4 = JSON_get_Item_m1796424386(L_3, _stringLiteral121, /*hidden argument*/NULL);
		float L_5 = Convert_ToSingle_m1579085759(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		Vector3_t3525329789  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m1846874791(&L_6, L_2, L_5, /*hidden argument*/NULL);
		Vector2_t3525329788  L_7 = Vector2_op_Implicit_m62903196(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// OnePF.JSON OnePF.JSON::op_Explicit(UnityEngine.Vector2)
extern Il2CppClass* JSON_t2649481148_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t958209021_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral120;
extern Il2CppCodeGenString* _stringLiteral121;
extern const uint32_t JSON_op_Explicit_m2790371584_MetadataUsageId;
extern "C"  JSON_t2649481148 * JSON_op_Explicit_m2790371584 (Il2CppObject * __this /* static, unused */, Vector2_t3525329788  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSON_op_Explicit_m2790371584_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JSON_t2649481148 * V_0 = NULL;
	{
		JSON_t2649481148 * L_0 = (JSON_t2649481148 *)il2cpp_codegen_object_new(JSON_t2649481148_il2cpp_TypeInfo_var);
		JSON__ctor_m802668213(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		JSON_t2649481148 * L_1 = V_0;
		float L_2 = (&___value0)->get_x_1();
		float L_3 = L_2;
		Il2CppObject * L_4 = Box(Single_t958209021_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		JSON_set_Item_m996622641(L_1, _stringLiteral120, L_4, /*hidden argument*/NULL);
		JSON_t2649481148 * L_5 = V_0;
		float L_6 = (&___value0)->get_y_2();
		float L_7 = L_6;
		Il2CppObject * L_8 = Box(Single_t958209021_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_5);
		JSON_set_Item_m996622641(L_5, _stringLiteral121, L_8, /*hidden argument*/NULL);
		JSON_t2649481148 * L_9 = V_0;
		return L_9;
	}
}
// UnityEngine.Vector3 OnePF.JSON::op_Implicit(OnePF.JSON)
extern Il2CppClass* Convert_t1097883944_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral120;
extern Il2CppCodeGenString* _stringLiteral121;
extern Il2CppCodeGenString* _stringLiteral122;
extern const uint32_t JSON_op_Implicit_m826917570_MetadataUsageId;
extern "C"  Vector3_t3525329789  JSON_op_Implicit_m826917570 (Il2CppObject * __this /* static, unused */, JSON_t2649481148 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSON_op_Implicit_m826917570_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		JSON_t2649481148 * L_0 = ___value0;
		NullCheck(L_0);
		Il2CppObject * L_1 = JSON_get_Item_m1796424386(L_0, _stringLiteral120, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		float L_2 = Convert_ToSingle_m1579085759(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		JSON_t2649481148 * L_3 = ___value0;
		NullCheck(L_3);
		Il2CppObject * L_4 = JSON_get_Item_m1796424386(L_3, _stringLiteral121, /*hidden argument*/NULL);
		float L_5 = Convert_ToSingle_m1579085759(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		JSON_t2649481148 * L_6 = ___value0;
		NullCheck(L_6);
		Il2CppObject * L_7 = JSON_get_Item_m1796424386(L_6, _stringLiteral122, /*hidden argument*/NULL);
		float L_8 = Convert_ToSingle_m1579085759(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		Vector3_t3525329789  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector3__ctor_m1243251509(&L_9, L_2, L_5, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// OnePF.JSON OnePF.JSON::op_Explicit(UnityEngine.Vector3)
extern Il2CppClass* JSON_t2649481148_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t958209021_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral120;
extern Il2CppCodeGenString* _stringLiteral121;
extern Il2CppCodeGenString* _stringLiteral122;
extern const uint32_t JSON_op_Explicit_m2790371615_MetadataUsageId;
extern "C"  JSON_t2649481148 * JSON_op_Explicit_m2790371615 (Il2CppObject * __this /* static, unused */, Vector3_t3525329789  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSON_op_Explicit_m2790371615_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JSON_t2649481148 * V_0 = NULL;
	{
		JSON_t2649481148 * L_0 = (JSON_t2649481148 *)il2cpp_codegen_object_new(JSON_t2649481148_il2cpp_TypeInfo_var);
		JSON__ctor_m802668213(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		JSON_t2649481148 * L_1 = V_0;
		float L_2 = (&___value0)->get_x_1();
		float L_3 = L_2;
		Il2CppObject * L_4 = Box(Single_t958209021_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		JSON_set_Item_m996622641(L_1, _stringLiteral120, L_4, /*hidden argument*/NULL);
		JSON_t2649481148 * L_5 = V_0;
		float L_6 = (&___value0)->get_y_2();
		float L_7 = L_6;
		Il2CppObject * L_8 = Box(Single_t958209021_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_5);
		JSON_set_Item_m996622641(L_5, _stringLiteral121, L_8, /*hidden argument*/NULL);
		JSON_t2649481148 * L_9 = V_0;
		float L_10 = (&___value0)->get_z_3();
		float L_11 = L_10;
		Il2CppObject * L_12 = Box(Single_t958209021_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_9);
		JSON_set_Item_m996622641(L_9, _stringLiteral122, L_12, /*hidden argument*/NULL);
		JSON_t2649481148 * L_13 = V_0;
		return L_13;
	}
}
// UnityEngine.Quaternion OnePF.JSON::op_Implicit(OnePF.JSON)
extern Il2CppClass* Convert_t1097883944_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral120;
extern Il2CppCodeGenString* _stringLiteral121;
extern Il2CppCodeGenString* _stringLiteral122;
extern Il2CppCodeGenString* _stringLiteral119;
extern const uint32_t JSON_op_Implicit_m1452766860_MetadataUsageId;
extern "C"  Quaternion_t1891715979  JSON_op_Implicit_m1452766860 (Il2CppObject * __this /* static, unused */, JSON_t2649481148 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSON_op_Implicit_m1452766860_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		JSON_t2649481148 * L_0 = ___value0;
		NullCheck(L_0);
		Il2CppObject * L_1 = JSON_get_Item_m1796424386(L_0, _stringLiteral120, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		float L_2 = Convert_ToSingle_m1579085759(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		JSON_t2649481148 * L_3 = ___value0;
		NullCheck(L_3);
		Il2CppObject * L_4 = JSON_get_Item_m1796424386(L_3, _stringLiteral121, /*hidden argument*/NULL);
		float L_5 = Convert_ToSingle_m1579085759(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		JSON_t2649481148 * L_6 = ___value0;
		NullCheck(L_6);
		Il2CppObject * L_7 = JSON_get_Item_m1796424386(L_6, _stringLiteral122, /*hidden argument*/NULL);
		float L_8 = Convert_ToSingle_m1579085759(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		JSON_t2649481148 * L_9 = ___value0;
		NullCheck(L_9);
		Il2CppObject * L_10 = JSON_get_Item_m1796424386(L_9, _stringLiteral119, /*hidden argument*/NULL);
		float L_11 = Convert_ToSingle_m1579085759(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		Quaternion_t1891715979  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Quaternion__ctor_m1100844011(&L_12, L_2, L_5, L_8, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// OnePF.JSON OnePF.JSON::op_Explicit(UnityEngine.Quaternion)
extern Il2CppClass* JSON_t2649481148_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t958209021_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral120;
extern Il2CppCodeGenString* _stringLiteral121;
extern Il2CppCodeGenString* _stringLiteral122;
extern Il2CppCodeGenString* _stringLiteral119;
extern const uint32_t JSON_op_Explicit_m3800652997_MetadataUsageId;
extern "C"  JSON_t2649481148 * JSON_op_Explicit_m3800652997 (Il2CppObject * __this /* static, unused */, Quaternion_t1891715979  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSON_op_Explicit_m3800652997_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JSON_t2649481148 * V_0 = NULL;
	{
		JSON_t2649481148 * L_0 = (JSON_t2649481148 *)il2cpp_codegen_object_new(JSON_t2649481148_il2cpp_TypeInfo_var);
		JSON__ctor_m802668213(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		JSON_t2649481148 * L_1 = V_0;
		float L_2 = (&___value0)->get_x_0();
		float L_3 = L_2;
		Il2CppObject * L_4 = Box(Single_t958209021_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		JSON_set_Item_m996622641(L_1, _stringLiteral120, L_4, /*hidden argument*/NULL);
		JSON_t2649481148 * L_5 = V_0;
		float L_6 = (&___value0)->get_y_1();
		float L_7 = L_6;
		Il2CppObject * L_8 = Box(Single_t958209021_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_5);
		JSON_set_Item_m996622641(L_5, _stringLiteral121, L_8, /*hidden argument*/NULL);
		JSON_t2649481148 * L_9 = V_0;
		float L_10 = (&___value0)->get_z_2();
		float L_11 = L_10;
		Il2CppObject * L_12 = Box(Single_t958209021_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_9);
		JSON_set_Item_m996622641(L_9, _stringLiteral122, L_12, /*hidden argument*/NULL);
		JSON_t2649481148 * L_13 = V_0;
		float L_14 = (&___value0)->get_w_3();
		float L_15 = L_14;
		Il2CppObject * L_16 = Box(Single_t958209021_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_13);
		JSON_set_Item_m996622641(L_13, _stringLiteral119, L_16, /*hidden argument*/NULL);
		JSON_t2649481148 * L_17 = V_0;
		return L_17;
	}
}
// UnityEngine.Color OnePF.JSON::op_Implicit(OnePF.JSON)
extern Il2CppClass* Convert_t1097883944_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral114;
extern Il2CppCodeGenString* _stringLiteral103;
extern Il2CppCodeGenString* _stringLiteral98;
extern Il2CppCodeGenString* _stringLiteral97;
extern const uint32_t JSON_op_Implicit_m3877481941_MetadataUsageId;
extern "C"  Color_t1588175760  JSON_op_Implicit_m3877481941 (Il2CppObject * __this /* static, unused */, JSON_t2649481148 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSON_op_Implicit_m3877481941_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		JSON_t2649481148 * L_0 = ___value0;
		NullCheck(L_0);
		Il2CppObject * L_1 = JSON_get_Item_m1796424386(L_0, _stringLiteral114, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		float L_2 = Convert_ToSingle_m1579085759(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		JSON_t2649481148 * L_3 = ___value0;
		NullCheck(L_3);
		Il2CppObject * L_4 = JSON_get_Item_m1796424386(L_3, _stringLiteral103, /*hidden argument*/NULL);
		float L_5 = Convert_ToSingle_m1579085759(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		JSON_t2649481148 * L_6 = ___value0;
		NullCheck(L_6);
		Il2CppObject * L_7 = JSON_get_Item_m1796424386(L_6, _stringLiteral98, /*hidden argument*/NULL);
		float L_8 = Convert_ToSingle_m1579085759(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		JSON_t2649481148 * L_9 = ___value0;
		NullCheck(L_9);
		Il2CppObject * L_10 = JSON_get_Item_m1796424386(L_9, _stringLiteral97, /*hidden argument*/NULL);
		float L_11 = Convert_ToSingle_m1579085759(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		Color_t1588175760  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Color__ctor_m4005717549(&L_12, L_2, L_5, L_8, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// OnePF.JSON OnePF.JSON::op_Explicit(UnityEngine.Color)
extern Il2CppClass* JSON_t2649481148_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t958209021_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral114;
extern Il2CppCodeGenString* _stringLiteral103;
extern Il2CppCodeGenString* _stringLiteral98;
extern Il2CppCodeGenString* _stringLiteral97;
extern const uint32_t JSON_op_Explicit_m3477382380_MetadataUsageId;
extern "C"  JSON_t2649481148 * JSON_op_Explicit_m3477382380 (Il2CppObject * __this /* static, unused */, Color_t1588175760  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSON_op_Explicit_m3477382380_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JSON_t2649481148 * V_0 = NULL;
	{
		JSON_t2649481148 * L_0 = (JSON_t2649481148 *)il2cpp_codegen_object_new(JSON_t2649481148_il2cpp_TypeInfo_var);
		JSON__ctor_m802668213(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		JSON_t2649481148 * L_1 = V_0;
		float L_2 = (&___value0)->get_r_0();
		float L_3 = L_2;
		Il2CppObject * L_4 = Box(Single_t958209021_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		JSON_set_Item_m996622641(L_1, _stringLiteral114, L_4, /*hidden argument*/NULL);
		JSON_t2649481148 * L_5 = V_0;
		float L_6 = (&___value0)->get_g_1();
		float L_7 = L_6;
		Il2CppObject * L_8 = Box(Single_t958209021_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_5);
		JSON_set_Item_m996622641(L_5, _stringLiteral103, L_8, /*hidden argument*/NULL);
		JSON_t2649481148 * L_9 = V_0;
		float L_10 = (&___value0)->get_b_2();
		float L_11 = L_10;
		Il2CppObject * L_12 = Box(Single_t958209021_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_9);
		JSON_set_Item_m996622641(L_9, _stringLiteral98, L_12, /*hidden argument*/NULL);
		JSON_t2649481148 * L_13 = V_0;
		float L_14 = (&___value0)->get_a_3();
		float L_15 = L_14;
		Il2CppObject * L_16 = Box(Single_t958209021_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_13);
		JSON_set_Item_m996622641(L_13, _stringLiteral97, L_16, /*hidden argument*/NULL);
		JSON_t2649481148 * L_17 = V_0;
		return L_17;
	}
}
// UnityEngine.Color32 OnePF.JSON::op_Implicit(OnePF.JSON)
extern Il2CppClass* Convert_t1097883944_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral114;
extern Il2CppCodeGenString* _stringLiteral103;
extern Il2CppCodeGenString* _stringLiteral98;
extern Il2CppCodeGenString* _stringLiteral97;
extern const uint32_t JSON_op_Implicit_m158659444_MetadataUsageId;
extern "C"  Color32_t4137084207  JSON_op_Implicit_m158659444 (Il2CppObject * __this /* static, unused */, JSON_t2649481148 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSON_op_Implicit_m158659444_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		JSON_t2649481148 * L_0 = ___value0;
		NullCheck(L_0);
		Il2CppObject * L_1 = JSON_get_Item_m1796424386(L_0, _stringLiteral114, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		uint8_t L_2 = Convert_ToByte_m248082303(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		JSON_t2649481148 * L_3 = ___value0;
		NullCheck(L_3);
		Il2CppObject * L_4 = JSON_get_Item_m1796424386(L_3, _stringLiteral103, /*hidden argument*/NULL);
		uint8_t L_5 = Convert_ToByte_m248082303(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		JSON_t2649481148 * L_6 = ___value0;
		NullCheck(L_6);
		Il2CppObject * L_7 = JSON_get_Item_m1796424386(L_6, _stringLiteral98, /*hidden argument*/NULL);
		uint8_t L_8 = Convert_ToByte_m248082303(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		JSON_t2649481148 * L_9 = ___value0;
		NullCheck(L_9);
		Il2CppObject * L_10 = JSON_get_Item_m1796424386(L_9, _stringLiteral97, /*hidden argument*/NULL);
		uint8_t L_11 = Convert_ToByte_m248082303(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		Color32_t4137084207  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Color32__ctor_m576906339(&L_12, L_2, L_5, L_8, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// OnePF.JSON OnePF.JSON::op_Explicit(UnityEngine.Color32)
extern Il2CppClass* JSON_t2649481148_il2cpp_TypeInfo_var;
extern Il2CppClass* Byte_t2778693821_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral114;
extern Il2CppCodeGenString* _stringLiteral103;
extern Il2CppCodeGenString* _stringLiteral98;
extern Il2CppCodeGenString* _stringLiteral97;
extern const uint32_t JSON_op_Explicit_m279922093_MetadataUsageId;
extern "C"  JSON_t2649481148 * JSON_op_Explicit_m279922093 (Il2CppObject * __this /* static, unused */, Color32_t4137084207  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSON_op_Explicit_m279922093_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JSON_t2649481148 * V_0 = NULL;
	{
		JSON_t2649481148 * L_0 = (JSON_t2649481148 *)il2cpp_codegen_object_new(JSON_t2649481148_il2cpp_TypeInfo_var);
		JSON__ctor_m802668213(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		JSON_t2649481148 * L_1 = V_0;
		uint8_t L_2 = (&___value0)->get_r_0();
		uint8_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Byte_t2778693821_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		JSON_set_Item_m996622641(L_1, _stringLiteral114, L_4, /*hidden argument*/NULL);
		JSON_t2649481148 * L_5 = V_0;
		uint8_t L_6 = (&___value0)->get_g_1();
		uint8_t L_7 = L_6;
		Il2CppObject * L_8 = Box(Byte_t2778693821_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_5);
		JSON_set_Item_m996622641(L_5, _stringLiteral103, L_8, /*hidden argument*/NULL);
		JSON_t2649481148 * L_9 = V_0;
		uint8_t L_10 = (&___value0)->get_b_2();
		uint8_t L_11 = L_10;
		Il2CppObject * L_12 = Box(Byte_t2778693821_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_9);
		JSON_set_Item_m996622641(L_9, _stringLiteral98, L_12, /*hidden argument*/NULL);
		JSON_t2649481148 * L_13 = V_0;
		uint8_t L_14 = (&___value0)->get_a_3();
		uint8_t L_15 = L_14;
		Il2CppObject * L_16 = Box(Byte_t2778693821_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_13);
		JSON_set_Item_m996622641(L_13, _stringLiteral97, L_16, /*hidden argument*/NULL);
		JSON_t2649481148 * L_17 = V_0;
		return L_17;
	}
}
// UnityEngine.Rect OnePF.JSON::op_Implicit(OnePF.JSON)
extern Il2CppClass* Convert_t1097883944_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3317767;
extern Il2CppCodeGenString* _stringLiteral115029;
extern Il2CppCodeGenString* _stringLiteral113126854;
extern Il2CppCodeGenString* _stringLiteral3073937703;
extern const uint32_t JSON_op_Implicit_m757897426_MetadataUsageId;
extern "C"  Rect_t1525428817  JSON_op_Implicit_m757897426 (Il2CppObject * __this /* static, unused */, JSON_t2649481148 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSON_op_Implicit_m757897426_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		JSON_t2649481148 * L_0 = ___value0;
		NullCheck(L_0);
		Il2CppObject * L_1 = JSON_get_Item_m1796424386(L_0, _stringLiteral3317767, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		uint8_t L_2 = Convert_ToByte_m248082303(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		JSON_t2649481148 * L_3 = ___value0;
		NullCheck(L_3);
		Il2CppObject * L_4 = JSON_get_Item_m1796424386(L_3, _stringLiteral115029, /*hidden argument*/NULL);
		uint8_t L_5 = Convert_ToByte_m248082303(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		JSON_t2649481148 * L_6 = ___value0;
		NullCheck(L_6);
		Il2CppObject * L_7 = JSON_get_Item_m1796424386(L_6, _stringLiteral113126854, /*hidden argument*/NULL);
		uint8_t L_8 = Convert_ToByte_m248082303(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		JSON_t2649481148 * L_9 = ___value0;
		NullCheck(L_9);
		Il2CppObject * L_10 = JSON_get_Item_m1796424386(L_9, _stringLiteral3073937703, /*hidden argument*/NULL);
		uint8_t L_11 = Convert_ToByte_m248082303(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		Rect_t1525428817  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Rect__ctor_m2378035624(&L_12, (((float)((float)L_2))), (((float)((float)L_5))), (((float)((float)L_8))), (((float)((float)L_11))), /*hidden argument*/NULL);
		return L_12;
	}
}
// OnePF.JSON OnePF.JSON::op_Explicit(UnityEngine.Rect)
extern Il2CppClass* JSON_t2649481148_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t958209021_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3317767;
extern Il2CppCodeGenString* _stringLiteral115029;
extern Il2CppCodeGenString* _stringLiteral113126854;
extern Il2CppCodeGenString* _stringLiteral3073937703;
extern const uint32_t JSON_op_Explicit_m1372645951_MetadataUsageId;
extern "C"  JSON_t2649481148 * JSON_op_Explicit_m1372645951 (Il2CppObject * __this /* static, unused */, Rect_t1525428817  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSON_op_Explicit_m1372645951_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JSON_t2649481148 * V_0 = NULL;
	{
		JSON_t2649481148 * L_0 = (JSON_t2649481148 *)il2cpp_codegen_object_new(JSON_t2649481148_il2cpp_TypeInfo_var);
		JSON__ctor_m802668213(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		JSON_t2649481148 * L_1 = V_0;
		float L_2 = Rect_get_xMin_m371109962((&___value0), /*hidden argument*/NULL);
		float L_3 = L_2;
		Il2CppObject * L_4 = Box(Single_t958209021_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		JSON_set_Item_m996622641(L_1, _stringLiteral3317767, L_4, /*hidden argument*/NULL);
		JSON_t2649481148 * L_5 = V_0;
		float L_6 = Rect_get_yMax_m399510395((&___value0), /*hidden argument*/NULL);
		float L_7 = L_6;
		Il2CppObject * L_8 = Box(Single_t958209021_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_5);
		JSON_set_Item_m996622641(L_5, _stringLiteral115029, L_8, /*hidden argument*/NULL);
		JSON_t2649481148 * L_9 = V_0;
		float L_10 = Rect_get_width_m44556481((&___value0), /*hidden argument*/NULL);
		float L_11 = L_10;
		Il2CppObject * L_12 = Box(Single_t958209021_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_9);
		JSON_set_Item_m996622641(L_9, _stringLiteral113126854, L_12, /*hidden argument*/NULL);
		JSON_t2649481148 * L_13 = V_0;
		float L_14 = Rect_get_height_m1885065262((&___value0), /*hidden argument*/NULL);
		float L_15 = L_14;
		Il2CppObject * L_16 = Box(Single_t958209021_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_13);
		JSON_set_Item_m996622641(L_13, _stringLiteral3073937703, L_16, /*hidden argument*/NULL);
		JSON_t2649481148 * L_17 = V_0;
		return L_17;
	}
}
// System.Void OnePF.JSON/_JSON::.ctor()
extern "C"  void _JSON__ctor_m4052829501 (_JSON_t90021319 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// OnePF.JSON OnePF.JSON/_JSON::Deserialize(System.String)
extern "C"  JSON_t2649481148 * _JSON_Deserialize_m1156793587 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___json0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (JSON_t2649481148 *)NULL;
	}

IL_0008:
	{
		String_t* L_1 = ___json0;
		JSON_t2649481148 * L_2 = Parser_Parse_m839535979(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String OnePF.JSON/_JSON::Serialize(OnePF.JSON)
extern "C"  String_t* _JSON_Serialize_m3685512094 (Il2CppObject * __this /* static, unused */, JSON_t2649481148 * ___obj0, const MethodInfo* method)
{
	{
		JSON_t2649481148 * L_0 = ___obj0;
		String_t* L_1 = Serializer_Serialize_m1966622095(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void OnePF.JSON/_JSON/Parser::.ctor(System.String)
extern Il2CppClass* StringReader_t2229325051_il2cpp_TypeInfo_var;
extern const uint32_t Parser__ctor_m3477549511_MetadataUsageId;
extern "C"  void Parser__ctor_m3477549511 (Parser_t2383423553 * __this, String_t* ___jsonString0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Parser__ctor_m3477549511_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___jsonString0;
		StringReader_t2229325051 * L_1 = (StringReader_t2229325051 *)il2cpp_codegen_object_new(StringReader_t2229325051_il2cpp_TypeInfo_var);
		StringReader__ctor_m1181104909(L_1, L_0, /*hidden argument*/NULL);
		__this->set_json_2(L_1);
		return;
	}
}
// OnePF.JSON OnePF.JSON/_JSON/Parser::Parse(System.String)
extern Il2CppClass* Parser_t2383423553_il2cpp_TypeInfo_var;
extern Il2CppClass* JSON_t2649481148_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Parser_Parse_m839535979_MetadataUsageId;
extern "C"  JSON_t2649481148 * Parser_Parse_m839535979 (Il2CppObject * __this /* static, unused */, String_t* ___jsonString0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Parser_Parse_m839535979_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Parser_t2383423553 * V_0 = NULL;
	JSON_t2649481148 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___jsonString0;
		Parser_t2383423553 * L_1 = (Parser_t2383423553 *)il2cpp_codegen_object_new(Parser_t2383423553_il2cpp_TypeInfo_var);
		Parser__ctor_m3477549511(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			Parser_t2383423553 * L_2 = V_0;
			NullCheck(L_2);
			Il2CppObject * L_3 = Parser_ParseValue_m3306942770(L_2, /*hidden argument*/NULL);
			V_1 = ((JSON_t2649481148 *)IsInstClass(L_3, JSON_t2649481148_il2cpp_TypeInfo_var));
			IL2CPP_LEAVE(0x2A, FINALLY_001d);
		}

IL_0018:
		{
			; // IL_0018: leave IL_002a
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_001d;
	}

FINALLY_001d:
	{ // begin finally (depth: 1)
		{
			Parser_t2383423553 * L_4 = V_0;
			if (!L_4)
			{
				goto IL_0029;
			}
		}

IL_0023:
		{
			Parser_t2383423553 * L_5 = V_0;
			NullCheck(L_5);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_5);
		}

IL_0029:
		{
			IL2CPP_END_FINALLY(29)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(29)
	{
		IL2CPP_JUMP_TBL(0x2A, IL_002a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002a:
	{
		JSON_t2649481148 * L_6 = V_1;
		return L_6;
	}
}
// System.Void OnePF.JSON/_JSON/Parser::Dispose()
extern "C"  void Parser_Dispose_m4146750104 (Parser_t2383423553 * __this, const MethodInfo* method)
{
	{
		StringReader_t2229325051 * L_0 = __this->get_json_2();
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(4 /* System.Void System.IO.TextReader::Dispose() */, L_0);
		__this->set_json_2((StringReader_t2229325051 *)NULL);
		return;
	}
}
// OnePF.JSON OnePF.JSON/_JSON/Parser::ParseObject()
extern Il2CppClass* Dictionary_2_t2474804324_il2cpp_TypeInfo_var;
extern Il2CppClass* JSON_t2649481148_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2887377984_MethodInfo_var;
extern const uint32_t Parser_ParseObject_m1044872726_MetadataUsageId;
extern "C"  JSON_t2649481148 * Parser_ParseObject_m1044872726 (Parser_t2383423553 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Parser_ParseObject_m1044872726_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t2474804324 * V_0 = NULL;
	JSON_t2649481148 * V_1 = NULL;
	String_t* V_2 = NULL;
	int32_t V_3 = 0;
	{
		Dictionary_2_t2474804324 * L_0 = (Dictionary_2_t2474804324 *)il2cpp_codegen_object_new(Dictionary_2_t2474804324_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2887377984(L_0, /*hidden argument*/Dictionary_2__ctor_m2887377984_MethodInfo_var);
		V_0 = L_0;
		JSON_t2649481148 * L_1 = (JSON_t2649481148 *)il2cpp_codegen_object_new(JSON_t2649481148_il2cpp_TypeInfo_var);
		JSON__ctor_m802668213(L_1, /*hidden argument*/NULL);
		V_1 = L_1;
		JSON_t2649481148 * L_2 = V_1;
		Dictionary_2_t2474804324 * L_3 = V_0;
		NullCheck(L_2);
		L_2->set_fields_0(L_3);
		StringReader_t2229325051 * L_4 = __this->get_json_2();
		NullCheck(L_4);
		VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.IO.StringReader::Read() */, L_4);
	}

IL_001f:
	{
		int32_t L_5 = Parser_get_NextToken_m2219232515(__this, /*hidden argument*/NULL);
		V_3 = L_5;
		int32_t L_6 = V_3;
		if (L_6 == 0)
		{
			goto IL_0044;
		}
		if (L_6 == 1)
		{
			goto IL_0038;
		}
		if (L_6 == 2)
		{
			goto IL_004b;
		}
	}

IL_0038:
	{
		int32_t L_7 = V_3;
		if ((((int32_t)L_7) == ((int32_t)6)))
		{
			goto IL_0046;
		}
	}
	{
		goto IL_004d;
	}

IL_0044:
	{
		return (JSON_t2649481148 *)NULL;
	}

IL_0046:
	{
		goto IL_001f;
	}

IL_004b:
	{
		JSON_t2649481148 * L_8 = V_1;
		return L_8;
	}

IL_004d:
	{
		String_t* L_9 = Parser_ParseString_m2727177408(__this, /*hidden argument*/NULL);
		V_2 = L_9;
		String_t* L_10 = V_2;
		if (L_10)
		{
			goto IL_005c;
		}
	}
	{
		return (JSON_t2649481148 *)NULL;
	}

IL_005c:
	{
		int32_t L_11 = Parser_get_NextToken_m2219232515(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_11) == ((int32_t)5)))
		{
			goto IL_006a;
		}
	}
	{
		return (JSON_t2649481148 *)NULL;
	}

IL_006a:
	{
		StringReader_t2229325051 * L_12 = __this->get_json_2();
		NullCheck(L_12);
		VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.IO.StringReader::Read() */, L_12);
		Dictionary_2_t2474804324 * L_13 = V_0;
		String_t* L_14 = V_2;
		Il2CppObject * L_15 = Parser_ParseValue_m3306942770(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		VirtActionInvoker2< String_t*, Il2CppObject * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::set_Item(!0,!1) */, L_13, L_14, L_15);
		goto IL_0088;
	}

IL_0088:
	{
		goto IL_001f;
	}
}
// System.Collections.Generic.List`1<System.Object> OnePF.JSON/_JSON/Parser::ParseArray()
extern Il2CppClass* List_1_t1634065389_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m348833073_MethodInfo_var;
extern const uint32_t Parser_ParseArray_m3171442056_MetadataUsageId;
extern "C"  List_1_t1634065389 * Parser_ParseArray_m3171442056 (Parser_t2383423553 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Parser_ParseArray_m3171442056_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t1634065389 * V_0 = NULL;
	bool V_1 = false;
	int32_t V_2 = 0;
	Il2CppObject * V_3 = NULL;
	int32_t V_4 = 0;
	{
		List_1_t1634065389 * L_0 = (List_1_t1634065389 *)il2cpp_codegen_object_new(List_1_t1634065389_il2cpp_TypeInfo_var);
		List_1__ctor_m348833073(L_0, /*hidden argument*/List_1__ctor_m348833073_MethodInfo_var);
		V_0 = L_0;
		StringReader_t2229325051 * L_1 = __this->get_json_2();
		NullCheck(L_1);
		VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.IO.StringReader::Read() */, L_1);
		V_1 = (bool)1;
		goto IL_0066;
	}

IL_0019:
	{
		int32_t L_2 = Parser_get_NextToken_m2219232515(__this, /*hidden argument*/NULL);
		V_2 = L_2;
		int32_t L_3 = V_2;
		V_4 = L_3;
		int32_t L_4 = V_4;
		if (((int32_t)((int32_t)L_4-(int32_t)4)) == 0)
		{
			goto IL_004b;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)4)) == 1)
		{
			goto IL_0038;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)4)) == 2)
		{
			goto IL_0046;
		}
	}

IL_0038:
	{
		int32_t L_5 = V_4;
		if (!L_5)
		{
			goto IL_0044;
		}
	}
	{
		goto IL_0052;
	}

IL_0044:
	{
		return (List_1_t1634065389 *)NULL;
	}

IL_0046:
	{
		goto IL_0066;
	}

IL_004b:
	{
		V_1 = (bool)0;
		goto IL_0066;
	}

IL_0052:
	{
		int32_t L_6 = V_2;
		Il2CppObject * L_7 = Parser_ParseByToken_m3825560905(__this, L_6, /*hidden argument*/NULL);
		V_3 = L_7;
		List_1_t1634065389 * L_8 = V_0;
		Il2CppObject * L_9 = V_3;
		NullCheck(L_8);
		VirtActionInvoker1< Il2CppObject * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Object>::Add(!0) */, L_8, L_9);
		goto IL_0066;
	}

IL_0066:
	{
		bool L_10 = V_1;
		if (L_10)
		{
			goto IL_0019;
		}
	}
	{
		List_1_t1634065389 * L_11 = V_0;
		return L_11;
	}
}
// System.Object OnePF.JSON/_JSON/Parser::ParseValue()
extern "C"  Il2CppObject * Parser_ParseValue_m3306942770 (Parser_t2383423553 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = Parser_get_NextToken_m2219232515(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		Il2CppObject * L_2 = Parser_ParseByToken_m3825560905(__this, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Object OnePF.JSON/_JSON/Parser::ParseByToken(OnePF.JSON/_JSON/Parser/TOKEN)
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern const uint32_t Parser_ParseByToken_m3825560905_MetadataUsageId;
extern "C"  Il2CppObject * Parser_ParseByToken_m3825560905 (Parser_t2383423553 * __this, int32_t ___token0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Parser_ParseByToken_m3825560905_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___token0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 0)
		{
			goto IL_0049;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 1)
		{
			goto IL_0067;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 2)
		{
			goto IL_0050;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 3)
		{
			goto IL_0067;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 4)
		{
			goto IL_0067;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 5)
		{
			goto IL_0067;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 6)
		{
			goto IL_003b;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 7)
		{
			goto IL_0042;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 8)
		{
			goto IL_0057;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 9)
		{
			goto IL_005e;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 10)
		{
			goto IL_0065;
		}
	}
	{
		goto IL_0067;
	}

IL_003b:
	{
		String_t* L_2 = Parser_ParseString_m2727177408(__this, /*hidden argument*/NULL);
		return L_2;
	}

IL_0042:
	{
		Il2CppObject * L_3 = Parser_ParseNumber_m3254755082(__this, /*hidden argument*/NULL);
		return L_3;
	}

IL_0049:
	{
		JSON_t2649481148 * L_4 = Parser_ParseObject_m1044872726(__this, /*hidden argument*/NULL);
		return L_4;
	}

IL_0050:
	{
		List_1_t1634065389 * L_5 = Parser_ParseArray_m3171442056(__this, /*hidden argument*/NULL);
		return L_5;
	}

IL_0057:
	{
		bool L_6 = ((bool)1);
		Il2CppObject * L_7 = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &L_6);
		return L_7;
	}

IL_005e:
	{
		bool L_8 = ((bool)0);
		Il2CppObject * L_9 = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &L_8);
		return L_9;
	}

IL_0065:
	{
		return NULL;
	}

IL_0067:
	{
		return NULL;
	}
}
// System.String OnePF.JSON/_JSON/Parser::ParseString()
extern Il2CppClass* StringBuilder_t3822575854_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1097883944_il2cpp_TypeInfo_var;
extern const uint32_t Parser_ParseString_m2727177408_MetadataUsageId;
extern "C"  String_t* Parser_ParseString_m2727177408 (Parser_t2383423553 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Parser_ParseString_m2727177408_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t3822575854 * V_0 = NULL;
	uint16_t V_1 = 0x0;
	bool V_2 = false;
	StringBuilder_t3822575854 * V_3 = NULL;
	int32_t V_4 = 0;
	uint16_t V_5 = 0x0;
	uint16_t V_6 = 0x0;
	{
		StringBuilder_t3822575854 * L_0 = (StringBuilder_t3822575854 *)il2cpp_codegen_object_new(StringBuilder_t3822575854_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m135953004(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		StringReader_t2229325051 * L_1 = __this->get_json_2();
		NullCheck(L_1);
		VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.IO.StringReader::Read() */, L_1);
		V_2 = (bool)1;
		goto IL_017e;
	}

IL_0019:
	{
		StringReader_t2229325051 * L_2 = __this->get_json_2();
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 System.IO.StringReader::Peek() */, L_2);
		if ((!(((uint32_t)L_3) == ((uint32_t)(-1)))))
		{
			goto IL_0031;
		}
	}
	{
		V_2 = (bool)0;
		goto IL_0184;
	}

IL_0031:
	{
		uint16_t L_4 = Parser_get_NextChar_m965618621(__this, /*hidden argument*/NULL);
		V_1 = L_4;
		uint16_t L_5 = V_1;
		V_5 = L_5;
		uint16_t L_6 = V_5;
		if ((((int32_t)L_6) == ((int32_t)((int32_t)34))))
		{
			goto IL_0052;
		}
	}
	{
		uint16_t L_7 = V_5;
		if ((((int32_t)L_7) == ((int32_t)((int32_t)92))))
		{
			goto IL_0059;
		}
	}
	{
		goto IL_0171;
	}

IL_0052:
	{
		V_2 = (bool)0;
		goto IL_017e;
	}

IL_0059:
	{
		StringReader_t2229325051 * L_8 = __this->get_json_2();
		NullCheck(L_8);
		int32_t L_9 = VirtFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 System.IO.StringReader::Peek() */, L_8);
		if ((!(((uint32_t)L_9) == ((uint32_t)(-1)))))
		{
			goto IL_0071;
		}
	}
	{
		V_2 = (bool)0;
		goto IL_017e;
	}

IL_0071:
	{
		uint16_t L_10 = Parser_get_NextChar_m965618621(__this, /*hidden argument*/NULL);
		V_1 = L_10;
		uint16_t L_11 = V_1;
		V_6 = L_11;
		uint16_t L_12 = V_6;
		if (((int32_t)((int32_t)L_12-(int32_t)((int32_t)110))) == 0)
		{
			goto IL_00ff;
		}
		if (((int32_t)((int32_t)L_12-(int32_t)((int32_t)110))) == 1)
		{
			goto IL_00a5;
		}
		if (((int32_t)((int32_t)L_12-(int32_t)((int32_t)110))) == 2)
		{
			goto IL_00a5;
		}
		if (((int32_t)((int32_t)L_12-(int32_t)((int32_t)110))) == 3)
		{
			goto IL_00a5;
		}
		if (((int32_t)((int32_t)L_12-(int32_t)((int32_t)110))) == 4)
		{
			goto IL_010d;
		}
		if (((int32_t)((int32_t)L_12-(int32_t)((int32_t)110))) == 5)
		{
			goto IL_00a5;
		}
		if (((int32_t)((int32_t)L_12-(int32_t)((int32_t)110))) == 6)
		{
			goto IL_011b;
		}
		if (((int32_t)((int32_t)L_12-(int32_t)((int32_t)110))) == 7)
		{
			goto IL_0129;
		}
	}

IL_00a5:
	{
		uint16_t L_13 = V_6;
		if ((((int32_t)L_13) == ((int32_t)((int32_t)34))))
		{
			goto IL_00d7;
		}
	}
	{
		uint16_t L_14 = V_6;
		if ((((int32_t)L_14) == ((int32_t)((int32_t)47))))
		{
			goto IL_00d7;
		}
	}
	{
		uint16_t L_15 = V_6;
		if ((((int32_t)L_15) == ((int32_t)((int32_t)92))))
		{
			goto IL_00d7;
		}
	}
	{
		uint16_t L_16 = V_6;
		if ((((int32_t)L_16) == ((int32_t)((int32_t)98))))
		{
			goto IL_00e4;
		}
	}
	{
		uint16_t L_17 = V_6;
		if ((((int32_t)L_17) == ((int32_t)((int32_t)102))))
		{
			goto IL_00f1;
		}
	}
	{
		goto IL_016c;
	}

IL_00d7:
	{
		StringBuilder_t3822575854 * L_18 = V_0;
		uint16_t L_19 = V_1;
		NullCheck(L_18);
		StringBuilder_Append_m2143093878(L_18, L_19, /*hidden argument*/NULL);
		goto IL_016c;
	}

IL_00e4:
	{
		StringBuilder_t3822575854 * L_20 = V_0;
		NullCheck(L_20);
		StringBuilder_Append_m2143093878(L_20, 8, /*hidden argument*/NULL);
		goto IL_016c;
	}

IL_00f1:
	{
		StringBuilder_t3822575854 * L_21 = V_0;
		NullCheck(L_21);
		StringBuilder_Append_m2143093878(L_21, ((int32_t)12), /*hidden argument*/NULL);
		goto IL_016c;
	}

IL_00ff:
	{
		StringBuilder_t3822575854 * L_22 = V_0;
		NullCheck(L_22);
		StringBuilder_Append_m2143093878(L_22, ((int32_t)10), /*hidden argument*/NULL);
		goto IL_016c;
	}

IL_010d:
	{
		StringBuilder_t3822575854 * L_23 = V_0;
		NullCheck(L_23);
		StringBuilder_Append_m2143093878(L_23, ((int32_t)13), /*hidden argument*/NULL);
		goto IL_016c;
	}

IL_011b:
	{
		StringBuilder_t3822575854 * L_24 = V_0;
		NullCheck(L_24);
		StringBuilder_Append_m2143093878(L_24, ((int32_t)9), /*hidden argument*/NULL);
		goto IL_016c;
	}

IL_0129:
	{
		StringBuilder_t3822575854 * L_25 = (StringBuilder_t3822575854 *)il2cpp_codegen_object_new(StringBuilder_t3822575854_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m135953004(L_25, /*hidden argument*/NULL);
		V_3 = L_25;
		V_4 = 0;
		goto IL_014a;
	}

IL_0137:
	{
		StringBuilder_t3822575854 * L_26 = V_3;
		uint16_t L_27 = Parser_get_NextChar_m965618621(__this, /*hidden argument*/NULL);
		NullCheck(L_26);
		StringBuilder_Append_m2143093878(L_26, L_27, /*hidden argument*/NULL);
		int32_t L_28 = V_4;
		V_4 = ((int32_t)((int32_t)L_28+(int32_t)1));
	}

IL_014a:
	{
		int32_t L_29 = V_4;
		if ((((int32_t)L_29) < ((int32_t)4)))
		{
			goto IL_0137;
		}
	}
	{
		StringBuilder_t3822575854 * L_30 = V_0;
		StringBuilder_t3822575854 * L_31 = V_3;
		NullCheck(L_31);
		String_t* L_32 = StringBuilder_ToString_m350379841(L_31, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		int32_t L_33 = Convert_ToInt32_m2645435816(NULL /*static, unused*/, L_32, ((int32_t)16), /*hidden argument*/NULL);
		NullCheck(L_30);
		StringBuilder_Append_m2143093878(L_30, (((int32_t)((uint16_t)L_33))), /*hidden argument*/NULL);
		goto IL_016c;
	}

IL_016c:
	{
		goto IL_017e;
	}

IL_0171:
	{
		StringBuilder_t3822575854 * L_34 = V_0;
		uint16_t L_35 = V_1;
		NullCheck(L_34);
		StringBuilder_Append_m2143093878(L_34, L_35, /*hidden argument*/NULL);
		goto IL_017e;
	}

IL_017e:
	{
		bool L_36 = V_2;
		if (L_36)
		{
			goto IL_0019;
		}
	}

IL_0184:
	{
		StringBuilder_t3822575854 * L_37 = V_0;
		NullCheck(L_37);
		String_t* L_38 = StringBuilder_ToString_m350379841(L_37, /*hidden argument*/NULL);
		return L_38;
	}
}
// System.Object OnePF.JSON/_JSON/Parser::ParseNumber()
extern Il2CppClass* Int64_t2847414882_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern const uint32_t Parser_ParseNumber_m3254755082_MetadataUsageId;
extern "C"  Il2CppObject * Parser_ParseNumber_m3254755082 (Parser_t2383423553 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Parser_ParseNumber_m3254755082_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	int64_t V_1 = 0;
	double V_2 = 0.0;
	{
		String_t* L_0 = Parser_get_NextWord_m2256749100(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = V_0;
		NullCheck(L_1);
		int32_t L_2 = String_IndexOf_m2775210486(L_1, ((int32_t)46), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_0025;
		}
	}
	{
		String_t* L_3 = V_0;
		Int64_TryParse_m2106581948(NULL /*static, unused*/, L_3, (&V_1), /*hidden argument*/NULL);
		int64_t L_4 = V_1;
		int64_t L_5 = L_4;
		Il2CppObject * L_6 = Box(Int64_t2847414882_il2cpp_TypeInfo_var, &L_5);
		return L_6;
	}

IL_0025:
	{
		String_t* L_7 = V_0;
		Double_TryParse_m2709942532(NULL /*static, unused*/, L_7, (&V_2), /*hidden argument*/NULL);
		double L_8 = V_2;
		double L_9 = L_8;
		Il2CppObject * L_10 = Box(Double_t534516614_il2cpp_TypeInfo_var, &L_9);
		return L_10;
	}
}
// System.Void OnePF.JSON/_JSON/Parser::EatWhitespace()
extern Il2CppCodeGenString* _stringLiteral962284;
extern const uint32_t Parser_EatWhitespace_m1457471726_MetadataUsageId;
extern "C"  void Parser_EatWhitespace_m1457471726 (Parser_t2383423553 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Parser_EatWhitespace_m1457471726_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		goto IL_0027;
	}

IL_0005:
	{
		StringReader_t2229325051 * L_0 = __this->get_json_2();
		NullCheck(L_0);
		VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.IO.StringReader::Read() */, L_0);
		StringReader_t2229325051 * L_1 = __this->get_json_2();
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 System.IO.StringReader::Peek() */, L_1);
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_0027;
		}
	}
	{
		goto IL_003d;
	}

IL_0027:
	{
		uint16_t L_3 = Parser_get_PeekChar_m2421335077(__this, /*hidden argument*/NULL);
		NullCheck(_stringLiteral962284);
		int32_t L_4 = String_IndexOf_m2775210486(_stringLiteral962284, L_3, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)(-1)))))
		{
			goto IL_0005;
		}
	}

IL_003d:
	{
		return;
	}
}
// System.Char OnePF.JSON/_JSON/Parser::get_PeekChar()
extern Il2CppClass* Convert_t1097883944_il2cpp_TypeInfo_var;
extern const uint32_t Parser_get_PeekChar_m2421335077_MetadataUsageId;
extern "C"  uint16_t Parser_get_PeekChar_m2421335077 (Parser_t2383423553 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Parser_get_PeekChar_m2421335077_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringReader_t2229325051 * L_0 = __this->get_json_2();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 System.IO.StringReader::Peek() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		uint16_t L_2 = Convert_ToChar_m353236550(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Char OnePF.JSON/_JSON/Parser::get_NextChar()
extern Il2CppClass* Convert_t1097883944_il2cpp_TypeInfo_var;
extern const uint32_t Parser_get_NextChar_m965618621_MetadataUsageId;
extern "C"  uint16_t Parser_get_NextChar_m965618621 (Parser_t2383423553 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Parser_get_NextChar_m965618621_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringReader_t2229325051 * L_0 = __this->get_json_2();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.IO.StringReader::Read() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		uint16_t L_2 = Convert_ToChar_m353236550(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String OnePF.JSON/_JSON/Parser::get_NextWord()
extern Il2CppClass* StringBuilder_t3822575854_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2304801572;
extern const uint32_t Parser_get_NextWord_m2256749100_MetadataUsageId;
extern "C"  String_t* Parser_get_NextWord_m2256749100 (Parser_t2383423553 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Parser_get_NextWord_m2256749100_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t3822575854 * V_0 = NULL;
	{
		StringBuilder_t3822575854 * L_0 = (StringBuilder_t3822575854 *)il2cpp_codegen_object_new(StringBuilder_t3822575854_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m135953004(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_002e;
	}

IL_000b:
	{
		StringBuilder_t3822575854 * L_1 = V_0;
		uint16_t L_2 = Parser_get_NextChar_m965618621(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		StringBuilder_Append_m2143093878(L_1, L_2, /*hidden argument*/NULL);
		StringReader_t2229325051 * L_3 = __this->get_json_2();
		NullCheck(L_3);
		int32_t L_4 = VirtFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 System.IO.StringReader::Peek() */, L_3);
		if ((!(((uint32_t)L_4) == ((uint32_t)(-1)))))
		{
			goto IL_002e;
		}
	}
	{
		goto IL_0044;
	}

IL_002e:
	{
		uint16_t L_5 = Parser_get_PeekChar_m2421335077(__this, /*hidden argument*/NULL);
		NullCheck(_stringLiteral2304801572);
		int32_t L_6 = String_IndexOf_m2775210486(_stringLiteral2304801572, L_5, /*hidden argument*/NULL);
		if ((((int32_t)L_6) == ((int32_t)(-1))))
		{
			goto IL_000b;
		}
	}

IL_0044:
	{
		StringBuilder_t3822575854 * L_7 = V_0;
		NullCheck(L_7);
		String_t* L_8 = StringBuilder_ToString_m350379841(L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// OnePF.JSON/_JSON/Parser/TOKEN OnePF.JSON/_JSON/Parser::get_NextToken()
extern Il2CppClass* Parser_t2383423553_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t190145395_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1958628151_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral97196323;
extern Il2CppCodeGenString* _stringLiteral3569038;
extern Il2CppCodeGenString* _stringLiteral3392903;
extern const uint32_t Parser_get_NextToken_m2219232515_MetadataUsageId;
extern "C"  int32_t Parser_get_NextToken_m2219232515 (Parser_t2383423553 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Parser_get_NextToken_m2219232515_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0x0;
	String_t* V_1 = NULL;
	uint16_t V_2 = 0x0;
	String_t* V_3 = NULL;
	Dictionary_2_t190145395 * V_4 = NULL;
	int32_t V_5 = 0;
	{
		Parser_EatWhitespace_m1457471726(__this, /*hidden argument*/NULL);
		StringReader_t2229325051 * L_0 = __this->get_json_2();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 System.IO.StringReader::Peek() */, L_0);
		if ((!(((uint32_t)L_1) == ((uint32_t)(-1)))))
		{
			goto IL_0019;
		}
	}
	{
		return (int32_t)(0);
	}

IL_0019:
	{
		uint16_t L_2 = Parser_get_PeekChar_m2421335077(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		uint16_t L_3 = V_0;
		V_2 = L_3;
		uint16_t L_4 = V_2;
		if (((int32_t)((int32_t)L_4-(int32_t)((int32_t)34))) == 0)
		{
			goto IL_00ec;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)((int32_t)34))) == 1)
		{
			goto IL_008f;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)((int32_t)34))) == 2)
		{
			goto IL_008f;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)((int32_t)34))) == 3)
		{
			goto IL_008f;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)((int32_t)34))) == 4)
		{
			goto IL_008f;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)((int32_t)34))) == 5)
		{
			goto IL_008f;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)((int32_t)34))) == 6)
		{
			goto IL_008f;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)((int32_t)34))) == 7)
		{
			goto IL_008f;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)((int32_t)34))) == 8)
		{
			goto IL_008f;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)((int32_t)34))) == 9)
		{
			goto IL_008f;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)((int32_t)34))) == 10)
		{
			goto IL_00de;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)((int32_t)34))) == 11)
		{
			goto IL_00f0;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)((int32_t)34))) == 12)
		{
			goto IL_008f;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)((int32_t)34))) == 13)
		{
			goto IL_008f;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)((int32_t)34))) == 14)
		{
			goto IL_00f0;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)((int32_t)34))) == 15)
		{
			goto IL_00f0;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)((int32_t)34))) == 16)
		{
			goto IL_00f0;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)((int32_t)34))) == 17)
		{
			goto IL_00f0;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)((int32_t)34))) == 18)
		{
			goto IL_00f0;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)((int32_t)34))) == 19)
		{
			goto IL_00f0;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)((int32_t)34))) == 20)
		{
			goto IL_00f0;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)((int32_t)34))) == 21)
		{
			goto IL_00f0;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)((int32_t)34))) == 22)
		{
			goto IL_00f0;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)((int32_t)34))) == 23)
		{
			goto IL_00f0;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)((int32_t)34))) == 24)
		{
			goto IL_00ee;
		}
	}

IL_008f:
	{
		uint16_t L_5 = V_2;
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)91))) == 0)
		{
			goto IL_00ce;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)91))) == 1)
		{
			goto IL_00a4;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)91))) == 2)
		{
			goto IL_00d0;
		}
	}

IL_00a4:
	{
		uint16_t L_6 = V_2;
		if (((int32_t)((int32_t)L_6-(int32_t)((int32_t)123))) == 0)
		{
			goto IL_00be;
		}
		if (((int32_t)((int32_t)L_6-(int32_t)((int32_t)123))) == 1)
		{
			goto IL_00f2;
		}
		if (((int32_t)((int32_t)L_6-(int32_t)((int32_t)123))) == 2)
		{
			goto IL_00c0;
		}
	}
	{
		goto IL_00f2;
	}

IL_00be:
	{
		return (int32_t)(1);
	}

IL_00c0:
	{
		StringReader_t2229325051 * L_7 = __this->get_json_2();
		NullCheck(L_7);
		VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.IO.StringReader::Read() */, L_7);
		return (int32_t)(2);
	}

IL_00ce:
	{
		return (int32_t)(3);
	}

IL_00d0:
	{
		StringReader_t2229325051 * L_8 = __this->get_json_2();
		NullCheck(L_8);
		VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.IO.StringReader::Read() */, L_8);
		return (int32_t)(4);
	}

IL_00de:
	{
		StringReader_t2229325051 * L_9 = __this->get_json_2();
		NullCheck(L_9);
		VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.IO.StringReader::Read() */, L_9);
		return (int32_t)(6);
	}

IL_00ec:
	{
		return (int32_t)(7);
	}

IL_00ee:
	{
		return (int32_t)(5);
	}

IL_00f0:
	{
		return (int32_t)(8);
	}

IL_00f2:
	{
		String_t* L_10 = Parser_get_NextWord_m2256749100(__this, /*hidden argument*/NULL);
		V_1 = L_10;
		String_t* L_11 = V_1;
		V_3 = L_11;
		String_t* L_12 = V_3;
		if (!L_12)
		{
			goto IL_0174;
		}
	}
	{
		Dictionary_2_t190145395 * L_13 = ((Parser_t2383423553_StaticFields*)Parser_t2383423553_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24map1_3();
		if (L_13)
		{
			goto IL_0141;
		}
	}
	{
		Dictionary_2_t190145395 * L_14 = (Dictionary_2_t190145395 *)il2cpp_codegen_object_new(Dictionary_2_t190145395_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1958628151(L_14, 3, /*hidden argument*/Dictionary_2__ctor_m1958628151_MethodInfo_var);
		V_4 = L_14;
		Dictionary_2_t190145395 * L_15 = V_4;
		NullCheck(L_15);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_15, _stringLiteral97196323, 0);
		Dictionary_2_t190145395 * L_16 = V_4;
		NullCheck(L_16);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_16, _stringLiteral3569038, 1);
		Dictionary_2_t190145395 * L_17 = V_4;
		NullCheck(L_17);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_17, _stringLiteral3392903, 2);
		Dictionary_2_t190145395 * L_18 = V_4;
		((Parser_t2383423553_StaticFields*)Parser_t2383423553_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__switchU24map1_3(L_18);
	}

IL_0141:
	{
		Dictionary_2_t190145395 * L_19 = ((Parser_t2383423553_StaticFields*)Parser_t2383423553_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24map1_3();
		String_t* L_20 = V_3;
		NullCheck(L_19);
		bool L_21 = VirtFuncInvoker2< bool, String_t*, int32_t* >::Invoke(21 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::TryGetValue(!0,!1&) */, L_19, L_20, (&V_5));
		if (!L_21)
		{
			goto IL_0174;
		}
	}
	{
		int32_t L_22 = V_5;
		if (L_22 == 0)
		{
			goto IL_016b;
		}
		if (L_22 == 1)
		{
			goto IL_016e;
		}
		if (L_22 == 2)
		{
			goto IL_0171;
		}
	}
	{
		goto IL_0174;
	}

IL_016b:
	{
		return (int32_t)(((int32_t)10));
	}

IL_016e:
	{
		return (int32_t)(((int32_t)9));
	}

IL_0171:
	{
		return (int32_t)(((int32_t)11));
	}

IL_0174:
	{
		return (int32_t)(0);
	}
}
// System.Void OnePF.JSON/_JSON/Serializer::.ctor()
extern Il2CppClass* StringBuilder_t3822575854_il2cpp_TypeInfo_var;
extern const uint32_t Serializer__ctor_m3624748424_MetadataUsageId;
extern "C"  void Serializer__ctor_m3624748424 (Serializer_t1395478963 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Serializer__ctor_m3624748424_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_0 = (StringBuilder_t3822575854 *)il2cpp_codegen_object_new(StringBuilder_t3822575854_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m135953004(L_0, /*hidden argument*/NULL);
		__this->set_builder_0(L_0);
		return;
	}
}
// System.String OnePF.JSON/_JSON/Serializer::Serialize(OnePF.JSON)
extern Il2CppClass* Serializer_t1395478963_il2cpp_TypeInfo_var;
extern const uint32_t Serializer_Serialize_m1966622095_MetadataUsageId;
extern "C"  String_t* Serializer_Serialize_m1966622095 (Il2CppObject * __this /* static, unused */, JSON_t2649481148 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Serializer_Serialize_m1966622095_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Serializer_t1395478963 * V_0 = NULL;
	{
		Serializer_t1395478963 * L_0 = (Serializer_t1395478963 *)il2cpp_codegen_object_new(Serializer_t1395478963_il2cpp_TypeInfo_var);
		Serializer__ctor_m3624748424(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Serializer_t1395478963 * L_1 = V_0;
		JSON_t2649481148 * L_2 = ___obj0;
		NullCheck(L_1);
		Serializer_SerializeValue_m824966023(L_1, L_2, /*hidden argument*/NULL);
		Serializer_t1395478963 * L_3 = V_0;
		NullCheck(L_3);
		StringBuilder_t3822575854 * L_4 = L_3->get_builder_0();
		NullCheck(L_4);
		String_t* L_5 = StringBuilder_ToString_m350379841(L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void OnePF.JSON/_JSON/Serializer::SerializeValue(System.Object)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern Il2CppClass* JSON_t2649481148_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_t1654916945_il2cpp_TypeInfo_var;
extern Il2CppClass* IList_t1612618265_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t2778706699_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3392903;
extern const uint32_t Serializer_SerializeValue_m824966023_MetadataUsageId;
extern "C"  void Serializer_SerializeValue_m824966023 (Serializer_t1395478963 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Serializer_SerializeValue_m824966023_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___value0;
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		StringBuilder_t3822575854 * L_1 = __this->get_builder_0();
		NullCheck(L_1);
		StringBuilder_Append_m3898090075(L_1, _stringLiteral3392903, /*hidden argument*/NULL);
		goto IL_00d6;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___value0;
		if (!((String_t*)IsInstSealed(L_2, String_t_il2cpp_TypeInfo_var)))
		{
			goto IL_0038;
		}
	}
	{
		Il2CppObject * L_3 = ___value0;
		Serializer_SerializeString_m207856939(__this, ((String_t*)IsInstSealed(L_3, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		goto IL_00d6;
	}

IL_0038:
	{
		Il2CppObject * L_4 = ___value0;
		if (!((Il2CppObject *)IsInstSealed(L_4, Boolean_t211005341_il2cpp_TypeInfo_var)))
		{
			goto IL_005f;
		}
	}
	{
		StringBuilder_t3822575854 * L_5 = __this->get_builder_0();
		Il2CppObject * L_6 = ___value0;
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_6);
		NullCheck(L_7);
		String_t* L_8 = String_ToLower_m2421900555(L_7, /*hidden argument*/NULL);
		NullCheck(L_5);
		StringBuilder_Append_m3898090075(L_5, L_8, /*hidden argument*/NULL);
		goto IL_00d6;
	}

IL_005f:
	{
		Il2CppObject * L_9 = ___value0;
		if (!((JSON_t2649481148 *)IsInstClass(L_9, JSON_t2649481148_il2cpp_TypeInfo_var)))
		{
			goto IL_007b;
		}
	}
	{
		Il2CppObject * L_10 = ___value0;
		Serializer_SerializeObject_m3219164747(__this, ((JSON_t2649481148 *)IsInstClass(L_10, JSON_t2649481148_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		goto IL_00d6;
	}

IL_007b:
	{
		Il2CppObject * L_11 = ___value0;
		if (!((Il2CppObject *)IsInst(L_11, IDictionary_t1654916945_il2cpp_TypeInfo_var)))
		{
			goto IL_0097;
		}
	}
	{
		Il2CppObject * L_12 = ___value0;
		Serializer_SerializeDictionary_m1938765173(__this, ((Il2CppObject *)IsInst(L_12, IDictionary_t1654916945_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		goto IL_00d6;
	}

IL_0097:
	{
		Il2CppObject * L_13 = ___value0;
		if (!((Il2CppObject *)IsInst(L_13, IList_t1612618265_il2cpp_TypeInfo_var)))
		{
			goto IL_00b3;
		}
	}
	{
		Il2CppObject * L_14 = ___value0;
		Serializer_SerializeArray_m3482085126(__this, ((Il2CppObject *)IsInst(L_14, IList_t1612618265_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		goto IL_00d6;
	}

IL_00b3:
	{
		Il2CppObject * L_15 = ___value0;
		if (!((Il2CppObject *)IsInstSealed(L_15, Char_t2778706699_il2cpp_TypeInfo_var)))
		{
			goto IL_00cf;
		}
	}
	{
		Il2CppObject * L_16 = ___value0;
		NullCheck(L_16);
		String_t* L_17 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_16);
		Serializer_SerializeString_m207856939(__this, L_17, /*hidden argument*/NULL);
		goto IL_00d6;
	}

IL_00cf:
	{
		Il2CppObject * L_18 = ___value0;
		Serializer_SerializeOther_m1994865672(__this, L_18, /*hidden argument*/NULL);
	}

IL_00d6:
	{
		return;
	}
}
// System.Void OnePF.JSON/_JSON/Serializer::SerializeObject(OnePF.JSON)
extern "C"  void Serializer_SerializeObject_m3219164747 (Serializer_t1395478963 * __this, JSON_t2649481148 * ___obj0, const MethodInfo* method)
{
	{
		JSON_t2649481148 * L_0 = ___obj0;
		NullCheck(L_0);
		Dictionary_2_t2474804324 * L_1 = L_0->get_fields_0();
		Serializer_SerializeDictionary_m1938765173(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OnePF.JSON/_JSON/Serializer::SerializeDictionary(System.Collections.IDictionary)
extern Il2CppClass* IDictionary_t1654916945_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_t287189635_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Serializer_SerializeDictionary_m1938765173_MetadataUsageId;
extern "C"  void Serializer_SerializeDictionary_m1938765173 (Serializer_t1395478963 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Serializer_SerializeDictionary_m1938765173_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (bool)1;
		StringBuilder_t3822575854 * L_0 = __this->get_builder_0();
		NullCheck(L_0);
		StringBuilder_Append_m2143093878(L_0, ((int32_t)123), /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___obj0;
		NullCheck(L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(4 /* System.Collections.ICollection System.Collections.IDictionary::get_Keys() */, IDictionary_t1654916945_il2cpp_TypeInfo_var, L_1);
		NullCheck(L_2);
		Il2CppObject * L_3 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t287189635_il2cpp_TypeInfo_var, L_2);
		V_2 = L_3;
	}

IL_001c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0065;
		}

IL_0021:
		{
			Il2CppObject * L_4 = V_2;
			NullCheck(L_4);
			Il2CppObject * L_5 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_4);
			V_1 = L_5;
			bool L_6 = V_0;
			if (L_6)
			{
				goto IL_003c;
			}
		}

IL_002e:
		{
			StringBuilder_t3822575854 * L_7 = __this->get_builder_0();
			NullCheck(L_7);
			StringBuilder_Append_m2143093878(L_7, ((int32_t)44), /*hidden argument*/NULL);
		}

IL_003c:
		{
			Il2CppObject * L_8 = V_1;
			NullCheck(L_8);
			String_t* L_9 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_8);
			Serializer_SerializeString_m207856939(__this, L_9, /*hidden argument*/NULL);
			StringBuilder_t3822575854 * L_10 = __this->get_builder_0();
			NullCheck(L_10);
			StringBuilder_Append_m2143093878(L_10, ((int32_t)58), /*hidden argument*/NULL);
			Il2CppObject * L_11 = ___obj0;
			Il2CppObject * L_12 = V_1;
			NullCheck(L_11);
			Il2CppObject * L_13 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t1654916945_il2cpp_TypeInfo_var, L_11, L_12);
			Serializer_SerializeValue_m824966023(__this, L_13, /*hidden argument*/NULL);
			V_0 = (bool)0;
		}

IL_0065:
		{
			Il2CppObject * L_14 = V_2;
			NullCheck(L_14);
			bool L_15 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_14);
			if (L_15)
			{
				goto IL_0021;
			}
		}

IL_0070:
		{
			IL2CPP_LEAVE(0x87, FINALLY_0075);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0075;
	}

FINALLY_0075:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_16 = V_2;
			V_3 = ((Il2CppObject *)IsInst(L_16, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			Il2CppObject * L_17 = V_3;
			if (L_17)
			{
				goto IL_0080;
			}
		}

IL_007f:
		{
			IL2CPP_END_FINALLY(117)
		}

IL_0080:
		{
			Il2CppObject * L_18 = V_3;
			NullCheck(L_18);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_18);
			IL2CPP_END_FINALLY(117)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(117)
	{
		IL2CPP_JUMP_TBL(0x87, IL_0087)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0087:
	{
		StringBuilder_t3822575854 * L_19 = __this->get_builder_0();
		NullCheck(L_19);
		StringBuilder_Append_m2143093878(L_19, ((int32_t)125), /*hidden argument*/NULL);
		return;
	}
}
// System.Void OnePF.JSON/_JSON/Serializer::SerializeArray(System.Collections.IList)
extern Il2CppClass* IEnumerable_t287189635_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Serializer_SerializeArray_m3482085126_MetadataUsageId;
extern "C"  void Serializer_SerializeArray_m3482085126 (Serializer_t1395478963 * __this, Il2CppObject * ___anArray0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Serializer_SerializeArray_m3482085126_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		StringBuilder_t3822575854 * L_0 = __this->get_builder_0();
		NullCheck(L_0);
		StringBuilder_Append_m2143093878(L_0, ((int32_t)91), /*hidden argument*/NULL);
		V_0 = (bool)1;
		Il2CppObject * L_1 = ___anArray0;
		NullCheck(L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t287189635_il2cpp_TypeInfo_var, L_1);
		V_2 = L_2;
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0040;
		}

IL_001c:
		{
			Il2CppObject * L_3 = V_2;
			NullCheck(L_3);
			Il2CppObject * L_4 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_3);
			V_1 = L_4;
			bool L_5 = V_0;
			if (L_5)
			{
				goto IL_0037;
			}
		}

IL_0029:
		{
			StringBuilder_t3822575854 * L_6 = __this->get_builder_0();
			NullCheck(L_6);
			StringBuilder_Append_m2143093878(L_6, ((int32_t)44), /*hidden argument*/NULL);
		}

IL_0037:
		{
			Il2CppObject * L_7 = V_1;
			Serializer_SerializeValue_m824966023(__this, L_7, /*hidden argument*/NULL);
			V_0 = (bool)0;
		}

IL_0040:
		{
			Il2CppObject * L_8 = V_2;
			NullCheck(L_8);
			bool L_9 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_8);
			if (L_9)
			{
				goto IL_001c;
			}
		}

IL_004b:
		{
			IL2CPP_LEAVE(0x62, FINALLY_0050);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0050;
	}

FINALLY_0050:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_10 = V_2;
			V_3 = ((Il2CppObject *)IsInst(L_10, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			Il2CppObject * L_11 = V_3;
			if (L_11)
			{
				goto IL_005b;
			}
		}

IL_005a:
		{
			IL2CPP_END_FINALLY(80)
		}

IL_005b:
		{
			Il2CppObject * L_12 = V_3;
			NullCheck(L_12);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_12);
			IL2CPP_END_FINALLY(80)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(80)
	{
		IL2CPP_JUMP_TBL(0x62, IL_0062)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0062:
	{
		StringBuilder_t3822575854 * L_13 = __this->get_builder_0();
		NullCheck(L_13);
		StringBuilder_Append_m2143093878(L_13, ((int32_t)93), /*hidden argument*/NULL);
		return;
	}
}
// System.Void OnePF.JSON/_JSON/Serializer::SerializeString(System.String)
extern Il2CppClass* Convert_t1097883944_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2886;
extern Il2CppCodeGenString* _stringLiteral2944;
extern Il2CppCodeGenString* _stringLiteral2950;
extern Il2CppCodeGenString* _stringLiteral2954;
extern Il2CppCodeGenString* _stringLiteral2962;
extern Il2CppCodeGenString* _stringLiteral2966;
extern Il2CppCodeGenString* _stringLiteral2968;
extern Il2CppCodeGenString* _stringLiteral2969;
extern const uint32_t Serializer_SerializeString_m207856939_MetadataUsageId;
extern "C"  void Serializer_SerializeString_m207856939 (Serializer_t1395478963 * __this, String_t* ___str0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Serializer_SerializeString_m207856939_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	CharU5BU5D_t3416858730* V_0 = NULL;
	uint16_t V_1 = 0x0;
	CharU5BU5D_t3416858730* V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	uint16_t V_5 = 0x0;
	{
		StringBuilder_t3822575854 * L_0 = __this->get_builder_0();
		NullCheck(L_0);
		StringBuilder_Append_m2143093878(L_0, ((int32_t)34), /*hidden argument*/NULL);
		String_t* L_1 = ___str0;
		NullCheck(L_1);
		CharU5BU5D_t3416858730* L_2 = String_ToCharArray_m1208288742(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		CharU5BU5D_t3416858730* L_3 = V_0;
		V_2 = L_3;
		V_3 = 0;
		goto IL_0153;
	}

IL_001e:
	{
		CharU5BU5D_t3416858730* L_4 = V_2;
		int32_t L_5 = V_3;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		V_1 = ((L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6)));
		uint16_t L_7 = V_1;
		V_5 = L_7;
		uint16_t L_8 = V_5;
		if (((int32_t)((int32_t)L_8-(int32_t)8)) == 0)
		{
			goto IL_0089;
		}
		if (((int32_t)((int32_t)L_8-(int32_t)8)) == 1)
		{
			goto IL_00e1;
		}
		if (((int32_t)((int32_t)L_8-(int32_t)8)) == 2)
		{
			goto IL_00b5;
		}
		if (((int32_t)((int32_t)L_8-(int32_t)8)) == 3)
		{
			goto IL_0046;
		}
		if (((int32_t)((int32_t)L_8-(int32_t)8)) == 4)
		{
			goto IL_009f;
		}
		if (((int32_t)((int32_t)L_8-(int32_t)8)) == 5)
		{
			goto IL_00cb;
		}
	}

IL_0046:
	{
		uint16_t L_9 = V_5;
		if ((((int32_t)L_9) == ((int32_t)((int32_t)34))))
		{
			goto IL_005d;
		}
	}
	{
		uint16_t L_10 = V_5;
		if ((((int32_t)L_10) == ((int32_t)((int32_t)92))))
		{
			goto IL_0073;
		}
	}
	{
		goto IL_00f7;
	}

IL_005d:
	{
		StringBuilder_t3822575854 * L_11 = __this->get_builder_0();
		NullCheck(L_11);
		StringBuilder_Append_m3898090075(L_11, _stringLiteral2886, /*hidden argument*/NULL);
		goto IL_014f;
	}

IL_0073:
	{
		StringBuilder_t3822575854 * L_12 = __this->get_builder_0();
		NullCheck(L_12);
		StringBuilder_Append_m3898090075(L_12, _stringLiteral2944, /*hidden argument*/NULL);
		goto IL_014f;
	}

IL_0089:
	{
		StringBuilder_t3822575854 * L_13 = __this->get_builder_0();
		NullCheck(L_13);
		StringBuilder_Append_m3898090075(L_13, _stringLiteral2950, /*hidden argument*/NULL);
		goto IL_014f;
	}

IL_009f:
	{
		StringBuilder_t3822575854 * L_14 = __this->get_builder_0();
		NullCheck(L_14);
		StringBuilder_Append_m3898090075(L_14, _stringLiteral2954, /*hidden argument*/NULL);
		goto IL_014f;
	}

IL_00b5:
	{
		StringBuilder_t3822575854 * L_15 = __this->get_builder_0();
		NullCheck(L_15);
		StringBuilder_Append_m3898090075(L_15, _stringLiteral2962, /*hidden argument*/NULL);
		goto IL_014f;
	}

IL_00cb:
	{
		StringBuilder_t3822575854 * L_16 = __this->get_builder_0();
		NullCheck(L_16);
		StringBuilder_Append_m3898090075(L_16, _stringLiteral2966, /*hidden argument*/NULL);
		goto IL_014f;
	}

IL_00e1:
	{
		StringBuilder_t3822575854 * L_17 = __this->get_builder_0();
		NullCheck(L_17);
		StringBuilder_Append_m3898090075(L_17, _stringLiteral2968, /*hidden argument*/NULL);
		goto IL_014f;
	}

IL_00f7:
	{
		uint16_t L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		int32_t L_19 = Convert_ToInt32_m100832938(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		V_4 = L_19;
		int32_t L_20 = V_4;
		if ((((int32_t)L_20) < ((int32_t)((int32_t)32))))
		{
			goto IL_0123;
		}
	}
	{
		int32_t L_21 = V_4;
		if ((((int32_t)L_21) > ((int32_t)((int32_t)126))))
		{
			goto IL_0123;
		}
	}
	{
		StringBuilder_t3822575854 * L_22 = __this->get_builder_0();
		uint16_t L_23 = V_1;
		NullCheck(L_22);
		StringBuilder_Append_m2143093878(L_22, L_23, /*hidden argument*/NULL);
		goto IL_014a;
	}

IL_0123:
	{
		StringBuilder_t3822575854 * L_24 = __this->get_builder_0();
		int32_t L_25 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		String_t* L_26 = Convert_ToString_m3908657329(NULL /*static, unused*/, L_25, ((int32_t)16), /*hidden argument*/NULL);
		NullCheck(L_26);
		String_t* L_27 = String_PadLeft_m3268206439(L_26, 4, ((int32_t)48), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_28 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral2969, L_27, /*hidden argument*/NULL);
		NullCheck(L_24);
		StringBuilder_Append_m3898090075(L_24, L_28, /*hidden argument*/NULL);
	}

IL_014a:
	{
		goto IL_014f;
	}

IL_014f:
	{
		int32_t L_29 = V_3;
		V_3 = ((int32_t)((int32_t)L_29+(int32_t)1));
	}

IL_0153:
	{
		int32_t L_30 = V_3;
		CharU5BU5D_t3416858730* L_31 = V_2;
		NullCheck(L_31);
		if ((((int32_t)L_30) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_31)->max_length)))))))
		{
			goto IL_001e;
		}
	}
	{
		StringBuilder_t3822575854 * L_32 = __this->get_builder_0();
		NullCheck(L_32);
		StringBuilder_Append_m2143093878(L_32, ((int32_t)34), /*hidden argument*/NULL);
		return;
	}
}
// System.Void OnePF.JSON/_JSON/Serializer::SerializeOther(System.Object)
extern Il2CppClass* Single_t958209021_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt32_t985925326_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t2847414882_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern Il2CppClass* SByte_t2855346064_il2cpp_TypeInfo_var;
extern Il2CppClass* Byte_t2778693821_il2cpp_TypeInfo_var;
extern Il2CppClass* Int16_t2847414729_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt16_t985925268_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt64_t985925421_il2cpp_TypeInfo_var;
extern Il2CppClass* Decimal_t1688557254_il2cpp_TypeInfo_var;
extern const uint32_t Serializer_SerializeOther_m1994865672_MetadataUsageId;
extern "C"  void Serializer_SerializeOther_m1994865672 (Serializer_t1395478963 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Serializer_SerializeOther_m1994865672_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___value0;
		if (((Il2CppObject *)IsInstSealed(L_0, Single_t958209021_il2cpp_TypeInfo_var)))
		{
			goto IL_0079;
		}
	}
	{
		Il2CppObject * L_1 = ___value0;
		if (((Il2CppObject *)IsInstSealed(L_1, Int32_t2847414787_il2cpp_TypeInfo_var)))
		{
			goto IL_0079;
		}
	}
	{
		Il2CppObject * L_2 = ___value0;
		if (((Il2CppObject *)IsInstSealed(L_2, UInt32_t985925326_il2cpp_TypeInfo_var)))
		{
			goto IL_0079;
		}
	}
	{
		Il2CppObject * L_3 = ___value0;
		if (((Il2CppObject *)IsInstSealed(L_3, Int64_t2847414882_il2cpp_TypeInfo_var)))
		{
			goto IL_0079;
		}
	}
	{
		Il2CppObject * L_4 = ___value0;
		if (((Il2CppObject *)IsInstSealed(L_4, Double_t534516614_il2cpp_TypeInfo_var)))
		{
			goto IL_0079;
		}
	}
	{
		Il2CppObject * L_5 = ___value0;
		if (((Il2CppObject *)IsInstSealed(L_5, SByte_t2855346064_il2cpp_TypeInfo_var)))
		{
			goto IL_0079;
		}
	}
	{
		Il2CppObject * L_6 = ___value0;
		if (((Il2CppObject *)IsInstSealed(L_6, Byte_t2778693821_il2cpp_TypeInfo_var)))
		{
			goto IL_0079;
		}
	}
	{
		Il2CppObject * L_7 = ___value0;
		if (((Il2CppObject *)IsInstSealed(L_7, Int16_t2847414729_il2cpp_TypeInfo_var)))
		{
			goto IL_0079;
		}
	}
	{
		Il2CppObject * L_8 = ___value0;
		if (((Il2CppObject *)IsInstSealed(L_8, UInt16_t985925268_il2cpp_TypeInfo_var)))
		{
			goto IL_0079;
		}
	}
	{
		Il2CppObject * L_9 = ___value0;
		if (((Il2CppObject *)IsInstSealed(L_9, UInt64_t985925421_il2cpp_TypeInfo_var)))
		{
			goto IL_0079;
		}
	}
	{
		Il2CppObject * L_10 = ___value0;
		if (!((Il2CppObject *)IsInstSealed(L_10, Decimal_t1688557254_il2cpp_TypeInfo_var)))
		{
			goto IL_0090;
		}
	}

IL_0079:
	{
		StringBuilder_t3822575854 * L_11 = __this->get_builder_0();
		Il2CppObject * L_12 = ___value0;
		NullCheck(L_12);
		String_t* L_13 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_12);
		NullCheck(L_11);
		StringBuilder_Append_m3898090075(L_11, L_13, /*hidden argument*/NULL);
		goto IL_009c;
	}

IL_0090:
	{
		Il2CppObject * L_14 = ___value0;
		NullCheck(L_14);
		String_t* L_15 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_14);
		Serializer_SerializeString_m207856939(__this, L_15, /*hidden argument*/NULL);
	}

IL_009c:
	{
		return;
	}
}
// System.Void OnePF.OpenIAB::.ctor()
extern "C"  void OpenIAB__ctor_m575505577 (OpenIAB_t3048630868 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OnePF.OpenIAB::.cctor()
extern Il2CppClass* OpenIAB_iOS_t446655906_il2cpp_TypeInfo_var;
extern Il2CppClass* OpenIAB_t3048630868_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4251022918;
extern const uint32_t OpenIAB__cctor_m178707492_MetadataUsageId;
extern "C"  void OpenIAB__cctor_m178707492 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIAB__cctor_m178707492_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		OpenIAB_iOS_t446655906 * L_0 = (OpenIAB_iOS_t446655906 *)il2cpp_codegen_object_new(OpenIAB_iOS_t446655906_il2cpp_TypeInfo_var);
		OpenIAB_iOS__ctor_m2472941083(L_0, /*hidden argument*/NULL);
		((OpenIAB_t3048630868_StaticFields*)OpenIAB_t3048630868_il2cpp_TypeInfo_var->static_fields)->set__billing_0(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m3349710197(NULL /*static, unused*/, _stringLiteral4251022918, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GameObject OnePF.OpenIAB::get_EventManager()
extern const Il2CppType* OpenIABEventManager_t528054099_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t OpenIAB_get_EventManager_m4227360122_MetadataUsageId;
extern "C"  GameObject_t4012695102 * OpenIAB_get_EventManager_m4227360122 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIAB_get_EventManager_m4227360122_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(OpenIABEventManager_t528054099_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, L_0);
		GameObject_t4012695102 * L_2 = GameObject_Find_m1853568733(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void OnePF.OpenIAB::mapSku(System.String,System.String,System.String)
extern Il2CppClass* OpenIAB_t3048630868_il2cpp_TypeInfo_var;
extern Il2CppClass* IOpenIAB_t1424766443_il2cpp_TypeInfo_var;
extern const uint32_t OpenIAB_mapSku_m368492606_MetadataUsageId;
extern "C"  void OpenIAB_mapSku_m368492606 (Il2CppObject * __this /* static, unused */, String_t* ___sku0, String_t* ___storeName1, String_t* ___storeSku2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIAB_mapSku_m368492606_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OpenIAB_t3048630868_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = ((OpenIAB_t3048630868_StaticFields*)OpenIAB_t3048630868_il2cpp_TypeInfo_var->static_fields)->get__billing_0();
		String_t* L_1 = ___sku0;
		String_t* L_2 = ___storeName1;
		String_t* L_3 = ___storeSku2;
		NullCheck(L_0);
		InterfaceActionInvoker3< String_t*, String_t*, String_t* >::Invoke(1 /* System.Void OnePF.IOpenIAB::mapSku(System.String,System.String,System.String) */, IOpenIAB_t1424766443_il2cpp_TypeInfo_var, L_0, L_1, L_2, L_3);
		return;
	}
}
// System.Void OnePF.OpenIAB::init(OnePF.Options)
extern Il2CppClass* OpenIAB_t3048630868_il2cpp_TypeInfo_var;
extern Il2CppClass* IOpenIAB_t1424766443_il2cpp_TypeInfo_var;
extern const uint32_t OpenIAB_init_m2625521499_MetadataUsageId;
extern "C"  void OpenIAB_init_m2625521499 (Il2CppObject * __this /* static, unused */, Options_t3062372690 * ___options0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIAB_init_m2625521499_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OpenIAB_t3048630868_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = ((OpenIAB_t3048630868_StaticFields*)OpenIAB_t3048630868_il2cpp_TypeInfo_var->static_fields)->get__billing_0();
		Options_t3062372690 * L_1 = ___options0;
		NullCheck(L_0);
		InterfaceActionInvoker1< Options_t3062372690 * >::Invoke(0 /* System.Void OnePF.IOpenIAB::init(OnePF.Options) */, IOpenIAB_t1424766443_il2cpp_TypeInfo_var, L_0, L_1);
		return;
	}
}
// System.Void OnePF.OpenIAB::unbindService()
extern Il2CppClass* OpenIAB_t3048630868_il2cpp_TypeInfo_var;
extern Il2CppClass* IOpenIAB_t1424766443_il2cpp_TypeInfo_var;
extern const uint32_t OpenIAB_unbindService_m4148534182_MetadataUsageId;
extern "C"  void OpenIAB_unbindService_m4148534182 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIAB_unbindService_m4148534182_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OpenIAB_t3048630868_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = ((OpenIAB_t3048630868_StaticFields*)OpenIAB_t3048630868_il2cpp_TypeInfo_var->static_fields)->get__billing_0();
		NullCheck(L_0);
		InterfaceActionInvoker0::Invoke(2 /* System.Void OnePF.IOpenIAB::unbindService() */, IOpenIAB_t1424766443_il2cpp_TypeInfo_var, L_0);
		return;
	}
}
// System.Boolean OnePF.OpenIAB::areSubscriptionsSupported()
extern Il2CppClass* OpenIAB_t3048630868_il2cpp_TypeInfo_var;
extern Il2CppClass* IOpenIAB_t1424766443_il2cpp_TypeInfo_var;
extern const uint32_t OpenIAB_areSubscriptionsSupported_m953657423_MetadataUsageId;
extern "C"  bool OpenIAB_areSubscriptionsSupported_m953657423 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIAB_areSubscriptionsSupported_m953657423_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OpenIAB_t3048630868_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = ((OpenIAB_t3048630868_StaticFields*)OpenIAB_t3048630868_il2cpp_TypeInfo_var->static_fields)->get__billing_0();
		NullCheck(L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(3 /* System.Boolean OnePF.IOpenIAB::areSubscriptionsSupported() */, IOpenIAB_t1424766443_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Void OnePF.OpenIAB::queryInventory()
extern Il2CppClass* OpenIAB_t3048630868_il2cpp_TypeInfo_var;
extern Il2CppClass* IOpenIAB_t1424766443_il2cpp_TypeInfo_var;
extern const uint32_t OpenIAB_queryInventory_m1258082127_MetadataUsageId;
extern "C"  void OpenIAB_queryInventory_m1258082127 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIAB_queryInventory_m1258082127_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OpenIAB_t3048630868_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = ((OpenIAB_t3048630868_StaticFields*)OpenIAB_t3048630868_il2cpp_TypeInfo_var->static_fields)->get__billing_0();
		NullCheck(L_0);
		InterfaceActionInvoker0::Invoke(4 /* System.Void OnePF.IOpenIAB::queryInventory() */, IOpenIAB_t1424766443_il2cpp_TypeInfo_var, L_0);
		return;
	}
}
// System.Void OnePF.OpenIAB::queryInventory(System.String[])
extern Il2CppClass* OpenIAB_t3048630868_il2cpp_TypeInfo_var;
extern Il2CppClass* IOpenIAB_t1424766443_il2cpp_TypeInfo_var;
extern const uint32_t OpenIAB_queryInventory_m811955825_MetadataUsageId;
extern "C"  void OpenIAB_queryInventory_m811955825 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t2956870243* ___skus0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIAB_queryInventory_m811955825_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OpenIAB_t3048630868_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = ((OpenIAB_t3048630868_StaticFields*)OpenIAB_t3048630868_il2cpp_TypeInfo_var->static_fields)->get__billing_0();
		StringU5BU5D_t2956870243* L_1 = ___skus0;
		NullCheck(L_0);
		InterfaceActionInvoker1< StringU5BU5D_t2956870243* >::Invoke(5 /* System.Void OnePF.IOpenIAB::queryInventory(System.String[]) */, IOpenIAB_t1424766443_il2cpp_TypeInfo_var, L_0, L_1);
		return;
	}
}
// System.Void OnePF.OpenIAB::purchaseProduct(System.String,System.String)
extern Il2CppClass* OpenIAB_t3048630868_il2cpp_TypeInfo_var;
extern Il2CppClass* IOpenIAB_t1424766443_il2cpp_TypeInfo_var;
extern const uint32_t OpenIAB_purchaseProduct_m501900041_MetadataUsageId;
extern "C"  void OpenIAB_purchaseProduct_m501900041 (Il2CppObject * __this /* static, unused */, String_t* ___sku0, String_t* ___developerPayload1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIAB_purchaseProduct_m501900041_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OpenIAB_t3048630868_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = ((OpenIAB_t3048630868_StaticFields*)OpenIAB_t3048630868_il2cpp_TypeInfo_var->static_fields)->get__billing_0();
		String_t* L_1 = ___sku0;
		String_t* L_2 = ___developerPayload1;
		NullCheck(L_0);
		InterfaceActionInvoker2< String_t*, String_t* >::Invoke(6 /* System.Void OnePF.IOpenIAB::purchaseProduct(System.String,System.String) */, IOpenIAB_t1424766443_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		return;
	}
}
// System.Void OnePF.OpenIAB::purchaseSubscription(System.String,System.String)
extern Il2CppClass* OpenIAB_t3048630868_il2cpp_TypeInfo_var;
extern Il2CppClass* IOpenIAB_t1424766443_il2cpp_TypeInfo_var;
extern const uint32_t OpenIAB_purchaseSubscription_m756822917_MetadataUsageId;
extern "C"  void OpenIAB_purchaseSubscription_m756822917 (Il2CppObject * __this /* static, unused */, String_t* ___sku0, String_t* ___developerPayload1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIAB_purchaseSubscription_m756822917_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OpenIAB_t3048630868_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = ((OpenIAB_t3048630868_StaticFields*)OpenIAB_t3048630868_il2cpp_TypeInfo_var->static_fields)->get__billing_0();
		String_t* L_1 = ___sku0;
		String_t* L_2 = ___developerPayload1;
		NullCheck(L_0);
		InterfaceActionInvoker2< String_t*, String_t* >::Invoke(7 /* System.Void OnePF.IOpenIAB::purchaseSubscription(System.String,System.String) */, IOpenIAB_t1424766443_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		return;
	}
}
// System.Void OnePF.OpenIAB::consumeProduct(OnePF.Purchase)
extern Il2CppClass* OpenIAB_t3048630868_il2cpp_TypeInfo_var;
extern Il2CppClass* IOpenIAB_t1424766443_il2cpp_TypeInfo_var;
extern const uint32_t OpenIAB_consumeProduct_m2513437947_MetadataUsageId;
extern "C"  void OpenIAB_consumeProduct_m2513437947 (Il2CppObject * __this /* static, unused */, Purchase_t160195573 * ___purchase0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIAB_consumeProduct_m2513437947_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OpenIAB_t3048630868_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = ((OpenIAB_t3048630868_StaticFields*)OpenIAB_t3048630868_il2cpp_TypeInfo_var->static_fields)->get__billing_0();
		Purchase_t160195573 * L_1 = ___purchase0;
		NullCheck(L_0);
		InterfaceActionInvoker1< Purchase_t160195573 * >::Invoke(8 /* System.Void OnePF.IOpenIAB::consumeProduct(OnePF.Purchase) */, IOpenIAB_t1424766443_il2cpp_TypeInfo_var, L_0, L_1);
		return;
	}
}
// System.Void OnePF.OpenIAB::restoreTransactions()
extern Il2CppClass* OpenIAB_t3048630868_il2cpp_TypeInfo_var;
extern Il2CppClass* IOpenIAB_t1424766443_il2cpp_TypeInfo_var;
extern const uint32_t OpenIAB_restoreTransactions_m2405457322_MetadataUsageId;
extern "C"  void OpenIAB_restoreTransactions_m2405457322 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIAB_restoreTransactions_m2405457322_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OpenIAB_t3048630868_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = ((OpenIAB_t3048630868_StaticFields*)OpenIAB_t3048630868_il2cpp_TypeInfo_var->static_fields)->get__billing_0();
		NullCheck(L_0);
		InterfaceActionInvoker0::Invoke(9 /* System.Void OnePF.IOpenIAB::restoreTransactions() */, IOpenIAB_t1424766443_il2cpp_TypeInfo_var, L_0);
		return;
	}
}
// System.Boolean OnePF.OpenIAB::isDebugLog()
extern Il2CppClass* OpenIAB_t3048630868_il2cpp_TypeInfo_var;
extern Il2CppClass* IOpenIAB_t1424766443_il2cpp_TypeInfo_var;
extern const uint32_t OpenIAB_isDebugLog_m2994064698_MetadataUsageId;
extern "C"  bool OpenIAB_isDebugLog_m2994064698 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIAB_isDebugLog_m2994064698_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OpenIAB_t3048630868_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = ((OpenIAB_t3048630868_StaticFields*)OpenIAB_t3048630868_il2cpp_TypeInfo_var->static_fields)->get__billing_0();
		NullCheck(L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(10 /* System.Boolean OnePF.IOpenIAB::isDebugLog() */, IOpenIAB_t1424766443_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Void OnePF.OpenIAB::enableDebugLogging(System.Boolean)
extern Il2CppClass* OpenIAB_t3048630868_il2cpp_TypeInfo_var;
extern Il2CppClass* IOpenIAB_t1424766443_il2cpp_TypeInfo_var;
extern const uint32_t OpenIAB_enableDebugLogging_m3667255489_MetadataUsageId;
extern "C"  void OpenIAB_enableDebugLogging_m3667255489 (Il2CppObject * __this /* static, unused */, bool ___enabled0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIAB_enableDebugLogging_m3667255489_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OpenIAB_t3048630868_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = ((OpenIAB_t3048630868_StaticFields*)OpenIAB_t3048630868_il2cpp_TypeInfo_var->static_fields)->get__billing_0();
		bool L_1 = ___enabled0;
		NullCheck(L_0);
		InterfaceActionInvoker1< bool >::Invoke(11 /* System.Void OnePF.IOpenIAB::enableDebugLogging(System.Boolean) */, IOpenIAB_t1424766443_il2cpp_TypeInfo_var, L_0, L_1);
		return;
	}
}
// System.Void OnePF.OpenIAB::enableDebugLogging(System.Boolean,System.String)
extern Il2CppClass* OpenIAB_t3048630868_il2cpp_TypeInfo_var;
extern Il2CppClass* IOpenIAB_t1424766443_il2cpp_TypeInfo_var;
extern const uint32_t OpenIAB_enableDebugLogging_m4256310077_MetadataUsageId;
extern "C"  void OpenIAB_enableDebugLogging_m4256310077 (Il2CppObject * __this /* static, unused */, bool ___enabled0, String_t* ___tag1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIAB_enableDebugLogging_m4256310077_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OpenIAB_t3048630868_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = ((OpenIAB_t3048630868_StaticFields*)OpenIAB_t3048630868_il2cpp_TypeInfo_var->static_fields)->get__billing_0();
		bool L_1 = ___enabled0;
		String_t* L_2 = ___tag1;
		NullCheck(L_0);
		InterfaceActionInvoker2< bool, String_t* >::Invoke(12 /* System.Void OnePF.IOpenIAB::enableDebugLogging(System.Boolean,System.String) */, IOpenIAB_t1424766443_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		return;
	}
}
// System.Void OnePF.OpenIAB_Android::.ctor()
extern "C"  void OpenIAB_Android__ctor_m2526296985 (OpenIAB_Android_t58679140 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OnePF.OpenIAB_Android::.cctor()
extern Il2CppClass* OpenIAB_Android_t58679140_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1493181303;
extern Il2CppCodeGenString* _stringLiteral1319160642;
extern Il2CppCodeGenString* _stringLiteral701801052;
extern Il2CppCodeGenString* _stringLiteral1578648644;
extern Il2CppCodeGenString* _stringLiteral1995543855;
extern const uint32_t OpenIAB_Android__cctor_m523698996_MetadataUsageId;
extern "C"  void OpenIAB_Android__cctor_m523698996 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIAB_Android__cctor_m523698996_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((OpenIAB_Android_t58679140_StaticFields*)OpenIAB_Android_t58679140_il2cpp_TypeInfo_var->static_fields)->set_STORE_GOOGLE_0(_stringLiteral1493181303);
		((OpenIAB_Android_t58679140_StaticFields*)OpenIAB_Android_t58679140_il2cpp_TypeInfo_var->static_fields)->set_STORE_AMAZON_1(_stringLiteral1319160642);
		((OpenIAB_Android_t58679140_StaticFields*)OpenIAB_Android_t58679140_il2cpp_TypeInfo_var->static_fields)->set_STORE_SAMSUNG_2(_stringLiteral701801052);
		((OpenIAB_Android_t58679140_StaticFields*)OpenIAB_Android_t58679140_il2cpp_TypeInfo_var->static_fields)->set_STORE_NOKIA_3(_stringLiteral1578648644);
		((OpenIAB_Android_t58679140_StaticFields*)OpenIAB_Android_t58679140_il2cpp_TypeInfo_var->static_fields)->set_STORE_YANDEX_6(_stringLiteral1995543855);
		return;
	}
}
// System.Void OnePF.OpenIAB_iOS::.ctor()
extern "C"  void OpenIAB_iOS__ctor_m2472941083 (OpenIAB_iOS_t446655906 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OnePF.OpenIAB_iOS::.cctor()
extern Il2CppClass* OpenIAB_iOS_t446655906_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t2606186806_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m640701813_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1186311008;
extern const uint32_t OpenIAB_iOS__cctor_m3164633330_MetadataUsageId;
extern "C"  void OpenIAB_iOS__cctor_m3164633330 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIAB_iOS__cctor_m3164633330_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((OpenIAB_iOS_t446655906_StaticFields*)OpenIAB_iOS_t446655906_il2cpp_TypeInfo_var->static_fields)->set_STORE_0(_stringLiteral1186311008);
		Dictionary_2_t2606186806 * L_0 = (Dictionary_2_t2606186806 *)il2cpp_codegen_object_new(Dictionary_2_t2606186806_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m640701813(L_0, /*hidden argument*/Dictionary_2__ctor_m640701813_MethodInfo_var);
		((OpenIAB_iOS_t446655906_StaticFields*)OpenIAB_iOS_t446655906_il2cpp_TypeInfo_var->static_fields)->set__sku2storeSkuMappings_1(L_0);
		Dictionary_2_t2606186806 * L_1 = (Dictionary_2_t2606186806 *)il2cpp_codegen_object_new(Dictionary_2_t2606186806_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m640701813(L_1, /*hidden argument*/Dictionary_2__ctor_m640701813_MethodInfo_var);
		((OpenIAB_iOS_t446655906_StaticFields*)OpenIAB_iOS_t446655906_il2cpp_TypeInfo_var->static_fields)->set__storeSku2skuMappings_2(L_1);
		return;
	}
}
// System.Void OnePF.OpenIAB_iOS::AppStore_requestProducts(System.String[],System.Int32)
extern "C" void DEFAULT_CALL AppStore_requestProducts(char**, int32_t);
extern "C"  void OpenIAB_iOS_AppStore_requestProducts_m1401556628 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t2956870243* ___skus0, int32_t ___skusNumber1, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char**, int32_t);

	// Marshaling of parameter '___skus0' to native representation
	char** ____skus0_marshaled = NULL;
	____skus0_marshaled = il2cpp_codegen_marshal_string_array(___skus0);

	// Marshaling of parameter '___skusNumber1' to native representation

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(AppStore_requestProducts)(____skus0_marshaled, ___skusNumber1);

	// Marshaling cleanup of parameter '___skus0' native representation
	if (___skus0 != NULL) il2cpp_codegen_marshal_free_string_array((void**)____skus0_marshaled, ((Il2CppCodeGenArray*)___skus0)->max_length);
	____skus0_marshaled = NULL;

	// Marshaling cleanup of parameter '___skusNumber1' native representation

}
// System.Void OnePF.OpenIAB_iOS::AppStore_startPurchase(System.String)
extern "C" void DEFAULT_CALL AppStore_startPurchase(char*);
extern "C"  void OpenIAB_iOS_AppStore_startPurchase_m3350896277 (Il2CppObject * __this /* static, unused */, String_t* ___sku0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___sku0' to native representation
	char* ____sku0_marshaled = NULL;
	____sku0_marshaled = il2cpp_codegen_marshal_string(___sku0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(AppStore_startPurchase)(____sku0_marshaled);

	// Marshaling cleanup of parameter '___sku0' native representation
	il2cpp_codegen_marshal_free(____sku0_marshaled);
	____sku0_marshaled = NULL;

}
// System.Void OnePF.OpenIAB_iOS::AppStore_restorePurchases()
extern "C" void DEFAULT_CALL AppStore_restorePurchases();
extern "C"  void OpenIAB_iOS_AppStore_restorePurchases_m1704837692 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(AppStore_restorePurchases)();

}
// System.Boolean OnePF.OpenIAB_iOS::Inventory_hasPurchase(System.String)
extern "C" int32_t DEFAULT_CALL Inventory_hasPurchase(char*);
extern "C"  bool OpenIAB_iOS_Inventory_hasPurchase_m3366375669 (Il2CppObject * __this /* static, unused */, String_t* ___sku0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___sku0' to native representation
	char* ____sku0_marshaled = NULL;
	____sku0_marshaled = il2cpp_codegen_marshal_string(___sku0);

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = reinterpret_cast<PInvokeFunc>(Inventory_hasPurchase)(____sku0_marshaled);

	// Marshaling cleanup of parameter '___sku0' native representation
	il2cpp_codegen_marshal_free(____sku0_marshaled);
	____sku0_marshaled = NULL;

	return _return_value;
}
// System.Void OnePF.OpenIAB_iOS::Inventory_query()
extern "C" void DEFAULT_CALL Inventory_query();
extern "C"  void OpenIAB_iOS_Inventory_query_m1367326750 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Inventory_query)();

}
// System.Void OnePF.OpenIAB_iOS::Inventory_removePurchase(System.String)
extern "C" void DEFAULT_CALL Inventory_removePurchase(char*);
extern "C"  void OpenIAB_iOS_Inventory_removePurchase_m653450737 (Il2CppObject * __this /* static, unused */, String_t* ___sku0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___sku0' to native representation
	char* ____sku0_marshaled = NULL;
	____sku0_marshaled = il2cpp_codegen_marshal_string(___sku0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Inventory_removePurchase)(____sku0_marshaled);

	// Marshaling cleanup of parameter '___sku0' native representation
	il2cpp_codegen_marshal_free(____sku0_marshaled);
	____sku0_marshaled = NULL;

}
// System.Boolean OnePF.OpenIAB_iOS::IsDevice()
extern "C"  bool OpenIAB_iOS_IsDevice_m4101686221 (OpenIAB_iOS_t446655906 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m2818272885(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)8)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		return (bool)1;
	}
}
// System.Void OnePF.OpenIAB_iOS::init(OnePF.Options)
extern "C"  void OpenIAB_iOS_init_m2526952909 (OpenIAB_iOS_t446655906 * __this, Options_t3062372690 * ___options0, const MethodInfo* method)
{
	{
		bool L_0 = OpenIAB_iOS_IsDevice_m4101686221(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Options_t3062372690 * L_1 = ___options0;
		NullCheck(L_1);
		Dictionary_2_t2606186806 * L_2 = L_1->get_storeKeys_7();
		OpenIAB_iOS_init_m205509482(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OnePF.OpenIAB_iOS::init(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern Il2CppClass* OpenIAB_iOS_t446655906_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern const MethodInfo* Enumerable_ElementAt_TisKeyValuePair_2_t2094718104_m631600327_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m730091314_MethodInfo_var;
extern const uint32_t OpenIAB_iOS_init_m205509482_MetadataUsageId;
extern "C"  void OpenIAB_iOS_init_m205509482 (OpenIAB_iOS_t446655906 * __this, Dictionary_2_t2606186806 * ___storeKeys0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIAB_iOS_init_m205509482_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t2956870243* V_0 = NULL;
	int32_t V_1 = 0;
	KeyValuePair_2_t2094718104  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		bool L_0 = OpenIAB_iOS_IsDevice_m4101686221(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(OpenIAB_iOS_t446655906_il2cpp_TypeInfo_var);
		Dictionary_2_t2606186806 * L_1 = ((OpenIAB_iOS_t446655906_StaticFields*)OpenIAB_iOS_t446655906_il2cpp_TypeInfo_var->static_fields)->get__sku2storeSkuMappings_1();
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<System.String,System.String>::get_Count() */, L_1);
		V_0 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)L_2));
		V_1 = 0;
		goto IL_003d;
	}

IL_0023:
	{
		StringU5BU5D_t2956870243* L_3 = V_0;
		int32_t L_4 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(OpenIAB_iOS_t446655906_il2cpp_TypeInfo_var);
		Dictionary_2_t2606186806 * L_5 = ((OpenIAB_iOS_t446655906_StaticFields*)OpenIAB_iOS_t446655906_il2cpp_TypeInfo_var->static_fields)->get__sku2storeSkuMappings_1();
		int32_t L_6 = V_1;
		KeyValuePair_2_t2094718104  L_7 = Enumerable_ElementAt_TisKeyValuePair_2_t2094718104_m631600327(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/Enumerable_ElementAt_TisKeyValuePair_2_t2094718104_m631600327_MethodInfo_var);
		V_2 = L_7;
		String_t* L_8 = KeyValuePair_2_get_Value_m730091314((&V_2), /*hidden argument*/KeyValuePair_2_get_Value_m730091314_MethodInfo_var);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		ArrayElementTypeCheck (L_3, L_8);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_4), (String_t*)L_8);
		int32_t L_9 = V_1;
		V_1 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_003d:
	{
		int32_t L_10 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(OpenIAB_iOS_t446655906_il2cpp_TypeInfo_var);
		Dictionary_2_t2606186806 * L_11 = ((OpenIAB_iOS_t446655906_StaticFields*)OpenIAB_iOS_t446655906_il2cpp_TypeInfo_var->static_fields)->get__sku2storeSkuMappings_1();
		NullCheck(L_11);
		int32_t L_12 = VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<System.String,System.String>::get_Count() */, L_11);
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_0023;
		}
	}
	{
		StringU5BU5D_t2956870243* L_13 = V_0;
		StringU5BU5D_t2956870243* L_14 = V_0;
		NullCheck(L_14);
		IL2CPP_RUNTIME_CLASS_INIT(OpenIAB_iOS_t446655906_il2cpp_TypeInfo_var);
		OpenIAB_iOS_AppStore_requestProducts_m1401556628(NULL /*static, unused*/, L_13, (((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))), /*hidden argument*/NULL);
		return;
	}
}
// System.Void OnePF.OpenIAB_iOS::mapSku(System.String,System.String,System.String)
extern Il2CppClass* OpenIAB_iOS_t446655906_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t OpenIAB_iOS_mapSku_m3488441136_MetadataUsageId;
extern "C"  void OpenIAB_iOS_mapSku_m3488441136 (OpenIAB_iOS_t446655906 * __this, String_t* ___sku0, String_t* ___storeName1, String_t* ___storeSku2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIAB_iOS_mapSku_m3488441136_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___storeName1;
		IL2CPP_RUNTIME_CLASS_INIT(OpenIAB_iOS_t446655906_il2cpp_TypeInfo_var);
		String_t* L_1 = ((OpenIAB_iOS_t446655906_StaticFields*)OpenIAB_iOS_t446655906_il2cpp_TypeInfo_var->static_fields)->get_STORE_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OpenIAB_iOS_t446655906_il2cpp_TypeInfo_var);
		Dictionary_2_t2606186806 * L_3 = ((OpenIAB_iOS_t446655906_StaticFields*)OpenIAB_iOS_t446655906_il2cpp_TypeInfo_var->static_fields)->get__sku2storeSkuMappings_1();
		String_t* L_4 = ___sku0;
		String_t* L_5 = ___storeSku2;
		NullCheck(L_3);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::set_Item(!0,!1) */, L_3, L_4, L_5);
		Dictionary_2_t2606186806 * L_6 = ((OpenIAB_iOS_t446655906_StaticFields*)OpenIAB_iOS_t446655906_il2cpp_TypeInfo_var->static_fields)->get__storeSku2skuMappings_2();
		String_t* L_7 = ___storeSku2;
		String_t* L_8 = ___sku0;
		NullCheck(L_6);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::set_Item(!0,!1) */, L_6, L_7, L_8);
	}

IL_0028:
	{
		return;
	}
}
// System.Void OnePF.OpenIAB_iOS::unbindService()
extern "C"  void OpenIAB_iOS_unbindService_m2578881304 (OpenIAB_iOS_t446655906 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Boolean OnePF.OpenIAB_iOS::areSubscriptionsSupported()
extern "C"  bool OpenIAB_iOS_areSubscriptionsSupported_m1369726657 (OpenIAB_iOS_t446655906 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void OnePF.OpenIAB_iOS::queryInventory()
extern Il2CppClass* OpenIAB_iOS_t446655906_il2cpp_TypeInfo_var;
extern const uint32_t OpenIAB_iOS_queryInventory_m4138450461_MetadataUsageId;
extern "C"  void OpenIAB_iOS_queryInventory_m4138450461 (OpenIAB_iOS_t446655906 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIAB_iOS_queryInventory_m4138450461_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = OpenIAB_iOS_IsDevice_m4101686221(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(OpenIAB_iOS_t446655906_il2cpp_TypeInfo_var);
		OpenIAB_iOS_Inventory_query_m1367326750(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OnePF.OpenIAB_iOS::queryInventory(System.String[])
extern "C"  void OpenIAB_iOS_queryInventory_m312300003 (OpenIAB_iOS_t446655906 * __this, StringU5BU5D_t2956870243* ___skus0, const MethodInfo* method)
{
	{
		VirtActionInvoker0::Invoke(8 /* System.Void OnePF.OpenIAB_iOS::queryInventory() */, __this);
		return;
	}
}
// System.Void OnePF.OpenIAB_iOS::purchaseProduct(System.String,System.String)
extern Il2CppClass* OpenIAB_iOS_t446655906_il2cpp_TypeInfo_var;
extern Il2CppClass* OpenIAB_t3048630868_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1519288865;
extern const uint32_t OpenIAB_iOS_purchaseProduct_m491925719_MetadataUsageId;
extern "C"  void OpenIAB_iOS_purchaseProduct_m491925719 (OpenIAB_iOS_t446655906 * __this, String_t* ___sku0, String_t* ___developerPayload1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIAB_iOS_purchaseProduct_m491925719_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(OpenIAB_iOS_t446655906_il2cpp_TypeInfo_var);
		Dictionary_2_t2606186806 * L_0 = ((OpenIAB_iOS_t446655906_StaticFields*)OpenIAB_iOS_t446655906_il2cpp_TypeInfo_var->static_fields)->get__sku2storeSkuMappings_1();
		String_t* L_1 = ___sku0;
		NullCheck(L_0);
		String_t* L_2 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(22 /* !1 System.Collections.Generic.Dictionary`2<System.String,System.String>::get_Item(!0) */, L_0, L_1);
		V_0 = L_2;
		bool L_3 = OpenIAB_iOS_IsDevice_m4101686221(__this, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0028;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OpenIAB_t3048630868_il2cpp_TypeInfo_var);
		GameObject_t4012695102 * L_4 = OpenIAB_get_EventManager_m4227360122(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_5 = V_0;
		NullCheck(L_4);
		GameObject_SendMessage_m3364342903(L_4, _stringLiteral1519288865, L_5, /*hidden argument*/NULL);
		return;
	}

IL_0028:
	{
		String_t* L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(OpenIAB_iOS_t446655906_il2cpp_TypeInfo_var);
		OpenIAB_iOS_AppStore_startPurchase_m3350896277(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OnePF.OpenIAB_iOS::purchaseSubscription(System.String,System.String)
extern "C"  void OpenIAB_iOS_purchaseSubscription_m3876771447 (OpenIAB_iOS_t446655906 * __this, String_t* ___sku0, String_t* ___developerPayload1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___sku0;
		String_t* L_1 = ___developerPayload1;
		VirtActionInvoker2< String_t*, String_t* >::Invoke(10 /* System.Void OnePF.OpenIAB_iOS::purchaseProduct(System.String,System.String) */, __this, L_0, L_1);
		return;
	}
}
// System.Void OnePF.OpenIAB_iOS::consumeProduct(OnePF.Purchase)
extern Il2CppClass* OpenIAB_t3048630868_il2cpp_TypeInfo_var;
extern Il2CppClass* OpenIAB_iOS_t446655906_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1564740323;
extern Il2CppCodeGenString* _stringLiteral2172644091;
extern Il2CppCodeGenString* _stringLiteral1355219894;
extern const uint32_t OpenIAB_iOS_consumeProduct_m2220225353_MetadataUsageId;
extern "C"  void OpenIAB_iOS_consumeProduct_m2220225353 (OpenIAB_iOS_t446655906 * __this, Purchase_t160195573 * ___purchase0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIAB_iOS_consumeProduct_m2220225353_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		bool L_0 = OpenIAB_iOS_IsDevice_m4101686221(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0021;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OpenIAB_t3048630868_il2cpp_TypeInfo_var);
		GameObject_t4012695102 * L_1 = OpenIAB_get_EventManager_m4227360122(NULL /*static, unused*/, /*hidden argument*/NULL);
		Purchase_t160195573 * L_2 = ___purchase0;
		NullCheck(L_2);
		String_t* L_3 = Purchase_Serialize_m500090903(L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SendMessage_m3364342903(L_1, _stringLiteral1564740323, L_3, /*hidden argument*/NULL);
		return;
	}

IL_0021:
	{
		Purchase_t160195573 * L_4 = ___purchase0;
		NullCheck(L_4);
		String_t* L_5 = Purchase_get_Sku_m889168491(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		String_t* L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(OpenIAB_iOS_t446655906_il2cpp_TypeInfo_var);
		bool L_7 = OpenIAB_iOS_Inventory_hasPurchase_m3366375669(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0053;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OpenIAB_t3048630868_il2cpp_TypeInfo_var);
		GameObject_t4012695102 * L_8 = OpenIAB_get_EventManager_m4227360122(NULL /*static, unused*/, /*hidden argument*/NULL);
		Purchase_t160195573 * L_9 = ___purchase0;
		NullCheck(L_9);
		String_t* L_10 = Purchase_Serialize_m500090903(L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		GameObject_SendMessage_m3364342903(L_8, _stringLiteral1564740323, L_10, /*hidden argument*/NULL);
		String_t* L_11 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(OpenIAB_iOS_t446655906_il2cpp_TypeInfo_var);
		OpenIAB_iOS_Inventory_removePurchase_m653450737(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		goto IL_0067;
	}

IL_0053:
	{
		IL2CPP_RUNTIME_CLASS_INIT(OpenIAB_t3048630868_il2cpp_TypeInfo_var);
		GameObject_t4012695102 * L_12 = OpenIAB_get_EventManager_m4227360122(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_12);
		GameObject_SendMessage_m3364342903(L_12, _stringLiteral2172644091, _stringLiteral1355219894, /*hidden argument*/NULL);
	}

IL_0067:
	{
		return;
	}
}
// System.Void OnePF.OpenIAB_iOS::restoreTransactions()
extern Il2CppClass* OpenIAB_iOS_t446655906_il2cpp_TypeInfo_var;
extern const uint32_t OpenIAB_iOS_restoreTransactions_m2170322844_MetadataUsageId;
extern "C"  void OpenIAB_iOS_restoreTransactions_m2170322844 (OpenIAB_iOS_t446655906 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIAB_iOS_restoreTransactions_m2170322844_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OpenIAB_iOS_t446655906_il2cpp_TypeInfo_var);
		OpenIAB_iOS_AppStore_restorePurchases_m1704837692(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean OnePF.OpenIAB_iOS::isDebugLog()
extern "C"  bool OpenIAB_iOS_isDebugLog_m433406216 (OpenIAB_iOS_t446655906 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void OnePF.OpenIAB_iOS::enableDebugLogging(System.Boolean)
extern "C"  void OpenIAB_iOS_enableDebugLogging_m482342927 (OpenIAB_iOS_t446655906 * __this, bool ___enabled0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void OnePF.OpenIAB_iOS::enableDebugLogging(System.Boolean,System.String)
extern "C"  void OpenIAB_iOS_enableDebugLogging_m1170364939 (OpenIAB_iOS_t446655906 * __this, bool ___enabled0, String_t* ___tag1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.String OnePF.OpenIAB_iOS::StoreSku2Sku(System.String)
extern Il2CppClass* OpenIAB_iOS_t446655906_il2cpp_TypeInfo_var;
extern const uint32_t OpenIAB_iOS_StoreSku2Sku_m2417310421_MetadataUsageId;
extern "C"  String_t* OpenIAB_iOS_StoreSku2Sku_m2417310421 (Il2CppObject * __this /* static, unused */, String_t* ___storeSku0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIAB_iOS_StoreSku2Sku_m2417310421_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OpenIAB_iOS_t446655906_il2cpp_TypeInfo_var);
		Dictionary_2_t2606186806 * L_0 = ((OpenIAB_iOS_t446655906_StaticFields*)OpenIAB_iOS_t446655906_il2cpp_TypeInfo_var->static_fields)->get__storeSku2skuMappings_2();
		String_t* L_1 = ___storeSku0;
		NullCheck(L_0);
		String_t* L_2 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(22 /* !1 System.Collections.Generic.Dictionary`2<System.String,System.String>::get_Item(!0) */, L_0, L_1);
		return L_2;
	}
}
// System.String OnePF.OpenIAB_iOS::Sku2StoreSku(System.String)
extern Il2CppClass* OpenIAB_iOS_t446655906_il2cpp_TypeInfo_var;
extern const uint32_t OpenIAB_iOS_Sku2StoreSku_m744454475_MetadataUsageId;
extern "C"  String_t* OpenIAB_iOS_Sku2StoreSku_m744454475 (Il2CppObject * __this /* static, unused */, String_t* ___sku0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIAB_iOS_Sku2StoreSku_m744454475_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OpenIAB_iOS_t446655906_il2cpp_TypeInfo_var);
		Dictionary_2_t2606186806 * L_0 = ((OpenIAB_iOS_t446655906_StaticFields*)OpenIAB_iOS_t446655906_il2cpp_TypeInfo_var->static_fields)->get__sku2storeSkuMappings_1();
		String_t* L_1 = ___sku0;
		NullCheck(L_0);
		String_t* L_2 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(22 /* !1 System.Collections.Generic.Dictionary`2<System.String,System.String>::get_Item(!0) */, L_0, L_1);
		return L_2;
	}
}
// System.Void OnePF.OpenIAB_WP8::.ctor()
extern "C"  void OpenIAB_WP8__ctor_m3658365417 (OpenIAB_WP8_t446638612 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OnePF.OpenIAB_WP8::.cctor()
extern Il2CppClass* OpenIAB_WP8_t446638612_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2572651809;
extern const uint32_t OpenIAB_WP8__cctor_m1258082020_MetadataUsageId;
extern "C"  void OpenIAB_WP8__cctor_m1258082020 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIAB_WP8__cctor_m1258082020_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((OpenIAB_WP8_t446638612_StaticFields*)OpenIAB_WP8_t446638612_il2cpp_TypeInfo_var->static_fields)->set_STORE_0(_stringLiteral2572651809);
		return;
	}
}
// System.Void OnePF.Options::.ctor()
extern Il2CppClass* Dictionary_2_t2606186806_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m640701813_MethodInfo_var;
extern const uint32_t Options__ctor_m3361592171_MetadataUsageId;
extern "C"  void Options__ctor_m3361592171 (Options_t3062372690 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Options__ctor_m3361592171_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_discoveryTimeoutMs_2(((int32_t)5000));
		__this->set_checkInventory_3((bool)1);
		__this->set_checkInventoryTimeoutMs_4(((int32_t)10000));
		Dictionary_2_t2606186806 * L_0 = (Dictionary_2_t2606186806 *)il2cpp_codegen_object_new(Dictionary_2_t2606186806_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m640701813(L_0, /*hidden argument*/Dictionary_2__ctor_m640701813_MethodInfo_var);
		__this->set_storeKeys_7(L_0);
		__this->set_prefferedStoreNames_8(((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)0)));
		__this->set_availableStoreNames_9(((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)0)));
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OnePF.Purchase::.ctor()
extern "C"  void Purchase__ctor_m3923332508 (Purchase_t160195573 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OnePF.Purchase::.ctor(System.String)
extern Il2CppClass* JSON_t2649481148_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1177533677;
extern Il2CppCodeGenString* _stringLiteral3087856905;
extern Il2CppCodeGenString* _stringLiteral908759025;
extern Il2CppCodeGenString* _stringLiteral113949;
extern Il2CppCodeGenString* _stringLiteral2450809710;
extern Il2CppCodeGenString* _stringLiteral2960050192;
extern Il2CppCodeGenString* _stringLiteral1783568996;
extern Il2CppCodeGenString* _stringLiteral110541305;
extern Il2CppCodeGenString* _stringLiteral2137692761;
extern Il2CppCodeGenString* _stringLiteral1073584312;
extern Il2CppCodeGenString* _stringLiteral1398139403;
extern Il2CppCodeGenString* _stringLiteral1082290744;
extern const uint32_t Purchase__ctor_m3455340262_MetadataUsageId;
extern "C"  void Purchase__ctor_m3455340262 (Purchase_t160195573 * __this, String_t* ___jsonString0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Purchase__ctor_m3455340262_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JSON_t2649481148 * V_0 = NULL;
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___jsonString0;
		JSON_t2649481148 * L_1 = (JSON_t2649481148 *)il2cpp_codegen_object_new(JSON_t2649481148_il2cpp_TypeInfo_var);
		JSON__ctor_m2841521773(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		JSON_t2649481148 * L_2 = V_0;
		NullCheck(L_2);
		String_t* L_3 = JSON_ToString_m2727470212(L_2, _stringLiteral1177533677, /*hidden argument*/NULL);
		Purchase_set_ItemType_m1162244464(__this, L_3, /*hidden argument*/NULL);
		JSON_t2649481148 * L_4 = V_0;
		NullCheck(L_4);
		String_t* L_5 = JSON_ToString_m2727470212(L_4, _stringLiteral3087856905, /*hidden argument*/NULL);
		Purchase_set_OrderId_m1618951580(__this, L_5, /*hidden argument*/NULL);
		JSON_t2649481148 * L_6 = V_0;
		NullCheck(L_6);
		String_t* L_7 = JSON_ToString_m2727470212(L_6, _stringLiteral908759025, /*hidden argument*/NULL);
		Purchase_set_PackageName_m1213580212(__this, L_7, /*hidden argument*/NULL);
		JSON_t2649481148 * L_8 = V_0;
		NullCheck(L_8);
		String_t* L_9 = JSON_ToString_m2727470212(L_8, _stringLiteral113949, /*hidden argument*/NULL);
		Purchase_set_Sku_m830831880(__this, L_9, /*hidden argument*/NULL);
		JSON_t2649481148 * L_10 = V_0;
		NullCheck(L_10);
		int64_t L_11 = JSON_ToLong_m2611652581(L_10, _stringLiteral2450809710, /*hidden argument*/NULL);
		Purchase_set_PurchaseTime_m1592977253(__this, L_11, /*hidden argument*/NULL);
		JSON_t2649481148 * L_12 = V_0;
		NullCheck(L_12);
		int32_t L_13 = JSON_ToInt_m961998353(L_12, _stringLiteral2960050192, /*hidden argument*/NULL);
		Purchase_set_PurchaseState_m2841895230(__this, L_13, /*hidden argument*/NULL);
		JSON_t2649481148 * L_14 = V_0;
		NullCheck(L_14);
		String_t* L_15 = JSON_ToString_m2727470212(L_14, _stringLiteral1783568996, /*hidden argument*/NULL);
		Purchase_set_DeveloperPayload_m560298777(__this, L_15, /*hidden argument*/NULL);
		JSON_t2649481148 * L_16 = V_0;
		NullCheck(L_16);
		String_t* L_17 = JSON_ToString_m2727470212(L_16, _stringLiteral110541305, /*hidden argument*/NULL);
		Purchase_set_Token_m2755081132(__this, L_17, /*hidden argument*/NULL);
		JSON_t2649481148 * L_18 = V_0;
		NullCheck(L_18);
		String_t* L_19 = JSON_ToString_m2727470212(L_18, _stringLiteral2137692761, /*hidden argument*/NULL);
		Purchase_set_OriginalJson_m1816068228(__this, L_19, /*hidden argument*/NULL);
		JSON_t2649481148 * L_20 = V_0;
		NullCheck(L_20);
		String_t* L_21 = JSON_ToString_m2727470212(L_20, _stringLiteral1073584312, /*hidden argument*/NULL);
		Purchase_set_Signature_m1181936909(__this, L_21, /*hidden argument*/NULL);
		JSON_t2649481148 * L_22 = V_0;
		NullCheck(L_22);
		String_t* L_23 = JSON_ToString_m2727470212(L_22, _stringLiteral1398139403, /*hidden argument*/NULL);
		Purchase_set_AppstoreName_m3218609810(__this, L_23, /*hidden argument*/NULL);
		JSON_t2649481148 * L_24 = V_0;
		NullCheck(L_24);
		String_t* L_25 = JSON_ToString_m2727470212(L_24, _stringLiteral1082290744, /*hidden argument*/NULL);
		Purchase_set_Receipt_m3800554637(__this, L_25, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OnePF.Purchase::.ctor(OnePF.JSON)
extern Il2CppClass* OpenIAB_iOS_t446655906_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1177533677;
extern Il2CppCodeGenString* _stringLiteral3087856905;
extern Il2CppCodeGenString* _stringLiteral908759025;
extern Il2CppCodeGenString* _stringLiteral113949;
extern Il2CppCodeGenString* _stringLiteral2450809710;
extern Il2CppCodeGenString* _stringLiteral2960050192;
extern Il2CppCodeGenString* _stringLiteral1783568996;
extern Il2CppCodeGenString* _stringLiteral110541305;
extern Il2CppCodeGenString* _stringLiteral2137692761;
extern Il2CppCodeGenString* _stringLiteral1073584312;
extern Il2CppCodeGenString* _stringLiteral1398139403;
extern const uint32_t Purchase__ctor_m2219165762_MetadataUsageId;
extern "C"  void Purchase__ctor_m2219165762 (Purchase_t160195573 * __this, JSON_t2649481148 * ___json0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Purchase__ctor_m2219165762_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		JSON_t2649481148 * L_0 = ___json0;
		NullCheck(L_0);
		String_t* L_1 = JSON_ToString_m2727470212(L_0, _stringLiteral1177533677, /*hidden argument*/NULL);
		Purchase_set_ItemType_m1162244464(__this, L_1, /*hidden argument*/NULL);
		JSON_t2649481148 * L_2 = ___json0;
		NullCheck(L_2);
		String_t* L_3 = JSON_ToString_m2727470212(L_2, _stringLiteral3087856905, /*hidden argument*/NULL);
		Purchase_set_OrderId_m1618951580(__this, L_3, /*hidden argument*/NULL);
		JSON_t2649481148 * L_4 = ___json0;
		NullCheck(L_4);
		String_t* L_5 = JSON_ToString_m2727470212(L_4, _stringLiteral908759025, /*hidden argument*/NULL);
		Purchase_set_PackageName_m1213580212(__this, L_5, /*hidden argument*/NULL);
		JSON_t2649481148 * L_6 = ___json0;
		NullCheck(L_6);
		String_t* L_7 = JSON_ToString_m2727470212(L_6, _stringLiteral113949, /*hidden argument*/NULL);
		Purchase_set_Sku_m830831880(__this, L_7, /*hidden argument*/NULL);
		JSON_t2649481148 * L_8 = ___json0;
		NullCheck(L_8);
		int64_t L_9 = JSON_ToLong_m2611652581(L_8, _stringLiteral2450809710, /*hidden argument*/NULL);
		Purchase_set_PurchaseTime_m1592977253(__this, L_9, /*hidden argument*/NULL);
		JSON_t2649481148 * L_10 = ___json0;
		NullCheck(L_10);
		int32_t L_11 = JSON_ToInt_m961998353(L_10, _stringLiteral2960050192, /*hidden argument*/NULL);
		Purchase_set_PurchaseState_m2841895230(__this, L_11, /*hidden argument*/NULL);
		JSON_t2649481148 * L_12 = ___json0;
		NullCheck(L_12);
		String_t* L_13 = JSON_ToString_m2727470212(L_12, _stringLiteral1783568996, /*hidden argument*/NULL);
		Purchase_set_DeveloperPayload_m560298777(__this, L_13, /*hidden argument*/NULL);
		JSON_t2649481148 * L_14 = ___json0;
		NullCheck(L_14);
		String_t* L_15 = JSON_ToString_m2727470212(L_14, _stringLiteral110541305, /*hidden argument*/NULL);
		Purchase_set_Token_m2755081132(__this, L_15, /*hidden argument*/NULL);
		JSON_t2649481148 * L_16 = ___json0;
		NullCheck(L_16);
		String_t* L_17 = JSON_ToString_m2727470212(L_16, _stringLiteral2137692761, /*hidden argument*/NULL);
		Purchase_set_OriginalJson_m1816068228(__this, L_17, /*hidden argument*/NULL);
		JSON_t2649481148 * L_18 = ___json0;
		NullCheck(L_18);
		String_t* L_19 = JSON_ToString_m2727470212(L_18, _stringLiteral1073584312, /*hidden argument*/NULL);
		Purchase_set_Signature_m1181936909(__this, L_19, /*hidden argument*/NULL);
		JSON_t2649481148 * L_20 = ___json0;
		NullCheck(L_20);
		String_t* L_21 = JSON_ToString_m2727470212(L_20, _stringLiteral1398139403, /*hidden argument*/NULL);
		Purchase_set_AppstoreName_m3218609810(__this, L_21, /*hidden argument*/NULL);
		String_t* L_22 = Purchase_get_Sku_m889168491(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(OpenIAB_iOS_t446655906_il2cpp_TypeInfo_var);
		String_t* L_23 = OpenIAB_iOS_StoreSku2Sku_m2417310421(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		Purchase_set_Sku_m830831880(__this, L_23, /*hidden argument*/NULL);
		return;
	}
}
// System.String OnePF.Purchase::get_ItemType()
extern "C"  String_t* Purchase_get_ItemType_m4191074113 (Purchase_t160195573 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CItemTypeU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void OnePF.Purchase::set_ItemType(System.String)
extern "C"  void Purchase_set_ItemType_m1162244464 (Purchase_t160195573 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CItemTypeU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String OnePF.Purchase::get_OrderId()
extern "C"  String_t* Purchase_get_OrderId_m4249747671 (Purchase_t160195573 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3COrderIdU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void OnePF.Purchase::set_OrderId(System.String)
extern "C"  void Purchase_set_OrderId_m1618951580 (Purchase_t160195573 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3COrderIdU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String OnePF.Purchase::get_PackageName()
extern "C"  String_t* Purchase_get_PackageName_m260911935 (Purchase_t160195573 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CPackageNameU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void OnePF.Purchase::set_PackageName(System.String)
extern "C"  void Purchase_set_PackageName_m1213580212 (Purchase_t160195573 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CPackageNameU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String OnePF.Purchase::get_Sku()
extern "C"  String_t* Purchase_get_Sku_m889168491 (Purchase_t160195573 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CSkuU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void OnePF.Purchase::set_Sku(System.String)
extern "C"  void Purchase_set_Sku_m830831880 (Purchase_t160195573 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CSkuU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Int64 OnePF.Purchase::get_PurchaseTime()
extern "C"  int64_t Purchase_get_PurchaseTime_m3484110166 (Purchase_t160195573 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = __this->get_U3CPurchaseTimeU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void OnePF.Purchase::set_PurchaseTime(System.Int64)
extern "C"  void Purchase_set_PurchaseTime_m1592977253 (Purchase_t160195573 * __this, int64_t ___value0, const MethodInfo* method)
{
	{
		int64_t L_0 = ___value0;
		__this->set_U3CPurchaseTimeU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Int32 OnePF.Purchase::get_PurchaseState()
extern "C"  int32_t Purchase_get_PurchaseState_m3082064043 (Purchase_t160195573 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CPurchaseStateU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void OnePF.Purchase::set_PurchaseState(System.Int32)
extern "C"  void Purchase_set_PurchaseState_m2841895230 (Purchase_t160195573 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CPurchaseStateU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.String OnePF.Purchase::get_DeveloperPayload()
extern "C"  String_t* Purchase_get_DeveloperPayload_m2728153080 (Purchase_t160195573 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CDeveloperPayloadU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void OnePF.Purchase::set_DeveloperPayload(System.String)
extern "C"  void Purchase_set_DeveloperPayload_m560298777 (Purchase_t160195573 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CDeveloperPayloadU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.String OnePF.Purchase::get_Token()
extern "C"  String_t* Purchase_get_Token_m787097863 (Purchase_t160195573 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CTokenU3Ek__BackingField_7();
		return L_0;
	}
}
// System.Void OnePF.Purchase::set_Token(System.String)
extern "C"  void Purchase_set_Token_m2755081132 (Purchase_t160195573 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CTokenU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.String OnePF.Purchase::get_OriginalJson()
extern "C"  String_t* Purchase_get_OriginalJson_m3460393005 (Purchase_t160195573 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3COriginalJsonU3Ek__BackingField_8();
		return L_0;
	}
}
// System.Void OnePF.Purchase::set_OriginalJson(System.String)
extern "C"  void Purchase_set_OriginalJson_m1816068228 (Purchase_t160195573 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3COriginalJsonU3Ek__BackingField_8(L_0);
		return;
	}
}
// System.String OnePF.Purchase::get_Signature()
extern "C"  String_t* Purchase_get_Signature_m3383715206 (Purchase_t160195573 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CSignatureU3Ek__BackingField_9();
		return L_0;
	}
}
// System.Void OnePF.Purchase::set_Signature(System.String)
extern "C"  void Purchase_set_Signature_m1181936909 (Purchase_t160195573 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CSignatureU3Ek__BackingField_9(L_0);
		return;
	}
}
// System.String OnePF.Purchase::get_AppstoreName()
extern "C"  String_t* Purchase_get_AppstoreName_m1419219807 (Purchase_t160195573 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CAppstoreNameU3Ek__BackingField_10();
		return L_0;
	}
}
// System.Void OnePF.Purchase::set_AppstoreName(System.String)
extern "C"  void Purchase_set_AppstoreName_m3218609810 (Purchase_t160195573 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CAppstoreNameU3Ek__BackingField_10(L_0);
		return;
	}
}
// System.String OnePF.Purchase::get_Receipt()
extern "C"  String_t* Purchase_get_Receipt_m1046015558 (Purchase_t160195573 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CReceiptU3Ek__BackingField_11();
		return L_0;
	}
}
// System.Void OnePF.Purchase::set_Receipt(System.String)
extern "C"  void Purchase_set_Receipt_m3800554637 (Purchase_t160195573 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CReceiptU3Ek__BackingField_11(L_0);
		return;
	}
}
// OnePF.Purchase OnePF.Purchase::CreateFromSku(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Purchase_CreateFromSku_m2993707363_MetadataUsageId;
extern "C"  Purchase_t160195573 * Purchase_CreateFromSku_m2993707363 (Il2CppObject * __this /* static, unused */, String_t* ___sku0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Purchase_CreateFromSku_m2993707363_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___sku0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		Purchase_t160195573 * L_2 = Purchase_CreateFromSku_m3333675103(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// OnePF.Purchase OnePF.Purchase::CreateFromSku(System.String,System.String)
extern Il2CppClass* Purchase_t160195573_il2cpp_TypeInfo_var;
extern const uint32_t Purchase_CreateFromSku_m3333675103_MetadataUsageId;
extern "C"  Purchase_t160195573 * Purchase_CreateFromSku_m3333675103 (Il2CppObject * __this /* static, unused */, String_t* ___sku0, String_t* ___developerPayload1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Purchase_CreateFromSku_m3333675103_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Purchase_t160195573 * V_0 = NULL;
	{
		Purchase_t160195573 * L_0 = (Purchase_t160195573 *)il2cpp_codegen_object_new(Purchase_t160195573_il2cpp_TypeInfo_var);
		Purchase__ctor_m3923332508(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Purchase_t160195573 * L_1 = V_0;
		String_t* L_2 = ___sku0;
		NullCheck(L_1);
		Purchase_set_Sku_m830831880(L_1, L_2, /*hidden argument*/NULL);
		Purchase_t160195573 * L_3 = V_0;
		String_t* L_4 = ___developerPayload1;
		NullCheck(L_3);
		Purchase_set_DeveloperPayload_m560298777(L_3, L_4, /*hidden argument*/NULL);
		Purchase_t160195573 * L_5 = V_0;
		Purchase_AddIOSHack_m2229156322(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		Purchase_t160195573 * L_6 = V_0;
		return L_6;
	}
}
// System.String OnePF.Purchase::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2547421;
extern Il2CppCodeGenString* _stringLiteral59;
extern const uint32_t Purchase_ToString_m1003583863_MetadataUsageId;
extern "C"  String_t* Purchase_ToString_m1003583863 (Purchase_t160195573 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Purchase_ToString_m1003583863_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = Purchase_get_Sku_m889168491(__this, /*hidden argument*/NULL);
		String_t* L_1 = Purchase_get_OriginalJson_m3460393005(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m2933632197(NULL /*static, unused*/, _stringLiteral2547421, L_0, _stringLiteral59, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void OnePF.Purchase::AddIOSHack(OnePF.Purchase)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Guid_t2778838590_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2535818113;
extern Il2CppCodeGenString* _stringLiteral70760092;
extern const uint32_t Purchase_AddIOSHack_m2229156322_MetadataUsageId;
extern "C"  void Purchase_AddIOSHack_m2229156322 (Il2CppObject * __this /* static, unused */, Purchase_t160195573 * ___p0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Purchase_AddIOSHack_m2229156322_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Guid_t2778838590  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Purchase_t160195573 * L_0 = ___p0;
		NullCheck(L_0);
		String_t* L_1 = Purchase_get_AppstoreName_m1419219807(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001b;
		}
	}
	{
		Purchase_t160195573 * L_3 = ___p0;
		NullCheck(L_3);
		Purchase_set_AppstoreName_m3218609810(L_3, _stringLiteral2535818113, /*hidden argument*/NULL);
	}

IL_001b:
	{
		Purchase_t160195573 * L_4 = ___p0;
		NullCheck(L_4);
		String_t* L_5 = Purchase_get_ItemType_m4191074113(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0036;
		}
	}
	{
		Purchase_t160195573 * L_7 = ___p0;
		NullCheck(L_7);
		Purchase_set_ItemType_m1162244464(L_7, _stringLiteral70760092, /*hidden argument*/NULL);
	}

IL_0036:
	{
		Purchase_t160195573 * L_8 = ___p0;
		NullCheck(L_8);
		String_t* L_9 = Purchase_get_OrderId_m4249747671(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_10 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0059;
		}
	}
	{
		Purchase_t160195573 * L_11 = ___p0;
		IL2CPP_RUNTIME_CLASS_INIT(Guid_t2778838590_il2cpp_TypeInfo_var);
		Guid_t2778838590  L_12 = Guid_NewGuid_m3560729310(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_12;
		String_t* L_13 = Guid_ToString_m2528531937((&V_0), /*hidden argument*/NULL);
		NullCheck(L_11);
		Purchase_set_OrderId_m1618951580(L_11, L_13, /*hidden argument*/NULL);
	}

IL_0059:
	{
		return;
	}
}
// System.String OnePF.Purchase::Serialize()
extern Il2CppClass* JSON_t2649481148_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t2847414882_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1177533677;
extern Il2CppCodeGenString* _stringLiteral3087856905;
extern Il2CppCodeGenString* _stringLiteral908759025;
extern Il2CppCodeGenString* _stringLiteral113949;
extern Il2CppCodeGenString* _stringLiteral2450809710;
extern Il2CppCodeGenString* _stringLiteral2960050192;
extern Il2CppCodeGenString* _stringLiteral1783568996;
extern Il2CppCodeGenString* _stringLiteral110541305;
extern Il2CppCodeGenString* _stringLiteral2137692761;
extern Il2CppCodeGenString* _stringLiteral1073584312;
extern Il2CppCodeGenString* _stringLiteral1398139403;
extern Il2CppCodeGenString* _stringLiteral1082290744;
extern const uint32_t Purchase_Serialize_m500090903_MetadataUsageId;
extern "C"  String_t* Purchase_Serialize_m500090903 (Purchase_t160195573 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Purchase_Serialize_m500090903_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JSON_t2649481148 * V_0 = NULL;
	{
		JSON_t2649481148 * L_0 = (JSON_t2649481148 *)il2cpp_codegen_object_new(JSON_t2649481148_il2cpp_TypeInfo_var);
		JSON__ctor_m802668213(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		JSON_t2649481148 * L_1 = V_0;
		String_t* L_2 = Purchase_get_ItemType_m4191074113(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		JSON_set_Item_m996622641(L_1, _stringLiteral1177533677, L_2, /*hidden argument*/NULL);
		JSON_t2649481148 * L_3 = V_0;
		String_t* L_4 = Purchase_get_OrderId_m4249747671(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		JSON_set_Item_m996622641(L_3, _stringLiteral3087856905, L_4, /*hidden argument*/NULL);
		JSON_t2649481148 * L_5 = V_0;
		String_t* L_6 = Purchase_get_PackageName_m260911935(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		JSON_set_Item_m996622641(L_5, _stringLiteral908759025, L_6, /*hidden argument*/NULL);
		JSON_t2649481148 * L_7 = V_0;
		String_t* L_8 = Purchase_get_Sku_m889168491(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		JSON_set_Item_m996622641(L_7, _stringLiteral113949, L_8, /*hidden argument*/NULL);
		JSON_t2649481148 * L_9 = V_0;
		int64_t L_10 = Purchase_get_PurchaseTime_m3484110166(__this, /*hidden argument*/NULL);
		int64_t L_11 = L_10;
		Il2CppObject * L_12 = Box(Int64_t2847414882_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_9);
		JSON_set_Item_m996622641(L_9, _stringLiteral2450809710, L_12, /*hidden argument*/NULL);
		JSON_t2649481148 * L_13 = V_0;
		int32_t L_14 = Purchase_get_PurchaseState_m3082064043(__this, /*hidden argument*/NULL);
		int32_t L_15 = L_14;
		Il2CppObject * L_16 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_13);
		JSON_set_Item_m996622641(L_13, _stringLiteral2960050192, L_16, /*hidden argument*/NULL);
		JSON_t2649481148 * L_17 = V_0;
		String_t* L_18 = Purchase_get_DeveloperPayload_m2728153080(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		JSON_set_Item_m996622641(L_17, _stringLiteral1783568996, L_18, /*hidden argument*/NULL);
		JSON_t2649481148 * L_19 = V_0;
		String_t* L_20 = Purchase_get_Token_m787097863(__this, /*hidden argument*/NULL);
		NullCheck(L_19);
		JSON_set_Item_m996622641(L_19, _stringLiteral110541305, L_20, /*hidden argument*/NULL);
		JSON_t2649481148 * L_21 = V_0;
		String_t* L_22 = Purchase_get_OriginalJson_m3460393005(__this, /*hidden argument*/NULL);
		NullCheck(L_21);
		JSON_set_Item_m996622641(L_21, _stringLiteral2137692761, L_22, /*hidden argument*/NULL);
		JSON_t2649481148 * L_23 = V_0;
		String_t* L_24 = Purchase_get_Signature_m3383715206(__this, /*hidden argument*/NULL);
		NullCheck(L_23);
		JSON_set_Item_m996622641(L_23, _stringLiteral1073584312, L_24, /*hidden argument*/NULL);
		JSON_t2649481148 * L_25 = V_0;
		String_t* L_26 = Purchase_get_AppstoreName_m1419219807(__this, /*hidden argument*/NULL);
		NullCheck(L_25);
		JSON_set_Item_m996622641(L_25, _stringLiteral1398139403, L_26, /*hidden argument*/NULL);
		JSON_t2649481148 * L_27 = V_0;
		String_t* L_28 = Purchase_get_Receipt_m1046015558(__this, /*hidden argument*/NULL);
		NullCheck(L_27);
		JSON_set_Item_m996622641(L_27, _stringLiteral1082290744, L_28, /*hidden argument*/NULL);
		JSON_t2649481148 * L_29 = V_0;
		NullCheck(L_29);
		String_t* L_30 = JSON_get_serialized_m2868133311(L_29, /*hidden argument*/NULL);
		return L_30;
	}
}
// System.Void OnePF.SkuDetails::.ctor(System.String)
extern Il2CppClass* JSON_t2649481148_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1177533677;
extern Il2CppCodeGenString* _stringLiteral113949;
extern Il2CppCodeGenString* _stringLiteral3575610;
extern Il2CppCodeGenString* _stringLiteral106934601;
extern Il2CppCodeGenString* _stringLiteral110371416;
extern Il2CppCodeGenString* _stringLiteral2570421244;
extern Il2CppCodeGenString* _stringLiteral3271912;
extern Il2CppCodeGenString* _stringLiteral1004773790;
extern Il2CppCodeGenString* _stringLiteral2822919592;
extern const uint32_t SkuDetails__ctor_m3647762378_MetadataUsageId;
extern "C"  void SkuDetails__ctor_m3647762378 (SkuDetails_t70130009 * __this, String_t* ___jsonString0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SkuDetails__ctor_m3647762378_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JSON_t2649481148 * V_0 = NULL;
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___jsonString0;
		JSON_t2649481148 * L_1 = (JSON_t2649481148 *)il2cpp_codegen_object_new(JSON_t2649481148_il2cpp_TypeInfo_var);
		JSON__ctor_m2841521773(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		JSON_t2649481148 * L_2 = V_0;
		NullCheck(L_2);
		String_t* L_3 = JSON_ToString_m2727470212(L_2, _stringLiteral1177533677, /*hidden argument*/NULL);
		SkuDetails_set_ItemType_m2364950540(__this, L_3, /*hidden argument*/NULL);
		JSON_t2649481148 * L_4 = V_0;
		NullCheck(L_4);
		String_t* L_5 = JSON_ToString_m2727470212(L_4, _stringLiteral113949, /*hidden argument*/NULL);
		SkuDetails_set_Sku_m1064891628(__this, L_5, /*hidden argument*/NULL);
		JSON_t2649481148 * L_6 = V_0;
		NullCheck(L_6);
		String_t* L_7 = JSON_ToString_m2727470212(L_6, _stringLiteral3575610, /*hidden argument*/NULL);
		SkuDetails_set_Type_m485764639(__this, L_7, /*hidden argument*/NULL);
		JSON_t2649481148 * L_8 = V_0;
		NullCheck(L_8);
		String_t* L_9 = JSON_ToString_m2727470212(L_8, _stringLiteral106934601, /*hidden argument*/NULL);
		SkuDetails_set_Price_m3037446464(__this, L_9, /*hidden argument*/NULL);
		JSON_t2649481148 * L_10 = V_0;
		NullCheck(L_10);
		String_t* L_11 = JSON_ToString_m2727470212(L_10, _stringLiteral110371416, /*hidden argument*/NULL);
		SkuDetails_set_Title_m1624453201(__this, L_11, /*hidden argument*/NULL);
		JSON_t2649481148 * L_12 = V_0;
		NullCheck(L_12);
		String_t* L_13 = JSON_ToString_m2727470212(L_12, _stringLiteral2570421244, /*hidden argument*/NULL);
		SkuDetails_set_Description_m810036269(__this, L_13, /*hidden argument*/NULL);
		JSON_t2649481148 * L_14 = V_0;
		NullCheck(L_14);
		String_t* L_15 = JSON_ToString_m2727470212(L_14, _stringLiteral3271912, /*hidden argument*/NULL);
		SkuDetails_set_Json_m374865585(__this, L_15, /*hidden argument*/NULL);
		JSON_t2649481148 * L_16 = V_0;
		NullCheck(L_16);
		String_t* L_17 = JSON_ToString_m2727470212(L_16, _stringLiteral1004773790, /*hidden argument*/NULL);
		SkuDetails_set_CurrencyCode_m2155024187(__this, L_17, /*hidden argument*/NULL);
		JSON_t2649481148 * L_18 = V_0;
		NullCheck(L_18);
		String_t* L_19 = JSON_ToString_m2727470212(L_18, _stringLiteral2822919592, /*hidden argument*/NULL);
		SkuDetails_set_PriceValue_m2641060849(__this, L_19, /*hidden argument*/NULL);
		SkuDetails_ParseFromJson_m3619400859(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OnePF.SkuDetails::.ctor(OnePF.JSON)
extern Il2CppClass* OpenIAB_iOS_t446655906_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1177533677;
extern Il2CppCodeGenString* _stringLiteral113949;
extern Il2CppCodeGenString* _stringLiteral3575610;
extern Il2CppCodeGenString* _stringLiteral106934601;
extern Il2CppCodeGenString* _stringLiteral110371416;
extern Il2CppCodeGenString* _stringLiteral2570421244;
extern Il2CppCodeGenString* _stringLiteral3271912;
extern Il2CppCodeGenString* _stringLiteral1004773790;
extern Il2CppCodeGenString* _stringLiteral2822919592;
extern const uint32_t SkuDetails__ctor_m108523998_MetadataUsageId;
extern "C"  void SkuDetails__ctor_m108523998 (SkuDetails_t70130009 * __this, JSON_t2649481148 * ___json0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SkuDetails__ctor_m108523998_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		JSON_t2649481148 * L_0 = ___json0;
		NullCheck(L_0);
		String_t* L_1 = JSON_ToString_m2727470212(L_0, _stringLiteral1177533677, /*hidden argument*/NULL);
		SkuDetails_set_ItemType_m2364950540(__this, L_1, /*hidden argument*/NULL);
		JSON_t2649481148 * L_2 = ___json0;
		NullCheck(L_2);
		String_t* L_3 = JSON_ToString_m2727470212(L_2, _stringLiteral113949, /*hidden argument*/NULL);
		SkuDetails_set_Sku_m1064891628(__this, L_3, /*hidden argument*/NULL);
		JSON_t2649481148 * L_4 = ___json0;
		NullCheck(L_4);
		String_t* L_5 = JSON_ToString_m2727470212(L_4, _stringLiteral3575610, /*hidden argument*/NULL);
		SkuDetails_set_Type_m485764639(__this, L_5, /*hidden argument*/NULL);
		JSON_t2649481148 * L_6 = ___json0;
		NullCheck(L_6);
		String_t* L_7 = JSON_ToString_m2727470212(L_6, _stringLiteral106934601, /*hidden argument*/NULL);
		SkuDetails_set_Price_m3037446464(__this, L_7, /*hidden argument*/NULL);
		JSON_t2649481148 * L_8 = ___json0;
		NullCheck(L_8);
		String_t* L_9 = JSON_ToString_m2727470212(L_8, _stringLiteral110371416, /*hidden argument*/NULL);
		SkuDetails_set_Title_m1624453201(__this, L_9, /*hidden argument*/NULL);
		JSON_t2649481148 * L_10 = ___json0;
		NullCheck(L_10);
		String_t* L_11 = JSON_ToString_m2727470212(L_10, _stringLiteral2570421244, /*hidden argument*/NULL);
		SkuDetails_set_Description_m810036269(__this, L_11, /*hidden argument*/NULL);
		JSON_t2649481148 * L_12 = ___json0;
		NullCheck(L_12);
		String_t* L_13 = JSON_ToString_m2727470212(L_12, _stringLiteral3271912, /*hidden argument*/NULL);
		SkuDetails_set_Json_m374865585(__this, L_13, /*hidden argument*/NULL);
		JSON_t2649481148 * L_14 = ___json0;
		NullCheck(L_14);
		String_t* L_15 = JSON_ToString_m2727470212(L_14, _stringLiteral1004773790, /*hidden argument*/NULL);
		SkuDetails_set_CurrencyCode_m2155024187(__this, L_15, /*hidden argument*/NULL);
		JSON_t2649481148 * L_16 = ___json0;
		NullCheck(L_16);
		String_t* L_17 = JSON_ToString_m2727470212(L_16, _stringLiteral2822919592, /*hidden argument*/NULL);
		SkuDetails_set_PriceValue_m2641060849(__this, L_17, /*hidden argument*/NULL);
		String_t* L_18 = SkuDetails_get_Sku_m1697287367(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(OpenIAB_iOS_t446655906_il2cpp_TypeInfo_var);
		String_t* L_19 = OpenIAB_iOS_StoreSku2Sku_m2417310421(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		SkuDetails_set_Sku_m1064891628(__this, L_19, /*hidden argument*/NULL);
		return;
	}
}
// System.String OnePF.SkuDetails::get_ItemType()
extern "C"  String_t* SkuDetails_get_ItemType_m1055123045 (SkuDetails_t70130009 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CItemTypeU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void OnePF.SkuDetails::set_ItemType(System.String)
extern "C"  void SkuDetails_set_ItemType_m2364950540 (SkuDetails_t70130009 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CItemTypeU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String OnePF.SkuDetails::get_Sku()
extern "C"  String_t* SkuDetails_get_Sku_m1697287367 (SkuDetails_t70130009 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CSkuU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void OnePF.SkuDetails::set_Sku(System.String)
extern "C"  void SkuDetails_set_Sku_m1064891628 (SkuDetails_t70130009 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CSkuU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String OnePF.SkuDetails::get_Type()
extern "C"  String_t* SkuDetails_get_Type_m1117768946 (SkuDetails_t70130009 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CTypeU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void OnePF.SkuDetails::set_Type(System.String)
extern "C"  void SkuDetails_set_Type_m485764639 (SkuDetails_t70130009 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CTypeU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String OnePF.SkuDetails::get_Price()
extern "C"  String_t* SkuDetails_get_Price_m829181875 (SkuDetails_t70130009 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CPriceU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void OnePF.SkuDetails::set_Price(System.String)
extern "C"  void SkuDetails_set_Price_m3037446464 (SkuDetails_t70130009 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CPriceU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.String OnePF.SkuDetails::get_Title()
extern "C"  String_t* SkuDetails_get_Title_m4131961090 (SkuDetails_t70130009 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CTitleU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void OnePF.SkuDetails::set_Title(System.String)
extern "C"  void SkuDetails_set_Title_m1624453201 (SkuDetails_t70130009 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CTitleU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.String OnePF.SkuDetails::get_Description()
extern "C"  String_t* SkuDetails_get_Description_m400826086 (SkuDetails_t70130009 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CDescriptionU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void OnePF.SkuDetails::set_Description(System.String)
extern "C"  void SkuDetails_set_Description_m810036269 (SkuDetails_t70130009 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CDescriptionU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.String OnePF.SkuDetails::get_Json()
extern "C"  String_t* SkuDetails_get_Json_m825915168 (SkuDetails_t70130009 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CJsonU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void OnePF.SkuDetails::set_Json(System.String)
extern "C"  void SkuDetails_set_Json_m374865585 (SkuDetails_t70130009 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CJsonU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.String OnePF.SkuDetails::get_CurrencyCode()
extern "C"  String_t* SkuDetails_get_CurrencyCode_m2608246614 (SkuDetails_t70130009 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CCurrencyCodeU3Ek__BackingField_7();
		return L_0;
	}
}
// System.Void OnePF.SkuDetails::set_CurrencyCode(System.String)
extern "C"  void SkuDetails_set_CurrencyCode_m2155024187 (SkuDetails_t70130009 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCurrencyCodeU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.String OnePF.SkuDetails::get_PriceValue()
extern "C"  String_t* SkuDetails_get_PriceValue_m3111594656 (SkuDetails_t70130009 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CPriceValueU3Ek__BackingField_8();
		return L_0;
	}
}
// System.Void OnePF.SkuDetails::set_PriceValue(System.String)
extern "C"  void SkuDetails_set_PriceValue_m2641060849 (SkuDetails_t70130009 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CPriceValueU3Ek__BackingField_8(L_0);
		return;
	}
}
// System.Void OnePF.SkuDetails::ParseFromJson()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* JSON_t2649481148_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral491701120;
extern Il2CppCodeGenString* _stringLiteral1535830245;
extern const uint32_t SkuDetails_ParseFromJson_m3619400859_MetadataUsageId;
extern "C"  void SkuDetails_ParseFromJson_m3619400859 (SkuDetails_t70130009 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SkuDetails_ParseFromJson_m3619400859_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JSON_t2649481148 * V_0 = NULL;
	float V_1 = 0.0f;
	{
		String_t* L_0 = SkuDetails_get_Json_m825915168(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		String_t* L_2 = SkuDetails_get_Json_m825915168(__this, /*hidden argument*/NULL);
		JSON_t2649481148 * L_3 = (JSON_t2649481148 *)il2cpp_codegen_object_new(JSON_t2649481148_il2cpp_TypeInfo_var);
		JSON__ctor_m2841521773(L_3, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		String_t* L_4 = SkuDetails_get_PriceValue_m3111594656(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_004e;
		}
	}
	{
		JSON_t2649481148 * L_6 = V_0;
		NullCheck(L_6);
		float L_7 = JSON_ToFloat_m3878641306(L_6, _stringLiteral491701120, /*hidden argument*/NULL);
		V_1 = L_7;
		float L_8 = V_1;
		V_1 = ((float)((float)L_8/(float)(1000000.0f)));
		String_t* L_9 = Single_ToString_m5736032((&V_1), /*hidden argument*/NULL);
		SkuDetails_set_PriceValue_m2641060849(__this, L_9, /*hidden argument*/NULL);
	}

IL_004e:
	{
		String_t* L_10 = SkuDetails_get_CurrencyCode_m2608246614(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_11 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_006f;
		}
	}
	{
		JSON_t2649481148 * L_12 = V_0;
		NullCheck(L_12);
		String_t* L_13 = JSON_ToString_m2727470212(L_12, _stringLiteral1535830245, /*hidden argument*/NULL);
		SkuDetails_set_CurrencyCode_m2155024187(__this, L_13, /*hidden argument*/NULL);
	}

IL_006f:
	{
		return;
	}
}
// System.String OnePF.SkuDetails::ToString()
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral674176224;
extern const uint32_t SkuDetails_ToString_m285465243_MetadataUsageId;
extern "C"  String_t* SkuDetails_ToString_m285465243 (SkuDetails_t70130009 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SkuDetails_ToString_m285465243_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t11523773* L_0 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)7));
		String_t* L_1 = SkuDetails_get_ItemType_m1055123045(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_1);
		ObjectU5BU5D_t11523773* L_2 = L_0;
		String_t* L_3 = SkuDetails_get_Sku_m1697287367(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_3);
		ObjectU5BU5D_t11523773* L_4 = L_2;
		String_t* L_5 = SkuDetails_get_Title_m4131961090(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_5);
		ObjectU5BU5D_t11523773* L_6 = L_4;
		String_t* L_7 = SkuDetails_get_Price_m829181875(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		ArrayElementTypeCheck (L_6, L_7);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_7);
		ObjectU5BU5D_t11523773* L_8 = L_6;
		String_t* L_9 = SkuDetails_get_Description_m400826086(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 4);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)L_9);
		ObjectU5BU5D_t11523773* L_10 = L_8;
		String_t* L_11 = SkuDetails_get_PriceValue_m3111594656(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 5);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_11);
		ObjectU5BU5D_t11523773* L_12 = L_10;
		String_t* L_13 = SkuDetails_get_CurrencyCode_m2608246614(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 6);
		ArrayElementTypeCheck (L_12, L_13);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)L_13);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Format_m4050103162(NULL /*static, unused*/, _stringLiteral674176224, L_12, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Void OpenIABEventManager::.ctor()
extern "C"  void OpenIABEventManager__ctor_m3426801220 (OpenIABEventManager_t528054099 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m350695606(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OpenIABEventManager::add_billingSupportedEvent(System.Action)
extern Il2CppClass* OpenIABEventManager_t528054099_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t OpenIABEventManager_add_billingSupportedEvent_m3299103698_MetadataUsageId;
extern "C"  void OpenIABEventManager_add_billingSupportedEvent_m3299103698 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIABEventManager_add_billingSupportedEvent_m3299103698_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t437523947 * L_0 = ((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->get_billingSupportedEvent_2();
		Action_t437523947 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->set_billingSupportedEvent_2(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void OpenIABEventManager::remove_billingSupportedEvent(System.Action)
extern Il2CppClass* OpenIABEventManager_t528054099_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t OpenIABEventManager_remove_billingSupportedEvent_m3486732849_MetadataUsageId;
extern "C"  void OpenIABEventManager_remove_billingSupportedEvent_m3486732849 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIABEventManager_remove_billingSupportedEvent_m3486732849_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t437523947 * L_0 = ((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->get_billingSupportedEvent_2();
		Action_t437523947 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->set_billingSupportedEvent_2(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void OpenIABEventManager::add_billingNotSupportedEvent(System.Action`1<System.String>)
extern Il2CppClass* OpenIABEventManager_t528054099_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t1116941607_il2cpp_TypeInfo_var;
extern const uint32_t OpenIABEventManager_add_billingNotSupportedEvent_m1533046238_MetadataUsageId;
extern "C"  void OpenIABEventManager_add_billingNotSupportedEvent_m1533046238 (Il2CppObject * __this /* static, unused */, Action_1_t1116941607 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIABEventManager_add_billingNotSupportedEvent_m1533046238_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t1116941607 * L_0 = ((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->get_billingNotSupportedEvent_3();
		Action_1_t1116941607 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->set_billingNotSupportedEvent_3(((Action_1_t1116941607 *)CastclassSealed(L_2, Action_1_t1116941607_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void OpenIABEventManager::remove_billingNotSupportedEvent(System.Action`1<System.String>)
extern Il2CppClass* OpenIABEventManager_t528054099_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t1116941607_il2cpp_TypeInfo_var;
extern const uint32_t OpenIABEventManager_remove_billingNotSupportedEvent_m1944553149_MetadataUsageId;
extern "C"  void OpenIABEventManager_remove_billingNotSupportedEvent_m1944553149 (Il2CppObject * __this /* static, unused */, Action_1_t1116941607 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIABEventManager_remove_billingNotSupportedEvent_m1944553149_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t1116941607 * L_0 = ((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->get_billingNotSupportedEvent_3();
		Action_1_t1116941607 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->set_billingNotSupportedEvent_3(((Action_1_t1116941607 *)CastclassSealed(L_2, Action_1_t1116941607_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void OpenIABEventManager::add_queryInventorySucceededEvent(System.Action`1<OnePF.Inventory>)
extern Il2CppClass* OpenIABEventManager_t528054099_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t2779015537_il2cpp_TypeInfo_var;
extern const uint32_t OpenIABEventManager_add_queryInventorySucceededEvent_m3971075585_MetadataUsageId;
extern "C"  void OpenIABEventManager_add_queryInventorySucceededEvent_m3971075585 (Il2CppObject * __this /* static, unused */, Action_1_t2779015537 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIABEventManager_add_queryInventorySucceededEvent_m3971075585_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t2779015537 * L_0 = ((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->get_queryInventorySucceededEvent_4();
		Action_1_t2779015537 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->set_queryInventorySucceededEvent_4(((Action_1_t2779015537 *)CastclassSealed(L_2, Action_1_t2779015537_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void OpenIABEventManager::remove_queryInventorySucceededEvent(System.Action`1<OnePF.Inventory>)
extern Il2CppClass* OpenIABEventManager_t528054099_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t2779015537_il2cpp_TypeInfo_var;
extern const uint32_t OpenIABEventManager_remove_queryInventorySucceededEvent_m3994321824_MetadataUsageId;
extern "C"  void OpenIABEventManager_remove_queryInventorySucceededEvent_m3994321824 (Il2CppObject * __this /* static, unused */, Action_1_t2779015537 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIABEventManager_remove_queryInventorySucceededEvent_m3994321824_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t2779015537 * L_0 = ((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->get_queryInventorySucceededEvent_4();
		Action_1_t2779015537 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->set_queryInventorySucceededEvent_4(((Action_1_t2779015537 *)CastclassSealed(L_2, Action_1_t2779015537_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void OpenIABEventManager::add_queryInventoryFailedEvent(System.Action`1<System.String>)
extern Il2CppClass* OpenIABEventManager_t528054099_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t1116941607_il2cpp_TypeInfo_var;
extern const uint32_t OpenIABEventManager_add_queryInventoryFailedEvent_m568864969_MetadataUsageId;
extern "C"  void OpenIABEventManager_add_queryInventoryFailedEvent_m568864969 (Il2CppObject * __this /* static, unused */, Action_1_t1116941607 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIABEventManager_add_queryInventoryFailedEvent_m568864969_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t1116941607 * L_0 = ((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->get_queryInventoryFailedEvent_5();
		Action_1_t1116941607 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->set_queryInventoryFailedEvent_5(((Action_1_t1116941607 *)CastclassSealed(L_2, Action_1_t1116941607_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void OpenIABEventManager::remove_queryInventoryFailedEvent(System.Action`1<System.String>)
extern Il2CppClass* OpenIABEventManager_t528054099_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t1116941607_il2cpp_TypeInfo_var;
extern const uint32_t OpenIABEventManager_remove_queryInventoryFailedEvent_m440677322_MetadataUsageId;
extern "C"  void OpenIABEventManager_remove_queryInventoryFailedEvent_m440677322 (Il2CppObject * __this /* static, unused */, Action_1_t1116941607 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIABEventManager_remove_queryInventoryFailedEvent_m440677322_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t1116941607 * L_0 = ((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->get_queryInventoryFailedEvent_5();
		Action_1_t1116941607 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->set_queryInventoryFailedEvent_5(((Action_1_t1116941607 *)CastclassSealed(L_2, Action_1_t1116941607_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void OpenIABEventManager::add_purchaseSucceededEvent(System.Action`1<OnePF.Purchase>)
extern Il2CppClass* OpenIABEventManager_t528054099_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t308648278_il2cpp_TypeInfo_var;
extern const uint32_t OpenIABEventManager_add_purchaseSucceededEvent_m3366236229_MetadataUsageId;
extern "C"  void OpenIABEventManager_add_purchaseSucceededEvent_m3366236229 (Il2CppObject * __this /* static, unused */, Action_1_t308648278 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIABEventManager_add_purchaseSucceededEvent_m3366236229_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t308648278 * L_0 = ((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->get_purchaseSucceededEvent_6();
		Action_1_t308648278 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->set_purchaseSucceededEvent_6(((Action_1_t308648278 *)CastclassSealed(L_2, Action_1_t308648278_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void OpenIABEventManager::remove_purchaseSucceededEvent(System.Action`1<OnePF.Purchase>)
extern Il2CppClass* OpenIABEventManager_t528054099_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t308648278_il2cpp_TypeInfo_var;
extern const uint32_t OpenIABEventManager_remove_purchaseSucceededEvent_m3933699974_MetadataUsageId;
extern "C"  void OpenIABEventManager_remove_purchaseSucceededEvent_m3933699974 (Il2CppObject * __this /* static, unused */, Action_1_t308648278 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIABEventManager_remove_purchaseSucceededEvent_m3933699974_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t308648278 * L_0 = ((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->get_purchaseSucceededEvent_6();
		Action_1_t308648278 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->set_purchaseSucceededEvent_6(((Action_1_t308648278 *)CastclassSealed(L_2, Action_1_t308648278_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void OpenIABEventManager::add_purchaseFailedEvent(System.Action`2<System.Int32,System.String>)
extern Il2CppClass* OpenIABEventManager_t528054099_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_2_t1740334453_il2cpp_TypeInfo_var;
extern const uint32_t OpenIABEventManager_add_purchaseFailedEvent_m1072578300_MetadataUsageId;
extern "C"  void OpenIABEventManager_add_purchaseFailedEvent_m1072578300 (Il2CppObject * __this /* static, unused */, Action_2_t1740334453 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIABEventManager_add_purchaseFailedEvent_m1072578300_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_2_t1740334453 * L_0 = ((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->get_purchaseFailedEvent_7();
		Action_2_t1740334453 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->set_purchaseFailedEvent_7(((Action_2_t1740334453 *)CastclassSealed(L_2, Action_2_t1740334453_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void OpenIABEventManager::remove_purchaseFailedEvent(System.Action`2<System.Int32,System.String>)
extern Il2CppClass* OpenIABEventManager_t528054099_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_2_t1740334453_il2cpp_TypeInfo_var;
extern const uint32_t OpenIABEventManager_remove_purchaseFailedEvent_m1937377499_MetadataUsageId;
extern "C"  void OpenIABEventManager_remove_purchaseFailedEvent_m1937377499 (Il2CppObject * __this /* static, unused */, Action_2_t1740334453 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIABEventManager_remove_purchaseFailedEvent_m1937377499_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_2_t1740334453 * L_0 = ((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->get_purchaseFailedEvent_7();
		Action_2_t1740334453 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->set_purchaseFailedEvent_7(((Action_2_t1740334453 *)CastclassSealed(L_2, Action_2_t1740334453_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void OpenIABEventManager::add_consumePurchaseSucceededEvent(System.Action`1<OnePF.Purchase>)
extern Il2CppClass* OpenIABEventManager_t528054099_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t308648278_il2cpp_TypeInfo_var;
extern const uint32_t OpenIABEventManager_add_consumePurchaseSucceededEvent_m890140291_MetadataUsageId;
extern "C"  void OpenIABEventManager_add_consumePurchaseSucceededEvent_m890140291 (Il2CppObject * __this /* static, unused */, Action_1_t308648278 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIABEventManager_add_consumePurchaseSucceededEvent_m890140291_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t308648278 * L_0 = ((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->get_consumePurchaseSucceededEvent_8();
		Action_1_t308648278 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->set_consumePurchaseSucceededEvent_8(((Action_1_t308648278 *)CastclassSealed(L_2, Action_1_t308648278_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void OpenIABEventManager::remove_consumePurchaseSucceededEvent(System.Action`1<OnePF.Purchase>)
extern Il2CppClass* OpenIABEventManager_t528054099_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t308648278_il2cpp_TypeInfo_var;
extern const uint32_t OpenIABEventManager_remove_consumePurchaseSucceededEvent_m913386530_MetadataUsageId;
extern "C"  void OpenIABEventManager_remove_consumePurchaseSucceededEvent_m913386530 (Il2CppObject * __this /* static, unused */, Action_1_t308648278 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIABEventManager_remove_consumePurchaseSucceededEvent_m913386530_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t308648278 * L_0 = ((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->get_consumePurchaseSucceededEvent_8();
		Action_1_t308648278 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->set_consumePurchaseSucceededEvent_8(((Action_1_t308648278 *)CastclassSealed(L_2, Action_1_t308648278_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void OpenIABEventManager::add_consumePurchaseFailedEvent(System.Action`1<System.String>)
extern Il2CppClass* OpenIABEventManager_t528054099_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t1116941607_il2cpp_TypeInfo_var;
extern const uint32_t OpenIABEventManager_add_consumePurchaseFailedEvent_m1880892058_MetadataUsageId;
extern "C"  void OpenIABEventManager_add_consumePurchaseFailedEvent_m1880892058 (Il2CppObject * __this /* static, unused */, Action_1_t1116941607 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIABEventManager_add_consumePurchaseFailedEvent_m1880892058_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t1116941607 * L_0 = ((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->get_consumePurchaseFailedEvent_9();
		Action_1_t1116941607 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->set_consumePurchaseFailedEvent_9(((Action_1_t1116941607 *)CastclassSealed(L_2, Action_1_t1116941607_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void OpenIABEventManager::remove_consumePurchaseFailedEvent(System.Action`1<System.String>)
extern Il2CppClass* OpenIABEventManager_t528054099_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t1116941607_il2cpp_TypeInfo_var;
extern const uint32_t OpenIABEventManager_remove_consumePurchaseFailedEvent_m2202042297_MetadataUsageId;
extern "C"  void OpenIABEventManager_remove_consumePurchaseFailedEvent_m2202042297 (Il2CppObject * __this /* static, unused */, Action_1_t1116941607 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIABEventManager_remove_consumePurchaseFailedEvent_m2202042297_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t1116941607 * L_0 = ((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->get_consumePurchaseFailedEvent_9();
		Action_1_t1116941607 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->set_consumePurchaseFailedEvent_9(((Action_1_t1116941607 *)CastclassSealed(L_2, Action_1_t1116941607_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void OpenIABEventManager::add_transactionRestoredEvent(System.Action`1<System.String>)
extern Il2CppClass* OpenIABEventManager_t528054099_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t1116941607_il2cpp_TypeInfo_var;
extern const uint32_t OpenIABEventManager_add_transactionRestoredEvent_m1251226944_MetadataUsageId;
extern "C"  void OpenIABEventManager_add_transactionRestoredEvent_m1251226944 (Il2CppObject * __this /* static, unused */, Action_1_t1116941607 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIABEventManager_add_transactionRestoredEvent_m1251226944_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t1116941607 * L_0 = ((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->get_transactionRestoredEvent_10();
		Action_1_t1116941607 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->set_transactionRestoredEvent_10(((Action_1_t1116941607 *)CastclassSealed(L_2, Action_1_t1116941607_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void OpenIABEventManager::remove_transactionRestoredEvent(System.Action`1<System.String>)
extern Il2CppClass* OpenIABEventManager_t528054099_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t1116941607_il2cpp_TypeInfo_var;
extern const uint32_t OpenIABEventManager_remove_transactionRestoredEvent_m1662733855_MetadataUsageId;
extern "C"  void OpenIABEventManager_remove_transactionRestoredEvent_m1662733855 (Il2CppObject * __this /* static, unused */, Action_1_t1116941607 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIABEventManager_remove_transactionRestoredEvent_m1662733855_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t1116941607 * L_0 = ((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->get_transactionRestoredEvent_10();
		Action_1_t1116941607 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->set_transactionRestoredEvent_10(((Action_1_t1116941607 *)CastclassSealed(L_2, Action_1_t1116941607_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void OpenIABEventManager::add_restoreFailedEvent(System.Action`1<System.String>)
extern Il2CppClass* OpenIABEventManager_t528054099_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t1116941607_il2cpp_TypeInfo_var;
extern const uint32_t OpenIABEventManager_add_restoreFailedEvent_m3227380105_MetadataUsageId;
extern "C"  void OpenIABEventManager_add_restoreFailedEvent_m3227380105 (Il2CppObject * __this /* static, unused */, Action_1_t1116941607 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIABEventManager_add_restoreFailedEvent_m3227380105_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t1116941607 * L_0 = ((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->get_restoreFailedEvent_11();
		Action_1_t1116941607 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->set_restoreFailedEvent_11(((Action_1_t1116941607 *)CastclassSealed(L_2, Action_1_t1116941607_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void OpenIABEventManager::remove_restoreFailedEvent(System.Action`1<System.String>)
extern Il2CppClass* OpenIABEventManager_t528054099_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t1116941607_il2cpp_TypeInfo_var;
extern const uint32_t OpenIABEventManager_remove_restoreFailedEvent_m2934232488_MetadataUsageId;
extern "C"  void OpenIABEventManager_remove_restoreFailedEvent_m2934232488 (Il2CppObject * __this /* static, unused */, Action_1_t1116941607 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIABEventManager_remove_restoreFailedEvent_m2934232488_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t1116941607 * L_0 = ((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->get_restoreFailedEvent_11();
		Action_1_t1116941607 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->set_restoreFailedEvent_11(((Action_1_t1116941607 *)CastclassSealed(L_2, Action_1_t1116941607_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void OpenIABEventManager::add_restoreSucceededEvent(System.Action)
extern Il2CppClass* OpenIABEventManager_t528054099_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t OpenIABEventManager_add_restoreSucceededEvent_m4043601618_MetadataUsageId;
extern "C"  void OpenIABEventManager_add_restoreSucceededEvent_m4043601618 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIABEventManager_add_restoreSucceededEvent_m4043601618_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t437523947 * L_0 = ((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->get_restoreSucceededEvent_12();
		Action_t437523947 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->set_restoreSucceededEvent_12(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void OpenIABEventManager::remove_restoreSucceededEvent(System.Action)
extern Il2CppClass* OpenIABEventManager_t528054099_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern const uint32_t OpenIABEventManager_remove_restoreSucceededEvent_m4231230769_MetadataUsageId;
extern "C"  void OpenIABEventManager_remove_restoreSucceededEvent_m4231230769 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIABEventManager_remove_restoreSucceededEvent_m4231230769_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t437523947 * L_0 = ((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->get_restoreSucceededEvent_12();
		Action_t437523947 * L_1 = ___value0;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->set_restoreSucceededEvent_12(((Action_t437523947 *)CastclassSealed(L_2, Action_t437523947_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void OpenIABEventManager::Awake()
extern "C"  void OpenIABEventManager_Awake_m3664406439 (OpenIABEventManager_t528054099 * __this, const MethodInfo* method)
{
	{
		GameObject_t4012695102 * L_0 = Component_get_gameObject_m2112202034(__this, /*hidden argument*/NULL);
		Type_t * L_1 = Object_GetType_m2022236990(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, L_1);
		NullCheck(L_0);
		Object_set_name_m2334706817(L_0, L_2, /*hidden argument*/NULL);
		Object_DontDestroyOnLoad_m2587754829(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OpenIABEventManager::OnBillingSupported(System.String)
extern Il2CppClass* OpenIABEventManager_t528054099_il2cpp_TypeInfo_var;
extern const uint32_t OpenIABEventManager_OnBillingSupported_m1355956240_MetadataUsageId;
extern "C"  void OpenIABEventManager_OnBillingSupported_m1355956240 (OpenIABEventManager_t528054099 * __this, String_t* ___empty0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIABEventManager_OnBillingSupported_m1355956240_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t437523947 * L_0 = ((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->get_billingSupportedEvent_2();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		Action_t437523947 * L_1 = ((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->get_billingSupportedEvent_2();
		NullCheck(L_1);
		Action_Invoke_m1445970038(L_1, /*hidden argument*/NULL);
	}

IL_0014:
	{
		return;
	}
}
// System.Void OpenIABEventManager::OnBillingNotSupported(System.String)
extern Il2CppClass* OpenIABEventManager_t528054099_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m518637026_MethodInfo_var;
extern const uint32_t OpenIABEventManager_OnBillingNotSupported_m173291305_MetadataUsageId;
extern "C"  void OpenIABEventManager_OnBillingNotSupported_m173291305 (OpenIABEventManager_t528054099 * __this, String_t* ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIABEventManager_OnBillingNotSupported_m173291305_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t1116941607 * L_0 = ((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->get_billingNotSupportedEvent_3();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		Action_1_t1116941607 * L_1 = ((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->get_billingNotSupportedEvent_3();
		String_t* L_2 = ___error0;
		NullCheck(L_1);
		Action_1_Invoke_m518637026(L_1, L_2, /*hidden argument*/Action_1_Invoke_m518637026_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
// System.Void OpenIABEventManager::OnQueryInventorySucceeded(System.String)
extern Il2CppClass* OpenIABEventManager_t528054099_il2cpp_TypeInfo_var;
extern Il2CppClass* Inventory_t2630562832_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1524425160_MethodInfo_var;
extern const uint32_t OpenIABEventManager_OnQueryInventorySucceeded_m2647396658_MetadataUsageId;
extern "C"  void OpenIABEventManager_OnQueryInventorySucceeded_m2647396658 (OpenIABEventManager_t528054099 * __this, String_t* ___json0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIABEventManager_OnQueryInventorySucceeded_m2647396658_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Inventory_t2630562832 * V_0 = NULL;
	{
		Action_1_t2779015537 * L_0 = ((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->get_queryInventorySucceededEvent_4();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = ___json0;
		Inventory_t2630562832 * L_2 = (Inventory_t2630562832 *)il2cpp_codegen_object_new(Inventory_t2630562832_il2cpp_TypeInfo_var);
		Inventory__ctor_m529083189(L_2, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Action_1_t2779015537 * L_3 = ((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->get_queryInventorySucceededEvent_4();
		Inventory_t2630562832 * L_4 = V_0;
		NullCheck(L_3);
		Action_1_Invoke_m1524425160(L_3, L_4, /*hidden argument*/Action_1_Invoke_m1524425160_MethodInfo_var);
	}

IL_001c:
	{
		return;
	}
}
// System.Void OpenIABEventManager::OnQueryInventoryFailed(System.String)
extern Il2CppClass* OpenIABEventManager_t528054099_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m518637026_MethodInfo_var;
extern const uint32_t OpenIABEventManager_OnQueryInventoryFailed_m177395954_MetadataUsageId;
extern "C"  void OpenIABEventManager_OnQueryInventoryFailed_m177395954 (OpenIABEventManager_t528054099 * __this, String_t* ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIABEventManager_OnQueryInventoryFailed_m177395954_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t1116941607 * L_0 = ((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->get_queryInventoryFailedEvent_5();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		Action_1_t1116941607 * L_1 = ((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->get_queryInventoryFailedEvent_5();
		String_t* L_2 = ___error0;
		NullCheck(L_1);
		Action_1_Invoke_m518637026(L_1, L_2, /*hidden argument*/Action_1_Invoke_m518637026_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
// System.Void OpenIABEventManager::OnPurchaseSucceeded(System.String)
extern Il2CppClass* OpenIABEventManager_t528054099_il2cpp_TypeInfo_var;
extern Il2CppClass* Purchase_t160195573_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m4112356107_MethodInfo_var;
extern const uint32_t OpenIABEventManager_OnPurchaseSucceeded_m721466431_MetadataUsageId;
extern "C"  void OpenIABEventManager_OnPurchaseSucceeded_m721466431 (OpenIABEventManager_t528054099 * __this, String_t* ___json0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIABEventManager_OnPurchaseSucceeded_m721466431_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t308648278 * L_0 = ((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->get_purchaseSucceededEvent_6();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		Action_1_t308648278 * L_1 = ((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->get_purchaseSucceededEvent_6();
		String_t* L_2 = ___json0;
		Purchase_t160195573 * L_3 = (Purchase_t160195573 *)il2cpp_codegen_object_new(Purchase_t160195573_il2cpp_TypeInfo_var);
		Purchase__ctor_m3455340262(L_3, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		Action_1_Invoke_m4112356107(L_1, L_3, /*hidden argument*/Action_1_Invoke_m4112356107_MethodInfo_var);
	}

IL_001a:
	{
		return;
	}
}
// System.Void OpenIABEventManager::OnPurchaseFailed(System.String)
extern Il2CppClass* OpenIABEventManager_t528054099_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_2_Invoke_m2349932106_MethodInfo_var;
extern const uint32_t OpenIABEventManager_OnPurchaseFailed_m1827068165_MetadataUsageId;
extern "C"  void OpenIABEventManager_OnPurchaseFailed_m1827068165 (OpenIABEventManager_t528054099 * __this, String_t* ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIABEventManager_OnPurchaseFailed_m1827068165_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_2_t1740334453 * L_0 = ((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->get_purchaseFailedEvent_7();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Action_2_t1740334453 * L_1 = ((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->get_purchaseFailedEvent_7();
		String_t* L_2 = ___error0;
		NullCheck(L_1);
		Action_2_Invoke_m2349932106(L_1, (-1), L_2, /*hidden argument*/Action_2_Invoke_m2349932106_MethodInfo_var);
	}

IL_0016:
	{
		return;
	}
}
// System.Void OpenIABEventManager::OnConsumePurchaseSucceeded(System.String)
extern Il2CppClass* OpenIABEventManager_t528054099_il2cpp_TypeInfo_var;
extern Il2CppClass* Purchase_t160195573_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m4112356107_MethodInfo_var;
extern const uint32_t OpenIABEventManager_OnConsumePurchaseSucceeded_m1419424927_MetadataUsageId;
extern "C"  void OpenIABEventManager_OnConsumePurchaseSucceeded_m1419424927 (OpenIABEventManager_t528054099 * __this, String_t* ___json0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIABEventManager_OnConsumePurchaseSucceeded_m1419424927_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t308648278 * L_0 = ((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->get_consumePurchaseSucceededEvent_8();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		Action_1_t308648278 * L_1 = ((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->get_consumePurchaseSucceededEvent_8();
		String_t* L_2 = ___json0;
		Purchase_t160195573 * L_3 = (Purchase_t160195573 *)il2cpp_codegen_object_new(Purchase_t160195573_il2cpp_TypeInfo_var);
		Purchase__ctor_m3455340262(L_3, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		Action_1_Invoke_m4112356107(L_1, L_3, /*hidden argument*/Action_1_Invoke_m4112356107_MethodInfo_var);
	}

IL_001a:
	{
		return;
	}
}
// System.Void OpenIABEventManager::OnConsumePurchaseFailed(System.String)
extern Il2CppClass* OpenIABEventManager_t528054099_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m518637026_MethodInfo_var;
extern const uint32_t OpenIABEventManager_OnConsumePurchaseFailed_m497267877_MetadataUsageId;
extern "C"  void OpenIABEventManager_OnConsumePurchaseFailed_m497267877 (OpenIABEventManager_t528054099 * __this, String_t* ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIABEventManager_OnConsumePurchaseFailed_m497267877_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t1116941607 * L_0 = ((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->get_consumePurchaseFailedEvent_9();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		Action_1_t1116941607 * L_1 = ((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->get_consumePurchaseFailedEvent_9();
		String_t* L_2 = ___error0;
		NullCheck(L_1);
		Action_1_Invoke_m518637026(L_1, L_2, /*hidden argument*/Action_1_Invoke_m518637026_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
// System.Void OpenIABEventManager::OnPurchaseRestored(System.String)
extern Il2CppClass* OpenIABEventManager_t528054099_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m518637026_MethodInfo_var;
extern const uint32_t OpenIABEventManager_OnPurchaseRestored_m1685775436_MetadataUsageId;
extern "C"  void OpenIABEventManager_OnPurchaseRestored_m1685775436 (OpenIABEventManager_t528054099 * __this, String_t* ___sku0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIABEventManager_OnPurchaseRestored_m1685775436_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t1116941607 * L_0 = ((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->get_transactionRestoredEvent_10();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		Action_1_t1116941607 * L_1 = ((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->get_transactionRestoredEvent_10();
		String_t* L_2 = ___sku0;
		NullCheck(L_1);
		Action_1_Invoke_m518637026(L_1, L_2, /*hidden argument*/Action_1_Invoke_m518637026_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
// System.Void OpenIABEventManager::OnRestoreFailed(System.String)
extern Il2CppClass* OpenIABEventManager_t528054099_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m518637026_MethodInfo_var;
extern const uint32_t OpenIABEventManager_OnRestoreFailed_m244541908_MetadataUsageId;
extern "C"  void OpenIABEventManager_OnRestoreFailed_m244541908 (OpenIABEventManager_t528054099 * __this, String_t* ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIABEventManager_OnRestoreFailed_m244541908_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t1116941607 * L_0 = ((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->get_restoreFailedEvent_11();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		Action_1_t1116941607 * L_1 = ((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->get_restoreFailedEvent_11();
		String_t* L_2 = ___error0;
		NullCheck(L_1);
		Action_1_Invoke_m518637026(L_1, L_2, /*hidden argument*/Action_1_Invoke_m518637026_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
// System.Void OpenIABEventManager::OnRestoreFinished(System.String)
extern Il2CppClass* OpenIABEventManager_t528054099_il2cpp_TypeInfo_var;
extern const uint32_t OpenIABEventManager_OnRestoreFinished_m1125116159_MetadataUsageId;
extern "C"  void OpenIABEventManager_OnRestoreFinished_m1125116159 (OpenIABEventManager_t528054099 * __this, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OpenIABEventManager_OnRestoreFinished_m1125116159_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t437523947 * L_0 = ((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->get_restoreSucceededEvent_12();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		Action_t437523947 * L_1 = ((OpenIABEventManager_t528054099_StaticFields*)OpenIABEventManager_t528054099_il2cpp_TypeInfo_var->static_fields)->get_restoreSucceededEvent_12();
		NullCheck(L_1);
		Action_Invoke_m1445970038(L_1, /*hidden argument*/NULL);
	}

IL_0014:
	{
		return;
	}
}
// System.Void PlayFab.CallRequestContainer::.ctor()
extern "C"  void CallRequestContainer__ctor_m751774308 (CallRequestContainer_t432318609 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayFab.CallRequestContainer::InvokeCallback()
extern const MethodInfo* Action_1_Invoke_m3002653267_MethodInfo_var;
extern const uint32_t CallRequestContainer_InvokeCallback_m3267545405_MetadataUsageId;
extern "C"  void CallRequestContainer_InvokeCallback_m3267545405 (CallRequestContainer_t432318609 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CallRequestContainer_InvokeCallback_m3267545405_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t580771314 * L_0 = __this->get_Callback_12();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Action_1_t580771314 * L_1 = __this->get_Callback_12();
		NullCheck(L_1);
		Action_1_Invoke_m3002653267(L_1, __this, /*hidden argument*/Action_1_Invoke_m3002653267_MethodInfo_var);
	}

IL_0017:
	{
		return;
	}
}
// System.Void PlayFab.ErrorCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void ErrorCallback__ctor_m2874342461 (ErrorCallback_t582108558 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void PlayFab.ErrorCallback::Invoke(PlayFab.PlayFabError)
extern "C"  void ErrorCallback_Invoke_m2764045351 (ErrorCallback_t582108558 * __this, PlayFabError_t750598646 * ___error0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ErrorCallback_Invoke_m2764045351((ErrorCallback_t582108558 *)__this->get_prev_9(),___error0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, PlayFabError_t750598646 * ___error0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___error0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, PlayFabError_t750598646 * ___error0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___error0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___error0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C" void pinvoke_delegate_wrapper_ErrorCallback_t582108558(Il2CppObject* delegate, PlayFabError_t750598646 * ___error0)
{
	// Marshaling of parameter '___error0' to native representation
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'PlayFab.PlayFabError'."));
}
// System.IAsyncResult PlayFab.ErrorCallback::BeginInvoke(PlayFab.PlayFabError,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ErrorCallback_BeginInvoke_m2638290106 (ErrorCallback_t582108558 * __this, PlayFabError_t750598646 * ___error0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___error0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void PlayFab.ErrorCallback::EndInvoke(System.IAsyncResult)
extern "C"  void ErrorCallback_EndInvoke_m2220889293 (ErrorCallback_t582108558 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void PlayFab.Internal.PlayFabPluginEventHandler::.ctor()
extern "C"  void PlayFabPluginEventHandler__ctor_m1745091279 (PlayFabPluginEventHandler_t3351841502 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m350695606(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayFab.Internal.PlayFabPluginEventHandler::.cctor()
extern Il2CppClass* Dictionary_2_t923129392_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayFabPluginEventHandler_t3351841502_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1746749140_MethodInfo_var;
extern const uint32_t PlayFabPluginEventHandler__cctor_m2076125886_MetadataUsageId;
extern "C"  void PlayFabPluginEventHandler__cctor_m2076125886 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayFabPluginEventHandler__cctor_m2076125886_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t923129392 * L_0 = (Dictionary_2_t923129392 *)il2cpp_codegen_object_new(Dictionary_2_t923129392_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1746749140(L_0, /*hidden argument*/Dictionary_2__ctor_m1746749140_MethodInfo_var);
		((PlayFabPluginEventHandler_t3351841502_StaticFields*)PlayFabPluginEventHandler_t3351841502_il2cpp_TypeInfo_var->static_fields)->set_HttpHandlers_3(L_0);
		return;
	}
}
// System.Void PlayFab.Internal.PlayFabPluginEventHandler::Init()
extern Il2CppClass* PlayFabPluginEventHandler_t3351841502_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t4012695102_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisPlayFabPluginEventHandler_t3351841502_m567520681_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisPlayFabPluginEventHandler_t3351841502_m2605444094_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2908843452;
extern const uint32_t PlayFabPluginEventHandler_Init_m1116208549_MetadataUsageId;
extern "C"  void PlayFabPluginEventHandler_Init_m1116208549 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayFabPluginEventHandler_Init_m1116208549_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t4012695102 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayFabPluginEventHandler_t3351841502_il2cpp_TypeInfo_var);
		PlayFabPluginEventHandler_t3351841502 * L_0 = ((PlayFabPluginEventHandler_t3351841502_StaticFields*)PlayFabPluginEventHandler_t3351841502_il2cpp_TypeInfo_var->static_fields)->get__playFabEvtHandler_2();
		bool L_1 = Object_op_Inequality_m1834679180(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		GameObject_t4012695102 * L_2 = GameObject_Find_m1853568733(NULL /*static, unused*/, _stringLiteral2908843452, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t4012695102 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m1690293457(NULL /*static, unused*/, L_3, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0033;
		}
	}
	{
		GameObject_t4012695102 * L_5 = (GameObject_t4012695102 *)il2cpp_codegen_object_new(GameObject_t4012695102_il2cpp_TypeInfo_var);
		GameObject__ctor_m1336994365(L_5, _stringLiteral2908843452, /*hidden argument*/NULL);
		V_0 = L_5;
	}

IL_0033:
	{
		GameObject_t4012695102 * L_6 = V_0;
		Object_DontDestroyOnLoad_m2587754829(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_7 = V_0;
		NullCheck(L_7);
		PlayFabPluginEventHandler_t3351841502 * L_8 = GameObject_GetComponent_TisPlayFabPluginEventHandler_t3351841502_m567520681(L_7, /*hidden argument*/GameObject_GetComponent_TisPlayFabPluginEventHandler_t3351841502_m567520681_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(PlayFabPluginEventHandler_t3351841502_il2cpp_TypeInfo_var);
		((PlayFabPluginEventHandler_t3351841502_StaticFields*)PlayFabPluginEventHandler_t3351841502_il2cpp_TypeInfo_var->static_fields)->set__playFabEvtHandler_2(L_8);
		PlayFabPluginEventHandler_t3351841502 * L_9 = ((PlayFabPluginEventHandler_t3351841502_StaticFields*)PlayFabPluginEventHandler_t3351841502_il2cpp_TypeInfo_var->static_fields)->get__playFabEvtHandler_2();
		bool L_10 = Object_op_Equality_m1690293457(NULL /*static, unused*/, L_9, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_005f;
		}
	}
	{
		GameObject_t4012695102 * L_11 = V_0;
		NullCheck(L_11);
		PlayFabPluginEventHandler_t3351841502 * L_12 = GameObject_AddComponent_TisPlayFabPluginEventHandler_t3351841502_m2605444094(L_11, /*hidden argument*/GameObject_AddComponent_TisPlayFabPluginEventHandler_t3351841502_m2605444094_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(PlayFabPluginEventHandler_t3351841502_il2cpp_TypeInfo_var);
		((PlayFabPluginEventHandler_t3351841502_StaticFields*)PlayFabPluginEventHandler_t3351841502_il2cpp_TypeInfo_var->static_fields)->set__playFabEvtHandler_2(L_12);
	}

IL_005f:
	{
		return;
	}
}
// System.Void PlayFab.Internal.PlayFabPluginEventHandler::GCMRegistrationReady(System.String)
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern const uint32_t PlayFabPluginEventHandler_GCMRegistrationReady_m705651220_MetadataUsageId;
extern "C"  void PlayFabPluginEventHandler_GCMRegistrationReady_m705651220 (PlayFabPluginEventHandler_t3351841502 * __this, String_t* ___status0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayFabPluginEventHandler_GCMRegistrationReady_m705651220_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		String_t* L_0 = ___status0;
		IL2CPP_RUNTIME_CLASS_INIT(Boolean_t211005341_il2cpp_TypeInfo_var);
		Boolean_TryParse_m4263405916(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayFab.Internal.PlayFabPluginEventHandler::GCMRegistered(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t PlayFabPluginEventHandler_GCMRegistered_m2666134370_MetadataUsageId;
extern "C"  void PlayFabPluginEventHandler_GCMRegistered_m2666134370 (PlayFabPluginEventHandler_t3351841502 * __this, String_t* ___token0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayFabPluginEventHandler_GCMRegistered_m2666134370_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* G_B3_0 = NULL;
	{
		String_t* L_0 = ___token0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		String_t* L_2 = ___token0;
		G_B3_0 = L_2;
		goto IL_0012;
	}

IL_0011:
	{
		G_B3_0 = ((String_t*)(NULL));
	}

IL_0012:
	{
		V_0 = G_B3_0;
		return;
	}
}
// System.Void PlayFab.Internal.PlayFabPluginEventHandler::GCMRegisterError(System.String)
extern "C"  void PlayFabPluginEventHandler_GCMRegisterError_m3763254905 (PlayFabPluginEventHandler_t3351841502 * __this, String_t* ___error0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void PlayFab.Internal.PlayFabPluginEventHandler::GCMMessageReceived(System.String)
extern "C"  void PlayFabPluginEventHandler_GCMMessageReceived_m2076682998 (PlayFabPluginEventHandler_t3351841502 * __this, String_t* ___message0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void PlayFab.Internal.PlayFabPluginEventHandler::AddHttpDelegate(PlayFab.CallRequestContainer)
extern Il2CppClass* PlayFabPluginEventHandler_t3351841502_il2cpp_TypeInfo_var;
extern const uint32_t PlayFabPluginEventHandler_AddHttpDelegate_m591552496_MetadataUsageId;
extern "C"  void PlayFabPluginEventHandler_AddHttpDelegate_m591552496 (Il2CppObject * __this /* static, unused */, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayFabPluginEventHandler_AddHttpDelegate_m591552496_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayFabPluginEventHandler_t3351841502_il2cpp_TypeInfo_var);
		PlayFabPluginEventHandler_Init_m1116208549(NULL /*static, unused*/, /*hidden argument*/NULL);
		Dictionary_2_t923129392 * L_0 = ((PlayFabPluginEventHandler_t3351841502_StaticFields*)PlayFabPluginEventHandler_t3351841502_il2cpp_TypeInfo_var->static_fields)->get_HttpHandlers_3();
		CallRequestContainer_t432318609 * L_1 = ___requestContainer0;
		NullCheck(L_1);
		int32_t L_2 = L_1->get_CallId_3();
		CallRequestContainer_t432318609 * L_3 = ___requestContainer0;
		NullCheck(L_0);
		VirtActionInvoker2< int32_t, CallRequestContainer_t432318609 * >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.Int32,PlayFab.CallRequestContainer>::Add(!0,!1) */, L_0, L_2, L_3);
		return;
	}
}
// System.Void PlayFab.Internal.PlayFabPluginEventHandler::OnHttpError(System.String)
extern Il2CppClass* PlayFabPluginEventHandler_t3351841502_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayFabError_t750598646_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral58;
extern Il2CppCodeGenString* _stringLiteral3497358675;
extern Il2CppCodeGenString* _stringLiteral49586;
extern Il2CppCodeGenString* _stringLiteral2958788249;
extern const uint32_t PlayFabPluginEventHandler_OnHttpError_m109204436_MetadataUsageId;
extern "C"  void PlayFabPluginEventHandler_OnHttpError_m109204436 (PlayFabPluginEventHandler_t3351841502 * __this, String_t* ___response0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayFabPluginEventHandler_OnHttpError_m109204436_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t2956870243* V_0 = NULL;
	int32_t V_1 = 0;
	CallRequestContainer_t432318609 * V_2 = NULL;
	Exception_t1967233988 * V_3 = NULL;
	PlayFabError_t750598646 * V_4 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_0 = ___response0;
			NullCheck(_stringLiteral58);
			CharU5BU5D_t3416858730* L_1 = String_ToCharArray_m1208288742(_stringLiteral58, /*hidden argument*/NULL);
			NullCheck(L_0);
			StringU5BU5D_t2956870243* L_2 = String_Split_m434660345(L_0, L_1, 2, /*hidden argument*/NULL);
			V_0 = L_2;
			StringU5BU5D_t2956870243* L_3 = V_0;
			NullCheck(L_3);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
			int32_t L_4 = 0;
			int32_t L_5 = Int32_Parse_m3837759498(NULL /*static, unused*/, ((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), /*hidden argument*/NULL);
			V_1 = L_5;
			IL2CPP_RUNTIME_CLASS_INIT(PlayFabPluginEventHandler_t3351841502_il2cpp_TypeInfo_var);
			Dictionary_2_t923129392 * L_6 = ((PlayFabPluginEventHandler_t3351841502_StaticFields*)PlayFabPluginEventHandler_t3351841502_il2cpp_TypeInfo_var->static_fields)->get_HttpHandlers_3();
			int32_t L_7 = V_1;
			NullCheck(L_6);
			bool L_8 = VirtFuncInvoker2< bool, int32_t, CallRequestContainer_t432318609 ** >::Invoke(21 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,PlayFab.CallRequestContainer>::TryGetValue(!0,!1&) */, L_6, L_7, (&V_2));
			if (L_8)
			{
				goto IL_0047;
			}
		}

IL_002d:
		{
			StringU5BU5D_t2956870243* L_9 = V_0;
			NullCheck(L_9);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
			int32_t L_10 = 0;
			StringU5BU5D_t2956870243* L_11 = V_0;
			NullCheck(L_11);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 1);
			int32_t L_12 = 1;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_13 = String_Format_m2398979370(NULL /*static, unused*/, _stringLiteral3497358675, ((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_10))), ((L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12))), /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
			Debug_LogWarning_m539478453(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
			goto IL_0099;
		}

IL_0047:
		{
			CallRequestContainer_t432318609 * L_14 = V_2;
			PlayFabError_t750598646 * L_15 = (PlayFabError_t750598646 *)il2cpp_codegen_object_new(PlayFabError_t750598646_il2cpp_TypeInfo_var);
			PlayFabError__ctor_m1421089695(L_15, /*hidden argument*/NULL);
			V_4 = L_15;
			PlayFabError_t750598646 * L_16 = V_4;
			NullCheck(L_16);
			L_16->set_HttpStatus_1(_stringLiteral49586);
			PlayFabError_t750598646 * L_17 = V_4;
			StringU5BU5D_t2956870243* L_18 = V_0;
			NullCheck(L_18);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 1);
			int32_t L_19 = 1;
			NullCheck(L_17);
			L_17->set_ErrorMessage_3(((L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19))));
			PlayFabError_t750598646 * L_20 = V_4;
			NullCheck(L_14);
			L_14->set_Error_11(L_20);
			CallRequestContainer_t432318609 * L_21 = V_2;
			NullCheck(L_21);
			CallRequestContainer_InvokeCallback_m3267545405(L_21, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(PlayFabPluginEventHandler_t3351841502_il2cpp_TypeInfo_var);
			Dictionary_2_t923129392 * L_22 = ((PlayFabPluginEventHandler_t3351841502_StaticFields*)PlayFabPluginEventHandler_t3351841502_il2cpp_TypeInfo_var->static_fields)->get_HttpHandlers_3();
			int32_t L_23 = V_1;
			NullCheck(L_22);
			VirtFuncInvoker1< bool, int32_t >::Invoke(20 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,PlayFab.CallRequestContainer>::Remove(!0) */, L_22, L_23);
			goto IL_0099;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0083;
		throw e;
	}

CATCH_0083:
	{ // begin catch(System.Exception)
		V_3 = ((Exception_t1967233988 *)__exception_local);
		Exception_t1967233988 * L_24 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_25 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral2958788249, L_24, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m1836388681(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		goto IL_0099;
	} // end catch (depth: 1)

IL_0099:
	{
		return;
	}
}
// System.Void PlayFab.Internal.PlayFabPluginEventHandler::OnHttpResponse(System.String)
extern Il2CppClass* PlayFabPluginEventHandler_t3351841502_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral58;
extern Il2CppCodeGenString* _stringLiteral589119333;
extern Il2CppCodeGenString* _stringLiteral2783400640;
extern const uint32_t PlayFabPluginEventHandler_OnHttpResponse_m1170359333_MetadataUsageId;
extern "C"  void PlayFabPluginEventHandler_OnHttpResponse_m1170359333 (PlayFabPluginEventHandler_t3351841502 * __this, String_t* ___response0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayFabPluginEventHandler_OnHttpResponse_m1170359333_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t2956870243* V_0 = NULL;
	int32_t V_1 = 0;
	CallRequestContainer_t432318609 * V_2 = NULL;
	Exception_t1967233988 * V_3 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_0 = ___response0;
			NullCheck(_stringLiteral58);
			CharU5BU5D_t3416858730* L_1 = String_ToCharArray_m1208288742(_stringLiteral58, /*hidden argument*/NULL);
			NullCheck(L_0);
			StringU5BU5D_t2956870243* L_2 = String_Split_m434660345(L_0, L_1, 2, /*hidden argument*/NULL);
			V_0 = L_2;
			StringU5BU5D_t2956870243* L_3 = V_0;
			NullCheck(L_3);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
			int32_t L_4 = 0;
			int32_t L_5 = Int32_Parse_m3837759498(NULL /*static, unused*/, ((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), /*hidden argument*/NULL);
			V_1 = L_5;
			IL2CPP_RUNTIME_CLASS_INIT(PlayFabPluginEventHandler_t3351841502_il2cpp_TypeInfo_var);
			Dictionary_2_t923129392 * L_6 = ((PlayFabPluginEventHandler_t3351841502_StaticFields*)PlayFabPluginEventHandler_t3351841502_il2cpp_TypeInfo_var->static_fields)->get_HttpHandlers_3();
			int32_t L_7 = V_1;
			NullCheck(L_6);
			bool L_8 = VirtFuncInvoker2< bool, int32_t, CallRequestContainer_t432318609 ** >::Invoke(21 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,PlayFab.CallRequestContainer>::TryGetValue(!0,!1&) */, L_6, L_7, (&V_2));
			if (L_8)
			{
				goto IL_0047;
			}
		}

IL_002d:
		{
			StringU5BU5D_t2956870243* L_9 = V_0;
			NullCheck(L_9);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
			int32_t L_10 = 0;
			StringU5BU5D_t2956870243* L_11 = V_0;
			NullCheck(L_11);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 1);
			int32_t L_12 = 1;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_13 = String_Format_m2398979370(NULL /*static, unused*/, _stringLiteral589119333, ((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_10))), ((L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12))), /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
			Debug_LogWarning_m539478453(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
			goto IL_007d;
		}

IL_0047:
		{
			CallRequestContainer_t432318609 * L_14 = V_2;
			StringU5BU5D_t2956870243* L_15 = V_0;
			NullCheck(L_15);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 1);
			int32_t L_16 = 1;
			NullCheck(L_14);
			L_14->set_ResultStr_8(((L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_16))));
			CallRequestContainer_t432318609 * L_17 = V_2;
			NullCheck(L_17);
			CallRequestContainer_InvokeCallback_m3267545405(L_17, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(PlayFabPluginEventHandler_t3351841502_il2cpp_TypeInfo_var);
			Dictionary_2_t923129392 * L_18 = ((PlayFabPluginEventHandler_t3351841502_StaticFields*)PlayFabPluginEventHandler_t3351841502_il2cpp_TypeInfo_var->static_fields)->get_HttpHandlers_3();
			int32_t L_19 = V_1;
			NullCheck(L_18);
			VirtFuncInvoker1< bool, int32_t >::Invoke(20 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,PlayFab.CallRequestContainer>::Remove(!0) */, L_18, L_19);
			goto IL_007d;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0067;
		throw e;
	}

CATCH_0067:
	{ // begin catch(System.Exception)
		V_3 = ((Exception_t1967233988 *)__exception_local);
		Exception_t1967233988 * L_20 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral2783400640, L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m1836388681(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		goto IL_007d;
	} // end catch (depth: 1)

IL_007d:
	{
		return;
	}
}
// System.Void PlayFab.PlayFabAndroidPlugin::.ctor()
extern "C"  void PlayFabAndroidPlugin__ctor_m1466085477 (PlayFabAndroidPlugin_t314518896 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayFab.PlayFabAndroidPlugin::.cctor()
extern "C"  void PlayFabAndroidPlugin__cctor_m2016880616 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Boolean PlayFab.PlayFabAndroidPlugin::IsAvailable()
extern "C"  bool PlayFabAndroidPlugin_IsAvailable_m3443727846 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void PlayFab.PlayFabAndroidPlugin::Init(System.String)
extern Il2CppClass* PlayFabAndroidPlugin_t314518896_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayFabPluginEventHandler_t3351841502_il2cpp_TypeInfo_var;
extern const uint32_t PlayFabAndroidPlugin_Init_m573356563_MetadataUsageId;
extern "C"  void PlayFabAndroidPlugin_Init_m573356563 (Il2CppObject * __this /* static, unused */, String_t* ___SenderID0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayFabAndroidPlugin_Init_m573356563_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayFabAndroidPlugin_t314518896_il2cpp_TypeInfo_var);
		bool L_0 = ((PlayFabAndroidPlugin_t314518896_StaticFields*)PlayFabAndroidPlugin_t314518896_il2cpp_TypeInfo_var->static_fields)->get_Initted_0();
		if (!L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayFabPluginEventHandler_t3351841502_il2cpp_TypeInfo_var);
		PlayFabPluginEventHandler_Init_m1116208549(NULL /*static, unused*/, /*hidden argument*/NULL);
		PlayFabGoogleCloudMessaging_Init_m1360790909(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayFabAndroidPlugin_t314518896_il2cpp_TypeInfo_var);
		((PlayFabAndroidPlugin_t314518896_StaticFields*)PlayFabAndroidPlugin_t314518896_il2cpp_TypeInfo_var->static_fields)->set_Initted_0((bool)1);
		return;
	}
}
// System.Boolean PlayFab.PlayFabAndroidPlugin::IsPlayServicesAvailable()
extern "C"  bool PlayFabAndroidPlugin_IsPlayServicesAvailable_m2306452692 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void PlayFab.PlayFabAndroidPlugin::UpdateRouting(System.Boolean)
extern "C"  void PlayFabAndroidPlugin_UpdateRouting_m3838781815 (Il2CppObject * __this /* static, unused */, bool ___routeToNotificationArea0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void PlayFab.PlayFabError::.ctor()
extern "C"  void PlayFabError__ctor_m1421089695 (PlayFabError_t750598646 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayFab.PlayFabGoogleCloudMessaging::.ctor()
extern "C"  void PlayFabGoogleCloudMessaging__ctor_m737209847 (PlayFabGoogleCloudMessaging_t2415988188 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayFab.PlayFabGoogleCloudMessaging::Init()
extern "C"  void PlayFabGoogleCloudMessaging_Init_m1360790909 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.String PlayFab.PlayFabGoogleCloudMessaging::GetToken()
extern "C"  String_t* PlayFabGoogleCloudMessaging_GetToken_m1051161709 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return (String_t*)NULL;
	}
}
// System.String PlayFab.PlayFabGoogleCloudMessaging::GetPushCacheData()
extern "C"  String_t* PlayFabGoogleCloudMessaging_GetPushCacheData_m3210769798 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return (String_t*)NULL;
	}
}
// PlayFabNotificationPackage PlayFab.PlayFabGoogleCloudMessaging::GetPushCache()
extern Il2CppClass* PlayFabNotificationPackage_t2037342920_il2cpp_TypeInfo_var;
extern const uint32_t PlayFabGoogleCloudMessaging_GetPushCache_m817942018_MetadataUsageId;
extern "C"  PlayFabNotificationPackage_t2037342920  PlayFabGoogleCloudMessaging_GetPushCache_m817942018 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayFabGoogleCloudMessaging_GetPushCache_m817942018_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	PlayFabNotificationPackage_t2037342920  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (PlayFabNotificationPackage_t2037342920_il2cpp_TypeInfo_var, (&V_0));
		PlayFabNotificationPackage_t2037342920  L_0 = V_0;
		return L_0;
	}
}
// System.Void PlayFab.PlayFabGoogleCloudMessaging::RegistrationReady(System.Boolean)
extern Il2CppClass* PlayFabGoogleCloudMessaging_t2415988188_il2cpp_TypeInfo_var;
extern const uint32_t PlayFabGoogleCloudMessaging_RegistrationReady_m4275567158_MetadataUsageId;
extern "C"  void PlayFabGoogleCloudMessaging_RegistrationReady_m4275567158 (Il2CppObject * __this /* static, unused */, bool ___status0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayFabGoogleCloudMessaging_RegistrationReady_m4275567158_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GCMRegisterReady_t3751777615 * L_0 = ((PlayFabGoogleCloudMessaging_t2415988188_StaticFields*)PlayFabGoogleCloudMessaging_t2415988188_il2cpp_TypeInfo_var->static_fields)->get__RegistrationReadyCallback_0();
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		GCMRegisterReady_t3751777615 * L_1 = ((PlayFabGoogleCloudMessaging_t2415988188_StaticFields*)PlayFabGoogleCloudMessaging_t2415988188_il2cpp_TypeInfo_var->static_fields)->get__RegistrationReadyCallback_0();
		bool L_2 = ___status0;
		NullCheck(L_1);
		GCMRegisterReady_Invoke_m292769666(L_1, L_2, /*hidden argument*/NULL);
		((PlayFabGoogleCloudMessaging_t2415988188_StaticFields*)PlayFabGoogleCloudMessaging_t2415988188_il2cpp_TypeInfo_var->static_fields)->set__RegistrationReadyCallback_0((GCMRegisterReady_t3751777615 *)NULL);
		return;
	}
}
// System.Void PlayFab.PlayFabGoogleCloudMessaging::RegistrationComplete(System.String,System.String)
extern Il2CppClass* PlayFabGoogleCloudMessaging_t2415988188_il2cpp_TypeInfo_var;
extern const uint32_t PlayFabGoogleCloudMessaging_RegistrationComplete_m2031169375_MetadataUsageId;
extern "C"  void PlayFabGoogleCloudMessaging_RegistrationComplete_m2031169375 (Il2CppObject * __this /* static, unused */, String_t* ___id0, String_t* ___error1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayFabGoogleCloudMessaging_RegistrationComplete_m2031169375_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GCMRegisterComplete_t1540081165 * L_0 = ((PlayFabGoogleCloudMessaging_t2415988188_StaticFields*)PlayFabGoogleCloudMessaging_t2415988188_il2cpp_TypeInfo_var->static_fields)->get__RegistrationCallback_1();
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		GCMRegisterComplete_t1540081165 * L_1 = ((PlayFabGoogleCloudMessaging_t2415988188_StaticFields*)PlayFabGoogleCloudMessaging_t2415988188_il2cpp_TypeInfo_var->static_fields)->get__RegistrationCallback_1();
		String_t* L_2 = ___id0;
		String_t* L_3 = ___error1;
		NullCheck(L_1);
		GCMRegisterComplete_Invoke_m429156347(L_1, L_2, L_3, /*hidden argument*/NULL);
		((PlayFabGoogleCloudMessaging_t2415988188_StaticFields*)PlayFabGoogleCloudMessaging_t2415988188_il2cpp_TypeInfo_var->static_fields)->set__RegistrationCallback_1((GCMRegisterComplete_t1540081165 *)NULL);
		return;
	}
}
// System.Void PlayFab.PlayFabGoogleCloudMessaging::MessageReceived(System.String)
extern Il2CppClass* PlayFabGoogleCloudMessaging_t2415988188_il2cpp_TypeInfo_var;
extern const uint32_t PlayFabGoogleCloudMessaging_MessageReceived_m3717748485_MetadataUsageId;
extern "C"  void PlayFabGoogleCloudMessaging_MessageReceived_m3717748485 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayFabGoogleCloudMessaging_MessageReceived_m3717748485_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GCMMessageReceived_t1036884951 * L_0 = ((PlayFabGoogleCloudMessaging_t2415988188_StaticFields*)PlayFabGoogleCloudMessaging_t2415988188_il2cpp_TypeInfo_var->static_fields)->get__MessageCallback_2();
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		GCMMessageReceived_t1036884951 * L_1 = ((PlayFabGoogleCloudMessaging_t2415988188_StaticFields*)PlayFabGoogleCloudMessaging_t2415988188_il2cpp_TypeInfo_var->static_fields)->get__MessageCallback_2();
		String_t* L_2 = ___message0;
		NullCheck(L_1);
		GCMMessageReceived_Invoke_m4197313743(L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayFab.PlayFabGoogleCloudMessaging/GCMMessageReceived::.ctor(System.Object,System.IntPtr)
extern "C"  void GCMMessageReceived__ctor_m2329524089 (GCMMessageReceived_t1036884951 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void PlayFab.PlayFabGoogleCloudMessaging/GCMMessageReceived::Invoke(System.String)
extern "C"  void GCMMessageReceived_Invoke_m4197313743 (GCMMessageReceived_t1036884951 * __this, String_t* ___message0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		GCMMessageReceived_Invoke_m4197313743((GCMMessageReceived_t1036884951 *)__this->get_prev_9(),___message0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___message0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___message0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___message0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___message0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___message0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C" void pinvoke_delegate_wrapper_GCMMessageReceived_t1036884951(Il2CppObject* delegate, String_t* ___message0)
{
	typedef void (STDCALL *native_function_ptr_type)(char*);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___message0' to native representation
	char* ____message0_marshaled = NULL;
	____message0_marshaled = il2cpp_codegen_marshal_string(___message0);

	// Native function invocation
	_il2cpp_pinvoke_func(____message0_marshaled);

	// Marshaling cleanup of parameter '___message0' native representation
	il2cpp_codegen_marshal_free(____message0_marshaled);
	____message0_marshaled = NULL;

}
// System.IAsyncResult PlayFab.PlayFabGoogleCloudMessaging/GCMMessageReceived::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GCMMessageReceived_BeginInvoke_m3003929940 (GCMMessageReceived_t1036884951 * __this, String_t* ___message0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___message0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void PlayFab.PlayFabGoogleCloudMessaging/GCMMessageReceived::EndInvoke(System.IAsyncResult)
extern "C"  void GCMMessageReceived_EndInvoke_m1478348297 (GCMMessageReceived_t1036884951 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void PlayFab.PlayFabGoogleCloudMessaging/GCMRegisterComplete::.ctor(System.Object,System.IntPtr)
extern "C"  void GCMRegisterComplete__ctor_m1214277705 (GCMRegisterComplete_t1540081165 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void PlayFab.PlayFabGoogleCloudMessaging/GCMRegisterComplete::Invoke(System.String,System.String)
extern "C"  void GCMRegisterComplete_Invoke_m429156347 (GCMRegisterComplete_t1540081165 * __this, String_t* ___id0, String_t* ___error1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		GCMRegisterComplete_Invoke_m429156347((GCMRegisterComplete_t1540081165 *)__this->get_prev_9(),___id0, ___error1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___id0, String_t* ___error1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___id0, ___error1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___id0, String_t* ___error1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___id0, ___error1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___error1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___id0, ___error1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C" void pinvoke_delegate_wrapper_GCMRegisterComplete_t1540081165(Il2CppObject* delegate, String_t* ___id0, String_t* ___error1)
{
	typedef void (STDCALL *native_function_ptr_type)(char*, char*);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___id0' to native representation
	char* ____id0_marshaled = NULL;
	____id0_marshaled = il2cpp_codegen_marshal_string(___id0);

	// Marshaling of parameter '___error1' to native representation
	char* ____error1_marshaled = NULL;
	____error1_marshaled = il2cpp_codegen_marshal_string(___error1);

	// Native function invocation
	_il2cpp_pinvoke_func(____id0_marshaled, ____error1_marshaled);

	// Marshaling cleanup of parameter '___id0' native representation
	il2cpp_codegen_marshal_free(____id0_marshaled);
	____id0_marshaled = NULL;

	// Marshaling cleanup of parameter '___error1' native representation
	il2cpp_codegen_marshal_free(____error1_marshaled);
	____error1_marshaled = NULL;

}
// System.IAsyncResult PlayFab.PlayFabGoogleCloudMessaging/GCMRegisterComplete::BeginInvoke(System.String,System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GCMRegisterComplete_BeginInvoke_m3689487568 (GCMRegisterComplete_t1540081165 * __this, String_t* ___id0, String_t* ___error1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___id0;
	__d_args[1] = ___error1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void PlayFab.PlayFabGoogleCloudMessaging/GCMRegisterComplete::EndInvoke(System.IAsyncResult)
extern "C"  void GCMRegisterComplete_EndInvoke_m2478663385 (GCMRegisterComplete_t1540081165 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void PlayFab.PlayFabGoogleCloudMessaging/GCMRegisterReady::.ctor(System.Object,System.IntPtr)
extern "C"  void GCMRegisterReady__ctor_m2105364913 (GCMRegisterReady_t3751777615 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void PlayFab.PlayFabGoogleCloudMessaging/GCMRegisterReady::Invoke(System.Boolean)
extern "C"  void GCMRegisterReady_Invoke_m292769666 (GCMRegisterReady_t3751777615 * __this, bool ___status0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		GCMRegisterReady_Invoke_m292769666((GCMRegisterReady_t3751777615 *)__this->get_prev_9(),___status0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___status0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___status0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___status0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___status0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C" void pinvoke_delegate_wrapper_GCMRegisterReady_t3751777615(Il2CppObject* delegate, bool ___status0)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___status0' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___status0);

	// Marshaling cleanup of parameter '___status0' native representation

}
// System.IAsyncResult PlayFab.PlayFabGoogleCloudMessaging/GCMRegisterReady::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern const uint32_t GCMRegisterReady_BeginInvoke_m3567146159_MetadataUsageId;
extern "C"  Il2CppObject * GCMRegisterReady_BeginInvoke_m3567146159 (GCMRegisterReady_t3751777615 * __this, bool ___status0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GCMRegisterReady_BeginInvoke_m3567146159_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &___status0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void PlayFab.PlayFabGoogleCloudMessaging/GCMRegisterReady::EndInvoke(System.IAsyncResult)
extern "C"  void GCMRegisterReady_EndInvoke_m223822913 (GCMRegisterReady_t3751777615 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// Conversion methods for marshalling of: PlayFabNotificationPackage
extern "C" void PlayFabNotificationPackage_t2037342920_marshal_pinvoke(const PlayFabNotificationPackage_t2037342920& unmarshaled, PlayFabNotificationPackage_t2037342920_marshaled_pinvoke& marshaled)
{
	marshaled.___Sound_0 = il2cpp_codegen_marshal_string(unmarshaled.get_Sound_0());
	marshaled.___Title_1 = il2cpp_codegen_marshal_string(unmarshaled.get_Title_1());
	marshaled.___Icon_2 = il2cpp_codegen_marshal_string(unmarshaled.get_Icon_2());
	marshaled.___Message_3 = il2cpp_codegen_marshal_string(unmarshaled.get_Message_3());
	marshaled.___CustomData_4 = il2cpp_codegen_marshal_string(unmarshaled.get_CustomData_4());
}
extern "C" void PlayFabNotificationPackage_t2037342920_marshal_pinvoke_back(const PlayFabNotificationPackage_t2037342920_marshaled_pinvoke& marshaled, PlayFabNotificationPackage_t2037342920& unmarshaled)
{
	unmarshaled.set_Sound_0(il2cpp_codegen_marshal_string_result(marshaled.___Sound_0));
	unmarshaled.set_Title_1(il2cpp_codegen_marshal_string_result(marshaled.___Title_1));
	unmarshaled.set_Icon_2(il2cpp_codegen_marshal_string_result(marshaled.___Icon_2));
	unmarshaled.set_Message_3(il2cpp_codegen_marshal_string_result(marshaled.___Message_3));
	unmarshaled.set_CustomData_4(il2cpp_codegen_marshal_string_result(marshaled.___CustomData_4));
}
// Conversion method for clean up from marshalling of: PlayFabNotificationPackage
extern "C" void PlayFabNotificationPackage_t2037342920_marshal_pinvoke_cleanup(PlayFabNotificationPackage_t2037342920_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___Sound_0);
	marshaled.___Sound_0 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___Title_1);
	marshaled.___Title_1 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___Icon_2);
	marshaled.___Icon_2 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___Message_3);
	marshaled.___Message_3 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___CustomData_4);
	marshaled.___CustomData_4 = NULL;
}
// Conversion methods for marshalling of: PlayFabNotificationPackage
extern "C" void PlayFabNotificationPackage_t2037342920_marshal_com(const PlayFabNotificationPackage_t2037342920& unmarshaled, PlayFabNotificationPackage_t2037342920_marshaled_com& marshaled)
{
	marshaled.___Sound_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_Sound_0());
	marshaled.___Title_1 = il2cpp_codegen_marshal_bstring(unmarshaled.get_Title_1());
	marshaled.___Icon_2 = il2cpp_codegen_marshal_bstring(unmarshaled.get_Icon_2());
	marshaled.___Message_3 = il2cpp_codegen_marshal_bstring(unmarshaled.get_Message_3());
	marshaled.___CustomData_4 = il2cpp_codegen_marshal_bstring(unmarshaled.get_CustomData_4());
}
extern "C" void PlayFabNotificationPackage_t2037342920_marshal_com_back(const PlayFabNotificationPackage_t2037342920_marshaled_com& marshaled, PlayFabNotificationPackage_t2037342920& unmarshaled)
{
	unmarshaled.set_Sound_0(il2cpp_codegen_marshal_bstring_result(marshaled.___Sound_0));
	unmarshaled.set_Title_1(il2cpp_codegen_marshal_bstring_result(marshaled.___Title_1));
	unmarshaled.set_Icon_2(il2cpp_codegen_marshal_bstring_result(marshaled.___Icon_2));
	unmarshaled.set_Message_3(il2cpp_codegen_marshal_bstring_result(marshaled.___Message_3));
	unmarshaled.set_CustomData_4(il2cpp_codegen_marshal_bstring_result(marshaled.___CustomData_4));
}
// Conversion method for clean up from marshalling of: PlayFabNotificationPackage
extern "C" void PlayFabNotificationPackage_t2037342920_marshal_com_cleanup(PlayFabNotificationPackage_t2037342920_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___Sound_0);
	marshaled.___Sound_0 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___Title_1);
	marshaled.___Title_1 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___Icon_2);
	marshaled.___Icon_2 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___Message_3);
	marshaled.___Message_3 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___CustomData_4);
	marshaled.___CustomData_4 = NULL;
}
// SimpleJSON.JSONNode SimpleJSON.JSON::Parse(System.String)
extern "C"  JSONNode_t580622632 * JSON_Parse_m2730962913 (Il2CppObject * __this /* static, unused */, String_t* ___aJSON0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___aJSON0;
		JSONNode_t580622632 * L_1 = JSONNode_Parse_m1279392515(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void SimpleJSON.JSONArray::.ctor()
extern Il2CppClass* List_1_t1377581601_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m4095719313_MethodInfo_var;
extern const uint32_t JSONArray__ctor_m2704428242_MetadataUsageId;
extern "C"  void JSONArray__ctor_m2704428242 (JSONArray_t3478762607 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONArray__ctor_m2704428242_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1377581601 * L_0 = (List_1_t1377581601 *)il2cpp_codegen_object_new(List_1_t1377581601_il2cpp_TypeInfo_var);
		List_1__ctor_m4095719313(L_0, /*hidden argument*/List_1__ctor_m4095719313_MethodInfo_var);
		__this->set_m_List_0(L_0);
		JSONNode__ctor_m39807289(__this, /*hidden argument*/NULL);
		return;
	}
}
// SimpleJSON.JSONNode SimpleJSON.JSONArray::get_Item(System.Int32)
extern Il2CppClass* JSONLazyCreator_t3067077166_il2cpp_TypeInfo_var;
extern const uint32_t JSONArray_get_Item_m3401459060_MetadataUsageId;
extern "C"  JSONNode_t580622632 * JSONArray_get_Item_m3401459060 (JSONArray_t3478762607 * __this, int32_t ___aIndex0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONArray_get_Item_m3401459060_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___aIndex0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_1 = ___aIndex0;
		List_1_t1377581601 * L_2 = __this->get_m_List_0();
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<SimpleJSON.JSONNode>::get_Count() */, L_2);
		if ((((int32_t)L_1) < ((int32_t)L_3)))
		{
			goto IL_001f;
		}
	}

IL_0018:
	{
		JSONLazyCreator_t3067077166 * L_4 = (JSONLazyCreator_t3067077166 *)il2cpp_codegen_object_new(JSONLazyCreator_t3067077166_il2cpp_TypeInfo_var);
		JSONLazyCreator__ctor_m3036761377(L_4, __this, /*hidden argument*/NULL);
		return L_4;
	}

IL_001f:
	{
		List_1_t1377581601 * L_5 = __this->get_m_List_0();
		int32_t L_6 = ___aIndex0;
		NullCheck(L_5);
		JSONNode_t580622632 * L_7 = VirtFuncInvoker1< JSONNode_t580622632 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<SimpleJSON.JSONNode>::get_Item(System.Int32) */, L_5, L_6);
		return L_7;
	}
}
// System.Void SimpleJSON.JSONArray::set_Item(System.Int32,SimpleJSON.JSONNode)
extern "C"  void JSONArray_set_Item_m2990769409 (JSONArray_t3478762607 * __this, int32_t ___aIndex0, JSONNode_t580622632 * ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___aIndex0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_1 = ___aIndex0;
		List_1_t1377581601 * L_2 = __this->get_m_List_0();
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<SimpleJSON.JSONNode>::get_Count() */, L_2);
		if ((((int32_t)L_1) < ((int32_t)L_3)))
		{
			goto IL_0029;
		}
	}

IL_0018:
	{
		List_1_t1377581601 * L_4 = __this->get_m_List_0();
		JSONNode_t580622632 * L_5 = ___value1;
		NullCheck(L_4);
		VirtActionInvoker1< JSONNode_t580622632 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<SimpleJSON.JSONNode>::Add(!0) */, L_4, L_5);
		goto IL_0036;
	}

IL_0029:
	{
		List_1_t1377581601 * L_6 = __this->get_m_List_0();
		int32_t L_7 = ___aIndex0;
		JSONNode_t580622632 * L_8 = ___value1;
		NullCheck(L_6);
		VirtActionInvoker2< int32_t, JSONNode_t580622632 * >::Invoke(32 /* System.Void System.Collections.Generic.List`1<SimpleJSON.JSONNode>::set_Item(System.Int32,!0) */, L_6, L_7, L_8);
	}

IL_0036:
	{
		return;
	}
}
// SimpleJSON.JSONNode SimpleJSON.JSONArray::get_Item(System.String)
extern Il2CppClass* JSONLazyCreator_t3067077166_il2cpp_TypeInfo_var;
extern const uint32_t JSONArray_get_Item_m2822714175_MetadataUsageId;
extern "C"  JSONNode_t580622632 * JSONArray_get_Item_m2822714175 (JSONArray_t3478762607 * __this, String_t* ___aKey0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONArray_get_Item_m2822714175_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		JSONLazyCreator_t3067077166 * L_0 = (JSONLazyCreator_t3067077166 *)il2cpp_codegen_object_new(JSONLazyCreator_t3067077166_il2cpp_TypeInfo_var);
		JSONLazyCreator__ctor_m3036761377(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void SimpleJSON.JSONArray::set_Item(System.String,SimpleJSON.JSONNode)
extern "C"  void JSONArray_set_Item_m1153404782 (JSONArray_t3478762607 * __this, String_t* ___aKey0, JSONNode_t580622632 * ___value1, const MethodInfo* method)
{
	{
		List_1_t1377581601 * L_0 = __this->get_m_List_0();
		JSONNode_t580622632 * L_1 = ___value1;
		NullCheck(L_0);
		VirtActionInvoker1< JSONNode_t580622632 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<SimpleJSON.JSONNode>::Add(!0) */, L_0, L_1);
		return;
	}
}
// System.Int32 SimpleJSON.JSONArray::get_Count()
extern "C"  int32_t JSONArray_get_Count_m2938021024 (JSONArray_t3478762607 * __this, const MethodInfo* method)
{
	{
		List_1_t1377581601 * L_0 = __this->get_m_List_0();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<SimpleJSON.JSONNode>::get_Count() */, L_0);
		return L_1;
	}
}
// System.Void SimpleJSON.JSONArray::Add(System.String,SimpleJSON.JSONNode)
extern "C"  void JSONArray_Add_m793281983 (JSONArray_t3478762607 * __this, String_t* ___aKey0, JSONNode_t580622632 * ___aItem1, const MethodInfo* method)
{
	{
		List_1_t1377581601 * L_0 = __this->get_m_List_0();
		JSONNode_t580622632 * L_1 = ___aItem1;
		NullCheck(L_0);
		VirtActionInvoker1< JSONNode_t580622632 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<SimpleJSON.JSONNode>::Add(!0) */, L_0, L_1);
		return;
	}
}
// SimpleJSON.JSONNode SimpleJSON.JSONArray::Remove(System.Int32)
extern "C"  JSONNode_t580622632 * JSONArray_Remove_m3211491100 (JSONArray_t3478762607 * __this, int32_t ___aIndex0, const MethodInfo* method)
{
	JSONNode_t580622632 * V_0 = NULL;
	{
		int32_t L_0 = ___aIndex0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_1 = ___aIndex0;
		List_1_t1377581601 * L_2 = __this->get_m_List_0();
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<SimpleJSON.JSONNode>::get_Count() */, L_2);
		if ((((int32_t)L_1) < ((int32_t)L_3)))
		{
			goto IL_001a;
		}
	}

IL_0018:
	{
		return (JSONNode_t580622632 *)NULL;
	}

IL_001a:
	{
		List_1_t1377581601 * L_4 = __this->get_m_List_0();
		int32_t L_5 = ___aIndex0;
		NullCheck(L_4);
		JSONNode_t580622632 * L_6 = VirtFuncInvoker1< JSONNode_t580622632 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<SimpleJSON.JSONNode>::get_Item(System.Int32) */, L_4, L_5);
		V_0 = L_6;
		List_1_t1377581601 * L_7 = __this->get_m_List_0();
		int32_t L_8 = ___aIndex0;
		NullCheck(L_7);
		VirtActionInvoker1< int32_t >::Invoke(30 /* System.Void System.Collections.Generic.List`1<SimpleJSON.JSONNode>::RemoveAt(System.Int32) */, L_7, L_8);
		JSONNode_t580622632 * L_9 = V_0;
		return L_9;
	}
}
// SimpleJSON.JSONNode SimpleJSON.JSONArray::Remove(SimpleJSON.JSONNode)
extern "C"  JSONNode_t580622632 * JSONArray_Remove_m3549899209 (JSONArray_t3478762607 * __this, JSONNode_t580622632 * ___aNode0, const MethodInfo* method)
{
	{
		List_1_t1377581601 * L_0 = __this->get_m_List_0();
		JSONNode_t580622632 * L_1 = ___aNode0;
		NullCheck(L_0);
		VirtFuncInvoker1< bool, JSONNode_t580622632 * >::Invoke(26 /* System.Boolean System.Collections.Generic.List`1<SimpleJSON.JSONNode>::Remove(!0) */, L_0, L_1);
		JSONNode_t580622632 * L_2 = ___aNode0;
		return L_2;
	}
}
// System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode> SimpleJSON.JSONArray::get_Childs()
extern Il2CppClass* U3CU3Ec__Iterator2_t2516598211_il2cpp_TypeInfo_var;
extern const uint32_t JSONArray_get_Childs_m358868316_MetadataUsageId;
extern "C"  Il2CppObject* JSONArray_get_Childs_m358868316 (JSONArray_t3478762607 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONArray_get_Childs_m358868316_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__Iterator2_t2516598211 * V_0 = NULL;
	{
		U3CU3Ec__Iterator2_t2516598211 * L_0 = (U3CU3Ec__Iterator2_t2516598211 *)il2cpp_codegen_object_new(U3CU3Ec__Iterator2_t2516598211_il2cpp_TypeInfo_var);
		U3CU3Ec__Iterator2__ctor_m3872687330(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__Iterator2_t2516598211 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_4(__this);
		U3CU3Ec__Iterator2_t2516598211 * L_2 = V_0;
		U3CU3Ec__Iterator2_t2516598211 * L_3 = L_2;
		NullCheck(L_3);
		L_3->set_U24PC_2(((int32_t)-2));
		return L_3;
	}
}
// System.Collections.IEnumerator SimpleJSON.JSONArray::GetEnumerator()
extern Il2CppClass* U3CGetEnumeratorU3Ec__Iterator3_t490656866_il2cpp_TypeInfo_var;
extern const uint32_t JSONArray_GetEnumerator_m2371155194_MetadataUsageId;
extern "C"  Il2CppObject * JSONArray_GetEnumerator_m2371155194 (JSONArray_t3478762607 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONArray_GetEnumerator_m2371155194_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetEnumeratorU3Ec__Iterator3_t490656866 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator3_t490656866 * L_0 = (U3CGetEnumeratorU3Ec__Iterator3_t490656866 *)il2cpp_codegen_object_new(U3CGetEnumeratorU3Ec__Iterator3_t490656866_il2cpp_TypeInfo_var);
		U3CGetEnumeratorU3Ec__Iterator3__ctor_m2238226143(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetEnumeratorU3Ec__Iterator3_t490656866 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_4(__this);
		U3CGetEnumeratorU3Ec__Iterator3_t490656866 * L_2 = V_0;
		return L_2;
	}
}
// System.String SimpleJSON.JSONArray::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t3758331889_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m2342562092_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1678478676_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m642647484_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2853;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral1085;
extern const uint32_t JSONArray_ToString_m4067479617_MetadataUsageId;
extern "C"  String_t* JSONArray_ToString_m4067479617 (JSONArray_t3478762607 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONArray_ToString_m4067479617_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	JSONNode_t580622632 * V_1 = NULL;
	Enumerator_t3758331889  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = _stringLiteral2853;
		List_1_t1377581601 * L_0 = __this->get_m_List_0();
		NullCheck(L_0);
		Enumerator_t3758331889  L_1 = List_1_GetEnumerator_m2342562092(L_0, /*hidden argument*/List_1_GetEnumerator_m2342562092_MethodInfo_var);
		V_2 = L_1;
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0044;
		}

IL_0017:
		{
			JSONNode_t580622632 * L_2 = Enumerator_get_Current_m1678478676((&V_2), /*hidden argument*/Enumerator_get_Current_m1678478676_MethodInfo_var);
			V_1 = L_2;
			String_t* L_3 = V_0;
			NullCheck(L_3);
			int32_t L_4 = String_get_Length_m2979997331(L_3, /*hidden argument*/NULL);
			if ((((int32_t)L_4) <= ((int32_t)2)))
			{
				goto IL_0037;
			}
		}

IL_002b:
		{
			String_t* L_5 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_6 = String_Concat_m138640077(NULL /*static, unused*/, L_5, _stringLiteral1396, /*hidden argument*/NULL);
			V_0 = L_6;
		}

IL_0037:
		{
			String_t* L_7 = V_0;
			JSONNode_t580622632 * L_8 = V_1;
			NullCheck(L_8);
			String_t* L_9 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String SimpleJSON.JSONNode::ToString() */, L_8);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_10 = String_Concat_m138640077(NULL /*static, unused*/, L_7, L_9, /*hidden argument*/NULL);
			V_0 = L_10;
		}

IL_0044:
		{
			bool L_11 = Enumerator_MoveNext_m642647484((&V_2), /*hidden argument*/Enumerator_MoveNext_m642647484_MethodInfo_var);
			if (L_11)
			{
				goto IL_0017;
			}
		}

IL_0050:
		{
			IL2CPP_LEAVE(0x61, FINALLY_0055);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0055;
	}

FINALLY_0055:
	{ // begin finally (depth: 1)
		Enumerator_t3758331889  L_12 = V_2;
		Enumerator_t3758331889  L_13 = L_12;
		Il2CppObject * L_14 = Box(Enumerator_t3758331889_il2cpp_TypeInfo_var, &L_13);
		NullCheck((Il2CppObject *)L_14);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_14);
		IL2CPP_END_FINALLY(85)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(85)
	{
		IL2CPP_JUMP_TBL(0x61, IL_0061)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0061:
	{
		String_t* L_15 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m138640077(NULL /*static, unused*/, L_15, _stringLiteral1085, /*hidden argument*/NULL);
		V_0 = L_16;
		String_t* L_17 = V_0;
		return L_17;
	}
}
// System.String SimpleJSON.JSONArray::ToString(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t3758331889_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m2342562092_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1678478676_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m642647484_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2853;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral10;
extern Il2CppCodeGenString* _stringLiteral31776;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t JSONArray_ToString_m3876782177_MetadataUsageId;
extern "C"  String_t* JSONArray_ToString_m3876782177 (JSONArray_t3478762607 * __this, String_t* ___aPrefix0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONArray_ToString_m3876782177_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	JSONNode_t580622632 * V_1 = NULL;
	Enumerator_t3758331889  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = _stringLiteral2853;
		List_1_t1377581601 * L_0 = __this->get_m_List_0();
		NullCheck(L_0);
		Enumerator_t3758331889  L_1 = List_1_GetEnumerator_m2342562092(L_0, /*hidden argument*/List_1_GetEnumerator_m2342562092_MethodInfo_var);
		V_2 = L_1;
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0061;
		}

IL_0017:
		{
			JSONNode_t580622632 * L_2 = Enumerator_get_Current_m1678478676((&V_2), /*hidden argument*/Enumerator_get_Current_m1678478676_MethodInfo_var);
			V_1 = L_2;
			String_t* L_3 = V_0;
			NullCheck(L_3);
			int32_t L_4 = String_get_Length_m2979997331(L_3, /*hidden argument*/NULL);
			if ((((int32_t)L_4) <= ((int32_t)3)))
			{
				goto IL_0037;
			}
		}

IL_002b:
		{
			String_t* L_5 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_6 = String_Concat_m138640077(NULL /*static, unused*/, L_5, _stringLiteral1396, /*hidden argument*/NULL);
			V_0 = L_6;
		}

IL_0037:
		{
			String_t* L_7 = V_0;
			String_t* L_8 = ___aPrefix0;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_9 = String_Concat_m2933632197(NULL /*static, unused*/, L_7, _stringLiteral10, L_8, _stringLiteral31776, /*hidden argument*/NULL);
			V_0 = L_9;
			String_t* L_10 = V_0;
			JSONNode_t580622632 * L_11 = V_1;
			String_t* L_12 = ___aPrefix0;
			String_t* L_13 = String_Concat_m138640077(NULL /*static, unused*/, L_12, _stringLiteral31776, /*hidden argument*/NULL);
			NullCheck(L_11);
			String_t* L_14 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(17 /* System.String SimpleJSON.JSONNode::ToString(System.String) */, L_11, L_13);
			String_t* L_15 = String_Concat_m138640077(NULL /*static, unused*/, L_10, L_14, /*hidden argument*/NULL);
			V_0 = L_15;
		}

IL_0061:
		{
			bool L_16 = Enumerator_MoveNext_m642647484((&V_2), /*hidden argument*/Enumerator_MoveNext_m642647484_MethodInfo_var);
			if (L_16)
			{
				goto IL_0017;
			}
		}

IL_006d:
		{
			IL2CPP_LEAVE(0x7E, FINALLY_0072);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0072;
	}

FINALLY_0072:
	{ // begin finally (depth: 1)
		Enumerator_t3758331889  L_17 = V_2;
		Enumerator_t3758331889  L_18 = L_17;
		Il2CppObject * L_19 = Box(Enumerator_t3758331889_il2cpp_TypeInfo_var, &L_18);
		NullCheck((Il2CppObject *)L_19);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_19);
		IL2CPP_END_FINALLY(114)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(114)
	{
		IL2CPP_JUMP_TBL(0x7E, IL_007e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_007e:
	{
		String_t* L_20 = V_0;
		String_t* L_21 = ___aPrefix0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Concat_m2933632197(NULL /*static, unused*/, L_20, _stringLiteral10, L_21, _stringLiteral93, /*hidden argument*/NULL);
		V_0 = L_22;
		String_t* L_23 = V_0;
		return L_23;
	}
}
// System.Void SimpleJSON.JSONArray::Serialize(System.IO.BinaryWriter)
extern "C"  void JSONArray_Serialize_m284758931 (JSONArray_t3478762607 * __this, BinaryWriter_t2314211483 * ___aWriter0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		BinaryWriter_t2314211483 * L_0 = ___aWriter0;
		NullCheck(L_0);
		VirtActionInvoker1< uint8_t >::Invoke(8 /* System.Void System.IO.BinaryWriter::Write(System.Byte) */, L_0, 1);
		BinaryWriter_t2314211483 * L_1 = ___aWriter0;
		List_1_t1377581601 * L_2 = __this->get_m_List_0();
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<SimpleJSON.JSONNode>::get_Count() */, L_2);
		NullCheck(L_1);
		VirtActionInvoker1< int32_t >::Invoke(16 /* System.Void System.IO.BinaryWriter::Write(System.Int32) */, L_1, L_3);
		V_0 = 0;
		goto IL_0035;
	}

IL_001f:
	{
		List_1_t1377581601 * L_4 = __this->get_m_List_0();
		int32_t L_5 = V_0;
		NullCheck(L_4);
		JSONNode_t580622632 * L_6 = VirtFuncInvoker1< JSONNode_t580622632 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<SimpleJSON.JSONNode>::get_Item(System.Int32) */, L_4, L_5);
		BinaryWriter_t2314211483 * L_7 = ___aWriter0;
		NullCheck(L_6);
		VirtActionInvoker1< BinaryWriter_t2314211483 * >::Invoke(28 /* System.Void SimpleJSON.JSONNode::Serialize(System.IO.BinaryWriter) */, L_6, L_7);
		int32_t L_8 = V_0;
		V_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0035:
	{
		int32_t L_9 = V_0;
		List_1_t1377581601 * L_10 = __this->get_m_List_0();
		NullCheck(L_10);
		int32_t L_11 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<SimpleJSON.JSONNode>::get_Count() */, L_10);
		if ((((int32_t)L_9) < ((int32_t)L_11)))
		{
			goto IL_001f;
		}
	}
	{
		return;
	}
}
// System.Void SimpleJSON.JSONArray/<>c__Iterator2::.ctor()
extern "C"  void U3CU3Ec__Iterator2__ctor_m3872687330 (U3CU3Ec__Iterator2_t2516598211 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// SimpleJSON.JSONNode SimpleJSON.JSONArray/<>c__Iterator2::System.Collections.Generic.IEnumerator<SimpleJSON.JSONNode>.get_Current()
extern "C"  JSONNode_t580622632 * U3CU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m1959704405 (U3CU3Ec__Iterator2_t2516598211 * __this, const MethodInfo* method)
{
	{
		JSONNode_t580622632 * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Object SimpleJSON.JSONArray/<>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m2408747204 (U3CU3Ec__Iterator2_t2516598211 * __this, const MethodInfo* method)
{
	{
		JSONNode_t580622632 * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Collections.IEnumerator SimpleJSON.JSONArray/<>c__Iterator2::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CU3Ec__Iterator2_System_Collections_IEnumerable_GetEnumerator_m856485975 (U3CU3Ec__Iterator2_t2516598211 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = U3CU3Ec__Iterator2_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m1772464492(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONArray/<>c__Iterator2::System.Collections.Generic.IEnumerable<SimpleJSON.JSONNode>.GetEnumerator()
extern Il2CppClass* U3CU3Ec__Iterator2_t2516598211_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec__Iterator2_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m1772464492_MetadataUsageId;
extern "C"  Il2CppObject* U3CU3Ec__Iterator2_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m1772464492 (U3CU3Ec__Iterator2_t2516598211 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__Iterator2_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m1772464492_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__Iterator2_t2516598211 * V_0 = NULL;
	{
		int32_t* L_0 = __this->get_address_of_U24PC_2();
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, L_0, 0, ((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CU3Ec__Iterator2_t2516598211 * L_2 = (U3CU3Ec__Iterator2_t2516598211 *)il2cpp_codegen_object_new(U3CU3Ec__Iterator2_t2516598211_il2cpp_TypeInfo_var);
		U3CU3Ec__Iterator2__ctor_m3872687330(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		U3CU3Ec__Iterator2_t2516598211 * L_3 = V_0;
		JSONArray_t3478762607 * L_4 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_4(L_4);
		U3CU3Ec__Iterator2_t2516598211 * L_5 = V_0;
		return L_5;
	}
}
// System.Boolean SimpleJSON.JSONArray/<>c__Iterator2::MoveNext()
extern Il2CppClass* Enumerator_t3758331889_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m2342562092_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1678478676_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m642647484_MethodInfo_var;
extern const uint32_t U3CU3Ec__Iterator2_MoveNext_m3692409834_MetadataUsageId;
extern "C"  bool U3CU3Ec__Iterator2_MoveNext_m3692409834 (U3CU3Ec__Iterator2_t2516598211 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__Iterator2_MoveNext_m3692409834_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_2();
		V_0 = L_0;
		__this->set_U24PC_2((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_003c;
		}
	}
	{
		goto IL_00a9;
	}

IL_0023:
	{
		JSONArray_t3478762607 * L_2 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_2);
		List_1_t1377581601 * L_3 = L_2->get_m_List_0();
		NullCheck(L_3);
		Enumerator_t3758331889  L_4 = List_1_GetEnumerator_m2342562092(L_3, /*hidden argument*/List_1_GetEnumerator_m2342562092_MethodInfo_var);
		__this->set_U3CU24s_20U3E__0_0(L_4);
		V_0 = ((int32_t)-3);
	}

IL_003c:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_5 = V_0;
			if (((int32_t)((int32_t)L_5-(int32_t)1)) == 0)
			{
				goto IL_0078;
			}
		}

IL_0048:
		{
			goto IL_0078;
		}

IL_004d:
		{
			Enumerator_t3758331889 * L_6 = __this->get_address_of_U3CU24s_20U3E__0_0();
			JSONNode_t580622632 * L_7 = Enumerator_get_Current_m1678478676(L_6, /*hidden argument*/Enumerator_get_Current_m1678478676_MethodInfo_var);
			__this->set_U3CNU3E__1_1(L_7);
			JSONNode_t580622632 * L_8 = __this->get_U3CNU3E__1_1();
			__this->set_U24current_3(L_8);
			__this->set_U24PC_2(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xAB, FINALLY_008d);
		}

IL_0078:
		{
			Enumerator_t3758331889 * L_9 = __this->get_address_of_U3CU24s_20U3E__0_0();
			bool L_10 = Enumerator_MoveNext_m642647484(L_9, /*hidden argument*/Enumerator_MoveNext_m642647484_MethodInfo_var);
			if (L_10)
			{
				goto IL_004d;
			}
		}

IL_0088:
		{
			IL2CPP_LEAVE(0xA2, FINALLY_008d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008d;
	}

FINALLY_008d:
	{ // begin finally (depth: 1)
		{
			bool L_11 = V_1;
			if (!L_11)
			{
				goto IL_0091;
			}
		}

IL_0090:
		{
			IL2CPP_END_FINALLY(141)
		}

IL_0091:
		{
			Enumerator_t3758331889  L_12 = __this->get_U3CU24s_20U3E__0_0();
			Enumerator_t3758331889  L_13 = L_12;
			Il2CppObject * L_14 = Box(Enumerator_t3758331889_il2cpp_TypeInfo_var, &L_13);
			NullCheck((Il2CppObject *)L_14);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_14);
			IL2CPP_END_FINALLY(141)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(141)
	{
		IL2CPP_JUMP_TBL(0xAB, IL_00ab)
		IL2CPP_JUMP_TBL(0xA2, IL_00a2)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00a2:
	{
		__this->set_U24PC_2((-1));
	}

IL_00a9:
	{
		return (bool)0;
	}

IL_00ab:
	{
		return (bool)1;
	}
	// Dead block : IL_00ad: ldloc.2
}
// System.Void SimpleJSON.JSONArray/<>c__Iterator2::Dispose()
extern Il2CppClass* Enumerator_t3758331889_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec__Iterator2_Dispose_m2109128863_MetadataUsageId;
extern "C"  void U3CU3Ec__Iterator2_Dispose_m2109128863 (U3CU3Ec__Iterator2_t2516598211 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__Iterator2_Dispose_m2109128863_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_2();
		V_0 = L_0;
		__this->set_U24PC_2((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0037;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_0037;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x37, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		Enumerator_t3758331889  L_2 = __this->get_U3CU24s_20U3E__0_0();
		Enumerator_t3758331889  L_3 = L_2;
		Il2CppObject * L_4 = Box(Enumerator_t3758331889_il2cpp_TypeInfo_var, &L_3);
		NullCheck((Il2CppObject *)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
		IL2CPP_END_FINALLY(38)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x37, IL_0037)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0037:
	{
		return;
	}
}
// System.Void SimpleJSON.JSONArray/<>c__Iterator2::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec__Iterator2_Reset_m1519120271_MetadataUsageId;
extern "C"  void U3CU3Ec__Iterator2_Reset_m1519120271 (U3CU3Ec__Iterator2_t2516598211 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__Iterator2_Reset_m1519120271_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void SimpleJSON.JSONArray/<GetEnumerator>c__Iterator3::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3__ctor_m2238226143 (U3CGetEnumeratorU3Ec__Iterator3_t490656866 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object SimpleJSON.JSONArray/<GetEnumerator>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m887470941 (U3CGetEnumeratorU3Ec__Iterator3_t490656866 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Object SimpleJSON.JSONArray/<GetEnumerator>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m2301368561 (U3CGetEnumeratorU3Ec__Iterator3_t490656866 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Boolean SimpleJSON.JSONArray/<GetEnumerator>c__Iterator3::MoveNext()
extern Il2CppClass* Enumerator_t3758331889_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m2342562092_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1678478676_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m642647484_MethodInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator3_MoveNext_m2398667141_MetadataUsageId;
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator3_MoveNext_m2398667141 (U3CGetEnumeratorU3Ec__Iterator3_t490656866 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator3_MoveNext_m2398667141_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_2();
		V_0 = L_0;
		__this->set_U24PC_2((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_003c;
		}
	}
	{
		goto IL_00a9;
	}

IL_0023:
	{
		JSONArray_t3478762607 * L_2 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_2);
		List_1_t1377581601 * L_3 = L_2->get_m_List_0();
		NullCheck(L_3);
		Enumerator_t3758331889  L_4 = List_1_GetEnumerator_m2342562092(L_3, /*hidden argument*/List_1_GetEnumerator_m2342562092_MethodInfo_var);
		__this->set_U3CU24s_21U3E__0_0(L_4);
		V_0 = ((int32_t)-3);
	}

IL_003c:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_5 = V_0;
			if (((int32_t)((int32_t)L_5-(int32_t)1)) == 0)
			{
				goto IL_0078;
			}
		}

IL_0048:
		{
			goto IL_0078;
		}

IL_004d:
		{
			Enumerator_t3758331889 * L_6 = __this->get_address_of_U3CU24s_21U3E__0_0();
			JSONNode_t580622632 * L_7 = Enumerator_get_Current_m1678478676(L_6, /*hidden argument*/Enumerator_get_Current_m1678478676_MethodInfo_var);
			__this->set_U3CNU3E__1_1(L_7);
			JSONNode_t580622632 * L_8 = __this->get_U3CNU3E__1_1();
			__this->set_U24current_3(L_8);
			__this->set_U24PC_2(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xAB, FINALLY_008d);
		}

IL_0078:
		{
			Enumerator_t3758331889 * L_9 = __this->get_address_of_U3CU24s_21U3E__0_0();
			bool L_10 = Enumerator_MoveNext_m642647484(L_9, /*hidden argument*/Enumerator_MoveNext_m642647484_MethodInfo_var);
			if (L_10)
			{
				goto IL_004d;
			}
		}

IL_0088:
		{
			IL2CPP_LEAVE(0xA2, FINALLY_008d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008d;
	}

FINALLY_008d:
	{ // begin finally (depth: 1)
		{
			bool L_11 = V_1;
			if (!L_11)
			{
				goto IL_0091;
			}
		}

IL_0090:
		{
			IL2CPP_END_FINALLY(141)
		}

IL_0091:
		{
			Enumerator_t3758331889  L_12 = __this->get_U3CU24s_21U3E__0_0();
			Enumerator_t3758331889  L_13 = L_12;
			Il2CppObject * L_14 = Box(Enumerator_t3758331889_il2cpp_TypeInfo_var, &L_13);
			NullCheck((Il2CppObject *)L_14);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_14);
			IL2CPP_END_FINALLY(141)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(141)
	{
		IL2CPP_JUMP_TBL(0xAB, IL_00ab)
		IL2CPP_JUMP_TBL(0xA2, IL_00a2)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00a2:
	{
		__this->set_U24PC_2((-1));
	}

IL_00a9:
	{
		return (bool)0;
	}

IL_00ab:
	{
		return (bool)1;
	}
	// Dead block : IL_00ad: ldloc.2
}
// System.Void SimpleJSON.JSONArray/<GetEnumerator>c__Iterator3::Dispose()
extern Il2CppClass* Enumerator_t3758331889_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator3_Dispose_m3349958492_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3_Dispose_m3349958492 (U3CGetEnumeratorU3Ec__Iterator3_t490656866 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator3_Dispose_m3349958492_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_2();
		V_0 = L_0;
		__this->set_U24PC_2((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0037;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_0037;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x37, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		Enumerator_t3758331889  L_2 = __this->get_U3CU24s_21U3E__0_0();
		Enumerator_t3758331889  L_3 = L_2;
		Il2CppObject * L_4 = Box(Enumerator_t3758331889_il2cpp_TypeInfo_var, &L_3);
		NullCheck((Il2CppObject *)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
		IL2CPP_END_FINALLY(38)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x37, IL_0037)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0037:
	{
		return;
	}
}
// System.Void SimpleJSON.JSONArray/<GetEnumerator>c__Iterator3::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator3_Reset_m4179626380_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3_Reset_m4179626380 (U3CGetEnumeratorU3Ec__Iterator3_t490656866 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator3_Reset_m4179626380_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void SimpleJSON.JSONClass::.ctor()
extern Il2CppClass* Dictionary_2_t2218320536_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1330921294_MethodInfo_var;
extern const uint32_t JSONClass__ctor_m158050451_MetadataUsageId;
extern "C"  void JSONClass__ctor_m158050451 (JSONClass_t3480415118 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONClass__ctor_m158050451_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2218320536 * L_0 = (Dictionary_2_t2218320536 *)il2cpp_codegen_object_new(Dictionary_2_t2218320536_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1330921294(L_0, /*hidden argument*/Dictionary_2__ctor_m1330921294_MethodInfo_var);
		__this->set_m_Dict_0(L_0);
		JSONNode__ctor_m39807289(__this, /*hidden argument*/NULL);
		return;
	}
}
// SimpleJSON.JSONNode SimpleJSON.JSONClass::get_Item(System.String)
extern Il2CppClass* JSONLazyCreator_t3067077166_il2cpp_TypeInfo_var;
extern const uint32_t JSONClass_get_Item_m2960321792_MetadataUsageId;
extern "C"  JSONNode_t580622632 * JSONClass_get_Item_m2960321792 (JSONClass_t3480415118 * __this, String_t* ___aKey0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONClass_get_Item_m2960321792_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2218320536 * L_0 = __this->get_m_Dict_0();
		String_t* L_1 = ___aKey0;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, String_t* >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,SimpleJSON.JSONNode>::ContainsKey(!0) */, L_0, L_1);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		Dictionary_2_t2218320536 * L_3 = __this->get_m_Dict_0();
		String_t* L_4 = ___aKey0;
		NullCheck(L_3);
		JSONNode_t580622632 * L_5 = VirtFuncInvoker1< JSONNode_t580622632 *, String_t* >::Invoke(22 /* !1 System.Collections.Generic.Dictionary`2<System.String,SimpleJSON.JSONNode>::get_Item(!0) */, L_3, L_4);
		return L_5;
	}

IL_001e:
	{
		String_t* L_6 = ___aKey0;
		JSONLazyCreator_t3067077166 * L_7 = (JSONLazyCreator_t3067077166 *)il2cpp_codegen_object_new(JSONLazyCreator_t3067077166_il2cpp_TypeInfo_var);
		JSONLazyCreator__ctor_m66611101(L_7, __this, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void SimpleJSON.JSONClass::set_Item(System.String,SimpleJSON.JSONNode)
extern "C"  void JSONClass_set_Item_m1043193519 (JSONClass_t3480415118 * __this, String_t* ___aKey0, JSONNode_t580622632 * ___value1, const MethodInfo* method)
{
	{
		Dictionary_2_t2218320536 * L_0 = __this->get_m_Dict_0();
		String_t* L_1 = ___aKey0;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, String_t* >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,SimpleJSON.JSONNode>::ContainsKey(!0) */, L_0, L_1);
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		Dictionary_2_t2218320536 * L_3 = __this->get_m_Dict_0();
		String_t* L_4 = ___aKey0;
		JSONNode_t580622632 * L_5 = ___value1;
		NullCheck(L_3);
		VirtActionInvoker2< String_t*, JSONNode_t580622632 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJSON.JSONNode>::set_Item(!0,!1) */, L_3, L_4, L_5);
		goto IL_0030;
	}

IL_0023:
	{
		Dictionary_2_t2218320536 * L_6 = __this->get_m_Dict_0();
		String_t* L_7 = ___aKey0;
		JSONNode_t580622632 * L_8 = ___value1;
		NullCheck(L_6);
		VirtActionInvoker2< String_t*, JSONNode_t580622632 * >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJSON.JSONNode>::Add(!0,!1) */, L_6, L_7, L_8);
	}

IL_0030:
	{
		return;
	}
}
// SimpleJSON.JSONNode SimpleJSON.JSONClass::get_Item(System.Int32)
extern const MethodInfo* Enumerable_ElementAt_TisKeyValuePair_2_t1706851834_m2914948309_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m1588328981_MethodInfo_var;
extern const uint32_t JSONClass_get_Item_m2990256019_MetadataUsageId;
extern "C"  JSONNode_t580622632 * JSONClass_get_Item_m2990256019 (JSONClass_t3480415118 * __this, int32_t ___aIndex0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONClass_get_Item_m2990256019_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2_t1706851834  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___aIndex0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_1 = ___aIndex0;
		Dictionary_2_t2218320536 * L_2 = __this->get_m_Dict_0();
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<System.String,SimpleJSON.JSONNode>::get_Count() */, L_2);
		if ((((int32_t)L_1) < ((int32_t)L_3)))
		{
			goto IL_001a;
		}
	}

IL_0018:
	{
		return (JSONNode_t580622632 *)NULL;
	}

IL_001a:
	{
		Dictionary_2_t2218320536 * L_4 = __this->get_m_Dict_0();
		int32_t L_5 = ___aIndex0;
		KeyValuePair_2_t1706851834  L_6 = Enumerable_ElementAt_TisKeyValuePair_2_t1706851834_m2914948309(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/Enumerable_ElementAt_TisKeyValuePair_2_t1706851834_m2914948309_MethodInfo_var);
		V_0 = L_6;
		JSONNode_t580622632 * L_7 = KeyValuePair_2_get_Value_m1588328981((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m1588328981_MethodInfo_var);
		return L_7;
	}
}
// System.Void SimpleJSON.JSONClass::set_Item(System.Int32,SimpleJSON.JSONNode)
extern const MethodInfo* Enumerable_ElementAt_TisKeyValuePair_2_t1706851834_m2914948309_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m2683132738_MethodInfo_var;
extern const uint32_t JSONClass_set_Item_m4095592864_MetadataUsageId;
extern "C"  void JSONClass_set_Item_m4095592864 (JSONClass_t3480415118 * __this, int32_t ___aIndex0, JSONNode_t580622632 * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONClass_set_Item_m4095592864_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	KeyValuePair_2_t1706851834  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		int32_t L_0 = ___aIndex0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_1 = ___aIndex0;
		Dictionary_2_t2218320536 * L_2 = __this->get_m_Dict_0();
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<System.String,SimpleJSON.JSONNode>::get_Count() */, L_2);
		if ((((int32_t)L_1) < ((int32_t)L_3)))
		{
			goto IL_0019;
		}
	}

IL_0018:
	{
		return;
	}

IL_0019:
	{
		Dictionary_2_t2218320536 * L_4 = __this->get_m_Dict_0();
		int32_t L_5 = ___aIndex0;
		KeyValuePair_2_t1706851834  L_6 = Enumerable_ElementAt_TisKeyValuePair_2_t1706851834_m2914948309(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/Enumerable_ElementAt_TisKeyValuePair_2_t1706851834_m2914948309_MethodInfo_var);
		V_1 = L_6;
		String_t* L_7 = KeyValuePair_2_get_Key_m2683132738((&V_1), /*hidden argument*/KeyValuePair_2_get_Key_m2683132738_MethodInfo_var);
		V_0 = L_7;
		Dictionary_2_t2218320536 * L_8 = __this->get_m_Dict_0();
		String_t* L_9 = V_0;
		JSONNode_t580622632 * L_10 = ___value1;
		NullCheck(L_8);
		VirtActionInvoker2< String_t*, JSONNode_t580622632 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJSON.JSONNode>::set_Item(!0,!1) */, L_8, L_9, L_10);
		return;
	}
}
// System.Int32 SimpleJSON.JSONClass::get_Count()
extern "C"  int32_t JSONClass_get_Count_m1607612385 (JSONClass_t3480415118 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2218320536 * L_0 = __this->get_m_Dict_0();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<System.String,SimpleJSON.JSONNode>::get_Count() */, L_0);
		return L_1;
	}
}
// System.Void SimpleJSON.JSONClass::Add(System.String,SimpleJSON.JSONNode)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Guid_t2778838590_il2cpp_TypeInfo_var;
extern const uint32_t JSONClass_Add_m3540251102_MetadataUsageId;
extern "C"  void JSONClass_Add_m3540251102 (JSONClass_t3480415118 * __this, String_t* ___aKey0, JSONNode_t580622632 * ___aItem1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONClass_Add_m3540251102_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Guid_t2778838590  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		String_t* L_0 = ___aKey0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0040;
		}
	}
	{
		Dictionary_2_t2218320536 * L_2 = __this->get_m_Dict_0();
		String_t* L_3 = ___aKey0;
		NullCheck(L_2);
		bool L_4 = VirtFuncInvoker1< bool, String_t* >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,SimpleJSON.JSONNode>::ContainsKey(!0) */, L_2, L_3);
		if (!L_4)
		{
			goto IL_002e;
		}
	}
	{
		Dictionary_2_t2218320536 * L_5 = __this->get_m_Dict_0();
		String_t* L_6 = ___aKey0;
		JSONNode_t580622632 * L_7 = ___aItem1;
		NullCheck(L_5);
		VirtActionInvoker2< String_t*, JSONNode_t580622632 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJSON.JSONNode>::set_Item(!0,!1) */, L_5, L_6, L_7);
		goto IL_003b;
	}

IL_002e:
	{
		Dictionary_2_t2218320536 * L_8 = __this->get_m_Dict_0();
		String_t* L_9 = ___aKey0;
		JSONNode_t580622632 * L_10 = ___aItem1;
		NullCheck(L_8);
		VirtActionInvoker2< String_t*, JSONNode_t580622632 * >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJSON.JSONNode>::Add(!0,!1) */, L_8, L_9, L_10);
	}

IL_003b:
	{
		goto IL_0059;
	}

IL_0040:
	{
		Dictionary_2_t2218320536 * L_11 = __this->get_m_Dict_0();
		IL2CPP_RUNTIME_CLASS_INIT(Guid_t2778838590_il2cpp_TypeInfo_var);
		Guid_t2778838590  L_12 = Guid_NewGuid_m3560729310(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_12;
		String_t* L_13 = Guid_ToString_m2528531937((&V_0), /*hidden argument*/NULL);
		JSONNode_t580622632 * L_14 = ___aItem1;
		NullCheck(L_11);
		VirtActionInvoker2< String_t*, JSONNode_t580622632 * >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJSON.JSONNode>::Add(!0,!1) */, L_11, L_13, L_14);
	}

IL_0059:
	{
		return;
	}
}
// SimpleJSON.JSONNode SimpleJSON.JSONClass::Remove(System.String)
extern "C"  JSONNode_t580622632 * JSONClass_Remove_m384126104 (JSONClass_t3480415118 * __this, String_t* ___aKey0, const MethodInfo* method)
{
	JSONNode_t580622632 * V_0 = NULL;
	{
		Dictionary_2_t2218320536 * L_0 = __this->get_m_Dict_0();
		String_t* L_1 = ___aKey0;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, String_t* >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,SimpleJSON.JSONNode>::ContainsKey(!0) */, L_0, L_1);
		if (L_2)
		{
			goto IL_0013;
		}
	}
	{
		return (JSONNode_t580622632 *)NULL;
	}

IL_0013:
	{
		Dictionary_2_t2218320536 * L_3 = __this->get_m_Dict_0();
		String_t* L_4 = ___aKey0;
		NullCheck(L_3);
		JSONNode_t580622632 * L_5 = VirtFuncInvoker1< JSONNode_t580622632 *, String_t* >::Invoke(22 /* !1 System.Collections.Generic.Dictionary`2<System.String,SimpleJSON.JSONNode>::get_Item(!0) */, L_3, L_4);
		V_0 = L_5;
		Dictionary_2_t2218320536 * L_6 = __this->get_m_Dict_0();
		String_t* L_7 = ___aKey0;
		NullCheck(L_6);
		VirtFuncInvoker1< bool, String_t* >::Invoke(20 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,SimpleJSON.JSONNode>::Remove(!0) */, L_6, L_7);
		JSONNode_t580622632 * L_8 = V_0;
		return L_8;
	}
}
// SimpleJSON.JSONNode SimpleJSON.JSONClass::Remove(System.Int32)
extern const MethodInfo* Enumerable_ElementAt_TisKeyValuePair_2_t1706851834_m2914948309_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m2683132738_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m1588328981_MethodInfo_var;
extern const uint32_t JSONClass_Remove_m1521679611_MetadataUsageId;
extern "C"  JSONNode_t580622632 * JSONClass_Remove_m1521679611 (JSONClass_t3480415118 * __this, int32_t ___aIndex0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONClass_Remove_m1521679611_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2_t1706851834  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___aIndex0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_1 = ___aIndex0;
		Dictionary_2_t2218320536 * L_2 = __this->get_m_Dict_0();
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<System.String,SimpleJSON.JSONNode>::get_Count() */, L_2);
		if ((((int32_t)L_1) < ((int32_t)L_3)))
		{
			goto IL_001a;
		}
	}

IL_0018:
	{
		return (JSONNode_t580622632 *)NULL;
	}

IL_001a:
	{
		Dictionary_2_t2218320536 * L_4 = __this->get_m_Dict_0();
		int32_t L_5 = ___aIndex0;
		KeyValuePair_2_t1706851834  L_6 = Enumerable_ElementAt_TisKeyValuePair_2_t1706851834_m2914948309(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/Enumerable_ElementAt_TisKeyValuePair_2_t1706851834_m2914948309_MethodInfo_var);
		V_0 = L_6;
		Dictionary_2_t2218320536 * L_7 = __this->get_m_Dict_0();
		String_t* L_8 = KeyValuePair_2_get_Key_m2683132738((&V_0), /*hidden argument*/KeyValuePair_2_get_Key_m2683132738_MethodInfo_var);
		NullCheck(L_7);
		VirtFuncInvoker1< bool, String_t* >::Invoke(20 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,SimpleJSON.JSONNode>::Remove(!0) */, L_7, L_8);
		JSONNode_t580622632 * L_9 = KeyValuePair_2_get_Value_m1588328981((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m1588328981_MethodInfo_var);
		return L_9;
	}
}
// SimpleJSON.JSONNode SimpleJSON.JSONClass::Remove(SimpleJSON.JSONNode)
extern Il2CppClass* U3CRemoveU3Ec__AnonStorey2E_t1556685308_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t124259971_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CRemoveU3Ec__AnonStorey2E_U3CU3Em__8_m1921890812_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m364554062_MethodInfo_var;
extern const MethodInfo* Enumerable_Where_TisKeyValuePair_2_t1706851834_m879253725_MethodInfo_var;
extern const MethodInfo* Enumerable_First_TisKeyValuePair_2_t1706851834_m4161586049_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m2683132738_MethodInfo_var;
extern const uint32_t JSONClass_Remove_m3286637322_MetadataUsageId;
extern "C"  JSONNode_t580622632 * JSONClass_Remove_m3286637322 (JSONClass_t3480415118 * __this, JSONNode_t580622632 * ___aNode0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONClass_Remove_m3286637322_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2_t1706851834  V_0;
	memset(&V_0, 0, sizeof(V_0));
	U3CRemoveU3Ec__AnonStorey2E_t1556685308 * V_1 = NULL;
	JSONNode_t580622632 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		U3CRemoveU3Ec__AnonStorey2E_t1556685308 * L_0 = (U3CRemoveU3Ec__AnonStorey2E_t1556685308 *)il2cpp_codegen_object_new(U3CRemoveU3Ec__AnonStorey2E_t1556685308_il2cpp_TypeInfo_var);
		U3CRemoveU3Ec__AnonStorey2E__ctor_m2732807750(L_0, /*hidden argument*/NULL);
		V_1 = L_0;
		U3CRemoveU3Ec__AnonStorey2E_t1556685308 * L_1 = V_1;
		JSONNode_t580622632 * L_2 = ___aNode0;
		NullCheck(L_1);
		L_1->set_aNode_0(L_2);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Dictionary_2_t2218320536 * L_3 = __this->get_m_Dict_0();
			U3CRemoveU3Ec__AnonStorey2E_t1556685308 * L_4 = V_1;
			IntPtr_t L_5;
			L_5.set_m_value_0((void*)(void*)U3CRemoveU3Ec__AnonStorey2E_U3CU3Em__8_m1921890812_MethodInfo_var);
			Func_2_t124259971 * L_6 = (Func_2_t124259971 *)il2cpp_codegen_object_new(Func_2_t124259971_il2cpp_TypeInfo_var);
			Func_2__ctor_m364554062(L_6, L_4, L_5, /*hidden argument*/Func_2__ctor_m364554062_MethodInfo_var);
			Il2CppObject* L_7 = Enumerable_Where_TisKeyValuePair_2_t1706851834_m879253725(NULL /*static, unused*/, L_3, L_6, /*hidden argument*/Enumerable_Where_TisKeyValuePair_2_t1706851834_m879253725_MethodInfo_var);
			KeyValuePair_2_t1706851834  L_8 = Enumerable_First_TisKeyValuePair_2_t1706851834_m4161586049(NULL /*static, unused*/, L_7, /*hidden argument*/Enumerable_First_TisKeyValuePair_2_t1706851834_m4161586049_MethodInfo_var);
			V_0 = L_8;
			Dictionary_2_t2218320536 * L_9 = __this->get_m_Dict_0();
			String_t* L_10 = KeyValuePair_2_get_Key_m2683132738((&V_0), /*hidden argument*/KeyValuePair_2_get_Key_m2683132738_MethodInfo_var);
			NullCheck(L_9);
			VirtFuncInvoker1< bool, String_t* >::Invoke(20 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,SimpleJSON.JSONNode>::Remove(!0) */, L_9, L_10);
			U3CRemoveU3Ec__AnonStorey2E_t1556685308 * L_11 = V_1;
			NullCheck(L_11);
			JSONNode_t580622632 * L_12 = L_11->get_aNode_0();
			V_2 = L_12;
			goto IL_005b;
		}

IL_0049:
		{
			; // IL_0049: leave IL_005b
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_004e;
		throw e;
	}

CATCH_004e:
	{ // begin catch(System.Object)
		{
			V_2 = (JSONNode_t580622632 *)NULL;
			goto IL_005b;
		}

IL_0056:
		{
			; // IL_0056: leave IL_005b
		}
	} // end catch (depth: 1)

IL_005b:
	{
		JSONNode_t580622632 * L_13 = V_2;
		return L_13;
	}
}
// System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode> SimpleJSON.JSONClass::get_Childs()
extern Il2CppClass* U3CU3Ec__Iterator4_t2516598213_il2cpp_TypeInfo_var;
extern const uint32_t JSONClass_get_Childs_m2065873467_MetadataUsageId;
extern "C"  Il2CppObject* JSONClass_get_Childs_m2065873467 (JSONClass_t3480415118 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONClass_get_Childs_m2065873467_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__Iterator4_t2516598213 * V_0 = NULL;
	{
		U3CU3Ec__Iterator4_t2516598213 * L_0 = (U3CU3Ec__Iterator4_t2516598213 *)il2cpp_codegen_object_new(U3CU3Ec__Iterator4_t2516598213_il2cpp_TypeInfo_var);
		U3CU3Ec__Iterator4__ctor_m3068457279(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__Iterator4_t2516598213 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_4(__this);
		U3CU3Ec__Iterator4_t2516598213 * L_2 = V_0;
		U3CU3Ec__Iterator4_t2516598213 * L_3 = L_2;
		NullCheck(L_3);
		L_3->set_U24PC_2(((int32_t)-2));
		return L_3;
	}
}
// System.Collections.IEnumerator SimpleJSON.JSONClass::GetEnumerator()
extern Il2CppClass* U3CGetEnumeratorU3Ec__Iterator5_t490656868_il2cpp_TypeInfo_var;
extern const uint32_t JSONClass_GetEnumerator_m3348823995_MetadataUsageId;
extern "C"  Il2CppObject * JSONClass_GetEnumerator_m3348823995 (JSONClass_t3480415118 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONClass_GetEnumerator_m3348823995_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetEnumeratorU3Ec__Iterator5_t490656868 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator5_t490656868 * L_0 = (U3CGetEnumeratorU3Ec__Iterator5_t490656868 *)il2cpp_codegen_object_new(U3CGetEnumeratorU3Ec__Iterator5_t490656868_il2cpp_TypeInfo_var);
		U3CGetEnumeratorU3Ec__Iterator5__ctor_m1110024542(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetEnumeratorU3Ec__Iterator5_t490656868 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_4(__this);
		U3CGetEnumeratorU3Ec__Iterator5_t490656868 * L_2 = V_0;
		return L_2;
	}
}
// System.String SimpleJSON.JSONClass::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t1985348477_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m1212952132_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m427985358_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m2683132738_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m1588328981_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m12398705_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral123;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral34;
extern Il2CppCodeGenString* _stringLiteral1112;
extern Il2CppCodeGenString* _stringLiteral125;
extern const uint32_t JSONClass_ToString_m2639089888_MetadataUsageId;
extern "C"  String_t* JSONClass_ToString_m2639089888 (JSONClass_t3480415118 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONClass_ToString_m2639089888_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	KeyValuePair_2_t1706851834  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Enumerator_t1985348477  V_2;
	memset(&V_2, 0, sizeof(V_2));
	String_t* V_3 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = _stringLiteral123;
		Dictionary_2_t2218320536 * L_0 = __this->get_m_Dict_0();
		NullCheck(L_0);
		Enumerator_t1985348477  L_1 = Dictionary_2_GetEnumerator_m1212952132(L_0, /*hidden argument*/Dictionary_2_GetEnumerator_m1212952132_MethodInfo_var);
		V_2 = L_1;
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0077;
		}

IL_0017:
		{
			KeyValuePair_2_t1706851834  L_2 = Enumerator_get_Current_m427985358((&V_2), /*hidden argument*/Enumerator_get_Current_m427985358_MethodInfo_var);
			V_1 = L_2;
			String_t* L_3 = V_0;
			NullCheck(L_3);
			int32_t L_4 = String_get_Length_m2979997331(L_3, /*hidden argument*/NULL);
			if ((((int32_t)L_4) <= ((int32_t)2)))
			{
				goto IL_0037;
			}
		}

IL_002b:
		{
			String_t* L_5 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_6 = String_Concat_m138640077(NULL /*static, unused*/, L_5, _stringLiteral1396, /*hidden argument*/NULL);
			V_0 = L_6;
		}

IL_0037:
		{
			String_t* L_7 = V_0;
			V_3 = L_7;
			StringU5BU5D_t2956870243* L_8 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)5));
			String_t* L_9 = V_3;
			NullCheck(L_8);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
			ArrayElementTypeCheck (L_8, L_9);
			(L_8)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_9);
			StringU5BU5D_t2956870243* L_10 = L_8;
			NullCheck(L_10);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 1);
			ArrayElementTypeCheck (L_10, _stringLiteral34);
			(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral34);
			StringU5BU5D_t2956870243* L_11 = L_10;
			String_t* L_12 = KeyValuePair_2_get_Key_m2683132738((&V_1), /*hidden argument*/KeyValuePair_2_get_Key_m2683132738_MethodInfo_var);
			String_t* L_13 = JSONNode_Escape_m3126929209(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
			NullCheck(L_11);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 2);
			ArrayElementTypeCheck (L_11, L_13);
			(L_11)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_13);
			StringU5BU5D_t2956870243* L_14 = L_11;
			NullCheck(L_14);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 3);
			ArrayElementTypeCheck (L_14, _stringLiteral1112);
			(L_14)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral1112);
			StringU5BU5D_t2956870243* L_15 = L_14;
			JSONNode_t580622632 * L_16 = KeyValuePair_2_get_Value_m1588328981((&V_1), /*hidden argument*/KeyValuePair_2_get_Value_m1588328981_MethodInfo_var);
			NullCheck(L_16);
			String_t* L_17 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String SimpleJSON.JSONNode::ToString() */, L_16);
			NullCheck(L_15);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 4);
			ArrayElementTypeCheck (L_15, L_17);
			(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)L_17);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_18 = String_Concat_m21867311(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
			V_0 = L_18;
		}

IL_0077:
		{
			bool L_19 = Enumerator_MoveNext_m12398705((&V_2), /*hidden argument*/Enumerator_MoveNext_m12398705_MethodInfo_var);
			if (L_19)
			{
				goto IL_0017;
			}
		}

IL_0083:
		{
			IL2CPP_LEAVE(0x94, FINALLY_0088);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0088;
	}

FINALLY_0088:
	{ // begin finally (depth: 1)
		Enumerator_t1985348477  L_20 = V_2;
		Enumerator_t1985348477  L_21 = L_20;
		Il2CppObject * L_22 = Box(Enumerator_t1985348477_il2cpp_TypeInfo_var, &L_21);
		NullCheck((Il2CppObject *)L_22);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_22);
		IL2CPP_END_FINALLY(136)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(136)
	{
		IL2CPP_JUMP_TBL(0x94, IL_0094)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0094:
	{
		String_t* L_23 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_24 = String_Concat_m138640077(NULL /*static, unused*/, L_23, _stringLiteral125, /*hidden argument*/NULL);
		V_0 = L_24;
		String_t* L_25 = V_0;
		return L_25;
	}
}
// System.String SimpleJSON.JSONClass::ToString(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t1985348477_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m1212952132_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m427985358_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m2683132738_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m1588328981_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m12398705_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3845;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral10;
extern Il2CppCodeGenString* _stringLiteral31776;
extern Il2CppCodeGenString* _stringLiteral34;
extern Il2CppCodeGenString* _stringLiteral1045476;
extern Il2CppCodeGenString* _stringLiteral125;
extern const uint32_t JSONClass_ToString_m4014389794_MetadataUsageId;
extern "C"  String_t* JSONClass_ToString_m4014389794 (JSONClass_t3480415118 * __this, String_t* ___aPrefix0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONClass_ToString_m4014389794_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	KeyValuePair_2_t1706851834  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Enumerator_t1985348477  V_2;
	memset(&V_2, 0, sizeof(V_2));
	String_t* V_3 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = _stringLiteral3845;
		Dictionary_2_t2218320536 * L_0 = __this->get_m_Dict_0();
		NullCheck(L_0);
		Enumerator_t1985348477  L_1 = Dictionary_2_GetEnumerator_m1212952132(L_0, /*hidden argument*/Dictionary_2_GetEnumerator_m1212952132_MethodInfo_var);
		V_2 = L_1;
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0094;
		}

IL_0017:
		{
			KeyValuePair_2_t1706851834  L_2 = Enumerator_get_Current_m427985358((&V_2), /*hidden argument*/Enumerator_get_Current_m427985358_MethodInfo_var);
			V_1 = L_2;
			String_t* L_3 = V_0;
			NullCheck(L_3);
			int32_t L_4 = String_get_Length_m2979997331(L_3, /*hidden argument*/NULL);
			if ((((int32_t)L_4) <= ((int32_t)3)))
			{
				goto IL_0037;
			}
		}

IL_002b:
		{
			String_t* L_5 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_6 = String_Concat_m138640077(NULL /*static, unused*/, L_5, _stringLiteral1396, /*hidden argument*/NULL);
			V_0 = L_6;
		}

IL_0037:
		{
			String_t* L_7 = V_0;
			String_t* L_8 = ___aPrefix0;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_9 = String_Concat_m2933632197(NULL /*static, unused*/, L_7, _stringLiteral10, L_8, _stringLiteral31776, /*hidden argument*/NULL);
			V_0 = L_9;
			String_t* L_10 = V_0;
			V_3 = L_10;
			StringU5BU5D_t2956870243* L_11 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)5));
			String_t* L_12 = V_3;
			NullCheck(L_11);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 0);
			ArrayElementTypeCheck (L_11, L_12);
			(L_11)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_12);
			StringU5BU5D_t2956870243* L_13 = L_11;
			NullCheck(L_13);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 1);
			ArrayElementTypeCheck (L_13, _stringLiteral34);
			(L_13)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral34);
			StringU5BU5D_t2956870243* L_14 = L_13;
			String_t* L_15 = KeyValuePair_2_get_Key_m2683132738((&V_1), /*hidden argument*/KeyValuePair_2_get_Key_m2683132738_MethodInfo_var);
			String_t* L_16 = JSONNode_Escape_m3126929209(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
			NullCheck(L_14);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 2);
			ArrayElementTypeCheck (L_14, L_16);
			(L_14)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_16);
			StringU5BU5D_t2956870243* L_17 = L_14;
			NullCheck(L_17);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 3);
			ArrayElementTypeCheck (L_17, _stringLiteral1045476);
			(L_17)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral1045476);
			StringU5BU5D_t2956870243* L_18 = L_17;
			JSONNode_t580622632 * L_19 = KeyValuePair_2_get_Value_m1588328981((&V_1), /*hidden argument*/KeyValuePair_2_get_Value_m1588328981_MethodInfo_var);
			String_t* L_20 = ___aPrefix0;
			String_t* L_21 = String_Concat_m138640077(NULL /*static, unused*/, L_20, _stringLiteral31776, /*hidden argument*/NULL);
			NullCheck(L_19);
			String_t* L_22 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(17 /* System.String SimpleJSON.JSONNode::ToString(System.String) */, L_19, L_21);
			NullCheck(L_18);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 4);
			ArrayElementTypeCheck (L_18, L_22);
			(L_18)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)L_22);
			String_t* L_23 = String_Concat_m21867311(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
			V_0 = L_23;
		}

IL_0094:
		{
			bool L_24 = Enumerator_MoveNext_m12398705((&V_2), /*hidden argument*/Enumerator_MoveNext_m12398705_MethodInfo_var);
			if (L_24)
			{
				goto IL_0017;
			}
		}

IL_00a0:
		{
			IL2CPP_LEAVE(0xB1, FINALLY_00a5);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00a5;
	}

FINALLY_00a5:
	{ // begin finally (depth: 1)
		Enumerator_t1985348477  L_25 = V_2;
		Enumerator_t1985348477  L_26 = L_25;
		Il2CppObject * L_27 = Box(Enumerator_t1985348477_il2cpp_TypeInfo_var, &L_26);
		NullCheck((Il2CppObject *)L_27);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_27);
		IL2CPP_END_FINALLY(165)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(165)
	{
		IL2CPP_JUMP_TBL(0xB1, IL_00b1)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00b1:
	{
		String_t* L_28 = V_0;
		String_t* L_29 = ___aPrefix0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_30 = String_Concat_m2933632197(NULL /*static, unused*/, L_28, _stringLiteral10, L_29, _stringLiteral125, /*hidden argument*/NULL);
		V_0 = L_30;
		String_t* L_31 = V_0;
		return L_31;
	}
}
// System.Void SimpleJSON.JSONClass::Serialize(System.IO.BinaryWriter)
extern Il2CppClass* Enumerator_t1985348478_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Keys_m999280912_MethodInfo_var;
extern const MethodInfo* KeyCollection_GetEnumerator_m3961372000_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m3744446609_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m3052051743_MethodInfo_var;
extern const uint32_t JSONClass_Serialize_m990138388_MetadataUsageId;
extern "C"  void JSONClass_Serialize_m990138388 (JSONClass_t3480415118 * __this, BinaryWriter_t2314211483 * ___aWriter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONClass_Serialize_m990138388_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	Enumerator_t1985348478  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		BinaryWriter_t2314211483 * L_0 = ___aWriter0;
		NullCheck(L_0);
		VirtActionInvoker1< uint8_t >::Invoke(8 /* System.Void System.IO.BinaryWriter::Write(System.Byte) */, L_0, 2);
		BinaryWriter_t2314211483 * L_1 = ___aWriter0;
		Dictionary_2_t2218320536 * L_2 = __this->get_m_Dict_0();
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<System.String,SimpleJSON.JSONNode>::get_Count() */, L_2);
		NullCheck(L_1);
		VirtActionInvoker1< int32_t >::Invoke(16 /* System.Void System.IO.BinaryWriter::Write(System.Int32) */, L_1, L_3);
		Dictionary_2_t2218320536 * L_4 = __this->get_m_Dict_0();
		NullCheck(L_4);
		KeyCollection_t246628520 * L_5 = Dictionary_2_get_Keys_m999280912(L_4, /*hidden argument*/Dictionary_2_get_Keys_m999280912_MethodInfo_var);
		NullCheck(L_5);
		Enumerator_t1985348478  L_6 = KeyCollection_GetEnumerator_m3961372000(L_5, /*hidden argument*/KeyCollection_GetEnumerator_m3961372000_MethodInfo_var);
		V_1 = L_6;
	}

IL_0029:
	try
	{ // begin try (depth: 1)
		{
			goto IL_004f;
		}

IL_002e:
		{
			String_t* L_7 = Enumerator_get_Current_m3744446609((&V_1), /*hidden argument*/Enumerator_get_Current_m3744446609_MethodInfo_var);
			V_0 = L_7;
			BinaryWriter_t2314211483 * L_8 = ___aWriter0;
			String_t* L_9 = V_0;
			NullCheck(L_8);
			VirtActionInvoker1< String_t* >::Invoke(20 /* System.Void System.IO.BinaryWriter::Write(System.String) */, L_8, L_9);
			Dictionary_2_t2218320536 * L_10 = __this->get_m_Dict_0();
			String_t* L_11 = V_0;
			NullCheck(L_10);
			JSONNode_t580622632 * L_12 = VirtFuncInvoker1< JSONNode_t580622632 *, String_t* >::Invoke(22 /* !1 System.Collections.Generic.Dictionary`2<System.String,SimpleJSON.JSONNode>::get_Item(!0) */, L_10, L_11);
			BinaryWriter_t2314211483 * L_13 = ___aWriter0;
			NullCheck(L_12);
			VirtActionInvoker1< BinaryWriter_t2314211483 * >::Invoke(28 /* System.Void SimpleJSON.JSONNode::Serialize(System.IO.BinaryWriter) */, L_12, L_13);
		}

IL_004f:
		{
			bool L_14 = Enumerator_MoveNext_m3052051743((&V_1), /*hidden argument*/Enumerator_MoveNext_m3052051743_MethodInfo_var);
			if (L_14)
			{
				goto IL_002e;
			}
		}

IL_005b:
		{
			IL2CPP_LEAVE(0x6C, FINALLY_0060);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0060;
	}

FINALLY_0060:
	{ // begin finally (depth: 1)
		Enumerator_t1985348478  L_15 = V_1;
		Enumerator_t1985348478  L_16 = L_15;
		Il2CppObject * L_17 = Box(Enumerator_t1985348478_il2cpp_TypeInfo_var, &L_16);
		NullCheck((Il2CppObject *)L_17);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_17);
		IL2CPP_END_FINALLY(96)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(96)
	{
		IL2CPP_JUMP_TBL(0x6C, IL_006c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_006c:
	{
		return;
	}
}
// System.Void SimpleJSON.JSONClass/<>c__Iterator4::.ctor()
extern "C"  void U3CU3Ec__Iterator4__ctor_m3068457279 (U3CU3Ec__Iterator4_t2516598213 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// SimpleJSON.JSONNode SimpleJSON.JSONClass/<>c__Iterator4::System.Collections.Generic.IEnumerator<SimpleJSON.JSONNode>.get_Current()
extern "C"  JSONNode_t580622632 * U3CU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m1985878130 (U3CU3Ec__Iterator4_t2516598213 * __this, const MethodInfo* method)
{
	{
		JSONNode_t580622632 * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Object SimpleJSON.JSONClass/<>c__Iterator4::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m3197667975 (U3CU3Ec__Iterator4_t2516598213 * __this, const MethodInfo* method)
{
	{
		JSONNode_t580622632 * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Collections.IEnumerator SimpleJSON.JSONClass/<>c__Iterator4::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CU3Ec__Iterator4_System_Collections_IEnumerable_GetEnumerator_m3095102810 (U3CU3Ec__Iterator4_t2516598213 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = U3CU3Ec__Iterator4_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m1155610441(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONClass/<>c__Iterator4::System.Collections.Generic.IEnumerable<SimpleJSON.JSONNode>.GetEnumerator()
extern Il2CppClass* U3CU3Ec__Iterator4_t2516598213_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec__Iterator4_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m1155610441_MetadataUsageId;
extern "C"  Il2CppObject* U3CU3Ec__Iterator4_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m1155610441 (U3CU3Ec__Iterator4_t2516598213 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__Iterator4_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m1155610441_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__Iterator4_t2516598213 * V_0 = NULL;
	{
		int32_t* L_0 = __this->get_address_of_U24PC_2();
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, L_0, 0, ((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CU3Ec__Iterator4_t2516598213 * L_2 = (U3CU3Ec__Iterator4_t2516598213 *)il2cpp_codegen_object_new(U3CU3Ec__Iterator4_t2516598213_il2cpp_TypeInfo_var);
		U3CU3Ec__Iterator4__ctor_m3068457279(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		U3CU3Ec__Iterator4_t2516598213 * L_3 = V_0;
		JSONClass_t3480415118 * L_4 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_4(L_4);
		U3CU3Ec__Iterator4_t2516598213 * L_5 = V_0;
		return L_5;
	}
}
// System.Boolean SimpleJSON.JSONClass/<>c__Iterator4::MoveNext()
extern Il2CppClass* Enumerator_t1985348477_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m1212952132_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m427985358_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m1588328981_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m12398705_MethodInfo_var;
extern const uint32_t U3CU3Ec__Iterator4_MoveNext_m2202537581_MetadataUsageId;
extern "C"  bool U3CU3Ec__Iterator4_MoveNext_m2202537581 (U3CU3Ec__Iterator4_t2516598213 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__Iterator4_MoveNext_m2202537581_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_2();
		V_0 = L_0;
		__this->set_U24PC_2((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_003c;
		}
	}
	{
		goto IL_00ae;
	}

IL_0023:
	{
		JSONClass_t3480415118 * L_2 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_2);
		Dictionary_2_t2218320536 * L_3 = L_2->get_m_Dict_0();
		NullCheck(L_3);
		Enumerator_t1985348477  L_4 = Dictionary_2_GetEnumerator_m1212952132(L_3, /*hidden argument*/Dictionary_2_GetEnumerator_m1212952132_MethodInfo_var);
		__this->set_U3CU24s_24U3E__0_0(L_4);
		V_0 = ((int32_t)-3);
	}

IL_003c:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_5 = V_0;
			if (((int32_t)((int32_t)L_5-(int32_t)1)) == 0)
			{
				goto IL_007d;
			}
		}

IL_0048:
		{
			goto IL_007d;
		}

IL_004d:
		{
			Enumerator_t1985348477 * L_6 = __this->get_address_of_U3CU24s_24U3E__0_0();
			KeyValuePair_2_t1706851834  L_7 = Enumerator_get_Current_m427985358(L_6, /*hidden argument*/Enumerator_get_Current_m427985358_MethodInfo_var);
			__this->set_U3CNU3E__1_1(L_7);
			KeyValuePair_2_t1706851834 * L_8 = __this->get_address_of_U3CNU3E__1_1();
			JSONNode_t580622632 * L_9 = KeyValuePair_2_get_Value_m1588328981(L_8, /*hidden argument*/KeyValuePair_2_get_Value_m1588328981_MethodInfo_var);
			__this->set_U24current_3(L_9);
			__this->set_U24PC_2(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xB0, FINALLY_0092);
		}

IL_007d:
		{
			Enumerator_t1985348477 * L_10 = __this->get_address_of_U3CU24s_24U3E__0_0();
			bool L_11 = Enumerator_MoveNext_m12398705(L_10, /*hidden argument*/Enumerator_MoveNext_m12398705_MethodInfo_var);
			if (L_11)
			{
				goto IL_004d;
			}
		}

IL_008d:
		{
			IL2CPP_LEAVE(0xA7, FINALLY_0092);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0092;
	}

FINALLY_0092:
	{ // begin finally (depth: 1)
		{
			bool L_12 = V_1;
			if (!L_12)
			{
				goto IL_0096;
			}
		}

IL_0095:
		{
			IL2CPP_END_FINALLY(146)
		}

IL_0096:
		{
			Enumerator_t1985348477  L_13 = __this->get_U3CU24s_24U3E__0_0();
			Enumerator_t1985348477  L_14 = L_13;
			Il2CppObject * L_15 = Box(Enumerator_t1985348477_il2cpp_TypeInfo_var, &L_14);
			NullCheck((Il2CppObject *)L_15);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_15);
			IL2CPP_END_FINALLY(146)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(146)
	{
		IL2CPP_JUMP_TBL(0xB0, IL_00b0)
		IL2CPP_JUMP_TBL(0xA7, IL_00a7)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00a7:
	{
		__this->set_U24PC_2((-1));
	}

IL_00ae:
	{
		return (bool)0;
	}

IL_00b0:
	{
		return (bool)1;
	}
	// Dead block : IL_00b2: ldloc.2
}
// System.Void SimpleJSON.JSONClass/<>c__Iterator4::Dispose()
extern Il2CppClass* Enumerator_t1985348477_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec__Iterator4_Dispose_m2338163132_MetadataUsageId;
extern "C"  void U3CU3Ec__Iterator4_Dispose_m2338163132 (U3CU3Ec__Iterator4_t2516598213 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__Iterator4_Dispose_m2338163132_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_2();
		V_0 = L_0;
		__this->set_U24PC_2((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0037;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_0037;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x37, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		Enumerator_t1985348477  L_2 = __this->get_U3CU24s_24U3E__0_0();
		Enumerator_t1985348477  L_3 = L_2;
		Il2CppObject * L_4 = Box(Enumerator_t1985348477_il2cpp_TypeInfo_var, &L_3);
		NullCheck((Il2CppObject *)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
		IL2CPP_END_FINALLY(38)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x37, IL_0037)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0037:
	{
		return;
	}
}
// System.Void SimpleJSON.JSONClass/<>c__Iterator4::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec__Iterator4_Reset_m714890220_MetadataUsageId;
extern "C"  void U3CU3Ec__Iterator4_Reset_m714890220 (U3CU3Ec__Iterator4_t2516598213 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__Iterator4_Reset_m714890220_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void SimpleJSON.JSONClass/<GetEnumerator>c__Iterator5::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator5__ctor_m1110024542 (U3CGetEnumeratorU3Ec__Iterator5_t490656868 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object SimpleJSON.JSONClass/<GetEnumerator>c__Iterator5::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2573584958 (U3CGetEnumeratorU3Ec__Iterator5_t490656868 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Object SimpleJSON.JSONClass/<GetEnumerator>c__Iterator5::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m1312819666 (U3CGetEnumeratorU3Ec__Iterator5_t490656868 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Boolean SimpleJSON.JSONClass/<GetEnumerator>c__Iterator5::MoveNext()
extern Il2CppClass* KeyValuePair_2_t1706851834_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t1985348477_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m1212952132_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m427985358_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m12398705_MethodInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator5_MoveNext_m263862950_MetadataUsageId;
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator5_MoveNext_m263862950 (U3CGetEnumeratorU3Ec__Iterator5_t490656868 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator5_MoveNext_m263862950_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_2();
		V_0 = L_0;
		__this->set_U24PC_2((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_003c;
		}
	}
	{
		goto IL_00ae;
	}

IL_0023:
	{
		JSONClass_t3480415118 * L_2 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_2);
		Dictionary_2_t2218320536 * L_3 = L_2->get_m_Dict_0();
		NullCheck(L_3);
		Enumerator_t1985348477  L_4 = Dictionary_2_GetEnumerator_m1212952132(L_3, /*hidden argument*/Dictionary_2_GetEnumerator_m1212952132_MethodInfo_var);
		__this->set_U3CU24s_25U3E__0_0(L_4);
		V_0 = ((int32_t)-3);
	}

IL_003c:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_5 = V_0;
			if (((int32_t)((int32_t)L_5-(int32_t)1)) == 0)
			{
				goto IL_007d;
			}
		}

IL_0048:
		{
			goto IL_007d;
		}

IL_004d:
		{
			Enumerator_t1985348477 * L_6 = __this->get_address_of_U3CU24s_25U3E__0_0();
			KeyValuePair_2_t1706851834  L_7 = Enumerator_get_Current_m427985358(L_6, /*hidden argument*/Enumerator_get_Current_m427985358_MethodInfo_var);
			__this->set_U3CNU3E__1_1(L_7);
			KeyValuePair_2_t1706851834  L_8 = __this->get_U3CNU3E__1_1();
			KeyValuePair_2_t1706851834  L_9 = L_8;
			Il2CppObject * L_10 = Box(KeyValuePair_2_t1706851834_il2cpp_TypeInfo_var, &L_9);
			__this->set_U24current_3(L_10);
			__this->set_U24PC_2(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xB0, FINALLY_0092);
		}

IL_007d:
		{
			Enumerator_t1985348477 * L_11 = __this->get_address_of_U3CU24s_25U3E__0_0();
			bool L_12 = Enumerator_MoveNext_m12398705(L_11, /*hidden argument*/Enumerator_MoveNext_m12398705_MethodInfo_var);
			if (L_12)
			{
				goto IL_004d;
			}
		}

IL_008d:
		{
			IL2CPP_LEAVE(0xA7, FINALLY_0092);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0092;
	}

FINALLY_0092:
	{ // begin finally (depth: 1)
		{
			bool L_13 = V_1;
			if (!L_13)
			{
				goto IL_0096;
			}
		}

IL_0095:
		{
			IL2CPP_END_FINALLY(146)
		}

IL_0096:
		{
			Enumerator_t1985348477  L_14 = __this->get_U3CU24s_25U3E__0_0();
			Enumerator_t1985348477  L_15 = L_14;
			Il2CppObject * L_16 = Box(Enumerator_t1985348477_il2cpp_TypeInfo_var, &L_15);
			NullCheck((Il2CppObject *)L_16);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_16);
			IL2CPP_END_FINALLY(146)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(146)
	{
		IL2CPP_JUMP_TBL(0xB0, IL_00b0)
		IL2CPP_JUMP_TBL(0xA7, IL_00a7)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00a7:
	{
		__this->set_U24PC_2((-1));
	}

IL_00ae:
	{
		return (bool)0;
	}

IL_00b0:
	{
		return (bool)1;
	}
	// Dead block : IL_00b2: ldloc.2
}
// System.Void SimpleJSON.JSONClass/<GetEnumerator>c__Iterator5::Dispose()
extern Il2CppClass* Enumerator_t1985348477_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator5_Dispose_m1479978523_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator5_Dispose_m1479978523 (U3CGetEnumeratorU3Ec__Iterator5_t490656868 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator5_Dispose_m1479978523_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_2();
		V_0 = L_0;
		__this->set_U24PC_2((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0037;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_0037;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x37, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		Enumerator_t1985348477  L_2 = __this->get_U3CU24s_25U3E__0_0();
		Enumerator_t1985348477  L_3 = L_2;
		Il2CppObject * L_4 = Box(Enumerator_t1985348477_il2cpp_TypeInfo_var, &L_3);
		NullCheck((Il2CppObject *)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
		IL2CPP_END_FINALLY(38)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x37, IL_0037)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0037:
	{
		return;
	}
}
// System.Void SimpleJSON.JSONClass/<GetEnumerator>c__Iterator5::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator5_Reset_m3051424779_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator5_Reset_m3051424779 (U3CGetEnumeratorU3Ec__Iterator5_t490656868 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator5_Reset_m3051424779_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void SimpleJSON.JSONClass/<Remove>c__AnonStorey2E::.ctor()
extern "C"  void U3CRemoveU3Ec__AnonStorey2E__ctor_m2732807750 (U3CRemoveU3Ec__AnonStorey2E_t1556685308 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean SimpleJSON.JSONClass/<Remove>c__AnonStorey2E::<>m__8(System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode>)
extern const MethodInfo* KeyValuePair_2_get_Value_m1588328981_MethodInfo_var;
extern const uint32_t U3CRemoveU3Ec__AnonStorey2E_U3CU3Em__8_m1921890812_MetadataUsageId;
extern "C"  bool U3CRemoveU3Ec__AnonStorey2E_U3CU3Em__8_m1921890812 (U3CRemoveU3Ec__AnonStorey2E_t1556685308 * __this, KeyValuePair_2_t1706851834  ___k0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CRemoveU3Ec__AnonStorey2E_U3CU3Em__8_m1921890812_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		JSONNode_t580622632 * L_0 = KeyValuePair_2_get_Value_m1588328981((&___k0), /*hidden argument*/KeyValuePair_2_get_Value_m1588328981_MethodInfo_var);
		JSONNode_t580622632 * L_1 = __this->get_aNode_0();
		bool L_2 = JSONNode_op_Equality_m912335575(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void SimpleJSON.JSONData::.ctor(System.String)
extern "C"  void JSONData__ctor_m1923599889 (JSONData_t580311760 * __this, String_t* ___aData0, const MethodInfo* method)
{
	{
		JSONNode__ctor_m39807289(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___aData0;
		__this->set_m_Data_0(L_0);
		return;
	}
}
// System.Void SimpleJSON.JSONData::.ctor(System.Single)
extern "C"  void JSONData__ctor_m1604923578 (JSONData_t580311760 * __this, float ___aData0, const MethodInfo* method)
{
	{
		JSONNode__ctor_m39807289(__this, /*hidden argument*/NULL);
		float L_0 = ___aData0;
		VirtActionInvoker1< float >::Invoke(21 /* System.Void SimpleJSON.JSONNode::set_AsFloat(System.Single) */, __this, L_0);
		return;
	}
}
// System.Void SimpleJSON.JSONData::.ctor(System.Double)
extern "C"  void JSONData__ctor_m1355360849 (JSONData_t580311760 * __this, double ___aData0, const MethodInfo* method)
{
	{
		JSONNode__ctor_m39807289(__this, /*hidden argument*/NULL);
		double L_0 = ___aData0;
		VirtActionInvoker1< double >::Invoke(23 /* System.Void SimpleJSON.JSONNode::set_AsDouble(System.Double) */, __this, L_0);
		return;
	}
}
// System.Void SimpleJSON.JSONData::.ctor(System.Boolean)
extern "C"  void JSONData__ctor_m4008077384 (JSONData_t580311760 * __this, bool ___aData0, const MethodInfo* method)
{
	{
		JSONNode__ctor_m39807289(__this, /*hidden argument*/NULL);
		bool L_0 = ___aData0;
		VirtActionInvoker1< bool >::Invoke(25 /* System.Void SimpleJSON.JSONNode::set_AsBool(System.Boolean) */, __this, L_0);
		return;
	}
}
// System.Void SimpleJSON.JSONData::.ctor(System.Int32)
extern "C"  void JSONData__ctor_m4065192034 (JSONData_t580311760 * __this, int32_t ___aData0, const MethodInfo* method)
{
	{
		JSONNode__ctor_m39807289(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___aData0;
		VirtActionInvoker1< int32_t >::Invoke(19 /* System.Void SimpleJSON.JSONNode::set_AsInt(System.Int32) */, __this, L_0);
		return;
	}
}
// System.String SimpleJSON.JSONData::get_Value()
extern "C"  String_t* JSONData_get_Value_m3294574362 (JSONData_t580311760 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_m_Data_0();
		return L_0;
	}
}
// System.Void SimpleJSON.JSONData::set_Value(System.String)
extern "C"  void JSONData_set_Value_m678588127 (JSONData_t580311760 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_m_Data_0(L_0);
		return;
	}
}
// System.String SimpleJSON.JSONData::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral34;
extern const uint32_t JSONData_ToString_m3533948284_MetadataUsageId;
extern "C"  String_t* JSONData_ToString_m3533948284 (JSONData_t580311760 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONData_ToString_m3533948284_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = __this->get_m_Data_0();
		String_t* L_1 = JSONNode_Escape_m3126929209(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral34, L_1, _stringLiteral34, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String SimpleJSON.JSONData::ToString(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral34;
extern const uint32_t JSONData_ToString_m3090391814_MetadataUsageId;
extern "C"  String_t* JSONData_ToString_m3090391814 (JSONData_t580311760 * __this, String_t* ___aPrefix0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONData_ToString_m3090391814_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = __this->get_m_Data_0();
		String_t* L_1 = JSONNode_Escape_m3126929209(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral34, L_1, _stringLiteral34, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void SimpleJSON.JSONData::Serialize(System.IO.BinaryWriter)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* JSONData_t580311760_il2cpp_TypeInfo_var;
extern const uint32_t JSONData_Serialize_m1313540754_MetadataUsageId;
extern "C"  void JSONData_Serialize_m1313540754 (JSONData_t580311760 * __this, BinaryWriter_t2314211483 * ___aWriter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONData_Serialize_m1313540754_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JSONData_t580311760 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		JSONData_t580311760 * L_1 = (JSONData_t580311760 *)il2cpp_codegen_object_new(JSONData_t580311760_il2cpp_TypeInfo_var);
		JSONData__ctor_m1923599889(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		JSONData_t580311760 * L_2 = V_0;
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(18 /* System.Int32 SimpleJSON.JSONNode::get_AsInt() */, __this);
		NullCheck(L_2);
		VirtActionInvoker1< int32_t >::Invoke(19 /* System.Void SimpleJSON.JSONNode::set_AsInt(System.Int32) */, L_2, L_3);
		JSONData_t580311760 * L_4 = V_0;
		NullCheck(L_4);
		String_t* L_5 = L_4->get_m_Data_0();
		String_t* L_6 = __this->get_m_Data_0();
		bool L_7 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0041;
		}
	}
	{
		BinaryWriter_t2314211483 * L_8 = ___aWriter0;
		NullCheck(L_8);
		VirtActionInvoker1< uint8_t >::Invoke(8 /* System.Void System.IO.BinaryWriter::Write(System.Byte) */, L_8, 4);
		BinaryWriter_t2314211483 * L_9 = ___aWriter0;
		int32_t L_10 = VirtFuncInvoker0< int32_t >::Invoke(18 /* System.Int32 SimpleJSON.JSONNode::get_AsInt() */, __this);
		NullCheck(L_9);
		VirtActionInvoker1< int32_t >::Invoke(16 /* System.Void System.IO.BinaryWriter::Write(System.Int32) */, L_9, L_10);
		return;
	}

IL_0041:
	{
		JSONData_t580311760 * L_11 = V_0;
		float L_12 = VirtFuncInvoker0< float >::Invoke(20 /* System.Single SimpleJSON.JSONNode::get_AsFloat() */, __this);
		NullCheck(L_11);
		VirtActionInvoker1< float >::Invoke(21 /* System.Void SimpleJSON.JSONNode::set_AsFloat(System.Single) */, L_11, L_12);
		JSONData_t580311760 * L_13 = V_0;
		NullCheck(L_13);
		String_t* L_14 = L_13->get_m_Data_0();
		String_t* L_15 = __this->get_m_Data_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_16 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0077;
		}
	}
	{
		BinaryWriter_t2314211483 * L_17 = ___aWriter0;
		NullCheck(L_17);
		VirtActionInvoker1< uint8_t >::Invoke(8 /* System.Void System.IO.BinaryWriter::Write(System.Byte) */, L_17, 7);
		BinaryWriter_t2314211483 * L_18 = ___aWriter0;
		float L_19 = VirtFuncInvoker0< float >::Invoke(20 /* System.Single SimpleJSON.JSONNode::get_AsFloat() */, __this);
		NullCheck(L_18);
		VirtActionInvoker1< float >::Invoke(19 /* System.Void System.IO.BinaryWriter::Write(System.Single) */, L_18, L_19);
		return;
	}

IL_0077:
	{
		JSONData_t580311760 * L_20 = V_0;
		double L_21 = VirtFuncInvoker0< double >::Invoke(22 /* System.Double SimpleJSON.JSONNode::get_AsDouble() */, __this);
		NullCheck(L_20);
		VirtActionInvoker1< double >::Invoke(23 /* System.Void SimpleJSON.JSONNode::set_AsDouble(System.Double) */, L_20, L_21);
		JSONData_t580311760 * L_22 = V_0;
		NullCheck(L_22);
		String_t* L_23 = L_22->get_m_Data_0();
		String_t* L_24 = __this->get_m_Data_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_25 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00ad;
		}
	}
	{
		BinaryWriter_t2314211483 * L_26 = ___aWriter0;
		NullCheck(L_26);
		VirtActionInvoker1< uint8_t >::Invoke(8 /* System.Void System.IO.BinaryWriter::Write(System.Byte) */, L_26, 5);
		BinaryWriter_t2314211483 * L_27 = ___aWriter0;
		double L_28 = VirtFuncInvoker0< double >::Invoke(22 /* System.Double SimpleJSON.JSONNode::get_AsDouble() */, __this);
		NullCheck(L_27);
		VirtActionInvoker1< double >::Invoke(14 /* System.Void System.IO.BinaryWriter::Write(System.Double) */, L_27, L_28);
		return;
	}

IL_00ad:
	{
		JSONData_t580311760 * L_29 = V_0;
		bool L_30 = VirtFuncInvoker0< bool >::Invoke(24 /* System.Boolean SimpleJSON.JSONNode::get_AsBool() */, __this);
		NullCheck(L_29);
		VirtActionInvoker1< bool >::Invoke(25 /* System.Void SimpleJSON.JSONNode::set_AsBool(System.Boolean) */, L_29, L_30);
		JSONData_t580311760 * L_31 = V_0;
		NullCheck(L_31);
		String_t* L_32 = L_31->get_m_Data_0();
		String_t* L_33 = __this->get_m_Data_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_34 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_32, L_33, /*hidden argument*/NULL);
		if (!L_34)
		{
			goto IL_00e3;
		}
	}
	{
		BinaryWriter_t2314211483 * L_35 = ___aWriter0;
		NullCheck(L_35);
		VirtActionInvoker1< uint8_t >::Invoke(8 /* System.Void System.IO.BinaryWriter::Write(System.Byte) */, L_35, 6);
		BinaryWriter_t2314211483 * L_36 = ___aWriter0;
		bool L_37 = VirtFuncInvoker0< bool >::Invoke(24 /* System.Boolean SimpleJSON.JSONNode::get_AsBool() */, __this);
		NullCheck(L_36);
		VirtActionInvoker1< bool >::Invoke(7 /* System.Void System.IO.BinaryWriter::Write(System.Boolean) */, L_36, L_37);
		return;
	}

IL_00e3:
	{
		BinaryWriter_t2314211483 * L_38 = ___aWriter0;
		NullCheck(L_38);
		VirtActionInvoker1< uint8_t >::Invoke(8 /* System.Void System.IO.BinaryWriter::Write(System.Byte) */, L_38, 3);
		BinaryWriter_t2314211483 * L_39 = ___aWriter0;
		String_t* L_40 = __this->get_m_Data_0();
		NullCheck(L_39);
		VirtActionInvoker1< String_t* >::Invoke(20 /* System.Void System.IO.BinaryWriter::Write(System.String) */, L_39, L_40);
		return;
	}
}
// System.Void SimpleJSON.JSONLazyCreator::.ctor(SimpleJSON.JSONNode)
extern "C"  void JSONLazyCreator__ctor_m3036761377 (JSONLazyCreator_t3067077166 * __this, JSONNode_t580622632 * ___aNode0, const MethodInfo* method)
{
	{
		JSONNode__ctor_m39807289(__this, /*hidden argument*/NULL);
		JSONNode_t580622632 * L_0 = ___aNode0;
		__this->set_m_Node_0(L_0);
		__this->set_m_Key_1((String_t*)NULL);
		return;
	}
}
// System.Void SimpleJSON.JSONLazyCreator::.ctor(SimpleJSON.JSONNode,System.String)
extern "C"  void JSONLazyCreator__ctor_m66611101 (JSONLazyCreator_t3067077166 * __this, JSONNode_t580622632 * ___aNode0, String_t* ___aKey1, const MethodInfo* method)
{
	{
		JSONNode__ctor_m39807289(__this, /*hidden argument*/NULL);
		JSONNode_t580622632 * L_0 = ___aNode0;
		__this->set_m_Node_0(L_0);
		String_t* L_1 = ___aKey1;
		__this->set_m_Key_1(L_1);
		return;
	}
}
// System.Void SimpleJSON.JSONLazyCreator::Set(SimpleJSON.JSONNode)
extern "C"  void JSONLazyCreator_Set_m646273057 (JSONLazyCreator_t3067077166 * __this, JSONNode_t580622632 * ___aVal0, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_m_Key_1();
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		JSONNode_t580622632 * L_1 = __this->get_m_Node_0();
		JSONNode_t580622632 * L_2 = ___aVal0;
		NullCheck(L_1);
		VirtActionInvoker1< JSONNode_t580622632 * >::Invoke(12 /* System.Void SimpleJSON.JSONNode::Add(SimpleJSON.JSONNode) */, L_1, L_2);
		goto IL_002e;
	}

IL_001c:
	{
		JSONNode_t580622632 * L_3 = __this->get_m_Node_0();
		String_t* L_4 = __this->get_m_Key_1();
		JSONNode_t580622632 * L_5 = ___aVal0;
		NullCheck(L_3);
		VirtActionInvoker2< String_t*, JSONNode_t580622632 * >::Invoke(4 /* System.Void SimpleJSON.JSONNode::Add(System.String,SimpleJSON.JSONNode) */, L_3, L_4, L_5);
	}

IL_002e:
	{
		__this->set_m_Node_0((JSONNode_t580622632 *)NULL);
		return;
	}
}
// SimpleJSON.JSONNode SimpleJSON.JSONLazyCreator::get_Item(System.Int32)
extern Il2CppClass* JSONLazyCreator_t3067077166_il2cpp_TypeInfo_var;
extern const uint32_t JSONLazyCreator_get_Item_m2852012659_MetadataUsageId;
extern "C"  JSONNode_t580622632 * JSONLazyCreator_get_Item_m2852012659 (JSONLazyCreator_t3067077166 * __this, int32_t ___aIndex0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONLazyCreator_get_Item_m2852012659_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		JSONLazyCreator_t3067077166 * L_0 = (JSONLazyCreator_t3067077166 *)il2cpp_codegen_object_new(JSONLazyCreator_t3067077166_il2cpp_TypeInfo_var);
		JSONLazyCreator__ctor_m3036761377(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void SimpleJSON.JSONLazyCreator::set_Item(System.Int32,SimpleJSON.JSONNode)
extern Il2CppClass* JSONArray_t3478762607_il2cpp_TypeInfo_var;
extern const uint32_t JSONLazyCreator_set_Item_m1102770240_MetadataUsageId;
extern "C"  void JSONLazyCreator_set_Item_m1102770240 (JSONLazyCreator_t3067077166 * __this, int32_t ___aIndex0, JSONNode_t580622632 * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONLazyCreator_set_Item_m1102770240_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JSONArray_t3478762607 * V_0 = NULL;
	{
		JSONArray_t3478762607 * L_0 = (JSONArray_t3478762607 *)il2cpp_codegen_object_new(JSONArray_t3478762607_il2cpp_TypeInfo_var);
		JSONArray__ctor_m2704428242(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONArray_t3478762607 * L_1 = V_0;
		JSONNode_t580622632 * L_2 = ___value1;
		NullCheck(L_1);
		VirtActionInvoker1< JSONNode_t580622632 * >::Invoke(12 /* System.Void SimpleJSON.JSONNode::Add(SimpleJSON.JSONNode) */, L_1, L_2);
		JSONArray_t3478762607 * L_3 = V_0;
		JSONLazyCreator_Set_m646273057(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// SimpleJSON.JSONNode SimpleJSON.JSONLazyCreator::get_Item(System.String)
extern Il2CppClass* JSONLazyCreator_t3067077166_il2cpp_TypeInfo_var;
extern const uint32_t JSONLazyCreator_get_Item_m2969744928_MetadataUsageId;
extern "C"  JSONNode_t580622632 * JSONLazyCreator_get_Item_m2969744928 (JSONLazyCreator_t3067077166 * __this, String_t* ___aKey0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONLazyCreator_get_Item_m2969744928_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___aKey0;
		JSONLazyCreator_t3067077166 * L_1 = (JSONLazyCreator_t3067077166 *)il2cpp_codegen_object_new(JSONLazyCreator_t3067077166_il2cpp_TypeInfo_var);
		JSONLazyCreator__ctor_m66611101(L_1, __this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void SimpleJSON.JSONLazyCreator::set_Item(System.String,SimpleJSON.JSONNode)
extern Il2CppClass* JSONClass_t3480415118_il2cpp_TypeInfo_var;
extern const uint32_t JSONLazyCreator_set_Item_m2754972687_MetadataUsageId;
extern "C"  void JSONLazyCreator_set_Item_m2754972687 (JSONLazyCreator_t3067077166 * __this, String_t* ___aKey0, JSONNode_t580622632 * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONLazyCreator_set_Item_m2754972687_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JSONClass_t3480415118 * V_0 = NULL;
	{
		JSONClass_t3480415118 * L_0 = (JSONClass_t3480415118 *)il2cpp_codegen_object_new(JSONClass_t3480415118_il2cpp_TypeInfo_var);
		JSONClass__ctor_m158050451(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONClass_t3480415118 * L_1 = V_0;
		String_t* L_2 = ___aKey0;
		JSONNode_t580622632 * L_3 = ___value1;
		NullCheck(L_1);
		VirtActionInvoker2< String_t*, JSONNode_t580622632 * >::Invoke(4 /* System.Void SimpleJSON.JSONClass::Add(System.String,SimpleJSON.JSONNode) */, L_1, L_2, L_3);
		JSONClass_t3480415118 * L_4 = V_0;
		JSONLazyCreator_Set_m646273057(__this, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SimpleJSON.JSONLazyCreator::Add(SimpleJSON.JSONNode)
extern Il2CppClass* JSONArray_t3478762607_il2cpp_TypeInfo_var;
extern const uint32_t JSONLazyCreator_Add_m3151759682_MetadataUsageId;
extern "C"  void JSONLazyCreator_Add_m3151759682 (JSONLazyCreator_t3067077166 * __this, JSONNode_t580622632 * ___aItem0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONLazyCreator_Add_m3151759682_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JSONArray_t3478762607 * V_0 = NULL;
	{
		JSONArray_t3478762607 * L_0 = (JSONArray_t3478762607 *)il2cpp_codegen_object_new(JSONArray_t3478762607_il2cpp_TypeInfo_var);
		JSONArray__ctor_m2704428242(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONArray_t3478762607 * L_1 = V_0;
		JSONNode_t580622632 * L_2 = ___aItem0;
		NullCheck(L_1);
		VirtActionInvoker1< JSONNode_t580622632 * >::Invoke(12 /* System.Void SimpleJSON.JSONNode::Add(SimpleJSON.JSONNode) */, L_1, L_2);
		JSONArray_t3478762607 * L_3 = V_0;
		JSONLazyCreator_Set_m646273057(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SimpleJSON.JSONLazyCreator::Add(System.String,SimpleJSON.JSONNode)
extern Il2CppClass* JSONClass_t3480415118_il2cpp_TypeInfo_var;
extern const uint32_t JSONLazyCreator_Add_m2105370750_MetadataUsageId;
extern "C"  void JSONLazyCreator_Add_m2105370750 (JSONLazyCreator_t3067077166 * __this, String_t* ___aKey0, JSONNode_t580622632 * ___aItem1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONLazyCreator_Add_m2105370750_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JSONClass_t3480415118 * V_0 = NULL;
	{
		JSONClass_t3480415118 * L_0 = (JSONClass_t3480415118 *)il2cpp_codegen_object_new(JSONClass_t3480415118_il2cpp_TypeInfo_var);
		JSONClass__ctor_m158050451(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONClass_t3480415118 * L_1 = V_0;
		String_t* L_2 = ___aKey0;
		JSONNode_t580622632 * L_3 = ___aItem1;
		NullCheck(L_1);
		VirtActionInvoker2< String_t*, JSONNode_t580622632 * >::Invoke(4 /* System.Void SimpleJSON.JSONClass::Add(System.String,SimpleJSON.JSONNode) */, L_1, L_2, L_3);
		JSONClass_t3480415118 * L_4 = V_0;
		JSONLazyCreator_Set_m646273057(__this, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean SimpleJSON.JSONLazyCreator::Equals(System.Object)
extern "C"  bool JSONLazyCreator_Equals_m4128503944 (JSONLazyCreator_t3067077166 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (bool)1;
	}

IL_0008:
	{
		Il2CppObject * L_1 = ___obj0;
		bool L_2 = Object_ReferenceEquals_m3695130242(NULL /*static, unused*/, __this, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 SimpleJSON.JSONLazyCreator::GetHashCode()
extern "C"  int32_t JSONLazyCreator_GetHashCode_m123586348 (JSONLazyCreator_t3067077166 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = JSONNode_GetHashCode_m4259024798(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String SimpleJSON.JSONLazyCreator::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t JSONLazyCreator_ToString_m4199207232_MetadataUsageId;
extern "C"  String_t* JSONLazyCreator_ToString_m4199207232 (JSONLazyCreator_t3067077166 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONLazyCreator_ToString_m4199207232_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_0;
	}
}
// System.String SimpleJSON.JSONLazyCreator::ToString(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t JSONLazyCreator_ToString_m3982808514_MetadataUsageId;
extern "C"  String_t* JSONLazyCreator_ToString_m3982808514 (JSONLazyCreator_t3067077166 * __this, String_t* ___aPrefix0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONLazyCreator_ToString_m3982808514_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_0;
	}
}
// System.Int32 SimpleJSON.JSONLazyCreator::get_AsInt()
extern Il2CppClass* JSONData_t580311760_il2cpp_TypeInfo_var;
extern const uint32_t JSONLazyCreator_get_AsInt_m219613071_MetadataUsageId;
extern "C"  int32_t JSONLazyCreator_get_AsInt_m219613071 (JSONLazyCreator_t3067077166 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONLazyCreator_get_AsInt_m219613071_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JSONData_t580311760 * V_0 = NULL;
	{
		JSONData_t580311760 * L_0 = (JSONData_t580311760 *)il2cpp_codegen_object_new(JSONData_t580311760_il2cpp_TypeInfo_var);
		JSONData__ctor_m4065192034(L_0, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONData_t580311760 * L_1 = V_0;
		JSONLazyCreator_Set_m646273057(__this, L_1, /*hidden argument*/NULL);
		return 0;
	}
}
// System.Void SimpleJSON.JSONLazyCreator::set_AsInt(System.Int32)
extern Il2CppClass* JSONData_t580311760_il2cpp_TypeInfo_var;
extern const uint32_t JSONLazyCreator_set_AsInt_m376808226_MetadataUsageId;
extern "C"  void JSONLazyCreator_set_AsInt_m376808226 (JSONLazyCreator_t3067077166 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONLazyCreator_set_AsInt_m376808226_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JSONData_t580311760 * V_0 = NULL;
	{
		int32_t L_0 = ___value0;
		JSONData_t580311760 * L_1 = (JSONData_t580311760 *)il2cpp_codegen_object_new(JSONData_t580311760_il2cpp_TypeInfo_var);
		JSONData__ctor_m4065192034(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		JSONData_t580311760 * L_2 = V_0;
		JSONLazyCreator_Set_m646273057(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Single SimpleJSON.JSONLazyCreator::get_AsFloat()
extern Il2CppClass* JSONData_t580311760_il2cpp_TypeInfo_var;
extern const uint32_t JSONLazyCreator_get_AsFloat_m1432128486_MetadataUsageId;
extern "C"  float JSONLazyCreator_get_AsFloat_m1432128486 (JSONLazyCreator_t3067077166 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONLazyCreator_get_AsFloat_m1432128486_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JSONData_t580311760 * V_0 = NULL;
	{
		JSONData_t580311760 * L_0 = (JSONData_t580311760 *)il2cpp_codegen_object_new(JSONData_t580311760_il2cpp_TypeInfo_var);
		JSONData__ctor_m1604923578(L_0, (0.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		JSONData_t580311760 * L_1 = V_0;
		JSONLazyCreator_Set_m646273057(__this, L_1, /*hidden argument*/NULL);
		return (0.0f);
	}
}
// System.Void SimpleJSON.JSONLazyCreator::set_AsFloat(System.Single)
extern Il2CppClass* JSONData_t580311760_il2cpp_TypeInfo_var;
extern const uint32_t JSONLazyCreator_set_AsFloat_m3951007789_MetadataUsageId;
extern "C"  void JSONLazyCreator_set_AsFloat_m3951007789 (JSONLazyCreator_t3067077166 * __this, float ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONLazyCreator_set_AsFloat_m3951007789_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JSONData_t580311760 * V_0 = NULL;
	{
		float L_0 = ___value0;
		JSONData_t580311760 * L_1 = (JSONData_t580311760 *)il2cpp_codegen_object_new(JSONData_t580311760_il2cpp_TypeInfo_var);
		JSONData__ctor_m1604923578(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		JSONData_t580311760 * L_2 = V_0;
		JSONLazyCreator_Set_m646273057(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Double SimpleJSON.JSONLazyCreator::get_AsDouble()
extern Il2CppClass* JSONData_t580311760_il2cpp_TypeInfo_var;
extern const uint32_t JSONLazyCreator_get_AsDouble_m673104640_MetadataUsageId;
extern "C"  double JSONLazyCreator_get_AsDouble_m673104640 (JSONLazyCreator_t3067077166 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONLazyCreator_get_AsDouble_m673104640_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JSONData_t580311760 * V_0 = NULL;
	{
		JSONData_t580311760 * L_0 = (JSONData_t580311760 *)il2cpp_codegen_object_new(JSONData_t580311760_il2cpp_TypeInfo_var);
		JSONData__ctor_m1355360849(L_0, (0.0), /*hidden argument*/NULL);
		V_0 = L_0;
		JSONData_t580311760 * L_1 = V_0;
		JSONLazyCreator_Set_m646273057(__this, L_1, /*hidden argument*/NULL);
		return (0.0);
	}
}
// System.Void SimpleJSON.JSONLazyCreator::set_AsDouble(System.Double)
extern Il2CppClass* JSONData_t580311760_il2cpp_TypeInfo_var;
extern const uint32_t JSONLazyCreator_set_AsDouble_m3512322065_MetadataUsageId;
extern "C"  void JSONLazyCreator_set_AsDouble_m3512322065 (JSONLazyCreator_t3067077166 * __this, double ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONLazyCreator_set_AsDouble_m3512322065_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JSONData_t580311760 * V_0 = NULL;
	{
		double L_0 = ___value0;
		JSONData_t580311760 * L_1 = (JSONData_t580311760 *)il2cpp_codegen_object_new(JSONData_t580311760_il2cpp_TypeInfo_var);
		JSONData__ctor_m1355360849(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		JSONData_t580311760 * L_2 = V_0;
		JSONLazyCreator_Set_m646273057(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean SimpleJSON.JSONLazyCreator::get_AsBool()
extern Il2CppClass* JSONData_t580311760_il2cpp_TypeInfo_var;
extern const uint32_t JSONLazyCreator_get_AsBool_m3226378386_MetadataUsageId;
extern "C"  bool JSONLazyCreator_get_AsBool_m3226378386 (JSONLazyCreator_t3067077166 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONLazyCreator_get_AsBool_m3226378386_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JSONData_t580311760 * V_0 = NULL;
	{
		JSONData_t580311760 * L_0 = (JSONData_t580311760 *)il2cpp_codegen_object_new(JSONData_t580311760_il2cpp_TypeInfo_var);
		JSONData__ctor_m4008077384(L_0, (bool)0, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONData_t580311760 * L_1 = V_0;
		JSONLazyCreator_Set_m646273057(__this, L_1, /*hidden argument*/NULL);
		return (bool)0;
	}
}
// System.Void SimpleJSON.JSONLazyCreator::set_AsBool(System.Boolean)
extern Il2CppClass* JSONData_t580311760_il2cpp_TypeInfo_var;
extern const uint32_t JSONLazyCreator_set_AsBool_m2723144545_MetadataUsageId;
extern "C"  void JSONLazyCreator_set_AsBool_m2723144545 (JSONLazyCreator_t3067077166 * __this, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONLazyCreator_set_AsBool_m2723144545_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JSONData_t580311760 * V_0 = NULL;
	{
		bool L_0 = ___value0;
		JSONData_t580311760 * L_1 = (JSONData_t580311760 *)il2cpp_codegen_object_new(JSONData_t580311760_il2cpp_TypeInfo_var);
		JSONData__ctor_m4008077384(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		JSONData_t580311760 * L_2 = V_0;
		JSONLazyCreator_Set_m646273057(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// SimpleJSON.JSONArray SimpleJSON.JSONLazyCreator::get_AsArray()
extern Il2CppClass* JSONArray_t3478762607_il2cpp_TypeInfo_var;
extern const uint32_t JSONLazyCreator_get_AsArray_m734393415_MetadataUsageId;
extern "C"  JSONArray_t3478762607 * JSONLazyCreator_get_AsArray_m734393415 (JSONLazyCreator_t3067077166 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONLazyCreator_get_AsArray_m734393415_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JSONArray_t3478762607 * V_0 = NULL;
	{
		JSONArray_t3478762607 * L_0 = (JSONArray_t3478762607 *)il2cpp_codegen_object_new(JSONArray_t3478762607_il2cpp_TypeInfo_var);
		JSONArray__ctor_m2704428242(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONArray_t3478762607 * L_1 = V_0;
		JSONLazyCreator_Set_m646273057(__this, L_1, /*hidden argument*/NULL);
		JSONArray_t3478762607 * L_2 = V_0;
		return L_2;
	}
}
// SimpleJSON.JSONClass SimpleJSON.JSONLazyCreator::get_AsObject()
extern Il2CppClass* JSONClass_t3480415118_il2cpp_TypeInfo_var;
extern const uint32_t JSONLazyCreator_get_AsObject_m1324955252_MetadataUsageId;
extern "C"  JSONClass_t3480415118 * JSONLazyCreator_get_AsObject_m1324955252 (JSONLazyCreator_t3067077166 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONLazyCreator_get_AsObject_m1324955252_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JSONClass_t3480415118 * V_0 = NULL;
	{
		JSONClass_t3480415118 * L_0 = (JSONClass_t3480415118 *)il2cpp_codegen_object_new(JSONClass_t3480415118_il2cpp_TypeInfo_var);
		JSONClass__ctor_m158050451(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		JSONClass_t3480415118 * L_1 = V_0;
		JSONLazyCreator_Set_m646273057(__this, L_1, /*hidden argument*/NULL);
		JSONClass_t3480415118 * L_2 = V_0;
		return L_2;
	}
}
// System.Boolean SimpleJSON.JSONLazyCreator::op_Equality(SimpleJSON.JSONLazyCreator,System.Object)
extern "C"  bool JSONLazyCreator_op_Equality_m759088255 (Il2CppObject * __this /* static, unused */, JSONLazyCreator_t3067077166 * ___a0, Il2CppObject * ___b1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___b1;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (bool)1;
	}

IL_0008:
	{
		JSONLazyCreator_t3067077166 * L_1 = ___a0;
		Il2CppObject * L_2 = ___b1;
		bool L_3 = Object_ReferenceEquals_m3695130242(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean SimpleJSON.JSONLazyCreator::op_Inequality(SimpleJSON.JSONLazyCreator,System.Object)
extern "C"  bool JSONLazyCreator_op_Inequality_m554062308 (Il2CppObject * __this /* static, unused */, JSONLazyCreator_t3067077166 * ___a0, Il2CppObject * ___b1, const MethodInfo* method)
{
	{
		JSONLazyCreator_t3067077166 * L_0 = ___a0;
		Il2CppObject * L_1 = ___b1;
		bool L_2 = JSONLazyCreator_op_Equality_m759088255(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void SimpleJSON.JSONNode::.ctor()
extern "C"  void JSONNode__ctor_m39807289 (JSONNode_t580622632 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SimpleJSON.JSONNode::Add(System.String,SimpleJSON.JSONNode)
extern "C"  void JSONNode_Add_m4209089656 (JSONNode_t580622632 * __this, String_t* ___aKey0, JSONNode_t580622632 * ___aItem1, const MethodInfo* method)
{
	{
		return;
	}
}
// SimpleJSON.JSONNode SimpleJSON.JSONNode::get_Item(System.Int32)
extern "C"  JSONNode_t580622632 * JSONNode_get_Item_m2900078435 (JSONNode_t580622632 * __this, int32_t ___aIndex0, const MethodInfo* method)
{
	{
		return (JSONNode_t580622632 *)NULL;
	}
}
// System.Void SimpleJSON.JSONNode::set_Item(System.Int32,SimpleJSON.JSONNode)
extern "C"  void JSONNode_set_Item_m1234212666 (JSONNode_t580622632 * __this, int32_t ___aIndex0, JSONNode_t580622632 * ___value1, const MethodInfo* method)
{
	{
		return;
	}
}
// SimpleJSON.JSONNode SimpleJSON.JSONNode::get_Item(System.String)
extern "C"  JSONNode_t580622632 * JSONNode_get_Item_m164816688 (JSONNode_t580622632 * __this, String_t* ___aKey0, const MethodInfo* method)
{
	{
		return (JSONNode_t580622632 *)NULL;
	}
}
// System.Void SimpleJSON.JSONNode::set_Item(System.String,SimpleJSON.JSONNode)
extern "C"  void JSONNode_set_Item_m2534720597 (JSONNode_t580622632 * __this, String_t* ___aKey0, JSONNode_t580622632 * ___value1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.String SimpleJSON.JSONNode::get_Value()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t JSONNode_get_Value_m716998594_MetadataUsageId;
extern "C"  String_t* JSONNode_get_Value_m716998594 (JSONNode_t580622632 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONNode_get_Value_m716998594_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_0;
	}
}
// System.Void SimpleJSON.JSONNode::set_Value(System.String)
extern "C"  void JSONNode_set_Value_m1237544759 (JSONNode_t580622632 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Int32 SimpleJSON.JSONNode::get_Count()
extern "C"  int32_t JSONNode_get_Count_m4199899827 (JSONNode_t580622632 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Void SimpleJSON.JSONNode::Add(SimpleJSON.JSONNode)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t JSONNode_Add_m3402717884_MetadataUsageId;
extern "C"  void JSONNode_Add_m3402717884 (JSONNode_t580622632 * __this, JSONNode_t580622632 * ___aItem0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONNode_Add_m3402717884_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		JSONNode_t580622632 * L_1 = ___aItem0;
		VirtActionInvoker2< String_t*, JSONNode_t580622632 * >::Invoke(4 /* System.Void SimpleJSON.JSONNode::Add(System.String,SimpleJSON.JSONNode) */, __this, L_0, L_1);
		return;
	}
}
// SimpleJSON.JSONNode SimpleJSON.JSONNode::Remove(System.String)
extern "C"  JSONNode_t580622632 * JSONNode_Remove_m2875069128 (JSONNode_t580622632 * __this, String_t* ___aKey0, const MethodInfo* method)
{
	{
		return (JSONNode_t580622632 *)NULL;
	}
}
// SimpleJSON.JSONNode SimpleJSON.JSONNode::Remove(System.Int32)
extern "C"  JSONNode_t580622632 * JSONNode_Remove_m770748619 (JSONNode_t580622632 * __this, int32_t ___aIndex0, const MethodInfo* method)
{
	{
		return (JSONNode_t580622632 *)NULL;
	}
}
// SimpleJSON.JSONNode SimpleJSON.JSONNode::Remove(SimpleJSON.JSONNode)
extern "C"  JSONNode_t580622632 * JSONNode_Remove_m459111738 (JSONNode_t580622632 * __this, JSONNode_t580622632 * ___aNode0, const MethodInfo* method)
{
	{
		JSONNode_t580622632 * L_0 = ___aNode0;
		return L_0;
	}
}
// System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode::get_Childs()
extern Il2CppClass* U3CU3Ec__Iterator0_t2516598209_il2cpp_TypeInfo_var;
extern const uint32_t JSONNode_get_Childs_m1772976129_MetadataUsageId;
extern "C"  Il2CppObject* JSONNode_get_Childs_m1772976129 (JSONNode_t580622632 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONNode_get_Childs_m1772976129_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__Iterator0_t2516598209 * V_0 = NULL;
	{
		U3CU3Ec__Iterator0_t2516598209 * L_0 = (U3CU3Ec__Iterator0_t2516598209 *)il2cpp_codegen_object_new(U3CU3Ec__Iterator0_t2516598209_il2cpp_TypeInfo_var);
		U3CU3Ec__Iterator0__ctor_m862538589(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__Iterator0_t2516598209 * L_1 = V_0;
		U3CU3Ec__Iterator0_t2516598209 * L_2 = L_1;
		NullCheck(L_2);
		L_2->set_U24PC_0(((int32_t)-2));
		return L_2;
	}
}
// System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode::get_DeepChilds()
extern Il2CppClass* U3CU3Ec__Iterator1_t2516598210_il2cpp_TypeInfo_var;
extern const uint32_t JSONNode_get_DeepChilds_m4161671309_MetadataUsageId;
extern "C"  Il2CppObject* JSONNode_get_DeepChilds_m4161671309 (JSONNode_t580622632 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONNode_get_DeepChilds_m4161671309_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__Iterator1_t2516598210 * V_0 = NULL;
	{
		U3CU3Ec__Iterator1_t2516598210 * L_0 = (U3CU3Ec__Iterator1_t2516598210 *)il2cpp_codegen_object_new(U3CU3Ec__Iterator1_t2516598210_il2cpp_TypeInfo_var);
		U3CU3Ec__Iterator1__ctor_m666025084(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__Iterator1_t2516598210 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_6(__this);
		U3CU3Ec__Iterator1_t2516598210 * L_2 = V_0;
		U3CU3Ec__Iterator1_t2516598210 * L_3 = L_2;
		NullCheck(L_3);
		L_3->set_U24PC_4(((int32_t)-2));
		return L_3;
	}
}
// System.String SimpleJSON.JSONNode::ToString()
extern Il2CppCodeGenString* _stringLiteral3103478538;
extern const uint32_t JSONNode_ToString_m679854036_MetadataUsageId;
extern "C"  String_t* JSONNode_ToString_m679854036 (JSONNode_t580622632 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONNode_ToString_m679854036_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return _stringLiteral3103478538;
	}
}
// System.String SimpleJSON.JSONNode::ToString(System.String)
extern Il2CppCodeGenString* _stringLiteral3103478538;
extern const uint32_t JSONNode_ToString_m4078253998_MetadataUsageId;
extern "C"  String_t* JSONNode_ToString_m4078253998 (JSONNode_t580622632 * __this, String_t* ___aPrefix0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONNode_ToString_m4078253998_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return _stringLiteral3103478538;
	}
}
// System.Int32 SimpleJSON.JSONNode::get_AsInt()
extern "C"  int32_t JSONNode_get_AsInt_m2498774145 (JSONNode_t580622632 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		String_t* L_0 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String SimpleJSON.JSONNode::get_Value() */, __this);
		bool L_1 = Int32_TryParse_m695344220(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_2 = V_0;
		return L_2;
	}

IL_0016:
	{
		return 0;
	}
}
// System.Void SimpleJSON.JSONNode::set_AsInt(System.Int32)
extern "C"  void JSONNode_set_AsInt_m523450984 (JSONNode_t580622632 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = Int32_ToString_m1286526384((&___value0), /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(10 /* System.Void SimpleJSON.JSONNode::set_Value(System.String) */, __this, L_0);
		return;
	}
}
// System.Single SimpleJSON.JSONNode::get_AsFloat()
extern "C"  float JSONNode_get_AsFloat_m4098928068 (JSONNode_t580622632 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		V_0 = (0.0f);
		String_t* L_0 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String SimpleJSON.JSONNode::get_Value() */, __this);
		bool L_1 = Single_TryParse_m3535027506(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		float L_2 = V_0;
		return L_2;
	}

IL_001a:
	{
		return (0.0f);
	}
}
// System.Void SimpleJSON.JSONNode::set_AsFloat(System.Single)
extern "C"  void JSONNode_set_AsFloat_m308704039 (JSONNode_t580622632 * __this, float ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = Single_ToString_m5736032((&___value0), /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(10 /* System.Void SimpleJSON.JSONNode::set_Value(System.String) */, __this, L_0);
		return;
	}
}
// System.Double SimpleJSON.JSONNode::get_AsDouble()
extern "C"  double JSONNode_get_AsDouble_m3231545108 (JSONNode_t580622632 * __this, const MethodInfo* method)
{
	double V_0 = 0.0;
	{
		V_0 = (0.0);
		String_t* L_0 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String SimpleJSON.JSONNode::get_Value() */, __this);
		bool L_1 = Double_TryParse_m2709942532(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		double L_2 = V_0;
		return L_2;
	}

IL_001e:
	{
		return (0.0);
	}
}
// System.Void SimpleJSON.JSONNode::set_AsDouble(System.Double)
extern "C"  void JSONNode_set_AsDouble_m2270055511 (JSONNode_t580622632 * __this, double ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = Double_ToString_m3380246633((&___value0), /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(10 /* System.Void SimpleJSON.JSONNode::set_Value(System.String) */, __this, L_0);
		return;
	}
}
// System.Boolean SimpleJSON.JSONNode::get_AsBool()
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t JSONNode_get_AsBool_m2927447380_MetadataUsageId;
extern "C"  bool JSONNode_get_AsBool_m2927447380 (JSONNode_t580622632 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONNode_get_AsBool_m2927447380_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		V_0 = (bool)0;
		String_t* L_0 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String SimpleJSON.JSONNode::get_Value() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(Boolean_t211005341_il2cpp_TypeInfo_var);
		bool L_1 = Boolean_TryParse_m4263405916(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		bool L_2 = V_0;
		return L_2;
	}

IL_0016:
	{
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String SimpleJSON.JSONNode::get_Value() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_4) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void SimpleJSON.JSONNode::set_AsBool(System.Boolean)
extern Il2CppCodeGenString* _stringLiteral3569038;
extern Il2CppCodeGenString* _stringLiteral97196323;
extern const uint32_t JSONNode_set_AsBool_m3375808091_MetadataUsageId;
extern "C"  void JSONNode_set_AsBool_m3375808091 (JSONNode_t580622632 * __this, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONNode_set_AsBool_m3375808091_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JSONNode_t580622632 * G_B2_0 = NULL;
	JSONNode_t580622632 * G_B1_0 = NULL;
	String_t* G_B3_0 = NULL;
	JSONNode_t580622632 * G_B3_1 = NULL;
	{
		bool L_0 = ___value0;
		G_B1_0 = __this;
		if (!L_0)
		{
			G_B2_0 = __this;
			goto IL_0011;
		}
	}
	{
		G_B3_0 = _stringLiteral3569038;
		G_B3_1 = G_B1_0;
		goto IL_0016;
	}

IL_0011:
	{
		G_B3_0 = _stringLiteral97196323;
		G_B3_1 = G_B2_0;
	}

IL_0016:
	{
		NullCheck(G_B3_1);
		VirtActionInvoker1< String_t* >::Invoke(10 /* System.Void SimpleJSON.JSONNode::set_Value(System.String) */, G_B3_1, G_B3_0);
		return;
	}
}
// SimpleJSON.JSONArray SimpleJSON.JSONNode::get_AsArray()
extern Il2CppClass* JSONArray_t3478762607_il2cpp_TypeInfo_var;
extern const uint32_t JSONNode_get_AsArray_m532209501_MetadataUsageId;
extern "C"  JSONArray_t3478762607 * JSONNode_get_AsArray_m532209501 (JSONNode_t580622632 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONNode_get_AsArray_m532209501_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return ((JSONArray_t3478762607 *)IsInstClass(__this, JSONArray_t3478762607_il2cpp_TypeInfo_var));
	}
}
// SimpleJSON.JSONClass SimpleJSON.JSONNode::get_AsObject()
extern Il2CppClass* JSONClass_t3480415118_il2cpp_TypeInfo_var;
extern const uint32_t JSONNode_get_AsObject_m1093981756_MetadataUsageId;
extern "C"  JSONClass_t3480415118 * JSONNode_get_AsObject_m1093981756 (JSONNode_t580622632 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONNode_get_AsObject_m1093981756_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return ((JSONClass_t3480415118 *)IsInstClass(__this, JSONClass_t3480415118_il2cpp_TypeInfo_var));
	}
}
// System.Boolean SimpleJSON.JSONNode::Equals(System.Object)
extern "C"  bool JSONNode_Equals_m2471396870 (JSONNode_t580622632 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		bool L_1 = Object_ReferenceEquals_m3695130242(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 SimpleJSON.JSONNode::GetHashCode()
extern "C"  int32_t JSONNode_GetHashCode_m4259024798 (JSONNode_t580622632 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Object_GetHashCode_m500842593(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String SimpleJSON.JSONNode::Escape(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t2778706699_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2944;
extern Il2CppCodeGenString* _stringLiteral2886;
extern Il2CppCodeGenString* _stringLiteral2962;
extern Il2CppCodeGenString* _stringLiteral2966;
extern Il2CppCodeGenString* _stringLiteral2968;
extern Il2CppCodeGenString* _stringLiteral2950;
extern Il2CppCodeGenString* _stringLiteral2954;
extern const uint32_t JSONNode_Escape_m3126929209_MetadataUsageId;
extern "C"  String_t* JSONNode_Escape_m3126929209 (Il2CppObject * __this /* static, unused */, String_t* ___aText0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONNode_Escape_m3126929209_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	uint16_t V_1 = 0x0;
	String_t* V_2 = NULL;
	int32_t V_3 = 0;
	uint16_t V_4 = 0x0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_0;
		String_t* L_1 = ___aText0;
		V_2 = L_1;
		V_3 = 0;
		goto IL_00df;
	}

IL_000f:
	{
		String_t* L_2 = V_2;
		int32_t L_3 = V_3;
		NullCheck(L_2);
		uint16_t L_4 = String_get_Chars_m3015341861(L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		uint16_t L_5 = V_1;
		V_4 = L_5;
		uint16_t L_6 = V_4;
		if (((int32_t)((int32_t)L_6-(int32_t)8)) == 0)
		{
			goto IL_00a7;
		}
		if (((int32_t)((int32_t)L_6-(int32_t)8)) == 1)
		{
			goto IL_0096;
		}
		if (((int32_t)((int32_t)L_6-(int32_t)8)) == 2)
		{
			goto IL_0074;
		}
		if (((int32_t)((int32_t)L_6-(int32_t)8)) == 3)
		{
			goto IL_003b;
		}
		if (((int32_t)((int32_t)L_6-(int32_t)8)) == 4)
		{
			goto IL_00b8;
		}
		if (((int32_t)((int32_t)L_6-(int32_t)8)) == 5)
		{
			goto IL_0085;
		}
	}

IL_003b:
	{
		uint16_t L_7 = V_4;
		if ((((int32_t)L_7) == ((int32_t)((int32_t)34))))
		{
			goto IL_0063;
		}
	}
	{
		uint16_t L_8 = V_4;
		if ((((int32_t)L_8) == ((int32_t)((int32_t)92))))
		{
			goto IL_0052;
		}
	}
	{
		goto IL_00c9;
	}

IL_0052:
	{
		String_t* L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m138640077(NULL /*static, unused*/, L_9, _stringLiteral2944, /*hidden argument*/NULL);
		V_0 = L_10;
		goto IL_00db;
	}

IL_0063:
	{
		String_t* L_11 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m138640077(NULL /*static, unused*/, L_11, _stringLiteral2886, /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_00db;
	}

IL_0074:
	{
		String_t* L_13 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m138640077(NULL /*static, unused*/, L_13, _stringLiteral2962, /*hidden argument*/NULL);
		V_0 = L_14;
		goto IL_00db;
	}

IL_0085:
	{
		String_t* L_15 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m138640077(NULL /*static, unused*/, L_15, _stringLiteral2966, /*hidden argument*/NULL);
		V_0 = L_16;
		goto IL_00db;
	}

IL_0096:
	{
		String_t* L_17 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m138640077(NULL /*static, unused*/, L_17, _stringLiteral2968, /*hidden argument*/NULL);
		V_0 = L_18;
		goto IL_00db;
	}

IL_00a7:
	{
		String_t* L_19 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = String_Concat_m138640077(NULL /*static, unused*/, L_19, _stringLiteral2950, /*hidden argument*/NULL);
		V_0 = L_20;
		goto IL_00db;
	}

IL_00b8:
	{
		String_t* L_21 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Concat_m138640077(NULL /*static, unused*/, L_21, _stringLiteral2954, /*hidden argument*/NULL);
		V_0 = L_22;
		goto IL_00db;
	}

IL_00c9:
	{
		String_t* L_23 = V_0;
		uint16_t L_24 = V_1;
		uint16_t L_25 = L_24;
		Il2CppObject * L_26 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_25);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m389863537(NULL /*static, unused*/, L_23, L_26, /*hidden argument*/NULL);
		V_0 = L_27;
		goto IL_00db;
	}

IL_00db:
	{
		int32_t L_28 = V_3;
		V_3 = ((int32_t)((int32_t)L_28+(int32_t)1));
	}

IL_00df:
	{
		int32_t L_29 = V_3;
		String_t* L_30 = V_2;
		NullCheck(L_30);
		int32_t L_31 = String_get_Length_m2979997331(L_30, /*hidden argument*/NULL);
		if ((((int32_t)L_29) < ((int32_t)L_31)))
		{
			goto IL_000f;
		}
	}
	{
		String_t* L_32 = V_0;
		return L_32;
	}
}
// SimpleJSON.JSONNode SimpleJSON.JSONNode::Parse(System.String)
extern Il2CppClass* Stack_1_t3151028667_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t2778706699_il2cpp_TypeInfo_var;
extern Il2CppClass* JSONClass_t3480415118_il2cpp_TypeInfo_var;
extern Il2CppClass* JSONArray_t3478762607_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const MethodInfo* Stack_1__ctor_m575962149_MethodInfo_var;
extern const MethodInfo* Stack_1_Push_m2853826250_MethodInfo_var;
extern const MethodInfo* Stack_1_Peek_m1120330052_MethodInfo_var;
extern const MethodInfo* Stack_1_Pop_m3500129386_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1364402922;
extern Il2CppCodeGenString* _stringLiteral1438436952;
extern const uint32_t JSONNode_Parse_m1279392515_MetadataUsageId;
extern "C"  JSONNode_t580622632 * JSONNode_Parse_m1279392515 (Il2CppObject * __this /* static, unused */, String_t* ___aJSON0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONNode_Parse_m1279392515_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Stack_1_t3151028667 * V_0 = NULL;
	JSONNode_t580622632 * V_1 = NULL;
	int32_t V_2 = 0;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	bool V_5 = false;
	uint16_t V_6 = 0x0;
	String_t* V_7 = NULL;
	uint16_t V_8 = 0x0;
	uint16_t V_9 = 0x0;
	{
		Stack_1_t3151028667 * L_0 = (Stack_1_t3151028667 *)il2cpp_codegen_object_new(Stack_1_t3151028667_il2cpp_TypeInfo_var);
		Stack_1__ctor_m575962149(L_0, /*hidden argument*/Stack_1__ctor_m575962149_MethodInfo_var);
		V_0 = L_0;
		V_1 = (JSONNode_t580622632 *)NULL;
		V_2 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_3 = L_1;
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_4 = L_2;
		V_5 = (bool)0;
		goto IL_046b;
	}

IL_001f:
	{
		String_t* L_3 = ___aJSON0;
		int32_t L_4 = V_2;
		NullCheck(L_3);
		uint16_t L_5 = String_get_Chars_m3015341861(L_3, L_4, /*hidden argument*/NULL);
		V_8 = L_5;
		uint16_t L_6 = V_8;
		if (((int32_t)((int32_t)L_6-(int32_t)((int32_t)9))) == 0)
		{
			goto IL_0333;
		}
		if (((int32_t)((int32_t)L_6-(int32_t)((int32_t)9))) == 1)
		{
			goto IL_032e;
		}
		if (((int32_t)((int32_t)L_6-(int32_t)((int32_t)9))) == 2)
		{
			goto IL_0046;
		}
		if (((int32_t)((int32_t)L_6-(int32_t)((int32_t)9))) == 3)
		{
			goto IL_0046;
		}
		if (((int32_t)((int32_t)L_6-(int32_t)((int32_t)9))) == 4)
		{
			goto IL_032e;
		}
	}

IL_0046:
	{
		uint16_t L_7 = V_8;
		if (((int32_t)((int32_t)L_7-(int32_t)((int32_t)32))) == 0)
		{
			goto IL_0333;
		}
		if (((int32_t)((int32_t)L_7-(int32_t)((int32_t)32))) == 1)
		{
			goto IL_005c;
		}
		if (((int32_t)((int32_t)L_7-(int32_t)((int32_t)32))) == 2)
		{
			goto IL_02a7;
		}
	}

IL_005c:
	{
		uint16_t L_8 = V_8;
		if (((int32_t)((int32_t)L_8-(int32_t)((int32_t)91))) == 0)
		{
			goto IL_0132;
		}
		if (((int32_t)((int32_t)L_8-(int32_t)((int32_t)91))) == 1)
		{
			goto IL_0352;
		}
		if (((int32_t)((int32_t)L_8-(int32_t)((int32_t)91))) == 2)
		{
			goto IL_01c5;
		}
	}
	{
		uint16_t L_9 = V_8;
		if (((int32_t)((int32_t)L_9-(int32_t)((int32_t)123))) == 0)
		{
			goto IL_009f;
		}
		if (((int32_t)((int32_t)L_9-(int32_t)((int32_t)123))) == 1)
		{
			goto IL_0088;
		}
		if (((int32_t)((int32_t)L_9-(int32_t)((int32_t)123))) == 2)
		{
			goto IL_01c5;
		}
	}

IL_0088:
	{
		uint16_t L_10 = V_8;
		if ((((int32_t)L_10) == ((int32_t)((int32_t)44))))
		{
			goto IL_02b2;
		}
	}
	{
		uint16_t L_11 = V_8;
		if ((((int32_t)L_11) == ((int32_t)((int32_t)58))))
		{
			goto IL_027a;
		}
	}
	{
		goto IL_044f;
	}

IL_009f:
	{
		bool L_12 = V_5;
		if (!L_12)
		{
			goto IL_00be;
		}
	}
	{
		String_t* L_13 = V_3;
		String_t* L_14 = ___aJSON0;
		int32_t L_15 = V_2;
		NullCheck(L_14);
		uint16_t L_16 = String_get_Chars_m3015341861(L_14, L_15, /*hidden argument*/NULL);
		uint16_t L_17 = L_16;
		Il2CppObject * L_18 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_17);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Concat_m389863537(NULL /*static, unused*/, L_13, L_18, /*hidden argument*/NULL);
		V_3 = L_19;
		goto IL_0467;
	}

IL_00be:
	{
		Stack_1_t3151028667 * L_20 = V_0;
		JSONClass_t3480415118 * L_21 = (JSONClass_t3480415118 *)il2cpp_codegen_object_new(JSONClass_t3480415118_il2cpp_TypeInfo_var);
		JSONClass__ctor_m158050451(L_21, /*hidden argument*/NULL);
		NullCheck(L_20);
		Stack_1_Push_m2853826250(L_20, L_21, /*hidden argument*/Stack_1_Push_m2853826250_MethodInfo_var);
		JSONNode_t580622632 * L_22 = V_1;
		bool L_23 = JSONNode_op_Inequality_m1017226386(NULL /*static, unused*/, L_22, NULL, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_0119;
		}
	}
	{
		String_t* L_24 = V_4;
		NullCheck(L_24);
		String_t* L_25 = String_Trim_m1030489823(L_24, /*hidden argument*/NULL);
		V_4 = L_25;
		JSONNode_t580622632 * L_26 = V_1;
		if (!((JSONArray_t3478762607 *)IsInstClass(L_26, JSONArray_t3478762607_il2cpp_TypeInfo_var)))
		{
			goto IL_00fa;
		}
	}
	{
		JSONNode_t580622632 * L_27 = V_1;
		Stack_1_t3151028667 * L_28 = V_0;
		NullCheck(L_28);
		JSONNode_t580622632 * L_29 = Stack_1_Peek_m1120330052(L_28, /*hidden argument*/Stack_1_Peek_m1120330052_MethodInfo_var);
		NullCheck(L_27);
		VirtActionInvoker1< JSONNode_t580622632 * >::Invoke(12 /* System.Void SimpleJSON.JSONNode::Add(SimpleJSON.JSONNode) */, L_27, L_29);
		goto IL_0119;
	}

IL_00fa:
	{
		String_t* L_30 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_31 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_32 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_30, L_31, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_0119;
		}
	}
	{
		JSONNode_t580622632 * L_33 = V_1;
		String_t* L_34 = V_4;
		Stack_1_t3151028667 * L_35 = V_0;
		NullCheck(L_35);
		JSONNode_t580622632 * L_36 = Stack_1_Peek_m1120330052(L_35, /*hidden argument*/Stack_1_Peek_m1120330052_MethodInfo_var);
		NullCheck(L_33);
		VirtActionInvoker2< String_t*, JSONNode_t580622632 * >::Invoke(4 /* System.Void SimpleJSON.JSONNode::Add(System.String,SimpleJSON.JSONNode) */, L_33, L_34, L_36);
	}

IL_0119:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_37 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_4 = L_37;
		String_t* L_38 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_3 = L_38;
		Stack_1_t3151028667 * L_39 = V_0;
		NullCheck(L_39);
		JSONNode_t580622632 * L_40 = Stack_1_Peek_m1120330052(L_39, /*hidden argument*/Stack_1_Peek_m1120330052_MethodInfo_var);
		V_1 = L_40;
		goto IL_0467;
	}

IL_0132:
	{
		bool L_41 = V_5;
		if (!L_41)
		{
			goto IL_0151;
		}
	}
	{
		String_t* L_42 = V_3;
		String_t* L_43 = ___aJSON0;
		int32_t L_44 = V_2;
		NullCheck(L_43);
		uint16_t L_45 = String_get_Chars_m3015341861(L_43, L_44, /*hidden argument*/NULL);
		uint16_t L_46 = L_45;
		Il2CppObject * L_47 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_46);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_48 = String_Concat_m389863537(NULL /*static, unused*/, L_42, L_47, /*hidden argument*/NULL);
		V_3 = L_48;
		goto IL_0467;
	}

IL_0151:
	{
		Stack_1_t3151028667 * L_49 = V_0;
		JSONArray_t3478762607 * L_50 = (JSONArray_t3478762607 *)il2cpp_codegen_object_new(JSONArray_t3478762607_il2cpp_TypeInfo_var);
		JSONArray__ctor_m2704428242(L_50, /*hidden argument*/NULL);
		NullCheck(L_49);
		Stack_1_Push_m2853826250(L_49, L_50, /*hidden argument*/Stack_1_Push_m2853826250_MethodInfo_var);
		JSONNode_t580622632 * L_51 = V_1;
		bool L_52 = JSONNode_op_Inequality_m1017226386(NULL /*static, unused*/, L_51, NULL, /*hidden argument*/NULL);
		if (!L_52)
		{
			goto IL_01ac;
		}
	}
	{
		String_t* L_53 = V_4;
		NullCheck(L_53);
		String_t* L_54 = String_Trim_m1030489823(L_53, /*hidden argument*/NULL);
		V_4 = L_54;
		JSONNode_t580622632 * L_55 = V_1;
		if (!((JSONArray_t3478762607 *)IsInstClass(L_55, JSONArray_t3478762607_il2cpp_TypeInfo_var)))
		{
			goto IL_018d;
		}
	}
	{
		JSONNode_t580622632 * L_56 = V_1;
		Stack_1_t3151028667 * L_57 = V_0;
		NullCheck(L_57);
		JSONNode_t580622632 * L_58 = Stack_1_Peek_m1120330052(L_57, /*hidden argument*/Stack_1_Peek_m1120330052_MethodInfo_var);
		NullCheck(L_56);
		VirtActionInvoker1< JSONNode_t580622632 * >::Invoke(12 /* System.Void SimpleJSON.JSONNode::Add(SimpleJSON.JSONNode) */, L_56, L_58);
		goto IL_01ac;
	}

IL_018d:
	{
		String_t* L_59 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_60 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_61 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_59, L_60, /*hidden argument*/NULL);
		if (!L_61)
		{
			goto IL_01ac;
		}
	}
	{
		JSONNode_t580622632 * L_62 = V_1;
		String_t* L_63 = V_4;
		Stack_1_t3151028667 * L_64 = V_0;
		NullCheck(L_64);
		JSONNode_t580622632 * L_65 = Stack_1_Peek_m1120330052(L_64, /*hidden argument*/Stack_1_Peek_m1120330052_MethodInfo_var);
		NullCheck(L_62);
		VirtActionInvoker2< String_t*, JSONNode_t580622632 * >::Invoke(4 /* System.Void SimpleJSON.JSONNode::Add(System.String,SimpleJSON.JSONNode) */, L_62, L_63, L_65);
	}

IL_01ac:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_66 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_4 = L_66;
		String_t* L_67 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_3 = L_67;
		Stack_1_t3151028667 * L_68 = V_0;
		NullCheck(L_68);
		JSONNode_t580622632 * L_69 = Stack_1_Peek_m1120330052(L_68, /*hidden argument*/Stack_1_Peek_m1120330052_MethodInfo_var);
		V_1 = L_69;
		goto IL_0467;
	}

IL_01c5:
	{
		bool L_70 = V_5;
		if (!L_70)
		{
			goto IL_01e4;
		}
	}
	{
		String_t* L_71 = V_3;
		String_t* L_72 = ___aJSON0;
		int32_t L_73 = V_2;
		NullCheck(L_72);
		uint16_t L_74 = String_get_Chars_m3015341861(L_72, L_73, /*hidden argument*/NULL);
		uint16_t L_75 = L_74;
		Il2CppObject * L_76 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_75);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_77 = String_Concat_m389863537(NULL /*static, unused*/, L_71, L_76, /*hidden argument*/NULL);
		V_3 = L_77;
		goto IL_0467;
	}

IL_01e4:
	{
		Stack_1_t3151028667 * L_78 = V_0;
		NullCheck(L_78);
		int32_t L_79 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Collections.Generic.Stack`1<SimpleJSON.JSONNode>::get_Count() */, L_78);
		if (L_79)
		{
			goto IL_01fa;
		}
	}
	{
		Exception_t1967233988 * L_80 = (Exception_t1967233988 *)il2cpp_codegen_object_new(Exception_t1967233988_il2cpp_TypeInfo_var);
		Exception__ctor_m3870771296(L_80, _stringLiteral1364402922, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_80);
	}

IL_01fa:
	{
		Stack_1_t3151028667 * L_81 = V_0;
		NullCheck(L_81);
		Stack_1_Pop_m3500129386(L_81, /*hidden argument*/Stack_1_Pop_m3500129386_MethodInfo_var);
		String_t* L_82 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_83 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_84 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_82, L_83, /*hidden argument*/NULL);
		if (!L_84)
		{
			goto IL_0255;
		}
	}
	{
		String_t* L_85 = V_4;
		NullCheck(L_85);
		String_t* L_86 = String_Trim_m1030489823(L_85, /*hidden argument*/NULL);
		V_4 = L_86;
		JSONNode_t580622632 * L_87 = V_1;
		if (!((JSONArray_t3478762607 *)IsInstClass(L_87, JSONArray_t3478762607_il2cpp_TypeInfo_var)))
		{
			goto IL_0236;
		}
	}
	{
		JSONNode_t580622632 * L_88 = V_1;
		String_t* L_89 = V_3;
		JSONNode_t580622632 * L_90 = JSONNode_op_Implicit_m2932149555(NULL /*static, unused*/, L_89, /*hidden argument*/NULL);
		NullCheck(L_88);
		VirtActionInvoker1< JSONNode_t580622632 * >::Invoke(12 /* System.Void SimpleJSON.JSONNode::Add(SimpleJSON.JSONNode) */, L_88, L_90);
		goto IL_0255;
	}

IL_0236:
	{
		String_t* L_91 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_92 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_93 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_91, L_92, /*hidden argument*/NULL);
		if (!L_93)
		{
			goto IL_0255;
		}
	}
	{
		JSONNode_t580622632 * L_94 = V_1;
		String_t* L_95 = V_4;
		String_t* L_96 = V_3;
		JSONNode_t580622632 * L_97 = JSONNode_op_Implicit_m2932149555(NULL /*static, unused*/, L_96, /*hidden argument*/NULL);
		NullCheck(L_94);
		VirtActionInvoker2< String_t*, JSONNode_t580622632 * >::Invoke(4 /* System.Void SimpleJSON.JSONNode::Add(System.String,SimpleJSON.JSONNode) */, L_94, L_95, L_97);
	}

IL_0255:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_98 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_4 = L_98;
		String_t* L_99 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_3 = L_99;
		Stack_1_t3151028667 * L_100 = V_0;
		NullCheck(L_100);
		int32_t L_101 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Collections.Generic.Stack`1<SimpleJSON.JSONNode>::get_Count() */, L_100);
		if ((((int32_t)L_101) <= ((int32_t)0)))
		{
			goto IL_0275;
		}
	}
	{
		Stack_1_t3151028667 * L_102 = V_0;
		NullCheck(L_102);
		JSONNode_t580622632 * L_103 = Stack_1_Peek_m1120330052(L_102, /*hidden argument*/Stack_1_Peek_m1120330052_MethodInfo_var);
		V_1 = L_103;
	}

IL_0275:
	{
		goto IL_0467;
	}

IL_027a:
	{
		bool L_104 = V_5;
		if (!L_104)
		{
			goto IL_0299;
		}
	}
	{
		String_t* L_105 = V_3;
		String_t* L_106 = ___aJSON0;
		int32_t L_107 = V_2;
		NullCheck(L_106);
		uint16_t L_108 = String_get_Chars_m3015341861(L_106, L_107, /*hidden argument*/NULL);
		uint16_t L_109 = L_108;
		Il2CppObject * L_110 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_109);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_111 = String_Concat_m389863537(NULL /*static, unused*/, L_105, L_110, /*hidden argument*/NULL);
		V_3 = L_111;
		goto IL_0467;
	}

IL_0299:
	{
		String_t* L_112 = V_3;
		V_4 = L_112;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_113 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_3 = L_113;
		goto IL_0467;
	}

IL_02a7:
	{
		bool L_114 = V_5;
		V_5 = (bool)((int32_t)((int32_t)L_114^(int32_t)1));
		goto IL_0467;
	}

IL_02b2:
	{
		bool L_115 = V_5;
		if (!L_115)
		{
			goto IL_02d1;
		}
	}
	{
		String_t* L_116 = V_3;
		String_t* L_117 = ___aJSON0;
		int32_t L_118 = V_2;
		NullCheck(L_117);
		uint16_t L_119 = String_get_Chars_m3015341861(L_117, L_118, /*hidden argument*/NULL);
		uint16_t L_120 = L_119;
		Il2CppObject * L_121 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_120);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_122 = String_Concat_m389863537(NULL /*static, unused*/, L_116, L_121, /*hidden argument*/NULL);
		V_3 = L_122;
		goto IL_0467;
	}

IL_02d1:
	{
		String_t* L_123 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_124 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_125 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_123, L_124, /*hidden argument*/NULL);
		if (!L_125)
		{
			goto IL_031c;
		}
	}
	{
		JSONNode_t580622632 * L_126 = V_1;
		if (!((JSONArray_t3478762607 *)IsInstClass(L_126, JSONArray_t3478762607_il2cpp_TypeInfo_var)))
		{
			goto IL_02fd;
		}
	}
	{
		JSONNode_t580622632 * L_127 = V_1;
		String_t* L_128 = V_3;
		JSONNode_t580622632 * L_129 = JSONNode_op_Implicit_m2932149555(NULL /*static, unused*/, L_128, /*hidden argument*/NULL);
		NullCheck(L_127);
		VirtActionInvoker1< JSONNode_t580622632 * >::Invoke(12 /* System.Void SimpleJSON.JSONNode::Add(SimpleJSON.JSONNode) */, L_127, L_129);
		goto IL_031c;
	}

IL_02fd:
	{
		String_t* L_130 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_131 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_132 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_130, L_131, /*hidden argument*/NULL);
		if (!L_132)
		{
			goto IL_031c;
		}
	}
	{
		JSONNode_t580622632 * L_133 = V_1;
		String_t* L_134 = V_4;
		String_t* L_135 = V_3;
		JSONNode_t580622632 * L_136 = JSONNode_op_Implicit_m2932149555(NULL /*static, unused*/, L_135, /*hidden argument*/NULL);
		NullCheck(L_133);
		VirtActionInvoker2< String_t*, JSONNode_t580622632 * >::Invoke(4 /* System.Void SimpleJSON.JSONNode::Add(System.String,SimpleJSON.JSONNode) */, L_133, L_134, L_136);
	}

IL_031c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_137 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_4 = L_137;
		String_t* L_138 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_3 = L_138;
		goto IL_0467;
	}

IL_032e:
	{
		goto IL_0467;
	}

IL_0333:
	{
		bool L_139 = V_5;
		if (!L_139)
		{
			goto IL_034d;
		}
	}
	{
		String_t* L_140 = V_3;
		String_t* L_141 = ___aJSON0;
		int32_t L_142 = V_2;
		NullCheck(L_141);
		uint16_t L_143 = String_get_Chars_m3015341861(L_141, L_142, /*hidden argument*/NULL);
		uint16_t L_144 = L_143;
		Il2CppObject * L_145 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_144);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_146 = String_Concat_m389863537(NULL /*static, unused*/, L_140, L_145, /*hidden argument*/NULL);
		V_3 = L_146;
	}

IL_034d:
	{
		goto IL_0467;
	}

IL_0352:
	{
		int32_t L_147 = V_2;
		V_2 = ((int32_t)((int32_t)L_147+(int32_t)1));
		bool L_148 = V_5;
		if (!L_148)
		{
			goto IL_044a;
		}
	}
	{
		String_t* L_149 = ___aJSON0;
		int32_t L_150 = V_2;
		NullCheck(L_149);
		uint16_t L_151 = String_get_Chars_m3015341861(L_149, L_150, /*hidden argument*/NULL);
		V_6 = L_151;
		uint16_t L_152 = V_6;
		V_9 = L_152;
		uint16_t L_153 = V_9;
		if (((int32_t)((int32_t)L_153-(int32_t)((int32_t)110))) == 0)
		{
			goto IL_03d1;
		}
		if (((int32_t)((int32_t)L_153-(int32_t)((int32_t)110))) == 1)
		{
			goto IL_0394;
		}
		if (((int32_t)((int32_t)L_153-(int32_t)((int32_t)110))) == 2)
		{
			goto IL_0394;
		}
		if (((int32_t)((int32_t)L_153-(int32_t)((int32_t)110))) == 3)
		{
			goto IL_0394;
		}
		if (((int32_t)((int32_t)L_153-(int32_t)((int32_t)110))) == 4)
		{
			goto IL_03be;
		}
		if (((int32_t)((int32_t)L_153-(int32_t)((int32_t)110))) == 5)
		{
			goto IL_0394;
		}
		if (((int32_t)((int32_t)L_153-(int32_t)((int32_t)110))) == 6)
		{
			goto IL_03ab;
		}
		if (((int32_t)((int32_t)L_153-(int32_t)((int32_t)110))) == 7)
		{
			goto IL_0409;
		}
	}

IL_0394:
	{
		uint16_t L_154 = V_9;
		if ((((int32_t)L_154) == ((int32_t)((int32_t)98))))
		{
			goto IL_03e4;
		}
	}
	{
		uint16_t L_155 = V_9;
		if ((((int32_t)L_155) == ((int32_t)((int32_t)102))))
		{
			goto IL_03f6;
		}
	}
	{
		goto IL_0437;
	}

IL_03ab:
	{
		String_t* L_156 = V_3;
		uint16_t L_157 = ((uint16_t)((int32_t)9));
		Il2CppObject * L_158 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_157);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_159 = String_Concat_m389863537(NULL /*static, unused*/, L_156, L_158, /*hidden argument*/NULL);
		V_3 = L_159;
		goto IL_044a;
	}

IL_03be:
	{
		String_t* L_160 = V_3;
		uint16_t L_161 = ((uint16_t)((int32_t)13));
		Il2CppObject * L_162 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_161);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_163 = String_Concat_m389863537(NULL /*static, unused*/, L_160, L_162, /*hidden argument*/NULL);
		V_3 = L_163;
		goto IL_044a;
	}

IL_03d1:
	{
		String_t* L_164 = V_3;
		uint16_t L_165 = ((uint16_t)((int32_t)10));
		Il2CppObject * L_166 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_165);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_167 = String_Concat_m389863537(NULL /*static, unused*/, L_164, L_166, /*hidden argument*/NULL);
		V_3 = L_167;
		goto IL_044a;
	}

IL_03e4:
	{
		String_t* L_168 = V_3;
		uint16_t L_169 = ((uint16_t)8);
		Il2CppObject * L_170 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_169);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_171 = String_Concat_m389863537(NULL /*static, unused*/, L_168, L_170, /*hidden argument*/NULL);
		V_3 = L_171;
		goto IL_044a;
	}

IL_03f6:
	{
		String_t* L_172 = V_3;
		uint16_t L_173 = ((uint16_t)((int32_t)12));
		Il2CppObject * L_174 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_173);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_175 = String_Concat_m389863537(NULL /*static, unused*/, L_172, L_174, /*hidden argument*/NULL);
		V_3 = L_175;
		goto IL_044a;
	}

IL_0409:
	{
		String_t* L_176 = ___aJSON0;
		int32_t L_177 = V_2;
		NullCheck(L_176);
		String_t* L_178 = String_Substring_m675079568(L_176, ((int32_t)((int32_t)L_177+(int32_t)1)), 4, /*hidden argument*/NULL);
		V_7 = L_178;
		String_t* L_179 = V_3;
		String_t* L_180 = V_7;
		int32_t L_181 = Int32_Parse_m4090328479(NULL /*static, unused*/, L_180, ((int32_t)512), /*hidden argument*/NULL);
		uint16_t L_182 = ((uint16_t)(((int32_t)((uint16_t)L_181))));
		Il2CppObject * L_183 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_182);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_184 = String_Concat_m389863537(NULL /*static, unused*/, L_179, L_183, /*hidden argument*/NULL);
		V_3 = L_184;
		int32_t L_185 = V_2;
		V_2 = ((int32_t)((int32_t)L_185+(int32_t)4));
		goto IL_044a;
	}

IL_0437:
	{
		String_t* L_186 = V_3;
		uint16_t L_187 = V_6;
		uint16_t L_188 = L_187;
		Il2CppObject * L_189 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_188);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_190 = String_Concat_m389863537(NULL /*static, unused*/, L_186, L_189, /*hidden argument*/NULL);
		V_3 = L_190;
		goto IL_044a;
	}

IL_044a:
	{
		goto IL_0467;
	}

IL_044f:
	{
		String_t* L_191 = V_3;
		String_t* L_192 = ___aJSON0;
		int32_t L_193 = V_2;
		NullCheck(L_192);
		uint16_t L_194 = String_get_Chars_m3015341861(L_192, L_193, /*hidden argument*/NULL);
		uint16_t L_195 = L_194;
		Il2CppObject * L_196 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_195);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_197 = String_Concat_m389863537(NULL /*static, unused*/, L_191, L_196, /*hidden argument*/NULL);
		V_3 = L_197;
		goto IL_0467;
	}

IL_0467:
	{
		int32_t L_198 = V_2;
		V_2 = ((int32_t)((int32_t)L_198+(int32_t)1));
	}

IL_046b:
	{
		int32_t L_199 = V_2;
		String_t* L_200 = ___aJSON0;
		NullCheck(L_200);
		int32_t L_201 = String_get_Length_m2979997331(L_200, /*hidden argument*/NULL);
		if ((((int32_t)L_199) < ((int32_t)L_201)))
		{
			goto IL_001f;
		}
	}
	{
		bool L_202 = V_5;
		if (!L_202)
		{
			goto IL_0489;
		}
	}
	{
		Exception_t1967233988 * L_203 = (Exception_t1967233988 *)il2cpp_codegen_object_new(Exception_t1967233988_il2cpp_TypeInfo_var);
		Exception__ctor_m3870771296(L_203, _stringLiteral1438436952, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_203);
	}

IL_0489:
	{
		JSONNode_t580622632 * L_204 = V_1;
		return L_204;
	}
}
// System.Void SimpleJSON.JSONNode::Serialize(System.IO.BinaryWriter)
extern "C"  void JSONNode_Serialize_m291040570 (JSONNode_t580622632 * __this, BinaryWriter_t2314211483 * ___aWriter0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SimpleJSON.JSONNode::SaveToStream(System.IO.Stream)
extern Il2CppClass* BinaryWriter_t2314211483_il2cpp_TypeInfo_var;
extern const uint32_t JSONNode_SaveToStream_m3872140282_MetadataUsageId;
extern "C"  void JSONNode_SaveToStream_m3872140282 (JSONNode_t580622632 * __this, Stream_t219029575 * ___aData0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONNode_SaveToStream_m3872140282_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	BinaryWriter_t2314211483 * V_0 = NULL;
	{
		Stream_t219029575 * L_0 = ___aData0;
		BinaryWriter_t2314211483 * L_1 = (BinaryWriter_t2314211483 *)il2cpp_codegen_object_new(BinaryWriter_t2314211483_il2cpp_TypeInfo_var);
		BinaryWriter__ctor_m3820043020(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		BinaryWriter_t2314211483 * L_2 = V_0;
		VirtActionInvoker1< BinaryWriter_t2314211483 * >::Invoke(28 /* System.Void SimpleJSON.JSONNode::Serialize(System.IO.BinaryWriter) */, __this, L_2);
		return;
	}
}
// System.Void SimpleJSON.JSONNode::SaveToCompressedStream(System.IO.Stream)
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral479969687;
extern const uint32_t JSONNode_SaveToCompressedStream_m1128141339_MetadataUsageId;
extern "C"  void JSONNode_SaveToCompressedStream_m1128141339 (JSONNode_t580622632 * __this, Stream_t219029575 * ___aData0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONNode_SaveToCompressedStream_m1128141339_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception_t1967233988 * L_0 = (Exception_t1967233988 *)il2cpp_codegen_object_new(Exception_t1967233988_il2cpp_TypeInfo_var);
		Exception__ctor_m3870771296(L_0, _stringLiteral479969687, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void SimpleJSON.JSONNode::SaveToCompressedFile(System.String)
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral479969687;
extern const uint32_t JSONNode_SaveToCompressedFile_m3524532162_MetadataUsageId;
extern "C"  void JSONNode_SaveToCompressedFile_m3524532162 (JSONNode_t580622632 * __this, String_t* ___aFileName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONNode_SaveToCompressedFile_m3524532162_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception_t1967233988 * L_0 = (Exception_t1967233988 *)il2cpp_codegen_object_new(Exception_t1967233988_il2cpp_TypeInfo_var);
		Exception__ctor_m3870771296(L_0, _stringLiteral479969687, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.String SimpleJSON.JSONNode::SaveToCompressedBase64()
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral479969687;
extern const uint32_t JSONNode_SaveToCompressedBase64_m3946704720_MetadataUsageId;
extern "C"  String_t* JSONNode_SaveToCompressedBase64_m3946704720 (JSONNode_t580622632 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONNode_SaveToCompressedBase64_m3946704720_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception_t1967233988 * L_0 = (Exception_t1967233988 *)il2cpp_codegen_object_new(Exception_t1967233988_il2cpp_TypeInfo_var);
		Exception__ctor_m3870771296(L_0, _stringLiteral479969687, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void SimpleJSON.JSONNode::SaveToFile(System.String)
extern Il2CppClass* FileInfo_t1355938065_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t JSONNode_SaveToFile_m2940669571_MetadataUsageId;
extern "C"  void JSONNode_SaveToFile_m2940669571 (JSONNode_t580622632 * __this, String_t* ___aFileName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONNode_SaveToFile_m2940669571_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FileStream_t1527309539 * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___aFileName0;
		FileInfo_t1355938065 * L_1 = (FileInfo_t1355938065 *)il2cpp_codegen_object_new(FileInfo_t1355938065_il2cpp_TypeInfo_var);
		FileInfo__ctor_m2163658659(L_1, L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		DirectoryInfo_t3421965634 * L_2 = FileInfo_get_Directory_m37520064(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(10 /* System.String System.IO.FileSystemInfo::get_FullName() */, L_2);
		Directory_CreateDirectory_m677877474(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		String_t* L_4 = ___aFileName0;
		FileStream_t1527309539 * L_5 = File_OpenWrite_m23896808(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
	}

IL_001d:
	try
	{ // begin try (depth: 1)
		FileStream_t1527309539 * L_6 = V_0;
		JSONNode_SaveToStream_m3872140282(__this, L_6, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x36, FINALLY_0029);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0029;
	}

FINALLY_0029:
	{ // begin finally (depth: 1)
		{
			FileStream_t1527309539 * L_7 = V_0;
			if (!L_7)
			{
				goto IL_0035;
			}
		}

IL_002f:
		{
			FileStream_t1527309539 * L_8 = V_0;
			NullCheck(L_8);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_8);
		}

IL_0035:
		{
			IL2CPP_END_FINALLY(41)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(41)
	{
		IL2CPP_JUMP_TBL(0x36, IL_0036)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0036:
	{
		return;
	}
}
// System.String SimpleJSON.JSONNode::SaveToBase64()
extern Il2CppClass* MemoryStream_t2881531048_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1097883944_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t JSONNode_SaveToBase64_m2556247919_MetadataUsageId;
extern "C"  String_t* JSONNode_SaveToBase64_m2556247919 (JSONNode_t580622632 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONNode_SaveToBase64_m2556247919_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	MemoryStream_t2881531048 * V_0 = NULL;
	String_t* V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		MemoryStream_t2881531048 * L_0 = (MemoryStream_t2881531048 *)il2cpp_codegen_object_new(MemoryStream_t2881531048_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m3603177736(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
	}

IL_0006:
	try
	{ // begin try (depth: 1)
		{
			MemoryStream_t2881531048 * L_1 = V_0;
			JSONNode_SaveToStream_m3872140282(__this, L_1, /*hidden argument*/NULL);
			MemoryStream_t2881531048 * L_2 = V_0;
			NullCheck(L_2);
			VirtActionInvoker1< int64_t >::Invoke(11 /* System.Void System.IO.MemoryStream::set_Position(System.Int64) */, L_2, (((int64_t)((int64_t)0))));
			MemoryStream_t2881531048 * L_3 = V_0;
			NullCheck(L_3);
			ByteU5BU5D_t58506160* L_4 = VirtFuncInvoker0< ByteU5BU5D_t58506160* >::Invoke(31 /* System.Byte[] System.IO.MemoryStream::ToArray() */, L_3);
			IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
			String_t* L_5 = Convert_ToBase64String_m1841808901(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
			V_1 = L_5;
			IL2CPP_LEAVE(0x38, FINALLY_002b);
		}

IL_0026:
		{
			; // IL_0026: leave IL_0038
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002b;
	}

FINALLY_002b:
	{ // begin finally (depth: 1)
		{
			MemoryStream_t2881531048 * L_6 = V_0;
			if (!L_6)
			{
				goto IL_0037;
			}
		}

IL_0031:
		{
			MemoryStream_t2881531048 * L_7 = V_0;
			NullCheck(L_7);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_7);
		}

IL_0037:
		{
			IL2CPP_END_FINALLY(43)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(43)
	{
		IL2CPP_JUMP_TBL(0x38, IL_0038)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0038:
	{
		String_t* L_8 = V_1;
		return L_8;
	}
}
// SimpleJSON.JSONNode SimpleJSON.JSONNode::Deserialize(System.IO.BinaryReader)
extern Il2CppClass* JSONArray_t3478762607_il2cpp_TypeInfo_var;
extern Il2CppClass* JSONClass_t3480415118_il2cpp_TypeInfo_var;
extern Il2CppClass* JSONData_t580311760_il2cpp_TypeInfo_var;
extern Il2CppClass* JSONBinaryTag_t1996404207_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1384384660;
extern const uint32_t JSONNode_Deserialize_m2114312606_MetadataUsageId;
extern "C"  JSONNode_t580622632 * JSONNode_Deserialize_m2114312606 (Il2CppObject * __this /* static, unused */, BinaryReader_t2158806251 * ___aReader0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONNode_Deserialize_m2114312606_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	JSONArray_t3478762607 * V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	JSONClass_t3480415118 * V_5 = NULL;
	int32_t V_6 = 0;
	String_t* V_7 = NULL;
	JSONNode_t580622632 * V_8 = NULL;
	int32_t V_9 = 0;
	{
		BinaryReader_t2158806251 * L_0 = ___aReader0;
		NullCheck(L_0);
		uint8_t L_1 = VirtFuncInvoker0< uint8_t >::Invoke(13 /* System.Byte System.IO.BinaryReader::ReadByte() */, L_0);
		V_0 = L_1;
		int32_t L_2 = V_0;
		V_9 = L_2;
		int32_t L_3 = V_9;
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 0)
		{
			goto IL_0034;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 1)
		{
			goto IL_0061;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 2)
		{
			goto IL_00a5;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 3)
		{
			goto IL_00b1;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 4)
		{
			goto IL_00bd;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 5)
		{
			goto IL_00c9;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)1)) == 6)
		{
			goto IL_00d5;
		}
	}
	{
		goto IL_00e1;
	}

IL_0034:
	{
		BinaryReader_t2158806251 * L_4 = ___aReader0;
		NullCheck(L_4);
		int32_t L_5 = VirtFuncInvoker0< int32_t >::Invoke(19 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, L_4);
		V_1 = L_5;
		JSONArray_t3478762607 * L_6 = (JSONArray_t3478762607 *)il2cpp_codegen_object_new(JSONArray_t3478762607_il2cpp_TypeInfo_var);
		JSONArray__ctor_m2704428242(L_6, /*hidden argument*/NULL);
		V_2 = L_6;
		V_3 = 0;
		goto IL_0058;
	}

IL_0048:
	{
		JSONArray_t3478762607 * L_7 = V_2;
		BinaryReader_t2158806251 * L_8 = ___aReader0;
		JSONNode_t580622632 * L_9 = JSONNode_Deserialize_m2114312606(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		VirtActionInvoker1< JSONNode_t580622632 * >::Invoke(12 /* System.Void SimpleJSON.JSONNode::Add(SimpleJSON.JSONNode) */, L_7, L_9);
		int32_t L_10 = V_3;
		V_3 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0058:
	{
		int32_t L_11 = V_3;
		int32_t L_12 = V_1;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_0048;
		}
	}
	{
		JSONArray_t3478762607 * L_13 = V_2;
		return L_13;
	}

IL_0061:
	{
		BinaryReader_t2158806251 * L_14 = ___aReader0;
		NullCheck(L_14);
		int32_t L_15 = VirtFuncInvoker0< int32_t >::Invoke(19 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, L_14);
		V_4 = L_15;
		JSONClass_t3480415118 * L_16 = (JSONClass_t3480415118 *)il2cpp_codegen_object_new(JSONClass_t3480415118_il2cpp_TypeInfo_var);
		JSONClass__ctor_m158050451(L_16, /*hidden argument*/NULL);
		V_5 = L_16;
		V_6 = 0;
		goto IL_0099;
	}

IL_0078:
	{
		BinaryReader_t2158806251 * L_17 = ___aReader0;
		NullCheck(L_17);
		String_t* L_18 = VirtFuncInvoker0< String_t* >::Invoke(22 /* System.String System.IO.BinaryReader::ReadString() */, L_17);
		V_7 = L_18;
		BinaryReader_t2158806251 * L_19 = ___aReader0;
		JSONNode_t580622632 * L_20 = JSONNode_Deserialize_m2114312606(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		V_8 = L_20;
		JSONClass_t3480415118 * L_21 = V_5;
		String_t* L_22 = V_7;
		JSONNode_t580622632 * L_23 = V_8;
		NullCheck(L_21);
		VirtActionInvoker2< String_t*, JSONNode_t580622632 * >::Invoke(4 /* System.Void SimpleJSON.JSONClass::Add(System.String,SimpleJSON.JSONNode) */, L_21, L_22, L_23);
		int32_t L_24 = V_6;
		V_6 = ((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_0099:
	{
		int32_t L_25 = V_6;
		int32_t L_26 = V_4;
		if ((((int32_t)L_25) < ((int32_t)L_26)))
		{
			goto IL_0078;
		}
	}
	{
		JSONClass_t3480415118 * L_27 = V_5;
		return L_27;
	}

IL_00a5:
	{
		BinaryReader_t2158806251 * L_28 = ___aReader0;
		NullCheck(L_28);
		String_t* L_29 = VirtFuncInvoker0< String_t* >::Invoke(22 /* System.String System.IO.BinaryReader::ReadString() */, L_28);
		JSONData_t580311760 * L_30 = (JSONData_t580311760 *)il2cpp_codegen_object_new(JSONData_t580311760_il2cpp_TypeInfo_var);
		JSONData__ctor_m1923599889(L_30, L_29, /*hidden argument*/NULL);
		return L_30;
	}

IL_00b1:
	{
		BinaryReader_t2158806251 * L_31 = ___aReader0;
		NullCheck(L_31);
		int32_t L_32 = VirtFuncInvoker0< int32_t >::Invoke(19 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, L_31);
		JSONData_t580311760 * L_33 = (JSONData_t580311760 *)il2cpp_codegen_object_new(JSONData_t580311760_il2cpp_TypeInfo_var);
		JSONData__ctor_m4065192034(L_33, L_32, /*hidden argument*/NULL);
		return L_33;
	}

IL_00bd:
	{
		BinaryReader_t2158806251 * L_34 = ___aReader0;
		NullCheck(L_34);
		double L_35 = VirtFuncInvoker0< double >::Invoke(17 /* System.Double System.IO.BinaryReader::ReadDouble() */, L_34);
		JSONData_t580311760 * L_36 = (JSONData_t580311760 *)il2cpp_codegen_object_new(JSONData_t580311760_il2cpp_TypeInfo_var);
		JSONData__ctor_m1355360849(L_36, L_35, /*hidden argument*/NULL);
		return L_36;
	}

IL_00c9:
	{
		BinaryReader_t2158806251 * L_37 = ___aReader0;
		NullCheck(L_37);
		bool L_38 = VirtFuncInvoker0< bool >::Invoke(12 /* System.Boolean System.IO.BinaryReader::ReadBoolean() */, L_37);
		JSONData_t580311760 * L_39 = (JSONData_t580311760 *)il2cpp_codegen_object_new(JSONData_t580311760_il2cpp_TypeInfo_var);
		JSONData__ctor_m4008077384(L_39, L_38, /*hidden argument*/NULL);
		return L_39;
	}

IL_00d5:
	{
		BinaryReader_t2158806251 * L_40 = ___aReader0;
		NullCheck(L_40);
		float L_41 = VirtFuncInvoker0< float >::Invoke(23 /* System.Single System.IO.BinaryReader::ReadSingle() */, L_40);
		JSONData_t580311760 * L_42 = (JSONData_t580311760 *)il2cpp_codegen_object_new(JSONData_t580311760_il2cpp_TypeInfo_var);
		JSONData__ctor_m1604923578(L_42, L_41, /*hidden argument*/NULL);
		return L_42;
	}

IL_00e1:
	{
		int32_t L_43 = V_0;
		int32_t L_44 = L_43;
		Il2CppObject * L_45 = Box(JSONBinaryTag_t1996404207_il2cpp_TypeInfo_var, &L_44);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_46 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral1384384660, L_45, /*hidden argument*/NULL);
		Exception_t1967233988 * L_47 = (Exception_t1967233988 *)il2cpp_codegen_object_new(Exception_t1967233988_il2cpp_TypeInfo_var);
		Exception__ctor_m3870771296(L_47, L_46, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_47);
	}
}
// SimpleJSON.JSONNode SimpleJSON.JSONNode::LoadFromCompressedFile(System.String)
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral479969687;
extern const uint32_t JSONNode_LoadFromCompressedFile_m3733501887_MetadataUsageId;
extern "C"  JSONNode_t580622632 * JSONNode_LoadFromCompressedFile_m3733501887 (Il2CppObject * __this /* static, unused */, String_t* ___aFileName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONNode_LoadFromCompressedFile_m3733501887_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception_t1967233988 * L_0 = (Exception_t1967233988 *)il2cpp_codegen_object_new(Exception_t1967233988_il2cpp_TypeInfo_var);
		Exception__ctor_m3870771296(L_0, _stringLiteral479969687, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// SimpleJSON.JSONNode SimpleJSON.JSONNode::LoadFromCompressedStream(System.IO.Stream)
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral479969687;
extern const uint32_t JSONNode_LoadFromCompressedStream_m3784239166_MetadataUsageId;
extern "C"  JSONNode_t580622632 * JSONNode_LoadFromCompressedStream_m3784239166 (Il2CppObject * __this /* static, unused */, Stream_t219029575 * ___aData0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONNode_LoadFromCompressedStream_m3784239166_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception_t1967233988 * L_0 = (Exception_t1967233988 *)il2cpp_codegen_object_new(Exception_t1967233988_il2cpp_TypeInfo_var);
		Exception__ctor_m3870771296(L_0, _stringLiteral479969687, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// SimpleJSON.JSONNode SimpleJSON.JSONNode::LoadFromCompressedBase64(System.String)
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral479969687;
extern const uint32_t JSONNode_LoadFromCompressedBase64_m4072157452_MetadataUsageId;
extern "C"  JSONNode_t580622632 * JSONNode_LoadFromCompressedBase64_m4072157452 (Il2CppObject * __this /* static, unused */, String_t* ___aBase640, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONNode_LoadFromCompressedBase64_m4072157452_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception_t1967233988 * L_0 = (Exception_t1967233988 *)il2cpp_codegen_object_new(Exception_t1967233988_il2cpp_TypeInfo_var);
		Exception__ctor_m3870771296(L_0, _stringLiteral479969687, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// SimpleJSON.JSONNode SimpleJSON.JSONNode::LoadFromStream(System.IO.Stream)
extern Il2CppClass* BinaryReader_t2158806251_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t JSONNode_LoadFromStream_m1296613853_MetadataUsageId;
extern "C"  JSONNode_t580622632 * JSONNode_LoadFromStream_m1296613853 (Il2CppObject * __this /* static, unused */, Stream_t219029575 * ___aData0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONNode_LoadFromStream_m1296613853_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	BinaryReader_t2158806251 * V_0 = NULL;
	JSONNode_t580622632 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Stream_t219029575 * L_0 = ___aData0;
		BinaryReader_t2158806251 * L_1 = (BinaryReader_t2158806251 *)il2cpp_codegen_object_new(BinaryReader_t2158806251_il2cpp_TypeInfo_var);
		BinaryReader__ctor_m449904828(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			BinaryReader_t2158806251 * L_2 = V_0;
			JSONNode_t580622632 * L_3 = JSONNode_Deserialize_m2114312606(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
			V_1 = L_3;
			IL2CPP_LEAVE(0x25, FINALLY_0018);
		}

IL_0013:
		{
			; // IL_0013: leave IL_0025
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0018;
	}

FINALLY_0018:
	{ // begin finally (depth: 1)
		{
			BinaryReader_t2158806251 * L_4 = V_0;
			if (!L_4)
			{
				goto IL_0024;
			}
		}

IL_001e:
		{
			BinaryReader_t2158806251 * L_5 = V_0;
			NullCheck(L_5);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_5);
		}

IL_0024:
		{
			IL2CPP_END_FINALLY(24)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(24)
	{
		IL2CPP_JUMP_TBL(0x25, IL_0025)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0025:
	{
		JSONNode_t580622632 * L_6 = V_1;
		return L_6;
	}
}
// SimpleJSON.JSONNode SimpleJSON.JSONNode::LoadFromFile(System.String)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t JSONNode_LoadFromFile_m329717440_MetadataUsageId;
extern "C"  JSONNode_t580622632 * JSONNode_LoadFromFile_m329717440 (Il2CppObject * __this /* static, unused */, String_t* ___aFileName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONNode_LoadFromFile_m329717440_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FileStream_t1527309539 * V_0 = NULL;
	JSONNode_t580622632 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___aFileName0;
		FileStream_t1527309539 * L_1 = File_OpenRead_m3104031109(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			FileStream_t1527309539 * L_2 = V_0;
			JSONNode_t580622632 * L_3 = JSONNode_LoadFromStream_m1296613853(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
			V_1 = L_3;
			IL2CPP_LEAVE(0x25, FINALLY_0018);
		}

IL_0013:
		{
			; // IL_0013: leave IL_0025
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0018;
	}

FINALLY_0018:
	{ // begin finally (depth: 1)
		{
			FileStream_t1527309539 * L_4 = V_0;
			if (!L_4)
			{
				goto IL_0024;
			}
		}

IL_001e:
		{
			FileStream_t1527309539 * L_5 = V_0;
			NullCheck(L_5);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_5);
		}

IL_0024:
		{
			IL2CPP_END_FINALLY(24)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(24)
	{
		IL2CPP_JUMP_TBL(0x25, IL_0025)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0025:
	{
		JSONNode_t580622632 * L_6 = V_1;
		return L_6;
	}
}
// SimpleJSON.JSONNode SimpleJSON.JSONNode::LoadFromBase64(System.String)
extern Il2CppClass* Convert_t1097883944_il2cpp_TypeInfo_var;
extern Il2CppClass* MemoryStream_t2881531048_il2cpp_TypeInfo_var;
extern const uint32_t JSONNode_LoadFromBase64_m1505416141_MetadataUsageId;
extern "C"  JSONNode_t580622632 * JSONNode_LoadFromBase64_m1505416141 (Il2CppObject * __this /* static, unused */, String_t* ___aBase640, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONNode_LoadFromBase64_m1505416141_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t58506160* V_0 = NULL;
	MemoryStream_t2881531048 * V_1 = NULL;
	{
		String_t* L_0 = ___aBase640;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		ByteU5BU5D_t58506160* L_1 = Convert_FromBase64String_m901846280(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		ByteU5BU5D_t58506160* L_2 = V_0;
		MemoryStream_t2881531048 * L_3 = (MemoryStream_t2881531048 *)il2cpp_codegen_object_new(MemoryStream_t2881531048_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m1231145921(L_3, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		MemoryStream_t2881531048 * L_4 = V_1;
		NullCheck(L_4);
		VirtActionInvoker1< int64_t >::Invoke(11 /* System.Void System.IO.MemoryStream::set_Position(System.Int64) */, L_4, (((int64_t)((int64_t)0))));
		MemoryStream_t2881531048 * L_5 = V_1;
		JSONNode_t580622632 * L_6 = JSONNode_LoadFromStream_m1296613853(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.String)
extern Il2CppClass* JSONData_t580311760_il2cpp_TypeInfo_var;
extern const uint32_t JSONNode_op_Implicit_m2932149555_MetadataUsageId;
extern "C"  JSONNode_t580622632 * JSONNode_op_Implicit_m2932149555 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONNode_op_Implicit_m2932149555_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___s0;
		JSONData_t580311760 * L_1 = (JSONData_t580311760 *)il2cpp_codegen_object_new(JSONData_t580311760_il2cpp_TypeInfo_var);
		JSONData__ctor_m1923599889(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern "C"  String_t* JSONNode_op_Implicit_m3884568727 (Il2CppObject * __this /* static, unused */, JSONNode_t580622632 * ___d0, const MethodInfo* method)
{
	String_t* G_B3_0 = NULL;
	{
		JSONNode_t580622632 * L_0 = ___d0;
		bool L_1 = JSONNode_op_Equality_m912335575(NULL /*static, unused*/, L_0, NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B3_0 = ((String_t*)(NULL));
		goto IL_0018;
	}

IL_0012:
	{
		JSONNode_t580622632 * L_2 = ___d0;
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String SimpleJSON.JSONNode::get_Value() */, L_2);
		G_B3_0 = L_3;
	}

IL_0018:
	{
		return G_B3_0;
	}
}
// System.Boolean SimpleJSON.JSONNode::op_Equality(SimpleJSON.JSONNode,System.Object)
extern Il2CppClass* JSONLazyCreator_t3067077166_il2cpp_TypeInfo_var;
extern const uint32_t JSONNode_op_Equality_m912335575_MetadataUsageId;
extern "C"  bool JSONNode_op_Equality_m912335575 (Il2CppObject * __this /* static, unused */, JSONNode_t580622632 * ___a0, Il2CppObject * ___b1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JSONNode_op_Equality_m912335575_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___b1;
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		JSONNode_t580622632 * L_1 = ___a0;
		if (!((JSONLazyCreator_t3067077166 *)IsInstClass(L_1, JSONLazyCreator_t3067077166_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		return (bool)1;
	}

IL_0013:
	{
		JSONNode_t580622632 * L_2 = ___a0;
		Il2CppObject * L_3 = ___b1;
		bool L_4 = Object_ReferenceEquals_m3695130242(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean SimpleJSON.JSONNode::op_Inequality(SimpleJSON.JSONNode,System.Object)
extern "C"  bool JSONNode_op_Inequality_m1017226386 (Il2CppObject * __this /* static, unused */, JSONNode_t580622632 * ___a0, Il2CppObject * ___b1, const MethodInfo* method)
{
	{
		JSONNode_t580622632 * L_0 = ___a0;
		Il2CppObject * L_1 = ___b1;
		bool L_2 = JSONNode_op_Equality_m912335575(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void SimpleJSON.JSONNode/<>c__Iterator0::.ctor()
extern "C"  void U3CU3Ec__Iterator0__ctor_m862538589 (U3CU3Ec__Iterator0_t2516598209 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// SimpleJSON.JSONNode SimpleJSON.JSONNode/<>c__Iterator0::System.Collections.Generic.IEnumerator<SimpleJSON.JSONNode>.get_Current()
extern "C"  JSONNode_t580622632 * U3CU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m207236806 (U3CU3Ec__Iterator0_t2516598209 * __this, const MethodInfo* method)
{
	{
		JSONNode_t580622632 * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object SimpleJSON.JSONNode/<>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m4022215667 (U3CU3Ec__Iterator0_t2516598209 * __this, const MethodInfo* method)
{
	{
		JSONNode_t580622632 * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Collections.IEnumerator SimpleJSON.JSONNode/<>c__Iterator0::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CU3Ec__Iterator0_System_Collections_IEnumerable_GetEnumerator_m872739292 (U3CU3Ec__Iterator0_t2516598209 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = U3CU3Ec__Iterator0_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m2415553691(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode/<>c__Iterator0::System.Collections.Generic.IEnumerable<SimpleJSON.JSONNode>.GetEnumerator()
extern Il2CppClass* U3CU3Ec__Iterator0_t2516598209_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec__Iterator0_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m2415553691_MetadataUsageId;
extern "C"  Il2CppObject* U3CU3Ec__Iterator0_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m2415553691 (U3CU3Ec__Iterator0_t2516598209 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__Iterator0_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m2415553691_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t* L_0 = __this->get_address_of_U24PC_0();
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, L_0, 0, ((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CU3Ec__Iterator0_t2516598209 * L_2 = (U3CU3Ec__Iterator0_t2516598209 *)il2cpp_codegen_object_new(U3CU3Ec__Iterator0_t2516598209_il2cpp_TypeInfo_var);
		U3CU3Ec__Iterator0__ctor_m862538589(L_2, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean SimpleJSON.JSONNode/<>c__Iterator0::MoveNext()
extern "C"  bool U3CU3Ec__Iterator0_MoveNext_m2619481415 (U3CU3Ec__Iterator0_t2516598209 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U24PC_0();
		__this->set_U24PC_0((-1));
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		goto IL_0017;
	}

IL_0017:
	{
		return (bool)0;
	}
}
// System.Void SimpleJSON.JSONNode/<>c__Iterator0::Dispose()
extern "C"  void U3CU3Ec__Iterator0_Dispose_m4164146266 (U3CU3Ec__Iterator0_t2516598209 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void SimpleJSON.JSONNode/<>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec__Iterator0_Reset_m2803938826_MetadataUsageId;
extern "C"  void U3CU3Ec__Iterator0_Reset_m2803938826 (U3CU3Ec__Iterator0_t2516598209 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__Iterator0_Reset_m2803938826_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void SimpleJSON.JSONNode/<>c__Iterator1::.ctor()
extern "C"  void U3CU3Ec__Iterator1__ctor_m666025084 (U3CU3Ec__Iterator1_t2516598210 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// SimpleJSON.JSONNode SimpleJSON.JSONNode/<>c__Iterator1::System.Collections.Generic.IEnumerator<SimpleJSON.JSONNode>.get_Current()
extern "C"  JSONNode_t580622632 * U3CU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m1316052005 (U3CU3Ec__Iterator1_t2516598210 * __this, const MethodInfo* method)
{
	{
		JSONNode_t580622632 * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Object SimpleJSON.JSONNode/<>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m1698187828 (U3CU3Ec__Iterator1_t2516598210 * __this, const MethodInfo* method)
{
	{
		JSONNode_t580622632 * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Collections.IEnumerator SimpleJSON.JSONNode/<>c__Iterator1::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CU3Ec__Iterator1_System_Collections_IEnumerable_GetEnumerator_m864979933 (U3CU3Ec__Iterator1_t2516598210 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = U3CU3Ec__Iterator1_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m2835070522(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode/<>c__Iterator1::System.Collections.Generic.IEnumerable<SimpleJSON.JSONNode>.GetEnumerator()
extern Il2CppClass* U3CU3Ec__Iterator1_t2516598210_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec__Iterator1_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m2835070522_MetadataUsageId;
extern "C"  Il2CppObject* U3CU3Ec__Iterator1_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m2835070522 (U3CU3Ec__Iterator1_t2516598210 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__Iterator1_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m2835070522_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__Iterator1_t2516598210 * V_0 = NULL;
	{
		int32_t* L_0 = __this->get_address_of_U24PC_4();
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, L_0, 0, ((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CU3Ec__Iterator1_t2516598210 * L_2 = (U3CU3Ec__Iterator1_t2516598210 *)il2cpp_codegen_object_new(U3CU3Ec__Iterator1_t2516598210_il2cpp_TypeInfo_var);
		U3CU3Ec__Iterator1__ctor_m666025084(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		U3CU3Ec__Iterator1_t2516598210 * L_3 = V_0;
		JSONNode_t580622632 * L_4 = __this->get_U3CU3Ef__this_6();
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_6(L_4);
		U3CU3Ec__Iterator1_t2516598210 * L_5 = V_0;
		return L_5;
	}
}
// System.Boolean SimpleJSON.JSONNode/<>c__Iterator1::MoveNext()
extern Il2CppClass* IEnumerable_1_t3452776988_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t2063729080_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec__Iterator1_MoveNext_m2326078408_MetadataUsageId;
extern "C"  bool U3CU3Ec__Iterator1_MoveNext_m2326078408 (U3CU3Ec__Iterator1_t2516598210 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__Iterator1_MoveNext_m2326078408_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_003c;
		}
	}
	{
		goto IL_0116;
	}

IL_0023:
	{
		JSONNode_t580622632 * L_2 = __this->get_U3CU3Ef__this_6();
		NullCheck(L_2);
		Il2CppObject* L_3 = VirtFuncInvoker0< Il2CppObject* >::Invoke(16 /* System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode::get_Childs() */, L_2);
		NullCheck(L_3);
		Il2CppObject* L_4 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode>::GetEnumerator() */, IEnumerable_1_t3452776988_il2cpp_TypeInfo_var, L_3);
		__this->set_U3CU24s_16U3E__0_0(L_4);
		V_0 = ((int32_t)-3);
	}

IL_003c:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_5 = V_0;
			if (((int32_t)((int32_t)L_5-(int32_t)1)) == 0)
			{
				goto IL_0077;
			}
		}

IL_0048:
		{
			goto IL_00e1;
		}

IL_004d:
		{
			Il2CppObject* L_6 = __this->get_U3CU24s_16U3E__0_0();
			NullCheck(L_6);
			JSONNode_t580622632 * L_7 = InterfaceFuncInvoker0< JSONNode_t580622632 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode>::get_Current() */, IEnumerator_1_t2063729080_il2cpp_TypeInfo_var, L_6);
			__this->set_U3CCU3E__1_1(L_7);
			JSONNode_t580622632 * L_8 = __this->get_U3CCU3E__1_1();
			NullCheck(L_8);
			Il2CppObject* L_9 = JSONNode_get_DeepChilds_m4161671309(L_8, /*hidden argument*/NULL);
			NullCheck(L_9);
			Il2CppObject* L_10 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode>::GetEnumerator() */, IEnumerable_1_t3452776988_il2cpp_TypeInfo_var, L_9);
			__this->set_U3CU24s_17U3E__2_2(L_10);
			V_0 = ((int32_t)-3);
		}

IL_0077:
		try
		{ // begin try (depth: 2)
			{
				uint32_t L_11 = V_0;
				if (((int32_t)((int32_t)L_11-(int32_t)1)) == 0)
				{
					goto IL_00b3;
				}
			}

IL_0083:
			{
				goto IL_00b3;
			}

IL_0088:
			{
				Il2CppObject* L_12 = __this->get_U3CU24s_17U3E__2_2();
				NullCheck(L_12);
				JSONNode_t580622632 * L_13 = InterfaceFuncInvoker0< JSONNode_t580622632 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode>::get_Current() */, IEnumerator_1_t2063729080_il2cpp_TypeInfo_var, L_12);
				__this->set_U3CDU3E__3_3(L_13);
				JSONNode_t580622632 * L_14 = __this->get_U3CDU3E__3_3();
				__this->set_U24current_5(L_14);
				__this->set_U24PC_4(1);
				V_1 = (bool)1;
				IL2CPP_LEAVE(0x118, FINALLY_00c8);
			}

IL_00b3:
			{
				Il2CppObject* L_15 = __this->get_U3CU24s_17U3E__2_2();
				NullCheck(L_15);
				bool L_16 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_15);
				if (L_16)
				{
					goto IL_0088;
				}
			}

IL_00c3:
			{
				IL2CPP_LEAVE(0xE1, FINALLY_00c8);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_00c8;
		}

FINALLY_00c8:
		{ // begin finally (depth: 2)
			{
				bool L_17 = V_1;
				if (!L_17)
				{
					goto IL_00cc;
				}
			}

IL_00cb:
			{
				IL2CPP_END_FINALLY(200)
			}

IL_00cc:
			{
				Il2CppObject* L_18 = __this->get_U3CU24s_17U3E__2_2();
				if (L_18)
				{
					goto IL_00d5;
				}
			}

IL_00d4:
			{
				IL2CPP_END_FINALLY(200)
			}

IL_00d5:
			{
				Il2CppObject* L_19 = __this->get_U3CU24s_17U3E__2_2();
				NullCheck(L_19);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_19);
				IL2CPP_END_FINALLY(200)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(200)
		{
			IL2CPP_END_CLEANUP(0x118, FINALLY_00f6);
			IL2CPP_JUMP_TBL(0xE1, IL_00e1)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_00e1:
		{
			Il2CppObject* L_20 = __this->get_U3CU24s_16U3E__0_0();
			NullCheck(L_20);
			bool L_21 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_20);
			if (L_21)
			{
				goto IL_004d;
			}
		}

IL_00f1:
		{
			IL2CPP_LEAVE(0x10F, FINALLY_00f6);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00f6;
	}

FINALLY_00f6:
	{ // begin finally (depth: 1)
		{
			bool L_22 = V_1;
			if (!L_22)
			{
				goto IL_00fa;
			}
		}

IL_00f9:
		{
			IL2CPP_END_FINALLY(246)
		}

IL_00fa:
		{
			Il2CppObject* L_23 = __this->get_U3CU24s_16U3E__0_0();
			if (L_23)
			{
				goto IL_0103;
			}
		}

IL_0102:
		{
			IL2CPP_END_FINALLY(246)
		}

IL_0103:
		{
			Il2CppObject* L_24 = __this->get_U3CU24s_16U3E__0_0();
			NullCheck(L_24);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_24);
			IL2CPP_END_FINALLY(246)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(246)
	{
		IL2CPP_JUMP_TBL(0x118, IL_0118)
		IL2CPP_JUMP_TBL(0x10F, IL_010f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_010f:
	{
		__this->set_U24PC_4((-1));
	}

IL_0116:
	{
		return (bool)0;
	}

IL_0118:
	{
		return (bool)1;
	}
	// Dead block : IL_011a: ldloc.2
}
// System.Void SimpleJSON.JSONNode/<>c__Iterator1::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec__Iterator1_Dispose_m4293228985_MetadataUsageId;
extern "C"  void U3CU3Ec__Iterator1_Dispose_m4293228985 (U3CU3Ec__Iterator1_t2516598210 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__Iterator1_Dispose_m4293228985_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0055;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_0055;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			IL2CPP_LEAVE(0x3B, FINALLY_0026);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0026;
		}

FINALLY_0026:
		{ // begin finally (depth: 2)
			{
				Il2CppObject* L_2 = __this->get_U3CU24s_17U3E__2_2();
				if (L_2)
				{
					goto IL_002f;
				}
			}

IL_002e:
			{
				IL2CPP_END_FINALLY(38)
			}

IL_002f:
			{
				Il2CppObject* L_3 = __this->get_U3CU24s_17U3E__2_2();
				NullCheck(L_3);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_3);
				IL2CPP_END_FINALLY(38)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(38)
		{
			IL2CPP_JUMP_TBL(0x3B, IL_003b)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_003b:
		{
			IL2CPP_LEAVE(0x55, FINALLY_0040);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0040;
	}

FINALLY_0040:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_4 = __this->get_U3CU24s_16U3E__0_0();
			if (L_4)
			{
				goto IL_0049;
			}
		}

IL_0048:
		{
			IL2CPP_END_FINALLY(64)
		}

IL_0049:
		{
			Il2CppObject* L_5 = __this->get_U3CU24s_16U3E__0_0();
			NullCheck(L_5);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_5);
			IL2CPP_END_FINALLY(64)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(64)
	{
		IL2CPP_JUMP_TBL(0x55, IL_0055)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0055:
	{
		return;
	}
}
// System.Void SimpleJSON.JSONNode/<>c__Iterator1::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec__Iterator1_Reset_m2607425321_MetadataUsageId;
extern "C"  void U3CU3Ec__Iterator1_Reset_m2607425321 (U3CU3Ec__Iterator1_t2516598210 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__Iterator1_Reset_m2607425321_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void TrackEventTests::.ctor()
extern "C"  void TrackEventTests__ctor_m3305739077 (TrackEventTests_t1452154674 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m350695606(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TrackEventTests::Start()
extern Il2CppCodeGenString* _stringLiteral4262950194;
extern const uint32_t TrackEventTests_Start_m2252876869_MetadataUsageId;
extern "C"  void TrackEventTests_Start_m2252876869 (TrackEventTests_t1452154674 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TrackEventTests_Start_m2252876869_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		MonoBehaviour_print_m1497342762(NULL /*static, unused*/, _stringLiteral4262950194, /*hidden argument*/NULL);
		TrackEventTests_TrackRichEventTest_m2404782308(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TrackEventTests::Update()
extern "C"  void TrackEventTests_Update_m1125558376 (TrackEventTests_t1452154674 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void TrackEventTests::TrackRichEventTest()
extern Il2CppClass* Dictionary_2_t2606186806_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m640701813_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1542855352;
extern Il2CppCodeGenString* _stringLiteral575402001;
extern Il2CppCodeGenString* _stringLiteral84326;
extern Il2CppCodeGenString* _stringLiteral3243136618;
extern Il2CppCodeGenString* _stringLiteral1450572480;
extern Il2CppCodeGenString* _stringLiteral106934601;
extern Il2CppCodeGenString* _stringLiteral48625;
extern Il2CppCodeGenString* _stringLiteral61027152;
extern const uint32_t TrackEventTests_TrackRichEventTest_m2404782308_MetadataUsageId;
extern "C"  void TrackEventTests_TrackRichEventTest_m2404782308 (TrackEventTests_t1452154674 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TrackEventTests_TrackRichEventTest_m2404782308_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t2606186806 * V_0 = NULL;
	{
		MonoBehaviour_print_m1497342762(NULL /*static, unused*/, _stringLiteral1542855352, /*hidden argument*/NULL);
		Dictionary_2_t2606186806 * L_0 = (Dictionary_2_t2606186806 *)il2cpp_codegen_object_new(Dictionary_2_t2606186806_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m640701813(L_0, /*hidden argument*/Dictionary_2__ctor_m640701813_MethodInfo_var);
		V_0 = L_0;
		Dictionary_2_t2606186806 * L_1 = V_0;
		NullCheck(L_1);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::Add(!0,!1) */, L_1, _stringLiteral575402001, _stringLiteral84326);
		Dictionary_2_t2606186806 * L_2 = V_0;
		NullCheck(L_2);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::Add(!0,!1) */, L_2, _stringLiteral3243136618, _stringLiteral1450572480);
		Dictionary_2_t2606186806 * L_3 = V_0;
		NullCheck(L_3);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::Add(!0,!1) */, L_3, _stringLiteral106934601, _stringLiteral48625);
		Dictionary_2_t2606186806 * L_4 = V_0;
		AppsFlyer_trackRichEvent_m3321556035(NULL /*static, unused*/, _stringLiteral61027152, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TrackEventTests::ValidateReceiptTest()
extern "C"  void TrackEventTests_ValidateReceiptTest_m2345315287 (TrackEventTests_t1452154674 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.AnimationCurveReactiveProperty::.ctor()
extern Il2CppClass* ReactiveProperty_1_t3951240486_il2cpp_TypeInfo_var;
extern const MethodInfo* ReactiveProperty_1__ctor_m2070470040_MethodInfo_var;
extern const uint32_t AnimationCurveReactiveProperty__ctor_m3150323390_MetadataUsageId;
extern "C"  void AnimationCurveReactiveProperty__ctor_m3150323390 (AnimationCurveReactiveProperty_t3280306475 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimationCurveReactiveProperty__ctor_m3150323390_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ReactiveProperty_1_t3951240486_il2cpp_TypeInfo_var);
		ReactiveProperty_1__ctor_m2070470040(__this, /*hidden argument*/ReactiveProperty_1__ctor_m2070470040_MethodInfo_var);
		return;
	}
}
// System.Void UniRx.AnimationCurveReactiveProperty::.ctor(UnityEngine.AnimationCurve)
extern Il2CppClass* ReactiveProperty_1_t3951240486_il2cpp_TypeInfo_var;
extern const MethodInfo* ReactiveProperty_1__ctor_m4055030470_MethodInfo_var;
extern const uint32_t AnimationCurveReactiveProperty__ctor_m1196052988_MetadataUsageId;
extern "C"  void AnimationCurveReactiveProperty__ctor_m1196052988 (AnimationCurveReactiveProperty_t3280306475 * __this, AnimationCurve_t3342907448 * ___initialValue0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimationCurveReactiveProperty__ctor_m1196052988_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AnimationCurve_t3342907448 * L_0 = ___initialValue0;
		IL2CPP_RUNTIME_CLASS_INIT(ReactiveProperty_1_t3951240486_il2cpp_TypeInfo_var);
		ReactiveProperty_1__ctor_m4055030470(__this, L_0, /*hidden argument*/ReactiveProperty_1__ctor_m4055030470_MethodInfo_var);
		return;
	}
}
// UniRx.IObservable`1<UnityEngine.AsyncOperation> UniRx.AsyncOperationExtensions::AsObservable(UnityEngine.AsyncOperation,UniRx.IProgress`1<System.Single>)
extern Il2CppClass* U3CAsObservableU3Ec__AnonStorey7B_t3743595045_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_3_t3732850935_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CAsObservableU3Ec__AnonStorey7B_U3CU3Em__A3_m2420906715_MethodInfo_var;
extern const MethodInfo* Func_3__ctor_m2760845612_MethodInfo_var;
extern const MethodInfo* Observable_FromCoroutine_TisAsyncOperation_t3374395064_m2741885049_MethodInfo_var;
extern const uint32_t AsyncOperationExtensions_AsObservable_m962243477_MetadataUsageId;
extern "C"  Il2CppObject* AsyncOperationExtensions_AsObservable_m962243477 (Il2CppObject * __this /* static, unused */, AsyncOperation_t3374395064 * ___asyncOperation0, Il2CppObject* ___progress1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AsyncOperationExtensions_AsObservable_m962243477_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CAsObservableU3Ec__AnonStorey7B_t3743595045 * V_0 = NULL;
	{
		U3CAsObservableU3Ec__AnonStorey7B_t3743595045 * L_0 = (U3CAsObservableU3Ec__AnonStorey7B_t3743595045 *)il2cpp_codegen_object_new(U3CAsObservableU3Ec__AnonStorey7B_t3743595045_il2cpp_TypeInfo_var);
		U3CAsObservableU3Ec__AnonStorey7B__ctor_m713408322(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CAsObservableU3Ec__AnonStorey7B_t3743595045 * L_1 = V_0;
		AsyncOperation_t3374395064 * L_2 = ___asyncOperation0;
		NullCheck(L_1);
		L_1->set_asyncOperation_0(L_2);
		U3CAsObservableU3Ec__AnonStorey7B_t3743595045 * L_3 = V_0;
		Il2CppObject* L_4 = ___progress1;
		NullCheck(L_3);
		L_3->set_progress_1(L_4);
		U3CAsObservableU3Ec__AnonStorey7B_t3743595045 * L_5 = V_0;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)U3CAsObservableU3Ec__AnonStorey7B_U3CU3Em__A3_m2420906715_MethodInfo_var);
		Func_3_t3732850935 * L_7 = (Func_3_t3732850935 *)il2cpp_codegen_object_new(Func_3_t3732850935_il2cpp_TypeInfo_var);
		Func_3__ctor_m2760845612(L_7, L_5, L_6, /*hidden argument*/Func_3__ctor_m2760845612_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_8 = Observable_FromCoroutine_TisAsyncOperation_t3374395064_m2741885049(NULL /*static, unused*/, L_7, /*hidden argument*/Observable_FromCoroutine_TisAsyncOperation_t3374395064_m2741885049_MethodInfo_var);
		return L_8;
	}
}
// System.Void UniRx.AsyncOperationExtensions/<AsObservable>c__AnonStorey7B::.ctor()
extern "C"  void U3CAsObservableU3Ec__AnonStorey7B__ctor_m713408322 (U3CAsObservableU3Ec__AnonStorey7B_t3743595045 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UniRx.AsyncOperationExtensions/<AsObservable>c__AnonStorey7B::<>m__A3(UniRx.IObserver`1<UnityEngine.AsyncOperation>,UniRx.CancellationToken)
extern const MethodInfo* AsyncOperationExtensions_AsObservableCore_TisAsyncOperation_t3374395064_m3718184494_MethodInfo_var;
extern const uint32_t U3CAsObservableU3Ec__AnonStorey7B_U3CU3Em__A3_m2420906715_MetadataUsageId;
extern "C"  Il2CppObject * U3CAsObservableU3Ec__AnonStorey7B_U3CU3Em__A3_m2420906715 (U3CAsObservableU3Ec__AnonStorey7B_t3743595045 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellation1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CAsObservableU3Ec__AnonStorey7B_U3CU3Em__A3_m2420906715_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AsyncOperation_t3374395064 * L_0 = __this->get_asyncOperation_0();
		Il2CppObject* L_1 = ___observer0;
		Il2CppObject* L_2 = __this->get_progress_1();
		CancellationToken_t1439151560  L_3 = ___cancellation1;
		Il2CppObject * L_4 = AsyncOperationExtensions_AsObservableCore_TisAsyncOperation_t3374395064_m3718184494(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/AsyncOperationExtensions_AsObservableCore_TisAsyncOperation_t3374395064_m3718184494_MethodInfo_var);
		return L_4;
	}
}
// System.Void UniRx.BooleanDisposable::.ctor()
extern "C"  void BooleanDisposable__ctor_m2534881639 (BooleanDisposable_t3065601722 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.BooleanDisposable::.ctor(System.Boolean)
extern "C"  void BooleanDisposable__ctor_m3878547102 (BooleanDisposable_t3065601722 * __this, bool ___isDisposed0, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		bool L_0 = ___isDisposed0;
		BooleanDisposable_set_IsDisposed_m4066294816(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UniRx.BooleanDisposable::get_IsDisposed()
extern "C"  bool BooleanDisposable_get_IsDisposed_m2624310745 (BooleanDisposable_t3065601722 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_U3CIsDisposedU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void UniRx.BooleanDisposable::set_IsDisposed(System.Boolean)
extern "C"  void BooleanDisposable_set_IsDisposed_m4066294816 (BooleanDisposable_t3065601722 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CIsDisposedU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void UniRx.BooleanDisposable::Dispose()
extern "C"  void BooleanDisposable_Dispose_m673081316 (BooleanDisposable_t3065601722 * __this, const MethodInfo* method)
{
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean UniRx.BooleanDisposable::get_IsDisposed() */, __this);
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		BooleanDisposable_set_IsDisposed_m4066294816(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_0012:
	{
		return;
	}
}
// System.Void UniRx.BooleanNotifier::.ctor(System.Boolean)
extern Il2CppClass* Subject_1_t2149039961_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m886839961_MethodInfo_var;
extern const uint32_t BooleanNotifier__ctor_m2255594648_MetadataUsageId;
extern "C"  void BooleanNotifier__ctor_m2255594648 (BooleanNotifier_t3248530688 * __this, bool ___initialValue0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BooleanNotifier__ctor_m2255594648_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t2149039961 * L_0 = (Subject_1_t2149039961 *)il2cpp_codegen_object_new(Subject_1_t2149039961_il2cpp_TypeInfo_var);
		Subject_1__ctor_m886839961(L_0, /*hidden argument*/Subject_1__ctor_m886839961_MethodInfo_var);
		__this->set_boolTrigger_0(L_0);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		bool L_1 = ___initialValue0;
		BooleanNotifier_set_Value_m509900170(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UniRx.BooleanNotifier::get_Value()
extern "C"  bool BooleanNotifier_get_Value_m3857580771 (BooleanNotifier_t3248530688 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_boolValue_1();
		return L_0;
	}
}
// System.Void UniRx.BooleanNotifier::set_Value(System.Boolean)
extern const MethodInfo* Subject_1_OnNext_m385413889_MethodInfo_var;
extern const uint32_t BooleanNotifier_set_Value_m509900170_MetadataUsageId;
extern "C"  void BooleanNotifier_set_Value_m509900170 (BooleanNotifier_t3248530688 * __this, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BooleanNotifier_set_Value_m509900170_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value0;
		__this->set_boolValue_1(L_0);
		Subject_1_t2149039961 * L_1 = __this->get_boolTrigger_0();
		bool L_2 = ___value0;
		NullCheck(L_1);
		Subject_1_OnNext_m385413889(L_1, L_2, /*hidden argument*/Subject_1_OnNext_m385413889_MethodInfo_var);
		return;
	}
}
// System.Void UniRx.BooleanNotifier::TurnOn()
extern "C"  void BooleanNotifier_TurnOn_m3132110847 (BooleanNotifier_t3248530688 * __this, const MethodInfo* method)
{
	{
		bool L_0 = BooleanNotifier_get_Value_m3857580771(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		BooleanNotifier_set_Value_m509900170(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_0012:
	{
		return;
	}
}
// System.Void UniRx.BooleanNotifier::TurnOff()
extern "C"  void BooleanNotifier_TurnOff_m2605977009 (BooleanNotifier_t3248530688 * __this, const MethodInfo* method)
{
	{
		bool L_0 = BooleanNotifier_get_Value_m3857580771(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		BooleanNotifier_set_Value_m509900170(__this, (bool)0, /*hidden argument*/NULL);
	}

IL_0012:
	{
		return;
	}
}
// System.Void UniRx.BooleanNotifier::SwitchValue()
extern "C"  void BooleanNotifier_SwitchValue_m2897531612 (BooleanNotifier_t3248530688 * __this, const MethodInfo* method)
{
	{
		bool L_0 = BooleanNotifier_get_Value_m3857580771(__this, /*hidden argument*/NULL);
		BooleanNotifier_set_Value_m509900170(__this, (bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		return;
	}
}
// System.IDisposable UniRx.BooleanNotifier::Subscribe(UniRx.IObserver`1<System.Boolean>)
extern const MethodInfo* Subject_1_Subscribe_m3869918062_MethodInfo_var;
extern const uint32_t BooleanNotifier_Subscribe_m1364813245_MetadataUsageId;
extern "C"  Il2CppObject * BooleanNotifier_Subscribe_m1364813245 (BooleanNotifier_t3248530688 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BooleanNotifier_Subscribe_m1364813245_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t2149039961 * L_0 = __this->get_boolTrigger_0();
		Il2CppObject* L_1 = ___observer0;
		NullCheck(L_0);
		Il2CppObject * L_2 = Subject_1_Subscribe_m3869918062(L_0, L_1, /*hidden argument*/Subject_1_Subscribe_m3869918062_MethodInfo_var);
		return L_2;
	}
}
// System.Void UniRx.BoolReactiveProperty::.ctor()
extern Il2CppClass* ReactiveProperty_1_t819338379_il2cpp_TypeInfo_var;
extern const MethodInfo* ReactiveProperty_1__ctor_m3742808319_MethodInfo_var;
extern const uint32_t BoolReactiveProperty__ctor_m466976127_MetadataUsageId;
extern "C"  void BoolReactiveProperty__ctor_m466976127 (BoolReactiveProperty_t3047538250 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BoolReactiveProperty__ctor_m466976127_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ReactiveProperty_1_t819338379_il2cpp_TypeInfo_var);
		ReactiveProperty_1__ctor_m3742808319(__this, /*hidden argument*/ReactiveProperty_1__ctor_m3742808319_MethodInfo_var);
		return;
	}
}
// System.Void UniRx.BoolReactiveProperty::.ctor(System.Boolean)
extern Il2CppClass* ReactiveProperty_1_t819338379_il2cpp_TypeInfo_var;
extern const MethodInfo* ReactiveProperty_1__ctor_m62942271_MethodInfo_var;
extern const uint32_t BoolReactiveProperty__ctor_m4231473334_MetadataUsageId;
extern "C"  void BoolReactiveProperty__ctor_m4231473334 (BoolReactiveProperty_t3047538250 * __this, bool ___initialValue0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BoolReactiveProperty__ctor_m4231473334_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___initialValue0;
		IL2CPP_RUNTIME_CLASS_INIT(ReactiveProperty_1_t819338379_il2cpp_TypeInfo_var);
		ReactiveProperty_1__ctor_m62942271(__this, L_0, /*hidden argument*/ReactiveProperty_1__ctor_m62942271_MethodInfo_var);
		return;
	}
}
// System.Void UniRx.BoundsReactiveProperty::.ctor()
extern Il2CppClass* ReactiveProperty_1_t4126848016_il2cpp_TypeInfo_var;
extern const MethodInfo* ReactiveProperty_1__ctor_m3808573442_MethodInfo_var;
extern const uint32_t BoundsReactiveProperty__ctor_m1171234580_MetadataUsageId;
extern "C"  void BoundsReactiveProperty__ctor_m1171234580 (BoundsReactiveProperty_t4100324373 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BoundsReactiveProperty__ctor_m1171234580_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ReactiveProperty_1_t4126848016_il2cpp_TypeInfo_var);
		ReactiveProperty_1__ctor_m3808573442(__this, /*hidden argument*/ReactiveProperty_1__ctor_m3808573442_MethodInfo_var);
		return;
	}
}
// System.Void UniRx.BoundsReactiveProperty::.ctor(UnityEngine.Bounds)
extern Il2CppClass* ReactiveProperty_1_t4126848016_il2cpp_TypeInfo_var;
extern const MethodInfo* ReactiveProperty_1__ctor_m2101661084_MethodInfo_var;
extern const uint32_t BoundsReactiveProperty__ctor_m2555393320_MetadataUsageId;
extern "C"  void BoundsReactiveProperty__ctor_m2555393320 (BoundsReactiveProperty_t4100324373 * __this, Bounds_t3518514978  ___initialValue0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BoundsReactiveProperty__ctor_m2555393320_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Bounds_t3518514978  L_0 = ___initialValue0;
		IL2CPP_RUNTIME_CLASS_INIT(ReactiveProperty_1_t4126848016_il2cpp_TypeInfo_var);
		ReactiveProperty_1__ctor_m2101661084(__this, L_0, /*hidden argument*/ReactiveProperty_1__ctor_m2101661084_MethodInfo_var);
		return;
	}
}
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Bounds> UniRx.BoundsReactiveProperty::get_EqualityComparer()
extern Il2CppClass* UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var;
extern const uint32_t BoundsReactiveProperty_get_EqualityComparer_m3459484445_MetadataUsageId;
extern "C"  Il2CppObject* BoundsReactiveProperty_get_EqualityComparer_m3459484445 (BoundsReactiveProperty_t4100324373 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BoundsReactiveProperty_get_EqualityComparer_m3459484445_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var);
		Il2CppObject* L_0 = ((UnityEqualityComparer_t3801418734_StaticFields*)UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var->static_fields)->get_Bounds_5();
		return L_0;
	}
}
// System.Void UniRx.ByteReactiveProperty::.ctor()
extern Il2CppClass* ReactiveProperty_1_t3387026859_il2cpp_TypeInfo_var;
extern const MethodInfo* ReactiveProperty_1__ctor_m1302869371_MethodInfo_var;
extern const uint32_t ByteReactiveProperty__ctor_m2510542113_MetadataUsageId;
extern "C"  void ByteReactiveProperty__ctor_m2510542113 (ByteReactiveProperty_t1619126376 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ByteReactiveProperty__ctor_m2510542113_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ReactiveProperty_1_t3387026859_il2cpp_TypeInfo_var);
		ReactiveProperty_1__ctor_m1302869371(__this, /*hidden argument*/ReactiveProperty_1__ctor_m1302869371_MethodInfo_var);
		return;
	}
}
// System.Void UniRx.ByteReactiveProperty::.ctor(System.Byte)
extern Il2CppClass* ReactiveProperty_1_t3387026859_il2cpp_TypeInfo_var;
extern const MethodInfo* ReactiveProperty_1__ctor_m1734246211_MethodInfo_var;
extern const uint32_t ByteReactiveProperty__ctor_m3329278890_MetadataUsageId;
extern "C"  void ByteReactiveProperty__ctor_m3329278890 (ByteReactiveProperty_t1619126376 * __this, uint8_t ___initialValue0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ByteReactiveProperty__ctor_m3329278890_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		uint8_t L_0 = ___initialValue0;
		IL2CPP_RUNTIME_CLASS_INIT(ReactiveProperty_1_t3387026859_il2cpp_TypeInfo_var);
		ReactiveProperty_1__ctor_m1734246211(__this, L_0, /*hidden argument*/ReactiveProperty_1__ctor_m1734246211_MethodInfo_var);
		return;
	}
}
// System.Void UniRx.CancellationToken::.ctor(UniRx.ICancelable)
extern "C"  void CancellationToken__ctor_m1453920148 (CancellationToken_t1439151560 * __this, Il2CppObject * ___source0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___source0;
		__this->set_source_0(L_0);
		return;
	}
}
// System.Void UniRx.CancellationToken::.cctor()
extern Il2CppClass* CancellationToken_t1439151560_il2cpp_TypeInfo_var;
extern const uint32_t CancellationToken__cctor_m2984451252_MetadataUsageId;
extern "C"  void CancellationToken__cctor_m2984451252 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CancellationToken__cctor_m2984451252_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		CancellationToken_t1439151560  L_0;
		memset(&L_0, 0, sizeof(L_0));
		CancellationToken__ctor_m1453920148(&L_0, (Il2CppObject *)NULL, /*hidden argument*/NULL);
		((CancellationToken_t1439151560_StaticFields*)CancellationToken_t1439151560_il2cpp_TypeInfo_var->static_fields)->set_Empty_1(L_0);
		return;
	}
}
// System.Boolean UniRx.CancellationToken::get_IsCancellationRequested()
extern Il2CppClass* ICancelable_t4109686575_il2cpp_TypeInfo_var;
extern const uint32_t CancellationToken_get_IsCancellationRequested_m1021497867_MetadataUsageId;
extern "C"  bool CancellationToken_get_IsCancellationRequested_m1021497867 (CancellationToken_t1439151560 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CancellationToken_get_IsCancellationRequested_m1021497867_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		Il2CppObject * L_0 = __this->get_source_0();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		G_B3_0 = 0;
		goto IL_001c;
	}

IL_0011:
	{
		Il2CppObject * L_1 = __this->get_source_0();
		NullCheck(L_1);
		bool L_2 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean UniRx.ICancelable::get_IsDisposed() */, ICancelable_t4109686575_il2cpp_TypeInfo_var, L_1);
		G_B3_0 = ((int32_t)(L_2));
	}

IL_001c:
	{
		return (bool)G_B3_0;
	}
}
// System.Void UniRx.CancellationToken::ThrowIfCancellationRequested()
extern Il2CppClass* OperationCanceledException_t3009926340_il2cpp_TypeInfo_var;
extern const uint32_t CancellationToken_ThrowIfCancellationRequested_m198048467_MetadataUsageId;
extern "C"  void CancellationToken_ThrowIfCancellationRequested_m198048467 (CancellationToken_t1439151560 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CancellationToken_ThrowIfCancellationRequested_m198048467_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = CancellationToken_get_IsCancellationRequested_m1021497867(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		OperationCanceledException_t3009926340 * L_1 = (OperationCanceledException_t3009926340 *)il2cpp_codegen_object_new(OperationCanceledException_t3009926340_il2cpp_TypeInfo_var);
		OperationCanceledException__ctor_m2028789062(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		return;
	}
}
// System.Void UniRx.ColorReactiveProperty::.ctor()
extern Il2CppClass* ReactiveProperty_1_t2196508798_il2cpp_TypeInfo_var;
extern const MethodInfo* ReactiveProperty_1__ctor_m1209776928_MethodInfo_var;
extern const uint32_t ColorReactiveProperty__ctor_m3247093694_MetadataUsageId;
extern "C"  void ColorReactiveProperty__ctor_m3247093694 (ColorReactiveProperty_t874298499 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ColorReactiveProperty__ctor_m3247093694_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ReactiveProperty_1_t2196508798_il2cpp_TypeInfo_var);
		ReactiveProperty_1__ctor_m1209776928(__this, /*hidden argument*/ReactiveProperty_1__ctor_m1209776928_MethodInfo_var);
		return;
	}
}
// System.Void UniRx.ColorReactiveProperty::.ctor(UnityEngine.Color)
extern Il2CppClass* ReactiveProperty_1_t2196508798_il2cpp_TypeInfo_var;
extern const MethodInfo* ReactiveProperty_1__ctor_m3143347774_MethodInfo_var;
extern const uint32_t ColorReactiveProperty__ctor_m1036944936_MetadataUsageId;
extern "C"  void ColorReactiveProperty__ctor_m1036944936 (ColorReactiveProperty_t874298499 * __this, Color_t1588175760  ___initialValue0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ColorReactiveProperty__ctor_m1036944936_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Color_t1588175760  L_0 = ___initialValue0;
		IL2CPP_RUNTIME_CLASS_INIT(ReactiveProperty_1_t2196508798_il2cpp_TypeInfo_var);
		ReactiveProperty_1__ctor_m3143347774(__this, L_0, /*hidden argument*/ReactiveProperty_1__ctor_m3143347774_MethodInfo_var);
		return;
	}
}
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Color> UniRx.ColorReactiveProperty::get_EqualityComparer()
extern Il2CppClass* UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var;
extern const uint32_t ColorReactiveProperty_get_EqualityComparer_m2916775839_MetadataUsageId;
extern "C"  Il2CppObject* ColorReactiveProperty_get_EqualityComparer_m2916775839 (ColorReactiveProperty_t874298499 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ColorReactiveProperty_get_EqualityComparer_m2916775839_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var);
		Il2CppObject* L_0 = ((UnityEqualityComparer_t3801418734_StaticFields*)UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var->static_fields)->get_Color_3();
		return L_0;
	}
}
// System.Void UniRx.CompositeDisposable::.ctor()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t2425880343_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m215590625_MethodInfo_var;
extern const uint32_t CompositeDisposable__ctor_m1237994920_MetadataUsageId;
extern "C"  void CompositeDisposable__ctor_m1237994920 (CompositeDisposable_t1894629977 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CompositeDisposable__ctor_m1237994920_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set__gate_1(L_0);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		List_1_t2425880343 * L_1 = (List_1_t2425880343 *)il2cpp_codegen_object_new(List_1_t2425880343_il2cpp_TypeInfo_var);
		List_1__ctor_m215590625(L_1, /*hidden argument*/List_1__ctor_m215590625_MethodInfo_var);
		__this->set__disposables_3(L_1);
		return;
	}
}
// System.Void UniRx.CompositeDisposable::.ctor(System.Int32)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t2425880343_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1028068018_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4227142842;
extern const uint32_t CompositeDisposable__ctor_m4031301369_MetadataUsageId;
extern "C"  void CompositeDisposable__ctor_m4031301369 (CompositeDisposable_t1894629977 * __this, int32_t ___capacity0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CompositeDisposable__ctor_m4031301369_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set__gate_1(L_0);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		int32_t L_1 = ___capacity0;
		if ((((int32_t)L_1) >= ((int32_t)0)))
		{
			goto IL_0023;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, _stringLiteral4227142842, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0023:
	{
		int32_t L_3 = ___capacity0;
		List_1_t2425880343 * L_4 = (List_1_t2425880343 *)il2cpp_codegen_object_new(List_1_t2425880343_il2cpp_TypeInfo_var);
		List_1__ctor_m1028068018(L_4, L_3, /*hidden argument*/List_1__ctor_m1028068018_MethodInfo_var);
		__this->set__disposables_3(L_4);
		return;
	}
}
// System.Void UniRx.CompositeDisposable::.ctor(System.IDisposable[])
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t2425880343_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2401491963_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral575459091;
extern const uint32_t CompositeDisposable__ctor_m654163804_MetadataUsageId;
extern "C"  void CompositeDisposable__ctor_m654163804 (CompositeDisposable_t1894629977 * __this, IDisposableU5BU5D_t165183403* ___disposables0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CompositeDisposable__ctor_m654163804_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set__gate_1(L_0);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		IDisposableU5BU5D_t165183403* L_1 = ___disposables0;
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_2 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_2, _stringLiteral575459091, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0022:
	{
		IDisposableU5BU5D_t165183403* L_3 = ___disposables0;
		List_1_t2425880343 * L_4 = (List_1_t2425880343 *)il2cpp_codegen_object_new(List_1_t2425880343_il2cpp_TypeInfo_var);
		List_1__ctor_m2401491963(L_4, (Il2CppObject*)(Il2CppObject*)L_3, /*hidden argument*/List_1__ctor_m2401491963_MethodInfo_var);
		__this->set__disposables_3(L_4);
		List_1_t2425880343 * L_5 = __this->get__disposables_3();
		NullCheck(L_5);
		int32_t L_6 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.IDisposable>::get_Count() */, L_5);
		__this->set__count_4(L_6);
		return;
	}
}
// System.Void UniRx.CompositeDisposable::.ctor(System.Collections.Generic.IEnumerable`1<System.IDisposable>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t2425880343_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2401491963_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral575459091;
extern const uint32_t CompositeDisposable__ctor_m2139080381_MetadataUsageId;
extern "C"  void CompositeDisposable__ctor_m2139080381 (CompositeDisposable_t1894629977 * __this, Il2CppObject* ___disposables0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CompositeDisposable__ctor_m2139080381_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set__gate_1(L_0);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___disposables0;
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_2 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_2, _stringLiteral575459091, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0022:
	{
		Il2CppObject* L_3 = ___disposables0;
		List_1_t2425880343 * L_4 = (List_1_t2425880343 *)il2cpp_codegen_object_new(List_1_t2425880343_il2cpp_TypeInfo_var);
		List_1__ctor_m2401491963(L_4, L_3, /*hidden argument*/List_1__ctor_m2401491963_MethodInfo_var);
		__this->set__disposables_3(L_4);
		List_1_t2425880343 * L_5 = __this->get__disposables_3();
		NullCheck(L_5);
		int32_t L_6 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.IDisposable>::get_Count() */, L_5);
		__this->set__count_4(L_6);
		return;
	}
}
// System.Collections.IEnumerator UniRx.CompositeDisposable::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * CompositeDisposable_System_Collections_IEnumerable_GetEnumerator_m204028113 (CompositeDisposable_t1894629977 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = CompositeDisposable_GetEnumerator_m1298705342(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Int32 UniRx.CompositeDisposable::get_Count()
extern "C"  int32_t CompositeDisposable_get_Count_m1817945890 (CompositeDisposable_t1894629977 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get__count_4();
		return L_0;
	}
}
// System.Void UniRx.CompositeDisposable::Add(System.IDisposable)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3242771;
extern const uint32_t CompositeDisposable_Add_m2171552957_MetadataUsageId;
extern "C"  void CompositeDisposable_Add_m2171552957 (CompositeDisposable_t1894629977 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CompositeDisposable_Add_m2171552957_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Il2CppObject * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = ___item0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, _stringLiteral3242771, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		V_0 = (bool)0;
		Il2CppObject * L_2 = __this->get__gate_1();
		V_1 = L_2;
		Il2CppObject * L_3 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0020:
	try
	{ // begin try (depth: 1)
		{
			bool L_4 = __this->get__disposed_2();
			V_0 = L_4;
			bool L_5 = __this->get__disposed_2();
			if (L_5)
			{
				goto IL_004c;
			}
		}

IL_0032:
		{
			List_1_t2425880343 * L_6 = __this->get__disposables_3();
			Il2CppObject * L_7 = ___item0;
			NullCheck(L_6);
			VirtActionInvoker1< Il2CppObject * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.IDisposable>::Add(!0) */, L_6, L_7);
			int32_t L_8 = __this->get__count_4();
			__this->set__count_4(((int32_t)((int32_t)L_8+(int32_t)1)));
		}

IL_004c:
		{
			IL2CPP_LEAVE(0x58, FINALLY_0051);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0051;
	}

FINALLY_0051:
	{ // begin finally (depth: 1)
		Il2CppObject * L_9 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(81)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(81)
	{
		IL2CPP_JUMP_TBL(0x58, IL_0058)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0058:
	{
		bool L_10 = V_0;
		if (!L_10)
		{
			goto IL_0064;
		}
	}
	{
		Il2CppObject * L_11 = ___item0;
		NullCheck(L_11);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_11);
	}

IL_0064:
	{
		return;
	}
}
// System.Boolean UniRx.CompositeDisposable::Remove(System.IDisposable)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t2425880343_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t511663335_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Capacity_m782090960_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m1028068018_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1613394326_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m443741914_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1919929410_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3242771;
extern const uint32_t CompositeDisposable_Remove_m495331162_MetadataUsageId;
extern "C"  bool CompositeDisposable_Remove_m495331162 (CompositeDisposable_t1894629977 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CompositeDisposable_Remove_m495331162_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Il2CppObject * V_1 = NULL;
	int32_t V_2 = 0;
	List_1_t2425880343 * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	Enumerator_t511663335  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = ___item0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, _stringLiteral3242771, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		V_0 = (bool)0;
		Il2CppObject * L_2 = __this->get__gate_1();
		V_1 = L_2;
		Il2CppObject * L_3 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0020:
	try
	{ // begin try (depth: 1)
		{
			bool L_4 = __this->get__disposed_2();
			if (L_4)
			{
				goto IL_00ed;
			}
		}

IL_002b:
		{
			List_1_t2425880343 * L_5 = __this->get__disposables_3();
			Il2CppObject * L_6 = ___item0;
			NullCheck(L_5);
			int32_t L_7 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(28 /* System.Int32 System.Collections.Generic.List`1<System.IDisposable>::IndexOf(!0) */, L_5, L_6);
			V_2 = L_7;
			int32_t L_8 = V_2;
			if ((((int32_t)L_8) < ((int32_t)0)))
			{
				goto IL_00ed;
			}
		}

IL_003f:
		{
			V_0 = (bool)1;
			List_1_t2425880343 * L_9 = __this->get__disposables_3();
			int32_t L_10 = V_2;
			NullCheck(L_9);
			VirtActionInvoker2< int32_t, Il2CppObject * >::Invoke(32 /* System.Void System.Collections.Generic.List`1<System.IDisposable>::set_Item(System.Int32,!0) */, L_9, L_10, (Il2CppObject *)NULL);
			int32_t L_11 = __this->get__count_4();
			__this->set__count_4(((int32_t)((int32_t)L_11-(int32_t)1)));
			List_1_t2425880343 * L_12 = __this->get__disposables_3();
			NullCheck(L_12);
			int32_t L_13 = List_1_get_Capacity_m782090960(L_12, /*hidden argument*/List_1_get_Capacity_m782090960_MethodInfo_var);
			if ((((int32_t)L_13) <= ((int32_t)((int32_t)64))))
			{
				goto IL_00ed;
			}
		}

IL_006e:
		{
			int32_t L_14 = __this->get__count_4();
			List_1_t2425880343 * L_15 = __this->get__disposables_3();
			NullCheck(L_15);
			int32_t L_16 = List_1_get_Capacity_m782090960(L_15, /*hidden argument*/List_1_get_Capacity_m782090960_MethodInfo_var);
			if ((((int32_t)L_14) >= ((int32_t)((int32_t)((int32_t)L_16/(int32_t)2)))))
			{
				goto IL_00ed;
			}
		}

IL_0086:
		{
			List_1_t2425880343 * L_17 = __this->get__disposables_3();
			V_3 = L_17;
			List_1_t2425880343 * L_18 = __this->get__disposables_3();
			NullCheck(L_18);
			int32_t L_19 = List_1_get_Capacity_m782090960(L_18, /*hidden argument*/List_1_get_Capacity_m782090960_MethodInfo_var);
			List_1_t2425880343 * L_20 = (List_1_t2425880343 *)il2cpp_codegen_object_new(List_1_t2425880343_il2cpp_TypeInfo_var);
			List_1__ctor_m1028068018(L_20, ((int32_t)((int32_t)L_19/(int32_t)2)), /*hidden argument*/List_1__ctor_m1028068018_MethodInfo_var);
			__this->set__disposables_3(L_20);
			List_1_t2425880343 * L_21 = V_3;
			NullCheck(L_21);
			Enumerator_t511663335  L_22 = List_1_GetEnumerator_m1613394326(L_21, /*hidden argument*/List_1_GetEnumerator_m1613394326_MethodInfo_var);
			V_5 = L_22;
		}

IL_00ad:
		try
		{ // begin try (depth: 2)
			{
				goto IL_00cf;
			}

IL_00b2:
			{
				Il2CppObject * L_23 = Enumerator_get_Current_m443741914((&V_5), /*hidden argument*/Enumerator_get_Current_m443741914_MethodInfo_var);
				V_4 = L_23;
				Il2CppObject * L_24 = V_4;
				if (!L_24)
				{
					goto IL_00cf;
				}
			}

IL_00c2:
			{
				List_1_t2425880343 * L_25 = __this->get__disposables_3();
				Il2CppObject * L_26 = V_4;
				NullCheck(L_25);
				VirtActionInvoker1< Il2CppObject * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.IDisposable>::Add(!0) */, L_25, L_26);
			}

IL_00cf:
			{
				bool L_27 = Enumerator_MoveNext_m1919929410((&V_5), /*hidden argument*/Enumerator_MoveNext_m1919929410_MethodInfo_var);
				if (L_27)
				{
					goto IL_00b2;
				}
			}

IL_00db:
			{
				IL2CPP_LEAVE(0xED, FINALLY_00e0);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_00e0;
		}

FINALLY_00e0:
		{ // begin finally (depth: 2)
			Enumerator_t511663335  L_28 = V_5;
			Enumerator_t511663335  L_29 = L_28;
			Il2CppObject * L_30 = Box(Enumerator_t511663335_il2cpp_TypeInfo_var, &L_29);
			NullCheck((Il2CppObject *)L_30);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_30);
			IL2CPP_END_FINALLY(224)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(224)
		{
			IL2CPP_JUMP_TBL(0xED, IL_00ed)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_00ed:
		{
			IL2CPP_LEAVE(0xF9, FINALLY_00f2);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00f2;
	}

FINALLY_00f2:
	{ // begin finally (depth: 1)
		Il2CppObject * L_31 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(242)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(242)
	{
		IL2CPP_JUMP_TBL(0xF9, IL_00f9)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00f9:
	{
		bool L_32 = V_0;
		if (!L_32)
		{
			goto IL_0105;
		}
	}
	{
		Il2CppObject * L_33 = ___item0;
		NullCheck(L_33);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_33);
	}

IL_0105:
	{
		bool L_34 = V_0;
		return L_34;
	}
}
// System.Void UniRx.CompositeDisposable::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_ToArray_m2466860817_MethodInfo_var;
extern const uint32_t CompositeDisposable_Dispose_m4200427493_MetadataUsageId;
extern "C"  void CompositeDisposable_Dispose_m4200427493 (CompositeDisposable_t1894629977 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CompositeDisposable_Dispose_m4200427493_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	IDisposableU5BU5D_t165183403* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	IDisposableU5BU5D_t165183403* V_3 = NULL;
	int32_t V_4 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (IDisposableU5BU5D_t165183403*)NULL;
		Il2CppObject * L_0 = __this->get__gate_1();
		V_1 = L_0;
		Il2CppObject * L_1 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000f:
	try
	{ // begin try (depth: 1)
		{
			bool L_2 = __this->get__disposed_2();
			if (L_2)
			{
				goto IL_003f;
			}
		}

IL_001a:
		{
			__this->set__disposed_2((bool)1);
			List_1_t2425880343 * L_3 = __this->get__disposables_3();
			NullCheck(L_3);
			IDisposableU5BU5D_t165183403* L_4 = List_1_ToArray_m2466860817(L_3, /*hidden argument*/List_1_ToArray_m2466860817_MethodInfo_var);
			V_0 = L_4;
			List_1_t2425880343 * L_5 = __this->get__disposables_3();
			NullCheck(L_5);
			VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<System.IDisposable>::Clear() */, L_5);
			__this->set__count_4(0);
		}

IL_003f:
		{
			IL2CPP_LEAVE(0x4B, FINALLY_0044);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0044;
	}

FINALLY_0044:
	{ // begin finally (depth: 1)
		Il2CppObject * L_6 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(68)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(68)
	{
		IL2CPP_JUMP_TBL(0x4B, IL_004b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_004b:
	{
		IDisposableU5BU5D_t165183403* L_7 = V_0;
		if (!L_7)
		{
			goto IL_007c;
		}
	}
	{
		IDisposableU5BU5D_t165183403* L_8 = V_0;
		V_3 = L_8;
		V_4 = 0;
		goto IL_0072;
	}

IL_005b:
	{
		IDisposableU5BU5D_t165183403* L_9 = V_3;
		int32_t L_10 = V_4;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		V_2 = ((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11)));
		Il2CppObject * L_12 = V_2;
		if (!L_12)
		{
			goto IL_006c;
		}
	}
	{
		Il2CppObject * L_13 = V_2;
		NullCheck(L_13);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_13);
	}

IL_006c:
	{
		int32_t L_14 = V_4;
		V_4 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0072:
	{
		int32_t L_15 = V_4;
		IDisposableU5BU5D_t165183403* L_16 = V_3;
		NullCheck(L_16);
		if ((((int32_t)L_15) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_16)->max_length)))))))
		{
			goto IL_005b;
		}
	}

IL_007c:
	{
		return;
	}
}
// System.Void UniRx.CompositeDisposable::Clear()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_ToArray_m2466860817_MethodInfo_var;
extern const uint32_t CompositeDisposable_Clear_m2939095507_MetadataUsageId;
extern "C"  void CompositeDisposable_Clear_m2939095507 (CompositeDisposable_t1894629977 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CompositeDisposable_Clear_m2939095507_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	IDisposableU5BU5D_t165183403* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	IDisposableU5BU5D_t165183403* V_3 = NULL;
	int32_t V_4 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (IDisposableU5BU5D_t165183403*)NULL;
		Il2CppObject * L_0 = __this->get__gate_1();
		V_1 = L_0;
		Il2CppObject * L_1 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000f:
	try
	{ // begin try (depth: 1)
		List_1_t2425880343 * L_2 = __this->get__disposables_3();
		NullCheck(L_2);
		IDisposableU5BU5D_t165183403* L_3 = List_1_ToArray_m2466860817(L_2, /*hidden argument*/List_1_ToArray_m2466860817_MethodInfo_var);
		V_0 = L_3;
		List_1_t2425880343 * L_4 = __this->get__disposables_3();
		NullCheck(L_4);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<System.IDisposable>::Clear() */, L_4);
		__this->set__count_4(0);
		IL2CPP_LEAVE(0x39, FINALLY_0032);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0032;
	}

FINALLY_0032:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(50)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(50)
	{
		IL2CPP_JUMP_TBL(0x39, IL_0039)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0039:
	{
		IDisposableU5BU5D_t165183403* L_6 = V_0;
		V_3 = L_6;
		V_4 = 0;
		goto IL_005a;
	}

IL_0043:
	{
		IDisposableU5BU5D_t165183403* L_7 = V_3;
		int32_t L_8 = V_4;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		V_2 = ((L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9)));
		Il2CppObject * L_10 = V_2;
		if (!L_10)
		{
			goto IL_0054;
		}
	}
	{
		Il2CppObject * L_11 = V_2;
		NullCheck(L_11);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_11);
	}

IL_0054:
	{
		int32_t L_12 = V_4;
		V_4 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_005a:
	{
		int32_t L_13 = V_4;
		IDisposableU5BU5D_t165183403* L_14 = V_3;
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0043;
		}
	}
	{
		return;
	}
}
// System.Boolean UniRx.CompositeDisposable::Contains(System.IDisposable)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3242771;
extern const uint32_t CompositeDisposable_Contains_m2190427573_MetadataUsageId;
extern "C"  bool CompositeDisposable_Contains_m2190427573 (CompositeDisposable_t1894629977 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CompositeDisposable_Contains_m2190427573_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = ___item0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, _stringLiteral3242771, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Il2CppObject * L_2 = __this->get__gate_1();
		V_0 = L_2;
		Il2CppObject * L_3 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_001e:
	try
	{ // begin try (depth: 1)
		{
			List_1_t2425880343 * L_4 = __this->get__disposables_3();
			Il2CppObject * L_5 = ___item0;
			NullCheck(L_4);
			bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(24 /* System.Boolean System.Collections.Generic.List`1<System.IDisposable>::Contains(!0) */, L_4, L_5);
			V_1 = L_6;
			IL2CPP_LEAVE(0x3C, FINALLY_0035);
		}

IL_0030:
		{
			; // IL_0030: leave IL_003c
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0035;
	}

FINALLY_0035:
	{ // begin finally (depth: 1)
		Il2CppObject * L_7 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(53)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(53)
	{
		IL2CPP_JUMP_TBL(0x3C, IL_003c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_003c:
	{
		bool L_8 = V_1;
		return L_8;
	}
}
// System.Void UniRx.CompositeDisposable::CopyTo(System.IDisposable[],System.Int32)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t2425880343_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t511663335_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m215590625_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1613394326_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m443741914_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1919929410_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m2466860817_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral93090393;
extern Il2CppCodeGenString* _stringLiteral3061995001;
extern const uint32_t CompositeDisposable_CopyTo_m3492652151_MetadataUsageId;
extern "C"  void CompositeDisposable_CopyTo_m3492652151 (CompositeDisposable_t1894629977 * __this, IDisposableU5BU5D_t165183403* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CompositeDisposable_CopyTo_m3492652151_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	List_1_t2425880343 * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Enumerator_t511663335  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IDisposableU5BU5D_t165183403* L_0 = ___array0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, _stringLiteral93090393, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___arrayIndex1;
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_3 = ___arrayIndex1;
		IDisposableU5BU5D_t165183403* L_4 = ___array0;
		NullCheck(L_4);
		if ((((int32_t)L_3) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length)))))))
		{
			goto IL_002c;
		}
	}

IL_0021:
	{
		ArgumentOutOfRangeException_t3479058991 * L_5 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_5, _stringLiteral3061995001, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_002c:
	{
		Il2CppObject * L_6 = __this->get__gate_1();
		V_0 = L_6;
		Il2CppObject * L_7 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
	}

IL_0039:
	try
	{ // begin try (depth: 1)
		{
			List_1_t2425880343 * L_8 = (List_1_t2425880343 *)il2cpp_codegen_object_new(List_1_t2425880343_il2cpp_TypeInfo_var);
			List_1__ctor_m215590625(L_8, /*hidden argument*/List_1__ctor_m215590625_MethodInfo_var);
			V_1 = L_8;
			List_1_t2425880343 * L_9 = V_1;
			NullCheck(L_9);
			Enumerator_t511663335  L_10 = List_1_GetEnumerator_m1613394326(L_9, /*hidden argument*/List_1_GetEnumerator_m1613394326_MethodInfo_var);
			V_3 = L_10;
		}

IL_0046:
		try
		{ // begin try (depth: 2)
			{
				goto IL_0060;
			}

IL_004b:
			{
				Il2CppObject * L_11 = Enumerator_get_Current_m443741914((&V_3), /*hidden argument*/Enumerator_get_Current_m443741914_MethodInfo_var);
				V_2 = L_11;
				Il2CppObject * L_12 = V_2;
				if (!L_12)
				{
					goto IL_0060;
				}
			}

IL_0059:
			{
				List_1_t2425880343 * L_13 = V_1;
				Il2CppObject * L_14 = V_2;
				NullCheck(L_13);
				VirtActionInvoker1< Il2CppObject * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.IDisposable>::Add(!0) */, L_13, L_14);
			}

IL_0060:
			{
				bool L_15 = Enumerator_MoveNext_m1919929410((&V_3), /*hidden argument*/Enumerator_MoveNext_m1919929410_MethodInfo_var);
				if (L_15)
				{
					goto IL_004b;
				}
			}

IL_006c:
			{
				IL2CPP_LEAVE(0x7D, FINALLY_0071);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0071;
		}

FINALLY_0071:
		{ // begin finally (depth: 2)
			Enumerator_t511663335  L_16 = V_3;
			Enumerator_t511663335  L_17 = L_16;
			Il2CppObject * L_18 = Box(Enumerator_t511663335_il2cpp_TypeInfo_var, &L_17);
			NullCheck((Il2CppObject *)L_18);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_18);
			IL2CPP_END_FINALLY(113)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(113)
		{
			IL2CPP_JUMP_TBL(0x7D, IL_007d)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_007d:
		{
			List_1_t2425880343 * L_19 = V_1;
			NullCheck(L_19);
			IDisposableU5BU5D_t165183403* L_20 = List_1_ToArray_m2466860817(L_19, /*hidden argument*/List_1_ToArray_m2466860817_MethodInfo_var);
			IDisposableU5BU5D_t165183403* L_21 = ___array0;
			int32_t L_22 = ___arrayIndex1;
			IDisposableU5BU5D_t165183403* L_23 = ___array0;
			NullCheck(L_23);
			int32_t L_24 = ___arrayIndex1;
			Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_20, 0, (Il2CppArray *)(Il2CppArray *)L_21, L_22, ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_23)->max_length))))-(int32_t)L_24)), /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x9C, FINALLY_0095);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0095;
	}

FINALLY_0095:
	{ // begin finally (depth: 1)
		Il2CppObject * L_25 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(149)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(149)
	{
		IL2CPP_JUMP_TBL(0x9C, IL_009c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_009c:
	{
		return;
	}
}
// System.Boolean UniRx.CompositeDisposable::get_IsReadOnly()
extern "C"  bool CompositeDisposable_get_IsReadOnly_m1220575925 (CompositeDisposable_t1894629977 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerator`1<System.IDisposable> UniRx.CompositeDisposable::GetEnumerator()
extern Il2CppClass* List_1_t2425880343_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t511663335_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m215590625_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1613394326_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m443741914_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1919929410_MethodInfo_var;
extern const uint32_t CompositeDisposable_GetEnumerator_m1298705342_MetadataUsageId;
extern "C"  Il2CppObject* CompositeDisposable_GetEnumerator_m1298705342 (CompositeDisposable_t1894629977 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CompositeDisposable_GetEnumerator_m1298705342_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t2425880343 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Enumerator_t511663335  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t2425880343 * L_0 = (List_1_t2425880343 *)il2cpp_codegen_object_new(List_1_t2425880343_il2cpp_TypeInfo_var);
		List_1__ctor_m215590625(L_0, /*hidden argument*/List_1__ctor_m215590625_MethodInfo_var);
		V_0 = L_0;
		Il2CppObject * L_1 = __this->get__gate_1();
		V_1 = L_1;
		Il2CppObject * L_2 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0013:
	try
	{ // begin try (depth: 1)
		{
			List_1_t2425880343 * L_3 = __this->get__disposables_3();
			NullCheck(L_3);
			Enumerator_t511663335  L_4 = List_1_GetEnumerator_m1613394326(L_3, /*hidden argument*/List_1_GetEnumerator_m1613394326_MethodInfo_var);
			V_3 = L_4;
		}

IL_001f:
		try
		{ // begin try (depth: 2)
			{
				goto IL_0039;
			}

IL_0024:
			{
				Il2CppObject * L_5 = Enumerator_get_Current_m443741914((&V_3), /*hidden argument*/Enumerator_get_Current_m443741914_MethodInfo_var);
				V_2 = L_5;
				Il2CppObject * L_6 = V_2;
				if (!L_6)
				{
					goto IL_0039;
				}
			}

IL_0032:
			{
				List_1_t2425880343 * L_7 = V_0;
				Il2CppObject * L_8 = V_2;
				NullCheck(L_7);
				VirtActionInvoker1< Il2CppObject * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.IDisposable>::Add(!0) */, L_7, L_8);
			}

IL_0039:
			{
				bool L_9 = Enumerator_MoveNext_m1919929410((&V_3), /*hidden argument*/Enumerator_MoveNext_m1919929410_MethodInfo_var);
				if (L_9)
				{
					goto IL_0024;
				}
			}

IL_0045:
			{
				IL2CPP_LEAVE(0x56, FINALLY_004a);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_004a;
		}

FINALLY_004a:
		{ // begin finally (depth: 2)
			Enumerator_t511663335  L_10 = V_3;
			Enumerator_t511663335  L_11 = L_10;
			Il2CppObject * L_12 = Box(Enumerator_t511663335_il2cpp_TypeInfo_var, &L_11);
			NullCheck((Il2CppObject *)L_12);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_12);
			IL2CPP_END_FINALLY(74)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(74)
		{
			IL2CPP_JUMP_TBL(0x56, IL_0056)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0056:
		{
			IL2CPP_LEAVE(0x62, FINALLY_005b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_005b;
	}

FINALLY_005b:
	{ // begin finally (depth: 1)
		Il2CppObject * L_13 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(91)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(91)
	{
		IL2CPP_JUMP_TBL(0x62, IL_0062)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0062:
	{
		List_1_t2425880343 * L_14 = V_0;
		NullCheck(L_14);
		Enumerator_t511663335  L_15 = List_1_GetEnumerator_m1613394326(L_14, /*hidden argument*/List_1_GetEnumerator_m1613394326_MethodInfo_var);
		Enumerator_t511663335  L_16 = L_15;
		Il2CppObject * L_17 = Box(Enumerator_t511663335_il2cpp_TypeInfo_var, &L_16);
		return (Il2CppObject*)L_17;
	}
}
// System.Boolean UniRx.CompositeDisposable::get_IsDisposed()
extern "C"  bool CompositeDisposable_get_IsDisposed_m1028393400 (CompositeDisposable_t1894629977 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get__disposed_2();
		return L_0;
	}
}
// System.Void UniRx.CountNotifier::.ctor(System.Int32)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* Subject_1_t1414295045_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m735375039_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral107876;
extern const uint32_t CountNotifier__ctor_m2875335723_MetadataUsageId;
extern "C"  void CountNotifier__ctor_m2875335723 (CountNotifier_t4027919079 * __this, int32_t ___max0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CountNotifier__ctor_m2875335723_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_lockObject_0(L_0);
		Subject_1_t1414295045 * L_1 = (Subject_1_t1414295045 *)il2cpp_codegen_object_new(Subject_1_t1414295045_il2cpp_TypeInfo_var);
		Subject_1__ctor_m735375039(L_1, /*hidden argument*/Subject_1__ctor_m735375039_MethodInfo_var);
		__this->set_statusChanged_1(L_1);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		int32_t L_2 = ___max0;
		if ((((int32_t)L_2) > ((int32_t)0)))
		{
			goto IL_002e;
		}
	}
	{
		ArgumentException_t124305799 * L_3 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_3, _stringLiteral107876, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002e:
	{
		int32_t L_4 = ___max0;
		__this->set_max_2(L_4);
		return;
	}
}
// System.Int32 UniRx.CountNotifier::get_Max()
extern "C"  int32_t CountNotifier_get_Max_m681577193 (CountNotifier_t4027919079 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_max_2();
		return L_0;
	}
}
// System.Int32 UniRx.CountNotifier::get_Count()
extern "C"  int32_t CountNotifier_get_Count_m2275747540 (CountNotifier_t4027919079 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CCountU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void UniRx.CountNotifier::set_Count(System.Int32)
extern "C"  void CountNotifier_set_Count_m2753574971 (CountNotifier_t4027919079 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CCountU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.IDisposable UniRx.CountNotifier::Increment(System.Int32)
extern Il2CppClass* U3CIncrementU3Ec__AnonStorey57_t1898758910_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1_OnNext_m856511911_MethodInfo_var;
extern const MethodInfo* U3CIncrementU3Ec__AnonStorey57_U3CU3Em__6B_m1950567380_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1787208416;
extern const uint32_t CountNotifier_Increment_m2323311795_MetadataUsageId;
extern "C"  Il2CppObject * CountNotifier_Increment_m2323311795 (CountNotifier_t4027919079 * __this, int32_t ___incrementCount0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CountNotifier_Increment_m2323311795_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	U3CIncrementU3Ec__AnonStorey57_t1898758910 * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		U3CIncrementU3Ec__AnonStorey57_t1898758910 * L_0 = (U3CIncrementU3Ec__AnonStorey57_t1898758910 *)il2cpp_codegen_object_new(U3CIncrementU3Ec__AnonStorey57_t1898758910_il2cpp_TypeInfo_var);
		U3CIncrementU3Ec__AnonStorey57__ctor_m3673366783(L_0, /*hidden argument*/NULL);
		V_1 = L_0;
		U3CIncrementU3Ec__AnonStorey57_t1898758910 * L_1 = V_1;
		int32_t L_2 = ___incrementCount0;
		NullCheck(L_1);
		L_1->set_incrementCount_0(L_2);
		U3CIncrementU3Ec__AnonStorey57_t1898758910 * L_3 = V_1;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_1(__this);
		U3CIncrementU3Ec__AnonStorey57_t1898758910 * L_4 = V_1;
		NullCheck(L_4);
		int32_t L_5 = L_4->get_incrementCount_0();
		if ((((int32_t)L_5) >= ((int32_t)0)))
		{
			goto IL_002b;
		}
	}
	{
		ArgumentException_t124305799 * L_6 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_6, _stringLiteral1787208416, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_002b:
	{
		Il2CppObject * L_7 = __this->get_lockObject_0();
		V_0 = L_7;
		Il2CppObject * L_8 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
	}

IL_0038:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_9 = CountNotifier_get_Count_m2275747540(__this, /*hidden argument*/NULL);
			int32_t L_10 = CountNotifier_get_Max_m681577193(__this, /*hidden argument*/NULL);
			if ((!(((uint32_t)L_9) == ((uint32_t)L_10))))
			{
				goto IL_0054;
			}
		}

IL_0049:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
			Il2CppObject * L_11 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
			V_2 = L_11;
			IL2CPP_LEAVE(0xDC, FINALLY_00d5);
		}

IL_0054:
		{
			U3CIncrementU3Ec__AnonStorey57_t1898758910 * L_12 = V_1;
			NullCheck(L_12);
			int32_t L_13 = L_12->get_incrementCount_0();
			int32_t L_14 = CountNotifier_get_Count_m2275747540(__this, /*hidden argument*/NULL);
			int32_t L_15 = CountNotifier_get_Max_m681577193(__this, /*hidden argument*/NULL);
			if ((((int32_t)((int32_t)((int32_t)L_13+(int32_t)L_14))) <= ((int32_t)L_15)))
			{
				goto IL_007d;
			}
		}

IL_006c:
		{
			int32_t L_16 = CountNotifier_get_Max_m681577193(__this, /*hidden argument*/NULL);
			CountNotifier_set_Count_m2753574971(__this, L_16, /*hidden argument*/NULL);
			goto IL_0090;
		}

IL_007d:
		{
			int32_t L_17 = CountNotifier_get_Count_m2275747540(__this, /*hidden argument*/NULL);
			U3CIncrementU3Ec__AnonStorey57_t1898758910 * L_18 = V_1;
			NullCheck(L_18);
			int32_t L_19 = L_18->get_incrementCount_0();
			CountNotifier_set_Count_m2753574971(__this, ((int32_t)((int32_t)L_17+(int32_t)L_19)), /*hidden argument*/NULL);
		}

IL_0090:
		{
			Subject_1_t1414295045 * L_20 = __this->get_statusChanged_1();
			NullCheck(L_20);
			Subject_1_OnNext_m856511911(L_20, 0, /*hidden argument*/Subject_1_OnNext_m856511911_MethodInfo_var);
			int32_t L_21 = CountNotifier_get_Count_m2275747540(__this, /*hidden argument*/NULL);
			int32_t L_22 = CountNotifier_get_Max_m681577193(__this, /*hidden argument*/NULL);
			if ((!(((uint32_t)L_21) == ((uint32_t)L_22))))
			{
				goto IL_00b9;
			}
		}

IL_00ad:
		{
			Subject_1_t1414295045 * L_23 = __this->get_statusChanged_1();
			NullCheck(L_23);
			Subject_1_OnNext_m856511911(L_23, 3, /*hidden argument*/Subject_1_OnNext_m856511911_MethodInfo_var);
		}

IL_00b9:
		{
			U3CIncrementU3Ec__AnonStorey57_t1898758910 * L_24 = V_1;
			IntPtr_t L_25;
			L_25.set_m_value_0((void*)(void*)U3CIncrementU3Ec__AnonStorey57_U3CU3Em__6B_m1950567380_MethodInfo_var);
			Action_t437523947 * L_26 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
			Action__ctor_m2957240604(L_26, L_24, L_25, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
			Il2CppObject * L_27 = Disposable_Create_m2910846009(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
			V_2 = L_27;
			IL2CPP_LEAVE(0xDC, FINALLY_00d5);
		}

IL_00d0:
		{
			; // IL_00d0: leave IL_00dc
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00d5;
	}

FINALLY_00d5:
	{ // begin finally (depth: 1)
		Il2CppObject * L_28 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(213)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(213)
	{
		IL2CPP_JUMP_TBL(0xDC, IL_00dc)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00dc:
	{
		Il2CppObject * L_29 = V_2;
		return L_29;
	}
}
// System.Void UniRx.CountNotifier::Decrement(System.Int32)
extern Il2CppClass* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1_OnNext_m856511911_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2660661308;
extern const uint32_t CountNotifier_Decrement_m482281500_MetadataUsageId;
extern "C"  void CountNotifier_Decrement_m482281500 (CountNotifier_t4027919079 * __this, int32_t ___decrementCount0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CountNotifier_Decrement_m482281500_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = ___decrementCount0;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentException_t124305799 * L_1 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_1, _stringLiteral2660661308, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Il2CppObject * L_2 = __this->get_lockObject_0();
		V_0 = L_2;
		Il2CppObject * L_3 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_001f:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_4 = CountNotifier_get_Count_m2275747540(__this, /*hidden argument*/NULL);
			if (L_4)
			{
				goto IL_002f;
			}
		}

IL_002a:
		{
			IL2CPP_LEAVE(0x86, FINALLY_007f);
		}

IL_002f:
		{
			int32_t L_5 = CountNotifier_get_Count_m2275747540(__this, /*hidden argument*/NULL);
			int32_t L_6 = ___decrementCount0;
			if ((((int32_t)((int32_t)((int32_t)L_5-(int32_t)L_6))) >= ((int32_t)0)))
			{
				goto IL_0049;
			}
		}

IL_003d:
		{
			CountNotifier_set_Count_m2753574971(__this, 0, /*hidden argument*/NULL);
			goto IL_0057;
		}

IL_0049:
		{
			int32_t L_7 = CountNotifier_get_Count_m2275747540(__this, /*hidden argument*/NULL);
			int32_t L_8 = ___decrementCount0;
			CountNotifier_set_Count_m2753574971(__this, ((int32_t)((int32_t)L_7-(int32_t)L_8)), /*hidden argument*/NULL);
		}

IL_0057:
		{
			Subject_1_t1414295045 * L_9 = __this->get_statusChanged_1();
			NullCheck(L_9);
			Subject_1_OnNext_m856511911(L_9, 1, /*hidden argument*/Subject_1_OnNext_m856511911_MethodInfo_var);
			int32_t L_10 = CountNotifier_get_Count_m2275747540(__this, /*hidden argument*/NULL);
			if (L_10)
			{
				goto IL_007a;
			}
		}

IL_006e:
		{
			Subject_1_t1414295045 * L_11 = __this->get_statusChanged_1();
			NullCheck(L_11);
			Subject_1_OnNext_m856511911(L_11, 2, /*hidden argument*/Subject_1_OnNext_m856511911_MethodInfo_var);
		}

IL_007a:
		{
			IL2CPP_LEAVE(0x86, FINALLY_007f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_007f;
	}

FINALLY_007f:
	{ // begin finally (depth: 1)
		Il2CppObject * L_12 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(127)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(127)
	{
		IL2CPP_JUMP_TBL(0x86, IL_0086)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0086:
	{
		return;
	}
}
// System.IDisposable UniRx.CountNotifier::Subscribe(UniRx.IObserver`1<UniRx.CountChangedStatus>)
extern const MethodInfo* Subject_1_Subscribe_m598603732_MethodInfo_var;
extern const uint32_t CountNotifier_Subscribe_m1347680650_MetadataUsageId;
extern "C"  Il2CppObject * CountNotifier_Subscribe_m1347680650 (CountNotifier_t4027919079 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CountNotifier_Subscribe_m1347680650_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t1414295045 * L_0 = __this->get_statusChanged_1();
		Il2CppObject* L_1 = ___observer0;
		NullCheck(L_0);
		Il2CppObject * L_2 = Subject_1_Subscribe_m598603732(L_0, L_1, /*hidden argument*/Subject_1_Subscribe_m598603732_MethodInfo_var);
		return L_2;
	}
}
// System.Void UniRx.CountNotifier/<Increment>c__AnonStorey57::.ctor()
extern "C"  void U3CIncrementU3Ec__AnonStorey57__ctor_m3673366783 (U3CIncrementU3Ec__AnonStorey57_t1898758910 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.CountNotifier/<Increment>c__AnonStorey57::<>m__6B()
extern "C"  void U3CIncrementU3Ec__AnonStorey57_U3CU3Em__6B_m1950567380 (U3CIncrementU3Ec__AnonStorey57_t1898758910 * __this, const MethodInfo* method)
{
	{
		CountNotifier_t4027919079 * L_0 = __this->get_U3CU3Ef__this_1();
		int32_t L_1 = __this->get_incrementCount_0();
		NullCheck(L_0);
		CountNotifier_Decrement_m482281500(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Diagnostics.LogEntry::.ctor(System.String,UnityEngine.LogType,System.DateTime,System.String,UnityEngine.Object,System.Exception,System.String,System.Object)
extern "C"  void LogEntry__ctor_m3810061886 (LogEntry_t1891155722 * __this, String_t* ___loggerName0, int32_t ___logType1, DateTime_t339033936  ___timestamp2, String_t* ___message3, Object_t3878351788 * ___context4, Exception_t1967233988 * ___exception5, String_t* ___stackTrace6, Il2CppObject * ___state7, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___loggerName0;
		LogEntry_set_LoggerName_m1213850445(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___logType1;
		LogEntry_set_LogType_m3919054117(__this, L_1, /*hidden argument*/NULL);
		DateTime_t339033936  L_2 = ___timestamp2;
		LogEntry_set_Timestamp_m3000247386(__this, L_2, /*hidden argument*/NULL);
		String_t* L_3 = ___message3;
		LogEntry_set_Message_m3538884819(__this, L_3, /*hidden argument*/NULL);
		Object_t3878351788 * L_4 = ___context4;
		LogEntry_set_Context_m12151361(__this, L_4, /*hidden argument*/NULL);
		Exception_t1967233988 * L_5 = ___exception5;
		LogEntry_set_Exception_m2294780359(__this, L_5, /*hidden argument*/NULL);
		String_t* L_6 = ___stackTrace6;
		LogEntry_set_StackTrace_m3865377707(__this, L_6, /*hidden argument*/NULL);
		Il2CppObject * L_7 = ___state7;
		LogEntry_set_State_m683787227(__this, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.String UniRx.Diagnostics.LogEntry::get_LoggerName()
extern "C"  String_t* LogEntry_get_LoggerName_m969967108 (LogEntry_t1891155722 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CLoggerNameU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void UniRx.Diagnostics.LogEntry::set_LoggerName(System.String)
extern "C"  void LogEntry_set_LoggerName_m1213850445 (LogEntry_t1891155722 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CLoggerNameU3Ek__BackingField_0(L_0);
		return;
	}
}
// UnityEngine.LogType UniRx.Diagnostics.LogEntry::get_LogType()
extern "C"  int32_t LogEntry_get_LogType_m344512238 (LogEntry_t1891155722 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CLogTypeU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void UniRx.Diagnostics.LogEntry::set_LogType(UnityEngine.LogType)
extern "C"  void LogEntry_set_LogType_m3919054117 (LogEntry_t1891155722 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CLogTypeU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String UniRx.Diagnostics.LogEntry::get_Message()
extern "C"  String_t* LogEntry_get_Message_m3138159808 (LogEntry_t1891155722 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CMessageU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void UniRx.Diagnostics.LogEntry::set_Message(System.String)
extern "C"  void LogEntry_set_Message_m3538884819 (LogEntry_t1891155722 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CMessageU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.DateTime UniRx.Diagnostics.LogEntry::get_Timestamp()
extern "C"  DateTime_t339033936  LogEntry_get_Timestamp_m708156057 (LogEntry_t1891155722 * __this, const MethodInfo* method)
{
	{
		DateTime_t339033936  L_0 = __this->get_U3CTimestampU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void UniRx.Diagnostics.LogEntry::set_Timestamp(System.DateTime)
extern "C"  void LogEntry_set_Timestamp_m3000247386 (LogEntry_t1891155722 * __this, DateTime_t339033936  ___value0, const MethodInfo* method)
{
	{
		DateTime_t339033936  L_0 = ___value0;
		__this->set_U3CTimestampU3Ek__BackingField_3(L_0);
		return;
	}
}
// UnityEngine.Object UniRx.Diagnostics.LogEntry::get_Context()
extern "C"  Object_t3878351788 * LogEntry_get_Context_m364543516 (LogEntry_t1891155722 * __this, const MethodInfo* method)
{
	{
		Object_t3878351788 * L_0 = __this->get_U3CContextU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void UniRx.Diagnostics.LogEntry::set_Context(UnityEngine.Object)
extern "C"  void LogEntry_set_Context_m12151361 (LogEntry_t1891155722 * __this, Object_t3878351788 * ___value0, const MethodInfo* method)
{
	{
		Object_t3878351788 * L_0 = ___value0;
		__this->set_U3CContextU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Exception UniRx.Diagnostics.LogEntry::get_Exception()
extern "C"  Exception_t1967233988 * LogEntry_get_Exception_m1674930326 (LogEntry_t1891155722 * __this, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = __this->get_U3CExceptionU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void UniRx.Diagnostics.LogEntry::set_Exception(System.Exception)
extern "C"  void LogEntry_set_Exception_m2294780359 (LogEntry_t1891155722 * __this, Exception_t1967233988 * ___value0, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = ___value0;
		__this->set_U3CExceptionU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.String UniRx.Diagnostics.LogEntry::get_StackTrace()
extern "C"  String_t* LogEntry_get_StackTrace_m3339660518 (LogEntry_t1891155722 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CStackTraceU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void UniRx.Diagnostics.LogEntry::set_StackTrace(System.String)
extern "C"  void LogEntry_set_StackTrace_m3865377707 (LogEntry_t1891155722 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CStackTraceU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Object UniRx.Diagnostics.LogEntry::get_State()
extern "C"  Il2CppObject * LogEntry_get_State_m3935062520 (LogEntry_t1891155722 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U3CStateU3Ek__BackingField_7();
		return L_0;
	}
}
// System.Void UniRx.Diagnostics.LogEntry::set_State(System.Object)
extern "C"  void LogEntry_set_State_m683787227 (LogEntry_t1891155722 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_U3CStateU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.String UniRx.Diagnostics.LogEntry::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* LogType_t3529269451_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral2974;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t LogEntry_ToString_m1215367404_MetadataUsageId;
extern "C"  String_t* LogEntry_ToString_m1215367404 (LogEntry_t1891155722 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LogEntry_ToString_m1215367404_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	DateTime_t339033936  V_1;
	memset(&V_1, 0, sizeof(V_1));
	String_t* G_B3_0 = NULL;
	{
		Exception_t1967233988 * L_0 = LogEntry_get_Exception_m1674930326(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0025;
		}
	}
	{
		String_t* L_1 = Environment_get_NewLine_m1034655108(NULL /*static, unused*/, /*hidden argument*/NULL);
		Exception_t1967233988 * L_2 = LogEntry_get_Exception_m1674930326(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Exception::ToString() */, L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m138640077(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		G_B3_0 = L_4;
		goto IL_002a;
	}

IL_0025:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
	}

IL_002a:
	{
		V_0 = G_B3_0;
		StringU5BU5D_t2956870243* L_6 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)((int32_t)9)));
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		ArrayElementTypeCheck (L_6, _stringLiteral91);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t2956870243* L_7 = L_6;
		DateTime_t339033936  L_8 = LogEntry_get_Timestamp_m708156057(__this, /*hidden argument*/NULL);
		V_1 = L_8;
		String_t* L_9 = DateTime_ToString_m3221907059((&V_1), /*hidden argument*/NULL);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 1);
		ArrayElementTypeCheck (L_7, L_9);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_9);
		StringU5BU5D_t2956870243* L_10 = L_7;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		ArrayElementTypeCheck (L_10, _stringLiteral2974);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral2974);
		StringU5BU5D_t2956870243* L_11 = L_10;
		String_t* L_12 = LogEntry_get_LoggerName_m969967108(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 3);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_12);
		StringU5BU5D_t2956870243* L_13 = L_11;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral2974);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral2974);
		StringU5BU5D_t2956870243* L_14 = L_13;
		int32_t L_15 = LogEntry_get_LogType_m344512238(__this, /*hidden argument*/NULL);
		int32_t L_16 = L_15;
		Il2CppObject * L_17 = Box(LogType_t3529269451_il2cpp_TypeInfo_var, &L_16);
		NullCheck((Enum_t2778772662 *)L_17);
		String_t* L_18 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2778772662 *)L_17);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 5);
		ArrayElementTypeCheck (L_14, L_18);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)L_18);
		StringU5BU5D_t2956870243* L_19 = L_14;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 6);
		ArrayElementTypeCheck (L_19, _stringLiteral93);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral93);
		StringU5BU5D_t2956870243* L_20 = L_19;
		String_t* L_21 = LogEntry_get_Message_m3138159808(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 7);
		ArrayElementTypeCheck (L_20, L_21);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(7), (String_t*)L_21);
		StringU5BU5D_t2956870243* L_22 = L_20;
		String_t* L_23 = V_0;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, 8);
		ArrayElementTypeCheck (L_22, L_23);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(8), (String_t*)L_23);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_24 = String_Concat_m21867311(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		return L_24;
	}
}
// System.IDisposable UniRx.Diagnostics.LogEntryExtensions::LogToUnityDebug(UniRx.IObservable`1<UniRx.Diagnostics.LogEntry>)
extern Il2CppClass* UnityDebugSink_t4077792013_il2cpp_TypeInfo_var;
extern Il2CppClass* IObservable_1_t1649954086_il2cpp_TypeInfo_var;
extern const uint32_t LogEntryExtensions_LogToUnityDebug_m3186420352_MetadataUsageId;
extern "C"  Il2CppObject * LogEntryExtensions_LogToUnityDebug_m3186420352 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LogEntryExtensions_LogToUnityDebug_m3186420352_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___source0;
		UnityDebugSink_t4077792013 * L_1 = (UnityDebugSink_t4077792013 *)il2cpp_codegen_object_new(UnityDebugSink_t4077792013_il2cpp_TypeInfo_var);
		UnityDebugSink__ctor_m1854900132(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Il2CppObject * L_2 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<UniRx.Diagnostics.LogEntry>::Subscribe(UniRx.IObserver`1<T>) */, IObservable_1_t1649954086_il2cpp_TypeInfo_var, L_0, L_1);
		return L_2;
	}
}
// System.Void UniRx.Diagnostics.Logger::.ctor(System.String)
extern Il2CppClass* ObservableLogger_t586897455_il2cpp_TypeInfo_var;
extern const uint32_t Logger__ctor_m3165661341_MetadataUsageId;
extern "C"  void Logger__ctor_m3165661341 (Logger_t2118475020 * __this, String_t* ___loggerName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Logger__ctor_m3165661341_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___loggerName0;
		Logger_set_Name_m4046211323(__this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ObservableLogger_t586897455_il2cpp_TypeInfo_var);
		Action_1_t2039608427 * L_1 = ObservableLogger_RegisterLogger_m4245567678(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		__this->set_logPublisher_2(L_1);
		return;
	}
}
// System.Void UniRx.Diagnostics.Logger::.cctor()
extern "C"  void Logger__cctor_m2729944520 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.String UniRx.Diagnostics.Logger::get_Name()
extern "C"  String_t* Logger_get_Name_m2162682262 (Logger_t2118475020 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CNameU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void UniRx.Diagnostics.Logger::set_Name(System.String)
extern "C"  void Logger_set_Name_m4046211323 (Logger_t2118475020 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CNameU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void UniRx.Diagnostics.Logger::Debug(System.Object,UnityEngine.Object)
extern Il2CppClass* Logger_t2118475020_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t339033936_il2cpp_TypeInfo_var;
extern Il2CppClass* LogEntry_t1891155722_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m719378102_MethodInfo_var;
extern const uint32_t Logger_Debug_m3604783602_MetadataUsageId;
extern "C"  void Logger_Debug_m3604783602 (Logger_t2118475020 * __this, Il2CppObject * ___message0, Object_t3878351788 * ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Logger_Debug_m3604783602_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	Action_1_t2039608427 * G_B5_0 = NULL;
	Action_1_t2039608427 * G_B4_0 = NULL;
	String_t* G_B6_0 = NULL;
	Action_1_t2039608427 * G_B6_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t2118475020_il2cpp_TypeInfo_var);
		bool L_0 = ((Logger_t2118475020_StaticFields*)Logger_t2118475020_il2cpp_TypeInfo_var->static_fields)->get_isInitialized_0();
		if (L_0)
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t2118475020_il2cpp_TypeInfo_var);
		((Logger_t2118475020_StaticFields*)Logger_t2118475020_il2cpp_TypeInfo_var->static_fields)->set_isInitialized_0((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		bool L_1 = Debug_get_isDebugBuild_m351497798(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Logger_t2118475020_StaticFields*)Logger_t2118475020_il2cpp_TypeInfo_var->static_fields)->set_isDebugBuild_1(L_1);
	}

IL_001a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t2118475020_il2cpp_TypeInfo_var);
		bool L_2 = ((Logger_t2118475020_StaticFields*)Logger_t2118475020_il2cpp_TypeInfo_var->static_fields)->get_isDebugBuild_1();
		if (!L_2)
		{
			goto IL_005c;
		}
	}
	{
		Action_1_t2039608427 * L_3 = __this->get_logPublisher_2();
		Il2CppObject * L_4 = ___message0;
		G_B4_0 = L_3;
		if (!L_4)
		{
			G_B5_0 = L_3;
			goto IL_003b;
		}
	}
	{
		Il2CppObject * L_5 = ___message0;
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_5);
		G_B6_0 = L_6;
		G_B6_1 = G_B4_0;
		goto IL_0040;
	}

IL_003b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_7;
		G_B6_1 = G_B5_0;
	}

IL_0040:
	{
		V_0 = G_B6_0;
		String_t* L_8 = Logger_get_Name_m2162682262(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t339033936_il2cpp_TypeInfo_var);
		DateTime_t339033936  L_9 = DateTime_get_Now_m1812131422(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_10 = V_0;
		Object_t3878351788 * L_11 = ___context1;
		LogEntry_t1891155722 * L_12 = (LogEntry_t1891155722 *)il2cpp_codegen_object_new(LogEntry_t1891155722_il2cpp_TypeInfo_var);
		LogEntry__ctor_m3810061886(L_12, L_8, 3, L_9, L_10, L_11, (Exception_t1967233988 *)NULL, (String_t*)NULL, NULL, /*hidden argument*/NULL);
		NullCheck(G_B6_1);
		Action_1_Invoke_m719378102(G_B6_1, L_12, /*hidden argument*/Action_1_Invoke_m719378102_MethodInfo_var);
	}

IL_005c:
	{
		return;
	}
}
// System.Void UniRx.Diagnostics.Logger::DebugFormat(System.String,System.Object[])
extern Il2CppClass* Logger_t2118475020_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t339033936_il2cpp_TypeInfo_var;
extern Il2CppClass* LogEntry_t1891155722_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m719378102_MethodInfo_var;
extern const uint32_t Logger_DebugFormat_m11770049_MetadataUsageId;
extern "C"  void Logger_DebugFormat_m11770049 (Logger_t2118475020 * __this, String_t* ___format0, ObjectU5BU5D_t11523773* ___args1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Logger_DebugFormat_m11770049_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	Action_1_t2039608427 * G_B5_0 = NULL;
	Action_1_t2039608427 * G_B4_0 = NULL;
	String_t* G_B6_0 = NULL;
	Action_1_t2039608427 * G_B6_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t2118475020_il2cpp_TypeInfo_var);
		bool L_0 = ((Logger_t2118475020_StaticFields*)Logger_t2118475020_il2cpp_TypeInfo_var->static_fields)->get_isInitialized_0();
		if (L_0)
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t2118475020_il2cpp_TypeInfo_var);
		((Logger_t2118475020_StaticFields*)Logger_t2118475020_il2cpp_TypeInfo_var->static_fields)->set_isInitialized_0((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		bool L_1 = Debug_get_isDebugBuild_m351497798(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Logger_t2118475020_StaticFields*)Logger_t2118475020_il2cpp_TypeInfo_var->static_fields)->set_isDebugBuild_1(L_1);
	}

IL_001a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Logger_t2118475020_il2cpp_TypeInfo_var);
		bool L_2 = ((Logger_t2118475020_StaticFields*)Logger_t2118475020_il2cpp_TypeInfo_var->static_fields)->get_isDebugBuild_1();
		if (!L_2)
		{
			goto IL_005d;
		}
	}
	{
		Action_1_t2039608427 * L_3 = __this->get_logPublisher_2();
		String_t* L_4 = ___format0;
		G_B4_0 = L_3;
		if (!L_4)
		{
			G_B5_0 = L_3;
			goto IL_003c;
		}
	}
	{
		String_t* L_5 = ___format0;
		ObjectU5BU5D_t11523773* L_6 = ___args1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Format_m4050103162(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		G_B6_0 = L_7;
		G_B6_1 = G_B4_0;
		goto IL_0041;
	}

IL_003c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_8;
		G_B6_1 = G_B5_0;
	}

IL_0041:
	{
		V_0 = G_B6_0;
		String_t* L_9 = Logger_get_Name_m2162682262(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t339033936_il2cpp_TypeInfo_var);
		DateTime_t339033936  L_10 = DateTime_get_Now_m1812131422(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_11 = V_0;
		LogEntry_t1891155722 * L_12 = (LogEntry_t1891155722 *)il2cpp_codegen_object_new(LogEntry_t1891155722_il2cpp_TypeInfo_var);
		LogEntry__ctor_m3810061886(L_12, L_9, 3, L_10, L_11, (Object_t3878351788 *)NULL, (Exception_t1967233988 *)NULL, (String_t*)NULL, NULL, /*hidden argument*/NULL);
		NullCheck(G_B6_1);
		Action_1_Invoke_m719378102(G_B6_1, L_12, /*hidden argument*/Action_1_Invoke_m719378102_MethodInfo_var);
	}

IL_005d:
	{
		return;
	}
}
// System.Void UniRx.Diagnostics.Logger::Log(System.Object,UnityEngine.Object)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t339033936_il2cpp_TypeInfo_var;
extern Il2CppClass* LogEntry_t1891155722_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m719378102_MethodInfo_var;
extern const uint32_t Logger_Log_m1113486115_MetadataUsageId;
extern "C"  void Logger_Log_m1113486115 (Logger_t2118475020 * __this, Il2CppObject * ___message0, Object_t3878351788 * ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Logger_Log_m1113486115_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	Action_1_t2039608427 * G_B2_0 = NULL;
	Action_1_t2039608427 * G_B1_0 = NULL;
	String_t* G_B3_0 = NULL;
	Action_1_t2039608427 * G_B3_1 = NULL;
	{
		Action_1_t2039608427 * L_0 = __this->get_logPublisher_2();
		Il2CppObject * L_1 = ___message0;
		G_B1_0 = L_0;
		if (!L_1)
		{
			G_B2_0 = L_0;
			goto IL_0017;
		}
	}
	{
		Il2CppObject * L_2 = ___message0;
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		G_B3_0 = L_3;
		G_B3_1 = G_B1_0;
		goto IL_001c;
	}

IL_0017:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_4;
		G_B3_1 = G_B2_0;
	}

IL_001c:
	{
		V_0 = G_B3_0;
		String_t* L_5 = Logger_get_Name_m2162682262(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t339033936_il2cpp_TypeInfo_var);
		DateTime_t339033936  L_6 = DateTime_get_Now_m1812131422(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_7 = V_0;
		Object_t3878351788 * L_8 = ___context1;
		LogEntry_t1891155722 * L_9 = (LogEntry_t1891155722 *)il2cpp_codegen_object_new(LogEntry_t1891155722_il2cpp_TypeInfo_var);
		LogEntry__ctor_m3810061886(L_9, L_5, 3, L_6, L_7, L_8, (Exception_t1967233988 *)NULL, (String_t*)NULL, NULL, /*hidden argument*/NULL);
		NullCheck(G_B3_1);
		Action_1_Invoke_m719378102(G_B3_1, L_9, /*hidden argument*/Action_1_Invoke_m719378102_MethodInfo_var);
		return;
	}
}
// System.Void UniRx.Diagnostics.Logger::LogFormat(System.String,System.Object[])
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t339033936_il2cpp_TypeInfo_var;
extern Il2CppClass* LogEntry_t1891155722_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m719378102_MethodInfo_var;
extern const uint32_t Logger_LogFormat_m3098177008_MetadataUsageId;
extern "C"  void Logger_LogFormat_m3098177008 (Logger_t2118475020 * __this, String_t* ___format0, ObjectU5BU5D_t11523773* ___args1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Logger_LogFormat_m3098177008_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	Action_1_t2039608427 * G_B2_0 = NULL;
	Action_1_t2039608427 * G_B1_0 = NULL;
	String_t* G_B3_0 = NULL;
	Action_1_t2039608427 * G_B3_1 = NULL;
	{
		Action_1_t2039608427 * L_0 = __this->get_logPublisher_2();
		String_t* L_1 = ___format0;
		G_B1_0 = L_0;
		if (!L_1)
		{
			G_B2_0 = L_0;
			goto IL_0018;
		}
	}
	{
		String_t* L_2 = ___format0;
		ObjectU5BU5D_t11523773* L_3 = ___args1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Format_m4050103162(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		goto IL_001d;
	}

IL_0018:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
	}

IL_001d:
	{
		V_0 = G_B3_0;
		String_t* L_6 = Logger_get_Name_m2162682262(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t339033936_il2cpp_TypeInfo_var);
		DateTime_t339033936  L_7 = DateTime_get_Now_m1812131422(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_8 = V_0;
		LogEntry_t1891155722 * L_9 = (LogEntry_t1891155722 *)il2cpp_codegen_object_new(LogEntry_t1891155722_il2cpp_TypeInfo_var);
		LogEntry__ctor_m3810061886(L_9, L_6, 3, L_7, L_8, (Object_t3878351788 *)NULL, (Exception_t1967233988 *)NULL, (String_t*)NULL, NULL, /*hidden argument*/NULL);
		NullCheck(G_B3_1);
		Action_1_Invoke_m719378102(G_B3_1, L_9, /*hidden argument*/Action_1_Invoke_m719378102_MethodInfo_var);
		return;
	}
}
// System.Void UniRx.Diagnostics.Logger::Warning(System.Object,UnityEngine.Object)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t339033936_il2cpp_TypeInfo_var;
extern Il2CppClass* LogEntry_t1891155722_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m719378102_MethodInfo_var;
extern const uint32_t Logger_Warning_m2950760571_MetadataUsageId;
extern "C"  void Logger_Warning_m2950760571 (Logger_t2118475020 * __this, Il2CppObject * ___message0, Object_t3878351788 * ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Logger_Warning_m2950760571_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	Action_1_t2039608427 * G_B2_0 = NULL;
	Action_1_t2039608427 * G_B1_0 = NULL;
	String_t* G_B3_0 = NULL;
	Action_1_t2039608427 * G_B3_1 = NULL;
	{
		Action_1_t2039608427 * L_0 = __this->get_logPublisher_2();
		Il2CppObject * L_1 = ___message0;
		G_B1_0 = L_0;
		if (!L_1)
		{
			G_B2_0 = L_0;
			goto IL_0017;
		}
	}
	{
		Il2CppObject * L_2 = ___message0;
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		G_B3_0 = L_3;
		G_B3_1 = G_B1_0;
		goto IL_001c;
	}

IL_0017:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_4;
		G_B3_1 = G_B2_0;
	}

IL_001c:
	{
		V_0 = G_B3_0;
		String_t* L_5 = Logger_get_Name_m2162682262(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t339033936_il2cpp_TypeInfo_var);
		DateTime_t339033936  L_6 = DateTime_get_Now_m1812131422(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_7 = V_0;
		Object_t3878351788 * L_8 = ___context1;
		LogEntry_t1891155722 * L_9 = (LogEntry_t1891155722 *)il2cpp_codegen_object_new(LogEntry_t1891155722_il2cpp_TypeInfo_var);
		LogEntry__ctor_m3810061886(L_9, L_5, 2, L_6, L_7, L_8, (Exception_t1967233988 *)NULL, (String_t*)NULL, NULL, /*hidden argument*/NULL);
		NullCheck(G_B3_1);
		Action_1_Invoke_m719378102(G_B3_1, L_9, /*hidden argument*/Action_1_Invoke_m719378102_MethodInfo_var);
		return;
	}
}
// System.Void UniRx.Diagnostics.Logger::WarningFormat(System.String,System.Object[])
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t339033936_il2cpp_TypeInfo_var;
extern Il2CppClass* LogEntry_t1891155722_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m719378102_MethodInfo_var;
extern const uint32_t Logger_WarningFormat_m2278275480_MetadataUsageId;
extern "C"  void Logger_WarningFormat_m2278275480 (Logger_t2118475020 * __this, String_t* ___format0, ObjectU5BU5D_t11523773* ___args1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Logger_WarningFormat_m2278275480_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	Action_1_t2039608427 * G_B2_0 = NULL;
	Action_1_t2039608427 * G_B1_0 = NULL;
	String_t* G_B3_0 = NULL;
	Action_1_t2039608427 * G_B3_1 = NULL;
	{
		Action_1_t2039608427 * L_0 = __this->get_logPublisher_2();
		String_t* L_1 = ___format0;
		G_B1_0 = L_0;
		if (!L_1)
		{
			G_B2_0 = L_0;
			goto IL_0018;
		}
	}
	{
		String_t* L_2 = ___format0;
		ObjectU5BU5D_t11523773* L_3 = ___args1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Format_m4050103162(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		goto IL_001d;
	}

IL_0018:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
	}

IL_001d:
	{
		V_0 = G_B3_0;
		String_t* L_6 = Logger_get_Name_m2162682262(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t339033936_il2cpp_TypeInfo_var);
		DateTime_t339033936  L_7 = DateTime_get_Now_m1812131422(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_8 = V_0;
		LogEntry_t1891155722 * L_9 = (LogEntry_t1891155722 *)il2cpp_codegen_object_new(LogEntry_t1891155722_il2cpp_TypeInfo_var);
		LogEntry__ctor_m3810061886(L_9, L_6, 2, L_7, L_8, (Object_t3878351788 *)NULL, (Exception_t1967233988 *)NULL, (String_t*)NULL, NULL, /*hidden argument*/NULL);
		NullCheck(G_B3_1);
		Action_1_Invoke_m719378102(G_B3_1, L_9, /*hidden argument*/Action_1_Invoke_m719378102_MethodInfo_var);
		return;
	}
}
// System.Void UniRx.Diagnostics.Logger::Error(System.Object,UnityEngine.Object)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t339033936_il2cpp_TypeInfo_var;
extern Il2CppClass* LogEntry_t1891155722_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m719378102_MethodInfo_var;
extern const uint32_t Logger_Error_m2103554151_MetadataUsageId;
extern "C"  void Logger_Error_m2103554151 (Logger_t2118475020 * __this, Il2CppObject * ___message0, Object_t3878351788 * ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Logger_Error_m2103554151_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	Action_1_t2039608427 * G_B2_0 = NULL;
	Action_1_t2039608427 * G_B1_0 = NULL;
	String_t* G_B3_0 = NULL;
	Action_1_t2039608427 * G_B3_1 = NULL;
	{
		Action_1_t2039608427 * L_0 = __this->get_logPublisher_2();
		Il2CppObject * L_1 = ___message0;
		G_B1_0 = L_0;
		if (!L_1)
		{
			G_B2_0 = L_0;
			goto IL_0017;
		}
	}
	{
		Il2CppObject * L_2 = ___message0;
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		G_B3_0 = L_3;
		G_B3_1 = G_B1_0;
		goto IL_001c;
	}

IL_0017:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_4;
		G_B3_1 = G_B2_0;
	}

IL_001c:
	{
		V_0 = G_B3_0;
		String_t* L_5 = Logger_get_Name_m2162682262(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t339033936_il2cpp_TypeInfo_var);
		DateTime_t339033936  L_6 = DateTime_get_Now_m1812131422(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_7 = V_0;
		Object_t3878351788 * L_8 = ___context1;
		LogEntry_t1891155722 * L_9 = (LogEntry_t1891155722 *)il2cpp_codegen_object_new(LogEntry_t1891155722_il2cpp_TypeInfo_var);
		LogEntry__ctor_m3810061886(L_9, L_5, 0, L_6, L_7, L_8, (Exception_t1967233988 *)NULL, (String_t*)NULL, NULL, /*hidden argument*/NULL);
		NullCheck(G_B3_1);
		Action_1_Invoke_m719378102(G_B3_1, L_9, /*hidden argument*/Action_1_Invoke_m719378102_MethodInfo_var);
		return;
	}
}
// System.Void UniRx.Diagnostics.Logger::ErrorFormat(System.String,System.Object[])
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t339033936_il2cpp_TypeInfo_var;
extern Il2CppClass* LogEntry_t1891155722_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m719378102_MethodInfo_var;
extern const uint32_t Logger_ErrorFormat_m379648556_MetadataUsageId;
extern "C"  void Logger_ErrorFormat_m379648556 (Logger_t2118475020 * __this, String_t* ___format0, ObjectU5BU5D_t11523773* ___args1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Logger_ErrorFormat_m379648556_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	Action_1_t2039608427 * G_B2_0 = NULL;
	Action_1_t2039608427 * G_B1_0 = NULL;
	String_t* G_B3_0 = NULL;
	Action_1_t2039608427 * G_B3_1 = NULL;
	{
		Action_1_t2039608427 * L_0 = __this->get_logPublisher_2();
		String_t* L_1 = ___format0;
		G_B1_0 = L_0;
		if (!L_1)
		{
			G_B2_0 = L_0;
			goto IL_0018;
		}
	}
	{
		String_t* L_2 = ___format0;
		ObjectU5BU5D_t11523773* L_3 = ___args1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Format_m4050103162(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		goto IL_001d;
	}

IL_0018:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
	}

IL_001d:
	{
		V_0 = G_B3_0;
		String_t* L_6 = Logger_get_Name_m2162682262(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t339033936_il2cpp_TypeInfo_var);
		DateTime_t339033936  L_7 = DateTime_get_Now_m1812131422(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_8 = V_0;
		LogEntry_t1891155722 * L_9 = (LogEntry_t1891155722 *)il2cpp_codegen_object_new(LogEntry_t1891155722_il2cpp_TypeInfo_var);
		LogEntry__ctor_m3810061886(L_9, L_6, 0, L_7, L_8, (Object_t3878351788 *)NULL, (Exception_t1967233988 *)NULL, (String_t*)NULL, NULL, /*hidden argument*/NULL);
		NullCheck(G_B3_1);
		Action_1_Invoke_m719378102(G_B3_1, L_9, /*hidden argument*/Action_1_Invoke_m719378102_MethodInfo_var);
		return;
	}
}
// System.Void UniRx.Diagnostics.Logger::Exception(System.Exception,UnityEngine.Object)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t339033936_il2cpp_TypeInfo_var;
extern Il2CppClass* LogEntry_t1891155722_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m719378102_MethodInfo_var;
extern const uint32_t Logger_Exception_m784696238_MetadataUsageId;
extern "C"  void Logger_Exception_m784696238 (Logger_t2118475020 * __this, Exception_t1967233988 * ___exception0, Object_t3878351788 * ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Logger_Exception_m784696238_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	Action_1_t2039608427 * G_B2_0 = NULL;
	Action_1_t2039608427 * G_B1_0 = NULL;
	String_t* G_B3_0 = NULL;
	Action_1_t2039608427 * G_B3_1 = NULL;
	{
		Action_1_t2039608427 * L_0 = __this->get_logPublisher_2();
		Exception_t1967233988 * L_1 = ___exception0;
		G_B1_0 = L_0;
		if (!L_1)
		{
			G_B2_0 = L_0;
			goto IL_0017;
		}
	}
	{
		Exception_t1967233988 * L_2 = ___exception0;
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Exception::ToString() */, L_2);
		G_B3_0 = L_3;
		G_B3_1 = G_B1_0;
		goto IL_001c;
	}

IL_0017:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_4;
		G_B3_1 = G_B2_0;
	}

IL_001c:
	{
		V_0 = G_B3_0;
		String_t* L_5 = Logger_get_Name_m2162682262(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t339033936_il2cpp_TypeInfo_var);
		DateTime_t339033936  L_6 = DateTime_get_Now_m1812131422(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_7 = V_0;
		Object_t3878351788 * L_8 = ___context1;
		Exception_t1967233988 * L_9 = ___exception0;
		LogEntry_t1891155722 * L_10 = (LogEntry_t1891155722 *)il2cpp_codegen_object_new(LogEntry_t1891155722_il2cpp_TypeInfo_var);
		LogEntry__ctor_m3810061886(L_10, L_5, 4, L_6, L_7, L_8, L_9, (String_t*)NULL, NULL, /*hidden argument*/NULL);
		NullCheck(G_B3_1);
		Action_1_Invoke_m719378102(G_B3_1, L_10, /*hidden argument*/Action_1_Invoke_m719378102_MethodInfo_var);
		return;
	}
}
// System.Void UniRx.Diagnostics.Logger::Raw(UniRx.Diagnostics.LogEntry)
extern const MethodInfo* Action_1_Invoke_m719378102_MethodInfo_var;
extern const uint32_t Logger_Raw_m3355044675_MetadataUsageId;
extern "C"  void Logger_Raw_m3355044675 (Logger_t2118475020 * __this, LogEntry_t1891155722 * ___logEntry0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Logger_Raw_m3355044675_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LogEntry_t1891155722 * L_0 = ___logEntry0;
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		Action_1_t2039608427 * L_1 = __this->get_logPublisher_2();
		LogEntry_t1891155722 * L_2 = ___logEntry0;
		NullCheck(L_1);
		Action_1_Invoke_m719378102(L_1, L_2, /*hidden argument*/Action_1_Invoke_m719378102_MethodInfo_var);
	}

IL_0012:
	{
		return;
	}
}
// System.Void UniRx.Diagnostics.ObservableLogger::.ctor()
extern "C"  void ObservableLogger__ctor_m2728320066 (ObservableLogger_t586897455 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Diagnostics.ObservableLogger::.cctor()
extern Il2CppClass* Subject_1_t3829190342_il2cpp_TypeInfo_var;
extern Il2CppClass* ObservableLogger_t586897455_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1__ctor_m221326712_MethodInfo_var;
extern const uint32_t ObservableLogger__cctor_m2491447211_MetadataUsageId;
extern "C"  void ObservableLogger__cctor_m2491447211 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableLogger__cctor_m2491447211_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t3829190342 * L_0 = (Subject_1_t3829190342 *)il2cpp_codegen_object_new(Subject_1_t3829190342_il2cpp_TypeInfo_var);
		Subject_1__ctor_m221326712(L_0, /*hidden argument*/Subject_1__ctor_m221326712_MethodInfo_var);
		((ObservableLogger_t586897455_StaticFields*)ObservableLogger_t586897455_il2cpp_TypeInfo_var->static_fields)->set_logPublisher_0(L_0);
		ObservableLogger_t586897455 * L_1 = (ObservableLogger_t586897455 *)il2cpp_codegen_object_new(ObservableLogger_t586897455_il2cpp_TypeInfo_var);
		ObservableLogger__ctor_m2728320066(L_1, /*hidden argument*/NULL);
		((ObservableLogger_t586897455_StaticFields*)ObservableLogger_t586897455_il2cpp_TypeInfo_var->static_fields)->set_Listener_1(L_1);
		return;
	}
}
// System.Action`1<UniRx.Diagnostics.LogEntry> UniRx.Diagnostics.ObservableLogger::RegisterLogger(UniRx.Diagnostics.Logger)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppClass* ObservableLogger_t586897455_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t2039608427_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1_OnNext_m777308704_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3129516011_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3537879526;
extern const uint32_t ObservableLogger_RegisterLogger_m4245567678_MetadataUsageId;
extern "C"  Action_1_t2039608427 * ObservableLogger_RegisterLogger_m4245567678 (Il2CppObject * __this /* static, unused */, Logger_t2118475020 * ___logger0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableLogger_RegisterLogger_m4245567678_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Logger_t2118475020 * L_0 = ___logger0;
		NullCheck(L_0);
		String_t* L_1 = Logger_get_Name_m2162682262(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0016;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_2 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_2, _stringLiteral3537879526, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0016:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ObservableLogger_t586897455_il2cpp_TypeInfo_var);
		Subject_1_t3829190342 * L_3 = ((ObservableLogger_t586897455_StaticFields*)ObservableLogger_t586897455_il2cpp_TypeInfo_var->static_fields)->get_logPublisher_0();
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)Subject_1_OnNext_m777308704_MethodInfo_var);
		Action_1_t2039608427 * L_5 = (Action_1_t2039608427 *)il2cpp_codegen_object_new(Action_1_t2039608427_il2cpp_TypeInfo_var);
		Action_1__ctor_m3129516011(L_5, L_3, L_4, /*hidden argument*/Action_1__ctor_m3129516011_MethodInfo_var);
		return L_5;
	}
}
// System.IDisposable UniRx.Diagnostics.ObservableLogger::Subscribe(UniRx.IObserver`1<UniRx.Diagnostics.LogEntry>)
extern Il2CppClass* ObservableLogger_t586897455_il2cpp_TypeInfo_var;
extern const MethodInfo* Subject_1_Subscribe_m319187149_MethodInfo_var;
extern const uint32_t ObservableLogger_Subscribe_m1405270705_MetadataUsageId;
extern "C"  Il2CppObject * ObservableLogger_Subscribe_m1405270705 (ObservableLogger_t586897455 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObservableLogger_Subscribe_m1405270705_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ObservableLogger_t586897455_il2cpp_TypeInfo_var);
		Subject_1_t3829190342 * L_0 = ((ObservableLogger_t586897455_StaticFields*)ObservableLogger_t586897455_il2cpp_TypeInfo_var->static_fields)->get_logPublisher_0();
		Il2CppObject* L_1 = ___observer0;
		NullCheck(L_0);
		Il2CppObject * L_2 = Subject_1_Subscribe_m319187149(L_0, L_1, /*hidden argument*/Subject_1_Subscribe_m319187149_MethodInfo_var);
		return L_2;
	}
}
// System.Void UniRx.Diagnostics.UnityDebugSink::.ctor()
extern "C"  void UnityDebugSink__ctor_m1854900132 (UnityDebugSink_t4077792013 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Diagnostics.UnityDebugSink::OnCompleted()
extern "C"  void UnityDebugSink_OnCompleted_m3304550446 (UnityDebugSink_t4077792013 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.Diagnostics.UnityDebugSink::OnError(System.Exception)
extern "C"  void UnityDebugSink_OnError_m1434799195 (UnityDebugSink_t4077792013 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.Diagnostics.UnityDebugSink::OnNext(UniRx.Diagnostics.LogEntry)
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern const uint32_t UnityDebugSink_OnNext_m3025534090_MetadataUsageId;
extern "C"  void UnityDebugSink_OnNext_m3025534090 (UnityDebugSink_t4077792013 * __this, LogEntry_t1891155722 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityDebugSink_OnNext_m3025534090_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	{
		LogEntry_t1891155722 * L_0 = ___value0;
		if (L_0)
		{
			goto IL_0007;
		}
	}
	{
		return;
	}

IL_0007:
	{
		LogEntry_t1891155722 * L_1 = ___value0;
		NullCheck(L_1);
		Object_t3878351788 * L_2 = LogEntry_get_Context_m364543516(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		LogEntry_t1891155722 * L_3 = ___value0;
		NullCheck(L_3);
		int32_t L_4 = LogEntry_get_LogType_m344512238(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		int32_t L_5 = V_1;
		if (L_5 == 0)
		{
			goto IL_0034;
		}
		if (L_5 == 1)
		{
			goto IL_00e4;
		}
		if (L_5 == 2)
		{
			goto IL_00b8;
		}
		if (L_5 == 3)
		{
			goto IL_008c;
		}
		if (L_5 == 4)
		{
			goto IL_0060;
		}
	}
	{
		goto IL_00e4;
	}

IL_0034:
	{
		Il2CppObject * L_6 = V_0;
		if (L_6)
		{
			goto IL_004a;
		}
	}
	{
		LogEntry_t1891155722 * L_7 = ___value0;
		NullCheck(L_7);
		String_t* L_8 = LogEntry_get_Message_m3138159808(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m1836388681(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		goto IL_005b;
	}

IL_004a:
	{
		LogEntry_t1891155722 * L_9 = ___value0;
		NullCheck(L_9);
		String_t* L_10 = LogEntry_get_Message_m3138159808(L_9, /*hidden argument*/NULL);
		LogEntry_t1891155722 * L_11 = ___value0;
		NullCheck(L_11);
		Object_t3878351788 * L_12 = LogEntry_get_Context_m364543516(L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m214246398(NULL /*static, unused*/, L_10, L_12, /*hidden argument*/NULL);
	}

IL_005b:
	{
		goto IL_00e9;
	}

IL_0060:
	{
		Il2CppObject * L_13 = V_0;
		if (L_13)
		{
			goto IL_0076;
		}
	}
	{
		LogEntry_t1891155722 * L_14 = ___value0;
		NullCheck(L_14);
		Exception_t1967233988 * L_15 = LogEntry_get_Exception_m1674930326(L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogException_m248970745(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		goto IL_0087;
	}

IL_0076:
	{
		LogEntry_t1891155722 * L_16 = ___value0;
		NullCheck(L_16);
		Exception_t1967233988 * L_17 = LogEntry_get_Exception_m1674930326(L_16, /*hidden argument*/NULL);
		LogEntry_t1891155722 * L_18 = ___value0;
		NullCheck(L_18);
		Object_t3878351788 * L_19 = LogEntry_get_Context_m364543516(L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogException_m2264672311(NULL /*static, unused*/, L_17, L_19, /*hidden argument*/NULL);
	}

IL_0087:
	{
		goto IL_00e9;
	}

IL_008c:
	{
		Il2CppObject * L_20 = V_0;
		if (L_20)
		{
			goto IL_00a2;
		}
	}
	{
		LogEntry_t1891155722 * L_21 = ___value0;
		NullCheck(L_21);
		String_t* L_22 = LogEntry_get_Message_m3138159808(L_21, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m3349710197(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		goto IL_00b3;
	}

IL_00a2:
	{
		LogEntry_t1891155722 * L_23 = ___value0;
		NullCheck(L_23);
		String_t* L_24 = LogEntry_get_Message_m3138159808(L_23, /*hidden argument*/NULL);
		LogEntry_t1891155722 * L_25 = ___value0;
		NullCheck(L_25);
		Object_t3878351788 * L_26 = LogEntry_get_Context_m364543516(L_25, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m776311748(NULL /*static, unused*/, L_24, L_26, /*hidden argument*/NULL);
	}

IL_00b3:
	{
		goto IL_00e9;
	}

IL_00b8:
	{
		Il2CppObject * L_27 = V_0;
		if (L_27)
		{
			goto IL_00ce;
		}
	}
	{
		LogEntry_t1891155722 * L_28 = ___value0;
		NullCheck(L_28);
		String_t* L_29 = LogEntry_get_Message_m3138159808(L_28, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogWarning_m539478453(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		goto IL_00df;
	}

IL_00ce:
	{
		LogEntry_t1891155722 * L_30 = ___value0;
		NullCheck(L_30);
		String_t* L_31 = LogEntry_get_Message_m3138159808(L_30, /*hidden argument*/NULL);
		LogEntry_t1891155722 * L_32 = ___value0;
		NullCheck(L_32);
		Object_t3878351788 * L_33 = LogEntry_get_Context_m364543516(L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogWarning_m4097176146(NULL /*static, unused*/, L_31, L_33, /*hidden argument*/NULL);
	}

IL_00df:
	{
		goto IL_00e9;
	}

IL_00e4:
	{
		goto IL_00e9;
	}

IL_00e9:
	{
		return;
	}
}
// System.Void UniRx.Disposable::.cctor()
extern Il2CppClass* EmptyDisposable_t3755833965_il2cpp_TypeInfo_var;
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const uint32_t Disposable__cctor_m2169092470_MetadataUsageId;
extern "C"  void Disposable__cctor_m2169092470 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Disposable__cctor_m2169092470_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EmptyDisposable_t3755833965_il2cpp_TypeInfo_var);
		EmptyDisposable_t3755833965 * L_0 = ((EmptyDisposable_t3755833965_StaticFields*)EmptyDisposable_t3755833965_il2cpp_TypeInfo_var->static_fields)->get_Singleton_0();
		((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->set_Empty_0(L_0);
		return;
	}
}
// System.IDisposable UniRx.Disposable::Create(System.Action)
extern Il2CppClass* AnonymousDisposable_t1367871885_il2cpp_TypeInfo_var;
extern const uint32_t Disposable_Create_m2910846009_MetadataUsageId;
extern "C"  Il2CppObject * Disposable_Create_m2910846009 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___disposeAction0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Disposable_Create_m2910846009_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t437523947 * L_0 = ___disposeAction0;
		AnonymousDisposable_t1367871885 * L_1 = (AnonymousDisposable_t1367871885 *)il2cpp_codegen_object_new(AnonymousDisposable_t1367871885_il2cpp_TypeInfo_var);
		AnonymousDisposable__ctor_m1501829700(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UniRx.Disposable/AnonymousDisposable::.ctor(System.Action)
extern "C"  void AnonymousDisposable__ctor_m1501829700 (AnonymousDisposable_t1367871885 * __this, Action_t437523947 * ___dispose0, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		Action_t437523947 * L_0 = ___dispose0;
		__this->set_dispose_1(L_0);
		return;
	}
}
// System.Void UniRx.Disposable/AnonymousDisposable::Dispose()
extern "C"  void AnonymousDisposable_Dispose_m1091325398 (AnonymousDisposable_t1367871885 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_isDisposed_0();
		if (L_0)
		{
			goto IL_001d;
		}
	}
	{
		__this->set_isDisposed_0((bool)1);
		Action_t437523947 * L_1 = __this->get_dispose_1();
		NullCheck(L_1);
		Action_Invoke_m1445970038(L_1, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Void UniRx.Disposable/EmptyDisposable::.ctor()
extern "C"  void EmptyDisposable__ctor_m2637963385 (EmptyDisposable_t3755833965 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Disposable/EmptyDisposable::.cctor()
extern Il2CppClass* EmptyDisposable_t3755833965_il2cpp_TypeInfo_var;
extern const uint32_t EmptyDisposable__cctor_m3985357396_MetadataUsageId;
extern "C"  void EmptyDisposable__cctor_m3985357396 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EmptyDisposable__cctor_m3985357396_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		EmptyDisposable_t3755833965 * L_0 = (EmptyDisposable_t3755833965 *)il2cpp_codegen_object_new(EmptyDisposable_t3755833965_il2cpp_TypeInfo_var);
		EmptyDisposable__ctor_m2637963385(L_0, /*hidden argument*/NULL);
		((EmptyDisposable_t3755833965_StaticFields*)EmptyDisposable_t3755833965_il2cpp_TypeInfo_var->static_fields)->set_Singleton_0(L_0);
		return;
	}
}
// System.Void UniRx.Disposable/EmptyDisposable::Dispose()
extern "C"  void EmptyDisposable_Dispose_m950391414 (EmptyDisposable_t3755833965 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UniRx.DoubleReactiveProperty::.ctor()
extern Il2CppClass* ReactiveProperty_1_t1142849652_il2cpp_TypeInfo_var;
extern const MethodInfo* ReactiveProperty_1__ctor_m3586573700_MethodInfo_var;
extern const uint32_t DoubleReactiveProperty__ctor_m3598373240_MetadataUsageId;
extern "C"  void DoubleReactiveProperty__ctor_m3598373240 (DoubleReactiveProperty_t414828849 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DoubleReactiveProperty__ctor_m3598373240_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ReactiveProperty_1_t1142849652_il2cpp_TypeInfo_var);
		ReactiveProperty_1__ctor_m3586573700(__this, /*hidden argument*/ReactiveProperty_1__ctor_m3586573700_MethodInfo_var);
		return;
	}
}
// System.Void UniRx.DoubleReactiveProperty::.ctor(System.Double)
extern Il2CppClass* ReactiveProperty_1_t1142849652_il2cpp_TypeInfo_var;
extern const MethodInfo* ReactiveProperty_1__ctor_m3809603674_MethodInfo_var;
extern const uint32_t DoubleReactiveProperty__ctor_m2782547146_MetadataUsageId;
extern "C"  void DoubleReactiveProperty__ctor_m2782547146 (DoubleReactiveProperty_t414828849 * __this, double ___initialValue0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DoubleReactiveProperty__ctor_m2782547146_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		double L_0 = ___initialValue0;
		IL2CPP_RUNTIME_CLASS_INIT(ReactiveProperty_1_t1142849652_il2cpp_TypeInfo_var);
		ReactiveProperty_1__ctor_m3809603674(__this, L_0, /*hidden argument*/ReactiveProperty_1__ctor_m3809603674_MethodInfo_var);
		return;
	}
}
// System.Void UniRx.Examples.Sample01_ObservableWWW::.ctor()
extern "C"  void Sample01_ObservableWWW__ctor_m3063018538 (Sample01_ObservableWWW_t1646932859 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m350695606(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Examples.Sample01_ObservableWWW::Start()
extern Il2CppClass* Sample01_ObservableWWW_t1646932859_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t1116941607_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t2115686693_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t676343372_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_3_t3610964670_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t1519452702_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppClass* IObservable_1U5BU5D_t855805015_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t3105322948_il2cpp_TypeInfo_var;
extern Il2CppClass* ScheduledNotifier_1_t2478225832_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t1106661726_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t3907447057_il2cpp_TypeInfo_var;
extern const MethodInfo* Sample01_ObservableWWW_U3CStartU3Em__9_m1978097430_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3871460119_MethodInfo_var;
extern const MethodInfo* Sample01_ObservableWWW_U3CStartU3Em__A_m348648996_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m15890083_MethodInfo_var;
extern const MethodInfo* ObservableExtensions_Subscribe_TisString_t_m3357525852_MethodInfo_var;
extern const MethodInfo* Sample01_ObservableWWW_U3CStartU3Em__B_m1918048653_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3040572673_MethodInfo_var;
extern const MethodInfo* Sample01_ObservableWWW_U3CStartU3Em__C_m713662703_MethodInfo_var;
extern const MethodInfo* Func_3__ctor_m2612643231_MethodInfo_var;
extern const MethodInfo* Observable_SelectMany_TisString_t_TisString_t_TisU3CU3E__AnonType0_2_t1370999997_m1859969858_MethodInfo_var;
extern const MethodInfo* Sample01_ObservableWWW_U3CStartU3Em__D_m183641227_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m2592018887_MethodInfo_var;
extern const MethodInfo* ObservableExtensions_Subscribe_TisU3CU3E__AnonType0_2_t1370999997_m765500324_MethodInfo_var;
extern const MethodInfo* Observable_WhenAll_TisString_t_m1484418158_MethodInfo_var;
extern const MethodInfo* Sample01_ObservableWWW_U3CStartU3Em__E_m3496170024_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m619640437_MethodInfo_var;
extern const MethodInfo* ObservableExtensions_Subscribe_TisStringU5BU5D_t2956870243_m2794009706_MethodInfo_var;
extern const MethodInfo* ScheduledNotifier_1__ctor_m184940776_MethodInfo_var;
extern const MethodInfo* Sample01_ObservableWWW_U3CStartU3Em__F_m3612411410_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m1610927424_MethodInfo_var;
extern const MethodInfo* ObservableExtensions_Subscribe_TisSingle_t958209021_m2633336639_MethodInfo_var;
extern const MethodInfo* ObservableExtensions_Subscribe_TisString_t_m1634303226_MethodInfo_var;
extern const MethodInfo* Sample01_ObservableWWW_U3CStartU3Em__10_m2096553598_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m1555254593_MethodInfo_var;
extern const MethodInfo* Observable_CatchIgnore_TisString_t_TisWWWErrorException_t3758994352_m2738378837_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2551628714;
extern Il2CppCodeGenString* _stringLiteral932264977;
extern Il2CppCodeGenString* _stringLiteral3308123626;
extern Il2CppCodeGenString* _stringLiteral2849422856;
extern Il2CppCodeGenString* _stringLiteral1510455230;
extern const uint32_t Sample01_ObservableWWW_Start_m2010156330_MetadataUsageId;
extern "C"  void Sample01_ObservableWWW_Start_m2010156330 (Sample01_ObservableWWW_t1646932859 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Sample01_ObservableWWW_Start_m2010156330_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject* V_2 = NULL;
	ScheduledNotifier_1_t2478225832 * V_3 = NULL;
	ScheduledNotifier_1_t2478225832 * V_4 = NULL;
	Il2CppObject* G_B2_0 = NULL;
	Il2CppObject* G_B1_0 = NULL;
	Action_1_t1116941607 * G_B4_0 = NULL;
	Il2CppObject* G_B4_1 = NULL;
	Action_1_t1116941607 * G_B3_0 = NULL;
	Il2CppObject* G_B3_1 = NULL;
	Il2CppObject* G_B6_0 = NULL;
	Il2CppObject* G_B5_0 = NULL;
	Func_2_t676343372 * G_B8_0 = NULL;
	Il2CppObject* G_B8_1 = NULL;
	Func_2_t676343372 * G_B7_0 = NULL;
	Il2CppObject* G_B7_1 = NULL;
	Il2CppObject* G_B10_0 = NULL;
	Il2CppObject* G_B9_0 = NULL;
	Il2CppObject* G_B12_0 = NULL;
	Il2CppObject* G_B11_0 = NULL;
	ScheduledNotifier_1_t2478225832 * G_B14_0 = NULL;
	ScheduledNotifier_1_t2478225832 * G_B13_0 = NULL;
	Il2CppObject* G_B16_0 = NULL;
	Il2CppObject* G_B15_0 = NULL;
	{
		Il2CppObject* L_0 = ObservableWWW_Get_m2633702238(NULL /*static, unused*/, _stringLiteral2551628714, (Dictionary_2_t2606186806 *)NULL, (Il2CppObject*)NULL, /*hidden argument*/NULL);
		Action_1_t1116941607 * L_1 = ((Sample01_ObservableWWW_t1646932859_StaticFields*)Sample01_ObservableWWW_t1646932859_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_2();
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_0024;
		}
	}
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)Sample01_ObservableWWW_U3CStartU3Em__9_m1978097430_MethodInfo_var);
		Action_1_t1116941607 * L_3 = (Action_1_t1116941607 *)il2cpp_codegen_object_new(Action_1_t1116941607_il2cpp_TypeInfo_var);
		Action_1__ctor_m3871460119(L_3, NULL, L_2, /*hidden argument*/Action_1__ctor_m3871460119_MethodInfo_var);
		((Sample01_ObservableWWW_t1646932859_StaticFields*)Sample01_ObservableWWW_t1646932859_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache0_2(L_3);
		G_B2_0 = G_B1_0;
	}

IL_0024:
	{
		Action_1_t1116941607 * L_4 = ((Sample01_ObservableWWW_t1646932859_StaticFields*)Sample01_ObservableWWW_t1646932859_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_2();
		Action_1_t2115686693 * L_5 = ((Sample01_ObservableWWW_t1646932859_StaticFields*)Sample01_ObservableWWW_t1646932859_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_3();
		G_B3_0 = L_4;
		G_B3_1 = G_B2_0;
		if (L_5)
		{
			G_B4_0 = L_4;
			G_B4_1 = G_B2_0;
			goto IL_0041;
		}
	}
	{
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)Sample01_ObservableWWW_U3CStartU3Em__A_m348648996_MethodInfo_var);
		Action_1_t2115686693 * L_7 = (Action_1_t2115686693 *)il2cpp_codegen_object_new(Action_1_t2115686693_il2cpp_TypeInfo_var);
		Action_1__ctor_m15890083(L_7, NULL, L_6, /*hidden argument*/Action_1__ctor_m15890083_MethodInfo_var);
		((Sample01_ObservableWWW_t1646932859_StaticFields*)Sample01_ObservableWWW_t1646932859_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache1_3(L_7);
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
	}

IL_0041:
	{
		Action_1_t2115686693 * L_8 = ((Sample01_ObservableWWW_t1646932859_StaticFields*)Sample01_ObservableWWW_t1646932859_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_3();
		ObservableExtensions_Subscribe_TisString_t_m3357525852(NULL /*static, unused*/, G_B4_1, G_B4_0, L_8, /*hidden argument*/ObservableExtensions_Subscribe_TisString_t_m3357525852_MethodInfo_var);
		Il2CppObject* L_9 = ObservableWWW_Get_m2633702238(NULL /*static, unused*/, _stringLiteral932264977, (Dictionary_2_t2606186806 *)NULL, (Il2CppObject*)NULL, /*hidden argument*/NULL);
		Func_2_t676343372 * L_10 = ((Sample01_ObservableWWW_t1646932859_StaticFields*)Sample01_ObservableWWW_t1646932859_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache2_4();
		G_B5_0 = L_9;
		if (L_10)
		{
			G_B6_0 = L_9;
			goto IL_0070;
		}
	}
	{
		IntPtr_t L_11;
		L_11.set_m_value_0((void*)(void*)Sample01_ObservableWWW_U3CStartU3Em__B_m1918048653_MethodInfo_var);
		Func_2_t676343372 * L_12 = (Func_2_t676343372 *)il2cpp_codegen_object_new(Func_2_t676343372_il2cpp_TypeInfo_var);
		Func_2__ctor_m3040572673(L_12, NULL, L_11, /*hidden argument*/Func_2__ctor_m3040572673_MethodInfo_var);
		((Sample01_ObservableWWW_t1646932859_StaticFields*)Sample01_ObservableWWW_t1646932859_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache2_4(L_12);
		G_B6_0 = G_B5_0;
	}

IL_0070:
	{
		Func_2_t676343372 * L_13 = ((Sample01_ObservableWWW_t1646932859_StaticFields*)Sample01_ObservableWWW_t1646932859_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache2_4();
		Func_3_t3610964670 * L_14 = ((Sample01_ObservableWWW_t1646932859_StaticFields*)Sample01_ObservableWWW_t1646932859_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache3_5();
		G_B7_0 = L_13;
		G_B7_1 = G_B6_0;
		if (L_14)
		{
			G_B8_0 = L_13;
			G_B8_1 = G_B6_0;
			goto IL_008d;
		}
	}
	{
		IntPtr_t L_15;
		L_15.set_m_value_0((void*)(void*)Sample01_ObservableWWW_U3CStartU3Em__C_m713662703_MethodInfo_var);
		Func_3_t3610964670 * L_16 = (Func_3_t3610964670 *)il2cpp_codegen_object_new(Func_3_t3610964670_il2cpp_TypeInfo_var);
		Func_3__ctor_m2612643231(L_16, NULL, L_15, /*hidden argument*/Func_3__ctor_m2612643231_MethodInfo_var);
		((Sample01_ObservableWWW_t1646932859_StaticFields*)Sample01_ObservableWWW_t1646932859_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache3_5(L_16);
		G_B8_0 = G_B7_0;
		G_B8_1 = G_B7_1;
	}

IL_008d:
	{
		Func_3_t3610964670 * L_17 = ((Sample01_ObservableWWW_t1646932859_StaticFields*)Sample01_ObservableWWW_t1646932859_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache3_5();
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_18 = Observable_SelectMany_TisString_t_TisString_t_TisU3CU3E__AnonType0_2_t1370999997_m1859969858(NULL /*static, unused*/, G_B8_1, G_B8_0, L_17, /*hidden argument*/Observable_SelectMany_TisString_t_TisString_t_TisU3CU3E__AnonType0_2_t1370999997_m1859969858_MethodInfo_var);
		V_0 = L_18;
		Il2CppObject* L_19 = V_0;
		Action_1_t1519452702 * L_20 = ((Sample01_ObservableWWW_t1646932859_StaticFields*)Sample01_ObservableWWW_t1646932859_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache4_6();
		G_B9_0 = L_19;
		if (L_20)
		{
			G_B10_0 = L_19;
			goto IL_00b1;
		}
	}
	{
		IntPtr_t L_21;
		L_21.set_m_value_0((void*)(void*)Sample01_ObservableWWW_U3CStartU3Em__D_m183641227_MethodInfo_var);
		Action_1_t1519452702 * L_22 = (Action_1_t1519452702 *)il2cpp_codegen_object_new(Action_1_t1519452702_il2cpp_TypeInfo_var);
		Action_1__ctor_m2592018887(L_22, NULL, L_21, /*hidden argument*/Action_1__ctor_m2592018887_MethodInfo_var);
		((Sample01_ObservableWWW_t1646932859_StaticFields*)Sample01_ObservableWWW_t1646932859_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache4_6(L_22);
		G_B10_0 = G_B9_0;
	}

IL_00b1:
	{
		Action_1_t1519452702 * L_23 = ((Sample01_ObservableWWW_t1646932859_StaticFields*)Sample01_ObservableWWW_t1646932859_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache4_6();
		Il2CppObject * L_24 = ObservableExtensions_Subscribe_TisU3CU3E__AnonType0_2_t1370999997_m765500324(NULL /*static, unused*/, G_B10_0, L_23, /*hidden argument*/ObservableExtensions_Subscribe_TisU3CU3E__AnonType0_2_t1370999997_m765500324_MethodInfo_var);
		V_1 = L_24;
		Il2CppObject * L_25 = V_1;
		NullCheck(L_25);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_25);
		IObservable_1U5BU5D_t855805015* L_26 = ((IObservable_1U5BU5D_t855805015*)SZArrayNew(IObservable_1U5BU5D_t855805015_il2cpp_TypeInfo_var, (uint32_t)3));
		Il2CppObject* L_27 = ObservableWWW_Get_m2633702238(NULL /*static, unused*/, _stringLiteral932264977, (Dictionary_2_t2606186806 *)NULL, (Il2CppObject*)NULL, /*hidden argument*/NULL);
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 0);
		ArrayElementTypeCheck (L_26, L_27);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject*)L_27);
		IObservable_1U5BU5D_t855805015* L_28 = L_26;
		Il2CppObject* L_29 = ObservableWWW_Get_m2633702238(NULL /*static, unused*/, _stringLiteral3308123626, (Dictionary_2_t2606186806 *)NULL, (Il2CppObject*)NULL, /*hidden argument*/NULL);
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, 1);
		ArrayElementTypeCheck (L_28, L_29);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject*)L_29);
		IObservable_1U5BU5D_t855805015* L_30 = L_28;
		Il2CppObject* L_31 = ObservableWWW_Get_m2633702238(NULL /*static, unused*/, _stringLiteral2849422856, (Dictionary_2_t2606186806 *)NULL, (Il2CppObject*)NULL, /*hidden argument*/NULL);
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 2);
		ArrayElementTypeCheck (L_30, L_31);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject*)L_31);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_32 = Observable_WhenAll_TisString_t_m1484418158(NULL /*static, unused*/, L_30, /*hidden argument*/Observable_WhenAll_TisString_t_m1484418158_MethodInfo_var);
		V_2 = L_32;
		Il2CppObject* L_33 = V_2;
		Action_1_t3105322948 * L_34 = ((Sample01_ObservableWWW_t1646932859_StaticFields*)Sample01_ObservableWWW_t1646932859_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache5_7();
		G_B11_0 = L_33;
		if (L_34)
		{
			G_B12_0 = L_33;
			goto IL_0114;
		}
	}
	{
		IntPtr_t L_35;
		L_35.set_m_value_0((void*)(void*)Sample01_ObservableWWW_U3CStartU3Em__E_m3496170024_MethodInfo_var);
		Action_1_t3105322948 * L_36 = (Action_1_t3105322948 *)il2cpp_codegen_object_new(Action_1_t3105322948_il2cpp_TypeInfo_var);
		Action_1__ctor_m619640437(L_36, NULL, L_35, /*hidden argument*/Action_1__ctor_m619640437_MethodInfo_var);
		((Sample01_ObservableWWW_t1646932859_StaticFields*)Sample01_ObservableWWW_t1646932859_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache5_7(L_36);
		G_B12_0 = G_B11_0;
	}

IL_0114:
	{
		Action_1_t3105322948 * L_37 = ((Sample01_ObservableWWW_t1646932859_StaticFields*)Sample01_ObservableWWW_t1646932859_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache5_7();
		ObservableExtensions_Subscribe_TisStringU5BU5D_t2956870243_m2794009706(NULL /*static, unused*/, G_B12_0, L_37, /*hidden argument*/ObservableExtensions_Subscribe_TisStringU5BU5D_t2956870243_m2794009706_MethodInfo_var);
		ScheduledNotifier_1_t2478225832 * L_38 = (ScheduledNotifier_1_t2478225832 *)il2cpp_codegen_object_new(ScheduledNotifier_1_t2478225832_il2cpp_TypeInfo_var);
		ScheduledNotifier_1__ctor_m184940776(L_38, /*hidden argument*/ScheduledNotifier_1__ctor_m184940776_MethodInfo_var);
		V_3 = L_38;
		ScheduledNotifier_1_t2478225832 * L_39 = V_3;
		Action_1_t1106661726 * L_40 = ((Sample01_ObservableWWW_t1646932859_StaticFields*)Sample01_ObservableWWW_t1646932859_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache6_8();
		G_B13_0 = L_39;
		if (L_40)
		{
			G_B14_0 = L_39;
			goto IL_013e;
		}
	}
	{
		IntPtr_t L_41;
		L_41.set_m_value_0((void*)(void*)Sample01_ObservableWWW_U3CStartU3Em__F_m3612411410_MethodInfo_var);
		Action_1_t1106661726 * L_42 = (Action_1_t1106661726 *)il2cpp_codegen_object_new(Action_1_t1106661726_il2cpp_TypeInfo_var);
		Action_1__ctor_m1610927424(L_42, NULL, L_41, /*hidden argument*/Action_1__ctor_m1610927424_MethodInfo_var);
		((Sample01_ObservableWWW_t1646932859_StaticFields*)Sample01_ObservableWWW_t1646932859_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache6_8(L_42);
		G_B14_0 = G_B13_0;
	}

IL_013e:
	{
		Action_1_t1106661726 * L_43 = ((Sample01_ObservableWWW_t1646932859_StaticFields*)Sample01_ObservableWWW_t1646932859_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache6_8();
		ObservableExtensions_Subscribe_TisSingle_t958209021_m2633336639(NULL /*static, unused*/, G_B14_0, L_43, /*hidden argument*/ObservableExtensions_Subscribe_TisSingle_t958209021_m2633336639_MethodInfo_var);
		ScheduledNotifier_1_t2478225832 * L_44 = V_3;
		V_4 = L_44;
		ScheduledNotifier_1_t2478225832 * L_45 = V_4;
		Il2CppObject* L_46 = ObservableWWW_Get_m2633702238(NULL /*static, unused*/, _stringLiteral932264977, (Dictionary_2_t2606186806 *)NULL, L_45, /*hidden argument*/NULL);
		ObservableExtensions_Subscribe_TisString_t_m1634303226(NULL /*static, unused*/, L_46, /*hidden argument*/ObservableExtensions_Subscribe_TisString_t_m1634303226_MethodInfo_var);
		Il2CppObject* L_47 = ObservableWWW_Get_m2633702238(NULL /*static, unused*/, _stringLiteral1510455230, (Dictionary_2_t2606186806 *)NULL, (Il2CppObject*)NULL, /*hidden argument*/NULL);
		Action_1_t3907447057 * L_48 = ((Sample01_ObservableWWW_t1646932859_StaticFields*)Sample01_ObservableWWW_t1646932859_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache7_9();
		G_B15_0 = L_47;
		if (L_48)
		{
			G_B16_0 = L_47;
			goto IL_0183;
		}
	}
	{
		IntPtr_t L_49;
		L_49.set_m_value_0((void*)(void*)Sample01_ObservableWWW_U3CStartU3Em__10_m2096553598_MethodInfo_var);
		Action_1_t3907447057 * L_50 = (Action_1_t3907447057 *)il2cpp_codegen_object_new(Action_1_t3907447057_il2cpp_TypeInfo_var);
		Action_1__ctor_m1555254593(L_50, NULL, L_49, /*hidden argument*/Action_1__ctor_m1555254593_MethodInfo_var);
		((Sample01_ObservableWWW_t1646932859_StaticFields*)Sample01_ObservableWWW_t1646932859_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache7_9(L_50);
		G_B16_0 = G_B15_0;
	}

IL_0183:
	{
		Action_1_t3907447057 * L_51 = ((Sample01_ObservableWWW_t1646932859_StaticFields*)Sample01_ObservableWWW_t1646932859_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache7_9();
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_52 = Observable_CatchIgnore_TisString_t_TisWWWErrorException_t3758994352_m2738378837(NULL /*static, unused*/, G_B16_0, L_51, /*hidden argument*/Observable_CatchIgnore_TisString_t_TisWWWErrorException_t3758994352_m2738378837_MethodInfo_var);
		ObservableExtensions_Subscribe_TisString_t_m1634303226(NULL /*static, unused*/, L_52, /*hidden argument*/ObservableExtensions_Subscribe_TisString_t_m1634303226_MethodInfo_var);
		return;
	}
}
// System.Void UniRx.Examples.Sample01_ObservableWWW::<Start>m__9(System.String)
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern const uint32_t Sample01_ObservableWWW_U3CStartU3Em__9_m1978097430_MetadataUsageId;
extern "C"  void Sample01_ObservableWWW_U3CStartU3Em__9_m1978097430 (Il2CppObject * __this /* static, unused */, String_t* ___x0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Sample01_ObservableWWW_U3CStartU3Em__9_m1978097430_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___x0;
		NullCheck(L_0);
		String_t* L_1 = String_Substring_m675079568(L_0, 0, ((int32_t)100), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m3349710197(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Examples.Sample01_ObservableWWW::<Start>m__A(System.Exception)
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern const uint32_t Sample01_ObservableWWW_U3CStartU3Em__A_m348648996_MetadataUsageId;
extern "C"  void Sample01_ObservableWWW_U3CStartU3Em__A_m348648996 (Il2CppObject * __this /* static, unused */, Exception_t1967233988 * ___ex0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Sample01_ObservableWWW_U3CStartU3Em__A_m348648996_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception_t1967233988 * L_0 = ___ex0;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogException_m248970745(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UniRx.IObservable`1<System.String> UniRx.Examples.Sample01_ObservableWWW::<Start>m__B(System.String)
extern Il2CppCodeGenString* _stringLiteral3308123626;
extern const uint32_t Sample01_ObservableWWW_U3CStartU3Em__B_m1918048653_MetadataUsageId;
extern "C"  Il2CppObject* Sample01_ObservableWWW_U3CStartU3Em__B_m1918048653 (Il2CppObject * __this /* static, unused */, String_t* ___google0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Sample01_ObservableWWW_U3CStartU3Em__B_m1918048653_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ObservableWWW_Get_m2633702238(NULL /*static, unused*/, _stringLiteral3308123626, (Dictionary_2_t2606186806 *)NULL, (Il2CppObject*)NULL, /*hidden argument*/NULL);
		return L_0;
	}
}
// <>__AnonType0`2<System.String,System.String> UniRx.Examples.Sample01_ObservableWWW::<Start>m__C(System.String,System.String)
extern Il2CppClass* U3CU3E__AnonType0_2_t1370999997_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3E__AnonType0_2__ctor_m100399262_MethodInfo_var;
extern const uint32_t Sample01_ObservableWWW_U3CStartU3Em__C_m713662703_MetadataUsageId;
extern "C"  U3CU3E__AnonType0_2_t1370999997 * Sample01_ObservableWWW_U3CStartU3Em__C_m713662703 (Il2CppObject * __this /* static, unused */, String_t* ___google0, String_t* ___bing1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Sample01_ObservableWWW_U3CStartU3Em__C_m713662703_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___google0;
		String_t* L_1 = ___bing1;
		U3CU3E__AnonType0_2_t1370999997 * L_2 = (U3CU3E__AnonType0_2_t1370999997 *)il2cpp_codegen_object_new(U3CU3E__AnonType0_2_t1370999997_il2cpp_TypeInfo_var);
		U3CU3E__AnonType0_2__ctor_m100399262(L_2, L_0, L_1, /*hidden argument*/U3CU3E__AnonType0_2__ctor_m100399262_MethodInfo_var);
		return L_2;
	}
}
// System.Void UniRx.Examples.Sample01_ObservableWWW::<Start>m__D(<>__AnonType0`2<System.String,System.String>)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3E__AnonType0_2_get_google_m1942857707_MethodInfo_var;
extern const MethodInfo* U3CU3E__AnonType0_2_get_bing_m4172723851_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral58;
extern const uint32_t Sample01_ObservableWWW_U3CStartU3Em__D_m183641227_MetadataUsageId;
extern "C"  void Sample01_ObservableWWW_U3CStartU3Em__D_m183641227 (Il2CppObject * __this /* static, unused */, U3CU3E__AnonType0_2_t1370999997 * ___x0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Sample01_ObservableWWW_U3CStartU3Em__D_m183641227_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		U3CU3E__AnonType0_2_t1370999997 * L_0 = ___x0;
		NullCheck(L_0);
		String_t* L_1 = U3CU3E__AnonType0_2_get_google_m1942857707(L_0, /*hidden argument*/U3CU3E__AnonType0_2_get_google_m1942857707_MethodInfo_var);
		NullCheck(L_1);
		String_t* L_2 = String_Substring_m675079568(L_1, 0, ((int32_t)100), /*hidden argument*/NULL);
		U3CU3E__AnonType0_2_t1370999997 * L_3 = ___x0;
		NullCheck(L_3);
		String_t* L_4 = U3CU3E__AnonType0_2_get_bing_m4172723851(L_3, /*hidden argument*/U3CU3E__AnonType0_2_get_bing_m4172723851_MethodInfo_var);
		NullCheck(L_4);
		String_t* L_5 = String_Substring_m675079568(L_4, 0, ((int32_t)100), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m1825781833(NULL /*static, unused*/, L_2, _stringLiteral58, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m3349710197(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Examples.Sample01_ObservableWWW::<Start>m__E(System.String[])
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern const uint32_t Sample01_ObservableWWW_U3CStartU3Em__E_m3496170024_MetadataUsageId;
extern "C"  void Sample01_ObservableWWW_U3CStartU3Em__E_m3496170024 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t2956870243* ___xs0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Sample01_ObservableWWW_U3CStartU3Em__E_m3496170024_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringU5BU5D_t2956870243* L_0 = ___xs0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		int32_t L_1 = 0;
		NullCheck(((L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_1))));
		String_t* L_2 = String_Substring_m675079568(((L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_1))), 0, ((int32_t)100), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m3349710197(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		StringU5BU5D_t2956870243* L_3 = ___xs0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 1);
		int32_t L_4 = 1;
		NullCheck(((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		String_t* L_5 = String_Substring_m675079568(((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), 0, ((int32_t)100), /*hidden argument*/NULL);
		Debug_Log_m3349710197(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		StringU5BU5D_t2956870243* L_6 = ___xs0;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		int32_t L_7 = 2;
		NullCheck(((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7))));
		String_t* L_8 = String_Substring_m675079568(((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7))), 0, ((int32_t)100), /*hidden argument*/NULL);
		Debug_Log_m3349710197(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Examples.Sample01_ObservableWWW::<Start>m__F(System.Single)
extern Il2CppClass* Single_t958209021_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern const uint32_t Sample01_ObservableWWW_U3CStartU3Em__F_m3612411410_MetadataUsageId;
extern "C"  void Sample01_ObservableWWW_U3CStartU3Em__F_m3612411410 (Il2CppObject * __this /* static, unused */, float ___x0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Sample01_ObservableWWW_U3CStartU3Em__F_m3612411410_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___x0;
		float L_1 = L_0;
		Il2CppObject * L_2 = Box(Single_t958209021_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m3349710197(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Examples.Sample01_ObservableWWW::<Start>m__10(UniRx.WWWErrorException)
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppClass* HttpStatusCode_t2736938801_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t2373214747_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m2759194411_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m2871721525_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m1739472607_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m730091314_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m2577713898_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral58;
extern const uint32_t Sample01_ObservableWWW_U3CStartU3Em__10_m2096553598_MetadataUsageId;
extern "C"  void Sample01_ObservableWWW_U3CStartU3Em__10_m2096553598 (Il2CppObject * __this /* static, unused */, WWWErrorException_t3758994352 * ___ex0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Sample01_ObservableWWW_U3CStartU3Em__10_m2096553598_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2_t2094718104  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Enumerator_t2373214747  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		WWWErrorException_t3758994352 * L_0 = ___ex0;
		NullCheck(L_0);
		String_t* L_1 = WWWErrorException_get_RawErrorMessage_m3100515952(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m3349710197(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		WWWErrorException_t3758994352 * L_2 = ___ex0;
		NullCheck(L_2);
		bool L_3 = WWWErrorException_get_HasResponse_m1677127805(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		WWWErrorException_t3758994352 * L_4 = ___ex0;
		NullCheck(L_4);
		int32_t L_5 = WWWErrorException_get_StatusCode_m715253407(L_4, /*hidden argument*/NULL);
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = Box(HttpStatusCode_t2736938801_il2cpp_TypeInfo_var, &L_6);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m3349710197(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
	}

IL_0026:
	{
		WWWErrorException_t3758994352 * L_8 = ___ex0;
		NullCheck(L_8);
		Dictionary_2_t2606186806 * L_9 = WWWErrorException_get_ResponseHeaders_m1839206209(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Enumerator_t2373214747  L_10 = Dictionary_2_GetEnumerator_m2759194411(L_9, /*hidden argument*/Dictionary_2_GetEnumerator_m2759194411_MethodInfo_var);
		V_1 = L_10;
	}

IL_0032:
	try
	{ // begin try (depth: 1)
		{
			goto IL_005c;
		}

IL_0037:
		{
			KeyValuePair_2_t2094718104  L_11 = Enumerator_get_Current_m2871721525((&V_1), /*hidden argument*/Enumerator_get_Current_m2871721525_MethodInfo_var);
			V_0 = L_11;
			String_t* L_12 = KeyValuePair_2_get_Key_m1739472607((&V_0), /*hidden argument*/KeyValuePair_2_get_Key_m1739472607_MethodInfo_var);
			String_t* L_13 = KeyValuePair_2_get_Value_m730091314((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m730091314_MethodInfo_var);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_14 = String_Concat_m1825781833(NULL /*static, unused*/, L_12, _stringLiteral58, L_13, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
			Debug_Log_m3349710197(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		}

IL_005c:
		{
			bool L_15 = Enumerator_MoveNext_m2577713898((&V_1), /*hidden argument*/Enumerator_MoveNext_m2577713898_MethodInfo_var);
			if (L_15)
			{
				goto IL_0037;
			}
		}

IL_0068:
		{
			IL2CPP_LEAVE(0x79, FINALLY_006d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_006d;
	}

FINALLY_006d:
	{ // begin finally (depth: 1)
		Enumerator_t2373214747  L_16 = V_1;
		Enumerator_t2373214747  L_17 = L_16;
		Il2CppObject * L_18 = Box(Enumerator_t2373214747_il2cpp_TypeInfo_var, &L_17);
		NullCheck((Il2CppObject *)L_18);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_18);
		IL2CPP_END_FINALLY(109)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(109)
	{
		IL2CPP_JUMP_TBL(0x79, IL_0079)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0079:
	{
		return;
	}
}
// System.Void UniRx.Examples.Sample02_ObservableTriggers::.ctor()
extern "C"  void Sample02_ObservableTriggers__ctor_m4264090891 (Sample02_ObservableTriggers_t2916496460 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m350695606(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Examples.Sample02_ObservableTriggers::Start()
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern Il2CppClass* Sample02_ObservableTriggers_t2916496460_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t2706738743_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisObservableUpdateTrigger_t3433526989_m342345901_MethodInfo_var;
extern const MethodInfo* Observable_SampleFrame_TisUnit_t2558286038_m1174250833_MethodInfo_var;
extern const MethodInfo* Sample02_ObservableTriggers_U3CStartU3Em__11_m899926226_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m1024136343_MethodInfo_var;
extern const MethodInfo* Sample02_ObservableTriggers_U3CStartU3Em__12_m602884271_MethodInfo_var;
extern const MethodInfo* ObservableExtensions_Subscribe_TisUnit_t2558286038_m2179646475_MethodInfo_var;
extern const uint32_t Sample02_ObservableTriggers_Start_m3211228683_MetadataUsageId;
extern "C"  void Sample02_ObservableTriggers_Start_m3211228683 (Sample02_ObservableTriggers_t2916496460 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Sample02_ObservableTriggers_Start_m3211228683_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t4012695102 * V_0 = NULL;
	Il2CppObject* G_B2_0 = NULL;
	Il2CppObject* G_B1_0 = NULL;
	Action_1_t2706738743 * G_B4_0 = NULL;
	Il2CppObject* G_B4_1 = NULL;
	Action_1_t2706738743 * G_B3_0 = NULL;
	Il2CppObject* G_B3_1 = NULL;
	{
		GameObject_t4012695102 * L_0 = GameObject_CreatePrimitive_m3259337130(NULL /*static, unused*/, 3, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObject_t4012695102 * L_1 = V_0;
		NullCheck(L_1);
		ObservableUpdateTrigger_t3433526989 * L_2 = GameObject_AddComponent_TisObservableUpdateTrigger_t3433526989_m342345901(L_1, /*hidden argument*/GameObject_AddComponent_TisObservableUpdateTrigger_t3433526989_m342345901_MethodInfo_var);
		NullCheck(L_2);
		Il2CppObject* L_3 = ObservableUpdateTrigger_UpdateAsObservable_m3223523532(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_4 = Observable_SampleFrame_TisUnit_t2558286038_m1174250833(NULL /*static, unused*/, L_3, ((int32_t)30), 0, /*hidden argument*/Observable_SampleFrame_TisUnit_t2558286038_m1174250833_MethodInfo_var);
		Action_1_t2706738743 * L_5 = ((Sample02_ObservableTriggers_t2916496460_StaticFields*)Sample02_ObservableTriggers_t2916496460_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_2();
		G_B1_0 = L_4;
		if (L_5)
		{
			G_B2_0 = L_4;
			goto IL_0032;
		}
	}
	{
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)Sample02_ObservableTriggers_U3CStartU3Em__11_m899926226_MethodInfo_var);
		Action_1_t2706738743 * L_7 = (Action_1_t2706738743 *)il2cpp_codegen_object_new(Action_1_t2706738743_il2cpp_TypeInfo_var);
		Action_1__ctor_m1024136343(L_7, NULL, L_6, /*hidden argument*/Action_1__ctor_m1024136343_MethodInfo_var);
		((Sample02_ObservableTriggers_t2916496460_StaticFields*)Sample02_ObservableTriggers_t2916496460_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache0_2(L_7);
		G_B2_0 = G_B1_0;
	}

IL_0032:
	{
		Action_1_t2706738743 * L_8 = ((Sample02_ObservableTriggers_t2916496460_StaticFields*)Sample02_ObservableTriggers_t2916496460_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_2();
		Action_t437523947 * L_9 = ((Sample02_ObservableTriggers_t2916496460_StaticFields*)Sample02_ObservableTriggers_t2916496460_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_3();
		G_B3_0 = L_8;
		G_B3_1 = G_B2_0;
		if (L_9)
		{
			G_B4_0 = L_8;
			G_B4_1 = G_B2_0;
			goto IL_004f;
		}
	}
	{
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)Sample02_ObservableTriggers_U3CStartU3Em__12_m602884271_MethodInfo_var);
		Action_t437523947 * L_11 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, NULL, L_10, /*hidden argument*/NULL);
		((Sample02_ObservableTriggers_t2916496460_StaticFields*)Sample02_ObservableTriggers_t2916496460_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache1_3(L_11);
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
	}

IL_004f:
	{
		Action_t437523947 * L_12 = ((Sample02_ObservableTriggers_t2916496460_StaticFields*)Sample02_ObservableTriggers_t2916496460_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_3();
		ObservableExtensions_Subscribe_TisUnit_t2558286038_m2179646475(NULL /*static, unused*/, G_B4_1, G_B4_0, L_12, /*hidden argument*/ObservableExtensions_Subscribe_TisUnit_t2558286038_m2179646475_MethodInfo_var);
		GameObject_t4012695102 * L_13 = V_0;
		Object_Destroy_m2260435093(NULL /*static, unused*/, L_13, (3.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Examples.Sample02_ObservableTriggers::<Start>m__11(UniRx.Unit)
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3064885;
extern const uint32_t Sample02_ObservableTriggers_U3CStartU3Em__11_m899926226_MetadataUsageId;
extern "C"  void Sample02_ObservableTriggers_U3CStartU3Em__11_m899926226 (Il2CppObject * __this /* static, unused */, Unit_t2558286038  ___x0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Sample02_ObservableTriggers_U3CStartU3Em__11_m899926226_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m3349710197(NULL /*static, unused*/, _stringLiteral3064885, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Examples.Sample02_ObservableTriggers::<Start>m__12()
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1557372922;
extern const uint32_t Sample02_ObservableTriggers_U3CStartU3Em__12_m602884271_MetadataUsageId;
extern "C"  void Sample02_ObservableTriggers_U3CStartU3Em__12_m602884271 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Sample02_ObservableTriggers_U3CStartU3Em__12_m602884271_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m3349710197(NULL /*static, unused*/, _stringLiteral1557372922, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Examples.Sample04_ConvertFromUnityCallback::.ctor()
extern "C"  void Sample04_ConvertFromUnityCallback__ctor_m2143655214 (Sample04_ConvertFromUnityCallback_t3574096393 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m350695606(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Examples.Sample04_ConvertFromUnityCallback::Awake()
extern Il2CppClass* Sample04_ConvertFromUnityCallback_t3574096393_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t2102879704_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t3384115434_il2cpp_TypeInfo_var;
extern const MethodInfo* Sample04_ConvertFromUnityCallback_U3CAwakeU3Em__13_m4234092497_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3611357982_MethodInfo_var;
extern const MethodInfo* Observable_Where_TisLogCallback_t3235662730_m1342342965_MethodInfo_var;
extern const MethodInfo* Sample04_ConvertFromUnityCallback_U3CAwakeU3Em__14_m2095151222_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m495595544_MethodInfo_var;
extern const MethodInfo* ObservableExtensions_Subscribe_TisLogCallback_t3235662730_m968574131_MethodInfo_var;
extern const MethodInfo* Sample04_ConvertFromUnityCallback_U3CAwakeU3Em__15_m3816695891_MethodInfo_var;
extern const MethodInfo* Sample04_ConvertFromUnityCallback_U3CAwakeU3Em__16_m1677754616_MethodInfo_var;
extern const uint32_t Sample04_ConvertFromUnityCallback_Awake_m2381260433_MetadataUsageId;
extern "C"  void Sample04_ConvertFromUnityCallback_Awake_m2381260433 (Sample04_ConvertFromUnityCallback_t3574096393 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Sample04_ConvertFromUnityCallback_Awake_m2381260433_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* G_B2_0 = NULL;
	Il2CppObject* G_B1_0 = NULL;
	Il2CppObject* G_B4_0 = NULL;
	Il2CppObject* G_B3_0 = NULL;
	Il2CppObject* G_B6_0 = NULL;
	Il2CppObject* G_B5_0 = NULL;
	Il2CppObject* G_B8_0 = NULL;
	Il2CppObject* G_B7_0 = NULL;
	{
		Il2CppObject* L_0 = LogHelper_LogCallbackAsObservable_m1038802592(NULL /*static, unused*/, /*hidden argument*/NULL);
		Func_2_t2102879704 * L_1 = ((Sample04_ConvertFromUnityCallback_t3574096393_StaticFields*)Sample04_ConvertFromUnityCallback_t3574096393_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_2();
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_001d;
		}
	}
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)Sample04_ConvertFromUnityCallback_U3CAwakeU3Em__13_m4234092497_MethodInfo_var);
		Func_2_t2102879704 * L_3 = (Func_2_t2102879704 *)il2cpp_codegen_object_new(Func_2_t2102879704_il2cpp_TypeInfo_var);
		Func_2__ctor_m3611357982(L_3, NULL, L_2, /*hidden argument*/Func_2__ctor_m3611357982_MethodInfo_var);
		((Sample04_ConvertFromUnityCallback_t3574096393_StaticFields*)Sample04_ConvertFromUnityCallback_t3574096393_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache0_2(L_3);
		G_B2_0 = G_B1_0;
	}

IL_001d:
	{
		Func_2_t2102879704 * L_4 = ((Sample04_ConvertFromUnityCallback_t3574096393_StaticFields*)Sample04_ConvertFromUnityCallback_t3574096393_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_2();
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_5 = Observable_Where_TisLogCallback_t3235662730_m1342342965(NULL /*static, unused*/, G_B2_0, L_4, /*hidden argument*/Observable_Where_TisLogCallback_t3235662730_m1342342965_MethodInfo_var);
		Action_1_t3384115434 * L_6 = ((Sample04_ConvertFromUnityCallback_t3574096393_StaticFields*)Sample04_ConvertFromUnityCallback_t3574096393_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_3();
		G_B3_0 = L_5;
		if (L_6)
		{
			G_B4_0 = L_5;
			goto IL_003f;
		}
	}
	{
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)Sample04_ConvertFromUnityCallback_U3CAwakeU3Em__14_m2095151222_MethodInfo_var);
		Action_1_t3384115434 * L_8 = (Action_1_t3384115434 *)il2cpp_codegen_object_new(Action_1_t3384115434_il2cpp_TypeInfo_var);
		Action_1__ctor_m495595544(L_8, NULL, L_7, /*hidden argument*/Action_1__ctor_m495595544_MethodInfo_var);
		((Sample04_ConvertFromUnityCallback_t3574096393_StaticFields*)Sample04_ConvertFromUnityCallback_t3574096393_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache1_3(L_8);
		G_B4_0 = G_B3_0;
	}

IL_003f:
	{
		Action_1_t3384115434 * L_9 = ((Sample04_ConvertFromUnityCallback_t3574096393_StaticFields*)Sample04_ConvertFromUnityCallback_t3574096393_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_3();
		ObservableExtensions_Subscribe_TisLogCallback_t3235662730_m968574131(NULL /*static, unused*/, G_B4_0, L_9, /*hidden argument*/ObservableExtensions_Subscribe_TisLogCallback_t3235662730_m968574131_MethodInfo_var);
		Il2CppObject* L_10 = LogHelper_LogCallbackAsObservable_m1038802592(NULL /*static, unused*/, /*hidden argument*/NULL);
		Func_2_t2102879704 * L_11 = ((Sample04_ConvertFromUnityCallback_t3574096393_StaticFields*)Sample04_ConvertFromUnityCallback_t3574096393_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache2_4();
		G_B5_0 = L_10;
		if (L_11)
		{
			G_B6_0 = L_10;
			goto IL_0067;
		}
	}
	{
		IntPtr_t L_12;
		L_12.set_m_value_0((void*)(void*)Sample04_ConvertFromUnityCallback_U3CAwakeU3Em__15_m3816695891_MethodInfo_var);
		Func_2_t2102879704 * L_13 = (Func_2_t2102879704 *)il2cpp_codegen_object_new(Func_2_t2102879704_il2cpp_TypeInfo_var);
		Func_2__ctor_m3611357982(L_13, NULL, L_12, /*hidden argument*/Func_2__ctor_m3611357982_MethodInfo_var);
		((Sample04_ConvertFromUnityCallback_t3574096393_StaticFields*)Sample04_ConvertFromUnityCallback_t3574096393_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache2_4(L_13);
		G_B6_0 = G_B5_0;
	}

IL_0067:
	{
		Func_2_t2102879704 * L_14 = ((Sample04_ConvertFromUnityCallback_t3574096393_StaticFields*)Sample04_ConvertFromUnityCallback_t3574096393_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache2_4();
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_15 = Observable_Where_TisLogCallback_t3235662730_m1342342965(NULL /*static, unused*/, G_B6_0, L_14, /*hidden argument*/Observable_Where_TisLogCallback_t3235662730_m1342342965_MethodInfo_var);
		Action_1_t3384115434 * L_16 = ((Sample04_ConvertFromUnityCallback_t3574096393_StaticFields*)Sample04_ConvertFromUnityCallback_t3574096393_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache3_5();
		G_B7_0 = L_15;
		if (L_16)
		{
			G_B8_0 = L_15;
			goto IL_0089;
		}
	}
	{
		IntPtr_t L_17;
		L_17.set_m_value_0((void*)(void*)Sample04_ConvertFromUnityCallback_U3CAwakeU3Em__16_m1677754616_MethodInfo_var);
		Action_1_t3384115434 * L_18 = (Action_1_t3384115434 *)il2cpp_codegen_object_new(Action_1_t3384115434_il2cpp_TypeInfo_var);
		Action_1__ctor_m495595544(L_18, NULL, L_17, /*hidden argument*/Action_1__ctor_m495595544_MethodInfo_var);
		((Sample04_ConvertFromUnityCallback_t3574096393_StaticFields*)Sample04_ConvertFromUnityCallback_t3574096393_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache3_5(L_18);
		G_B8_0 = G_B7_0;
	}

IL_0089:
	{
		Action_1_t3384115434 * L_19 = ((Sample04_ConvertFromUnityCallback_t3574096393_StaticFields*)Sample04_ConvertFromUnityCallback_t3574096393_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache3_5();
		ObservableExtensions_Subscribe_TisLogCallback_t3235662730_m968574131(NULL /*static, unused*/, G_B8_0, L_19, /*hidden argument*/ObservableExtensions_Subscribe_TisLogCallback_t3235662730_m968574131_MethodInfo_var);
		return;
	}
}
// System.Boolean UniRx.Examples.Sample04_ConvertFromUnityCallback::<Awake>m__13(UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback)
extern "C"  bool Sample04_ConvertFromUnityCallback_U3CAwakeU3Em__13_m4234092497 (Il2CppObject * __this /* static, unused */, LogCallback_t3235662730 * ___x0, const MethodInfo* method)
{
	{
		LogCallback_t3235662730 * L_0 = ___x0;
		NullCheck(L_0);
		int32_t L_1 = L_0->get_LogType_2();
		return (bool)((((int32_t)L_1) == ((int32_t)2))? 1 : 0);
	}
}
// System.Void UniRx.Examples.Sample04_ConvertFromUnityCallback::<Awake>m__14(UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback)
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern const uint32_t Sample04_ConvertFromUnityCallback_U3CAwakeU3Em__14_m2095151222_MetadataUsageId;
extern "C"  void Sample04_ConvertFromUnityCallback_U3CAwakeU3Em__14_m2095151222 (Il2CppObject * __this /* static, unused */, LogCallback_t3235662730 * ___x0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Sample04_ConvertFromUnityCallback_U3CAwakeU3Em__14_m2095151222_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LogCallback_t3235662730 * L_0 = ___x0;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m3349710197(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UniRx.Examples.Sample04_ConvertFromUnityCallback::<Awake>m__15(UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback)
extern "C"  bool Sample04_ConvertFromUnityCallback_U3CAwakeU3Em__15_m3816695891 (Il2CppObject * __this /* static, unused */, LogCallback_t3235662730 * ___x0, const MethodInfo* method)
{
	{
		LogCallback_t3235662730 * L_0 = ___x0;
		NullCheck(L_0);
		int32_t L_1 = L_0->get_LogType_2();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void UniRx.Examples.Sample04_ConvertFromUnityCallback::<Awake>m__16(UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback)
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern const uint32_t Sample04_ConvertFromUnityCallback_U3CAwakeU3Em__16_m1677754616_MetadataUsageId;
extern "C"  void Sample04_ConvertFromUnityCallback_U3CAwakeU3Em__16_m1677754616 (Il2CppObject * __this /* static, unused */, LogCallback_t3235662730 * ___x0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Sample04_ConvertFromUnityCallback_U3CAwakeU3Em__16_m1677754616_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LogCallback_t3235662730 * L_0 = ___x0;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m3349710197(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback::.ctor()
extern "C"  void LogCallback__ctor_m1262722548 (LogCallback_t3235662730 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// UniRx.IObservable`1<UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback> UniRx.Examples.Sample04_ConvertFromUnityCallback/LogHelper::LogCallbackAsObservable()
extern Il2CppClass* LogHelper_t3627301874_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t4092726719_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t3384115435_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const MethodInfo* LogHelper_U3CLogCallbackAsObservableU3Em__17_m1808264757_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m2837513348_MethodInfo_var;
extern const MethodInfo* LogHelper_U3CLogCallbackAsObservableU3Em__18_m436926318_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m1925770228_MethodInfo_var;
extern const MethodInfo* LogHelper_U3CLogCallbackAsObservableU3Em__19_m2069730317_MethodInfo_var;
extern const MethodInfo* Observable_FromEvent_TisLogCallback_t3235662729_TisLogCallback_t3235662730_m2618871987_MethodInfo_var;
extern const uint32_t LogHelper_LogCallbackAsObservable_m1038802592_MetadataUsageId;
extern "C"  Il2CppObject* LogHelper_LogCallbackAsObservable_m1038802592 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LogHelper_LogCallbackAsObservable_m1038802592_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Func_2_t4092726719 * G_B4_0 = NULL;
	Func_2_t4092726719 * G_B3_0 = NULL;
	Action_1_t3384115435 * G_B6_0 = NULL;
	Func_2_t4092726719 * G_B6_1 = NULL;
	Action_1_t3384115435 * G_B5_0 = NULL;
	Func_2_t4092726719 * G_B5_1 = NULL;
	{
		Func_2_t4092726719 * L_0 = ((LogHelper_t3627301874_StaticFields*)LogHelper_t3627301874_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_0();
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)LogHelper_U3CLogCallbackAsObservableU3Em__17_m1808264757_MethodInfo_var);
		Func_2_t4092726719 * L_2 = (Func_2_t4092726719 *)il2cpp_codegen_object_new(Func_2_t4092726719_il2cpp_TypeInfo_var);
		Func_2__ctor_m2837513348(L_2, NULL, L_1, /*hidden argument*/Func_2__ctor_m2837513348_MethodInfo_var);
		((LogHelper_t3627301874_StaticFields*)LogHelper_t3627301874_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache0_0(L_2);
	}

IL_0018:
	{
		Func_2_t4092726719 * L_3 = ((LogHelper_t3627301874_StaticFields*)LogHelper_t3627301874_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_0();
		Action_1_t3384115435 * L_4 = ((LogHelper_t3627301874_StaticFields*)LogHelper_t3627301874_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_1();
		G_B3_0 = L_3;
		if (L_4)
		{
			G_B4_0 = L_3;
			goto IL_0035;
		}
	}
	{
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)LogHelper_U3CLogCallbackAsObservableU3Em__18_m436926318_MethodInfo_var);
		Action_1_t3384115435 * L_6 = (Action_1_t3384115435 *)il2cpp_codegen_object_new(Action_1_t3384115435_il2cpp_TypeInfo_var);
		Action_1__ctor_m1925770228(L_6, NULL, L_5, /*hidden argument*/Action_1__ctor_m1925770228_MethodInfo_var);
		((LogHelper_t3627301874_StaticFields*)LogHelper_t3627301874_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache1_1(L_6);
		G_B4_0 = G_B3_0;
	}

IL_0035:
	{
		Action_1_t3384115435 * L_7 = ((LogHelper_t3627301874_StaticFields*)LogHelper_t3627301874_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_1();
		Action_1_t3384115435 * L_8 = ((LogHelper_t3627301874_StaticFields*)LogHelper_t3627301874_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache2_2();
		G_B5_0 = L_7;
		G_B5_1 = G_B4_0;
		if (L_8)
		{
			G_B6_0 = L_7;
			G_B6_1 = G_B4_0;
			goto IL_0052;
		}
	}
	{
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)(void*)LogHelper_U3CLogCallbackAsObservableU3Em__19_m2069730317_MethodInfo_var);
		Action_1_t3384115435 * L_10 = (Action_1_t3384115435 *)il2cpp_codegen_object_new(Action_1_t3384115435_il2cpp_TypeInfo_var);
		Action_1__ctor_m1925770228(L_10, NULL, L_9, /*hidden argument*/Action_1__ctor_m1925770228_MethodInfo_var);
		((LogHelper_t3627301874_StaticFields*)LogHelper_t3627301874_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache2_2(L_10);
		G_B6_0 = G_B5_0;
		G_B6_1 = G_B5_1;
	}

IL_0052:
	{
		Action_1_t3384115435 * L_11 = ((LogHelper_t3627301874_StaticFields*)LogHelper_t3627301874_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache2_2();
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_12 = Observable_FromEvent_TisLogCallback_t3235662729_TisLogCallback_t3235662730_m2618871987(NULL /*static, unused*/, G_B6_1, G_B6_0, L_11, /*hidden argument*/Observable_FromEvent_TisLogCallback_t3235662729_TisLogCallback_t3235662730_m2618871987_MethodInfo_var);
		return L_12;
	}
}
// UnityEngine.Application/LogCallback UniRx.Examples.Sample04_ConvertFromUnityCallback/LogHelper::<LogCallbackAsObservable>m__17(System.Action`1<UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback>)
extern Il2CppClass* U3CLogCallbackAsObservableU3Ec__AnonStorey2F_t1039971231_il2cpp_TypeInfo_var;
extern Il2CppClass* LogCallback_t3235662729_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CLogCallbackAsObservableU3Ec__AnonStorey2F_U3CU3Em__1A_m1551801725_MethodInfo_var;
extern const uint32_t LogHelper_U3CLogCallbackAsObservableU3Em__17_m1808264757_MetadataUsageId;
extern "C"  LogCallback_t3235662729 * LogHelper_U3CLogCallbackAsObservableU3Em__17_m1808264757 (Il2CppObject * __this /* static, unused */, Action_1_t3384115434 * ___h0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LogHelper_U3CLogCallbackAsObservableU3Em__17_m1808264757_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CLogCallbackAsObservableU3Ec__AnonStorey2F_t1039971231 * V_0 = NULL;
	{
		U3CLogCallbackAsObservableU3Ec__AnonStorey2F_t1039971231 * L_0 = (U3CLogCallbackAsObservableU3Ec__AnonStorey2F_t1039971231 *)il2cpp_codegen_object_new(U3CLogCallbackAsObservableU3Ec__AnonStorey2F_t1039971231_il2cpp_TypeInfo_var);
		U3CLogCallbackAsObservableU3Ec__AnonStorey2F__ctor_m549354189(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CLogCallbackAsObservableU3Ec__AnonStorey2F_t1039971231 * L_1 = V_0;
		Action_1_t3384115434 * L_2 = ___h0;
		NullCheck(L_1);
		L_1->set_h_0(L_2);
		U3CLogCallbackAsObservableU3Ec__AnonStorey2F_t1039971231 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CLogCallbackAsObservableU3Ec__AnonStorey2F_U3CU3Em__1A_m1551801725_MethodInfo_var);
		LogCallback_t3235662729 * L_5 = (LogCallback_t3235662729 *)il2cpp_codegen_object_new(LogCallback_t3235662729_il2cpp_TypeInfo_var);
		LogCallback__ctor_m1350407504(L_5, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void UniRx.Examples.Sample04_ConvertFromUnityCallback/LogHelper::<LogCallbackAsObservable>m__18(UnityEngine.Application/LogCallback)
extern "C"  void LogHelper_U3CLogCallbackAsObservableU3Em__18_m436926318 (Il2CppObject * __this /* static, unused */, LogCallback_t3235662729 * ___h0, const MethodInfo* method)
{
	{
		LogCallback_t3235662729 * L_0 = ___h0;
		Application_add_logMessageReceived_m601763714(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Examples.Sample04_ConvertFromUnityCallback/LogHelper::<LogCallbackAsObservable>m__19(UnityEngine.Application/LogCallback)
extern "C"  void LogHelper_U3CLogCallbackAsObservableU3Em__19_m2069730317 (Il2CppObject * __this /* static, unused */, LogCallback_t3235662729 * ___h0, const MethodInfo* method)
{
	{
		LogCallback_t3235662729 * L_0 = ___h0;
		Application_remove_logMessageReceived_m293388825(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Examples.Sample04_ConvertFromUnityCallback/LogHelper/<LogCallbackAsObservable>c__AnonStorey2F::.ctor()
extern "C"  void U3CLogCallbackAsObservableU3Ec__AnonStorey2F__ctor_m549354189 (U3CLogCallbackAsObservableU3Ec__AnonStorey2F_t1039971231 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Examples.Sample04_ConvertFromUnityCallback/LogHelper/<LogCallbackAsObservable>c__AnonStorey2F::<>m__1A(System.String,System.String,UnityEngine.LogType)
extern Il2CppClass* LogCallback_t3235662730_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1783225827_MethodInfo_var;
extern const uint32_t U3CLogCallbackAsObservableU3Ec__AnonStorey2F_U3CU3Em__1A_m1551801725_MetadataUsageId;
extern "C"  void U3CLogCallbackAsObservableU3Ec__AnonStorey2F_U3CU3Em__1A_m1551801725 (U3CLogCallbackAsObservableU3Ec__AnonStorey2F_t1039971231 * __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CLogCallbackAsObservableU3Ec__AnonStorey2F_U3CU3Em__1A_m1551801725_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	LogCallback_t3235662730 * V_0 = NULL;
	{
		Action_1_t3384115434 * L_0 = __this->get_h_0();
		LogCallback_t3235662730 * L_1 = (LogCallback_t3235662730 *)il2cpp_codegen_object_new(LogCallback_t3235662730_il2cpp_TypeInfo_var);
		LogCallback__ctor_m1262722548(L_1, /*hidden argument*/NULL);
		V_0 = L_1;
		LogCallback_t3235662730 * L_2 = V_0;
		String_t* L_3 = ___condition0;
		NullCheck(L_2);
		L_2->set_Condition_0(L_3);
		LogCallback_t3235662730 * L_4 = V_0;
		String_t* L_5 = ___stackTrace1;
		NullCheck(L_4);
		L_4->set_StackTrace_1(L_5);
		LogCallback_t3235662730 * L_6 = V_0;
		int32_t L_7 = ___type2;
		NullCheck(L_6);
		L_6->set_LogType_2(L_7);
		LogCallback_t3235662730 * L_8 = V_0;
		NullCheck(L_0);
		Action_1_Invoke_m1783225827(L_0, L_8, /*hidden argument*/Action_1_Invoke_m1783225827_MethodInfo_var);
		return;
	}
}
// System.Void UniRx.Examples.Sample05_ConvertFromCoroutine::.ctor()
extern "C"  void Sample05_ConvertFromCoroutine__ctor_m989510065 (Sample05_ConvertFromCoroutine_t2367087270 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// UniRx.IObservable`1<System.String> UniRx.Examples.Sample05_ConvertFromCoroutine::GetWWW(System.String)
extern Il2CppClass* U3CGetWWWU3Ec__AnonStorey30_t1804159459_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_3_t212750549_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CGetWWWU3Ec__AnonStorey30_U3CU3Em__1B_m3149040067_MethodInfo_var;
extern const MethodInfo* Func_3__ctor_m1557367802_MethodInfo_var;
extern const MethodInfo* Observable_FromCoroutine_TisString_t_m3641581413_MethodInfo_var;
extern const uint32_t Sample05_ConvertFromCoroutine_GetWWW_m44874254_MetadataUsageId;
extern "C"  Il2CppObject* Sample05_ConvertFromCoroutine_GetWWW_m44874254 (Il2CppObject * __this /* static, unused */, String_t* ___url0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Sample05_ConvertFromCoroutine_GetWWW_m44874254_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetWWWU3Ec__AnonStorey30_t1804159459 * V_0 = NULL;
	{
		U3CGetWWWU3Ec__AnonStorey30_t1804159459 * L_0 = (U3CGetWWWU3Ec__AnonStorey30_t1804159459 *)il2cpp_codegen_object_new(U3CGetWWWU3Ec__AnonStorey30_t1804159459_il2cpp_TypeInfo_var);
		U3CGetWWWU3Ec__AnonStorey30__ctor_m2985900381(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetWWWU3Ec__AnonStorey30_t1804159459 * L_1 = V_0;
		String_t* L_2 = ___url0;
		NullCheck(L_1);
		L_1->set_url_0(L_2);
		U3CGetWWWU3Ec__AnonStorey30_t1804159459 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CGetWWWU3Ec__AnonStorey30_U3CU3Em__1B_m3149040067_MethodInfo_var);
		Func_3_t212750549 * L_5 = (Func_3_t212750549 *)il2cpp_codegen_object_new(Func_3_t212750549_il2cpp_TypeInfo_var);
		Func_3__ctor_m1557367802(L_5, L_3, L_4, /*hidden argument*/Func_3__ctor_m1557367802_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_6 = Observable_FromCoroutine_TisString_t_m3641581413(NULL /*static, unused*/, L_5, /*hidden argument*/Observable_FromCoroutine_TisString_t_m3641581413_MethodInfo_var);
		return L_6;
	}
}
// System.Collections.IEnumerator UniRx.Examples.Sample05_ConvertFromCoroutine::GetWWWCore(System.String,UniRx.IObserver`1<System.String>,UniRx.CancellationToken)
extern Il2CppClass* U3CGetWWWCoreU3Ec__Iterator6_t2275077895_il2cpp_TypeInfo_var;
extern const uint32_t Sample05_ConvertFromCoroutine_GetWWWCore_m3349104995_MetadataUsageId;
extern "C"  Il2CppObject * Sample05_ConvertFromCoroutine_GetWWWCore_m3349104995 (Il2CppObject * __this /* static, unused */, String_t* ___url0, Il2CppObject* ___observer1, CancellationToken_t1439151560  ___cancellationToken2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Sample05_ConvertFromCoroutine_GetWWWCore_m3349104995_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetWWWCoreU3Ec__Iterator6_t2275077895 * V_0 = NULL;
	{
		U3CGetWWWCoreU3Ec__Iterator6_t2275077895 * L_0 = (U3CGetWWWCoreU3Ec__Iterator6_t2275077895 *)il2cpp_codegen_object_new(U3CGetWWWCoreU3Ec__Iterator6_t2275077895_il2cpp_TypeInfo_var);
		U3CGetWWWCoreU3Ec__Iterator6__ctor_m350843295(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetWWWCoreU3Ec__Iterator6_t2275077895 * L_1 = V_0;
		String_t* L_2 = ___url0;
		NullCheck(L_1);
		L_1->set_url_0(L_2);
		U3CGetWWWCoreU3Ec__Iterator6_t2275077895 * L_3 = V_0;
		CancellationToken_t1439151560  L_4 = ___cancellationToken2;
		NullCheck(L_3);
		L_3->set_cancellationToken_2(L_4);
		U3CGetWWWCoreU3Ec__Iterator6_t2275077895 * L_5 = V_0;
		Il2CppObject* L_6 = ___observer1;
		NullCheck(L_5);
		L_5->set_observer_3(L_6);
		U3CGetWWWCoreU3Ec__Iterator6_t2275077895 * L_7 = V_0;
		String_t* L_8 = ___url0;
		NullCheck(L_7);
		L_7->set_U3CU24U3Eurl_6(L_8);
		U3CGetWWWCoreU3Ec__Iterator6_t2275077895 * L_9 = V_0;
		CancellationToken_t1439151560  L_10 = ___cancellationToken2;
		NullCheck(L_9);
		L_9->set_U3CU24U3EcancellationToken_7(L_10);
		U3CGetWWWCoreU3Ec__Iterator6_t2275077895 * L_11 = V_0;
		Il2CppObject* L_12 = ___observer1;
		NullCheck(L_11);
		L_11->set_U3CU24U3Eobserver_8(L_12);
		U3CGetWWWCoreU3Ec__Iterator6_t2275077895 * L_13 = V_0;
		return L_13;
	}
}
// System.Void UniRx.Examples.Sample05_ConvertFromCoroutine/<GetWWW>c__AnonStorey30::.ctor()
extern "C"  void U3CGetWWWU3Ec__AnonStorey30__ctor_m2985900381 (U3CGetWWWU3Ec__AnonStorey30_t1804159459 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UniRx.Examples.Sample05_ConvertFromCoroutine/<GetWWW>c__AnonStorey30::<>m__1B(UniRx.IObserver`1<System.String>,UniRx.CancellationToken)
extern "C"  Il2CppObject * U3CGetWWWU3Ec__AnonStorey30_U3CU3Em__1B_m3149040067 (U3CGetWWWU3Ec__AnonStorey30_t1804159459 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellationToken1, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_url_0();
		Il2CppObject* L_1 = ___observer0;
		CancellationToken_t1439151560  L_2 = ___cancellationToken1;
		Il2CppObject * L_3 = Sample05_ConvertFromCoroutine_GetWWWCore_m3349104995(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void UniRx.Examples.Sample05_ConvertFromCoroutine/<GetWWWCore>c__Iterator6::.ctor()
extern "C"  void U3CGetWWWCoreU3Ec__Iterator6__ctor_m350843295 (U3CGetWWWCoreU3Ec__Iterator6_t2275077895 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UniRx.Examples.Sample05_ConvertFromCoroutine/<GetWWWCore>c__Iterator6::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGetWWWCoreU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2584035795 (U3CGetWWWCoreU3Ec__Iterator6_t2275077895 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Object UniRx.Examples.Sample05_ConvertFromCoroutine/<GetWWWCore>c__Iterator6::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetWWWCoreU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m1621314407 (U3CGetWWWCoreU3Ec__Iterator6_t2275077895 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Boolean UniRx.Examples.Sample05_ConvertFromCoroutine/<GetWWWCore>c__Iterator6::MoveNext()
extern Il2CppClass* WWW_t1522972100_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppClass* IObserver_1_t3180487805_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetWWWCoreU3Ec__Iterator6_MoveNext_m2321053965_MetadataUsageId;
extern "C"  bool U3CGetWWWCoreU3Ec__Iterator6_MoveNext_m2321053965 (U3CGetWWWCoreU3Ec__Iterator6_t2275077895 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetWWWCoreU3Ec__Iterator6_MoveNext_m2321053965_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_004a;
		}
	}
	{
		goto IL_00d7;
	}

IL_0021:
	{
		String_t* L_2 = __this->get_url_0();
		WWW_t1522972100 * L_3 = (WWW_t1522972100 *)il2cpp_codegen_object_new(WWW_t1522972100_il2cpp_TypeInfo_var);
		WWW__ctor_m1985874080(L_3, L_2, /*hidden argument*/NULL);
		__this->set_U3CwwwU3E__0_1(L_3);
		goto IL_004a;
	}

IL_0037:
	{
		__this->set_U24current_5(NULL);
		__this->set_U24PC_4(1);
		goto IL_00d9;
	}

IL_004a:
	{
		WWW_t1522972100 * L_4 = __this->get_U3CwwwU3E__0_1();
		NullCheck(L_4);
		bool L_5 = WWW_get_isDone_m634060017(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_006a;
		}
	}
	{
		CancellationToken_t1439151560 * L_6 = __this->get_address_of_cancellationToken_2();
		bool L_7 = CancellationToken_get_IsCancellationRequested_m1021497867(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0037;
		}
	}

IL_006a:
	{
		CancellationToken_t1439151560 * L_8 = __this->get_address_of_cancellationToken_2();
		bool L_9 = CancellationToken_get_IsCancellationRequested_m1021497867(L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_007f;
		}
	}
	{
		goto IL_00d7;
	}

IL_007f:
	{
		WWW_t1522972100 * L_10 = __this->get_U3CwwwU3E__0_1();
		NullCheck(L_10);
		String_t* L_11 = WWW_get_error_m1787423074(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_00af;
		}
	}
	{
		Il2CppObject* L_12 = __this->get_observer_3();
		WWW_t1522972100 * L_13 = __this->get_U3CwwwU3E__0_1();
		NullCheck(L_13);
		String_t* L_14 = WWW_get_error_m1787423074(L_13, /*hidden argument*/NULL);
		Exception_t1967233988 * L_15 = (Exception_t1967233988 *)il2cpp_codegen_object_new(Exception_t1967233988_il2cpp_TypeInfo_var);
		Exception__ctor_m3870771296(L_15, L_14, /*hidden argument*/NULL);
		NullCheck(L_12);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.String>::OnError(System.Exception) */, IObserver_1_t3180487805_il2cpp_TypeInfo_var, L_12, L_15);
		goto IL_00d0;
	}

IL_00af:
	{
		Il2CppObject* L_16 = __this->get_observer_3();
		WWW_t1522972100 * L_17 = __this->get_U3CwwwU3E__0_1();
		NullCheck(L_17);
		String_t* L_18 = WWW_get_text_m4216049525(L_17, /*hidden argument*/NULL);
		NullCheck(L_16);
		InterfaceActionInvoker1< String_t* >::Invoke(2 /* System.Void UniRx.IObserver`1<System.String>::OnNext(T) */, IObserver_1_t3180487805_il2cpp_TypeInfo_var, L_16, L_18);
		Il2CppObject* L_19 = __this->get_observer_3();
		NullCheck(L_19);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.String>::OnCompleted() */, IObserver_1_t3180487805_il2cpp_TypeInfo_var, L_19);
	}

IL_00d0:
	{
		__this->set_U24PC_4((-1));
	}

IL_00d7:
	{
		return (bool)0;
	}

IL_00d9:
	{
		return (bool)1;
	}
	// Dead block : IL_00db: ldloc.1
}
// System.Void UniRx.Examples.Sample05_ConvertFromCoroutine/<GetWWWCore>c__Iterator6::Dispose()
extern "C"  void U3CGetWWWCoreU3Ec__Iterator6_Dispose_m2051240476 (U3CGetWWWCoreU3Ec__Iterator6_t2275077895 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void UniRx.Examples.Sample05_ConvertFromCoroutine/<GetWWWCore>c__Iterator6::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetWWWCoreU3Ec__Iterator6_Reset_m2292243532_MetadataUsageId;
extern "C"  void U3CGetWWWCoreU3Ec__Iterator6_Reset_m2292243532 (U3CGetWWWCoreU3Ec__Iterator6_t2275077895 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetWWWCoreU3Ec__Iterator6_Reset_m2292243532_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UniRx.Examples.Sample06_ConvertToCoroutine::.ctor()
extern "C"  void Sample06_ConvertToCoroutine__ctor_m1767416259 (Sample06_ConvertToCoroutine_t617332884 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m350695606(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Examples.Sample06_ConvertToCoroutine::Start()
extern "C"  void Sample06_ConvertToCoroutine_Start_m714554051 (Sample06_ConvertToCoroutine_t617332884 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Sample06_ConvertToCoroutine_ComplexCoroutineTest_m4082061963(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2942381565(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UniRx.Examples.Sample06_ConvertToCoroutine::ComplexCoroutineTest()
extern Il2CppClass* U3CComplexCoroutineTestU3Ec__Iterator7_t4070488366_il2cpp_TypeInfo_var;
extern const uint32_t Sample06_ConvertToCoroutine_ComplexCoroutineTest_m4082061963_MetadataUsageId;
extern "C"  Il2CppObject * Sample06_ConvertToCoroutine_ComplexCoroutineTest_m4082061963 (Sample06_ConvertToCoroutine_t617332884 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Sample06_ConvertToCoroutine_ComplexCoroutineTest_m4082061963_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CComplexCoroutineTestU3Ec__Iterator7_t4070488366 * V_0 = NULL;
	{
		U3CComplexCoroutineTestU3Ec__Iterator7_t4070488366 * L_0 = (U3CComplexCoroutineTestU3Ec__Iterator7_t4070488366 *)il2cpp_codegen_object_new(U3CComplexCoroutineTestU3Ec__Iterator7_t4070488366_il2cpp_TypeInfo_var);
		U3CComplexCoroutineTestU3Ec__Iterator7__ctor_m1602681414(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CComplexCoroutineTestU3Ec__Iterator7_t4070488366 * L_1 = V_0;
		return L_1;
	}
}
// System.Collections.IEnumerator UniRx.Examples.Sample06_ConvertToCoroutine::LazyTaskTest()
extern Il2CppClass* U3CLazyTaskTestU3Ec__Iterator8_t1612341822_il2cpp_TypeInfo_var;
extern const uint32_t Sample06_ConvertToCoroutine_LazyTaskTest_m122679516_MetadataUsageId;
extern "C"  Il2CppObject * Sample06_ConvertToCoroutine_LazyTaskTest_m122679516 (Sample06_ConvertToCoroutine_t617332884 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Sample06_ConvertToCoroutine_LazyTaskTest_m122679516_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CLazyTaskTestU3Ec__Iterator8_t1612341822 * V_0 = NULL;
	{
		U3CLazyTaskTestU3Ec__Iterator8_t1612341822 * L_0 = (U3CLazyTaskTestU3Ec__Iterator8_t1612341822 *)il2cpp_codegen_object_new(U3CLazyTaskTestU3Ec__Iterator8_t1612341822_il2cpp_TypeInfo_var);
		U3CLazyTaskTestU3Ec__Iterator8__ctor_m244270134(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CLazyTaskTestU3Ec__Iterator8_t1612341822 * L_1 = V_0;
		return L_1;
	}
}
// System.Collections.IEnumerator UniRx.Examples.Sample06_ConvertToCoroutine::TestNewCustomYieldInstruction()
extern Il2CppClass* U3CTestNewCustomYieldInstructionU3Ec__Iterator9_t2930300994_il2cpp_TypeInfo_var;
extern const uint32_t Sample06_ConvertToCoroutine_TestNewCustomYieldInstruction_m562664785_MetadataUsageId;
extern "C"  Il2CppObject * Sample06_ConvertToCoroutine_TestNewCustomYieldInstruction_m562664785 (Sample06_ConvertToCoroutine_t617332884 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Sample06_ConvertToCoroutine_TestNewCustomYieldInstruction_m562664785_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CTestNewCustomYieldInstructionU3Ec__Iterator9_t2930300994 * V_0 = NULL;
	{
		U3CTestNewCustomYieldInstructionU3Ec__Iterator9_t2930300994 * L_0 = (U3CTestNewCustomYieldInstructionU3Ec__Iterator9_t2930300994 *)il2cpp_codegen_object_new(U3CTestNewCustomYieldInstructionU3Ec__Iterator9_t2930300994_il2cpp_TypeInfo_var);
		U3CTestNewCustomYieldInstructionU3Ec__Iterator9__ctor_m1174907504(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CTestNewCustomYieldInstructionU3Ec__Iterator9_t2930300994 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CTestNewCustomYieldInstructionU3Ec__Iterator9_t2930300994 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UniRx.Examples.Sample06_ConvertToCoroutine/<ComplexCoroutineTest>c__Iterator7::.ctor()
extern "C"  void U3CComplexCoroutineTestU3Ec__Iterator7__ctor_m1602681414 (U3CComplexCoroutineTestU3Ec__Iterator7_t4070488366 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UniRx.Examples.Sample06_ConvertToCoroutine/<ComplexCoroutineTest>c__Iterator7::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CComplexCoroutineTestU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2109536396 (U3CComplexCoroutineTestU3Ec__Iterator7_t4070488366 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object UniRx.Examples.Sample06_ConvertToCoroutine/<ComplexCoroutineTest>c__Iterator7::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CComplexCoroutineTestU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m4280914976 (U3CComplexCoroutineTestU3Ec__Iterator7_t4070488366 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean UniRx.Examples.Sample06_ConvertToCoroutine/<ComplexCoroutineTest>c__Iterator7::MoveNext()
extern Il2CppClass* WaitForSeconds_t1291133240_il2cpp_TypeInfo_var;
extern Il2CppClass* Observable_t258071701_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t2995867492_il2cpp_TypeInfo_var;
extern Il2CppClass* CancellationToken_t1439151560_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CComplexCoroutineTestU3Ec__Iterator7_U3CU3Em__1C_m2281343250_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m1091880516_MethodInfo_var;
extern const MethodInfo* Observable_StartAsCoroutine_TisInt32_t2847414787_m1205650364_MethodInfo_var;
extern const MethodInfo* Observable_Return_TisInt32_t2847414787_m3069752271_MethodInfo_var;
extern const MethodInfo* U3CComplexCoroutineTestU3Ec__Iterator7_U3CU3Em__1D_m3788895059_MethodInfo_var;
extern const uint32_t U3CComplexCoroutineTestU3Ec__Iterator7_MoveNext_m1892660998_MetadataUsageId;
extern "C"  bool U3CComplexCoroutineTestU3Ec__Iterator7_MoveNext_m1892660998 (U3CComplexCoroutineTestU3Ec__Iterator7_t4070488366 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CComplexCoroutineTestU3Ec__Iterator7_MoveNext_m1892660998_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	CancellationToken_t1439151560  V_1;
	memset(&V_1, 0, sizeof(V_1));
	CancellationToken_t1439151560  V_2;
	memset(&V_2, 0, sizeof(V_2));
	bool V_3 = false;
	{
		int32_t L_0 = __this->get_U24PC_1();
		V_0 = L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_002d;
		}
		if (L_1 == 1)
		{
			goto IL_0049;
		}
		if (L_1 == 2)
		{
			goto IL_0084;
		}
		if (L_1 == 3)
		{
			goto IL_00b0;
		}
		if (L_1 == 4)
		{
			goto IL_00e3;
		}
	}
	{
		goto IL_00fa;
	}

IL_002d:
	{
		WaitForSeconds_t1291133240 * L_2 = (WaitForSeconds_t1291133240 *)il2cpp_codegen_object_new(WaitForSeconds_t1291133240_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_2, (1.0f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_2);
		__this->set_U24PC_1(1);
		goto IL_00fc;
	}

IL_0049:
	{
		__this->set_U3CvU3E__0_0(0);
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_3 = Observable_Range_m751165906(NULL /*static, unused*/, 1, ((int32_t)10), /*hidden argument*/NULL);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CComplexCoroutineTestU3Ec__Iterator7_U3CU3Em__1C_m2281343250_MethodInfo_var);
		Action_1_t2995867492 * L_5 = (Action_1_t2995867492 *)il2cpp_codegen_object_new(Action_1_t2995867492_il2cpp_TypeInfo_var);
		Action_1__ctor_m1091880516(L_5, __this, L_4, /*hidden argument*/Action_1__ctor_m1091880516_MethodInfo_var);
		Initobj (CancellationToken_t1439151560_il2cpp_TypeInfo_var, (&V_1));
		CancellationToken_t1439151560  L_6 = V_1;
		Coroutine_t2246592261 * L_7 = Observable_StartAsCoroutine_TisInt32_t2847414787_m1205650364(NULL /*static, unused*/, L_3, L_5, L_6, /*hidden argument*/Observable_StartAsCoroutine_TisInt32_t2847414787_m1205650364_MethodInfo_var);
		__this->set_U24current_2(L_7);
		__this->set_U24PC_1(2);
		goto IL_00fc;
	}

IL_0084:
	{
		int32_t L_8 = __this->get_U3CvU3E__0_0();
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_9);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m3349710197(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		WaitForSeconds_t1291133240 * L_11 = (WaitForSeconds_t1291133240 *)il2cpp_codegen_object_new(WaitForSeconds_t1291133240_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_11, (3.0f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_11);
		__this->set_U24PC_1(3);
		goto IL_00fc;
	}

IL_00b0:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Observable_t258071701_il2cpp_TypeInfo_var);
		Il2CppObject* L_12 = Observable_Return_TisInt32_t2847414787_m3069752271(NULL /*static, unused*/, ((int32_t)100), /*hidden argument*/Observable_Return_TisInt32_t2847414787_m3069752271_MethodInfo_var);
		IntPtr_t L_13;
		L_13.set_m_value_0((void*)(void*)U3CComplexCoroutineTestU3Ec__Iterator7_U3CU3Em__1D_m3788895059_MethodInfo_var);
		Action_1_t2995867492 * L_14 = (Action_1_t2995867492 *)il2cpp_codegen_object_new(Action_1_t2995867492_il2cpp_TypeInfo_var);
		Action_1__ctor_m1091880516(L_14, __this, L_13, /*hidden argument*/Action_1__ctor_m1091880516_MethodInfo_var);
		Initobj (CancellationToken_t1439151560_il2cpp_TypeInfo_var, (&V_2));
		CancellationToken_t1439151560  L_15 = V_2;
		Coroutine_t2246592261 * L_16 = Observable_StartAsCoroutine_TisInt32_t2847414787_m1205650364(NULL /*static, unused*/, L_12, L_14, L_15, /*hidden argument*/Observable_StartAsCoroutine_TisInt32_t2847414787_m1205650364_MethodInfo_var);
		__this->set_U24current_2(L_16);
		__this->set_U24PC_1(4);
		goto IL_00fc;
	}

IL_00e3:
	{
		int32_t L_17 = __this->get_U3CvU3E__0_0();
		int32_t L_18 = L_17;
		Il2CppObject * L_19 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_18);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m3349710197(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		__this->set_U24PC_1((-1));
	}

IL_00fa:
	{
		return (bool)0;
	}

IL_00fc:
	{
		return (bool)1;
	}
	// Dead block : IL_00fe: ldloc.3
}
// System.Void UniRx.Examples.Sample06_ConvertToCoroutine/<ComplexCoroutineTest>c__Iterator7::Dispose()
extern "C"  void U3CComplexCoroutineTestU3Ec__Iterator7_Dispose_m2476829955 (U3CComplexCoroutineTestU3Ec__Iterator7_t4070488366 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void UniRx.Examples.Sample06_ConvertToCoroutine/<ComplexCoroutineTest>c__Iterator7::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CComplexCoroutineTestU3Ec__Iterator7_Reset_m3544081651_MetadataUsageId;
extern "C"  void U3CComplexCoroutineTestU3Ec__Iterator7_Reset_m3544081651 (U3CComplexCoroutineTestU3Ec__Iterator7_t4070488366 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CComplexCoroutineTestU3Ec__Iterator7_Reset_m3544081651_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UniRx.Examples.Sample06_ConvertToCoroutine/<ComplexCoroutineTest>c__Iterator7::<>m__1C(System.Int32)
extern "C"  void U3CComplexCoroutineTestU3Ec__Iterator7_U3CU3Em__1C_m2281343250 (U3CComplexCoroutineTestU3Ec__Iterator7_t4070488366 * __this, int32_t ___x0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___x0;
		__this->set_U3CvU3E__0_0(L_0);
		return;
	}
}
// System.Void UniRx.Examples.Sample06_ConvertToCoroutine/<ComplexCoroutineTest>c__Iterator7::<>m__1D(System.Int32)
extern "C"  void U3CComplexCoroutineTestU3Ec__Iterator7_U3CU3Em__1D_m3788895059 (U3CComplexCoroutineTestU3Ec__Iterator7_t4070488366 * __this, int32_t ___x0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___x0;
		__this->set_U3CvU3E__0_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
