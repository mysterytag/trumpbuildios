﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.UnityEqualityComparer/Vector4EqualityComparer
struct Vector4EqualityComparer_t4075241624;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector43525329790.h"

// System.Void UniRx.UnityEqualityComparer/Vector4EqualityComparer::.ctor()
extern "C"  void Vector4EqualityComparer__ctor_m4103827274 (Vector4EqualityComparer_t4075241624 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.UnityEqualityComparer/Vector4EqualityComparer::Equals(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C"  bool Vector4EqualityComparer_Equals_m2581370029 (Vector4EqualityComparer_t4075241624 * __this, Vector4_t3525329790  ___self0, Vector4_t3525329790  ___vector1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UniRx.UnityEqualityComparer/Vector4EqualityComparer::GetHashCode(UnityEngine.Vector4)
extern "C"  int32_t Vector4EqualityComparer_GetHashCode_m1835083433 (Vector4EqualityComparer_t4075241624 * __this, Vector4_t3525329790  ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
