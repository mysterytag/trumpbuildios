﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<Zenject.ITickable>
struct List_1_t1586470990;
// System.Collections.Generic.List`1<Zenject.IFixedTickable>
struct List_1_t4125350576;
// System.Collections.Generic.List`1<Zenject.ILateTickable>
struct List_1_t4262769812;
// System.Collections.Generic.List`1<ModestTree.Util.Tuple`2<System.Type,System.Int32>>
struct List_1_t221540724;
// Zenject.SingletonInstanceHelper
struct SingletonInstanceHelper_t833281251;
// Zenject.TaskUpdater`1<Zenject.ITickable>
struct TaskUpdater_1_t1658401268;
// Zenject.TaskUpdater`1<Zenject.IFixedTickable>
struct TaskUpdater_1_t4197280854;
// Zenject.TaskUpdater`1<Zenject.ILateTickable>
struct TaskUpdater_1_t39732794;
// System.Func`2<Zenject.ITickable,System.Type>
struct Func_2_t1456098478;
// System.Func`2<ModestTree.Util.Tuple`2<System.Type,System.Int32>,System.Type>
struct Func_2_t1899971632;
// System.Func`2<ModestTree.Util.Tuple`2<System.Type,System.Int32>,System.Int32>
struct Func_2_t1968156484;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TickableManager
struct  TickableManager_t278367883  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<Zenject.ITickable> Zenject.TickableManager::_tickables
	List_1_t1586470990 * ____tickables_0;
	// System.Collections.Generic.List`1<Zenject.IFixedTickable> Zenject.TickableManager::_fixedTickables
	List_1_t4125350576 * ____fixedTickables_1;
	// System.Collections.Generic.List`1<Zenject.ILateTickable> Zenject.TickableManager::_lateTickables
	List_1_t4262769812 * ____lateTickables_2;
	// System.Collections.Generic.List`1<ModestTree.Util.Tuple`2<System.Type,System.Int32>> Zenject.TickableManager::_priorities
	List_1_t221540724 * ____priorities_3;
	// System.Collections.Generic.List`1<ModestTree.Util.Tuple`2<System.Type,System.Int32>> Zenject.TickableManager::_fixedPriorities
	List_1_t221540724 * ____fixedPriorities_4;
	// System.Collections.Generic.List`1<ModestTree.Util.Tuple`2<System.Type,System.Int32>> Zenject.TickableManager::_latePriorities
	List_1_t221540724 * ____latePriorities_5;
	// Zenject.SingletonInstanceHelper Zenject.TickableManager::_singletonInstanceHelper
	SingletonInstanceHelper_t833281251 * ____singletonInstanceHelper_6;
	// Zenject.TaskUpdater`1<Zenject.ITickable> Zenject.TickableManager::_updater
	TaskUpdater_1_t1658401268 * ____updater_7;
	// Zenject.TaskUpdater`1<Zenject.IFixedTickable> Zenject.TickableManager::_fixedUpdater
	TaskUpdater_1_t4197280854 * ____fixedUpdater_8;
	// Zenject.TaskUpdater`1<Zenject.ILateTickable> Zenject.TickableManager::_lateUpdater
	TaskUpdater_1_t39732794 * ____lateUpdater_9;
	// System.Boolean Zenject.TickableManager::_warnForMissing
	bool ____warnForMissing_10;

public:
	inline static int32_t get_offset_of__tickables_0() { return static_cast<int32_t>(offsetof(TickableManager_t278367883, ____tickables_0)); }
	inline List_1_t1586470990 * get__tickables_0() const { return ____tickables_0; }
	inline List_1_t1586470990 ** get_address_of__tickables_0() { return &____tickables_0; }
	inline void set__tickables_0(List_1_t1586470990 * value)
	{
		____tickables_0 = value;
		Il2CppCodeGenWriteBarrier(&____tickables_0, value);
	}

	inline static int32_t get_offset_of__fixedTickables_1() { return static_cast<int32_t>(offsetof(TickableManager_t278367883, ____fixedTickables_1)); }
	inline List_1_t4125350576 * get__fixedTickables_1() const { return ____fixedTickables_1; }
	inline List_1_t4125350576 ** get_address_of__fixedTickables_1() { return &____fixedTickables_1; }
	inline void set__fixedTickables_1(List_1_t4125350576 * value)
	{
		____fixedTickables_1 = value;
		Il2CppCodeGenWriteBarrier(&____fixedTickables_1, value);
	}

	inline static int32_t get_offset_of__lateTickables_2() { return static_cast<int32_t>(offsetof(TickableManager_t278367883, ____lateTickables_2)); }
	inline List_1_t4262769812 * get__lateTickables_2() const { return ____lateTickables_2; }
	inline List_1_t4262769812 ** get_address_of__lateTickables_2() { return &____lateTickables_2; }
	inline void set__lateTickables_2(List_1_t4262769812 * value)
	{
		____lateTickables_2 = value;
		Il2CppCodeGenWriteBarrier(&____lateTickables_2, value);
	}

	inline static int32_t get_offset_of__priorities_3() { return static_cast<int32_t>(offsetof(TickableManager_t278367883, ____priorities_3)); }
	inline List_1_t221540724 * get__priorities_3() const { return ____priorities_3; }
	inline List_1_t221540724 ** get_address_of__priorities_3() { return &____priorities_3; }
	inline void set__priorities_3(List_1_t221540724 * value)
	{
		____priorities_3 = value;
		Il2CppCodeGenWriteBarrier(&____priorities_3, value);
	}

	inline static int32_t get_offset_of__fixedPriorities_4() { return static_cast<int32_t>(offsetof(TickableManager_t278367883, ____fixedPriorities_4)); }
	inline List_1_t221540724 * get__fixedPriorities_4() const { return ____fixedPriorities_4; }
	inline List_1_t221540724 ** get_address_of__fixedPriorities_4() { return &____fixedPriorities_4; }
	inline void set__fixedPriorities_4(List_1_t221540724 * value)
	{
		____fixedPriorities_4 = value;
		Il2CppCodeGenWriteBarrier(&____fixedPriorities_4, value);
	}

	inline static int32_t get_offset_of__latePriorities_5() { return static_cast<int32_t>(offsetof(TickableManager_t278367883, ____latePriorities_5)); }
	inline List_1_t221540724 * get__latePriorities_5() const { return ____latePriorities_5; }
	inline List_1_t221540724 ** get_address_of__latePriorities_5() { return &____latePriorities_5; }
	inline void set__latePriorities_5(List_1_t221540724 * value)
	{
		____latePriorities_5 = value;
		Il2CppCodeGenWriteBarrier(&____latePriorities_5, value);
	}

	inline static int32_t get_offset_of__singletonInstanceHelper_6() { return static_cast<int32_t>(offsetof(TickableManager_t278367883, ____singletonInstanceHelper_6)); }
	inline SingletonInstanceHelper_t833281251 * get__singletonInstanceHelper_6() const { return ____singletonInstanceHelper_6; }
	inline SingletonInstanceHelper_t833281251 ** get_address_of__singletonInstanceHelper_6() { return &____singletonInstanceHelper_6; }
	inline void set__singletonInstanceHelper_6(SingletonInstanceHelper_t833281251 * value)
	{
		____singletonInstanceHelper_6 = value;
		Il2CppCodeGenWriteBarrier(&____singletonInstanceHelper_6, value);
	}

	inline static int32_t get_offset_of__updater_7() { return static_cast<int32_t>(offsetof(TickableManager_t278367883, ____updater_7)); }
	inline TaskUpdater_1_t1658401268 * get__updater_7() const { return ____updater_7; }
	inline TaskUpdater_1_t1658401268 ** get_address_of__updater_7() { return &____updater_7; }
	inline void set__updater_7(TaskUpdater_1_t1658401268 * value)
	{
		____updater_7 = value;
		Il2CppCodeGenWriteBarrier(&____updater_7, value);
	}

	inline static int32_t get_offset_of__fixedUpdater_8() { return static_cast<int32_t>(offsetof(TickableManager_t278367883, ____fixedUpdater_8)); }
	inline TaskUpdater_1_t4197280854 * get__fixedUpdater_8() const { return ____fixedUpdater_8; }
	inline TaskUpdater_1_t4197280854 ** get_address_of__fixedUpdater_8() { return &____fixedUpdater_8; }
	inline void set__fixedUpdater_8(TaskUpdater_1_t4197280854 * value)
	{
		____fixedUpdater_8 = value;
		Il2CppCodeGenWriteBarrier(&____fixedUpdater_8, value);
	}

	inline static int32_t get_offset_of__lateUpdater_9() { return static_cast<int32_t>(offsetof(TickableManager_t278367883, ____lateUpdater_9)); }
	inline TaskUpdater_1_t39732794 * get__lateUpdater_9() const { return ____lateUpdater_9; }
	inline TaskUpdater_1_t39732794 ** get_address_of__lateUpdater_9() { return &____lateUpdater_9; }
	inline void set__lateUpdater_9(TaskUpdater_1_t39732794 * value)
	{
		____lateUpdater_9 = value;
		Il2CppCodeGenWriteBarrier(&____lateUpdater_9, value);
	}

	inline static int32_t get_offset_of__warnForMissing_10() { return static_cast<int32_t>(offsetof(TickableManager_t278367883, ____warnForMissing_10)); }
	inline bool get__warnForMissing_10() const { return ____warnForMissing_10; }
	inline bool* get_address_of__warnForMissing_10() { return &____warnForMissing_10; }
	inline void set__warnForMissing_10(bool value)
	{
		____warnForMissing_10 = value;
	}
};

struct TickableManager_t278367883_StaticFields
{
public:
	// System.Func`2<Zenject.ITickable,System.Type> Zenject.TickableManager::<>f__am$cacheB
	Func_2_t1456098478 * ___U3CU3Ef__amU24cacheB_11;
	// System.Func`2<ModestTree.Util.Tuple`2<System.Type,System.Int32>,System.Type> Zenject.TickableManager::<>f__am$cacheC
	Func_2_t1899971632 * ___U3CU3Ef__amU24cacheC_12;
	// System.Func`2<ModestTree.Util.Tuple`2<System.Type,System.Int32>,System.Int32> Zenject.TickableManager::<>f__am$cacheD
	Func_2_t1968156484 * ___U3CU3Ef__amU24cacheD_13;
	// System.Func`2<ModestTree.Util.Tuple`2<System.Type,System.Int32>,System.Type> Zenject.TickableManager::<>f__am$cacheE
	Func_2_t1899971632 * ___U3CU3Ef__amU24cacheE_14;
	// System.Func`2<ModestTree.Util.Tuple`2<System.Type,System.Int32>,System.Int32> Zenject.TickableManager::<>f__am$cacheF
	Func_2_t1968156484 * ___U3CU3Ef__amU24cacheF_15;
	// System.Func`2<ModestTree.Util.Tuple`2<System.Type,System.Int32>,System.Type> Zenject.TickableManager::<>f__am$cache10
	Func_2_t1899971632 * ___U3CU3Ef__amU24cache10_16;
	// System.Func`2<ModestTree.Util.Tuple`2<System.Type,System.Int32>,System.Int32> Zenject.TickableManager::<>f__am$cache11
	Func_2_t1968156484 * ___U3CU3Ef__amU24cache11_17;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_11() { return static_cast<int32_t>(offsetof(TickableManager_t278367883_StaticFields, ___U3CU3Ef__amU24cacheB_11)); }
	inline Func_2_t1456098478 * get_U3CU3Ef__amU24cacheB_11() const { return ___U3CU3Ef__amU24cacheB_11; }
	inline Func_2_t1456098478 ** get_address_of_U3CU3Ef__amU24cacheB_11() { return &___U3CU3Ef__amU24cacheB_11; }
	inline void set_U3CU3Ef__amU24cacheB_11(Func_2_t1456098478 * value)
	{
		___U3CU3Ef__amU24cacheB_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheB_11, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheC_12() { return static_cast<int32_t>(offsetof(TickableManager_t278367883_StaticFields, ___U3CU3Ef__amU24cacheC_12)); }
	inline Func_2_t1899971632 * get_U3CU3Ef__amU24cacheC_12() const { return ___U3CU3Ef__amU24cacheC_12; }
	inline Func_2_t1899971632 ** get_address_of_U3CU3Ef__amU24cacheC_12() { return &___U3CU3Ef__amU24cacheC_12; }
	inline void set_U3CU3Ef__amU24cacheC_12(Func_2_t1899971632 * value)
	{
		___U3CU3Ef__amU24cacheC_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheC_12, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheD_13() { return static_cast<int32_t>(offsetof(TickableManager_t278367883_StaticFields, ___U3CU3Ef__amU24cacheD_13)); }
	inline Func_2_t1968156484 * get_U3CU3Ef__amU24cacheD_13() const { return ___U3CU3Ef__amU24cacheD_13; }
	inline Func_2_t1968156484 ** get_address_of_U3CU3Ef__amU24cacheD_13() { return &___U3CU3Ef__amU24cacheD_13; }
	inline void set_U3CU3Ef__amU24cacheD_13(Func_2_t1968156484 * value)
	{
		___U3CU3Ef__amU24cacheD_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheD_13, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheE_14() { return static_cast<int32_t>(offsetof(TickableManager_t278367883_StaticFields, ___U3CU3Ef__amU24cacheE_14)); }
	inline Func_2_t1899971632 * get_U3CU3Ef__amU24cacheE_14() const { return ___U3CU3Ef__amU24cacheE_14; }
	inline Func_2_t1899971632 ** get_address_of_U3CU3Ef__amU24cacheE_14() { return &___U3CU3Ef__amU24cacheE_14; }
	inline void set_U3CU3Ef__amU24cacheE_14(Func_2_t1899971632 * value)
	{
		___U3CU3Ef__amU24cacheE_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheE_14, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheF_15() { return static_cast<int32_t>(offsetof(TickableManager_t278367883_StaticFields, ___U3CU3Ef__amU24cacheF_15)); }
	inline Func_2_t1968156484 * get_U3CU3Ef__amU24cacheF_15() const { return ___U3CU3Ef__amU24cacheF_15; }
	inline Func_2_t1968156484 ** get_address_of_U3CU3Ef__amU24cacheF_15() { return &___U3CU3Ef__amU24cacheF_15; }
	inline void set_U3CU3Ef__amU24cacheF_15(Func_2_t1968156484 * value)
	{
		___U3CU3Ef__amU24cacheF_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheF_15, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache10_16() { return static_cast<int32_t>(offsetof(TickableManager_t278367883_StaticFields, ___U3CU3Ef__amU24cache10_16)); }
	inline Func_2_t1899971632 * get_U3CU3Ef__amU24cache10_16() const { return ___U3CU3Ef__amU24cache10_16; }
	inline Func_2_t1899971632 ** get_address_of_U3CU3Ef__amU24cache10_16() { return &___U3CU3Ef__amU24cache10_16; }
	inline void set_U3CU3Ef__amU24cache10_16(Func_2_t1899971632 * value)
	{
		___U3CU3Ef__amU24cache10_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache10_16, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache11_17() { return static_cast<int32_t>(offsetof(TickableManager_t278367883_StaticFields, ___U3CU3Ef__amU24cache11_17)); }
	inline Func_2_t1968156484 * get_U3CU3Ef__amU24cache11_17() const { return ___U3CU3Ef__amU24cache11_17; }
	inline Func_2_t1968156484 ** get_address_of_U3CU3Ef__amU24cache11_17() { return &___U3CU3Ef__amU24cache11_17; }
	inline void set_U3CU3Ef__amU24cache11_17(Func_2_t1968156484 * value)
	{
		___U3CU3Ef__amU24cache11_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache11_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
