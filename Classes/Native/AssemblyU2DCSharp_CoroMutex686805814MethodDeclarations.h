﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CoroMutex
struct CoroMutex_t686805814;
// CoroMutex/LockProc
struct LockProc_t1973859137;
// IProcess
struct IProcess_t4026237414;
// System.IDisposable
struct IDisposable_t1628921374;
// IConnectionCollector
struct IConnectionCollector_t665399366;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CoroMutex_LockProc1973859137.h"

// System.Void CoroMutex::.ctor()
extern "C"  void CoroMutex__ctor_m13008629 (CoroMutex_t686805814 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CoroMutex::RemoveLock(CoroMutex/LockProc)
extern "C"  void CoroMutex_RemoveLock_m4084621124 (CoroMutex_t686805814 * __this, LockProc_t1973859137 * ___proc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// IProcess CoroMutex::Lock(System.IDisposable&)
extern "C"  Il2CppObject * CoroMutex_Lock_m2261242419 (CoroMutex_t686805814 * __this, Il2CppObject ** ___unlock0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// IProcess CoroMutex::Lock(IConnectionCollector)
extern "C"  Il2CppObject * CoroMutex_Lock_m3140874973 (CoroMutex_t686805814 * __this, Il2CppObject * ___collector0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// IProcess CoroMutex::Lock()
extern "C"  Il2CppObject * CoroMutex_Lock_m3764190051 (CoroMutex_t686805814 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable CoroMutex::UnlockAsDisposable()
extern "C"  Il2CppObject * CoroMutex_UnlockAsDisposable_m904776082 (CoroMutex_t686805814 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CoroMutex::UnlockCurrent()
extern "C"  void CoroMutex_UnlockCurrent_m1967399528 (CoroMutex_t686805814 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CoroMutex::get_finished()
extern "C"  bool CoroMutex_get_finished_m535119318 (CoroMutex_t686805814 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CoroMutex::Tick(System.Single)
extern "C"  void CoroMutex_Tick_m2187156255 (CoroMutex_t686805814 * __this, float ___dt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CoroMutex::Dispose()
extern "C"  void CoroMutex_Dispose_m3809640946 (CoroMutex_t686805814 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CoroMutex::Clear()
extern "C"  void CoroMutex_Clear_m1714109216 (CoroMutex_t686805814 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CoroMutex::<UnlockAsDisposable>m__1E8()
extern "C"  void CoroMutex_U3CUnlockAsDisposableU3Em__1E8_m4085115954 (CoroMutex_t686805814 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
