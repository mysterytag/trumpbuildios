﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.UnityGraphicExtensions/<DirtyLayoutCallbackAsObservable>c__AnonStorey9E
struct U3CDirtyLayoutCallbackAsObservableU3Ec__AnonStorey9E_t2584692431;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.UnityGraphicExtensions/<DirtyLayoutCallbackAsObservable>c__AnonStorey9E::.ctor()
extern "C"  void U3CDirtyLayoutCallbackAsObservableU3Ec__AnonStorey9E__ctor_m4005661496 (U3CDirtyLayoutCallbackAsObservableU3Ec__AnonStorey9E_t2584692431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable UniRx.UnityGraphicExtensions/<DirtyLayoutCallbackAsObservable>c__AnonStorey9E::<>m__D8(UniRx.IObserver`1<UniRx.Unit>)
extern "C"  Il2CppObject * U3CDirtyLayoutCallbackAsObservableU3Ec__AnonStorey9E_U3CU3Em__D8_m3127089028 (U3CDirtyLayoutCallbackAsObservableU3Ec__AnonStorey9E_t2584692431 * __this, Il2CppObject* ___observer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
