﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.Tuple`3<System.Object,System.Object,System.Object>>
struct U3CSubscribeU3Ec__AnonStorey5B_t1248905831;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.Tuple`3<System.Object,System.Object,System.Object>>::.ctor()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B__ctor_m4125943511_gshared (U3CSubscribeU3Ec__AnonStorey5B_t1248905831 * __this, const MethodInfo* method);
#define U3CSubscribeU3Ec__AnonStorey5B__ctor_m4125943511(__this, method) ((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t1248905831 *, const MethodInfo*))U3CSubscribeU3Ec__AnonStorey5B__ctor_m4125943511_gshared)(__this, method)
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.Tuple`3<System.Object,System.Object,System.Object>>::<>m__73()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m3085121468_gshared (U3CSubscribeU3Ec__AnonStorey5B_t1248905831 * __this, const MethodInfo* method);
#define U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m3085121468(__this, method) ((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t1248905831 *, const MethodInfo*))U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m3085121468_gshared)(__this, method)
