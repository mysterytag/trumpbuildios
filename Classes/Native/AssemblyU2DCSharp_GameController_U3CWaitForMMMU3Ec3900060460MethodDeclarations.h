﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameController/<WaitForMMM>c__Iterator15
struct U3CWaitForMMMU3Ec__Iterator15_t3900060460;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GameController/<WaitForMMM>c__Iterator15::.ctor()
extern "C"  void U3CWaitForMMMU3Ec__Iterator15__ctor_m3476761936 (U3CWaitForMMMU3Ec__Iterator15_t3900060460 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameController/<WaitForMMM>c__Iterator15::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitForMMMU3Ec__Iterator15_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1283316108 (U3CWaitForMMMU3Ec__Iterator15_t3900060460 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameController/<WaitForMMM>c__Iterator15::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitForMMMU3Ec__Iterator15_System_Collections_IEnumerator_get_Current_m97285408 (U3CWaitForMMMU3Ec__Iterator15_t3900060460 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameController/<WaitForMMM>c__Iterator15::MoveNext()
extern "C"  bool U3CWaitForMMMU3Ec__Iterator15_MoveNext_m1743422092 (U3CWaitForMMMU3Ec__Iterator15_t3900060460 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController/<WaitForMMM>c__Iterator15::Dispose()
extern "C"  void U3CWaitForMMMU3Ec__Iterator15_Dispose_m3876914573 (U3CWaitForMMMU3Ec__Iterator15_t3900060460 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController/<WaitForMMM>c__Iterator15::Reset()
extern "C"  void U3CWaitForMMMU3Ec__Iterator15_Reset_m1123194877 (U3CWaitForMMMU3Ec__Iterator15_t3900060460 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
