﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct U3CSubscribeU3Ec__AnonStorey5B_t1546684115;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B__ctor_m2871798460_gshared (U3CSubscribeU3Ec__AnonStorey5B_t1546684115 * __this, const MethodInfo* method);
#define U3CSubscribeU3Ec__AnonStorey5B__ctor_m2871798460(__this, method) ((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t1546684115 *, const MethodInfo*))U3CSubscribeU3Ec__AnonStorey5B__ctor_m2871798460_gshared)(__this, method)
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::<>m__73()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m442570337_gshared (U3CSubscribeU3Ec__AnonStorey5B_t1546684115 * __this, const MethodInfo* method);
#define U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m442570337(__this, method) ((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t1546684115 *, const MethodInfo*))U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m442570337_gshared)(__this, method)
