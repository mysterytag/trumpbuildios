﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

// UnityEngine.Vector3 DG.Tweening.Core.Utils::Vector3FromAngle(System.Single,System.Single)
extern "C"  Vector3_t3525329789  Utils_Vector3FromAngle_m446042616 (Il2CppObject * __this /* static, unused */, float ___degrees0, float ___magnitude1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single DG.Tweening.Core.Utils::Angle2D(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float Utils_Angle2D_m3063727382 (Il2CppObject * __this /* static, unused */, Vector3_t3525329789  ___from0, Vector3_t3525329789  ___to1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
