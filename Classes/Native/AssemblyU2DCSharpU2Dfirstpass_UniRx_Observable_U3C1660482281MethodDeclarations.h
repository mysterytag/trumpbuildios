﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/<FromAsyncPattern>c__AnonStorey3F`1/<FromAsyncPattern>c__AnonStorey40`1<System.Object>
struct U3CFromAsyncPatternU3Ec__AnonStorey40_1_t1660482281;
// System.IAsyncResult
struct IAsyncResult_t537683269;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Observable/<FromAsyncPattern>c__AnonStorey3F`1/<FromAsyncPattern>c__AnonStorey40`1<System.Object>::.ctor()
extern "C"  void U3CFromAsyncPatternU3Ec__AnonStorey40_1__ctor_m3080020335_gshared (U3CFromAsyncPatternU3Ec__AnonStorey40_1_t1660482281 * __this, const MethodInfo* method);
#define U3CFromAsyncPatternU3Ec__AnonStorey40_1__ctor_m3080020335(__this, method) ((  void (*) (U3CFromAsyncPatternU3Ec__AnonStorey40_1_t1660482281 *, const MethodInfo*))U3CFromAsyncPatternU3Ec__AnonStorey40_1__ctor_m3080020335_gshared)(__this, method)
// System.Void UniRx.Observable/<FromAsyncPattern>c__AnonStorey3F`1/<FromAsyncPattern>c__AnonStorey40`1<System.Object>::<>m__65(System.IAsyncResult)
extern "C"  void U3CFromAsyncPatternU3Ec__AnonStorey40_1_U3CU3Em__65_m1377915148_gshared (U3CFromAsyncPatternU3Ec__AnonStorey40_1_t1660482281 * __this, Il2CppObject * ___iar0, const MethodInfo* method);
#define U3CFromAsyncPatternU3Ec__AnonStorey40_1_U3CU3Em__65_m1377915148(__this, ___iar0, method) ((  void (*) (U3CFromAsyncPatternU3Ec__AnonStorey40_1_t1660482281 *, Il2CppObject *, const MethodInfo*))U3CFromAsyncPatternU3Ec__AnonStorey40_1_U3CU3Em__65_m1377915148_gshared)(__this, ___iar0, method)
