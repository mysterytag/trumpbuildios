﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// UniRx.UnityUIComponentExtensions/<OnValueChangedAsObservable>c__AnonStoreyAC
struct U3COnValueChangedAsObservableU3Ec__AnonStoreyAC_t3117525758;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.String>
struct IObserver_1_t3180487805;
// UniRx.IObservable`1<System.String>
struct IObservable_1_t727287266;
// UnityEngine.Events.UnityEvent`1<System.String>
struct UnityEvent_1_t4205911009;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// UnityEngine.Events.UnityEvent`1<System.Object>
struct UnityEvent_1_t4074528527;
// UniRx.UnityUIComponentExtensions/<SubscribeToInteractable>c__AnonStoreyA6
struct U3CSubscribeToInteractableU3Ec__AnonStoreyA6_t961618071;
// UniRx.UnityUIComponentExtensions/<SubscribeToText>c__AnonStoreyA3
struct U3CSubscribeToTextU3Ec__AnonStoreyA3_t320382001;
// System.String
struct String_t;
// UniRx.Vector2ReactiveProperty
struct Vector2ReactiveProperty_t3641574511;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Vector2>
struct IEqualityComparer_1_t1554629143;
// UniRx.Vector3ReactiveProperty
struct Vector3ReactiveProperty_t699916912;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Vector3>
struct IEqualityComparer_1_t1554629144;
// UniRx.Vector4ReactiveProperty
struct Vector4ReactiveProperty_t2053226609;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Vector4>
struct IEqualityComparer_1_t1554629145;
// UniRx.IObservable`1<System.Net.WebResponse>
struct IObservable_1_t2170090779;
// System.Net.WebRequest
struct WebRequest_t3488810021;
// System.Func`3<System.AsyncCallback,System.Object,System.IAsyncResult>
struct Func_3_t3381739440;
// System.Func`2<System.IAsyncResult,System.Net.WebResponse>
struct Func_2_t2424576270;
// System.Func`2<System.IAsyncResult,System.Object>
struct Func_2_t850390275;
// UniRx.IObservable`1<System.Net.HttpWebResponse>
struct IObservable_1_t2426765171;
// System.Net.HttpWebRequest
struct HttpWebRequest_t171953869;
// System.Func`2<System.IAsyncResult,System.Net.HttpWebResponse>
struct Func_2_t2681250662;
// UniRx.IObservable`1<System.IO.Stream>
struct IObservable_1_t4272795235;
// System.Func`2<System.IAsyncResult,System.IO.Stream>
struct Func_2_t232313430;
// UniRx.WebRequestExtensions/<GetResponseAsObservable>c__AnonStorey35
struct U3CGetResponseAsObservableU3Ec__AnonStorey35_t2663964859;
// System.Net.HttpWebResponse
struct HttpWebResponse_t2667966807;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// UniRx.WWWErrorException
struct WWWErrorException_t3758994352;
// UnityEngine.WWW
struct WWW_t1522972100;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;
// System.Object
struct Il2CppObject;
// UnityEngine.Advertisements.MiniJSON.Json/Parser
struct Parser_t2383423552;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t2474804324;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t1634065389;
// UnityEngine.Advertisements.MiniJSON.Json/Serializer
struct Serializer_t1395478962;
// System.Collections.IDictionary
struct IDictionary_t1654916945;
// System.Collections.IList
struct IList_t1612618265;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_UnityUICompone3117525758.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_UnityUICompone3117525758MethodDeclarations.h"
#include "mscorlib_System_Void2779279689.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField2345609593MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_UnityEventExte3926190443MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField2345609593.h"
#include "mscorlib_System_String968488902.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_OnChangeE3894605131.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen4205911009.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_UnityEventExte3926190443.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_UnityUIComponen961618071.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_UnityUIComponen961618071MethodDeclarations.h"
#include "mscorlib_System_Boolean211005341.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable3621744255MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable3621744255.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_UnityUIComponen320382001.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_UnityUIComponen320382001MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Text3286458198.h"
#include "UnityEngine_UI_UnityEngine_UI_Text3286458198MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Vector2Reactiv3641574511.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Vector2Reactiv3641574511MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper4133662826MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_UnityEqualityC3801418734.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_UnityEqualityC3801418734MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Vector3Reactive699916912.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Vector3Reactive699916912MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper4133662827MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Vector4Reactiv2053226609.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Vector4Reactiv2053226609MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper4133662828MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector43525329790.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_WebRequestExte2125060705.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_WebRequestExte2125060705MethodDeclarations.h"
#include "System_System_Net_WebRequest3488810021.h"
#include "System_Core_System_Func_3_gen3381739440MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2424576270MethodDeclarations.h"
#include "System_System_Net_WebRequest3488810021MethodDeclarations.h"
#include "mscorlib_System_AsyncCallback1363551830.h"
#include "mscorlib_System_Object837106420.h"
#include "System_Core_System_Func_3_gen3381739440.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "System_System_Net_WebResponse2411292415.h"
#include "System_Core_System_Func_2_gen2424576270.h"
#include "System_System_Net_HttpWebRequest171953869.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_WebRequestExte2663964859MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2681250662MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_WebRequestExte2663964859.h"
#include "System_System_Net_HttpWebResponse2667966807.h"
#include "System_Core_System_Func_2_gen2681250662.h"
#include "System_Core_System_Func_2_gen232313430MethodDeclarations.h"
#include "mscorlib_System_IO_Stream219029575.h"
#include "System_Core_System_Func_2_gen232313430.h"
#include "System_System_Net_HttpWebRequest171953869MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_WWWErrorExcept3758994352.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_WWWErrorExcept3758994352MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWW1522972100.h"
#include "mscorlib_System_Exception1967233988MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWW1522972100MethodDeclarations.h"
#include "mscorlib_System_String968488902MethodDeclarations.h"
#include "mscorlib_System_Int322847414787MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Int322847414787.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2606186806.h"
#include "mscorlib_System_Char2778706699.h"
#include "System_System_Net_HttpStatusCode2736938801.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_YieldInstructi1940766803.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_YieldInstructi1940766803MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrame1917318876MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForFixedUpdate896427542MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrame1917318876.h"
#include "UnityEngine_UnityEngine_WaitForFixedUpdate896427542.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertise466290624.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertise466290624MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertis2383423551MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertis1395478962MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertis2383423551.h"
#include "mscorlib_System_IO_StringReader2229325051MethodDeclarations.h"
#include "mscorlib_System_IO_StringReader2229325051.h"
#include "mscorlib_System_Char2778706699MethodDeclarations.h"
#include "mscorlib_System_IO_TextReader1534522647MethodDeclarations.h"
#include "mscorlib_System_IO_TextReader1534522647.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2474804324.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2474804324MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisem80003545.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1634065389.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1634065389MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder3822575854MethodDeclarations.h"
#include "mscorlib_System_Convert1097883944MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder3822575854.h"
#include "mscorlib_System_Globalization_CultureInfo3603717042MethodDeclarations.h"
#include "mscorlib_System_Int642847414882MethodDeclarations.h"
#include "mscorlib_System_Double534516614MethodDeclarations.h"
#include "mscorlib_System_Int642847414882.h"
#include "mscorlib_System_Double534516614.h"
#include "mscorlib_System_Globalization_CultureInfo3603717042.h"
#include "mscorlib_System_Globalization_NumberStyles3988678145.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge190145395MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge190145395.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertisem80003545MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityEngine_Advertis1395478962.h"
#include "mscorlib_System_Single958209021MethodDeclarations.h"
#include "mscorlib_System_Single958209021.h"
#include "mscorlib_System_UInt32985925326.h"
#include "mscorlib_System_SByte2855346064.h"
#include "mscorlib_System_Byte2778693821.h"
#include "mscorlib_System_Int162847414729.h"
#include "mscorlib_System_UInt16985925268.h"
#include "mscorlib_System_UInt64985925421.h"
#include "mscorlib_System_Decimal1688557254.h"

// UniRx.IObservable`1<!!0> UniRx.UnityEventExtensions::AsObservable<System.Object>(UnityEngine.Events.UnityEvent`1<!!0>)
extern "C"  Il2CppObject* UnityEventExtensions_AsObservable_TisIl2CppObject_m373391309_gshared (Il2CppObject * __this /* static, unused */, UnityEvent_1_t4074528527 * p0, const MethodInfo* method);
#define UnityEventExtensions_AsObservable_TisIl2CppObject_m373391309(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, UnityEvent_1_t4074528527 *, const MethodInfo*))UnityEventExtensions_AsObservable_TisIl2CppObject_m373391309_gshared)(__this /* static, unused */, p0, method)
// UniRx.IObservable`1<!!0> UniRx.UnityEventExtensions::AsObservable<System.String>(UnityEngine.Events.UnityEvent`1<!!0>)
#define UnityEventExtensions_AsObservable_TisString_t_m459332283(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, UnityEvent_1_t4205911009 *, const MethodInfo*))UnityEventExtensions_AsObservable_TisIl2CppObject_m373391309_gshared)(__this /* static, unused */, p0, method)
// UniRx.IObservable`1<!!0> UniRx.WebRequestExtensions::AbortableDeferredAsyncRequest<System.Object>(System.Func`3<System.AsyncCallback,System.Object,System.IAsyncResult>,System.Func`2<System.IAsyncResult,!!0>,System.Net.WebRequest)
extern "C"  Il2CppObject* WebRequestExtensions_AbortableDeferredAsyncRequest_TisIl2CppObject_m3296923424_gshared (Il2CppObject * __this /* static, unused */, Func_3_t3381739440 * p0, Func_2_t850390275 * p1, WebRequest_t3488810021 * p2, const MethodInfo* method);
#define WebRequestExtensions_AbortableDeferredAsyncRequest_TisIl2CppObject_m3296923424(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Func_3_t3381739440 *, Func_2_t850390275 *, WebRequest_t3488810021 *, const MethodInfo*))WebRequestExtensions_AbortableDeferredAsyncRequest_TisIl2CppObject_m3296923424_gshared)(__this /* static, unused */, p0, p1, p2, method)
// UniRx.IObservable`1<!!0> UniRx.WebRequestExtensions::AbortableDeferredAsyncRequest<System.Net.WebResponse>(System.Func`3<System.AsyncCallback,System.Object,System.IAsyncResult>,System.Func`2<System.IAsyncResult,!!0>,System.Net.WebRequest)
#define WebRequestExtensions_AbortableDeferredAsyncRequest_TisWebResponse_t2411292415_m593820035(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Func_3_t3381739440 *, Func_2_t2424576270 *, WebRequest_t3488810021 *, const MethodInfo*))WebRequestExtensions_AbortableDeferredAsyncRequest_TisIl2CppObject_m3296923424_gshared)(__this /* static, unused */, p0, p1, p2, method)
// UniRx.IObservable`1<!!0> UniRx.WebRequestExtensions::AbortableDeferredAsyncRequest<System.Net.HttpWebResponse>(System.Func`3<System.AsyncCallback,System.Object,System.IAsyncResult>,System.Func`2<System.IAsyncResult,!!0>,System.Net.WebRequest)
#define WebRequestExtensions_AbortableDeferredAsyncRequest_TisHttpWebResponse_t2667966807_m2112254891(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Func_3_t3381739440 *, Func_2_t2681250662 *, WebRequest_t3488810021 *, const MethodInfo*))WebRequestExtensions_AbortableDeferredAsyncRequest_TisIl2CppObject_m3296923424_gshared)(__this /* static, unused */, p0, p1, p2, method)
// UniRx.IObservable`1<!!0> UniRx.WebRequestExtensions::AbortableDeferredAsyncRequest<System.IO.Stream>(System.Func`3<System.AsyncCallback,System.Object,System.IAsyncResult>,System.Func`2<System.IAsyncResult,!!0>,System.Net.WebRequest)
#define WebRequestExtensions_AbortableDeferredAsyncRequest_TisStream_t219029575_m3730534975(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Func_3_t3381739440 *, Func_2_t232313430 *, WebRequest_t3488810021 *, const MethodInfo*))WebRequestExtensions_AbortableDeferredAsyncRequest_TisIl2CppObject_m3296923424_gshared)(__this /* static, unused */, p0, p1, p2, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.UnityUIComponentExtensions/<OnValueChangedAsObservable>c__AnonStoreyAC::.ctor()
extern "C"  void U3COnValueChangedAsObservableU3Ec__AnonStoreyAC__ctor_m23670144 (U3COnValueChangedAsObservableU3Ec__AnonStoreyAC_t3117525758 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.IDisposable UniRx.UnityUIComponentExtensions/<OnValueChangedAsObservable>c__AnonStoreyAC::<>m__EA(UniRx.IObserver`1<System.String>)
extern Il2CppClass* IObserver_1_t3180487805_il2cpp_TypeInfo_var;
extern Il2CppClass* IObservable_1_t727287266_il2cpp_TypeInfo_var;
extern const MethodInfo* UnityEventExtensions_AsObservable_TisString_t_m459332283_MethodInfo_var;
extern const uint32_t U3COnValueChangedAsObservableU3Ec__AnonStoreyAC_U3CU3Em__EA_m3352563800_MetadataUsageId;
extern "C"  Il2CppObject * U3COnValueChangedAsObservableU3Ec__AnonStoreyAC_U3CU3Em__EA_m3352563800 (U3COnValueChangedAsObservableU3Ec__AnonStoreyAC_t3117525758 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3COnValueChangedAsObservableU3Ec__AnonStoreyAC_U3CU3Em__EA_m3352563800_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___observer0;
		InputField_t2345609593 * L_1 = __this->get_inputField_0();
		NullCheck(L_1);
		String_t* L_2 = InputField_get_text_m3972300732(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		InterfaceActionInvoker1< String_t* >::Invoke(2 /* System.Void UniRx.IObserver`1<System.String>::OnNext(T) */, IObserver_1_t3180487805_il2cpp_TypeInfo_var, L_0, L_2);
		InputField_t2345609593 * L_3 = __this->get_inputField_0();
		NullCheck(L_3);
		OnChangeEvent_t3894605131 * L_4 = InputField_get_onValueChanged_m2321341872(L_3, /*hidden argument*/NULL);
		Il2CppObject* L_5 = UnityEventExtensions_AsObservable_TisString_t_m459332283(NULL /*static, unused*/, L_4, /*hidden argument*/UnityEventExtensions_AsObservable_TisString_t_m459332283_MethodInfo_var);
		Il2CppObject* L_6 = ___observer0;
		NullCheck(L_5);
		Il2CppObject * L_7 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.String>::Subscribe(UniRx.IObserver`1<T>) */, IObservable_1_t727287266_il2cpp_TypeInfo_var, L_5, L_6);
		return L_7;
	}
}
// System.Void UniRx.UnityUIComponentExtensions/<SubscribeToInteractable>c__AnonStoreyA6::.ctor()
extern "C"  void U3CSubscribeToInteractableU3Ec__AnonStoreyA6__ctor_m2562781553 (U3CSubscribeToInteractableU3Ec__AnonStoreyA6_t961618071 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.UnityUIComponentExtensions/<SubscribeToInteractable>c__AnonStoreyA6::<>m__E4(System.Boolean)
extern "C"  void U3CSubscribeToInteractableU3Ec__AnonStoreyA6_U3CU3Em__E4_m2253620864 (U3CSubscribeToInteractableU3Ec__AnonStoreyA6_t961618071 * __this, bool ___x0, const MethodInfo* method)
{
	{
		Selectable_t3621744255 * L_0 = __this->get_selectable_0();
		bool L_1 = ___x0;
		NullCheck(L_0);
		Selectable_set_interactable_m2686686419(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.UnityUIComponentExtensions/<SubscribeToText>c__AnonStoreyA3::.ctor()
extern "C"  void U3CSubscribeToTextU3Ec__AnonStoreyA3__ctor_m3025358871 (U3CSubscribeToTextU3Ec__AnonStoreyA3_t320382001 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.UnityUIComponentExtensions/<SubscribeToText>c__AnonStoreyA3::<>m__E1(System.String)
extern "C"  void U3CSubscribeToTextU3Ec__AnonStoreyA3_U3CU3Em__E1_m2483564502 (U3CSubscribeToTextU3Ec__AnonStoreyA3_t320382001 * __this, String_t* ___x0, const MethodInfo* method)
{
	{
		Text_t3286458198 * L_0 = __this->get_text_0();
		String_t* L_1 = ___x0;
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_1);
		return;
	}
}
// System.Void UniRx.Vector2ReactiveProperty::.ctor()
extern Il2CppClass* ReactiveProperty_1_t4133662826_il2cpp_TypeInfo_var;
extern const MethodInfo* ReactiveProperty_1__ctor_m3508624908_MethodInfo_var;
extern const uint32_t Vector2ReactiveProperty__ctor_m1684401746_MetadataUsageId;
extern "C"  void Vector2ReactiveProperty__ctor_m1684401746 (Vector2ReactiveProperty_t3641574511 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector2ReactiveProperty__ctor_m1684401746_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ReactiveProperty_1_t4133662826_il2cpp_TypeInfo_var);
		ReactiveProperty_1__ctor_m3508624908(__this, /*hidden argument*/ReactiveProperty_1__ctor_m3508624908_MethodInfo_var);
		return;
	}
}
// System.Void UniRx.Vector2ReactiveProperty::.ctor(UnityEngine.Vector2)
extern Il2CppClass* ReactiveProperty_1_t4133662826_il2cpp_TypeInfo_var;
extern const MethodInfo* ReactiveProperty_1__ctor_m1393191122_MethodInfo_var;
extern const uint32_t Vector2ReactiveProperty__ctor_m670379048_MetadataUsageId;
extern "C"  void Vector2ReactiveProperty__ctor_m670379048 (Vector2ReactiveProperty_t3641574511 * __this, Vector2_t3525329788  ___initialValue0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector2ReactiveProperty__ctor_m670379048_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector2_t3525329788  L_0 = ___initialValue0;
		IL2CPP_RUNTIME_CLASS_INIT(ReactiveProperty_1_t4133662826_il2cpp_TypeInfo_var);
		ReactiveProperty_1__ctor_m1393191122(__this, L_0, /*hidden argument*/ReactiveProperty_1__ctor_m1393191122_MethodInfo_var);
		return;
	}
}
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Vector2> UniRx.Vector2ReactiveProperty::get_EqualityComparer()
extern Il2CppClass* UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var;
extern const uint32_t Vector2ReactiveProperty_get_EqualityComparer_m227569759_MetadataUsageId;
extern "C"  Il2CppObject* Vector2ReactiveProperty_get_EqualityComparer_m227569759 (Vector2ReactiveProperty_t3641574511 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector2ReactiveProperty_get_EqualityComparer_m227569759_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var);
		Il2CppObject* L_0 = ((UnityEqualityComparer_t3801418734_StaticFields*)UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var->static_fields)->get_Vector2_0();
		return L_0;
	}
}
// System.Void UniRx.Vector3ReactiveProperty::.ctor()
extern Il2CppClass* ReactiveProperty_1_t4133662827_il2cpp_TypeInfo_var;
extern const MethodInfo* ReactiveProperty_1__ctor_m1711673549_MethodInfo_var;
extern const uint32_t Vector3ReactiveProperty__ctor_m1715421553_MetadataUsageId;
extern "C"  void Vector3ReactiveProperty__ctor_m1715421553 (Vector3ReactiveProperty_t699916912 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector3ReactiveProperty__ctor_m1715421553_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ReactiveProperty_1_t4133662827_il2cpp_TypeInfo_var);
		ReactiveProperty_1__ctor_m1711673549(__this, /*hidden argument*/ReactiveProperty_1__ctor_m1711673549_MethodInfo_var);
		return;
	}
}
// System.Void UniRx.Vector3ReactiveProperty::.ctor(UnityEngine.Vector3)
extern Il2CppClass* ReactiveProperty_1_t4133662827_il2cpp_TypeInfo_var;
extern const MethodInfo* ReactiveProperty_1__ctor_m1522273841_MethodInfo_var;
extern const uint32_t Vector3ReactiveProperty__ctor_m3264605896_MetadataUsageId;
extern "C"  void Vector3ReactiveProperty__ctor_m3264605896 (Vector3ReactiveProperty_t699916912 * __this, Vector3_t3525329789  ___initialValue0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector3ReactiveProperty__ctor_m3264605896_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector3_t3525329789  L_0 = ___initialValue0;
		IL2CPP_RUNTIME_CLASS_INIT(ReactiveProperty_1_t4133662827_il2cpp_TypeInfo_var);
		ReactiveProperty_1__ctor_m1522273841(__this, L_0, /*hidden argument*/ReactiveProperty_1__ctor_m1522273841_MethodInfo_var);
		return;
	}
}
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Vector3> UniRx.Vector3ReactiveProperty::get_EqualityComparer()
extern Il2CppClass* UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var;
extern const uint32_t Vector3ReactiveProperty_get_EqualityComparer_m2607214143_MetadataUsageId;
extern "C"  Il2CppObject* Vector3ReactiveProperty_get_EqualityComparer_m2607214143 (Vector3ReactiveProperty_t699916912 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector3ReactiveProperty_get_EqualityComparer_m2607214143_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var);
		Il2CppObject* L_0 = ((UnityEqualityComparer_t3801418734_StaticFields*)UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var->static_fields)->get_Vector3_1();
		return L_0;
	}
}
// System.Void UniRx.Vector4ReactiveProperty::.ctor()
extern Il2CppClass* ReactiveProperty_1_t4133662828_il2cpp_TypeInfo_var;
extern const MethodInfo* ReactiveProperty_1__ctor_m4209689486_MethodInfo_var;
extern const uint32_t Vector4ReactiveProperty__ctor_m1746441360_MetadataUsageId;
extern "C"  void Vector4ReactiveProperty__ctor_m1746441360 (Vector4ReactiveProperty_t2053226609 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector4ReactiveProperty__ctor_m1746441360_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ReactiveProperty_1_t4133662828_il2cpp_TypeInfo_var);
		ReactiveProperty_1__ctor_m4209689486(__this, /*hidden argument*/ReactiveProperty_1__ctor_m4209689486_MethodInfo_var);
		return;
	}
}
// System.Void UniRx.Vector4ReactiveProperty::.ctor(UnityEngine.Vector4)
extern Il2CppClass* ReactiveProperty_1_t4133662828_il2cpp_TypeInfo_var;
extern const MethodInfo* ReactiveProperty_1__ctor_m1651356560_MethodInfo_var;
extern const uint32_t Vector4ReactiveProperty__ctor_m1563865448_MetadataUsageId;
extern "C"  void Vector4ReactiveProperty__ctor_m1563865448 (Vector4ReactiveProperty_t2053226609 * __this, Vector4_t3525329790  ___initialValue0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector4ReactiveProperty__ctor_m1563865448_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector4_t3525329790  L_0 = ___initialValue0;
		IL2CPP_RUNTIME_CLASS_INIT(ReactiveProperty_1_t4133662828_il2cpp_TypeInfo_var);
		ReactiveProperty_1__ctor_m1651356560(__this, L_0, /*hidden argument*/ReactiveProperty_1__ctor_m1651356560_MethodInfo_var);
		return;
	}
}
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Vector4> UniRx.Vector4ReactiveProperty::get_EqualityComparer()
extern Il2CppClass* UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var;
extern const uint32_t Vector4ReactiveProperty_get_EqualityComparer_m691891231_MetadataUsageId;
extern "C"  Il2CppObject* Vector4ReactiveProperty_get_EqualityComparer_m691891231 (Vector4ReactiveProperty_t2053226609 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector4ReactiveProperty_get_EqualityComparer_m691891231_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var);
		Il2CppObject* L_0 = ((UnityEqualityComparer_t3801418734_StaticFields*)UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var->static_fields)->get_Vector4_2();
		return L_0;
	}
}
// UniRx.IObservable`1<System.Net.WebResponse> UniRx.WebRequestExtensions::GetResponseAsObservable(System.Net.WebRequest)
extern Il2CppClass* Func_3_t3381739440_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t2424576270_il2cpp_TypeInfo_var;
extern const MethodInfo* Func_3__ctor_m1408183116_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m4151380256_MethodInfo_var;
extern const MethodInfo* WebRequestExtensions_AbortableDeferredAsyncRequest_TisWebResponse_t2411292415_m593820035_MethodInfo_var;
extern const uint32_t WebRequestExtensions_GetResponseAsObservable_m313833062_MetadataUsageId;
extern "C"  Il2CppObject* WebRequestExtensions_GetResponseAsObservable_m313833062 (Il2CppObject * __this /* static, unused */, WebRequest_t3488810021 * ___request0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebRequestExtensions_GetResponseAsObservable_m313833062_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		WebRequest_t3488810021 * L_0 = ___request0;
		WebRequest_t3488810021 * L_1 = L_0;
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)GetVirtualMethodInfo(L_1, 32));
		Func_3_t3381739440 * L_3 = (Func_3_t3381739440 *)il2cpp_codegen_object_new(Func_3_t3381739440_il2cpp_TypeInfo_var);
		Func_3__ctor_m1408183116(L_3, L_1, L_2, /*hidden argument*/Func_3__ctor_m1408183116_MethodInfo_var);
		WebRequest_t3488810021 * L_4 = ___request0;
		WebRequest_t3488810021 * L_5 = L_4;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)GetVirtualMethodInfo(L_5, 34));
		Func_2_t2424576270 * L_7 = (Func_2_t2424576270 *)il2cpp_codegen_object_new(Func_2_t2424576270_il2cpp_TypeInfo_var);
		Func_2__ctor_m4151380256(L_7, L_5, L_6, /*hidden argument*/Func_2__ctor_m4151380256_MethodInfo_var);
		WebRequest_t3488810021 * L_8 = ___request0;
		Il2CppObject* L_9 = WebRequestExtensions_AbortableDeferredAsyncRequest_TisWebResponse_t2411292415_m593820035(NULL /*static, unused*/, L_3, L_7, L_8, /*hidden argument*/WebRequestExtensions_AbortableDeferredAsyncRequest_TisWebResponse_t2411292415_m593820035_MethodInfo_var);
		return L_9;
	}
}
// UniRx.IObservable`1<System.Net.HttpWebResponse> UniRx.WebRequestExtensions::GetResponseAsObservable(System.Net.HttpWebRequest)
extern Il2CppClass* U3CGetResponseAsObservableU3Ec__AnonStorey35_t2663964859_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_3_t3381739440_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t2681250662_il2cpp_TypeInfo_var;
extern const MethodInfo* Func_3__ctor_m1408183116_MethodInfo_var;
extern const MethodInfo* U3CGetResponseAsObservableU3Ec__AnonStorey35_U3CU3Em__37_m661311674_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m773097928_MethodInfo_var;
extern const MethodInfo* WebRequestExtensions_AbortableDeferredAsyncRequest_TisHttpWebResponse_t2667966807_m2112254891_MethodInfo_var;
extern const uint32_t WebRequestExtensions_GetResponseAsObservable_m3915343894_MetadataUsageId;
extern "C"  Il2CppObject* WebRequestExtensions_GetResponseAsObservable_m3915343894 (Il2CppObject * __this /* static, unused */, HttpWebRequest_t171953869 * ___request0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebRequestExtensions_GetResponseAsObservable_m3915343894_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetResponseAsObservableU3Ec__AnonStorey35_t2663964859 * V_0 = NULL;
	{
		U3CGetResponseAsObservableU3Ec__AnonStorey35_t2663964859 * L_0 = (U3CGetResponseAsObservableU3Ec__AnonStorey35_t2663964859 *)il2cpp_codegen_object_new(U3CGetResponseAsObservableU3Ec__AnonStorey35_t2663964859_il2cpp_TypeInfo_var);
		U3CGetResponseAsObservableU3Ec__AnonStorey35__ctor_m1648715956(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetResponseAsObservableU3Ec__AnonStorey35_t2663964859 * L_1 = V_0;
		HttpWebRequest_t171953869 * L_2 = ___request0;
		NullCheck(L_1);
		L_1->set_request_0(L_2);
		U3CGetResponseAsObservableU3Ec__AnonStorey35_t2663964859 * L_3 = V_0;
		NullCheck(L_3);
		HttpWebRequest_t171953869 * L_4 = L_3->get_request_0();
		HttpWebRequest_t171953869 * L_5 = L_4;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)GetVirtualMethodInfo(L_5, 32));
		Func_3_t3381739440 * L_7 = (Func_3_t3381739440 *)il2cpp_codegen_object_new(Func_3_t3381739440_il2cpp_TypeInfo_var);
		Func_3__ctor_m1408183116(L_7, L_5, L_6, /*hidden argument*/Func_3__ctor_m1408183116_MethodInfo_var);
		U3CGetResponseAsObservableU3Ec__AnonStorey35_t2663964859 * L_8 = V_0;
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)(void*)U3CGetResponseAsObservableU3Ec__AnonStorey35_U3CU3Em__37_m661311674_MethodInfo_var);
		Func_2_t2681250662 * L_10 = (Func_2_t2681250662 *)il2cpp_codegen_object_new(Func_2_t2681250662_il2cpp_TypeInfo_var);
		Func_2__ctor_m773097928(L_10, L_8, L_9, /*hidden argument*/Func_2__ctor_m773097928_MethodInfo_var);
		U3CGetResponseAsObservableU3Ec__AnonStorey35_t2663964859 * L_11 = V_0;
		NullCheck(L_11);
		HttpWebRequest_t171953869 * L_12 = L_11->get_request_0();
		Il2CppObject* L_13 = WebRequestExtensions_AbortableDeferredAsyncRequest_TisHttpWebResponse_t2667966807_m2112254891(NULL /*static, unused*/, L_7, L_10, L_12, /*hidden argument*/WebRequestExtensions_AbortableDeferredAsyncRequest_TisHttpWebResponse_t2667966807_m2112254891_MethodInfo_var);
		return L_13;
	}
}
// UniRx.IObservable`1<System.IO.Stream> UniRx.WebRequestExtensions::GetRequestStreamAsObservable(System.Net.WebRequest)
extern Il2CppClass* Func_3_t3381739440_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t232313430_il2cpp_TypeInfo_var;
extern const MethodInfo* Func_3__ctor_m1408183116_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3621814300_MethodInfo_var;
extern const MethodInfo* WebRequestExtensions_AbortableDeferredAsyncRequest_TisStream_t219029575_m3730534975_MethodInfo_var;
extern const uint32_t WebRequestExtensions_GetRequestStreamAsObservable_m1484252616_MetadataUsageId;
extern "C"  Il2CppObject* WebRequestExtensions_GetRequestStreamAsObservable_m1484252616 (Il2CppObject * __this /* static, unused */, WebRequest_t3488810021 * ___request0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebRequestExtensions_GetRequestStreamAsObservable_m1484252616_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		WebRequest_t3488810021 * L_0 = ___request0;
		WebRequest_t3488810021 * L_1 = L_0;
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)GetVirtualMethodInfo(L_1, 31));
		Func_3_t3381739440 * L_3 = (Func_3_t3381739440 *)il2cpp_codegen_object_new(Func_3_t3381739440_il2cpp_TypeInfo_var);
		Func_3__ctor_m1408183116(L_3, L_1, L_2, /*hidden argument*/Func_3__ctor_m1408183116_MethodInfo_var);
		WebRequest_t3488810021 * L_4 = ___request0;
		WebRequest_t3488810021 * L_5 = L_4;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)GetVirtualMethodInfo(L_5, 33));
		Func_2_t232313430 * L_7 = (Func_2_t232313430 *)il2cpp_codegen_object_new(Func_2_t232313430_il2cpp_TypeInfo_var);
		Func_2__ctor_m3621814300(L_7, L_5, L_6, /*hidden argument*/Func_2__ctor_m3621814300_MethodInfo_var);
		WebRequest_t3488810021 * L_8 = ___request0;
		Il2CppObject* L_9 = WebRequestExtensions_AbortableDeferredAsyncRequest_TisStream_t219029575_m3730534975(NULL /*static, unused*/, L_3, L_7, L_8, /*hidden argument*/WebRequestExtensions_AbortableDeferredAsyncRequest_TisStream_t219029575_m3730534975_MethodInfo_var);
		return L_9;
	}
}
// System.Void UniRx.WebRequestExtensions/<GetResponseAsObservable>c__AnonStorey35::.ctor()
extern "C"  void U3CGetResponseAsObservableU3Ec__AnonStorey35__ctor_m1648715956 (U3CGetResponseAsObservableU3Ec__AnonStorey35_t2663964859 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Net.HttpWebResponse UniRx.WebRequestExtensions/<GetResponseAsObservable>c__AnonStorey35::<>m__37(System.IAsyncResult)
extern Il2CppClass* HttpWebResponse_t2667966807_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetResponseAsObservableU3Ec__AnonStorey35_U3CU3Em__37_m661311674_MetadataUsageId;
extern "C"  HttpWebResponse_t2667966807 * U3CGetResponseAsObservableU3Ec__AnonStorey35_U3CU3Em__37_m661311674 (U3CGetResponseAsObservableU3Ec__AnonStorey35_t2663964859 * __this, Il2CppObject * ___ar0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetResponseAsObservableU3Ec__AnonStorey35_U3CU3Em__37_m661311674_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		HttpWebRequest_t171953869 * L_0 = __this->get_request_0();
		Il2CppObject * L_1 = ___ar0;
		NullCheck(L_0);
		WebResponse_t2411292415 * L_2 = VirtFuncInvoker1< WebResponse_t2411292415 *, Il2CppObject * >::Invoke(34 /* System.Net.WebResponse System.Net.HttpWebRequest::EndGetResponse(System.IAsyncResult) */, L_0, L_1);
		return ((HttpWebResponse_t2667966807 *)CastclassClass(L_2, HttpWebResponse_t2667966807_il2cpp_TypeInfo_var));
	}
}
// System.Void UniRx.WWWErrorException::.ctor(UnityEngine.WWW)
extern Il2CppClass* CharU5BU5D_t3416858730_il2cpp_TypeInfo_var;
extern const uint32_t WWWErrorException__ctor_m2195240225_MetadataUsageId;
extern "C"  void WWWErrorException__ctor_m2195240225 (WWWErrorException_t3758994352 * __this, WWW_t1522972100 * ___www0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WWWErrorException__ctor_m2195240225_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t2956870243* V_0 = NULL;
	int32_t V_1 = 0;
	{
		Exception__ctor_m3223090658(__this, /*hidden argument*/NULL);
		WWW_t1522972100 * L_0 = ___www0;
		WWWErrorException_set_WWW_m3688321737(__this, L_0, /*hidden argument*/NULL);
		WWW_t1522972100 * L_1 = ___www0;
		NullCheck(L_1);
		String_t* L_2 = WWW_get_error_m1787423074(L_1, /*hidden argument*/NULL);
		WWWErrorException_set_RawErrorMessage_m367948041(__this, L_2, /*hidden argument*/NULL);
		WWW_t1522972100 * L_3 = ___www0;
		NullCheck(L_3);
		Dictionary_2_t2606186806 * L_4 = WWW_get_responseHeaders_m2488150044(L_3, /*hidden argument*/NULL);
		WWWErrorException_set_ResponseHeaders_m3784622504(__this, L_4, /*hidden argument*/NULL);
		WWWErrorException_set_HasResponse_m1059041444(__this, (bool)0, /*hidden argument*/NULL);
		WWW_t1522972100 * L_5 = ___www0;
		NullCheck(L_5);
		String_t* L_6 = WWW_get_text_m4216049525(L_5, /*hidden argument*/NULL);
		WWWErrorException_set_Text_m2582704325(__this, L_6, /*hidden argument*/NULL);
		String_t* L_7 = WWWErrorException_get_RawErrorMessage_m3100515952(__this, /*hidden argument*/NULL);
		CharU5BU5D_t3416858730* L_8 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)2));
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint16_t)((int32_t)32));
		CharU5BU5D_t3416858730* L_9 = L_8;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 1);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(1), (uint16_t)((int32_t)58));
		NullCheck(L_7);
		StringU5BU5D_t2956870243* L_10 = String_Split_m290179486(L_7, L_9, /*hidden argument*/NULL);
		V_0 = L_10;
		StringU5BU5D_t2956870243* L_11 = V_0;
		NullCheck(L_11);
		if (!(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))
		{
			goto IL_0079;
		}
	}
	{
		StringU5BU5D_t2956870243* L_12 = V_0;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 0);
		int32_t L_13 = 0;
		bool L_14 = Int32_TryParse_m695344220(NULL /*static, unused*/, ((L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_13))), (&V_1), /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0079;
		}
	}
	{
		WWWErrorException_set_HasResponse_m1059041444(__this, (bool)1, /*hidden argument*/NULL);
		int32_t L_15 = V_1;
		WWWErrorException_set_StatusCode_m1127295532(__this, L_15, /*hidden argument*/NULL);
	}

IL_0079:
	{
		return;
	}
}
// System.String UniRx.WWWErrorException::get_RawErrorMessage()
extern "C"  String_t* WWWErrorException_get_RawErrorMessage_m3100515952 (WWWErrorException_t3758994352 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CRawErrorMessageU3Ek__BackingField_11();
		return L_0;
	}
}
// System.Void UniRx.WWWErrorException::set_RawErrorMessage(System.String)
extern "C"  void WWWErrorException_set_RawErrorMessage_m367948041 (WWWErrorException_t3758994352 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CRawErrorMessageU3Ek__BackingField_11(L_0);
		return;
	}
}
// System.Boolean UniRx.WWWErrorException::get_HasResponse()
extern "C"  bool WWWErrorException_get_HasResponse_m1677127805 (WWWErrorException_t3758994352 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_U3CHasResponseU3Ek__BackingField_12();
		return L_0;
	}
}
// System.Void UniRx.WWWErrorException::set_HasResponse(System.Boolean)
extern "C"  void WWWErrorException_set_HasResponse_m1059041444 (WWWErrorException_t3758994352 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CHasResponseU3Ek__BackingField_12(L_0);
		return;
	}
}
// System.String UniRx.WWWErrorException::get_Text()
extern "C"  String_t* WWWErrorException_get_Text_m3655654886 (WWWErrorException_t3758994352 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CTextU3Ek__BackingField_13();
		return L_0;
	}
}
// System.Void UniRx.WWWErrorException::set_Text(System.String)
extern "C"  void WWWErrorException_set_Text_m2582704325 (WWWErrorException_t3758994352 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CTextU3Ek__BackingField_13(L_0);
		return;
	}
}
// System.Net.HttpStatusCode UniRx.WWWErrorException::get_StatusCode()
extern "C"  int32_t WWWErrorException_get_StatusCode_m715253407 (WWWErrorException_t3758994352 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CStatusCodeU3Ek__BackingField_14();
		return L_0;
	}
}
// System.Void UniRx.WWWErrorException::set_StatusCode(System.Net.HttpStatusCode)
extern "C"  void WWWErrorException_set_StatusCode_m1127295532 (WWWErrorException_t3758994352 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CStatusCodeU3Ek__BackingField_14(L_0);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.String> UniRx.WWWErrorException::get_ResponseHeaders()
extern "C"  Dictionary_2_t2606186806 * WWWErrorException_get_ResponseHeaders_m1839206209 (WWWErrorException_t3758994352 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2606186806 * L_0 = __this->get_U3CResponseHeadersU3Ek__BackingField_15();
		return L_0;
	}
}
// System.Void UniRx.WWWErrorException::set_ResponseHeaders(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void WWWErrorException_set_ResponseHeaders_m3784622504 (WWWErrorException_t3758994352 * __this, Dictionary_2_t2606186806 * ___value0, const MethodInfo* method)
{
	{
		Dictionary_2_t2606186806 * L_0 = ___value0;
		__this->set_U3CResponseHeadersU3Ek__BackingField_15(L_0);
		return;
	}
}
// UnityEngine.WWW UniRx.WWWErrorException::get_WWW()
extern "C"  WWW_t1522972100 * WWWErrorException_get_WWW_m2013148880 (WWWErrorException_t3758994352 * __this, const MethodInfo* method)
{
	{
		WWW_t1522972100 * L_0 = __this->get_U3CWWWU3Ek__BackingField_16();
		return L_0;
	}
}
// System.Void UniRx.WWWErrorException::set_WWW(UnityEngine.WWW)
extern "C"  void WWWErrorException_set_WWW_m3688321737 (WWWErrorException_t3758994352 * __this, WWW_t1522972100 * ___value0, const MethodInfo* method)
{
	{
		WWW_t1522972100 * L_0 = ___value0;
		__this->set_U3CWWWU3Ek__BackingField_16(L_0);
		return;
	}
}
// System.String UniRx.WWWErrorException::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral32;
extern const uint32_t WWWErrorException_ToString_m2841568860_MetadataUsageId;
extern "C"  String_t* WWWErrorException_ToString_m2841568860 (WWWErrorException_t3758994352 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WWWErrorException_ToString_m2841568860_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		String_t* L_0 = WWWErrorException_get_Text_m3655654886(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0019;
		}
	}
	{
		String_t* L_3 = WWWErrorException_get_RawErrorMessage_m3100515952(__this, /*hidden argument*/NULL);
		return L_3;
	}

IL_0019:
	{
		String_t* L_4 = WWWErrorException_get_RawErrorMessage_m3100515952(__this, /*hidden argument*/NULL);
		String_t* L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m1825781833(NULL /*static, unused*/, L_4, _stringLiteral32, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void UniRx.YieldInstructionCache::.cctor()
extern Il2CppClass* WaitForEndOfFrame_t1917318876_il2cpp_TypeInfo_var;
extern Il2CppClass* YieldInstructionCache_t1940766803_il2cpp_TypeInfo_var;
extern Il2CppClass* WaitForFixedUpdate_t896427542_il2cpp_TypeInfo_var;
extern const uint32_t YieldInstructionCache__cctor_m3108670335_MetadataUsageId;
extern "C"  void YieldInstructionCache__cctor_m3108670335 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (YieldInstructionCache__cctor_m3108670335_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		WaitForEndOfFrame_t1917318876 * L_0 = (WaitForEndOfFrame_t1917318876 *)il2cpp_codegen_object_new(WaitForEndOfFrame_t1917318876_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m4124201226(L_0, /*hidden argument*/NULL);
		((YieldInstructionCache_t1940766803_StaticFields*)YieldInstructionCache_t1940766803_il2cpp_TypeInfo_var->static_fields)->set_WaitForEndOfFrame_0(L_0);
		WaitForFixedUpdate_t896427542 * L_1 = (WaitForFixedUpdate_t896427542 *)il2cpp_codegen_object_new(WaitForFixedUpdate_t896427542_il2cpp_TypeInfo_var);
		WaitForFixedUpdate__ctor_m2916734308(L_1, /*hidden argument*/NULL);
		((YieldInstructionCache_t1940766803_StaticFields*)YieldInstructionCache_t1940766803_il2cpp_TypeInfo_var->static_fields)->set_WaitForFixedUpdate_1(L_1);
		return;
	}
}
// System.Object UnityEngine.Advertisements.MiniJSON.Json::Deserialize(System.String)
extern "C"  Il2CppObject * Json_Deserialize_m1234816281 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___json0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return NULL;
	}

IL_0008:
	{
		String_t* L_1 = ___json0;
		Il2CppObject * L_2 = Parser_Parse_m3258239493(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String UnityEngine.Advertisements.MiniJSON.Json::Serialize(System.Object)
extern "C"  String_t* Json_Serialize_m3560679896 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		String_t* L_1 = Serializer_Serialize_m2042365519(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.Advertisements.MiniJSON.Json/Parser::.ctor(System.String)
extern Il2CppClass* StringReader_t2229325051_il2cpp_TypeInfo_var;
extern const uint32_t Parser__ctor_m448531979_MetadataUsageId;
extern "C"  void Parser__ctor_m448531979 (Parser_t2383423552 * __this, String_t* ___jsonString0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Parser__ctor_m448531979_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___jsonString0;
		StringReader_t2229325051 * L_1 = (StringReader_t2229325051 *)il2cpp_codegen_object_new(StringReader_t2229325051_il2cpp_TypeInfo_var);
		StringReader__ctor_m1181104909(L_1, L_0, /*hidden argument*/NULL);
		__this->set_json_1(L_1);
		return;
	}
}
// System.Boolean UnityEngine.Advertisements.MiniJSON.Json/Parser::IsWordBreak(System.Char)
extern Il2CppClass* Char_t2778706699_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1159302032;
extern const uint32_t Parser_IsWordBreak_m4083015937_MetadataUsageId;
extern "C"  bool Parser_IsWordBreak_m4083015937 (Il2CppObject * __this /* static, unused */, uint16_t ___c0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Parser_IsWordBreak_m4083015937_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		uint16_t L_0 = ___c0;
		IL2CPP_RUNTIME_CLASS_INIT(Char_t2778706699_il2cpp_TypeInfo_var);
		bool L_1 = Char_IsWhiteSpace_m2745315955(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001e;
		}
	}
	{
		uint16_t L_2 = ___c0;
		NullCheck(_stringLiteral1159302032);
		int32_t L_3 = String_IndexOf_m2775210486(_stringLiteral1159302032, L_2, /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)((((int32_t)L_3) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_001f;
	}

IL_001e:
	{
		G_B3_0 = 1;
	}

IL_001f:
	{
		return (bool)G_B3_0;
	}
}
// System.Object UnityEngine.Advertisements.MiniJSON.Json/Parser::Parse(System.String)
extern Il2CppClass* Parser_t2383423552_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Parser_Parse_m3258239493_MetadataUsageId;
extern "C"  Il2CppObject * Parser_Parse_m3258239493 (Il2CppObject * __this /* static, unused */, String_t* ___jsonString0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Parser_Parse_m3258239493_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Parser_t2383423552 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___jsonString0;
		Parser_t2383423552 * L_1 = (Parser_t2383423552 *)il2cpp_codegen_object_new(Parser_t2383423552_il2cpp_TypeInfo_var);
		Parser__ctor_m448531979(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			Parser_t2383423552 * L_2 = V_0;
			NullCheck(L_2);
			Il2CppObject * L_3 = Parser_ParseValue_m819626102(L_2, /*hidden argument*/NULL);
			V_1 = L_3;
			IL2CPP_LEAVE(0x25, FINALLY_0018);
		}

IL_0013:
		{
			; // IL_0013: leave IL_0025
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0018;
	}

FINALLY_0018:
	{ // begin finally (depth: 1)
		{
			Parser_t2383423552 * L_4 = V_0;
			if (!L_4)
			{
				goto IL_0024;
			}
		}

IL_001e:
		{
			Parser_t2383423552 * L_5 = V_0;
			NullCheck(L_5);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_5);
		}

IL_0024:
		{
			IL2CPP_END_FINALLY(24)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(24)
	{
		IL2CPP_JUMP_TBL(0x25, IL_0025)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0025:
	{
		Il2CppObject * L_6 = V_1;
		return L_6;
	}
}
// System.Void UnityEngine.Advertisements.MiniJSON.Json/Parser::Dispose()
extern "C"  void Parser_Dispose_m2812578004 (Parser_t2383423552 * __this, const MethodInfo* method)
{
	{
		StringReader_t2229325051 * L_0 = __this->get_json_1();
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(4 /* System.Void System.IO.TextReader::Dispose() */, L_0);
		__this->set_json_1((StringReader_t2229325051 *)NULL);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Advertisements.MiniJSON.Json/Parser::ParseObject()
extern Il2CppClass* Dictionary_2_t2474804324_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2887377984_MethodInfo_var;
extern const uint32_t Parser_ParseObject_m2400284235_MetadataUsageId;
extern "C"  Dictionary_2_t2474804324 * Parser_ParseObject_m2400284235 (Parser_t2383423552 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Parser_ParseObject_m2400284235_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t2474804324 * V_0 = NULL;
	String_t* V_1 = NULL;
	int32_t V_2 = 0;
	{
		Dictionary_2_t2474804324 * L_0 = (Dictionary_2_t2474804324 *)il2cpp_codegen_object_new(Dictionary_2_t2474804324_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2887377984(L_0, /*hidden argument*/Dictionary_2__ctor_m2887377984_MethodInfo_var);
		V_0 = L_0;
		StringReader_t2229325051 * L_1 = __this->get_json_1();
		NullCheck(L_1);
		VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.IO.StringReader::Read() */, L_1);
	}

IL_0012:
	{
		int32_t L_2 = Parser_get_NextToken_m2157722235(__this, /*hidden argument*/NULL);
		V_2 = L_2;
		int32_t L_3 = V_2;
		if (L_3 == 0)
		{
			goto IL_0037;
		}
		if (L_3 == 1)
		{
			goto IL_002b;
		}
		if (L_3 == 2)
		{
			goto IL_003e;
		}
	}

IL_002b:
	{
		int32_t L_4 = V_2;
		if ((((int32_t)L_4) == ((int32_t)6)))
		{
			goto IL_0039;
		}
	}
	{
		goto IL_0040;
	}

IL_0037:
	{
		return (Dictionary_2_t2474804324 *)NULL;
	}

IL_0039:
	{
		goto IL_0012;
	}

IL_003e:
	{
		Dictionary_2_t2474804324 * L_5 = V_0;
		return L_5;
	}

IL_0040:
	{
		String_t* L_6 = Parser_ParseString_m3784989692(__this, /*hidden argument*/NULL);
		V_1 = L_6;
		String_t* L_7 = V_1;
		if (L_7)
		{
			goto IL_004f;
		}
	}
	{
		return (Dictionary_2_t2474804324 *)NULL;
	}

IL_004f:
	{
		int32_t L_8 = Parser_get_NextToken_m2157722235(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_8) == ((int32_t)5)))
		{
			goto IL_005d;
		}
	}
	{
		return (Dictionary_2_t2474804324 *)NULL;
	}

IL_005d:
	{
		StringReader_t2229325051 * L_9 = __this->get_json_1();
		NullCheck(L_9);
		VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.IO.StringReader::Read() */, L_9);
		Dictionary_2_t2474804324 * L_10 = V_0;
		String_t* L_11 = V_1;
		Il2CppObject * L_12 = Parser_ParseValue_m819626102(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		VirtActionInvoker2< String_t*, Il2CppObject * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::set_Item(!0,!1) */, L_10, L_11, L_12);
		goto IL_007b;
	}

IL_007b:
	{
		goto IL_0012;
	}
}
// System.Collections.Generic.List`1<System.Object> UnityEngine.Advertisements.MiniJSON.Json/Parser::ParseArray()
extern Il2CppClass* List_1_t1634065389_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m348833073_MethodInfo_var;
extern const uint32_t Parser_ParseArray_m3181970124_MetadataUsageId;
extern "C"  List_1_t1634065389 * Parser_ParseArray_m3181970124 (Parser_t2383423552 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Parser_ParseArray_m3181970124_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t1634065389 * V_0 = NULL;
	bool V_1 = false;
	int32_t V_2 = 0;
	Il2CppObject * V_3 = NULL;
	int32_t V_4 = 0;
	{
		List_1_t1634065389 * L_0 = (List_1_t1634065389 *)il2cpp_codegen_object_new(List_1_t1634065389_il2cpp_TypeInfo_var);
		List_1__ctor_m348833073(L_0, /*hidden argument*/List_1__ctor_m348833073_MethodInfo_var);
		V_0 = L_0;
		StringReader_t2229325051 * L_1 = __this->get_json_1();
		NullCheck(L_1);
		VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.IO.StringReader::Read() */, L_1);
		V_1 = (bool)1;
		goto IL_0066;
	}

IL_0019:
	{
		int32_t L_2 = Parser_get_NextToken_m2157722235(__this, /*hidden argument*/NULL);
		V_2 = L_2;
		int32_t L_3 = V_2;
		V_4 = L_3;
		int32_t L_4 = V_4;
		if (((int32_t)((int32_t)L_4-(int32_t)4)) == 0)
		{
			goto IL_004b;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)4)) == 1)
		{
			goto IL_0038;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)4)) == 2)
		{
			goto IL_0046;
		}
	}

IL_0038:
	{
		int32_t L_5 = V_4;
		if (!L_5)
		{
			goto IL_0044;
		}
	}
	{
		goto IL_0052;
	}

IL_0044:
	{
		return (List_1_t1634065389 *)NULL;
	}

IL_0046:
	{
		goto IL_0066;
	}

IL_004b:
	{
		V_1 = (bool)0;
		goto IL_0066;
	}

IL_0052:
	{
		int32_t L_6 = V_2;
		Il2CppObject * L_7 = Parser_ParseByToken_m3070971841(__this, L_6, /*hidden argument*/NULL);
		V_3 = L_7;
		List_1_t1634065389 * L_8 = V_0;
		Il2CppObject * L_9 = V_3;
		NullCheck(L_8);
		VirtActionInvoker1< Il2CppObject * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Object>::Add(!0) */, L_8, L_9);
		goto IL_0066;
	}

IL_0066:
	{
		bool L_10 = V_1;
		if (L_10)
		{
			goto IL_0019;
		}
	}
	{
		List_1_t1634065389 * L_11 = V_0;
		return L_11;
	}
}
// System.Object UnityEngine.Advertisements.MiniJSON.Json/Parser::ParseValue()
extern "C"  Il2CppObject * Parser_ParseValue_m819626102 (Parser_t2383423552 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = Parser_get_NextToken_m2157722235(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		Il2CppObject * L_2 = Parser_ParseByToken_m3070971841(__this, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Object UnityEngine.Advertisements.MiniJSON.Json/Parser::ParseByToken(UnityEngine.Advertisements.MiniJSON.Json/Parser/TOKEN)
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern const uint32_t Parser_ParseByToken_m3070971841_MetadataUsageId;
extern "C"  Il2CppObject * Parser_ParseByToken_m3070971841 (Parser_t2383423552 * __this, int32_t ___token0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Parser_ParseByToken_m3070971841_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___token0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 0)
		{
			goto IL_0049;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 1)
		{
			goto IL_0067;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 2)
		{
			goto IL_0050;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 3)
		{
			goto IL_0067;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 4)
		{
			goto IL_0067;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 5)
		{
			goto IL_0067;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 6)
		{
			goto IL_003b;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 7)
		{
			goto IL_0042;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 8)
		{
			goto IL_0057;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 9)
		{
			goto IL_005e;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 10)
		{
			goto IL_0065;
		}
	}
	{
		goto IL_0067;
	}

IL_003b:
	{
		String_t* L_2 = Parser_ParseString_m3784989692(__this, /*hidden argument*/NULL);
		return L_2;
	}

IL_0042:
	{
		Il2CppObject * L_3 = Parser_ParseNumber_m3457349702(__this, /*hidden argument*/NULL);
		return L_3;
	}

IL_0049:
	{
		Dictionary_2_t2474804324 * L_4 = Parser_ParseObject_m2400284235(__this, /*hidden argument*/NULL);
		return L_4;
	}

IL_0050:
	{
		List_1_t1634065389 * L_5 = Parser_ParseArray_m3181970124(__this, /*hidden argument*/NULL);
		return L_5;
	}

IL_0057:
	{
		bool L_6 = ((bool)1);
		Il2CppObject * L_7 = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &L_6);
		return L_7;
	}

IL_005e:
	{
		bool L_8 = ((bool)0);
		Il2CppObject * L_9 = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &L_8);
		return L_9;
	}

IL_0065:
	{
		return NULL;
	}

IL_0067:
	{
		return NULL;
	}
}
// System.String UnityEngine.Advertisements.MiniJSON.Json/Parser::ParseString()
extern Il2CppClass* StringBuilder_t3822575854_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t3416858730_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1097883944_il2cpp_TypeInfo_var;
extern const uint32_t Parser_ParseString_m3784989692_MetadataUsageId;
extern "C"  String_t* Parser_ParseString_m3784989692 (Parser_t2383423552 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Parser_ParseString_m3784989692_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t3822575854 * V_0 = NULL;
	uint16_t V_1 = 0x0;
	bool V_2 = false;
	CharU5BU5D_t3416858730* V_3 = NULL;
	int32_t V_4 = 0;
	uint16_t V_5 = 0x0;
	uint16_t V_6 = 0x0;
	{
		StringBuilder_t3822575854 * L_0 = (StringBuilder_t3822575854 *)il2cpp_codegen_object_new(StringBuilder_t3822575854_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m135953004(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		StringReader_t2229325051 * L_1 = __this->get_json_1();
		NullCheck(L_1);
		VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.IO.StringReader::Read() */, L_1);
		V_2 = (bool)1;
		goto IL_017c;
	}

IL_0019:
	{
		StringReader_t2229325051 * L_2 = __this->get_json_1();
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 System.IO.StringReader::Peek() */, L_2);
		if ((!(((uint32_t)L_3) == ((uint32_t)(-1)))))
		{
			goto IL_0031;
		}
	}
	{
		V_2 = (bool)0;
		goto IL_0182;
	}

IL_0031:
	{
		uint16_t L_4 = Parser_get_NextChar_m2602820865(__this, /*hidden argument*/NULL);
		V_1 = L_4;
		uint16_t L_5 = V_1;
		V_5 = L_5;
		uint16_t L_6 = V_5;
		if ((((int32_t)L_6) == ((int32_t)((int32_t)34))))
		{
			goto IL_0052;
		}
	}
	{
		uint16_t L_7 = V_5;
		if ((((int32_t)L_7) == ((int32_t)((int32_t)92))))
		{
			goto IL_0059;
		}
	}
	{
		goto IL_016f;
	}

IL_0052:
	{
		V_2 = (bool)0;
		goto IL_017c;
	}

IL_0059:
	{
		StringReader_t2229325051 * L_8 = __this->get_json_1();
		NullCheck(L_8);
		int32_t L_9 = VirtFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 System.IO.StringReader::Peek() */, L_8);
		if ((!(((uint32_t)L_9) == ((uint32_t)(-1)))))
		{
			goto IL_0071;
		}
	}
	{
		V_2 = (bool)0;
		goto IL_017c;
	}

IL_0071:
	{
		uint16_t L_10 = Parser_get_NextChar_m2602820865(__this, /*hidden argument*/NULL);
		V_1 = L_10;
		uint16_t L_11 = V_1;
		V_6 = L_11;
		uint16_t L_12 = V_6;
		if (((int32_t)((int32_t)L_12-(int32_t)((int32_t)110))) == 0)
		{
			goto IL_00ff;
		}
		if (((int32_t)((int32_t)L_12-(int32_t)((int32_t)110))) == 1)
		{
			goto IL_00a5;
		}
		if (((int32_t)((int32_t)L_12-(int32_t)((int32_t)110))) == 2)
		{
			goto IL_00a5;
		}
		if (((int32_t)((int32_t)L_12-(int32_t)((int32_t)110))) == 3)
		{
			goto IL_00a5;
		}
		if (((int32_t)((int32_t)L_12-(int32_t)((int32_t)110))) == 4)
		{
			goto IL_010d;
		}
		if (((int32_t)((int32_t)L_12-(int32_t)((int32_t)110))) == 5)
		{
			goto IL_00a5;
		}
		if (((int32_t)((int32_t)L_12-(int32_t)((int32_t)110))) == 6)
		{
			goto IL_011b;
		}
		if (((int32_t)((int32_t)L_12-(int32_t)((int32_t)110))) == 7)
		{
			goto IL_0129;
		}
	}

IL_00a5:
	{
		uint16_t L_13 = V_6;
		if ((((int32_t)L_13) == ((int32_t)((int32_t)34))))
		{
			goto IL_00d7;
		}
	}
	{
		uint16_t L_14 = V_6;
		if ((((int32_t)L_14) == ((int32_t)((int32_t)47))))
		{
			goto IL_00d7;
		}
	}
	{
		uint16_t L_15 = V_6;
		if ((((int32_t)L_15) == ((int32_t)((int32_t)92))))
		{
			goto IL_00d7;
		}
	}
	{
		uint16_t L_16 = V_6;
		if ((((int32_t)L_16) == ((int32_t)((int32_t)98))))
		{
			goto IL_00e4;
		}
	}
	{
		uint16_t L_17 = V_6;
		if ((((int32_t)L_17) == ((int32_t)((int32_t)102))))
		{
			goto IL_00f1;
		}
	}
	{
		goto IL_016a;
	}

IL_00d7:
	{
		StringBuilder_t3822575854 * L_18 = V_0;
		uint16_t L_19 = V_1;
		NullCheck(L_18);
		StringBuilder_Append_m2143093878(L_18, L_19, /*hidden argument*/NULL);
		goto IL_016a;
	}

IL_00e4:
	{
		StringBuilder_t3822575854 * L_20 = V_0;
		NullCheck(L_20);
		StringBuilder_Append_m2143093878(L_20, 8, /*hidden argument*/NULL);
		goto IL_016a;
	}

IL_00f1:
	{
		StringBuilder_t3822575854 * L_21 = V_0;
		NullCheck(L_21);
		StringBuilder_Append_m2143093878(L_21, ((int32_t)12), /*hidden argument*/NULL);
		goto IL_016a;
	}

IL_00ff:
	{
		StringBuilder_t3822575854 * L_22 = V_0;
		NullCheck(L_22);
		StringBuilder_Append_m2143093878(L_22, ((int32_t)10), /*hidden argument*/NULL);
		goto IL_016a;
	}

IL_010d:
	{
		StringBuilder_t3822575854 * L_23 = V_0;
		NullCheck(L_23);
		StringBuilder_Append_m2143093878(L_23, ((int32_t)13), /*hidden argument*/NULL);
		goto IL_016a;
	}

IL_011b:
	{
		StringBuilder_t3822575854 * L_24 = V_0;
		NullCheck(L_24);
		StringBuilder_Append_m2143093878(L_24, ((int32_t)9), /*hidden argument*/NULL);
		goto IL_016a;
	}

IL_0129:
	{
		V_3 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)4));
		V_4 = 0;
		goto IL_0148;
	}

IL_0138:
	{
		CharU5BU5D_t3416858730* L_25 = V_3;
		int32_t L_26 = V_4;
		uint16_t L_27 = Parser_get_NextChar_m2602820865(__this, /*hidden argument*/NULL);
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, L_26);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(L_26), (uint16_t)L_27);
		int32_t L_28 = V_4;
		V_4 = ((int32_t)((int32_t)L_28+(int32_t)1));
	}

IL_0148:
	{
		int32_t L_29 = V_4;
		if ((((int32_t)L_29) < ((int32_t)4)))
		{
			goto IL_0138;
		}
	}
	{
		StringBuilder_t3822575854 * L_30 = V_0;
		CharU5BU5D_t3416858730* L_31 = V_3;
		String_t* L_32 = String_CreateString_m578950865(NULL, L_31, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		int32_t L_33 = Convert_ToInt32_m2645435816(NULL /*static, unused*/, L_32, ((int32_t)16), /*hidden argument*/NULL);
		NullCheck(L_30);
		StringBuilder_Append_m2143093878(L_30, (((int32_t)((uint16_t)L_33))), /*hidden argument*/NULL);
		goto IL_016a;
	}

IL_016a:
	{
		goto IL_017c;
	}

IL_016f:
	{
		StringBuilder_t3822575854 * L_34 = V_0;
		uint16_t L_35 = V_1;
		NullCheck(L_34);
		StringBuilder_Append_m2143093878(L_34, L_35, /*hidden argument*/NULL);
		goto IL_017c;
	}

IL_017c:
	{
		bool L_36 = V_2;
		if (L_36)
		{
			goto IL_0019;
		}
	}

IL_0182:
	{
		StringBuilder_t3822575854 * L_37 = V_0;
		NullCheck(L_37);
		String_t* L_38 = StringBuilder_ToString_m350379841(L_37, /*hidden argument*/NULL);
		return L_38;
	}
}
// System.Object UnityEngine.Advertisements.MiniJSON.Json/Parser::ParseNumber()
extern Il2CppClass* CultureInfo_t3603717042_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t2847414882_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern const uint32_t Parser_ParseNumber_m3457349702_MetadataUsageId;
extern "C"  Il2CppObject * Parser_ParseNumber_m3457349702 (Parser_t2383423552 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Parser_ParseNumber_m3457349702_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	int64_t V_1 = 0;
	double V_2 = 0.0;
	{
		String_t* L_0 = Parser_get_NextWord_m689191536(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = V_0;
		NullCheck(L_1);
		int32_t L_2 = String_IndexOf_m2775210486(L_1, ((int32_t)46), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		String_t* L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3603717042_il2cpp_TypeInfo_var);
		CultureInfo_t3603717042 * L_4 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		Int64_TryParse_m1199101227(NULL /*static, unused*/, L_3, ((int32_t)511), L_4, (&V_1), /*hidden argument*/NULL);
		int64_t L_5 = V_1;
		int64_t L_6 = L_5;
		Il2CppObject * L_7 = Box(Int64_t2847414882_il2cpp_TypeInfo_var, &L_6);
		return L_7;
	}

IL_002f:
	{
		String_t* L_8 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3603717042_il2cpp_TypeInfo_var);
		CultureInfo_t3603717042 * L_9 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		Double_TryParse_m707798013(NULL /*static, unused*/, L_8, ((int32_t)511), L_9, (&V_2), /*hidden argument*/NULL);
		double L_10 = V_2;
		double L_11 = L_10;
		Il2CppObject * L_12 = Box(Double_t534516614_il2cpp_TypeInfo_var, &L_11);
		return L_12;
	}
}
// System.Void UnityEngine.Advertisements.MiniJSON.Json/Parser::EatWhitespace()
extern Il2CppClass* Char_t2778706699_il2cpp_TypeInfo_var;
extern const uint32_t Parser_EatWhitespace_m3600468522_MetadataUsageId;
extern "C"  void Parser_EatWhitespace_m3600468522 (Parser_t2383423552 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Parser_EatWhitespace_m3600468522_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		goto IL_0027;
	}

IL_0005:
	{
		StringReader_t2229325051 * L_0 = __this->get_json_1();
		NullCheck(L_0);
		VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.IO.StringReader::Read() */, L_0);
		StringReader_t2229325051 * L_1 = __this->get_json_1();
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 System.IO.StringReader::Peek() */, L_1);
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_0027;
		}
	}
	{
		goto IL_0037;
	}

IL_0027:
	{
		uint16_t L_3 = Parser_get_PeekChar_m4058537321(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t2778706699_il2cpp_TypeInfo_var);
		bool L_4 = Char_IsWhiteSpace_m2745315955(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0005;
		}
	}

IL_0037:
	{
		return;
	}
}
// System.Char UnityEngine.Advertisements.MiniJSON.Json/Parser::get_PeekChar()
extern Il2CppClass* Convert_t1097883944_il2cpp_TypeInfo_var;
extern const uint32_t Parser_get_PeekChar_m4058537321_MetadataUsageId;
extern "C"  uint16_t Parser_get_PeekChar_m4058537321 (Parser_t2383423552 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Parser_get_PeekChar_m4058537321_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringReader_t2229325051 * L_0 = __this->get_json_1();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 System.IO.StringReader::Peek() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		uint16_t L_2 = Convert_ToChar_m353236550(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Char UnityEngine.Advertisements.MiniJSON.Json/Parser::get_NextChar()
extern Il2CppClass* Convert_t1097883944_il2cpp_TypeInfo_var;
extern const uint32_t Parser_get_NextChar_m2602820865_MetadataUsageId;
extern "C"  uint16_t Parser_get_NextChar_m2602820865 (Parser_t2383423552 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Parser_get_NextChar_m2602820865_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringReader_t2229325051 * L_0 = __this->get_json_1();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.IO.StringReader::Read() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		uint16_t L_2 = Convert_ToChar_m353236550(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String UnityEngine.Advertisements.MiniJSON.Json/Parser::get_NextWord()
extern Il2CppClass* StringBuilder_t3822575854_il2cpp_TypeInfo_var;
extern const uint32_t Parser_get_NextWord_m689191536_MetadataUsageId;
extern "C"  String_t* Parser_get_NextWord_m689191536 (Parser_t2383423552 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Parser_get_NextWord_m689191536_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t3822575854 * V_0 = NULL;
	{
		StringBuilder_t3822575854 * L_0 = (StringBuilder_t3822575854 *)il2cpp_codegen_object_new(StringBuilder_t3822575854_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m135953004(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_002e;
	}

IL_000b:
	{
		StringBuilder_t3822575854 * L_1 = V_0;
		uint16_t L_2 = Parser_get_NextChar_m2602820865(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		StringBuilder_Append_m2143093878(L_1, L_2, /*hidden argument*/NULL);
		StringReader_t2229325051 * L_3 = __this->get_json_1();
		NullCheck(L_3);
		int32_t L_4 = VirtFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 System.IO.StringReader::Peek() */, L_3);
		if ((!(((uint32_t)L_4) == ((uint32_t)(-1)))))
		{
			goto IL_002e;
		}
	}
	{
		goto IL_003e;
	}

IL_002e:
	{
		uint16_t L_5 = Parser_get_PeekChar_m4058537321(__this, /*hidden argument*/NULL);
		bool L_6 = Parser_IsWordBreak_m4083015937(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_000b;
		}
	}

IL_003e:
	{
		StringBuilder_t3822575854 * L_7 = V_0;
		NullCheck(L_7);
		String_t* L_8 = StringBuilder_ToString_m350379841(L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// UnityEngine.Advertisements.MiniJSON.Json/Parser/TOKEN UnityEngine.Advertisements.MiniJSON.Json/Parser::get_NextToken()
extern Il2CppClass* Parser_t2383423552_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t190145395_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1958628151_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral97196323;
extern Il2CppCodeGenString* _stringLiteral3569038;
extern Il2CppCodeGenString* _stringLiteral3392903;
extern const uint32_t Parser_get_NextToken_m2157722235_MetadataUsageId;
extern "C"  int32_t Parser_get_NextToken_m2157722235 (Parser_t2383423552 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Parser_get_NextToken_m2157722235_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0x0;
	String_t* V_1 = NULL;
	Dictionary_2_t190145395 * V_2 = NULL;
	int32_t V_3 = 0;
	{
		Parser_EatWhitespace_m3600468522(__this, /*hidden argument*/NULL);
		StringReader_t2229325051 * L_0 = __this->get_json_1();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 System.IO.StringReader::Peek() */, L_0);
		if ((!(((uint32_t)L_1) == ((uint32_t)(-1)))))
		{
			goto IL_0019;
		}
	}
	{
		return (int32_t)(0);
	}

IL_0019:
	{
		uint16_t L_2 = Parser_get_PeekChar_m4058537321(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		uint16_t L_3 = V_0;
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)34))) == 0)
		{
			goto IL_00ea;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)34))) == 1)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)34))) == 2)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)34))) == 3)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)34))) == 4)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)34))) == 5)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)34))) == 6)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)34))) == 7)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)34))) == 8)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)34))) == 9)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)34))) == 10)
		{
			goto IL_00dc;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)34))) == 11)
		{
			goto IL_00ee;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)34))) == 12)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)34))) == 13)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)34))) == 14)
		{
			goto IL_00ee;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)34))) == 15)
		{
			goto IL_00ee;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)34))) == 16)
		{
			goto IL_00ee;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)34))) == 17)
		{
			goto IL_00ee;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)34))) == 18)
		{
			goto IL_00ee;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)34))) == 19)
		{
			goto IL_00ee;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)34))) == 20)
		{
			goto IL_00ee;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)34))) == 21)
		{
			goto IL_00ee;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)34))) == 22)
		{
			goto IL_00ee;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)34))) == 23)
		{
			goto IL_00ee;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)34))) == 24)
		{
			goto IL_00ec;
		}
	}

IL_008d:
	{
		uint16_t L_4 = V_0;
		if (((int32_t)((int32_t)L_4-(int32_t)((int32_t)91))) == 0)
		{
			goto IL_00cc;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)((int32_t)91))) == 1)
		{
			goto IL_00a2;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)((int32_t)91))) == 2)
		{
			goto IL_00ce;
		}
	}

IL_00a2:
	{
		uint16_t L_5 = V_0;
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)123))) == 0)
		{
			goto IL_00bc;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)123))) == 1)
		{
			goto IL_00f0;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)123))) == 2)
		{
			goto IL_00be;
		}
	}
	{
		goto IL_00f0;
	}

IL_00bc:
	{
		return (int32_t)(1);
	}

IL_00be:
	{
		StringReader_t2229325051 * L_6 = __this->get_json_1();
		NullCheck(L_6);
		VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.IO.StringReader::Read() */, L_6);
		return (int32_t)(2);
	}

IL_00cc:
	{
		return (int32_t)(3);
	}

IL_00ce:
	{
		StringReader_t2229325051 * L_7 = __this->get_json_1();
		NullCheck(L_7);
		VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.IO.StringReader::Read() */, L_7);
		return (int32_t)(4);
	}

IL_00dc:
	{
		StringReader_t2229325051 * L_8 = __this->get_json_1();
		NullCheck(L_8);
		VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.IO.StringReader::Read() */, L_8);
		return (int32_t)(6);
	}

IL_00ea:
	{
		return (int32_t)(7);
	}

IL_00ec:
	{
		return (int32_t)(5);
	}

IL_00ee:
	{
		return (int32_t)(8);
	}

IL_00f0:
	{
		String_t* L_9 = Parser_get_NextWord_m689191536(__this, /*hidden argument*/NULL);
		V_1 = L_9;
		String_t* L_10 = V_1;
		if (!L_10)
		{
			goto IL_016a;
		}
	}
	{
		Dictionary_2_t190145395 * L_11 = ((Parser_t2383423552_StaticFields*)Parser_t2383423552_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24map0_2();
		if (L_11)
		{
			goto IL_0138;
		}
	}
	{
		Dictionary_2_t190145395 * L_12 = (Dictionary_2_t190145395 *)il2cpp_codegen_object_new(Dictionary_2_t190145395_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1958628151(L_12, 3, /*hidden argument*/Dictionary_2__ctor_m1958628151_MethodInfo_var);
		V_2 = L_12;
		Dictionary_2_t190145395 * L_13 = V_2;
		NullCheck(L_13);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_13, _stringLiteral97196323, 0);
		Dictionary_2_t190145395 * L_14 = V_2;
		NullCheck(L_14);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_14, _stringLiteral3569038, 1);
		Dictionary_2_t190145395 * L_15 = V_2;
		NullCheck(L_15);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_15, _stringLiteral3392903, 2);
		Dictionary_2_t190145395 * L_16 = V_2;
		((Parser_t2383423552_StaticFields*)Parser_t2383423552_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__switchU24map0_2(L_16);
	}

IL_0138:
	{
		Dictionary_2_t190145395 * L_17 = ((Parser_t2383423552_StaticFields*)Parser_t2383423552_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24map0_2();
		String_t* L_18 = V_1;
		NullCheck(L_17);
		bool L_19 = VirtFuncInvoker2< bool, String_t*, int32_t* >::Invoke(21 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::TryGetValue(!0,!1&) */, L_17, L_18, (&V_3));
		if (!L_19)
		{
			goto IL_016a;
		}
	}
	{
		int32_t L_20 = V_3;
		if (L_20 == 0)
		{
			goto IL_0161;
		}
		if (L_20 == 1)
		{
			goto IL_0164;
		}
		if (L_20 == 2)
		{
			goto IL_0167;
		}
	}
	{
		goto IL_016a;
	}

IL_0161:
	{
		return (int32_t)(((int32_t)10));
	}

IL_0164:
	{
		return (int32_t)(((int32_t)9));
	}

IL_0167:
	{
		return (int32_t)(((int32_t)11));
	}

IL_016a:
	{
		return (int32_t)(0);
	}
}
// System.Void UnityEngine.Advertisements.MiniJSON.Json/Serializer::.ctor()
extern Il2CppClass* StringBuilder_t3822575854_il2cpp_TypeInfo_var;
extern const uint32_t Serializer__ctor_m1385614532_MetadataUsageId;
extern "C"  void Serializer__ctor_m1385614532 (Serializer_t1395478962 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Serializer__ctor_m1385614532_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_0 = (StringBuilder_t3822575854 *)il2cpp_codegen_object_new(StringBuilder_t3822575854_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m135953004(L_0, /*hidden argument*/NULL);
		__this->set_builder_0(L_0);
		return;
	}
}
// System.String UnityEngine.Advertisements.MiniJSON.Json/Serializer::Serialize(System.Object)
extern Il2CppClass* Serializer_t1395478962_il2cpp_TypeInfo_var;
extern const uint32_t Serializer_Serialize_m2042365519_MetadataUsageId;
extern "C"  String_t* Serializer_Serialize_m2042365519 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Serializer_Serialize_m2042365519_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Serializer_t1395478962 * V_0 = NULL;
	{
		Serializer_t1395478962 * L_0 = (Serializer_t1395478962 *)il2cpp_codegen_object_new(Serializer_t1395478962_il2cpp_TypeInfo_var);
		Serializer__ctor_m1385614532(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Serializer_t1395478962 * L_1 = V_0;
		Il2CppObject * L_2 = ___obj0;
		NullCheck(L_1);
		Serializer_SerializeValue_m3127258563(L_1, L_2, /*hidden argument*/NULL);
		Serializer_t1395478962 * L_3 = V_0;
		NullCheck(L_3);
		StringBuilder_t3822575854 * L_4 = L_3->get_builder_0();
		NullCheck(L_4);
		String_t* L_5 = StringBuilder_ToString_m350379841(L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void UnityEngine.Advertisements.MiniJSON.Json/Serializer::SerializeValue(System.Object)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern Il2CppClass* IList_t1612618265_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_t1654916945_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t2778706699_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3392903;
extern Il2CppCodeGenString* _stringLiteral3569038;
extern Il2CppCodeGenString* _stringLiteral97196323;
extern const uint32_t Serializer_SerializeValue_m3127258563_MetadataUsageId;
extern "C"  void Serializer_SerializeValue_m3127258563 (Serializer_t1395478962 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Serializer_SerializeValue_m3127258563_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	String_t* V_2 = NULL;
	StringBuilder_t3822575854 * G_B7_0 = NULL;
	StringBuilder_t3822575854 * G_B6_0 = NULL;
	String_t* G_B8_0 = NULL;
	StringBuilder_t3822575854 * G_B8_1 = NULL;
	{
		Il2CppObject * L_0 = ___value0;
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		StringBuilder_t3822575854 * L_1 = __this->get_builder_0();
		NullCheck(L_1);
		StringBuilder_Append_m3898090075(L_1, _stringLiteral3392903, /*hidden argument*/NULL);
		goto IL_00c6;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___value0;
		String_t* L_3 = ((String_t*)IsInstSealed(L_2, String_t_il2cpp_TypeInfo_var));
		V_2 = L_3;
		if (!L_3)
		{
			goto IL_0035;
		}
	}
	{
		String_t* L_4 = V_2;
		Serializer_SerializeString_m2859448943(__this, L_4, /*hidden argument*/NULL);
		goto IL_00c6;
	}

IL_0035:
	{
		Il2CppObject * L_5 = ___value0;
		if (!((Il2CppObject *)IsInstSealed(L_5, Boolean_t211005341_il2cpp_TypeInfo_var)))
		{
			goto IL_006b;
		}
	}
	{
		StringBuilder_t3822575854 * L_6 = __this->get_builder_0();
		Il2CppObject * L_7 = ___value0;
		G_B6_0 = L_6;
		if (!((*(bool*)((bool*)UnBox (L_7, Boolean_t211005341_il2cpp_TypeInfo_var)))))
		{
			G_B7_0 = L_6;
			goto IL_005b;
		}
	}
	{
		G_B8_0 = _stringLiteral3569038;
		G_B8_1 = G_B6_0;
		goto IL_0060;
	}

IL_005b:
	{
		G_B8_0 = _stringLiteral97196323;
		G_B8_1 = G_B7_0;
	}

IL_0060:
	{
		NullCheck(G_B8_1);
		StringBuilder_Append_m3898090075(G_B8_1, G_B8_0, /*hidden argument*/NULL);
		goto IL_00c6;
	}

IL_006b:
	{
		Il2CppObject * L_8 = ___value0;
		Il2CppObject * L_9 = ((Il2CppObject *)IsInst(L_8, IList_t1612618265_il2cpp_TypeInfo_var));
		V_0 = L_9;
		if (!L_9)
		{
			goto IL_0084;
		}
	}
	{
		Il2CppObject * L_10 = V_0;
		Serializer_SerializeArray_m2650912586(__this, L_10, /*hidden argument*/NULL);
		goto IL_00c6;
	}

IL_0084:
	{
		Il2CppObject * L_11 = ___value0;
		Il2CppObject * L_12 = ((Il2CppObject *)IsInst(L_11, IDictionary_t1654916945_il2cpp_TypeInfo_var));
		V_1 = L_12;
		if (!L_12)
		{
			goto IL_009d;
		}
	}
	{
		Il2CppObject * L_13 = V_1;
		Serializer_SerializeObject_m580155866(__this, L_13, /*hidden argument*/NULL);
		goto IL_00c6;
	}

IL_009d:
	{
		Il2CppObject * L_14 = ___value0;
		if (!((Il2CppObject *)IsInstSealed(L_14, Char_t2778706699_il2cpp_TypeInfo_var)))
		{
			goto IL_00bf;
		}
	}
	{
		Il2CppObject * L_15 = ___value0;
		String_t* L_16 = String_CreateString_m356585284(NULL, ((*(uint16_t*)((uint16_t*)UnBox (L_15, Char_t2778706699_il2cpp_TypeInfo_var)))), 1, /*hidden argument*/NULL);
		Serializer_SerializeString_m2859448943(__this, L_16, /*hidden argument*/NULL);
		goto IL_00c6;
	}

IL_00bf:
	{
		Il2CppObject * L_17 = ___value0;
		Serializer_SerializeOther_m2190916(__this, L_17, /*hidden argument*/NULL);
	}

IL_00c6:
	{
		return;
	}
}
// System.Void UnityEngine.Advertisements.MiniJSON.Json/Serializer::SerializeObject(System.Collections.IDictionary)
extern Il2CppClass* IDictionary_t1654916945_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_t287189635_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Serializer_SerializeObject_m580155866_MetadataUsageId;
extern "C"  void Serializer_SerializeObject_m580155866 (Serializer_t1395478962 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Serializer_SerializeObject_m580155866_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (bool)1;
		StringBuilder_t3822575854 * L_0 = __this->get_builder_0();
		NullCheck(L_0);
		StringBuilder_Append_m2143093878(L_0, ((int32_t)123), /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___obj0;
		NullCheck(L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(4 /* System.Collections.ICollection System.Collections.IDictionary::get_Keys() */, IDictionary_t1654916945_il2cpp_TypeInfo_var, L_1);
		NullCheck(L_2);
		Il2CppObject * L_3 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t287189635_il2cpp_TypeInfo_var, L_2);
		V_2 = L_3;
	}

IL_001c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0065;
		}

IL_0021:
		{
			Il2CppObject * L_4 = V_2;
			NullCheck(L_4);
			Il2CppObject * L_5 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_4);
			V_1 = L_5;
			bool L_6 = V_0;
			if (L_6)
			{
				goto IL_003c;
			}
		}

IL_002e:
		{
			StringBuilder_t3822575854 * L_7 = __this->get_builder_0();
			NullCheck(L_7);
			StringBuilder_Append_m2143093878(L_7, ((int32_t)44), /*hidden argument*/NULL);
		}

IL_003c:
		{
			Il2CppObject * L_8 = V_1;
			NullCheck(L_8);
			String_t* L_9 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_8);
			Serializer_SerializeString_m2859448943(__this, L_9, /*hidden argument*/NULL);
			StringBuilder_t3822575854 * L_10 = __this->get_builder_0();
			NullCheck(L_10);
			StringBuilder_Append_m2143093878(L_10, ((int32_t)58), /*hidden argument*/NULL);
			Il2CppObject * L_11 = ___obj0;
			Il2CppObject * L_12 = V_1;
			NullCheck(L_11);
			Il2CppObject * L_13 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t1654916945_il2cpp_TypeInfo_var, L_11, L_12);
			Serializer_SerializeValue_m3127258563(__this, L_13, /*hidden argument*/NULL);
			V_0 = (bool)0;
		}

IL_0065:
		{
			Il2CppObject * L_14 = V_2;
			NullCheck(L_14);
			bool L_15 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_14);
			if (L_15)
			{
				goto IL_0021;
			}
		}

IL_0070:
		{
			IL2CPP_LEAVE(0x87, FINALLY_0075);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0075;
	}

FINALLY_0075:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_16 = V_2;
			V_3 = ((Il2CppObject *)IsInst(L_16, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			Il2CppObject * L_17 = V_3;
			if (L_17)
			{
				goto IL_0080;
			}
		}

IL_007f:
		{
			IL2CPP_END_FINALLY(117)
		}

IL_0080:
		{
			Il2CppObject * L_18 = V_3;
			NullCheck(L_18);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_18);
			IL2CPP_END_FINALLY(117)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(117)
	{
		IL2CPP_JUMP_TBL(0x87, IL_0087)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0087:
	{
		StringBuilder_t3822575854 * L_19 = __this->get_builder_0();
		NullCheck(L_19);
		StringBuilder_Append_m2143093878(L_19, ((int32_t)125), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Advertisements.MiniJSON.Json/Serializer::SerializeArray(System.Collections.IList)
extern Il2CppClass* IEnumerable_t287189635_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Serializer_SerializeArray_m2650912586_MetadataUsageId;
extern "C"  void Serializer_SerializeArray_m2650912586 (Serializer_t1395478962 * __this, Il2CppObject * ___anArray0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Serializer_SerializeArray_m2650912586_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		StringBuilder_t3822575854 * L_0 = __this->get_builder_0();
		NullCheck(L_0);
		StringBuilder_Append_m2143093878(L_0, ((int32_t)91), /*hidden argument*/NULL);
		V_0 = (bool)1;
		Il2CppObject * L_1 = ___anArray0;
		NullCheck(L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t287189635_il2cpp_TypeInfo_var, L_1);
		V_2 = L_2;
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0040;
		}

IL_001c:
		{
			Il2CppObject * L_3 = V_2;
			NullCheck(L_3);
			Il2CppObject * L_4 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_3);
			V_1 = L_4;
			bool L_5 = V_0;
			if (L_5)
			{
				goto IL_0037;
			}
		}

IL_0029:
		{
			StringBuilder_t3822575854 * L_6 = __this->get_builder_0();
			NullCheck(L_6);
			StringBuilder_Append_m2143093878(L_6, ((int32_t)44), /*hidden argument*/NULL);
		}

IL_0037:
		{
			Il2CppObject * L_7 = V_1;
			Serializer_SerializeValue_m3127258563(__this, L_7, /*hidden argument*/NULL);
			V_0 = (bool)0;
		}

IL_0040:
		{
			Il2CppObject * L_8 = V_2;
			NullCheck(L_8);
			bool L_9 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_8);
			if (L_9)
			{
				goto IL_001c;
			}
		}

IL_004b:
		{
			IL2CPP_LEAVE(0x62, FINALLY_0050);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0050;
	}

FINALLY_0050:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_10 = V_2;
			V_3 = ((Il2CppObject *)IsInst(L_10, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			Il2CppObject * L_11 = V_3;
			if (L_11)
			{
				goto IL_005b;
			}
		}

IL_005a:
		{
			IL2CPP_END_FINALLY(80)
		}

IL_005b:
		{
			Il2CppObject * L_12 = V_3;
			NullCheck(L_12);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_12);
			IL2CPP_END_FINALLY(80)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(80)
	{
		IL2CPP_JUMP_TBL(0x62, IL_0062)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0062:
	{
		StringBuilder_t3822575854 * L_13 = __this->get_builder_0();
		NullCheck(L_13);
		StringBuilder_Append_m2143093878(L_13, ((int32_t)93), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Advertisements.MiniJSON.Json/Serializer::SerializeString(System.String)
extern Il2CppClass* Convert_t1097883944_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2886;
extern Il2CppCodeGenString* _stringLiteral2944;
extern Il2CppCodeGenString* _stringLiteral2950;
extern Il2CppCodeGenString* _stringLiteral2954;
extern Il2CppCodeGenString* _stringLiteral2962;
extern Il2CppCodeGenString* _stringLiteral2966;
extern Il2CppCodeGenString* _stringLiteral2968;
extern Il2CppCodeGenString* _stringLiteral2969;
extern Il2CppCodeGenString* _stringLiteral3772;
extern const uint32_t Serializer_SerializeString_m2859448943_MetadataUsageId;
extern "C"  void Serializer_SerializeString_m2859448943 (Serializer_t1395478962 * __this, String_t* ___str0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Serializer_SerializeString_m2859448943_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	CharU5BU5D_t3416858730* V_0 = NULL;
	uint16_t V_1 = 0x0;
	CharU5BU5D_t3416858730* V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	uint16_t V_5 = 0x0;
	{
		StringBuilder_t3822575854 * L_0 = __this->get_builder_0();
		NullCheck(L_0);
		StringBuilder_Append_m2143093878(L_0, ((int32_t)34), /*hidden argument*/NULL);
		String_t* L_1 = ___str0;
		NullCheck(L_1);
		CharU5BU5D_t3416858730* L_2 = String_ToCharArray_m1208288742(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		CharU5BU5D_t3416858730* L_3 = V_0;
		V_2 = L_3;
		V_3 = 0;
		goto IL_0155;
	}

IL_001e:
	{
		CharU5BU5D_t3416858730* L_4 = V_2;
		int32_t L_5 = V_3;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		V_1 = ((L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6)));
		uint16_t L_7 = V_1;
		V_5 = L_7;
		uint16_t L_8 = V_5;
		if (((int32_t)((int32_t)L_8-(int32_t)8)) == 0)
		{
			goto IL_0089;
		}
		if (((int32_t)((int32_t)L_8-(int32_t)8)) == 1)
		{
			goto IL_00e1;
		}
		if (((int32_t)((int32_t)L_8-(int32_t)8)) == 2)
		{
			goto IL_00b5;
		}
		if (((int32_t)((int32_t)L_8-(int32_t)8)) == 3)
		{
			goto IL_0046;
		}
		if (((int32_t)((int32_t)L_8-(int32_t)8)) == 4)
		{
			goto IL_009f;
		}
		if (((int32_t)((int32_t)L_8-(int32_t)8)) == 5)
		{
			goto IL_00cb;
		}
	}

IL_0046:
	{
		uint16_t L_9 = V_5;
		if ((((int32_t)L_9) == ((int32_t)((int32_t)34))))
		{
			goto IL_005d;
		}
	}
	{
		uint16_t L_10 = V_5;
		if ((((int32_t)L_10) == ((int32_t)((int32_t)92))))
		{
			goto IL_0073;
		}
	}
	{
		goto IL_00f7;
	}

IL_005d:
	{
		StringBuilder_t3822575854 * L_11 = __this->get_builder_0();
		NullCheck(L_11);
		StringBuilder_Append_m3898090075(L_11, _stringLiteral2886, /*hidden argument*/NULL);
		goto IL_0151;
	}

IL_0073:
	{
		StringBuilder_t3822575854 * L_12 = __this->get_builder_0();
		NullCheck(L_12);
		StringBuilder_Append_m3898090075(L_12, _stringLiteral2944, /*hidden argument*/NULL);
		goto IL_0151;
	}

IL_0089:
	{
		StringBuilder_t3822575854 * L_13 = __this->get_builder_0();
		NullCheck(L_13);
		StringBuilder_Append_m3898090075(L_13, _stringLiteral2950, /*hidden argument*/NULL);
		goto IL_0151;
	}

IL_009f:
	{
		StringBuilder_t3822575854 * L_14 = __this->get_builder_0();
		NullCheck(L_14);
		StringBuilder_Append_m3898090075(L_14, _stringLiteral2954, /*hidden argument*/NULL);
		goto IL_0151;
	}

IL_00b5:
	{
		StringBuilder_t3822575854 * L_15 = __this->get_builder_0();
		NullCheck(L_15);
		StringBuilder_Append_m3898090075(L_15, _stringLiteral2962, /*hidden argument*/NULL);
		goto IL_0151;
	}

IL_00cb:
	{
		StringBuilder_t3822575854 * L_16 = __this->get_builder_0();
		NullCheck(L_16);
		StringBuilder_Append_m3898090075(L_16, _stringLiteral2966, /*hidden argument*/NULL);
		goto IL_0151;
	}

IL_00e1:
	{
		StringBuilder_t3822575854 * L_17 = __this->get_builder_0();
		NullCheck(L_17);
		StringBuilder_Append_m3898090075(L_17, _stringLiteral2968, /*hidden argument*/NULL);
		goto IL_0151;
	}

IL_00f7:
	{
		uint16_t L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		int32_t L_19 = Convert_ToInt32_m100832938(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		V_4 = L_19;
		int32_t L_20 = V_4;
		if ((((int32_t)L_20) < ((int32_t)((int32_t)32))))
		{
			goto IL_0123;
		}
	}
	{
		int32_t L_21 = V_4;
		if ((((int32_t)L_21) > ((int32_t)((int32_t)126))))
		{
			goto IL_0123;
		}
	}
	{
		StringBuilder_t3822575854 * L_22 = __this->get_builder_0();
		uint16_t L_23 = V_1;
		NullCheck(L_22);
		StringBuilder_Append_m2143093878(L_22, L_23, /*hidden argument*/NULL);
		goto IL_014c;
	}

IL_0123:
	{
		StringBuilder_t3822575854 * L_24 = __this->get_builder_0();
		NullCheck(L_24);
		StringBuilder_Append_m3898090075(L_24, _stringLiteral2969, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_25 = __this->get_builder_0();
		String_t* L_26 = Int32_ToString_m3853485906((&V_4), _stringLiteral3772, /*hidden argument*/NULL);
		NullCheck(L_25);
		StringBuilder_Append_m3898090075(L_25, L_26, /*hidden argument*/NULL);
	}

IL_014c:
	{
		goto IL_0151;
	}

IL_0151:
	{
		int32_t L_27 = V_3;
		V_3 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_0155:
	{
		int32_t L_28 = V_3;
		CharU5BU5D_t3416858730* L_29 = V_2;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_29)->max_length)))))))
		{
			goto IL_001e;
		}
	}
	{
		StringBuilder_t3822575854 * L_30 = __this->get_builder_0();
		NullCheck(L_30);
		StringBuilder_Append_m2143093878(L_30, ((int32_t)34), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Advertisements.MiniJSON.Json/Serializer::SerializeOther(System.Object)
extern Il2CppClass* Single_t958209021_il2cpp_TypeInfo_var;
extern Il2CppClass* CultureInfo_t3603717042_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt32_t985925326_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t2847414882_il2cpp_TypeInfo_var;
extern Il2CppClass* SByte_t2855346064_il2cpp_TypeInfo_var;
extern Il2CppClass* Byte_t2778693821_il2cpp_TypeInfo_var;
extern Il2CppClass* Int16_t2847414729_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt16_t985925268_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt64_t985925421_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern Il2CppClass* Decimal_t1688557254_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1097883944_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral82;
extern const uint32_t Serializer_SerializeOther_m2190916_MetadataUsageId;
extern "C"  void Serializer_SerializeOther_m2190916 (Serializer_t1395478962 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Serializer_SerializeOther_m2190916_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	double V_1 = 0.0;
	{
		Il2CppObject * L_0 = ___value0;
		if (!((Il2CppObject *)IsInstSealed(L_0, Single_t958209021_il2cpp_TypeInfo_var)))
		{
			goto IL_0034;
		}
	}
	{
		StringBuilder_t3822575854 * L_1 = __this->get_builder_0();
		Il2CppObject * L_2 = ___value0;
		V_0 = ((*(float*)((float*)UnBox (L_2, Single_t958209021_il2cpp_TypeInfo_var))));
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3603717042_il2cpp_TypeInfo_var);
		CultureInfo_t3603717042 * L_3 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_4 = Single_ToString_m3798733330((&V_0), _stringLiteral82, L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		StringBuilder_Append_m3898090075(L_1, L_4, /*hidden argument*/NULL);
		goto IL_00e9;
	}

IL_0034:
	{
		Il2CppObject * L_5 = ___value0;
		if (((Il2CppObject *)IsInstSealed(L_5, Int32_t2847414787_il2cpp_TypeInfo_var)))
		{
			goto IL_008c;
		}
	}
	{
		Il2CppObject * L_6 = ___value0;
		if (((Il2CppObject *)IsInstSealed(L_6, UInt32_t985925326_il2cpp_TypeInfo_var)))
		{
			goto IL_008c;
		}
	}
	{
		Il2CppObject * L_7 = ___value0;
		if (((Il2CppObject *)IsInstSealed(L_7, Int64_t2847414882_il2cpp_TypeInfo_var)))
		{
			goto IL_008c;
		}
	}
	{
		Il2CppObject * L_8 = ___value0;
		if (((Il2CppObject *)IsInstSealed(L_8, SByte_t2855346064_il2cpp_TypeInfo_var)))
		{
			goto IL_008c;
		}
	}
	{
		Il2CppObject * L_9 = ___value0;
		if (((Il2CppObject *)IsInstSealed(L_9, Byte_t2778693821_il2cpp_TypeInfo_var)))
		{
			goto IL_008c;
		}
	}
	{
		Il2CppObject * L_10 = ___value0;
		if (((Il2CppObject *)IsInstSealed(L_10, Int16_t2847414729_il2cpp_TypeInfo_var)))
		{
			goto IL_008c;
		}
	}
	{
		Il2CppObject * L_11 = ___value0;
		if (((Il2CppObject *)IsInstSealed(L_11, UInt16_t985925268_il2cpp_TypeInfo_var)))
		{
			goto IL_008c;
		}
	}
	{
		Il2CppObject * L_12 = ___value0;
		if (!((Il2CppObject *)IsInstSealed(L_12, UInt64_t985925421_il2cpp_TypeInfo_var)))
		{
			goto IL_009e;
		}
	}

IL_008c:
	{
		StringBuilder_t3822575854 * L_13 = __this->get_builder_0();
		Il2CppObject * L_14 = ___value0;
		NullCheck(L_13);
		StringBuilder_Append_m4120200429(L_13, L_14, /*hidden argument*/NULL);
		goto IL_00e9;
	}

IL_009e:
	{
		Il2CppObject * L_15 = ___value0;
		if (((Il2CppObject *)IsInstSealed(L_15, Double_t534516614_il2cpp_TypeInfo_var)))
		{
			goto IL_00b4;
		}
	}
	{
		Il2CppObject * L_16 = ___value0;
		if (!((Il2CppObject *)IsInstSealed(L_16, Decimal_t1688557254_il2cpp_TypeInfo_var)))
		{
			goto IL_00dd;
		}
	}

IL_00b4:
	{
		StringBuilder_t3822575854 * L_17 = __this->get_builder_0();
		Il2CppObject * L_18 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		double L_19 = Convert_ToDouble_m1941295007(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		V_1 = L_19;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3603717042_il2cpp_TypeInfo_var);
		CultureInfo_t3603717042 * L_20 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_21 = Double_ToString_m2573497243((&V_1), _stringLiteral82, L_20, /*hidden argument*/NULL);
		NullCheck(L_17);
		StringBuilder_Append_m3898090075(L_17, L_21, /*hidden argument*/NULL);
		goto IL_00e9;
	}

IL_00dd:
	{
		Il2CppObject * L_22 = ___value0;
		NullCheck(L_22);
		String_t* L_23 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_22);
		Serializer_SerializeString_m2859448943(__this, L_23, /*hidden argument*/NULL);
	}

IL_00e9:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
