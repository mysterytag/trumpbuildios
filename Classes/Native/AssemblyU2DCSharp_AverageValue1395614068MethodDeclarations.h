﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AverageValue
struct AverageValue_t1395614068;

#include "codegen/il2cpp-codegen.h"

// System.Void AverageValue::.ctor()
extern "C"  void AverageValue__ctor_m3734827943 (AverageValue_t1395614068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AverageValue::Result()
extern "C"  float AverageValue_Result_m3640040038 (AverageValue_t1395614068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
