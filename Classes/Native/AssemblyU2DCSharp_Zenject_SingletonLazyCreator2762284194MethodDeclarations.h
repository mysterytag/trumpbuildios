﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.SingletonLazyCreator
struct SingletonLazyCreator_t2762284194;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// Zenject.SingletonProviderMap
struct SingletonProviderMap_t1557411893;
// Zenject.SingletonId
struct SingletonId_t1838183899;
// System.Func`2<Zenject.InjectContext,System.Object>
struct Func_2_t2621245597;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// Zenject.InjectContext
struct InjectContext_t3456483891;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449.h"
#include "AssemblyU2DCSharp_Zenject_SingletonProviderMap1557411893.h"
#include "AssemblyU2DCSharp_Zenject_SingletonId1838183899.h"
#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharp_Zenject_InjectContext3456483891.h"
#include "mscorlib_System_Type2779229935.h"

// System.Void Zenject.SingletonLazyCreator::.ctor(Zenject.DiContainer,Zenject.SingletonProviderMap,Zenject.SingletonId,System.Func`2<Zenject.InjectContext,System.Object>)
extern "C"  void SingletonLazyCreator__ctor_m2760220428 (SingletonLazyCreator_t2762284194 * __this, DiContainer_t2383114449 * ___container0, SingletonProviderMap_t1557411893 * ___owner1, SingletonId_t1838183899 * ___id2, Func_2_t2621245597 * ___createMethod3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.SingletonLazyCreator::.ctor(Zenject.DiContainer,Zenject.SingletonProviderMap,Zenject.SingletonId)
extern "C"  void SingletonLazyCreator__ctor_m1748858214 (SingletonLazyCreator_t2762284194 * __this, DiContainer_t2383114449 * ___container0, SingletonProviderMap_t1557411893 * ___owner1, SingletonId_t1838183899 * ___id2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.SingletonId Zenject.SingletonLazyCreator::get_Id()
extern "C"  SingletonId_t1838183899 * SingletonLazyCreator_get_Id_m617832261 (SingletonLazyCreator_t2762284194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.SingletonLazyCreator::get_HasCustomCreateMethod()
extern "C"  bool SingletonLazyCreator_get_HasCustomCreateMethod_m2482092184 (SingletonLazyCreator_t2762284194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.SingletonLazyCreator::IncRefCount()
extern "C"  void SingletonLazyCreator_IncRefCount_m1541995047 (SingletonLazyCreator_t2762284194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.SingletonLazyCreator::DecRefCount()
extern "C"  void SingletonLazyCreator_DecRefCount_m1847265611 (SingletonLazyCreator_t2762284194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.SingletonLazyCreator::SetInstance(System.Object)
extern "C"  void SingletonLazyCreator_SetInstance_m332882000 (SingletonLazyCreator_t2762284194 * __this, Il2CppObject * ___instance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.SingletonLazyCreator::HasInstance()
extern "C"  bool SingletonLazyCreator_HasInstance_m2375219400 (SingletonLazyCreator_t2762284194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Zenject.SingletonLazyCreator::GetInstanceType()
extern "C"  Type_t * SingletonLazyCreator_GetInstanceType_m3813166264 (SingletonLazyCreator_t2762284194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Zenject.SingletonLazyCreator::GetInstance(Zenject.InjectContext)
extern "C"  Il2CppObject * SingletonLazyCreator_GetInstance_m3399859088 (SingletonLazyCreator_t2762284194 * __this, InjectContext_t3456483891 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Zenject.SingletonLazyCreator::GetTypeToInstantiate(System.Type)
extern "C"  Type_t * SingletonLazyCreator_GetTypeToInstantiate_m884638635 (SingletonLazyCreator_t2762284194 * __this, Type_t * ___contractType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
