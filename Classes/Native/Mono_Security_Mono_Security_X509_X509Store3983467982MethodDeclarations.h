﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.X509.X509Store
struct X509Store_t3983467982;
// System.String
struct String_t;
// Mono.Security.X509.X509CertificateCollection
struct X509CertificateCollection_t3336811651;
// System.Collections.ArrayList
struct ArrayList_t2121638921;
// Mono.Security.X509.X509Certificate
struct X509Certificate_t273828613;
// Mono.Security.X509.X509Crl
struct X509Crl_t2942805322;
// System.Byte[]
struct ByteU5BU5D_t58506160;
// Mono.Security.X509.X509ExtensionCollection
struct X509ExtensionCollection_t3528692651;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "Mono_Security_Mono_Security_X509_X509Certificate273828612.h"
#include "Mono_Security_Mono_Security_X509_X509Crl2942805322.h"
#include "Mono_Security_Mono_Security_X509_X509ExtensionColl3528692650.h"

// System.Void Mono.Security.X509.X509Store::.ctor(System.String,System.Boolean)
extern "C"  void X509Store__ctor_m3740673213 (X509Store_t3983467982 * __this, String_t* ___path0, bool ___crl1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509CertificateCollection Mono.Security.X509.X509Store::get_Certificates()
extern "C"  X509CertificateCollection_t3336811651 * X509Store_get_Certificates_m3707205492 (X509Store_t3983467982 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList Mono.Security.X509.X509Store::get_Crls()
extern "C"  ArrayList_t2121638921 * X509Store_get_Crls_m3945141465 (X509Store_t3983467982 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.X509.X509Store::get_Name()
extern "C"  String_t* X509Store_get_Name_m2532535353 (X509Store_t3983467982 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509Store::Clear()
extern "C"  void X509Store_Clear_m3209086893 (X509Store_t3983467982 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509Store::Import(Mono.Security.X509.X509Certificate)
extern "C"  void X509Store_Import_m1303492027 (X509Store_t3983467982 * __this, X509Certificate_t273828613 * ___certificate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509Store::Import(Mono.Security.X509.X509Crl)
extern "C"  void X509Store_Import_m1614862133 (X509Store_t3983467982 * __this, X509Crl_t2942805322 * ___crl0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509Store::Remove(Mono.Security.X509.X509Certificate)
extern "C"  void X509Store_Remove_m2220521242 (X509Store_t3983467982 * __this, X509Certificate_t273828613 * ___certificate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509Store::Remove(Mono.Security.X509.X509Crl)
extern "C"  void X509Store_Remove_m2517919636 (X509Store_t3983467982 * __this, X509Crl_t2942805322 * ___crl0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.X509.X509Store::GetUniqueName(Mono.Security.X509.X509Certificate)
extern "C"  String_t* X509Store_GetUniqueName_m2991468195 (X509Store_t3983467982 * __this, X509Certificate_t273828613 * ___certificate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.X509.X509Store::GetUniqueName(Mono.Security.X509.X509Crl)
extern "C"  String_t* X509Store_GetUniqueName_m3971528733 (X509Store_t3983467982 * __this, X509Crl_t2942805322 * ___crl0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.X509Store::GetUniqueName(Mono.Security.X509.X509ExtensionCollection)
extern "C"  ByteU5BU5D_t58506160* X509Store_GetUniqueName_m863422230 (X509Store_t3983467982 * __this, X509ExtensionCollection_t3528692651 * ___extensions0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.X509.X509Store::GetUniqueName(System.String,System.Byte[],System.String)
extern "C"  String_t* X509Store_GetUniqueName_m1097475666 (X509Store_t3983467982 * __this, String_t* ___method0, ByteU5BU5D_t58506160* ___name1, String_t* ___fileExtension2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.X509Store::Load(System.String)
extern "C"  ByteU5BU5D_t58506160* X509Store_Load_m507057712 (X509Store_t3983467982 * __this, String_t* ___filename0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Certificate Mono.Security.X509.X509Store::LoadCertificate(System.String)
extern "C"  X509Certificate_t273828613 * X509Store_LoadCertificate_m2281266908 (X509Store_t3983467982 * __this, String_t* ___filename0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Crl Mono.Security.X509.X509Store::LoadCrl(System.String)
extern "C"  X509Crl_t2942805322 * X509Store_LoadCrl_m371472464 (X509Store_t3983467982 * __this, String_t* ___filename0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509Store::CheckStore(System.String,System.Boolean)
extern "C"  bool X509Store_CheckStore_m4197144772 (X509Store_t3983467982 * __this, String_t* ___path0, bool ___throwException1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509CertificateCollection Mono.Security.X509.X509Store::BuildCertificatesCollection(System.String)
extern "C"  X509CertificateCollection_t3336811651 * X509Store_BuildCertificatesCollection_m1851273927 (X509Store_t3983467982 * __this, String_t* ___storeName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList Mono.Security.X509.X509Store::BuildCrlsCollection(System.String)
extern "C"  ArrayList_t2121638921 * X509Store_BuildCrlsCollection_m2551806712 (X509Store_t3983467982 * __this, String_t* ___storeName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
