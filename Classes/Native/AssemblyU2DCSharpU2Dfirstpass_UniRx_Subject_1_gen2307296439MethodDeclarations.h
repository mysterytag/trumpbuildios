﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1<UniRx.Tuple`2<System.Object,System.Object>>
struct Subject_1_t2307296439;
// System.Exception
struct Exception_t1967233988;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Object>>
struct IObserver_1_t2581260722;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen369261819.h"

// System.Void UniRx.Subject`1<UniRx.Tuple`2<System.Object,System.Object>>::.ctor()
extern "C"  void Subject_1__ctor_m3673523288_gshared (Subject_1_t2307296439 * __this, const MethodInfo* method);
#define Subject_1__ctor_m3673523288(__this, method) ((  void (*) (Subject_1_t2307296439 *, const MethodInfo*))Subject_1__ctor_m3673523288_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.Tuple`2<System.Object,System.Object>>::get_HasObservers()
extern "C"  bool Subject_1_get_HasObservers_m1225079420_gshared (Subject_1_t2307296439 * __this, const MethodInfo* method);
#define Subject_1_get_HasObservers_m1225079420(__this, method) ((  bool (*) (Subject_1_t2307296439 *, const MethodInfo*))Subject_1_get_HasObservers_m1225079420_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.Tuple`2<System.Object,System.Object>>::OnCompleted()
extern "C"  void Subject_1_OnCompleted_m1708266978_gshared (Subject_1_t2307296439 * __this, const MethodInfo* method);
#define Subject_1_OnCompleted_m1708266978(__this, method) ((  void (*) (Subject_1_t2307296439 *, const MethodInfo*))Subject_1_OnCompleted_m1708266978_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.Tuple`2<System.Object,System.Object>>::OnError(System.Exception)
extern "C"  void Subject_1_OnError_m2796618767_gshared (Subject_1_t2307296439 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Subject_1_OnError_m2796618767(__this, ___error0, method) ((  void (*) (Subject_1_t2307296439 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m2796618767_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<UniRx.Tuple`2<System.Object,System.Object>>::OnNext(T)
extern "C"  void Subject_1_OnNext_m2623465728_gshared (Subject_1_t2307296439 * __this, Tuple_2_t369261819  ___value0, const MethodInfo* method);
#define Subject_1_OnNext_m2623465728(__this, ___value0, method) ((  void (*) (Subject_1_t2307296439 *, Tuple_2_t369261819 , const MethodInfo*))Subject_1_OnNext_m2623465728_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<UniRx.Tuple`2<System.Object,System.Object>>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * Subject_1_Subscribe_m376261037_gshared (Subject_1_t2307296439 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define Subject_1_Subscribe_m376261037(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t2307296439 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m376261037_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<UniRx.Tuple`2<System.Object,System.Object>>::Dispose()
extern "C"  void Subject_1_Dispose_m3986012821_gshared (Subject_1_t2307296439 * __this, const MethodInfo* method);
#define Subject_1_Dispose_m3986012821(__this, method) ((  void (*) (Subject_1_t2307296439 *, const MethodInfo*))Subject_1_Dispose_m3986012821_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.Tuple`2<System.Object,System.Object>>::ThrowIfDisposed()
extern "C"  void Subject_1_ThrowIfDisposed_m2620260126_gshared (Subject_1_t2307296439 * __this, const MethodInfo* method);
#define Subject_1_ThrowIfDisposed_m2620260126(__this, method) ((  void (*) (Subject_1_t2307296439 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m2620260126_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.Tuple`2<System.Object,System.Object>>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool Subject_1_IsRequiredSubscribeOnCurrentThread_m4284383347_gshared (Subject_1_t2307296439 * __this, const MethodInfo* method);
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m4284383347(__this, method) ((  bool (*) (Subject_1_t2307296439 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m4284383347_gshared)(__this, method)
