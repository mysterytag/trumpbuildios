﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SubstanceBase`2/<Affect>c__AnonStorey144<System.Single,System.Object>
struct U3CAffectU3Ec__AnonStorey144_t2722434671;

#include "codegen/il2cpp-codegen.h"

// System.Void SubstanceBase`2/<Affect>c__AnonStorey144<System.Single,System.Object>::.ctor()
extern "C"  void U3CAffectU3Ec__AnonStorey144__ctor_m2070073021_gshared (U3CAffectU3Ec__AnonStorey144_t2722434671 * __this, const MethodInfo* method);
#define U3CAffectU3Ec__AnonStorey144__ctor_m2070073021(__this, method) ((  void (*) (U3CAffectU3Ec__AnonStorey144_t2722434671 *, const MethodInfo*))U3CAffectU3Ec__AnonStorey144__ctor_m2070073021_gshared)(__this, method)
// System.Void SubstanceBase`2/<Affect>c__AnonStorey144<System.Single,System.Object>::<>m__1DA()
extern "C"  void U3CAffectU3Ec__AnonStorey144_U3CU3Em__1DA_m940347722_gshared (U3CAffectU3Ec__AnonStorey144_t2722434671 * __this, const MethodInfo* method);
#define U3CAffectU3Ec__AnonStorey144_U3CU3Em__1DA_m940347722(__this, method) ((  void (*) (U3CAffectU3Ec__AnonStorey144_t2722434671 *, const MethodInfo*))U3CAffectU3Ec__AnonStorey144_U3CU3Em__1DA_m940347722_gshared)(__this, method)
