﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Predicate_1_gen1408070318MethodDeclarations.h"

// System.Void System.Predicate`1<SubstanceBase`2/Intrusion<System.Single,System.Single>>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m2174496814(__this, ___object0, ___method1, method) ((  void (*) (Predicate_1_t649860322 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m982040097_gshared)(__this, ___object0, ___method1, method)
// System.Boolean System.Predicate`1<SubstanceBase`2/Intrusion<System.Single,System.Single>>::Invoke(T)
#define Predicate_1_Invoke_m1850544788(__this, ___obj0, method) ((  bool (*) (Predicate_1_t649860322 *, Intrusion_t78896424 *, const MethodInfo*))Predicate_1_Invoke_m4106178309_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Predicate`1<SubstanceBase`2/Intrusion<System.Single,System.Single>>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m2481759523(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Predicate_1_t649860322 *, Intrusion_t78896424 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Predicate_1_BeginInvoke_m2038073176_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Boolean System.Predicate`1<SubstanceBase`2/Intrusion<System.Single,System.Single>>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m3742782912(__this, ___result0, method) ((  bool (*) (Predicate_1_t649860322 *, Il2CppObject *, const MethodInfo*))Predicate_1_EndInvoke_m3970497007_gshared)(__this, ___result0, method)
