﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Int32,System.Object>
struct CombineLatest_t2150356605;
// UniRx.Operators.CombineLatestObservable`3<System.Boolean,System.Int32,System.Object>
struct CombineLatestObservable_3_t2543452560;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Int32,System.Object>::.ctor(UniRx.Operators.CombineLatestObservable`3<TLeft,TRight,TResult>,UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void CombineLatest__ctor_m669378075_gshared (CombineLatest_t2150356605 * __this, CombineLatestObservable_3_t2543452560 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define CombineLatest__ctor_m669378075(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (CombineLatest_t2150356605 *, CombineLatestObservable_3_t2543452560 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))CombineLatest__ctor_m669378075_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Int32,System.Object>::Run()
extern "C"  Il2CppObject * CombineLatest_Run_m496495875_gshared (CombineLatest_t2150356605 * __this, const MethodInfo* method);
#define CombineLatest_Run_m496495875(__this, method) ((  Il2CppObject * (*) (CombineLatest_t2150356605 *, const MethodInfo*))CombineLatest_Run_m496495875_gshared)(__this, method)
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Int32,System.Object>::Publish()
extern "C"  void CombineLatest_Publish_m2417128034_gshared (CombineLatest_t2150356605 * __this, const MethodInfo* method);
#define CombineLatest_Publish_m2417128034(__this, method) ((  void (*) (CombineLatest_t2150356605 *, const MethodInfo*))CombineLatest_Publish_m2417128034_gshared)(__this, method)
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Int32,System.Object>::OnNext(TResult)
extern "C"  void CombineLatest_OnNext_m3307201088_gshared (CombineLatest_t2150356605 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define CombineLatest_OnNext_m3307201088(__this, ___value0, method) ((  void (*) (CombineLatest_t2150356605 *, Il2CppObject *, const MethodInfo*))CombineLatest_OnNext_m3307201088_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Int32,System.Object>::OnError(System.Exception)
extern "C"  void CombineLatest_OnError_m3196643052_gshared (CombineLatest_t2150356605 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define CombineLatest_OnError_m3196643052(__this, ___error0, method) ((  void (*) (CombineLatest_t2150356605 *, Exception_t1967233988 *, const MethodInfo*))CombineLatest_OnError_m3196643052_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.CombineLatestObservable`3/CombineLatest<System.Boolean,System.Int32,System.Object>::OnCompleted()
extern "C"  void CombineLatest_OnCompleted_m2368314431_gshared (CombineLatest_t2150356605 * __this, const MethodInfo* method);
#define CombineLatest_OnCompleted_m2368314431(__this, method) ((  void (*) (CombineLatest_t2150356605 *, const MethodInfo*))CombineLatest_OnCompleted_m2368314431_gshared)(__this, method)
