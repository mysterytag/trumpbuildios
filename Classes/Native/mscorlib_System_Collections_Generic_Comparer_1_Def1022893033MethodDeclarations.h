﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<PlayFab.Internal.GMFB_327/testRegion>
struct DefaultComparer_t1022893034;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PlayFab_Internal_GMFB_327_testRe1684939302.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<PlayFab.Internal.GMFB_327/testRegion>::.ctor()
extern "C"  void DefaultComparer__ctor_m3428806730_gshared (DefaultComparer_t1022893034 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3428806730(__this, method) ((  void (*) (DefaultComparer_t1022893034 *, const MethodInfo*))DefaultComparer__ctor_m3428806730_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<PlayFab.Internal.GMFB_327/testRegion>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m2732097645_gshared (DefaultComparer_t1022893034 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m2732097645(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t1022893034 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Compare_m2732097645_gshared)(__this, ___x0, ___y1, method)
