﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SponsorPay.SponsorPayPlugin
struct SponsorPayPlugin_t4106591195;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SponsorPayPluginMonoBehaviour
struct  SponsorPayPluginMonoBehaviour_t1958993529  : public MonoBehaviour_t3012272455
{
public:

public:
};

struct SponsorPayPluginMonoBehaviour_t1958993529_StaticFields
{
public:
	// SponsorPay.SponsorPayPlugin SponsorPayPluginMonoBehaviour::PluginInstance
	SponsorPayPlugin_t4106591195 * ___PluginInstance_2;

public:
	inline static int32_t get_offset_of_PluginInstance_2() { return static_cast<int32_t>(offsetof(SponsorPayPluginMonoBehaviour_t1958993529_StaticFields, ___PluginInstance_2)); }
	inline SponsorPayPlugin_t4106591195 * get_PluginInstance_2() const { return ___PluginInstance_2; }
	inline SponsorPayPlugin_t4106591195 ** get_address_of_PluginInstance_2() { return &___PluginInstance_2; }
	inline void set_PluginInstance_2(SponsorPayPlugin_t4106591195 * value)
	{
		___PluginInstance_2 = value;
		Il2CppCodeGenWriteBarrier(&___PluginInstance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
