﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/<OnErrorRetry>c__AnonStorey3D`2/<OnErrorRetry>c__AnonStorey3E`2<System.Object,System.Object>
struct U3COnErrorRetryU3Ec__AnonStorey3E_2_t1069553519;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.Observable/<OnErrorRetry>c__AnonStorey3D`2/<OnErrorRetry>c__AnonStorey3E`2<System.Object,System.Object>::.ctor()
extern "C"  void U3COnErrorRetryU3Ec__AnonStorey3E_2__ctor_m2508790297_gshared (U3COnErrorRetryU3Ec__AnonStorey3E_2_t1069553519 * __this, const MethodInfo* method);
#define U3COnErrorRetryU3Ec__AnonStorey3E_2__ctor_m2508790297(__this, method) ((  void (*) (U3COnErrorRetryU3Ec__AnonStorey3E_2_t1069553519 *, const MethodInfo*))U3COnErrorRetryU3Ec__AnonStorey3E_2__ctor_m2508790297_gshared)(__this, method)
// UniRx.IObservable`1<TSource> UniRx.Observable/<OnErrorRetry>c__AnonStorey3D`2/<OnErrorRetry>c__AnonStorey3E`2<System.Object,System.Object>::<>m__64(TException)
extern "C"  Il2CppObject* U3COnErrorRetryU3Ec__AnonStorey3E_2_U3CU3Em__64_m530446180_gshared (U3COnErrorRetryU3Ec__AnonStorey3E_2_t1069553519 * __this, Il2CppObject * ___ex0, const MethodInfo* method);
#define U3COnErrorRetryU3Ec__AnonStorey3E_2_U3CU3Em__64_m530446180(__this, ___ex0, method) ((  Il2CppObject* (*) (U3COnErrorRetryU3Ec__AnonStorey3E_2_t1069553519 *, Il2CppObject *, const MethodInfo*))U3COnErrorRetryU3Ec__AnonStorey3E_2_U3CU3Em__64_m530446180_gshared)(__this, ___ex0, method)
