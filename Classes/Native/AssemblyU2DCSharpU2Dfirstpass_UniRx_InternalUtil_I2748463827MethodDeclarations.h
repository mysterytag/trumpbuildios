﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I1897310919MethodDeclarations.h"

// System.Void UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UniRx.CountChangedStatus>>::.ctor()
#define ImmutableList_1__ctor_m668908322(__this, method) ((  void (*) (ImmutableList_1_t2748463827 *, const MethodInfo*))ImmutableList_1__ctor_m2467204245_gshared)(__this, method)
// System.Void UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UniRx.CountChangedStatus>>::.ctor(T[])
#define ImmutableList_1__ctor_m3095905946(__this, ___data0, method) ((  void (*) (ImmutableList_1_t2748463827 *, IObserver_1U5BU5D_t335784193*, const MethodInfo*))ImmutableList_1__ctor_m707697735_gshared)(__this, ___data0, method)
// System.Void UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UniRx.CountChangedStatus>>::.cctor()
#define ImmutableList_1__cctor_m3074192587(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ImmutableList_1__cctor_m2986791352_gshared)(__this /* static, unused */, method)
// T[] UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UniRx.CountChangedStatus>>::get_Data()
#define ImmutableList_1_get_Data_m4022212312(__this, method) ((  IObserver_1U5BU5D_t335784193* (*) (ImmutableList_1_t2748463827 *, const MethodInfo*))ImmutableList_1_get_Data_m1676800965_gshared)(__this, method)
// UniRx.InternalUtil.ImmutableList`1<T> UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UniRx.CountChangedStatus>>::Add(T)
#define ImmutableList_1_Add_m3235034902(__this, ___value0, method) ((  ImmutableList_1_t2748463827 * (*) (ImmutableList_1_t2748463827 *, Il2CppObject*, const MethodInfo*))ImmutableList_1_Add_m948892931_gshared)(__this, ___value0, method)
// UniRx.InternalUtil.ImmutableList`1<T> UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UniRx.CountChangedStatus>>::Remove(T)
#define ImmutableList_1_Remove_m2697962591(__this, ___value0, method) ((  ImmutableList_1_t2748463827 * (*) (ImmutableList_1_t2748463827 *, Il2CppObject*, const MethodInfo*))ImmutableList_1_Remove_m1538917202_gshared)(__this, ___value0, method)
// System.Int32 UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UniRx.CountChangedStatus>>::IndexOf(T)
#define ImmutableList_1_IndexOf_m3725281855(__this, ___value0, method) ((  int32_t (*) (ImmutableList_1_t2748463827 *, Il2CppObject*, const MethodInfo*))ImmutableList_1_IndexOf_m3149323628_gshared)(__this, ___value0, method)
