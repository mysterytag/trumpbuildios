﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ReturnObservable`1/<SubscribeCore>c__AnonStorey68<System.Int32>
struct U3CSubscribeCoreU3Ec__AnonStorey68_t4150718336;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.ReturnObservable`1/<SubscribeCore>c__AnonStorey68<System.Int32>::.ctor()
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey68__ctor_m2140084248_gshared (U3CSubscribeCoreU3Ec__AnonStorey68_t4150718336 * __this, const MethodInfo* method);
#define U3CSubscribeCoreU3Ec__AnonStorey68__ctor_m2140084248(__this, method) ((  void (*) (U3CSubscribeCoreU3Ec__AnonStorey68_t4150718336 *, const MethodInfo*))U3CSubscribeCoreU3Ec__AnonStorey68__ctor_m2140084248_gshared)(__this, method)
// System.Void UniRx.Operators.ReturnObservable`1/<SubscribeCore>c__AnonStorey68<System.Int32>::<>m__86()
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey68_U3CU3Em__86_m1639881823_gshared (U3CSubscribeCoreU3Ec__AnonStorey68_t4150718336 * __this, const MethodInfo* method);
#define U3CSubscribeCoreU3Ec__AnonStorey68_U3CU3Em__86_m1639881823(__this, method) ((  void (*) (U3CSubscribeCoreU3Ec__AnonStorey68_t4150718336 *, const MethodInfo*))U3CSubscribeCoreU3Ec__AnonStorey68_U3CU3Em__86_m1639881823_gshared)(__this, method)
