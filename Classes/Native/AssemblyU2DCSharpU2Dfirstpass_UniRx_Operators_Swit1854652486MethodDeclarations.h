﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SwitchObservable`1<System.Object>
struct SwitchObservable_1_t1854652486;
// UniRx.IObservable`1<UniRx.IObservable`1<System.Object>>
struct IObservable_1_t354703148;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.SwitchObservable`1<System.Object>::.ctor(UniRx.IObservable`1<UniRx.IObservable`1<T>>)
extern "C"  void SwitchObservable_1__ctor_m2020426066_gshared (SwitchObservable_1_t1854652486 * __this, Il2CppObject* ___sources0, const MethodInfo* method);
#define SwitchObservable_1__ctor_m2020426066(__this, ___sources0, method) ((  void (*) (SwitchObservable_1_t1854652486 *, Il2CppObject*, const MethodInfo*))SwitchObservable_1__ctor_m2020426066_gshared)(__this, ___sources0, method)
// System.IDisposable UniRx.Operators.SwitchObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * SwitchObservable_1_SubscribeCore_m2713713870_gshared (SwitchObservable_1_t1854652486 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define SwitchObservable_1_SubscribeCore_m2713713870(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (SwitchObservable_1_t1854652486 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SwitchObservable_1_SubscribeCore_m2713713870_gshared)(__this, ___observer0, ___cancel1, method)
