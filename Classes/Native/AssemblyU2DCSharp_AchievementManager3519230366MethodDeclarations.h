﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AchievementManager
struct AchievementManager_t3519230366;

#include "codegen/il2cpp-codegen.h"

// System.Void AchievementManager::.ctor()
extern "C"  void AchievementManager__ctor_m633192637 (AchievementManager_t3519230366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AchievementManager::Start()
extern "C"  void AchievementManager_Start_m3875297725 (AchievementManager_t3519230366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AchievementManager::Update()
extern "C"  void AchievementManager_Update_m4175964656 (AchievementManager_t3519230366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
