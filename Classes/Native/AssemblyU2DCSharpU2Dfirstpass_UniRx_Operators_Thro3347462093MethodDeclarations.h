﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ThrowObservable`1/<SubscribeCore>c__AnonStorey6B<UniRx.Unit>
struct U3CSubscribeCoreU3Ec__AnonStorey6B_t3347462093;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.ThrowObservable`1/<SubscribeCore>c__AnonStorey6B<UniRx.Unit>::.ctor()
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey6B__ctor_m3062253659_gshared (U3CSubscribeCoreU3Ec__AnonStorey6B_t3347462093 * __this, const MethodInfo* method);
#define U3CSubscribeCoreU3Ec__AnonStorey6B__ctor_m3062253659(__this, method) ((  void (*) (U3CSubscribeCoreU3Ec__AnonStorey6B_t3347462093 *, const MethodInfo*))U3CSubscribeCoreU3Ec__AnonStorey6B__ctor_m3062253659_gshared)(__this, method)
// System.Void UniRx.Operators.ThrowObservable`1/<SubscribeCore>c__AnonStorey6B<UniRx.Unit>::<>m__89()
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey6B_U3CU3Em__89_m3081425701_gshared (U3CSubscribeCoreU3Ec__AnonStorey6B_t3347462093 * __this, const MethodInfo* method);
#define U3CSubscribeCoreU3Ec__AnonStorey6B_U3CU3Em__89_m3081425701(__this, method) ((  void (*) (U3CSubscribeCoreU3Ec__AnonStorey6B_t3347462093 *, const MethodInfo*))U3CSubscribeCoreU3Ec__AnonStorey6B_U3CU3Em__89_m3081425701_gshared)(__this, method)
