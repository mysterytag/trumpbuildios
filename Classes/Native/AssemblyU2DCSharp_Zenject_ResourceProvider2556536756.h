﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type
struct Type_t;
// System.String
struct String_t;

#include "AssemblyU2DCSharp_Zenject_ProviderBase1627494391.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ResourceProvider
struct  ResourceProvider_t2556536756  : public ProviderBase_t1627494391
{
public:
	// System.Type Zenject.ResourceProvider::_concreteType
	Type_t * ____concreteType_2;
	// System.String Zenject.ResourceProvider::_resourcePath
	String_t* ____resourcePath_3;

public:
	inline static int32_t get_offset_of__concreteType_2() { return static_cast<int32_t>(offsetof(ResourceProvider_t2556536756, ____concreteType_2)); }
	inline Type_t * get__concreteType_2() const { return ____concreteType_2; }
	inline Type_t ** get_address_of__concreteType_2() { return &____concreteType_2; }
	inline void set__concreteType_2(Type_t * value)
	{
		____concreteType_2 = value;
		Il2CppCodeGenWriteBarrier(&____concreteType_2, value);
	}

	inline static int32_t get_offset_of__resourcePath_3() { return static_cast<int32_t>(offsetof(ResourceProvider_t2556536756, ____resourcePath_3)); }
	inline String_t* get__resourcePath_3() const { return ____resourcePath_3; }
	inline String_t** get_address_of__resourcePath_3() { return &____resourcePath_3; }
	inline void set__resourcePath_3(String_t* value)
	{
		____resourcePath_3 = value;
		Il2CppCodeGenWriteBarrier(&____resourcePath_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
