﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.UUnit.ObjOptNumFieldTest
struct ObjOptNumFieldTest_t3771869740;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen1446416676.h"
#include "mscorlib_System_Nullable_1_gen1369764433.h"
#include "mscorlib_System_Nullable_1_gen1438485341.h"
#include "mscorlib_System_Nullable_1_gen3871963176.h"
#include "mscorlib_System_Nullable_1_gen1438485399.h"
#include "mscorlib_System_Nullable_1_gen3871963234.h"
#include "mscorlib_System_Nullable_1_gen1438485494.h"
#include "mscorlib_System_Nullable_1_gen3871963329.h"
#include "mscorlib_System_Nullable_1_gen3844246929.h"
#include "mscorlib_System_Nullable_1_gen3420554522.h"

// System.Void PlayFab.UUnit.ObjOptNumFieldTest::.ctor()
extern "C"  void ObjOptNumFieldTest__ctor_m2343136137 (ObjOptNumFieldTest_t3771869740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.ObjOptNumFieldTest::.cctor()
extern "C"  void ObjOptNumFieldTest__cctor_m3435647300 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.SByte> PlayFab.UUnit.ObjOptNumFieldTest::get_SbyteValue()
extern "C"  Nullable_1_t1446416676  ObjOptNumFieldTest_get_SbyteValue_m2174063446 (ObjOptNumFieldTest_t3771869740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.ObjOptNumFieldTest::set_SbyteValue(System.Nullable`1<System.SByte>)
extern "C"  void ObjOptNumFieldTest_set_SbyteValue_m3881783629 (ObjOptNumFieldTest_t3771869740 * __this, Nullable_1_t1446416676  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Byte> PlayFab.UUnit.ObjOptNumFieldTest::get_ByteValue()
extern "C"  Nullable_1_t1369764433  ObjOptNumFieldTest_get_ByteValue_m2928311788 (ObjOptNumFieldTest_t3771869740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.ObjOptNumFieldTest::set_ByteValue(System.Nullable`1<System.Byte>)
extern "C"  void ObjOptNumFieldTest_set_ByteValue_m2228855441 (ObjOptNumFieldTest_t3771869740 * __this, Nullable_1_t1369764433  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int16> PlayFab.UUnit.ObjOptNumFieldTest::get_ShortValue()
extern "C"  Nullable_1_t1438485341  ObjOptNumFieldTest_get_ShortValue_m2376833902 (ObjOptNumFieldTest_t3771869740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.ObjOptNumFieldTest::set_ShortValue(System.Nullable`1<System.Int16>)
extern "C"  void ObjOptNumFieldTest_set_ShortValue_m3892366599 (ObjOptNumFieldTest_t3771869740 * __this, Nullable_1_t1438485341  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.UInt16> PlayFab.UUnit.ObjOptNumFieldTest::get_UshortValue()
extern "C"  Nullable_1_t3871963176  ObjOptNumFieldTest_get_UshortValue_m4084073782 (ObjOptNumFieldTest_t3771869740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.ObjOptNumFieldTest::set_UshortValue(System.Nullable`1<System.UInt16>)
extern "C"  void ObjOptNumFieldTest_set_UshortValue_m1996713961 (ObjOptNumFieldTest_t3771869740 * __this, Nullable_1_t3871963176  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> PlayFab.UUnit.ObjOptNumFieldTest::get_IntValue()
extern "C"  Nullable_1_t1438485399  ObjOptNumFieldTest_get_IntValue_m1775469653 (ObjOptNumFieldTest_t3771869740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.ObjOptNumFieldTest::set_IntValue(System.Nullable`1<System.Int32>)
extern "C"  void ObjOptNumFieldTest_set_IntValue_m3625077300 (ObjOptNumFieldTest_t3771869740 * __this, Nullable_1_t1438485399  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.UInt32> PlayFab.UUnit.ObjOptNumFieldTest::get_UintValue()
extern "C"  Nullable_1_t3871963234  ObjOptNumFieldTest_get_UintValue_m3032742889 (ObjOptNumFieldTest_t3771869740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.ObjOptNumFieldTest::set_UintValue(System.Nullable`1<System.UInt32>)
extern "C"  void ObjOptNumFieldTest_set_UintValue_m3533697616 (ObjOptNumFieldTest_t3771869740 * __this, Nullable_1_t3871963234  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int64> PlayFab.UUnit.ObjOptNumFieldTest::get_LongValue()
extern "C"  Nullable_1_t1438485494  ObjOptNumFieldTest_get_LongValue_m2586752645 (ObjOptNumFieldTest_t3771869740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.ObjOptNumFieldTest::set_LongValue(System.Nullable`1<System.Int64>)
extern "C"  void ObjOptNumFieldTest_set_LongValue_m3368824238 (ObjOptNumFieldTest_t3771869740 * __this, Nullable_1_t1438485494  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.UInt64> PlayFab.UUnit.ObjOptNumFieldTest::get_UlongValue()
extern "C"  Nullable_1_t3871963329  ObjOptNumFieldTest_get_UlongValue_m313771791 (ObjOptNumFieldTest_t3771869740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.ObjOptNumFieldTest::set_UlongValue(System.Nullable`1<System.UInt64>)
extern "C"  void ObjOptNumFieldTest_set_UlongValue_m3333158022 (ObjOptNumFieldTest_t3771869740 * __this, Nullable_1_t3871963329  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Single> PlayFab.UUnit.ObjOptNumFieldTest::get_FloatValue()
extern "C"  Nullable_1_t3844246929  ObjOptNumFieldTest_get_FloatValue_m1739065236 (ObjOptNumFieldTest_t3771869740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.ObjOptNumFieldTest::set_FloatValue(System.Nullable`1<System.Single>)
extern "C"  void ObjOptNumFieldTest_set_FloatValue_m1872491787 (ObjOptNumFieldTest_t3771869740 * __this, Nullable_1_t3844246929  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Double> PlayFab.UUnit.ObjOptNumFieldTest::get_DoubleValue()
extern "C"  Nullable_1_t3420554522  ObjOptNumFieldTest_get_DoubleValue_m2816426394 (ObjOptNumFieldTest_t3771869740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.ObjOptNumFieldTest::set_DoubleValue(System.Nullable`1<System.Double>)
extern "C"  void ObjOptNumFieldTest_set_DoubleValue_m60442225 (ObjOptNumFieldTest_t3771869740 * __this, Nullable_1_t3420554522  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
