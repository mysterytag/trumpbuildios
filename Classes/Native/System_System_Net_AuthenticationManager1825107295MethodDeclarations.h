﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.AuthenticationManager
struct AuthenticationManager_t1825107295;
// System.Net.ICredentialPolicy
struct ICredentialPolicy_t4181605948;
// System.Exception
struct Exception_t1967233988;
// System.Collections.Specialized.StringDictionary
struct StringDictionary_t2792775730;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Net.Authorization
struct Authorization_t3226214339;
// System.String
struct String_t;
// System.Net.WebRequest
struct WebRequest_t3488810021;
// System.Net.ICredentials
struct ICredentials_t2307785309;
// System.Net.IAuthenticationModule
struct IAuthenticationModule_t486080599;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "System_System_Net_WebRequest3488810021.h"

// System.Void System.Net.AuthenticationManager::.ctor()
extern "C"  void AuthenticationManager__ctor_m1811449559 (AuthenticationManager_t1825107295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.AuthenticationManager::.cctor()
extern "C"  void AuthenticationManager__cctor_m4133232566 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.AuthenticationManager::EnsureModules()
extern "C"  void AuthenticationManager_EnsureModules_m3823797022 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.ICredentialPolicy System.Net.AuthenticationManager::get_CredentialPolicy()
extern "C"  Il2CppObject * AuthenticationManager_get_CredentialPolicy_m113317020 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.AuthenticationManager::set_CredentialPolicy(System.Net.ICredentialPolicy)
extern "C"  void AuthenticationManager_set_CredentialPolicy_m1882058673 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception System.Net.AuthenticationManager::GetMustImplement()
extern "C"  Exception_t1967233988 * AuthenticationManager_GetMustImplement_m1884614784 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Specialized.StringDictionary System.Net.AuthenticationManager::get_CustomTargetNameDictionary()
extern "C"  StringDictionary_t2792775730 * AuthenticationManager_get_CustomTargetNameDictionary_m3071367688 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Net.AuthenticationManager::get_RegisteredModules()
extern "C"  Il2CppObject * AuthenticationManager_get_RegisteredModules_m17421319 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.AuthenticationManager::Clear()
extern "C"  void AuthenticationManager_Clear_m3512550146 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.Authorization System.Net.AuthenticationManager::Authenticate(System.String,System.Net.WebRequest,System.Net.ICredentials)
extern "C"  Authorization_t3226214339 * AuthenticationManager_Authenticate_m926148532 (Il2CppObject * __this /* static, unused */, String_t* ___challenge0, WebRequest_t3488810021 * ___request1, Il2CppObject * ___credentials2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.Authorization System.Net.AuthenticationManager::DoAuthenticate(System.String,System.Net.WebRequest,System.Net.ICredentials)
extern "C"  Authorization_t3226214339 * AuthenticationManager_DoAuthenticate_m3748190185 (Il2CppObject * __this /* static, unused */, String_t* ___challenge0, WebRequest_t3488810021 * ___request1, Il2CppObject * ___credentials2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.Authorization System.Net.AuthenticationManager::PreAuthenticate(System.Net.WebRequest,System.Net.ICredentials)
extern "C"  Authorization_t3226214339 * AuthenticationManager_PreAuthenticate_m2542705561 (Il2CppObject * __this /* static, unused */, WebRequest_t3488810021 * ___request0, Il2CppObject * ___credentials1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.AuthenticationManager::Register(System.Net.IAuthenticationModule)
extern "C"  void AuthenticationManager_Register_m345494195 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___authenticationModule0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.AuthenticationManager::Unregister(System.Net.IAuthenticationModule)
extern "C"  void AuthenticationManager_Unregister_m3993935628 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___authenticationModule0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.AuthenticationManager::Unregister(System.String)
extern "C"  void AuthenticationManager_Unregister_m2266235353 (Il2CppObject * __this /* static, unused */, String_t* ___authenticationScheme0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.AuthenticationManager::DoUnregister(System.String,System.Boolean)
extern "C"  void AuthenticationManager_DoUnregister_m345561903 (Il2CppObject * __this /* static, unused */, String_t* ___authenticationScheme0, bool ___throwEx1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
