﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t1522972100;
// SimpleJSON.JSONNode
struct JSONNode_t580622632;
// System.Action`1<System.Single>
struct Action_1_t1106661726;
// System.Object
struct Il2CppObject;
// AnalyticsController
struct AnalyticsController_t2265609122;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnalyticsController/<ConvertCurrencyCoro>c__Iterator1D
struct  U3CConvertCurrencyCoroU3Ec__Iterator1D_t2074409111  : public Il2CppObject
{
public:
	// System.Single AnalyticsController/<ConvertCurrencyCoro>c__Iterator1D::revenue
	float ___revenue_0;
	// System.Single AnalyticsController/<ConvertCurrencyCoro>c__Iterator1D::<usdRevenue>__0
	float ___U3CusdRevenueU3E__0_1;
	// System.String AnalyticsController/<ConvertCurrencyCoro>c__Iterator1D::currency
	String_t* ___currency_2;
	// UnityEngine.WWW AnalyticsController/<ConvertCurrencyCoro>c__Iterator1D::<request>__1
	WWW_t1522972100 * ___U3CrequestU3E__1_3;
	// SimpleJSON.JSONNode AnalyticsController/<ConvertCurrencyCoro>c__Iterator1D::<json>__2
	JSONNode_t580622632 * ___U3CjsonU3E__2_4;
	// System.Single AnalyticsController/<ConvertCurrencyCoro>c__Iterator1D::<usdFromCurr>__3
	float ___U3CusdFromCurrU3E__3_5;
	// System.Action`1<System.Single> AnalyticsController/<ConvertCurrencyCoro>c__Iterator1D::callback
	Action_1_t1106661726 * ___callback_6;
	// System.Int32 AnalyticsController/<ConvertCurrencyCoro>c__Iterator1D::$PC
	int32_t ___U24PC_7;
	// System.Object AnalyticsController/<ConvertCurrencyCoro>c__Iterator1D::$current
	Il2CppObject * ___U24current_8;
	// System.Single AnalyticsController/<ConvertCurrencyCoro>c__Iterator1D::<$>revenue
	float ___U3CU24U3Erevenue_9;
	// System.String AnalyticsController/<ConvertCurrencyCoro>c__Iterator1D::<$>currency
	String_t* ___U3CU24U3Ecurrency_10;
	// System.Action`1<System.Single> AnalyticsController/<ConvertCurrencyCoro>c__Iterator1D::<$>callback
	Action_1_t1106661726 * ___U3CU24U3Ecallback_11;
	// AnalyticsController AnalyticsController/<ConvertCurrencyCoro>c__Iterator1D::<>f__this
	AnalyticsController_t2265609122 * ___U3CU3Ef__this_12;

public:
	inline static int32_t get_offset_of_revenue_0() { return static_cast<int32_t>(offsetof(U3CConvertCurrencyCoroU3Ec__Iterator1D_t2074409111, ___revenue_0)); }
	inline float get_revenue_0() const { return ___revenue_0; }
	inline float* get_address_of_revenue_0() { return &___revenue_0; }
	inline void set_revenue_0(float value)
	{
		___revenue_0 = value;
	}

	inline static int32_t get_offset_of_U3CusdRevenueU3E__0_1() { return static_cast<int32_t>(offsetof(U3CConvertCurrencyCoroU3Ec__Iterator1D_t2074409111, ___U3CusdRevenueU3E__0_1)); }
	inline float get_U3CusdRevenueU3E__0_1() const { return ___U3CusdRevenueU3E__0_1; }
	inline float* get_address_of_U3CusdRevenueU3E__0_1() { return &___U3CusdRevenueU3E__0_1; }
	inline void set_U3CusdRevenueU3E__0_1(float value)
	{
		___U3CusdRevenueU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_currency_2() { return static_cast<int32_t>(offsetof(U3CConvertCurrencyCoroU3Ec__Iterator1D_t2074409111, ___currency_2)); }
	inline String_t* get_currency_2() const { return ___currency_2; }
	inline String_t** get_address_of_currency_2() { return &___currency_2; }
	inline void set_currency_2(String_t* value)
	{
		___currency_2 = value;
		Il2CppCodeGenWriteBarrier(&___currency_2, value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__1_3() { return static_cast<int32_t>(offsetof(U3CConvertCurrencyCoroU3Ec__Iterator1D_t2074409111, ___U3CrequestU3E__1_3)); }
	inline WWW_t1522972100 * get_U3CrequestU3E__1_3() const { return ___U3CrequestU3E__1_3; }
	inline WWW_t1522972100 ** get_address_of_U3CrequestU3E__1_3() { return &___U3CrequestU3E__1_3; }
	inline void set_U3CrequestU3E__1_3(WWW_t1522972100 * value)
	{
		___U3CrequestU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrequestU3E__1_3, value);
	}

	inline static int32_t get_offset_of_U3CjsonU3E__2_4() { return static_cast<int32_t>(offsetof(U3CConvertCurrencyCoroU3Ec__Iterator1D_t2074409111, ___U3CjsonU3E__2_4)); }
	inline JSONNode_t580622632 * get_U3CjsonU3E__2_4() const { return ___U3CjsonU3E__2_4; }
	inline JSONNode_t580622632 ** get_address_of_U3CjsonU3E__2_4() { return &___U3CjsonU3E__2_4; }
	inline void set_U3CjsonU3E__2_4(JSONNode_t580622632 * value)
	{
		___U3CjsonU3E__2_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CjsonU3E__2_4, value);
	}

	inline static int32_t get_offset_of_U3CusdFromCurrU3E__3_5() { return static_cast<int32_t>(offsetof(U3CConvertCurrencyCoroU3Ec__Iterator1D_t2074409111, ___U3CusdFromCurrU3E__3_5)); }
	inline float get_U3CusdFromCurrU3E__3_5() const { return ___U3CusdFromCurrU3E__3_5; }
	inline float* get_address_of_U3CusdFromCurrU3E__3_5() { return &___U3CusdFromCurrU3E__3_5; }
	inline void set_U3CusdFromCurrU3E__3_5(float value)
	{
		___U3CusdFromCurrU3E__3_5 = value;
	}

	inline static int32_t get_offset_of_callback_6() { return static_cast<int32_t>(offsetof(U3CConvertCurrencyCoroU3Ec__Iterator1D_t2074409111, ___callback_6)); }
	inline Action_1_t1106661726 * get_callback_6() const { return ___callback_6; }
	inline Action_1_t1106661726 ** get_address_of_callback_6() { return &___callback_6; }
	inline void set_callback_6(Action_1_t1106661726 * value)
	{
		___callback_6 = value;
		Il2CppCodeGenWriteBarrier(&___callback_6, value);
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CConvertCurrencyCoroU3Ec__Iterator1D_t2074409111, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}

	inline static int32_t get_offset_of_U24current_8() { return static_cast<int32_t>(offsetof(U3CConvertCurrencyCoroU3Ec__Iterator1D_t2074409111, ___U24current_8)); }
	inline Il2CppObject * get_U24current_8() const { return ___U24current_8; }
	inline Il2CppObject ** get_address_of_U24current_8() { return &___U24current_8; }
	inline void set_U24current_8(Il2CppObject * value)
	{
		___U24current_8 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_8, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Erevenue_9() { return static_cast<int32_t>(offsetof(U3CConvertCurrencyCoroU3Ec__Iterator1D_t2074409111, ___U3CU24U3Erevenue_9)); }
	inline float get_U3CU24U3Erevenue_9() const { return ___U3CU24U3Erevenue_9; }
	inline float* get_address_of_U3CU24U3Erevenue_9() { return &___U3CU24U3Erevenue_9; }
	inline void set_U3CU24U3Erevenue_9(float value)
	{
		___U3CU24U3Erevenue_9 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Ecurrency_10() { return static_cast<int32_t>(offsetof(U3CConvertCurrencyCoroU3Ec__Iterator1D_t2074409111, ___U3CU24U3Ecurrency_10)); }
	inline String_t* get_U3CU24U3Ecurrency_10() const { return ___U3CU24U3Ecurrency_10; }
	inline String_t** get_address_of_U3CU24U3Ecurrency_10() { return &___U3CU24U3Ecurrency_10; }
	inline void set_U3CU24U3Ecurrency_10(String_t* value)
	{
		___U3CU24U3Ecurrency_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Ecurrency_10, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Ecallback_11() { return static_cast<int32_t>(offsetof(U3CConvertCurrencyCoroU3Ec__Iterator1D_t2074409111, ___U3CU24U3Ecallback_11)); }
	inline Action_1_t1106661726 * get_U3CU24U3Ecallback_11() const { return ___U3CU24U3Ecallback_11; }
	inline Action_1_t1106661726 ** get_address_of_U3CU24U3Ecallback_11() { return &___U3CU24U3Ecallback_11; }
	inline void set_U3CU24U3Ecallback_11(Action_1_t1106661726 * value)
	{
		___U3CU24U3Ecallback_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Ecallback_11, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_12() { return static_cast<int32_t>(offsetof(U3CConvertCurrencyCoroU3Ec__Iterator1D_t2074409111, ___U3CU3Ef__this_12)); }
	inline AnalyticsController_t2265609122 * get_U3CU3Ef__this_12() const { return ___U3CU3Ef__this_12; }
	inline AnalyticsController_t2265609122 ** get_address_of_U3CU3Ef__this_12() { return &___U3CU3Ef__this_12; }
	inline void set_U3CU3Ef__this_12(AnalyticsController_t2265609122 * value)
	{
		___U3CU3Ef__this_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
