﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// PlayFab.ErrorCallback
struct ErrorCallback_t582108558;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.HashSet`1<System.Reflection.MethodInfo>>
struct Dictionary_2_t3502640556;
// System.Collections.Generic.Dictionary`2<System.Reflection.MethodInfo,System.Collections.Generic.HashSet`1<System.Object>>
struct Dictionary_2_t3884860400;
// System.Collections.Generic.HashSet`1<System.Reflection.MethodInfo>
struct HashSet_1_t1864942652;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_t3667177573;
// System.Object[]
struct ObjectU5BU5D_t11523773;

#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharp_PlayFab_PlayFabLogLevel379797012.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_WebRequestTy2827971030.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.PlayFabSettings
struct  PlayFabSettings_t4113663895  : public Il2CppObject
{
public:

public:
};

struct PlayFabSettings_t4113663895_StaticFields
{
public:
	// PlayFab.ErrorCallback PlayFab.PlayFabSettings::GlobalErrorHandler
	ErrorCallback_t582108558 * ___GlobalErrorHandler_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.HashSet`1<System.Reflection.MethodInfo>> PlayFab.PlayFabSettings::ApiRequestHandlers
	Dictionary_2_t3502640556 * ___ApiRequestHandlers_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.HashSet`1<System.Reflection.MethodInfo>> PlayFab.PlayFabSettings::ApiResponseHandlers
	Dictionary_2_t3502640556 * ___ApiResponseHandlers_5;
	// System.Collections.Generic.Dictionary`2<System.Reflection.MethodInfo,System.Collections.Generic.HashSet`1<System.Object>> PlayFab.PlayFabSettings::CallbackInstances
	Dictionary_2_t3884860400 * ___CallbackInstances_6;
	// System.String PlayFab.PlayFabSettings::ProductionEnvironmentUrl
	String_t* ___ProductionEnvironmentUrl_7;
	// System.String PlayFab.PlayFabSettings::TitleId
	String_t* ___TitleId_8;
	// PlayFab.PlayFabLogLevel PlayFab.PlayFabSettings::LogLevel
	int32_t ___LogLevel_9;
	// System.Boolean PlayFab.PlayFabSettings::IsTesting
	bool ___IsTesting_10;
	// PlayFab.WebRequestType PlayFab.PlayFabSettings::RequestType
	int32_t ___RequestType_11;
	// System.Int32 PlayFab.PlayFabSettings::RequestTimeout
	int32_t ___RequestTimeout_12;
	// System.Boolean PlayFab.PlayFabSettings::RequestKeepAlive
	bool ___RequestKeepAlive_13;
	// System.String PlayFab.PlayFabSettings::LogicServerUrl
	String_t* ___LogicServerUrl_14;
	// System.String PlayFab.PlayFabSettings::AdvertisingIdType
	String_t* ___AdvertisingIdType_15;
	// System.String PlayFab.PlayFabSettings::AdvertisingIdValue
	String_t* ___AdvertisingIdValue_16;
	// System.Boolean PlayFab.PlayFabSettings::DisableAdvertising
	bool ___DisableAdvertising_17;
	// System.Collections.Generic.HashSet`1<System.Reflection.MethodInfo> PlayFab.PlayFabSettings::tempMI
	HashSet_1_t1864942652 * ___tempMI_18;
	// System.Collections.Generic.HashSet`1<System.String> PlayFab.PlayFabSettings::tempUrl
	HashSet_1_t3667177573 * ___tempUrl_19;
	// System.Object[] PlayFab.PlayFabSettings::RequestParams
	ObjectU5BU5D_t11523773* ___RequestParams_20;
	// System.Object[] PlayFab.PlayFabSettings::ResponseParams
	ObjectU5BU5D_t11523773* ___ResponseParams_21;

public:
	inline static int32_t get_offset_of_GlobalErrorHandler_3() { return static_cast<int32_t>(offsetof(PlayFabSettings_t4113663895_StaticFields, ___GlobalErrorHandler_3)); }
	inline ErrorCallback_t582108558 * get_GlobalErrorHandler_3() const { return ___GlobalErrorHandler_3; }
	inline ErrorCallback_t582108558 ** get_address_of_GlobalErrorHandler_3() { return &___GlobalErrorHandler_3; }
	inline void set_GlobalErrorHandler_3(ErrorCallback_t582108558 * value)
	{
		___GlobalErrorHandler_3 = value;
		Il2CppCodeGenWriteBarrier(&___GlobalErrorHandler_3, value);
	}

	inline static int32_t get_offset_of_ApiRequestHandlers_4() { return static_cast<int32_t>(offsetof(PlayFabSettings_t4113663895_StaticFields, ___ApiRequestHandlers_4)); }
	inline Dictionary_2_t3502640556 * get_ApiRequestHandlers_4() const { return ___ApiRequestHandlers_4; }
	inline Dictionary_2_t3502640556 ** get_address_of_ApiRequestHandlers_4() { return &___ApiRequestHandlers_4; }
	inline void set_ApiRequestHandlers_4(Dictionary_2_t3502640556 * value)
	{
		___ApiRequestHandlers_4 = value;
		Il2CppCodeGenWriteBarrier(&___ApiRequestHandlers_4, value);
	}

	inline static int32_t get_offset_of_ApiResponseHandlers_5() { return static_cast<int32_t>(offsetof(PlayFabSettings_t4113663895_StaticFields, ___ApiResponseHandlers_5)); }
	inline Dictionary_2_t3502640556 * get_ApiResponseHandlers_5() const { return ___ApiResponseHandlers_5; }
	inline Dictionary_2_t3502640556 ** get_address_of_ApiResponseHandlers_5() { return &___ApiResponseHandlers_5; }
	inline void set_ApiResponseHandlers_5(Dictionary_2_t3502640556 * value)
	{
		___ApiResponseHandlers_5 = value;
		Il2CppCodeGenWriteBarrier(&___ApiResponseHandlers_5, value);
	}

	inline static int32_t get_offset_of_CallbackInstances_6() { return static_cast<int32_t>(offsetof(PlayFabSettings_t4113663895_StaticFields, ___CallbackInstances_6)); }
	inline Dictionary_2_t3884860400 * get_CallbackInstances_6() const { return ___CallbackInstances_6; }
	inline Dictionary_2_t3884860400 ** get_address_of_CallbackInstances_6() { return &___CallbackInstances_6; }
	inline void set_CallbackInstances_6(Dictionary_2_t3884860400 * value)
	{
		___CallbackInstances_6 = value;
		Il2CppCodeGenWriteBarrier(&___CallbackInstances_6, value);
	}

	inline static int32_t get_offset_of_ProductionEnvironmentUrl_7() { return static_cast<int32_t>(offsetof(PlayFabSettings_t4113663895_StaticFields, ___ProductionEnvironmentUrl_7)); }
	inline String_t* get_ProductionEnvironmentUrl_7() const { return ___ProductionEnvironmentUrl_7; }
	inline String_t** get_address_of_ProductionEnvironmentUrl_7() { return &___ProductionEnvironmentUrl_7; }
	inline void set_ProductionEnvironmentUrl_7(String_t* value)
	{
		___ProductionEnvironmentUrl_7 = value;
		Il2CppCodeGenWriteBarrier(&___ProductionEnvironmentUrl_7, value);
	}

	inline static int32_t get_offset_of_TitleId_8() { return static_cast<int32_t>(offsetof(PlayFabSettings_t4113663895_StaticFields, ___TitleId_8)); }
	inline String_t* get_TitleId_8() const { return ___TitleId_8; }
	inline String_t** get_address_of_TitleId_8() { return &___TitleId_8; }
	inline void set_TitleId_8(String_t* value)
	{
		___TitleId_8 = value;
		Il2CppCodeGenWriteBarrier(&___TitleId_8, value);
	}

	inline static int32_t get_offset_of_LogLevel_9() { return static_cast<int32_t>(offsetof(PlayFabSettings_t4113663895_StaticFields, ___LogLevel_9)); }
	inline int32_t get_LogLevel_9() const { return ___LogLevel_9; }
	inline int32_t* get_address_of_LogLevel_9() { return &___LogLevel_9; }
	inline void set_LogLevel_9(int32_t value)
	{
		___LogLevel_9 = value;
	}

	inline static int32_t get_offset_of_IsTesting_10() { return static_cast<int32_t>(offsetof(PlayFabSettings_t4113663895_StaticFields, ___IsTesting_10)); }
	inline bool get_IsTesting_10() const { return ___IsTesting_10; }
	inline bool* get_address_of_IsTesting_10() { return &___IsTesting_10; }
	inline void set_IsTesting_10(bool value)
	{
		___IsTesting_10 = value;
	}

	inline static int32_t get_offset_of_RequestType_11() { return static_cast<int32_t>(offsetof(PlayFabSettings_t4113663895_StaticFields, ___RequestType_11)); }
	inline int32_t get_RequestType_11() const { return ___RequestType_11; }
	inline int32_t* get_address_of_RequestType_11() { return &___RequestType_11; }
	inline void set_RequestType_11(int32_t value)
	{
		___RequestType_11 = value;
	}

	inline static int32_t get_offset_of_RequestTimeout_12() { return static_cast<int32_t>(offsetof(PlayFabSettings_t4113663895_StaticFields, ___RequestTimeout_12)); }
	inline int32_t get_RequestTimeout_12() const { return ___RequestTimeout_12; }
	inline int32_t* get_address_of_RequestTimeout_12() { return &___RequestTimeout_12; }
	inline void set_RequestTimeout_12(int32_t value)
	{
		___RequestTimeout_12 = value;
	}

	inline static int32_t get_offset_of_RequestKeepAlive_13() { return static_cast<int32_t>(offsetof(PlayFabSettings_t4113663895_StaticFields, ___RequestKeepAlive_13)); }
	inline bool get_RequestKeepAlive_13() const { return ___RequestKeepAlive_13; }
	inline bool* get_address_of_RequestKeepAlive_13() { return &___RequestKeepAlive_13; }
	inline void set_RequestKeepAlive_13(bool value)
	{
		___RequestKeepAlive_13 = value;
	}

	inline static int32_t get_offset_of_LogicServerUrl_14() { return static_cast<int32_t>(offsetof(PlayFabSettings_t4113663895_StaticFields, ___LogicServerUrl_14)); }
	inline String_t* get_LogicServerUrl_14() const { return ___LogicServerUrl_14; }
	inline String_t** get_address_of_LogicServerUrl_14() { return &___LogicServerUrl_14; }
	inline void set_LogicServerUrl_14(String_t* value)
	{
		___LogicServerUrl_14 = value;
		Il2CppCodeGenWriteBarrier(&___LogicServerUrl_14, value);
	}

	inline static int32_t get_offset_of_AdvertisingIdType_15() { return static_cast<int32_t>(offsetof(PlayFabSettings_t4113663895_StaticFields, ___AdvertisingIdType_15)); }
	inline String_t* get_AdvertisingIdType_15() const { return ___AdvertisingIdType_15; }
	inline String_t** get_address_of_AdvertisingIdType_15() { return &___AdvertisingIdType_15; }
	inline void set_AdvertisingIdType_15(String_t* value)
	{
		___AdvertisingIdType_15 = value;
		Il2CppCodeGenWriteBarrier(&___AdvertisingIdType_15, value);
	}

	inline static int32_t get_offset_of_AdvertisingIdValue_16() { return static_cast<int32_t>(offsetof(PlayFabSettings_t4113663895_StaticFields, ___AdvertisingIdValue_16)); }
	inline String_t* get_AdvertisingIdValue_16() const { return ___AdvertisingIdValue_16; }
	inline String_t** get_address_of_AdvertisingIdValue_16() { return &___AdvertisingIdValue_16; }
	inline void set_AdvertisingIdValue_16(String_t* value)
	{
		___AdvertisingIdValue_16 = value;
		Il2CppCodeGenWriteBarrier(&___AdvertisingIdValue_16, value);
	}

	inline static int32_t get_offset_of_DisableAdvertising_17() { return static_cast<int32_t>(offsetof(PlayFabSettings_t4113663895_StaticFields, ___DisableAdvertising_17)); }
	inline bool get_DisableAdvertising_17() const { return ___DisableAdvertising_17; }
	inline bool* get_address_of_DisableAdvertising_17() { return &___DisableAdvertising_17; }
	inline void set_DisableAdvertising_17(bool value)
	{
		___DisableAdvertising_17 = value;
	}

	inline static int32_t get_offset_of_tempMI_18() { return static_cast<int32_t>(offsetof(PlayFabSettings_t4113663895_StaticFields, ___tempMI_18)); }
	inline HashSet_1_t1864942652 * get_tempMI_18() const { return ___tempMI_18; }
	inline HashSet_1_t1864942652 ** get_address_of_tempMI_18() { return &___tempMI_18; }
	inline void set_tempMI_18(HashSet_1_t1864942652 * value)
	{
		___tempMI_18 = value;
		Il2CppCodeGenWriteBarrier(&___tempMI_18, value);
	}

	inline static int32_t get_offset_of_tempUrl_19() { return static_cast<int32_t>(offsetof(PlayFabSettings_t4113663895_StaticFields, ___tempUrl_19)); }
	inline HashSet_1_t3667177573 * get_tempUrl_19() const { return ___tempUrl_19; }
	inline HashSet_1_t3667177573 ** get_address_of_tempUrl_19() { return &___tempUrl_19; }
	inline void set_tempUrl_19(HashSet_1_t3667177573 * value)
	{
		___tempUrl_19 = value;
		Il2CppCodeGenWriteBarrier(&___tempUrl_19, value);
	}

	inline static int32_t get_offset_of_RequestParams_20() { return static_cast<int32_t>(offsetof(PlayFabSettings_t4113663895_StaticFields, ___RequestParams_20)); }
	inline ObjectU5BU5D_t11523773* get_RequestParams_20() const { return ___RequestParams_20; }
	inline ObjectU5BU5D_t11523773** get_address_of_RequestParams_20() { return &___RequestParams_20; }
	inline void set_RequestParams_20(ObjectU5BU5D_t11523773* value)
	{
		___RequestParams_20 = value;
		Il2CppCodeGenWriteBarrier(&___RequestParams_20, value);
	}

	inline static int32_t get_offset_of_ResponseParams_21() { return static_cast<int32_t>(offsetof(PlayFabSettings_t4113663895_StaticFields, ___ResponseParams_21)); }
	inline ObjectU5BU5D_t11523773* get_ResponseParams_21() const { return ___ResponseParams_21; }
	inline ObjectU5BU5D_t11523773** get_address_of_ResponseParams_21() { return &___ResponseParams_21; }
	inline void set_ResponseParams_21(ObjectU5BU5D_t11523773* value)
	{
		___ResponseParams_21 = value;
		Il2CppCodeGenWriteBarrier(&___ResponseParams_21, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
