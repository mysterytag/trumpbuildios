﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// WindowManager
struct WindowManager_t3316821373;
// BarygaVkController
struct BarygaVkController_t3617163921;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BarygaVkController/<InviteFriends>c__AnonStoreyDF
struct  U3CInviteFriendsU3Ec__AnonStoreyDF_t3157660603  : public Il2CppObject
{
public:
	// WindowManager BarygaVkController/<InviteFriends>c__AnonStoreyDF::manager
	WindowManager_t3316821373 * ___manager_0;
	// BarygaVkController BarygaVkController/<InviteFriends>c__AnonStoreyDF::<>f__this
	BarygaVkController_t3617163921 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_manager_0() { return static_cast<int32_t>(offsetof(U3CInviteFriendsU3Ec__AnonStoreyDF_t3157660603, ___manager_0)); }
	inline WindowManager_t3316821373 * get_manager_0() const { return ___manager_0; }
	inline WindowManager_t3316821373 ** get_address_of_manager_0() { return &___manager_0; }
	inline void set_manager_0(WindowManager_t3316821373 * value)
	{
		___manager_0 = value;
		Il2CppCodeGenWriteBarrier(&___manager_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CInviteFriendsU3Ec__AnonStoreyDF_t3157660603, ___U3CU3Ef__this_1)); }
	inline BarygaVkController_t3617163921 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline BarygaVkController_t3617163921 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(BarygaVkController_t3617163921 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
