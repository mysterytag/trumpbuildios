﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TestCoro/<Bearutine2>c__Iterator30
struct U3CBearutine2U3Ec__Iterator30_t3407062897;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void TestCoro/<Bearutine2>c__Iterator30::.ctor()
extern "C"  void U3CBearutine2U3Ec__Iterator30__ctor_m2341744478 (U3CBearutine2U3Ec__Iterator30_t3407062897 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TestCoro/<Bearutine2>c__Iterator30::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CBearutine2U3Ec__Iterator30_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1307779070 (U3CBearutine2U3Ec__Iterator30_t3407062897 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TestCoro/<Bearutine2>c__Iterator30::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CBearutine2U3Ec__Iterator30_System_Collections_IEnumerator_get_Current_m229695378 (U3CBearutine2U3Ec__Iterator30_t3407062897 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TestCoro/<Bearutine2>c__Iterator30::MoveNext()
extern "C"  bool U3CBearutine2U3Ec__Iterator30_MoveNext_m3855168702 (U3CBearutine2U3Ec__Iterator30_t3407062897 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestCoro/<Bearutine2>c__Iterator30::Dispose()
extern "C"  void U3CBearutine2U3Ec__Iterator30_Dispose_m4046830619 (U3CBearutine2U3Ec__Iterator30_t3407062897 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestCoro/<Bearutine2>c__Iterator30::Reset()
extern "C"  void U3CBearutine2U3Ec__Iterator30_Reset_m4283144715 (U3CBearutine2U3Ec__Iterator30_t3407062897 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
