﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IconsProvider/<Get>c__AnonStoreyE8
struct  U3CGetU3Ec__AnonStoreyE8_t497370582  : public Il2CppObject
{
public:
	// System.String IconsProvider/<Get>c__AnonStoreyE8::s
	String_t* ___s_0;

public:
	inline static int32_t get_offset_of_s_0() { return static_cast<int32_t>(offsetof(U3CGetU3Ec__AnonStoreyE8_t497370582, ___s_0)); }
	inline String_t* get_s_0() const { return ___s_0; }
	inline String_t** get_address_of_s_0() { return &___s_0; }
	inline void set_s_0(String_t* value)
	{
		___s_0 = value;
		Il2CppCodeGenWriteBarrier(&___s_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
