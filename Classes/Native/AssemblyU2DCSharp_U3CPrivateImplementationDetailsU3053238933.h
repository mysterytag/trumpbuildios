﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3214874490.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3336100496.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2366143930.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2366141822.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2366141818.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3214874676.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3214874647.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2366141793.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3214874515.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2366145633.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2366142878.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3335951570.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2366141789.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3053238940  : public Il2CppObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3053238940_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType$16 <PrivateImplementationDetails>::$$field-0
	U24ArrayTypeU2416_t214874494  ___U24U24fieldU2D0_0;
	// <PrivateImplementationDetails>/$ArrayType$6144 <PrivateImplementationDetails>::$$field-1
	U24ArrayTypeU246144_t336100496  ___U24U24fieldU2D1_1;
	// <PrivateImplementationDetails>/$ArrayType$384 <PrivateImplementationDetails>::$$field-2
	U24ArrayTypeU24384_t2366143930  ___U24U24fieldU2D2_2;
	// <PrivateImplementationDetails>/$ArrayType$124 <PrivateImplementationDetails>::$$field-3
	U24ArrayTypeU24124_t2366141823  ___U24U24fieldU2D3_3;
	// <PrivateImplementationDetails>/$ArrayType$124 <PrivateImplementationDetails>::$$field-4
	U24ArrayTypeU24124_t2366141823  ___U24U24fieldU2D4_4;
	// <PrivateImplementationDetails>/$ArrayType$120 <PrivateImplementationDetails>::$$field-5
	U24ArrayTypeU24120_t2366141820  ___U24U24fieldU2D5_5;
	// <PrivateImplementationDetails>/$ArrayType$120 <PrivateImplementationDetails>::$$field-6
	U24ArrayTypeU24120_t2366141820  ___U24U24fieldU2D6_6;
	// <PrivateImplementationDetails>/$ArrayType$76 <PrivateImplementationDetails>::$$field-7
	U24ArrayTypeU2476_t214874676  ___U24U24fieldU2D7_7;
	// <PrivateImplementationDetails>/$ArrayType$68 <PrivateImplementationDetails>::$$field-8
	U24ArrayTypeU2468_t214874647  ___U24U24fieldU2D8_8;
	// <PrivateImplementationDetails>/$ArrayType$116 <PrivateImplementationDetails>::$$field-9
	U24ArrayTypeU24116_t2366141793  ___U24U24fieldU2D9_9;
	// <PrivateImplementationDetails>/$ArrayType$120 <PrivateImplementationDetails>::$$field-10
	U24ArrayTypeU24120_t2366141820  ___U24U24fieldU2D10_10;
	// <PrivateImplementationDetails>/$ArrayType$20 <PrivateImplementationDetails>::$$field-11
	U24ArrayTypeU2420_t214874517  ___U24U24fieldU2D11_11;
	// <PrivateImplementationDetails>/$ArrayType$512 <PrivateImplementationDetails>::$$field-12
	U24ArrayTypeU24512_t2366145633  ___U24U24fieldU2D12_12;
	// <PrivateImplementationDetails>/$ArrayType$256 <PrivateImplementationDetails>::$$field-13
	U24ArrayTypeU24256_t2366142882  ___U24U24fieldU2D13_13;
	// <PrivateImplementationDetails>/$ArrayType$116 <PrivateImplementationDetails>::$$field-14
	U24ArrayTypeU24116_t2366141793  ___U24U24fieldU2D14_14;
	// <PrivateImplementationDetails>/$ArrayType$120 <PrivateImplementationDetails>::$$field-15
	U24ArrayTypeU24120_t2366141820  ___U24U24fieldU2D15_15;
	// <PrivateImplementationDetails>/$ArrayType$1152 <PrivateImplementationDetails>::$$field-16
	U24ArrayTypeU241152_t335951570  ___U24U24fieldU2D16_16;
	// <PrivateImplementationDetails>/$ArrayType$120 <PrivateImplementationDetails>::$$field-17
	U24ArrayTypeU24120_t2366141820  ___U24U24fieldU2D17_17;
	// <PrivateImplementationDetails>/$ArrayType$20 <PrivateImplementationDetails>::$$field-18
	U24ArrayTypeU2420_t214874517  ___U24U24fieldU2D18_18;
	// <PrivateImplementationDetails>/$ArrayType$112 <PrivateImplementationDetails>::$$field-19
	U24ArrayTypeU24112_t2366141789  ___U24U24fieldU2D19_19;

public:
	inline static int32_t get_offset_of_U24U24fieldU2D0_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3053238940_StaticFields, ___U24U24fieldU2D0_0)); }
	inline U24ArrayTypeU2416_t214874494  get_U24U24fieldU2D0_0() const { return ___U24U24fieldU2D0_0; }
	inline U24ArrayTypeU2416_t214874494 * get_address_of_U24U24fieldU2D0_0() { return &___U24U24fieldU2D0_0; }
	inline void set_U24U24fieldU2D0_0(U24ArrayTypeU2416_t214874494  value)
	{
		___U24U24fieldU2D0_0 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D1_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3053238940_StaticFields, ___U24U24fieldU2D1_1)); }
	inline U24ArrayTypeU246144_t336100496  get_U24U24fieldU2D1_1() const { return ___U24U24fieldU2D1_1; }
	inline U24ArrayTypeU246144_t336100496 * get_address_of_U24U24fieldU2D1_1() { return &___U24U24fieldU2D1_1; }
	inline void set_U24U24fieldU2D1_1(U24ArrayTypeU246144_t336100496  value)
	{
		___U24U24fieldU2D1_1 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D2_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3053238940_StaticFields, ___U24U24fieldU2D2_2)); }
	inline U24ArrayTypeU24384_t2366143930  get_U24U24fieldU2D2_2() const { return ___U24U24fieldU2D2_2; }
	inline U24ArrayTypeU24384_t2366143930 * get_address_of_U24U24fieldU2D2_2() { return &___U24U24fieldU2D2_2; }
	inline void set_U24U24fieldU2D2_2(U24ArrayTypeU24384_t2366143930  value)
	{
		___U24U24fieldU2D2_2 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D3_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3053238940_StaticFields, ___U24U24fieldU2D3_3)); }
	inline U24ArrayTypeU24124_t2366141823  get_U24U24fieldU2D3_3() const { return ___U24U24fieldU2D3_3; }
	inline U24ArrayTypeU24124_t2366141823 * get_address_of_U24U24fieldU2D3_3() { return &___U24U24fieldU2D3_3; }
	inline void set_U24U24fieldU2D3_3(U24ArrayTypeU24124_t2366141823  value)
	{
		___U24U24fieldU2D3_3 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D4_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3053238940_StaticFields, ___U24U24fieldU2D4_4)); }
	inline U24ArrayTypeU24124_t2366141823  get_U24U24fieldU2D4_4() const { return ___U24U24fieldU2D4_4; }
	inline U24ArrayTypeU24124_t2366141823 * get_address_of_U24U24fieldU2D4_4() { return &___U24U24fieldU2D4_4; }
	inline void set_U24U24fieldU2D4_4(U24ArrayTypeU24124_t2366141823  value)
	{
		___U24U24fieldU2D4_4 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D5_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3053238940_StaticFields, ___U24U24fieldU2D5_5)); }
	inline U24ArrayTypeU24120_t2366141820  get_U24U24fieldU2D5_5() const { return ___U24U24fieldU2D5_5; }
	inline U24ArrayTypeU24120_t2366141820 * get_address_of_U24U24fieldU2D5_5() { return &___U24U24fieldU2D5_5; }
	inline void set_U24U24fieldU2D5_5(U24ArrayTypeU24120_t2366141820  value)
	{
		___U24U24fieldU2D5_5 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D6_6() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3053238940_StaticFields, ___U24U24fieldU2D6_6)); }
	inline U24ArrayTypeU24120_t2366141820  get_U24U24fieldU2D6_6() const { return ___U24U24fieldU2D6_6; }
	inline U24ArrayTypeU24120_t2366141820 * get_address_of_U24U24fieldU2D6_6() { return &___U24U24fieldU2D6_6; }
	inline void set_U24U24fieldU2D6_6(U24ArrayTypeU24120_t2366141820  value)
	{
		___U24U24fieldU2D6_6 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D7_7() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3053238940_StaticFields, ___U24U24fieldU2D7_7)); }
	inline U24ArrayTypeU2476_t214874676  get_U24U24fieldU2D7_7() const { return ___U24U24fieldU2D7_7; }
	inline U24ArrayTypeU2476_t214874676 * get_address_of_U24U24fieldU2D7_7() { return &___U24U24fieldU2D7_7; }
	inline void set_U24U24fieldU2D7_7(U24ArrayTypeU2476_t214874676  value)
	{
		___U24U24fieldU2D7_7 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D8_8() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3053238940_StaticFields, ___U24U24fieldU2D8_8)); }
	inline U24ArrayTypeU2468_t214874647  get_U24U24fieldU2D8_8() const { return ___U24U24fieldU2D8_8; }
	inline U24ArrayTypeU2468_t214874647 * get_address_of_U24U24fieldU2D8_8() { return &___U24U24fieldU2D8_8; }
	inline void set_U24U24fieldU2D8_8(U24ArrayTypeU2468_t214874647  value)
	{
		___U24U24fieldU2D8_8 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D9_9() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3053238940_StaticFields, ___U24U24fieldU2D9_9)); }
	inline U24ArrayTypeU24116_t2366141793  get_U24U24fieldU2D9_9() const { return ___U24U24fieldU2D9_9; }
	inline U24ArrayTypeU24116_t2366141793 * get_address_of_U24U24fieldU2D9_9() { return &___U24U24fieldU2D9_9; }
	inline void set_U24U24fieldU2D9_9(U24ArrayTypeU24116_t2366141793  value)
	{
		___U24U24fieldU2D9_9 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D10_10() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3053238940_StaticFields, ___U24U24fieldU2D10_10)); }
	inline U24ArrayTypeU24120_t2366141820  get_U24U24fieldU2D10_10() const { return ___U24U24fieldU2D10_10; }
	inline U24ArrayTypeU24120_t2366141820 * get_address_of_U24U24fieldU2D10_10() { return &___U24U24fieldU2D10_10; }
	inline void set_U24U24fieldU2D10_10(U24ArrayTypeU24120_t2366141820  value)
	{
		___U24U24fieldU2D10_10 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D11_11() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3053238940_StaticFields, ___U24U24fieldU2D11_11)); }
	inline U24ArrayTypeU2420_t214874517  get_U24U24fieldU2D11_11() const { return ___U24U24fieldU2D11_11; }
	inline U24ArrayTypeU2420_t214874517 * get_address_of_U24U24fieldU2D11_11() { return &___U24U24fieldU2D11_11; }
	inline void set_U24U24fieldU2D11_11(U24ArrayTypeU2420_t214874517  value)
	{
		___U24U24fieldU2D11_11 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D12_12() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3053238940_StaticFields, ___U24U24fieldU2D12_12)); }
	inline U24ArrayTypeU24512_t2366145633  get_U24U24fieldU2D12_12() const { return ___U24U24fieldU2D12_12; }
	inline U24ArrayTypeU24512_t2366145633 * get_address_of_U24U24fieldU2D12_12() { return &___U24U24fieldU2D12_12; }
	inline void set_U24U24fieldU2D12_12(U24ArrayTypeU24512_t2366145633  value)
	{
		___U24U24fieldU2D12_12 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D13_13() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3053238940_StaticFields, ___U24U24fieldU2D13_13)); }
	inline U24ArrayTypeU24256_t2366142882  get_U24U24fieldU2D13_13() const { return ___U24U24fieldU2D13_13; }
	inline U24ArrayTypeU24256_t2366142882 * get_address_of_U24U24fieldU2D13_13() { return &___U24U24fieldU2D13_13; }
	inline void set_U24U24fieldU2D13_13(U24ArrayTypeU24256_t2366142882  value)
	{
		___U24U24fieldU2D13_13 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D14_14() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3053238940_StaticFields, ___U24U24fieldU2D14_14)); }
	inline U24ArrayTypeU24116_t2366141793  get_U24U24fieldU2D14_14() const { return ___U24U24fieldU2D14_14; }
	inline U24ArrayTypeU24116_t2366141793 * get_address_of_U24U24fieldU2D14_14() { return &___U24U24fieldU2D14_14; }
	inline void set_U24U24fieldU2D14_14(U24ArrayTypeU24116_t2366141793  value)
	{
		___U24U24fieldU2D14_14 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D15_15() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3053238940_StaticFields, ___U24U24fieldU2D15_15)); }
	inline U24ArrayTypeU24120_t2366141820  get_U24U24fieldU2D15_15() const { return ___U24U24fieldU2D15_15; }
	inline U24ArrayTypeU24120_t2366141820 * get_address_of_U24U24fieldU2D15_15() { return &___U24U24fieldU2D15_15; }
	inline void set_U24U24fieldU2D15_15(U24ArrayTypeU24120_t2366141820  value)
	{
		___U24U24fieldU2D15_15 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D16_16() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3053238940_StaticFields, ___U24U24fieldU2D16_16)); }
	inline U24ArrayTypeU241152_t335951570  get_U24U24fieldU2D16_16() const { return ___U24U24fieldU2D16_16; }
	inline U24ArrayTypeU241152_t335951570 * get_address_of_U24U24fieldU2D16_16() { return &___U24U24fieldU2D16_16; }
	inline void set_U24U24fieldU2D16_16(U24ArrayTypeU241152_t335951570  value)
	{
		___U24U24fieldU2D16_16 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D17_17() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3053238940_StaticFields, ___U24U24fieldU2D17_17)); }
	inline U24ArrayTypeU24120_t2366141820  get_U24U24fieldU2D17_17() const { return ___U24U24fieldU2D17_17; }
	inline U24ArrayTypeU24120_t2366141820 * get_address_of_U24U24fieldU2D17_17() { return &___U24U24fieldU2D17_17; }
	inline void set_U24U24fieldU2D17_17(U24ArrayTypeU24120_t2366141820  value)
	{
		___U24U24fieldU2D17_17 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D18_18() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3053238940_StaticFields, ___U24U24fieldU2D18_18)); }
	inline U24ArrayTypeU2420_t214874517  get_U24U24fieldU2D18_18() const { return ___U24U24fieldU2D18_18; }
	inline U24ArrayTypeU2420_t214874517 * get_address_of_U24U24fieldU2D18_18() { return &___U24U24fieldU2D18_18; }
	inline void set_U24U24fieldU2D18_18(U24ArrayTypeU2420_t214874517  value)
	{
		___U24U24fieldU2D18_18 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D19_19() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3053238940_StaticFields, ___U24U24fieldU2D19_19)); }
	inline U24ArrayTypeU24112_t2366141789  get_U24U24fieldU2D19_19() const { return ___U24U24fieldU2D19_19; }
	inline U24ArrayTypeU24112_t2366141789 * get_address_of_U24U24fieldU2D19_19() { return &___U24U24fieldU2D19_19; }
	inline void set_U24U24fieldU2D19_19(U24ArrayTypeU24112_t2366141789  value)
	{
		___U24U24fieldU2D19_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
