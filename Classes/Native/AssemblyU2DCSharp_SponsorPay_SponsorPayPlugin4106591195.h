﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// SponsorPay.ISponsorPayPlugin
struct ISponsorPayPlugin_t4113951268;
// SponsorPay.SPUser
struct SPUser_t2606281602;
// SponsorPay.DeltaOfCoinsResponseReceivedHandler
struct DeltaOfCoinsResponseReceivedHandler_t2871420975;
// SponsorPay.SuccessfulCurrencyResponseReceivedHandler
struct SuccessfulCurrencyResponseReceivedHandler_t2285112823;
// SponsorPay.ErrorHandler
struct ErrorHandler_t1258969084;
// SponsorPay.OfferWallResultHandler
struct OfferWallResultHandler_t3220154273;
// SponsorPay.BrandEngageRequestResponseReceivedHandler
struct BrandEngageRequestResponseReceivedHandler_t263189403;
// SponsorPay.BrandEngageRequestErrorReceivedHandler
struct BrandEngageRequestErrorReceivedHandler_t816797986;
// SponsorPay.BrandEngageResultHandler
struct BrandEngageResultHandler_t570975071;
// SponsorPay.InterstitialRequestResponseReceivedHandler
struct InterstitialRequestResponseReceivedHandler_t2506718783;
// SponsorPay.InterstitialRequestErrorReceivedHandler
struct InterstitialRequestErrorReceivedHandler_t2305716478;
// SponsorPay.InterstitialStatusCloseHandler
struct InterstitialStatusCloseHandler_t3952291690;
// SponsorPay.InterstitialStatusErrorHandler
struct InterstitialStatusErrorHandler_t1250724186;
// SponsorPay.NativeExceptionHandler
struct NativeExceptionHandler_t1751116300;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SponsorPay.SponsorPayPlugin
struct  SponsorPayPlugin_t4106591195  : public Il2CppObject
{
public:
	// SponsorPay.ISponsorPayPlugin SponsorPay.SponsorPayPlugin::plugin
	Il2CppObject * ___plugin_1;
	// SponsorPay.SPUser SponsorPay.SponsorPayPlugin::SPUserInstance
	SPUser_t2606281602 * ___SPUserInstance_2;
	// SponsorPay.DeltaOfCoinsResponseReceivedHandler SponsorPay.SponsorPayPlugin::OnDeltaOfCoinsReceived
	DeltaOfCoinsResponseReceivedHandler_t2871420975 * ___OnDeltaOfCoinsReceived_3;
	// SponsorPay.SuccessfulCurrencyResponseReceivedHandler SponsorPay.SponsorPayPlugin::OnSuccessfulCurrencyRequestReceived
	SuccessfulCurrencyResponseReceivedHandler_t2285112823 * ___OnSuccessfulCurrencyRequestReceived_4;
	// SponsorPay.ErrorHandler SponsorPay.SponsorPayPlugin::OnDeltaOfCoinsRequestFailed
	ErrorHandler_t1258969084 * ___OnDeltaOfCoinsRequestFailed_5;
	// SponsorPay.OfferWallResultHandler SponsorPay.SponsorPayPlugin::OnOfferWallResultReceived
	OfferWallResultHandler_t3220154273 * ___OnOfferWallResultReceived_6;
	// SponsorPay.BrandEngageRequestResponseReceivedHandler SponsorPay.SponsorPayPlugin::OnBrandEngageRequestResponseReceived
	BrandEngageRequestResponseReceivedHandler_t263189403 * ___OnBrandEngageRequestResponseReceived_7;
	// SponsorPay.BrandEngageRequestErrorReceivedHandler SponsorPay.SponsorPayPlugin::OnBrandEngageRequestErrorReceived
	BrandEngageRequestErrorReceivedHandler_t816797986 * ___OnBrandEngageRequestErrorReceived_8;
	// SponsorPay.BrandEngageResultHandler SponsorPay.SponsorPayPlugin::OnBrandEngageResultReceived
	BrandEngageResultHandler_t570975071 * ___OnBrandEngageResultReceived_9;
	// SponsorPay.InterstitialRequestResponseReceivedHandler SponsorPay.SponsorPayPlugin::OnInterstitialRequestResponseReceived
	InterstitialRequestResponseReceivedHandler_t2506718783 * ___OnInterstitialRequestResponseReceived_10;
	// SponsorPay.InterstitialRequestErrorReceivedHandler SponsorPay.SponsorPayPlugin::OnInterstitialRequestErrorReceived
	InterstitialRequestErrorReceivedHandler_t2305716478 * ___OnInterstitialRequestErrorReceived_11;
	// SponsorPay.InterstitialStatusCloseHandler SponsorPay.SponsorPayPlugin::OnInterstitialStatusCloseReceived
	InterstitialStatusCloseHandler_t3952291690 * ___OnInterstitialStatusCloseReceived_12;
	// SponsorPay.InterstitialStatusErrorHandler SponsorPay.SponsorPayPlugin::OnInterstitialStatusErrorReceived
	InterstitialStatusErrorHandler_t1250724186 * ___OnInterstitialStatusErrorReceived_13;
	// SponsorPay.NativeExceptionHandler SponsorPay.SponsorPayPlugin::OnNativeExceptionReceived
	NativeExceptionHandler_t1751116300 * ___OnNativeExceptionReceived_14;

public:
	inline static int32_t get_offset_of_plugin_1() { return static_cast<int32_t>(offsetof(SponsorPayPlugin_t4106591195, ___plugin_1)); }
	inline Il2CppObject * get_plugin_1() const { return ___plugin_1; }
	inline Il2CppObject ** get_address_of_plugin_1() { return &___plugin_1; }
	inline void set_plugin_1(Il2CppObject * value)
	{
		___plugin_1 = value;
		Il2CppCodeGenWriteBarrier(&___plugin_1, value);
	}

	inline static int32_t get_offset_of_SPUserInstance_2() { return static_cast<int32_t>(offsetof(SponsorPayPlugin_t4106591195, ___SPUserInstance_2)); }
	inline SPUser_t2606281602 * get_SPUserInstance_2() const { return ___SPUserInstance_2; }
	inline SPUser_t2606281602 ** get_address_of_SPUserInstance_2() { return &___SPUserInstance_2; }
	inline void set_SPUserInstance_2(SPUser_t2606281602 * value)
	{
		___SPUserInstance_2 = value;
		Il2CppCodeGenWriteBarrier(&___SPUserInstance_2, value);
	}

	inline static int32_t get_offset_of_OnDeltaOfCoinsReceived_3() { return static_cast<int32_t>(offsetof(SponsorPayPlugin_t4106591195, ___OnDeltaOfCoinsReceived_3)); }
	inline DeltaOfCoinsResponseReceivedHandler_t2871420975 * get_OnDeltaOfCoinsReceived_3() const { return ___OnDeltaOfCoinsReceived_3; }
	inline DeltaOfCoinsResponseReceivedHandler_t2871420975 ** get_address_of_OnDeltaOfCoinsReceived_3() { return &___OnDeltaOfCoinsReceived_3; }
	inline void set_OnDeltaOfCoinsReceived_3(DeltaOfCoinsResponseReceivedHandler_t2871420975 * value)
	{
		___OnDeltaOfCoinsReceived_3 = value;
		Il2CppCodeGenWriteBarrier(&___OnDeltaOfCoinsReceived_3, value);
	}

	inline static int32_t get_offset_of_OnSuccessfulCurrencyRequestReceived_4() { return static_cast<int32_t>(offsetof(SponsorPayPlugin_t4106591195, ___OnSuccessfulCurrencyRequestReceived_4)); }
	inline SuccessfulCurrencyResponseReceivedHandler_t2285112823 * get_OnSuccessfulCurrencyRequestReceived_4() const { return ___OnSuccessfulCurrencyRequestReceived_4; }
	inline SuccessfulCurrencyResponseReceivedHandler_t2285112823 ** get_address_of_OnSuccessfulCurrencyRequestReceived_4() { return &___OnSuccessfulCurrencyRequestReceived_4; }
	inline void set_OnSuccessfulCurrencyRequestReceived_4(SuccessfulCurrencyResponseReceivedHandler_t2285112823 * value)
	{
		___OnSuccessfulCurrencyRequestReceived_4 = value;
		Il2CppCodeGenWriteBarrier(&___OnSuccessfulCurrencyRequestReceived_4, value);
	}

	inline static int32_t get_offset_of_OnDeltaOfCoinsRequestFailed_5() { return static_cast<int32_t>(offsetof(SponsorPayPlugin_t4106591195, ___OnDeltaOfCoinsRequestFailed_5)); }
	inline ErrorHandler_t1258969084 * get_OnDeltaOfCoinsRequestFailed_5() const { return ___OnDeltaOfCoinsRequestFailed_5; }
	inline ErrorHandler_t1258969084 ** get_address_of_OnDeltaOfCoinsRequestFailed_5() { return &___OnDeltaOfCoinsRequestFailed_5; }
	inline void set_OnDeltaOfCoinsRequestFailed_5(ErrorHandler_t1258969084 * value)
	{
		___OnDeltaOfCoinsRequestFailed_5 = value;
		Il2CppCodeGenWriteBarrier(&___OnDeltaOfCoinsRequestFailed_5, value);
	}

	inline static int32_t get_offset_of_OnOfferWallResultReceived_6() { return static_cast<int32_t>(offsetof(SponsorPayPlugin_t4106591195, ___OnOfferWallResultReceived_6)); }
	inline OfferWallResultHandler_t3220154273 * get_OnOfferWallResultReceived_6() const { return ___OnOfferWallResultReceived_6; }
	inline OfferWallResultHandler_t3220154273 ** get_address_of_OnOfferWallResultReceived_6() { return &___OnOfferWallResultReceived_6; }
	inline void set_OnOfferWallResultReceived_6(OfferWallResultHandler_t3220154273 * value)
	{
		___OnOfferWallResultReceived_6 = value;
		Il2CppCodeGenWriteBarrier(&___OnOfferWallResultReceived_6, value);
	}

	inline static int32_t get_offset_of_OnBrandEngageRequestResponseReceived_7() { return static_cast<int32_t>(offsetof(SponsorPayPlugin_t4106591195, ___OnBrandEngageRequestResponseReceived_7)); }
	inline BrandEngageRequestResponseReceivedHandler_t263189403 * get_OnBrandEngageRequestResponseReceived_7() const { return ___OnBrandEngageRequestResponseReceived_7; }
	inline BrandEngageRequestResponseReceivedHandler_t263189403 ** get_address_of_OnBrandEngageRequestResponseReceived_7() { return &___OnBrandEngageRequestResponseReceived_7; }
	inline void set_OnBrandEngageRequestResponseReceived_7(BrandEngageRequestResponseReceivedHandler_t263189403 * value)
	{
		___OnBrandEngageRequestResponseReceived_7 = value;
		Il2CppCodeGenWriteBarrier(&___OnBrandEngageRequestResponseReceived_7, value);
	}

	inline static int32_t get_offset_of_OnBrandEngageRequestErrorReceived_8() { return static_cast<int32_t>(offsetof(SponsorPayPlugin_t4106591195, ___OnBrandEngageRequestErrorReceived_8)); }
	inline BrandEngageRequestErrorReceivedHandler_t816797986 * get_OnBrandEngageRequestErrorReceived_8() const { return ___OnBrandEngageRequestErrorReceived_8; }
	inline BrandEngageRequestErrorReceivedHandler_t816797986 ** get_address_of_OnBrandEngageRequestErrorReceived_8() { return &___OnBrandEngageRequestErrorReceived_8; }
	inline void set_OnBrandEngageRequestErrorReceived_8(BrandEngageRequestErrorReceivedHandler_t816797986 * value)
	{
		___OnBrandEngageRequestErrorReceived_8 = value;
		Il2CppCodeGenWriteBarrier(&___OnBrandEngageRequestErrorReceived_8, value);
	}

	inline static int32_t get_offset_of_OnBrandEngageResultReceived_9() { return static_cast<int32_t>(offsetof(SponsorPayPlugin_t4106591195, ___OnBrandEngageResultReceived_9)); }
	inline BrandEngageResultHandler_t570975071 * get_OnBrandEngageResultReceived_9() const { return ___OnBrandEngageResultReceived_9; }
	inline BrandEngageResultHandler_t570975071 ** get_address_of_OnBrandEngageResultReceived_9() { return &___OnBrandEngageResultReceived_9; }
	inline void set_OnBrandEngageResultReceived_9(BrandEngageResultHandler_t570975071 * value)
	{
		___OnBrandEngageResultReceived_9 = value;
		Il2CppCodeGenWriteBarrier(&___OnBrandEngageResultReceived_9, value);
	}

	inline static int32_t get_offset_of_OnInterstitialRequestResponseReceived_10() { return static_cast<int32_t>(offsetof(SponsorPayPlugin_t4106591195, ___OnInterstitialRequestResponseReceived_10)); }
	inline InterstitialRequestResponseReceivedHandler_t2506718783 * get_OnInterstitialRequestResponseReceived_10() const { return ___OnInterstitialRequestResponseReceived_10; }
	inline InterstitialRequestResponseReceivedHandler_t2506718783 ** get_address_of_OnInterstitialRequestResponseReceived_10() { return &___OnInterstitialRequestResponseReceived_10; }
	inline void set_OnInterstitialRequestResponseReceived_10(InterstitialRequestResponseReceivedHandler_t2506718783 * value)
	{
		___OnInterstitialRequestResponseReceived_10 = value;
		Il2CppCodeGenWriteBarrier(&___OnInterstitialRequestResponseReceived_10, value);
	}

	inline static int32_t get_offset_of_OnInterstitialRequestErrorReceived_11() { return static_cast<int32_t>(offsetof(SponsorPayPlugin_t4106591195, ___OnInterstitialRequestErrorReceived_11)); }
	inline InterstitialRequestErrorReceivedHandler_t2305716478 * get_OnInterstitialRequestErrorReceived_11() const { return ___OnInterstitialRequestErrorReceived_11; }
	inline InterstitialRequestErrorReceivedHandler_t2305716478 ** get_address_of_OnInterstitialRequestErrorReceived_11() { return &___OnInterstitialRequestErrorReceived_11; }
	inline void set_OnInterstitialRequestErrorReceived_11(InterstitialRequestErrorReceivedHandler_t2305716478 * value)
	{
		___OnInterstitialRequestErrorReceived_11 = value;
		Il2CppCodeGenWriteBarrier(&___OnInterstitialRequestErrorReceived_11, value);
	}

	inline static int32_t get_offset_of_OnInterstitialStatusCloseReceived_12() { return static_cast<int32_t>(offsetof(SponsorPayPlugin_t4106591195, ___OnInterstitialStatusCloseReceived_12)); }
	inline InterstitialStatusCloseHandler_t3952291690 * get_OnInterstitialStatusCloseReceived_12() const { return ___OnInterstitialStatusCloseReceived_12; }
	inline InterstitialStatusCloseHandler_t3952291690 ** get_address_of_OnInterstitialStatusCloseReceived_12() { return &___OnInterstitialStatusCloseReceived_12; }
	inline void set_OnInterstitialStatusCloseReceived_12(InterstitialStatusCloseHandler_t3952291690 * value)
	{
		___OnInterstitialStatusCloseReceived_12 = value;
		Il2CppCodeGenWriteBarrier(&___OnInterstitialStatusCloseReceived_12, value);
	}

	inline static int32_t get_offset_of_OnInterstitialStatusErrorReceived_13() { return static_cast<int32_t>(offsetof(SponsorPayPlugin_t4106591195, ___OnInterstitialStatusErrorReceived_13)); }
	inline InterstitialStatusErrorHandler_t1250724186 * get_OnInterstitialStatusErrorReceived_13() const { return ___OnInterstitialStatusErrorReceived_13; }
	inline InterstitialStatusErrorHandler_t1250724186 ** get_address_of_OnInterstitialStatusErrorReceived_13() { return &___OnInterstitialStatusErrorReceived_13; }
	inline void set_OnInterstitialStatusErrorReceived_13(InterstitialStatusErrorHandler_t1250724186 * value)
	{
		___OnInterstitialStatusErrorReceived_13 = value;
		Il2CppCodeGenWriteBarrier(&___OnInterstitialStatusErrorReceived_13, value);
	}

	inline static int32_t get_offset_of_OnNativeExceptionReceived_14() { return static_cast<int32_t>(offsetof(SponsorPayPlugin_t4106591195, ___OnNativeExceptionReceived_14)); }
	inline NativeExceptionHandler_t1751116300 * get_OnNativeExceptionReceived_14() const { return ___OnNativeExceptionReceived_14; }
	inline NativeExceptionHandler_t1751116300 ** get_address_of_OnNativeExceptionReceived_14() { return &___OnNativeExceptionReceived_14; }
	inline void set_OnNativeExceptionReceived_14(NativeExceptionHandler_t1751116300 * value)
	{
		___OnNativeExceptionReceived_14 = value;
		Il2CppCodeGenWriteBarrier(&___OnNativeExceptionReceived_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
