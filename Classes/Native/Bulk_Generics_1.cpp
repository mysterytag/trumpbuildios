﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// CellReactiveApi/<WhateverBind>c__AnonStoreyFE`1<System.DateTime>
struct U3CWhateverBindU3Ec__AnonStoreyFE_1_t560459497;
// CellReactiveApi/<WhateverBind>c__AnonStoreyFE`1<System.Object>
struct U3CWhateverBindU3Ec__AnonStoreyFE_1_t1058531981;
// System.Object
struct Il2CppObject;
// CellReactiveApi/<When>c__AnonStorey10A`1/<When>c__AnonStorey10B`1<System.Boolean>
struct U3CWhenU3Ec__AnonStorey10B_1_t427924399;
// CellReactiveApi/<When>c__AnonStorey10A`1/<When>c__AnonStorey10B`1<System.Object>
struct U3CWhenU3Ec__AnonStorey10B_1_t1054025478;
// CellReactiveApi/<When>c__AnonStorey10A`1<System.Boolean>
struct U3CWhenU3Ec__AnonStorey10A_1_t872949398;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action
struct Action_t437523947;
// CellReactiveApi/<When>c__AnonStorey10A`1<System.Object>
struct U3CWhenU3Ec__AnonStorey10A_1_t1499050477;
// CellReactiveApi/<WhenOnce>c__AnonStorey107`1/<WhenOnce>c__AnonStorey108`1<System.Object>
struct U3CWhenOnceU3Ec__AnonStorey108_1_t2286074227;
// CellReactiveApi/<WhenOnce>c__AnonStorey107`1/<WhenOnce>c__AnonStorey109`1<System.Object>
struct U3CWhenOnceU3Ec__AnonStorey109_1_t1841049228;
// CellReactiveApi/<WhenOnce>c__AnonStorey107`1<System.Object>
struct U3CWhenOnceU3Ec__AnonStorey107_1_t2731099226;
// CellReactiveApi/<WhenSatisfy>c__AnonStorey10C`1/<WhenSatisfy>c__AnonStorey10D`1<System.Object>
struct U3CWhenSatisfyU3Ec__AnonStorey10D_1_t3834446745;
// CellReactiveApi/<WhenSatisfy>c__AnonStorey10C`1<System.Object>
struct U3CWhenSatisfyU3Ec__AnonStorey10C_1_t4279471744;
// CellReactiveApi/Carrier`1<System.Object>
struct Carrier_1_t2723215674;
// CellReactiveApi/DisposableContainer`1<System.Boolean>
struct DisposableContainer_1_t2879821764;
// CellReactiveApi/DisposableContainer`1<System.Object>
struct DisposableContainer_1_t3505922843;
// CellUtils.CellJoinDisposable`1<System.Double>
struct CellJoinDisposable_1_t3433903597;
// CellUtils.CellJoinDisposable`1<System.Int32>
struct CellJoinDisposable_1_t1451834474;
// CellUtils.CellJoinDisposable`1<System.Object>
struct CellJoinDisposable_1_t3736493403;
// CollectionMoveEvent`1<System.Object>
struct CollectionMoveEvent_1_t3572055381;
// System.String
struct String_t;
// CollectionRemoveEvent`1<System.Object>
struct CollectionRemoveEvent_1_t898259258;
// CollectionReplaceEvent`1<System.Object>
struct CollectionReplaceEvent_1_t2497372070;
// DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>
struct DOGetter_1_t1740296090;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;
// DG.Tweening.Core.DOGetter`1<System.Double>
struct DOGetter_1_t3523644891;
// DG.Tweening.Core.DOGetter`1<System.Int32>
struct DOGetter_1_t1541575768;
// DG.Tweening.Core.DOGetter`1<System.Int64>
struct DOGetter_1_t1541575863;
// DG.Tweening.Core.DOGetter`1<System.Object>
struct DOGetter_1_t3826234697;
// DG.Tweening.Core.DOGetter`1<System.Single>
struct DOGetter_1_t3947337298;
// DG.Tweening.Core.DOGetter`1<System.UInt32>
struct DOGetter_1_t3975053603;
// DG.Tweening.Core.DOGetter`1<System.UInt64>
struct DOGetter_1_t3975053698;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Color>
struct DOGetter_1_t282336741;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>
struct DOGetter_1_t585876960;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>
struct DOGetter_1_t219589798;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>
struct DOGetter_1_t2219490769;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>
struct DOGetter_1_t2219490770;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>
struct DOGetter_1_t2219490771;
// DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>
struct DOSetter_1_t2776935918;
// DG.Tweening.Core.DOSetter`1<System.Double>
struct DOSetter_1_t265317423;
// DG.Tweening.Core.DOSetter`1<System.Int32>
struct DOSetter_1_t2578215596;
// DG.Tweening.Core.DOSetter`1<System.Int64>
struct DOSetter_1_t2578215691;
// DG.Tweening.Core.DOSetter`1<System.Object>
struct DOSetter_1_t567907229;
// DG.Tweening.Core.DOSetter`1<System.Single>
struct DOSetter_1_t689009830;
// DG.Tweening.Core.DOSetter`1<System.UInt32>
struct DOSetter_1_t716726135;
// DG.Tweening.Core.DOSetter`1<System.UInt64>
struct DOSetter_1_t716726230;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Color>
struct DOSetter_1_t1318976569;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>
struct DOSetter_1_t1622516788;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>
struct DOSetter_1_t1256229626;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>
struct DOSetter_1_t3256130597;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>
struct DOSetter_1_t3256130598;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>
struct DOSetter_1_t3256130599;
// DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>
struct TweenerCore_3_t182370955;
// DG.Tweening.Tweener
struct Tweener_t1766303790;
// DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>
struct TweenerCore_3_t1963399521;
// DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>
struct TweenerCore_3_t2554842309;
// DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>
struct TweenerCore_3_t908793361;
// DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>
struct TweenerCore_3_t1619290041;
// DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>
struct TweenerCore_3_t4210682729;
// DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>
struct TweenerCore_3_t2841494226;
// DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>
struct TweenerCore_3_t4224275457;
// DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>
struct TweenerCore_3_t2578226509;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>
struct TweenerCore_3_t4289140679;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>
struct TweenerCore_3_t1968778341;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>
struct TweenerCore_3_t3741400174;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>
struct TweenerCore_3_t2274666266;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>
struct TweenerCore_3_t753146263;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>
struct TweenerCore_3_t1820784502;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>
struct TweenerCore_3_t1616349746;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>
struct TweenerCore_3_t2905908171;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>
struct TweenerCore_3_t763702783;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>
struct ABSTweenPlugin_3_t1075249930;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>
struct ABSTweenPlugin_3_t2856278496;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>
struct ABSTweenPlugin_3_t3447721284;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>
struct ABSTweenPlugin_3_t1801672336;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>
struct ABSTweenPlugin_3_t2512169016;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>
struct ABSTweenPlugin_3_t808594408;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>
struct ABSTweenPlugin_3_t3734373201;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>
struct ABSTweenPlugin_3_t822187136;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>
struct ABSTweenPlugin_3_t3471105484;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>
struct ABSTweenPlugin_3_t887052358;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>
struct ABSTweenPlugin_3_t2861657316;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>
struct ABSTweenPlugin_3_t339311853;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>
struct ABSTweenPlugin_3_t3167545241;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>
struct ABSTweenPlugin_3_t1646025238;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>
struct ABSTweenPlugin_3_t2713663477;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>
struct ABSTweenPlugin_3_t2509228721;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>
struct ABSTweenPlugin_3_t3798787146;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>
struct ABSTweenPlugin_3_t1656581758;
// DG.Tweening.TweenCallback`1<System.Int32>
struct TweenCallback_1_t1868435431;
// DG.Tweening.TweenCallback`1<System.Object>
struct TweenCallback_1_t4153094360;
// DG.Tweening.TweenCallback`1<System.Single>
struct TweenCallback_1_t4274196961;
// Facebook.Unity.Canvas.CanvasFacebook/CanvasUIMethodCall`1<System.Object>
struct CanvasUIMethodCall_1_t4051398709;
// Facebook.Unity.Canvas.CanvasFacebook
struct CanvasFacebook_t943264545;
// Facebook.Unity.MethodArguments
struct MethodArguments_t3878806324;
// Facebook.Unity.FacebookDelegate`1<System.Object>
struct FacebookDelegate_1_t1473468476;
// Facebook.Unity.MethodCall`1<System.Object>
struct MethodCall_1_t2121494416;
// Facebook.Unity.FacebookBase
struct FacebookBase_t2319813814;
// Facebook.Unity.Mobile.Android.AndroidFacebook/JavaMethodCall`1<System.Object>
struct JavaMethodCall_1_t3793613819;
// Facebook.Unity.Mobile.Android.AndroidFacebook
struct AndroidFacebook_t1604313921;
// Facebook.Unity.Utilities/Callback`1<System.Object>
struct Callback_1_t2504244479;
// FlurryAnalytics.MonoSingleton`1<System.Object>
struct MonoSingleton_1_t858865150;
// GenericDataSource`2<System.Object,System.Object>
struct GenericDataSource_2_t1679676449;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// UnityEngine.RectTransform
struct RectTransform_t3317474837;
// Tacticsoft.TableViewCell
struct TableViewCell_t776419755;
// Tacticsoft.TableView
struct TableView_t692333993;
// GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator4`1<System.Object>
struct U3CToEnumerableU3Ec__Iterator4_1_t4039804212;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2320212868;
// GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutMethod`1<System.Object>
struct OutMethod_1_t3356189497;
// System.Object[]
struct ObjectU5BU5D_t11523773;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CWhateverBindU3560459497.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CWhateverBindU3560459497MethodDeclarations.h"
#include "mscorlib_System_Void2779279689.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "mscorlib_System_DateTime339033936.h"
#include "AssemblyU2DCSharp_ConnectionCollector444796719MethodDeclarations.h"
#include "AssemblyU2DCSharp_ConnectionCollector444796719.h"
#include "System_Core_System_Action_2_gen1818238909.h"
#include "System_Core_System_Action_2_gen1818238909MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CWhateverBindU1058531981.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CWhateverBindU1058531981MethodDeclarations.h"
#include "mscorlib_System_Object837106420.h"
#include "System_Core_System_Action_2_gen3713150217.h"
#include "System_Core_System_Action_2_gen3713150217MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CWhenU3Ec__Anon427924399.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CWhenU3Ec__Anon427924399MethodDeclarations.h"
#include "mscorlib_System_Boolean211005341.h"
#include "System_Core_System_Action437523947MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CWhenU3Ec__Anon872949398.h"
#include "System_Core_System_Func_2_gen1008118516.h"
#include "System_Core_System_Func_2_gen1008118516MethodDeclarations.h"
#include "System_Core_System_Action437523947.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CWhenU3Ec__Ano1054025478.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CWhenU3Ec__Ano1054025478MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CWhenU3Ec__Ano1499050477.h"
#include "System_Core_System_Func_2_gen1509682273.h"
#include "System_Core_System_Func_2_gen1509682273MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CWhenU3Ec__Anon872949398MethodDeclarations.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"
#include "mscorlib_System_Action_1_gen359458046.h"
#include "mscorlib_System_Action_1_gen359458046MethodDeclarations.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CWhenU3Ec__Ano1499050477MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen985559125.h"
#include "mscorlib_System_Action_1_gen985559125MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CWhenOnceU3Ec_2286074227.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CWhenOnceU3Ec_2286074227MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CWhenOnceU3Ec_1841049228.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CWhenOnceU3Ec_1841049228MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CWhenOnceU3Ec_2731099226.h"
#include "AssemblyU2DCSharp_CellUtils_SingleAssignmentDispos1832432170.h"
#include "AssemblyU2DCSharp_CellUtils_SingleAssignmentDispos1832432170MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CWhenOnceU3Ec_2731099226MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellUtils_EmptyDisposable1512564738MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellUtils_EmptyDisposable1512564738.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CWhenSatisfyU33834446745.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CWhenSatisfyU33834446745MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CWhenSatisfyU34279471744.h"
#include "AssemblyU2DCSharp_CellReactiveApi_DisposableContai2879821764.h"
#include "AssemblyU2DCSharp_CellReactiveApi_U3CWhenSatisfyU34279471744MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_DisposableContai2879821764MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_Carrier_1_gen2723215674.h"
#include "AssemblyU2DCSharp_CellReactiveApi_Carrier_1_gen2723215674MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellReactiveApi_DisposableContai3505922843.h"
#include "AssemblyU2DCSharp_CellReactiveApi_DisposableContai3505922843MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellUtils_CellJoinDisposable_1_g3433903597.h"
#include "AssemblyU2DCSharp_CellUtils_CellJoinDisposable_1_g3433903597MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellUtils_ListDisposable2393830995MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellUtils_CellJoinDisposable_1_g1451834474.h"
#include "AssemblyU2DCSharp_CellUtils_CellJoinDisposable_1_g1451834474MethodDeclarations.h"
#include "AssemblyU2DCSharp_CellUtils_CellJoinDisposable_1_g3736493403.h"
#include "AssemblyU2DCSharp_CellUtils_CellJoinDisposable_1_g3736493403MethodDeclarations.h"
#include "AssemblyU2DCSharp_CollectionMoveEvent_1_gen3572055381.h"
#include "AssemblyU2DCSharp_CollectionMoveEvent_1_gen3572055381MethodDeclarations.h"
#include "mscorlib_System_Int322847414787.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_String968488902MethodDeclarations.h"
#include "AssemblyU2DCSharp_CollectionRemoveEvent_1_gen898259258.h"
#include "AssemblyU2DCSharp_CollectionRemoveEvent_1_gen898259258MethodDeclarations.h"
#include "AssemblyU2DCSharp_CollectionReplaceEvent_1_gen2497372070.h"
#include "AssemblyU2DCSharp_CollectionReplaceEvent_1_gen2497372070MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen1740296090.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen1740296090MethodDeclarations.h"
#include "DOTween_DG_Tweening_Color23046135109.h"
#include "mscorlib_System_AsyncCallback1363551830.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen3523644891.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen3523644891MethodDeclarations.h"
#include "mscorlib_System_Double534516614.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen1541575768.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen1541575768MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen1541575863.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen1541575863MethodDeclarations.h"
#include "mscorlib_System_Int642847414882.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen3826234697.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen3826234697MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen3947337298.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen3947337298MethodDeclarations.h"
#include "mscorlib_System_Single958209021.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen3975053603.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen3975053603MethodDeclarations.h"
#include "mscorlib_System_UInt32985925326.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen3975053698.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen3975053698MethodDeclarations.h"
#include "mscorlib_System_UInt64985925421.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen282336741.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen282336741MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen585876960.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen585876960MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen219589798.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen219589798MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect1525428817.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen2219490769.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen2219490769MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen2219490770.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen2219490770MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen2219490771.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen2219490771MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector43525329790.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen2776935918.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen2776935918MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen265317423.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen265317423MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen2578215596.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen2578215596MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen2578215691.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen2578215691MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen567907229.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen567907229MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen689009830.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen689009830MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen716726135.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen716726135MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen716726230.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen716726230MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen1318976569.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen1318976569MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen1622516788.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen1622516788MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen1256229626.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen1256229626MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3256130597.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3256130597MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3256130598.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3256130598MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3256130599.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3256130599MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen182370955.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen182370955MethodDeclarations.h"
#include "DOTween_DG_Tweening_Tweener1766303790MethodDeclarations.h"
#include "mscorlib_System_Type2779229935MethodDeclarations.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_System_RuntimeTypeHandle1864875887.h"
#include "DOTween_DG_Tweening_Tween1103364673.h"
#include "DOTween_DG_Tweening_Core_ABSSequentiable2598715689.h"
#include "DOTween_DG_Tweening_TweenType3195136027.h"
#include "DOTween_DG_Tweening_Tween1103364673MethodDeclarations.h"
#include "DOTween_DG_Tweening_Tweener1766303790.h"
#include "DOTween_DG_Tweening_Core_Debugger131542730MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_Debugger131542730.h"
#include "mscorlib_ArrayTypes.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_1075249930.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_1075249930MethodDeclarations.h"
#include "mscorlib_System_Activator690001546MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Options_ColorOptions4161000167.h"
#include "mscorlib_System_Activator690001546.h"
#include "DOTween_DG_Tweening_Core_Enums_UpdateMode3005523297.h"
#include "DOTween_DG_Tweening_Core_Enums_UpdateNotice384566358.h"
#include "DOTween_DG_Tweening_DOTween3585775766.h"
#include "DOTween_DG_Tweening_DOTween3585775766MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen1963399521.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen1963399521MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_2856278496.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_2856278496MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Options_NoOptions32144009.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen2554842309.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen2554842309MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_3447721284.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_3447721284MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen908793361.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen908793361MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_1801672336.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_1801672336MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen1619290041.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen1619290041MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_2512169016.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_2512169016MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen4210682729.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen4210682729MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_g808594408.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_g808594408MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Options_StringOptions2623536697.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen2841494226.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen2841494226MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_3734373201.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_3734373201MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Options_FloatOptions3779806670.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen4224275457.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen4224275457MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_g822187136.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_g822187136MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen2578226509.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen2578226509MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_3471105484.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_3471105484MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen4289140679.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen4289140679MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_g887052358.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_g887052358MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen1968778341.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen1968778341MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_2861657316.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_2861657316MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen3741400174.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen3741400174MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_g339311853.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_g339311853MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Options_QuaternionOpti1813596812.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen2274666266.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen2274666266MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_3167545241.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_3167545241MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Options_RectOptions1160272134.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen753146263.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen753146263MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_1646025238.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_1646025238MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Options_VectorOptions3308462279.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen1820784502.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen1820784502MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_2713663477.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_2713663477MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Options_PathOptions1144419237.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen1616349746.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen1616349746MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_2509228721.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_2509228721MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Options_Vector3ArrayOpt939984481.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen2905908171.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen2905908171MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_3798787146.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_3798787146MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen763702783.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen763702783MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_1656581758.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_1656581758MethodDeclarations.h"
#include "DOTween_DG_Tweening_TweenCallback_1_gen1868435431.h"
#include "DOTween_DG_Tweening_TweenCallback_1_gen1868435431MethodDeclarations.h"
#include "DOTween_DG_Tweening_TweenCallback_1_gen4153094360.h"
#include "DOTween_DG_Tweening_TweenCallback_1_gen4153094360MethodDeclarations.h"
#include "DOTween_DG_Tweening_TweenCallback_1_gen4274196961.h"
#include "DOTween_DG_Tweening_TweenCallback_1_gen4274196961MethodDeclarations.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Canvas_CanvasFace4051398709.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Canvas_CanvasFace4051398709MethodDeclarations.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Canvas_CanvasFaceb943264545.h"
#include "AssemblyU2DCSharp_Facebook_Unity_MethodCall_1_gen2121494416MethodDeclarations.h"
#include "AssemblyU2DCSharp_Facebook_Unity_FacebookBase2319813814.h"
#include "AssemblyU2DCSharp_Facebook_Unity_MethodArguments3878806324.h"
#include "AssemblyU2DCSharp_Facebook_Unity_FacebookDelegate_1473468476.h"
#include "AssemblyU2DCSharp_Facebook_Unity_MethodArguments3878806324MethodDeclarations.h"
#include "AssemblyU2DCSharp_Facebook_Unity_FacebookBase2319813814MethodDeclarations.h"
#include "AssemblyU2DCSharp_Facebook_Unity_CallbackManager1943358823.h"
#include "AssemblyU2DCSharp_Facebook_Unity_CallbackManager1943358823MethodDeclarations.h"
#include "AssemblyU2DCSharp_Facebook_Unity_FacebookDelegate_1473468476MethodDeclarations.h"
#include "AssemblyU2DCSharp_Facebook_Unity_MethodCall_1_gen2121494416.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Mobile_Android_An3793613819.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Mobile_Android_An3793613819MethodDeclarations.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Mobile_Android_An1604313921.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Mobile_Android_An1604313921MethodDeclarations.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Utilities_Callbac2504244479.h"
#include "AssemblyU2DCSharp_Facebook_Unity_Utilities_Callbac2504244479MethodDeclarations.h"
#include "AssemblyU2DCSharp_FlurryAnalytics_MonoSingleton_1_g858865150.h"
#include "AssemblyU2DCSharp_FlurryAnalytics_MonoSingleton_1_g858865150MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour3012272455MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3878351788MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject4012695102MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"
#include "UnityEngine_UnityEngine_Object3878351788.h"
#include "mscorlib_System_Reflection_MemberInfo2843033814MethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfo2843033814.h"
#include "UnityEngine_UnityEngine_Component2126946602.h"
#include "AssemblyU2DCSharp_GenericDataSource_2_gen1679676449.h"
#include "AssemblyU2DCSharp_GenericDataSource_2_gen1679676449MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform3317474837MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect1525428817MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform3317474837.h"
#include "AssemblyU2DCSharp_Tacticsoft_TableViewCell776419755.h"
#include "AssemblyU2DCSharp_Tacticsoft_TableViewCell776419755MethodDeclarations.h"
#include "AssemblyU2DCSharp_Tacticsoft_TableView692333993.h"
#include "AssemblyU2DCSharp_Tacticsoft_TableView692333993MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component2126946602MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug1588791936MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449MethodDeclarations.h"
#include "System_Core_System_Action_2_gen4105459918.h"
#include "System_Core_System_Action_2_gen4105459918MethodDeclarations.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_P4039804212.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_P4039804212MethodDeclarations.h"
#include "mscorlib_System_Threading_Interlocked3007803305MethodDeclarations.h"
#include "mscorlib_System_UIntPtr3100060873.h"
#include "System_Core_System_Func_2_gen1403134703.h"
#include "mscorlib_System_UIntPtr3100060873MethodDeclarations.h"
#include "System_Core_System_Func_2_gen1403134703MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1374155497MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1374155497.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_P3356189497.h"
#include "AssemblyU2DCSharp_GooglePlayGames_Native_PInvoke_P3356189497MethodDeclarations.h"

// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeStartValue<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeStartValue_TisColor2_t3046135109_TisColor2_t3046135109_TisColorOptions_t4161000167_m2503113380_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t182370955 * ___t0, Color2_t3046135109  ___newStartValue1, float ___newDuration2, const MethodInfo* method);
#define Tweener_DoChangeStartValue_TisColor2_t3046135109_TisColor2_t3046135109_TisColorOptions_t4161000167_m2503113380(__this /* static, unused */, ___t0, ___newStartValue1, ___newDuration2, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t182370955 *, Color2_t3046135109 , float, const MethodInfo*))Tweener_DoChangeStartValue_TisColor2_t3046135109_TisColor2_t3046135109_TisColorOptions_t4161000167_m2503113380_gshared)(__this /* static, unused */, ___t0, ___newStartValue1, ___newDuration2, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeEndValue<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single,System.Boolean)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeEndValue_TisColor2_t3046135109_TisColor2_t3046135109_TisColorOptions_t4161000167_m3377251968_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t182370955 * ___t0, Color2_t3046135109  ___newEndValue1, float ___newDuration2, bool ___snapStartValue3, const MethodInfo* method);
#define Tweener_DoChangeEndValue_TisColor2_t3046135109_TisColor2_t3046135109_TisColorOptions_t4161000167_m3377251968(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t182370955 *, Color2_t3046135109 , float, bool, const MethodInfo*))Tweener_DoChangeEndValue_TisColor2_t3046135109_TisColor2_t3046135109_TisColorOptions_t4161000167_m3377251968_gshared)(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeValues<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,T2,System.Single)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeValues_TisColor2_t3046135109_TisColor2_t3046135109_TisColorOptions_t4161000167_m3247867489_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t182370955 * ___t0, Color2_t3046135109  ___newStartValue1, Color2_t3046135109  ___newEndValue2, float ___newDuration3, const MethodInfo* method);
#define Tweener_DoChangeValues_TisColor2_t3046135109_TisColor2_t3046135109_TisColorOptions_t4161000167_m3247867489(__this /* static, unused */, ___t0, ___newStartValue1, ___newEndValue2, ___newDuration3, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t182370955 *, Color2_t3046135109 , Color2_t3046135109 , float, const MethodInfo*))Tweener_DoChangeValues_TisColor2_t3046135109_TisColor2_t3046135109_TisColorOptions_t4161000167_m3247867489_gshared)(__this /* static, unused */, ___t0, ___newStartValue1, ___newEndValue2, ___newDuration3, method)
// !!0 System.Activator::CreateInstance<DG.Tweening.Plugins.Options.ColorOptions>()
extern "C"  ColorOptions_t4161000167  Activator_CreateInstance_TisColorOptions_t4161000167_m2427166485_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Activator_CreateInstance_TisColorOptions_t4161000167_m2427166485(__this /* static, unused */, method) ((  ColorOptions_t4161000167  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Activator_CreateInstance_TisColorOptions_t4161000167_m2427166485_gshared)(__this /* static, unused */, method)
// System.Single DG.Tweening.Tweener::DoUpdateDelay<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
extern "C"  float Tweener_DoUpdateDelay_TisColor2_t3046135109_TisColor2_t3046135109_TisColorOptions_t4161000167_m1193809274_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t182370955 * ___t0, float ___elapsed1, const MethodInfo* method);
#define Tweener_DoUpdateDelay_TisColor2_t3046135109_TisColor2_t3046135109_TisColorOptions_t4161000167_m1193809274(__this /* static, unused */, ___t0, ___elapsed1, method) ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t182370955 *, float, const MethodInfo*))Tweener_DoUpdateDelay_TisColor2_t3046135109_TisColor2_t3046135109_TisColorOptions_t4161000167_m1193809274_gshared)(__this /* static, unused */, ___t0, ___elapsed1, method)
// System.Boolean DG.Tweening.Tweener::DoStartup<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern "C"  bool Tweener_DoStartup_TisColor2_t3046135109_TisColor2_t3046135109_TisColorOptions_t4161000167_m286928356_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t182370955 * ___t0, const MethodInfo* method);
#define Tweener_DoStartup_TisColor2_t3046135109_TisColor2_t3046135109_TisColorOptions_t4161000167_m286928356(__this /* static, unused */, ___t0, method) ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t182370955 *, const MethodInfo*))Tweener_DoStartup_TisColor2_t3046135109_TisColor2_t3046135109_TisColorOptions_t4161000167_m286928356_gshared)(__this /* static, unused */, ___t0, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeStartValue<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeStartValue_TisDouble_t534516614_TisDouble_t534516614_TisNoOptions_t32144009_m702533556_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t1963399521 * ___t0, double ___newStartValue1, float ___newDuration2, const MethodInfo* method);
#define Tweener_DoChangeStartValue_TisDouble_t534516614_TisDouble_t534516614_TisNoOptions_t32144009_m702533556(__this /* static, unused */, ___t0, ___newStartValue1, ___newDuration2, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1963399521 *, double, float, const MethodInfo*))Tweener_DoChangeStartValue_TisDouble_t534516614_TisDouble_t534516614_TisNoOptions_t32144009_m702533556_gshared)(__this /* static, unused */, ___t0, ___newStartValue1, ___newDuration2, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeEndValue<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single,System.Boolean)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeEndValue_TisDouble_t534516614_TisDouble_t534516614_TisNoOptions_t32144009_m1774200770_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t1963399521 * ___t0, double ___newEndValue1, float ___newDuration2, bool ___snapStartValue3, const MethodInfo* method);
#define Tweener_DoChangeEndValue_TisDouble_t534516614_TisDouble_t534516614_TisNoOptions_t32144009_m1774200770(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1963399521 *, double, float, bool, const MethodInfo*))Tweener_DoChangeEndValue_TisDouble_t534516614_TisDouble_t534516614_TisNoOptions_t32144009_m1774200770_gshared)(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeValues<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,T2,System.Single)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeValues_TisDouble_t534516614_TisDouble_t534516614_TisNoOptions_t32144009_m1243007627_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t1963399521 * ___t0, double ___newStartValue1, double ___newEndValue2, float ___newDuration3, const MethodInfo* method);
#define Tweener_DoChangeValues_TisDouble_t534516614_TisDouble_t534516614_TisNoOptions_t32144009_m1243007627(__this /* static, unused */, ___t0, ___newStartValue1, ___newEndValue2, ___newDuration3, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1963399521 *, double, double, float, const MethodInfo*))Tweener_DoChangeValues_TisDouble_t534516614_TisDouble_t534516614_TisNoOptions_t32144009_m1243007627_gshared)(__this /* static, unused */, ___t0, ___newStartValue1, ___newEndValue2, ___newDuration3, method)
// !!0 System.Activator::CreateInstance<DG.Tweening.Plugins.Options.NoOptions>()
extern "C"  NoOptions_t32144009  Activator_CreateInstance_TisNoOptions_t32144009_m3306699697_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Activator_CreateInstance_TisNoOptions_t32144009_m3306699697(__this /* static, unused */, method) ((  NoOptions_t32144009  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Activator_CreateInstance_TisNoOptions_t32144009_m3306699697_gshared)(__this /* static, unused */, method)
// System.Single DG.Tweening.Tweener::DoUpdateDelay<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
extern "C"  float Tweener_DoUpdateDelay_TisDouble_t534516614_TisDouble_t534516614_TisNoOptions_t32144009_m4277840466_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t1963399521 * ___t0, float ___elapsed1, const MethodInfo* method);
#define Tweener_DoUpdateDelay_TisDouble_t534516614_TisDouble_t534516614_TisNoOptions_t32144009_m4277840466(__this /* static, unused */, ___t0, ___elapsed1, method) ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1963399521 *, float, const MethodInfo*))Tweener_DoUpdateDelay_TisDouble_t534516614_TisDouble_t534516614_TisNoOptions_t32144009_m4277840466_gshared)(__this /* static, unused */, ___t0, ___elapsed1, method)
// System.Boolean DG.Tweening.Tweener::DoStartup<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern "C"  bool Tweener_DoStartup_TisDouble_t534516614_TisDouble_t534516614_TisNoOptions_t32144009_m2961918782_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t1963399521 * ___t0, const MethodInfo* method);
#define Tweener_DoStartup_TisDouble_t534516614_TisDouble_t534516614_TisNoOptions_t32144009_m2961918782(__this /* static, unused */, ___t0, method) ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1963399521 *, const MethodInfo*))Tweener_DoStartup_TisDouble_t534516614_TisDouble_t534516614_TisNoOptions_t32144009_m2961918782_gshared)(__this /* static, unused */, ___t0, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeStartValue<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeStartValue_TisInt32_t2847414787_TisInt32_t2847414787_TisNoOptions_t32144009_m1961167180_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t2554842309 * ___t0, int32_t ___newStartValue1, float ___newDuration2, const MethodInfo* method);
#define Tweener_DoChangeStartValue_TisInt32_t2847414787_TisInt32_t2847414787_TisNoOptions_t32144009_m1961167180(__this /* static, unused */, ___t0, ___newStartValue1, ___newDuration2, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2554842309 *, int32_t, float, const MethodInfo*))Tweener_DoChangeStartValue_TisInt32_t2847414787_TisInt32_t2847414787_TisNoOptions_t32144009_m1961167180_gshared)(__this /* static, unused */, ___t0, ___newStartValue1, ___newDuration2, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeEndValue<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single,System.Boolean)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeEndValue_TisInt32_t2847414787_TisInt32_t2847414787_TisNoOptions_t32144009_m1608373610_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t2554842309 * ___t0, int32_t ___newEndValue1, float ___newDuration2, bool ___snapStartValue3, const MethodInfo* method);
#define Tweener_DoChangeEndValue_TisInt32_t2847414787_TisInt32_t2847414787_TisNoOptions_t32144009_m1608373610(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2554842309 *, int32_t, float, bool, const MethodInfo*))Tweener_DoChangeEndValue_TisInt32_t2847414787_TisInt32_t2847414787_TisNoOptions_t32144009_m1608373610_gshared)(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeValues<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,T2,System.Single)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeValues_TisInt32_t2847414787_TisInt32_t2847414787_TisNoOptions_t32144009_m1743302963_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t2554842309 * ___t0, int32_t ___newStartValue1, int32_t ___newEndValue2, float ___newDuration3, const MethodInfo* method);
#define Tweener_DoChangeValues_TisInt32_t2847414787_TisInt32_t2847414787_TisNoOptions_t32144009_m1743302963(__this /* static, unused */, ___t0, ___newStartValue1, ___newEndValue2, ___newDuration3, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2554842309 *, int32_t, int32_t, float, const MethodInfo*))Tweener_DoChangeValues_TisInt32_t2847414787_TisInt32_t2847414787_TisNoOptions_t32144009_m1743302963_gshared)(__this /* static, unused */, ___t0, ___newStartValue1, ___newEndValue2, ___newDuration3, method)
// System.Single DG.Tweening.Tweener::DoUpdateDelay<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
extern "C"  float Tweener_DoUpdateDelay_TisInt32_t2847414787_TisInt32_t2847414787_TisNoOptions_t32144009_m3893662906_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t2554842309 * ___t0, float ___elapsed1, const MethodInfo* method);
#define Tweener_DoUpdateDelay_TisInt32_t2847414787_TisInt32_t2847414787_TisNoOptions_t32144009_m3893662906(__this /* static, unused */, ___t0, ___elapsed1, method) ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2554842309 *, float, const MethodInfo*))Tweener_DoUpdateDelay_TisInt32_t2847414787_TisInt32_t2847414787_TisNoOptions_t32144009_m3893662906_gshared)(__this /* static, unused */, ___t0, ___elapsed1, method)
// System.Boolean DG.Tweening.Tweener::DoStartup<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern "C"  bool Tweener_DoStartup_TisInt32_t2847414787_TisInt32_t2847414787_TisNoOptions_t32144009_m575085542_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t2554842309 * ___t0, const MethodInfo* method);
#define Tweener_DoStartup_TisInt32_t2847414787_TisInt32_t2847414787_TisNoOptions_t32144009_m575085542(__this /* static, unused */, ___t0, method) ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2554842309 *, const MethodInfo*))Tweener_DoStartup_TisInt32_t2847414787_TisInt32_t2847414787_TisNoOptions_t32144009_m575085542_gshared)(__this /* static, unused */, ___t0, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeStartValue<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeStartValue_TisInt64_t2847414882_TisInt64_t2847414882_TisNoOptions_t32144009_m3410348972_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t908793361 * ___t0, int64_t ___newStartValue1, float ___newDuration2, const MethodInfo* method);
#define Tweener_DoChangeStartValue_TisInt64_t2847414882_TisInt64_t2847414882_TisNoOptions_t32144009_m3410348972(__this /* static, unused */, ___t0, ___newStartValue1, ___newDuration2, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t908793361 *, int64_t, float, const MethodInfo*))Tweener_DoChangeStartValue_TisInt64_t2847414882_TisInt64_t2847414882_TisNoOptions_t32144009_m3410348972_gshared)(__this /* static, unused */, ___t0, ___newStartValue1, ___newDuration2, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeEndValue<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single,System.Boolean)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeEndValue_TisInt64_t2847414882_TisInt64_t2847414882_TisNoOptions_t32144009_m2656306954_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t908793361 * ___t0, int64_t ___newEndValue1, float ___newDuration2, bool ___snapStartValue3, const MethodInfo* method);
#define Tweener_DoChangeEndValue_TisInt64_t2847414882_TisInt64_t2847414882_TisNoOptions_t32144009_m2656306954(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t908793361 *, int64_t, float, bool, const MethodInfo*))Tweener_DoChangeEndValue_TisInt64_t2847414882_TisInt64_t2847414882_TisNoOptions_t32144009_m2656306954_gshared)(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeValues<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,T2,System.Single)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeValues_TisInt64_t2847414882_TisInt64_t2847414882_TisNoOptions_t32144009_m1306809043_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t908793361 * ___t0, int64_t ___newStartValue1, int64_t ___newEndValue2, float ___newDuration3, const MethodInfo* method);
#define Tweener_DoChangeValues_TisInt64_t2847414882_TisInt64_t2847414882_TisNoOptions_t32144009_m1306809043(__this /* static, unused */, ___t0, ___newStartValue1, ___newEndValue2, ___newDuration3, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t908793361 *, int64_t, int64_t, float, const MethodInfo*))Tweener_DoChangeValues_TisInt64_t2847414882_TisInt64_t2847414882_TisNoOptions_t32144009_m1306809043_gshared)(__this /* static, unused */, ___t0, ___newStartValue1, ___newEndValue2, ___newDuration3, method)
// System.Single DG.Tweening.Tweener::DoUpdateDelay<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
extern "C"  float Tweener_DoUpdateDelay_TisInt64_t2847414882_TisInt64_t2847414882_TisNoOptions_t32144009_m1431432794_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t908793361 * ___t0, float ___elapsed1, const MethodInfo* method);
#define Tweener_DoUpdateDelay_TisInt64_t2847414882_TisInt64_t2847414882_TisNoOptions_t32144009_m1431432794(__this /* static, unused */, ___t0, ___elapsed1, method) ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t908793361 *, float, const MethodInfo*))Tweener_DoUpdateDelay_TisInt64_t2847414882_TisInt64_t2847414882_TisNoOptions_t32144009_m1431432794_gshared)(__this /* static, unused */, ___t0, ___elapsed1, method)
// System.Boolean DG.Tweening.Tweener::DoStartup<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern "C"  bool Tweener_DoStartup_TisInt64_t2847414882_TisInt64_t2847414882_TisNoOptions_t32144009_m552324486_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t908793361 * ___t0, const MethodInfo* method);
#define Tweener_DoStartup_TisInt64_t2847414882_TisInt64_t2847414882_TisNoOptions_t32144009_m552324486(__this /* static, unused */, ___t0, method) ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t908793361 *, const MethodInfo*))Tweener_DoStartup_TisInt64_t2847414882_TisInt64_t2847414882_TisNoOptions_t32144009_m552324486_gshared)(__this /* static, unused */, ___t0, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeStartValue<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeStartValue_TisIl2CppObject_TisIl2CppObject_TisNoOptions_t32144009_m2485488656_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t1619290041 * ___t0, Il2CppObject * ___newStartValue1, float ___newDuration2, const MethodInfo* method);
#define Tweener_DoChangeStartValue_TisIl2CppObject_TisIl2CppObject_TisNoOptions_t32144009_m2485488656(__this /* static, unused */, ___t0, ___newStartValue1, ___newDuration2, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1619290041 *, Il2CppObject *, float, const MethodInfo*))Tweener_DoChangeStartValue_TisIl2CppObject_TisIl2CppObject_TisNoOptions_t32144009_m2485488656_gshared)(__this /* static, unused */, ___t0, ___newStartValue1, ___newDuration2, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeEndValue<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single,System.Boolean)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeEndValue_TisIl2CppObject_TisIl2CppObject_TisNoOptions_t32144009_m3157698534_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t1619290041 * ___t0, Il2CppObject * ___newEndValue1, float ___newDuration2, bool ___snapStartValue3, const MethodInfo* method);
#define Tweener_DoChangeEndValue_TisIl2CppObject_TisIl2CppObject_TisNoOptions_t32144009_m3157698534(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1619290041 *, Il2CppObject *, float, bool, const MethodInfo*))Tweener_DoChangeEndValue_TisIl2CppObject_TisIl2CppObject_TisNoOptions_t32144009_m3157698534_gshared)(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeValues<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,T2,System.Single)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeValues_TisIl2CppObject_TisIl2CppObject_TisNoOptions_t32144009_m1397842095_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t1619290041 * ___t0, Il2CppObject * ___newStartValue1, Il2CppObject * ___newEndValue2, float ___newDuration3, const MethodInfo* method);
#define Tweener_DoChangeValues_TisIl2CppObject_TisIl2CppObject_TisNoOptions_t32144009_m1397842095(__this /* static, unused */, ___t0, ___newStartValue1, ___newEndValue2, ___newDuration3, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1619290041 *, Il2CppObject *, Il2CppObject *, float, const MethodInfo*))Tweener_DoChangeValues_TisIl2CppObject_TisIl2CppObject_TisNoOptions_t32144009_m1397842095_gshared)(__this /* static, unused */, ___t0, ___newStartValue1, ___newEndValue2, ___newDuration3, method)
// System.Single DG.Tweening.Tweener::DoUpdateDelay<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
extern "C"  float Tweener_DoUpdateDelay_TisIl2CppObject_TisIl2CppObject_TisNoOptions_t32144009_m3571034998_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t1619290041 * ___t0, float ___elapsed1, const MethodInfo* method);
#define Tweener_DoUpdateDelay_TisIl2CppObject_TisIl2CppObject_TisNoOptions_t32144009_m3571034998(__this /* static, unused */, ___t0, ___elapsed1, method) ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1619290041 *, float, const MethodInfo*))Tweener_DoUpdateDelay_TisIl2CppObject_TisIl2CppObject_TisNoOptions_t32144009_m3571034998_gshared)(__this /* static, unused */, ___t0, ___elapsed1, method)
// System.Boolean DG.Tweening.Tweener::DoStartup<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern "C"  bool Tweener_DoStartup_TisIl2CppObject_TisIl2CppObject_TisNoOptions_t32144009_m1424710498_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t1619290041 * ___t0, const MethodInfo* method);
#define Tweener_DoStartup_TisIl2CppObject_TisIl2CppObject_TisNoOptions_t32144009_m1424710498(__this /* static, unused */, ___t0, method) ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1619290041 *, const MethodInfo*))Tweener_DoStartup_TisIl2CppObject_TisIl2CppObject_TisNoOptions_t32144009_m1424710498_gshared)(__this /* static, unused */, ___t0, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeStartValue<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeStartValue_TisIl2CppObject_TisIl2CppObject_TisStringOptions_t2623536697_m339392576_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t4210682729 * ___t0, Il2CppObject * ___newStartValue1, float ___newDuration2, const MethodInfo* method);
#define Tweener_DoChangeStartValue_TisIl2CppObject_TisIl2CppObject_TisStringOptions_t2623536697_m339392576(__this /* static, unused */, ___t0, ___newStartValue1, ___newDuration2, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t4210682729 *, Il2CppObject *, float, const MethodInfo*))Tweener_DoChangeStartValue_TisIl2CppObject_TisIl2CppObject_TisStringOptions_t2623536697_m339392576_gshared)(__this /* static, unused */, ___t0, ___newStartValue1, ___newDuration2, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeEndValue<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single,System.Boolean)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeEndValue_TisIl2CppObject_TisIl2CppObject_TisStringOptions_t2623536697_m1421622_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t4210682729 * ___t0, Il2CppObject * ___newEndValue1, float ___newDuration2, bool ___snapStartValue3, const MethodInfo* method);
#define Tweener_DoChangeEndValue_TisIl2CppObject_TisIl2CppObject_TisStringOptions_t2623536697_m1421622(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t4210682729 *, Il2CppObject *, float, bool, const MethodInfo*))Tweener_DoChangeEndValue_TisIl2CppObject_TisIl2CppObject_TisStringOptions_t2623536697_m1421622_gshared)(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeValues<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,T2,System.Single)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeValues_TisIl2CppObject_TisIl2CppObject_TisStringOptions_t2623536697_m542353407_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t4210682729 * ___t0, Il2CppObject * ___newStartValue1, Il2CppObject * ___newEndValue2, float ___newDuration3, const MethodInfo* method);
#define Tweener_DoChangeValues_TisIl2CppObject_TisIl2CppObject_TisStringOptions_t2623536697_m542353407(__this /* static, unused */, ___t0, ___newStartValue1, ___newEndValue2, ___newDuration3, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t4210682729 *, Il2CppObject *, Il2CppObject *, float, const MethodInfo*))Tweener_DoChangeValues_TisIl2CppObject_TisIl2CppObject_TisStringOptions_t2623536697_m542353407_gshared)(__this /* static, unused */, ___t0, ___newStartValue1, ___newEndValue2, ___newDuration3, method)
// !!0 System.Activator::CreateInstance<DG.Tweening.Plugins.Options.StringOptions>()
extern "C"  StringOptions_t2623536697  Activator_CreateInstance_TisStringOptions_t2623536697_m1446967937_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Activator_CreateInstance_TisStringOptions_t2623536697_m1446967937(__this /* static, unused */, method) ((  StringOptions_t2623536697  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Activator_CreateInstance_TisStringOptions_t2623536697_m1446967937_gshared)(__this /* static, unused */, method)
// System.Single DG.Tweening.Tweener::DoUpdateDelay<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
extern "C"  float Tweener_DoUpdateDelay_TisIl2CppObject_TisIl2CppObject_TisStringOptions_t2623536697_m1869296454_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t4210682729 * ___t0, float ___elapsed1, const MethodInfo* method);
#define Tweener_DoUpdateDelay_TisIl2CppObject_TisIl2CppObject_TisStringOptions_t2623536697_m1869296454(__this /* static, unused */, ___t0, ___elapsed1, method) ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t4210682729 *, float, const MethodInfo*))Tweener_DoUpdateDelay_TisIl2CppObject_TisIl2CppObject_TisStringOptions_t2623536697_m1869296454_gshared)(__this /* static, unused */, ___t0, ___elapsed1, method)
// System.Boolean DG.Tweening.Tweener::DoStartup<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern "C"  bool Tweener_DoStartup_TisIl2CppObject_TisIl2CppObject_TisStringOptions_t2623536697_m3507936946_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t4210682729 * ___t0, const MethodInfo* method);
#define Tweener_DoStartup_TisIl2CppObject_TisIl2CppObject_TisStringOptions_t2623536697_m3507936946(__this /* static, unused */, ___t0, method) ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t4210682729 *, const MethodInfo*))Tweener_DoStartup_TisIl2CppObject_TisIl2CppObject_TisStringOptions_t2623536697_m3507936946_gshared)(__this /* static, unused */, ___t0, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeStartValue<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeStartValue_TisSingle_t958209021_TisSingle_t958209021_TisFloatOptions_t3779806670_m3995509173_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t2841494226 * ___t0, float ___newStartValue1, float ___newDuration2, const MethodInfo* method);
#define Tweener_DoChangeStartValue_TisSingle_t958209021_TisSingle_t958209021_TisFloatOptions_t3779806670_m3995509173(__this /* static, unused */, ___t0, ___newStartValue1, ___newDuration2, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2841494226 *, float, float, const MethodInfo*))Tweener_DoChangeStartValue_TisSingle_t958209021_TisSingle_t958209021_TisFloatOptions_t3779806670_m3995509173_gshared)(__this /* static, unused */, ___t0, ___newStartValue1, ___newDuration2, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeEndValue<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single,System.Boolean)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeEndValue_TisSingle_t958209021_TisSingle_t958209021_TisFloatOptions_t3779806670_m296595471_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t2841494226 * ___t0, float ___newEndValue1, float ___newDuration2, bool ___snapStartValue3, const MethodInfo* method);
#define Tweener_DoChangeEndValue_TisSingle_t958209021_TisSingle_t958209021_TisFloatOptions_t3779806670_m296595471(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2841494226 *, float, float, bool, const MethodInfo*))Tweener_DoChangeEndValue_TisSingle_t958209021_TisSingle_t958209021_TisFloatOptions_t3779806670_m296595471_gshared)(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeValues<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,T2,System.Single)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeValues_TisSingle_t958209021_TisSingle_t958209021_TisFloatOptions_t3779806670_m1920875888_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t2841494226 * ___t0, float ___newStartValue1, float ___newEndValue2, float ___newDuration3, const MethodInfo* method);
#define Tweener_DoChangeValues_TisSingle_t958209021_TisSingle_t958209021_TisFloatOptions_t3779806670_m1920875888(__this /* static, unused */, ___t0, ___newStartValue1, ___newEndValue2, ___newDuration3, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2841494226 *, float, float, float, const MethodInfo*))Tweener_DoChangeValues_TisSingle_t958209021_TisSingle_t958209021_TisFloatOptions_t3779806670_m1920875888_gshared)(__this /* static, unused */, ___t0, ___newStartValue1, ___newEndValue2, ___newDuration3, method)
// !!0 System.Activator::CreateInstance<DG.Tweening.Plugins.Options.FloatOptions>()
extern "C"  FloatOptions_t3779806670  Activator_CreateInstance_TisFloatOptions_t3779806670_m2185227982_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Activator_CreateInstance_TisFloatOptions_t3779806670_m2185227982(__this /* static, unused */, method) ((  FloatOptions_t3779806670  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Activator_CreateInstance_TisFloatOptions_t3779806670_m2185227982_gshared)(__this /* static, unused */, method)
// System.Single DG.Tweening.Tweener::DoUpdateDelay<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
extern "C"  float Tweener_DoUpdateDelay_TisSingle_t958209021_TisSingle_t958209021_TisFloatOptions_t3779806670_m1646714633_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t2841494226 * ___t0, float ___elapsed1, const MethodInfo* method);
#define Tweener_DoUpdateDelay_TisSingle_t958209021_TisSingle_t958209021_TisFloatOptions_t3779806670_m1646714633(__this /* static, unused */, ___t0, ___elapsed1, method) ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2841494226 *, float, const MethodInfo*))Tweener_DoUpdateDelay_TisSingle_t958209021_TisSingle_t958209021_TisFloatOptions_t3779806670_m1646714633_gshared)(__this /* static, unused */, ___t0, ___elapsed1, method)
// System.Boolean DG.Tweening.Tweener::DoStartup<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern "C"  bool Tweener_DoStartup_TisSingle_t958209021_TisSingle_t958209021_TisFloatOptions_t3779806670_m2981782643_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t2841494226 * ___t0, const MethodInfo* method);
#define Tweener_DoStartup_TisSingle_t958209021_TisSingle_t958209021_TisFloatOptions_t3779806670_m2981782643(__this /* static, unused */, ___t0, method) ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2841494226 *, const MethodInfo*))Tweener_DoStartup_TisSingle_t958209021_TisSingle_t958209021_TisFloatOptions_t3779806670_m2981782643_gshared)(__this /* static, unused */, ___t0, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeStartValue<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeStartValue_TisUInt32_t985925326_TisUInt32_t985925326_TisNoOptions_t32144009_m821379140_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t4224275457 * ___t0, uint32_t ___newStartValue1, float ___newDuration2, const MethodInfo* method);
#define Tweener_DoChangeStartValue_TisUInt32_t985925326_TisUInt32_t985925326_TisNoOptions_t32144009_m821379140(__this /* static, unused */, ___t0, ___newStartValue1, ___newDuration2, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t4224275457 *, uint32_t, float, const MethodInfo*))Tweener_DoChangeStartValue_TisUInt32_t985925326_TisUInt32_t985925326_TisNoOptions_t32144009_m821379140_gshared)(__this /* static, unused */, ___t0, ___newStartValue1, ___newDuration2, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeEndValue<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single,System.Boolean)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeEndValue_TisUInt32_t985925326_TisUInt32_t985925326_TisNoOptions_t32144009_m1764328754_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t4224275457 * ___t0, uint32_t ___newEndValue1, float ___newDuration2, bool ___snapStartValue3, const MethodInfo* method);
#define Tweener_DoChangeEndValue_TisUInt32_t985925326_TisUInt32_t985925326_TisNoOptions_t32144009_m1764328754(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t4224275457 *, uint32_t, float, bool, const MethodInfo*))Tweener_DoChangeEndValue_TisUInt32_t985925326_TisUInt32_t985925326_TisNoOptions_t32144009_m1764328754_gshared)(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeValues<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,T2,System.Single)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeValues_TisUInt32_t985925326_TisUInt32_t985925326_TisNoOptions_t32144009_m2718748667_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t4224275457 * ___t0, uint32_t ___newStartValue1, uint32_t ___newEndValue2, float ___newDuration3, const MethodInfo* method);
#define Tweener_DoChangeValues_TisUInt32_t985925326_TisUInt32_t985925326_TisNoOptions_t32144009_m2718748667(__this /* static, unused */, ___t0, ___newStartValue1, ___newEndValue2, ___newDuration3, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t4224275457 *, uint32_t, uint32_t, float, const MethodInfo*))Tweener_DoChangeValues_TisUInt32_t985925326_TisUInt32_t985925326_TisNoOptions_t32144009_m2718748667_gshared)(__this /* static, unused */, ___t0, ___newStartValue1, ___newEndValue2, ___newDuration3, method)
// System.Single DG.Tweening.Tweener::DoUpdateDelay<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
extern "C"  float Tweener_DoUpdateDelay_TisUInt32_t985925326_TisUInt32_t985925326_TisNoOptions_t32144009_m4085089218_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t4224275457 * ___t0, float ___elapsed1, const MethodInfo* method);
#define Tweener_DoUpdateDelay_TisUInt32_t985925326_TisUInt32_t985925326_TisNoOptions_t32144009_m4085089218(__this /* static, unused */, ___t0, ___elapsed1, method) ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t4224275457 *, float, const MethodInfo*))Tweener_DoUpdateDelay_TisUInt32_t985925326_TisUInt32_t985925326_TisNoOptions_t32144009_m4085089218_gshared)(__this /* static, unused */, ___t0, ___elapsed1, method)
// System.Boolean DG.Tweening.Tweener::DoStartup<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern "C"  bool Tweener_DoStartup_TisUInt32_t985925326_TisUInt32_t985925326_TisNoOptions_t32144009_m3712379054_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t4224275457 * ___t0, const MethodInfo* method);
#define Tweener_DoStartup_TisUInt32_t985925326_TisUInt32_t985925326_TisNoOptions_t32144009_m3712379054(__this /* static, unused */, ___t0, method) ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t4224275457 *, const MethodInfo*))Tweener_DoStartup_TisUInt32_t985925326_TisUInt32_t985925326_TisNoOptions_t32144009_m3712379054_gshared)(__this /* static, unused */, ___t0, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeStartValue<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeStartValue_TisUInt64_t985925421_TisUInt64_t985925421_TisNoOptions_t32144009_m1015642562_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t2578226509 * ___t0, uint64_t ___newStartValue1, float ___newDuration2, const MethodInfo* method);
#define Tweener_DoChangeStartValue_TisUInt64_t985925421_TisUInt64_t985925421_TisNoOptions_t32144009_m1015642562(__this /* static, unused */, ___t0, ___newStartValue1, ___newDuration2, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2578226509 *, uint64_t, float, const MethodInfo*))Tweener_DoChangeStartValue_TisUInt64_t985925421_TisUInt64_t985925421_TisNoOptions_t32144009_m1015642562_gshared)(__this /* static, unused */, ___t0, ___newStartValue1, ___newDuration2, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeEndValue<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single,System.Boolean)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeEndValue_TisUInt64_t985925421_TisUInt64_t985925421_TisNoOptions_t32144009_m3276234740_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t2578226509 * ___t0, uint64_t ___newEndValue1, float ___newDuration2, bool ___snapStartValue3, const MethodInfo* method);
#define Tweener_DoChangeEndValue_TisUInt64_t985925421_TisUInt64_t985925421_TisNoOptions_t32144009_m3276234740(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2578226509 *, uint64_t, float, bool, const MethodInfo*))Tweener_DoChangeEndValue_TisUInt64_t985925421_TisUInt64_t985925421_TisNoOptions_t32144009_m3276234740_gshared)(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeValues<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,T2,System.Single)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeValues_TisUInt64_t985925421_TisUInt64_t985925421_TisNoOptions_t32144009_m404438461_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t2578226509 * ___t0, uint64_t ___newStartValue1, uint64_t ___newEndValue2, float ___newDuration3, const MethodInfo* method);
#define Tweener_DoChangeValues_TisUInt64_t985925421_TisUInt64_t985925421_TisNoOptions_t32144009_m404438461(__this /* static, unused */, ___t0, ___newStartValue1, ___newEndValue2, ___newDuration3, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2578226509 *, uint64_t, uint64_t, float, const MethodInfo*))Tweener_DoChangeValues_TisUInt64_t985925421_TisUInt64_t985925421_TisNoOptions_t32144009_m404438461_gshared)(__this /* static, unused */, ___t0, ___newStartValue1, ___newEndValue2, ___newDuration3, method)
// System.Single DG.Tweening.Tweener::DoUpdateDelay<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
extern "C"  float Tweener_DoUpdateDelay_TisUInt64_t985925421_TisUInt64_t985925421_TisNoOptions_t32144009_m4183419652_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t2578226509 * ___t0, float ___elapsed1, const MethodInfo* method);
#define Tweener_DoUpdateDelay_TisUInt64_t985925421_TisUInt64_t985925421_TisNoOptions_t32144009_m4183419652(__this /* static, unused */, ___t0, ___elapsed1, method) ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2578226509 *, float, const MethodInfo*))Tweener_DoUpdateDelay_TisUInt64_t985925421_TisUInt64_t985925421_TisNoOptions_t32144009_m4183419652_gshared)(__this /* static, unused */, ___t0, ___elapsed1, method)
// System.Boolean DG.Tweening.Tweener::DoStartup<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern "C"  bool Tweener_DoStartup_TisUInt64_t985925421_TisUInt64_t985925421_TisNoOptions_t32144009_m670026608_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t2578226509 * ___t0, const MethodInfo* method);
#define Tweener_DoStartup_TisUInt64_t985925421_TisUInt64_t985925421_TisNoOptions_t32144009_m670026608(__this /* static, unused */, ___t0, method) ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2578226509 *, const MethodInfo*))Tweener_DoStartup_TisUInt64_t985925421_TisUInt64_t985925421_TisNoOptions_t32144009_m670026608_gshared)(__this /* static, unused */, ___t0, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeStartValue<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeStartValue_TisColor_t1588175760_TisColor_t1588175760_TisColorOptions_t4161000167_m2758608196_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t4289140679 * ___t0, Color_t1588175760  ___newStartValue1, float ___newDuration2, const MethodInfo* method);
#define Tweener_DoChangeStartValue_TisColor_t1588175760_TisColor_t1588175760_TisColorOptions_t4161000167_m2758608196(__this /* static, unused */, ___t0, ___newStartValue1, ___newDuration2, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t4289140679 *, Color_t1588175760 , float, const MethodInfo*))Tweener_DoChangeStartValue_TisColor_t1588175760_TisColor_t1588175760_TisColorOptions_t4161000167_m2758608196_gshared)(__this /* static, unused */, ___t0, ___newStartValue1, ___newDuration2, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeEndValue<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single,System.Boolean)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeEndValue_TisColor_t1588175760_TisColor_t1588175760_TisColorOptions_t4161000167_m3834774944_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t4289140679 * ___t0, Color_t1588175760  ___newEndValue1, float ___newDuration2, bool ___snapStartValue3, const MethodInfo* method);
#define Tweener_DoChangeEndValue_TisColor_t1588175760_TisColor_t1588175760_TisColorOptions_t4161000167_m3834774944(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t4289140679 *, Color_t1588175760 , float, bool, const MethodInfo*))Tweener_DoChangeEndValue_TisColor_t1588175760_TisColor_t1588175760_TisColorOptions_t4161000167_m3834774944_gshared)(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeValues<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,T2,System.Single)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeValues_TisColor_t1588175760_TisColor_t1588175760_TisColorOptions_t4161000167_m2974914689_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t4289140679 * ___t0, Color_t1588175760  ___newStartValue1, Color_t1588175760  ___newEndValue2, float ___newDuration3, const MethodInfo* method);
#define Tweener_DoChangeValues_TisColor_t1588175760_TisColor_t1588175760_TisColorOptions_t4161000167_m2974914689(__this /* static, unused */, ___t0, ___newStartValue1, ___newEndValue2, ___newDuration3, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t4289140679 *, Color_t1588175760 , Color_t1588175760 , float, const MethodInfo*))Tweener_DoChangeValues_TisColor_t1588175760_TisColor_t1588175760_TisColorOptions_t4161000167_m2974914689_gshared)(__this /* static, unused */, ___t0, ___newStartValue1, ___newEndValue2, ___newDuration3, method)
// System.Single DG.Tweening.Tweener::DoUpdateDelay<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
extern "C"  float Tweener_DoUpdateDelay_TisColor_t1588175760_TisColor_t1588175760_TisColorOptions_t4161000167_m2708124122_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t4289140679 * ___t0, float ___elapsed1, const MethodInfo* method);
#define Tweener_DoUpdateDelay_TisColor_t1588175760_TisColor_t1588175760_TisColorOptions_t4161000167_m2708124122(__this /* static, unused */, ___t0, ___elapsed1, method) ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t4289140679 *, float, const MethodInfo*))Tweener_DoUpdateDelay_TisColor_t1588175760_TisColor_t1588175760_TisColorOptions_t4161000167_m2708124122_gshared)(__this /* static, unused */, ___t0, ___elapsed1, method)
// System.Boolean DG.Tweening.Tweener::DoStartup<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern "C"  bool Tweener_DoStartup_TisColor_t1588175760_TisColor_t1588175760_TisColorOptions_t4161000167_m1018382852_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t4289140679 * ___t0, const MethodInfo* method);
#define Tweener_DoStartup_TisColor_t1588175760_TisColor_t1588175760_TisColorOptions_t4161000167_m1018382852(__this /* static, unused */, ___t0, method) ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t4289140679 *, const MethodInfo*))Tweener_DoStartup_TisColor_t1588175760_TisColor_t1588175760_TisColorOptions_t4161000167_m1018382852_gshared)(__this /* static, unused */, ___t0, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeStartValue<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeStartValue_TisQuaternion_t1891715979_TisQuaternion_t1891715979_TisNoOptions_t32144009_m265267660_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t1968778341 * ___t0, Quaternion_t1891715979  ___newStartValue1, float ___newDuration2, const MethodInfo* method);
#define Tweener_DoChangeStartValue_TisQuaternion_t1891715979_TisQuaternion_t1891715979_TisNoOptions_t32144009_m265267660(__this /* static, unused */, ___t0, ___newStartValue1, ___newDuration2, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1968778341 *, Quaternion_t1891715979 , float, const MethodInfo*))Tweener_DoChangeStartValue_TisQuaternion_t1891715979_TisQuaternion_t1891715979_TisNoOptions_t32144009_m265267660_gshared)(__this /* static, unused */, ___t0, ___newStartValue1, ___newDuration2, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeEndValue<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single,System.Boolean)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeEndValue_TisQuaternion_t1891715979_TisQuaternion_t1891715979_TisNoOptions_t32144009_m4227436138_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t1968778341 * ___t0, Quaternion_t1891715979  ___newEndValue1, float ___newDuration2, bool ___snapStartValue3, const MethodInfo* method);
#define Tweener_DoChangeEndValue_TisQuaternion_t1891715979_TisQuaternion_t1891715979_TisNoOptions_t32144009_m4227436138(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1968778341 *, Quaternion_t1891715979 , float, bool, const MethodInfo*))Tweener_DoChangeEndValue_TisQuaternion_t1891715979_TisQuaternion_t1891715979_TisNoOptions_t32144009_m4227436138_gshared)(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeValues<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,T2,System.Single)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeValues_TisQuaternion_t1891715979_TisQuaternion_t1891715979_TisNoOptions_t32144009_m1228818483_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t1968778341 * ___t0, Quaternion_t1891715979  ___newStartValue1, Quaternion_t1891715979  ___newEndValue2, float ___newDuration3, const MethodInfo* method);
#define Tweener_DoChangeValues_TisQuaternion_t1891715979_TisQuaternion_t1891715979_TisNoOptions_t32144009_m1228818483(__this /* static, unused */, ___t0, ___newStartValue1, ___newEndValue2, ___newDuration3, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1968778341 *, Quaternion_t1891715979 , Quaternion_t1891715979 , float, const MethodInfo*))Tweener_DoChangeValues_TisQuaternion_t1891715979_TisQuaternion_t1891715979_TisNoOptions_t32144009_m1228818483_gshared)(__this /* static, unused */, ___t0, ___newStartValue1, ___newEndValue2, ___newDuration3, method)
// System.Single DG.Tweening.Tweener::DoUpdateDelay<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
extern "C"  float Tweener_DoUpdateDelay_TisQuaternion_t1891715979_TisQuaternion_t1891715979_TisNoOptions_t32144009_m3395166778_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t1968778341 * ___t0, float ___elapsed1, const MethodInfo* method);
#define Tweener_DoUpdateDelay_TisQuaternion_t1891715979_TisQuaternion_t1891715979_TisNoOptions_t32144009_m3395166778(__this /* static, unused */, ___t0, ___elapsed1, method) ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1968778341 *, float, const MethodInfo*))Tweener_DoUpdateDelay_TisQuaternion_t1891715979_TisQuaternion_t1891715979_TisNoOptions_t32144009_m3395166778_gshared)(__this /* static, unused */, ___t0, ___elapsed1, method)
// System.Boolean DG.Tweening.Tweener::DoStartup<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern "C"  bool Tweener_DoStartup_TisQuaternion_t1891715979_TisQuaternion_t1891715979_TisNoOptions_t32144009_m428177126_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t1968778341 * ___t0, const MethodInfo* method);
#define Tweener_DoStartup_TisQuaternion_t1891715979_TisQuaternion_t1891715979_TisNoOptions_t32144009_m428177126(__this /* static, unused */, ___t0, method) ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1968778341 *, const MethodInfo*))Tweener_DoStartup_TisQuaternion_t1891715979_TisQuaternion_t1891715979_TisNoOptions_t32144009_m428177126_gshared)(__this /* static, unused */, ___t0, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeStartValue<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeStartValue_TisQuaternion_t1891715979_TisVector3_t3525329789_TisQuaternionOptions_t1813596812_m1753658123_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t3741400174 * ___t0, Vector3_t3525329789  ___newStartValue1, float ___newDuration2, const MethodInfo* method);
#define Tweener_DoChangeStartValue_TisQuaternion_t1891715979_TisVector3_t3525329789_TisQuaternionOptions_t1813596812_m1753658123(__this /* static, unused */, ___t0, ___newStartValue1, ___newDuration2, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3741400174 *, Vector3_t3525329789 , float, const MethodInfo*))Tweener_DoChangeStartValue_TisQuaternion_t1891715979_TisVector3_t3525329789_TisQuaternionOptions_t1813596812_m1753658123_gshared)(__this /* static, unused */, ___t0, ___newStartValue1, ___newDuration2, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeEndValue<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single,System.Boolean)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeEndValue_TisQuaternion_t1891715979_TisVector3_t3525329789_TisQuaternionOptions_t1813596812_m2273611385_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t3741400174 * ___t0, Vector3_t3525329789  ___newEndValue1, float ___newDuration2, bool ___snapStartValue3, const MethodInfo* method);
#define Tweener_DoChangeEndValue_TisQuaternion_t1891715979_TisVector3_t3525329789_TisQuaternionOptions_t1813596812_m2273611385(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3741400174 *, Vector3_t3525329789 , float, bool, const MethodInfo*))Tweener_DoChangeEndValue_TisQuaternion_t1891715979_TisVector3_t3525329789_TisQuaternionOptions_t1813596812_m2273611385_gshared)(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeValues<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,T2,System.Single)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeValues_TisQuaternion_t1891715979_TisVector3_t3525329789_TisQuaternionOptions_t1813596812_m3272494298_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t3741400174 * ___t0, Vector3_t3525329789  ___newStartValue1, Vector3_t3525329789  ___newEndValue2, float ___newDuration3, const MethodInfo* method);
#define Tweener_DoChangeValues_TisQuaternion_t1891715979_TisVector3_t3525329789_TisQuaternionOptions_t1813596812_m3272494298(__this /* static, unused */, ___t0, ___newStartValue1, ___newEndValue2, ___newDuration3, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3741400174 *, Vector3_t3525329789 , Vector3_t3525329789 , float, const MethodInfo*))Tweener_DoChangeValues_TisQuaternion_t1891715979_TisVector3_t3525329789_TisQuaternionOptions_t1813596812_m3272494298_gshared)(__this /* static, unused */, ___t0, ___newStartValue1, ___newEndValue2, ___newDuration3, method)
// !!0 System.Activator::CreateInstance<DG.Tweening.Plugins.Options.QuaternionOptions>()
extern "C"  QuaternionOptions_t1813596812  Activator_CreateInstance_TisQuaternionOptions_t1813596812_m619891150_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Activator_CreateInstance_TisQuaternionOptions_t1813596812_m619891150(__this /* static, unused */, method) ((  QuaternionOptions_t1813596812  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Activator_CreateInstance_TisQuaternionOptions_t1813596812_m619891150_gshared)(__this /* static, unused */, method)
// System.Single DG.Tweening.Tweener::DoUpdateDelay<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
extern "C"  float Tweener_DoUpdateDelay_TisQuaternion_t1891715979_TisVector3_t3525329789_TisQuaternionOptions_t1813596812_m1562419059_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t3741400174 * ___t0, float ___elapsed1, const MethodInfo* method);
#define Tweener_DoUpdateDelay_TisQuaternion_t1891715979_TisVector3_t3525329789_TisQuaternionOptions_t1813596812_m1562419059(__this /* static, unused */, ___t0, ___elapsed1, method) ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3741400174 *, float, const MethodInfo*))Tweener_DoUpdateDelay_TisQuaternion_t1891715979_TisVector3_t3525329789_TisQuaternionOptions_t1813596812_m1562419059_gshared)(__this /* static, unused */, ___t0, ___elapsed1, method)
// System.Boolean DG.Tweening.Tweener::DoStartup<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern "C"  bool Tweener_DoStartup_TisQuaternion_t1891715979_TisVector3_t3525329789_TisQuaternionOptions_t1813596812_m598477021_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t3741400174 * ___t0, const MethodInfo* method);
#define Tweener_DoStartup_TisQuaternion_t1891715979_TisVector3_t3525329789_TisQuaternionOptions_t1813596812_m598477021(__this /* static, unused */, ___t0, method) ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3741400174 *, const MethodInfo*))Tweener_DoStartup_TisQuaternion_t1891715979_TisVector3_t3525329789_TisQuaternionOptions_t1813596812_m598477021_gshared)(__this /* static, unused */, ___t0, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeStartValue<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeStartValue_TisRect_t1525428817_TisRect_t1525428817_TisRectOptions_t1160272134_m2991735625_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t2274666266 * ___t0, Rect_t1525428817  ___newStartValue1, float ___newDuration2, const MethodInfo* method);
#define Tweener_DoChangeStartValue_TisRect_t1525428817_TisRect_t1525428817_TisRectOptions_t1160272134_m2991735625(__this /* static, unused */, ___t0, ___newStartValue1, ___newDuration2, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2274666266 *, Rect_t1525428817 , float, const MethodInfo*))Tweener_DoChangeStartValue_TisRect_t1525428817_TisRect_t1525428817_TisRectOptions_t1160272134_m2991735625_gshared)(__this /* static, unused */, ___t0, ___newStartValue1, ___newDuration2, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeEndValue<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single,System.Boolean)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeEndValue_TisRect_t1525428817_TisRect_t1525428817_TisRectOptions_t1160272134_m492170893_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t2274666266 * ___t0, Rect_t1525428817  ___newEndValue1, float ___newDuration2, bool ___snapStartValue3, const MethodInfo* method);
#define Tweener_DoChangeEndValue_TisRect_t1525428817_TisRect_t1525428817_TisRectOptions_t1160272134_m492170893(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2274666266 *, Rect_t1525428817 , float, bool, const MethodInfo*))Tweener_DoChangeEndValue_TisRect_t1525428817_TisRect_t1525428817_TisRectOptions_t1160272134_m492170893_gshared)(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeValues<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,T2,System.Single)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeValues_TisRect_t1525428817_TisRect_t1525428817_TisRectOptions_t1160272134_m2193929686_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t2274666266 * ___t0, Rect_t1525428817  ___newStartValue1, Rect_t1525428817  ___newEndValue2, float ___newDuration3, const MethodInfo* method);
#define Tweener_DoChangeValues_TisRect_t1525428817_TisRect_t1525428817_TisRectOptions_t1160272134_m2193929686(__this /* static, unused */, ___t0, ___newStartValue1, ___newEndValue2, ___newDuration3, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2274666266 *, Rect_t1525428817 , Rect_t1525428817 , float, const MethodInfo*))Tweener_DoChangeValues_TisRect_t1525428817_TisRect_t1525428817_TisRectOptions_t1160272134_m2193929686_gshared)(__this /* static, unused */, ___t0, ___newStartValue1, ___newEndValue2, ___newDuration3, method)
// !!0 System.Activator::CreateInstance<DG.Tweening.Plugins.Options.RectOptions>()
extern "C"  RectOptions_t1160272134  Activator_CreateInstance_TisRectOptions_t1160272134_m3185608788_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Activator_CreateInstance_TisRectOptions_t1160272134_m3185608788(__this /* static, unused */, method) ((  RectOptions_t1160272134  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Activator_CreateInstance_TisRectOptions_t1160272134_m3185608788_gshared)(__this /* static, unused */, method)
// System.Single DG.Tweening.Tweener::DoUpdateDelay<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
extern "C"  float Tweener_DoUpdateDelay_TisRect_t1525428817_TisRect_t1525428817_TisRectOptions_t1160272134_m2872210141_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t2274666266 * ___t0, float ___elapsed1, const MethodInfo* method);
#define Tweener_DoUpdateDelay_TisRect_t1525428817_TisRect_t1525428817_TisRectOptions_t1160272134_m2872210141(__this /* static, unused */, ___t0, ___elapsed1, method) ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2274666266 *, float, const MethodInfo*))Tweener_DoUpdateDelay_TisRect_t1525428817_TisRect_t1525428817_TisRectOptions_t1160272134_m2872210141_gshared)(__this /* static, unused */, ___t0, ___elapsed1, method)
// System.Boolean DG.Tweening.Tweener::DoStartup<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern "C"  bool Tweener_DoStartup_TisRect_t1525428817_TisRect_t1525428817_TisRectOptions_t1160272134_m3050336777_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t2274666266 * ___t0, const MethodInfo* method);
#define Tweener_DoStartup_TisRect_t1525428817_TisRect_t1525428817_TisRectOptions_t1160272134_m3050336777(__this /* static, unused */, ___t0, method) ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2274666266 *, const MethodInfo*))Tweener_DoStartup_TisRect_t1525428817_TisRect_t1525428817_TisRectOptions_t1160272134_m3050336777_gshared)(__this /* static, unused */, ___t0, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeStartValue<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeStartValue_TisVector2_t3525329788_TisVector2_t3525329788_TisVectorOptions_t3308462279_m1318064898_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t753146263 * ___t0, Vector2_t3525329788  ___newStartValue1, float ___newDuration2, const MethodInfo* method);
#define Tweener_DoChangeStartValue_TisVector2_t3525329788_TisVector2_t3525329788_TisVectorOptions_t3308462279_m1318064898(__this /* static, unused */, ___t0, ___newStartValue1, ___newDuration2, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t753146263 *, Vector2_t3525329788 , float, const MethodInfo*))Tweener_DoChangeStartValue_TisVector2_t3525329788_TisVector2_t3525329788_TisVectorOptions_t3308462279_m1318064898_gshared)(__this /* static, unused */, ___t0, ___newStartValue1, ___newDuration2, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeEndValue<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single,System.Boolean)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeEndValue_TisVector2_t3525329788_TisVector2_t3525329788_TisVectorOptions_t3308462279_m731450548_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t753146263 * ___t0, Vector2_t3525329788  ___newEndValue1, float ___newDuration2, bool ___snapStartValue3, const MethodInfo* method);
#define Tweener_DoChangeEndValue_TisVector2_t3525329788_TisVector2_t3525329788_TisVectorOptions_t3308462279_m731450548(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t753146263 *, Vector2_t3525329788 , float, bool, const MethodInfo*))Tweener_DoChangeEndValue_TisVector2_t3525329788_TisVector2_t3525329788_TisVectorOptions_t3308462279_m731450548_gshared)(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeValues<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,T2,System.Single)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeValues_TisVector2_t3525329788_TisVector2_t3525329788_TisVectorOptions_t3308462279_m2574721661_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t753146263 * ___t0, Vector2_t3525329788  ___newStartValue1, Vector2_t3525329788  ___newEndValue2, float ___newDuration3, const MethodInfo* method);
#define Tweener_DoChangeValues_TisVector2_t3525329788_TisVector2_t3525329788_TisVectorOptions_t3308462279_m2574721661(__this /* static, unused */, ___t0, ___newStartValue1, ___newEndValue2, ___newDuration3, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t753146263 *, Vector2_t3525329788 , Vector2_t3525329788 , float, const MethodInfo*))Tweener_DoChangeValues_TisVector2_t3525329788_TisVector2_t3525329788_TisVectorOptions_t3308462279_m2574721661_gshared)(__this /* static, unused */, ___t0, ___newStartValue1, ___newEndValue2, ___newDuration3, method)
// !!0 System.Activator::CreateInstance<DG.Tweening.Plugins.Options.VectorOptions>()
extern "C"  VectorOptions_t3308462279  Activator_CreateInstance_TisVectorOptions_t3308462279_m675358003_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Activator_CreateInstance_TisVectorOptions_t3308462279_m675358003(__this /* static, unused */, method) ((  VectorOptions_t3308462279  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Activator_CreateInstance_TisVectorOptions_t3308462279_m675358003_gshared)(__this /* static, unused */, method)
// System.Single DG.Tweening.Tweener::DoUpdateDelay<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
extern "C"  float Tweener_DoUpdateDelay_TisVector2_t3525329788_TisVector2_t3525329788_TisVectorOptions_t3308462279_m3640193476_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t753146263 * ___t0, float ___elapsed1, const MethodInfo* method);
#define Tweener_DoUpdateDelay_TisVector2_t3525329788_TisVector2_t3525329788_TisVectorOptions_t3308462279_m3640193476(__this /* static, unused */, ___t0, ___elapsed1, method) ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t753146263 *, float, const MethodInfo*))Tweener_DoUpdateDelay_TisVector2_t3525329788_TisVector2_t3525329788_TisVectorOptions_t3308462279_m3640193476_gshared)(__this /* static, unused */, ___t0, ___elapsed1, method)
// System.Boolean DG.Tweening.Tweener::DoStartup<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern "C"  bool Tweener_DoStartup_TisVector2_t3525329788_TisVector2_t3525329788_TisVectorOptions_t3308462279_m3865272368_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t753146263 * ___t0, const MethodInfo* method);
#define Tweener_DoStartup_TisVector2_t3525329788_TisVector2_t3525329788_TisVectorOptions_t3308462279_m3865272368(__this /* static, unused */, ___t0, method) ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t753146263 *, const MethodInfo*))Tweener_DoStartup_TisVector2_t3525329788_TisVector2_t3525329788_TisVectorOptions_t3308462279_m3865272368_gshared)(__this /* static, unused */, ___t0, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeStartValue<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeStartValue_TisVector3_t3525329789_TisIl2CppObject_TisPathOptions_t1144419237_m3199528071_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t1820784502 * ___t0, Il2CppObject * ___newStartValue1, float ___newDuration2, const MethodInfo* method);
#define Tweener_DoChangeStartValue_TisVector3_t3525329789_TisIl2CppObject_TisPathOptions_t1144419237_m3199528071(__this /* static, unused */, ___t0, ___newStartValue1, ___newDuration2, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1820784502 *, Il2CppObject *, float, const MethodInfo*))Tweener_DoChangeStartValue_TisVector3_t3525329789_TisIl2CppObject_TisPathOptions_t1144419237_m3199528071_gshared)(__this /* static, unused */, ___t0, ___newStartValue1, ___newDuration2, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeEndValue<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single,System.Boolean)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeEndValue_TisVector3_t3525329789_TisIl2CppObject_TisPathOptions_t1144419237_m1837097359_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t1820784502 * ___t0, Il2CppObject * ___newEndValue1, float ___newDuration2, bool ___snapStartValue3, const MethodInfo* method);
#define Tweener_DoChangeEndValue_TisVector3_t3525329789_TisIl2CppObject_TisPathOptions_t1144419237_m1837097359(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1820784502 *, Il2CppObject *, float, bool, const MethodInfo*))Tweener_DoChangeEndValue_TisVector3_t3525329789_TisIl2CppObject_TisPathOptions_t1144419237_m1837097359_gshared)(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeValues<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,T2,System.Single)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeValues_TisVector3_t3525329789_TisIl2CppObject_TisPathOptions_t1144419237_m3490814936_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t1820784502 * ___t0, Il2CppObject * ___newStartValue1, Il2CppObject * ___newEndValue2, float ___newDuration3, const MethodInfo* method);
#define Tweener_DoChangeValues_TisVector3_t3525329789_TisIl2CppObject_TisPathOptions_t1144419237_m3490814936(__this /* static, unused */, ___t0, ___newStartValue1, ___newEndValue2, ___newDuration3, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1820784502 *, Il2CppObject *, Il2CppObject *, float, const MethodInfo*))Tweener_DoChangeValues_TisVector3_t3525329789_TisIl2CppObject_TisPathOptions_t1144419237_m3490814936_gshared)(__this /* static, unused */, ___t0, ___newStartValue1, ___newEndValue2, ___newDuration3, method)
// !!0 System.Activator::CreateInstance<DG.Tweening.Plugins.Options.PathOptions>()
extern "C"  PathOptions_t1144419237  Activator_CreateInstance_TisPathOptions_t1144419237_m3358356821_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Activator_CreateInstance_TisPathOptions_t1144419237_m3358356821(__this /* static, unused */, method) ((  PathOptions_t1144419237  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Activator_CreateInstance_TisPathOptions_t1144419237_m3358356821_gshared)(__this /* static, unused */, method)
// System.Single DG.Tweening.Tweener::DoUpdateDelay<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
extern "C"  float Tweener_DoUpdateDelay_TisVector3_t3525329789_TisIl2CppObject_TisPathOptions_t1144419237_m2510062175_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t1820784502 * ___t0, float ___elapsed1, const MethodInfo* method);
#define Tweener_DoUpdateDelay_TisVector3_t3525329789_TisIl2CppObject_TisPathOptions_t1144419237_m2510062175(__this /* static, unused */, ___t0, ___elapsed1, method) ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1820784502 *, float, const MethodInfo*))Tweener_DoUpdateDelay_TisVector3_t3525329789_TisIl2CppObject_TisPathOptions_t1144419237_m2510062175_gshared)(__this /* static, unused */, ___t0, ___elapsed1, method)
// System.Boolean DG.Tweening.Tweener::DoStartup<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern "C"  bool Tweener_DoStartup_TisVector3_t3525329789_TisIl2CppObject_TisPathOptions_t1144419237_m2063002891_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t1820784502 * ___t0, const MethodInfo* method);
#define Tweener_DoStartup_TisVector3_t3525329789_TisIl2CppObject_TisPathOptions_t1144419237_m2063002891(__this /* static, unused */, ___t0, method) ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1820784502 *, const MethodInfo*))Tweener_DoStartup_TisVector3_t3525329789_TisIl2CppObject_TisPathOptions_t1144419237_m2063002891_gshared)(__this /* static, unused */, ___t0, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeStartValue<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeStartValue_TisVector3_t3525329789_TisIl2CppObject_TisVector3ArrayOptions_t939984481_m4108988483_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t1616349746 * ___t0, Il2CppObject * ___newStartValue1, float ___newDuration2, const MethodInfo* method);
#define Tweener_DoChangeStartValue_TisVector3_t3525329789_TisIl2CppObject_TisVector3ArrayOptions_t939984481_m4108988483(__this /* static, unused */, ___t0, ___newStartValue1, ___newDuration2, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1616349746 *, Il2CppObject *, float, const MethodInfo*))Tweener_DoChangeStartValue_TisVector3_t3525329789_TisIl2CppObject_TisVector3ArrayOptions_t939984481_m4108988483_gshared)(__this /* static, unused */, ___t0, ___newStartValue1, ___newDuration2, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeEndValue<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single,System.Boolean)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeEndValue_TisVector3_t3525329789_TisIl2CppObject_TisVector3ArrayOptions_t939984481_m2452268371_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t1616349746 * ___t0, Il2CppObject * ___newEndValue1, float ___newDuration2, bool ___snapStartValue3, const MethodInfo* method);
#define Tweener_DoChangeEndValue_TisVector3_t3525329789_TisIl2CppObject_TisVector3ArrayOptions_t939984481_m2452268371(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1616349746 *, Il2CppObject *, float, bool, const MethodInfo*))Tweener_DoChangeEndValue_TisVector3_t3525329789_TisIl2CppObject_TisVector3ArrayOptions_t939984481_m2452268371_gshared)(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeValues<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,T2,System.Single)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeValues_TisVector3_t3525329789_TisIl2CppObject_TisVector3ArrayOptions_t939984481_m707359132_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t1616349746 * ___t0, Il2CppObject * ___newStartValue1, Il2CppObject * ___newEndValue2, float ___newDuration3, const MethodInfo* method);
#define Tweener_DoChangeValues_TisVector3_t3525329789_TisIl2CppObject_TisVector3ArrayOptions_t939984481_m707359132(__this /* static, unused */, ___t0, ___newStartValue1, ___newEndValue2, ___newDuration3, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1616349746 *, Il2CppObject *, Il2CppObject *, float, const MethodInfo*))Tweener_DoChangeValues_TisVector3_t3525329789_TisIl2CppObject_TisVector3ArrayOptions_t939984481_m707359132_gshared)(__this /* static, unused */, ___t0, ___newStartValue1, ___newEndValue2, ___newDuration3, method)
// !!0 System.Activator::CreateInstance<DG.Tweening.Plugins.Options.Vector3ArrayOptions>()
extern "C"  Vector3ArrayOptions_t939984481  Activator_CreateInstance_TisVector3ArrayOptions_t939984481_m3771587097_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Activator_CreateInstance_TisVector3ArrayOptions_t939984481_m3771587097(__this /* static, unused */, method) ((  Vector3ArrayOptions_t939984481  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Activator_CreateInstance_TisVector3ArrayOptions_t939984481_m3771587097_gshared)(__this /* static, unused */, method)
// System.Single DG.Tweening.Tweener::DoUpdateDelay<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
extern "C"  float Tweener_DoUpdateDelay_TisVector3_t3525329789_TisIl2CppObject_TisVector3ArrayOptions_t939984481_m3034629155_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t1616349746 * ___t0, float ___elapsed1, const MethodInfo* method);
#define Tweener_DoUpdateDelay_TisVector3_t3525329789_TisIl2CppObject_TisVector3ArrayOptions_t939984481_m3034629155(__this /* static, unused */, ___t0, ___elapsed1, method) ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1616349746 *, float, const MethodInfo*))Tweener_DoUpdateDelay_TisVector3_t3525329789_TisIl2CppObject_TisVector3ArrayOptions_t939984481_m3034629155_gshared)(__this /* static, unused */, ___t0, ___elapsed1, method)
// System.Boolean DG.Tweening.Tweener::DoStartup<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern "C"  bool Tweener_DoStartup_TisVector3_t3525329789_TisIl2CppObject_TisVector3ArrayOptions_t939984481_m3360109775_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t1616349746 * ___t0, const MethodInfo* method);
#define Tweener_DoStartup_TisVector3_t3525329789_TisIl2CppObject_TisVector3ArrayOptions_t939984481_m3360109775(__this /* static, unused */, ___t0, method) ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1616349746 *, const MethodInfo*))Tweener_DoStartup_TisVector3_t3525329789_TisIl2CppObject_TisVector3ArrayOptions_t939984481_m3360109775_gshared)(__this /* static, unused */, ___t0, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeStartValue<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeStartValue_TisVector3_t3525329789_TisVector3_t3525329789_TisVectorOptions_t3308462279_m215886980_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t2905908171 * ___t0, Vector3_t3525329789  ___newStartValue1, float ___newDuration2, const MethodInfo* method);
#define Tweener_DoChangeStartValue_TisVector3_t3525329789_TisVector3_t3525329789_TisVectorOptions_t3308462279_m215886980(__this /* static, unused */, ___t0, ___newStartValue1, ___newDuration2, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2905908171 *, Vector3_t3525329789 , float, const MethodInfo*))Tweener_DoChangeStartValue_TisVector3_t3525329789_TisVector3_t3525329789_TisVectorOptions_t3308462279_m215886980_gshared)(__this /* static, unused */, ___t0, ___newStartValue1, ___newDuration2, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeEndValue<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single,System.Boolean)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeEndValue_TisVector3_t3525329789_TisVector3_t3525329789_TisVectorOptions_t3308462279_m306740978_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t2905908171 * ___t0, Vector3_t3525329789  ___newEndValue1, float ___newDuration2, bool ___snapStartValue3, const MethodInfo* method);
#define Tweener_DoChangeEndValue_TisVector3_t3525329789_TisVector3_t3525329789_TisVectorOptions_t3308462279_m306740978(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2905908171 *, Vector3_t3525329789 , float, bool, const MethodInfo*))Tweener_DoChangeEndValue_TisVector3_t3525329789_TisVector3_t3525329789_TisVectorOptions_t3308462279_m306740978_gshared)(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeValues<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,T2,System.Single)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeValues_TisVector3_t3525329789_TisVector3_t3525329789_TisVectorOptions_t3308462279_m2617344443_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t2905908171 * ___t0, Vector3_t3525329789  ___newStartValue1, Vector3_t3525329789  ___newEndValue2, float ___newDuration3, const MethodInfo* method);
#define Tweener_DoChangeValues_TisVector3_t3525329789_TisVector3_t3525329789_TisVectorOptions_t3308462279_m2617344443(__this /* static, unused */, ___t0, ___newStartValue1, ___newEndValue2, ___newDuration3, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2905908171 *, Vector3_t3525329789 , Vector3_t3525329789 , float, const MethodInfo*))Tweener_DoChangeValues_TisVector3_t3525329789_TisVector3_t3525329789_TisVectorOptions_t3308462279_m2617344443_gshared)(__this /* static, unused */, ___t0, ___newStartValue1, ___newEndValue2, ___newDuration3, method)
// System.Single DG.Tweening.Tweener::DoUpdateDelay<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
extern "C"  float Tweener_DoUpdateDelay_TisVector3_t3525329789_TisVector3_t3525329789_TisVectorOptions_t3308462279_m2912098178_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t2905908171 * ___t0, float ___elapsed1, const MethodInfo* method);
#define Tweener_DoUpdateDelay_TisVector3_t3525329789_TisVector3_t3525329789_TisVectorOptions_t3308462279_m2912098178(__this /* static, unused */, ___t0, ___elapsed1, method) ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2905908171 *, float, const MethodInfo*))Tweener_DoUpdateDelay_TisVector3_t3525329789_TisVector3_t3525329789_TisVectorOptions_t3308462279_m2912098178_gshared)(__this /* static, unused */, ___t0, ___elapsed1, method)
// System.Boolean DG.Tweening.Tweener::DoStartup<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern "C"  bool Tweener_DoStartup_TisVector3_t3525329789_TisVector3_t3525329789_TisVectorOptions_t3308462279_m1023101550_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t2905908171 * ___t0, const MethodInfo* method);
#define Tweener_DoStartup_TisVector3_t3525329789_TisVector3_t3525329789_TisVectorOptions_t3308462279_m1023101550(__this /* static, unused */, ___t0, method) ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2905908171 *, const MethodInfo*))Tweener_DoStartup_TisVector3_t3525329789_TisVector3_t3525329789_TisVectorOptions_t3308462279_m1023101550_gshared)(__this /* static, unused */, ___t0, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeStartValue<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeStartValue_TisVector4_t3525329790_TisVector4_t3525329790_TisVectorOptions_t3308462279_m3408676358_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t763702783 * ___t0, Vector4_t3525329790  ___newStartValue1, float ___newDuration2, const MethodInfo* method);
#define Tweener_DoChangeStartValue_TisVector4_t3525329790_TisVector4_t3525329790_TisVectorOptions_t3308462279_m3408676358(__this /* static, unused */, ___t0, ___newStartValue1, ___newDuration2, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t763702783 *, Vector4_t3525329790 , float, const MethodInfo*))Tweener_DoChangeStartValue_TisVector4_t3525329790_TisVector4_t3525329790_TisVectorOptions_t3308462279_m3408676358_gshared)(__this /* static, unused */, ___t0, ___newStartValue1, ___newDuration2, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeEndValue<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single,System.Boolean)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeEndValue_TisVector4_t3525329790_TisVector4_t3525329790_TisVectorOptions_t3308462279_m4176998704_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t763702783 * ___t0, Vector4_t3525329790  ___newEndValue1, float ___newDuration2, bool ___snapStartValue3, const MethodInfo* method);
#define Tweener_DoChangeEndValue_TisVector4_t3525329790_TisVector4_t3525329790_TisVectorOptions_t3308462279_m4176998704(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t763702783 *, Vector4_t3525329790 , float, bool, const MethodInfo*))Tweener_DoChangeEndValue_TisVector4_t3525329790_TisVector4_t3525329790_TisVectorOptions_t3308462279_m4176998704_gshared)(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeValues<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,T2,System.Single)
extern "C"  Tweener_t1766303790 * Tweener_DoChangeValues_TisVector4_t3525329790_TisVector4_t3525329790_TisVectorOptions_t3308462279_m2659967225_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t763702783 * ___t0, Vector4_t3525329790  ___newStartValue1, Vector4_t3525329790  ___newEndValue2, float ___newDuration3, const MethodInfo* method);
#define Tweener_DoChangeValues_TisVector4_t3525329790_TisVector4_t3525329790_TisVectorOptions_t3308462279_m2659967225(__this /* static, unused */, ___t0, ___newStartValue1, ___newEndValue2, ___newDuration3, method) ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t763702783 *, Vector4_t3525329790 , Vector4_t3525329790 , float, const MethodInfo*))Tweener_DoChangeValues_TisVector4_t3525329790_TisVector4_t3525329790_TisVectorOptions_t3308462279_m2659967225_gshared)(__this /* static, unused */, ___t0, ___newStartValue1, ___newEndValue2, ___newDuration3, method)
// System.Single DG.Tweening.Tweener::DoUpdateDelay<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
extern "C"  float Tweener_DoUpdateDelay_TisVector4_t3525329790_TisVector4_t3525329790_TisVectorOptions_t3308462279_m2184002880_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t763702783 * ___t0, float ___elapsed1, const MethodInfo* method);
#define Tweener_DoUpdateDelay_TisVector4_t3525329790_TisVector4_t3525329790_TisVectorOptions_t3308462279_m2184002880(__this /* static, unused */, ___t0, ___elapsed1, method) ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t763702783 *, float, const MethodInfo*))Tweener_DoUpdateDelay_TisVector4_t3525329790_TisVector4_t3525329790_TisVectorOptions_t3308462279_m2184002880_gshared)(__this /* static, unused */, ___t0, ___elapsed1, method)
// System.Boolean DG.Tweening.Tweener::DoStartup<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern "C"  bool Tweener_DoStartup_TisVector4_t3525329790_TisVector4_t3525329790_TisVectorOptions_t3308462279_m2475898028_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t763702783 * ___t0, const MethodInfo* method);
#define Tweener_DoStartup_TisVector4_t3525329790_TisVector4_t3525329790_TisVectorOptions_t3308462279_m2475898028(__this /* static, unused */, ___t0, method) ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t763702783 *, const MethodInfo*))Tweener_DoStartup_TisVector4_t3525329790_TisVector4_t3525329790_TisVectorOptions_t3308462279_m2475898028_gshared)(__this /* static, unused */, ___t0, method)
// System.String Facebook.Unity.CallbackManager::AddFacebookDelegate<System.Object>(Facebook.Unity.FacebookDelegate`1<!!0>)
extern "C"  String_t* CallbackManager_AddFacebookDelegate_TisIl2CppObject_m3982619062_gshared (CallbackManager_t1943358823 * __this, FacebookDelegate_1_t1473468476 * p0, const MethodInfo* method);
#define CallbackManager_AddFacebookDelegate_TisIl2CppObject_m3982619062(__this, p0, method) ((  String_t* (*) (CallbackManager_t1943358823 *, FacebookDelegate_1_t1473468476 *, const MethodInfo*))CallbackManager_AddFacebookDelegate_TisIl2CppObject_m3982619062_gshared)(__this, p0, method)
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared (GameObject_t4012695102 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2447772384(__this, method) ((  Il2CppObject * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.RectTransform>()
#define GameObject_GetComponent_TisRectTransform_t3317474837_m406276429(__this, method) ((  RectTransform_t3317474837 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<Tacticsoft.TableViewCell>()
#define GameObject_GetComponent_TisTableViewCell_t776419755_m1986117684(__this, method) ((  TableViewCell_t776419755 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m3133387403_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define Object_Instantiate_TisIl2CppObject_m3133387403(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m3133387403_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0)
#define Object_Instantiate_TisGameObject_t4012695102_m3917608929(__this /* static, unused */, p0, method) ((  GameObject_t4012695102 * (*) (Il2CppObject * /* static, unused */, GameObject_t4012695102 *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m3133387403_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m267839954_gshared (Component_t2126946602 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m267839954(__this, method) ((  Il2CppObject * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CellReactiveApi/<WhateverBind>c__AnonStoreyFE`1<System.DateTime>::.ctor()
extern "C"  void U3CWhateverBindU3Ec__AnonStoreyFE_1__ctor_m4104613631_gshared (U3CWhateverBindU3Ec__AnonStoreyFE_1_t560459497 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CellReactiveApi/<WhateverBind>c__AnonStoreyFE`1<System.DateTime>::<>m__174(T)
extern "C"  void U3CWhateverBindU3Ec__AnonStoreyFE_1_U3CU3Em__174_m3529454134_gshared (U3CWhateverBindU3Ec__AnonStoreyFE_1_t560459497 * __this, DateTime_t339033936  ___val0, const MethodInfo* method)
{
	{
		ConnectionCollector_t444796719 * L_0 = (ConnectionCollector_t444796719 *)__this->get_collector_0();
		NullCheck((ConnectionCollector_t444796719 *)L_0);
		ConnectionCollector_DisconnectAll_m662901599((ConnectionCollector_t444796719 *)L_0, /*hidden argument*/NULL);
		Action_2_t1818238909 * L_1 = (Action_2_t1818238909 *)__this->get_action_1();
		DateTime_t339033936  L_2 = ___val0;
		ConnectionCollector_t444796719 * L_3 = (ConnectionCollector_t444796719 *)__this->get_collector_0();
		NullCheck((Action_2_t1818238909 *)L_1);
		((  void (*) (Action_2_t1818238909 *, DateTime_t339033936 , ConnectionCollector_t444796719 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Action_2_t1818238909 *)L_1, (DateTime_t339033936 )L_2, (ConnectionCollector_t444796719 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void CellReactiveApi/<WhateverBind>c__AnonStoreyFE`1<System.Object>::.ctor()
extern "C"  void U3CWhateverBindU3Ec__AnonStoreyFE_1__ctor_m3541360995_gshared (U3CWhateverBindU3Ec__AnonStoreyFE_1_t1058531981 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CellReactiveApi/<WhateverBind>c__AnonStoreyFE`1<System.Object>::<>m__174(T)
extern "C"  void U3CWhateverBindU3Ec__AnonStoreyFE_1_U3CU3Em__174_m4265923226_gshared (U3CWhateverBindU3Ec__AnonStoreyFE_1_t1058531981 * __this, Il2CppObject * ___val0, const MethodInfo* method)
{
	{
		ConnectionCollector_t444796719 * L_0 = (ConnectionCollector_t444796719 *)__this->get_collector_0();
		NullCheck((ConnectionCollector_t444796719 *)L_0);
		ConnectionCollector_DisconnectAll_m662901599((ConnectionCollector_t444796719 *)L_0, /*hidden argument*/NULL);
		Action_2_t3713150217 * L_1 = (Action_2_t3713150217 *)__this->get_action_1();
		Il2CppObject * L_2 = ___val0;
		ConnectionCollector_t444796719 * L_3 = (ConnectionCollector_t444796719 *)__this->get_collector_0();
		NullCheck((Action_2_t3713150217 *)L_1);
		((  void (*) (Action_2_t3713150217 *, Il2CppObject *, ConnectionCollector_t444796719 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Action_2_t3713150217 *)L_1, (Il2CppObject *)L_2, (ConnectionCollector_t444796719 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void CellReactiveApi/<When>c__AnonStorey10A`1/<When>c__AnonStorey10B`1<System.Boolean>::.ctor()
extern "C"  void U3CWhenU3Ec__AnonStorey10B_1__ctor_m2201070194_gshared (U3CWhenU3Ec__AnonStorey10B_1_t427924399 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CellReactiveApi/<When>c__AnonStorey10A`1/<When>c__AnonStorey10B`1<System.Boolean>::<>m__195(T)
extern "C"  void U3CWhenU3Ec__AnonStorey10B_1_U3CU3Em__195_m1371873162_gshared (U3CWhenU3Ec__AnonStorey10B_1_t427924399 * __this, bool ___val0, const MethodInfo* method)
{
	{
		U3CWhenU3Ec__AnonStorey10A_1_t872949398 * L_0 = (U3CWhenU3Ec__AnonStorey10A_1_t872949398 *)__this->get_U3CU3Ef__refU24266_1();
		NullCheck(L_0);
		Func_2_t1008118516 * L_1 = (Func_2_t1008118516 *)L_0->get_filter_1();
		bool L_2 = ___val0;
		NullCheck((Func_2_t1008118516 *)L_1);
		bool L_3 = ((  bool (*) (Func_2_t1008118516 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Func_2_t1008118516 *)L_1, (bool)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		if (L_3)
		{
			goto IL_0017;
		}
	}
	{
		return;
	}

IL_0017:
	{
		Action_t437523947 * L_4 = (Action_t437523947 *)__this->get_reaction_0();
		NullCheck((Action_t437523947 *)L_4);
		Action_Invoke_m1445970038((Action_t437523947 *)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CellReactiveApi/<When>c__AnonStorey10A`1/<When>c__AnonStorey10B`1<System.Object>::.ctor()
extern "C"  void U3CWhenU3Ec__AnonStorey10B_1__ctor_m3102183327_gshared (U3CWhenU3Ec__AnonStorey10B_1_t1054025478 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CellReactiveApi/<When>c__AnonStorey10A`1/<When>c__AnonStorey10B`1<System.Object>::<>m__195(T)
extern "C"  void U3CWhenU3Ec__AnonStorey10B_1_U3CU3Em__195_m1115334199_gshared (U3CWhenU3Ec__AnonStorey10B_1_t1054025478 * __this, Il2CppObject * ___val0, const MethodInfo* method)
{
	{
		U3CWhenU3Ec__AnonStorey10A_1_t1499050477 * L_0 = (U3CWhenU3Ec__AnonStorey10A_1_t1499050477 *)__this->get_U3CU3Ef__refU24266_1();
		NullCheck(L_0);
		Func_2_t1509682273 * L_1 = (Func_2_t1509682273 *)L_0->get_filter_1();
		Il2CppObject * L_2 = ___val0;
		NullCheck((Func_2_t1509682273 *)L_1);
		bool L_3 = ((  bool (*) (Func_2_t1509682273 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Func_2_t1509682273 *)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		if (L_3)
		{
			goto IL_0017;
		}
	}
	{
		return;
	}

IL_0017:
	{
		Action_t437523947 * L_4 = (Action_t437523947 *)__this->get_reaction_0();
		NullCheck((Action_t437523947 *)L_4);
		Action_Invoke_m1445970038((Action_t437523947 *)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CellReactiveApi/<When>c__AnonStorey10A`1<System.Boolean>::.ctor()
extern "C"  void U3CWhenU3Ec__AnonStorey10A_1__ctor_m3575667422_gshared (U3CWhenU3Ec__AnonStorey10A_1_t872949398 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.IDisposable CellReactiveApi/<When>c__AnonStorey10A`1<System.Boolean>::<>m__17D(System.Action,Priority)
extern "C"  Il2CppObject * U3CWhenU3Ec__AnonStorey10A_1_U3CU3Em__17D_m1390952753_gshared (U3CWhenU3Ec__AnonStorey10A_1_t872949398 * __this, Action_t437523947 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	U3CWhenU3Ec__AnonStorey10B_1_t427924399 * V_0 = NULL;
	{
		U3CWhenU3Ec__AnonStorey10B_1_t427924399 * L_0 = (U3CWhenU3Ec__AnonStorey10B_1_t427924399 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CWhenU3Ec__AnonStorey10B_1_t427924399 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CWhenU3Ec__AnonStorey10B_1_t427924399 *)L_0;
		U3CWhenU3Ec__AnonStorey10B_1_t427924399 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU24266_1(__this);
		U3CWhenU3Ec__AnonStorey10B_1_t427924399 * L_2 = V_0;
		Action_t437523947 * L_3 = ___reaction0;
		NullCheck(L_2);
		L_2->set_reaction_0(L_3);
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_cell_0();
		U3CWhenU3Ec__AnonStorey10B_1_t427924399 * L_5 = V_0;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_1_t359458046 * L_7 = (Action_1_t359458046 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (Action_1_t359458046 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_7, (Il2CppObject *)L_5, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		int32_t L_8 = ___p1;
		NullCheck((Il2CppObject*)L_4);
		Il2CppObject * L_9 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t359458046 *, int32_t >::Invoke(0 /* System.IDisposable ICell`1<System.Boolean>::Bind(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_4, (Action_1_t359458046 *)L_7, (int32_t)L_8);
		return L_9;
	}
}
// System.Void CellReactiveApi/<When>c__AnonStorey10A`1<System.Object>::.ctor()
extern "C"  void U3CWhenU3Ec__AnonStorey10A_1__ctor_m929767859_gshared (U3CWhenU3Ec__AnonStorey10A_1_t1499050477 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.IDisposable CellReactiveApi/<When>c__AnonStorey10A`1<System.Object>::<>m__17D(System.Action,Priority)
extern "C"  Il2CppObject * U3CWhenU3Ec__AnonStorey10A_1_U3CU3Em__17D_m4012637302_gshared (U3CWhenU3Ec__AnonStorey10A_1_t1499050477 * __this, Action_t437523947 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	U3CWhenU3Ec__AnonStorey10B_1_t1054025478 * V_0 = NULL;
	{
		U3CWhenU3Ec__AnonStorey10B_1_t1054025478 * L_0 = (U3CWhenU3Ec__AnonStorey10B_1_t1054025478 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CWhenU3Ec__AnonStorey10B_1_t1054025478 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CWhenU3Ec__AnonStorey10B_1_t1054025478 *)L_0;
		U3CWhenU3Ec__AnonStorey10B_1_t1054025478 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU24266_1(__this);
		U3CWhenU3Ec__AnonStorey10B_1_t1054025478 * L_2 = V_0;
		Action_t437523947 * L_3 = ___reaction0;
		NullCheck(L_2);
		L_2->set_reaction_0(L_3);
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_cell_0();
		U3CWhenU3Ec__AnonStorey10B_1_t1054025478 * L_5 = V_0;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_1_t985559125 * L_7 = (Action_1_t985559125 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_7, (Il2CppObject *)L_5, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		int32_t L_8 = ___p1;
		NullCheck((Il2CppObject*)L_4);
		Il2CppObject * L_9 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t985559125 *, int32_t >::Invoke(0 /* System.IDisposable ICell`1<System.Object>::Bind(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_4, (Action_1_t985559125 *)L_7, (int32_t)L_8);
		return L_9;
	}
}
// System.Void CellReactiveApi/<WhenOnce>c__AnonStorey107`1/<WhenOnce>c__AnonStorey108`1<System.Object>::.ctor()
extern "C"  void U3CWhenOnceU3Ec__AnonStorey108_1__ctor_m103729727_gshared (U3CWhenOnceU3Ec__AnonStorey108_1_t2286074227 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CellReactiveApi/<WhenOnce>c__AnonStorey107`1/<WhenOnce>c__AnonStorey109`1<System.Object>::.ctor()
extern "C"  void U3CWhenOnceU3Ec__AnonStorey109_1__ctor_m1065343744_gshared (U3CWhenOnceU3Ec__AnonStorey109_1_t1841049228 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CellReactiveApi/<WhenOnce>c__AnonStorey107`1/<WhenOnce>c__AnonStorey109`1<System.Object>::<>m__194(T)
extern "C"  void U3CWhenOnceU3Ec__AnonStorey109_1_U3CU3Em__194_m3813401785_gshared (U3CWhenOnceU3Ec__AnonStorey109_1_t1841049228 * __this, Il2CppObject * ___val0, const MethodInfo* method)
{
	{
		U3CWhenOnceU3Ec__AnonStorey107_1_t2731099226 * L_0 = (U3CWhenOnceU3Ec__AnonStorey107_1_t2731099226 *)__this->get_U3CU3Ef__refU24263_1();
		NullCheck(L_0);
		Func_2_t1509682273 * L_1 = (Func_2_t1509682273 *)L_0->get_filter_0();
		Il2CppObject * L_2 = ___val0;
		NullCheck((Func_2_t1509682273 *)L_1);
		bool L_3 = ((  bool (*) (Func_2_t1509682273 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Func_2_t1509682273 *)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		if (L_3)
		{
			goto IL_0017;
		}
	}
	{
		return;
	}

IL_0017:
	{
		U3CWhenOnceU3Ec__AnonStorey108_1_t2286074227 * L_4 = (U3CWhenOnceU3Ec__AnonStorey108_1_t2286074227 *)__this->get_U3CU3Ef__refU24264_2();
		NullCheck(L_4);
		Action_t437523947 * L_5 = (Action_t437523947 *)L_4->get_reaction_0();
		NullCheck((Action_t437523947 *)L_5);
		Action_Invoke_m1445970038((Action_t437523947 *)L_5, /*hidden argument*/NULL);
		SingleAssignmentDisposable_t1832432170 * L_6 = (SingleAssignmentDisposable_t1832432170 *)__this->get_disp_0();
		NullCheck((SingleAssignmentDisposable_t1832432170 *)L_6);
		VirtActionInvoker0::Invoke(4 /* System.Void CellUtils.SingleAssignmentDisposable::Dispose() */, (SingleAssignmentDisposable_t1832432170 *)L_6);
		return;
	}
}
// System.Void CellReactiveApi/<WhenOnce>c__AnonStorey107`1<System.Object>::.ctor()
extern "C"  void U3CWhenOnceU3Ec__AnonStorey107_1__ctor_m1862142952_gshared (U3CWhenOnceU3Ec__AnonStorey107_1_t2731099226 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.IDisposable CellReactiveApi/<WhenOnce>c__AnonStorey107`1<System.Object>::<>m__17C(System.Action,Priority)
extern Il2CppClass* EmptyDisposable_t1512564738_il2cpp_TypeInfo_var;
extern Il2CppClass* SingleAssignmentDisposable_t1832432170_il2cpp_TypeInfo_var;
extern const uint32_t U3CWhenOnceU3Ec__AnonStorey107_1_U3CU3Em__17C_m2009263200_MetadataUsageId;
extern "C"  Il2CppObject * U3CWhenOnceU3Ec__AnonStorey107_1_U3CU3Em__17C_m2009263200_gshared (U3CWhenOnceU3Ec__AnonStorey107_1_t2731099226 * __this, Action_t437523947 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CWhenOnceU3Ec__AnonStorey107_1_U3CU3Em__17C_m2009263200_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CWhenOnceU3Ec__AnonStorey108_1_t2286074227 * V_0 = NULL;
	U3CWhenOnceU3Ec__AnonStorey109_1_t1841049228 * V_1 = NULL;
	{
		U3CWhenOnceU3Ec__AnonStorey108_1_t2286074227 * L_0 = (U3CWhenOnceU3Ec__AnonStorey108_1_t2286074227 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CWhenOnceU3Ec__AnonStorey108_1_t2286074227 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CWhenOnceU3Ec__AnonStorey108_1_t2286074227 *)L_0;
		U3CWhenOnceU3Ec__AnonStorey108_1_t2286074227 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU24263_1(__this);
		U3CWhenOnceU3Ec__AnonStorey108_1_t2286074227 * L_2 = V_0;
		Action_t437523947 * L_3 = ___reaction0;
		NullCheck(L_2);
		L_2->set_reaction_0(L_3);
		Func_2_t1509682273 * L_4 = (Func_2_t1509682273 *)__this->get_filter_0();
		Il2CppObject* L_5 = (Il2CppObject*)__this->get_cell_1();
		NullCheck((Il2CppObject*)L_5);
		Il2CppObject * L_6 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* T ICell`1<System.Object>::get_value() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_5);
		NullCheck((Func_2_t1509682273 *)L_4);
		bool L_7 = ((  bool (*) (Func_2_t1509682273 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Func_2_t1509682273 *)L_4, (Il2CppObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		if (!L_7)
		{
			goto IL_0040;
		}
	}
	{
		U3CWhenOnceU3Ec__AnonStorey108_1_t2286074227 * L_8 = V_0;
		NullCheck(L_8);
		Action_t437523947 * L_9 = (Action_t437523947 *)L_8->get_reaction_0();
		NullCheck((Action_t437523947 *)L_9);
		Action_Invoke_m1445970038((Action_t437523947 *)L_9, /*hidden argument*/NULL);
		EmptyDisposable_t1512564738 * L_10 = (EmptyDisposable_t1512564738 *)il2cpp_codegen_object_new(EmptyDisposable_t1512564738_il2cpp_TypeInfo_var);
		EmptyDisposable__ctor_m791146077(L_10, /*hidden argument*/NULL);
		return L_10;
	}

IL_0040:
	{
		U3CWhenOnceU3Ec__AnonStorey109_1_t1841049228 * L_11 = (U3CWhenOnceU3Ec__AnonStorey109_1_t1841049228 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (U3CWhenOnceU3Ec__AnonStorey109_1_t1841049228 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_1 = (U3CWhenOnceU3Ec__AnonStorey109_1_t1841049228 *)L_11;
		U3CWhenOnceU3Ec__AnonStorey109_1_t1841049228 * L_12 = V_1;
		NullCheck(L_12);
		L_12->set_U3CU3Ef__refU24263_1(__this);
		U3CWhenOnceU3Ec__AnonStorey109_1_t1841049228 * L_13 = V_1;
		U3CWhenOnceU3Ec__AnonStorey108_1_t2286074227 * L_14 = V_0;
		NullCheck(L_13);
		L_13->set_U3CU3Ef__refU24264_2(L_14);
		U3CWhenOnceU3Ec__AnonStorey109_1_t1841049228 * L_15 = V_1;
		SingleAssignmentDisposable_t1832432170 * L_16 = (SingleAssignmentDisposable_t1832432170 *)il2cpp_codegen_object_new(SingleAssignmentDisposable_t1832432170_il2cpp_TypeInfo_var);
		SingleAssignmentDisposable__ctor_m2805131143(L_16, /*hidden argument*/NULL);
		NullCheck(L_15);
		L_15->set_disp_0(L_16);
		U3CWhenOnceU3Ec__AnonStorey109_1_t1841049228 * L_17 = V_1;
		NullCheck(L_17);
		SingleAssignmentDisposable_t1832432170 * L_18 = (SingleAssignmentDisposable_t1832432170 *)L_17->get_disp_0();
		Il2CppObject* L_19 = (Il2CppObject*)__this->get_cell_1();
		U3CWhenOnceU3Ec__AnonStorey109_1_t1841049228 * L_20 = V_1;
		IntPtr_t L_21;
		L_21.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		Action_1_t985559125 * L_22 = (Action_1_t985559125 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_22, (Il2CppObject *)L_20, (IntPtr_t)L_21, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		int32_t L_23 = ___p1;
		NullCheck((Il2CppObject*)L_19);
		Il2CppObject * L_24 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t985559125 *, int32_t >::Invoke(1 /* System.IDisposable ICell`1<System.Object>::ListenUpdates(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_19, (Action_1_t985559125 *)L_22, (int32_t)L_23);
		NullCheck((SingleAssignmentDisposable_t1832432170 *)L_18);
		SingleAssignmentDisposable_set_Disposable_m1918433680((SingleAssignmentDisposable_t1832432170 *)L_18, (Il2CppObject *)L_24, /*hidden argument*/NULL);
		U3CWhenOnceU3Ec__AnonStorey109_1_t1841049228 * L_25 = V_1;
		NullCheck(L_25);
		SingleAssignmentDisposable_t1832432170 * L_26 = (SingleAssignmentDisposable_t1832432170 *)L_25->get_disp_0();
		return L_26;
	}
}
// System.Void CellReactiveApi/<WhenSatisfy>c__AnonStorey10C`1/<WhenSatisfy>c__AnonStorey10D`1<System.Object>::.ctor()
extern "C"  void U3CWhenSatisfyU3Ec__AnonStorey10D_1__ctor_m3774480543_gshared (U3CWhenSatisfyU3Ec__AnonStorey10D_1_t3834446745 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CellReactiveApi/<WhenSatisfy>c__AnonStorey10C`1/<WhenSatisfy>c__AnonStorey10D`1<System.Object>::<>m__196(T)
extern "C"  void U3CWhenSatisfyU3Ec__AnonStorey10D_1_U3CU3Em__196_m1240271766_gshared (U3CWhenSatisfyU3Ec__AnonStorey10D_1_t3834446745 * __this, Il2CppObject * ___val0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		U3CWhenSatisfyU3Ec__AnonStorey10C_1_t4279471744 * L_0 = (U3CWhenSatisfyU3Ec__AnonStorey10C_1_t4279471744 *)__this->get_U3CU3Ef__refU24268_2();
		NullCheck(L_0);
		Func_2_t1509682273 * L_1 = (Func_2_t1509682273 *)L_0->get_filter_1();
		Il2CppObject * L_2 = ___val0;
		NullCheck((Func_2_t1509682273 *)L_1);
		bool L_3 = ((  bool (*) (Func_2_t1509682273 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Func_2_t1509682273 *)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		V_0 = (bool)L_3;
		DisposableContainer_1_t2879821764 * L_4 = (DisposableContainer_1_t2879821764 *)__this->get_disp_0();
		NullCheck(L_4);
		bool L_5 = (bool)L_4->get_value_0();
		if (L_5)
		{
			goto IL_0033;
		}
	}
	{
		bool L_6 = V_0;
		if (!L_6)
		{
			goto IL_0033;
		}
	}
	{
		Action_t437523947 * L_7 = (Action_t437523947 *)__this->get_reaction_1();
		NullCheck((Action_t437523947 *)L_7);
		Action_Invoke_m1445970038((Action_t437523947 *)L_7, /*hidden argument*/NULL);
	}

IL_0033:
	{
		DisposableContainer_1_t2879821764 * L_8 = (DisposableContainer_1_t2879821764 *)__this->get_disp_0();
		bool L_9 = V_0;
		NullCheck(L_8);
		L_8->set_value_0(L_9);
		return;
	}
}
// System.Void CellReactiveApi/<WhenSatisfy>c__AnonStorey10C`1<System.Object>::.ctor()
extern "C"  void U3CWhenSatisfyU3Ec__AnonStorey10C_1__ctor_m3173308558_gshared (U3CWhenSatisfyU3Ec__AnonStorey10C_1_t4279471744 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.IDisposable CellReactiveApi/<WhenSatisfy>c__AnonStorey10C`1<System.Object>::<>m__17F(System.Action,Priority)
extern Il2CppClass* DisposableContainer_1_t2879821764_il2cpp_TypeInfo_var;
extern const MethodInfo* DisposableContainer_1__ctor_m461451184_MethodInfo_var;
extern const uint32_t U3CWhenSatisfyU3Ec__AnonStorey10C_1_U3CU3Em__17F_m2871339331_MetadataUsageId;
extern "C"  Il2CppObject * U3CWhenSatisfyU3Ec__AnonStorey10C_1_U3CU3Em__17F_m2871339331_gshared (U3CWhenSatisfyU3Ec__AnonStorey10C_1_t4279471744 * __this, Action_t437523947 * ___reaction0, int32_t ___p1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CWhenSatisfyU3Ec__AnonStorey10C_1_U3CU3Em__17F_m2871339331_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CWhenSatisfyU3Ec__AnonStorey10D_1_t3834446745 * V_0 = NULL;
	{
		U3CWhenSatisfyU3Ec__AnonStorey10D_1_t3834446745 * L_0 = (U3CWhenSatisfyU3Ec__AnonStorey10D_1_t3834446745 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CWhenSatisfyU3Ec__AnonStorey10D_1_t3834446745 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CWhenSatisfyU3Ec__AnonStorey10D_1_t3834446745 *)L_0;
		U3CWhenSatisfyU3Ec__AnonStorey10D_1_t3834446745 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU24268_2(__this);
		U3CWhenSatisfyU3Ec__AnonStorey10D_1_t3834446745 * L_2 = V_0;
		Action_t437523947 * L_3 = ___reaction0;
		NullCheck(L_2);
		L_2->set_reaction_1(L_3);
		U3CWhenSatisfyU3Ec__AnonStorey10D_1_t3834446745 * L_4 = V_0;
		DisposableContainer_1_t2879821764 * L_5 = (DisposableContainer_1_t2879821764 *)il2cpp_codegen_object_new(DisposableContainer_1_t2879821764_il2cpp_TypeInfo_var);
		DisposableContainer_1__ctor_m461451184(L_5, /*hidden argument*/DisposableContainer_1__ctor_m461451184_MethodInfo_var);
		NullCheck(L_4);
		L_4->set_disp_0(L_5);
		U3CWhenSatisfyU3Ec__AnonStorey10D_1_t3834446745 * L_6 = V_0;
		NullCheck(L_6);
		DisposableContainer_1_t2879821764 * L_7 = (DisposableContainer_1_t2879821764 *)L_6->get_disp_0();
		Il2CppObject* L_8 = (Il2CppObject*)__this->get_cell_0();
		U3CWhenSatisfyU3Ec__AnonStorey10D_1_t3834446745 * L_9 = V_0;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Action_1_t985559125 * L_11 = (Action_1_t985559125 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_11, (Il2CppObject *)L_9, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		int32_t L_12 = ___p1;
		NullCheck((Il2CppObject*)L_8);
		Il2CppObject * L_13 = InterfaceFuncInvoker2< Il2CppObject *, Action_1_t985559125 *, int32_t >::Invoke(0 /* System.IDisposable ICell`1<System.Object>::Bind(System.Action`1<T>,Priority) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_8, (Action_1_t985559125 *)L_11, (int32_t)L_12);
		NullCheck(L_7);
		L_7->set_disp_1(L_13);
		U3CWhenSatisfyU3Ec__AnonStorey10D_1_t3834446745 * L_14 = V_0;
		NullCheck(L_14);
		DisposableContainer_1_t2879821764 * L_15 = (DisposableContainer_1_t2879821764 *)L_14->get_disp_0();
		return L_15;
	}
}
// System.Void CellReactiveApi/Carrier`1<System.Object>::.ctor()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Carrier_1__ctor_m1719464920_MetadataUsageId;
extern "C"  void Carrier_1__ctor_m1719464920_gshared (Carrier_1_t2723215674 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Carrier_1__ctor_m1719464920_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_0 = V_0;
		__this->set_val_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CellReactiveApi/DisposableContainer`1<System.Boolean>::.ctor()
extern "C"  void DisposableContainer_1__ctor_m461451184_gshared (DisposableContainer_1_t2879821764 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CellReactiveApi/DisposableContainer`1<System.Boolean>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t DisposableContainer_1_Dispose_m971239405_MetadataUsageId;
extern "C"  void DisposableContainer_1_Dispose_m971239405_gshared (DisposableContainer_1_t2879821764 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposableContainer_1_Dispose_m971239405_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_disp_1();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_disp_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
		__this->set_disp_1((Il2CppObject *)NULL);
		return;
	}
}
// System.Void CellReactiveApi/DisposableContainer`1<System.Object>::.ctor()
extern "C"  void DisposableContainer_1__ctor_m3323161249_gshared (DisposableContainer_1_t3505922843 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CellReactiveApi/DisposableContainer`1<System.Object>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t DisposableContainer_1_Dispose_m2295542430_MetadataUsageId;
extern "C"  void DisposableContainer_1_Dispose_m2295542430_gshared (DisposableContainer_1_t3505922843 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DisposableContainer_1_Dispose_m2295542430_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_disp_1();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_disp_1();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
		__this->set_disp_1((Il2CppObject *)NULL);
		return;
	}
}
// System.Void CellUtils.CellJoinDisposable`1<System.Double>::.ctor()
extern "C"  void CellJoinDisposable_1__ctor_m3806792957_gshared (CellJoinDisposable_1_t3433903597 * __this, const MethodInfo* method)
{
	{
		NullCheck((ListDisposable_t2393830995 *)__this);
		ListDisposable__ctor_m4289441534((ListDisposable_t2393830995 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CellUtils.CellJoinDisposable`1<System.Int32>::.ctor()
extern "C"  void CellJoinDisposable_1__ctor_m1277547980_gshared (CellJoinDisposable_1_t1451834474 * __this, const MethodInfo* method)
{
	{
		NullCheck((ListDisposable_t2393830995 *)__this);
		ListDisposable__ctor_m4289441534((ListDisposable_t2393830995 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CellUtils.CellJoinDisposable`1<System.Object>::.ctor()
extern "C"  void CellJoinDisposable_1__ctor_m2402304747_gshared (CellJoinDisposable_1_t3736493403 * __this, const MethodInfo* method)
{
	{
		NullCheck((ListDisposable_t2393830995 *)__this);
		ListDisposable__ctor_m4289441534((ListDisposable_t2393830995 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CollectionMoveEvent`1<System.Object>::.ctor(System.Int32,System.Int32,T)
extern "C"  void CollectionMoveEvent_1__ctor_m2094255715_gshared (CollectionMoveEvent_1_t3572055381 * __this, int32_t ___oldIndex0, int32_t ___newIndex1, Il2CppObject * ___value2, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___oldIndex0;
		NullCheck((CollectionMoveEvent_1_t3572055381 *)__this);
		((  void (*) (CollectionMoveEvent_1_t3572055381 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((CollectionMoveEvent_1_t3572055381 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = ___newIndex1;
		NullCheck((CollectionMoveEvent_1_t3572055381 *)__this);
		((  void (*) (CollectionMoveEvent_1_t3572055381 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((CollectionMoveEvent_1_t3572055381 *)__this, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject * L_2 = ___value2;
		NullCheck((CollectionMoveEvent_1_t3572055381 *)__this);
		((  void (*) (CollectionMoveEvent_1_t3572055381 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((CollectionMoveEvent_1_t3572055381 *)__this, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
// System.Int32 CollectionMoveEvent`1<System.Object>::get_OldIndex()
extern "C"  int32_t CollectionMoveEvent_1_get_OldIndex_m1896787371_gshared (CollectionMoveEvent_1_t3572055381 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_U3COldIndexU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void CollectionMoveEvent`1<System.Object>::set_OldIndex(System.Int32)
extern "C"  void CollectionMoveEvent_1_set_OldIndex_m3746196962_gshared (CollectionMoveEvent_1_t3572055381 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3COldIndexU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Int32 CollectionMoveEvent`1<System.Object>::get_NewIndex()
extern "C"  int32_t CollectionMoveEvent_1_get_NewIndex_m614238226_gshared (CollectionMoveEvent_1_t3572055381 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_U3CNewIndexU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void CollectionMoveEvent`1<System.Object>::set_NewIndex(System.Int32)
extern "C"  void CollectionMoveEvent_1_set_NewIndex_m832103881_gshared (CollectionMoveEvent_1_t3572055381 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CNewIndexU3Ek__BackingField_1(L_0);
		return;
	}
}
// T CollectionMoveEvent`1<System.Object>::get_Value()
extern "C"  Il2CppObject * CollectionMoveEvent_1_get_Value_m3472123074_gshared (CollectionMoveEvent_1_t3572055381 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3CValueU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void CollectionMoveEvent`1<System.Object>::set_Value(T)
extern "C"  void CollectionMoveEvent_1_set_Value_m4226272145_gshared (CollectionMoveEvent_1_t3572055381 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_U3CValueU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String CollectionMoveEvent`1<System.Object>::ToString()
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral272725123;
extern const uint32_t CollectionMoveEvent_1_ToString_m512063864_MetadataUsageId;
extern "C"  String_t* CollectionMoveEvent_1_ToString_m512063864_gshared (CollectionMoveEvent_1_t3572055381 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CollectionMoveEvent_1_ToString_m512063864_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((CollectionMoveEvent_1_t3572055381 *)__this);
		int32_t L_0 = ((  int32_t (*) (CollectionMoveEvent_1_t3572055381 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((CollectionMoveEvent_1_t3572055381 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_1);
		NullCheck((CollectionMoveEvent_1_t3572055381 *)__this);
		int32_t L_3 = ((  int32_t (*) (CollectionMoveEvent_1_t3572055381 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((CollectionMoveEvent_1_t3572055381 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_4);
		NullCheck((CollectionMoveEvent_1_t3572055381 *)__this);
		Il2CppObject * L_6 = ((  Il2CppObject * (*) (CollectionMoveEvent_1_t3572055381 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((CollectionMoveEvent_1_t3572055381 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Format_m3928391288(NULL /*static, unused*/, (String_t*)_stringLiteral272725123, (Il2CppObject *)L_2, (Il2CppObject *)L_5, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void CollectionRemoveEvent`1<System.Object>::.ctor(System.Int32,T)
extern "C"  void CollectionRemoveEvent_1__ctor_m1174003473_gshared (CollectionRemoveEvent_1_t898259258 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___index0;
		NullCheck((CollectionRemoveEvent_1_t898259258 *)__this);
		((  void (*) (CollectionRemoveEvent_1_t898259258 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((CollectionRemoveEvent_1_t898259258 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		NullCheck((CollectionRemoveEvent_1_t898259258 *)__this);
		((  void (*) (CollectionRemoveEvent_1_t898259258 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((CollectionRemoveEvent_1_t898259258 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
// System.Int32 CollectionRemoveEvent`1<System.Object>::get_Index()
extern "C"  int32_t CollectionRemoveEvent_1_get_Index_m2777665697_gshared (CollectionRemoveEvent_1_t898259258 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_U3CIndexU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void CollectionRemoveEvent`1<System.Object>::set_Index(System.Int32)
extern "C"  void CollectionRemoveEvent_1_set_Index_m1446049612_gshared (CollectionRemoveEvent_1_t898259258 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CIndexU3Ek__BackingField_0(L_0);
		return;
	}
}
// T CollectionRemoveEvent`1<System.Object>::get_Value()
extern "C"  Il2CppObject * CollectionRemoveEvent_1_get_Value_m445974639_gshared (CollectionRemoveEvent_1_t898259258 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3CValueU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void CollectionRemoveEvent`1<System.Object>::set_Value(T)
extern "C"  void CollectionRemoveEvent_1_set_Value_m347083076_gshared (CollectionRemoveEvent_1_t898259258 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_U3CValueU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String CollectionRemoveEvent`1<System.Object>::ToString()
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2032474014;
extern const uint32_t CollectionRemoveEvent_1_ToString_m2292716331_MetadataUsageId;
extern "C"  String_t* CollectionRemoveEvent_1_ToString_m2292716331_gshared (CollectionRemoveEvent_1_t898259258 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CollectionRemoveEvent_1_ToString_m2292716331_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((CollectionRemoveEvent_1_t898259258 *)__this);
		int32_t L_0 = ((  int32_t (*) (CollectionRemoveEvent_1_t898259258 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((CollectionRemoveEvent_1_t898259258 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_1);
		NullCheck((CollectionRemoveEvent_1_t898259258 *)__this);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (CollectionRemoveEvent_1_t898259258 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((CollectionRemoveEvent_1_t898259258 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Format_m2398979370(NULL /*static, unused*/, (String_t*)_stringLiteral2032474014, (Il2CppObject *)L_2, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void CollectionReplaceEvent`1<System.Object>::.ctor(System.Int32,T,T)
extern "C"  void CollectionReplaceEvent_1__ctor_m1290910597_gshared (CollectionReplaceEvent_1_t2497372070 * __this, int32_t ___index0, Il2CppObject * ___oldValue1, Il2CppObject * ___newValue2, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___index0;
		NullCheck((CollectionReplaceEvent_1_t2497372070 *)__this);
		((  void (*) (CollectionReplaceEvent_1_t2497372070 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((CollectionReplaceEvent_1_t2497372070 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___oldValue1;
		NullCheck((CollectionReplaceEvent_1_t2497372070 *)__this);
		((  void (*) (CollectionReplaceEvent_1_t2497372070 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((CollectionReplaceEvent_1_t2497372070 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject * L_2 = ___newValue2;
		NullCheck((CollectionReplaceEvent_1_t2497372070 *)__this);
		((  void (*) (CollectionReplaceEvent_1_t2497372070 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((CollectionReplaceEvent_1_t2497372070 *)__this, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
// System.Int32 CollectionReplaceEvent`1<System.Object>::get_Index()
extern "C"  int32_t CollectionReplaceEvent_1_get_Index_m2241178009_gshared (CollectionReplaceEvent_1_t2497372070 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_U3CIndexU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void CollectionReplaceEvent`1<System.Object>::set_Index(System.Int32)
extern "C"  void CollectionReplaceEvent_1_set_Index_m4096015976_gshared (CollectionReplaceEvent_1_t2497372070 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CIndexU3Ek__BackingField_0(L_0);
		return;
	}
}
// T CollectionReplaceEvent`1<System.Object>::get_OldValue()
extern "C"  Il2CppObject * CollectionReplaceEvent_1_get_OldValue_m3651064692_gshared (CollectionReplaceEvent_1_t2497372070 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3COldValueU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void CollectionReplaceEvent`1<System.Object>::set_OldValue(T)
extern "C"  void CollectionReplaceEvent_1_set_OldValue_m1129314551_gshared (CollectionReplaceEvent_1_t2497372070 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_U3COldValueU3Ek__BackingField_1(L_0);
		return;
	}
}
// T CollectionReplaceEvent`1<System.Object>::get_NewValue()
extern "C"  Il2CppObject * CollectionReplaceEvent_1_get_NewValue_m2368515547_gshared (CollectionReplaceEvent_1_t2497372070 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3CNewValueU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void CollectionReplaceEvent`1<System.Object>::set_NewValue(T)
extern "C"  void CollectionReplaceEvent_1_set_NewValue_m24996720_gshared (CollectionReplaceEvent_1_t2497372070 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_U3CNewValueU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String CollectionReplaceEvent`1<System.Object>::ToString()
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3696740198;
extern const uint32_t CollectionReplaceEvent_1_ToString_m2524726313_MetadataUsageId;
extern "C"  String_t* CollectionReplaceEvent_1_ToString_m2524726313_gshared (CollectionReplaceEvent_1_t2497372070 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CollectionReplaceEvent_1_ToString_m2524726313_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((CollectionReplaceEvent_1_t2497372070 *)__this);
		int32_t L_0 = ((  int32_t (*) (CollectionReplaceEvent_1_t2497372070 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((CollectionReplaceEvent_1_t2497372070 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_1);
		NullCheck((CollectionReplaceEvent_1_t2497372070 *)__this);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (CollectionReplaceEvent_1_t2497372070 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((CollectionReplaceEvent_1_t2497372070 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((CollectionReplaceEvent_1_t2497372070 *)__this);
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (CollectionReplaceEvent_1_t2497372070 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((CollectionReplaceEvent_1_t2497372070 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Format_m3928391288(NULL /*static, unused*/, (String_t*)_stringLiteral3696740198, (Il2CppObject *)L_2, (Il2CppObject *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m818164474_gshared (DOGetter_1_t1740296090 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>::Invoke()
extern "C"  Color2_t3046135109  DOGetter_1_Invoke_m3454115765_gshared (DOGetter_1_t1740296090 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m3454115765((DOGetter_1_t1740296090 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Color2_t3046135109  (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Color2_t3046135109  (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m982680375_gshared (DOGetter_1_t1740296090 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>::EndInvoke(System.IAsyncResult)
extern "C"  Color2_t3046135109  DOGetter_1_EndInvoke_m1123393323_gshared (DOGetter_1_t1740296090 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Color2_t3046135109 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<System.Double>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m2027841653_gshared (DOGetter_1_t3523644891 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<System.Double>::Invoke()
extern "C"  double DOGetter_1_Invoke_m3739392142_gshared (DOGetter_1_t3523644891 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m3739392142((DOGetter_1_t3523644891 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef double (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef double (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<System.Double>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m3661121556_gshared (DOGetter_1_t3523644891 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<System.Double>::EndInvoke(System.IAsyncResult)
extern "C"  double DOGetter_1_EndInvoke_m2222423876_gshared (DOGetter_1_t3523644891 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(double*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m705743206_gshared (DOGetter_1_t1541575768 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<System.Int32>::Invoke()
extern "C"  int32_t DOGetter_1_Invoke_m3396739553_gshared (DOGetter_1_t1541575768 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m3396739553((DOGetter_1_t1541575768 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<System.Int32>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m1324822347_gshared (DOGetter_1_t1541575768 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t DOGetter_1_EndInvoke_m3370317911_gshared (DOGetter_1_t1541575768 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<System.Int64>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m1203300455_gshared (DOGetter_1_t1541575863 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<System.Int64>::Invoke()
extern "C"  int64_t DOGetter_1_Invoke_m2774695970_gshared (DOGetter_1_t1541575863 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m2774695970((DOGetter_1_t1541575863 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef int64_t (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int64_t (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<System.Int64>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m1603746282_gshared (DOGetter_1_t1541575863 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<System.Int64>::EndInvoke(System.IAsyncResult)
extern "C"  int64_t DOGetter_1_EndInvoke_m2007810008_gshared (DOGetter_1_t1541575863 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int64_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m2412834247_gshared (DOGetter_1_t3826234697 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<System.Object>::Invoke()
extern "C"  Il2CppObject * DOGetter_1_Invoke_m3149930592_gshared (DOGetter_1_t3826234697 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m3149930592((DOGetter_1_t3826234697 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<System.Object>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m2941286402_gshared (DOGetter_1_t3826234697 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * DOGetter_1_EndInvoke_m2987752854_gshared (DOGetter_1_t3826234697 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void DG.Tweening.Core.DOGetter`1<System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m2525572702_gshared (DOGetter_1_t3947337298 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<System.Single>::Invoke()
extern "C"  float DOGetter_1_Invoke_m1136684983_gshared (DOGetter_1_t3947337298 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m1136684983((DOGetter_1_t3947337298 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef float (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef float (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<System.Single>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m4291962059_gshared (DOGetter_1_t3947337298 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  float DOGetter_1_EndInvoke_m3208356269_gshared (DOGetter_1_t3947337298 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(float*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<System.UInt32>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m3158204973_gshared (DOGetter_1_t3975053603 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<System.UInt32>::Invoke()
extern "C"  uint32_t DOGetter_1_Invoke_m2274052166_gshared (DOGetter_1_t3975053603 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m2274052166((DOGetter_1_t3975053603 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef uint32_t (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef uint32_t (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<System.UInt32>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m1505614172_gshared (DOGetter_1_t3975053603 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<System.UInt32>::EndInvoke(System.IAsyncResult)
extern "C"  uint32_t DOGetter_1_EndInvoke_m4203287804_gshared (DOGetter_1_t3975053603 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(uint32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<System.UInt64>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m3655762222_gshared (DOGetter_1_t3975053698 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<System.UInt64>::Invoke()
extern "C"  uint64_t DOGetter_1_Invoke_m1652008583_gshared (DOGetter_1_t3975053698 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m1652008583((DOGetter_1_t3975053698 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef uint64_t (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef uint64_t (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<System.UInt64>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m1784538107_gshared (DOGetter_1_t3975053698 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<System.UInt64>::EndInvoke(System.IAsyncResult)
extern "C"  uint64_t DOGetter_1_EndInvoke_m2840779901_gshared (DOGetter_1_t3975053698 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(uint64_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m602983897_gshared (DOGetter_1_t282336741 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Color>::Invoke()
extern "C"  Color_t1588175760  DOGetter_1_Invoke_m952048498_gshared (DOGetter_1_t282336741 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m952048498((DOGetter_1_t282336741 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Color_t1588175760  (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Color_t1588175760  (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<UnityEngine.Color>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m1324771120_gshared (DOGetter_1_t282336741 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Color>::EndInvoke(System.IAsyncResult)
extern "C"  Color_t1588175760  DOGetter_1_EndInvoke_m357760808_gshared (DOGetter_1_t282336741 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Color_t1588175760 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m3151867296_gshared (DOGetter_1_t585876960 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>::Invoke()
extern "C"  Quaternion_t1891715979  DOGetter_1_Invoke_m1361134939_gshared (DOGetter_1_t585876960 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m1361134939((DOGetter_1_t585876960 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Quaternion_t1891715979  (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Quaternion_t1891715979  (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m4132676689_gshared (DOGetter_1_t585876960 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>::EndInvoke(System.IAsyncResult)
extern "C"  Quaternion_t1891715979  DOGetter_1_EndInvoke_m1357177937_gshared (DOGetter_1_t585876960 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Quaternion_t1891715979 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m3776501594_gshared (DOGetter_1_t219589798 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>::Invoke()
extern "C"  Rect_t1525428817  DOGetter_1_Invoke_m2364178517_gshared (DOGetter_1_t219589798 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m2364178517((DOGetter_1_t219589798 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Rect_t1525428817  (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Rect_t1525428817  (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m343051223_gshared (DOGetter_1_t219589798 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>::EndInvoke(System.IAsyncResult)
extern "C"  Rect_t1525428817  DOGetter_1_EndInvoke_m1360414667_gshared (DOGetter_1_t219589798 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Rect_t1525428817 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m2194922925_gshared (DOGetter_1_t2219490769 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>::Invoke()
extern "C"  Vector2_t3525329788  DOGetter_1_Invoke_m1502300294_gshared (DOGetter_1_t2219490769 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m1502300294((DOGetter_1_t2219490769 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Vector2_t3525329788  (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Vector2_t3525329788  (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m4042989788_gshared (DOGetter_1_t2219490769 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>::EndInvoke(System.IAsyncResult)
extern "C"  Vector2_t3525329788  DOGetter_1_EndInvoke_m3639567676_gshared (DOGetter_1_t2219490769 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Vector2_t3525329788 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m3827726924_gshared (DOGetter_1_t2219490770 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>::Invoke()
extern "C"  Vector3_t3525329789  DOGetter_1_Invoke_m1631383013_gshared (DOGetter_1_t2219490770 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m1631383013((DOGetter_1_t2219490770 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Vector3_t3525329789  (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Vector3_t3525329789  (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m881213085_gshared (DOGetter_1_t2219490770 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>::EndInvoke(System.IAsyncResult)
extern "C"  Vector3_t3525329789  DOGetter_1_EndInvoke_m370092379_gshared (DOGetter_1_t2219490770 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Vector3_t3525329789 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m1165563627_gshared (DOGetter_1_t2219490771 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>::Invoke()
extern "C"  Vector4_t3525329790  DOGetter_1_Invoke_m1760465732_gshared (DOGetter_1_t2219490771 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m1760465732((DOGetter_1_t2219490771 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Vector4_t3525329790  (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Vector4_t3525329790  (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m2014403678_gshared (DOGetter_1_t2219490771 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>::EndInvoke(System.IAsyncResult)
extern "C"  Vector4_t3525329790  DOGetter_1_EndInvoke_m1395584378_gshared (DOGetter_1_t2219490771 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Vector4_t3525329790 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m491807086_gshared (DOSetter_1_t2776935918 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m904819478_gshared (DOSetter_1_t2776935918 * __this, Color2_t3046135109  ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m904819478((DOSetter_1_t2776935918 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Color2_t3046135109  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Color2_t3046135109  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Color2_t3046135109_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m1457673323_MetadataUsageId;
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m1457673323_gshared (DOSetter_1_t2776935918 * __this, Color2_t3046135109  ___pNewValue0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m1457673323_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Color2_t3046135109_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m1274657406_gshared (DOSetter_1_t2776935918 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Double>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m1097073025_gshared (DOSetter_1_t265317423 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Double>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m3272252323_gshared (DOSetter_1_t265317423 * __this, double ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m3272252323((DOSetter_1_t265317423 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, double ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, double ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<System.Double>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m2341169264_MetadataUsageId;
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m2341169264_gshared (DOSetter_1_t265317423 * __this, double ___pNewValue0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m2341169264_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Double_t534516614_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Double>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m1160666129_gshared (DOSetter_1_t265317423 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m3308117722_gshared (DOSetter_1_t2578215596 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Int32>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m3712128042_gshared (DOSetter_1_t2578215596 * __this, int32_t ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m3712128042((DOSetter_1_t2578215596 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<System.Int32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m3815801983_MetadataUsageId;
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m3815801983_gshared (DOSetter_1_t2578215596 * __this, int32_t ___pNewValue0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m3815801983_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m10931690_gshared (DOSetter_1_t2578215596 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Int64>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m3805674971_gshared (DOSetter_1_t2578215691 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Int64>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m1608646153_gshared (DOSetter_1_t2578215691 * __this, int64_t ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m1608646153((DOSetter_1_t2578215691 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int64_t ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int64_t ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<System.Int64>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Int64_t2847414882_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m1278763870_MetadataUsageId;
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m1278763870_gshared (DOSetter_1_t2578215691 * __this, int64_t ___pNewValue0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m1278763870_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int64_t2847414882_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Int64>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m2943391083_gshared (DOSetter_1_t2578215691 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m1482065619_gshared (DOSetter_1_t567907229 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Object>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m2178813457_gshared (DOSetter_1_t567907229 * __this, Il2CppObject * ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m2178813457((DOSetter_1_t567907229 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m2069320926_gshared (DOSetter_1_t567907229 * __this, Il2CppObject * ___pNewValue0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___pNewValue0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m1925995107_gshared (DOSetter_1_t567907229 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m1594804074_gshared (DOSetter_1_t689009830 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Single>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m4192709018_gshared (DOSetter_1_t689009830 * __this, float ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m4192709018((DOSetter_1_t689009830 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, float ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, float ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<System.Single>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Single_t958209021_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m2988503911_MetadataUsageId;
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m2988503911_gshared (DOSetter_1_t689009830 * __this, float ___pNewValue0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m2988503911_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Single_t958209021_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m2146598522_gshared (DOSetter_1_t689009830 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.UInt32>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m2227436345_gshared (DOSetter_1_t716726135 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.UInt32>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m796386027_gshared (DOSetter_1_t716726135 * __this, uint32_t ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m796386027((DOSetter_1_t716726135 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, uint32_t ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, uint32_t ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<System.UInt32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt32_t985925326_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m1072809912_MetadataUsageId;
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m1072809912_gshared (DOSetter_1_t716726135 * __this, uint32_t ___pNewValue0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m1072809912_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UInt32_t985925326_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.UInt32>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m3141530057_gshared (DOSetter_1_t716726135 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.UInt64>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m2724993594_gshared (DOSetter_1_t716726230 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.UInt64>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m2987871434_gshared (DOSetter_1_t716726230 * __this, uint64_t ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m2987871434((DOSetter_1_t716726230 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, uint64_t ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, uint64_t ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<System.UInt64>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt64_t985925421_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m2830739095_MetadataUsageId;
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m2830739095_gshared (DOSetter_1_t716726230 * __this, uint64_t ___pNewValue0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m2830739095_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UInt64_t985925421_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.UInt64>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m1779022154_gshared (DOSetter_1_t716726230 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m2393571557_gshared (DOSetter_1_t1318976569 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Color>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m3782456255_gshared (DOSetter_1_t1318976569 * __this, Color_t1588175760  ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m3782456255((DOSetter_1_t1318976569 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Color_t1588175760  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Color_t1588175760  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<UnityEngine.Color>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Color_t1588175760_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m3087160972_MetadataUsageId;
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m3087160972_gshared (DOSetter_1_t1318976569 * __this, Color_t1588175760  ___pNewValue0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m3087160972_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Color_t1588175760_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Color>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m2995039605_gshared (DOSetter_1_t1318976569 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m285573652_gshared (DOSetter_1_t1622516788 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m770790320_gshared (DOSetter_1_t1622516788 * __this, Quaternion_t1891715979  ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m770790320((DOSetter_1_t1622516788 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Quaternion_t1891715979  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Quaternion_t1891715979  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Quaternion_t1891715979_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m2864918789_MetadataUsageId;
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m2864918789_gshared (DOSetter_1_t1622516788 * __this, Quaternion_t1891715979  ___pNewValue0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m2864918789_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Quaternion_t1891715979_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m3103071780_gshared (DOSetter_1_t1622516788 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m3557167822_gshared (DOSetter_1_t1256229626 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m969078198_gshared (DOSetter_1_t1256229626 * __this, Rect_t1525428817  ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m969078198((DOSetter_1_t1256229626 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Rect_t1525428817  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Rect_t1525428817  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Rect_t1525428817_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m3970304011_MetadataUsageId;
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m3970304011_gshared (DOSetter_1_t1256229626 * __this, Rect_t1525428817  ___pNewValue0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m3970304011_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Rect_t1525428817_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m2969508830_gshared (DOSetter_1_t1256229626 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m667778489_gshared (DOSetter_1_t3256130597 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m2210697323_gshared (DOSetter_1_t3256130597 * __this, Vector2_t3525329788  ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m2210697323((DOSetter_1_t3256130597 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Vector2_t3525329788  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Vector2_t3525329788  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Vector2_t3525329788_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m655902776_MetadataUsageId;
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m655902776_gshared (DOSetter_1_t3256130597 * __this, Vector2_t3525329788  ___pNewValue0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m655902776_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector2_t3525329788_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m4033786953_gshared (DOSetter_1_t3256130597 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m2300582488_gshared (DOSetter_1_t3256130598 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m1917294316_gshared (DOSetter_1_t3256130598 * __this, Vector3_t3525329789  ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m1917294316((DOSetter_1_t3256130598 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Vector3_t3525329789  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Vector3_t3525329789  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Vector3_t3525329789_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m3025336761_MetadataUsageId;
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m3025336761_gshared (DOSetter_1_t3256130598 * __this, Vector3_t3525329789  ___pNewValue0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m3025336761_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector3_t3525329789_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m764311656_gshared (DOSetter_1_t3256130598 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m3933386487_gshared (DOSetter_1_t3256130599 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m1623891309_gshared (DOSetter_1_t3256130599 * __this, Vector4_t3525329790  ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m1623891309((DOSetter_1_t3256130599 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Vector4_t3525329790  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Vector4_t3525329790  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Vector4_t3525329790_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m1099803450_MetadataUsageId;
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m1099803450_gshared (DOSetter_1_t3256130599 * __this, Vector4_t3525329790  ___pNewValue0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m1099803450_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector4_t3525329790_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m1789803655_gshared (DOSetter_1_t3256130599 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::.ctor()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3__ctor_m525155785_MetadataUsageId;
extern "C"  void TweenerCore_3__ctor_m525155785_gshared (TweenerCore_3_t182370955 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m525155785_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Tweener_t1766303790 *)__this);
		Tweener__ctor_m1348308877((Tweener_t1766303790 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2598715689 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t1103364673 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t1103364673 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::ChangeStartValue(System.Object,System.Single)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1899955427;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeStartValue_m1161765368_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeStartValue_m1161765368_gshared (TweenerCore_3_t182370955 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m1161765368_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_7 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, _stringLiteral1899955427);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1899955427);
		ObjectU5BU5D_t11523773* L_8 = (ObjectU5BU5D_t11523773*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		ArrayElementTypeCheck (L_10, _stringLiteral3718369476);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_11 = (ObjectU5BU5D_t11523773*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 3);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_11;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral41);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t1766303790 * L_17 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t182370955 *, Color2_t3046135109 , float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, (TweenerCore_3_t182370955 *)__this, (Color2_t3046135109 )((*(Color2_t3046135109 *)((Color2_t3046135109 *)UnBox (L_15, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeEndValue_m3901769713_gshared (TweenerCore_3_t182370955 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t1766303790 *)__this);
		Tweener_t1766303790 * L_2 = VirtFuncInvoker3< Tweener_t1766303790 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t1766303790 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1647271171;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeEndValue_m1398610796_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeEndValue_m1398610796_gshared (TweenerCore_3_t182370955 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m1398610796_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_7 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, _stringLiteral1647271171);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1647271171);
		ObjectU5BU5D_t11523773* L_8 = (ObjectU5BU5D_t11523773*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		ArrayElementTypeCheck (L_10, _stringLiteral3718369476);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_11 = (ObjectU5BU5D_t11523773*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 3);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_11;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral41);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t1766303790 * L_18 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t182370955 *, Color2_t3046135109 , float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (TweenerCore_3_t182370955 *)__this, (Color2_t3046135109 )((*(Color2_t3046135109 *)((Color2_t3046135109 *)UnBox (L_15, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1932880146;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeValues_m2462101939_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeValues_m2462101939_gshared (TweenerCore_3_t182370955 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m2462101939_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m2022236990((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_9 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		ArrayElementTypeCheck (L_9, _stringLiteral1932880146);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1932880146);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 1);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t11523773* L_12 = (ObjectU5BU5D_t11523773*)L_10;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 2);
		ArrayElementTypeCheck (L_12, _stringLiteral3718369476);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 3);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t11523773* L_15 = (ObjectU5BU5D_t11523773*)L_13;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 4);
		ArrayElementTypeCheck (L_15, _stringLiteral41);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_20 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 0);
		ArrayElementTypeCheck (L_20, _stringLiteral1932880146);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1932880146);
		ObjectU5BU5D_t11523773* L_21 = (ObjectU5BU5D_t11523773*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 1);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t11523773* L_23 = (ObjectU5BU5D_t11523773*)L_21;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 2);
		ArrayElementTypeCheck (L_23, _stringLiteral3718369476);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_24 = (ObjectU5BU5D_t11523773*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 3);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t11523773* L_26 = (ObjectU5BU5D_t11523773*)L_24;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 4);
		ArrayElementTypeCheck (L_26, _stringLiteral41);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t1766303790 * L_31 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t182370955 *, Color2_t3046135109 , Color2_t3046135109 , float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, (TweenerCore_3_t182370955 *)__this, (Color2_t3046135109 )((*(Color2_t3046135109 *)((Color2_t3046135109 *)UnBox (L_28, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (Color2_t3046135109 )((*(Color2_t3046135109 *)((Color2_t3046135109 *)UnBox (L_29, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t1766303790 * TweenerCore_3_SetFrom_m832224483_gshared (TweenerCore_3_t182370955 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t1075249930 * L_0 = (ABSTweenPlugin_3_t1075249930 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t1075249930 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t182370955 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t1075249930 *)L_0, (TweenerCore_3_t182370955 *)__this, (bool)L_1);
		((Tweener_t1766303790 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m2466556022_gshared (TweenerCore_3_t182370955 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t1103364673 *)__this);
		Tween_Reset_m1561947655((Tween_t1103364673 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t1075249930 * L_0 = (ABSTweenPlugin_3_t1075249930 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t1075249930 * L_1 = (ABSTweenPlugin_3_t1075249930 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t1075249930 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t182370955 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t1075249930 *)L_1, (TweenerCore_3_t182370955 *)__this);
	}

IL_001a:
	{
		ColorOptions_t4161000167  L_2 = ((  ColorOptions_t4161000167  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t1740296090 *)NULL);
		__this->set_setter_58((DOSetter_1_t2776935918 *)NULL);
		((Tweener_t1766303790 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t1766303790 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::Validate()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_Validate_m554416521_MetadataUsageId;
extern "C"  bool TweenerCore_3_Validate_m554416521_gshared (TweenerCore_3_t182370955 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m554416521_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t1740296090 * L_0 = (DOGetter_1_t1740296090 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t1740296090 *)L_0);
		((  Color2_t3046135109  (*) (DOGetter_1_t1740296090 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((DOGetter_1_t1740296090 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m3544669046_gshared (TweenerCore_3_t182370955 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t182370955 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(NULL /*static, unused*/, (TweenerCore_3_t182370955 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m523382028_gshared (TweenerCore_3_t182370955 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t182370955 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)(NULL /*static, unused*/, (TweenerCore_3_t182370955 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern Il2CppClass* DOTween_t3585775766_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_ApplyTween_m3024958543_MetadataUsageId;
extern "C"  bool TweenerCore_3_ApplyTween_m3024958543_gshared (TweenerCore_3_t182370955 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m3024958543_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t1103364673 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t1103364673 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t3585775766_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t3585775766_StaticFields*)DOTween_t3585775766_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t1075249930 * L_5 = (ABSTweenPlugin_3_t1075249930 *)__this->get_tweenPlugin_59();
		ColorOptions_t4161000167  L_6 = (ColorOptions_t4161000167 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t1103364673 *)__this)->get_isRelative_27();
		DOGetter_1_t1740296090 * L_8 = (DOGetter_1_t1740296090 *)__this->get_getter_57();
		DOSetter_1_t2776935918 * L_9 = (DOSetter_1_t2776935918 *)__this->get_setter_58();
		float L_10 = V_0;
		Color2_t3046135109  L_11 = (Color2_t3046135109 )__this->get_startValue_53();
		Color2_t3046135109  L_12 = (Color2_t3046135109 )__this->get_changeValue_55();
		float L_13 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t1075249930 *)L_5);
		VirtActionInvoker11< ColorOptions_t4161000167 , Tween_t1103364673 *, bool, DOGetter_1_t1740296090 *, DOSetter_1_t2776935918 *, float, Color2_t3046135109 , Color2_t3046135109 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t1075249930 *)L_5, (ColorOptions_t4161000167 )L_6, (Tween_t1103364673 *)__this, (bool)L_7, (DOGetter_1_t1740296090 *)L_8, (DOSetter_1_t2776935918 *)L_9, (float)L_10, (Color2_t3046135109 )L_11, (Color2_t3046135109 )L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t1075249930 * L_16 = (ABSTweenPlugin_3_t1075249930 *)__this->get_tweenPlugin_59();
		ColorOptions_t4161000167  L_17 = (ColorOptions_t4161000167 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t1103364673 *)__this)->get_isRelative_27();
		DOGetter_1_t1740296090 * L_19 = (DOGetter_1_t1740296090 *)__this->get_getter_57();
		DOSetter_1_t2776935918 * L_20 = (DOSetter_1_t2776935918 *)__this->get_setter_58();
		float L_21 = V_0;
		Color2_t3046135109  L_22 = (Color2_t3046135109 )__this->get_startValue_53();
		Color2_t3046135109  L_23 = (Color2_t3046135109 )__this->get_changeValue_55();
		float L_24 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t1075249930 *)L_16);
		VirtActionInvoker11< ColorOptions_t4161000167 , Tween_t1103364673 *, bool, DOGetter_1_t1740296090 *, DOSetter_1_t2776935918 *, float, Color2_t3046135109 , Color2_t3046135109 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t1075249930 *)L_16, (ColorOptions_t4161000167 )L_17, (Tween_t1103364673 *)__this, (bool)L_18, (DOGetter_1_t1740296090 *)L_19, (DOSetter_1_t2776935918 *)L_20, (float)L_21, (Color2_t3046135109 )L_22, (Color2_t3046135109 )L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3__ctor_m779761761_MetadataUsageId;
extern "C"  void TweenerCore_3__ctor_m779761761_gshared (TweenerCore_3_t1963399521 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m779761761_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Tweener_t1766303790 *)__this);
		Tweener__ctor_m1348308877((Tweener_t1766303790 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2598715689 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t1103364673 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t1103364673 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::ChangeStartValue(System.Object,System.Single)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1899955427;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeStartValue_m607056190_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeStartValue_m607056190_gshared (TweenerCore_3_t1963399521 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m607056190_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_7 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, _stringLiteral1899955427);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1899955427);
		ObjectU5BU5D_t11523773* L_8 = (ObjectU5BU5D_t11523773*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		ArrayElementTypeCheck (L_10, _stringLiteral3718369476);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_11 = (ObjectU5BU5D_t11523773*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 3);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_11;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral41);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t1766303790 * L_17 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1963399521 *, double, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, (TweenerCore_3_t1963399521 *)__this, (double)((*(double*)((double*)UnBox (L_15, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeEndValue_m2221307883_gshared (TweenerCore_3_t1963399521 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t1766303790 *)__this);
		Tweener_t1766303790 * L_2 = VirtFuncInvoker3< Tweener_t1766303790 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t1766303790 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1647271171;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeEndValue_m2306526694_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeEndValue_m2306526694_gshared (TweenerCore_3_t1963399521 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m2306526694_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_7 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, _stringLiteral1647271171);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1647271171);
		ObjectU5BU5D_t11523773* L_8 = (ObjectU5BU5D_t11523773*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		ArrayElementTypeCheck (L_10, _stringLiteral3718369476);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_11 = (ObjectU5BU5D_t11523773*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 3);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_11;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral41);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t1766303790 * L_18 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1963399521 *, double, float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (TweenerCore_3_t1963399521 *)__this, (double)((*(double*)((double*)UnBox (L_15, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1932880146;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeValues_m1979883897_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeValues_m1979883897_gshared (TweenerCore_3_t1963399521 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m1979883897_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m2022236990((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_9 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		ArrayElementTypeCheck (L_9, _stringLiteral1932880146);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1932880146);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 1);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t11523773* L_12 = (ObjectU5BU5D_t11523773*)L_10;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 2);
		ArrayElementTypeCheck (L_12, _stringLiteral3718369476);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 3);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t11523773* L_15 = (ObjectU5BU5D_t11523773*)L_13;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 4);
		ArrayElementTypeCheck (L_15, _stringLiteral41);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_20 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 0);
		ArrayElementTypeCheck (L_20, _stringLiteral1932880146);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1932880146);
		ObjectU5BU5D_t11523773* L_21 = (ObjectU5BU5D_t11523773*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 1);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t11523773* L_23 = (ObjectU5BU5D_t11523773*)L_21;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 2);
		ArrayElementTypeCheck (L_23, _stringLiteral3718369476);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_24 = (ObjectU5BU5D_t11523773*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 3);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t11523773* L_26 = (ObjectU5BU5D_t11523773*)L_24;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 4);
		ArrayElementTypeCheck (L_26, _stringLiteral41);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t1766303790 * L_31 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1963399521 *, double, double, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, (TweenerCore_3_t1963399521 *)__this, (double)((*(double*)((double*)UnBox (L_28, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (double)((*(double*)((double*)UnBox (L_29, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t1766303790 * TweenerCore_3_SetFrom_m2212316329_gshared (TweenerCore_3_t1963399521 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t2856278496 * L_0 = (ABSTweenPlugin_3_t2856278496 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t2856278496 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t1963399521 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t2856278496 *)L_0, (TweenerCore_3_t1963399521 *)__this, (bool)L_1);
		((Tweener_t1766303790 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m2721161998_gshared (TweenerCore_3_t1963399521 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t1103364673 *)__this);
		Tween_Reset_m1561947655((Tween_t1103364673 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t2856278496 * L_0 = (ABSTweenPlugin_3_t2856278496 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t2856278496 * L_1 = (ABSTweenPlugin_3_t2856278496 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t2856278496 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t1963399521 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t2856278496 *)L_1, (TweenerCore_3_t1963399521 *)__this);
	}

IL_001a:
	{
		NoOptions_t32144009  L_2 = ((  NoOptions_t32144009  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t3523644891 *)NULL);
		__this->set_setter_58((DOSetter_1_t265317423 *)NULL);
		((Tweener_t1766303790 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t1766303790 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::Validate()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_Validate_m2104889281_MetadataUsageId;
extern "C"  bool TweenerCore_3_Validate_m2104889281_gshared (TweenerCore_3_t1963399521 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m2104889281_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t3523644891 * L_0 = (DOGetter_1_t3523644891 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t3523644891 *)L_0);
		((  double (*) (DOGetter_1_t3523644891 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((DOGetter_1_t3523644891 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m3694733766_gshared (TweenerCore_3_t1963399521 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1963399521 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(NULL /*static, unused*/, (TweenerCore_3_t1963399521 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m2374512596_gshared (TweenerCore_3_t1963399521 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1963399521 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)(NULL /*static, unused*/, (TweenerCore_3_t1963399521 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern Il2CppClass* DOTween_t3585775766_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_ApplyTween_m181155975_MetadataUsageId;
extern "C"  bool TweenerCore_3_ApplyTween_m181155975_gshared (TweenerCore_3_t1963399521 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m181155975_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t1103364673 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t1103364673 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t3585775766_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t3585775766_StaticFields*)DOTween_t3585775766_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t2856278496 * L_5 = (ABSTweenPlugin_3_t2856278496 *)__this->get_tweenPlugin_59();
		NoOptions_t32144009  L_6 = (NoOptions_t32144009 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t1103364673 *)__this)->get_isRelative_27();
		DOGetter_1_t3523644891 * L_8 = (DOGetter_1_t3523644891 *)__this->get_getter_57();
		DOSetter_1_t265317423 * L_9 = (DOSetter_1_t265317423 *)__this->get_setter_58();
		float L_10 = V_0;
		double L_11 = (double)__this->get_startValue_53();
		double L_12 = (double)__this->get_changeValue_55();
		float L_13 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t2856278496 *)L_5);
		VirtActionInvoker11< NoOptions_t32144009 , Tween_t1103364673 *, bool, DOGetter_1_t3523644891 *, DOSetter_1_t265317423 *, float, double, double, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t2856278496 *)L_5, (NoOptions_t32144009 )L_6, (Tween_t1103364673 *)__this, (bool)L_7, (DOGetter_1_t3523644891 *)L_8, (DOSetter_1_t265317423 *)L_9, (float)L_10, (double)L_11, (double)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t2856278496 * L_16 = (ABSTweenPlugin_3_t2856278496 *)__this->get_tweenPlugin_59();
		NoOptions_t32144009  L_17 = (NoOptions_t32144009 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t1103364673 *)__this)->get_isRelative_27();
		DOGetter_1_t3523644891 * L_19 = (DOGetter_1_t3523644891 *)__this->get_getter_57();
		DOSetter_1_t265317423 * L_20 = (DOSetter_1_t265317423 *)__this->get_setter_58();
		float L_21 = V_0;
		double L_22 = (double)__this->get_startValue_53();
		double L_23 = (double)__this->get_changeValue_55();
		float L_24 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t2856278496 *)L_16);
		VirtActionInvoker11< NoOptions_t32144009 , Tween_t1103364673 *, bool, DOGetter_1_t3523644891 *, DOSetter_1_t265317423 *, float, double, double, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t2856278496 *)L_16, (NoOptions_t32144009 )L_17, (Tween_t1103364673 *)__this, (bool)L_18, (DOGetter_1_t3523644891 *)L_19, (DOSetter_1_t265317423 *)L_20, (float)L_21, (double)L_22, (double)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3__ctor_m260331257_MetadataUsageId;
extern "C"  void TweenerCore_3__ctor_m260331257_gshared (TweenerCore_3_t2554842309 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m260331257_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Tweener_t1766303790 *)__this);
		Tweener__ctor_m1348308877((Tweener_t1766303790 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2598715689 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t1103364673 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t1103364673 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::ChangeStartValue(System.Object,System.Single)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1899955427;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeStartValue_m524612502_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeStartValue_m524612502_gshared (TweenerCore_3_t2554842309 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m524612502_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_7 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, _stringLiteral1899955427);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1899955427);
		ObjectU5BU5D_t11523773* L_8 = (ObjectU5BU5D_t11523773*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		ArrayElementTypeCheck (L_10, _stringLiteral3718369476);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_11 = (ObjectU5BU5D_t11523773*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 3);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_11;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral41);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t1766303790 * L_17 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2554842309 *, int32_t, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, (TweenerCore_3_t2554842309 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_15, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeEndValue_m4158311059_gshared (TweenerCore_3_t2554842309 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t1766303790 *)__this);
		Tweener_t1766303790 * L_2 = VirtFuncInvoker3< Tweener_t1766303790 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t1766303790 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1647271171;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeEndValue_m569727630_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeEndValue_m569727630_gshared (TweenerCore_3_t2554842309 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m569727630_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_7 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, _stringLiteral1647271171);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1647271171);
		ObjectU5BU5D_t11523773* L_8 = (ObjectU5BU5D_t11523773*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		ArrayElementTypeCheck (L_10, _stringLiteral3718369476);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_11 = (ObjectU5BU5D_t11523773*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 3);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_11;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral41);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t1766303790 * L_18 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2554842309 *, int32_t, float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (TweenerCore_3_t2554842309 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_15, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1932880146;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeValues_m94082513_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeValues_m94082513_gshared (TweenerCore_3_t2554842309 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m94082513_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m2022236990((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_9 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		ArrayElementTypeCheck (L_9, _stringLiteral1932880146);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1932880146);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 1);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t11523773* L_12 = (ObjectU5BU5D_t11523773*)L_10;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 2);
		ArrayElementTypeCheck (L_12, _stringLiteral3718369476);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 3);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t11523773* L_15 = (ObjectU5BU5D_t11523773*)L_13;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 4);
		ArrayElementTypeCheck (L_15, _stringLiteral41);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_20 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 0);
		ArrayElementTypeCheck (L_20, _stringLiteral1932880146);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1932880146);
		ObjectU5BU5D_t11523773* L_21 = (ObjectU5BU5D_t11523773*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 1);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t11523773* L_23 = (ObjectU5BU5D_t11523773*)L_21;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 2);
		ArrayElementTypeCheck (L_23, _stringLiteral3718369476);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_24 = (ObjectU5BU5D_t11523773*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 3);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t11523773* L_26 = (ObjectU5BU5D_t11523773*)L_24;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 4);
		ArrayElementTypeCheck (L_26, _stringLiteral41);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t1766303790 * L_31 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2554842309 *, int32_t, int32_t, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, (TweenerCore_3_t2554842309 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_28, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (int32_t)((*(int32_t*)((int32_t*)UnBox (L_29, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t1766303790 * TweenerCore_3_SetFrom_m127768833_gshared (TweenerCore_3_t2554842309 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t3447721284 * L_0 = (ABSTweenPlugin_3_t3447721284 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t3447721284 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t2554842309 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t3447721284 *)L_0, (TweenerCore_3_t2554842309 *)__this, (bool)L_1);
		((Tweener_t1766303790 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m2201731494_gshared (TweenerCore_3_t2554842309 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t1103364673 *)__this);
		Tween_Reset_m1561947655((Tween_t1103364673 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t3447721284 * L_0 = (ABSTweenPlugin_3_t3447721284 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t3447721284 * L_1 = (ABSTweenPlugin_3_t3447721284 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t3447721284 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t2554842309 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t3447721284 *)L_1, (TweenerCore_3_t2554842309 *)__this);
	}

IL_001a:
	{
		NoOptions_t32144009  L_2 = ((  NoOptions_t32144009  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t1541575768 *)NULL);
		__this->set_setter_58((DOSetter_1_t2578215596 *)NULL);
		((Tweener_t1766303790 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t1766303790 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::Validate()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_Validate_m2620736553_MetadataUsageId;
extern "C"  bool TweenerCore_3_Validate_m2620736553_gshared (TweenerCore_3_t2554842309 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m2620736553_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t1541575768 * L_0 = (DOGetter_1_t1541575768 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t1541575768 *)L_0);
		((  int32_t (*) (DOGetter_1_t1541575768 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((DOGetter_1_t1541575768 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m3198587182_gshared (TweenerCore_3_t2554842309 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2554842309 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(NULL /*static, unused*/, (TweenerCore_3_t2554842309 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m3915173484_gshared (TweenerCore_3_t2554842309 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2554842309 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)(NULL /*static, unused*/, (TweenerCore_3_t2554842309 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern Il2CppClass* DOTween_t3585775766_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_ApplyTween_m1036969711_MetadataUsageId;
extern "C"  bool TweenerCore_3_ApplyTween_m1036969711_gshared (TweenerCore_3_t2554842309 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m1036969711_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t1103364673 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t1103364673 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t3585775766_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t3585775766_StaticFields*)DOTween_t3585775766_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t3447721284 * L_5 = (ABSTweenPlugin_3_t3447721284 *)__this->get_tweenPlugin_59();
		NoOptions_t32144009  L_6 = (NoOptions_t32144009 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t1103364673 *)__this)->get_isRelative_27();
		DOGetter_1_t1541575768 * L_8 = (DOGetter_1_t1541575768 *)__this->get_getter_57();
		DOSetter_1_t2578215596 * L_9 = (DOSetter_1_t2578215596 *)__this->get_setter_58();
		float L_10 = V_0;
		int32_t L_11 = (int32_t)__this->get_startValue_53();
		int32_t L_12 = (int32_t)__this->get_changeValue_55();
		float L_13 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t3447721284 *)L_5);
		VirtActionInvoker11< NoOptions_t32144009 , Tween_t1103364673 *, bool, DOGetter_1_t1541575768 *, DOSetter_1_t2578215596 *, float, int32_t, int32_t, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t3447721284 *)L_5, (NoOptions_t32144009 )L_6, (Tween_t1103364673 *)__this, (bool)L_7, (DOGetter_1_t1541575768 *)L_8, (DOSetter_1_t2578215596 *)L_9, (float)L_10, (int32_t)L_11, (int32_t)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t3447721284 * L_16 = (ABSTweenPlugin_3_t3447721284 *)__this->get_tweenPlugin_59();
		NoOptions_t32144009  L_17 = (NoOptions_t32144009 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t1103364673 *)__this)->get_isRelative_27();
		DOGetter_1_t1541575768 * L_19 = (DOGetter_1_t1541575768 *)__this->get_getter_57();
		DOSetter_1_t2578215596 * L_20 = (DOSetter_1_t2578215596 *)__this->get_setter_58();
		float L_21 = V_0;
		int32_t L_22 = (int32_t)__this->get_startValue_53();
		int32_t L_23 = (int32_t)__this->get_changeValue_55();
		float L_24 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t3447721284 *)L_16);
		VirtActionInvoker11< NoOptions_t32144009 , Tween_t1103364673 *, bool, DOGetter_1_t1541575768 *, DOSetter_1_t2578215596 *, float, int32_t, int32_t, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t3447721284 *)L_16, (NoOptions_t32144009 )L_17, (Tween_t1103364673 *)__this, (bool)L_18, (DOGetter_1_t1541575768 *)L_19, (DOSetter_1_t2578215596 *)L_20, (float)L_21, (int32_t)L_22, (int32_t)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3__ctor_m3871336793_MetadataUsageId;
extern "C"  void TweenerCore_3__ctor_m3871336793_gshared (TweenerCore_3_t908793361 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m3871336793_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Tweener_t1766303790 *)__this);
		Tweener__ctor_m1348308877((Tweener_t1766303790 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2598715689 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t1103364673 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t1103364673 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::ChangeStartValue(System.Object,System.Single)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1899955427;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeStartValue_m2678046198_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeStartValue_m2678046198_gshared (TweenerCore_3_t908793361 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m2678046198_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_7 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, _stringLiteral1899955427);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1899955427);
		ObjectU5BU5D_t11523773* L_8 = (ObjectU5BU5D_t11523773*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		ArrayElementTypeCheck (L_10, _stringLiteral3718369476);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_11 = (ObjectU5BU5D_t11523773*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 3);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_11;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral41);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t1766303790 * L_17 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t908793361 *, int64_t, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, (TweenerCore_3_t908793361 *)__this, (int64_t)((*(int64_t*)((int64_t*)UnBox (L_15, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeEndValue_m902640691_gshared (TweenerCore_3_t908793361 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t1766303790 *)__this);
		Tweener_t1766303790 * L_2 = VirtFuncInvoker3< Tweener_t1766303790 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t1766303790 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1647271171;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeEndValue_m941517870_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeEndValue_m941517870_gshared (TweenerCore_3_t908793361 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m941517870_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_7 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, _stringLiteral1647271171);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1647271171);
		ObjectU5BU5D_t11523773* L_8 = (ObjectU5BU5D_t11523773*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		ArrayElementTypeCheck (L_10, _stringLiteral3718369476);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_11 = (ObjectU5BU5D_t11523773*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 3);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_11;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral41);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t1766303790 * L_18 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t908793361 *, int64_t, float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (TweenerCore_3_t908793361 *)__this, (int64_t)((*(int64_t*)((int64_t*)UnBox (L_15, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1932880146;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeValues_m2823088177_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeValues_m2823088177_gshared (TweenerCore_3_t908793361 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m2823088177_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m2022236990((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_9 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		ArrayElementTypeCheck (L_9, _stringLiteral1932880146);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1932880146);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 1);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t11523773* L_12 = (ObjectU5BU5D_t11523773*)L_10;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 2);
		ArrayElementTypeCheck (L_12, _stringLiteral3718369476);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 3);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t11523773* L_15 = (ObjectU5BU5D_t11523773*)L_13;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 4);
		ArrayElementTypeCheck (L_15, _stringLiteral41);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_20 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 0);
		ArrayElementTypeCheck (L_20, _stringLiteral1932880146);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1932880146);
		ObjectU5BU5D_t11523773* L_21 = (ObjectU5BU5D_t11523773*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 1);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t11523773* L_23 = (ObjectU5BU5D_t11523773*)L_21;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 2);
		ArrayElementTypeCheck (L_23, _stringLiteral3718369476);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_24 = (ObjectU5BU5D_t11523773*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 3);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t11523773* L_26 = (ObjectU5BU5D_t11523773*)L_24;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 4);
		ArrayElementTypeCheck (L_26, _stringLiteral41);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t1766303790 * L_31 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t908793361 *, int64_t, int64_t, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, (TweenerCore_3_t908793361 *)__this, (int64_t)((*(int64_t*)((int64_t*)UnBox (L_28, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (int64_t)((*(int64_t*)((int64_t*)UnBox (L_29, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t1766303790 * TweenerCore_3_SetFrom_m690514785_gshared (TweenerCore_3_t908793361 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t1801672336 * L_0 = (ABSTweenPlugin_3_t1801672336 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t1801672336 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t908793361 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t1801672336 *)L_0, (TweenerCore_3_t908793361 *)__this, (bool)L_1);
		((Tweener_t1766303790 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m1517769734_gshared (TweenerCore_3_t908793361 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t1103364673 *)__this);
		Tween_Reset_m1561947655((Tween_t1103364673 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t1801672336 * L_0 = (ABSTweenPlugin_3_t1801672336 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t1801672336 * L_1 = (ABSTweenPlugin_3_t1801672336 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t1801672336 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t908793361 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t1801672336 *)L_1, (TweenerCore_3_t908793361 *)__this);
	}

IL_001a:
	{
		NoOptions_t32144009  L_2 = ((  NoOptions_t32144009  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t1541575863 *)NULL);
		__this->set_setter_58((DOSetter_1_t2578215691 *)NULL);
		((Tweener_t1766303790 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t1766303790 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::Validate()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_Validate_m2040796617_MetadataUsageId;
extern "C"  bool TweenerCore_3_Validate_m2040796617_gshared (TweenerCore_3_t908793361 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m2040796617_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t1541575863 * L_0 = (DOGetter_1_t1541575863 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t1541575863 *)L_0);
		((  int64_t (*) (DOGetter_1_t1541575863 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((DOGetter_1_t1541575863 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m410919630_gshared (TweenerCore_3_t908793361 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t908793361 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(NULL /*static, unused*/, (TweenerCore_3_t908793361 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m3757918412_gshared (TweenerCore_3_t908793361 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t908793361 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)(NULL /*static, unused*/, (TweenerCore_3_t908793361 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern Il2CppClass* DOTween_t3585775766_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_ApplyTween_m1087086735_MetadataUsageId;
extern "C"  bool TweenerCore_3_ApplyTween_m1087086735_gshared (TweenerCore_3_t908793361 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m1087086735_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t1103364673 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t1103364673 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t3585775766_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t3585775766_StaticFields*)DOTween_t3585775766_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t1801672336 * L_5 = (ABSTweenPlugin_3_t1801672336 *)__this->get_tweenPlugin_59();
		NoOptions_t32144009  L_6 = (NoOptions_t32144009 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t1103364673 *)__this)->get_isRelative_27();
		DOGetter_1_t1541575863 * L_8 = (DOGetter_1_t1541575863 *)__this->get_getter_57();
		DOSetter_1_t2578215691 * L_9 = (DOSetter_1_t2578215691 *)__this->get_setter_58();
		float L_10 = V_0;
		int64_t L_11 = (int64_t)__this->get_startValue_53();
		int64_t L_12 = (int64_t)__this->get_changeValue_55();
		float L_13 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t1801672336 *)L_5);
		VirtActionInvoker11< NoOptions_t32144009 , Tween_t1103364673 *, bool, DOGetter_1_t1541575863 *, DOSetter_1_t2578215691 *, float, int64_t, int64_t, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t1801672336 *)L_5, (NoOptions_t32144009 )L_6, (Tween_t1103364673 *)__this, (bool)L_7, (DOGetter_1_t1541575863 *)L_8, (DOSetter_1_t2578215691 *)L_9, (float)L_10, (int64_t)L_11, (int64_t)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t1801672336 * L_16 = (ABSTweenPlugin_3_t1801672336 *)__this->get_tweenPlugin_59();
		NoOptions_t32144009  L_17 = (NoOptions_t32144009 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t1103364673 *)__this)->get_isRelative_27();
		DOGetter_1_t1541575863 * L_19 = (DOGetter_1_t1541575863 *)__this->get_getter_57();
		DOSetter_1_t2578215691 * L_20 = (DOSetter_1_t2578215691 *)__this->get_setter_58();
		float L_21 = V_0;
		int64_t L_22 = (int64_t)__this->get_startValue_53();
		int64_t L_23 = (int64_t)__this->get_changeValue_55();
		float L_24 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t1801672336 *)L_16);
		VirtActionInvoker11< NoOptions_t32144009 , Tween_t1103364673 *, bool, DOGetter_1_t1541575863 *, DOSetter_1_t2578215691 *, float, int64_t, int64_t, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t1801672336 *)L_16, (NoOptions_t32144009 )L_17, (Tween_t1103364673 *)__this, (bool)L_18, (DOGetter_1_t1541575863 *)L_19, (DOSetter_1_t2578215691 *)L_20, (float)L_21, (int64_t)L_22, (int64_t)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3__ctor_m724780733_MetadataUsageId;
extern "C"  void TweenerCore_3__ctor_m724780733_gshared (TweenerCore_3_t1619290041 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m724780733_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Tweener_t1766303790 *)__this);
		Tweener__ctor_m1348308877((Tweener_t1766303790 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2598715689 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t1103364673 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t1103364673 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::ChangeStartValue(System.Object,System.Single)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1899955427;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeStartValue_m403069594_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeStartValue_m403069594_gshared (TweenerCore_3_t1619290041 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m403069594_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_7 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, _stringLiteral1899955427);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1899955427);
		ObjectU5BU5D_t11523773* L_8 = (ObjectU5BU5D_t11523773*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		ArrayElementTypeCheck (L_10, _stringLiteral3718369476);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_11 = (ObjectU5BU5D_t11523773*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 3);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_11;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral41);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t1766303790 * L_17 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1619290041 *, Il2CppObject *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, (TweenerCore_3_t1619290041 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_15, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeEndValue_m3046011663_gshared (TweenerCore_3_t1619290041 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t1766303790 *)__this);
		Tweener_t1766303790 * L_2 = VirtFuncInvoker3< Tweener_t1766303790 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t1766303790 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1647271171;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeEndValue_m3999955466_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeEndValue_m3999955466_gshared (TweenerCore_3_t1619290041 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m3999955466_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_7 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, _stringLiteral1647271171);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1647271171);
		ObjectU5BU5D_t11523773* L_8 = (ObjectU5BU5D_t11523773*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		ArrayElementTypeCheck (L_10, _stringLiteral3718369476);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_11 = (ObjectU5BU5D_t11523773*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 3);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_11;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral41);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t1766303790 * L_18 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1619290041 *, Il2CppObject *, float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (TweenerCore_3_t1619290041 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_15, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1932880146;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeValues_m412813269_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeValues_m412813269_gshared (TweenerCore_3_t1619290041 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m412813269_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m2022236990((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_9 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		ArrayElementTypeCheck (L_9, _stringLiteral1932880146);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1932880146);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 1);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t11523773* L_12 = (ObjectU5BU5D_t11523773*)L_10;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 2);
		ArrayElementTypeCheck (L_12, _stringLiteral3718369476);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 3);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t11523773* L_15 = (ObjectU5BU5D_t11523773*)L_13;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 4);
		ArrayElementTypeCheck (L_15, _stringLiteral41);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_20 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 0);
		ArrayElementTypeCheck (L_20, _stringLiteral1932880146);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1932880146);
		ObjectU5BU5D_t11523773* L_21 = (ObjectU5BU5D_t11523773*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 1);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t11523773* L_23 = (ObjectU5BU5D_t11523773*)L_21;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 2);
		ArrayElementTypeCheck (L_23, _stringLiteral3718369476);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_24 = (ObjectU5BU5D_t11523773*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 3);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t11523773* L_26 = (ObjectU5BU5D_t11523773*)L_24;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 4);
		ArrayElementTypeCheck (L_26, _stringLiteral41);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t1766303790 * L_31 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1619290041 *, Il2CppObject *, Il2CppObject *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, (TweenerCore_3_t1619290041 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_28, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))), (Il2CppObject *)((Il2CppObject *)Castclass(L_29, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t1766303790 * TweenerCore_3_SetFrom_m902175493_gshared (TweenerCore_3_t1619290041 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t2512169016 * L_0 = (ABSTweenPlugin_3_t2512169016 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t2512169016 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t1619290041 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t2512169016 *)L_0, (TweenerCore_3_t1619290041 *)__this, (bool)L_1);
		((Tweener_t1766303790 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m2666180970_gshared (TweenerCore_3_t1619290041 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t1103364673 *)__this);
		Tween_Reset_m1561947655((Tween_t1103364673 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t2512169016 * L_0 = (ABSTweenPlugin_3_t2512169016 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t2512169016 * L_1 = (ABSTweenPlugin_3_t2512169016 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t2512169016 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t1619290041 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t2512169016 *)L_1, (TweenerCore_3_t1619290041 *)__this);
	}

IL_001a:
	{
		NoOptions_t32144009  L_2 = ((  NoOptions_t32144009  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t3826234697 *)NULL);
		__this->set_setter_58((DOSetter_1_t567907229 *)NULL);
		((Tweener_t1766303790 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t1766303790 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::Validate()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_Validate_m547623909_MetadataUsageId;
extern "C"  bool TweenerCore_3_Validate_m547623909_gshared (TweenerCore_3_t1619290041 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m547623909_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t3826234697 * L_0 = (DOGetter_1_t3826234697 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t3826234697 *)L_0);
		((  Il2CppObject * (*) (DOGetter_1_t3826234697 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((DOGetter_1_t3826234697 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m1656907242_gshared (TweenerCore_3_t1619290041 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1619290041 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(NULL /*static, unused*/, (TweenerCore_3_t1619290041 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m1077352240_gshared (TweenerCore_3_t1619290041 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1619290041 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)(NULL /*static, unused*/, (TweenerCore_3_t1619290041 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern Il2CppClass* DOTween_t3585775766_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_ApplyTween_m591312299_MetadataUsageId;
extern "C"  bool TweenerCore_3_ApplyTween_m591312299_gshared (TweenerCore_3_t1619290041 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m591312299_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t1103364673 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t1103364673 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t3585775766_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t3585775766_StaticFields*)DOTween_t3585775766_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t2512169016 * L_5 = (ABSTweenPlugin_3_t2512169016 *)__this->get_tweenPlugin_59();
		NoOptions_t32144009  L_6 = (NoOptions_t32144009 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t1103364673 *)__this)->get_isRelative_27();
		DOGetter_1_t3826234697 * L_8 = (DOGetter_1_t3826234697 *)__this->get_getter_57();
		DOSetter_1_t567907229 * L_9 = (DOSetter_1_t567907229 *)__this->get_setter_58();
		float L_10 = V_0;
		Il2CppObject * L_11 = (Il2CppObject *)__this->get_startValue_53();
		Il2CppObject * L_12 = (Il2CppObject *)__this->get_changeValue_55();
		float L_13 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t2512169016 *)L_5);
		VirtActionInvoker11< NoOptions_t32144009 , Tween_t1103364673 *, bool, DOGetter_1_t3826234697 *, DOSetter_1_t567907229 *, float, Il2CppObject *, Il2CppObject *, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t2512169016 *)L_5, (NoOptions_t32144009 )L_6, (Tween_t1103364673 *)__this, (bool)L_7, (DOGetter_1_t3826234697 *)L_8, (DOSetter_1_t567907229 *)L_9, (float)L_10, (Il2CppObject *)L_11, (Il2CppObject *)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t2512169016 * L_16 = (ABSTweenPlugin_3_t2512169016 *)__this->get_tweenPlugin_59();
		NoOptions_t32144009  L_17 = (NoOptions_t32144009 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t1103364673 *)__this)->get_isRelative_27();
		DOGetter_1_t3826234697 * L_19 = (DOGetter_1_t3826234697 *)__this->get_getter_57();
		DOSetter_1_t567907229 * L_20 = (DOSetter_1_t567907229 *)__this->get_setter_58();
		float L_21 = V_0;
		Il2CppObject * L_22 = (Il2CppObject *)__this->get_startValue_53();
		Il2CppObject * L_23 = (Il2CppObject *)__this->get_changeValue_55();
		float L_24 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t2512169016 *)L_16);
		VirtActionInvoker11< NoOptions_t32144009 , Tween_t1103364673 *, bool, DOGetter_1_t3826234697 *, DOSetter_1_t567907229 *, float, Il2CppObject *, Il2CppObject *, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t2512169016 *)L_16, (NoOptions_t32144009 )L_17, (Tween_t1103364673 *)__this, (bool)L_18, (DOGetter_1_t3826234697 *)L_19, (DOSetter_1_t567907229 *)L_20, (float)L_21, (Il2CppObject *)L_22, (Il2CppObject *)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::.ctor()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3__ctor_m1500756205_MetadataUsageId;
extern "C"  void TweenerCore_3__ctor_m1500756205_gshared (TweenerCore_3_t4210682729 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m1500756205_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Tweener_t1766303790 *)__this);
		Tweener__ctor_m1348308877((Tweener_t1766303790 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2598715689 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t1103364673 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t1103364673 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::ChangeStartValue(System.Object,System.Single)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1899955427;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeStartValue_m392154442_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeStartValue_m392154442_gshared (TweenerCore_3_t4210682729 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m392154442_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_7 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, _stringLiteral1899955427);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1899955427);
		ObjectU5BU5D_t11523773* L_8 = (ObjectU5BU5D_t11523773*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		ArrayElementTypeCheck (L_10, _stringLiteral3718369476);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_11 = (ObjectU5BU5D_t11523773*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 3);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_11;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral41);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t1766303790 * L_17 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t4210682729 *, Il2CppObject *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, (TweenerCore_3_t4210682729 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_15, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeEndValue_m551807583_gshared (TweenerCore_3_t4210682729 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t1766303790 *)__this);
		Tweener_t1766303790 * L_2 = VirtFuncInvoker3< Tweener_t1766303790 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t1766303790 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1647271171;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeEndValue_m1945429338_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeEndValue_m1945429338_gshared (TweenerCore_3_t4210682729 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m1945429338_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_7 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, _stringLiteral1647271171);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1647271171);
		ObjectU5BU5D_t11523773* L_8 = (ObjectU5BU5D_t11523773*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		ArrayElementTypeCheck (L_10, _stringLiteral3718369476);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_11 = (ObjectU5BU5D_t11523773*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 3);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_11;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral41);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t1766303790 * L_18 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t4210682729 *, Il2CppObject *, float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (TweenerCore_3_t4210682729 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_15, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1932880146;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeValues_m2978537093_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeValues_m2978537093_gshared (TweenerCore_3_t4210682729 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m2978537093_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m2022236990((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_9 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		ArrayElementTypeCheck (L_9, _stringLiteral1932880146);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1932880146);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 1);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t11523773* L_12 = (ObjectU5BU5D_t11523773*)L_10;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 2);
		ArrayElementTypeCheck (L_12, _stringLiteral3718369476);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 3);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t11523773* L_15 = (ObjectU5BU5D_t11523773*)L_13;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 4);
		ArrayElementTypeCheck (L_15, _stringLiteral41);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_20 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 0);
		ArrayElementTypeCheck (L_20, _stringLiteral1932880146);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1932880146);
		ObjectU5BU5D_t11523773* L_21 = (ObjectU5BU5D_t11523773*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 1);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t11523773* L_23 = (ObjectU5BU5D_t11523773*)L_21;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 2);
		ArrayElementTypeCheck (L_23, _stringLiteral3718369476);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_24 = (ObjectU5BU5D_t11523773*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 3);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t11523773* L_26 = (ObjectU5BU5D_t11523773*)L_24;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 4);
		ArrayElementTypeCheck (L_26, _stringLiteral41);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t1766303790 * L_31 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t4210682729 *, Il2CppObject *, Il2CppObject *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, (TweenerCore_3_t4210682729 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_28, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))), (Il2CppObject *)((Il2CppObject *)Castclass(L_29, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t1766303790 * TweenerCore_3_SetFrom_m2990191029_gshared (TweenerCore_3_t4210682729 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t808594408 * L_0 = (ABSTweenPlugin_3_t808594408 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t808594408 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t4210682729 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t808594408 *)L_0, (TweenerCore_3_t4210682729 *)__this, (bool)L_1);
		((Tweener_t1766303790 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m3442156442_gshared (TweenerCore_3_t4210682729 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t1103364673 *)__this);
		Tween_Reset_m1561947655((Tween_t1103364673 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t808594408 * L_0 = (ABSTweenPlugin_3_t808594408 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t808594408 * L_1 = (ABSTweenPlugin_3_t808594408 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t808594408 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t4210682729 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t808594408 *)L_1, (TweenerCore_3_t4210682729 *)__this);
	}

IL_001a:
	{
		StringOptions_t2623536697  L_2 = ((  StringOptions_t2623536697  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t3826234697 *)NULL);
		__this->set_setter_58((DOSetter_1_t567907229 *)NULL);
		((Tweener_t1766303790 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t1766303790 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::Validate()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_Validate_m3278749621_MetadataUsageId;
extern "C"  bool TweenerCore_3_Validate_m3278749621_gshared (TweenerCore_3_t4210682729 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m3278749621_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t3826234697 * L_0 = (DOGetter_1_t3826234697 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t3826234697 *)L_0);
		((  Il2CppObject * (*) (DOGetter_1_t3826234697 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((DOGetter_1_t3826234697 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m4066421690_gshared (TweenerCore_3_t4210682729 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t4210682729 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(NULL /*static, unused*/, (TweenerCore_3_t4210682729 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m3936399712_gshared (TweenerCore_3_t4210682729 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t4210682729 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)(NULL /*static, unused*/, (TweenerCore_3_t4210682729 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern Il2CppClass* DOTween_t3585775766_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_ApplyTween_m2069108091_MetadataUsageId;
extern "C"  bool TweenerCore_3_ApplyTween_m2069108091_gshared (TweenerCore_3_t4210682729 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m2069108091_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t1103364673 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t1103364673 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t3585775766_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t3585775766_StaticFields*)DOTween_t3585775766_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t808594408 * L_5 = (ABSTweenPlugin_3_t808594408 *)__this->get_tweenPlugin_59();
		StringOptions_t2623536697  L_6 = (StringOptions_t2623536697 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t1103364673 *)__this)->get_isRelative_27();
		DOGetter_1_t3826234697 * L_8 = (DOGetter_1_t3826234697 *)__this->get_getter_57();
		DOSetter_1_t567907229 * L_9 = (DOSetter_1_t567907229 *)__this->get_setter_58();
		float L_10 = V_0;
		Il2CppObject * L_11 = (Il2CppObject *)__this->get_startValue_53();
		Il2CppObject * L_12 = (Il2CppObject *)__this->get_changeValue_55();
		float L_13 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t808594408 *)L_5);
		VirtActionInvoker11< StringOptions_t2623536697 , Tween_t1103364673 *, bool, DOGetter_1_t3826234697 *, DOSetter_1_t567907229 *, float, Il2CppObject *, Il2CppObject *, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t808594408 *)L_5, (StringOptions_t2623536697 )L_6, (Tween_t1103364673 *)__this, (bool)L_7, (DOGetter_1_t3826234697 *)L_8, (DOSetter_1_t567907229 *)L_9, (float)L_10, (Il2CppObject *)L_11, (Il2CppObject *)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t808594408 * L_16 = (ABSTweenPlugin_3_t808594408 *)__this->get_tweenPlugin_59();
		StringOptions_t2623536697  L_17 = (StringOptions_t2623536697 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t1103364673 *)__this)->get_isRelative_27();
		DOGetter_1_t3826234697 * L_19 = (DOGetter_1_t3826234697 *)__this->get_getter_57();
		DOSetter_1_t567907229 * L_20 = (DOSetter_1_t567907229 *)__this->get_setter_58();
		float L_21 = V_0;
		Il2CppObject * L_22 = (Il2CppObject *)__this->get_startValue_53();
		Il2CppObject * L_23 = (Il2CppObject *)__this->get_changeValue_55();
		float L_24 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t808594408 *)L_16);
		VirtActionInvoker11< StringOptions_t2623536697 , Tween_t1103364673 *, bool, DOGetter_1_t3826234697 *, DOSetter_1_t567907229 *, float, Il2CppObject *, Il2CppObject *, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t808594408 *)L_16, (StringOptions_t2623536697 )L_17, (Tween_t1103364673 *)__this, (bool)L_18, (DOGetter_1_t3826234697 *)L_19, (DOSetter_1_t567907229 *)L_20, (float)L_21, (Il2CppObject *)L_22, (Il2CppObject *)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::.ctor()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3__ctor_m4207454042_MetadataUsageId;
extern "C"  void TweenerCore_3__ctor_m4207454042_gshared (TweenerCore_3_t2841494226 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m4207454042_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Tweener_t1766303790 *)__this);
		Tweener__ctor_m1348308877((Tweener_t1766303790 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2598715689 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t1103364673 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t1103364673 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::ChangeStartValue(System.Object,System.Single)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1899955427;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeStartValue_m4171514377_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeStartValue_m4171514377_gshared (TweenerCore_3_t2841494226 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m4171514377_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_7 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, _stringLiteral1899955427);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1899955427);
		ObjectU5BU5D_t11523773* L_8 = (ObjectU5BU5D_t11523773*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		ArrayElementTypeCheck (L_10, _stringLiteral3718369476);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_11 = (ObjectU5BU5D_t11523773*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 3);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_11;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral41);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t1766303790 * L_17 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2841494226 *, float, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, (TweenerCore_3_t2841494226 *)__this, (float)((*(float*)((float*)UnBox (L_15, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeEndValue_m1089364416_gshared (TweenerCore_3_t2841494226 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t1766303790 *)__this);
		Tweener_t1766303790 * L_2 = VirtFuncInvoker3< Tweener_t1766303790 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t1766303790 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1647271171;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeEndValue_m3305529083_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeEndValue_m3305529083_gshared (TweenerCore_3_t2841494226 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m3305529083_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_7 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, _stringLiteral1647271171);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1647271171);
		ObjectU5BU5D_t11523773* L_8 = (ObjectU5BU5D_t11523773*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		ArrayElementTypeCheck (L_10, _stringLiteral3718369476);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_11 = (ObjectU5BU5D_t11523773*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 3);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_11;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral41);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t1766303790 * L_18 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2841494226 *, float, float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (TweenerCore_3_t2841494226 *)__this, (float)((*(float*)((float*)UnBox (L_15, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1932880146;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeValues_m2238558340_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeValues_m2238558340_gshared (TweenerCore_3_t2841494226 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m2238558340_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m2022236990((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_9 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		ArrayElementTypeCheck (L_9, _stringLiteral1932880146);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1932880146);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 1);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t11523773* L_12 = (ObjectU5BU5D_t11523773*)L_10;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 2);
		ArrayElementTypeCheck (L_12, _stringLiteral3718369476);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 3);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t11523773* L_15 = (ObjectU5BU5D_t11523773*)L_13;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 4);
		ArrayElementTypeCheck (L_15, _stringLiteral41);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_20 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 0);
		ArrayElementTypeCheck (L_20, _stringLiteral1932880146);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1932880146);
		ObjectU5BU5D_t11523773* L_21 = (ObjectU5BU5D_t11523773*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 1);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t11523773* L_23 = (ObjectU5BU5D_t11523773*)L_21;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 2);
		ArrayElementTypeCheck (L_23, _stringLiteral3718369476);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_24 = (ObjectU5BU5D_t11523773*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 3);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t11523773* L_26 = (ObjectU5BU5D_t11523773*)L_24;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 4);
		ArrayElementTypeCheck (L_26, _stringLiteral41);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t1766303790 * L_31 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2841494226 *, float, float, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, (TweenerCore_3_t2841494226 *)__this, (float)((*(float*)((float*)UnBox (L_28, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (float)((*(float*)((float*)UnBox (L_29, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t1766303790 * TweenerCore_3_SetFrom_m699350964_gshared (TweenerCore_3_t2841494226 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t3734373201 * L_0 = (ABSTweenPlugin_3_t3734373201 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t3734373201 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t2841494226 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t3734373201 *)L_0, (TweenerCore_3_t2841494226 *)__this, (bool)L_1);
		((Tweener_t1766303790 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m1853886983_gshared (TweenerCore_3_t2841494226 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t1103364673 *)__this);
		Tween_Reset_m1561947655((Tween_t1103364673 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t3734373201 * L_0 = (ABSTweenPlugin_3_t3734373201 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t3734373201 * L_1 = (ABSTweenPlugin_3_t3734373201 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t3734373201 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t2841494226 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t3734373201 *)L_1, (TweenerCore_3_t2841494226 *)__this);
	}

IL_001a:
	{
		FloatOptions_t3779806670  L_2 = ((  FloatOptions_t3779806670  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t3947337298 *)NULL);
		__this->set_setter_58((DOSetter_1_t689009830 *)NULL);
		((Tweener_t1766303790 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t1766303790 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::Validate()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_Validate_m558299224_MetadataUsageId;
extern "C"  bool TweenerCore_3_Validate_m558299224_gshared (TweenerCore_3_t2841494226 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m558299224_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t3947337298 * L_0 = (DOGetter_1_t3947337298 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t3947337298 *)L_0);
		((  float (*) (DOGetter_1_t3947337298 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((DOGetter_1_t3947337298 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m4260228421_gshared (TweenerCore_3_t2841494226 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2841494226 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(NULL /*static, unused*/, (TweenerCore_3_t2841494226 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m1077696605_gshared (TweenerCore_3_t2841494226 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2841494226 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)(NULL /*static, unused*/, (TweenerCore_3_t2841494226 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern Il2CppClass* DOTween_t3585775766_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_ApplyTween_m65006302_MetadataUsageId;
extern "C"  bool TweenerCore_3_ApplyTween_m65006302_gshared (TweenerCore_3_t2841494226 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m65006302_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t1103364673 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t1103364673 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t3585775766_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t3585775766_StaticFields*)DOTween_t3585775766_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t3734373201 * L_5 = (ABSTweenPlugin_3_t3734373201 *)__this->get_tweenPlugin_59();
		FloatOptions_t3779806670  L_6 = (FloatOptions_t3779806670 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t1103364673 *)__this)->get_isRelative_27();
		DOGetter_1_t3947337298 * L_8 = (DOGetter_1_t3947337298 *)__this->get_getter_57();
		DOSetter_1_t689009830 * L_9 = (DOSetter_1_t689009830 *)__this->get_setter_58();
		float L_10 = V_0;
		float L_11 = (float)__this->get_startValue_53();
		float L_12 = (float)__this->get_changeValue_55();
		float L_13 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t3734373201 *)L_5);
		VirtActionInvoker11< FloatOptions_t3779806670 , Tween_t1103364673 *, bool, DOGetter_1_t3947337298 *, DOSetter_1_t689009830 *, float, float, float, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t3734373201 *)L_5, (FloatOptions_t3779806670 )L_6, (Tween_t1103364673 *)__this, (bool)L_7, (DOGetter_1_t3947337298 *)L_8, (DOSetter_1_t689009830 *)L_9, (float)L_10, (float)L_11, (float)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t3734373201 * L_16 = (ABSTweenPlugin_3_t3734373201 *)__this->get_tweenPlugin_59();
		FloatOptions_t3779806670  L_17 = (FloatOptions_t3779806670 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t1103364673 *)__this)->get_isRelative_27();
		DOGetter_1_t3947337298 * L_19 = (DOGetter_1_t3947337298 *)__this->get_getter_57();
		DOSetter_1_t689009830 * L_20 = (DOSetter_1_t689009830 *)__this->get_setter_58();
		float L_21 = V_0;
		float L_22 = (float)__this->get_startValue_53();
		float L_23 = (float)__this->get_changeValue_55();
		float L_24 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t3734373201 *)L_16);
		VirtActionInvoker11< FloatOptions_t3779806670 , Tween_t1103364673 *, bool, DOGetter_1_t3947337298 *, DOSetter_1_t689009830 *, float, float, float, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t3734373201 *)L_16, (FloatOptions_t3779806670 )L_17, (Tween_t1103364673 *)__this, (bool)L_18, (DOGetter_1_t3947337298 *)L_19, (DOSetter_1_t689009830 *)L_20, (float)L_21, (float)L_22, (float)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3__ctor_m1724997873_MetadataUsageId;
extern "C"  void TweenerCore_3__ctor_m1724997873_gshared (TweenerCore_3_t4224275457 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m1724997873_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Tweener_t1766303790 *)__this);
		Tweener__ctor_m1348308877((Tweener_t1766303790 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2598715689 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t1103364673 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t1103364673 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>::ChangeStartValue(System.Object,System.Single)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1899955427;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeStartValue_m2591239630_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeStartValue_m2591239630_gshared (TweenerCore_3_t4224275457 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m2591239630_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_7 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, _stringLiteral1899955427);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1899955427);
		ObjectU5BU5D_t11523773* L_8 = (ObjectU5BU5D_t11523773*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		ArrayElementTypeCheck (L_10, _stringLiteral3718369476);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_11 = (ObjectU5BU5D_t11523773*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 3);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_11;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral41);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t1766303790 * L_17 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t4224275457 *, uint32_t, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, (TweenerCore_3_t4224275457 *)__this, (uint32_t)((*(uint32_t*)((uint32_t*)UnBox (L_15, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeEndValue_m761293147_gshared (TweenerCore_3_t4224275457 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t1766303790 *)__this);
		Tweener_t1766303790 * L_2 = VirtFuncInvoker3< Tweener_t1766303790 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t1766303790 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1647271171;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeEndValue_m1773763414_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeEndValue_m1773763414_gshared (TweenerCore_3_t4224275457 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m1773763414_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_7 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, _stringLiteral1647271171);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1647271171);
		ObjectU5BU5D_t11523773* L_8 = (ObjectU5BU5D_t11523773*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		ArrayElementTypeCheck (L_10, _stringLiteral3718369476);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_11 = (ObjectU5BU5D_t11523773*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 3);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_11;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral41);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t1766303790 * L_18 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t4224275457 *, uint32_t, float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (TweenerCore_3_t4224275457 *)__this, (uint32_t)((*(uint32_t*)((uint32_t*)UnBox (L_15, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1932880146;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeValues_m1104177673_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeValues_m1104177673_gshared (TweenerCore_3_t4224275457 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m1104177673_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m2022236990((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_9 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		ArrayElementTypeCheck (L_9, _stringLiteral1932880146);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1932880146);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 1);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t11523773* L_12 = (ObjectU5BU5D_t11523773*)L_10;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 2);
		ArrayElementTypeCheck (L_12, _stringLiteral3718369476);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 3);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t11523773* L_15 = (ObjectU5BU5D_t11523773*)L_13;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 4);
		ArrayElementTypeCheck (L_15, _stringLiteral41);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_20 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 0);
		ArrayElementTypeCheck (L_20, _stringLiteral1932880146);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1932880146);
		ObjectU5BU5D_t11523773* L_21 = (ObjectU5BU5D_t11523773*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 1);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t11523773* L_23 = (ObjectU5BU5D_t11523773*)L_21;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 2);
		ArrayElementTypeCheck (L_23, _stringLiteral3718369476);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_24 = (ObjectU5BU5D_t11523773*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 3);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t11523773* L_26 = (ObjectU5BU5D_t11523773*)L_24;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 4);
		ArrayElementTypeCheck (L_26, _stringLiteral41);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t1766303790 * L_31 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t4224275457 *, uint32_t, uint32_t, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, (TweenerCore_3_t4224275457 *)__this, (uint32_t)((*(uint32_t*)((uint32_t*)UnBox (L_28, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (uint32_t)((*(uint32_t*)((uint32_t*)UnBox (L_29, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t1766303790 * TweenerCore_3_SetFrom_m410078521_gshared (TweenerCore_3_t4224275457 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t822187136 * L_0 = (ABSTweenPlugin_3_t822187136 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t822187136 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t4224275457 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t822187136 *)L_0, (TweenerCore_3_t4224275457 *)__this, (bool)L_1);
		((Tweener_t1766303790 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m3666398110_gshared (TweenerCore_3_t4224275457 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t1103364673 *)__this);
		Tween_Reset_m1561947655((Tween_t1103364673 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t822187136 * L_0 = (ABSTweenPlugin_3_t822187136 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t822187136 * L_1 = (ABSTweenPlugin_3_t822187136 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t822187136 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t4224275457 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t822187136 *)L_1, (TweenerCore_3_t4224275457 *)__this);
	}

IL_001a:
	{
		NoOptions_t32144009  L_2 = ((  NoOptions_t32144009  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t3975053603 *)NULL);
		__this->set_setter_58((DOSetter_1_t716726135 *)NULL);
		((Tweener_t1766303790 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t1766303790 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>::Validate()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_Validate_m3828309297_MetadataUsageId;
extern "C"  bool TweenerCore_3_Validate_m3828309297_gshared (TweenerCore_3_t4224275457 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m3828309297_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t3975053603 * L_0 = (DOGetter_1_t3975053603 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t3975053603 *)L_0);
		((  uint32_t (*) (DOGetter_1_t3975053603 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((DOGetter_1_t3975053603 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m319395638_gshared (TweenerCore_3_t4224275457 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t4224275457 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(NULL /*static, unused*/, (TweenerCore_3_t4224275457 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m213349476_gshared (TweenerCore_3_t4224275457 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t4224275457 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)(NULL /*static, unused*/, (TweenerCore_3_t4224275457 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern Il2CppClass* DOTween_t3585775766_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_ApplyTween_m2035491319_MetadataUsageId;
extern "C"  bool TweenerCore_3_ApplyTween_m2035491319_gshared (TweenerCore_3_t4224275457 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m2035491319_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t1103364673 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t1103364673 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t3585775766_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t3585775766_StaticFields*)DOTween_t3585775766_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t822187136 * L_5 = (ABSTweenPlugin_3_t822187136 *)__this->get_tweenPlugin_59();
		NoOptions_t32144009  L_6 = (NoOptions_t32144009 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t1103364673 *)__this)->get_isRelative_27();
		DOGetter_1_t3975053603 * L_8 = (DOGetter_1_t3975053603 *)__this->get_getter_57();
		DOSetter_1_t716726135 * L_9 = (DOSetter_1_t716726135 *)__this->get_setter_58();
		float L_10 = V_0;
		uint32_t L_11 = (uint32_t)__this->get_startValue_53();
		uint32_t L_12 = (uint32_t)__this->get_changeValue_55();
		float L_13 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t822187136 *)L_5);
		VirtActionInvoker11< NoOptions_t32144009 , Tween_t1103364673 *, bool, DOGetter_1_t3975053603 *, DOSetter_1_t716726135 *, float, uint32_t, uint32_t, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t822187136 *)L_5, (NoOptions_t32144009 )L_6, (Tween_t1103364673 *)__this, (bool)L_7, (DOGetter_1_t3975053603 *)L_8, (DOSetter_1_t716726135 *)L_9, (float)L_10, (uint32_t)L_11, (uint32_t)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t822187136 * L_16 = (ABSTweenPlugin_3_t822187136 *)__this->get_tweenPlugin_59();
		NoOptions_t32144009  L_17 = (NoOptions_t32144009 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t1103364673 *)__this)->get_isRelative_27();
		DOGetter_1_t3975053603 * L_19 = (DOGetter_1_t3975053603 *)__this->get_getter_57();
		DOSetter_1_t716726135 * L_20 = (DOSetter_1_t716726135 *)__this->get_setter_58();
		float L_21 = V_0;
		uint32_t L_22 = (uint32_t)__this->get_startValue_53();
		uint32_t L_23 = (uint32_t)__this->get_changeValue_55();
		float L_24 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t822187136 *)L_16);
		VirtActionInvoker11< NoOptions_t32144009 , Tween_t1103364673 *, bool, DOGetter_1_t3975053603 *, DOSetter_1_t716726135 *, float, uint32_t, uint32_t, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t822187136 *)L_16, (NoOptions_t32144009 )L_17, (Tween_t1103364673 *)__this, (bool)L_18, (DOGetter_1_t3975053603 *)L_19, (DOSetter_1_t716726135 *)L_20, (float)L_21, (uint32_t)L_22, (uint32_t)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3__ctor_m2636356463_MetadataUsageId;
extern "C"  void TweenerCore_3__ctor_m2636356463_gshared (TweenerCore_3_t2578226509 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m2636356463_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Tweener_t1766303790 *)__this);
		Tweener__ctor_m1348308877((Tweener_t1766303790 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2598715689 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t1103364673 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t1103364673 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::ChangeStartValue(System.Object,System.Single)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1899955427;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeStartValue_m2848168396_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeStartValue_m2848168396_gshared (TweenerCore_3_t2578226509 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m2848168396_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_7 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, _stringLiteral1899955427);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1899955427);
		ObjectU5BU5D_t11523773* L_8 = (ObjectU5BU5D_t11523773*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		ArrayElementTypeCheck (L_10, _stringLiteral3718369476);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_11 = (ObjectU5BU5D_t11523773*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 3);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_11;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral41);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t1766303790 * L_17 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2578226509 *, uint64_t, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, (TweenerCore_3_t2578226509 *)__this, (uint64_t)((*(uint64_t*)((uint64_t*)UnBox (L_15, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeEndValue_m2155054493_gshared (TweenerCore_3_t2578226509 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t1766303790 *)__this);
		Tweener_t1766303790 * L_2 = VirtFuncInvoker3< Tweener_t1766303790 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t1766303790 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1647271171;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeEndValue_m2310863896_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeEndValue_m2310863896_gshared (TweenerCore_3_t2578226509 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m2310863896_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_7 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, _stringLiteral1647271171);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1647271171);
		ObjectU5BU5D_t11523773* L_8 = (ObjectU5BU5D_t11523773*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		ArrayElementTypeCheck (L_10, _stringLiteral3718369476);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_11 = (ObjectU5BU5D_t11523773*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 3);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_11;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral41);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t1766303790 * L_18 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2578226509 *, uint64_t, float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (TweenerCore_3_t2578226509 *)__this, (uint64_t)((*(uint64_t*)((uint64_t*)UnBox (L_15, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1932880146;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeValues_m3200282759_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeValues_m3200282759_gshared (TweenerCore_3_t2578226509 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m3200282759_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m2022236990((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_9 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		ArrayElementTypeCheck (L_9, _stringLiteral1932880146);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1932880146);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 1);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t11523773* L_12 = (ObjectU5BU5D_t11523773*)L_10;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 2);
		ArrayElementTypeCheck (L_12, _stringLiteral3718369476);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 3);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t11523773* L_15 = (ObjectU5BU5D_t11523773*)L_13;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 4);
		ArrayElementTypeCheck (L_15, _stringLiteral41);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_20 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 0);
		ArrayElementTypeCheck (L_20, _stringLiteral1932880146);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1932880146);
		ObjectU5BU5D_t11523773* L_21 = (ObjectU5BU5D_t11523773*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 1);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t11523773* L_23 = (ObjectU5BU5D_t11523773*)L_21;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 2);
		ArrayElementTypeCheck (L_23, _stringLiteral3718369476);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_24 = (ObjectU5BU5D_t11523773*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 3);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t11523773* L_26 = (ObjectU5BU5D_t11523773*)L_24;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 4);
		ArrayElementTypeCheck (L_26, _stringLiteral41);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t1766303790 * L_31 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2578226509 *, uint64_t, uint64_t, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, (TweenerCore_3_t2578226509 *)__this, (uint64_t)((*(uint64_t*)((uint64_t*)UnBox (L_28, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (uint64_t)((*(uint64_t*)((uint64_t*)UnBox (L_29, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t1766303790 * TweenerCore_3_SetFrom_m2594884535_gshared (TweenerCore_3_t2578226509 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t3471105484 * L_0 = (ABSTweenPlugin_3_t3471105484 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t3471105484 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t2578226509 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t3471105484 *)L_0, (TweenerCore_3_t2578226509 *)__this, (bool)L_1);
		((Tweener_t1766303790 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m282789404_gshared (TweenerCore_3_t2578226509 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t1103364673 *)__this);
		Tween_Reset_m1561947655((Tween_t1103364673 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t3471105484 * L_0 = (ABSTweenPlugin_3_t3471105484 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t3471105484 * L_1 = (ABSTweenPlugin_3_t3471105484 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t3471105484 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t2578226509 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t3471105484 *)L_1, (TweenerCore_3_t2578226509 *)__this);
	}

IL_001a:
	{
		NoOptions_t32144009  L_2 = ((  NoOptions_t32144009  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t3975053698 *)NULL);
		__this->set_setter_58((DOSetter_1_t716726230 *)NULL);
		((Tweener_t1766303790 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t1766303790 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::Validate()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_Validate_m1328818675_MetadataUsageId;
extern "C"  bool TweenerCore_3_Validate_m1328818675_gshared (TweenerCore_3_t2578226509 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m1328818675_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t3975053698 * L_0 = (DOGetter_1_t3975053698 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t3975053698 *)L_0);
		((  uint64_t (*) (DOGetter_1_t3975053698 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((DOGetter_1_t3975053698 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m1940955128_gshared (TweenerCore_3_t2578226509 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2578226509 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(NULL /*static, unused*/, (TweenerCore_3_t2578226509 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m4150593378_gshared (TweenerCore_3_t2578226509 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2578226509 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)(NULL /*static, unused*/, (TweenerCore_3_t2578226509 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern Il2CppClass* DOTween_t3585775766_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_ApplyTween_m506599225_MetadataUsageId;
extern "C"  bool TweenerCore_3_ApplyTween_m506599225_gshared (TweenerCore_3_t2578226509 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m506599225_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t1103364673 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t1103364673 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t3585775766_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t3585775766_StaticFields*)DOTween_t3585775766_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t3471105484 * L_5 = (ABSTweenPlugin_3_t3471105484 *)__this->get_tweenPlugin_59();
		NoOptions_t32144009  L_6 = (NoOptions_t32144009 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t1103364673 *)__this)->get_isRelative_27();
		DOGetter_1_t3975053698 * L_8 = (DOGetter_1_t3975053698 *)__this->get_getter_57();
		DOSetter_1_t716726230 * L_9 = (DOSetter_1_t716726230 *)__this->get_setter_58();
		float L_10 = V_0;
		uint64_t L_11 = (uint64_t)__this->get_startValue_53();
		uint64_t L_12 = (uint64_t)__this->get_changeValue_55();
		float L_13 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t3471105484 *)L_5);
		VirtActionInvoker11< NoOptions_t32144009 , Tween_t1103364673 *, bool, DOGetter_1_t3975053698 *, DOSetter_1_t716726230 *, float, uint64_t, uint64_t, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t3471105484 *)L_5, (NoOptions_t32144009 )L_6, (Tween_t1103364673 *)__this, (bool)L_7, (DOGetter_1_t3975053698 *)L_8, (DOSetter_1_t716726230 *)L_9, (float)L_10, (uint64_t)L_11, (uint64_t)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t3471105484 * L_16 = (ABSTweenPlugin_3_t3471105484 *)__this->get_tweenPlugin_59();
		NoOptions_t32144009  L_17 = (NoOptions_t32144009 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t1103364673 *)__this)->get_isRelative_27();
		DOGetter_1_t3975053698 * L_19 = (DOGetter_1_t3975053698 *)__this->get_getter_57();
		DOSetter_1_t716726230 * L_20 = (DOSetter_1_t716726230 *)__this->get_setter_58();
		float L_21 = V_0;
		uint64_t L_22 = (uint64_t)__this->get_startValue_53();
		uint64_t L_23 = (uint64_t)__this->get_changeValue_55();
		float L_24 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t3471105484 *)L_16);
		VirtActionInvoker11< NoOptions_t32144009 , Tween_t1103364673 *, bool, DOGetter_1_t3975053698 *, DOSetter_1_t716726230 *, float, uint64_t, uint64_t, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t3471105484 *)L_16, (NoOptions_t32144009 )L_17, (Tween_t1103364673 *)__this, (bool)L_18, (DOGetter_1_t3975053698 *)L_19, (DOSetter_1_t716726230 *)L_20, (float)L_21, (uint64_t)L_22, (uint64_t)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::.ctor()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3__ctor_m253454185_MetadataUsageId;
extern "C"  void TweenerCore_3__ctor_m253454185_gshared (TweenerCore_3_t4289140679 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m253454185_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Tweener_t1766303790 *)__this);
		Tweener__ctor_m1348308877((Tweener_t1766303790 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2598715689 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t1103364673 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t1103364673 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::ChangeStartValue(System.Object,System.Single)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1899955427;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeStartValue_m701683160_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeStartValue_m701683160_gshared (TweenerCore_3_t4289140679 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m701683160_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_7 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, _stringLiteral1899955427);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1899955427);
		ObjectU5BU5D_t11523773* L_8 = (ObjectU5BU5D_t11523773*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		ArrayElementTypeCheck (L_10, _stringLiteral3718369476);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_11 = (ObjectU5BU5D_t11523773*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 3);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_11;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral41);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t1766303790 * L_17 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t4289140679 *, Color_t1588175760 , float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, (TweenerCore_3_t4289140679 *)__this, (Color_t1588175760 )((*(Color_t1588175760 *)((Color_t1588175760 *)UnBox (L_15, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeEndValue_m1254529041_gshared (TweenerCore_3_t4289140679 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t1766303790 *)__this);
		Tweener_t1766303790 * L_2 = VirtFuncInvoker3< Tweener_t1766303790 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t1766303790 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1647271171;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeEndValue_m1807004556_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeEndValue_m1807004556_gshared (TweenerCore_3_t4289140679 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m1807004556_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_7 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, _stringLiteral1647271171);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1647271171);
		ObjectU5BU5D_t11523773* L_8 = (ObjectU5BU5D_t11523773*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		ArrayElementTypeCheck (L_10, _stringLiteral3718369476);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_11 = (ObjectU5BU5D_t11523773*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 3);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_11;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral41);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t1766303790 * L_18 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t4289140679 *, Color_t1588175760 , float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (TweenerCore_3_t4289140679 *)__this, (Color_t1588175760 )((*(Color_t1588175760 *)((Color_t1588175760 *)UnBox (L_15, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1932880146;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeValues_m1286409619_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeValues_m1286409619_gshared (TweenerCore_3_t4289140679 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m1286409619_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m2022236990((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_9 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		ArrayElementTypeCheck (L_9, _stringLiteral1932880146);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1932880146);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 1);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t11523773* L_12 = (ObjectU5BU5D_t11523773*)L_10;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 2);
		ArrayElementTypeCheck (L_12, _stringLiteral3718369476);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 3);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t11523773* L_15 = (ObjectU5BU5D_t11523773*)L_13;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 4);
		ArrayElementTypeCheck (L_15, _stringLiteral41);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_20 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 0);
		ArrayElementTypeCheck (L_20, _stringLiteral1932880146);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1932880146);
		ObjectU5BU5D_t11523773* L_21 = (ObjectU5BU5D_t11523773*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 1);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t11523773* L_23 = (ObjectU5BU5D_t11523773*)L_21;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 2);
		ArrayElementTypeCheck (L_23, _stringLiteral3718369476);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_24 = (ObjectU5BU5D_t11523773*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 3);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t11523773* L_26 = (ObjectU5BU5D_t11523773*)L_24;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 4);
		ArrayElementTypeCheck (L_26, _stringLiteral41);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t1766303790 * L_31 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t4289140679 *, Color_t1588175760 , Color_t1588175760 , float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, (TweenerCore_3_t4289140679 *)__this, (Color_t1588175760 )((*(Color_t1588175760 *)((Color_t1588175760 *)UnBox (L_28, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (Color_t1588175760 )((*(Color_t1588175760 *)((Color_t1588175760 *)UnBox (L_29, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t1766303790 * TweenerCore_3_SetFrom_m1290705091_gshared (TweenerCore_3_t4289140679 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t887052358 * L_0 = (ABSTweenPlugin_3_t887052358 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t887052358 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t4289140679 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t887052358 *)L_0, (TweenerCore_3_t4289140679 *)__this, (bool)L_1);
		((Tweener_t1766303790 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m2194854422_gshared (TweenerCore_3_t4289140679 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t1103364673 *)__this);
		Tween_Reset_m1561947655((Tween_t1103364673 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t887052358 * L_0 = (ABSTweenPlugin_3_t887052358 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t887052358 * L_1 = (ABSTweenPlugin_3_t887052358 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t887052358 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t4289140679 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t887052358 *)L_1, (TweenerCore_3_t4289140679 *)__this);
	}

IL_001a:
	{
		ColorOptions_t4161000167  L_2 = ((  ColorOptions_t4161000167  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t282336741 *)NULL);
		__this->set_setter_58((DOSetter_1_t1318976569 *)NULL);
		((Tweener_t1766303790 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t1766303790 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::Validate()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_Validate_m237957609_MetadataUsageId;
extern "C"  bool TweenerCore_3_Validate_m237957609_gshared (TweenerCore_3_t4289140679 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m237957609_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t282336741 * L_0 = (DOGetter_1_t282336741 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t282336741 *)L_0);
		((  Color_t1588175760  (*) (DOGetter_1_t282336741 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((DOGetter_1_t282336741 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m2968957142_gshared (TweenerCore_3_t4289140679 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t4289140679 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(NULL /*static, unused*/, (TweenerCore_3_t4289140679 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m513173676_gshared (TweenerCore_3_t4289140679 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t4289140679 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)(NULL /*static, unused*/, (TweenerCore_3_t4289140679 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern Il2CppClass* DOTween_t3585775766_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_ApplyTween_m1513948335_MetadataUsageId;
extern "C"  bool TweenerCore_3_ApplyTween_m1513948335_gshared (TweenerCore_3_t4289140679 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m1513948335_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t1103364673 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t1103364673 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t3585775766_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t3585775766_StaticFields*)DOTween_t3585775766_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t887052358 * L_5 = (ABSTweenPlugin_3_t887052358 *)__this->get_tweenPlugin_59();
		ColorOptions_t4161000167  L_6 = (ColorOptions_t4161000167 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t1103364673 *)__this)->get_isRelative_27();
		DOGetter_1_t282336741 * L_8 = (DOGetter_1_t282336741 *)__this->get_getter_57();
		DOSetter_1_t1318976569 * L_9 = (DOSetter_1_t1318976569 *)__this->get_setter_58();
		float L_10 = V_0;
		Color_t1588175760  L_11 = (Color_t1588175760 )__this->get_startValue_53();
		Color_t1588175760  L_12 = (Color_t1588175760 )__this->get_changeValue_55();
		float L_13 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t887052358 *)L_5);
		VirtActionInvoker11< ColorOptions_t4161000167 , Tween_t1103364673 *, bool, DOGetter_1_t282336741 *, DOSetter_1_t1318976569 *, float, Color_t1588175760 , Color_t1588175760 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t887052358 *)L_5, (ColorOptions_t4161000167 )L_6, (Tween_t1103364673 *)__this, (bool)L_7, (DOGetter_1_t282336741 *)L_8, (DOSetter_1_t1318976569 *)L_9, (float)L_10, (Color_t1588175760 )L_11, (Color_t1588175760 )L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t887052358 * L_16 = (ABSTweenPlugin_3_t887052358 *)__this->get_tweenPlugin_59();
		ColorOptions_t4161000167  L_17 = (ColorOptions_t4161000167 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t1103364673 *)__this)->get_isRelative_27();
		DOGetter_1_t282336741 * L_19 = (DOGetter_1_t282336741 *)__this->get_getter_57();
		DOSetter_1_t1318976569 * L_20 = (DOSetter_1_t1318976569 *)__this->get_setter_58();
		float L_21 = V_0;
		Color_t1588175760  L_22 = (Color_t1588175760 )__this->get_startValue_53();
		Color_t1588175760  L_23 = (Color_t1588175760 )__this->get_changeValue_55();
		float L_24 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t887052358 *)L_16);
		VirtActionInvoker11< ColorOptions_t4161000167 , Tween_t1103364673 *, bool, DOGetter_1_t282336741 *, DOSetter_1_t1318976569 *, float, Color_t1588175760 , Color_t1588175760 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t887052358 *)L_16, (ColorOptions_t4161000167 )L_17, (Tween_t1103364673 *)__this, (bool)L_18, (DOGetter_1_t282336741 *)L_19, (DOSetter_1_t1318976569 *)L_20, (float)L_21, (Color_t1588175760 )L_22, (Color_t1588175760 )L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3__ctor_m499584377_MetadataUsageId;
extern "C"  void TweenerCore_3__ctor_m499584377_gshared (TweenerCore_3_t1968778341 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m499584377_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Tweener_t1766303790 *)__this);
		Tweener__ctor_m1348308877((Tweener_t1766303790 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2598715689 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t1103364673 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t1103364673 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>::ChangeStartValue(System.Object,System.Single)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1899955427;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeStartValue_m2839060630_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeStartValue_m2839060630_gshared (TweenerCore_3_t1968778341 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m2839060630_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_7 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, _stringLiteral1899955427);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1899955427);
		ObjectU5BU5D_t11523773* L_8 = (ObjectU5BU5D_t11523773*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		ArrayElementTypeCheck (L_10, _stringLiteral3718369476);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_11 = (ObjectU5BU5D_t11523773*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 3);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_11;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral41);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t1766303790 * L_17 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1968778341 *, Quaternion_t1891715979 , float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, (TweenerCore_3_t1968778341 *)__this, (Quaternion_t1891715979 )((*(Quaternion_t1891715979 *)((Quaternion_t1891715979 *)UnBox (L_15, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeEndValue_m3124592019_gshared (TweenerCore_3_t1968778341 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t1766303790 *)__this);
		Tweener_t1766303790 * L_2 = VirtFuncInvoker3< Tweener_t1766303790 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t1766303790 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1647271171;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeEndValue_m2082655630_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeEndValue_m2082655630_gshared (TweenerCore_3_t1968778341 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m2082655630_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_7 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, _stringLiteral1647271171);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1647271171);
		ObjectU5BU5D_t11523773* L_8 = (ObjectU5BU5D_t11523773*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		ArrayElementTypeCheck (L_10, _stringLiteral3718369476);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_11 = (ObjectU5BU5D_t11523773*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 3);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_11;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral41);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t1766303790 * L_18 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1968778341 *, Quaternion_t1891715979 , float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (TweenerCore_3_t1968778341 *)__this, (Quaternion_t1891715979 )((*(Quaternion_t1891715979 *)((Quaternion_t1891715979 *)UnBox (L_15, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1932880146;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeValues_m2291860177_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeValues_m2291860177_gshared (TweenerCore_3_t1968778341 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m2291860177_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m2022236990((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_9 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		ArrayElementTypeCheck (L_9, _stringLiteral1932880146);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1932880146);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 1);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t11523773* L_12 = (ObjectU5BU5D_t11523773*)L_10;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 2);
		ArrayElementTypeCheck (L_12, _stringLiteral3718369476);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 3);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t11523773* L_15 = (ObjectU5BU5D_t11523773*)L_13;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 4);
		ArrayElementTypeCheck (L_15, _stringLiteral41);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_20 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 0);
		ArrayElementTypeCheck (L_20, _stringLiteral1932880146);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1932880146);
		ObjectU5BU5D_t11523773* L_21 = (ObjectU5BU5D_t11523773*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 1);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t11523773* L_23 = (ObjectU5BU5D_t11523773*)L_21;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 2);
		ArrayElementTypeCheck (L_23, _stringLiteral3718369476);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_24 = (ObjectU5BU5D_t11523773*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 3);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t11523773* L_26 = (ObjectU5BU5D_t11523773*)L_24;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 4);
		ArrayElementTypeCheck (L_26, _stringLiteral41);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t1766303790 * L_31 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1968778341 *, Quaternion_t1891715979 , Quaternion_t1891715979 , float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, (TweenerCore_3_t1968778341 *)__this, (Quaternion_t1891715979 )((*(Quaternion_t1891715979 *)((Quaternion_t1891715979 *)UnBox (L_28, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (Quaternion_t1891715979 )((*(Quaternion_t1891715979 *)((Quaternion_t1891715979 *)UnBox (L_29, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t1766303790 * TweenerCore_3_SetFrom_m999360001_gshared (TweenerCore_3_t1968778341 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t2861657316 * L_0 = (ABSTweenPlugin_3_t2861657316 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t2861657316 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t1968778341 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t2861657316 *)L_0, (TweenerCore_3_t1968778341 *)__this, (bool)L_1);
		((Tweener_t1766303790 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m2440984614_gshared (TweenerCore_3_t1968778341 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t1103364673 *)__this);
		Tween_Reset_m1561947655((Tween_t1103364673 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t2861657316 * L_0 = (ABSTweenPlugin_3_t2861657316 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t2861657316 * L_1 = (ABSTweenPlugin_3_t2861657316 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t2861657316 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t1968778341 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t2861657316 *)L_1, (TweenerCore_3_t1968778341 *)__this);
	}

IL_001a:
	{
		NoOptions_t32144009  L_2 = ((  NoOptions_t32144009  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t585876960 *)NULL);
		__this->set_setter_58((DOSetter_1_t1622516788 *)NULL);
		((Tweener_t1766303790 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t1766303790 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>::Validate()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_Validate_m76790185_MetadataUsageId;
extern "C"  bool TweenerCore_3_Validate_m76790185_gshared (TweenerCore_3_t1968778341 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m76790185_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t585876960 * L_0 = (DOGetter_1_t585876960 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t585876960 *)L_0);
		((  Quaternion_t1891715979  (*) (DOGetter_1_t585876960 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((DOGetter_1_t585876960 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m3475482798_gshared (TweenerCore_3_t1968778341 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1968778341 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(NULL /*static, unused*/, (TweenerCore_3_t1968778341 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m1754900716_gshared (TweenerCore_3_t1968778341 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1968778341 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)(NULL /*static, unused*/, (TweenerCore_3_t1968778341 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern Il2CppClass* DOTween_t3585775766_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_ApplyTween_m3144989807_MetadataUsageId;
extern "C"  bool TweenerCore_3_ApplyTween_m3144989807_gshared (TweenerCore_3_t1968778341 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m3144989807_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t1103364673 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t1103364673 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t3585775766_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t3585775766_StaticFields*)DOTween_t3585775766_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t2861657316 * L_5 = (ABSTweenPlugin_3_t2861657316 *)__this->get_tweenPlugin_59();
		NoOptions_t32144009  L_6 = (NoOptions_t32144009 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t1103364673 *)__this)->get_isRelative_27();
		DOGetter_1_t585876960 * L_8 = (DOGetter_1_t585876960 *)__this->get_getter_57();
		DOSetter_1_t1622516788 * L_9 = (DOSetter_1_t1622516788 *)__this->get_setter_58();
		float L_10 = V_0;
		Quaternion_t1891715979  L_11 = (Quaternion_t1891715979 )__this->get_startValue_53();
		Quaternion_t1891715979  L_12 = (Quaternion_t1891715979 )__this->get_changeValue_55();
		float L_13 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t2861657316 *)L_5);
		VirtActionInvoker11< NoOptions_t32144009 , Tween_t1103364673 *, bool, DOGetter_1_t585876960 *, DOSetter_1_t1622516788 *, float, Quaternion_t1891715979 , Quaternion_t1891715979 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t2861657316 *)L_5, (NoOptions_t32144009 )L_6, (Tween_t1103364673 *)__this, (bool)L_7, (DOGetter_1_t585876960 *)L_8, (DOSetter_1_t1622516788 *)L_9, (float)L_10, (Quaternion_t1891715979 )L_11, (Quaternion_t1891715979 )L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t2861657316 * L_16 = (ABSTweenPlugin_3_t2861657316 *)__this->get_tweenPlugin_59();
		NoOptions_t32144009  L_17 = (NoOptions_t32144009 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t1103364673 *)__this)->get_isRelative_27();
		DOGetter_1_t585876960 * L_19 = (DOGetter_1_t585876960 *)__this->get_getter_57();
		DOSetter_1_t1622516788 * L_20 = (DOSetter_1_t1622516788 *)__this->get_setter_58();
		float L_21 = V_0;
		Quaternion_t1891715979  L_22 = (Quaternion_t1891715979 )__this->get_startValue_53();
		Quaternion_t1891715979  L_23 = (Quaternion_t1891715979 )__this->get_changeValue_55();
		float L_24 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t2861657316 *)L_16);
		VirtActionInvoker11< NoOptions_t32144009 , Tween_t1103364673 *, bool, DOGetter_1_t585876960 *, DOSetter_1_t1622516788 *, float, Quaternion_t1891715979 , Quaternion_t1891715979 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t2861657316 *)L_16, (NoOptions_t32144009 )L_17, (Tween_t1103364673 *)__this, (bool)L_18, (DOGetter_1_t585876960 *)L_19, (DOSetter_1_t1622516788 *)L_20, (float)L_21, (Quaternion_t1891715979 )L_22, (Quaternion_t1891715979 )L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::.ctor()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3__ctor_m2411673520_MetadataUsageId;
extern "C"  void TweenerCore_3__ctor_m2411673520_gshared (TweenerCore_3_t3741400174 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m2411673520_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Tweener_t1766303790 *)__this);
		Tweener__ctor_m1348308877((Tweener_t1766303790 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2598715689 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t1103364673 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t1103364673 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::ChangeStartValue(System.Object,System.Single)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1899955427;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeStartValue_m1026956127_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeStartValue_m1026956127_gshared (TweenerCore_3_t3741400174 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m1026956127_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_7 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, _stringLiteral1899955427);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1899955427);
		ObjectU5BU5D_t11523773* L_8 = (ObjectU5BU5D_t11523773*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		ArrayElementTypeCheck (L_10, _stringLiteral3718369476);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_11 = (ObjectU5BU5D_t11523773*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 3);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_11;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral41);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t1766303790 * L_17 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3741400174 *, Vector3_t3525329789 , float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, (TweenerCore_3_t3741400174 *)__this, (Vector3_t3525329789 )((*(Vector3_t3525329789 *)((Vector3_t3525329789 *)UnBox (L_15, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeEndValue_m1957758378_gshared (TweenerCore_3_t3741400174 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t1766303790 *)__this);
		Tweener_t1766303790 * L_2 = VirtFuncInvoker3< Tweener_t1766303790 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t1766303790 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1647271171;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeEndValue_m343859557_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeEndValue_m343859557_gshared (TweenerCore_3_t3741400174 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m343859557_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_7 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, _stringLiteral1647271171);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1647271171);
		ObjectU5BU5D_t11523773* L_8 = (ObjectU5BU5D_t11523773*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		ArrayElementTypeCheck (L_10, _stringLiteral3718369476);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_11 = (ObjectU5BU5D_t11523773*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 3);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_11;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral41);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t1766303790 * L_18 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3741400174 *, Vector3_t3525329789 , float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (TweenerCore_3_t3741400174 *)__this, (Vector3_t3525329789 )((*(Vector3_t3525329789 *)((Vector3_t3525329789 *)UnBox (L_15, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1932880146;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeValues_m3910686298_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeValues_m3910686298_gshared (TweenerCore_3_t3741400174 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m3910686298_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m2022236990((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_9 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		ArrayElementTypeCheck (L_9, _stringLiteral1932880146);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1932880146);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 1);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t11523773* L_12 = (ObjectU5BU5D_t11523773*)L_10;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 2);
		ArrayElementTypeCheck (L_12, _stringLiteral3718369476);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 3);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t11523773* L_15 = (ObjectU5BU5D_t11523773*)L_13;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 4);
		ArrayElementTypeCheck (L_15, _stringLiteral41);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_20 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 0);
		ArrayElementTypeCheck (L_20, _stringLiteral1932880146);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1932880146);
		ObjectU5BU5D_t11523773* L_21 = (ObjectU5BU5D_t11523773*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 1);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t11523773* L_23 = (ObjectU5BU5D_t11523773*)L_21;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 2);
		ArrayElementTypeCheck (L_23, _stringLiteral3718369476);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_24 = (ObjectU5BU5D_t11523773*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 3);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t11523773* L_26 = (ObjectU5BU5D_t11523773*)L_24;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 4);
		ArrayElementTypeCheck (L_26, _stringLiteral41);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t1766303790 * L_31 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3741400174 *, Vector3_t3525329789 , Vector3_t3525329789 , float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, (TweenerCore_3_t3741400174 *)__this, (Vector3_t3525329789 )((*(Vector3_t3525329789 *)((Vector3_t3525329789 *)UnBox (L_28, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (Vector3_t3525329789 )((*(Vector3_t3525329789 *)((Vector3_t3525329789 *)UnBox (L_29, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t1766303790 * TweenerCore_3_SetFrom_m3761683850_gshared (TweenerCore_3_t3741400174 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t339311853 * L_0 = (ABSTweenPlugin_3_t339311853 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t339311853 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t3741400174 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t339311853 *)L_0, (TweenerCore_3_t3741400174 *)__this, (bool)L_1);
		((Tweener_t1766303790 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m58106461_gshared (TweenerCore_3_t3741400174 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t1103364673 *)__this);
		Tween_Reset_m1561947655((Tween_t1103364673 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t339311853 * L_0 = (ABSTweenPlugin_3_t339311853 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t339311853 * L_1 = (ABSTweenPlugin_3_t339311853 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t339311853 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t3741400174 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t339311853 *)L_1, (TweenerCore_3_t3741400174 *)__this);
	}

IL_001a:
	{
		QuaternionOptions_t1813596812  L_2 = ((  QuaternionOptions_t1813596812  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t585876960 *)NULL);
		__this->set_setter_58((DOSetter_1_t1622516788 *)NULL);
		((Tweener_t1766303790 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t1766303790 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::Validate()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_Validate_m1352156226_MetadataUsageId;
extern "C"  bool TweenerCore_3_Validate_m1352156226_gshared (TweenerCore_3_t3741400174 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m1352156226_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t585876960 * L_0 = (DOGetter_1_t585876960 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t585876960 *)L_0);
		((  Quaternion_t1891715979  (*) (DOGetter_1_t585876960 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((DOGetter_1_t585876960 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m2634400559_gshared (TweenerCore_3_t3741400174 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3741400174 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(NULL /*static, unused*/, (TweenerCore_3_t3741400174 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m687662899_gshared (TweenerCore_3_t3741400174 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3741400174 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)(NULL /*static, unused*/, (TweenerCore_3_t3741400174 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern Il2CppClass* DOTween_t3585775766_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_ApplyTween_m3140913480_MetadataUsageId;
extern "C"  bool TweenerCore_3_ApplyTween_m3140913480_gshared (TweenerCore_3_t3741400174 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m3140913480_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t1103364673 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t1103364673 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t3585775766_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t3585775766_StaticFields*)DOTween_t3585775766_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t339311853 * L_5 = (ABSTweenPlugin_3_t339311853 *)__this->get_tweenPlugin_59();
		QuaternionOptions_t1813596812  L_6 = (QuaternionOptions_t1813596812 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t1103364673 *)__this)->get_isRelative_27();
		DOGetter_1_t585876960 * L_8 = (DOGetter_1_t585876960 *)__this->get_getter_57();
		DOSetter_1_t1622516788 * L_9 = (DOSetter_1_t1622516788 *)__this->get_setter_58();
		float L_10 = V_0;
		Vector3_t3525329789  L_11 = (Vector3_t3525329789 )__this->get_startValue_53();
		Vector3_t3525329789  L_12 = (Vector3_t3525329789 )__this->get_changeValue_55();
		float L_13 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t339311853 *)L_5);
		VirtActionInvoker11< QuaternionOptions_t1813596812 , Tween_t1103364673 *, bool, DOGetter_1_t585876960 *, DOSetter_1_t1622516788 *, float, Vector3_t3525329789 , Vector3_t3525329789 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t339311853 *)L_5, (QuaternionOptions_t1813596812 )L_6, (Tween_t1103364673 *)__this, (bool)L_7, (DOGetter_1_t585876960 *)L_8, (DOSetter_1_t1622516788 *)L_9, (float)L_10, (Vector3_t3525329789 )L_11, (Vector3_t3525329789 )L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t339311853 * L_16 = (ABSTweenPlugin_3_t339311853 *)__this->get_tweenPlugin_59();
		QuaternionOptions_t1813596812  L_17 = (QuaternionOptions_t1813596812 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t1103364673 *)__this)->get_isRelative_27();
		DOGetter_1_t585876960 * L_19 = (DOGetter_1_t585876960 *)__this->get_getter_57();
		DOSetter_1_t1622516788 * L_20 = (DOSetter_1_t1622516788 *)__this->get_setter_58();
		float L_21 = V_0;
		Vector3_t3525329789  L_22 = (Vector3_t3525329789 )__this->get_startValue_53();
		Vector3_t3525329789  L_23 = (Vector3_t3525329789 )__this->get_changeValue_55();
		float L_24 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t339311853 *)L_16);
		VirtActionInvoker11< QuaternionOptions_t1813596812 , Tween_t1103364673 *, bool, DOGetter_1_t585876960 *, DOSetter_1_t1622516788 *, float, Vector3_t3525329789 , Vector3_t3525329789 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t339311853 *)L_16, (QuaternionOptions_t1813596812 )L_17, (Tween_t1103364673 *)__this, (bool)L_18, (DOGetter_1_t585876960 *)L_19, (DOSetter_1_t1622516788 *)L_20, (float)L_21, (Vector3_t3525329789 )L_22, (Vector3_t3525329789 )L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::.ctor()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3__ctor_m1181334902_MetadataUsageId;
extern "C"  void TweenerCore_3__ctor_m1181334902_gshared (TweenerCore_3_t2274666266 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m1181334902_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Tweener_t1766303790 *)__this);
		Tweener__ctor_m1348308877((Tweener_t1766303790 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2598715689 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t1103364673 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t1103364673 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::ChangeStartValue(System.Object,System.Single)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1899955427;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeStartValue_m2925594515_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeStartValue_m2925594515_gshared (TweenerCore_3_t2274666266 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m2925594515_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_7 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, _stringLiteral1899955427);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1899955427);
		ObjectU5BU5D_t11523773* L_8 = (ObjectU5BU5D_t11523773*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		ArrayElementTypeCheck (L_10, _stringLiteral3718369476);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_11 = (ObjectU5BU5D_t11523773*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 3);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_11;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral41);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t1766303790 * L_17 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2274666266 *, Rect_t1525428817 , float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, (TweenerCore_3_t2274666266 *)__this, (Rect_t1525428817 )((*(Rect_t1525428817 *)((Rect_t1525428817 *)UnBox (L_15, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeEndValue_m2434646774_gshared (TweenerCore_3_t2274666266 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t1766303790 *)__this);
		Tweener_t1766303790 * L_2 = VirtFuncInvoker3< Tweener_t1766303790 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t1766303790 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1647271171;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeEndValue_m2927651249_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeEndValue_m2927651249_gshared (TweenerCore_3_t2274666266 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m2927651249_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_7 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, _stringLiteral1647271171);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1647271171);
		ObjectU5BU5D_t11523773* L_8 = (ObjectU5BU5D_t11523773*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		ArrayElementTypeCheck (L_10, _stringLiteral3718369476);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_11 = (ObjectU5BU5D_t11523773*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 3);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_11;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral41);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t1766303790 * L_18 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2274666266 *, Rect_t1525428817 , float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (TweenerCore_3_t2274666266 *)__this, (Rect_t1525428817 )((*(Rect_t1525428817 *)((Rect_t1525428817 *)UnBox (L_15, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1932880146;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeValues_m2164298126_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeValues_m2164298126_gshared (TweenerCore_3_t2274666266 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m2164298126_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m2022236990((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_9 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		ArrayElementTypeCheck (L_9, _stringLiteral1932880146);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1932880146);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 1);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t11523773* L_12 = (ObjectU5BU5D_t11523773*)L_10;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 2);
		ArrayElementTypeCheck (L_12, _stringLiteral3718369476);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 3);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t11523773* L_15 = (ObjectU5BU5D_t11523773*)L_13;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 4);
		ArrayElementTypeCheck (L_15, _stringLiteral41);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_20 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 0);
		ArrayElementTypeCheck (L_20, _stringLiteral1932880146);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1932880146);
		ObjectU5BU5D_t11523773* L_21 = (ObjectU5BU5D_t11523773*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 1);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t11523773* L_23 = (ObjectU5BU5D_t11523773*)L_21;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 2);
		ArrayElementTypeCheck (L_23, _stringLiteral3718369476);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_24 = (ObjectU5BU5D_t11523773*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 3);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t11523773* L_26 = (ObjectU5BU5D_t11523773*)L_24;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 4);
		ArrayElementTypeCheck (L_26, _stringLiteral41);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t1766303790 * L_31 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2274666266 *, Rect_t1525428817 , Rect_t1525428817 , float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, (TweenerCore_3_t2274666266 *)__this, (Rect_t1525428817 )((*(Rect_t1525428817 *)((Rect_t1525428817 *)UnBox (L_28, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (Rect_t1525428817 )((*(Rect_t1525428817 *)((Rect_t1525428817 *)UnBox (L_29, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t1766303790 * TweenerCore_3_SetFrom_m725141694_gshared (TweenerCore_3_t2274666266 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t3167545241 * L_0 = (ABSTweenPlugin_3_t3167545241 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t3167545241 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t2274666266 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t3167545241 *)L_0, (TweenerCore_3_t2274666266 *)__this, (bool)L_1);
		((Tweener_t1766303790 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m3122735139_gshared (TweenerCore_3_t2274666266 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t1103364673 *)__this);
		Tween_Reset_m1561947655((Tween_t1103364673 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t3167545241 * L_0 = (ABSTweenPlugin_3_t3167545241 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t3167545241 * L_1 = (ABSTweenPlugin_3_t3167545241 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t3167545241 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t2274666266 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t3167545241 *)L_1, (TweenerCore_3_t2274666266 *)__this);
	}

IL_001a:
	{
		RectOptions_t1160272134  L_2 = ((  RectOptions_t1160272134  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t219589798 *)NULL);
		__this->set_setter_58((DOSetter_1_t1256229626 *)NULL);
		((Tweener_t1766303790 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t1766303790 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::Validate()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_Validate_m2923630220_MetadataUsageId;
extern "C"  bool TweenerCore_3_Validate_m2923630220_gshared (TweenerCore_3_t2274666266 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m2923630220_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t219589798 * L_0 = (DOGetter_1_t219589798 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t219589798 *)L_0);
		((  Rect_t1525428817  (*) (DOGetter_1_t219589798 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((DOGetter_1_t219589798 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m2942084753_gshared (TweenerCore_3_t2274666266 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2274666266 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(NULL /*static, unused*/, (TweenerCore_3_t2274666266 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m1292544937_gshared (TweenerCore_3_t2274666266 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2274666266 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)(NULL /*static, unused*/, (TweenerCore_3_t2274666266 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern Il2CppClass* DOTween_t3585775766_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_ApplyTween_m1546436114_MetadataUsageId;
extern "C"  bool TweenerCore_3_ApplyTween_m1546436114_gshared (TweenerCore_3_t2274666266 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m1546436114_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t1103364673 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t1103364673 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t3585775766_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t3585775766_StaticFields*)DOTween_t3585775766_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t3167545241 * L_5 = (ABSTweenPlugin_3_t3167545241 *)__this->get_tweenPlugin_59();
		RectOptions_t1160272134  L_6 = (RectOptions_t1160272134 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t1103364673 *)__this)->get_isRelative_27();
		DOGetter_1_t219589798 * L_8 = (DOGetter_1_t219589798 *)__this->get_getter_57();
		DOSetter_1_t1256229626 * L_9 = (DOSetter_1_t1256229626 *)__this->get_setter_58();
		float L_10 = V_0;
		Rect_t1525428817  L_11 = (Rect_t1525428817 )__this->get_startValue_53();
		Rect_t1525428817  L_12 = (Rect_t1525428817 )__this->get_changeValue_55();
		float L_13 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t3167545241 *)L_5);
		VirtActionInvoker11< RectOptions_t1160272134 , Tween_t1103364673 *, bool, DOGetter_1_t219589798 *, DOSetter_1_t1256229626 *, float, Rect_t1525428817 , Rect_t1525428817 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t3167545241 *)L_5, (RectOptions_t1160272134 )L_6, (Tween_t1103364673 *)__this, (bool)L_7, (DOGetter_1_t219589798 *)L_8, (DOSetter_1_t1256229626 *)L_9, (float)L_10, (Rect_t1525428817 )L_11, (Rect_t1525428817 )L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t3167545241 * L_16 = (ABSTweenPlugin_3_t3167545241 *)__this->get_tweenPlugin_59();
		RectOptions_t1160272134  L_17 = (RectOptions_t1160272134 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t1103364673 *)__this)->get_isRelative_27();
		DOGetter_1_t219589798 * L_19 = (DOGetter_1_t219589798 *)__this->get_getter_57();
		DOSetter_1_t1256229626 * L_20 = (DOSetter_1_t1256229626 *)__this->get_setter_58();
		float L_21 = V_0;
		Rect_t1525428817  L_22 = (Rect_t1525428817 )__this->get_startValue_53();
		Rect_t1525428817  L_23 = (Rect_t1525428817 )__this->get_changeValue_55();
		float L_24 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t3167545241 *)L_16);
		VirtActionInvoker11< RectOptions_t1160272134 , Tween_t1103364673 *, bool, DOGetter_1_t219589798 *, DOSetter_1_t1256229626 *, float, Rect_t1525428817 , Rect_t1525428817 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t3167545241 *)L_16, (RectOptions_t1160272134 )L_17, (Tween_t1103364673 *)__this, (bool)L_18, (DOGetter_1_t219589798 *)L_19, (DOSetter_1_t1256229626 *)L_20, (float)L_21, (Rect_t1525428817 )L_22, (Rect_t1525428817 )L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::.ctor()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3__ctor_m2784562351_MetadataUsageId;
extern "C"  void TweenerCore_3__ctor_m2784562351_gshared (TweenerCore_3_t753146263 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m2784562351_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Tweener_t1766303790 *)__this);
		Tweener__ctor_m1348308877((Tweener_t1766303790 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2598715689 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t1103364673 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t1103364673 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::ChangeStartValue(System.Object,System.Single)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1899955427;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeStartValue_m2786405644_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeStartValue_m2786405644_gshared (TweenerCore_3_t753146263 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m2786405644_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_7 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, _stringLiteral1899955427);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1899955427);
		ObjectU5BU5D_t11523773* L_8 = (ObjectU5BU5D_t11523773*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		ArrayElementTypeCheck (L_10, _stringLiteral3718369476);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_11 = (ObjectU5BU5D_t11523773*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 3);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_11;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral41);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t1766303790 * L_17 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t753146263 *, Vector2_t3525329788 , float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, (TweenerCore_3_t753146263 *)__this, (Vector2_t3525329788 )((*(Vector2_t3525329788 *)((Vector2_t3525329788 *)UnBox (L_15, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeEndValue_m906136157_gshared (TweenerCore_3_t753146263 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t1766303790 *)__this);
		Tweener_t1766303790 * L_2 = VirtFuncInvoker3< Tweener_t1766303790 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t1766303790 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1647271171;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeEndValue_m2873405656_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeEndValue_m2873405656_gshared (TweenerCore_3_t753146263 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m2873405656_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_7 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, _stringLiteral1647271171);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1647271171);
		ObjectU5BU5D_t11523773* L_8 = (ObjectU5BU5D_t11523773*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		ArrayElementTypeCheck (L_10, _stringLiteral3718369476);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_11 = (ObjectU5BU5D_t11523773*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 3);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_11;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral41);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t1766303790 * L_18 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t753146263 *, Vector2_t3525329788 , float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (TweenerCore_3_t753146263 *)__this, (Vector2_t3525329788 )((*(Vector2_t3525329788 *)((Vector2_t3525329788 *)UnBox (L_15, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1932880146;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeValues_m3584226247_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeValues_m3584226247_gshared (TweenerCore_3_t753146263 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m3584226247_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m2022236990((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_9 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		ArrayElementTypeCheck (L_9, _stringLiteral1932880146);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1932880146);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 1);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t11523773* L_12 = (ObjectU5BU5D_t11523773*)L_10;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 2);
		ArrayElementTypeCheck (L_12, _stringLiteral3718369476);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 3);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t11523773* L_15 = (ObjectU5BU5D_t11523773*)L_13;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 4);
		ArrayElementTypeCheck (L_15, _stringLiteral41);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_20 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 0);
		ArrayElementTypeCheck (L_20, _stringLiteral1932880146);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1932880146);
		ObjectU5BU5D_t11523773* L_21 = (ObjectU5BU5D_t11523773*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 1);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t11523773* L_23 = (ObjectU5BU5D_t11523773*)L_21;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 2);
		ArrayElementTypeCheck (L_23, _stringLiteral3718369476);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_24 = (ObjectU5BU5D_t11523773*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 3);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t11523773* L_26 = (ObjectU5BU5D_t11523773*)L_24;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 4);
		ArrayElementTypeCheck (L_26, _stringLiteral41);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t1766303790 * L_31 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t753146263 *, Vector2_t3525329788 , Vector2_t3525329788 , float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, (TweenerCore_3_t753146263 *)__this, (Vector2_t3525329788 )((*(Vector2_t3525329788 *)((Vector2_t3525329788 *)UnBox (L_28, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (Vector2_t3525329788 )((*(Vector2_t3525329788 *)((Vector2_t3525329788 *)UnBox (L_29, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t1766303790 * TweenerCore_3_SetFrom_m2456440567_gshared (TweenerCore_3_t753146263 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t1646025238 * L_0 = (ABSTweenPlugin_3_t1646025238 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t1646025238 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t753146263 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t1646025238 *)L_0, (TweenerCore_3_t753146263 *)__this, (bool)L_1);
		((Tweener_t1766303790 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m430995292_gshared (TweenerCore_3_t753146263 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t1103364673 *)__this);
		Tween_Reset_m1561947655((Tween_t1103364673 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t1646025238 * L_0 = (ABSTweenPlugin_3_t1646025238 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t1646025238 * L_1 = (ABSTweenPlugin_3_t1646025238 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t1646025238 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t753146263 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t1646025238 *)L_1, (TweenerCore_3_t753146263 *)__this);
	}

IL_001a:
	{
		VectorOptions_t3308462279  L_2 = ((  VectorOptions_t3308462279  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t2219490769 *)NULL);
		__this->set_setter_58((DOSetter_1_t3256130597 *)NULL);
		((Tweener_t1766303790 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t1766303790 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::Validate()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_Validate_m1482449075_MetadataUsageId;
extern "C"  bool TweenerCore_3_Validate_m1482449075_gshared (TweenerCore_3_t753146263 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m1482449075_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t2219490769 * L_0 = (DOGetter_1_t2219490769 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t2219490769 *)L_0);
		((  Vector2_t3525329788  (*) (DOGetter_1_t2219490769 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((DOGetter_1_t2219490769 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m656293560_gshared (TweenerCore_3_t753146263 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t753146263 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(NULL /*static, unused*/, (TweenerCore_3_t753146263 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m553318562_gshared (TweenerCore_3_t753146263 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t753146263 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)(NULL /*static, unused*/, (TweenerCore_3_t753146263 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern Il2CppClass* DOTween_t3585775766_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_ApplyTween_m3628924409_MetadataUsageId;
extern "C"  bool TweenerCore_3_ApplyTween_m3628924409_gshared (TweenerCore_3_t753146263 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m3628924409_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t1103364673 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t1103364673 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t3585775766_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t3585775766_StaticFields*)DOTween_t3585775766_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t1646025238 * L_5 = (ABSTweenPlugin_3_t1646025238 *)__this->get_tweenPlugin_59();
		VectorOptions_t3308462279  L_6 = (VectorOptions_t3308462279 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t1103364673 *)__this)->get_isRelative_27();
		DOGetter_1_t2219490769 * L_8 = (DOGetter_1_t2219490769 *)__this->get_getter_57();
		DOSetter_1_t3256130597 * L_9 = (DOSetter_1_t3256130597 *)__this->get_setter_58();
		float L_10 = V_0;
		Vector2_t3525329788  L_11 = (Vector2_t3525329788 )__this->get_startValue_53();
		Vector2_t3525329788  L_12 = (Vector2_t3525329788 )__this->get_changeValue_55();
		float L_13 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t1646025238 *)L_5);
		VirtActionInvoker11< VectorOptions_t3308462279 , Tween_t1103364673 *, bool, DOGetter_1_t2219490769 *, DOSetter_1_t3256130597 *, float, Vector2_t3525329788 , Vector2_t3525329788 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t1646025238 *)L_5, (VectorOptions_t3308462279 )L_6, (Tween_t1103364673 *)__this, (bool)L_7, (DOGetter_1_t2219490769 *)L_8, (DOSetter_1_t3256130597 *)L_9, (float)L_10, (Vector2_t3525329788 )L_11, (Vector2_t3525329788 )L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t1646025238 * L_16 = (ABSTweenPlugin_3_t1646025238 *)__this->get_tweenPlugin_59();
		VectorOptions_t3308462279  L_17 = (VectorOptions_t3308462279 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t1103364673 *)__this)->get_isRelative_27();
		DOGetter_1_t2219490769 * L_19 = (DOGetter_1_t2219490769 *)__this->get_getter_57();
		DOSetter_1_t3256130597 * L_20 = (DOSetter_1_t3256130597 *)__this->get_setter_58();
		float L_21 = V_0;
		Vector2_t3525329788  L_22 = (Vector2_t3525329788 )__this->get_startValue_53();
		Vector2_t3525329788  L_23 = (Vector2_t3525329788 )__this->get_changeValue_55();
		float L_24 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t1646025238 *)L_16);
		VirtActionInvoker11< VectorOptions_t3308462279 , Tween_t1103364673 *, bool, DOGetter_1_t2219490769 *, DOSetter_1_t3256130597 *, float, Vector2_t3525329788 , Vector2_t3525329788 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t1646025238 *)L_16, (VectorOptions_t3308462279 )L_17, (Tween_t1103364673 *)__this, (bool)L_18, (DOGetter_1_t2219490769 *)L_19, (DOSetter_1_t3256130597 *)L_20, (float)L_21, (Vector2_t3525329788 )L_22, (Vector2_t3525329788 )L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::.ctor()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3__ctor_m3114960820_MetadataUsageId;
extern "C"  void TweenerCore_3__ctor_m3114960820_gshared (TweenerCore_3_t1820784502 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m3114960820_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Tweener_t1766303790 *)__this);
		Tweener__ctor_m1348308877((Tweener_t1766303790 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2598715689 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t1103364673 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t1103364673 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::ChangeStartValue(System.Object,System.Single)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1899955427;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeStartValue_m586560337_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeStartValue_m586560337_gshared (TweenerCore_3_t1820784502 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m586560337_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_7 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, _stringLiteral1899955427);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1899955427);
		ObjectU5BU5D_t11523773* L_8 = (ObjectU5BU5D_t11523773*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		ArrayElementTypeCheck (L_10, _stringLiteral3718369476);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_11 = (ObjectU5BU5D_t11523773*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 3);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_11;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral41);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t1766303790 * L_17 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1820784502 *, Il2CppObject *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, (TweenerCore_3_t1820784502 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_15, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeEndValue_m3883214712_gshared (TweenerCore_3_t1820784502 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t1766303790 *)__this);
		Tweener_t1766303790 * L_2 = VirtFuncInvoker3< Tweener_t1766303790 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t1766303790 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1647271171;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeEndValue_m4152672947_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeEndValue_m4152672947_gshared (TweenerCore_3_t1820784502 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m4152672947_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_7 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, _stringLiteral1647271171);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1647271171);
		ObjectU5BU5D_t11523773* L_8 = (ObjectU5BU5D_t11523773*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		ArrayElementTypeCheck (L_10, _stringLiteral3718369476);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_11 = (ObjectU5BU5D_t11523773*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 3);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_11;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral41);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t1766303790 * L_18 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1820784502 *, Il2CppObject *, float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (TweenerCore_3_t1820784502 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_15, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1932880146;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeValues_m3944261580_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeValues_m3944261580_gshared (TweenerCore_3_t1820784502 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m3944261580_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m2022236990((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_9 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		ArrayElementTypeCheck (L_9, _stringLiteral1932880146);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1932880146);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 1);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t11523773* L_12 = (ObjectU5BU5D_t11523773*)L_10;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 2);
		ArrayElementTypeCheck (L_12, _stringLiteral3718369476);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 3);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t11523773* L_15 = (ObjectU5BU5D_t11523773*)L_13;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 4);
		ArrayElementTypeCheck (L_15, _stringLiteral41);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_20 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 0);
		ArrayElementTypeCheck (L_20, _stringLiteral1932880146);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1932880146);
		ObjectU5BU5D_t11523773* L_21 = (ObjectU5BU5D_t11523773*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 1);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t11523773* L_23 = (ObjectU5BU5D_t11523773*)L_21;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 2);
		ArrayElementTypeCheck (L_23, _stringLiteral3718369476);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_24 = (ObjectU5BU5D_t11523773*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 3);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t11523773* L_26 = (ObjectU5BU5D_t11523773*)L_24;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 4);
		ArrayElementTypeCheck (L_26, _stringLiteral41);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t1766303790 * L_31 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1820784502 *, Il2CppObject *, Il2CppObject *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, (TweenerCore_3_t1820784502 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_28, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))), (Il2CppObject *)((Il2CppObject *)Castclass(L_29, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t1766303790 * TweenerCore_3_SetFrom_m2965985020_gshared (TweenerCore_3_t1820784502 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t2713663477 * L_0 = (ABSTweenPlugin_3_t2713663477 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t2713663477 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t1820784502 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t2713663477 *)L_0, (TweenerCore_3_t1820784502 *)__this, (bool)L_1);
		((Tweener_t1766303790 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m761393761_gshared (TweenerCore_3_t1820784502 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t1103364673 *)__this);
		Tween_Reset_m1561947655((Tween_t1103364673 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t2713663477 * L_0 = (ABSTweenPlugin_3_t2713663477 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t2713663477 * L_1 = (ABSTweenPlugin_3_t2713663477 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t2713663477 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t1820784502 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t2713663477 *)L_1, (TweenerCore_3_t1820784502 *)__this);
	}

IL_001a:
	{
		PathOptions_t1144419237  L_2 = ((  PathOptions_t1144419237  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t2219490770 *)NULL);
		__this->set_setter_58((DOSetter_1_t3256130598 *)NULL);
		((Tweener_t1766303790 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t1766303790 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::Validate()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_Validate_m3471979406_MetadataUsageId;
extern "C"  bool TweenerCore_3_Validate_m3471979406_gshared (TweenerCore_3_t1820784502 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m3471979406_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t2219490770 * L_0 = (DOGetter_1_t2219490770 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t2219490770 *)L_0);
		((  Vector3_t3525329789  (*) (DOGetter_1_t2219490770 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((DOGetter_1_t2219490770 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m3228927891_gshared (TweenerCore_3_t1820784502 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1820784502 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(NULL /*static, unused*/, (TweenerCore_3_t1820784502 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m4081180263_gshared (TweenerCore_3_t1820784502 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1820784502 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)(NULL /*static, unused*/, (TweenerCore_3_t1820784502 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern Il2CppClass* DOTween_t3585775766_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_ApplyTween_m3005946772_MetadataUsageId;
extern "C"  bool TweenerCore_3_ApplyTween_m3005946772_gshared (TweenerCore_3_t1820784502 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m3005946772_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t1103364673 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t1103364673 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t3585775766_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t3585775766_StaticFields*)DOTween_t3585775766_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t2713663477 * L_5 = (ABSTweenPlugin_3_t2713663477 *)__this->get_tweenPlugin_59();
		PathOptions_t1144419237  L_6 = (PathOptions_t1144419237 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t1103364673 *)__this)->get_isRelative_27();
		DOGetter_1_t2219490770 * L_8 = (DOGetter_1_t2219490770 *)__this->get_getter_57();
		DOSetter_1_t3256130598 * L_9 = (DOSetter_1_t3256130598 *)__this->get_setter_58();
		float L_10 = V_0;
		Il2CppObject * L_11 = (Il2CppObject *)__this->get_startValue_53();
		Il2CppObject * L_12 = (Il2CppObject *)__this->get_changeValue_55();
		float L_13 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t2713663477 *)L_5);
		VirtActionInvoker11< PathOptions_t1144419237 , Tween_t1103364673 *, bool, DOGetter_1_t2219490770 *, DOSetter_1_t3256130598 *, float, Il2CppObject *, Il2CppObject *, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t2713663477 *)L_5, (PathOptions_t1144419237 )L_6, (Tween_t1103364673 *)__this, (bool)L_7, (DOGetter_1_t2219490770 *)L_8, (DOSetter_1_t3256130598 *)L_9, (float)L_10, (Il2CppObject *)L_11, (Il2CppObject *)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t2713663477 * L_16 = (ABSTweenPlugin_3_t2713663477 *)__this->get_tweenPlugin_59();
		PathOptions_t1144419237  L_17 = (PathOptions_t1144419237 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t1103364673 *)__this)->get_isRelative_27();
		DOGetter_1_t2219490770 * L_19 = (DOGetter_1_t2219490770 *)__this->get_getter_57();
		DOSetter_1_t3256130598 * L_20 = (DOSetter_1_t3256130598 *)__this->get_setter_58();
		float L_21 = V_0;
		Il2CppObject * L_22 = (Il2CppObject *)__this->get_startValue_53();
		Il2CppObject * L_23 = (Il2CppObject *)__this->get_changeValue_55();
		float L_24 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t2713663477 *)L_16);
		VirtActionInvoker11< PathOptions_t1144419237 , Tween_t1103364673 *, bool, DOGetter_1_t2219490770 *, DOSetter_1_t3256130598 *, float, Il2CppObject *, Il2CppObject *, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t2713663477 *)L_16, (PathOptions_t1144419237 )L_17, (Tween_t1103364673 *)__this, (bool)L_18, (DOGetter_1_t2219490770 *)L_19, (DOSetter_1_t3256130598 *)L_20, (float)L_21, (Il2CppObject *)L_22, (Il2CppObject *)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::.ctor()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3__ctor_m2367105392_MetadataUsageId;
extern "C"  void TweenerCore_3__ctor_m2367105392_gshared (TweenerCore_3_t1616349746 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m2367105392_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Tweener_t1766303790 *)__this);
		Tweener__ctor_m1348308877((Tweener_t1766303790 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2598715689 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t1103364673 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t1103364673 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::ChangeStartValue(System.Object,System.Single)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1899955427;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeStartValue_m3950555405_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeStartValue_m3950555405_gshared (TweenerCore_3_t1616349746 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m3950555405_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_7 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, _stringLiteral1899955427);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1899955427);
		ObjectU5BU5D_t11523773* L_8 = (ObjectU5BU5D_t11523773*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		ArrayElementTypeCheck (L_10, _stringLiteral3718369476);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_11 = (ObjectU5BU5D_t11523773*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 3);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_11;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral41);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t1766303790 * L_17 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1616349746 *, Il2CppObject *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, (TweenerCore_3_t1616349746 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_15, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeEndValue_m1913520700_gshared (TweenerCore_3_t1616349746 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t1766303790 *)__this);
		Tweener_t1766303790 * L_2 = VirtFuncInvoker3< Tweener_t1766303790 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t1766303790 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1647271171;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeEndValue_m3163911799_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeEndValue_m3163911799_gshared (TweenerCore_3_t1616349746 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m3163911799_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_7 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, _stringLiteral1647271171);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1647271171);
		ObjectU5BU5D_t11523773* L_8 = (ObjectU5BU5D_t11523773*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		ArrayElementTypeCheck (L_10, _stringLiteral3718369476);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_11 = (ObjectU5BU5D_t11523773*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 3);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_11;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral41);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t1766303790 * L_18 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1616349746 *, Il2CppObject *, float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (TweenerCore_3_t1616349746 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_15, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1932880146;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeValues_m2086021768_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeValues_m2086021768_gshared (TweenerCore_3_t1616349746 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m2086021768_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m2022236990((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_9 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		ArrayElementTypeCheck (L_9, _stringLiteral1932880146);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1932880146);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 1);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t11523773* L_12 = (ObjectU5BU5D_t11523773*)L_10;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 2);
		ArrayElementTypeCheck (L_12, _stringLiteral3718369476);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 3);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t11523773* L_15 = (ObjectU5BU5D_t11523773*)L_13;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 4);
		ArrayElementTypeCheck (L_15, _stringLiteral41);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_20 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 0);
		ArrayElementTypeCheck (L_20, _stringLiteral1932880146);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1932880146);
		ObjectU5BU5D_t11523773* L_21 = (ObjectU5BU5D_t11523773*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 1);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t11523773* L_23 = (ObjectU5BU5D_t11523773*)L_21;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 2);
		ArrayElementTypeCheck (L_23, _stringLiteral3718369476);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_24 = (ObjectU5BU5D_t11523773*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 3);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t11523773* L_26 = (ObjectU5BU5D_t11523773*)L_24;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 4);
		ArrayElementTypeCheck (L_26, _stringLiteral41);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t1766303790 * L_31 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1616349746 *, Il2CppObject *, Il2CppObject *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, (TweenerCore_3_t1616349746 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_28, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))), (Il2CppObject *)((Il2CppObject *)Castclass(L_29, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t1766303790 * TweenerCore_3_SetFrom_m3789605304_gshared (TweenerCore_3_t1616349746 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t2509228721 * L_0 = (ABSTweenPlugin_3_t2509228721 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t2509228721 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t1616349746 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t2509228721 *)L_0, (TweenerCore_3_t1616349746 *)__this, (bool)L_1);
		((Tweener_t1766303790 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m13538333_gshared (TweenerCore_3_t1616349746 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t1103364673 *)__this);
		Tween_Reset_m1561947655((Tween_t1103364673 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t2509228721 * L_0 = (ABSTweenPlugin_3_t2509228721 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t2509228721 * L_1 = (ABSTweenPlugin_3_t2509228721 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t2509228721 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t1616349746 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t2509228721 *)L_1, (TweenerCore_3_t1616349746 *)__this);
	}

IL_001a:
	{
		Vector3ArrayOptions_t939984481  L_2 = ((  Vector3ArrayOptions_t939984481  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t2219490770 *)NULL);
		__this->set_setter_58((DOSetter_1_t3256130598 *)NULL);
		((Tweener_t1766303790 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t1766303790 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::Validate()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_Validate_m3247472722_MetadataUsageId;
extern "C"  bool TweenerCore_3_Validate_m3247472722_gshared (TweenerCore_3_t1616349746 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m3247472722_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t2219490770 * L_0 = (DOGetter_1_t2219490770 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t2219490770 *)L_0);
		((  Vector3_t3525329789  (*) (DOGetter_1_t2219490770 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((DOGetter_1_t2219490770 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m1404387415_gshared (TweenerCore_3_t1616349746 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1616349746 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(NULL /*static, unused*/, (TweenerCore_3_t1616349746 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m3104106787_gshared (TweenerCore_3_t1616349746 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1616349746 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)(NULL /*static, unused*/, (TweenerCore_3_t1616349746 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern Il2CppClass* DOTween_t3585775766_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_ApplyTween_m3323824472_MetadataUsageId;
extern "C"  bool TweenerCore_3_ApplyTween_m3323824472_gshared (TweenerCore_3_t1616349746 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m3323824472_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t1103364673 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t1103364673 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t3585775766_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t3585775766_StaticFields*)DOTween_t3585775766_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t2509228721 * L_5 = (ABSTweenPlugin_3_t2509228721 *)__this->get_tweenPlugin_59();
		Vector3ArrayOptions_t939984481  L_6 = (Vector3ArrayOptions_t939984481 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t1103364673 *)__this)->get_isRelative_27();
		DOGetter_1_t2219490770 * L_8 = (DOGetter_1_t2219490770 *)__this->get_getter_57();
		DOSetter_1_t3256130598 * L_9 = (DOSetter_1_t3256130598 *)__this->get_setter_58();
		float L_10 = V_0;
		Il2CppObject * L_11 = (Il2CppObject *)__this->get_startValue_53();
		Il2CppObject * L_12 = (Il2CppObject *)__this->get_changeValue_55();
		float L_13 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t2509228721 *)L_5);
		VirtActionInvoker11< Vector3ArrayOptions_t939984481 , Tween_t1103364673 *, bool, DOGetter_1_t2219490770 *, DOSetter_1_t3256130598 *, float, Il2CppObject *, Il2CppObject *, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t2509228721 *)L_5, (Vector3ArrayOptions_t939984481 )L_6, (Tween_t1103364673 *)__this, (bool)L_7, (DOGetter_1_t2219490770 *)L_8, (DOSetter_1_t3256130598 *)L_9, (float)L_10, (Il2CppObject *)L_11, (Il2CppObject *)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t2509228721 * L_16 = (ABSTweenPlugin_3_t2509228721 *)__this->get_tweenPlugin_59();
		Vector3ArrayOptions_t939984481  L_17 = (Vector3ArrayOptions_t939984481 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t1103364673 *)__this)->get_isRelative_27();
		DOGetter_1_t2219490770 * L_19 = (DOGetter_1_t2219490770 *)__this->get_getter_57();
		DOSetter_1_t3256130598 * L_20 = (DOSetter_1_t3256130598 *)__this->get_setter_58();
		float L_21 = V_0;
		Il2CppObject * L_22 = (Il2CppObject *)__this->get_startValue_53();
		Il2CppObject * L_23 = (Il2CppObject *)__this->get_changeValue_55();
		float L_24 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t2509228721 *)L_16);
		VirtActionInvoker11< Vector3ArrayOptions_t939984481 , Tween_t1103364673 *, bool, DOGetter_1_t2219490770 *, DOSetter_1_t3256130598 *, float, Il2CppObject *, Il2CppObject *, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t2509228721 *)L_16, (Vector3ArrayOptions_t939984481 )L_17, (Tween_t1103364673 *)__this, (bool)L_18, (DOGetter_1_t2219490770 *)L_19, (DOSetter_1_t3256130598 *)L_20, (float)L_21, (Il2CppObject *)L_22, (Il2CppObject *)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::.ctor()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3__ctor_m4198475057_MetadataUsageId;
extern "C"  void TweenerCore_3__ctor_m4198475057_gshared (TweenerCore_3_t2905908171 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m4198475057_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Tweener_t1766303790 *)__this);
		Tweener__ctor_m1348308877((Tweener_t1766303790 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2598715689 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t1103364673 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t1103364673 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::ChangeStartValue(System.Object,System.Single)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1899955427;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeStartValue_m992433166_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeStartValue_m992433166_gshared (TweenerCore_3_t2905908171 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m992433166_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_7 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, _stringLiteral1899955427);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1899955427);
		ObjectU5BU5D_t11523773* L_8 = (ObjectU5BU5D_t11523773*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		ArrayElementTypeCheck (L_10, _stringLiteral3718369476);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_11 = (ObjectU5BU5D_t11523773*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 3);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_11;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral41);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t1766303790 * L_17 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2905908171 *, Vector3_t3525329789 , float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, (TweenerCore_3_t2905908171 *)__this, (Vector3_t3525329789 )((*(Vector3_t3525329789 *)((Vector3_t3525329789 *)UnBox (L_15, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeEndValue_m2926476059_gshared (TweenerCore_3_t2905908171 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t1766303790 *)__this);
		Tweener_t1766303790 * L_2 = VirtFuncInvoker3< Tweener_t1766303790 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t1766303790 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1647271171;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeEndValue_m4041571606_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeEndValue_m4041571606_gshared (TweenerCore_3_t2905908171 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m4041571606_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_7 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, _stringLiteral1647271171);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1647271171);
		ObjectU5BU5D_t11523773* L_8 = (ObjectU5BU5D_t11523773*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		ArrayElementTypeCheck (L_10, _stringLiteral3718369476);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_11 = (ObjectU5BU5D_t11523773*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 3);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_11;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral41);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t1766303790 * L_18 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2905908171 *, Vector3_t3525329789 , float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (TweenerCore_3_t2905908171 *)__this, (Vector3_t3525329789 )((*(Vector3_t3525329789 *)((Vector3_t3525329789 *)UnBox (L_15, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1932880146;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeValues_m3174678601_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeValues_m3174678601_gshared (TweenerCore_3_t2905908171 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m3174678601_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m2022236990((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_9 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		ArrayElementTypeCheck (L_9, _stringLiteral1932880146);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1932880146);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 1);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t11523773* L_12 = (ObjectU5BU5D_t11523773*)L_10;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 2);
		ArrayElementTypeCheck (L_12, _stringLiteral3718369476);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 3);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t11523773* L_15 = (ObjectU5BU5D_t11523773*)L_13;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 4);
		ArrayElementTypeCheck (L_15, _stringLiteral41);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_20 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 0);
		ArrayElementTypeCheck (L_20, _stringLiteral1932880146);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1932880146);
		ObjectU5BU5D_t11523773* L_21 = (ObjectU5BU5D_t11523773*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 1);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t11523773* L_23 = (ObjectU5BU5D_t11523773*)L_21;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 2);
		ArrayElementTypeCheck (L_23, _stringLiteral3718369476);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_24 = (ObjectU5BU5D_t11523773*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 3);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t11523773* L_26 = (ObjectU5BU5D_t11523773*)L_24;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 4);
		ArrayElementTypeCheck (L_26, _stringLiteral41);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t1766303790 * L_31 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2905908171 *, Vector3_t3525329789 , Vector3_t3525329789 , float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, (TweenerCore_3_t2905908171 *)__this, (Vector3_t3525329789 )((*(Vector3_t3525329789 *)((Vector3_t3525329789 *)UnBox (L_28, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (Vector3_t3525329789 )((*(Vector3_t3525329789 *)((Vector3_t3525329789 *)UnBox (L_29, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t1766303790 * TweenerCore_3_SetFrom_m1824760697_gshared (TweenerCore_3_t2905908171 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t3798787146 * L_0 = (ABSTweenPlugin_3_t3798787146 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t3798787146 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t2905908171 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t3798787146 *)L_0, (TweenerCore_3_t2905908171 *)__this, (bool)L_1);
		((Tweener_t1766303790 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m1844907998_gshared (TweenerCore_3_t2905908171 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t1103364673 *)__this);
		Tween_Reset_m1561947655((Tween_t1103364673 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t3798787146 * L_0 = (ABSTweenPlugin_3_t3798787146 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t3798787146 * L_1 = (ABSTweenPlugin_3_t3798787146 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t3798787146 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t2905908171 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t3798787146 *)L_1, (TweenerCore_3_t2905908171 *)__this);
	}

IL_001a:
	{
		VectorOptions_t3308462279  L_2 = ((  VectorOptions_t3308462279  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t2219490770 *)NULL);
		__this->set_setter_58((DOSetter_1_t3256130598 *)NULL);
		((Tweener_t1766303790 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t1766303790 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::Validate()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_Validate_m2611601649_MetadataUsageId;
extern "C"  bool TweenerCore_3_Validate_m2611601649_gshared (TweenerCore_3_t2905908171 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m2611601649_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t2219490770 * L_0 = (DOGetter_1_t2219490770 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t2219490770 *)L_0);
		((  Vector3_t3525329789  (*) (DOGetter_1_t2219490770 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((DOGetter_1_t2219490770 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m2827977462_gshared (TweenerCore_3_t2905908171 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2905908171 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(NULL /*static, unused*/, (TweenerCore_3_t2905908171 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m2113763492_gshared (TweenerCore_3_t2905908171 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2905908171 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)(NULL /*static, unused*/, (TweenerCore_3_t2905908171 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern Il2CppClass* DOTween_t3585775766_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_ApplyTween_m371168695_MetadataUsageId;
extern "C"  bool TweenerCore_3_ApplyTween_m371168695_gshared (TweenerCore_3_t2905908171 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m371168695_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t1103364673 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t1103364673 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t3585775766_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t3585775766_StaticFields*)DOTween_t3585775766_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t3798787146 * L_5 = (ABSTweenPlugin_3_t3798787146 *)__this->get_tweenPlugin_59();
		VectorOptions_t3308462279  L_6 = (VectorOptions_t3308462279 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t1103364673 *)__this)->get_isRelative_27();
		DOGetter_1_t2219490770 * L_8 = (DOGetter_1_t2219490770 *)__this->get_getter_57();
		DOSetter_1_t3256130598 * L_9 = (DOSetter_1_t3256130598 *)__this->get_setter_58();
		float L_10 = V_0;
		Vector3_t3525329789  L_11 = (Vector3_t3525329789 )__this->get_startValue_53();
		Vector3_t3525329789  L_12 = (Vector3_t3525329789 )__this->get_changeValue_55();
		float L_13 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t3798787146 *)L_5);
		VirtActionInvoker11< VectorOptions_t3308462279 , Tween_t1103364673 *, bool, DOGetter_1_t2219490770 *, DOSetter_1_t3256130598 *, float, Vector3_t3525329789 , Vector3_t3525329789 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t3798787146 *)L_5, (VectorOptions_t3308462279 )L_6, (Tween_t1103364673 *)__this, (bool)L_7, (DOGetter_1_t2219490770 *)L_8, (DOSetter_1_t3256130598 *)L_9, (float)L_10, (Vector3_t3525329789 )L_11, (Vector3_t3525329789 )L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t3798787146 * L_16 = (ABSTweenPlugin_3_t3798787146 *)__this->get_tweenPlugin_59();
		VectorOptions_t3308462279  L_17 = (VectorOptions_t3308462279 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t1103364673 *)__this)->get_isRelative_27();
		DOGetter_1_t2219490770 * L_19 = (DOGetter_1_t2219490770 *)__this->get_getter_57();
		DOSetter_1_t3256130598 * L_20 = (DOSetter_1_t3256130598 *)__this->get_setter_58();
		float L_21 = V_0;
		Vector3_t3525329789  L_22 = (Vector3_t3525329789 )__this->get_startValue_53();
		Vector3_t3525329789  L_23 = (Vector3_t3525329789 )__this->get_changeValue_55();
		float L_24 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t3798787146 *)L_16);
		VirtActionInvoker11< VectorOptions_t3308462279 , Tween_t1103364673 *, bool, DOGetter_1_t2219490770 *, DOSetter_1_t3256130598 *, float, Vector3_t3525329789 , Vector3_t3525329789 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t3798787146 *)L_16, (VectorOptions_t3308462279 )L_17, (Tween_t1103364673 *)__this, (bool)L_18, (DOGetter_1_t2219490770 *)L_19, (DOSetter_1_t3256130598 *)L_20, (float)L_21, (Vector3_t3525329789 )L_22, (Vector3_t3525329789 )L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::.ctor()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3__ctor_m1317420467_MetadataUsageId;
extern "C"  void TweenerCore_3__ctor_m1317420467_gshared (TweenerCore_3_t763702783 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m1317420467_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Tweener_t1766303790 *)__this);
		Tweener__ctor_m1348308877((Tweener_t1766303790 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t1103364673 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2598715689 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t1103364673 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t1103364673 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::ChangeStartValue(System.Object,System.Single)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1899955427;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeStartValue_m3493427984_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeStartValue_m3493427984_gshared (TweenerCore_3_t763702783 * __this, Il2CppObject * ___newStartValue0, float ___newDuration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeStartValue_m3493427984_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_7 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, _stringLiteral1899955427);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1899955427);
		ObjectU5BU5D_t11523773* L_8 = (ObjectU5BU5D_t11523773*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		ArrayElementTypeCheck (L_10, _stringLiteral3718369476);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_11 = (ObjectU5BU5D_t11523773*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 3);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_11;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral41);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newStartValue0;
		float L_16 = ___newDuration1;
		Tweener_t1766303790 * L_17 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t763702783 *, Vector4_t3525329790 , float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, (TweenerCore_3_t763702783 *)__this, (Vector4_t3525329790 )((*(Vector4_t3525329790 *)((Vector4_t3525329790 *)UnBox (L_15, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeEndValue_m651848665_gshared (TweenerCore_3_t763702783 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t1766303790 *)__this);
		Tweener_t1766303790 * L_2 = VirtFuncInvoker3< Tweener_t1766303790 *, Il2CppObject *, float, bool >::Invoke(10 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t1766303790 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1647271171;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeEndValue_m914770260_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeEndValue_m914770260_gshared (TweenerCore_3_t763702783 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m914770260_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_7 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, _stringLiteral1647271171);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1647271171);
		ObjectU5BU5D_t11523773* L_8 = (ObjectU5BU5D_t11523773*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		ArrayElementTypeCheck (L_10, _stringLiteral3718369476);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_11 = (ObjectU5BU5D_t11523773*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 3);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_11;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral41);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t1766303790 * L_18 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t763702783 *, Vector4_t3525329790 , float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (TweenerCore_3_t763702783 *)__this, (Vector4_t3525329790 )((*(Vector4_t3525329790 *)((Vector4_t3525329790 *)UnBox (L_15, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::ChangeValues(System.Object,System.Object,System.Single)
extern Il2CppClass* Debugger_t131542730_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2661037621;
extern Il2CppCodeGenString* _stringLiteral1932880146;
extern Il2CppCodeGenString* _stringLiteral3718369476;
extern Il2CppCodeGenString* _stringLiteral41;
extern const uint32_t TweenerCore_3_ChangeValues_m2765130955_MetadataUsageId;
extern "C"  Tweener_t1766303790 * TweenerCore_3_ChangeValues_m2765130955_gshared (TweenerCore_3_t763702783 * __this, Il2CppObject * ___newStartValue0, Il2CppObject * ___newEndValue1, float ___newDuration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeValues_m2765130955_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	{
		bool L_0 = (bool)((Tween_t1103364673 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2661037621, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newStartValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Il2CppObject * L_4 = ___newEndValue1;
		NullCheck((Il2CppObject *)L_4);
		Type_t * L_5 = Object_GetType_m2022236990((Il2CppObject *)L_4, /*hidden argument*/NULL);
		V_1 = (Type_t *)L_5;
		Type_t * L_6 = V_0;
		Type_t * L_7 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_8 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_8) < ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_9 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		ArrayElementTypeCheck (L_9, _stringLiteral1932880146);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1932880146);
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)L_9;
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 1);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t11523773* L_12 = (ObjectU5BU5D_t11523773*)L_10;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 2);
		ArrayElementTypeCheck (L_12, _stringLiteral3718369476);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_12;
		Type_t * L_14 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 3);
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_14);
		ObjectU5BU5D_t11523773* L_15 = (ObjectU5BU5D_t11523773*)L_13;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 4);
		ArrayElementTypeCheck (L_15, _stringLiteral41);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_15, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}

IL_0072:
	{
		Type_t * L_17 = V_1;
		Type_t * L_18 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_18)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_19 = ((Debugger_t131542730_StaticFields*)Debugger_t131542730_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_19) < ((int32_t)1)))
		{
			goto IL_00b8;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_20 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 0);
		ArrayElementTypeCheck (L_20, _stringLiteral1932880146);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1932880146);
		ObjectU5BU5D_t11523773* L_21 = (ObjectU5BU5D_t11523773*)L_20;
		Type_t * L_22 = V_1;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 1);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t11523773* L_23 = (ObjectU5BU5D_t11523773*)L_21;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 2);
		ArrayElementTypeCheck (L_23, _stringLiteral3718369476);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3718369476);
		ObjectU5BU5D_t11523773* L_24 = (ObjectU5BU5D_t11523773*)L_23;
		Type_t * L_25 = (Type_t *)((Tween_t1103364673 *)__this)->get_typeofT2_33();
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 3);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_25);
		ObjectU5BU5D_t11523773* L_26 = (ObjectU5BU5D_t11523773*)L_24;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 4);
		ArrayElementTypeCheck (L_26, _stringLiteral41);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_26, /*hidden argument*/NULL);
		Debugger_LogWarning_m2505295565(NULL /*static, unused*/, (Il2CppObject *)L_27, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return __this;
	}

IL_00ba:
	{
		Il2CppObject * L_28 = ___newStartValue0;
		Il2CppObject * L_29 = ___newEndValue1;
		float L_30 = ___newDuration2;
		Tweener_t1766303790 * L_31 = ((  Tweener_t1766303790 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t763702783 *, Vector4_t3525329790 , Vector4_t3525329790 , float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, (TweenerCore_3_t763702783 *)__this, (Vector4_t3525329790 )((*(Vector4_t3525329790 *)((Vector4_t3525329790 *)UnBox (L_28, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (Vector4_t3525329790 )((*(Vector4_t3525329790 *)((Vector4_t3525329790 *)UnBox (L_29, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), (float)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_31;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t1766303790 * TweenerCore_3_SetFrom_m1193080827_gshared (TweenerCore_3_t763702783 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t1656581758 * L_0 = (ABSTweenPlugin_3_t1656581758 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t1656581758 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t763702783 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t1656581758 *)L_0, (TweenerCore_3_t763702783 *)__this, (bool)L_1);
		((Tweener_t1766303790 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m3258820704_gshared (TweenerCore_3_t763702783 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t1103364673 *)__this);
		Tween_Reset_m1561947655((Tween_t1103364673 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t1656581758 * L_0 = (ABSTweenPlugin_3_t1656581758 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t1656581758 * L_1 = (ABSTweenPlugin_3_t1656581758 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t1656581758 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t763702783 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t1656581758 *)L_1, (TweenerCore_3_t763702783 *)__this);
	}

IL_001a:
	{
		VectorOptions_t3308462279  L_2 = ((  VectorOptions_t3308462279  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t2219490771 *)NULL);
		__this->set_setter_58((DOSetter_1_t3256130599 *)NULL);
		((Tweener_t1766303790 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t1766303790 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::Validate()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_Validate_m3740754223_MetadataUsageId;
extern "C"  bool TweenerCore_3_Validate_m3740754223_gshared (TweenerCore_3_t763702783 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_Validate_m3740754223_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t2219490771 * L_0 = (DOGetter_1_t2219490771 *)__this->get_getter_57();
		NullCheck((DOGetter_1_t2219490771 *)L_0);
		((  Vector4_t3525329790  (*) (DOGetter_1_t2219490771 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((DOGetter_1_t2219490771 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return (bool)1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m704694068_gshared (TweenerCore_3_t763702783 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t763702783 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(NULL /*static, unused*/, (TweenerCore_3_t763702783 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m3674208422_gshared (TweenerCore_3_t763702783 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t763702783 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)(NULL /*static, unused*/, (TweenerCore_3_t763702783 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern Il2CppClass* DOTween_t3585775766_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_ApplyTween_m1408380277_MetadataUsageId;
extern "C"  bool TweenerCore_3_ApplyTween_m1408380277_gshared (TweenerCore_3_t763702783 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m1408380277_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t1103364673 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t1103364673 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t3585775766_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t3585775766_StaticFields*)DOTween_t3585775766_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t1656581758 * L_5 = (ABSTweenPlugin_3_t1656581758 *)__this->get_tweenPlugin_59();
		VectorOptions_t3308462279  L_6 = (VectorOptions_t3308462279 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t1103364673 *)__this)->get_isRelative_27();
		DOGetter_1_t2219490771 * L_8 = (DOGetter_1_t2219490771 *)__this->get_getter_57();
		DOSetter_1_t3256130599 * L_9 = (DOSetter_1_t3256130599 *)__this->get_setter_58();
		float L_10 = V_0;
		Vector4_t3525329790  L_11 = (Vector4_t3525329790 )__this->get_startValue_53();
		Vector4_t3525329790  L_12 = (Vector4_t3525329790 )__this->get_changeValue_55();
		float L_13 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t1656581758 *)L_5);
		VirtActionInvoker11< VectorOptions_t3308462279 , Tween_t1103364673 *, bool, DOGetter_1_t2219490771 *, DOSetter_1_t3256130599 *, float, Vector4_t3525329790 , Vector4_t3525329790 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t1656581758 *)L_5, (VectorOptions_t3308462279 )L_6, (Tween_t1103364673 *)__this, (bool)L_7, (DOGetter_1_t2219490771 *)L_8, (DOSetter_1_t3256130599 *)L_9, (float)L_10, (Vector4_t3525329790 )L_11, (Vector4_t3525329790 )L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t1656581758 * L_16 = (ABSTweenPlugin_3_t1656581758 *)__this->get_tweenPlugin_59();
		VectorOptions_t3308462279  L_17 = (VectorOptions_t3308462279 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t1103364673 *)__this)->get_isRelative_27();
		DOGetter_1_t2219490771 * L_19 = (DOGetter_1_t2219490771 *)__this->get_getter_57();
		DOSetter_1_t3256130599 * L_20 = (DOSetter_1_t3256130599 *)__this->get_setter_58();
		float L_21 = V_0;
		Vector4_t3525329790  L_22 = (Vector4_t3525329790 )__this->get_startValue_53();
		Vector4_t3525329790  L_23 = (Vector4_t3525329790 )__this->get_changeValue_55();
		float L_24 = (float)((Tween_t1103364673 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t1656581758 *)L_16);
		VirtActionInvoker11< VectorOptions_t3308462279 , Tween_t1103364673 *, bool, DOGetter_1_t2219490771 *, DOSetter_1_t3256130599 *, float, Vector4_t3525329790 , Vector4_t3525329790 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t1656581758 *)L_16, (VectorOptions_t3308462279 )L_17, (Tween_t1103364673 *)__this, (bool)L_18, (DOGetter_1_t2219490771 *)L_19, (DOSetter_1_t3256130599 *)L_20, (float)L_21, (Vector4_t3525329790 )L_22, (Vector4_t3525329790 )L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m1597813448_gshared (ABSTweenPlugin_3_t1075249930 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m540381442_gshared (ABSTweenPlugin_3_t2856278496 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m3889128410_gshared (ABSTweenPlugin_3_t3447721284 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m3205166650_gshared (ABSTweenPlugin_3_t1801672336 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m485400414_gshared (ABSTweenPlugin_3_t2512169016 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m3600800014_gshared (ABSTweenPlugin_3_t808594408 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m2474082073_gshared (ABSTweenPlugin_3_t3734373201 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m1485617554_gshared (ABSTweenPlugin_3_t822187136 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m2396976144_gshared (ABSTweenPlugin_3_t3471105484 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m840044584_gshared (ABSTweenPlugin_3_t887052358 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m545513434_gshared (ABSTweenPlugin_3_t2861657316 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m3787176431_gshared (ABSTweenPlugin_3_t339311853 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m307684631_gshared (ABSTweenPlugin_3_t3167545241 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m3772318544_gshared (ABSTweenPlugin_3_t1646025238 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m2241310549_gshared (ABSTweenPlugin_3_t2713663477 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m3354861585_gshared (ABSTweenPlugin_3_t2509228721 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m891263954_gshared (ABSTweenPlugin_3_t3798787146 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m2305176660_gshared (ABSTweenPlugin_3_t1656581758 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.TweenCallback`1<System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void TweenCallback_1__ctor_m3659365073_gshared (TweenCallback_1_t1868435431 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.TweenCallback`1<System.Int32>::Invoke(T)
extern "C"  void TweenCallback_1_Invoke_m2284046419_gshared (TweenCallback_1_t1868435431 * __this, int32_t ___value0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		TweenCallback_1_Invoke_m2284046419((TweenCallback_1_t1868435431 *)__this->get_prev_9(),___value0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___value0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___value0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___value0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___value0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.TweenCallback`1<System.Int32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t TweenCallback_1_BeginInvoke_m1739889576_MetadataUsageId;
extern "C"  Il2CppObject * TweenCallback_1_BeginInvoke_m1739889576_gshared (TweenCallback_1_t1868435431 * __this, int32_t ___value0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenCallback_1_BeginInvoke_m1739889576_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &___value0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.TweenCallback`1<System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  void TweenCallback_1_EndInvoke_m1371077985_gshared (TweenCallback_1_t1868435431 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.TweenCallback`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void TweenCallback_1__ctor_m3780798908_gshared (TweenCallback_1_t4153094360 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.TweenCallback`1<System.Object>::Invoke(T)
extern "C"  void TweenCallback_1_Invoke_m857956104_gshared (TweenCallback_1_t4153094360 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		TweenCallback_1_Invoke_m857956104((TweenCallback_1_t4153094360 *)__this->get_prev_9(),___value0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___value0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___value0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___value0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___value0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___value0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.TweenCallback`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * TweenCallback_1_BeginInvoke_m2140545749_gshared (TweenCallback_1_t4153094360 * __this, Il2CppObject * ___value0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___value0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.TweenCallback`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void TweenCallback_1_EndInvoke_m1140857292_gshared (TweenCallback_1_t4153094360 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.TweenCallback`1<System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void TweenCallback_1__ctor_m3893537363_gshared (TweenCallback_1_t4274196961 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.TweenCallback`1<System.Single>::Invoke(T)
extern "C"  void TweenCallback_1_Invoke_m2871851665_gshared (TweenCallback_1_t4274196961 * __this, float ___value0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		TweenCallback_1_Invoke_m2871851665((TweenCallback_1_t4274196961 *)__this->get_prev_9(),___value0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, float ___value0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___value0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, float ___value0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___value0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.TweenCallback`1<System.Single>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Single_t958209021_il2cpp_TypeInfo_var;
extern const uint32_t TweenCallback_1_BeginInvoke_m3059728734_MetadataUsageId;
extern "C"  Il2CppObject * TweenCallback_1_BeginInvoke_m3059728734_gshared (TweenCallback_1_t4274196961 * __this, float ___value0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TweenCallback_1_BeginInvoke_m3059728734_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Single_t958209021_il2cpp_TypeInfo_var, &___value0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.TweenCallback`1<System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  void TweenCallback_1_EndInvoke_m1361460707_gshared (TweenCallback_1_t4274196961 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void Facebook.Unity.Canvas.CanvasFacebook/CanvasUIMethodCall`1<System.Object>::.ctor(Facebook.Unity.Canvas.CanvasFacebook,System.String,System.String)
extern "C"  void CanvasUIMethodCall_1__ctor_m2024179164_gshared (CanvasUIMethodCall_1_t4051398709 * __this, CanvasFacebook_t943264545 * ___canvasImpl0, String_t* ___methodName1, String_t* ___callbackMethod2, const MethodInfo* method)
{
	{
		CanvasFacebook_t943264545 * L_0 = ___canvasImpl0;
		String_t* L_1 = ___methodName1;
		NullCheck((MethodCall_1_t2121494416 *)__this);
		((  void (*) (MethodCall_1_t2121494416 *, FacebookBase_t2319813814 *, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((MethodCall_1_t2121494416 *)__this, (FacebookBase_t2319813814 *)L_0, (String_t*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		CanvasFacebook_t943264545 * L_2 = ___canvasImpl0;
		__this->set_canvasImpl_4(L_2);
		String_t* L_3 = ___callbackMethod2;
		__this->set_callbackMethod_5(L_3);
		return;
	}
}
// System.Void Facebook.Unity.Canvas.CanvasFacebook/CanvasUIMethodCall`1<System.Object>::Call(Facebook.Unity.MethodArguments)
extern "C"  void CanvasUIMethodCall_1_Call_m4214808791_gshared (CanvasUIMethodCall_1_t4051398709 * __this, MethodArguments_t3878806324 * ___args0, const MethodInfo* method)
{
	{
		NullCheck((MethodCall_1_t2121494416 *)__this);
		String_t* L_0 = ((  String_t* (*) (MethodCall_1_t2121494416 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((MethodCall_1_t2121494416 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		MethodArguments_t3878806324 * L_1 = ___args0;
		NullCheck((MethodCall_1_t2121494416 *)__this);
		FacebookDelegate_1_t1473468476 * L_2 = ((  FacebookDelegate_1_t1473468476 * (*) (MethodCall_1_t2121494416 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((MethodCall_1_t2121494416 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((CanvasUIMethodCall_1_t4051398709 *)__this);
		((  void (*) (CanvasUIMethodCall_1_t4051398709 *, String_t*, MethodArguments_t3878806324 *, FacebookDelegate_1_t1473468476 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((CanvasUIMethodCall_1_t4051398709 *)__this, (String_t*)L_0, (MethodArguments_t3878806324 *)L_1, (FacebookDelegate_1_t1473468476 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
// System.Void Facebook.Unity.Canvas.CanvasFacebook/CanvasUIMethodCall`1<System.Object>::UI(System.String,Facebook.Unity.MethodArguments,Facebook.Unity.FacebookDelegate`1<T>)
extern Il2CppClass* ICanvasJSWrapper_t1740207084_il2cpp_TypeInfo_var;
extern Il2CppClass* MethodArguments_t3878806324_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2883893241;
extern Il2CppCodeGenString* _stringLiteral3217412321;
extern Il2CppCodeGenString* _stringLiteral4164035721;
extern const uint32_t CanvasUIMethodCall_1_UI_m1731377964_MetadataUsageId;
extern "C"  void CanvasUIMethodCall_1_UI_m1731377964_gshared (CanvasUIMethodCall_1_t4051398709 * __this, String_t* ___method0, MethodArguments_t3878806324 * ___args1, FacebookDelegate_1_t1473468476 * ___callback2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CanvasUIMethodCall_1_UI_m1731377964_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	MethodArguments_t3878806324 * V_0 = NULL;
	String_t* V_1 = NULL;
	{
		CanvasFacebook_t943264545 * L_0 = (CanvasFacebook_t943264545 *)__this->get_canvasImpl_4();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_canvasJSWrapper_14();
		NullCheck((Il2CppObject *)L_1);
		InterfaceActionInvoker0::Invoke(3 /* System.Void Facebook.Unity.Canvas.ICanvasJSWrapper::DisableFullScreen() */, ICanvasJSWrapper_t1740207084_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
		MethodArguments_t3878806324 * L_2 = ___args1;
		MethodArguments_t3878806324 * L_3 = (MethodArguments_t3878806324 *)il2cpp_codegen_object_new(MethodArguments_t3878806324_il2cpp_TypeInfo_var);
		MethodArguments__ctor_m2910977459(L_3, (MethodArguments_t3878806324 *)L_2, /*hidden argument*/NULL);
		V_0 = (MethodArguments_t3878806324 *)L_3;
		MethodArguments_t3878806324 * L_4 = V_0;
		CanvasFacebook_t943264545 * L_5 = (CanvasFacebook_t943264545 *)__this->get_canvasImpl_4();
		NullCheck(L_5);
		String_t* L_6 = (String_t*)L_5->get_appId_12();
		NullCheck((MethodArguments_t3878806324 *)L_4);
		MethodArguments_AddString_m620728135((MethodArguments_t3878806324 *)L_4, (String_t*)_stringLiteral2883893241, (String_t*)L_6, /*hidden argument*/NULL);
		MethodArguments_t3878806324 * L_7 = V_0;
		String_t* L_8 = ___method0;
		NullCheck((MethodArguments_t3878806324 *)L_7);
		MethodArguments_AddString_m620728135((MethodArguments_t3878806324 *)L_7, (String_t*)_stringLiteral3217412321, (String_t*)L_8, /*hidden argument*/NULL);
		CanvasFacebook_t943264545 * L_9 = (CanvasFacebook_t943264545 *)__this->get_canvasImpl_4();
		NullCheck((FacebookBase_t2319813814 *)L_9);
		CallbackManager_t1943358823 * L_10 = FacebookBase_get_CallbackManager_m3167100966((FacebookBase_t2319813814 *)L_9, /*hidden argument*/NULL);
		FacebookDelegate_1_t1473468476 * L_11 = ___callback2;
		NullCheck((CallbackManager_t1943358823 *)L_10);
		String_t* L_12 = ((  String_t* (*) (CallbackManager_t1943358823 *, FacebookDelegate_1_t1473468476 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((CallbackManager_t1943358823 *)L_10, (FacebookDelegate_1_t1473468476 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_1 = (String_t*)L_12;
		CanvasFacebook_t943264545 * L_13 = (CanvasFacebook_t943264545 *)__this->get_canvasImpl_4();
		NullCheck(L_13);
		Il2CppObject * L_14 = (Il2CppObject *)L_13->get_canvasJSWrapper_14();
		ObjectU5BU5D_t11523773* L_15 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)3));
		MethodArguments_t3878806324 * L_16 = V_0;
		NullCheck((MethodArguments_t3878806324 *)L_16);
		String_t* L_17 = MethodArguments_ToJsonString_m3811311828((MethodArguments_t3878806324 *)L_16, /*hidden argument*/NULL);
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 0);
		ArrayElementTypeCheck (L_15, L_17);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_17);
		ObjectU5BU5D_t11523773* L_18 = (ObjectU5BU5D_t11523773*)L_15;
		String_t* L_19 = V_1;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 1);
		ArrayElementTypeCheck (L_18, L_19);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_19);
		ObjectU5BU5D_t11523773* L_20 = (ObjectU5BU5D_t11523773*)L_18;
		String_t* L_21 = (String_t*)__this->get_callbackMethod_5();
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 2);
		ArrayElementTypeCheck (L_20, L_21);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_21);
		NullCheck((Il2CppObject *)L_14);
		InterfaceActionInvoker2< String_t*, ObjectU5BU5D_t11523773* >::Invoke(2 /* System.Void Facebook.Unity.Canvas.ICanvasJSWrapper::ExternalCall(System.String,System.Object[]) */, ICanvasJSWrapper_t1740207084_il2cpp_TypeInfo_var, (Il2CppObject *)L_14, (String_t*)_stringLiteral4164035721, (ObjectU5BU5D_t11523773*)L_20);
		return;
	}
}
// System.Void Facebook.Unity.FacebookDelegate`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void FacebookDelegate_1__ctor_m823687686_gshared (FacebookDelegate_1_t1473468476 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Facebook.Unity.FacebookDelegate`1<System.Object>::Invoke(T)
extern "C"  void FacebookDelegate_1_Invoke_m3541671806_gshared (FacebookDelegate_1_t1473468476 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		FacebookDelegate_1_Invoke_m3541671806((FacebookDelegate_1_t1473468476 *)__this->get_prev_9(),___result0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___result0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___result0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___result0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___result0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___result0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult Facebook.Unity.FacebookDelegate`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * FacebookDelegate_1_BeginInvoke_m222181963_gshared (FacebookDelegate_1_t1473468476 * __this, Il2CppObject * ___result0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___result0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void Facebook.Unity.FacebookDelegate`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void FacebookDelegate_1_EndInvoke_m3390430998_gshared (FacebookDelegate_1_t1473468476 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void Facebook.Unity.MethodCall`1<System.Object>::.ctor(Facebook.Unity.FacebookBase,System.String)
extern Il2CppClass* MethodArguments_t3878806324_il2cpp_TypeInfo_var;
extern const uint32_t MethodCall_1__ctor_m1960000006_MetadataUsageId;
extern "C"  void MethodCall_1__ctor_m1960000006_gshared (MethodCall_1_t2121494416 * __this, FacebookBase_t2319813814 * ___facebookImpl0, String_t* ___methodName1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MethodCall_1__ctor_m1960000006_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		MethodArguments_t3878806324 * L_0 = (MethodArguments_t3878806324 *)il2cpp_codegen_object_new(MethodArguments_t3878806324_il2cpp_TypeInfo_var);
		MethodArguments__ctor_m2104387431(L_0, /*hidden argument*/NULL);
		NullCheck((MethodCall_1_t2121494416 *)__this);
		((  void (*) (MethodCall_1_t2121494416 *, MethodArguments_t3878806324 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((MethodCall_1_t2121494416 *)__this, (MethodArguments_t3878806324 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		FacebookBase_t2319813814 * L_1 = ___facebookImpl0;
		NullCheck((MethodCall_1_t2121494416 *)__this);
		((  void (*) (MethodCall_1_t2121494416 *, FacebookBase_t2319813814 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((MethodCall_1_t2121494416 *)__this, (FacebookBase_t2319813814 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		String_t* L_2 = ___methodName1;
		NullCheck((MethodCall_1_t2121494416 *)__this);
		((  void (*) (MethodCall_1_t2121494416 *, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((MethodCall_1_t2121494416 *)__this, (String_t*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
// System.String Facebook.Unity.MethodCall`1<System.Object>::get_MethodName()
extern "C"  String_t* MethodCall_1_get_MethodName_m759377580_gshared (MethodCall_1_t2121494416 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (String_t*)__this->get_U3CMethodNameU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void Facebook.Unity.MethodCall`1<System.Object>::set_MethodName(System.String)
extern "C"  void MethodCall_1_set_MethodName_m817744037_gshared (MethodCall_1_t2121494416 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CMethodNameU3Ek__BackingField_0(L_0);
		return;
	}
}
// Facebook.Unity.FacebookDelegate`1<T> Facebook.Unity.MethodCall`1<System.Object>::get_Callback()
extern "C"  FacebookDelegate_1_t1473468476 * MethodCall_1_get_Callback_m708582070_gshared (MethodCall_1_t2121494416 * __this, const MethodInfo* method)
{
	{
		FacebookDelegate_1_t1473468476 * L_0 = (FacebookDelegate_1_t1473468476 *)__this->get_U3CCallbackU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void Facebook.Unity.MethodCall`1<System.Object>::set_Callback(Facebook.Unity.FacebookDelegate`1<T>)
extern "C"  void MethodCall_1_set_Callback_m933548397_gshared (MethodCall_1_t2121494416 * __this, FacebookDelegate_1_t1473468476 * ___value0, const MethodInfo* method)
{
	{
		FacebookDelegate_1_t1473468476 * L_0 = ___value0;
		__this->set_U3CCallbackU3Ek__BackingField_1(L_0);
		return;
	}
}
// Facebook.Unity.FacebookBase Facebook.Unity.MethodCall`1<System.Object>::get_FacebookImpl()
extern "C"  FacebookBase_t2319813814 * MethodCall_1_get_FacebookImpl_m1272944382_gshared (MethodCall_1_t2121494416 * __this, const MethodInfo* method)
{
	{
		FacebookBase_t2319813814 * L_0 = (FacebookBase_t2319813814 *)__this->get_U3CFacebookImplU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void Facebook.Unity.MethodCall`1<System.Object>::set_FacebookImpl(Facebook.Unity.FacebookBase)
extern "C"  void MethodCall_1_set_FacebookImpl_m2871452611_gshared (MethodCall_1_t2121494416 * __this, FacebookBase_t2319813814 * ___value0, const MethodInfo* method)
{
	{
		FacebookBase_t2319813814 * L_0 = ___value0;
		__this->set_U3CFacebookImplU3Ek__BackingField_2(L_0);
		return;
	}
}
// Facebook.Unity.MethodArguments Facebook.Unity.MethodCall`1<System.Object>::get_Parameters()
extern "C"  MethodArguments_t3878806324 * MethodCall_1_get_Parameters_m2809693360_gshared (MethodCall_1_t2121494416 * __this, const MethodInfo* method)
{
	{
		MethodArguments_t3878806324 * L_0 = (MethodArguments_t3878806324 *)__this->get_U3CParametersU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void Facebook.Unity.MethodCall`1<System.Object>::set_Parameters(Facebook.Unity.MethodArguments)
extern "C"  void MethodCall_1_set_Parameters_m2033621031_gshared (MethodCall_1_t2121494416 * __this, MethodArguments_t3878806324 * ___value0, const MethodInfo* method)
{
	{
		MethodArguments_t3878806324 * L_0 = ___value0;
		__this->set_U3CParametersU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void Facebook.Unity.Mobile.Android.AndroidFacebook/JavaMethodCall`1<System.Object>::.ctor(Facebook.Unity.Mobile.Android.AndroidFacebook,System.String)
extern "C"  void JavaMethodCall_1__ctor_m3083954302_gshared (JavaMethodCall_1_t3793613819 * __this, AndroidFacebook_t1604313921 * ___androidImpl0, String_t* ___methodName1, const MethodInfo* method)
{
	{
		AndroidFacebook_t1604313921 * L_0 = ___androidImpl0;
		String_t* L_1 = ___methodName1;
		NullCheck((MethodCall_1_t2121494416 *)__this);
		((  void (*) (MethodCall_1_t2121494416 *, FacebookBase_t2319813814 *, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((MethodCall_1_t2121494416 *)__this, (FacebookBase_t2319813814 *)L_0, (String_t*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		AndroidFacebook_t1604313921 * L_2 = ___androidImpl0;
		__this->set_androidImpl_4(L_2);
		return;
	}
}
// System.Void Facebook.Unity.Mobile.Android.AndroidFacebook/JavaMethodCall`1<System.Object>::Call(Facebook.Unity.MethodArguments)
extern Il2CppClass* MethodArguments_t3878806324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1869655893;
extern const uint32_t JavaMethodCall_1_Call_m1503093513_MetadataUsageId;
extern "C"  void JavaMethodCall_1_Call_m1503093513_gshared (JavaMethodCall_1_t3793613819 * __this, MethodArguments_t3878806324 * ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JavaMethodCall_1_Call_m1503093513_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	MethodArguments_t3878806324 * V_0 = NULL;
	{
		MethodArguments_t3878806324 * L_0 = ___args0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		MethodArguments_t3878806324 * L_1 = (MethodArguments_t3878806324 *)il2cpp_codegen_object_new(MethodArguments_t3878806324_il2cpp_TypeInfo_var);
		MethodArguments__ctor_m2104387431(L_1, /*hidden argument*/NULL);
		V_0 = (MethodArguments_t3878806324 *)L_1;
		goto IL_0018;
	}

IL_0011:
	{
		MethodArguments_t3878806324 * L_2 = ___args0;
		MethodArguments_t3878806324 * L_3 = (MethodArguments_t3878806324 *)il2cpp_codegen_object_new(MethodArguments_t3878806324_il2cpp_TypeInfo_var);
		MethodArguments__ctor_m2910977459(L_3, (MethodArguments_t3878806324 *)L_2, /*hidden argument*/NULL);
		V_0 = (MethodArguments_t3878806324 *)L_3;
	}

IL_0018:
	{
		NullCheck((MethodCall_1_t2121494416 *)__this);
		FacebookDelegate_1_t1473468476 * L_4 = ((  FacebookDelegate_1_t1473468476 * (*) (MethodCall_1_t2121494416 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((MethodCall_1_t2121494416 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		if (!L_4)
		{
			goto IL_0044;
		}
	}
	{
		MethodArguments_t3878806324 * L_5 = V_0;
		AndroidFacebook_t1604313921 * L_6 = (AndroidFacebook_t1604313921 *)__this->get_androidImpl_4();
		NullCheck((FacebookBase_t2319813814 *)L_6);
		CallbackManager_t1943358823 * L_7 = FacebookBase_get_CallbackManager_m3167100966((FacebookBase_t2319813814 *)L_6, /*hidden argument*/NULL);
		NullCheck((MethodCall_1_t2121494416 *)__this);
		FacebookDelegate_1_t1473468476 * L_8 = ((  FacebookDelegate_1_t1473468476 * (*) (MethodCall_1_t2121494416 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((MethodCall_1_t2121494416 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		NullCheck((CallbackManager_t1943358823 *)L_7);
		String_t* L_9 = ((  String_t* (*) (CallbackManager_t1943358823 *, FacebookDelegate_1_t1473468476 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((CallbackManager_t1943358823 *)L_7, (FacebookDelegate_1_t1473468476 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((MethodArguments_t3878806324 *)L_5);
		MethodArguments_AddString_m620728135((MethodArguments_t3878806324 *)L_5, (String_t*)_stringLiteral1869655893, (String_t*)L_9, /*hidden argument*/NULL);
	}

IL_0044:
	{
		AndroidFacebook_t1604313921 * L_10 = (AndroidFacebook_t1604313921 *)__this->get_androidImpl_4();
		NullCheck((MethodCall_1_t2121494416 *)__this);
		String_t* L_11 = ((  String_t* (*) (MethodCall_1_t2121494416 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((MethodCall_1_t2121494416 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		MethodArguments_t3878806324 * L_12 = V_0;
		NullCheck((MethodArguments_t3878806324 *)L_12);
		String_t* L_13 = MethodArguments_ToJsonString_m3811311828((MethodArguments_t3878806324 *)L_12, /*hidden argument*/NULL);
		NullCheck((AndroidFacebook_t1604313921 *)L_10);
		AndroidFacebook_CallFB_m560138752((AndroidFacebook_t1604313921 *)L_10, (String_t*)L_11, (String_t*)L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Utilities/Callback`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Callback_1__ctor_m1218655815_gshared (Callback_1_t2504244479 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Facebook.Unity.Utilities/Callback`1<System.Object>::Invoke(T)
extern "C"  void Callback_1_Invoke_m4211213341_gshared (Callback_1_t2504244479 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Callback_1_Invoke_m4211213341((Callback_1_t2504244479 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult Facebook.Unity.Utilities/Callback`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Callback_1_BeginInvoke_m1576322282_gshared (Callback_1_t2504244479 * __this, Il2CppObject * ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___obj0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void Facebook.Unity.Utilities/Callback`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Callback_1_EndInvoke_m3978486743_gshared (Callback_1_t2504244479 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void FlurryAnalytics.MonoSingleton`1<System.Object>::.ctor()
extern "C"  void MonoSingleton_1__ctor_m3346382598_gshared (MonoSingleton_1_t858865150 * __this, const MethodInfo* method)
{
	{
		NullCheck((MonoBehaviour_t3012272455 *)__this);
		MonoBehaviour__ctor_m350695606((MonoBehaviour_t3012272455 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T FlurryAnalytics.MonoSingleton`1<System.Object>::get_Instance()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t4012695102_il2cpp_TypeInfo_var;
extern const uint32_t MonoSingleton_1_get_Instance_m3218306971_MetadataUsageId;
extern "C"  Il2CppObject * MonoSingleton_1_get_Instance_m3218306971_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoSingleton_1_get_Instance_m3218306971_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t4012695102 * V_0 = NULL;
	{
		bool L_0 = ((MonoSingleton_1_t858865150_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_IsDestroyed_3();
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		return ((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)));
	}

IL_0011:
	{
		Il2CppObject * L_1 = ((MonoSingleton_1_t858865150_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_Instance_2();
		bool L_2 = Object_op_Equality_m1690293457(NULL /*static, unused*/, (Object_t3878351788 *)L_1, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0093;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		Object_t3878351788 * L_4 = Object_FindObjectOfType_m709000164(NULL /*static, unused*/, (Type_t *)L_3, /*hidden argument*/NULL);
		((MonoSingleton_1_t858865150_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_s_Instance_2(((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1))), IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1))));
		Il2CppObject * L_5 = ((MonoSingleton_1_t858865150_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_Instance_2();
		bool L_6 = Object_op_Equality_m1690293457(NULL /*static, unused*/, (Object_t3878351788 *)L_5, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0093;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_7);
		GameObject_t4012695102 * L_9 = (GameObject_t4012695102 *)il2cpp_codegen_object_new(GameObject_t4012695102_il2cpp_TypeInfo_var);
		GameObject__ctor_m1336994365(L_9, (String_t*)L_8, /*hidden argument*/NULL);
		V_0 = (GameObject_t4012695102 *)L_9;
		GameObject_t4012695102 * L_10 = V_0;
		Object_DontDestroyOnLoad_m2587754829(NULL /*static, unused*/, (Object_t3878351788 *)L_10, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_11 = V_0;
		Type_t * L_12 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck((GameObject_t4012695102 *)L_11);
		Component_t2126946602 * L_13 = GameObject_AddComponent_m3062249957((GameObject_t4012695102 *)L_11, (Type_t *)L_12, /*hidden argument*/NULL);
		((MonoSingleton_1_t858865150_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_s_Instance_2(((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_13, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1))), IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1))));
	}

IL_0093:
	{
		Il2CppObject * L_14 = ((MonoSingleton_1_t858865150_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_Instance_2();
		return L_14;
	}
}
// System.Void FlurryAnalytics.MonoSingleton`1<System.Object>::OnDestroy()
extern "C"  void MonoSingleton_1_OnDestroy_m413442303_gshared (MonoSingleton_1_t858865150 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ((MonoSingleton_1_t858865150_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_Instance_2();
		bool L_1 = Object_op_Implicit_m3627549526(NULL /*static, unused*/, (Object_t3878351788 *)L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		Il2CppObject * L_2 = ((MonoSingleton_1_t858865150_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_Instance_2();
		Object_Destroy_m3720418393(NULL /*static, unused*/, (Object_t3878351788 *)L_2, /*hidden argument*/NULL);
	}

IL_0023:
	{
		((MonoSingleton_1_t858865150_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_s_Instance_2(((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1))));
		((MonoSingleton_1_t858865150_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_s_IsDestroyed_3((bool)1);
		return;
	}
}
// System.Void GenericDataSource`2<System.Object,System.Object>::.ctor()
extern Il2CppCodeGenString* _stringLiteral97;
extern const uint32_t GenericDataSource_2__ctor_m1409035199_MetadataUsageId;
extern "C"  void GenericDataSource_2__ctor_m1409035199_gshared (GenericDataSource_2_t1679676449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GenericDataSource_2__ctor_m1409035199_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_rId_1(_stringLiteral97);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GenericDataSource`2<System.Object,System.Object>::set_cellPrefab(UnityEngine.GameObject)
extern const MethodInfo* GameObject_GetComponent_TisRectTransform_t3317474837_m406276429_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisTableViewCell_t776419755_m1986117684_MethodInfo_var;
extern const uint32_t GenericDataSource_2_set_cellPrefab_m1627438784_MetadataUsageId;
extern "C"  void GenericDataSource_2_set_cellPrefab_m1627438784_gshared (GenericDataSource_2_t1679676449 * __this, GameObject_t4012695102 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GenericDataSource_2_set_cellPrefab_m1627438784_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t1525428817  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		GameObject_t4012695102 * L_0 = ___value0;
		__this->set__cellPrefab_3(L_0);
		GameObject_t4012695102 * L_1 = (GameObject_t4012695102 *)__this->get__cellPrefab_3();
		NullCheck((GameObject_t4012695102 *)L_1);
		RectTransform_t3317474837 * L_2 = GameObject_GetComponent_TisRectTransform_t3317474837_m406276429((GameObject_t4012695102 *)L_1, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t3317474837_m406276429_MethodInfo_var);
		NullCheck((RectTransform_t3317474837 *)L_2);
		Rect_t1525428817  L_3 = RectTransform_get_rect_m1566017036((RectTransform_t3317474837 *)L_2, /*hidden argument*/NULL);
		V_0 = (Rect_t1525428817 )L_3;
		float L_4 = Rect_get_height_m1885065262((Rect_t1525428817 *)(&V_0), /*hidden argument*/NULL);
		__this->set_cellHeight_4(L_4);
		GameObject_t4012695102 * L_5 = (GameObject_t4012695102 *)__this->get__cellPrefab_3();
		NullCheck((GameObject_t4012695102 *)L_5);
		TableViewCell_t776419755 * L_6 = GameObject_GetComponent_TisTableViewCell_t776419755_m1986117684((GameObject_t4012695102 *)L_5, /*hidden argument*/GameObject_GetComponent_TisTableViewCell_t776419755_m1986117684_MethodInfo_var);
		NullCheck((TableViewCell_t776419755 *)L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String Tacticsoft.TableViewCell::get_reuseIdentifier() */, (TableViewCell_t776419755 *)L_6);
		__this->set_rId_1(L_7);
		GameObject_t4012695102 * L_8 = (GameObject_t4012695102 *)__this->get__cellPrefab_3();
		NullCheck((GameObject_t4012695102 *)L_8);
		GameObject_SetActive_m3538205401((GameObject_t4012695102 *)L_8, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 GenericDataSource`2<System.Object,System.Object>::GetNumberOfRowsForTableView(Tacticsoft.TableView)
extern "C"  int32_t GenericDataSource_2_GetNumberOfRowsForTableView_m4207950081_gshared (GenericDataSource_2_t1679676449 * __this, TableView_t692333993 * ___tableView0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_data_0();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		G_B3_0 = 0;
		goto IL_001c;
	}

IL_0011:
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get_data_0();
		NullCheck((Il2CppObject*)L_1);
		int32_t L_2 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_1);
		G_B3_0 = L_2;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// System.Single GenericDataSource`2<System.Object,System.Object>::GetHeightForRowInTableView(Tacticsoft.TableView,System.Int32)
extern "C"  float GenericDataSource_2_GetHeightForRowInTableView_m2709442445_gshared (GenericDataSource_2_t1679676449 * __this, TableView_t692333993 * ___tableView0, int32_t ___row1, const MethodInfo* method)
{
	{
		float L_0 = (float)__this->get_cellHeight_4();
		return L_0;
	}
}
// Tacticsoft.TableViewCell GenericDataSource`2<System.Object,System.Object>::GetCellForRowInTableView(Tacticsoft.TableView,System.Int32)
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t4012695102_m3917608929_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisTableViewCell_t776419755_m1986117684_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2901800260;
extern const uint32_t GenericDataSource_2_GetCellForRowInTableView_m102593472_MetadataUsageId;
extern "C"  TableViewCell_t776419755 * GenericDataSource_2_GetCellForRowInTableView_m102593472_gshared (GenericDataSource_2_t1679676449 * __this, TableView_t692333993 * ___tableView0, int32_t ___row1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GenericDataSource_2_GetCellForRowInTableView_m102593472_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TableViewCell_t776419755 * V_0 = NULL;
	GameObject_t4012695102 * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	{
		TableView_t692333993 * L_0 = ___tableView0;
		String_t* L_1 = (String_t*)__this->get_rId_1();
		NullCheck((TableView_t692333993 *)L_0);
		TableViewCell_t776419755 * L_2 = TableView_GetReusableCell_m1366329345((TableView_t692333993 *)L_0, (String_t*)L_1, /*hidden argument*/NULL);
		V_0 = (TableViewCell_t776419755 *)L_2;
		TableViewCell_t776419755 * L_3 = V_0;
		bool L_4 = Object_op_Implicit_m3627549526(NULL /*static, unused*/, (Object_t3878351788 *)L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0043;
		}
	}
	{
		GameObject_t4012695102 * L_5 = (GameObject_t4012695102 *)__this->get__cellPrefab_3();
		GameObject_t4012695102 * L_6 = Object_Instantiate_TisGameObject_t4012695102_m3917608929(NULL /*static, unused*/, (GameObject_t4012695102 *)L_5, /*hidden argument*/Object_Instantiate_TisGameObject_t4012695102_m3917608929_MethodInfo_var);
		V_1 = (GameObject_t4012695102 *)L_6;
		TableView_t692333993 * L_7 = ___tableView0;
		NullCheck(L_7);
		DiContainer_t2383114449 * L_8 = (DiContainer_t2383114449 *)L_7->get_DiContainer_3();
		GameObject_t4012695102 * L_9 = V_1;
		NullCheck((DiContainer_t2383114449 *)L_8);
		VirtActionInvoker1< GameObject_t4012695102 * >::Invoke(62 /* System.Void Zenject.DiContainer::InjectGameObject(UnityEngine.GameObject) */, (DiContainer_t2383114449 *)L_8, (GameObject_t4012695102 *)L_9);
		GameObject_t4012695102 * L_10 = V_1;
		NullCheck((GameObject_t4012695102 *)L_10);
		TableViewCell_t776419755 * L_11 = GameObject_GetComponent_TisTableViewCell_t776419755_m1986117684((GameObject_t4012695102 *)L_10, /*hidden argument*/GameObject_GetComponent_TisTableViewCell_t776419755_m1986117684_MethodInfo_var);
		V_0 = (TableViewCell_t776419755 *)L_11;
		TableViewCell_t776419755 * L_12 = V_0;
		String_t* L_13 = (String_t*)__this->get_rId_1();
		NullCheck((TableViewCell_t776419755 *)L_12);
		VirtActionInvoker1< String_t* >::Invoke(5 /* System.Void Tacticsoft.TableViewCell::set_reuseIdentifier(System.String) */, (TableViewCell_t776419755 *)L_12, (String_t*)L_13);
	}

IL_0043:
	{
		TableViewCell_t776419755 * L_14 = V_0;
		NullCheck((Component_t2126946602 *)L_14);
		GameObject_t4012695102 * L_15 = Component_get_gameObject_m2112202034((Component_t2126946602 *)L_14, /*hidden argument*/NULL);
		NullCheck((GameObject_t4012695102 *)L_15);
		GameObject_SetActive_m3538205401((GameObject_t4012695102 *)L_15, (bool)1, /*hidden argument*/NULL);
		Il2CppObject* L_16 = (Il2CppObject*)__this->get_data_0();
		NullCheck((Il2CppObject*)L_16);
		int32_t L_17 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), (Il2CppObject*)L_16);
		int32_t L_18 = ___row1;
		if ((((int32_t)L_17) > ((int32_t)L_18)))
		{
			goto IL_006c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m1836388681(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2901800260, /*hidden argument*/NULL);
		TableViewCell_t776419755 * L_19 = V_0;
		return L_19;
	}

IL_006c:
	{
		Il2CppObject* L_20 = (Il2CppObject*)__this->get_data_0();
		int32_t L_21 = ___row1;
		NullCheck((Il2CppObject*)L_20);
		Il2CppObject * L_22 = InterfaceFuncInvoker1< Il2CppObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (Il2CppObject*)L_20, (int32_t)L_21);
		V_2 = (Il2CppObject *)L_22;
		Action_2_t4105459918 * L_23 = (Action_2_t4105459918 *)__this->get_fillFactory_2();
		Il2CppObject * L_24 = V_2;
		TableViewCell_t776419755 * L_25 = V_0;
		NullCheck((Component_t2126946602 *)L_25);
		Il2CppObject * L_26 = ((  Il2CppObject * (*) (Component_t2126946602 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Component_t2126946602 *)L_25, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		NullCheck((Action_2_t4105459918 *)L_23);
		((  void (*) (Action_2_t4105459918 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Action_2_t4105459918 *)L_23, (Il2CppObject *)L_24, (Il2CppObject *)L_26, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		TableViewCell_t776419755 * L_27 = V_0;
		NullCheck((Component_t2126946602 *)L_27);
		Il2CppObject * L_28 = ((  Il2CppObject * (*) (Component_t2126946602 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Component_t2126946602 *)L_27, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return (TableViewCell_t776419755 *)L_28;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator4`1<System.Object>::.ctor()
extern "C"  void U3CToEnumerableU3Ec__Iterator4_1__ctor_m2253352140_gshared (U3CToEnumerableU3Ec__Iterator4_1_t4039804212 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator4`1<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Il2CppObject * U3CToEnumerableU3Ec__Iterator4_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1583042899_gshared (U3CToEnumerableU3Ec__Iterator4_1_t4039804212 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_4();
		return L_0;
	}
}
// System.Object GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator4`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CToEnumerableU3Ec__Iterator4_1_System_Collections_IEnumerator_get_Current_m949637914_gshared (U3CToEnumerableU3Ec__Iterator4_1_t4039804212 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_4();
		return L_0;
	}
}
// System.Collections.IEnumerator GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator4`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CToEnumerableU3Ec__Iterator4_1_System_Collections_IEnumerable_GetEnumerator_m1545923861_gshared (U3CToEnumerableU3Ec__Iterator4_1_t4039804212 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CToEnumerableU3Ec__Iterator4_1_t4039804212 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CToEnumerableU3Ec__Iterator4_1_t4039804212 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((U3CToEnumerableU3Ec__Iterator4_1_t4039804212 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<T> GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator4`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* U3CToEnumerableU3Ec__Iterator4_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4203936214_gshared (U3CToEnumerableU3Ec__Iterator4_1_t4039804212 * __this, const MethodInfo* method)
{
	U3CToEnumerableU3Ec__Iterator4_1_t4039804212 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_3();
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CToEnumerableU3Ec__Iterator4_1_t4039804212 * L_2 = (U3CToEnumerableU3Ec__Iterator4_1_t4039804212 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (U3CToEnumerableU3Ec__Iterator4_1_t4039804212 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (U3CToEnumerableU3Ec__Iterator4_1_t4039804212 *)L_2;
		U3CToEnumerableU3Ec__Iterator4_1_t4039804212 * L_3 = V_0;
		UIntPtr_t  L_4 = (UIntPtr_t )__this->get_U3CU24U3Esize_5();
		NullCheck(L_3);
		L_3->set_size_1(L_4);
		U3CToEnumerableU3Ec__Iterator4_1_t4039804212 * L_5 = V_0;
		Func_2_t1403134703 * L_6 = (Func_2_t1403134703 *)__this->get_U3CU24U3EgetElement_6();
		NullCheck(L_5);
		L_5->set_getElement_2(L_6);
		U3CToEnumerableU3Ec__Iterator4_1_t4039804212 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator4`1<System.Object>::MoveNext()
extern "C"  bool U3CToEnumerableU3Ec__Iterator4_1_MoveNext_m3624260520_gshared (U3CToEnumerableU3Ec__Iterator4_1_t4039804212 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_3();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0056;
		}
	}
	{
		goto IL_0082;
	}

IL_0021:
	{
		__this->set_U3CiU3E__0_0((((int64_t)((int64_t)0))));
		goto IL_0065;
	}

IL_002e:
	{
		Func_2_t1403134703 * L_2 = (Func_2_t1403134703 *)__this->get_getElement_2();
		uint64_t L_3 = (uint64_t)__this->get_U3CiU3E__0_0();
		UIntPtr_t  L_4;
		memset(&L_4, 0, sizeof(L_4));
		UIntPtr__ctor_m3952300446(&L_4, (uint64_t)L_3, /*hidden argument*/NULL);
		NullCheck((Func_2_t1403134703 *)L_2);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (Func_2_t1403134703 *, UIntPtr_t , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Func_2_t1403134703 *)L_2, (UIntPtr_t )L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		__this->set_U24current_4(L_5);
		__this->set_U24PC_3(1);
		goto IL_0084;
	}

IL_0056:
	{
		uint64_t L_6 = (uint64_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int64_t)((int64_t)L_6+(int64_t)(((int64_t)((int64_t)1))))));
	}

IL_0065:
	{
		uint64_t L_7 = (uint64_t)__this->get_U3CiU3E__0_0();
		UIntPtr_t * L_8 = (UIntPtr_t *)__this->get_address_of_size_1();
		uint64_t L_9 = UIntPtr_ToUInt64_m958857238((UIntPtr_t *)L_8, /*hidden argument*/NULL);
		if ((!(((uint64_t)L_7) >= ((uint64_t)L_9))))
		{
			goto IL_002e;
		}
	}
	{
		__this->set_U24PC_3((-1));
	}

IL_0082:
	{
		return (bool)0;
	}

IL_0084:
	{
		return (bool)1;
	}
	// Dead block : IL_0086: ldloc.1
}
// System.Void GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator4`1<System.Object>::Dispose()
extern "C"  void U3CToEnumerableU3Ec__Iterator4_1_Dispose_m706172425_gshared (U3CToEnumerableU3Ec__Iterator4_1_t4039804212 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void GooglePlayGames.Native.PInvoke.PInvokeUtilities/<ToEnumerable>c__Iterator4`1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CToEnumerableU3Ec__Iterator4_1_Reset_m4194752377_MetadataUsageId;
extern "C"  void U3CToEnumerableU3Ec__Iterator4_1_Reset_m4194752377_gshared (U3CToEnumerableU3Ec__Iterator4_1_t4039804212 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CToEnumerableU3Ec__Iterator4_1_Reset_m4194752377_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutMethod`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void OutMethod_1__ctor_m3096051917_gshared (OutMethod_1_t3356189497 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.UIntPtr GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutMethod`1<System.Object>::Invoke(T[],System.UIntPtr)
extern "C"  UIntPtr_t  OutMethod_1_Invoke_m2612981764_gshared (OutMethod_1_t3356189497 * __this, ObjectU5BU5D_t11523773* ___out_bytes0, UIntPtr_t  ___out_size1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		OutMethod_1_Invoke_m2612981764((OutMethod_1_t3356189497 *)__this->get_prev_9(),___out_bytes0, ___out_size1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef UIntPtr_t  (*FunctionPointerType) (Il2CppObject *, void* __this, ObjectU5BU5D_t11523773* ___out_bytes0, UIntPtr_t  ___out_size1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___out_bytes0, ___out_size1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef UIntPtr_t  (*FunctionPointerType) (void* __this, ObjectU5BU5D_t11523773* ___out_bytes0, UIntPtr_t  ___out_size1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___out_bytes0, ___out_size1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef UIntPtr_t  (*FunctionPointerType) (void* __this, UIntPtr_t  ___out_size1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___out_bytes0, ___out_size1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutMethod`1<System.Object>::BeginInvoke(T[],System.UIntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* UIntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t OutMethod_1_BeginInvoke_m3366706221_MetadataUsageId;
extern "C"  Il2CppObject * OutMethod_1_BeginInvoke_m3366706221_gshared (OutMethod_1_t3356189497 * __this, ObjectU5BU5D_t11523773* ___out_bytes0, UIntPtr_t  ___out_size1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OutMethod_1_BeginInvoke_m3366706221_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___out_bytes0;
	__d_args[1] = Box(UIntPtr_t_il2cpp_TypeInfo_var, &___out_size1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.UIntPtr GooglePlayGames.Native.PInvoke.PInvokeUtilities/OutMethod`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  UIntPtr_t  OutMethod_1_EndInvoke_m1995468389_gshared (OutMethod_1_t3356189497 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(UIntPtr_t *)UnBox ((Il2CppCodeGenObject*)__result);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
