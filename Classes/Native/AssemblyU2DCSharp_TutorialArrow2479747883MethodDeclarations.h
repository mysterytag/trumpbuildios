﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TutorialArrow
struct TutorialArrow_t2479747883;

#include "codegen/il2cpp-codegen.h"

// System.Void TutorialArrow::.ctor()
extern "C"  void TutorialArrow__ctor_m138661536 (TutorialArrow_t2479747883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TutorialArrow::PostInject()
extern "C"  void TutorialArrow_PostInject_m2061396597 (TutorialArrow_t2479747883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
