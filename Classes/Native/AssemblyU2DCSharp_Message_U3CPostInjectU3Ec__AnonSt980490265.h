﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.RectTransform
struct RectTransform_t3317474837;
// Message
struct Message_t2619578343;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Message/<PostInject>c__AnonStorey58
struct  U3CPostInjectU3Ec__AnonStorey58_t980490265  : public Il2CppObject
{
public:
	// UnityEngine.RectTransform Message/<PostInject>c__AnonStorey58::iconRectTransform
	RectTransform_t3317474837 * ___iconRectTransform_0;
	// UnityEngine.RectTransform Message/<PostInject>c__AnonStorey58::textRectTransform
	RectTransform_t3317474837 * ___textRectTransform_1;
	// Message Message/<PostInject>c__AnonStorey58::<>f__this
	Message_t2619578343 * ___U3CU3Ef__this_2;

public:
	inline static int32_t get_offset_of_iconRectTransform_0() { return static_cast<int32_t>(offsetof(U3CPostInjectU3Ec__AnonStorey58_t980490265, ___iconRectTransform_0)); }
	inline RectTransform_t3317474837 * get_iconRectTransform_0() const { return ___iconRectTransform_0; }
	inline RectTransform_t3317474837 ** get_address_of_iconRectTransform_0() { return &___iconRectTransform_0; }
	inline void set_iconRectTransform_0(RectTransform_t3317474837 * value)
	{
		___iconRectTransform_0 = value;
		Il2CppCodeGenWriteBarrier(&___iconRectTransform_0, value);
	}

	inline static int32_t get_offset_of_textRectTransform_1() { return static_cast<int32_t>(offsetof(U3CPostInjectU3Ec__AnonStorey58_t980490265, ___textRectTransform_1)); }
	inline RectTransform_t3317474837 * get_textRectTransform_1() const { return ___textRectTransform_1; }
	inline RectTransform_t3317474837 ** get_address_of_textRectTransform_1() { return &___textRectTransform_1; }
	inline void set_textRectTransform_1(RectTransform_t3317474837 * value)
	{
		___textRectTransform_1 = value;
		Il2CppCodeGenWriteBarrier(&___textRectTransform_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_2() { return static_cast<int32_t>(offsetof(U3CPostInjectU3Ec__AnonStorey58_t980490265, ___U3CU3Ef__this_2)); }
	inline Message_t2619578343 * get_U3CU3Ef__this_2() const { return ___U3CU3Ef__this_2; }
	inline Message_t2619578343 ** get_address_of_U3CU3Ef__this_2() { return &___U3CU3Ef__this_2; }
	inline void set_U3CU3Ef__this_2(Message_t2619578343 * value)
	{
		___U3CU3Ef__this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
