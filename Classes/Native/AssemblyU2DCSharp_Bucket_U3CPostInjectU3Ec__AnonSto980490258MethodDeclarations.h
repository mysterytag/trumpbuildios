﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Bucket/<PostInject>c__AnonStorey51
struct U3CPostInjectU3Ec__AnonStorey51_t980490258;

#include "codegen/il2cpp-codegen.h"

// System.Void Bucket/<PostInject>c__AnonStorey51::.ctor()
extern "C"  void U3CPostInjectU3Ec__AnonStorey51__ctor_m2160798126 (U3CPostInjectU3Ec__AnonStorey51_t980490258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Bucket/<PostInject>c__AnonStorey51::<>m__3(System.Int64)
extern "C"  void U3CPostInjectU3Ec__AnonStorey51_U3CU3Em__3_m2013472144 (U3CPostInjectU3Ec__AnonStorey51_t980490258 * __this, int64_t ___l0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
