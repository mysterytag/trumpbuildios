﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.Object>
struct Action_1_t985559125;
// CellReactiveApi/<EachNotNull>c__AnonStorey103`1<System.Object>
struct U3CEachNotNullU3Ec__AnonStorey103_1_t2033536640;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CellReactiveApi/<EachNotNull>c__AnonStorey103`1/<EachNotNull>c__AnonStorey104`1<System.Object>
struct  U3CEachNotNullU3Ec__AnonStorey104_1_t1588511641  : public Il2CppObject
{
public:
	// System.Action`1<T> CellReactiveApi/<EachNotNull>c__AnonStorey103`1/<EachNotNull>c__AnonStorey104`1::act
	Action_1_t985559125 * ___act_0;
	// CellReactiveApi/<EachNotNull>c__AnonStorey103`1<T> CellReactiveApi/<EachNotNull>c__AnonStorey103`1/<EachNotNull>c__AnonStorey104`1::<>f__ref$259
	U3CEachNotNullU3Ec__AnonStorey103_1_t2033536640 * ___U3CU3Ef__refU24259_1;

public:
	inline static int32_t get_offset_of_act_0() { return static_cast<int32_t>(offsetof(U3CEachNotNullU3Ec__AnonStorey104_1_t1588511641, ___act_0)); }
	inline Action_1_t985559125 * get_act_0() const { return ___act_0; }
	inline Action_1_t985559125 ** get_address_of_act_0() { return &___act_0; }
	inline void set_act_0(Action_1_t985559125 * value)
	{
		___act_0 = value;
		Il2CppCodeGenWriteBarrier(&___act_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24259_1() { return static_cast<int32_t>(offsetof(U3CEachNotNullU3Ec__AnonStorey104_1_t1588511641, ___U3CU3Ef__refU24259_1)); }
	inline U3CEachNotNullU3Ec__AnonStorey103_1_t2033536640 * get_U3CU3Ef__refU24259_1() const { return ___U3CU3Ef__refU24259_1; }
	inline U3CEachNotNullU3Ec__AnonStorey103_1_t2033536640 ** get_address_of_U3CU3Ef__refU24259_1() { return &___U3CU3Ef__refU24259_1; }
	inline void set_U3CU3Ef__refU24259_1(U3CEachNotNullU3Ec__AnonStorey103_1_t2033536640 * value)
	{
		___U3CU3Ef__refU24259_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24259_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
