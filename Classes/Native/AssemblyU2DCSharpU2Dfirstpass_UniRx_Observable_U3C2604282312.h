﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`1<System.Collections.IEnumerator>
struct Func_1_t1429988286;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/<FromCoroutine>c__AnonStorey4A
struct  U3CFromCoroutineU3Ec__AnonStorey4A_t2604282312  : public Il2CppObject
{
public:
	// System.Func`1<System.Collections.IEnumerator> UniRx.Observable/<FromCoroutine>c__AnonStorey4A::coroutine
	Func_1_t1429988286 * ___coroutine_0;
	// System.Boolean UniRx.Observable/<FromCoroutine>c__AnonStorey4A::publishEveryYield
	bool ___publishEveryYield_1;

public:
	inline static int32_t get_offset_of_coroutine_0() { return static_cast<int32_t>(offsetof(U3CFromCoroutineU3Ec__AnonStorey4A_t2604282312, ___coroutine_0)); }
	inline Func_1_t1429988286 * get_coroutine_0() const { return ___coroutine_0; }
	inline Func_1_t1429988286 ** get_address_of_coroutine_0() { return &___coroutine_0; }
	inline void set_coroutine_0(Func_1_t1429988286 * value)
	{
		___coroutine_0 = value;
		Il2CppCodeGenWriteBarrier(&___coroutine_0, value);
	}

	inline static int32_t get_offset_of_publishEveryYield_1() { return static_cast<int32_t>(offsetof(U3CFromCoroutineU3Ec__AnonStorey4A_t2604282312, ___publishEveryYield_1)); }
	inline bool get_publishEveryYield_1() const { return ___publishEveryYield_1; }
	inline bool* get_address_of_publishEveryYield_1() { return &___publishEveryYield_1; }
	inline void set_publishEveryYield_1(bool value)
	{
		___publishEveryYield_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
