﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ForEachAsyncObservable`1<System.Object>
struct ForEachAsyncObservable_1_t3176723348;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// System.Action`2<System.Object,System.Int32>
struct Action_2_t1820800989;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.ForEachAsyncObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Action`1<T>)
extern "C"  void ForEachAsyncObservable_1__ctor_m2188122063_gshared (ForEachAsyncObservable_1_t3176723348 * __this, Il2CppObject* ___source0, Action_1_t985559125 * ___onNext1, const MethodInfo* method);
#define ForEachAsyncObservable_1__ctor_m2188122063(__this, ___source0, ___onNext1, method) ((  void (*) (ForEachAsyncObservable_1_t3176723348 *, Il2CppObject*, Action_1_t985559125 *, const MethodInfo*))ForEachAsyncObservable_1__ctor_m2188122063_gshared)(__this, ___source0, ___onNext1, method)
// System.Void UniRx.Operators.ForEachAsyncObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Action`2<T,System.Int32>)
extern "C"  void ForEachAsyncObservable_1__ctor_m2329194913_gshared (ForEachAsyncObservable_1_t3176723348 * __this, Il2CppObject* ___source0, Action_2_t1820800989 * ___onNext1, const MethodInfo* method);
#define ForEachAsyncObservable_1__ctor_m2329194913(__this, ___source0, ___onNext1, method) ((  void (*) (ForEachAsyncObservable_1_t3176723348 *, Il2CppObject*, Action_2_t1820800989 *, const MethodInfo*))ForEachAsyncObservable_1__ctor_m2329194913_gshared)(__this, ___source0, ___onNext1, method)
// System.IDisposable UniRx.Operators.ForEachAsyncObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<UniRx.Unit>,System.IDisposable)
extern "C"  Il2CppObject * ForEachAsyncObservable_1_SubscribeCore_m3095022402_gshared (ForEachAsyncObservable_1_t3176723348 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define ForEachAsyncObservable_1_SubscribeCore_m3095022402(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (ForEachAsyncObservable_1_t3176723348 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))ForEachAsyncObservable_1_SubscribeCore_m3095022402_gshared)(__this, ___observer0, ___cancel1, method)
