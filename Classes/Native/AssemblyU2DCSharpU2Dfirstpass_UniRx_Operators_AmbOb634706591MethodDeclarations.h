﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.AmbObservable`1/AmbOuterObserver/Amb<System.Object>
struct Amb_t634706591;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.AmbObservable`1/AmbOuterObserver/Amb<System.Object>::.ctor()
extern "C"  void Amb__ctor_m305749593_gshared (Amb_t634706591 * __this, const MethodInfo* method);
#define Amb__ctor_m305749593(__this, method) ((  void (*) (Amb_t634706591 *, const MethodInfo*))Amb__ctor_m305749593_gshared)(__this, method)
// System.Void UniRx.Operators.AmbObservable`1/AmbOuterObserver/Amb<System.Object>::OnNext(T)
extern "C"  void Amb_OnNext_m303318721_gshared (Amb_t634706591 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Amb_OnNext_m303318721(__this, ___value0, method) ((  void (*) (Amb_t634706591 *, Il2CppObject *, const MethodInfo*))Amb_OnNext_m303318721_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.AmbObservable`1/AmbOuterObserver/Amb<System.Object>::OnError(System.Exception)
extern "C"  void Amb_OnError_m2552020944_gshared (Amb_t634706591 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Amb_OnError_m2552020944(__this, ___error0, method) ((  void (*) (Amb_t634706591 *, Exception_t1967233988 *, const MethodInfo*))Amb_OnError_m2552020944_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.AmbObservable`1/AmbOuterObserver/Amb<System.Object>::OnCompleted()
extern "C"  void Amb_OnCompleted_m573614883_gshared (Amb_t634706591 * __this, const MethodInfo* method);
#define Amb_OnCompleted_m573614883(__this, method) ((  void (*) (Amb_t634706591 *, const MethodInfo*))Amb_OnCompleted_m573614883_gshared)(__this, method)
