﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetCatalogItemsResponseCallback
struct GetCatalogItemsResponseCallback_t3116596035;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetCatalogItemsRequest
struct GetCatalogItemsRequest_t2824754786;
// PlayFab.ClientModels.GetCatalogItemsResult
struct GetCatalogItemsResult_t680458250;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetCatalogI2824754786.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetCatalogIt680458250.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetCatalogItemsResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetCatalogItemsResponseCallback__ctor_m1210835538 (GetCatalogItemsResponseCallback_t3116596035 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetCatalogItemsResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetCatalogItemsRequest,PlayFab.ClientModels.GetCatalogItemsResult,PlayFab.PlayFabError,System.Object)
extern "C"  void GetCatalogItemsResponseCallback_Invoke_m2536939063 (GetCatalogItemsResponseCallback_t3116596035 * __this, String_t* ___urlPath0, int32_t ___callId1, GetCatalogItemsRequest_t2824754786 * ___request2, GetCatalogItemsResult_t680458250 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetCatalogItemsResponseCallback_t3116596035(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetCatalogItemsRequest_t2824754786 * ___request2, GetCatalogItemsResult_t680458250 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetCatalogItemsResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetCatalogItemsRequest,PlayFab.ClientModels.GetCatalogItemsResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetCatalogItemsResponseCallback_BeginInvoke_m3180968444 (GetCatalogItemsResponseCallback_t3116596035 * __this, String_t* ___urlPath0, int32_t ___callId1, GetCatalogItemsRequest_t2824754786 * ___request2, GetCatalogItemsResult_t680458250 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetCatalogItemsResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetCatalogItemsResponseCallback_EndInvoke_m3965799778 (GetCatalogItemsResponseCallback_t3116596035 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
