﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.SwitchObservable`1<System.Object>
struct SwitchObservable_1_t1854652486;
// System.Object
struct Il2CppObject;
// UniRx.SerialDisposable
struct SerialDisposable_t2547852742;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3381670217.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.SwitchObservable`1/SwitchObserver<System.Object>
struct  SwitchObserver_t321759539  : public OperatorObserverBase_2_t3381670217
{
public:
	// UniRx.Operators.SwitchObservable`1<T> UniRx.Operators.SwitchObservable`1/SwitchObserver::parent
	SwitchObservable_1_t1854652486 * ___parent_2;
	// System.Object UniRx.Operators.SwitchObservable`1/SwitchObserver::gate
	Il2CppObject * ___gate_3;
	// UniRx.SerialDisposable UniRx.Operators.SwitchObservable`1/SwitchObserver::innerSubscription
	SerialDisposable_t2547852742 * ___innerSubscription_4;
	// System.Boolean UniRx.Operators.SwitchObservable`1/SwitchObserver::isStopped
	bool ___isStopped_5;
	// System.UInt64 UniRx.Operators.SwitchObservable`1/SwitchObserver::latest
	uint64_t ___latest_6;
	// System.Boolean UniRx.Operators.SwitchObservable`1/SwitchObserver::hasLatest
	bool ___hasLatest_7;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(SwitchObserver_t321759539, ___parent_2)); }
	inline SwitchObservable_1_t1854652486 * get_parent_2() const { return ___parent_2; }
	inline SwitchObservable_1_t1854652486 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(SwitchObservable_1_t1854652486 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_gate_3() { return static_cast<int32_t>(offsetof(SwitchObserver_t321759539, ___gate_3)); }
	inline Il2CppObject * get_gate_3() const { return ___gate_3; }
	inline Il2CppObject ** get_address_of_gate_3() { return &___gate_3; }
	inline void set_gate_3(Il2CppObject * value)
	{
		___gate_3 = value;
		Il2CppCodeGenWriteBarrier(&___gate_3, value);
	}

	inline static int32_t get_offset_of_innerSubscription_4() { return static_cast<int32_t>(offsetof(SwitchObserver_t321759539, ___innerSubscription_4)); }
	inline SerialDisposable_t2547852742 * get_innerSubscription_4() const { return ___innerSubscription_4; }
	inline SerialDisposable_t2547852742 ** get_address_of_innerSubscription_4() { return &___innerSubscription_4; }
	inline void set_innerSubscription_4(SerialDisposable_t2547852742 * value)
	{
		___innerSubscription_4 = value;
		Il2CppCodeGenWriteBarrier(&___innerSubscription_4, value);
	}

	inline static int32_t get_offset_of_isStopped_5() { return static_cast<int32_t>(offsetof(SwitchObserver_t321759539, ___isStopped_5)); }
	inline bool get_isStopped_5() const { return ___isStopped_5; }
	inline bool* get_address_of_isStopped_5() { return &___isStopped_5; }
	inline void set_isStopped_5(bool value)
	{
		___isStopped_5 = value;
	}

	inline static int32_t get_offset_of_latest_6() { return static_cast<int32_t>(offsetof(SwitchObserver_t321759539, ___latest_6)); }
	inline uint64_t get_latest_6() const { return ___latest_6; }
	inline uint64_t* get_address_of_latest_6() { return &___latest_6; }
	inline void set_latest_6(uint64_t value)
	{
		___latest_6 = value;
	}

	inline static int32_t get_offset_of_hasLatest_7() { return static_cast<int32_t>(offsetof(SwitchObserver_t321759539, ___hasLatest_7)); }
	inline bool get_hasLatest_7() const { return ___hasLatest_7; }
	inline bool* get_address_of_hasLatest_7() { return &___hasLatest_7; }
	inline void set_hasLatest_7(bool value)
	{
		___hasLatest_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
