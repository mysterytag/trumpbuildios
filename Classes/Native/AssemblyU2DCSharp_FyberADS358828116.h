﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SponsorPay.SponsorPayPlugin
struct SponsorPayPlugin_t4106591195;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// System.String
struct String_t;
// GlobalStat
struct GlobalStat_t1134678199;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FyberADS
struct  FyberADS_t358828116  : public MonoBehaviour_t3012272455
{
public:
	// SponsorPay.SponsorPayPlugin FyberADS::sponsorPayPlugin
	SponsorPayPlugin_t4106591195 * ___sponsorPayPlugin_2;
	// System.Double FyberADS::DiamCash
	double ___DiamCash_3;
	// UnityEngine.GameObject FyberADS::Hard
	GameObject_t4012695102 * ___Hard_4;
	// System.String FyberADS::appID
	String_t* ___appID_5;
	// System.String FyberADS::userID
	String_t* ___userID_6;
	// System.String FyberADS::SecurityToken
	String_t* ___SecurityToken_7;
	// GlobalStat FyberADS::GlobalStat
	GlobalStat_t1134678199 * ___GlobalStat_8;

public:
	inline static int32_t get_offset_of_sponsorPayPlugin_2() { return static_cast<int32_t>(offsetof(FyberADS_t358828116, ___sponsorPayPlugin_2)); }
	inline SponsorPayPlugin_t4106591195 * get_sponsorPayPlugin_2() const { return ___sponsorPayPlugin_2; }
	inline SponsorPayPlugin_t4106591195 ** get_address_of_sponsorPayPlugin_2() { return &___sponsorPayPlugin_2; }
	inline void set_sponsorPayPlugin_2(SponsorPayPlugin_t4106591195 * value)
	{
		___sponsorPayPlugin_2 = value;
		Il2CppCodeGenWriteBarrier(&___sponsorPayPlugin_2, value);
	}

	inline static int32_t get_offset_of_DiamCash_3() { return static_cast<int32_t>(offsetof(FyberADS_t358828116, ___DiamCash_3)); }
	inline double get_DiamCash_3() const { return ___DiamCash_3; }
	inline double* get_address_of_DiamCash_3() { return &___DiamCash_3; }
	inline void set_DiamCash_3(double value)
	{
		___DiamCash_3 = value;
	}

	inline static int32_t get_offset_of_Hard_4() { return static_cast<int32_t>(offsetof(FyberADS_t358828116, ___Hard_4)); }
	inline GameObject_t4012695102 * get_Hard_4() const { return ___Hard_4; }
	inline GameObject_t4012695102 ** get_address_of_Hard_4() { return &___Hard_4; }
	inline void set_Hard_4(GameObject_t4012695102 * value)
	{
		___Hard_4 = value;
		Il2CppCodeGenWriteBarrier(&___Hard_4, value);
	}

	inline static int32_t get_offset_of_appID_5() { return static_cast<int32_t>(offsetof(FyberADS_t358828116, ___appID_5)); }
	inline String_t* get_appID_5() const { return ___appID_5; }
	inline String_t** get_address_of_appID_5() { return &___appID_5; }
	inline void set_appID_5(String_t* value)
	{
		___appID_5 = value;
		Il2CppCodeGenWriteBarrier(&___appID_5, value);
	}

	inline static int32_t get_offset_of_userID_6() { return static_cast<int32_t>(offsetof(FyberADS_t358828116, ___userID_6)); }
	inline String_t* get_userID_6() const { return ___userID_6; }
	inline String_t** get_address_of_userID_6() { return &___userID_6; }
	inline void set_userID_6(String_t* value)
	{
		___userID_6 = value;
		Il2CppCodeGenWriteBarrier(&___userID_6, value);
	}

	inline static int32_t get_offset_of_SecurityToken_7() { return static_cast<int32_t>(offsetof(FyberADS_t358828116, ___SecurityToken_7)); }
	inline String_t* get_SecurityToken_7() const { return ___SecurityToken_7; }
	inline String_t** get_address_of_SecurityToken_7() { return &___SecurityToken_7; }
	inline void set_SecurityToken_7(String_t* value)
	{
		___SecurityToken_7 = value;
		Il2CppCodeGenWriteBarrier(&___SecurityToken_7, value);
	}

	inline static int32_t get_offset_of_GlobalStat_8() { return static_cast<int32_t>(offsetof(FyberADS_t358828116, ___GlobalStat_8)); }
	inline GlobalStat_t1134678199 * get_GlobalStat_8() const { return ___GlobalStat_8; }
	inline GlobalStat_t1134678199 ** get_address_of_GlobalStat_8() { return &___GlobalStat_8; }
	inline void set_GlobalStat_8(GlobalStat_t1134678199 * value)
	{
		___GlobalStat_8 = value;
		Il2CppCodeGenWriteBarrier(&___GlobalStat_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
