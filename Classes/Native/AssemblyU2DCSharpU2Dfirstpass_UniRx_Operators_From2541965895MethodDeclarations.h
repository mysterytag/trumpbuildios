﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,System.Object>
struct FromEvent_t2541965895;
// UniRx.Operators.FromEventObservable`2<System.Object,System.Object>
struct FromEventObservable_2_t2252139597;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,System.Object>::.ctor(UniRx.Operators.FromEventObservable`2<TDelegate,TEventArgs>,UniRx.IObserver`1<TEventArgs>)
extern "C"  void FromEvent__ctor_m1718772281_gshared (FromEvent_t2541965895 * __this, FromEventObservable_2_t2252139597 * ___parent0, Il2CppObject* ___observer1, const MethodInfo* method);
#define FromEvent__ctor_m1718772281(__this, ___parent0, ___observer1, method) ((  void (*) (FromEvent_t2541965895 *, FromEventObservable_2_t2252139597 *, Il2CppObject*, const MethodInfo*))FromEvent__ctor_m1718772281_gshared)(__this, ___parent0, ___observer1, method)
// System.Boolean UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,System.Object>::Register()
extern "C"  bool FromEvent_Register_m1129938703_gshared (FromEvent_t2541965895 * __this, const MethodInfo* method);
#define FromEvent_Register_m1129938703(__this, method) ((  bool (*) (FromEvent_t2541965895 *, const MethodInfo*))FromEvent_Register_m1129938703_gshared)(__this, method)
// System.Void UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,System.Object>::OnNext(TEventArgs)
extern "C"  void FromEvent_OnNext_m1896537279_gshared (FromEvent_t2541965895 * __this, Il2CppObject * ___args0, const MethodInfo* method);
#define FromEvent_OnNext_m1896537279(__this, ___args0, method) ((  void (*) (FromEvent_t2541965895 *, Il2CppObject *, const MethodInfo*))FromEvent_OnNext_m1896537279_gshared)(__this, ___args0, method)
// System.Void UniRx.Operators.FromEventObservable`2/FromEvent<System.Object,System.Object>::Dispose()
extern "C"  void FromEvent_Dispose_m1014559473_gshared (FromEvent_t2541965895 * __this, const MethodInfo* method);
#define FromEvent_Dispose_m1014559473(__this, method) ((  void (*) (FromEvent_t2541965895 *, const MethodInfo*))FromEvent_Dispose_m1014559473_gshared)(__this, method)
