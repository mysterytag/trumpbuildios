﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.ISchedulerPeriodic
struct ISchedulerPeriodic_t2870089311;
// UniRx.Operators.TimerObservable
struct TimerObservable_t1697791765;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey6E
struct  U3CSubscribeCoreU3Ec__AnonStorey6E_t1995955653  : public Il2CppObject
{
public:
	// UniRx.ISchedulerPeriodic UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey6E::periodicScheduler
	Il2CppObject * ___periodicScheduler_0;
	// UniRx.Operators.TimerObservable UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey6E::<>f__this
	TimerObservable_t1697791765 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_periodicScheduler_0() { return static_cast<int32_t>(offsetof(U3CSubscribeCoreU3Ec__AnonStorey6E_t1995955653, ___periodicScheduler_0)); }
	inline Il2CppObject * get_periodicScheduler_0() const { return ___periodicScheduler_0; }
	inline Il2CppObject ** get_address_of_periodicScheduler_0() { return &___periodicScheduler_0; }
	inline void set_periodicScheduler_0(Il2CppObject * value)
	{
		___periodicScheduler_0 = value;
		Il2CppCodeGenWriteBarrier(&___periodicScheduler_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CSubscribeCoreU3Ec__AnonStorey6E_t1995955653, ___U3CU3Ef__this_1)); }
	inline TimerObservable_t1697791765 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline TimerObservable_t1697791765 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(TimerObservable_t1697791765 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
