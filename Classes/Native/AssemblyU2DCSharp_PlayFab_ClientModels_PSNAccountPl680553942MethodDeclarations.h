﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.PSNAccountPlayFabIdPair
struct PSNAccountPlayFabIdPair_t680553942;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.PSNAccountPlayFabIdPair::.ctor()
extern "C"  void PSNAccountPlayFabIdPair__ctor_m494601363 (PSNAccountPlayFabIdPair_t680553942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.PSNAccountPlayFabIdPair::get_PSNAccountId()
extern "C"  String_t* PSNAccountPlayFabIdPair_get_PSNAccountId_m3183950554 (PSNAccountPlayFabIdPair_t680553942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.PSNAccountPlayFabIdPair::set_PSNAccountId(System.String)
extern "C"  void PSNAccountPlayFabIdPair_set_PSNAccountId_m3433630391 (PSNAccountPlayFabIdPair_t680553942 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.PSNAccountPlayFabIdPair::get_PlayFabId()
extern "C"  String_t* PSNAccountPlayFabIdPair_get_PlayFabId_m2843160371 (PSNAccountPlayFabIdPair_t680553942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.PSNAccountPlayFabIdPair::set_PlayFabId(System.String)
extern "C"  void PSNAccountPlayFabIdPair_set_PlayFabId_m1742028992 (PSNAccountPlayFabIdPair_t680553942 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
