﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetPhotonAuthenticationTokenRequest
struct GetPhotonAuthenticationTokenRequest_t2686429680;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.GetPhotonAuthenticationTokenRequest::.ctor()
extern "C"  void GetPhotonAuthenticationTokenRequest__ctor_m3667973305 (GetPhotonAuthenticationTokenRequest_t2686429680 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GetPhotonAuthenticationTokenRequest::get_PhotonApplicationId()
extern "C"  String_t* GetPhotonAuthenticationTokenRequest_get_PhotonApplicationId_m738460890 (GetPhotonAuthenticationTokenRequest_t2686429680 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetPhotonAuthenticationTokenRequest::set_PhotonApplicationId(System.String)
extern "C"  void GetPhotonAuthenticationTokenRequest_set_PhotonApplicationId_m2534886841 (GetPhotonAuthenticationTokenRequest_t2686429680 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
