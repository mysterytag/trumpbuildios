﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Notification`1/<ToObservable>c__AnonStorey55/<ToObservable>c__AnonStorey56<System.Object>
struct U3CToObservableU3Ec__AnonStorey56_t3651252413;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Notification`1/<ToObservable>c__AnonStorey55/<ToObservable>c__AnonStorey56<System.Object>::.ctor()
extern "C"  void U3CToObservableU3Ec__AnonStorey56__ctor_m2315758680_gshared (U3CToObservableU3Ec__AnonStorey56_t3651252413 * __this, const MethodInfo* method);
#define U3CToObservableU3Ec__AnonStorey56__ctor_m2315758680(__this, method) ((  void (*) (U3CToObservableU3Ec__AnonStorey56_t3651252413 *, const MethodInfo*))U3CToObservableU3Ec__AnonStorey56__ctor_m2315758680_gshared)(__this, method)
// System.Void UniRx.Notification`1/<ToObservable>c__AnonStorey55/<ToObservable>c__AnonStorey56<System.Object>::<>m__6A()
extern "C"  void U3CToObservableU3Ec__AnonStorey56_U3CU3Em__6A_m2959237420_gshared (U3CToObservableU3Ec__AnonStorey56_t3651252413 * __this, const MethodInfo* method);
#define U3CToObservableU3Ec__AnonStorey56_U3CU3Em__6A_m2959237420(__this, method) ((  void (*) (U3CToObservableU3Ec__AnonStorey56_t3651252413 *, const MethodInfo*))U3CToObservableU3Ec__AnonStorey56_U3CU3Em__6A_m2959237420_gshared)(__this, method)
