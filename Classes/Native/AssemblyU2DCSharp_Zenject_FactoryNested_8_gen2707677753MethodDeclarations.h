﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.FactoryNested`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct FactoryNested_8_t2707677753;
// Zenject.IFactory`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct IFactory_7_t2749981566;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void Zenject.FactoryNested`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(Zenject.IFactory`7<TParam1,TParam2,TParam3,TParam4,TParam5,TParam6,TConcrete>)
extern "C"  void FactoryNested_8__ctor_m2013945128_gshared (FactoryNested_8_t2707677753 * __this, Il2CppObject* ___concreteFactory0, const MethodInfo* method);
#define FactoryNested_8__ctor_m2013945128(__this, ___concreteFactory0, method) ((  void (*) (FactoryNested_8_t2707677753 *, Il2CppObject*, const MethodInfo*))FactoryNested_8__ctor_m2013945128_gshared)(__this, ___concreteFactory0, method)
// TContract Zenject.FactoryNested`8<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Create(TParam1,TParam2,TParam3,TParam4,TParam5,TParam6)
extern "C"  Il2CppObject * FactoryNested_8_Create_m2166554265_gshared (FactoryNested_8_t2707677753 * __this, Il2CppObject * ___param10, Il2CppObject * ___param21, Il2CppObject * ___param32, Il2CppObject * ___param43, Il2CppObject * ___param54, Il2CppObject * ___param65, const MethodInfo* method);
#define FactoryNested_8_Create_m2166554265(__this, ___param10, ___param21, ___param32, ___param43, ___param54, ___param65, method) ((  Il2CppObject * (*) (FactoryNested_8_t2707677753 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))FactoryNested_8_Create_m2166554265_gshared)(__this, ___param10, ___param21, ___param32, ___param43, ___param54, ___param65, method)
