﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform
struct GameCenterPlatform_t899915837;
// AchievementController
struct AchievementController_t3677499403;
// AchievementController/AchievementsInfo
struct AchievementsInfo_t1267089938;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;
// GlobalStat
struct GlobalStat_t1134678199;
// GameController
struct GameController_t2782302542;
// System.Action
struct Action_t437523947;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AchievementController
struct  AchievementController_t3677499403  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform AchievementController::center
	GameCenterPlatform_t899915837 * ___center_2;
	// AchievementController/AchievementsInfo AchievementController::info
	AchievementsInfo_t1267089938 * ___info_4;
	// System.Int32 AchievementController::reportedCount
	int32_t ___reportedCount_5;
	// System.Boolean AchievementController::loggedIn
	bool ___loggedIn_6;
	// System.Collections.Generic.List`1<System.String> AchievementController::toReport
	List_1_t1765447871 * ___toReport_7;
	// System.Boolean AchievementController::tried
	bool ___tried_8;
	// System.Int32 AchievementController::tryCount
	int32_t ___tryCount_9;
	// GlobalStat AchievementController::GlobalStat
	GlobalStat_t1134678199 * ___GlobalStat_10;
	// GameController AchievementController::GameController
	GameController_t2782302542 * ___GameController_11;
	// System.Boolean AchievementController::reporting
	bool ___reporting_12;

public:
	inline static int32_t get_offset_of_center_2() { return static_cast<int32_t>(offsetof(AchievementController_t3677499403, ___center_2)); }
	inline GameCenterPlatform_t899915837 * get_center_2() const { return ___center_2; }
	inline GameCenterPlatform_t899915837 ** get_address_of_center_2() { return &___center_2; }
	inline void set_center_2(GameCenterPlatform_t899915837 * value)
	{
		___center_2 = value;
		Il2CppCodeGenWriteBarrier(&___center_2, value);
	}

	inline static int32_t get_offset_of_info_4() { return static_cast<int32_t>(offsetof(AchievementController_t3677499403, ___info_4)); }
	inline AchievementsInfo_t1267089938 * get_info_4() const { return ___info_4; }
	inline AchievementsInfo_t1267089938 ** get_address_of_info_4() { return &___info_4; }
	inline void set_info_4(AchievementsInfo_t1267089938 * value)
	{
		___info_4 = value;
		Il2CppCodeGenWriteBarrier(&___info_4, value);
	}

	inline static int32_t get_offset_of_reportedCount_5() { return static_cast<int32_t>(offsetof(AchievementController_t3677499403, ___reportedCount_5)); }
	inline int32_t get_reportedCount_5() const { return ___reportedCount_5; }
	inline int32_t* get_address_of_reportedCount_5() { return &___reportedCount_5; }
	inline void set_reportedCount_5(int32_t value)
	{
		___reportedCount_5 = value;
	}

	inline static int32_t get_offset_of_loggedIn_6() { return static_cast<int32_t>(offsetof(AchievementController_t3677499403, ___loggedIn_6)); }
	inline bool get_loggedIn_6() const { return ___loggedIn_6; }
	inline bool* get_address_of_loggedIn_6() { return &___loggedIn_6; }
	inline void set_loggedIn_6(bool value)
	{
		___loggedIn_6 = value;
	}

	inline static int32_t get_offset_of_toReport_7() { return static_cast<int32_t>(offsetof(AchievementController_t3677499403, ___toReport_7)); }
	inline List_1_t1765447871 * get_toReport_7() const { return ___toReport_7; }
	inline List_1_t1765447871 ** get_address_of_toReport_7() { return &___toReport_7; }
	inline void set_toReport_7(List_1_t1765447871 * value)
	{
		___toReport_7 = value;
		Il2CppCodeGenWriteBarrier(&___toReport_7, value);
	}

	inline static int32_t get_offset_of_tried_8() { return static_cast<int32_t>(offsetof(AchievementController_t3677499403, ___tried_8)); }
	inline bool get_tried_8() const { return ___tried_8; }
	inline bool* get_address_of_tried_8() { return &___tried_8; }
	inline void set_tried_8(bool value)
	{
		___tried_8 = value;
	}

	inline static int32_t get_offset_of_tryCount_9() { return static_cast<int32_t>(offsetof(AchievementController_t3677499403, ___tryCount_9)); }
	inline int32_t get_tryCount_9() const { return ___tryCount_9; }
	inline int32_t* get_address_of_tryCount_9() { return &___tryCount_9; }
	inline void set_tryCount_9(int32_t value)
	{
		___tryCount_9 = value;
	}

	inline static int32_t get_offset_of_GlobalStat_10() { return static_cast<int32_t>(offsetof(AchievementController_t3677499403, ___GlobalStat_10)); }
	inline GlobalStat_t1134678199 * get_GlobalStat_10() const { return ___GlobalStat_10; }
	inline GlobalStat_t1134678199 ** get_address_of_GlobalStat_10() { return &___GlobalStat_10; }
	inline void set_GlobalStat_10(GlobalStat_t1134678199 * value)
	{
		___GlobalStat_10 = value;
		Il2CppCodeGenWriteBarrier(&___GlobalStat_10, value);
	}

	inline static int32_t get_offset_of_GameController_11() { return static_cast<int32_t>(offsetof(AchievementController_t3677499403, ___GameController_11)); }
	inline GameController_t2782302542 * get_GameController_11() const { return ___GameController_11; }
	inline GameController_t2782302542 ** get_address_of_GameController_11() { return &___GameController_11; }
	inline void set_GameController_11(GameController_t2782302542 * value)
	{
		___GameController_11 = value;
		Il2CppCodeGenWriteBarrier(&___GameController_11, value);
	}

	inline static int32_t get_offset_of_reporting_12() { return static_cast<int32_t>(offsetof(AchievementController_t3677499403, ___reporting_12)); }
	inline bool get_reporting_12() const { return ___reporting_12; }
	inline bool* get_address_of_reporting_12() { return &___reporting_12; }
	inline void set_reporting_12(bool value)
	{
		___reporting_12 = value;
	}
};

struct AchievementController_t3677499403_StaticFields
{
public:
	// AchievementController AchievementController::inst
	AchievementController_t3677499403 * ___inst_3;
	// System.Action AchievementController::<>f__am$cacheB
	Action_t437523947 * ___U3CU3Ef__amU24cacheB_13;

public:
	inline static int32_t get_offset_of_inst_3() { return static_cast<int32_t>(offsetof(AchievementController_t3677499403_StaticFields, ___inst_3)); }
	inline AchievementController_t3677499403 * get_inst_3() const { return ___inst_3; }
	inline AchievementController_t3677499403 ** get_address_of_inst_3() { return &___inst_3; }
	inline void set_inst_3(AchievementController_t3677499403 * value)
	{
		___inst_3 = value;
		Il2CppCodeGenWriteBarrier(&___inst_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_13() { return static_cast<int32_t>(offsetof(AchievementController_t3677499403_StaticFields, ___U3CU3Ef__amU24cacheB_13)); }
	inline Action_t437523947 * get_U3CU3Ef__amU24cacheB_13() const { return ___U3CU3Ef__amU24cacheB_13; }
	inline Action_t437523947 ** get_address_of_U3CU3Ef__amU24cacheB_13() { return &___U3CU3Ef__amU24cacheB_13; }
	inline void set_U3CU3Ef__amU24cacheB_13(Action_t437523947 * value)
	{
		___U3CU3Ef__amU24cacheB_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheB_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
