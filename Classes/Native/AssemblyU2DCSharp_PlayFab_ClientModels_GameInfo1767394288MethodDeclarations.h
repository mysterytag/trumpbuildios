﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GameInfo
struct GameInfo_t1767394288;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen212373688.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen1438485399.h"

// System.Void PlayFab.ClientModels.GameInfo::.ctor()
extern "C"  void GameInfo__ctor_m3710540877 (GameInfo_t1767394288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<PlayFab.ClientModels.Region> PlayFab.ClientModels.GameInfo::get_Region()
extern "C"  Nullable_1_t212373688  GameInfo_get_Region_m56677550 (GameInfo_t1767394288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GameInfo::set_Region(System.Nullable`1<PlayFab.ClientModels.Region>)
extern "C"  void GameInfo_set_Region_m142143473 (GameInfo_t1767394288 * __this, Nullable_1_t212373688  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GameInfo::get_LobbyID()
extern "C"  String_t* GameInfo_get_LobbyID_m476287254 (GameInfo_t1767394288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GameInfo::set_LobbyID(System.String)
extern "C"  void GameInfo_set_LobbyID_m2655991779 (GameInfo_t1767394288 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GameInfo::get_BuildVersion()
extern "C"  String_t* GameInfo_get_BuildVersion_m959845031 (GameInfo_t1767394288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GameInfo::set_BuildVersion(System.String)
extern "C"  void GameInfo_set_BuildVersion_m2716166116 (GameInfo_t1767394288 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GameInfo::get_GameMode()
extern "C"  String_t* GameInfo_get_GameMode_m1532140562 (GameInfo_t1767394288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GameInfo::set_GameMode(System.String)
extern "C"  void GameInfo_set_GameMode_m4217192665 (GameInfo_t1767394288 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GameInfo::get_StatisticName()
extern "C"  String_t* GameInfo_get_StatisticName_m3982185504 (GameInfo_t1767394288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GameInfo::set_StatisticName(System.String)
extern "C"  void GameInfo_set_StatisticName_m1835823065 (GameInfo_t1767394288 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> PlayFab.ClientModels.GameInfo::get_MaxPlayers()
extern "C"  Nullable_1_t1438485399  GameInfo_get_MaxPlayers_m3258606975 (GameInfo_t1767394288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GameInfo::set_MaxPlayers(System.Nullable`1<System.Int32>)
extern "C"  void GameInfo_set_MaxPlayers_m210157900 (GameInfo_t1767394288 * __this, Nullable_1_t1438485399  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.GameInfo::get_PlayerUserIds()
extern "C"  List_1_t1765447871 * GameInfo_get_PlayerUserIds_m4219838719 (GameInfo_t1767394288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GameInfo::set_PlayerUserIds(System.Collections.Generic.List`1<System.String>)
extern "C"  void GameInfo_set_PlayerUserIds_m2512317070 (GameInfo_t1767394288 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 PlayFab.ClientModels.GameInfo::get_RunTime()
extern "C"  uint32_t GameInfo_get_RunTime_m1536837077 (GameInfo_t1767394288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GameInfo::set_RunTime(System.UInt32)
extern "C"  void GameInfo_set_RunTime_m4028010100 (GameInfo_t1767394288 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GameInfo::get_GameServerState()
extern "C"  String_t* GameInfo_get_GameServerState_m3066545825 (GameInfo_t1767394288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GameInfo::set_GameServerState(System.String)
extern "C"  void GameInfo_set_GameServerState_m3501421880 (GameInfo_t1767394288 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
