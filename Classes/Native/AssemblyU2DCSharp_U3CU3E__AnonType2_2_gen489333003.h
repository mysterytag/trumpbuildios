﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <>__AnonType2`2<System.Object,System.Object>
struct  U3CU3E__AnonType2_2_t489333003  : public Il2CppObject
{
public:
	// <hidden>__T <>__AnonType2`2::<hidden>
	Il2CppObject * ___U3ChiddenU3E_0;
	// <showTable>__T <>__AnonType2`2::<showTable>
	Il2CppObject * ___U3CshowTableU3E_1;

public:
	inline static int32_t get_offset_of_U3ChiddenU3E_0() { return static_cast<int32_t>(offsetof(U3CU3E__AnonType2_2_t489333003, ___U3ChiddenU3E_0)); }
	inline Il2CppObject * get_U3ChiddenU3E_0() const { return ___U3ChiddenU3E_0; }
	inline Il2CppObject ** get_address_of_U3ChiddenU3E_0() { return &___U3ChiddenU3E_0; }
	inline void set_U3ChiddenU3E_0(Il2CppObject * value)
	{
		___U3ChiddenU3E_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3ChiddenU3E_0, value);
	}

	inline static int32_t get_offset_of_U3CshowTableU3E_1() { return static_cast<int32_t>(offsetof(U3CU3E__AnonType2_2_t489333003, ___U3CshowTableU3E_1)); }
	inline Il2CppObject * get_U3CshowTableU3E_1() const { return ___U3CshowTableU3E_1; }
	inline Il2CppObject ** get_address_of_U3CshowTableU3E_1() { return &___U3CshowTableU3E_1; }
	inline void set_U3CshowTableU3E_1(Il2CppObject * value)
	{
		___U3CshowTableU3E_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CshowTableU3E_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
