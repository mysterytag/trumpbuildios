﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey97`2<System.Object,System.Object>
struct U3CAsObservableU3Ec__AnonStorey97_2_t564295989;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey97`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CAsObservableU3Ec__AnonStorey97_2__ctor_m4133186201_gshared (U3CAsObservableU3Ec__AnonStorey97_2_t564295989 * __this, const MethodInfo* method);
#define U3CAsObservableU3Ec__AnonStorey97_2__ctor_m4133186201(__this, method) ((  void (*) (U3CAsObservableU3Ec__AnonStorey97_2_t564295989 *, const MethodInfo*))U3CAsObservableU3Ec__AnonStorey97_2__ctor_m4133186201_gshared)(__this, method)
// System.Void UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey97`2<System.Object,System.Object>::<>m__D5(T0,T1)
extern "C"  void U3CAsObservableU3Ec__AnonStorey97_2_U3CU3Em__D5_m3071829298_gshared (U3CAsObservableU3Ec__AnonStorey97_2_t564295989 * __this, Il2CppObject * ___t00, Il2CppObject * ___t11, const MethodInfo* method);
#define U3CAsObservableU3Ec__AnonStorey97_2_U3CU3Em__D5_m3071829298(__this, ___t00, ___t11, method) ((  void (*) (U3CAsObservableU3Ec__AnonStorey97_2_t564295989 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))U3CAsObservableU3Ec__AnonStorey97_2_U3CU3Em__D5_m3071829298_gshared)(__this, ___t00, ___t11, method)
