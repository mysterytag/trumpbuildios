﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnalyticsController/<SendRevenue>c__Iterator1F
struct U3CSendRevenueU3Ec__Iterator1F_t965039904;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void AnalyticsController/<SendRevenue>c__Iterator1F::.ctor()
extern "C"  void U3CSendRevenueU3Ec__Iterator1F__ctor_m3033939342 (U3CSendRevenueU3Ec__Iterator1F_t965039904 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AnalyticsController/<SendRevenue>c__Iterator1F::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSendRevenueU3Ec__Iterator1F_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m934881358 (U3CSendRevenueU3Ec__Iterator1F_t965039904 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AnalyticsController/<SendRevenue>c__Iterator1F::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSendRevenueU3Ec__Iterator1F_System_Collections_IEnumerator_get_Current_m1219594210 (U3CSendRevenueU3Ec__Iterator1F_t965039904 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AnalyticsController/<SendRevenue>c__Iterator1F::MoveNext()
extern "C"  bool U3CSendRevenueU3Ec__Iterator1F_MoveNext_m1559644814 (U3CSendRevenueU3Ec__Iterator1F_t965039904 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController/<SendRevenue>c__Iterator1F::Dispose()
extern "C"  void U3CSendRevenueU3Ec__Iterator1F_Dispose_m3526164043 (U3CSendRevenueU3Ec__Iterator1F_t965039904 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController/<SendRevenue>c__Iterator1F::Reset()
extern "C"  void U3CSendRevenueU3Ec__Iterator1F_Reset_m680372283 (U3CSendRevenueU3Ec__Iterator1F_t965039904 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController/<SendRevenue>c__Iterator1F::<>m__12C(System.Single)
extern "C"  void U3CSendRevenueU3Ec__Iterator1F_U3CU3Em__12C_m3403556126 (U3CSendRevenueU3Ec__Iterator1F_t965039904 * __this, float ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
