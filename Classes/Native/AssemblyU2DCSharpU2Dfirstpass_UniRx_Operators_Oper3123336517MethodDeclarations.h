﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UnityEngine.Vector2>
struct U3CSubscribeU3Ec__AnonStorey5B_t3123336517;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UnityEngine.Vector2>::.ctor()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B__ctor_m1676577376_gshared (U3CSubscribeU3Ec__AnonStorey5B_t3123336517 * __this, const MethodInfo* method);
#define U3CSubscribeU3Ec__AnonStorey5B__ctor_m1676577376(__this, method) ((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t3123336517 *, const MethodInfo*))U3CSubscribeU3Ec__AnonStorey5B__ctor_m1676577376_gshared)(__this, method)
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<UnityEngine.Vector2>::<>m__73()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m2886343941_gshared (U3CSubscribeU3Ec__AnonStorey5B_t3123336517 * __this, const MethodInfo* method);
#define U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m2886343941(__this, method) ((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t3123336517 *, const MethodInfo*))U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m2886343941_gshared)(__this, method)
