﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

// System.Void System.Linq.Enumerable/PredicateOf`1<System.Int32>::.cctor()
extern "C"  void PredicateOf_1__cctor_m686174972_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define PredicateOf_1__cctor_m686174972(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))PredicateOf_1__cctor_m686174972_gshared)(__this /* static, unused */, method)
// System.Boolean System.Linq.Enumerable/PredicateOf`1<System.Int32>::<Always>m__76(T)
extern "C"  bool PredicateOf_1_U3CAlwaysU3Em__76_m1746297546_gshared (Il2CppObject * __this /* static, unused */, int32_t ___t0, const MethodInfo* method);
#define PredicateOf_1_U3CAlwaysU3Em__76_m1746297546(__this /* static, unused */, ___t0, method) ((  bool (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))PredicateOf_1_U3CAlwaysU3Em__76_m1746297546_gshared)(__this /* static, unused */, ___t0, method)
