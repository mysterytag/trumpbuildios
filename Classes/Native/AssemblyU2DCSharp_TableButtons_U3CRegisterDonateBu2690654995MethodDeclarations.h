﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey164
struct U3CRegisterDonateButtonsU3Ec__AnonStorey164_t2690654995;
// <>__AnonType7`2<System.Boolean,System.Boolean>
struct U3CU3E__AnonType7_2_t554355766;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey164::.ctor()
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey164__ctor_m1766529435 (U3CRegisterDonateButtonsU3Ec__AnonStorey164_t2690654995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey164::<>m__266(UniRx.Unit)
extern "C"  bool U3CRegisterDonateButtonsU3Ec__AnonStorey164_U3CU3Em__266_m718534088 (U3CRegisterDonateButtonsU3Ec__AnonStorey164_t2690654995 * __this, Unit_t2558286038  ___unit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey164::<>m__267(UniRx.Unit)
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey164_U3CU3Em__267_m2232631957 (U3CRegisterDonateButtonsU3Ec__AnonStorey164_t2690654995 * __this, Unit_t2558286038  ___unit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey164::<>m__269(<>__AnonType7`2<System.Boolean,System.Boolean>)
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey164_U3CU3Em__269_m3960935782 (U3CRegisterDonateButtonsU3Ec__AnonStorey164_t2690654995 * __this, U3CU3E__AnonType7_2_t554355766 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
