﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"

// System.Void UniRx.Stubs`1<UnityEngine.Color>::.cctor()
extern "C"  void Stubs_1__cctor_m2319247534_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Stubs_1__cctor_m2319247534(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Stubs_1__cctor_m2319247534_gshared)(__this /* static, unused */, method)
// System.Void UniRx.Stubs`1<UnityEngine.Color>::<Ignore>m__71(T)
extern "C"  void Stubs_1_U3CIgnoreU3Em__71_m1612066730_gshared (Il2CppObject * __this /* static, unused */, Color_t1588175760  ___t0, const MethodInfo* method);
#define Stubs_1_U3CIgnoreU3Em__71_m1612066730(__this /* static, unused */, ___t0, method) ((  void (*) (Il2CppObject * /* static, unused */, Color_t1588175760 , const MethodInfo*))Stubs_1_U3CIgnoreU3Em__71_m1612066730_gshared)(__this /* static, unused */, ___t0, method)
// T UniRx.Stubs`1<UnityEngine.Color>::<Identity>m__72(T)
extern "C"  Color_t1588175760  Stubs_1_U3CIdentityU3Em__72_m3211182492_gshared (Il2CppObject * __this /* static, unused */, Color_t1588175760  ___t0, const MethodInfo* method);
#define Stubs_1_U3CIdentityU3Em__72_m3211182492(__this /* static, unused */, ___t0, method) ((  Color_t1588175760  (*) (Il2CppObject * /* static, unused */, Color_t1588175760 , const MethodInfo*))Stubs_1_U3CIdentityU3Em__72_m3211182492_gshared)(__this /* static, unused */, ___t0, method)
