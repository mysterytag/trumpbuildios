﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ObserveExtensions/<PublishUnityObjectValueChanged>c__Iterator25`2<System.Object,System.Object>
struct U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t345210939;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.ObserveExtensions/<PublishUnityObjectValueChanged>c__Iterator25`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2__ctor_m1422127802_gshared (U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t345210939 * __this, const MethodInfo* method);
#define U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2__ctor_m1422127802(__this, method) ((  void (*) (U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t345210939 *, const MethodInfo*))U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2__ctor_m1422127802_gshared)(__this, method)
// System.Object UniRx.ObserveExtensions/<PublishUnityObjectValueChanged>c__Iterator25`2<System.Object,System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m631470562_gshared (U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t345210939 * __this, const MethodInfo* method);
#define U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m631470562(__this, method) ((  Il2CppObject * (*) (U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t345210939 *, const MethodInfo*))U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m631470562_gshared)(__this, method)
// System.Object UniRx.ObserveExtensions/<PublishUnityObjectValueChanged>c__Iterator25`2<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_System_Collections_IEnumerator_get_Current_m3380183414_gshared (U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t345210939 * __this, const MethodInfo* method);
#define U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_System_Collections_IEnumerator_get_Current_m3380183414(__this, method) ((  Il2CppObject * (*) (U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t345210939 *, const MethodInfo*))U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_System_Collections_IEnumerator_get_Current_m3380183414_gshared)(__this, method)
// System.Boolean UniRx.ObserveExtensions/<PublishUnityObjectValueChanged>c__Iterator25`2<System.Object,System.Object>::MoveNext()
extern "C"  bool U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_MoveNext_m1296090570_gshared (U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t345210939 * __this, const MethodInfo* method);
#define U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_MoveNext_m1296090570(__this, method) ((  bool (*) (U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t345210939 *, const MethodInfo*))U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_MoveNext_m1296090570_gshared)(__this, method)
// System.Void UniRx.ObserveExtensions/<PublishUnityObjectValueChanged>c__Iterator25`2<System.Object,System.Object>::Dispose()
extern "C"  void U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_Dispose_m763500663_gshared (U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t345210939 * __this, const MethodInfo* method);
#define U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_Dispose_m763500663(__this, method) ((  void (*) (U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t345210939 *, const MethodInfo*))U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_Dispose_m763500663_gshared)(__this, method)
// System.Void UniRx.ObserveExtensions/<PublishUnityObjectValueChanged>c__Iterator25`2<System.Object,System.Object>::Reset()
extern "C"  void U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_Reset_m3363528039_gshared (U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t345210939 * __this, const MethodInfo* method);
#define U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_Reset_m3363528039(__this, method) ((  void (*) (U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t345210939 *, const MethodInfo*))U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_Reset_m3363528039_gshared)(__this, method)
