﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Func`3<System.Object,System.Object,System.Object>
struct Func_3_t1892209229;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4196218687.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.ScanObservable`1<System.Object>
struct  ScanObservable_1_t4160826919  : public OperatorObservableBase_1_t4196218687
{
public:
	// UniRx.IObservable`1<TSource> UniRx.Operators.ScanObservable`1::source
	Il2CppObject* ___source_1;
	// System.Func`3<TSource,TSource,TSource> UniRx.Operators.ScanObservable`1::accumulator
	Func_3_t1892209229 * ___accumulator_2;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(ScanObservable_1_t4160826919, ___source_1)); }
	inline Il2CppObject* get_source_1() const { return ___source_1; }
	inline Il2CppObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(Il2CppObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier(&___source_1, value);
	}

	inline static int32_t get_offset_of_accumulator_2() { return static_cast<int32_t>(offsetof(ScanObservable_1_t4160826919, ___accumulator_2)); }
	inline Func_3_t1892209229 * get_accumulator_2() const { return ___accumulator_2; }
	inline Func_3_t1892209229 ** get_address_of_accumulator_2() { return &___accumulator_2; }
	inline void set_accumulator_2(Func_3_t1892209229 * value)
	{
		___accumulator_2 = value;
		Il2CppCodeGenWriteBarrier(&___accumulator_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
