﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.TakeObservable`1<UniRx.Unit>
struct TakeObservable_1_t505031811;
// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;
// UniRx.IScheduler
struct IScheduler_t2938318244;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_TimeSpan763862892.h"

// System.Void UniRx.Operators.TakeObservable`1<UniRx.Unit>::.ctor(UniRx.IObservable`1<T>,System.Int32)
extern "C"  void TakeObservable_1__ctor_m622237057_gshared (TakeObservable_1_t505031811 * __this, Il2CppObject* ___source0, int32_t ___count1, const MethodInfo* method);
#define TakeObservable_1__ctor_m622237057(__this, ___source0, ___count1, method) ((  void (*) (TakeObservable_1_t505031811 *, Il2CppObject*, int32_t, const MethodInfo*))TakeObservable_1__ctor_m622237057_gshared)(__this, ___source0, ___count1, method)
// System.Void UniRx.Operators.TakeObservable`1<UniRx.Unit>::.ctor(UniRx.IObservable`1<T>,System.TimeSpan,UniRx.IScheduler)
extern "C"  void TakeObservable_1__ctor_m3571795376_gshared (TakeObservable_1_t505031811 * __this, Il2CppObject* ___source0, TimeSpan_t763862892  ___duration1, Il2CppObject * ___scheduler2, const MethodInfo* method);
#define TakeObservable_1__ctor_m3571795376(__this, ___source0, ___duration1, ___scheduler2, method) ((  void (*) (TakeObservable_1_t505031811 *, Il2CppObject*, TimeSpan_t763862892 , Il2CppObject *, const MethodInfo*))TakeObservable_1__ctor_m3571795376_gshared)(__this, ___source0, ___duration1, ___scheduler2, method)
// UniRx.IObservable`1<T> UniRx.Operators.TakeObservable`1<UniRx.Unit>::Combine(System.Int32)
extern "C"  Il2CppObject* TakeObservable_1_Combine_m2705690337_gshared (TakeObservable_1_t505031811 * __this, int32_t ___count0, const MethodInfo* method);
#define TakeObservable_1_Combine_m2705690337(__this, ___count0, method) ((  Il2CppObject* (*) (TakeObservable_1_t505031811 *, int32_t, const MethodInfo*))TakeObservable_1_Combine_m2705690337_gshared)(__this, ___count0, method)
// UniRx.IObservable`1<T> UniRx.Operators.TakeObservable`1<UniRx.Unit>::Combine(System.TimeSpan)
extern "C"  Il2CppObject* TakeObservable_1_Combine_m2584892940_gshared (TakeObservable_1_t505031811 * __this, TimeSpan_t763862892  ___duration0, const MethodInfo* method);
#define TakeObservable_1_Combine_m2584892940(__this, ___duration0, method) ((  Il2CppObject* (*) (TakeObservable_1_t505031811 *, TimeSpan_t763862892 , const MethodInfo*))TakeObservable_1_Combine_m2584892940_gshared)(__this, ___duration0, method)
// System.IDisposable UniRx.Operators.TakeObservable`1<UniRx.Unit>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * TakeObservable_1_SubscribeCore_m3384233799_gshared (TakeObservable_1_t505031811 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define TakeObservable_1_SubscribeCore_m3384233799(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (TakeObservable_1_t505031811 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))TakeObservable_1_SubscribeCore_m3384233799_gshared)(__this, ___observer0, ___cancel1, method)
