﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SubstanceBase`2/<OnChanged>c__AnonStorey142<System.Object,System.Object>
struct U3COnChangedU3Ec__AnonStorey142_t2463620062;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void SubstanceBase`2/<OnChanged>c__AnonStorey142<System.Object,System.Object>::.ctor()
extern "C"  void U3COnChangedU3Ec__AnonStorey142__ctor_m1824389346_gshared (U3COnChangedU3Ec__AnonStorey142_t2463620062 * __this, const MethodInfo* method);
#define U3COnChangedU3Ec__AnonStorey142__ctor_m1824389346(__this, method) ((  void (*) (U3COnChangedU3Ec__AnonStorey142_t2463620062 *, const MethodInfo*))U3COnChangedU3Ec__AnonStorey142__ctor_m1824389346_gshared)(__this, method)
// System.Void SubstanceBase`2/<OnChanged>c__AnonStorey142<System.Object,System.Object>::<>m__1D8(T)
extern "C"  void U3COnChangedU3Ec__AnonStorey142_U3CU3Em__1D8_m3879802274_gshared (U3COnChangedU3Ec__AnonStorey142_t2463620062 * __this, Il2CppObject * ____0, const MethodInfo* method);
#define U3COnChangedU3Ec__AnonStorey142_U3CU3Em__1D8_m3879802274(__this, ____0, method) ((  void (*) (U3COnChangedU3Ec__AnonStorey142_t2463620062 *, Il2CppObject *, const MethodInfo*))U3COnChangedU3Ec__AnonStorey142_U3CU3Em__1D8_m3879802274_gshared)(__this, ____0, method)
