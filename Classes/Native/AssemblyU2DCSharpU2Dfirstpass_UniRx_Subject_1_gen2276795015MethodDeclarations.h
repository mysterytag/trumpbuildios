﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1<UnityEngine.NetworkDisconnection>
struct Subject_1_t2276795015;
// System.Exception
struct Exception_t1967233988;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UnityEngine.NetworkDisconnection>
struct IObserver_1_t2550759298;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_NetworkDisconnection338760395.h"

// System.Void UniRx.Subject`1<UnityEngine.NetworkDisconnection>::.ctor()
extern "C"  void Subject_1__ctor_m542219653_gshared (Subject_1_t2276795015 * __this, const MethodInfo* method);
#define Subject_1__ctor_m542219653(__this, method) ((  void (*) (Subject_1_t2276795015 *, const MethodInfo*))Subject_1__ctor_m542219653_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UnityEngine.NetworkDisconnection>::get_HasObservers()
extern "C"  bool Subject_1_get_HasObservers_m3377602607_gshared (Subject_1_t2276795015 * __this, const MethodInfo* method);
#define Subject_1_get_HasObservers_m3377602607(__this, method) ((  bool (*) (Subject_1_t2276795015 *, const MethodInfo*))Subject_1_get_HasObservers_m3377602607_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.NetworkDisconnection>::OnCompleted()
extern "C"  void Subject_1_OnCompleted_m4268742991_gshared (Subject_1_t2276795015 * __this, const MethodInfo* method);
#define Subject_1_OnCompleted_m4268742991(__this, method) ((  void (*) (Subject_1_t2276795015 *, const MethodInfo*))Subject_1_OnCompleted_m4268742991_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.NetworkDisconnection>::OnError(System.Exception)
extern "C"  void Subject_1_OnError_m2542537724_gshared (Subject_1_t2276795015 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Subject_1_OnError_m2542537724(__this, ___error0, method) ((  void (*) (Subject_1_t2276795015 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m2542537724_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<UnityEngine.NetworkDisconnection>::OnNext(T)
extern "C"  void Subject_1_OnNext_m4212746989_gshared (Subject_1_t2276795015 * __this, int32_t ___value0, const MethodInfo* method);
#define Subject_1_OnNext_m4212746989(__this, ___value0, method) ((  void (*) (Subject_1_t2276795015 *, int32_t, const MethodInfo*))Subject_1_OnNext_m4212746989_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<UnityEngine.NetworkDisconnection>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * Subject_1_Subscribe_m324045210_gshared (Subject_1_t2276795015 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define Subject_1_Subscribe_m324045210(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t2276795015 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m324045210_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<UnityEngine.NetworkDisconnection>::Dispose()
extern "C"  void Subject_1_Dispose_m1280326786_gshared (Subject_1_t2276795015 * __this, const MethodInfo* method);
#define Subject_1_Dispose_m1280326786(__this, method) ((  void (*) (Subject_1_t2276795015 *, const MethodInfo*))Subject_1_Dispose_m1280326786_gshared)(__this, method)
// System.Void UniRx.Subject`1<UnityEngine.NetworkDisconnection>::ThrowIfDisposed()
extern "C"  void Subject_1_ThrowIfDisposed_m1613906955_gshared (Subject_1_t2276795015 * __this, const MethodInfo* method);
#define Subject_1_ThrowIfDisposed_m1613906955(__this, method) ((  void (*) (Subject_1_t2276795015 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m1613906955_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UnityEngine.NetworkDisconnection>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool Subject_1_IsRequiredSubscribeOnCurrentThread_m3158212454_gshared (Subject_1_t2276795015 * __this, const MethodInfo* method);
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m3158212454(__this, method) ((  bool (*) (Subject_1_t2276795015 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m3158212454_gshared)(__this, method)
