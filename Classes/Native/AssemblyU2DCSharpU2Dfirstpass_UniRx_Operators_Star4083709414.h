﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action
struct Action_t437523947;
// System.Func`1<UniRx.Unit>
struct Func_1_t3701067285;
// UniRx.IScheduler
struct IScheduler_t2938318244;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1622431009.h"
#include "mscorlib_System_Nullable_1_gen3649900800.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.StartObservable`1<UniRx.Unit>
struct  StartObservable_1_t4083709414  : public OperatorObservableBase_1_t1622431009
{
public:
	// System.Action UniRx.Operators.StartObservable`1::action
	Action_t437523947 * ___action_1;
	// System.Func`1<T> UniRx.Operators.StartObservable`1::function
	Func_1_t3701067285 * ___function_2;
	// UniRx.IScheduler UniRx.Operators.StartObservable`1::scheduler
	Il2CppObject * ___scheduler_3;
	// System.Nullable`1<System.TimeSpan> UniRx.Operators.StartObservable`1::startAfter
	Nullable_1_t3649900800  ___startAfter_4;

public:
	inline static int32_t get_offset_of_action_1() { return static_cast<int32_t>(offsetof(StartObservable_1_t4083709414, ___action_1)); }
	inline Action_t437523947 * get_action_1() const { return ___action_1; }
	inline Action_t437523947 ** get_address_of_action_1() { return &___action_1; }
	inline void set_action_1(Action_t437523947 * value)
	{
		___action_1 = value;
		Il2CppCodeGenWriteBarrier(&___action_1, value);
	}

	inline static int32_t get_offset_of_function_2() { return static_cast<int32_t>(offsetof(StartObservable_1_t4083709414, ___function_2)); }
	inline Func_1_t3701067285 * get_function_2() const { return ___function_2; }
	inline Func_1_t3701067285 ** get_address_of_function_2() { return &___function_2; }
	inline void set_function_2(Func_1_t3701067285 * value)
	{
		___function_2 = value;
		Il2CppCodeGenWriteBarrier(&___function_2, value);
	}

	inline static int32_t get_offset_of_scheduler_3() { return static_cast<int32_t>(offsetof(StartObservable_1_t4083709414, ___scheduler_3)); }
	inline Il2CppObject * get_scheduler_3() const { return ___scheduler_3; }
	inline Il2CppObject ** get_address_of_scheduler_3() { return &___scheduler_3; }
	inline void set_scheduler_3(Il2CppObject * value)
	{
		___scheduler_3 = value;
		Il2CppCodeGenWriteBarrier(&___scheduler_3, value);
	}

	inline static int32_t get_offset_of_startAfter_4() { return static_cast<int32_t>(offsetof(StartObservable_1_t4083709414, ___startAfter_4)); }
	inline Nullable_1_t3649900800  get_startAfter_4() const { return ___startAfter_4; }
	inline Nullable_1_t3649900800 * get_address_of_startAfter_4() { return &___startAfter_4; }
	inline void set_startAfter_4(Nullable_1_t3649900800  value)
	{
		___startAfter_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
