﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Bounds>
struct DisposedObserver_1_t2692972440;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_Bounds3518514978.h"

// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Bounds>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m212441886_gshared (DisposedObserver_1_t2692972440 * __this, const MethodInfo* method);
#define DisposedObserver_1__ctor_m212441886(__this, method) ((  void (*) (DisposedObserver_1_t2692972440 *, const MethodInfo*))DisposedObserver_1__ctor_m212441886_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Bounds>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m1808634959_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define DisposedObserver_1__cctor_m1808634959(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))DisposedObserver_1__cctor_m1808634959_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Bounds>::OnCompleted()
extern "C"  void DisposedObserver_1_OnCompleted_m867139624_gshared (DisposedObserver_1_t2692972440 * __this, const MethodInfo* method);
#define DisposedObserver_1_OnCompleted_m867139624(__this, method) ((  void (*) (DisposedObserver_1_t2692972440 *, const MethodInfo*))DisposedObserver_1_OnCompleted_m867139624_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Bounds>::OnError(System.Exception)
extern "C"  void DisposedObserver_1_OnError_m160642389_gshared (DisposedObserver_1_t2692972440 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define DisposedObserver_1_OnError_m160642389(__this, ___error0, method) ((  void (*) (DisposedObserver_1_t2692972440 *, Exception_t1967233988 *, const MethodInfo*))DisposedObserver_1_OnError_m160642389_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Bounds>::OnNext(T)
extern "C"  void DisposedObserver_1_OnNext_m828925510_gshared (DisposedObserver_1_t2692972440 * __this, Bounds_t3518514978  ___value0, const MethodInfo* method);
#define DisposedObserver_1_OnNext_m828925510(__this, ___value0, method) ((  void (*) (DisposedObserver_1_t2692972440 *, Bounds_t3518514978 , const MethodInfo*))DisposedObserver_1_OnNext_m828925510_gshared)(__this, ___value0, method)
