﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.AnimationCurveReactiveProperty
struct AnimationCurveReactiveProperty_t3280306475;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3342907448;
struct AnimationCurve_t3342907448_marshaled_pinvoke;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_AnimationCurve3342907448.h"

// System.Void UniRx.AnimationCurveReactiveProperty::.ctor()
extern "C"  void AnimationCurveReactiveProperty__ctor_m3150323390 (AnimationCurveReactiveProperty_t3280306475 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.AnimationCurveReactiveProperty::.ctor(UnityEngine.AnimationCurve)
extern "C"  void AnimationCurveReactiveProperty__ctor_m1196052988 (AnimationCurveReactiveProperty_t3280306475 * __this, AnimationCurve_t3342907448 * ___initialValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
