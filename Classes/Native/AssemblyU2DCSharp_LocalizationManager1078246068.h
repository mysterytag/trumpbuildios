﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t190145395;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LocalizationManager
struct  LocalizationManager_t1078246068  : public Il2CppObject
{
public:

public:
};

struct LocalizationManager_t1078246068_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.String> LocalizationManager::strings
	Dictionary_2_t2606186806 * ___strings_0;
	// System.String LocalizationManager::language
	String_t* ___language_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> LocalizationManager::<>f__switch$mapD
	Dictionary_2_t190145395 * ___U3CU3Ef__switchU24mapD_2;

public:
	inline static int32_t get_offset_of_strings_0() { return static_cast<int32_t>(offsetof(LocalizationManager_t1078246068_StaticFields, ___strings_0)); }
	inline Dictionary_2_t2606186806 * get_strings_0() const { return ___strings_0; }
	inline Dictionary_2_t2606186806 ** get_address_of_strings_0() { return &___strings_0; }
	inline void set_strings_0(Dictionary_2_t2606186806 * value)
	{
		___strings_0 = value;
		Il2CppCodeGenWriteBarrier(&___strings_0, value);
	}

	inline static int32_t get_offset_of_language_1() { return static_cast<int32_t>(offsetof(LocalizationManager_t1078246068_StaticFields, ___language_1)); }
	inline String_t* get_language_1() const { return ___language_1; }
	inline String_t** get_address_of_language_1() { return &___language_1; }
	inline void set_language_1(String_t* value)
	{
		___language_1 = value;
		Il2CppCodeGenWriteBarrier(&___language_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapD_2() { return static_cast<int32_t>(offsetof(LocalizationManager_t1078246068_StaticFields, ___U3CU3Ef__switchU24mapD_2)); }
	inline Dictionary_2_t190145395 * get_U3CU3Ef__switchU24mapD_2() const { return ___U3CU3Ef__switchU24mapD_2; }
	inline Dictionary_2_t190145395 ** get_address_of_U3CU3Ef__switchU24mapD_2() { return &___U3CU3Ef__switchU24mapD_2; }
	inline void set_U3CU3Ef__switchU24mapD_2(Dictionary_2_t190145395 * value)
	{
		___U3CU3Ef__switchU24mapD_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24mapD_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
