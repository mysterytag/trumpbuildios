﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.DisposedObserver`1<UnityEngine.NetworkConnectionError>
struct DisposedObserver_1_t198263455;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_NetworkConnectionError1023805993.h"

// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.NetworkConnectionError>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m2392258917_gshared (DisposedObserver_1_t198263455 * __this, const MethodInfo* method);
#define DisposedObserver_1__ctor_m2392258917(__this, method) ((  void (*) (DisposedObserver_1_t198263455 *, const MethodInfo*))DisposedObserver_1__ctor_m2392258917_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.NetworkConnectionError>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m663486184_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define DisposedObserver_1__cctor_m663486184(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))DisposedObserver_1__cctor_m663486184_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.NetworkConnectionError>::OnCompleted()
extern "C"  void DisposedObserver_1_OnCompleted_m91802927_gshared (DisposedObserver_1_t198263455 * __this, const MethodInfo* method);
#define DisposedObserver_1_OnCompleted_m91802927(__this, method) ((  void (*) (DisposedObserver_1_t198263455 *, const MethodInfo*))DisposedObserver_1_OnCompleted_m91802927_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.NetworkConnectionError>::OnError(System.Exception)
extern "C"  void DisposedObserver_1_OnError_m1684418524_gshared (DisposedObserver_1_t198263455 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define DisposedObserver_1_OnError_m1684418524(__this, ___error0, method) ((  void (*) (DisposedObserver_1_t198263455 *, Exception_t1967233988 *, const MethodInfo*))DisposedObserver_1_OnError_m1684418524_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.NetworkConnectionError>::OnNext(T)
extern "C"  void DisposedObserver_1_OnNext_m3984019149_gshared (DisposedObserver_1_t198263455 * __this, int32_t ___value0, const MethodInfo* method);
#define DisposedObserver_1_OnNext_m3984019149(__this, ___value0, method) ((  void (*) (DisposedObserver_1_t198263455 *, int32_t, const MethodInfo*))DisposedObserver_1_OnNext_m3984019149_gshared)(__this, ___value0, method)
