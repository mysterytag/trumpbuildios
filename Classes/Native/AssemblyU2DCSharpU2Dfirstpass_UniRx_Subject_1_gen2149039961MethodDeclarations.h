﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1<System.Boolean>
struct Subject_1_t2149039961;
// System.Exception
struct Exception_t1967233988;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Boolean>
struct IObserver_1_t2423004244;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Subject`1<System.Boolean>::.ctor()
extern "C"  void Subject_1__ctor_m886839961_gshared (Subject_1_t2149039961 * __this, const MethodInfo* method);
#define Subject_1__ctor_m886839961(__this, method) ((  void (*) (Subject_1_t2149039961 *, const MethodInfo*))Subject_1__ctor_m886839961_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<System.Boolean>::get_HasObservers()
extern "C"  bool Subject_1_get_HasObservers_m3612237723_gshared (Subject_1_t2149039961 * __this, const MethodInfo* method);
#define Subject_1_get_HasObservers_m3612237723(__this, method) ((  bool (*) (Subject_1_t2149039961 *, const MethodInfo*))Subject_1_get_HasObservers_m3612237723_gshared)(__this, method)
// System.Void UniRx.Subject`1<System.Boolean>::OnCompleted()
extern "C"  void Subject_1_OnCompleted_m2422552419_gshared (Subject_1_t2149039961 * __this, const MethodInfo* method);
#define Subject_1_OnCompleted_m2422552419(__this, method) ((  void (*) (Subject_1_t2149039961 *, const MethodInfo*))Subject_1_OnCompleted_m2422552419_gshared)(__this, method)
// System.Void UniRx.Subject`1<System.Boolean>::OnError(System.Exception)
extern "C"  void Subject_1_OnError_m2694900752_gshared (Subject_1_t2149039961 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Subject_1_OnError_m2694900752(__this, ___error0, method) ((  void (*) (Subject_1_t2149039961 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m2694900752_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<System.Boolean>::OnNext(T)
extern "C"  void Subject_1_OnNext_m385413889_gshared (Subject_1_t2149039961 * __this, bool ___value0, const MethodInfo* method);
#define Subject_1_OnNext_m385413889(__this, ___value0, method) ((  void (*) (Subject_1_t2149039961 *, bool, const MethodInfo*))Subject_1_OnNext_m385413889_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<System.Boolean>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * Subject_1_Subscribe_m3869918062_gshared (Subject_1_t2149039961 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define Subject_1_Subscribe_m3869918062(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t2149039961 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m3869918062_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<System.Boolean>::Dispose()
extern "C"  void Subject_1_Dispose_m1747960982_gshared (Subject_1_t2149039961 * __this, const MethodInfo* method);
#define Subject_1_Dispose_m1747960982(__this, method) ((  void (*) (Subject_1_t2149039961 *, const MethodInfo*))Subject_1_Dispose_m1747960982_gshared)(__this, method)
// System.Void UniRx.Subject`1<System.Boolean>::ThrowIfDisposed()
extern "C"  void Subject_1_ThrowIfDisposed_m492992543_gshared (Subject_1_t2149039961 * __this, const MethodInfo* method);
#define Subject_1_ThrowIfDisposed_m492992543(__this, method) ((  void (*) (Subject_1_t2149039961 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m492992543_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<System.Boolean>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool Subject_1_IsRequiredSubscribeOnCurrentThread_m1965702098_gshared (Subject_1_t2149039961 * __this, const MethodInfo* method);
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m1965702098(__this, method) ((  bool (*) (Subject_1_t2149039961 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m1965702098_gshared)(__this, method)
