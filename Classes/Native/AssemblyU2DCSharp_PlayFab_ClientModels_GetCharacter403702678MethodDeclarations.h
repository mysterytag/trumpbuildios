﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetCharacterInventoryResult
struct GetCharacterInventoryResult_t403702678;
// System.String
struct String_t;
// System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance>
struct List_1_t1322103281;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t190145395;
// System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.VirtualCurrencyRechargeTime>
struct Dictionary_2_t3222006224;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.GetCharacterInventoryResult::.ctor()
extern "C"  void GetCharacterInventoryResult__ctor_m3150343123 (GetCharacterInventoryResult_t403702678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GetCharacterInventoryResult::get_PlayFabId()
extern "C"  String_t* GetCharacterInventoryResult_get_PlayFabId_m3033182195 (GetCharacterInventoryResult_t403702678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetCharacterInventoryResult::set_PlayFabId(System.String)
extern "C"  void GetCharacterInventoryResult_set_PlayFabId_m2783895936 (GetCharacterInventoryResult_t403702678 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GetCharacterInventoryResult::get_CharacterId()
extern "C"  String_t* GetCharacterInventoryResult_get_CharacterId_m523604969 (GetCharacterInventoryResult_t403702678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetCharacterInventoryResult::set_CharacterId(System.String)
extern "C"  void GetCharacterInventoryResult_set_CharacterId_m840891658 (GetCharacterInventoryResult_t403702678 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance> PlayFab.ClientModels.GetCharacterInventoryResult::get_Inventory()
extern "C"  List_1_t1322103281 * GetCharacterInventoryResult_get_Inventory_m2001290653 (GetCharacterInventoryResult_t403702678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetCharacterInventoryResult::set_Inventory(System.Collections.Generic.List`1<PlayFab.ClientModels.ItemInstance>)
extern "C"  void GetCharacterInventoryResult_set_Inventory_m141594458 (GetCharacterInventoryResult_t403702678 * __this, List_1_t1322103281 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32> PlayFab.ClientModels.GetCharacterInventoryResult::get_VirtualCurrency()
extern "C"  Dictionary_2_t190145395 * GetCharacterInventoryResult_get_VirtualCurrency_m2401886175 (GetCharacterInventoryResult_t403702678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetCharacterInventoryResult::set_VirtualCurrency(System.Collections.Generic.Dictionary`2<System.String,System.Int32>)
extern "C"  void GetCharacterInventoryResult_set_VirtualCurrency_m3685507796 (GetCharacterInventoryResult_t403702678 * __this, Dictionary_2_t190145395 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.VirtualCurrencyRechargeTime> PlayFab.ClientModels.GetCharacterInventoryResult::get_VirtualCurrencyRechargeTimes()
extern "C"  Dictionary_2_t3222006224 * GetCharacterInventoryResult_get_VirtualCurrencyRechargeTimes_m933667877 (GetCharacterInventoryResult_t403702678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetCharacterInventoryResult::set_VirtualCurrencyRechargeTimes(System.Collections.Generic.Dictionary`2<System.String,PlayFab.ClientModels.VirtualCurrencyRechargeTime>)
extern "C"  void GetCharacterInventoryResult_set_VirtualCurrencyRechargeTimes_m128372790 (GetCharacterInventoryResult_t403702678 * __this, Dictionary_2_t3222006224 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
