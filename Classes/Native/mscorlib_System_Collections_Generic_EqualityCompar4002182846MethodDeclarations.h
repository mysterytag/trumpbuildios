﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UniRx.Tuple`2<System.Object,System.Object>>
struct DefaultComparer_t4002182846;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen369261819.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UniRx.Tuple`2<System.Object,System.Object>>::.ctor()
extern "C"  void DefaultComparer__ctor_m336310676_gshared (DefaultComparer_t4002182846 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m336310676(__this, method) ((  void (*) (DefaultComparer_t4002182846 *, const MethodInfo*))DefaultComparer__ctor_m336310676_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UniRx.Tuple`2<System.Object,System.Object>>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3041796575_gshared (DefaultComparer_t4002182846 * __this, Tuple_2_t369261819  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m3041796575(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t4002182846 *, Tuple_2_t369261819 , const MethodInfo*))DefaultComparer_GetHashCode_m3041796575_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UniRx.Tuple`2<System.Object,System.Object>>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m805737769_gshared (DefaultComparer_t4002182846 * __this, Tuple_2_t369261819  ___x0, Tuple_2_t369261819  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m805737769(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t4002182846 *, Tuple_2_t369261819 , Tuple_2_t369261819 , const MethodInfo*))DefaultComparer_Equals_m805737769_gshared)(__this, ___x0, ___y1, method)
