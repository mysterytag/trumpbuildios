﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ObservableWWW/<FetchText>c__Iterator21
struct U3CFetchTextU3Ec__Iterator21_t605446781;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.ObservableWWW/<FetchText>c__Iterator21::.ctor()
extern "C"  void U3CFetchTextU3Ec__Iterator21__ctor_m3794028703 (U3CFetchTextU3Ec__Iterator21_t605446781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UniRx.ObservableWWW/<FetchText>c__Iterator21::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CFetchTextU3Ec__Iterator21_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3473370141 (U3CFetchTextU3Ec__Iterator21_t605446781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UniRx.ObservableWWW/<FetchText>c__Iterator21::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CFetchTextU3Ec__Iterator21_System_Collections_IEnumerator_get_Current_m1615284657 (U3CFetchTextU3Ec__Iterator21_t605446781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.ObservableWWW/<FetchText>c__Iterator21::MoveNext()
extern "C"  bool U3CFetchTextU3Ec__Iterator21_MoveNext_m4172672453 (U3CFetchTextU3Ec__Iterator21_t605446781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableWWW/<FetchText>c__Iterator21::Dispose()
extern "C"  void U3CFetchTextU3Ec__Iterator21_Dispose_m3827599644 (U3CFetchTextU3Ec__Iterator21_t605446781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.ObservableWWW/<FetchText>c__Iterator21::Reset()
extern "C"  void U3CFetchTextU3Ec__Iterator21_Reset_m1440461644 (U3CFetchTextU3Ec__Iterator21_t605446781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
