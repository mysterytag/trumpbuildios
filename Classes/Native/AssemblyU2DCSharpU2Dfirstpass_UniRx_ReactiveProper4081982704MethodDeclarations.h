﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper1445439458MethodDeclarations.h"

// System.Void UniRx.ReactiveProperty`1<VKProfileInfo>::.ctor()
#define ReactiveProperty_1__ctor_m2030997450(__this, method) ((  void (*) (ReactiveProperty_1_t4081982704 *, const MethodInfo*))ReactiveProperty_1__ctor_m2182085490_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<VKProfileInfo>::.ctor(T)
#define ReactiveProperty_1__ctor_m1684076936(__this, ___initialValue0, method) ((  void (*) (ReactiveProperty_1_t4081982704 *, VKProfileInfo_t3473649666 *, const MethodInfo*))ReactiveProperty_1__ctor_m3220142124_gshared)(__this, ___initialValue0, method)
// System.Void UniRx.ReactiveProperty`1<VKProfileInfo>::.ctor(UniRx.IObservable`1<T>)
#define ReactiveProperty_1__ctor_m2413498357(__this, ___source0, method) ((  void (*) (ReactiveProperty_1_t4081982704 *, Il2CppObject*, const MethodInfo*))ReactiveProperty_1__ctor_m2559762385_gshared)(__this, ___source0, method)
// System.Void UniRx.ReactiveProperty`1<VKProfileInfo>::.ctor(UniRx.IObservable`1<T>,T)
#define ReactiveProperty_1__ctor_m89586765(__this, ___source0, ___initialValue1, method) ((  void (*) (ReactiveProperty_1_t4081982704 *, Il2CppObject*, VKProfileInfo_t3473649666 *, const MethodInfo*))ReactiveProperty_1__ctor_m3210364201_gshared)(__this, ___source0, ___initialValue1, method)
// System.Void UniRx.ReactiveProperty`1<VKProfileInfo>::.cctor()
#define ReactiveProperty_1__cctor_m1201979351(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ReactiveProperty_1__cctor_m2738044539_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1<VKProfileInfo>::get_EqualityComparer()
#define ReactiveProperty_1_get_EqualityComparer_m707841919(__this, method) ((  Il2CppObject* (*) (ReactiveProperty_1_t4081982704 *, const MethodInfo*))ReactiveProperty_1_get_EqualityComparer_m3290095395_gshared)(__this, method)
// T UniRx.ReactiveProperty`1<VKProfileInfo>::get_Value()
#define ReactiveProperty_1_get_Value_m456664187(__this, method) ((  VKProfileInfo_t3473649666 * (*) (ReactiveProperty_1_t4081982704 *, const MethodInfo*))ReactiveProperty_1_get_Value_m2793108311_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<VKProfileInfo>::set_Value(T)
#define ReactiveProperty_1_set_Value_m2165381590(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t4081982704 *, VKProfileInfo_t3473649666 *, const MethodInfo*))ReactiveProperty_1_set_Value_m1580705402_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1<VKProfileInfo>::SetValue(T)
#define ReactiveProperty_1_SetValue_m1818106145(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t4081982704 *, VKProfileInfo_t3473649666 *, const MethodInfo*))ReactiveProperty_1_SetValue_m4154550269_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1<VKProfileInfo>::SetValueAndForceNotify(T)
#define ReactiveProperty_1_SetValueAndForceNotify_m1783370020(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t4081982704 *, VKProfileInfo_t3473649666 *, const MethodInfo*))ReactiveProperty_1_SetValueAndForceNotify_m2537434880_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.ReactiveProperty`1<VKProfileInfo>::Subscribe(UniRx.IObserver`1<T>)
#define ReactiveProperty_1_Subscribe_m2132194667(__this, ___observer0, method) ((  Il2CppObject * (*) (ReactiveProperty_1_t4081982704 *, Il2CppObject*, const MethodInfo*))ReactiveProperty_1_Subscribe_m958004807_gshared)(__this, ___observer0, method)
// System.Void UniRx.ReactiveProperty`1<VKProfileInfo>::Dispose()
#define ReactiveProperty_1_Dispose_m565017939(__this, method) ((  void (*) (ReactiveProperty_1_t4081982704 *, const MethodInfo*))ReactiveProperty_1_Dispose_m938398511_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<VKProfileInfo>::Dispose(System.Boolean)
#define ReactiveProperty_1_Dispose_m1985537418(__this, ___disposing0, method) ((  void (*) (ReactiveProperty_1_t4081982704 *, bool, const MethodInfo*))ReactiveProperty_1_Dispose_m431016550_gshared)(__this, ___disposing0, method)
// System.String UniRx.ReactiveProperty`1<VKProfileInfo>::ToString()
#define ReactiveProperty_1_ToString_m4032450775(__this, method) ((  String_t* (*) (ReactiveProperty_1_t4081982704 *, const MethodInfo*))ReactiveProperty_1_ToString_m2722346619_gshared)(__this, method)
// System.Boolean UniRx.ReactiveProperty`1<VKProfileInfo>::IsRequiredSubscribeOnCurrentThread()
#define ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m2116800757(__this, method) ((  bool (*) (ReactiveProperty_1_t4081982704 *, const MethodInfo*))ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m3797922201_gshared)(__this, method)
