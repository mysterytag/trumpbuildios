﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EditorVkConnector
struct EditorVkConnector_t1520390475;
// UniRx.ReactiveProperty`1<System.Boolean>
struct ReactiveProperty_1_t819338379;
// UniRx.ReactiveProperty`1<VKProfileInfo>
struct ReactiveProperty_1_t4081982704;
// System.Action`1<System.Boolean>
struct Action_1_t359458046;
// System.Action`1<VKProfileInfo>
struct Action_1_t3622102371;
// System.String
struct String_t;
// AchievementDescription
struct AchievementDescription_t2323334509;
// System.Action`1<System.Collections.Generic.List`1<FriendInfo>>
struct Action_1_t1181882086;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_AchievementDescription2323334509.h"

// System.Void EditorVkConnector::.ctor()
extern "C"  void EditorVkConnector__ctor_m2388893824 (EditorVkConnector_t1520390475 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.ReactiveProperty`1<System.Boolean> EditorVkConnector::get_Connected()
extern "C"  ReactiveProperty_1_t819338379 * EditorVkConnector_get_Connected_m698971947 (EditorVkConnector_t1520390475 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EditorVkConnector::set_Connected(UniRx.ReactiveProperty`1<System.Boolean>)
extern "C"  void EditorVkConnector_set_Connected_m1013399482 (EditorVkConnector_t1520390475 * __this, ReactiveProperty_1_t819338379 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.ReactiveProperty`1<VKProfileInfo> EditorVkConnector::get_VkProfile()
extern "C"  ReactiveProperty_1_t4081982704 * EditorVkConnector_get_VkProfile_m893230427 (EditorVkConnector_t1520390475 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EditorVkConnector::set_VkProfile(UniRx.ReactiveProperty`1<VKProfileInfo>)
extern "C"  void EditorVkConnector_set_VkProfile_m1868439996 (EditorVkConnector_t1520390475 * __this, ReactiveProperty_1_t4081982704 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EditorVkConnector::JoinGroup(System.Action`1<System.Boolean>)
extern "C"  void EditorVkConnector_JoinGroup_m929624576 (EditorVkConnector_t1520390475 * __this, Action_1_t359458046 * ___joinResult0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EditorVkConnector::GetUserProfile(System.Action`1<VKProfileInfo>)
extern "C"  void EditorVkConnector_GetUserProfile_m899247770 (EditorVkConnector_t1520390475 * __this, Action_1_t3622102371 * ___doWithProfile0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EditorVkConnector::Login()
extern "C"  void EditorVkConnector_Login_m3575561927 (EditorVkConnector_t1520390475 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EditorVkConnector::WallPost(System.String,System.String)
extern "C"  void EditorVkConnector_WallPost_m3522463920 (EditorVkConnector_t1520390475 * __this, String_t* ___text0, String_t* ___url1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EditorVkConnector::PostAchievement(AchievementDescription)
extern "C"  void EditorVkConnector_PostAchievement_m2322359616 (EditorVkConnector_t1520390475 * __this, AchievementDescription_t2323334509 * ___achievementDescription0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EditorVkConnector::CheckLoginStatus()
extern "C"  void EditorVkConnector_CheckLoginStatus_m1699585559 (EditorVkConnector_t1520390475 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EditorVkConnector::InviteFriend(System.String,System.Action`1<System.Boolean>)
extern "C"  void EditorVkConnector_InviteFriend_m965521988 (EditorVkConnector_t1520390475 * __this, String_t* ___friendId0, Action_1_t359458046 * ___succesfull1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EditorVkConnector::GetFriends(System.Action`1<System.Collections.Generic.List`1<FriendInfo>>,System.Collections.Generic.List`1<System.String>)
extern "C"  void EditorVkConnector_GetFriends_m3190485015 (EditorVkConnector_t1520390475 * __this, Action_1_t1181882086 * ___withFriends0, List_1_t1765447871 * ___exceptIds1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EditorVkConnector::Logout()
extern "C"  void EditorVkConnector_Logout_m3474060046 (EditorVkConnector_t1520390475 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
