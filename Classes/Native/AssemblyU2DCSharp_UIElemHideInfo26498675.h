﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t284553113;

#include "mscorlib_System_Object837106420.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIElemHideInfo
struct  UIElemHideInfo_t26498675  : public Il2CppObject
{
public:
	// UnityEngine.Vector3 UIElemHideInfo::startPos
	Vector3_t3525329789  ___startPos_0;
	// UnityEngine.Vector3 UIElemHideInfo::hidePos
	Vector3_t3525329789  ___hidePos_1;
	// UnityEngine.Transform UIElemHideInfo::node
	Transform_t284553113 * ___node_2;

public:
	inline static int32_t get_offset_of_startPos_0() { return static_cast<int32_t>(offsetof(UIElemHideInfo_t26498675, ___startPos_0)); }
	inline Vector3_t3525329789  get_startPos_0() const { return ___startPos_0; }
	inline Vector3_t3525329789 * get_address_of_startPos_0() { return &___startPos_0; }
	inline void set_startPos_0(Vector3_t3525329789  value)
	{
		___startPos_0 = value;
	}

	inline static int32_t get_offset_of_hidePos_1() { return static_cast<int32_t>(offsetof(UIElemHideInfo_t26498675, ___hidePos_1)); }
	inline Vector3_t3525329789  get_hidePos_1() const { return ___hidePos_1; }
	inline Vector3_t3525329789 * get_address_of_hidePos_1() { return &___hidePos_1; }
	inline void set_hidePos_1(Vector3_t3525329789  value)
	{
		___hidePos_1 = value;
	}

	inline static int32_t get_offset_of_node_2() { return static_cast<int32_t>(offsetof(UIElemHideInfo_t26498675, ___node_2)); }
	inline Transform_t284553113 * get_node_2() const { return ___node_2; }
	inline Transform_t284553113 ** get_address_of_node_2() { return &___node_2; }
	inline void set_node_2(Transform_t284553113 * value)
	{
		___node_2 = value;
		Il2CppCodeGenWriteBarrier(&___node_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
