﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/ValidateAmazonIAPReceiptRequestCallback
struct ValidateAmazonIAPReceiptRequestCallback_t946679130;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.ValidateAmazonReceiptRequest
struct ValidateAmazonReceiptRequest_t2422909633;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ValidateAma2422909633.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/ValidateAmazonIAPReceiptRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void ValidateAmazonIAPReceiptRequestCallback__ctor_m465618153 (ValidateAmazonIAPReceiptRequestCallback_t946679130 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/ValidateAmazonIAPReceiptRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.ValidateAmazonReceiptRequest,System.Object)
extern "C"  void ValidateAmazonIAPReceiptRequestCallback_Invoke_m569255555 (ValidateAmazonIAPReceiptRequestCallback_t946679130 * __this, String_t* ___urlPath0, int32_t ___callId1, ValidateAmazonReceiptRequest_t2422909633 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_ValidateAmazonIAPReceiptRequestCallback_t946679130(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, ValidateAmazonReceiptRequest_t2422909633 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/ValidateAmazonIAPReceiptRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.ValidateAmazonReceiptRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ValidateAmazonIAPReceiptRequestCallback_BeginInvoke_m3625744126 (ValidateAmazonIAPReceiptRequestCallback_t946679130 * __this, String_t* ___urlPath0, int32_t ___callId1, ValidateAmazonReceiptRequest_t2422909633 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/ValidateAmazonIAPReceiptRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void ValidateAmazonIAPReceiptRequestCallback_EndInvoke_m1644499833 (ValidateAmazonIAPReceiptRequestCallback_t946679130 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
