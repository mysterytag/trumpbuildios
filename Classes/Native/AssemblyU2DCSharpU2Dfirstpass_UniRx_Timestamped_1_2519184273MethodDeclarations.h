﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Timestamped_1_2519184273.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_DateTimeOffset3712260035.h"

// System.Void UniRx.Timestamped`1<System.Object>::.ctor(T,System.DateTimeOffset)
extern "C"  void Timestamped_1__ctor_m4138394428_gshared (Timestamped_1_t2519184273 * __this, Il2CppObject * ___value0, DateTimeOffset_t3712260035  ___timestamp1, const MethodInfo* method);
#define Timestamped_1__ctor_m4138394428(__this, ___value0, ___timestamp1, method) ((  void (*) (Timestamped_1_t2519184273 *, Il2CppObject *, DateTimeOffset_t3712260035 , const MethodInfo*))Timestamped_1__ctor_m4138394428_gshared)(__this, ___value0, ___timestamp1, method)
// T UniRx.Timestamped`1<System.Object>::get_Value()
extern "C"  Il2CppObject * Timestamped_1_get_Value_m2509742440_gshared (Timestamped_1_t2519184273 * __this, const MethodInfo* method);
#define Timestamped_1_get_Value_m2509742440(__this, method) ((  Il2CppObject * (*) (Timestamped_1_t2519184273 *, const MethodInfo*))Timestamped_1_get_Value_m2509742440_gshared)(__this, method)
// System.DateTimeOffset UniRx.Timestamped`1<System.Object>::get_Timestamp()
extern "C"  DateTimeOffset_t3712260035  Timestamped_1_get_Timestamp_m1232229670_gshared (Timestamped_1_t2519184273 * __this, const MethodInfo* method);
#define Timestamped_1_get_Timestamp_m1232229670(__this, method) ((  DateTimeOffset_t3712260035  (*) (Timestamped_1_t2519184273 *, const MethodInfo*))Timestamped_1_get_Timestamp_m1232229670_gshared)(__this, method)
// System.Boolean UniRx.Timestamped`1<System.Object>::Equals(UniRx.Timestamped`1<T>)
extern "C"  bool Timestamped_1_Equals_m2155160614_gshared (Timestamped_1_t2519184273 * __this, Timestamped_1_t2519184273  ___other0, const MethodInfo* method);
#define Timestamped_1_Equals_m2155160614(__this, ___other0, method) ((  bool (*) (Timestamped_1_t2519184273 *, Timestamped_1_t2519184273 , const MethodInfo*))Timestamped_1_Equals_m2155160614_gshared)(__this, ___other0, method)
// System.Boolean UniRx.Timestamped`1<System.Object>::Equals(System.Object)
extern "C"  bool Timestamped_1_Equals_m1273898102_gshared (Timestamped_1_t2519184273 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define Timestamped_1_Equals_m1273898102(__this, ___obj0, method) ((  bool (*) (Timestamped_1_t2519184273 *, Il2CppObject *, const MethodInfo*))Timestamped_1_Equals_m1273898102_gshared)(__this, ___obj0, method)
// System.Int32 UniRx.Timestamped`1<System.Object>::GetHashCode()
extern "C"  int32_t Timestamped_1_GetHashCode_m851540762_gshared (Timestamped_1_t2519184273 * __this, const MethodInfo* method);
#define Timestamped_1_GetHashCode_m851540762(__this, method) ((  int32_t (*) (Timestamped_1_t2519184273 *, const MethodInfo*))Timestamped_1_GetHashCode_m851540762_gshared)(__this, method)
// System.String UniRx.Timestamped`1<System.Object>::ToString()
extern "C"  String_t* Timestamped_1_ToString_m3480459026_gshared (Timestamped_1_t2519184273 * __this, const MethodInfo* method);
#define Timestamped_1_ToString_m3480459026(__this, method) ((  String_t* (*) (Timestamped_1_t2519184273 *, const MethodInfo*))Timestamped_1_ToString_m3480459026_gshared)(__this, method)
// System.Boolean UniRx.Timestamped`1<System.Object>::op_Equality(UniRx.Timestamped`1<T>,UniRx.Timestamped`1<T>)
extern "C"  bool Timestamped_1_op_Equality_m4214055979_gshared (Il2CppObject * __this /* static, unused */, Timestamped_1_t2519184273  ___first0, Timestamped_1_t2519184273  ___second1, const MethodInfo* method);
#define Timestamped_1_op_Equality_m4214055979(__this /* static, unused */, ___first0, ___second1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Timestamped_1_t2519184273 , Timestamped_1_t2519184273 , const MethodInfo*))Timestamped_1_op_Equality_m4214055979_gshared)(__this /* static, unused */, ___first0, ___second1, method)
// System.Boolean UniRx.Timestamped`1<System.Object>::op_Inequality(UniRx.Timestamped`1<T>,UniRx.Timestamped`1<T>)
extern "C"  bool Timestamped_1_op_Inequality_m1008632166_gshared (Il2CppObject * __this /* static, unused */, Timestamped_1_t2519184273  ___first0, Timestamped_1_t2519184273  ___second1, const MethodInfo* method);
#define Timestamped_1_op_Inequality_m1008632166(__this /* static, unused */, ___first0, ___second1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Timestamped_1_t2519184273 , Timestamped_1_t2519184273 , const MethodInfo*))Timestamped_1_op_Inequality_m1008632166_gshared)(__this /* static, unused */, ___first0, ___second1, method)
