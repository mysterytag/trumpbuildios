﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.LoginWithKongregateRequest
struct LoginWithKongregateRequest_t366847061;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen3097043249.h"

// System.Void PlayFab.ClientModels.LoginWithKongregateRequest::.ctor()
extern "C"  void LoginWithKongregateRequest__ctor_m2722549512 (LoginWithKongregateRequest_t366847061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LoginWithKongregateRequest::get_TitleId()
extern "C"  String_t* LoginWithKongregateRequest_get_TitleId_m1480423027 (LoginWithKongregateRequest_t366847061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LoginWithKongregateRequest::set_TitleId(System.String)
extern "C"  void LoginWithKongregateRequest_set_TitleId_m2178426598 (LoginWithKongregateRequest_t366847061 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LoginWithKongregateRequest::get_KongregateId()
extern "C"  String_t* LoginWithKongregateRequest_get_KongregateId_m426331800 (LoginWithKongregateRequest_t366847061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LoginWithKongregateRequest::set_KongregateId(System.String)
extern "C"  void LoginWithKongregateRequest_set_KongregateId_m548908627 (LoginWithKongregateRequest_t366847061 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LoginWithKongregateRequest::get_AuthTicket()
extern "C"  String_t* LoginWithKongregateRequest_get_AuthTicket_m1759390166 (LoginWithKongregateRequest_t366847061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LoginWithKongregateRequest::set_AuthTicket(System.String)
extern "C"  void LoginWithKongregateRequest_set_AuthTicket_m715402773 (LoginWithKongregateRequest_t366847061 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.LoginWithKongregateRequest::get_CreateAccount()
extern "C"  Nullable_1_t3097043249  LoginWithKongregateRequest_get_CreateAccount_m1870508247 (LoginWithKongregateRequest_t366847061 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LoginWithKongregateRequest::set_CreateAccount(System.Nullable`1<System.Boolean>)
extern "C"  void LoginWithKongregateRequest_set_CreateAccount_m2257618766 (LoginWithKongregateRequest_t366847061 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
