﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Func`5<System.Object,System.Int32,System.Int64,System.Int32,System.Object>
struct Func_5_t3557632551;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Func`5<System.Object,System.Int32,System.Int64,System.Int32,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_5__ctor_m449268497_gshared (Func_5_t3557632551 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Func_5__ctor_m449268497(__this, ___object0, ___method1, method) ((  void (*) (Func_5_t3557632551 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_5__ctor_m449268497_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`5<System.Object,System.Int32,System.Int64,System.Int32,System.Object>::Invoke(T1,T2,T3,T4)
extern "C"  Il2CppObject * Func_5_Invoke_m1298299867_gshared (Func_5_t3557632551 * __this, Il2CppObject * ___arg10, int32_t ___arg21, int64_t ___arg32, int32_t ___arg43, const MethodInfo* method);
#define Func_5_Invoke_m1298299867(__this, ___arg10, ___arg21, ___arg32, ___arg43, method) ((  Il2CppObject * (*) (Func_5_t3557632551 *, Il2CppObject *, int32_t, int64_t, int32_t, const MethodInfo*))Func_5_Invoke_m1298299867_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, method)
// System.IAsyncResult System.Func`5<System.Object,System.Int32,System.Int64,System.Int32,System.Object>::BeginInvoke(T1,T2,T3,T4,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_5_BeginInvoke_m4275219650_gshared (Func_5_t3557632551 * __this, Il2CppObject * ___arg10, int32_t ___arg21, int64_t ___arg32, int32_t ___arg43, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method);
#define Func_5_BeginInvoke_m4275219650(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___callback4, ___object5, method) ((  Il2CppObject * (*) (Func_5_t3557632551 *, Il2CppObject *, int32_t, int64_t, int32_t, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_5_BeginInvoke_m4275219650_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___callback4, ___object5, method)
// TResult System.Func`5<System.Object,System.Int32,System.Int64,System.Int32,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Func_5_EndInvoke_m391759935_gshared (Func_5_t3557632551 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Func_5_EndInvoke_m391759935(__this, ___result0, method) ((  Il2CppObject * (*) (Func_5_t3557632551 *, Il2CppObject *, const MethodInfo*))Func_5_EndInvoke_m391759935_gshared)(__this, ___result0, method)
