﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.InjectContext/<>c__Iterator43
struct U3CU3Ec__Iterator43_t705133326;
// Zenject.InjectContext
struct InjectContext_t3456483891;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<Zenject.InjectContext>
struct IEnumerator_1_t644623043;

#include "codegen/il2cpp-codegen.h"

// System.Void Zenject.InjectContext/<>c__Iterator43::.ctor()
extern "C"  void U3CU3Ec__Iterator43__ctor_m395839277 (U3CU3Ec__Iterator43_t705133326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.InjectContext Zenject.InjectContext/<>c__Iterator43::System.Collections.Generic.IEnumerator<Zenject.InjectContext>.get_Current()
extern "C"  InjectContext_t3456483891 * U3CU3Ec__Iterator43_System_Collections_Generic_IEnumeratorU3CZenject_InjectContextU3E_get_Current_m2472960862 (U3CU3Ec__Iterator43_t705133326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Zenject.InjectContext/<>c__Iterator43::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CU3Ec__Iterator43_System_Collections_IEnumerator_get_Current_m74760089 (U3CU3Ec__Iterator43_t705133326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Zenject.InjectContext/<>c__Iterator43::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CU3Ec__Iterator43_System_Collections_IEnumerable_GetEnumerator_m803770772 (U3CU3Ec__Iterator43_t705133326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<Zenject.InjectContext> Zenject.InjectContext/<>c__Iterator43::System.Collections.Generic.IEnumerable<Zenject.InjectContext>.GetEnumerator()
extern "C"  Il2CppObject* U3CU3Ec__Iterator43_System_Collections_Generic_IEnumerableU3CZenject_InjectContextU3E_GetEnumerator_m3672254743 (U3CU3Ec__Iterator43_t705133326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.InjectContext/<>c__Iterator43::MoveNext()
extern "C"  bool U3CU3Ec__Iterator43_MoveNext_m96756391 (U3CU3Ec__Iterator43_t705133326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.InjectContext/<>c__Iterator43::Dispose()
extern "C"  void U3CU3Ec__Iterator43_Dispose_m2342706218 (U3CU3Ec__Iterator43_t705133326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.InjectContext/<>c__Iterator43::Reset()
extern "C"  void U3CU3Ec__Iterator43_Reset_m2337239514 (U3CU3Ec__Iterator43_t705133326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
