﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XPath.FollowingSiblingIterator
struct FollowingSiblingIterator_t1992378268;
// System.Xml.XPath.BaseIterator
struct BaseIterator_t3696600956;
// System.Xml.XPath.XPathNodeIterator
struct XPathNodeIterator_t2394191562;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XPath_BaseIterator3696600956.h"
#include "System_Xml_System_Xml_XPath_FollowingSiblingIterat1992378268.h"

// System.Void System.Xml.XPath.FollowingSiblingIterator::.ctor(System.Xml.XPath.BaseIterator)
extern "C"  void FollowingSiblingIterator__ctor_m3707604177 (FollowingSiblingIterator_t1992378268 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XPath.FollowingSiblingIterator::.ctor(System.Xml.XPath.FollowingSiblingIterator)
extern "C"  void FollowingSiblingIterator__ctor_m1388330545 (FollowingSiblingIterator_t1992378268 * __this, FollowingSiblingIterator_t1992378268 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.FollowingSiblingIterator::Clone()
extern "C"  XPathNodeIterator_t2394191562 * FollowingSiblingIterator_Clone_m83290849 (FollowingSiblingIterator_t1992378268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.FollowingSiblingIterator::MoveNextCore()
extern "C"  bool FollowingSiblingIterator_MoveNextCore_m1382672580 (FollowingSiblingIterator_t1992378268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
