﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UniRx.Unit>
struct List_1_t3355245007;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1441027999.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UniRx.Unit>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2198432111_gshared (Enumerator_t1441027999 * __this, List_1_t3355245007 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m2198432111(__this, ___l0, method) ((  void (*) (Enumerator_t1441027999 *, List_1_t3355245007 *, const MethodInfo*))Enumerator__ctor_m2198432111_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UniRx.Unit>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1435976067_gshared (Enumerator_t1441027999 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1435976067(__this, method) ((  void (*) (Enumerator_t1441027999 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1435976067_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UniRx.Unit>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1783527929_gshared (Enumerator_t1441027999 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1783527929(__this, method) ((  Il2CppObject * (*) (Enumerator_t1441027999 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1783527929_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UniRx.Unit>::Dispose()
extern "C"  void Enumerator_Dispose_m1092092692_gshared (Enumerator_t1441027999 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1092092692(__this, method) ((  void (*) (Enumerator_t1441027999 *, const MethodInfo*))Enumerator_Dispose_m1092092692_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UniRx.Unit>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2309757005_gshared (Enumerator_t1441027999 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m2309757005(__this, method) ((  void (*) (Enumerator_t1441027999 *, const MethodInfo*))Enumerator_VerifyState_m2309757005_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UniRx.Unit>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2043901747_gshared (Enumerator_t1441027999 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2043901747(__this, method) ((  bool (*) (Enumerator_t1441027999 *, const MethodInfo*))Enumerator_MoveNext_m2043901747_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UniRx.Unit>::get_Current()
extern "C"  Unit_t2558286038  Enumerator_get_Current_m4264646822_gshared (Enumerator_t1441027999 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m4264646822(__this, method) ((  Unit_t2558286038  (*) (Enumerator_t1441027999 *, const MethodInfo*))Enumerator_get_Current_m4264646822_gshared)(__this, method)
