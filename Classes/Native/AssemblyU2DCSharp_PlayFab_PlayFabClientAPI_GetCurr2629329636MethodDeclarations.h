﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetCurrentGamesResponseCallback
struct GetCurrentGamesResponseCallback_t2629329636;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.CurrentGamesRequest
struct CurrentGamesRequest_t3387974807;
// PlayFab.ClientModels.CurrentGamesResult
struct CurrentGamesResult_t4162309941;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_CurrentGame3387974807.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_CurrentGame4162309941.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetCurrentGamesResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetCurrentGamesResponseCallback__ctor_m2374323571 (GetCurrentGamesResponseCallback_t2629329636 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetCurrentGamesResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.CurrentGamesRequest,PlayFab.ClientModels.CurrentGamesResult,PlayFab.PlayFabError,System.Object)
extern "C"  void GetCurrentGamesResponseCallback_Invoke_m4021361280 (GetCurrentGamesResponseCallback_t2629329636 * __this, String_t* ___urlPath0, int32_t ___callId1, CurrentGamesRequest_t3387974807 * ___request2, CurrentGamesResult_t4162309941 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetCurrentGamesResponseCallback_t2629329636(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, CurrentGamesRequest_t3387974807 * ___request2, CurrentGamesResult_t4162309941 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetCurrentGamesResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.CurrentGamesRequest,PlayFab.ClientModels.CurrentGamesResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetCurrentGamesResponseCallback_BeginInvoke_m3818712501 (GetCurrentGamesResponseCallback_t2629329636 * __this, String_t* ___urlPath0, int32_t ___callId1, CurrentGamesRequest_t3387974807 * ___request2, CurrentGamesResult_t4162309941 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetCurrentGamesResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetCurrentGamesResponseCallback_EndInvoke_m3467079939 (GetCurrentGamesResponseCallback_t2629329636 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
