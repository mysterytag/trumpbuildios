﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// UniRx.Subject`1<UniRx.CountChangedStatus>
struct Subject_1_t1414295045;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.CountNotifier
struct  CountNotifier_t4027919079  : public Il2CppObject
{
public:
	// System.Object UniRx.CountNotifier::lockObject
	Il2CppObject * ___lockObject_0;
	// UniRx.Subject`1<UniRx.CountChangedStatus> UniRx.CountNotifier::statusChanged
	Subject_1_t1414295045 * ___statusChanged_1;
	// System.Int32 UniRx.CountNotifier::max
	int32_t ___max_2;
	// System.Int32 UniRx.CountNotifier::<Count>k__BackingField
	int32_t ___U3CCountU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_lockObject_0() { return static_cast<int32_t>(offsetof(CountNotifier_t4027919079, ___lockObject_0)); }
	inline Il2CppObject * get_lockObject_0() const { return ___lockObject_0; }
	inline Il2CppObject ** get_address_of_lockObject_0() { return &___lockObject_0; }
	inline void set_lockObject_0(Il2CppObject * value)
	{
		___lockObject_0 = value;
		Il2CppCodeGenWriteBarrier(&___lockObject_0, value);
	}

	inline static int32_t get_offset_of_statusChanged_1() { return static_cast<int32_t>(offsetof(CountNotifier_t4027919079, ___statusChanged_1)); }
	inline Subject_1_t1414295045 * get_statusChanged_1() const { return ___statusChanged_1; }
	inline Subject_1_t1414295045 ** get_address_of_statusChanged_1() { return &___statusChanged_1; }
	inline void set_statusChanged_1(Subject_1_t1414295045 * value)
	{
		___statusChanged_1 = value;
		Il2CppCodeGenWriteBarrier(&___statusChanged_1, value);
	}

	inline static int32_t get_offset_of_max_2() { return static_cast<int32_t>(offsetof(CountNotifier_t4027919079, ___max_2)); }
	inline int32_t get_max_2() const { return ___max_2; }
	inline int32_t* get_address_of_max_2() { return &___max_2; }
	inline void set_max_2(int32_t value)
	{
		___max_2 = value;
	}

	inline static int32_t get_offset_of_U3CCountU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CountNotifier_t4027919079, ___U3CCountU3Ek__BackingField_3)); }
	inline int32_t get_U3CCountU3Ek__BackingField_3() const { return ___U3CCountU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CCountU3Ek__BackingField_3() { return &___U3CCountU3Ek__BackingField_3; }
	inline void set_U3CCountU3Ek__BackingField_3(int32_t value)
	{
		___U3CCountU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
