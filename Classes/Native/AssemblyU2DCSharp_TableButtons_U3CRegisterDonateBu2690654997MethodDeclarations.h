﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey166
struct U3CRegisterDonateButtonsU3Ec__AnonStorey166_t2690654997;
// <>__AnonType7`2<System.Boolean,System.Boolean>
struct U3CU3E__AnonType7_2_t554355766;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey166::.ctor()
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey166__ctor_m1373502425 (U3CRegisterDonateButtonsU3Ec__AnonStorey166_t2690654997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey166::<>m__26C(UniRx.Unit)
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey166_U3CU3Em__26C_m1201324835 (U3CRegisterDonateButtonsU3Ec__AnonStorey166_t2690654997 * __this, Unit_t2558286038  ___unit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey166::<>m__26E(<>__AnonType7`2<System.Boolean,System.Boolean>)
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey166_U3CU3Em__26E_m133555956 (U3CRegisterDonateButtonsU3Ec__AnonStorey166_t2690654997 * __this, U3CU3E__AnonType7_2_t554355766 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
