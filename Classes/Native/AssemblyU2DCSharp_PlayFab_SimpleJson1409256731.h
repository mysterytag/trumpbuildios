﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Char[]
struct CharU5BU5D_t3416858730;
// System.Collections.Generic.List`1<System.Type>
struct List_1_t3576188904;
// System.Text.StringBuilder
struct StringBuilder_t3822575854;
// PlayFab.IJsonSerializerStrategy
struct IJsonSerializerStrategy_t3082203415;
// PlayFab.PocoJsonSerializerStrategy
struct PocoJsonSerializerStrategy_t344526361;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.SimpleJson
struct  SimpleJson_t1409256731  : public Il2CppObject
{
public:

public:
};

struct SimpleJson_t1409256731_StaticFields
{
public:
	// System.Char[] PlayFab.SimpleJson::EscapeTable
	CharU5BU5D_t3416858730* ___EscapeTable_1;
	// System.Char[] PlayFab.SimpleJson::EscapeCharacters
	CharU5BU5D_t3416858730* ___EscapeCharacters_2;
	// System.Collections.Generic.List`1<System.Type> PlayFab.SimpleJson::NumberTypes
	List_1_t3576188904 * ___NumberTypes_3;
	// PlayFab.IJsonSerializerStrategy PlayFab.SimpleJson::_currentJsonSerializerStrategy
	Il2CppObject * ____currentJsonSerializerStrategy_6;
	// PlayFab.PocoJsonSerializerStrategy PlayFab.SimpleJson::_pocoJsonSerializerStrategy
	PocoJsonSerializerStrategy_t344526361 * ____pocoJsonSerializerStrategy_7;

public:
	inline static int32_t get_offset_of_EscapeTable_1() { return static_cast<int32_t>(offsetof(SimpleJson_t1409256731_StaticFields, ___EscapeTable_1)); }
	inline CharU5BU5D_t3416858730* get_EscapeTable_1() const { return ___EscapeTable_1; }
	inline CharU5BU5D_t3416858730** get_address_of_EscapeTable_1() { return &___EscapeTable_1; }
	inline void set_EscapeTable_1(CharU5BU5D_t3416858730* value)
	{
		___EscapeTable_1 = value;
		Il2CppCodeGenWriteBarrier(&___EscapeTable_1, value);
	}

	inline static int32_t get_offset_of_EscapeCharacters_2() { return static_cast<int32_t>(offsetof(SimpleJson_t1409256731_StaticFields, ___EscapeCharacters_2)); }
	inline CharU5BU5D_t3416858730* get_EscapeCharacters_2() const { return ___EscapeCharacters_2; }
	inline CharU5BU5D_t3416858730** get_address_of_EscapeCharacters_2() { return &___EscapeCharacters_2; }
	inline void set_EscapeCharacters_2(CharU5BU5D_t3416858730* value)
	{
		___EscapeCharacters_2 = value;
		Il2CppCodeGenWriteBarrier(&___EscapeCharacters_2, value);
	}

	inline static int32_t get_offset_of_NumberTypes_3() { return static_cast<int32_t>(offsetof(SimpleJson_t1409256731_StaticFields, ___NumberTypes_3)); }
	inline List_1_t3576188904 * get_NumberTypes_3() const { return ___NumberTypes_3; }
	inline List_1_t3576188904 ** get_address_of_NumberTypes_3() { return &___NumberTypes_3; }
	inline void set_NumberTypes_3(List_1_t3576188904 * value)
	{
		___NumberTypes_3 = value;
		Il2CppCodeGenWriteBarrier(&___NumberTypes_3, value);
	}

	inline static int32_t get_offset_of__currentJsonSerializerStrategy_6() { return static_cast<int32_t>(offsetof(SimpleJson_t1409256731_StaticFields, ____currentJsonSerializerStrategy_6)); }
	inline Il2CppObject * get__currentJsonSerializerStrategy_6() const { return ____currentJsonSerializerStrategy_6; }
	inline Il2CppObject ** get_address_of__currentJsonSerializerStrategy_6() { return &____currentJsonSerializerStrategy_6; }
	inline void set__currentJsonSerializerStrategy_6(Il2CppObject * value)
	{
		____currentJsonSerializerStrategy_6 = value;
		Il2CppCodeGenWriteBarrier(&____currentJsonSerializerStrategy_6, value);
	}

	inline static int32_t get_offset_of__pocoJsonSerializerStrategy_7() { return static_cast<int32_t>(offsetof(SimpleJson_t1409256731_StaticFields, ____pocoJsonSerializerStrategy_7)); }
	inline PocoJsonSerializerStrategy_t344526361 * get__pocoJsonSerializerStrategy_7() const { return ____pocoJsonSerializerStrategy_7; }
	inline PocoJsonSerializerStrategy_t344526361 ** get_address_of__pocoJsonSerializerStrategy_7() { return &____pocoJsonSerializerStrategy_7; }
	inline void set__pocoJsonSerializerStrategy_7(PocoJsonSerializerStrategy_t344526361 * value)
	{
		____pocoJsonSerializerStrategy_7 = value;
		Il2CppCodeGenWriteBarrier(&____pocoJsonSerializerStrategy_7, value);
	}
};

struct SimpleJson_t1409256731_ThreadStaticFields
{
public:
	// System.Text.StringBuilder PlayFab.SimpleJson::_serializeObjectBuilder
	StringBuilder_t3822575854 * ____serializeObjectBuilder_4;
	// System.Text.StringBuilder PlayFab.SimpleJson::_parseStringBuilder
	StringBuilder_t3822575854 * ____parseStringBuilder_5;

public:
	inline static int32_t get_offset_of__serializeObjectBuilder_4() { return static_cast<int32_t>(offsetof(SimpleJson_t1409256731_ThreadStaticFields, ____serializeObjectBuilder_4)); }
	inline StringBuilder_t3822575854 * get__serializeObjectBuilder_4() const { return ____serializeObjectBuilder_4; }
	inline StringBuilder_t3822575854 ** get_address_of__serializeObjectBuilder_4() { return &____serializeObjectBuilder_4; }
	inline void set__serializeObjectBuilder_4(StringBuilder_t3822575854 * value)
	{
		____serializeObjectBuilder_4 = value;
		Il2CppCodeGenWriteBarrier(&____serializeObjectBuilder_4, value);
	}

	inline static int32_t get_offset_of__parseStringBuilder_5() { return static_cast<int32_t>(offsetof(SimpleJson_t1409256731_ThreadStaticFields, ____parseStringBuilder_5)); }
	inline StringBuilder_t3822575854 * get__parseStringBuilder_5() const { return ____parseStringBuilder_5; }
	inline StringBuilder_t3822575854 ** get_address_of__parseStringBuilder_5() { return &____parseStringBuilder_5; }
	inline void set__parseStringBuilder_5(StringBuilder_t3822575854 * value)
	{
		____parseStringBuilder_5 = value;
		Il2CppCodeGenWriteBarrier(&____parseStringBuilder_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
