﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.RepeatObservable`1/Repeat<System.Object>
struct Repeat_t85959292;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.RepeatObservable`1/Repeat<System.Object>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Repeat__ctor_m888409843_gshared (Repeat_t85959292 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define Repeat__ctor_m888409843(__this, ___observer0, ___cancel1, method) ((  void (*) (Repeat_t85959292 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Repeat__ctor_m888409843_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.RepeatObservable`1/Repeat<System.Object>::OnNext(T)
extern "C"  void Repeat_OnNext_m3726854335_gshared (Repeat_t85959292 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Repeat_OnNext_m3726854335(__this, ___value0, method) ((  void (*) (Repeat_t85959292 *, Il2CppObject *, const MethodInfo*))Repeat_OnNext_m3726854335_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.RepeatObservable`1/Repeat<System.Object>::OnError(System.Exception)
extern "C"  void Repeat_OnError_m412353998_gshared (Repeat_t85959292 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Repeat_OnError_m412353998(__this, ___error0, method) ((  void (*) (Repeat_t85959292 *, Exception_t1967233988 *, const MethodInfo*))Repeat_OnError_m412353998_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.RepeatObservable`1/Repeat<System.Object>::OnCompleted()
extern "C"  void Repeat_OnCompleted_m1792179745_gshared (Repeat_t85959292 * __this, const MethodInfo* method);
#define Repeat_OnCompleted_m1792179745(__this, method) ((  void (*) (Repeat_t85959292 *, const MethodInfo*))Repeat_OnCompleted_m1792179745_gshared)(__this, method)
