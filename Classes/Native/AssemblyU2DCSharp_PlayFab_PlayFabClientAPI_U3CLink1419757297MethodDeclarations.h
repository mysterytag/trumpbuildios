﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<LinkAndroidDeviceID>c__AnonStorey75
struct U3CLinkAndroidDeviceIDU3Ec__AnonStorey75_t1419757297;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<LinkAndroidDeviceID>c__AnonStorey75::.ctor()
extern "C"  void U3CLinkAndroidDeviceIDU3Ec__AnonStorey75__ctor_m4034670466 (U3CLinkAndroidDeviceIDU3Ec__AnonStorey75_t1419757297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<LinkAndroidDeviceID>c__AnonStorey75::<>m__62(PlayFab.CallRequestContainer)
extern "C"  void U3CLinkAndroidDeviceIDU3Ec__AnonStorey75_U3CU3Em__62_m794117276 (U3CLinkAndroidDeviceIDU3Ec__AnonStorey75_t1419757297 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
