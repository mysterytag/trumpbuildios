﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.DiContainerProvider
struct DiContainerProvider_t1124384482;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// Zenject.InjectContext
struct InjectContext_t3456483891;
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException>
struct IEnumerable_1_t4073207355;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449.h"
#include "AssemblyU2DCSharp_Zenject_InjectContext3456483891.h"

// System.Void Zenject.DiContainerProvider::.ctor(Zenject.DiContainer)
extern "C"  void DiContainerProvider__ctor_m3192889688 (DiContainerProvider_t1124384482 * __this, DiContainer_t2383114449 * ___container0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Zenject.DiContainerProvider::GetInstanceType()
extern "C"  Type_t * DiContainerProvider_GetInstanceType_m1140617498 (DiContainerProvider_t1124384482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Zenject.DiContainerProvider::GetInstance(Zenject.InjectContext)
extern "C"  Il2CppObject * DiContainerProvider_GetInstance_m4001426648 (DiContainerProvider_t1124384482 * __this, InjectContext_t3456483891 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Zenject.ZenjectResolveException> Zenject.DiContainerProvider::ValidateBinding(Zenject.InjectContext)
extern "C"  Il2CppObject* DiContainerProvider_ValidateBinding_m282367082 (DiContainerProvider_t1124384482 * __this, InjectContext_t3456483891 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
