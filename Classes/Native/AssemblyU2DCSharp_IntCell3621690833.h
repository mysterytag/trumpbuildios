﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Stream`1<System.Int32>
struct Stream_1_t1538553809;

#include "AssemblyU2DCSharp_Cell_1_gen3029512419.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IntCell
struct  IntCell_t3621690833  : public Cell_1_t3029512419
{
public:
	// Stream`1<System.Int32> IntCell::diff_
	Stream_1_t1538553809 * ___diff__5;

public:
	inline static int32_t get_offset_of_diff__5() { return static_cast<int32_t>(offsetof(IntCell_t3621690833, ___diff__5)); }
	inline Stream_1_t1538553809 * get_diff__5() const { return ___diff__5; }
	inline Stream_1_t1538553809 ** get_address_of_diff__5() { return &___diff__5; }
	inline void set_diff__5(Stream_1_t1538553809 * value)
	{
		___diff__5 = value;
		Il2CppCodeGenWriteBarrier(&___diff__5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
