﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetFriendsListResponseCallback
struct GetFriendsListResponseCallback_t3629031555;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetFriendsListRequest
struct GetFriendsListRequest_t3925362978;
// PlayFab.ClientModels.GetFriendsListResult
struct GetFriendsListResult_t2794171722;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetFriendsL3925362978.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetFriendsL2794171722.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetFriendsListResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetFriendsListResponseCallback__ctor_m2539218930 (GetFriendsListResponseCallback_t3629031555 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetFriendsListResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetFriendsListRequest,PlayFab.ClientModels.GetFriendsListResult,PlayFab.PlayFabError,System.Object)
extern "C"  void GetFriendsListResponseCallback_Invoke_m1710385951 (GetFriendsListResponseCallback_t3629031555 * __this, String_t* ___urlPath0, int32_t ___callId1, GetFriendsListRequest_t3925362978 * ___request2, GetFriendsListResult_t2794171722 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetFriendsListResponseCallback_t3629031555(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetFriendsListRequest_t3925362978 * ___request2, GetFriendsListResult_t2794171722 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetFriendsListResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetFriendsListRequest,PlayFab.ClientModels.GetFriendsListResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetFriendsListResponseCallback_BeginInvoke_m2969154572 (GetFriendsListResponseCallback_t3629031555 * __this, String_t* ___urlPath0, int32_t ___callId1, GetFriendsListRequest_t3925362978 * ___request2, GetFriendsListResult_t2794171722 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetFriendsListResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetFriendsListResponseCallback_EndInvoke_m1970447106 (GetFriendsListResponseCallback_t3629031555 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
