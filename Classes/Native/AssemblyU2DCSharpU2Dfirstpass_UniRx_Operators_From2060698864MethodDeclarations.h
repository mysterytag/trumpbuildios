﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.FromEventObservable/FromEvent
struct FromEvent_t2060698864;
// UniRx.Operators.FromEventObservable
struct FromEventObservable_t1591372576;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_From1591372576.h"

// System.Void UniRx.Operators.FromEventObservable/FromEvent::.ctor(UniRx.Operators.FromEventObservable,UniRx.IObserver`1<UniRx.Unit>)
extern "C"  void FromEvent__ctor_m2262758506 (FromEvent_t2060698864 * __this, FromEventObservable_t1591372576 * ___parent0, Il2CppObject* ___observer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.Operators.FromEventObservable/FromEvent::Register()
extern "C"  bool FromEvent_Register_m31338865 (FromEvent_t2060698864 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Operators.FromEventObservable/FromEvent::OnNext()
extern "C"  void FromEvent_OnNext_m3700796636 (FromEvent_t2060698864 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Operators.FromEventObservable/FromEvent::Dispose()
extern "C"  void FromEvent_Dispose_m123127191 (FromEvent_t2060698864 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
