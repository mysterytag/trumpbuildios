﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Zenject.InjectContext
struct InjectContext_t3456483891;
// System.Collections.Generic.IEnumerator`1<Zenject.ZenjectResolveException>
struct IEnumerator_1_t2684159447;
// Zenject.ZenjectResolveException
struct ZenjectResolveException_t1201052999;
// Zenject.PrefabSingletonProvider
struct PrefabSingletonProvider_t2854807885;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PrefabSingletonProvider/<ValidateBinding>c__Iterator4E
struct  U3CValidateBindingU3Ec__Iterator4E_t3066593111  : public Il2CppObject
{
public:
	// Zenject.InjectContext Zenject.PrefabSingletonProvider/<ValidateBinding>c__Iterator4E::context
	InjectContext_t3456483891 * ___context_0;
	// System.Collections.Generic.IEnumerator`1<Zenject.ZenjectResolveException> Zenject.PrefabSingletonProvider/<ValidateBinding>c__Iterator4E::<$s_322>__0
	Il2CppObject* ___U3CU24s_322U3E__0_1;
	// Zenject.ZenjectResolveException Zenject.PrefabSingletonProvider/<ValidateBinding>c__Iterator4E::<err>__1
	ZenjectResolveException_t1201052999 * ___U3CerrU3E__1_2;
	// System.Int32 Zenject.PrefabSingletonProvider/<ValidateBinding>c__Iterator4E::$PC
	int32_t ___U24PC_3;
	// Zenject.ZenjectResolveException Zenject.PrefabSingletonProvider/<ValidateBinding>c__Iterator4E::$current
	ZenjectResolveException_t1201052999 * ___U24current_4;
	// Zenject.InjectContext Zenject.PrefabSingletonProvider/<ValidateBinding>c__Iterator4E::<$>context
	InjectContext_t3456483891 * ___U3CU24U3Econtext_5;
	// Zenject.PrefabSingletonProvider Zenject.PrefabSingletonProvider/<ValidateBinding>c__Iterator4E::<>f__this
	PrefabSingletonProvider_t2854807885 * ___U3CU3Ef__this_6;

public:
	inline static int32_t get_offset_of_context_0() { return static_cast<int32_t>(offsetof(U3CValidateBindingU3Ec__Iterator4E_t3066593111, ___context_0)); }
	inline InjectContext_t3456483891 * get_context_0() const { return ___context_0; }
	inline InjectContext_t3456483891 ** get_address_of_context_0() { return &___context_0; }
	inline void set_context_0(InjectContext_t3456483891 * value)
	{
		___context_0 = value;
		Il2CppCodeGenWriteBarrier(&___context_0, value);
	}

	inline static int32_t get_offset_of_U3CU24s_322U3E__0_1() { return static_cast<int32_t>(offsetof(U3CValidateBindingU3Ec__Iterator4E_t3066593111, ___U3CU24s_322U3E__0_1)); }
	inline Il2CppObject* get_U3CU24s_322U3E__0_1() const { return ___U3CU24s_322U3E__0_1; }
	inline Il2CppObject** get_address_of_U3CU24s_322U3E__0_1() { return &___U3CU24s_322U3E__0_1; }
	inline void set_U3CU24s_322U3E__0_1(Il2CppObject* value)
	{
		___U3CU24s_322U3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_322U3E__0_1, value);
	}

	inline static int32_t get_offset_of_U3CerrU3E__1_2() { return static_cast<int32_t>(offsetof(U3CValidateBindingU3Ec__Iterator4E_t3066593111, ___U3CerrU3E__1_2)); }
	inline ZenjectResolveException_t1201052999 * get_U3CerrU3E__1_2() const { return ___U3CerrU3E__1_2; }
	inline ZenjectResolveException_t1201052999 ** get_address_of_U3CerrU3E__1_2() { return &___U3CerrU3E__1_2; }
	inline void set_U3CerrU3E__1_2(ZenjectResolveException_t1201052999 * value)
	{
		___U3CerrU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CerrU3E__1_2, value);
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CValidateBindingU3Ec__Iterator4E_t3066593111, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CValidateBindingU3Ec__Iterator4E_t3066593111, ___U24current_4)); }
	inline ZenjectResolveException_t1201052999 * get_U24current_4() const { return ___U24current_4; }
	inline ZenjectResolveException_t1201052999 ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(ZenjectResolveException_t1201052999 * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Econtext_5() { return static_cast<int32_t>(offsetof(U3CValidateBindingU3Ec__Iterator4E_t3066593111, ___U3CU24U3Econtext_5)); }
	inline InjectContext_t3456483891 * get_U3CU24U3Econtext_5() const { return ___U3CU24U3Econtext_5; }
	inline InjectContext_t3456483891 ** get_address_of_U3CU24U3Econtext_5() { return &___U3CU24U3Econtext_5; }
	inline void set_U3CU24U3Econtext_5(InjectContext_t3456483891 * value)
	{
		___U3CU24U3Econtext_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Econtext_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_6() { return static_cast<int32_t>(offsetof(U3CValidateBindingU3Ec__Iterator4E_t3066593111, ___U3CU3Ef__this_6)); }
	inline PrefabSingletonProvider_t2854807885 * get_U3CU3Ef__this_6() const { return ___U3CU3Ef__this_6; }
	inline PrefabSingletonProvider_t2854807885 ** get_address_of_U3CU3Ef__this_6() { return &___U3CU3Ef__this_6; }
	inline void set_U3CU3Ef__this_6(PrefabSingletonProvider_t2854807885 * value)
	{
		___U3CU3Ef__this_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
