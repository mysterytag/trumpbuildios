﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<GetCharacterLeaderboard>c__AnonStoreyC7
struct U3CGetCharacterLeaderboardU3Ec__AnonStoreyC7_t3290890571;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<GetCharacterLeaderboard>c__AnonStoreyC7::.ctor()
extern "C"  void U3CGetCharacterLeaderboardU3Ec__AnonStoreyC7__ctor_m1920209512 (U3CGetCharacterLeaderboardU3Ec__AnonStoreyC7_t3290890571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<GetCharacterLeaderboard>c__AnonStoreyC7::<>m__B4(PlayFab.CallRequestContainer)
extern "C"  void U3CGetCharacterLeaderboardU3Ec__AnonStoreyC7_U3CU3Em__B4_m4245561336 (U3CGetCharacterLeaderboardU3Ec__AnonStoreyC7_t3290890571 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
