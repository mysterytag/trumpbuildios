﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>
struct DisposedObserver_1_t2090891309;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionMove2916433847.h"

// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m4281705674_gshared (DisposedObserver_1_t2090891309 * __this, const MethodInfo* method);
#define DisposedObserver_1__ctor_m4281705674(__this, method) ((  void (*) (DisposedObserver_1_t2090891309 *, const MethodInfo*))DisposedObserver_1__ctor_m4281705674_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m3401760803_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define DisposedObserver_1__cctor_m3401760803(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))DisposedObserver_1__cctor_m3401760803_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>::OnCompleted()
extern "C"  void DisposedObserver_1_OnCompleted_m2952829140_gshared (DisposedObserver_1_t2090891309 * __this, const MethodInfo* method);
#define DisposedObserver_1_OnCompleted_m2952829140(__this, method) ((  void (*) (DisposedObserver_1_t2090891309 *, const MethodInfo*))DisposedObserver_1_OnCompleted_m2952829140_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>::OnError(System.Exception)
extern "C"  void DisposedObserver_1_OnError_m487107585_gshared (DisposedObserver_1_t2090891309 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define DisposedObserver_1_OnError_m487107585(__this, ___error0, method) ((  void (*) (DisposedObserver_1_t2090891309 *, Exception_t1967233988 *, const MethodInfo*))DisposedObserver_1_OnError_m487107585_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>::OnNext(T)
extern "C"  void DisposedObserver_1_OnNext_m2971186418_gshared (DisposedObserver_1_t2090891309 * __this, CollectionMoveEvent_1_t2916433847  ___value0, const MethodInfo* method);
#define DisposedObserver_1_OnNext_m2971186418(__this, ___value0, method) ((  void (*) (DisposedObserver_1_t2090891309 *, CollectionMoveEvent_1_t2916433847 , const MethodInfo*))DisposedObserver_1_OnNext_m2971186418_gshared)(__this, ___value0, method)
