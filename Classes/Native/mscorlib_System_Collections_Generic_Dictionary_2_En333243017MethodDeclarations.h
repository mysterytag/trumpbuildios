﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Double,System.Object>
struct Dictionary_2_t566215076;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En333243017.h"
#include "mscorlib_System_Collections_DictionaryEntry130027246.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g54746374.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Double,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2536120371_gshared (Enumerator_t333243018 * __this, Dictionary_2_t566215076 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m2536120371(__this, ___dictionary0, method) ((  void (*) (Enumerator_t333243018 *, Dictionary_2_t566215076 *, const MethodInfo*))Enumerator__ctor_m2536120371_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Double,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1912820430_gshared (Enumerator_t333243018 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1912820430(__this, method) ((  Il2CppObject * (*) (Enumerator_t333243018 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1912820430_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Double,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1284658850_gshared (Enumerator_t333243018 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1284658850(__this, method) ((  void (*) (Enumerator_t333243018 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1284658850_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Double,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t130027246  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1428556203_gshared (Enumerator_t333243018 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1428556203(__this, method) ((  DictionaryEntry_t130027246  (*) (Enumerator_t333243018 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1428556203_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Double,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3247488298_gshared (Enumerator_t333243018 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3247488298(__this, method) ((  Il2CppObject * (*) (Enumerator_t333243018 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3247488298_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Double,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3738433852_gshared (Enumerator_t333243018 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3738433852(__this, method) ((  Il2CppObject * (*) (Enumerator_t333243018 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3738433852_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Double,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3400303246_gshared (Enumerator_t333243018 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3400303246(__this, method) ((  bool (*) (Enumerator_t333243018 *, const MethodInfo*))Enumerator_MoveNext_m3400303246_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Double,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t54746374  Enumerator_get_Current_m3991428322_gshared (Enumerator_t333243018 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3991428322(__this, method) ((  KeyValuePair_2_t54746374  (*) (Enumerator_t333243018 *, const MethodInfo*))Enumerator_get_Current_m3991428322_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Double,System.Object>::get_CurrentKey()
extern "C"  double Enumerator_get_CurrentKey_m2296830939_gshared (Enumerator_t333243018 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m2296830939(__this, method) ((  double (*) (Enumerator_t333243018 *, const MethodInfo*))Enumerator_get_CurrentKey_m2296830939_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Double,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m3846010303_gshared (Enumerator_t333243018 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m3846010303(__this, method) ((  Il2CppObject * (*) (Enumerator_t333243018 *, const MethodInfo*))Enumerator_get_CurrentValue_m3846010303_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Double,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m3277856261_gshared (Enumerator_t333243018 * __this, const MethodInfo* method);
#define Enumerator_Reset_m3277856261(__this, method) ((  void (*) (Enumerator_t333243018 *, const MethodInfo*))Enumerator_Reset_m3277856261_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Double,System.Object>::VerifyState()
extern "C"  void Enumerator_VerifyState_m4041479758_gshared (Enumerator_t333243018 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m4041479758(__this, method) ((  void (*) (Enumerator_t333243018 *, const MethodInfo*))Enumerator_VerifyState_m4041479758_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Double,System.Object>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m3956483638_gshared (Enumerator_t333243018 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m3956483638(__this, method) ((  void (*) (Enumerator_t333243018 *, const MethodInfo*))Enumerator_VerifyCurrent_m3956483638_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Double,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m37300629_gshared (Enumerator_t333243018 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m37300629(__this, method) ((  void (*) (Enumerator_t333243018 *, const MethodInfo*))Enumerator_Dispose_m37300629_gshared)(__this, method)
