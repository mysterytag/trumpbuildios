﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.Boolean>
struct Action_1_t359458046;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VkConnectorBase/<JoinGroup>c__AnonStoreyD9
struct  U3CJoinGroupU3Ec__AnonStoreyD9_t3333112023  : public Il2CppObject
{
public:
	// System.Action`1<System.Boolean> VkConnectorBase/<JoinGroup>c__AnonStoreyD9::joinResult
	Action_1_t359458046 * ___joinResult_0;

public:
	inline static int32_t get_offset_of_joinResult_0() { return static_cast<int32_t>(offsetof(U3CJoinGroupU3Ec__AnonStoreyD9_t3333112023, ___joinResult_0)); }
	inline Action_1_t359458046 * get_joinResult_0() const { return ___joinResult_0; }
	inline Action_1_t359458046 ** get_address_of_joinResult_0() { return &___joinResult_0; }
	inline void set_joinResult_0(Action_1_t359458046 * value)
	{
		___joinResult_0 = value;
		Il2CppCodeGenWriteBarrier(&___joinResult_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
