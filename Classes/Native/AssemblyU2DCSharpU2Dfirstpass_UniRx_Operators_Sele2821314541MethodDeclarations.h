﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectObservable`2/Select<System.Int64,UnityEngine.Vector2>
struct Select_t2821314541;
// UniRx.Operators.SelectObservable`2<System.Int64,UnityEngine.Vector2>
struct SelectObservable_2_t4005727219;
// UniRx.IObserver`1<UnityEngine.Vector2>
struct IObserver_1_t1442361395;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SelectObservable`2/Select<System.Int64,UnityEngine.Vector2>::.ctor(UniRx.Operators.SelectObservable`2<T,TR>,UniRx.IObserver`1<TR>,System.IDisposable)
extern "C"  void Select__ctor_m4176532596_gshared (Select_t2821314541 * __this, SelectObservable_2_t4005727219 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Select__ctor_m4176532596(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Select_t2821314541 *, SelectObservable_2_t4005727219 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Select__ctor_m4176532596_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.SelectObservable`2/Select<System.Int64,UnityEngine.Vector2>::OnNext(T)
extern "C"  void Select_OnNext_m3228142228_gshared (Select_t2821314541 * __this, int64_t ___value0, const MethodInfo* method);
#define Select_OnNext_m3228142228(__this, ___value0, method) ((  void (*) (Select_t2821314541 *, int64_t, const MethodInfo*))Select_OnNext_m3228142228_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SelectObservable`2/Select<System.Int64,UnityEngine.Vector2>::OnError(System.Exception)
extern "C"  void Select_OnError_m3985505699_gshared (Select_t2821314541 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Select_OnError_m3985505699(__this, ___error0, method) ((  void (*) (Select_t2821314541 *, Exception_t1967233988 *, const MethodInfo*))Select_OnError_m3985505699_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SelectObservable`2/Select<System.Int64,UnityEngine.Vector2>::OnCompleted()
extern "C"  void Select_OnCompleted_m1506397558_gshared (Select_t2821314541 * __this, const MethodInfo* method);
#define Select_OnCompleted_m1506397558(__this, method) ((  void (*) (Select_t2821314541 *, const MethodInfo*))Select_OnCompleted_m1506397558_gshared)(__this, method)
