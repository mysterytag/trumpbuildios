﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<System.Object>
struct U3CSubscribeU3Ec__AnonStorey5B_t435113149;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<System.Object>::.ctor()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B__ctor_m3489265350_gshared (U3CSubscribeU3Ec__AnonStorey5B_t435113149 * __this, const MethodInfo* method);
#define U3CSubscribeU3Ec__AnonStorey5B__ctor_m3489265350(__this, method) ((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t435113149 *, const MethodInfo*))U3CSubscribeU3Ec__AnonStorey5B__ctor_m3489265350_gshared)(__this, method)
// System.Void UniRx.Operators.OperatorObservableBase`1/<Subscribe>c__AnonStorey5B<System.Object>::<>m__73()
extern "C"  void U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m1122764779_gshared (U3CSubscribeU3Ec__AnonStorey5B_t435113149 * __this, const MethodInfo* method);
#define U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m1122764779(__this, method) ((  void (*) (U3CSubscribeU3Ec__AnonStorey5B_t435113149 *, const MethodInfo*))U3CSubscribeU3Ec__AnonStorey5B_U3CU3Em__73_m1122764779_gshared)(__this, method)
