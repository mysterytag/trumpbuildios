﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Queue`1<UniRx.Timestamped`1<System.Object>>
struct Queue_1_t4227271813;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_Enumerat1401926234.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Timestamped_1_2519184273.h"

// System.Void System.Collections.Generic.Queue`1/Enumerator<UniRx.Timestamped`1<System.Object>>::.ctor(System.Collections.Generic.Queue`1<T>)
extern "C"  void Enumerator__ctor_m1917381129_gshared (Enumerator_t1401926234 * __this, Queue_1_t4227271813 * ___q0, const MethodInfo* method);
#define Enumerator__ctor_m1917381129(__this, ___q0, method) ((  void (*) (Enumerator_t1401926234 *, Queue_1_t4227271813 *, const MethodInfo*))Enumerator__ctor_m1917381129_gshared)(__this, ___q0, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<UniRx.Timestamped`1<System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3116543620_gshared (Enumerator_t1401926234 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3116543620(__this, method) ((  void (*) (Enumerator_t1401926234 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3116543620_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1/Enumerator<UniRx.Timestamped`1<System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2555389936_gshared (Enumerator_t1401926234 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2555389936(__this, method) ((  Il2CppObject * (*) (Enumerator_t1401926234 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2555389936_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<UniRx.Timestamped`1<System.Object>>::Dispose()
extern "C"  void Enumerator_Dispose_m2765563507_gshared (Enumerator_t1401926234 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2765563507(__this, method) ((  void (*) (Enumerator_t1401926234 *, const MethodInfo*))Enumerator_Dispose_m2765563507_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Queue`1/Enumerator<UniRx.Timestamped`1<System.Object>>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1067784412_gshared (Enumerator_t1401926234 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1067784412(__this, method) ((  bool (*) (Enumerator_t1401926234 *, const MethodInfo*))Enumerator_MoveNext_m1067784412_gshared)(__this, method)
// T System.Collections.Generic.Queue`1/Enumerator<UniRx.Timestamped`1<System.Object>>::get_Current()
extern "C"  Timestamped_1_t2519184273  Enumerator_get_Current_m2989000291_gshared (Enumerator_t1401926234 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2989000291(__this, method) ((  Timestamped_1_t2519184273  (*) (Enumerator_t1401926234 *, const MethodInfo*))Enumerator_get_Current_m2989000291_gshared)(__this, method)
