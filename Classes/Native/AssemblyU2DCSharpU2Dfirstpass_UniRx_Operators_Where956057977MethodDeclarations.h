﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.WhereObservable`1/<CombinePredicate>c__AnonStorey72<UnityEngine.Vector2>
struct U3CCombinePredicateU3Ec__AnonStorey72_t956057977;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"

// System.Void UniRx.Operators.WhereObservable`1/<CombinePredicate>c__AnonStorey72<UnityEngine.Vector2>::.ctor()
extern "C"  void U3CCombinePredicateU3Ec__AnonStorey72__ctor_m1270904442_gshared (U3CCombinePredicateU3Ec__AnonStorey72_t956057977 * __this, const MethodInfo* method);
#define U3CCombinePredicateU3Ec__AnonStorey72__ctor_m1270904442(__this, method) ((  void (*) (U3CCombinePredicateU3Ec__AnonStorey72_t956057977 *, const MethodInfo*))U3CCombinePredicateU3Ec__AnonStorey72__ctor_m1270904442_gshared)(__this, method)
// System.Boolean UniRx.Operators.WhereObservable`1/<CombinePredicate>c__AnonStorey72<UnityEngine.Vector2>::<>m__8F(T)
extern "C"  bool U3CCombinePredicateU3Ec__AnonStorey72_U3CU3Em__8F_m2396907721_gshared (U3CCombinePredicateU3Ec__AnonStorey72_t956057977 * __this, Vector2_t3525329788  ___x0, const MethodInfo* method);
#define U3CCombinePredicateU3Ec__AnonStorey72_U3CU3Em__8F_m2396907721(__this, ___x0, method) ((  bool (*) (U3CCombinePredicateU3Ec__AnonStorey72_t956057977 *, Vector2_t3525329788 , const MethodInfo*))U3CCombinePredicateU3Ec__AnonStorey72_U3CU3Em__8F_m2396907721_gshared)(__this, ___x0, method)
