﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<GetCharacterReadOnlyData>c__AnonStoreyCE
struct U3CGetCharacterReadOnlyDataU3Ec__AnonStoreyCE_t3350837190;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<GetCharacterReadOnlyData>c__AnonStoreyCE::.ctor()
extern "C"  void U3CGetCharacterReadOnlyDataU3Ec__AnonStoreyCE__ctor_m394904237 (U3CGetCharacterReadOnlyDataU3Ec__AnonStoreyCE_t3350837190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<GetCharacterReadOnlyData>c__AnonStoreyCE::<>m__BB(PlayFab.CallRequestContainer)
extern "C"  void U3CGetCharacterReadOnlyDataU3Ec__AnonStoreyCE_U3CU3Em__BB_m545832203 (U3CGetCharacterReadOnlyDataU3Ec__AnonStoreyCE_t3350837190 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
