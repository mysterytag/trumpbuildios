﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Counter/<TextChangeCoroutine>c__Iterator32
struct U3CTextChangeCoroutineU3Ec__Iterator32_t2757232561;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Counter/<TextChangeCoroutine>c__Iterator32::.ctor()
extern "C"  void U3CTextChangeCoroutineU3Ec__Iterator32__ctor_m3904582743 (U3CTextChangeCoroutineU3Ec__Iterator32_t2757232561 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Counter/<TextChangeCoroutine>c__Iterator32::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTextChangeCoroutineU3Ec__Iterator32_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3718725541 (U3CTextChangeCoroutineU3Ec__Iterator32_t2757232561 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Counter/<TextChangeCoroutine>c__Iterator32::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTextChangeCoroutineU3Ec__Iterator32_System_Collections_IEnumerator_get_Current_m3805228345 (U3CTextChangeCoroutineU3Ec__Iterator32_t2757232561 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Counter/<TextChangeCoroutine>c__Iterator32::MoveNext()
extern "C"  bool U3CTextChangeCoroutineU3Ec__Iterator32_MoveNext_m4053789989 (U3CTextChangeCoroutineU3Ec__Iterator32_t2757232561 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Counter/<TextChangeCoroutine>c__Iterator32::Dispose()
extern "C"  void U3CTextChangeCoroutineU3Ec__Iterator32_Dispose_m2695849684 (U3CTextChangeCoroutineU3Ec__Iterator32_t2757232561 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Counter/<TextChangeCoroutine>c__Iterator32::Reset()
extern "C"  void U3CTextChangeCoroutineU3Ec__Iterator32_Reset_m1551015684 (U3CTextChangeCoroutineU3Ec__Iterator32_t2757232561 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
