﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ComponentModel.CollectionChangeEventArgs
struct CollectionChangeEventArgs_t3787252946;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_System_ComponentModel_CollectionChangeActio1156311693.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.ComponentModel.CollectionChangeEventArgs::.ctor(System.ComponentModel.CollectionChangeAction,System.Object)
extern "C"  void CollectionChangeEventArgs__ctor_m2861972811 (CollectionChangeEventArgs_t3787252946 * __this, int32_t ___action0, Il2CppObject * ___element1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ComponentModel.CollectionChangeAction System.ComponentModel.CollectionChangeEventArgs::get_Action()
extern "C"  int32_t CollectionChangeEventArgs_get_Action_m821724647 (CollectionChangeEventArgs_t3787252946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.ComponentModel.CollectionChangeEventArgs::get_Element()
extern "C"  Il2CppObject * CollectionChangeEventArgs_get_Element_m3252924714 (CollectionChangeEventArgs_t3787252946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
