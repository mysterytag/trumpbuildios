﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Func`2<System.Object,UniRx.IObservable`1<System.Int64>>
struct Func_2_t3904890178;
// System.Func`3<System.Object,System.Int32,UniRx.IObservable`1<System.Int64>>
struct Func_3_t1164808108;
// System.Func`2<System.Object,System.Collections.Generic.IEnumerable`1<System.Int64>>
struct Func_2_t2723278874;
// System.Func`3<System.Object,System.Int32,System.Collections.Generic.IEnumerable`1<System.Int64>>
struct Func_3_t4278164100;
// System.Func`3<System.Object,System.Int64,System.Object>
struct Func_3_t2633863527;
// System.Func`5<System.Object,System.Int32,System.Int64,System.Int32,System.Object>
struct Func_5_t3557632551;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4196218687.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.SelectManyObservable`3<System.Object,System.Int64,System.Object>
struct  SelectManyObservable_3_t744723289  : public OperatorObservableBase_1_t4196218687
{
public:
	// UniRx.IObservable`1<TSource> UniRx.Operators.SelectManyObservable`3::source
	Il2CppObject* ___source_1;
	// System.Func`2<TSource,UniRx.IObservable`1<TCollection>> UniRx.Operators.SelectManyObservable`3::collectionSelector
	Func_2_t3904890178 * ___collectionSelector_2;
	// System.Func`3<TSource,System.Int32,UniRx.IObservable`1<TCollection>> UniRx.Operators.SelectManyObservable`3::collectionSelectorWithIndex
	Func_3_t1164808108 * ___collectionSelectorWithIndex_3;
	// System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TCollection>> UniRx.Operators.SelectManyObservable`3::collectionSelectorEnumerable
	Func_2_t2723278874 * ___collectionSelectorEnumerable_4;
	// System.Func`3<TSource,System.Int32,System.Collections.Generic.IEnumerable`1<TCollection>> UniRx.Operators.SelectManyObservable`3::collectionSelectorEnumerableWithIndex
	Func_3_t4278164100 * ___collectionSelectorEnumerableWithIndex_5;
	// System.Func`3<TSource,TCollection,TResult> UniRx.Operators.SelectManyObservable`3::resultSelector
	Func_3_t2633863527 * ___resultSelector_6;
	// System.Func`5<TSource,System.Int32,TCollection,System.Int32,TResult> UniRx.Operators.SelectManyObservable`3::resultSelectorWithIndex
	Func_5_t3557632551 * ___resultSelectorWithIndex_7;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(SelectManyObservable_3_t744723289, ___source_1)); }
	inline Il2CppObject* get_source_1() const { return ___source_1; }
	inline Il2CppObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(Il2CppObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier(&___source_1, value);
	}

	inline static int32_t get_offset_of_collectionSelector_2() { return static_cast<int32_t>(offsetof(SelectManyObservable_3_t744723289, ___collectionSelector_2)); }
	inline Func_2_t3904890178 * get_collectionSelector_2() const { return ___collectionSelector_2; }
	inline Func_2_t3904890178 ** get_address_of_collectionSelector_2() { return &___collectionSelector_2; }
	inline void set_collectionSelector_2(Func_2_t3904890178 * value)
	{
		___collectionSelector_2 = value;
		Il2CppCodeGenWriteBarrier(&___collectionSelector_2, value);
	}

	inline static int32_t get_offset_of_collectionSelectorWithIndex_3() { return static_cast<int32_t>(offsetof(SelectManyObservable_3_t744723289, ___collectionSelectorWithIndex_3)); }
	inline Func_3_t1164808108 * get_collectionSelectorWithIndex_3() const { return ___collectionSelectorWithIndex_3; }
	inline Func_3_t1164808108 ** get_address_of_collectionSelectorWithIndex_3() { return &___collectionSelectorWithIndex_3; }
	inline void set_collectionSelectorWithIndex_3(Func_3_t1164808108 * value)
	{
		___collectionSelectorWithIndex_3 = value;
		Il2CppCodeGenWriteBarrier(&___collectionSelectorWithIndex_3, value);
	}

	inline static int32_t get_offset_of_collectionSelectorEnumerable_4() { return static_cast<int32_t>(offsetof(SelectManyObservable_3_t744723289, ___collectionSelectorEnumerable_4)); }
	inline Func_2_t2723278874 * get_collectionSelectorEnumerable_4() const { return ___collectionSelectorEnumerable_4; }
	inline Func_2_t2723278874 ** get_address_of_collectionSelectorEnumerable_4() { return &___collectionSelectorEnumerable_4; }
	inline void set_collectionSelectorEnumerable_4(Func_2_t2723278874 * value)
	{
		___collectionSelectorEnumerable_4 = value;
		Il2CppCodeGenWriteBarrier(&___collectionSelectorEnumerable_4, value);
	}

	inline static int32_t get_offset_of_collectionSelectorEnumerableWithIndex_5() { return static_cast<int32_t>(offsetof(SelectManyObservable_3_t744723289, ___collectionSelectorEnumerableWithIndex_5)); }
	inline Func_3_t4278164100 * get_collectionSelectorEnumerableWithIndex_5() const { return ___collectionSelectorEnumerableWithIndex_5; }
	inline Func_3_t4278164100 ** get_address_of_collectionSelectorEnumerableWithIndex_5() { return &___collectionSelectorEnumerableWithIndex_5; }
	inline void set_collectionSelectorEnumerableWithIndex_5(Func_3_t4278164100 * value)
	{
		___collectionSelectorEnumerableWithIndex_5 = value;
		Il2CppCodeGenWriteBarrier(&___collectionSelectorEnumerableWithIndex_5, value);
	}

	inline static int32_t get_offset_of_resultSelector_6() { return static_cast<int32_t>(offsetof(SelectManyObservable_3_t744723289, ___resultSelector_6)); }
	inline Func_3_t2633863527 * get_resultSelector_6() const { return ___resultSelector_6; }
	inline Func_3_t2633863527 ** get_address_of_resultSelector_6() { return &___resultSelector_6; }
	inline void set_resultSelector_6(Func_3_t2633863527 * value)
	{
		___resultSelector_6 = value;
		Il2CppCodeGenWriteBarrier(&___resultSelector_6, value);
	}

	inline static int32_t get_offset_of_resultSelectorWithIndex_7() { return static_cast<int32_t>(offsetof(SelectManyObservable_3_t744723289, ___resultSelectorWithIndex_7)); }
	inline Func_5_t3557632551 * get_resultSelectorWithIndex_7() const { return ___resultSelectorWithIndex_7; }
	inline Func_5_t3557632551 ** get_address_of_resultSelectorWithIndex_7() { return &___resultSelectorWithIndex_7; }
	inline void set_resultSelectorWithIndex_7(Func_5_t3557632551 * value)
	{
		___resultSelectorWithIndex_7 = value;
		Il2CppCodeGenWriteBarrier(&___resultSelectorWithIndex_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
