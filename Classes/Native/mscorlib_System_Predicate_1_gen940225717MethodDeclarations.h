﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Predicate`1<UniRx.Tuple`2<System.Object,System.Object>>
struct Predicate_1_t940225717;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen369261819.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Predicate`1<UniRx.Tuple`2<System.Object,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3164355731_gshared (Predicate_1_t940225717 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Predicate_1__ctor_m3164355731(__this, ___object0, ___method1, method) ((  void (*) (Predicate_1_t940225717 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m3164355731_gshared)(__this, ___object0, ___method1, method)
// System.Boolean System.Predicate`1<UniRx.Tuple`2<System.Object,System.Object>>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m790483855_gshared (Predicate_1_t940225717 * __this, Tuple_2_t369261819  ___obj0, const MethodInfo* method);
#define Predicate_1_Invoke_m790483855(__this, ___obj0, method) ((  bool (*) (Predicate_1_t940225717 *, Tuple_2_t369261819 , const MethodInfo*))Predicate_1_Invoke_m790483855_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Predicate`1<UniRx.Tuple`2<System.Object,System.Object>>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m4132879390_gshared (Predicate_1_t940225717 * __this, Tuple_2_t369261819  ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method);
#define Predicate_1_BeginInvoke_m4132879390(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Predicate_1_t940225717 *, Tuple_2_t369261819 , AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Predicate_1_BeginInvoke_m4132879390_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Boolean System.Predicate`1<UniRx.Tuple`2<System.Object,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m4049309605_gshared (Predicate_1_t940225717 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Predicate_1_EndInvoke_m4049309605(__this, ___result0, method) ((  bool (*) (Predicate_1_t940225717 *, Il2CppObject *, const MethodInfo*))Predicate_1_EndInvoke_m4049309605_gshared)(__this, ___result0, method)
