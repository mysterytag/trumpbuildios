﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PlayFab_Internal_SingletonMonoBeh312324993MethodDeclarations.h"

// System.Void PlayFab.Internal.SingletonMonoBehaviour`1<PlayFab.Internal.PlayFabHTTP>::.ctor()
#define SingletonMonoBehaviour_1__ctor_m2294850776(__this, method) ((  void (*) (SingletonMonoBehaviour_1_t662878960 *, const MethodInfo*))SingletonMonoBehaviour_1__ctor_m4044296597_gshared)(__this, method)
// T PlayFab.Internal.SingletonMonoBehaviour`1<PlayFab.Internal.PlayFabHTTP>::get_instance()
#define SingletonMonoBehaviour_1_get_instance_m168176587(__this /* static, unused */, method) ((  PlayFabHTTP_t1187660387 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SingletonMonoBehaviour_1_get_instance_m1518032716_gshared)(__this /* static, unused */, method)
// System.Void PlayFab.Internal.SingletonMonoBehaviour`1<PlayFab.Internal.PlayFabHTTP>::Awake()
#define SingletonMonoBehaviour_1_Awake_m2532455995(__this, method) ((  void (*) (SingletonMonoBehaviour_1_t662878960 *, const MethodInfo*))SingletonMonoBehaviour_1_Awake_m4281901816_gshared)(__this, method)
// System.Boolean PlayFab.Internal.SingletonMonoBehaviour`1<PlayFab.Internal.PlayFabHTTP>::get_initialized()
#define SingletonMonoBehaviour_1_get_initialized_m14452277(__this, method) ((  bool (*) (SingletonMonoBehaviour_1_t662878960 *, const MethodInfo*))SingletonMonoBehaviour_1_get_initialized_m1888521674_gshared)(__this, method)
// System.Void PlayFab.Internal.SingletonMonoBehaviour`1<PlayFab.Internal.PlayFabHTTP>::set_initialized(System.Boolean)
#define SingletonMonoBehaviour_1_set_initialized_m3986461508(__this, ___value0, method) ((  void (*) (SingletonMonoBehaviour_1_t662878960 *, bool, const MethodInfo*))SingletonMonoBehaviour_1_set_initialized_m1544018177_gshared)(__this, ___value0, method)
// System.Void PlayFab.Internal.SingletonMonoBehaviour`1<PlayFab.Internal.PlayFabHTTP>::Initialize()
#define SingletonMonoBehaviour_1_Initialize_m872478908(__this, method) ((  void (*) (SingletonMonoBehaviour_1_t662878960 *, const MethodInfo*))SingletonMonoBehaviour_1_Initialize_m2506226207_gshared)(__this, method)
