﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;

#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.AsyncSubject`1<UniRx.Unit>
struct  AsyncSubject_1_t3257925142  : public Il2CppObject
{
public:
	// System.Object UniRx.AsyncSubject`1::observerLock
	Il2CppObject * ___observerLock_0;
	// T UniRx.AsyncSubject`1::lastValue
	Unit_t2558286038  ___lastValue_1;
	// System.Boolean UniRx.AsyncSubject`1::hasValue
	bool ___hasValue_2;
	// System.Boolean UniRx.AsyncSubject`1::isStopped
	bool ___isStopped_3;
	// System.Boolean UniRx.AsyncSubject`1::isDisposed
	bool ___isDisposed_4;
	// System.Exception UniRx.AsyncSubject`1::lastError
	Exception_t1967233988 * ___lastError_5;
	// UniRx.IObserver`1<T> UniRx.AsyncSubject`1::outObserver
	Il2CppObject* ___outObserver_6;

public:
	inline static int32_t get_offset_of_observerLock_0() { return static_cast<int32_t>(offsetof(AsyncSubject_1_t3257925142, ___observerLock_0)); }
	inline Il2CppObject * get_observerLock_0() const { return ___observerLock_0; }
	inline Il2CppObject ** get_address_of_observerLock_0() { return &___observerLock_0; }
	inline void set_observerLock_0(Il2CppObject * value)
	{
		___observerLock_0 = value;
		Il2CppCodeGenWriteBarrier(&___observerLock_0, value);
	}

	inline static int32_t get_offset_of_lastValue_1() { return static_cast<int32_t>(offsetof(AsyncSubject_1_t3257925142, ___lastValue_1)); }
	inline Unit_t2558286038  get_lastValue_1() const { return ___lastValue_1; }
	inline Unit_t2558286038 * get_address_of_lastValue_1() { return &___lastValue_1; }
	inline void set_lastValue_1(Unit_t2558286038  value)
	{
		___lastValue_1 = value;
	}

	inline static int32_t get_offset_of_hasValue_2() { return static_cast<int32_t>(offsetof(AsyncSubject_1_t3257925142, ___hasValue_2)); }
	inline bool get_hasValue_2() const { return ___hasValue_2; }
	inline bool* get_address_of_hasValue_2() { return &___hasValue_2; }
	inline void set_hasValue_2(bool value)
	{
		___hasValue_2 = value;
	}

	inline static int32_t get_offset_of_isStopped_3() { return static_cast<int32_t>(offsetof(AsyncSubject_1_t3257925142, ___isStopped_3)); }
	inline bool get_isStopped_3() const { return ___isStopped_3; }
	inline bool* get_address_of_isStopped_3() { return &___isStopped_3; }
	inline void set_isStopped_3(bool value)
	{
		___isStopped_3 = value;
	}

	inline static int32_t get_offset_of_isDisposed_4() { return static_cast<int32_t>(offsetof(AsyncSubject_1_t3257925142, ___isDisposed_4)); }
	inline bool get_isDisposed_4() const { return ___isDisposed_4; }
	inline bool* get_address_of_isDisposed_4() { return &___isDisposed_4; }
	inline void set_isDisposed_4(bool value)
	{
		___isDisposed_4 = value;
	}

	inline static int32_t get_offset_of_lastError_5() { return static_cast<int32_t>(offsetof(AsyncSubject_1_t3257925142, ___lastError_5)); }
	inline Exception_t1967233988 * get_lastError_5() const { return ___lastError_5; }
	inline Exception_t1967233988 ** get_address_of_lastError_5() { return &___lastError_5; }
	inline void set_lastError_5(Exception_t1967233988 * value)
	{
		___lastError_5 = value;
		Il2CppCodeGenWriteBarrier(&___lastError_5, value);
	}

	inline static int32_t get_offset_of_outObserver_6() { return static_cast<int32_t>(offsetof(AsyncSubject_1_t3257925142, ___outObserver_6)); }
	inline Il2CppObject* get_outObserver_6() const { return ___outObserver_6; }
	inline Il2CppObject** get_address_of_outObserver_6() { return &___outObserver_6; }
	inline void set_outObserver_6(Il2CppObject* value)
	{
		___outObserver_6 = value;
		Il2CppCodeGenWriteBarrier(&___outObserver_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
