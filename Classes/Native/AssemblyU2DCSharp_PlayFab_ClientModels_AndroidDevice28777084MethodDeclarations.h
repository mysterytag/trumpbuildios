﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.AndroidDevicePushNotificationRegistrationRequest
struct AndroidDevicePushNotificationRegistrationRequest_t28777084;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen3097043249.h"

// System.Void PlayFab.ClientModels.AndroidDevicePushNotificationRegistrationRequest::.ctor()
extern "C"  void AndroidDevicePushNotificationRegistrationRequest__ctor_m3814111553 (AndroidDevicePushNotificationRegistrationRequest_t28777084 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.AndroidDevicePushNotificationRegistrationRequest::get_DeviceToken()
extern "C"  String_t* AndroidDevicePushNotificationRegistrationRequest_get_DeviceToken_m3762215260 (AndroidDevicePushNotificationRegistrationRequest_t28777084 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.AndroidDevicePushNotificationRegistrationRequest::set_DeviceToken(System.String)
extern "C"  void AndroidDevicePushNotificationRegistrationRequest_set_DeviceToken_m1272078045 (AndroidDevicePushNotificationRegistrationRequest_t28777084 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> PlayFab.ClientModels.AndroidDevicePushNotificationRegistrationRequest::get_SendPushNotificationConfirmation()
extern "C"  Nullable_1_t3097043249  AndroidDevicePushNotificationRegistrationRequest_get_SendPushNotificationConfirmation_m3553732325 (AndroidDevicePushNotificationRegistrationRequest_t28777084 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.AndroidDevicePushNotificationRegistrationRequest::set_SendPushNotificationConfirmation(System.Nullable`1<System.Boolean>)
extern "C"  void AndroidDevicePushNotificationRegistrationRequest_set_SendPushNotificationConfirmation_m1705727590 (AndroidDevicePushNotificationRegistrationRequest_t28777084 * __this, Nullable_1_t3097043249  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.AndroidDevicePushNotificationRegistrationRequest::get_ConfirmationMessege()
extern "C"  String_t* AndroidDevicePushNotificationRegistrationRequest_get_ConfirmationMessege_m1535741039 (AndroidDevicePushNotificationRegistrationRequest_t28777084 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.AndroidDevicePushNotificationRegistrationRequest::set_ConfirmationMessege(System.String)
extern "C"  void AndroidDevicePushNotificationRegistrationRequest_set_ConfirmationMessege_m1609620394 (AndroidDevicePushNotificationRegistrationRequest_t28777084 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
