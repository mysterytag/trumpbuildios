﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.StartWithObservable`1/StartWith<System.Int32>
struct StartWith_t1733935216;
// UniRx.Operators.StartWithObservable`1<System.Int32>
struct StartWithObservable_1_t338430729;
// UniRx.IObserver`1<System.Int32>
struct IObserver_1_t764446394;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.StartWithObservable`1/StartWith<System.Int32>::.ctor(UniRx.Operators.StartWithObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void StartWith__ctor_m4118200041_gshared (StartWith_t1733935216 * __this, StartWithObservable_1_t338430729 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define StartWith__ctor_m4118200041(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (StartWith_t1733935216 *, StartWithObservable_1_t338430729 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))StartWith__ctor_m4118200041_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.StartWithObservable`1/StartWith<System.Int32>::Run()
extern "C"  Il2CppObject * StartWith_Run_m3126324458_gshared (StartWith_t1733935216 * __this, const MethodInfo* method);
#define StartWith_Run_m3126324458(__this, method) ((  Il2CppObject * (*) (StartWith_t1733935216 *, const MethodInfo*))StartWith_Run_m3126324458_gshared)(__this, method)
// System.Void UniRx.Operators.StartWithObservable`1/StartWith<System.Int32>::OnNext(T)
extern "C"  void StartWith_OnNext_m1897755310_gshared (StartWith_t1733935216 * __this, int32_t ___value0, const MethodInfo* method);
#define StartWith_OnNext_m1897755310(__this, ___value0, method) ((  void (*) (StartWith_t1733935216 *, int32_t, const MethodInfo*))StartWith_OnNext_m1897755310_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.StartWithObservable`1/StartWith<System.Int32>::OnError(System.Exception)
extern "C"  void StartWith_OnError_m4205998525_gshared (StartWith_t1733935216 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define StartWith_OnError_m4205998525(__this, ___error0, method) ((  void (*) (StartWith_t1733935216 *, Exception_t1967233988 *, const MethodInfo*))StartWith_OnError_m4205998525_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.StartWithObservable`1/StartWith<System.Int32>::OnCompleted()
extern "C"  void StartWith_OnCompleted_m1069029520_gshared (StartWith_t1733935216 * __this, const MethodInfo* method);
#define StartWith_OnCompleted_m1069029520(__this, method) ((  void (*) (StartWith_t1733935216 *, const MethodInfo*))StartWith_OnCompleted_m1069029520_gshared)(__this, method)
