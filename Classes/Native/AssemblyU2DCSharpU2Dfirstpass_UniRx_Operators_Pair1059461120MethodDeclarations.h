﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.PairwiseObservable`1<System.Object>
struct PairwiseObservable_1_t1059461120;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.Pair`1<System.Object>>
struct IObserver_1_t1748129189;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.PairwiseObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>)
extern "C"  void PairwiseObservable_1__ctor_m1936169059_gshared (PairwiseObservable_1_t1059461120 * __this, Il2CppObject* ___source0, const MethodInfo* method);
#define PairwiseObservable_1__ctor_m1936169059(__this, ___source0, method) ((  void (*) (PairwiseObservable_1_t1059461120 *, Il2CppObject*, const MethodInfo*))PairwiseObservable_1__ctor_m1936169059_gshared)(__this, ___source0, method)
// System.IDisposable UniRx.Operators.PairwiseObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<UniRx.Pair`1<T>>,System.IDisposable)
extern "C"  Il2CppObject * PairwiseObservable_1_SubscribeCore_m3828750197_gshared (PairwiseObservable_1_t1059461120 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define PairwiseObservable_1_SubscribeCore_m3828750197(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (PairwiseObservable_1_t1059461120 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))PairwiseObservable_1_SubscribeCore_m3828750197_gshared)(__this, ___observer0, ___cancel1, method)
