﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<System.DateTime>
struct ReadOnlyCollection_1_t3502179284;
// System.Collections.Generic.IList`1<System.DateTime>
struct IList_1_t2505526250;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// System.DateTime[]
struct DateTimeU5BU5D_t2411579761;
// System.Collections.Generic.IEnumerator`1<System.DateTime>
struct IEnumerator_1_t1822140384;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime339033936.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.DateTime>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m2261276854_gshared (ReadOnlyCollection_1_t3502179284 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m2261276854(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t3502179284 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m2261276854_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.DateTime>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3133489056_gshared (ReadOnlyCollection_1_t3502179284 * __this, DateTime_t339033936  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3133489056(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t3502179284 *, DateTime_t339033936 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3133489056_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.DateTime>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m360603946_gshared (ReadOnlyCollection_1_t3502179284 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m360603946(__this, method) ((  void (*) (ReadOnlyCollection_1_t3502179284 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m360603946_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.DateTime>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3236870791_gshared (ReadOnlyCollection_1_t3502179284 * __this, int32_t ___index0, DateTime_t339033936  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3236870791(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t3502179284 *, int32_t, DateTime_t339033936 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3236870791_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.DateTime>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3403716307_gshared (ReadOnlyCollection_1_t3502179284 * __this, DateTime_t339033936  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3403716307(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t3502179284 *, DateTime_t339033936 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3403716307_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.DateTime>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1110723661_gshared (ReadOnlyCollection_1_t3502179284 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1110723661(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t3502179284 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1110723661_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.DateTime>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  DateTime_t339033936  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m659442897_gshared (ReadOnlyCollection_1_t3502179284 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m659442897(__this, ___index0, method) ((  DateTime_t339033936  (*) (ReadOnlyCollection_1_t3502179284 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m659442897_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.DateTime>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2886104478_gshared (ReadOnlyCollection_1_t3502179284 * __this, int32_t ___index0, DateTime_t339033936  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2886104478(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3502179284 *, int32_t, DateTime_t339033936 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2886104478_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.DateTime>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m888824668_gshared (ReadOnlyCollection_1_t3502179284 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m888824668(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3502179284 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m888824668_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.DateTime>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m4163679333_gshared (ReadOnlyCollection_1_t3502179284 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m4163679333(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t3502179284 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m4163679333_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<System.DateTime>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1944494496_gshared (ReadOnlyCollection_1_t3502179284 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1944494496(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3502179284 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1944494496_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.DateTime>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m1630587473_gshared (ReadOnlyCollection_1_t3502179284 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m1630587473(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3502179284 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1630587473_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.DateTime>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m1788647283_gshared (ReadOnlyCollection_1_t3502179284 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m1788647283(__this, method) ((  void (*) (ReadOnlyCollection_1_t3502179284 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m1788647283_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.DateTime>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m841668891_gshared (ReadOnlyCollection_1_t3502179284 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m841668891(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t3502179284 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m841668891_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.DateTime>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m460419113_gshared (ReadOnlyCollection_1_t3502179284 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m460419113(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3502179284 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m460419113_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.DateTime>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m3368707284_gshared (ReadOnlyCollection_1_t3502179284 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m3368707284(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3502179284 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m3368707284_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.DateTime>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m3227646932_gshared (ReadOnlyCollection_1_t3502179284 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m3227646932(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t3502179284 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m3227646932_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.DateTime>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2858162788_gshared (ReadOnlyCollection_1_t3502179284 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2858162788(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t3502179284 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2858162788_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.DateTime>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m133833825_gshared (ReadOnlyCollection_1_t3502179284 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m133833825(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3502179284 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m133833825_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.DateTime>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m481418893_gshared (ReadOnlyCollection_1_t3502179284 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m481418893(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3502179284 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m481418893_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.DateTime>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2066102474_gshared (ReadOnlyCollection_1_t3502179284 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2066102474(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3502179284 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2066102474_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.DateTime>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3568698607_gshared (ReadOnlyCollection_1_t3502179284 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3568698607(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3502179284 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3568698607_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.DateTime>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m4147667156_gshared (ReadOnlyCollection_1_t3502179284 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m4147667156(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3502179284 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m4147667156_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.DateTime>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m3831278507_gshared (ReadOnlyCollection_1_t3502179284 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m3831278507(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3502179284 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m3831278507_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.DateTime>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m2607133464_gshared (ReadOnlyCollection_1_t3502179284 * __this, DateTime_t339033936  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m2607133464(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t3502179284 *, DateTime_t339033936 , const MethodInfo*))ReadOnlyCollection_1_Contains_m2607133464_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.DateTime>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m2319020752_gshared (ReadOnlyCollection_1_t3502179284 * __this, DateTimeU5BU5D_t2411579761* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m2319020752(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t3502179284 *, DateTimeU5BU5D_t2411579761*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m2319020752_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<System.DateTime>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m3161732731_gshared (ReadOnlyCollection_1_t3502179284 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m3161732731(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t3502179284 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m3161732731_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.DateTime>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m2299594324_gshared (ReadOnlyCollection_1_t3502179284 * __this, DateTime_t339033936  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m2299594324(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3502179284 *, DateTime_t339033936 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m2299594324_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.DateTime>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m2385254823_gshared (ReadOnlyCollection_1_t3502179284 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m2385254823(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t3502179284 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m2385254823_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.DateTime>::get_Item(System.Int32)
extern "C"  DateTime_t339033936  ReadOnlyCollection_1_get_Item_m3560669969_gshared (ReadOnlyCollection_1_t3502179284 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m3560669969(__this, ___index0, method) ((  DateTime_t339033936  (*) (ReadOnlyCollection_1_t3502179284 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m3560669969_gshared)(__this, ___index0, method)
