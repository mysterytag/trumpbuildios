﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.IEnumerable`1<ICell`1<System.Object>>
struct IEnumerable_1_t965924457;
// System.Func`1<System.Collections.Generic.IEnumerable`1<System.Object>>
struct Func_1_t557074727;
// System.Func`2<ICell`1<System.Object>,System.Object>
struct Func_2_t2767404563;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CellReactiveApi/<Sequence>c__AnonStorey110`1<System.Object>
struct  U3CSequenceU3Ec__AnonStorey110_1_t1717931772  : public Il2CppObject
{
public:
	// System.Collections.Generic.IEnumerable`1<ICell`1<T>> CellReactiveApi/<Sequence>c__AnonStorey110`1::cells
	Il2CppObject* ___cells_0;
	// System.Func`1<System.Collections.Generic.IEnumerable`1<T>> CellReactiveApi/<Sequence>c__AnonStorey110`1::values
	Func_1_t557074727 * ___values_1;

public:
	inline static int32_t get_offset_of_cells_0() { return static_cast<int32_t>(offsetof(U3CSequenceU3Ec__AnonStorey110_1_t1717931772, ___cells_0)); }
	inline Il2CppObject* get_cells_0() const { return ___cells_0; }
	inline Il2CppObject** get_address_of_cells_0() { return &___cells_0; }
	inline void set_cells_0(Il2CppObject* value)
	{
		___cells_0 = value;
		Il2CppCodeGenWriteBarrier(&___cells_0, value);
	}

	inline static int32_t get_offset_of_values_1() { return static_cast<int32_t>(offsetof(U3CSequenceU3Ec__AnonStorey110_1_t1717931772, ___values_1)); }
	inline Func_1_t557074727 * get_values_1() const { return ___values_1; }
	inline Func_1_t557074727 ** get_address_of_values_1() { return &___values_1; }
	inline void set_values_1(Func_1_t557074727 * value)
	{
		___values_1 = value;
		Il2CppCodeGenWriteBarrier(&___values_1, value);
	}
};

struct U3CSequenceU3Ec__AnonStorey110_1_t1717931772_StaticFields
{
public:
	// System.Func`2<ICell`1<T>,T> CellReactiveApi/<Sequence>c__AnonStorey110`1::<>f__am$cache2
	Func_2_t2767404563 * ___U3CU3Ef__amU24cache2_2;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_2() { return static_cast<int32_t>(offsetof(U3CSequenceU3Ec__AnonStorey110_1_t1717931772_StaticFields, ___U3CU3Ef__amU24cache2_2)); }
	inline Func_2_t2767404563 * get_U3CU3Ef__amU24cache2_2() const { return ___U3CU3Ef__amU24cache2_2; }
	inline Func_2_t2767404563 ** get_address_of_U3CU3Ef__amU24cache2_2() { return &___U3CU3Ef__amU24cache2_2; }
	inline void set_U3CU3Ef__amU24cache2_2(Func_2_t2767404563 * value)
	{
		___U3CU3Ef__amU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
