﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnonymousCell`1/<OnChanged>c__AnonStoreyFA<System.Int32>
struct U3COnChangedU3Ec__AnonStoreyFA_t3785798141;

#include "codegen/il2cpp-codegen.h"

// System.Void AnonymousCell`1/<OnChanged>c__AnonStoreyFA<System.Int32>::.ctor()
extern "C"  void U3COnChangedU3Ec__AnonStoreyFA__ctor_m740527004_gshared (U3COnChangedU3Ec__AnonStoreyFA_t3785798141 * __this, const MethodInfo* method);
#define U3COnChangedU3Ec__AnonStoreyFA__ctor_m740527004(__this, method) ((  void (*) (U3COnChangedU3Ec__AnonStoreyFA_t3785798141 *, const MethodInfo*))U3COnChangedU3Ec__AnonStoreyFA__ctor_m740527004_gshared)(__this, method)
// System.Void AnonymousCell`1/<OnChanged>c__AnonStoreyFA<System.Int32>::<>m__170(T)
extern "C"  void U3COnChangedU3Ec__AnonStoreyFA_U3CU3Em__170_m2131748567_gshared (U3COnChangedU3Ec__AnonStoreyFA_t3785798141 * __this, int32_t ____0, const MethodInfo* method);
#define U3COnChangedU3Ec__AnonStoreyFA_U3CU3Em__170_m2131748567(__this, ____0, method) ((  void (*) (U3COnChangedU3Ec__AnonStoreyFA_t3785798141 *, int32_t, const MethodInfo*))U3COnChangedU3Ec__AnonStoreyFA_U3CU3Em__170_m2131748567_gshared)(__this, ____0, method)
