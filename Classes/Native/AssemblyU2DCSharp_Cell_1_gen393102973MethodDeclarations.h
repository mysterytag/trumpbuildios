﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Cell`1<System.Boolean>
struct Cell_1_t393102973;
// ICell`1<System.Boolean>
struct ICell_1_t1762636318;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action
struct Action_t437523947;
// System.Object
struct Il2CppObject;
// System.Action`1<System.Boolean>
struct Action_1_t359458046;
// IStream`1<System.Boolean>
struct IStream_1_t763696332;
// UnityEngine.UI.Text
struct Text_t3286458198;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"
#include "UnityEngine_UI_UnityEngine_UI_Text3286458198.h"

// System.Void Cell`1<System.Boolean>::.ctor()
extern "C"  void Cell_1__ctor_m1744551183_gshared (Cell_1_t393102973 * __this, const MethodInfo* method);
#define Cell_1__ctor_m1744551183(__this, method) ((  void (*) (Cell_1_t393102973 *, const MethodInfo*))Cell_1__ctor_m1744551183_gshared)(__this, method)
// System.Void Cell`1<System.Boolean>::.ctor(T)
extern "C"  void Cell_1__ctor_m2541480495_gshared (Cell_1_t393102973 * __this, bool ___initial0, const MethodInfo* method);
#define Cell_1__ctor_m2541480495(__this, ___initial0, method) ((  void (*) (Cell_1_t393102973 *, bool, const MethodInfo*))Cell_1__ctor_m2541480495_gshared)(__this, ___initial0, method)
// System.Void Cell`1<System.Boolean>::.ctor(ICell`1<T>)
extern "C"  void Cell_1__ctor_m1361635077_gshared (Cell_1_t393102973 * __this, Il2CppObject* ___other0, const MethodInfo* method);
#define Cell_1__ctor_m1361635077(__this, ___other0, method) ((  void (*) (Cell_1_t393102973 *, Il2CppObject*, const MethodInfo*))Cell_1__ctor_m1361635077_gshared)(__this, ___other0, method)
// System.Void Cell`1<System.Boolean>::SetInputCell(ICell`1<T>)
extern "C"  void Cell_1_SetInputCell_m4043152341_gshared (Cell_1_t393102973 * __this, Il2CppObject* ___cell0, const MethodInfo* method);
#define Cell_1_SetInputCell_m4043152341(__this, ___cell0, method) ((  void (*) (Cell_1_t393102973 *, Il2CppObject*, const MethodInfo*))Cell_1_SetInputCell_m4043152341_gshared)(__this, ___cell0, method)
// T Cell`1<System.Boolean>::get_value()
extern "C"  bool Cell_1_get_value_m4241254838_gshared (Cell_1_t393102973 * __this, const MethodInfo* method);
#define Cell_1_get_value_m4241254838(__this, method) ((  bool (*) (Cell_1_t393102973 *, const MethodInfo*))Cell_1_get_value_m4241254838_gshared)(__this, method)
// System.Void Cell`1<System.Boolean>::set_value(T)
extern "C"  void Cell_1_set_value_m3532323549_gshared (Cell_1_t393102973 * __this, bool ___value0, const MethodInfo* method);
#define Cell_1_set_value_m3532323549(__this, ___value0, method) ((  void (*) (Cell_1_t393102973 *, bool, const MethodInfo*))Cell_1_set_value_m3532323549_gshared)(__this, ___value0, method)
// System.Void Cell`1<System.Boolean>::Set(T)
extern "C"  void Cell_1_Set_m4026826287_gshared (Cell_1_t393102973 * __this, bool ___val0, const MethodInfo* method);
#define Cell_1_Set_m4026826287(__this, ___val0, method) ((  void (*) (Cell_1_t393102973 *, bool, const MethodInfo*))Cell_1_Set_m4026826287_gshared)(__this, ___val0, method)
// System.IDisposable Cell`1<System.Boolean>::OnChanged(System.Action,Priority)
extern "C"  Il2CppObject * Cell_1_OnChanged_m3436308404_gshared (Cell_1_t393102973 * __this, Action_t437523947 * ___action0, int32_t ___p1, const MethodInfo* method);
#define Cell_1_OnChanged_m3436308404(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (Cell_1_t393102973 *, Action_t437523947 *, int32_t, const MethodInfo*))Cell_1_OnChanged_m3436308404_gshared)(__this, ___action0, ___p1, method)
// System.Object Cell`1<System.Boolean>::get_valueObject()
extern "C"  Il2CppObject * Cell_1_get_valueObject_m1046160671_gshared (Cell_1_t393102973 * __this, const MethodInfo* method);
#define Cell_1_get_valueObject_m1046160671(__this, method) ((  Il2CppObject * (*) (Cell_1_t393102973 *, const MethodInfo*))Cell_1_get_valueObject_m1046160671_gshared)(__this, method)
// System.IDisposable Cell`1<System.Boolean>::ListenUpdates(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Cell_1_ListenUpdates_m1565576101_gshared (Cell_1_t393102973 * __this, Action_1_t359458046 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define Cell_1_ListenUpdates_m1565576101(__this, ___reaction0, ___p1, method) ((  Il2CppObject * (*) (Cell_1_t393102973 *, Action_1_t359458046 *, int32_t, const MethodInfo*))Cell_1_ListenUpdates_m1565576101_gshared)(__this, ___reaction0, ___p1, method)
// IStream`1<T> Cell`1<System.Boolean>::get_updates()
extern "C"  Il2CppObject* Cell_1_get_updates_m762843005_gshared (Cell_1_t393102973 * __this, const MethodInfo* method);
#define Cell_1_get_updates_m762843005(__this, method) ((  Il2CppObject* (*) (Cell_1_t393102973 *, const MethodInfo*))Cell_1_get_updates_m762843005_gshared)(__this, method)
// System.IDisposable Cell`1<System.Boolean>::Bind(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Cell_1_Bind_m2867406077_gshared (Cell_1_t393102973 * __this, Action_1_t359458046 * ___action0, int32_t ___p1, const MethodInfo* method);
#define Cell_1_Bind_m2867406077(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (Cell_1_t393102973 *, Action_1_t359458046 *, int32_t, const MethodInfo*))Cell_1_Bind_m2867406077_gshared)(__this, ___action0, ___p1, method)
// System.Void Cell`1<System.Boolean>::SetInputStream(IStream`1<T>)
extern "C"  void Cell_1_SetInputStream_m979608145_gshared (Cell_1_t393102973 * __this, Il2CppObject* ___stream0, const MethodInfo* method);
#define Cell_1_SetInputStream_m979608145(__this, ___stream0, method) ((  void (*) (Cell_1_t393102973 *, Il2CppObject*, const MethodInfo*))Cell_1_SetInputStream_m979608145_gshared)(__this, ___stream0, method)
// System.Void Cell`1<System.Boolean>::ResetInputStream()
extern "C"  void Cell_1_ResetInputStream_m3591659696_gshared (Cell_1_t393102973 * __this, const MethodInfo* method);
#define Cell_1_ResetInputStream_m3591659696(__this, method) ((  void (*) (Cell_1_t393102973 *, const MethodInfo*))Cell_1_ResetInputStream_m3591659696_gshared)(__this, method)
// System.Void Cell`1<System.Boolean>::TransactionIterationFinished()
extern "C"  void Cell_1_TransactionIterationFinished_m3098048838_gshared (Cell_1_t393102973 * __this, const MethodInfo* method);
#define Cell_1_TransactionIterationFinished_m3098048838(__this, method) ((  void (*) (Cell_1_t393102973 *, const MethodInfo*))Cell_1_TransactionIterationFinished_m3098048838_gshared)(__this, method)
// System.Void Cell`1<System.Boolean>::Unpack(System.Int32)
extern "C"  void Cell_1_Unpack_m4206045144_gshared (Cell_1_t393102973 * __this, int32_t ___p0, const MethodInfo* method);
#define Cell_1_Unpack_m4206045144(__this, ___p0, method) ((  void (*) (Cell_1_t393102973 *, int32_t, const MethodInfo*))Cell_1_Unpack_m4206045144_gshared)(__this, ___p0, method)
// System.Boolean Cell`1<System.Boolean>::op_LessThanOrEqual(Cell`1<T>,UnityEngine.UI.Text)
extern "C"  bool Cell_1_op_LessThanOrEqual_m2487722393_gshared (Il2CppObject * __this /* static, unused */, Cell_1_t393102973 * ___cell0, Text_t3286458198 * ___text1, const MethodInfo* method);
#define Cell_1_op_LessThanOrEqual_m2487722393(__this /* static, unused */, ___cell0, ___text1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Cell_1_t393102973 *, Text_t3286458198 *, const MethodInfo*))Cell_1_op_LessThanOrEqual_m2487722393_gshared)(__this /* static, unused */, ___cell0, ___text1, method)
// System.Boolean Cell`1<System.Boolean>::op_GreaterThanOrEqual(Cell`1<T>,UnityEngine.UI.Text)
extern "C"  bool Cell_1_op_GreaterThanOrEqual_m2159315534_gshared (Il2CppObject * __this /* static, unused */, Cell_1_t393102973 * ___cell0, Text_t3286458198 * ___text1, const MethodInfo* method);
#define Cell_1_op_GreaterThanOrEqual_m2159315534(__this /* static, unused */, ___cell0, ___text1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Cell_1_t393102973 *, Text_t3286458198 *, const MethodInfo*))Cell_1_op_GreaterThanOrEqual_m2159315534_gshared)(__this /* static, unused */, ___cell0, ___text1, method)
