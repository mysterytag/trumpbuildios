﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// FontDef[]
struct FontDefU5BU5D_t616749523;
// AchievementController
struct AchievementController_t3677499403;
// GameController
struct GameController_t2782302542;
// TutorialPlayer
struct TutorialPlayer_t4281139199;
// WindowManager
struct WindowManager_t3316821373;
// OligarchParameters
struct OligarchParameters_t821144443;
// System.Func`2<UnityEngine.Canvas,System.Boolean>
struct Func_2_t2143940396;

#include "AssemblyU2DCSharp_Zenject_MonoInstaller3807575866.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StartSceneInstaller
struct  StartSceneInstaller_t3492553022  : public MonoInstaller_t3807575866
{
public:
	// FontDef[] StartSceneInstaller::CacheFonts
	FontDefU5BU5D_t616749523* ___CacheFonts_4;
	// AchievementController StartSceneInstaller::AchievementController
	AchievementController_t3677499403 * ___AchievementController_5;
	// GameController StartSceneInstaller::GameController
	GameController_t2782302542 * ___GameController_6;
	// TutorialPlayer StartSceneInstaller::TutorialPlayer
	TutorialPlayer_t4281139199 * ___TutorialPlayer_7;
	// WindowManager StartSceneInstaller::WindowManager
	WindowManager_t3316821373 * ___WindowManager_8;
	// OligarchParameters StartSceneInstaller::OligarchParameters
	OligarchParameters_t821144443 * ___OligarchParameters_9;

public:
	inline static int32_t get_offset_of_CacheFonts_4() { return static_cast<int32_t>(offsetof(StartSceneInstaller_t3492553022, ___CacheFonts_4)); }
	inline FontDefU5BU5D_t616749523* get_CacheFonts_4() const { return ___CacheFonts_4; }
	inline FontDefU5BU5D_t616749523** get_address_of_CacheFonts_4() { return &___CacheFonts_4; }
	inline void set_CacheFonts_4(FontDefU5BU5D_t616749523* value)
	{
		___CacheFonts_4 = value;
		Il2CppCodeGenWriteBarrier(&___CacheFonts_4, value);
	}

	inline static int32_t get_offset_of_AchievementController_5() { return static_cast<int32_t>(offsetof(StartSceneInstaller_t3492553022, ___AchievementController_5)); }
	inline AchievementController_t3677499403 * get_AchievementController_5() const { return ___AchievementController_5; }
	inline AchievementController_t3677499403 ** get_address_of_AchievementController_5() { return &___AchievementController_5; }
	inline void set_AchievementController_5(AchievementController_t3677499403 * value)
	{
		___AchievementController_5 = value;
		Il2CppCodeGenWriteBarrier(&___AchievementController_5, value);
	}

	inline static int32_t get_offset_of_GameController_6() { return static_cast<int32_t>(offsetof(StartSceneInstaller_t3492553022, ___GameController_6)); }
	inline GameController_t2782302542 * get_GameController_6() const { return ___GameController_6; }
	inline GameController_t2782302542 ** get_address_of_GameController_6() { return &___GameController_6; }
	inline void set_GameController_6(GameController_t2782302542 * value)
	{
		___GameController_6 = value;
		Il2CppCodeGenWriteBarrier(&___GameController_6, value);
	}

	inline static int32_t get_offset_of_TutorialPlayer_7() { return static_cast<int32_t>(offsetof(StartSceneInstaller_t3492553022, ___TutorialPlayer_7)); }
	inline TutorialPlayer_t4281139199 * get_TutorialPlayer_7() const { return ___TutorialPlayer_7; }
	inline TutorialPlayer_t4281139199 ** get_address_of_TutorialPlayer_7() { return &___TutorialPlayer_7; }
	inline void set_TutorialPlayer_7(TutorialPlayer_t4281139199 * value)
	{
		___TutorialPlayer_7 = value;
		Il2CppCodeGenWriteBarrier(&___TutorialPlayer_7, value);
	}

	inline static int32_t get_offset_of_WindowManager_8() { return static_cast<int32_t>(offsetof(StartSceneInstaller_t3492553022, ___WindowManager_8)); }
	inline WindowManager_t3316821373 * get_WindowManager_8() const { return ___WindowManager_8; }
	inline WindowManager_t3316821373 ** get_address_of_WindowManager_8() { return &___WindowManager_8; }
	inline void set_WindowManager_8(WindowManager_t3316821373 * value)
	{
		___WindowManager_8 = value;
		Il2CppCodeGenWriteBarrier(&___WindowManager_8, value);
	}

	inline static int32_t get_offset_of_OligarchParameters_9() { return static_cast<int32_t>(offsetof(StartSceneInstaller_t3492553022, ___OligarchParameters_9)); }
	inline OligarchParameters_t821144443 * get_OligarchParameters_9() const { return ___OligarchParameters_9; }
	inline OligarchParameters_t821144443 ** get_address_of_OligarchParameters_9() { return &___OligarchParameters_9; }
	inline void set_OligarchParameters_9(OligarchParameters_t821144443 * value)
	{
		___OligarchParameters_9 = value;
		Il2CppCodeGenWriteBarrier(&___OligarchParameters_9, value);
	}
};

struct StartSceneInstaller_t3492553022_StaticFields
{
public:
	// System.String StartSceneInstaller::kPrecacheFontGlyphsString
	String_t* ___kPrecacheFontGlyphsString_3;
	// System.Func`2<UnityEngine.Canvas,System.Boolean> StartSceneInstaller::<>f__am$cache7
	Func_2_t2143940396 * ___U3CU3Ef__amU24cache7_10;

public:
	inline static int32_t get_offset_of_kPrecacheFontGlyphsString_3() { return static_cast<int32_t>(offsetof(StartSceneInstaller_t3492553022_StaticFields, ___kPrecacheFontGlyphsString_3)); }
	inline String_t* get_kPrecacheFontGlyphsString_3() const { return ___kPrecacheFontGlyphsString_3; }
	inline String_t** get_address_of_kPrecacheFontGlyphsString_3() { return &___kPrecacheFontGlyphsString_3; }
	inline void set_kPrecacheFontGlyphsString_3(String_t* value)
	{
		___kPrecacheFontGlyphsString_3 = value;
		Il2CppCodeGenWriteBarrier(&___kPrecacheFontGlyphsString_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_10() { return static_cast<int32_t>(offsetof(StartSceneInstaller_t3492553022_StaticFields, ___U3CU3Ef__amU24cache7_10)); }
	inline Func_2_t2143940396 * get_U3CU3Ef__amU24cache7_10() const { return ___U3CU3Ef__amU24cache7_10; }
	inline Func_2_t2143940396 ** get_address_of_U3CU3Ef__amU24cache7_10() { return &___U3CU3Ef__amU24cache7_10; }
	inline void set_U3CU3Ef__amU24cache7_10(Func_2_t2143940396 * value)
	{
		___U3CU3Ef__amU24cache7_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
