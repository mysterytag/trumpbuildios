﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// Mono.Xml.DictionaryBase
struct DictionaryBase_t4261756523;
// System.Collections.Generic.IEnumerable`1<Mono.Xml.DTDNode>
struct IEnumerable_1_t310611086;
// Mono.Xml.DictionaryBase/<>c__Iterator3
struct U3CU3Ec__Iterator3_t2516598212;
// Mono.Xml.DTDNode
struct DTDNode_t1733424026;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<Mono.Xml.DTDNode>
struct IEnumerator_1_t3216530474;
// Mono.Xml.DTDAttListDeclaration
struct DTDAttListDeclaration_t1258293171;
// Mono.Xml.DTDObjectModel
struct DTDObjectModel_t709926554;
// System.String
struct String_t;
// Mono.Xml.DTDAttributeDefinition
struct DTDAttributeDefinition_t1053410431;
// System.Collections.IList
struct IList_t1612618265;
// Mono.Xml.DTDAttListDeclarationCollection
struct DTDAttListDeclarationCollection_t733492145;
// System.Xml.Schema.XmlSchemaDatatype
struct XmlSchemaDatatype_t2590121;
// Mono.Xml.DTDAutomataFactory
struct DTDAutomataFactory_t2001760042;
// Mono.Xml.DTDCollectionBase
struct DTDCollectionBase_t3975886631;
// Mono.Xml.DTDContentModel
struct DTDContentModel_t1462496200;
// Mono.Xml.DTDContentModelCollection
struct DTDContentModelCollection_t605667718;
// Mono.Xml.DTDElementDeclaration
struct DTDElementDeclaration_t95357814;
// Mono.Xml.DTDElementDeclarationCollection
struct DTDElementDeclarationCollection_t2951978932;
// Mono.Xml.DTDEntityBase
struct DTDEntityBase_t1395379180;
// System.Xml.XmlResolver
struct XmlResolver_t2502213349;
// Mono.Xml.DTDEntityDeclaration
struct DTDEntityDeclaration_t2806387719;
// System.Collections.ArrayList
struct ArrayList_t2121638921;
// Mono.Xml.DTDEntityDeclarationCollection
struct DTDEntityDeclarationCollection_t4226410245;
// System.Xml.XmlException
struct XmlException_t3490696160;
// Mono.Xml.DTDNotationDeclaration
struct DTDNotationDeclaration_t353707912;
// Mono.Xml.DTDNotationDeclarationCollection
struct DTDNotationDeclarationCollection_t3011202374;
// System.Xml.XmlNameTable
struct XmlNameTable_t3232213908;
// System.Collections.Hashtable
struct Hashtable_t3875263730;
// Mono.Xml.DTDParameterEntityDeclarationCollection
struct DTDParameterEntityDeclarationCollection_t457236420;
// Mono.Xml2.XmlTextReader
struct XmlTextReader_t3066586409;
// System.Xml.XmlParserContext
struct XmlParserContext_t3629084577;
// Mono.Xml.DTDParameterEntityDeclaration
struct DTDParameterEntityDeclaration_t2256150406;
// Mono.Xml.Schema.XdtAnyAtomicType
struct XdtAnyAtomicType_t941707450;
// Mono.Xml.Schema.XdtDayTimeDuration
struct XdtDayTimeDuration_t2689613030;
// Mono.Xml.Schema.XdtUntypedAtomic
struct XdtUntypedAtomic_t3803926693;
// Mono.Xml.Schema.XdtYearMonthDuration
struct XdtYearMonthDuration_t3115315360;
// Mono.Xml.Schema.XsdAnySimpleType
struct XsdAnySimpleType_t8663200;
// Mono.Xml.Schema.XsdAnyURI
struct XsdAnyURI_t1070214554;
// Mono.Xml.Schema.XsdBase64Binary
struct XsdBase64Binary_t3314116906;
// Mono.Xml.Schema.XsdBoolean
struct XsdBoolean_t235052784;
// Mono.Xml.Schema.XsdByte
struct XsdByte_t2013201666;
// Mono.Xml.Schema.XsdDate
struct XsdDate_t2013238184;
// Mono.Xml.Schema.XsdDateTime
struct XsdDateTime_t3288893205;
// Mono.Xml.Schema.XsdDecimal
struct XsdDecimal_t1712604697;
// Mono.Xml.Schema.XsdDouble
struct XsdDouble_t1156919691;
// Mono.Xml.Schema.XsdDuration
struct XsdDuration_t3799098638;
// Mono.Xml.Schema.XsdEntities
struct XsdEntities_t3688996667;
// Mono.Xml.Schema.XsdEntity
struct XsdEntity_t1184602525;
// Mono.Xml.Schema.XsdFloat
struct XsdFloat_t2793549540;
// Mono.Xml.Schema.XsdGDay
struct XsdGDay_t2013299119;
// Mono.Xml.Schema.XsdGMonth
struct XsdGMonth_t1211240467;
// Mono.Xml.Schema.XsdGMonthDay
struct XsdGMonthDay_t2202709899;
// Mono.Xml.Schema.XsdGYear
struct XsdGYear_t2793897420;
// Mono.Xml.Schema.XsdGYearMonth
struct XsdGYearMonth_t878671062;
// Mono.Xml.Schema.XsdHexBinary
struct XsdHexBinary_t1695962116;
// Mono.Xml.Schema.XsdID
struct XsdID_t2215259957;
// Mono.Xml.Schema.XsdIDRef
struct XsdIDRef_t2795100704;
// Mono.Xml.Schema.XsdIDRefs
struct XsdIDRefs_t1259314069;
// Mono.Xml.Schema.XsdInt
struct XsdInt_t464121399;
// Mono.Xml.Schema.XsdInteger
struct XsdInteger_t2128393222;
// Mono.Xml.Schema.XsdLanguage
struct XsdLanguage_t4177521362;
// Mono.Xml.Schema.XsdLong
struct XsdLong_t2013489782;
// Mono.Xml.Schema.XsdName
struct XsdName_t2013535877;
// Mono.Xml.Schema.XsdNCName
struct XsdNCName_t1401413498;
// Mono.Xml.Schema.XsdNegativeInteger
struct XsdNegativeInteger_t2648572689;
// Mono.Xml.Schema.XsdNMToken
struct XsdNMToken_t1296931426;
// Mono.Xml.Schema.XsdNMTokens
struct XsdNMTokens_t2060706707;
// Mono.Xml.Schema.XsdNonNegativeInteger
struct XsdNonNegativeInteger_t906772726;
// Mono.Xml.Schema.XsdNonPositiveInteger
struct XsdNonPositiveInteger_t673083058;
// Mono.Xml.Schema.XsdNormalizedString
struct XsdNormalizedString_t2753230946;
// Mono.Xml.Schema.XsdNotation
struct XsdNotation_t3074536316;
// Mono.Xml.Schema.XsdPositiveInteger
struct XsdPositiveInteger_t2414883021;
// Mono.Xml.Schema.XsdQName
struct XsdQName_t2802801444;
// Mono.Xml.Schema.XsdShort
struct XsdShort_t2805436676;
// Mono.Xml.Schema.XsdString
struct XsdString_t1590891979;
// Mono.Xml.Schema.XsdTime
struct XsdTime_t2013722311;
// Mono.Xml.Schema.XsdToken
struct XsdToken_t2806564481;
// Mono.Xml.Schema.XsdUnsignedByte
struct XsdUnsignedByte_t1376388247;
// Mono.Xml.Schema.XsdUnsignedInt
struct XsdUnsignedInt_t4045809666;
// Mono.Xml.Schema.XsdUnsignedLong
struct XsdUnsignedLong_t1376676363;
// Mono.Xml.Schema.XsdUnsignedShort
struct XsdUnsignedShort_t244089871;
// Mono.Xml.XPath.Tokenizer
struct Tokenizer_t3551440624;
// Mono.Xml.XPath.XmlDocumentEditableNavigator
struct XmlDocumentEditableNavigator_t1066973242;
// Mono.Xml.XPath.XPathEditableDocument
struct XPathEditableDocument_t469941845;
// System.Xml.XPath.XPathNavigator
struct XPathNavigator_t1624538935;
// System.Xml.XmlNode
struct XmlNode_t3592213601;
// Mono.Xml.XPath.XPathParser
struct XPathParser_t618877717;
// System.Xml.Xsl.IStaticXsltContext
struct IStaticXsltContext_t2347050862;
// System.Xml.XPath.Expression
struct Expression_t4217024437;
// System.Xml.XPath.NodeSet
struct NodeSet_t3503134685;
// System.Xml.XPath.NodeTest
struct NodeTest_t911751889;
// Mono.Xml.XPath.yyParser.yyInput
struct yyInput_t1890241104;
// Mono.Xml.XPath.yyParser.yyException
struct yyException_t1136059093;
// Mono.Xml.XPath.yyParser.yyUnexpectedEof
struct yyUnexpectedEof_t2997709169;
// System.IO.TextReader
struct TextReader_t1534522647;
// System.IO.Stream
struct Stream_t219029575;
// System.Xml.XmlReaderSettings
struct XmlReaderSettings_t3693321125;
// System.Char[]
struct CharU5BU5D_t3416858730;
// Mono.Xml2.XmlTextReader/XmlTokenInfo
struct XmlTokenInfo_t2571680784;
// Mono.Xml2.XmlTextReader/DtdInputStateStack
struct DtdInputStateStack_t1263389581;
// Mono.Xml2.XmlTextReader/XmlAttributeTokenInfo
struct XmlAttributeTokenInfo_t922105698;
// System.MonoTODOAttribute
struct MonoTODOAttribute_t1287393899;
// System.Xml.DTDReader
struct DTDReader_t4257441119;
// System.Xml.MonoFIXAttribute
struct MonoFIXAttribute_t792940146;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "System_Xml_U3CModuleU3E86524790.h"
#include "System_Xml_U3CModuleU3E86524790MethodDeclarations.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E3053238933.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E3053238933MethodDeclarations.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24Ar214874486.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24Ar214874486MethodDeclarations.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24Ar335952622.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24Ar335952622MethodDeclarations.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24Ar335955352.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24Ar335955352MethodDeclarations.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A2366142725.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A2366142725MethodDeclarations.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A2366142816.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A2366142816MethodDeclarations.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A2366142878.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A2366142878MethodDeclarations.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24Ar214874672.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24Ar214874672MethodDeclarations.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A2777878083.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A2777878083MethodDeclarations.h"
#include "System_Xml_Mono_Xml_DictionaryBase4261756523.h"
#include "System_Xml_Mono_Xml_DictionaryBase4261756523MethodDeclarations.h"
#include "mscorlib_System_Void2779279689.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3656612197MethodDeclarations.h"
#include "System_Xml_Mono_Xml_DictionaryBase_U3CU3Ec__Iterat2516598212MethodDeclarations.h"
#include "System_Xml_Mono_Xml_DictionaryBase_U3CU3Ec__Iterat2516598212.h"
#include "mscorlib_System_Int322847414787.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "System_Xml_Mono_Xml_DTDNode1733424026.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Threading_Interlocked3007803305MethodDeclarations.h"
#include "mscorlib_System_Boolean211005341.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1742395189MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22859653228MethodDeclarations.h"
#include "mscorlib_System_UInt32985925326.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1742395189.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22859653228.h"
#include "mscorlib_System_NotSupportedException1374155497MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1374155497.h"
#include "System_Xml_Mono_Xml_DTDAttListDeclaration1258293171.h"
#include "System_Xml_Mono_Xml_DTDAttListDeclaration1258293171MethodDeclarations.h"
#include "System_Xml_Mono_Xml_DTDObjectModel709926554.h"
#include "mscorlib_System_Collections_Hashtable3875263730MethodDeclarations.h"
#include "mscorlib_System_Collections_ArrayList2121638921MethodDeclarations.h"
#include "System_Xml_Mono_Xml_DTDNode1733424026MethodDeclarations.h"
#include "mscorlib_System_Collections_Hashtable3875263730.h"
#include "mscorlib_System_Collections_ArrayList2121638921.h"
#include "mscorlib_System_String968488902.h"
#include "System_Xml_Mono_Xml_DTDAttributeDefinition1053410431.h"
#include "System_Xml_Mono_Xml_DTDAttributeDefinition1053410431MethodDeclarations.h"
#include "mscorlib_System_String968488902MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException2420574324MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException2420574324.h"
#include "System_Xml_Mono_Xml_DTDAttListDeclarationCollection733492145.h"
#include "System_Xml_Mono_Xml_DTDAttListDeclarationCollection733492145MethodDeclarations.h"
#include "System_Xml_Mono_Xml_DTDCollectionBase3975886631MethodDeclarations.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaDatatype2590121.h"
#include "mscorlib_System_Text_StringBuilder3822575854MethodDeclarations.h"
#include "mscorlib_System_Globalization_CultureInfo3603717042MethodDeclarations.h"
#include "mscorlib_System_Int322847414787MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlChar3591879093MethodDeclarations.h"
#include "System_Xml_Mono_Xml_DTDObjectModel709926554MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder3822575854.h"
#include "mscorlib_System_Char2778706699.h"
#include "mscorlib_System_Globalization_NumberStyles3988678145.h"
#include "mscorlib_System_Globalization_CultureInfo3603717042.h"
#include "System_Xml_Mono_Xml_DTDAutomataFactory2001760042.h"
#include "System_Xml_Mono_Xml_DTDAutomataFactory2001760042MethodDeclarations.h"
#include "System_Xml_Mono_Xml_DTDCollectionBase3975886631.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3656612197.h"
#include "System_Xml_Mono_Xml_DTDContentModel1462496200.h"
#include "System_Xml_Mono_Xml_DTDContentModel1462496200MethodDeclarations.h"
#include "System_Xml_Mono_Xml_DTDContentModelCollection605667718MethodDeclarations.h"
#include "System_Xml_Mono_Xml_DTDContentModelCollection605667718.h"
#include "System_Xml_Mono_Xml_DTDOccurence3238327079.h"
#include "System_Xml_Mono_Xml_DTDContentOrderType2313699015.h"
#include "System_Xml_Mono_Xml_DTDContentOrderType2313699015MethodDeclarations.h"
#include "System_Xml_Mono_Xml_DTDElementDeclaration95357814.h"
#include "System_Xml_Mono_Xml_DTDElementDeclaration95357814MethodDeclarations.h"
#include "System_Xml_Mono_Xml_DTDElementDeclarationCollectio2951978932.h"
#include "System_Xml_Mono_Xml_DTDElementDeclarationCollectio2951978932MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlException3490696160MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlException3490696160.h"
#include "mscorlib_System_Exception1967233988.h"
#include "System_Xml_Mono_Xml_DTDEntityBase1395379180.h"
#include "System_Xml_Mono_Xml_DTDEntityBase1395379180MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlResolver2502213349.h"
#include "System_System_Uri2776692961MethodDeclarations.h"
#include "System_System_Uri2776692961.h"
#include "System_System_UriFormatException1145000641.h"
#include "System_Xml_System_Xml_XmlResolver2502213349MethodDeclarations.h"
#include "mscorlib_System_Type2779229935MethodDeclarations.h"
#include "System_Xml_Mono_Xml2_XmlTextReader3066586409MethodDeclarations.h"
#include "mscorlib_System_IO_Stream219029575.h"
#include "System_Xml_Mono_Xml2_XmlTextReader3066586409.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_System_RuntimeTypeHandle1864875887.h"
#include "System_Xml_System_Xml_XmlNameTable3232213908.h"
#include "mscorlib_System_IO_TextReader1534522647.h"
#include "mscorlib_System_IO_TextReader1534522647MethodDeclarations.h"
#include "mscorlib_System_IO_Stream219029575MethodDeclarations.h"
#include "System_Xml_Mono_Xml_DTDEntityDeclaration2806387719.h"
#include "System_Xml_Mono_Xml_DTDEntityDeclaration2806387719MethodDeclarations.h"
#include "System_Xml_Mono_Xml_DTDEntityDeclarationCollection4226410245MethodDeclarations.h"
#include "System_Xml_Mono_Xml_DTDEntityDeclarationCollection4226410245.h"
#include "System_Xml_Mono_Xml_DTDNotationDeclaration353707912.h"
#include "System_Xml_Mono_Xml_DTDNotationDeclaration353707912MethodDeclarations.h"
#include "System_Xml_Mono_Xml_DTDNotationDeclarationCollecti3011202374.h"
#include "System_Xml_Mono_Xml_DTDNotationDeclarationCollecti3011202374MethodDeclarations.h"
#include "System_Xml_Mono_Xml_DTDParameterEntityDeclarationCo457236420MethodDeclarations.h"
#include "System_Xml_Mono_Xml_DTDParameterEntityDeclarationCo457236420.h"
#include "System_Xml_System_Xml_XmlParserContext3629084577.h"
#include "System_Xml_System_Xml_XmlNodeType3966624571.h"
#include "System_Xml_Mono_Xml_DTDOccurence3238327079MethodDeclarations.h"
#include "System_Xml_Mono_Xml_DTDParameterEntityDeclaration2256150406.h"
#include "System_Xml_Mono_Xml_DTDParameterEntityDeclaration2256150406MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XdtAnyAtomicType941707450.h"
#include "System_Xml_Mono_Xml_Schema_XdtAnyAtomicType941707450MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdAnySimpleType8663200MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XdtDayTimeDuration2689613030.h"
#include "System_Xml_Mono_Xml_Schema_XdtDayTimeDuration2689613030MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdDuration3799098638MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XdtUntypedAtomic3803926693.h"
#include "System_Xml_Mono_Xml_Schema_XdtUntypedAtomic3803926693MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XdtYearMonthDuration3115315360.h"
#include "System_Xml_Mono_Xml_Schema_XdtYearMonthDuration3115315360MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdAnySimpleType8663200.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaDatatype2590121MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaFacet_Facet67634583.h"
#include "System_Xml_System_Xml_XmlTokenizedType1796599028.h"
#include "System_Xml_Mono_Xml_Schema_XsdAnyURI1070214554.h"
#include "System_Xml_Mono_Xml_Schema_XsdAnyURI1070214554MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdString1590891979MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdBase64Binary3314116906.h"
#include "System_Xml_Mono_Xml_Schema_XsdBase64Binary3314116906MethodDeclarations.h"
#include "mscorlib_System_Byte2778693821.h"
#include "System_Xml_Mono_Xml_Schema_XsdBoolean235052784.h"
#include "System_Xml_Mono_Xml_Schema_XsdBoolean235052784MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdWhitespaceFacet3778568450.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaUtil3432835015.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaUtil3432835015MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdByte2013201666.h"
#include "System_Xml_Mono_Xml_Schema_XsdByte2013201666MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdShort2805436676MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdDate2013238184.h"
#include "System_Xml_Mono_Xml_Schema_XsdDate2013238184MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdDateTime3288893205.h"
#include "System_Xml_Mono_Xml_Schema_XsdDateTime3288893205MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdDecimal1712604697.h"
#include "System_Xml_Mono_Xml_Schema_XsdDecimal1712604697MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdDouble1156919691.h"
#include "System_Xml_Mono_Xml_Schema_XsdDouble1156919691MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdDuration3799098638.h"
#include "System_Xml_Mono_Xml_Schema_XsdEntities3688996667.h"
#include "System_Xml_Mono_Xml_Schema_XsdEntities3688996667MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdName2013535877MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdEntity1184602525.h"
#include "System_Xml_Mono_Xml_Schema_XsdEntity1184602525MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdFloat2793549540.h"
#include "System_Xml_Mono_Xml_Schema_XsdFloat2793549540MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdGDay2013299119.h"
#include "System_Xml_Mono_Xml_Schema_XsdGDay2013299119MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdGMonth1211240467.h"
#include "System_Xml_Mono_Xml_Schema_XsdGMonth1211240467MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdGMonthDay2202709899.h"
#include "System_Xml_Mono_Xml_Schema_XsdGMonthDay2202709899MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdGYear2793897420.h"
#include "System_Xml_Mono_Xml_Schema_XsdGYear2793897420MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdGYearMonth878671062.h"
#include "System_Xml_Mono_Xml_Schema_XsdGYearMonth878671062MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdHexBinary1695962116.h"
#include "System_Xml_Mono_Xml_Schema_XsdHexBinary1695962116MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdID2215259957.h"
#include "System_Xml_Mono_Xml_Schema_XsdID2215259957MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdIDRef2795100704.h"
#include "System_Xml_Mono_Xml_Schema_XsdIDRef2795100704MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdIDRefs1259314069.h"
#include "System_Xml_Mono_Xml_Schema_XsdIDRefs1259314069MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdInt464121399.h"
#include "System_Xml_Mono_Xml_Schema_XsdInt464121399MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdLong2013489782MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdInteger2128393222.h"
#include "System_Xml_Mono_Xml_Schema_XsdInteger2128393222MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdLanguage4177521362.h"
#include "System_Xml_Mono_Xml_Schema_XsdLanguage4177521362MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdToken2806564481MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdLong2013489782.h"
#include "System_Xml_Mono_Xml_Schema_XsdName2013535877.h"
#include "System_Xml_Mono_Xml_Schema_XsdNCName1401413498.h"
#include "System_Xml_Mono_Xml_Schema_XsdNCName1401413498MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdNegativeInteger2648572689.h"
#include "System_Xml_Mono_Xml_Schema_XsdNegativeInteger2648572689MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdNonPositiveInteger673083058MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdNMToken1296931426.h"
#include "System_Xml_Mono_Xml_Schema_XsdNMToken1296931426MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdNMTokens2060706707.h"
#include "System_Xml_Mono_Xml_Schema_XsdNMTokens2060706707MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdNonNegativeInteger906772726.h"
#include "System_Xml_Mono_Xml_Schema_XsdNonNegativeInteger906772726MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdNonPositiveInteger673083058.h"
#include "System_Xml_Mono_Xml_Schema_XsdNormalizedString2753230946.h"
#include "System_Xml_Mono_Xml_Schema_XsdNormalizedString2753230946MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdNotation3074536316.h"
#include "System_Xml_Mono_Xml_Schema_XsdNotation3074536316MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdPositiveInteger2414883021.h"
#include "System_Xml_Mono_Xml_Schema_XsdPositiveInteger2414883021MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdQName2802801444.h"
#include "System_Xml_Mono_Xml_Schema_XsdQName2802801444MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdShort2805436676.h"
#include "System_Xml_Mono_Xml_Schema_XsdString1590891979.h"
#include "System_Xml_Mono_Xml_Schema_XsdTime2013722311.h"
#include "System_Xml_Mono_Xml_Schema_XsdTime2013722311MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdToken2806564481.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedByte1376388247.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedByte1376388247MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedShort244089871MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedInt4045809666.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedInt4045809666MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedLong1376676363MethodDeclarations.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedLong1376676363.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedShort244089871.h"
#include "System_Xml_Mono_Xml_Schema_XsdWhitespaceFacet3778568450MethodDeclarations.h"
#include "System_Xml_Mono_Xml_XPath_Tokenizer3551440624.h"
#include "System_Xml_Mono_Xml_XPath_Tokenizer3551440624MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathException2353341743MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathException2353341743.h"
#include "mscorlib_System_Globalization_NumberFormatInfo3411951076MethodDeclarations.h"
#include "mscorlib_System_Double534516614MethodDeclarations.h"
#include "mscorlib_System_Globalization_NumberFormatInfo3411951076.h"
#include "mscorlib_System_Double534516614.h"
#include "mscorlib_System_Char2778706699MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlQualifiedName176365656MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlQualifiedName176365656.h"
#include "System_Xml_Mono_Xml_XPath_XmlDocumentEditableNavig1066973242.h"
#include "System_Xml_Mono_Xml_XPath_XmlDocumentEditableNavig1066973242MethodDeclarations.h"
#include "System_Xml_Mono_Xml_XPath_XPathEditableDocument469941845.h"
#include "System_Xml_System_Xml_XPath_XPathNavigator1624538935MethodDeclarations.h"
#include "System_Xml_Mono_Xml_XPath_XPathEditableDocument469941845MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlDocumentNavigator569158157MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlNode3592213601.h"
#include "System_Xml_System_Xml_XmlDocumentNavigator569158157.h"
#include "System_Xml_System_Xml_XPath_XPathNavigator1624538935.h"
#include "mscorlib_System_Reflection_Assembly1882292308.h"
#include "System_Xml_System_Xml_XPath_XPathNodeType4070737174.h"
#include "System_Xml_System_Xml_XPath_XPathItem880576077MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_XPathItem880576077.h"
#include "System_Xml_System_Xml_XPath_XPathNamespaceScope2865249459.h"
#include "System_Xml_System_Xml_XmlNodeOrder2460046845.h"
#include "System_Xml_Mono_Xml_XPath_XPathParser618877717.h"
#include "System_Xml_Mono_Xml_XPath_XPathParser618877717MethodDeclarations.h"
#include "mscorlib_System_Console1097803980MethodDeclarations.h"
#include "mscorlib_System_IO_TextWriter1689927879.h"
#include "mscorlib_System_IO_TextWriter1689927879MethodDeclarations.h"
#include "mscorlib_System_Runtime_CompilerServices_RuntimeHe1695827251MethodDeclarations.h"
#include "mscorlib_System_Int162847414729.h"
#include "mscorlib_System_RuntimeFieldHandle3184214143.h"
#include "System_Xml_System_Xml_XPath_Expression4217024437.h"
#include "System_Xml_System_Xml_XPath_Axes4021066818.h"
#include "System_Xml_System_Xml_XPath_NodeSet3503134685.h"
#include "System_Xml_System_Xml_XPath_ExprFilter3320087274MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_NodeTest911751889.h"
#include "System_Xml_System_Xml_XPath_ExprFilter3320087274.h"
#include "System_Xml_System_Xml_XPath_NodeTypeTest2651955243MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_NodeNameTest873888508MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_NodeTypeTest2651955243.h"
#include "System_Xml_System_Xml_XPath_NodeNameTest873888508.h"
#include "System_Xml_Mono_Xml_XPath_yyParser_yyUnexpectedEof2997709169MethodDeclarations.h"
#include "System_Xml_Mono_Xml_XPath_yyParser_yyException1136059093MethodDeclarations.h"
#include "System_Xml_Mono_Xml_XPath_XPathParser_YYRules10764727MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprUNION2208364215MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprRoot2159432084MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprSLASH2206450021MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprSLASH23664170439MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprLiteral569227735MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_FunctionArguments3402201403MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprFunctionCall3745907208MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprOR1813727157MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprAND79682687MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprEQ1813726846MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprNE1813727113MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprLT1813727066MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprGT1813726911MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprLE1813727051MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprGE1813726896MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprPLUS2159338028MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprMINUS2200832088MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprMULT2159257026MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprDIV79685433MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprMOD79694250MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprNEG79694904MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprVariable1838989486MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprParens3599155227MethodDeclarations.h"
#include "System_Xml_System_Xml_XPath_ExprNumber3560215227MethodDeclarations.h"
#include "mscorlib_System_Array2840145358MethodDeclarations.h"
#include "System_Xml_Mono_Xml_XPath_yyParser_yyUnexpectedEof2997709169.h"
#include "System_Xml_Mono_Xml_XPath_yyParser_yyException1136059093.h"
#include "System_Xml_System_Xml_XPath_ExprUNION2208364215.h"
#include "System_Xml_System_Xml_XPath_ExprRoot2159432084.h"
#include "System_Xml_System_Xml_XPath_ExprSLASH2206450021.h"
#include "System_Xml_System_Xml_XPath_ExprSLASH23664170439.h"
#include "System_Xml_System_Xml_XPath_ExprLiteral569227735.h"
#include "System_Xml_System_Xml_XPath_FunctionArguments3402201403.h"
#include "System_Xml_System_Xml_XPath_ExprOR1813727157.h"
#include "System_Xml_System_Xml_XPath_ExprAND79682687.h"
#include "System_Xml_System_Xml_XPath_ExprEQ1813726846.h"
#include "System_Xml_System_Xml_XPath_ExprNE1813727113.h"
#include "System_Xml_System_Xml_XPath_ExprLT1813727066.h"
#include "System_Xml_System_Xml_XPath_ExprGT1813726911.h"
#include "System_Xml_System_Xml_XPath_ExprLE1813727051.h"
#include "System_Xml_System_Xml_XPath_ExprGE1813726896.h"
#include "System_Xml_System_Xml_XPath_ExprPLUS2159338028.h"
#include "System_Xml_System_Xml_XPath_ExprMINUS2200832088.h"
#include "System_Xml_System_Xml_XPath_ExprMULT2159257026.h"
#include "System_Xml_System_Xml_XPath_ExprDIV79685433.h"
#include "System_Xml_System_Xml_XPath_ExprMOD79694250.h"
#include "System_Xml_System_Xml_XPath_ExprNEG79694904.h"
#include "System_Xml_System_Xml_XPath_ExprVariable1838989486.h"
#include "System_Xml_System_Xml_XPath_ExprParens3599155227.h"
#include "System_Xml_System_Xml_XPath_ExprNumber3560215227.h"
#include "System_Xml_Mono_Xml_XPath_XPathParser_YYRules10764727.h"
#include "mscorlib_System_Exception1967233988MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlParserContext3629084577MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlStreamReader1190433730MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlStreamReader1190433730.h"
#include "mscorlib_System_IO_StringReader2229325051MethodDeclarations.h"
#include "mscorlib_System_IO_StringReader2229325051.h"
#include "System_Xml_System_Xml_XmlUrlResolver215921638MethodDeclarations.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_DtdInputStateSt1263389581MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlReader4229084514MethodDeclarations.h"
#include "System.Xml_ArrayTypes.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_XmlAttributeToke922105698.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_XmlTokenInfo2571680784.h"
#include "System_Xml_System_Xml_XmlUrlResolver215921638.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_DtdInputStateSt1263389581.h"
#include "System_Xml_System_Xml_EntityHandling2614841296.h"
#include "System_Xml_System_Xml_ReadState457987651.h"
#include "System_Xml_System_Xml_XmlReaderSettings3693321125.h"
#include "System_Xml_System_Xml_XmlReader4229084514.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_XmlTokenInfo2571680784MethodDeclarations.h"
#include "System_Xml_System_Xml_WhitespaceHandling3134486634.h"
#include "System_Xml_System_Xml_XmlSpace3747690775.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_XmlAttributeToke922105698MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlNamespaceManager1861067185MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlNamespaceManager1861067185.h"
#include "mscorlib_System_ArgumentOutOfRangeException3479058991MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException3479058991.h"
#include "System_Xml_System_Xml_XmlReaderBinarySupport2724083932MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlReaderBinarySupport2724083932.h"
#include "System_Xml_System_Xml_XmlReaderBinarySupport_CharG2496392225MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlReaderSettings3693321125MethodDeclarations.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_TagName115468581.h"
#include "System_Xml_System_Xml_XmlReaderBinarySupport_CharG2496392225.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "System_Xml_System_Xml_NameTable2805661099MethodDeclarations.h"
#include "System_Xml_System_Xml_NameTable2805661099.h"
#include "System_System_UriKind4268040853.h"
#include "System_Xml_System_Xml_ConformanceLevel2571527895.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge190145395MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge190145395.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_TagName115468581MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlNameTable3232213908MethodDeclarations.h"
#include "System_Xml_System_Xml_NonBlockingStreamReader19630413MethodDeclarations.h"
#include "mscorlib_System_Text_Encoding180559927MethodDeclarations.h"
#include "mscorlib_System_Text_Encoding180559927.h"
#include "System_Xml_System_Xml_DTDReader4257441119MethodDeclarations.h"
#include "System_Xml_System_Xml_DTDReader4257441119.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_DtdInputState3379700667.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_DtdInputState3379700667MethodDeclarations.h"
#include "mscorlib_System_Collections_Stack1623036922MethodDeclarations.h"
#include "mscorlib_System_Collections_Stack1623036922.h"
#include "System_Xml_System_MonoTODOAttribute1287393896.h"
#include "System_Xml_System_MonoTODOAttribute1287393896MethodDeclarations.h"
#include "mscorlib_System_Attribute498693649MethodDeclarations.h"
#include "System_Xml_System_Xml_ConformanceLevel2571527895MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlParserInput3311178812MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlParserInput3311178812.h"
#include "System_Xml_System_Xml_XmlTextReader3719122287MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlTextReader3719122287.h"
#include "mscorlib_System_FormatException2404802957.h"
#include "mscorlib_System_NotImplementedException1091014741MethodDeclarations.h"
#include "mscorlib_System_NotImplementedException1091014741.h"
#include "mscorlib_System_Globalization_CompareInfo4023832425.h"
#include "mscorlib_System_Globalization_CompareInfo4023832425MethodDeclarations.h"
#include "mscorlib_System_Globalization_CompareOptions1115053679.h"
#include "mscorlib_System_IO_MemoryStream2881531048MethodDeclarations.h"
#include "mscorlib_System_IO_MemoryStream2881531048.h"
#include "mscorlib_System_Int642847414882.h"
#include "System_Xml_System_Xml_EntityHandling2614841296MethodDeclarations.h"
#include "System_Xml_System_Xml_MonoFIXAttribute792940146.h"
#include "System_Xml_System_Xml_MonoFIXAttribute792940146MethodDeclarations.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$12
extern "C" void U24ArrayTypeU2412_t214874489_marshal_pinvoke(const U24ArrayTypeU2412_t214874489& unmarshaled, U24ArrayTypeU2412_t214874489_marshaled_pinvoke& marshaled)
{
}
extern "C" void U24ArrayTypeU2412_t214874489_marshal_pinvoke_back(const U24ArrayTypeU2412_t214874489_marshaled_pinvoke& marshaled, U24ArrayTypeU2412_t214874489& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$12
extern "C" void U24ArrayTypeU2412_t214874489_marshal_pinvoke_cleanup(U24ArrayTypeU2412_t214874489_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$12
extern "C" void U24ArrayTypeU2412_t214874489_marshal_com(const U24ArrayTypeU2412_t214874489& unmarshaled, U24ArrayTypeU2412_t214874489_marshaled_com& marshaled)
{
}
extern "C" void U24ArrayTypeU2412_t214874489_marshal_com_back(const U24ArrayTypeU2412_t214874489_marshaled_com& marshaled, U24ArrayTypeU2412_t214874489& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$12
extern "C" void U24ArrayTypeU2412_t214874489_marshal_com_cleanup(U24ArrayTypeU2412_t214874489_marshaled_com& marshaled)
{
}
// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$1280
extern "C" void U24ArrayTypeU241280_t335952622_marshal_pinvoke(const U24ArrayTypeU241280_t335952622& unmarshaled, U24ArrayTypeU241280_t335952622_marshaled_pinvoke& marshaled)
{
}
extern "C" void U24ArrayTypeU241280_t335952622_marshal_pinvoke_back(const U24ArrayTypeU241280_t335952622_marshaled_pinvoke& marshaled, U24ArrayTypeU241280_t335952622& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$1280
extern "C" void U24ArrayTypeU241280_t335952622_marshal_pinvoke_cleanup(U24ArrayTypeU241280_t335952622_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$1280
extern "C" void U24ArrayTypeU241280_t335952622_marshal_com(const U24ArrayTypeU241280_t335952622& unmarshaled, U24ArrayTypeU241280_t335952622_marshaled_com& marshaled)
{
}
extern "C" void U24ArrayTypeU241280_t335952622_marshal_com_back(const U24ArrayTypeU241280_t335952622_marshaled_com& marshaled, U24ArrayTypeU241280_t335952622& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$1280
extern "C" void U24ArrayTypeU241280_t335952622_marshal_com_cleanup(U24ArrayTypeU241280_t335952622_marshaled_com& marshaled)
{
}
// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$1532
extern "C" void U24ArrayTypeU241532_t335955352_marshal_pinvoke(const U24ArrayTypeU241532_t335955352& unmarshaled, U24ArrayTypeU241532_t335955352_marshaled_pinvoke& marshaled)
{
}
extern "C" void U24ArrayTypeU241532_t335955352_marshal_pinvoke_back(const U24ArrayTypeU241532_t335955352_marshaled_pinvoke& marshaled, U24ArrayTypeU241532_t335955352& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$1532
extern "C" void U24ArrayTypeU241532_t335955352_marshal_pinvoke_cleanup(U24ArrayTypeU241532_t335955352_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$1532
extern "C" void U24ArrayTypeU241532_t335955352_marshal_com(const U24ArrayTypeU241532_t335955352& unmarshaled, U24ArrayTypeU241532_t335955352_marshaled_com& marshaled)
{
}
extern "C" void U24ArrayTypeU241532_t335955352_marshal_com_back(const U24ArrayTypeU241532_t335955352_marshaled_com& marshaled, U24ArrayTypeU241532_t335955352& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$1532
extern "C" void U24ArrayTypeU241532_t335955352_marshal_com_cleanup(U24ArrayTypeU241532_t335955352_marshaled_com& marshaled)
{
}
// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$208
extern "C" void U24ArrayTypeU24208_t2366142725_marshal_pinvoke(const U24ArrayTypeU24208_t2366142725& unmarshaled, U24ArrayTypeU24208_t2366142725_marshaled_pinvoke& marshaled)
{
}
extern "C" void U24ArrayTypeU24208_t2366142725_marshal_pinvoke_back(const U24ArrayTypeU24208_t2366142725_marshaled_pinvoke& marshaled, U24ArrayTypeU24208_t2366142725& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$208
extern "C" void U24ArrayTypeU24208_t2366142725_marshal_pinvoke_cleanup(U24ArrayTypeU24208_t2366142725_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$208
extern "C" void U24ArrayTypeU24208_t2366142725_marshal_com(const U24ArrayTypeU24208_t2366142725& unmarshaled, U24ArrayTypeU24208_t2366142725_marshaled_com& marshaled)
{
}
extern "C" void U24ArrayTypeU24208_t2366142725_marshal_com_back(const U24ArrayTypeU24208_t2366142725_marshaled_com& marshaled, U24ArrayTypeU24208_t2366142725& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$208
extern "C" void U24ArrayTypeU24208_t2366142725_marshal_com_cleanup(U24ArrayTypeU24208_t2366142725_marshaled_com& marshaled)
{
}
// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$236
extern "C" void U24ArrayTypeU24236_t2366142816_marshal_pinvoke(const U24ArrayTypeU24236_t2366142816& unmarshaled, U24ArrayTypeU24236_t2366142816_marshaled_pinvoke& marshaled)
{
}
extern "C" void U24ArrayTypeU24236_t2366142816_marshal_pinvoke_back(const U24ArrayTypeU24236_t2366142816_marshaled_pinvoke& marshaled, U24ArrayTypeU24236_t2366142816& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$236
extern "C" void U24ArrayTypeU24236_t2366142816_marshal_pinvoke_cleanup(U24ArrayTypeU24236_t2366142816_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$236
extern "C" void U24ArrayTypeU24236_t2366142816_marshal_com(const U24ArrayTypeU24236_t2366142816& unmarshaled, U24ArrayTypeU24236_t2366142816_marshaled_com& marshaled)
{
}
extern "C" void U24ArrayTypeU24236_t2366142816_marshal_com_back(const U24ArrayTypeU24236_t2366142816_marshaled_com& marshaled, U24ArrayTypeU24236_t2366142816& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$236
extern "C" void U24ArrayTypeU24236_t2366142816_marshal_com_cleanup(U24ArrayTypeU24236_t2366142816_marshaled_com& marshaled)
{
}
// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$256
extern "C" void U24ArrayTypeU24256_t2366142881_marshal_pinvoke(const U24ArrayTypeU24256_t2366142881& unmarshaled, U24ArrayTypeU24256_t2366142881_marshaled_pinvoke& marshaled)
{
}
extern "C" void U24ArrayTypeU24256_t2366142881_marshal_pinvoke_back(const U24ArrayTypeU24256_t2366142881_marshaled_pinvoke& marshaled, U24ArrayTypeU24256_t2366142881& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$256
extern "C" void U24ArrayTypeU24256_t2366142881_marshal_pinvoke_cleanup(U24ArrayTypeU24256_t2366142881_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$256
extern "C" void U24ArrayTypeU24256_t2366142881_marshal_com(const U24ArrayTypeU24256_t2366142881& unmarshaled, U24ArrayTypeU24256_t2366142881_marshaled_com& marshaled)
{
}
extern "C" void U24ArrayTypeU24256_t2366142881_marshal_com_back(const U24ArrayTypeU24256_t2366142881_marshaled_com& marshaled, U24ArrayTypeU24256_t2366142881& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$256
extern "C" void U24ArrayTypeU24256_t2366142881_marshal_com_cleanup(U24ArrayTypeU24256_t2366142881_marshaled_com& marshaled)
{
}
// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$72
extern "C" void U24ArrayTypeU2472_t214874673_marshal_pinvoke(const U24ArrayTypeU2472_t214874673& unmarshaled, U24ArrayTypeU2472_t214874673_marshaled_pinvoke& marshaled)
{
}
extern "C" void U24ArrayTypeU2472_t214874673_marshal_pinvoke_back(const U24ArrayTypeU2472_t214874673_marshaled_pinvoke& marshaled, U24ArrayTypeU2472_t214874673& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$72
extern "C" void U24ArrayTypeU2472_t214874673_marshal_pinvoke_cleanup(U24ArrayTypeU2472_t214874673_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$72
extern "C" void U24ArrayTypeU2472_t214874673_marshal_com(const U24ArrayTypeU2472_t214874673& unmarshaled, U24ArrayTypeU2472_t214874673_marshaled_com& marshaled)
{
}
extern "C" void U24ArrayTypeU2472_t214874673_marshal_com_back(const U24ArrayTypeU2472_t214874673_marshaled_com& marshaled, U24ArrayTypeU2472_t214874673& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$72
extern "C" void U24ArrayTypeU2472_t214874673_marshal_com_cleanup(U24ArrayTypeU2472_t214874673_marshaled_com& marshaled)
{
}
// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$8
extern "C" void U24ArrayTypeU248_t2777878084_marshal_pinvoke(const U24ArrayTypeU248_t2777878084& unmarshaled, U24ArrayTypeU248_t2777878084_marshaled_pinvoke& marshaled)
{
}
extern "C" void U24ArrayTypeU248_t2777878084_marshal_pinvoke_back(const U24ArrayTypeU248_t2777878084_marshaled_pinvoke& marshaled, U24ArrayTypeU248_t2777878084& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$8
extern "C" void U24ArrayTypeU248_t2777878084_marshal_pinvoke_cleanup(U24ArrayTypeU248_t2777878084_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$8
extern "C" void U24ArrayTypeU248_t2777878084_marshal_com(const U24ArrayTypeU248_t2777878084& unmarshaled, U24ArrayTypeU248_t2777878084_marshaled_com& marshaled)
{
}
extern "C" void U24ArrayTypeU248_t2777878084_marshal_com_back(const U24ArrayTypeU248_t2777878084_marshaled_com& marshaled, U24ArrayTypeU248_t2777878084& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$8
extern "C" void U24ArrayTypeU248_t2777878084_marshal_com_cleanup(U24ArrayTypeU248_t2777878084_marshaled_com& marshaled)
{
}
// System.Void Mono.Xml.DictionaryBase::.ctor()
extern Il2CppClass* List_1_t3656612197_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1960690472_MethodInfo_var;
extern const uint32_t DictionaryBase__ctor_m2042137016_MetadataUsageId;
extern "C"  void DictionaryBase__ctor_m2042137016 (DictionaryBase_t4261756523 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryBase__ctor_m2042137016_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(List_1_t3656612197_il2cpp_TypeInfo_var);
		List_1__ctor_m1960690472(__this, /*hidden argument*/List_1__ctor_m1960690472_MethodInfo_var);
		return;
	}
}
// System.Collections.Generic.IEnumerable`1<Mono.Xml.DTDNode> Mono.Xml.DictionaryBase::get_Values()
extern Il2CppClass* U3CU3Ec__Iterator3_t2516598212_il2cpp_TypeInfo_var;
extern const uint32_t DictionaryBase_get_Values_m4015003527_MetadataUsageId;
extern "C"  Il2CppObject* DictionaryBase_get_Values_m4015003527 (DictionaryBase_t4261756523 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DictionaryBase_get_Values_m4015003527_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__Iterator3_t2516598212 * V_0 = NULL;
	{
		U3CU3Ec__Iterator3_t2516598212 * L_0 = (U3CU3Ec__Iterator3_t2516598212 *)il2cpp_codegen_object_new(U3CU3Ec__Iterator3_t2516598212_il2cpp_TypeInfo_var);
		U3CU3Ec__Iterator3__ctor_m4276457563(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__Iterator3_t2516598212 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_4(__this);
		U3CU3Ec__Iterator3_t2516598212 * L_2 = V_0;
		U3CU3Ec__Iterator3_t2516598212 * L_3 = L_2;
		NullCheck(L_3);
		L_3->set_U24PC_2(((int32_t)-2));
		return L_3;
	}
}
// System.Void Mono.Xml.DictionaryBase/<>c__Iterator3::.ctor()
extern "C"  void U3CU3Ec__Iterator3__ctor_m4276457563 (U3CU3Ec__Iterator3_t2516598212 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// Mono.Xml.DTDNode Mono.Xml.DictionaryBase/<>c__Iterator3::System.Collections.Generic.IEnumerator<Mono.Xml.DTDNode>.get_Current()
extern "C"  DTDNode_t1733424026 * U3CU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CMono_Xml_DTDNodeU3E_get_Current_m363154420 (U3CU3Ec__Iterator3_t2516598212 * __this, const MethodInfo* method)
{
	{
		DTDNode_t1733424026 * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Object Mono.Xml.DictionaryBase/<>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1698455733 (U3CU3Ec__Iterator3_t2516598212 * __this, const MethodInfo* method)
{
	{
		DTDNode_t1733424026 * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Collections.IEnumerator Mono.Xml.DictionaryBase/<>c__Iterator3::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CU3Ec__Iterator3_System_Collections_IEnumerable_GetEnumerator_m1099848330 (U3CU3Ec__Iterator3_t2516598212 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = U3CU3Ec__Iterator3_System_Collections_Generic_IEnumerableU3CMono_Xml_DTDNodeU3E_GetEnumerator_m2866801913(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<Mono.Xml.DTDNode> Mono.Xml.DictionaryBase/<>c__Iterator3::System.Collections.Generic.IEnumerable<Mono.Xml.DTDNode>.GetEnumerator()
extern Il2CppClass* U3CU3Ec__Iterator3_t2516598212_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec__Iterator3_System_Collections_Generic_IEnumerableU3CMono_Xml_DTDNodeU3E_GetEnumerator_m2866801913_MetadataUsageId;
extern "C"  Il2CppObject* U3CU3Ec__Iterator3_System_Collections_Generic_IEnumerableU3CMono_Xml_DTDNodeU3E_GetEnumerator_m2866801913 (U3CU3Ec__Iterator3_t2516598212 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__Iterator3_System_Collections_Generic_IEnumerableU3CMono_Xml_DTDNodeU3E_GetEnumerator_m2866801913_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__Iterator3_t2516598212 * V_0 = NULL;
	{
		int32_t* L_0 = __this->get_address_of_U24PC_2();
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, L_0, 0, ((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CU3Ec__Iterator3_t2516598212 * L_2 = (U3CU3Ec__Iterator3_t2516598212 *)il2cpp_codegen_object_new(U3CU3Ec__Iterator3_t2516598212_il2cpp_TypeInfo_var);
		U3CU3Ec__Iterator3__ctor_m4276457563(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		U3CU3Ec__Iterator3_t2516598212 * L_3 = V_0;
		DictionaryBase_t4261756523 * L_4 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_4(L_4);
		U3CU3Ec__Iterator3_t2516598212 * L_5 = V_0;
		return L_5;
	}
}
// System.Boolean Mono.Xml.DictionaryBase/<>c__Iterator3::MoveNext()
extern Il2CppClass* Enumerator_t1742395189_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m4006862359_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4070407275_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m1416717449_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1464264581_MethodInfo_var;
extern const uint32_t U3CU3Ec__Iterator3_MoveNext_m3913652981_MetadataUsageId;
extern "C"  bool U3CU3Ec__Iterator3_MoveNext_m3913652981 (U3CU3Ec__Iterator3_t2516598212 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__Iterator3_MoveNext_m3913652981_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_2();
		V_0 = L_0;
		__this->set_U24PC_2((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_0037;
		}
	}
	{
		goto IL_00a9;
	}

IL_0023:
	{
		DictionaryBase_t4261756523 * L_2 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_2);
		Enumerator_t1742395189  L_3 = List_1_GetEnumerator_m4006862359(L_2, /*hidden argument*/List_1_GetEnumerator_m4006862359_MethodInfo_var);
		__this->set_U3CU24s_431U3E__0_0(L_3);
		V_0 = ((int32_t)-3);
	}

IL_0037:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_4 = V_0;
			if (((int32_t)((int32_t)L_4-(int32_t)1)) == 0)
			{
				goto IL_0078;
			}
		}

IL_0043:
		{
			goto IL_0078;
		}

IL_0048:
		{
			Enumerator_t1742395189 * L_5 = __this->get_address_of_U3CU24s_431U3E__0_0();
			KeyValuePair_2_t2859653228  L_6 = Enumerator_get_Current_m4070407275(L_5, /*hidden argument*/Enumerator_get_Current_m4070407275_MethodInfo_var);
			__this->set_U3CpU3E__1_1(L_6);
			KeyValuePair_2_t2859653228 * L_7 = __this->get_address_of_U3CpU3E__1_1();
			DTDNode_t1733424026 * L_8 = KeyValuePair_2_get_Value_m1416717449(L_7, /*hidden argument*/KeyValuePair_2_get_Value_m1416717449_MethodInfo_var);
			__this->set_U24current_3(L_8);
			__this->set_U24PC_2(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xAB, FINALLY_008d);
		}

IL_0078:
		{
			Enumerator_t1742395189 * L_9 = __this->get_address_of_U3CU24s_431U3E__0_0();
			bool L_10 = Enumerator_MoveNext_m1464264581(L_9, /*hidden argument*/Enumerator_MoveNext_m1464264581_MethodInfo_var);
			if (L_10)
			{
				goto IL_0048;
			}
		}

IL_0088:
		{
			IL2CPP_LEAVE(0xA2, FINALLY_008d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008d;
	}

FINALLY_008d:
	{ // begin finally (depth: 1)
		{
			bool L_11 = V_1;
			if (!L_11)
			{
				goto IL_0091;
			}
		}

IL_0090:
		{
			IL2CPP_END_FINALLY(141)
		}

IL_0091:
		{
			Enumerator_t1742395189  L_12 = __this->get_U3CU24s_431U3E__0_0();
			Enumerator_t1742395189  L_13 = L_12;
			Il2CppObject * L_14 = Box(Enumerator_t1742395189_il2cpp_TypeInfo_var, &L_13);
			NullCheck((Il2CppObject *)L_14);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_14);
			IL2CPP_END_FINALLY(141)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(141)
	{
		IL2CPP_JUMP_TBL(0xAB, IL_00ab)
		IL2CPP_JUMP_TBL(0xA2, IL_00a2)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00a2:
	{
		__this->set_U24PC_2((-1));
	}

IL_00a9:
	{
		return (bool)0;
	}

IL_00ab:
	{
		return (bool)1;
	}
	// Dead block : IL_00ad: ldloc.2
}
// System.Void Mono.Xml.DictionaryBase/<>c__Iterator3::Dispose()
extern Il2CppClass* Enumerator_t1742395189_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec__Iterator3_Dispose_m3585266136_MetadataUsageId;
extern "C"  void U3CU3Ec__Iterator3_Dispose_m3585266136 (U3CU3Ec__Iterator3_t2516598212 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__Iterator3_Dispose_m3585266136_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_2();
		V_0 = L_0;
		__this->set_U24PC_2((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0037;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_0037;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x37, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		Enumerator_t1742395189  L_2 = __this->get_U3CU24s_431U3E__0_0();
		Enumerator_t1742395189  L_3 = L_2;
		Il2CppObject * L_4 = Box(Enumerator_t1742395189_il2cpp_TypeInfo_var, &L_3);
		NullCheck((Il2CppObject *)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
		IL2CPP_END_FINALLY(38)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x37, IL_0037)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0037:
	{
		return;
	}
}
// System.Void Mono.Xml.DictionaryBase/<>c__Iterator3::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec__Iterator3_Reset_m1922890504_MetadataUsageId;
extern "C"  void U3CU3Ec__Iterator3_Reset_m1922890504 (U3CU3Ec__Iterator3_t2516598212 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__Iterator3_Reset_m1922890504_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Mono.Xml.DTDAttListDeclaration::.ctor(Mono.Xml.DTDObjectModel)
extern Il2CppClass* Hashtable_t3875263730_il2cpp_TypeInfo_var;
extern Il2CppClass* ArrayList_t2121638921_il2cpp_TypeInfo_var;
extern const uint32_t DTDAttListDeclaration__ctor_m1247135746_MetadataUsageId;
extern "C"  void DTDAttListDeclaration__ctor_m1247135746 (DTDAttListDeclaration_t1258293171 * __this, DTDObjectModel_t709926554 * ___root0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDAttListDeclaration__ctor_m1247135746_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Hashtable_t3875263730 * L_0 = (Hashtable_t3875263730 *)il2cpp_codegen_object_new(Hashtable_t3875263730_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1514037738(L_0, /*hidden argument*/NULL);
		__this->set_attributeOrders_6(L_0);
		ArrayList_t2121638921 * L_1 = (ArrayList_t2121638921 *)il2cpp_codegen_object_new(ArrayList_t2121638921_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1878432947(L_1, /*hidden argument*/NULL);
		__this->set_attributes_7(L_1);
		DTDNode__ctor_m2554517745(__this, /*hidden argument*/NULL);
		DTDObjectModel_t709926554 * L_2 = ___root0;
		DTDNode_SetRoot_m970259879(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.String Mono.Xml.DTDAttListDeclaration::get_Name()
extern "C"  String_t* DTDAttListDeclaration_get_Name_m3744810115 (DTDAttListDeclaration_t1258293171 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_name_5();
		return L_0;
	}
}
// System.Void Mono.Xml.DTDAttListDeclaration::set_Name(System.String)
extern "C"  void DTDAttListDeclaration_set_Name_m1965188718 (DTDAttListDeclaration_t1258293171 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_name_5(L_0);
		return;
	}
}
// Mono.Xml.DTDAttributeDefinition Mono.Xml.DTDAttListDeclaration::get_Item(System.Int32)
extern "C"  DTDAttributeDefinition_t1053410431 * DTDAttListDeclaration_get_Item_m3277382479 (DTDAttListDeclaration_t1258293171 * __this, int32_t ___i0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___i0;
		DTDAttributeDefinition_t1053410431 * L_1 = DTDAttListDeclaration_Get_m1876834279(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// Mono.Xml.DTDAttributeDefinition Mono.Xml.DTDAttListDeclaration::get_Item(System.String)
extern "C"  DTDAttributeDefinition_t1053410431 * DTDAttListDeclaration_get_Item_m3271307460 (DTDAttListDeclaration_t1258293171 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name0;
		DTDAttributeDefinition_t1053410431 * L_1 = DTDAttListDeclaration_Get_m2803986220(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// Mono.Xml.DTDAttributeDefinition Mono.Xml.DTDAttListDeclaration::Get(System.Int32)
extern Il2CppClass* DTDAttributeDefinition_t1053410431_il2cpp_TypeInfo_var;
extern const uint32_t DTDAttListDeclaration_Get_m1876834279_MetadataUsageId;
extern "C"  DTDAttributeDefinition_t1053410431 * DTDAttListDeclaration_Get_m1876834279 (DTDAttListDeclaration_t1258293171 * __this, int32_t ___i0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDAttListDeclaration_Get_m1876834279_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ArrayList_t2121638921 * L_0 = __this->get_attributes_7();
		int32_t L_1 = ___i0;
		NullCheck(L_0);
		Il2CppObject * L_2 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_0, L_1);
		return ((DTDAttributeDefinition_t1053410431 *)IsInstClass(L_2, DTDAttributeDefinition_t1053410431_il2cpp_TypeInfo_var));
	}
}
// Mono.Xml.DTDAttributeDefinition Mono.Xml.DTDAttListDeclaration::Get(System.String)
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* DTDAttributeDefinition_t1053410431_il2cpp_TypeInfo_var;
extern const uint32_t DTDAttListDeclaration_Get_m2803986220_MetadataUsageId;
extern "C"  DTDAttributeDefinition_t1053410431 * DTDAttListDeclaration_Get_m2803986220 (DTDAttListDeclaration_t1258293171 * __this, String_t* ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDAttListDeclaration_Get_m2803986220_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Hashtable_t3875263730 * L_0 = __this->get_attributeOrders_6();
		String_t* L_1 = ___name0;
		NullCheck(L_0);
		Il2CppObject * L_2 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_0, L_1);
		V_0 = L_2;
		Il2CppObject * L_3 = V_0;
		if (!L_3)
		{
			goto IL_002a;
		}
	}
	{
		ArrayList_t2121638921 * L_4 = __this->get_attributes_7();
		Il2CppObject * L_5 = V_0;
		NullCheck(L_4);
		Il2CppObject * L_6 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_4, ((*(int32_t*)((int32_t*)UnBox (L_5, Int32_t2847414787_il2cpp_TypeInfo_var)))));
		return ((DTDAttributeDefinition_t1053410431 *)IsInstClass(L_6, DTDAttributeDefinition_t1053410431_il2cpp_TypeInfo_var));
	}

IL_002a:
	{
		return (DTDAttributeDefinition_t1053410431 *)NULL;
	}
}
// System.Collections.IList Mono.Xml.DTDAttListDeclaration::get_Definitions()
extern "C"  Il2CppObject * DTDAttListDeclaration_get_Definitions_m1730561771 (DTDAttListDeclaration_t1258293171 * __this, const MethodInfo* method)
{
	{
		ArrayList_t2121638921 * L_0 = __this->get_attributes_7();
		return L_0;
	}
}
// System.Void Mono.Xml.DTDAttListDeclaration::Add(Mono.Xml.DTDAttributeDefinition)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3342815969;
extern const uint32_t DTDAttListDeclaration_Add_m3535383838_MetadataUsageId;
extern "C"  void DTDAttListDeclaration_Add_m3535383838 (DTDAttListDeclaration_t1258293171 * __this, DTDAttributeDefinition_t1053410431 * ___def0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDAttListDeclaration_Add_m3535383838_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Hashtable_t3875263730 * L_0 = __this->get_attributeOrders_6();
		DTDAttributeDefinition_t1053410431 * L_1 = ___def0;
		NullCheck(L_1);
		String_t* L_2 = DTDAttributeDefinition_get_Name_m1998089073(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Il2CppObject * L_3 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_0, L_2);
		if (!L_3)
		{
			goto IL_0032;
		}
	}
	{
		DTDAttributeDefinition_t1053410431 * L_4 = ___def0;
		NullCheck(L_4);
		String_t* L_5 = DTDAttributeDefinition_get_Name_m1998089073(L_4, /*hidden argument*/NULL);
		String_t* L_6 = DTDAttListDeclaration_get_Name_m3744810115(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Format_m2398979370(NULL /*static, unused*/, _stringLiteral3342815969, L_5, L_6, /*hidden argument*/NULL);
		InvalidOperationException_t2420574324 * L_8 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_8, L_7, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_0032:
	{
		DTDAttributeDefinition_t1053410431 * L_9 = ___def0;
		DTDObjectModel_t709926554 * L_10 = DTDNode_get_Root_m1036806329(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		DTDNode_SetRoot_m970259879(L_9, L_10, /*hidden argument*/NULL);
		Hashtable_t3875263730 * L_11 = __this->get_attributeOrders_6();
		DTDAttributeDefinition_t1053410431 * L_12 = ___def0;
		NullCheck(L_12);
		String_t* L_13 = DTDAttributeDefinition_get_Name_m1998089073(L_12, /*hidden argument*/NULL);
		ArrayList_t2121638921 * L_14 = __this->get_attributes_7();
		NullCheck(L_14);
		int32_t L_15 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_14);
		int32_t L_16 = L_15;
		Il2CppObject * L_17 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_16);
		NullCheck(L_11);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_11, L_13, L_17);
		ArrayList_t2121638921 * L_18 = __this->get_attributes_7();
		DTDAttributeDefinition_t1053410431 * L_19 = ___def0;
		NullCheck(L_18);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_18, L_19);
		return;
	}
}
// System.Void Mono.Xml.DTDAttListDeclarationCollection::.ctor(Mono.Xml.DTDObjectModel)
extern "C"  void DTDAttListDeclarationCollection__ctor_m3747246336 (DTDAttListDeclarationCollection_t733492145 * __this, DTDObjectModel_t709926554 * ___root0, const MethodInfo* method)
{
	{
		DTDObjectModel_t709926554 * L_0 = ___root0;
		DTDCollectionBase__ctor_m1839072630(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// Mono.Xml.DTDAttListDeclaration Mono.Xml.DTDAttListDeclarationCollection::get_Item(System.String)
extern Il2CppClass* DTDAttListDeclaration_t1258293171_il2cpp_TypeInfo_var;
extern const uint32_t DTDAttListDeclarationCollection_get_Item_m920644408_MetadataUsageId;
extern "C"  DTDAttListDeclaration_t1258293171 * DTDAttListDeclarationCollection_get_Item_m920644408 (DTDAttListDeclarationCollection_t733492145 * __this, String_t* ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDAttListDeclarationCollection_get_Item_m920644408_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name0;
		Il2CppObject * L_1 = DTDCollectionBase_BaseGet_m344774896(__this, L_0, /*hidden argument*/NULL);
		return ((DTDAttListDeclaration_t1258293171 *)IsInstClass(L_1, DTDAttListDeclaration_t1258293171_il2cpp_TypeInfo_var));
	}
}
// System.Void Mono.Xml.DTDAttListDeclarationCollection::Add(System.String,Mono.Xml.DTDAttListDeclaration)
extern Il2CppClass* IEnumerable_t287189635_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* DTDAttributeDefinition_t1053410431_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t DTDAttListDeclarationCollection_Add_m1772263056_MetadataUsageId;
extern "C"  void DTDAttListDeclarationCollection_Add_m1772263056 (DTDAttListDeclarationCollection_t733492145 * __this, String_t* ___name0, DTDAttListDeclaration_t1258293171 * ___decl1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDAttListDeclarationCollection_Add_m1772263056_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DTDAttListDeclaration_t1258293171 * V_0 = NULL;
	DTDAttributeDefinition_t1053410431 * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___name0;
		DTDAttListDeclaration_t1258293171 * L_1 = DTDAttListDeclarationCollection_get_Item_m920644408(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		DTDAttListDeclaration_t1258293171 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_006a;
		}
	}
	{
		DTDAttListDeclaration_t1258293171 * L_3 = ___decl1;
		NullCheck(L_3);
		Il2CppObject * L_4 = DTDAttListDeclaration_get_Definitions_m1730561771(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Il2CppObject * L_5 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t287189635_il2cpp_TypeInfo_var, L_4);
		V_2 = L_5;
	}

IL_001a:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0043;
		}

IL_001f:
		{
			Il2CppObject * L_6 = V_2;
			NullCheck(L_6);
			Il2CppObject * L_7 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_6);
			V_1 = ((DTDAttributeDefinition_t1053410431 *)CastclassClass(L_7, DTDAttributeDefinition_t1053410431_il2cpp_TypeInfo_var));
			DTDAttListDeclaration_t1258293171 * L_8 = ___decl1;
			DTDAttributeDefinition_t1053410431 * L_9 = V_1;
			NullCheck(L_9);
			String_t* L_10 = DTDAttributeDefinition_get_Name_m1998089073(L_9, /*hidden argument*/NULL);
			NullCheck(L_8);
			DTDAttributeDefinition_t1053410431 * L_11 = DTDAttListDeclaration_Get_m2803986220(L_8, L_10, /*hidden argument*/NULL);
			if (L_11)
			{
				goto IL_0043;
			}
		}

IL_003c:
		{
			DTDAttListDeclaration_t1258293171 * L_12 = V_0;
			DTDAttributeDefinition_t1053410431 * L_13 = V_1;
			NullCheck(L_12);
			DTDAttListDeclaration_Add_m3535383838(L_12, L_13, /*hidden argument*/NULL);
		}

IL_0043:
		{
			Il2CppObject * L_14 = V_2;
			NullCheck(L_14);
			bool L_15 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_14);
			if (L_15)
			{
				goto IL_001f;
			}
		}

IL_004e:
		{
			IL2CPP_LEAVE(0x65, FINALLY_0053);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0053;
	}

FINALLY_0053:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_16 = V_2;
			V_3 = ((Il2CppObject *)IsInst(L_16, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			Il2CppObject * L_17 = V_3;
			if (L_17)
			{
				goto IL_005e;
			}
		}

IL_005d:
		{
			IL2CPP_END_FINALLY(83)
		}

IL_005e:
		{
			Il2CppObject * L_18 = V_3;
			NullCheck(L_18);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_18);
			IL2CPP_END_FINALLY(83)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(83)
	{
		IL2CPP_JUMP_TBL(0x65, IL_0065)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0065:
	{
		goto IL_007e;
	}

IL_006a:
	{
		DTDAttListDeclaration_t1258293171 * L_19 = ___decl1;
		DTDObjectModel_t709926554 * L_20 = DTDCollectionBase_get_Root_m1145982982(__this, /*hidden argument*/NULL);
		NullCheck(L_19);
		DTDNode_SetRoot_m970259879(L_19, L_20, /*hidden argument*/NULL);
		String_t* L_21 = ___name0;
		DTDAttListDeclaration_t1258293171 * L_22 = ___decl1;
		DTDCollectionBase_BaseAdd_m1540252226(__this, L_21, L_22, /*hidden argument*/NULL);
	}

IL_007e:
	{
		return;
	}
}
// System.String Mono.Xml.DTDAttributeDefinition::get_Name()
extern "C"  String_t* DTDAttributeDefinition_get_Name_m1998089073 (DTDAttributeDefinition_t1053410431 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_name_5();
		return L_0;
	}
}
// System.Xml.Schema.XmlSchemaDatatype Mono.Xml.DTDAttributeDefinition::get_Datatype()
extern "C"  XmlSchemaDatatype_t2590121 * DTDAttributeDefinition_get_Datatype_m2869597951 (DTDAttributeDefinition_t1053410431 * __this, const MethodInfo* method)
{
	{
		XmlSchemaDatatype_t2590121 * L_0 = __this->get_datatype_6();
		return L_0;
	}
}
// System.String Mono.Xml.DTDAttributeDefinition::get_DefaultValue()
extern "C"  String_t* DTDAttributeDefinition_get_DefaultValue_m347427414 (DTDAttributeDefinition_t1053410431 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_resolvedDefaultValue_8();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		String_t* L_1 = DTDAttributeDefinition_ComputeDefaultValue_m3929866668(__this, /*hidden argument*/NULL);
		__this->set_resolvedDefaultValue_8(L_1);
	}

IL_0017:
	{
		String_t* L_2 = __this->get_resolvedDefaultValue_8();
		return L_2;
	}
}
// System.String Mono.Xml.DTDAttributeDefinition::get_UnresolvedDefaultValue()
extern "C"  String_t* DTDAttributeDefinition_get_UnresolvedDefaultValue_m3220743175 (DTDAttributeDefinition_t1053410431 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_unresolvedDefault_7();
		return L_0;
	}
}
// System.String Mono.Xml.DTDAttributeDefinition::ComputeDefaultValue()
extern Il2CppClass* StringBuilder_t3822575854_il2cpp_TypeInfo_var;
extern Il2CppClass* CultureInfo_t3603717042_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlChar_t3591879093_il2cpp_TypeInfo_var;
extern const uint32_t DTDAttributeDefinition_ComputeDefaultValue_m3929866668_MetadataUsageId;
extern "C"  String_t* DTDAttributeDefinition_ComputeDefaultValue_m3929866668 (DTDAttributeDefinition_t1053410431 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDAttributeDefinition_ComputeDefaultValue_m3929866668_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t3822575854 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	String_t* V_3 = NULL;
	int32_t V_4 = 0;
	uint16_t V_5 = 0x0;
	int32_t V_6 = 0;
	String_t* V_7 = NULL;
	String_t* V_8 = NULL;
	int32_t V_9 = 0;
	String_t* V_10 = NULL;
	{
		String_t* L_0 = DTDAttributeDefinition_get_UnresolvedDefaultValue_m3220743175(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_000d:
	{
		StringBuilder_t3822575854 * L_1 = (StringBuilder_t3822575854 *)il2cpp_codegen_object_new(StringBuilder_t3822575854_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m135953004(L_1, /*hidden argument*/NULL);
		V_0 = L_1;
		V_1 = 0;
		V_2 = 0;
		String_t* L_2 = DTDAttributeDefinition_get_UnresolvedDefaultValue_m3220743175(__this, /*hidden argument*/NULL);
		V_3 = L_2;
		goto IL_0102;
	}

IL_0023:
	{
		String_t* L_3 = V_3;
		int32_t L_4 = V_2;
		NullCheck(L_3);
		int32_t L_5 = String_IndexOf_m204546721(L_3, ((int32_t)59), L_4, /*hidden argument*/NULL);
		V_4 = L_5;
		String_t* L_6 = V_3;
		int32_t L_7 = V_2;
		NullCheck(L_6);
		uint16_t L_8 = String_get_Chars_m3015341861(L_6, ((int32_t)((int32_t)L_7+(int32_t)1)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_8) == ((uint32_t)((int32_t)35)))))
		{
			goto IL_00aa;
		}
	}
	{
		String_t* L_9 = V_3;
		int32_t L_10 = V_2;
		NullCheck(L_9);
		uint16_t L_11 = String_get_Chars_m3015341861(L_9, ((int32_t)((int32_t)L_10+(int32_t)2)), /*hidden argument*/NULL);
		V_5 = L_11;
		V_6 = 7;
		uint16_t L_12 = V_5;
		if ((((int32_t)L_12) == ((int32_t)((int32_t)120))))
		{
			goto IL_005e;
		}
	}
	{
		uint16_t L_13 = V_5;
		if ((!(((uint32_t)L_13) == ((uint32_t)((int32_t)88)))))
		{
			goto IL_007e;
		}
	}

IL_005e:
	{
		String_t* L_14 = V_3;
		int32_t L_15 = V_2;
		int32_t L_16 = V_4;
		int32_t L_17 = V_2;
		NullCheck(L_14);
		String_t* L_18 = String_Substring_m675079568(L_14, ((int32_t)((int32_t)L_15+(int32_t)3)), ((int32_t)((int32_t)((int32_t)((int32_t)L_16-(int32_t)L_17))-(int32_t)3)), /*hidden argument*/NULL);
		V_7 = L_18;
		int32_t L_19 = V_6;
		V_6 = ((int32_t)((int32_t)L_19|(int32_t)((int32_t)515)));
		goto IL_008f;
	}

IL_007e:
	{
		String_t* L_20 = V_3;
		int32_t L_21 = V_2;
		int32_t L_22 = V_4;
		int32_t L_23 = V_2;
		NullCheck(L_20);
		String_t* L_24 = String_Substring_m675079568(L_20, ((int32_t)((int32_t)L_21+(int32_t)2)), ((int32_t)((int32_t)((int32_t)((int32_t)L_22-(int32_t)L_23))-(int32_t)2)), /*hidden argument*/NULL);
		V_7 = L_24;
	}

IL_008f:
	{
		StringBuilder_t3822575854 * L_25 = V_0;
		String_t* L_26 = V_7;
		int32_t L_27 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3603717042_il2cpp_TypeInfo_var);
		CultureInfo_t3603717042 * L_28 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_29 = Int32_Parse_m2659730549(NULL /*static, unused*/, L_26, L_27, L_28, /*hidden argument*/NULL);
		NullCheck(L_25);
		StringBuilder_Append_m2143093878(L_25, (((int32_t)((uint16_t)L_29))), /*hidden argument*/NULL);
		goto IL_00fd;
	}

IL_00aa:
	{
		StringBuilder_t3822575854 * L_30 = V_0;
		String_t* L_31 = V_3;
		int32_t L_32 = V_1;
		int32_t L_33 = V_2;
		NullCheck(L_31);
		String_t* L_34 = String_Substring_m675079568(L_31, L_32, ((int32_t)((int32_t)L_33-(int32_t)1)), /*hidden argument*/NULL);
		NullCheck(L_30);
		StringBuilder_Append_m3898090075(L_30, L_34, /*hidden argument*/NULL);
		String_t* L_35 = V_3;
		int32_t L_36 = V_2;
		int32_t L_37 = V_4;
		NullCheck(L_35);
		String_t* L_38 = String_Substring_m675079568(L_35, ((int32_t)((int32_t)L_36+(int32_t)1)), ((int32_t)((int32_t)L_37-(int32_t)2)), /*hidden argument*/NULL);
		V_8 = L_38;
		String_t* L_39 = V_8;
		IL2CPP_RUNTIME_CLASS_INIT(XmlChar_t3591879093_il2cpp_TypeInfo_var);
		int32_t L_40 = XmlChar_GetPredefinedEntity_m4157892537(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		V_9 = L_40;
		int32_t L_41 = V_9;
		if ((((int32_t)L_41) < ((int32_t)0)))
		{
			goto IL_00e9;
		}
	}
	{
		StringBuilder_t3822575854 * L_42 = V_0;
		int32_t L_43 = V_9;
		NullCheck(L_42);
		StringBuilder_Append_m2189222616(L_42, L_43, /*hidden argument*/NULL);
		goto IL_00fd;
	}

IL_00e9:
	{
		StringBuilder_t3822575854 * L_44 = V_0;
		DTDObjectModel_t709926554 * L_45 = DTDNode_get_Root_m1036806329(__this, /*hidden argument*/NULL);
		String_t* L_46 = V_8;
		NullCheck(L_45);
		String_t* L_47 = DTDObjectModel_ResolveEntity_m1835942729(L_45, L_46, /*hidden argument*/NULL);
		NullCheck(L_44);
		StringBuilder_Append_m3898090075(L_44, L_47, /*hidden argument*/NULL);
	}

IL_00fd:
	{
		int32_t L_48 = V_4;
		V_1 = ((int32_t)((int32_t)L_48+(int32_t)1));
	}

IL_0102:
	{
		String_t* L_49 = V_3;
		int32_t L_50 = V_1;
		NullCheck(L_49);
		int32_t L_51 = String_IndexOf_m204546721(L_49, ((int32_t)38), L_50, /*hidden argument*/NULL);
		int32_t L_52 = L_51;
		V_2 = L_52;
		if ((((int32_t)L_52) >= ((int32_t)0)))
		{
			goto IL_0023;
		}
	}
	{
		StringBuilder_t3822575854 * L_53 = V_0;
		String_t* L_54 = V_3;
		int32_t L_55 = V_1;
		NullCheck(L_54);
		String_t* L_56 = String_Substring_m2809233063(L_54, L_55, /*hidden argument*/NULL);
		NullCheck(L_53);
		StringBuilder_Append_m3898090075(L_53, L_56, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_57 = V_0;
		StringBuilder_t3822575854 * L_58 = V_0;
		NullCheck(L_58);
		int32_t L_59 = StringBuilder_get_Length_m2443133099(L_58, /*hidden argument*/NULL);
		NullCheck(L_57);
		String_t* L_60 = StringBuilder_ToString_m3621056261(L_57, 1, ((int32_t)((int32_t)L_59-(int32_t)2)), /*hidden argument*/NULL);
		V_10 = L_60;
		StringBuilder_t3822575854 * L_61 = V_0;
		NullCheck(L_61);
		StringBuilder_set_Length_m1952332172(L_61, 0, /*hidden argument*/NULL);
		String_t* L_62 = V_10;
		return L_62;
	}
}
// System.Void Mono.Xml.DTDAutomataFactory::.ctor(Mono.Xml.DTDObjectModel)
extern Il2CppClass* Hashtable_t3875263730_il2cpp_TypeInfo_var;
extern const uint32_t DTDAutomataFactory__ctor_m2953278209_MetadataUsageId;
extern "C"  void DTDAutomataFactory__ctor_m2953278209 (DTDAutomataFactory_t2001760042 * __this, DTDObjectModel_t709926554 * ___root0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDAutomataFactory__ctor_m2953278209_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Hashtable_t3875263730 * L_0 = (Hashtable_t3875263730 *)il2cpp_codegen_object_new(Hashtable_t3875263730_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1514037738(L_0, /*hidden argument*/NULL);
		__this->set_choiceTable_1(L_0);
		Hashtable_t3875263730 * L_1 = (Hashtable_t3875263730 *)il2cpp_codegen_object_new(Hashtable_t3875263730_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1514037738(L_1, /*hidden argument*/NULL);
		__this->set_sequenceTable_2(L_1);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		DTDObjectModel_t709926554 * L_2 = ___root0;
		__this->set_root_0(L_2);
		return;
	}
}
// System.Void Mono.Xml.DTDCollectionBase::.ctor(Mono.Xml.DTDObjectModel)
extern "C"  void DTDCollectionBase__ctor_m1839072630 (DTDCollectionBase_t3975886631 * __this, DTDObjectModel_t709926554 * ___root0, const MethodInfo* method)
{
	{
		DictionaryBase__ctor_m2042137016(__this, /*hidden argument*/NULL);
		DTDObjectModel_t709926554 * L_0 = ___root0;
		__this->set_root_5(L_0);
		return;
	}
}
// Mono.Xml.DTDObjectModel Mono.Xml.DTDCollectionBase::get_Root()
extern "C"  DTDObjectModel_t709926554 * DTDCollectionBase_get_Root_m1145982982 (DTDCollectionBase_t3975886631 * __this, const MethodInfo* method)
{
	{
		DTDObjectModel_t709926554 * L_0 = __this->get_root_5();
		return L_0;
	}
}
// System.Void Mono.Xml.DTDCollectionBase::BaseAdd(System.String,Mono.Xml.DTDNode)
extern const MethodInfo* KeyValuePair_2__ctor_m1331657475_MethodInfo_var;
extern const MethodInfo* List_1_Add_m1655051288_MethodInfo_var;
extern const uint32_t DTDCollectionBase_BaseAdd_m1540252226_MetadataUsageId;
extern "C"  void DTDCollectionBase_BaseAdd_m1540252226 (DTDCollectionBase_t3975886631 * __this, String_t* ___name0, DTDNode_t1733424026 * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDCollectionBase_BaseAdd_m1540252226_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name0;
		DTDNode_t1733424026 * L_1 = ___value1;
		KeyValuePair_2_t2859653228  L_2;
		memset(&L_2, 0, sizeof(L_2));
		KeyValuePair_2__ctor_m1331657475(&L_2, L_0, L_1, /*hidden argument*/KeyValuePair_2__ctor_m1331657475_MethodInfo_var);
		List_1_Add_m1655051288(__this, L_2, /*hidden argument*/List_1_Add_m1655051288_MethodInfo_var);
		return;
	}
}
// System.Boolean Mono.Xml.DTDCollectionBase::Contains(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t1742395189_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m4006862359_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4070407275_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m1956205848_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1464264581_MethodInfo_var;
extern const uint32_t DTDCollectionBase_Contains_m1398227387_MetadataUsageId;
extern "C"  bool DTDCollectionBase_Contains_m1398227387 (DTDCollectionBase_t3975886631 * __this, String_t* ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDCollectionBase_Contains_m1398227387_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2_t2859653228  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Enumerator_t1742395189  V_1;
	memset(&V_1, 0, sizeof(V_1));
	bool V_2 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Enumerator_t1742395189  L_0 = List_1_GetEnumerator_m4006862359(__this, /*hidden argument*/List_1_GetEnumerator_m4006862359_MethodInfo_var);
		V_1 = L_0;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002d;
		}

IL_000c:
		{
			KeyValuePair_2_t2859653228  L_1 = Enumerator_get_Current_m4070407275((&V_1), /*hidden argument*/Enumerator_get_Current_m4070407275_MethodInfo_var);
			V_0 = L_1;
			String_t* L_2 = KeyValuePair_2_get_Key_m1956205848((&V_0), /*hidden argument*/KeyValuePair_2_get_Key_m1956205848_MethodInfo_var);
			String_t* L_3 = ___key0;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_4 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
			if (!L_4)
			{
				goto IL_002d;
			}
		}

IL_0026:
		{
			V_2 = (bool)1;
			IL2CPP_LEAVE(0x4C, FINALLY_003e);
		}

IL_002d:
		{
			bool L_5 = Enumerator_MoveNext_m1464264581((&V_1), /*hidden argument*/Enumerator_MoveNext_m1464264581_MethodInfo_var);
			if (L_5)
			{
				goto IL_000c;
			}
		}

IL_0039:
		{
			IL2CPP_LEAVE(0x4A, FINALLY_003e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003e;
	}

FINALLY_003e:
	{ // begin finally (depth: 1)
		Enumerator_t1742395189  L_6 = V_1;
		Enumerator_t1742395189  L_7 = L_6;
		Il2CppObject * L_8 = Box(Enumerator_t1742395189_il2cpp_TypeInfo_var, &L_7);
		NullCheck((Il2CppObject *)L_8);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
		IL2CPP_END_FINALLY(62)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(62)
	{
		IL2CPP_JUMP_TBL(0x4C, IL_004c)
		IL2CPP_JUMP_TBL(0x4A, IL_004a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_004a:
	{
		return (bool)0;
	}

IL_004c:
	{
		bool L_9 = V_2;
		return L_9;
	}
}
// System.Object Mono.Xml.DTDCollectionBase::BaseGet(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t1742395189_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m4006862359_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4070407275_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m1956205848_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m1416717449_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1464264581_MethodInfo_var;
extern const uint32_t DTDCollectionBase_BaseGet_m344774896_MetadataUsageId;
extern "C"  Il2CppObject * DTDCollectionBase_BaseGet_m344774896 (DTDCollectionBase_t3975886631 * __this, String_t* ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDCollectionBase_BaseGet_m344774896_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2_t2859653228  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Enumerator_t1742395189  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Il2CppObject * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Enumerator_t1742395189  L_0 = List_1_GetEnumerator_m4006862359(__this, /*hidden argument*/List_1_GetEnumerator_m4006862359_MethodInfo_var);
		V_1 = L_0;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0033;
		}

IL_000c:
		{
			KeyValuePair_2_t2859653228  L_1 = Enumerator_get_Current_m4070407275((&V_1), /*hidden argument*/Enumerator_get_Current_m4070407275_MethodInfo_var);
			V_0 = L_1;
			String_t* L_2 = KeyValuePair_2_get_Key_m1956205848((&V_0), /*hidden argument*/KeyValuePair_2_get_Key_m1956205848_MethodInfo_var);
			String_t* L_3 = ___name0;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_4 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
			if (!L_4)
			{
				goto IL_0033;
			}
		}

IL_0026:
		{
			DTDNode_t1733424026 * L_5 = KeyValuePair_2_get_Value_m1416717449((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m1416717449_MethodInfo_var);
			V_2 = L_5;
			IL2CPP_LEAVE(0x52, FINALLY_0044);
		}

IL_0033:
		{
			bool L_6 = Enumerator_MoveNext_m1464264581((&V_1), /*hidden argument*/Enumerator_MoveNext_m1464264581_MethodInfo_var);
			if (L_6)
			{
				goto IL_000c;
			}
		}

IL_003f:
		{
			IL2CPP_LEAVE(0x50, FINALLY_0044);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0044;
	}

FINALLY_0044:
	{ // begin finally (depth: 1)
		Enumerator_t1742395189  L_7 = V_1;
		Enumerator_t1742395189  L_8 = L_7;
		Il2CppObject * L_9 = Box(Enumerator_t1742395189_il2cpp_TypeInfo_var, &L_8);
		NullCheck((Il2CppObject *)L_9);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_9);
		IL2CPP_END_FINALLY(68)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(68)
	{
		IL2CPP_JUMP_TBL(0x52, IL_0052)
		IL2CPP_JUMP_TBL(0x50, IL_0050)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0050:
	{
		return NULL;
	}

IL_0052:
	{
		Il2CppObject * L_10 = V_2;
		return L_10;
	}
}
// System.Void Mono.Xml.DTDContentModel::.ctor(Mono.Xml.DTDObjectModel,System.String)
extern Il2CppClass* DTDContentModelCollection_t605667718_il2cpp_TypeInfo_var;
extern const uint32_t DTDContentModel__ctor_m248936723_MetadataUsageId;
extern "C"  void DTDContentModel__ctor_m248936723 (DTDContentModel_t1462496200 * __this, DTDObjectModel_t709926554 * ___root0, String_t* ___ownerElementName1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDContentModel__ctor_m248936723_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DTDContentModelCollection_t605667718 * L_0 = (DTDContentModelCollection_t605667718 *)il2cpp_codegen_object_new(DTDContentModelCollection_t605667718_il2cpp_TypeInfo_var);
		DTDContentModelCollection__ctor_m1422793605(L_0, /*hidden argument*/NULL);
		__this->set_childModels_9(L_0);
		DTDNode__ctor_m2554517745(__this, /*hidden argument*/NULL);
		DTDObjectModel_t709926554 * L_1 = ___root0;
		__this->set_root_5(L_1);
		String_t* L_2 = ___ownerElementName1;
		__this->set_ownerElementName_6(L_2);
		return;
	}
}
// Mono.Xml.DTDContentModelCollection Mono.Xml.DTDContentModel::get_ChildModels()
extern "C"  DTDContentModelCollection_t605667718 * DTDContentModel_get_ChildModels_m1642723597 (DTDContentModel_t1462496200 * __this, const MethodInfo* method)
{
	{
		DTDContentModelCollection_t605667718 * L_0 = __this->get_childModels_9();
		return L_0;
	}
}
// System.String Mono.Xml.DTDContentModel::get_ElementName()
extern "C"  String_t* DTDContentModel_get_ElementName_m461002204 (DTDContentModel_t1462496200 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_elementName_7();
		return L_0;
	}
}
// System.Void Mono.Xml.DTDContentModel::set_ElementName(System.String)
extern "C"  void DTDContentModel_set_ElementName_m4279929463 (DTDContentModel_t1462496200 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_elementName_7(L_0);
		return;
	}
}
// System.Void Mono.Xml.DTDContentModel::set_Occurence(Mono.Xml.DTDOccurence)
extern "C"  void DTDContentModel_set_Occurence_m1938639474 (DTDContentModel_t1462496200 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_occurence_10(L_0);
		return;
	}
}
// Mono.Xml.DTDContentOrderType Mono.Xml.DTDContentModel::get_OrderType()
extern "C"  int32_t DTDContentModel_get_OrderType_m2382989904 (DTDContentModel_t1462496200 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_orderType_8();
		return L_0;
	}
}
// System.Void Mono.Xml.DTDContentModel::set_OrderType(Mono.Xml.DTDContentOrderType)
extern "C"  void DTDContentModel_set_OrderType_m2020937195 (DTDContentModel_t1462496200 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_orderType_8(L_0);
		return;
	}
}
// System.Void Mono.Xml.DTDContentModelCollection::.ctor()
extern Il2CppClass* ArrayList_t2121638921_il2cpp_TypeInfo_var;
extern const uint32_t DTDContentModelCollection__ctor_m1422793605_MetadataUsageId;
extern "C"  void DTDContentModelCollection__ctor_m1422793605 (DTDContentModelCollection_t605667718 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDContentModelCollection__ctor_m1422793605_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ArrayList_t2121638921 * L_0 = (ArrayList_t2121638921 *)il2cpp_codegen_object_new(ArrayList_t2121638921_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1878432947(L_0, /*hidden argument*/NULL);
		__this->set_contentModel_0(L_0);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// Mono.Xml.DTDContentModel Mono.Xml.DTDContentModelCollection::get_Item(System.Int32)
extern Il2CppClass* DTDContentModel_t1462496200_il2cpp_TypeInfo_var;
extern const uint32_t DTDContentModelCollection_get_Item_m3127043259_MetadataUsageId;
extern "C"  DTDContentModel_t1462496200 * DTDContentModelCollection_get_Item_m3127043259 (DTDContentModelCollection_t605667718 * __this, int32_t ___i0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDContentModelCollection_get_Item_m3127043259_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ArrayList_t2121638921 * L_0 = __this->get_contentModel_0();
		int32_t L_1 = ___i0;
		NullCheck(L_0);
		Il2CppObject * L_2 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_0, L_1);
		return ((DTDContentModel_t1462496200 *)IsInstClass(L_2, DTDContentModel_t1462496200_il2cpp_TypeInfo_var));
	}
}
// System.Int32 Mono.Xml.DTDContentModelCollection::get_Count()
extern "C"  int32_t DTDContentModelCollection_get_Count_m1800755431 (DTDContentModelCollection_t605667718 * __this, const MethodInfo* method)
{
	{
		ArrayList_t2121638921 * L_0 = __this->get_contentModel_0();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_0);
		return L_1;
	}
}
// System.Void Mono.Xml.DTDContentModelCollection::Add(Mono.Xml.DTDContentModel)
extern "C"  void DTDContentModelCollection_Add_m3413500450 (DTDContentModelCollection_t605667718 * __this, DTDContentModel_t1462496200 * ___model0, const MethodInfo* method)
{
	{
		ArrayList_t2121638921 * L_0 = __this->get_contentModel_0();
		DTDContentModel_t1462496200 * L_1 = ___model0;
		NullCheck(L_0);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_0, L_1);
		return;
	}
}
// System.Void Mono.Xml.DTDElementDeclaration::.ctor(Mono.Xml.DTDObjectModel)
extern "C"  void DTDElementDeclaration__ctor_m3951886277 (DTDElementDeclaration_t95357814 * __this, DTDObjectModel_t709926554 * ___root0, const MethodInfo* method)
{
	{
		DTDNode__ctor_m2554517745(__this, /*hidden argument*/NULL);
		DTDObjectModel_t709926554 * L_0 = ___root0;
		__this->set_root_5(L_0);
		return;
	}
}
// System.String Mono.Xml.DTDElementDeclaration::get_Name()
extern "C"  String_t* DTDElementDeclaration_get_Name_m1111866310 (DTDElementDeclaration_t95357814 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_name_7();
		return L_0;
	}
}
// System.Void Mono.Xml.DTDElementDeclaration::set_Name(System.String)
extern "C"  void DTDElementDeclaration_set_Name_m3252098059 (DTDElementDeclaration_t95357814 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_name_7(L_0);
		return;
	}
}
// System.Void Mono.Xml.DTDElementDeclaration::set_IsEmpty(System.Boolean)
extern "C"  void DTDElementDeclaration_set_IsEmpty_m370233552 (DTDElementDeclaration_t95357814 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_isEmpty_8(L_0);
		return;
	}
}
// System.Void Mono.Xml.DTDElementDeclaration::set_IsAny(System.Boolean)
extern "C"  void DTDElementDeclaration_set_IsAny_m2296438319 (DTDElementDeclaration_t95357814 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_isAny_9(L_0);
		return;
	}
}
// System.Void Mono.Xml.DTDElementDeclaration::set_IsMixedContent(System.Boolean)
extern "C"  void DTDElementDeclaration_set_IsMixedContent_m3652418955 (DTDElementDeclaration_t95357814 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_isMixedContent_10(L_0);
		return;
	}
}
// Mono.Xml.DTDContentModel Mono.Xml.DTDElementDeclaration::get_ContentModel()
extern Il2CppClass* DTDContentModel_t1462496200_il2cpp_TypeInfo_var;
extern const uint32_t DTDElementDeclaration_get_ContentModel_m3021018039_MetadataUsageId;
extern "C"  DTDContentModel_t1462496200 * DTDElementDeclaration_get_ContentModel_m3021018039 (DTDElementDeclaration_t95357814 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDElementDeclaration_get_ContentModel_m3021018039_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DTDContentModel_t1462496200 * L_0 = __this->get_contentModel_6();
		if (L_0)
		{
			goto IL_0022;
		}
	}
	{
		DTDObjectModel_t709926554 * L_1 = __this->get_root_5();
		String_t* L_2 = DTDElementDeclaration_get_Name_m1111866310(__this, /*hidden argument*/NULL);
		DTDContentModel_t1462496200 * L_3 = (DTDContentModel_t1462496200 *)il2cpp_codegen_object_new(DTDContentModel_t1462496200_il2cpp_TypeInfo_var);
		DTDContentModel__ctor_m248936723(L_3, L_1, L_2, /*hidden argument*/NULL);
		__this->set_contentModel_6(L_3);
	}

IL_0022:
	{
		DTDContentModel_t1462496200 * L_4 = __this->get_contentModel_6();
		return L_4;
	}
}
// System.Void Mono.Xml.DTDElementDeclarationCollection::.ctor(Mono.Xml.DTDObjectModel)
extern "C"  void DTDElementDeclarationCollection__ctor_m879563011 (DTDElementDeclarationCollection_t2951978932 * __this, DTDObjectModel_t709926554 * ___root0, const MethodInfo* method)
{
	{
		DTDObjectModel_t709926554 * L_0 = ___root0;
		DTDCollectionBase__ctor_m1839072630(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.DTDElementDeclarationCollection::Add(System.String,Mono.Xml.DTDElementDeclaration)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlException_t3490696160_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4256492166;
extern const uint32_t DTDElementDeclarationCollection_Add_m2137189962_MetadataUsageId;
extern "C"  void DTDElementDeclarationCollection_Add_m2137189962 (DTDElementDeclarationCollection_t2951978932 * __this, String_t* ___name0, DTDElementDeclaration_t95357814 * ___decl1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDElementDeclarationCollection_Add_m2137189962_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name0;
		bool L_1 = DTDCollectionBase_Contains_m1398227387(__this, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0029;
		}
	}
	{
		DTDObjectModel_t709926554 * L_2 = DTDCollectionBase_get_Root_m1145982982(__this, /*hidden argument*/NULL);
		String_t* L_3 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral4256492166, L_3, /*hidden argument*/NULL);
		XmlException_t3490696160 * L_5 = (XmlException_t3490696160 *)il2cpp_codegen_object_new(XmlException_t3490696160_il2cpp_TypeInfo_var);
		XmlException__ctor_m1015471719(L_5, L_4, (Exception_t1967233988 *)NULL, /*hidden argument*/NULL);
		NullCheck(L_2);
		DTDObjectModel_AddError_m2755935394(L_2, L_5, /*hidden argument*/NULL);
		return;
	}

IL_0029:
	{
		DTDElementDeclaration_t95357814 * L_6 = ___decl1;
		DTDObjectModel_t709926554 * L_7 = DTDCollectionBase_get_Root_m1145982982(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		DTDNode_SetRoot_m970259879(L_6, L_7, /*hidden argument*/NULL);
		String_t* L_8 = ___name0;
		DTDElementDeclaration_t95357814 * L_9 = ___decl1;
		DTDCollectionBase_BaseAdd_m1540252226(__this, L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.DTDEntityBase::.ctor(Mono.Xml.DTDObjectModel)
extern "C"  void DTDEntityBase__ctor_m3123258427 (DTDEntityBase_t1395379180 * __this, DTDObjectModel_t709926554 * ___root0, const MethodInfo* method)
{
	{
		DTDNode__ctor_m2554517745(__this, /*hidden argument*/NULL);
		DTDObjectModel_t709926554 * L_0 = ___root0;
		DTDNode_SetRoot_m970259879(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Mono.Xml.DTDEntityBase::get_IsInvalid()
extern "C"  bool DTDEntityBase_get_IsInvalid_m1263955641 (DTDEntityBase_t1395379180 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_isInvalid_12();
		return L_0;
	}
}
// System.Void Mono.Xml.DTDEntityBase::set_LoadFailed(System.Boolean)
extern "C"  void DTDEntityBase_set_LoadFailed_m2990733308 (DTDEntityBase_t1395379180 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_loadFailed_13(L_0);
		return;
	}
}
// System.String Mono.Xml.DTDEntityBase::get_Name()
extern "C"  String_t* DTDEntityBase_get_Name_m1635306044 (DTDEntityBase_t1395379180 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_name_5();
		return L_0;
	}
}
// System.Void Mono.Xml.DTDEntityBase::set_Name(System.String)
extern "C"  void DTDEntityBase_set_Name_m1787521109 (DTDEntityBase_t1395379180 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_name_5(L_0);
		return;
	}
}
// System.String Mono.Xml.DTDEntityBase::get_PublicId()
extern "C"  String_t* DTDEntityBase_get_PublicId_m4078602965 (DTDEntityBase_t1395379180 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_publicId_6();
		return L_0;
	}
}
// System.Void Mono.Xml.DTDEntityBase::set_PublicId(System.String)
extern "C"  void DTDEntityBase_set_PublicId_m977897692 (DTDEntityBase_t1395379180 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_publicId_6(L_0);
		return;
	}
}
// System.String Mono.Xml.DTDEntityBase::get_SystemId()
extern "C"  String_t* DTDEntityBase_get_SystemId_m2816997275 (DTDEntityBase_t1395379180 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_systemId_7();
		return L_0;
	}
}
// System.Void Mono.Xml.DTDEntityBase::set_SystemId(System.String)
extern "C"  void DTDEntityBase_set_SystemId_m3174403542 (DTDEntityBase_t1395379180 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_systemId_7(L_0);
		return;
	}
}
// System.String Mono.Xml.DTDEntityBase::get_LiteralEntityValue()
extern "C"  String_t* DTDEntityBase_get_LiteralEntityValue_m3210771920 (DTDEntityBase_t1395379180 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_literalValue_8();
		return L_0;
	}
}
// System.Void Mono.Xml.DTDEntityBase::set_LiteralEntityValue(System.String)
extern "C"  void DTDEntityBase_set_LiteralEntityValue_m256564993 (DTDEntityBase_t1395379180 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_literalValue_8(L_0);
		return;
	}
}
// System.String Mono.Xml.DTDEntityBase::get_ReplacementText()
extern "C"  String_t* DTDEntityBase_get_ReplacementText_m3411513392 (DTDEntityBase_t1395379180 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_replacementText_9();
		return L_0;
	}
}
// System.Void Mono.Xml.DTDEntityBase::set_ReplacementText(System.String)
extern "C"  void DTDEntityBase_set_ReplacementText_m1535392611 (DTDEntityBase_t1395379180 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_replacementText_9(L_0);
		return;
	}
}
// System.Void Mono.Xml.DTDEntityBase::set_XmlResolver(System.Xml.XmlResolver)
extern "C"  void DTDEntityBase_set_XmlResolver_m2788162230 (DTDEntityBase_t1395379180 * __this, XmlResolver_t2502213349 * ___value0, const MethodInfo* method)
{
	{
		XmlResolver_t2502213349 * L_0 = ___value0;
		__this->set_resolver_14(L_0);
		return;
	}
}
// System.String Mono.Xml.DTDEntityBase::get_ActualUri()
extern Il2CppClass* Uri_t2776692961_il2cpp_TypeInfo_var;
extern Il2CppClass* UriFormatException_t1145000641_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t DTDEntityBase_get_ActualUri_m104526607_MetadataUsageId;
extern "C"  String_t* DTDEntityBase_get_ActualUri_m104526607 (DTDEntityBase_t1395379180 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDEntityBase_get_ActualUri_m104526607_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Uri_t2776692961 * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	DTDEntityBase_t1395379180 * G_B13_0 = NULL;
	DTDEntityBase_t1395379180 * G_B12_0 = NULL;
	String_t* G_B14_0 = NULL;
	DTDEntityBase_t1395379180 * G_B14_1 = NULL;
	{
		String_t* L_0 = __this->get_uriString_10();
		if (L_0)
		{
			goto IL_00bb;
		}
	}
	{
		XmlResolver_t2502213349 * L_1 = __this->get_resolver_14();
		if (!L_1)
		{
			goto IL_0031;
		}
	}
	{
		String_t* L_2 = DTDEntityBase_get_SystemId_m2816997275(__this, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0031;
		}
	}
	{
		String_t* L_3 = DTDEntityBase_get_SystemId_m2816997275(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = String_get_Length_m2979997331(L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0042;
		}
	}

IL_0031:
	{
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String Mono.Xml.DTDNode::get_BaseURI() */, __this);
		__this->set_uriString_10(L_5);
		goto IL_00bb;
	}

IL_0042:
	{
		V_0 = (Uri_t2776692961 *)NULL;
	}

IL_0044:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String Mono.Xml.DTDNode::get_BaseURI() */, __this);
			if (!L_6)
			{
				goto IL_006c;
			}
		}

IL_004f:
		{
			String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String Mono.Xml.DTDNode::get_BaseURI() */, __this);
			NullCheck(L_7);
			int32_t L_8 = String_get_Length_m2979997331(L_7, /*hidden argument*/NULL);
			if ((((int32_t)L_8) <= ((int32_t)0)))
			{
				goto IL_006c;
			}
		}

IL_0060:
		{
			String_t* L_9 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String Mono.Xml.DTDNode::get_BaseURI() */, __this);
			Uri_t2776692961 * L_10 = (Uri_t2776692961 *)il2cpp_codegen_object_new(Uri_t2776692961_il2cpp_TypeInfo_var);
			Uri__ctor_m1721267859(L_10, L_9, /*hidden argument*/NULL);
			V_0 = L_10;
		}

IL_006c:
		{
			goto IL_0077;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (UriFormatException_t1145000641_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0071;
		throw e;
	}

CATCH_0071:
	{ // begin catch(System.UriFormatException)
		goto IL_0077;
	} // end catch (depth: 1)

IL_0077:
	{
		XmlResolver_t2502213349 * L_11 = __this->get_resolver_14();
		Uri_t2776692961 * L_12 = V_0;
		String_t* L_13 = DTDEntityBase_get_SystemId_m2816997275(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Uri_t2776692961 * L_14 = VirtFuncInvoker2< Uri_t2776692961 *, Uri_t2776692961 *, String_t* >::Invoke(5 /* System.Uri System.Xml.XmlResolver::ResolveUri(System.Uri,System.String) */, L_11, L_12, L_13);
		__this->set_absUri_11(L_14);
		Uri_t2776692961 * L_15 = __this->get_absUri_11();
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t2776692961_il2cpp_TypeInfo_var);
		bool L_16 = Uri_op_Inequality_m2899852498(NULL /*static, unused*/, L_15, (Uri_t2776692961 *)NULL, /*hidden argument*/NULL);
		G_B12_0 = __this;
		if (!L_16)
		{
			G_B13_0 = __this;
			goto IL_00b1;
		}
	}
	{
		Uri_t2776692961 * L_17 = __this->get_absUri_11();
		NullCheck(L_17);
		String_t* L_18 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Uri::ToString() */, L_17);
		G_B14_0 = L_18;
		G_B14_1 = G_B12_0;
		goto IL_00b6;
	}

IL_00b1:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B14_0 = L_19;
		G_B14_1 = G_B13_0;
	}

IL_00b6:
	{
		NullCheck(G_B14_1);
		G_B14_1->set_uriString_10(G_B14_0);
	}

IL_00bb:
	{
		String_t* L_20 = __this->get_uriString_10();
		return L_20;
	}
}
// System.Void Mono.Xml.DTDEntityBase::Resolve()
extern const Il2CppType* Stream_t219029575_0_0_0_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Stream_t219029575_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlTextReader_t3066586409_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2100152231;
extern const uint32_t DTDEntityBase_Resolve_m3364394729_MetadataUsageId;
extern "C"  void DTDEntityBase_Resolve_m3364394729 (DTDEntityBase_t1395379180 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDEntityBase_Resolve_m3364394729_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Stream_t219029575 * V_0 = NULL;
	XmlTextReader_t3066586409 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = DTDEntityBase_get_ActualUri_m104526607(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_2 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		DTDEntityBase_set_LoadFailed_m2990733308(__this, (bool)1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		DTDEntityBase_set_LiteralEntityValue_m256564993(__this, L_3, /*hidden argument*/NULL);
		return;
	}

IL_0028:
	{
		DTDObjectModel_t709926554 * L_4 = DTDNode_get_Root_m1036806329(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Hashtable_t3875263730 * L_5 = DTDObjectModel_get_ExternalResources_m2105766529(L_4, /*hidden argument*/NULL);
		String_t* L_6 = DTDEntityBase_get_ActualUri_m104526607(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_7 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_5, L_6);
		if (!L_7)
		{
			goto IL_0064;
		}
	}
	{
		DTDObjectModel_t709926554 * L_8 = DTDNode_get_Root_m1036806329(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Hashtable_t3875263730 * L_9 = DTDObjectModel_get_ExternalResources_m2105766529(L_8, /*hidden argument*/NULL);
		String_t* L_10 = DTDEntityBase_get_ActualUri_m104526607(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Il2CppObject * L_11 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_9, L_10);
		DTDEntityBase_set_LiteralEntityValue_m256564993(__this, ((String_t*)CastclassSealed(L_11, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
	}

IL_0064:
	{
		V_0 = (Stream_t219029575 *)NULL;
	}

IL_0066:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			{
				XmlResolver_t2502213349 * L_12 = __this->get_resolver_14();
				Uri_t2776692961 * L_13 = __this->get_absUri_11();
				IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
				Type_t * L_14 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Stream_t219029575_0_0_0_var), /*hidden argument*/NULL);
				NullCheck(L_12);
				Il2CppObject * L_15 = VirtFuncInvoker3< Il2CppObject *, Uri_t2776692961 *, String_t*, Type_t * >::Invoke(4 /* System.Object System.Xml.XmlResolver::GetEntity(System.Uri,System.String,System.Type) */, L_12, L_13, (String_t*)NULL, L_14);
				V_0 = ((Stream_t219029575 *)IsInstClass(L_15, Stream_t219029575_il2cpp_TypeInfo_var));
				String_t* L_16 = DTDEntityBase_get_ActualUri_m104526607(__this, /*hidden argument*/NULL);
				Stream_t219029575 * L_17 = V_0;
				DTDObjectModel_t709926554 * L_18 = DTDNode_get_Root_m1036806329(__this, /*hidden argument*/NULL);
				NullCheck(L_18);
				XmlNameTable_t3232213908 * L_19 = DTDObjectModel_get_NameTable_m2752706194(L_18, /*hidden argument*/NULL);
				XmlTextReader_t3066586409 * L_20 = (XmlTextReader_t3066586409 *)il2cpp_codegen_object_new(XmlTextReader_t3066586409_il2cpp_TypeInfo_var);
				XmlTextReader__ctor_m2102759687(L_20, L_16, L_17, L_19, /*hidden argument*/NULL);
				V_1 = L_20;
				XmlTextReader_t3066586409 * L_21 = V_1;
				NullCheck(L_21);
				TextReader_t1534522647 * L_22 = XmlTextReader_GetRemainder_m318592377(L_21, /*hidden argument*/NULL);
				NullCheck(L_22);
				String_t* L_23 = VirtFuncInvoker0< String_t* >::Invoke(11 /* System.String System.IO.TextReader::ReadToEnd() */, L_22);
				DTDEntityBase_set_LiteralEntityValue_m256564993(__this, L_23, /*hidden argument*/NULL);
				DTDObjectModel_t709926554 * L_24 = DTDNode_get_Root_m1036806329(__this, /*hidden argument*/NULL);
				NullCheck(L_24);
				Hashtable_t3875263730 * L_25 = DTDObjectModel_get_ExternalResources_m2105766529(L_24, /*hidden argument*/NULL);
				String_t* L_26 = DTDEntityBase_get_ActualUri_m104526607(__this, /*hidden argument*/NULL);
				String_t* L_27 = DTDEntityBase_get_LiteralEntityValue_m3210771920(__this, /*hidden argument*/NULL);
				NullCheck(L_25);
				VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_25, L_26, L_27);
				DTDObjectModel_t709926554 * L_28 = DTDNode_get_Root_m1036806329(__this, /*hidden argument*/NULL);
				NullCheck(L_28);
				Hashtable_t3875263730 * L_29 = DTDObjectModel_get_ExternalResources_m2105766529(L_28, /*hidden argument*/NULL);
				NullCheck(L_29);
				int32_t L_30 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.Hashtable::get_Count() */, L_29);
				if ((((int32_t)L_30) <= ((int32_t)((int32_t)256))))
				{
					goto IL_00f2;
				}
			}

IL_00e7:
			{
				InvalidOperationException_t2420574324 * L_31 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
				InvalidOperationException__ctor_m1485483280(L_31, _stringLiteral2100152231, /*hidden argument*/NULL);
				IL2CPP_RAISE_MANAGED_EXCEPTION(L_31);
			}

IL_00f2:
			{
				IL2CPP_LEAVE(0x11C, FINALLY_010f);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1967233988 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_00f7;
			throw e;
		}

CATCH_00f7:
		{ // begin catch(System.Exception)
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_32 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
			DTDEntityBase_set_LiteralEntityValue_m256564993(__this, L_32, /*hidden argument*/NULL);
			DTDEntityBase_set_LoadFailed_m2990733308(__this, (bool)1, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x11C, FINALLY_010f);
		} // end catch (depth: 2)
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_010f;
	}

FINALLY_010f:
	{ // begin finally (depth: 1)
		{
			Stream_t219029575 * L_33 = V_0;
			if (!L_33)
			{
				goto IL_011b;
			}
		}

IL_0115:
		{
			Stream_t219029575 * L_34 = V_0;
			NullCheck(L_34);
			VirtActionInvoker0::Invoke(13 /* System.Void System.IO.Stream::Close() */, L_34);
		}

IL_011b:
		{
			IL2CPP_END_FINALLY(271)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(271)
	{
		IL2CPP_JUMP_TBL(0x11C, IL_011c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_011c:
	{
		return;
	}
}
// System.Void Mono.Xml.DTDEntityDeclaration::.ctor(Mono.Xml.DTDObjectModel)
extern Il2CppClass* ArrayList_t2121638921_il2cpp_TypeInfo_var;
extern const uint32_t DTDEntityDeclaration__ctor_m705712862_MetadataUsageId;
extern "C"  void DTDEntityDeclaration__ctor_m705712862 (DTDEntityDeclaration_t2806387719 * __this, DTDObjectModel_t709926554 * ___root0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDEntityDeclaration__ctor_m705712862_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ArrayList_t2121638921 * L_0 = (ArrayList_t2121638921 *)il2cpp_codegen_object_new(ArrayList_t2121638921_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1878432947(L_0, /*hidden argument*/NULL);
		__this->set_ReferencingEntities_17(L_0);
		DTDObjectModel_t709926554 * L_1 = ___root0;
		DTDEntityBase__ctor_m3123258427(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String Mono.Xml.DTDEntityDeclaration::get_NotationName()
extern "C"  String_t* DTDEntityDeclaration_get_NotationName_m528268507 (DTDEntityDeclaration_t2806387719 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_notationName_16();
		return L_0;
	}
}
// System.Void Mono.Xml.DTDEntityDeclaration::set_NotationName(System.String)
extern "C"  void DTDEntityDeclaration_set_NotationName_m2884802480 (DTDEntityDeclaration_t2806387719 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_notationName_16(L_0);
		return;
	}
}
// System.Boolean Mono.Xml.DTDEntityDeclaration::get_HasExternalReference()
extern Il2CppClass* ArrayList_t2121638921_il2cpp_TypeInfo_var;
extern const uint32_t DTDEntityDeclaration_get_HasExternalReference_m850645615_MetadataUsageId;
extern "C"  bool DTDEntityDeclaration_get_HasExternalReference_m850645615 (DTDEntityDeclaration_t2806387719 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDEntityDeclaration_get_HasExternalReference_m850645615_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = __this->get_scanned_18();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		ArrayList_t2121638921 * L_1 = (ArrayList_t2121638921 *)il2cpp_codegen_object_new(ArrayList_t2121638921_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1878432947(L_1, /*hidden argument*/NULL);
		DTDEntityDeclaration_ScanEntityValue_m3595758284(__this, L_1, /*hidden argument*/NULL);
	}

IL_0016:
	{
		bool L_2 = __this->get_hasExternalReference_20();
		return L_2;
	}
}
// System.String Mono.Xml.DTDEntityDeclaration::get_EntityValue()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArrayList_t2121638921_il2cpp_TypeInfo_var;
extern const uint32_t DTDEntityDeclaration_get_EntityValue_m1737901506_MetadataUsageId;
extern "C"  String_t* DTDEntityDeclaration_get_EntityValue_m1737901506 (DTDEntityDeclaration_t2806387719 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDEntityDeclaration_get_EntityValue_m1737901506_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = DTDEntityBase_get_IsInvalid_m1263955641(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_1;
	}

IL_0011:
	{
		String_t* L_2 = DTDEntityBase_get_PublicId_m4078602965(__this, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0038;
		}
	}
	{
		String_t* L_3 = DTDEntityBase_get_SystemId_m2816997275(__this, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0038;
		}
	}
	{
		String_t* L_4 = DTDEntityBase_get_LiteralEntityValue_m3210771920(__this, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0038;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_5;
	}

IL_0038:
	{
		String_t* L_6 = __this->get_entityValue_15();
		if (L_6)
		{
			goto IL_00bc;
		}
	}
	{
		String_t* L_7 = DTDEntityDeclaration_get_NotationName_m528268507(__this, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_005e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_entityValue_15(L_8);
		goto IL_00b1;
	}

IL_005e:
	{
		String_t* L_9 = DTDEntityBase_get_SystemId_m2816997275(__this, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_007e;
		}
	}
	{
		String_t* L_10 = DTDEntityBase_get_SystemId_m2816997275(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_12 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_00a5;
		}
	}

IL_007e:
	{
		String_t* L_13 = DTDEntityBase_get_ReplacementText_m3411513392(__this, /*hidden argument*/NULL);
		__this->set_entityValue_15(L_13);
		String_t* L_14 = __this->get_entityValue_15();
		if (L_14)
		{
			goto IL_00a0;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_entityValue_15(L_15);
	}

IL_00a0:
	{
		goto IL_00b1;
	}

IL_00a5:
	{
		String_t* L_16 = DTDEntityBase_get_ReplacementText_m3411513392(__this, /*hidden argument*/NULL);
		__this->set_entityValue_15(L_16);
	}

IL_00b1:
	{
		ArrayList_t2121638921 * L_17 = (ArrayList_t2121638921 *)il2cpp_codegen_object_new(ArrayList_t2121638921_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1878432947(L_17, /*hidden argument*/NULL);
		DTDEntityDeclaration_ScanEntityValue_m3595758284(__this, L_17, /*hidden argument*/NULL);
	}

IL_00bc:
	{
		String_t* L_18 = __this->get_entityValue_15();
		return L_18;
	}
}
// System.Void Mono.Xml.DTDEntityDeclaration::ScanEntityValue(System.Collections.ArrayList)
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlChar_t3591879093_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlException_t3490696160_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4118806910;
extern Il2CppCodeGenString* _stringLiteral1765786629;
extern Il2CppCodeGenString* _stringLiteral1678006811;
extern Il2CppCodeGenString* _stringLiteral681472551;
extern const uint32_t DTDEntityDeclaration_ScanEntityValue_m3595758284_MetadataUsageId;
extern "C"  void DTDEntityDeclaration_ScanEntityValue_m3595758284 (DTDEntityDeclaration_t2806387719 * __this, ArrayList_t2121638921 * ___refs0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDEntityDeclaration_ScanEntityValue_m3595758284_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	String_t* V_6 = NULL;
	DTDEntityDeclaration_t2806387719 * V_7 = NULL;
	String_t* V_8 = NULL;
	Il2CppObject * V_9 = NULL;
	Il2CppObject * V_10 = NULL;
	uint16_t V_11 = 0x0;
	Il2CppObject * V_12 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = DTDEntityDeclaration_get_EntityValue_m1737901506(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = DTDEntityBase_get_SystemId_m2816997275(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		__this->set_hasExternalReference_20((bool)1);
	}

IL_0019:
	{
		bool L_2 = __this->get_recursed_19();
		if (!L_2)
		{
			goto IL_0030;
		}
	}
	{
		XmlException_t3490696160 * L_3 = DTDNode_NotWFError_m2314023332(__this, _stringLiteral4118806910, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0030:
	{
		__this->set_recursed_19((bool)1);
		bool L_4 = __this->get_scanned_18();
		if (!L_4)
		{
			goto IL_00b0;
		}
	}
	{
		ArrayList_t2121638921 * L_5 = ___refs0;
		NullCheck(L_5);
		Il2CppObject * L_6 = VirtFuncInvoker0< Il2CppObject * >::Invoke(43 /* System.Collections.IEnumerator System.Collections.ArrayList::GetEnumerator() */, L_5);
		V_2 = L_6;
	}

IL_0049:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0083;
		}

IL_004e:
		{
			Il2CppObject * L_7 = V_2;
			NullCheck(L_7);
			Il2CppObject * L_8 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_7);
			V_1 = ((String_t*)CastclassSealed(L_8, String_t_il2cpp_TypeInfo_var));
			ArrayList_t2121638921 * L_9 = __this->get_ReferencingEntities_17();
			String_t* L_10 = V_1;
			NullCheck(L_9);
			bool L_11 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(32 /* System.Boolean System.Collections.ArrayList::Contains(System.Object) */, L_9, L_10);
			if (!L_11)
			{
				goto IL_0083;
			}
		}

IL_006b:
		{
			String_t* L_12 = V_1;
			String_t* L_13 = DTDEntityBase_get_Name_m1635306044(__this, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_14 = String_Format_m2398979370(NULL /*static, unused*/, _stringLiteral1765786629, L_12, L_13, /*hidden argument*/NULL);
			XmlException_t3490696160 * L_15 = DTDNode_NotWFError_m2314023332(__this, L_14, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_15);
		}

IL_0083:
		{
			Il2CppObject * L_16 = V_2;
			NullCheck(L_16);
			bool L_17 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_16);
			if (L_17)
			{
				goto IL_004e;
			}
		}

IL_008e:
		{
			IL2CPP_LEAVE(0xA8, FINALLY_0093);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0093;
	}

FINALLY_0093:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_18 = V_2;
			V_10 = ((Il2CppObject *)IsInst(L_18, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			Il2CppObject * L_19 = V_10;
			if (L_19)
			{
				goto IL_00a0;
			}
		}

IL_009f:
		{
			IL2CPP_END_FINALLY(147)
		}

IL_00a0:
		{
			Il2CppObject * L_20 = V_10;
			NullCheck(L_20);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_20);
			IL2CPP_END_FINALLY(147)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(147)
	{
		IL2CPP_JUMP_TBL(0xA8, IL_00a8)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00a8:
	{
		__this->set_recursed_19((bool)0);
		return;
	}

IL_00b0:
	{
		String_t* L_21 = V_0;
		NullCheck(L_21);
		int32_t L_22 = String_get_Length_m2979997331(L_21, /*hidden argument*/NULL);
		V_3 = L_22;
		V_4 = 0;
		V_5 = 0;
		goto IL_0243;
	}

IL_00c2:
	{
		String_t* L_23 = V_0;
		int32_t L_24 = V_5;
		NullCheck(L_23);
		uint16_t L_25 = String_get_Chars_m3015341861(L_23, L_24, /*hidden argument*/NULL);
		V_11 = L_25;
		uint16_t L_26 = V_11;
		if ((((int32_t)L_26) == ((int32_t)((int32_t)38))))
		{
			goto IL_00e3;
		}
	}
	{
		uint16_t L_27 = V_11;
		if ((((int32_t)L_27) == ((int32_t)((int32_t)59))))
		{
			goto IL_00ee;
		}
	}
	{
		goto IL_023d;
	}

IL_00e3:
	{
		int32_t L_28 = V_5;
		V_4 = ((int32_t)((int32_t)L_28+(int32_t)1));
		goto IL_023d;
	}

IL_00ee:
	{
		int32_t L_29 = V_4;
		if (L_29)
		{
			goto IL_00fa;
		}
	}
	{
		goto IL_023d;
	}

IL_00fa:
	{
		String_t* L_30 = V_0;
		int32_t L_31 = V_4;
		int32_t L_32 = V_5;
		int32_t L_33 = V_4;
		NullCheck(L_30);
		String_t* L_34 = String_Substring_m675079568(L_30, L_31, ((int32_t)((int32_t)L_32-(int32_t)L_33)), /*hidden argument*/NULL);
		V_6 = L_34;
		String_t* L_35 = V_6;
		NullCheck(L_35);
		int32_t L_36 = String_get_Length_m2979997331(L_35, /*hidden argument*/NULL);
		if (L_36)
		{
			goto IL_0121;
		}
	}
	{
		XmlException_t3490696160 * L_37 = DTDNode_NotWFError_m2314023332(__this, _stringLiteral1678006811, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_37);
	}

IL_0121:
	{
		String_t* L_38 = V_6;
		NullCheck(L_38);
		uint16_t L_39 = String_get_Chars_m3015341861(L_38, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_39) == ((uint32_t)((int32_t)35)))))
		{
			goto IL_0135;
		}
	}
	{
		goto IL_023d;
	}

IL_0135:
	{
		String_t* L_40 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(XmlChar_t3591879093_il2cpp_TypeInfo_var);
		int32_t L_41 = XmlChar_GetPredefinedEntity_m4157892537(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
		if ((((int32_t)L_41) < ((int32_t)0)))
		{
			goto IL_0147;
		}
	}
	{
		goto IL_023d;
	}

IL_0147:
	{
		ArrayList_t2121638921 * L_42 = __this->get_ReferencingEntities_17();
		String_t* L_43 = V_6;
		NullCheck(L_42);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_42, L_43);
		DTDObjectModel_t709926554 * L_44 = DTDNode_get_Root_m1036806329(__this, /*hidden argument*/NULL);
		NullCheck(L_44);
		DTDEntityDeclarationCollection_t4226410245 * L_45 = DTDObjectModel_get_EntityDecls_m3251787732(L_44, /*hidden argument*/NULL);
		String_t* L_46 = V_6;
		NullCheck(L_45);
		DTDEntityDeclaration_t2806387719 * L_47 = DTDEntityDeclarationCollection_get_Item_m2370831310(L_45, L_46, /*hidden argument*/NULL);
		V_7 = L_47;
		DTDEntityDeclaration_t2806387719 * L_48 = V_7;
		if (!L_48)
		{
			goto IL_0235;
		}
	}
	{
		DTDEntityDeclaration_t2806387719 * L_49 = V_7;
		NullCheck(L_49);
		String_t* L_50 = DTDEntityBase_get_SystemId_m2816997275(L_49, /*hidden argument*/NULL);
		if (!L_50)
		{
			goto IL_0183;
		}
	}
	{
		__this->set_hasExternalReference_20((bool)1);
	}

IL_0183:
	{
		ArrayList_t2121638921 * L_51 = ___refs0;
		String_t* L_52 = DTDEntityBase_get_Name_m1635306044(__this, /*hidden argument*/NULL);
		NullCheck(L_51);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_51, L_52);
		DTDEntityDeclaration_t2806387719 * L_53 = V_7;
		ArrayList_t2121638921 * L_54 = ___refs0;
		NullCheck(L_53);
		DTDEntityDeclaration_ScanEntityValue_m3595758284(L_53, L_54, /*hidden argument*/NULL);
		DTDEntityDeclaration_t2806387719 * L_55 = V_7;
		NullCheck(L_55);
		ArrayList_t2121638921 * L_56 = L_55->get_ReferencingEntities_17();
		NullCheck(L_56);
		Il2CppObject * L_57 = VirtFuncInvoker0< Il2CppObject * >::Invoke(43 /* System.Collections.IEnumerator System.Collections.ArrayList::GetEnumerator() */, L_56);
		V_9 = L_57;
	}

IL_01a6:
	try
	{ // begin try (depth: 1)
		{
			goto IL_01c7;
		}

IL_01ab:
		{
			Il2CppObject * L_58 = V_9;
			NullCheck(L_58);
			Il2CppObject * L_59 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_58);
			V_8 = ((String_t*)CastclassSealed(L_59, String_t_il2cpp_TypeInfo_var));
			ArrayList_t2121638921 * L_60 = __this->get_ReferencingEntities_17();
			String_t* L_61 = V_8;
			NullCheck(L_60);
			VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_60, L_61);
		}

IL_01c7:
		{
			Il2CppObject * L_62 = V_9;
			NullCheck(L_62);
			bool L_63 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_62);
			if (L_63)
			{
				goto IL_01ab;
			}
		}

IL_01d3:
		{
			IL2CPP_LEAVE(0x1EE, FINALLY_01d8);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_01d8;
	}

FINALLY_01d8:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_64 = V_9;
			V_12 = ((Il2CppObject *)IsInst(L_64, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			Il2CppObject * L_65 = V_12;
			if (L_65)
			{
				goto IL_01e6;
			}
		}

IL_01e5:
		{
			IL2CPP_END_FINALLY(472)
		}

IL_01e6:
		{
			Il2CppObject * L_66 = V_12;
			NullCheck(L_66);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_66);
			IL2CPP_END_FINALLY(472)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(472)
	{
		IL2CPP_JUMP_TBL(0x1EE, IL_01ee)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_01ee:
	{
		ArrayList_t2121638921 * L_67 = ___refs0;
		String_t* L_68 = DTDEntityBase_get_Name_m1635306044(__this, /*hidden argument*/NULL);
		NullCheck(L_67);
		VirtActionInvoker1< Il2CppObject * >::Invoke(38 /* System.Void System.Collections.ArrayList::Remove(System.Object) */, L_67, L_68);
		String_t* L_69 = V_0;
		int32_t L_70 = V_4;
		String_t* L_71 = V_6;
		NullCheck(L_71);
		int32_t L_72 = String_get_Length_m2979997331(L_71, /*hidden argument*/NULL);
		NullCheck(L_69);
		String_t* L_73 = String_Remove_m242090629(L_69, ((int32_t)((int32_t)L_70-(int32_t)1)), ((int32_t)((int32_t)L_72+(int32_t)2)), /*hidden argument*/NULL);
		V_0 = L_73;
		String_t* L_74 = V_0;
		int32_t L_75 = V_4;
		DTDEntityDeclaration_t2806387719 * L_76 = V_7;
		NullCheck(L_76);
		String_t* L_77 = DTDEntityDeclaration_get_EntityValue_m1737901506(L_76, /*hidden argument*/NULL);
		NullCheck(L_74);
		String_t* L_78 = String_Insert_m3926397187(L_74, ((int32_t)((int32_t)L_75-(int32_t)1)), L_77, /*hidden argument*/NULL);
		V_0 = L_78;
		int32_t L_79 = V_5;
		String_t* L_80 = V_6;
		NullCheck(L_80);
		int32_t L_81 = String_get_Length_m2979997331(L_80, /*hidden argument*/NULL);
		V_5 = ((int32_t)((int32_t)L_79-(int32_t)((int32_t)((int32_t)L_81+(int32_t)1))));
		String_t* L_82 = V_0;
		NullCheck(L_82);
		int32_t L_83 = String_get_Length_m2979997331(L_82, /*hidden argument*/NULL);
		V_3 = L_83;
	}

IL_0235:
	{
		V_4 = 0;
		goto IL_023d;
	}

IL_023d:
	{
		int32_t L_84 = V_5;
		V_5 = ((int32_t)((int32_t)L_84+(int32_t)1));
	}

IL_0243:
	{
		int32_t L_85 = V_5;
		int32_t L_86 = V_3;
		if ((((int32_t)L_85) < ((int32_t)L_86)))
		{
			goto IL_00c2;
		}
	}
	{
		int32_t L_87 = V_4;
		if (!L_87)
		{
			goto IL_026e;
		}
	}
	{
		DTDObjectModel_t709926554 * L_88 = DTDNode_get_Root_m1036806329(__this, /*hidden argument*/NULL);
		String_t* L_89 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String Mono.Xml.DTDNode::get_BaseURI() */, __this);
		XmlException_t3490696160 * L_90 = (XmlException_t3490696160 *)il2cpp_codegen_object_new(XmlException_t3490696160_il2cpp_TypeInfo_var);
		XmlException__ctor_m3778248785(L_90, __this, L_89, _stringLiteral681472551, /*hidden argument*/NULL);
		NullCheck(L_88);
		DTDObjectModel_AddError_m2755935394(L_88, L_90, /*hidden argument*/NULL);
	}

IL_026e:
	{
		__this->set_scanned_18((bool)1);
		__this->set_recursed_19((bool)0);
		return;
	}
}
// System.Void Mono.Xml.DTDEntityDeclarationCollection::.ctor(Mono.Xml.DTDObjectModel)
extern "C"  void DTDEntityDeclarationCollection__ctor_m460986588 (DTDEntityDeclarationCollection_t4226410245 * __this, DTDObjectModel_t709926554 * ___root0, const MethodInfo* method)
{
	{
		DTDObjectModel_t709926554 * L_0 = ___root0;
		DTDCollectionBase__ctor_m1839072630(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// Mono.Xml.DTDEntityDeclaration Mono.Xml.DTDEntityDeclarationCollection::get_Item(System.String)
extern Il2CppClass* DTDEntityDeclaration_t2806387719_il2cpp_TypeInfo_var;
extern const uint32_t DTDEntityDeclarationCollection_get_Item_m2370831310_MetadataUsageId;
extern "C"  DTDEntityDeclaration_t2806387719 * DTDEntityDeclarationCollection_get_Item_m2370831310 (DTDEntityDeclarationCollection_t4226410245 * __this, String_t* ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDEntityDeclarationCollection_get_Item_m2370831310_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name0;
		Il2CppObject * L_1 = DTDCollectionBase_BaseGet_m344774896(__this, L_0, /*hidden argument*/NULL);
		return ((DTDEntityDeclaration_t2806387719 *)IsInstClass(L_1, DTDEntityDeclaration_t2806387719_il2cpp_TypeInfo_var));
	}
}
// System.Void Mono.Xml.DTDEntityDeclarationCollection::Add(System.String,Mono.Xml.DTDEntityDeclaration)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2919457663;
extern const uint32_t DTDEntityDeclarationCollection_Add_m889264620_MetadataUsageId;
extern "C"  void DTDEntityDeclarationCollection_Add_m889264620 (DTDEntityDeclarationCollection_t4226410245 * __this, String_t* ___name0, DTDEntityDeclaration_t2806387719 * ___decl1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDEntityDeclarationCollection_Add_m889264620_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name0;
		bool L_1 = DTDCollectionBase_Contains_m1398227387(__this, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		String_t* L_2 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral2919457663, L_2, /*hidden argument*/NULL);
		InvalidOperationException_t2420574324 * L_4 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_4, L_3, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_001d:
	{
		DTDEntityDeclaration_t2806387719 * L_5 = ___decl1;
		DTDObjectModel_t709926554 * L_6 = DTDCollectionBase_get_Root_m1145982982(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		DTDNode_SetRoot_m970259879(L_5, L_6, /*hidden argument*/NULL);
		String_t* L_7 = ___name0;
		DTDEntityDeclaration_t2806387719 * L_8 = ___decl1;
		DTDCollectionBase_BaseAdd_m1540252226(__this, L_7, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.DTDNode::.ctor()
extern "C"  void DTDNode__ctor_m2554517745 (DTDNode_t1733424026 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String Mono.Xml.DTDNode::get_BaseURI()
extern "C"  String_t* DTDNode_get_BaseURI_m1049621246 (DTDNode_t1733424026 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_baseURI_2();
		return L_0;
	}
}
// System.Void Mono.Xml.DTDNode::set_BaseURI(System.String)
extern "C"  void DTDNode_set_BaseURI_m1446023701 (DTDNode_t1733424026 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_baseURI_2(L_0);
		return;
	}
}
// System.Boolean Mono.Xml.DTDNode::get_IsInternalSubset()
extern "C"  bool DTDNode_get_IsInternalSubset_m3131993229 (DTDNode_t1733424026 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_isInternalSubset_1();
		return L_0;
	}
}
// System.Void Mono.Xml.DTDNode::set_IsInternalSubset(System.Boolean)
extern "C"  void DTDNode_set_IsInternalSubset_m3109582768 (DTDNode_t1733424026 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_isInternalSubset_1(L_0);
		return;
	}
}
// System.Int32 Mono.Xml.DTDNode::get_LineNumber()
extern "C"  int32_t DTDNode_get_LineNumber_m4112711291 (DTDNode_t1733424026 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_lineNumber_3();
		return L_0;
	}
}
// System.Int32 Mono.Xml.DTDNode::get_LinePosition()
extern "C"  int32_t DTDNode_get_LinePosition_m2720230427 (DTDNode_t1733424026 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_linePosition_4();
		return L_0;
	}
}
// System.Void Mono.Xml.DTDNode::SetRoot(Mono.Xml.DTDObjectModel)
extern "C"  void DTDNode_SetRoot_m970259879 (DTDNode_t1733424026 * __this, DTDObjectModel_t709926554 * ___root0, const MethodInfo* method)
{
	{
		DTDObjectModel_t709926554 * L_0 = ___root0;
		__this->set_root_0(L_0);
		String_t* L_1 = __this->get_baseURI_2();
		if (L_1)
		{
			goto IL_001e;
		}
	}
	{
		DTDObjectModel_t709926554 * L_2 = ___root0;
		NullCheck(L_2);
		String_t* L_3 = DTDObjectModel_get_BaseURI_m398146012(L_2, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(7 /* System.Void Mono.Xml.DTDNode::set_BaseURI(System.String) */, __this, L_3);
	}

IL_001e:
	{
		return;
	}
}
// Mono.Xml.DTDObjectModel Mono.Xml.DTDNode::get_Root()
extern "C"  DTDObjectModel_t709926554 * DTDNode_get_Root_m1036806329 (DTDNode_t1733424026 * __this, const MethodInfo* method)
{
	{
		DTDObjectModel_t709926554 * L_0 = __this->get_root_0();
		return L_0;
	}
}
// System.Xml.XmlException Mono.Xml.DTDNode::NotWFError(System.String)
extern Il2CppClass* XmlException_t3490696160_il2cpp_TypeInfo_var;
extern const uint32_t DTDNode_NotWFError_m2314023332_MetadataUsageId;
extern "C"  XmlException_t3490696160 * DTDNode_NotWFError_m2314023332 (DTDNode_t1733424026 * __this, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDNode_NotWFError_m2314023332_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String Mono.Xml.DTDNode::get_BaseURI() */, __this);
		String_t* L_1 = ___message0;
		XmlException_t3490696160 * L_2 = (XmlException_t3490696160 *)il2cpp_codegen_object_new(XmlException_t3490696160_il2cpp_TypeInfo_var);
		XmlException__ctor_m3778248785(L_2, __this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void Mono.Xml.DTDNotationDeclaration::.ctor(Mono.Xml.DTDObjectModel)
extern "C"  void DTDNotationDeclaration__ctor_m2368688479 (DTDNotationDeclaration_t353707912 * __this, DTDObjectModel_t709926554 * ___root0, const MethodInfo* method)
{
	{
		DTDNode__ctor_m2554517745(__this, /*hidden argument*/NULL);
		DTDObjectModel_t709926554 * L_0 = ___root0;
		DTDNode_SetRoot_m970259879(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String Mono.Xml.DTDNotationDeclaration::get_Name()
extern "C"  String_t* DTDNotationDeclaration_get_Name_m1124239610 (DTDNotationDeclaration_t353707912 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_name_5();
		return L_0;
	}
}
// System.Void Mono.Xml.DTDNotationDeclaration::set_Name(System.String)
extern "C"  void DTDNotationDeclaration_set_Name_m1844837297 (DTDNotationDeclaration_t353707912 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_name_5(L_0);
		return;
	}
}
// System.String Mono.Xml.DTDNotationDeclaration::get_PublicId()
extern "C"  String_t* DTDNotationDeclaration_get_PublicId_m1745533587 (DTDNotationDeclaration_t353707912 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_publicId_8();
		return L_0;
	}
}
// System.Void Mono.Xml.DTDNotationDeclaration::set_PublicId(System.String)
extern "C"  void DTDNotationDeclaration_set_PublicId_m2504199736 (DTDNotationDeclaration_t353707912 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_publicId_8(L_0);
		return;
	}
}
// System.String Mono.Xml.DTDNotationDeclaration::get_SystemId()
extern "C"  String_t* DTDNotationDeclaration_get_SystemId_m483927897 (DTDNotationDeclaration_t353707912 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_systemId_9();
		return L_0;
	}
}
// System.Void Mono.Xml.DTDNotationDeclaration::set_SystemId(System.String)
extern "C"  void DTDNotationDeclaration_set_SystemId_m405738290 (DTDNotationDeclaration_t353707912 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_systemId_9(L_0);
		return;
	}
}
// System.String Mono.Xml.DTDNotationDeclaration::get_LocalName()
extern "C"  String_t* DTDNotationDeclaration_get_LocalName_m634176681 (DTDNotationDeclaration_t353707912 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_localName_6();
		return L_0;
	}
}
// System.Void Mono.Xml.DTDNotationDeclaration::set_LocalName(System.String)
extern "C"  void DTDNotationDeclaration_set_LocalName_m4033785648 (DTDNotationDeclaration_t353707912 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_localName_6(L_0);
		return;
	}
}
// System.String Mono.Xml.DTDNotationDeclaration::get_Prefix()
extern "C"  String_t* DTDNotationDeclaration_get_Prefix_m3524691905 (DTDNotationDeclaration_t353707912 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_prefix_7();
		return L_0;
	}
}
// System.Void Mono.Xml.DTDNotationDeclaration::set_Prefix(System.String)
extern "C"  void DTDNotationDeclaration_set_Prefix_m4201923082 (DTDNotationDeclaration_t353707912 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_prefix_7(L_0);
		return;
	}
}
// System.Void Mono.Xml.DTDNotationDeclarationCollection::.ctor(Mono.Xml.DTDObjectModel)
extern "C"  void DTDNotationDeclarationCollection__ctor_m2712775197 (DTDNotationDeclarationCollection_t3011202374 * __this, DTDObjectModel_t709926554 * ___root0, const MethodInfo* method)
{
	{
		DTDObjectModel_t709926554 * L_0 = ___root0;
		DTDCollectionBase__ctor_m1839072630(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.DTDNotationDeclarationCollection::Add(System.String,Mono.Xml.DTDNotationDeclaration)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3827932800;
extern const uint32_t DTDNotationDeclarationCollection_Add_m754289612_MetadataUsageId;
extern "C"  void DTDNotationDeclarationCollection_Add_m754289612 (DTDNotationDeclarationCollection_t3011202374 * __this, String_t* ___name0, DTDNotationDeclaration_t353707912 * ___decl1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDNotationDeclarationCollection_Add_m754289612_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name0;
		bool L_1 = DTDCollectionBase_Contains_m1398227387(__this, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		String_t* L_2 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral3827932800, L_2, /*hidden argument*/NULL);
		InvalidOperationException_t2420574324 * L_4 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_4, L_3, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_001d:
	{
		DTDNotationDeclaration_t353707912 * L_5 = ___decl1;
		DTDObjectModel_t709926554 * L_6 = DTDCollectionBase_get_Root_m1145982982(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		DTDNode_SetRoot_m970259879(L_5, L_6, /*hidden argument*/NULL);
		String_t* L_7 = ___name0;
		DTDNotationDeclaration_t353707912 * L_8 = ___decl1;
		DTDCollectionBase_BaseAdd_m1540252226(__this, L_7, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.DTDObjectModel::.ctor(System.Xml.XmlNameTable)
extern Il2CppClass* DTDElementDeclarationCollection_t2951978932_il2cpp_TypeInfo_var;
extern Il2CppClass* DTDAttListDeclarationCollection_t733492145_il2cpp_TypeInfo_var;
extern Il2CppClass* DTDEntityDeclarationCollection_t4226410245_il2cpp_TypeInfo_var;
extern Il2CppClass* DTDParameterEntityDeclarationCollection_t457236420_il2cpp_TypeInfo_var;
extern Il2CppClass* DTDNotationDeclarationCollection_t3011202374_il2cpp_TypeInfo_var;
extern Il2CppClass* DTDAutomataFactory_t2001760042_il2cpp_TypeInfo_var;
extern Il2CppClass* ArrayList_t2121638921_il2cpp_TypeInfo_var;
extern Il2CppClass* Hashtable_t3875263730_il2cpp_TypeInfo_var;
extern const uint32_t DTDObjectModel__ctor_m3320301063_MetadataUsageId;
extern "C"  void DTDObjectModel__ctor_m3320301063 (DTDObjectModel_t709926554 * __this, XmlNameTable_t3232213908 * ___nameTable0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDObjectModel__ctor_m3320301063_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		XmlNameTable_t3232213908 * L_0 = ___nameTable0;
		__this->set_nameTable_8(L_0);
		DTDElementDeclarationCollection_t2951978932 * L_1 = (DTDElementDeclarationCollection_t2951978932 *)il2cpp_codegen_object_new(DTDElementDeclarationCollection_t2951978932_il2cpp_TypeInfo_var);
		DTDElementDeclarationCollection__ctor_m879563011(L_1, __this, /*hidden argument*/NULL);
		__this->set_elementDecls_1(L_1);
		DTDAttListDeclarationCollection_t733492145 * L_2 = (DTDAttListDeclarationCollection_t733492145 *)il2cpp_codegen_object_new(DTDAttListDeclarationCollection_t733492145_il2cpp_TypeInfo_var);
		DTDAttListDeclarationCollection__ctor_m3747246336(L_2, __this, /*hidden argument*/NULL);
		__this->set_attListDecls_2(L_2);
		DTDEntityDeclarationCollection_t4226410245 * L_3 = (DTDEntityDeclarationCollection_t4226410245 *)il2cpp_codegen_object_new(DTDEntityDeclarationCollection_t4226410245_il2cpp_TypeInfo_var);
		DTDEntityDeclarationCollection__ctor_m460986588(L_3, __this, /*hidden argument*/NULL);
		__this->set_entityDecls_4(L_3);
		DTDParameterEntityDeclarationCollection_t457236420 * L_4 = (DTDParameterEntityDeclarationCollection_t457236420 *)il2cpp_codegen_object_new(DTDParameterEntityDeclarationCollection_t457236420_il2cpp_TypeInfo_var);
		DTDParameterEntityDeclarationCollection__ctor_m1161712915(L_4, __this, /*hidden argument*/NULL);
		__this->set_peDecls_3(L_4);
		DTDNotationDeclarationCollection_t3011202374 * L_5 = (DTDNotationDeclarationCollection_t3011202374 *)il2cpp_codegen_object_new(DTDNotationDeclarationCollection_t3011202374_il2cpp_TypeInfo_var);
		DTDNotationDeclarationCollection__ctor_m2712775197(L_5, __this, /*hidden argument*/NULL);
		__this->set_notationDecls_5(L_5);
		DTDAutomataFactory_t2001760042 * L_6 = (DTDAutomataFactory_t2001760042 *)il2cpp_codegen_object_new(DTDAutomataFactory_t2001760042_il2cpp_TypeInfo_var);
		DTDAutomataFactory__ctor_m2953278209(L_6, __this, /*hidden argument*/NULL);
		__this->set_factory_0(L_6);
		ArrayList_t2121638921 * L_7 = (ArrayList_t2121638921 *)il2cpp_codegen_object_new(ArrayList_t2121638921_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1878432947(L_7, /*hidden argument*/NULL);
		__this->set_validationErrors_6(L_7);
		Hashtable_t3875263730 * L_8 = (Hashtable_t3875263730 *)il2cpp_codegen_object_new(Hashtable_t3875263730_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1514037738(L_8, /*hidden argument*/NULL);
		__this->set_externalResources_9(L_8);
		return;
	}
}
// System.String Mono.Xml.DTDObjectModel::get_BaseURI()
extern "C"  String_t* DTDObjectModel_get_BaseURI_m398146012 (DTDObjectModel_t709926554 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_baseURI_10();
		return L_0;
	}
}
// System.Void Mono.Xml.DTDObjectModel::set_BaseURI(System.String)
extern "C"  void DTDObjectModel_set_BaseURI_m3388004765 (DTDObjectModel_t709926554 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_baseURI_10(L_0);
		return;
	}
}
// System.Boolean Mono.Xml.DTDObjectModel::get_IsStandalone()
extern "C"  bool DTDObjectModel_get_IsStandalone_m3806876939 (DTDObjectModel_t709926554 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_isStandalone_16();
		return L_0;
	}
}
// System.Void Mono.Xml.DTDObjectModel::set_IsStandalone(System.Boolean)
extern "C"  void DTDObjectModel_set_IsStandalone_m3307440830 (DTDObjectModel_t709926554 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_isStandalone_16(L_0);
		return;
	}
}
// System.String Mono.Xml.DTDObjectModel::get_Name()
extern "C"  String_t* DTDObjectModel_get_Name_m3702687244 (DTDObjectModel_t709926554 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_name_11();
		return L_0;
	}
}
// System.Void Mono.Xml.DTDObjectModel::set_Name(System.String)
extern "C"  void DTDObjectModel_set_Name_m1399978207 (DTDObjectModel_t709926554 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_name_11(L_0);
		return;
	}
}
// System.Xml.XmlNameTable Mono.Xml.DTDObjectModel::get_NameTable()
extern "C"  XmlNameTable_t3232213908 * DTDObjectModel_get_NameTable_m2752706194 (DTDObjectModel_t709926554 * __this, const MethodInfo* method)
{
	{
		XmlNameTable_t3232213908 * L_0 = __this->get_nameTable_8();
		return L_0;
	}
}
// System.String Mono.Xml.DTDObjectModel::get_PublicId()
extern "C"  String_t* DTDObjectModel_get_PublicId_m2154946213 (DTDObjectModel_t709926554 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_publicId_12();
		return L_0;
	}
}
// System.Void Mono.Xml.DTDObjectModel::set_PublicId(System.String)
extern "C"  void DTDObjectModel_set_PublicId_m889242726 (DTDObjectModel_t709926554 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_publicId_12(L_0);
		return;
	}
}
// System.String Mono.Xml.DTDObjectModel::get_SystemId()
extern "C"  String_t* DTDObjectModel_get_SystemId_m893340523 (DTDObjectModel_t709926554 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_systemId_13();
		return L_0;
	}
}
// System.Void Mono.Xml.DTDObjectModel::set_SystemId(System.String)
extern "C"  void DTDObjectModel_set_SystemId_m3085748576 (DTDObjectModel_t709926554 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_systemId_13(L_0);
		return;
	}
}
// System.String Mono.Xml.DTDObjectModel::get_InternalSubset()
extern "C"  String_t* DTDObjectModel_get_InternalSubset_m746677696 (DTDObjectModel_t709926554 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_intSubset_14();
		return L_0;
	}
}
// System.Void Mono.Xml.DTDObjectModel::set_InternalSubset(System.String)
extern "C"  void DTDObjectModel_set_InternalSubset_m2199752299 (DTDObjectModel_t709926554 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_intSubset_14(L_0);
		return;
	}
}
// System.Boolean Mono.Xml.DTDObjectModel::get_InternalSubsetHasPEReference()
extern "C"  bool DTDObjectModel_get_InternalSubsetHasPEReference_m603105815 (DTDObjectModel_t709926554 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_intSubsetHasPERef_15();
		return L_0;
	}
}
// System.Void Mono.Xml.DTDObjectModel::set_InternalSubsetHasPEReference(System.Boolean)
extern "C"  void DTDObjectModel_set_InternalSubsetHasPEReference_m2420613834 (DTDObjectModel_t709926554 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_intSubsetHasPERef_15(L_0);
		return;
	}
}
// System.Int32 Mono.Xml.DTDObjectModel::get_LineNumber()
extern "C"  int32_t DTDObjectModel_get_LineNumber_m3725868799 (DTDObjectModel_t709926554 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_lineNumber_17();
		return L_0;
	}
}
// System.Void Mono.Xml.DTDObjectModel::set_LineNumber(System.Int32)
extern "C"  void DTDObjectModel_set_LineNumber_m4288668902 (DTDObjectModel_t709926554 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_lineNumber_17(L_0);
		return;
	}
}
// System.Int32 Mono.Xml.DTDObjectModel::get_LinePosition()
extern "C"  int32_t DTDObjectModel_get_LinePosition_m331783071 (DTDObjectModel_t709926554 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_linePosition_18();
		return L_0;
	}
}
// System.Void Mono.Xml.DTDObjectModel::set_LinePosition(System.Int32)
extern "C"  void DTDObjectModel_set_LinePosition_m3330991110 (DTDObjectModel_t709926554 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_linePosition_18(L_0);
		return;
	}
}
// System.String Mono.Xml.DTDObjectModel::ResolveEntity(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlException_t3490696160_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3889341600;
extern Il2CppCodeGenString* _stringLiteral32;
extern const uint32_t DTDObjectModel_ResolveEntity_m1835942729_MetadataUsageId;
extern "C"  String_t* DTDObjectModel_ResolveEntity_m1835942729 (DTDObjectModel_t709926554 * __this, String_t* ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDObjectModel_ResolveEntity_m1835942729_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DTDEntityDeclaration_t2806387719 * V_0 = NULL;
	{
		DTDEntityDeclarationCollection_t4226410245 * L_0 = DTDObjectModel_get_EntityDecls_m3251787732(__this, /*hidden argument*/NULL);
		String_t* L_1 = ___name0;
		NullCheck(L_0);
		DTDEntityDeclaration_t2806387719 * L_2 = DTDEntityDeclarationCollection_get_Item_m2370831310(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		DTDEntityDeclaration_t2806387719 * L_3 = V_0;
		if (L_3)
		{
			goto IL_003c;
		}
	}
	{
		String_t* L_4 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral3889341600, L_4, /*hidden argument*/NULL);
		int32_t L_6 = DTDObjectModel_get_LineNumber_m3725868799(__this, /*hidden argument*/NULL);
		int32_t L_7 = DTDObjectModel_get_LinePosition_m331783071(__this, /*hidden argument*/NULL);
		XmlException_t3490696160 * L_8 = (XmlException_t3490696160 *)il2cpp_codegen_object_new(XmlException_t3490696160_il2cpp_TypeInfo_var);
		XmlException__ctor_m1096610055(L_8, L_5, (Exception_t1967233988 *)NULL, L_6, L_7, /*hidden argument*/NULL);
		DTDObjectModel_AddError_m2755935394(__this, L_8, /*hidden argument*/NULL);
		return _stringLiteral32;
	}

IL_003c:
	{
		DTDEntityDeclaration_t2806387719 * L_9 = V_0;
		NullCheck(L_9);
		String_t* L_10 = DTDEntityDeclaration_get_EntityValue_m1737901506(L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.Xml.XmlResolver Mono.Xml.DTDObjectModel::get_Resolver()
extern "C"  XmlResolver_t2502213349 * DTDObjectModel_get_Resolver_m1268564320 (DTDObjectModel_t709926554 * __this, const MethodInfo* method)
{
	{
		XmlResolver_t2502213349 * L_0 = __this->get_resolver_7();
		return L_0;
	}
}
// System.Void Mono.Xml.DTDObjectModel::set_XmlResolver(System.Xml.XmlResolver)
extern "C"  void DTDObjectModel_set_XmlResolver_m193476416 (DTDObjectModel_t709926554 * __this, XmlResolver_t2502213349 * ___value0, const MethodInfo* method)
{
	{
		XmlResolver_t2502213349 * L_0 = ___value0;
		__this->set_resolver_7(L_0);
		return;
	}
}
// System.Collections.Hashtable Mono.Xml.DTDObjectModel::get_ExternalResources()
extern "C"  Hashtable_t3875263730 * DTDObjectModel_get_ExternalResources_m2105766529 (DTDObjectModel_t709926554 * __this, const MethodInfo* method)
{
	{
		Hashtable_t3875263730 * L_0 = __this->get_externalResources_9();
		return L_0;
	}
}
// Mono.Xml.DTDElementDeclarationCollection Mono.Xml.DTDObjectModel::get_ElementDecls()
extern "C"  DTDElementDeclarationCollection_t2951978932 * DTDObjectModel_get_ElementDecls_m3999237198 (DTDObjectModel_t709926554 * __this, const MethodInfo* method)
{
	{
		DTDElementDeclarationCollection_t2951978932 * L_0 = __this->get_elementDecls_1();
		return L_0;
	}
}
// Mono.Xml.DTDAttListDeclarationCollection Mono.Xml.DTDObjectModel::get_AttListDecls()
extern "C"  DTDAttListDeclarationCollection_t733492145 * DTDObjectModel_get_AttListDecls_m3653137288 (DTDObjectModel_t709926554 * __this, const MethodInfo* method)
{
	{
		DTDAttListDeclarationCollection_t733492145 * L_0 = __this->get_attListDecls_2();
		return L_0;
	}
}
// Mono.Xml.DTDEntityDeclarationCollection Mono.Xml.DTDObjectModel::get_EntityDecls()
extern "C"  DTDEntityDeclarationCollection_t4226410245 * DTDObjectModel_get_EntityDecls_m3251787732 (DTDObjectModel_t709926554 * __this, const MethodInfo* method)
{
	{
		DTDEntityDeclarationCollection_t4226410245 * L_0 = __this->get_entityDecls_4();
		return L_0;
	}
}
// Mono.Xml.DTDParameterEntityDeclarationCollection Mono.Xml.DTDObjectModel::get_PEDecls()
extern "C"  DTDParameterEntityDeclarationCollection_t457236420 * DTDObjectModel_get_PEDecls_m3945708037 (DTDObjectModel_t709926554 * __this, const MethodInfo* method)
{
	{
		DTDParameterEntityDeclarationCollection_t457236420 * L_0 = __this->get_peDecls_3();
		return L_0;
	}
}
// Mono.Xml.DTDNotationDeclarationCollection Mono.Xml.DTDObjectModel::get_NotationDecls()
extern "C"  DTDNotationDeclarationCollection_t3011202374 * DTDObjectModel_get_NotationDecls_m2487965492 (DTDObjectModel_t709926554 * __this, const MethodInfo* method)
{
	{
		DTDNotationDeclarationCollection_t3011202374 * L_0 = __this->get_notationDecls_5();
		return L_0;
	}
}
// System.Void Mono.Xml.DTDObjectModel::AddError(System.Xml.XmlException)
extern "C"  void DTDObjectModel_AddError_m2755935394 (DTDObjectModel_t709926554 * __this, XmlException_t3490696160 * ___ex0, const MethodInfo* method)
{
	{
		ArrayList_t2121638921 * L_0 = __this->get_validationErrors_6();
		XmlException_t3490696160 * L_1 = ___ex0;
		NullCheck(L_0);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_0, L_1);
		return;
	}
}
// System.String Mono.Xml.DTDObjectModel::GenerateEntityAttributeText(System.String)
extern "C"  String_t* DTDObjectModel_GenerateEntityAttributeText_m1046632615 (DTDObjectModel_t709926554 * __this, String_t* ___entityName0, const MethodInfo* method)
{
	DTDEntityDeclaration_t2806387719 * V_0 = NULL;
	{
		DTDEntityDeclarationCollection_t4226410245 * L_0 = DTDObjectModel_get_EntityDecls_m3251787732(__this, /*hidden argument*/NULL);
		String_t* L_1 = ___entityName0;
		NullCheck(L_0);
		DTDEntityDeclaration_t2806387719 * L_2 = DTDEntityDeclarationCollection_get_Item_m2370831310(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		DTDEntityDeclaration_t2806387719 * L_3 = V_0;
		if (L_3)
		{
			goto IL_0015;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_0015:
	{
		DTDEntityDeclaration_t2806387719 * L_4 = V_0;
		NullCheck(L_4);
		String_t* L_5 = DTDEntityDeclaration_get_EntityValue_m1737901506(L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// Mono.Xml2.XmlTextReader Mono.Xml.DTDObjectModel::GenerateEntityContentReader(System.String,System.Xml.XmlParserContext)
extern const Il2CppType* Stream_t219029575_0_0_0_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Uri_t2776692961_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Stream_t219029575_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlTextReader_t3066586409_il2cpp_TypeInfo_var;
extern const uint32_t DTDObjectModel_GenerateEntityContentReader_m2932930400_MetadataUsageId;
extern "C"  XmlTextReader_t3066586409 * DTDObjectModel_GenerateEntityContentReader_m2932930400 (DTDObjectModel_t709926554 * __this, String_t* ___entityName0, XmlParserContext_t3629084577 * ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDObjectModel_GenerateEntityContentReader_m2932930400_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DTDEntityDeclaration_t2806387719 * V_0 = NULL;
	Uri_t2776692961 * V_1 = NULL;
	Stream_t219029575 * V_2 = NULL;
	Uri_t2776692961 * G_B6_0 = NULL;
	{
		DTDEntityDeclarationCollection_t4226410245 * L_0 = DTDObjectModel_get_EntityDecls_m3251787732(__this, /*hidden argument*/NULL);
		String_t* L_1 = ___entityName0;
		NullCheck(L_0);
		DTDEntityDeclaration_t2806387719 * L_2 = DTDEntityDeclarationCollection_get_Item_m2370831310(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		DTDEntityDeclaration_t2806387719 * L_3 = V_0;
		if (L_3)
		{
			goto IL_0015;
		}
	}
	{
		return (XmlTextReader_t3066586409 *)NULL;
	}

IL_0015:
	{
		DTDEntityDeclaration_t2806387719 * L_4 = V_0;
		NullCheck(L_4);
		String_t* L_5 = DTDEntityBase_get_SystemId_m2816997275(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_007e;
		}
	}
	{
		DTDEntityDeclaration_t2806387719 * L_6 = V_0;
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String Mono.Xml.DTDNode::get_BaseURI() */, L_6);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_9 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_003b;
		}
	}
	{
		G_B6_0 = ((Uri_t2776692961 *)(NULL));
		goto IL_0046;
	}

IL_003b:
	{
		DTDEntityDeclaration_t2806387719 * L_10 = V_0;
		NullCheck(L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String Mono.Xml.DTDNode::get_BaseURI() */, L_10);
		Uri_t2776692961 * L_12 = (Uri_t2776692961 *)il2cpp_codegen_object_new(Uri_t2776692961_il2cpp_TypeInfo_var);
		Uri__ctor_m1721267859(L_12, L_11, /*hidden argument*/NULL);
		G_B6_0 = L_12;
	}

IL_0046:
	{
		V_1 = G_B6_0;
		XmlResolver_t2502213349 * L_13 = __this->get_resolver_7();
		XmlResolver_t2502213349 * L_14 = __this->get_resolver_7();
		Uri_t2776692961 * L_15 = V_1;
		DTDEntityDeclaration_t2806387719 * L_16 = V_0;
		NullCheck(L_16);
		String_t* L_17 = DTDEntityBase_get_SystemId_m2816997275(L_16, /*hidden argument*/NULL);
		NullCheck(L_14);
		Uri_t2776692961 * L_18 = VirtFuncInvoker2< Uri_t2776692961 *, Uri_t2776692961 *, String_t* >::Invoke(5 /* System.Uri System.Xml.XmlResolver::ResolveUri(System.Uri,System.String) */, L_14, L_15, L_17);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_19 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Stream_t219029575_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_13);
		Il2CppObject * L_20 = VirtFuncInvoker3< Il2CppObject *, Uri_t2776692961 *, String_t*, Type_t * >::Invoke(4 /* System.Object System.Xml.XmlResolver::GetEntity(System.Uri,System.String,System.Type) */, L_13, L_18, (String_t*)NULL, L_19);
		V_2 = ((Stream_t219029575 *)IsInstClass(L_20, Stream_t219029575_il2cpp_TypeInfo_var));
		Stream_t219029575 * L_21 = V_2;
		XmlParserContext_t3629084577 * L_22 = ___context1;
		XmlTextReader_t3066586409 * L_23 = (XmlTextReader_t3066586409 *)il2cpp_codegen_object_new(XmlTextReader_t3066586409_il2cpp_TypeInfo_var);
		XmlTextReader__ctor_m2476688333(L_23, L_21, 1, L_22, /*hidden argument*/NULL);
		return L_23;
	}

IL_007e:
	{
		DTDEntityDeclaration_t2806387719 * L_24 = V_0;
		NullCheck(L_24);
		String_t* L_25 = DTDEntityDeclaration_get_EntityValue_m1737901506(L_24, /*hidden argument*/NULL);
		XmlParserContext_t3629084577 * L_26 = ___context1;
		XmlTextReader_t3066586409 * L_27 = (XmlTextReader_t3066586409 *)il2cpp_codegen_object_new(XmlTextReader_t3066586409_il2cpp_TypeInfo_var);
		XmlTextReader__ctor_m4251487366(L_27, L_25, 1, L_26, /*hidden argument*/NULL);
		return L_27;
	}
}
// System.Void Mono.Xml.DTDParameterEntityDeclaration::.ctor(Mono.Xml.DTDObjectModel)
extern "C"  void DTDParameterEntityDeclaration__ctor_m3796965333 (DTDParameterEntityDeclaration_t2256150406 * __this, DTDObjectModel_t709926554 * ___root0, const MethodInfo* method)
{
	{
		DTDObjectModel_t709926554 * L_0 = ___root0;
		DTDEntityBase__ctor_m3123258427(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.DTDParameterEntityDeclarationCollection::.ctor(Mono.Xml.DTDObjectModel)
extern Il2CppClass* Hashtable_t3875263730_il2cpp_TypeInfo_var;
extern const uint32_t DTDParameterEntityDeclarationCollection__ctor_m1161712915_MetadataUsageId;
extern "C"  void DTDParameterEntityDeclarationCollection__ctor_m1161712915 (DTDParameterEntityDeclarationCollection_t457236420 * __this, DTDObjectModel_t709926554 * ___root0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDParameterEntityDeclarationCollection__ctor_m1161712915_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Hashtable_t3875263730 * L_0 = (Hashtable_t3875263730 *)il2cpp_codegen_object_new(Hashtable_t3875263730_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1514037738(L_0, /*hidden argument*/NULL);
		__this->set_peDecls_0(L_0);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		DTDObjectModel_t709926554 * L_1 = ___root0;
		__this->set_root_1(L_1);
		return;
	}
}
// Mono.Xml.DTDParameterEntityDeclaration Mono.Xml.DTDParameterEntityDeclarationCollection::get_Item(System.String)
extern Il2CppClass* DTDParameterEntityDeclaration_t2256150406_il2cpp_TypeInfo_var;
extern const uint32_t DTDParameterEntityDeclarationCollection_get_Item_m4053598104_MetadataUsageId;
extern "C"  DTDParameterEntityDeclaration_t2256150406 * DTDParameterEntityDeclarationCollection_get_Item_m4053598104 (DTDParameterEntityDeclarationCollection_t457236420 * __this, String_t* ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDParameterEntityDeclarationCollection_get_Item_m4053598104_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Hashtable_t3875263730 * L_0 = __this->get_peDecls_0();
		String_t* L_1 = ___name0;
		NullCheck(L_0);
		Il2CppObject * L_2 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_0, L_1);
		return ((DTDParameterEntityDeclaration_t2256150406 *)IsInstClass(L_2, DTDParameterEntityDeclaration_t2256150406_il2cpp_TypeInfo_var));
	}
}
// System.Void Mono.Xml.DTDParameterEntityDeclarationCollection::Add(System.String,Mono.Xml.DTDParameterEntityDeclaration)
extern "C"  void DTDParameterEntityDeclarationCollection_Add_m3192507178 (DTDParameterEntityDeclarationCollection_t457236420 * __this, String_t* ___name0, DTDParameterEntityDeclaration_t2256150406 * ___decl1, const MethodInfo* method)
{
	{
		Hashtable_t3875263730 * L_0 = __this->get_peDecls_0();
		String_t* L_1 = ___name0;
		NullCheck(L_0);
		Il2CppObject * L_2 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_0, L_1);
		if (!L_2)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		DTDParameterEntityDeclaration_t2256150406 * L_3 = ___decl1;
		DTDObjectModel_t709926554 * L_4 = __this->get_root_1();
		NullCheck(L_3);
		DTDNode_SetRoot_m970259879(L_3, L_4, /*hidden argument*/NULL);
		Hashtable_t3875263730 * L_5 = __this->get_peDecls_0();
		String_t* L_6 = ___name0;
		DTDParameterEntityDeclaration_t2256150406 * L_7 = ___decl1;
		NullCheck(L_5);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_5, L_6, L_7);
		return;
	}
}
// System.Void Mono.Xml.Schema.XdtAnyAtomicType::.ctor()
extern Il2CppClass* XsdAnySimpleType_t8663200_il2cpp_TypeInfo_var;
extern const uint32_t XdtAnyAtomicType__ctor_m822337841_MetadataUsageId;
extern "C"  void XdtAnyAtomicType__ctor_m822337841 (XdtAnyAtomicType_t941707450 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XdtAnyAtomicType__ctor_m822337841_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XsdAnySimpleType_t8663200_il2cpp_TypeInfo_var);
		XsdAnySimpleType__ctor_m711427083(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.Schema.XdtDayTimeDuration::.ctor()
extern "C"  void XdtDayTimeDuration__ctor_m155016389 (XdtDayTimeDuration_t2689613030 * __this, const MethodInfo* method)
{
	{
		XsdDuration__ctor_m651334095(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.Schema.XdtUntypedAtomic::.ctor()
extern "C"  void XdtUntypedAtomic__ctor_m3382942118 (XdtUntypedAtomic_t3803926693 * __this, const MethodInfo* method)
{
	{
		XdtAnyAtomicType__ctor_m822337841(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.Schema.XdtYearMonthDuration::.ctor()
extern "C"  void XdtYearMonthDuration__ctor_m737026187 (XdtYearMonthDuration_t3115315360 * __this, const MethodInfo* method)
{
	{
		XsdDuration__ctor_m651334095(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.Schema.XsdAnySimpleType::.ctor()
extern Il2CppClass* XmlSchemaDatatype_t2590121_il2cpp_TypeInfo_var;
extern const uint32_t XsdAnySimpleType__ctor_m711427083_MetadataUsageId;
extern "C"  void XsdAnySimpleType__ctor_m711427083 (XsdAnySimpleType_t8663200 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XsdAnySimpleType__ctor_m711427083_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaDatatype_t2590121_il2cpp_TypeInfo_var);
		XmlSchemaDatatype__ctor_m503494556(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.Schema.XsdAnySimpleType::.cctor()
extern Il2CppClass* CharU5BU5D_t3416858730_il2cpp_TypeInfo_var;
extern Il2CppClass* XsdAnySimpleType_t8663200_il2cpp_TypeInfo_var;
extern const uint32_t XsdAnySimpleType__cctor_m97306882_MetadataUsageId;
extern "C"  void XsdAnySimpleType__cctor_m97306882 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XsdAnySimpleType__cctor_m97306882_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		CharU5BU5D_t3416858730* L_0 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint16_t)((int32_t)32));
		((XsdAnySimpleType_t8663200_StaticFields*)XsdAnySimpleType_t8663200_il2cpp_TypeInfo_var->static_fields)->set_whitespaceArray_56(L_0);
		((XsdAnySimpleType_t8663200_StaticFields*)XsdAnySimpleType_t8663200_il2cpp_TypeInfo_var->static_fields)->set_booleanAllowedFacets_57(((int32_t)40));
		((XsdAnySimpleType_t8663200_StaticFields*)XsdAnySimpleType_t8663200_il2cpp_TypeInfo_var->static_fields)->set_decimalAllowedFacets_58(((int32_t)4088));
		((XsdAnySimpleType_t8663200_StaticFields*)XsdAnySimpleType_t8663200_il2cpp_TypeInfo_var->static_fields)->set_durationAllowedFacets_59(((int32_t)1016));
		((XsdAnySimpleType_t8663200_StaticFields*)XsdAnySimpleType_t8663200_il2cpp_TypeInfo_var->static_fields)->set_stringAllowedFacets_60(((int32_t)63));
		XsdAnySimpleType_t8663200 * L_1 = (XsdAnySimpleType_t8663200 *)il2cpp_codegen_object_new(XsdAnySimpleType_t8663200_il2cpp_TypeInfo_var);
		XsdAnySimpleType__ctor_m711427083(L_1, /*hidden argument*/NULL);
		((XsdAnySimpleType_t8663200_StaticFields*)XsdAnySimpleType_t8663200_il2cpp_TypeInfo_var->static_fields)->set_instance_55(L_1);
		return;
	}
}
// Mono.Xml.Schema.XsdAnySimpleType Mono.Xml.Schema.XsdAnySimpleType::get_Instance()
extern Il2CppClass* XsdAnySimpleType_t8663200_il2cpp_TypeInfo_var;
extern const uint32_t XsdAnySimpleType_get_Instance_m4236154734_MetadataUsageId;
extern "C"  XsdAnySimpleType_t8663200 * XsdAnySimpleType_get_Instance_m4236154734 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XsdAnySimpleType_get_Instance_m4236154734_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XsdAnySimpleType_t8663200_il2cpp_TypeInfo_var);
		XsdAnySimpleType_t8663200 * L_0 = ((XsdAnySimpleType_t8663200_StaticFields*)XsdAnySimpleType_t8663200_il2cpp_TypeInfo_var->static_fields)->get_instance_55();
		return L_0;
	}
}
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdAnySimpleType::get_TokenizedType()
extern "C"  int32_t XsdAnySimpleType_get_TokenizedType_m2531507378 (XsdAnySimpleType_t8663200 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(((int32_t)12));
	}
}
// System.Void Mono.Xml.Schema.XsdAnyURI::.ctor()
extern "C"  void XsdAnyURI__ctor_m2654983427 (XsdAnyURI_t1070214554 * __this, const MethodInfo* method)
{
	{
		XsdString__ctor_m952800242(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdAnyURI::get_TokenizedType()
extern "C"  int32_t XsdAnyURI_get_TokenizedType_m1762949612 (XsdAnyURI_t1070214554 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(0);
	}
}
// System.Void Mono.Xml.Schema.XsdBase64Binary::.ctor()
extern "C"  void XsdBase64Binary__ctor_m501055667 (XsdBase64Binary_t3314116906 * __this, const MethodInfo* method)
{
	{
		XsdString__ctor_m952800242(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.Schema.XsdBase64Binary::.cctor()
extern Il2CppClass* XsdBase64Binary_t3314116906_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t58506160_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3958812739;
extern const uint32_t XsdBase64Binary__cctor_m2165727578_MetadataUsageId;
extern "C"  void XsdBase64Binary__cctor_m2165727578 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XsdBase64Binary__cctor_m2165727578_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	uint16_t V_3 = 0x0;
	{
		((XsdBase64Binary_t3314116906_StaticFields*)XsdBase64Binary_t3314116906_il2cpp_TypeInfo_var->static_fields)->set_ALPHABET_61(_stringLiteral3958812739);
		String_t* L_0 = ((XsdBase64Binary_t3314116906_StaticFields*)XsdBase64Binary_t3314116906_il2cpp_TypeInfo_var->static_fields)->get_ALPHABET_61();
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m2979997331(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		((XsdBase64Binary_t3314116906_StaticFields*)XsdBase64Binary_t3314116906_il2cpp_TypeInfo_var->static_fields)->set_decodeTable_62(((ByteU5BU5D_t58506160*)SZArrayNew(ByteU5BU5D_t58506160_il2cpp_TypeInfo_var, (uint32_t)((int32_t)123))));
		V_1 = 0;
		goto IL_0038;
	}

IL_0028:
	{
		ByteU5BU5D_t58506160* L_2 = ((XsdBase64Binary_t3314116906_StaticFields*)XsdBase64Binary_t3314116906_il2cpp_TypeInfo_var->static_fields)->get_decodeTable_62();
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_3), (uint8_t)((int32_t)255));
		int32_t L_4 = V_1;
		V_1 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_0038:
	{
		int32_t L_5 = V_1;
		ByteU5BU5D_t58506160* L_6 = ((XsdBase64Binary_t3314116906_StaticFields*)XsdBase64Binary_t3314116906_il2cpp_TypeInfo_var->static_fields)->get_decodeTable_62();
		NullCheck(L_6);
		if ((((int32_t)L_5) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length)))))))
		{
			goto IL_0028;
		}
	}
	{
		V_2 = 0;
		goto IL_0065;
	}

IL_004c:
	{
		String_t* L_7 = ((XsdBase64Binary_t3314116906_StaticFields*)XsdBase64Binary_t3314116906_il2cpp_TypeInfo_var->static_fields)->get_ALPHABET_61();
		int32_t L_8 = V_2;
		NullCheck(L_7);
		uint16_t L_9 = String_get_Chars_m3015341861(L_7, L_8, /*hidden argument*/NULL);
		V_3 = L_9;
		ByteU5BU5D_t58506160* L_10 = ((XsdBase64Binary_t3314116906_StaticFields*)XsdBase64Binary_t3314116906_il2cpp_TypeInfo_var->static_fields)->get_decodeTable_62();
		uint16_t L_11 = V_3;
		int32_t L_12 = V_2;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(L_11), (uint8_t)(((int32_t)((uint8_t)L_12))));
		int32_t L_13 = V_2;
		V_2 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0065:
	{
		int32_t L_14 = V_2;
		int32_t L_15 = V_0;
		if ((((int32_t)L_14) < ((int32_t)L_15)))
		{
			goto IL_004c;
		}
	}
	{
		return;
	}
}
// System.Void Mono.Xml.Schema.XsdBoolean::.ctor()
extern Il2CppClass* XsdAnySimpleType_t8663200_il2cpp_TypeInfo_var;
extern const uint32_t XsdBoolean__ctor_m4109790459_MetadataUsageId;
extern "C"  void XsdBoolean__ctor_m4109790459 (XsdBoolean_t235052784 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XsdBoolean__ctor_m4109790459_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XsdAnySimpleType_t8663200_il2cpp_TypeInfo_var);
		XsdAnySimpleType__ctor_m711427083(__this, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t2590121 *)__this)->set_WhitespaceValue_0(2);
		return;
	}
}
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdBoolean::get_TokenizedType()
extern Il2CppClass* XmlSchemaUtil_t3432835015_il2cpp_TypeInfo_var;
extern const uint32_t XsdBoolean_get_TokenizedType_m200189154_MetadataUsageId;
extern "C"  int32_t XsdBoolean_get_TokenizedType_m200189154 (XsdBoolean_t235052784 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XsdBoolean_get_TokenizedType_m200189154_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlSchemaUtil_t3432835015_il2cpp_TypeInfo_var);
		bool L_0 = ((XmlSchemaUtil_t3432835015_StaticFields*)XmlSchemaUtil_t3432835015_il2cpp_TypeInfo_var->static_fields)->get_StrictMsCompliant_3();
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (int32_t)(((int32_t)12));
	}

IL_000d:
	{
		return (int32_t)(0);
	}
}
// System.Void Mono.Xml.Schema.XsdByte::.ctor()
extern "C"  void XsdByte__ctor_m3595325659 (XsdByte_t2013201666 * __this, const MethodInfo* method)
{
	{
		XsdShort__ctor_m2889145127(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.Schema.XsdDate::.ctor()
extern Il2CppClass* XsdAnySimpleType_t8663200_il2cpp_TypeInfo_var;
extern const uint32_t XsdDate__ctor_m4205501685_MetadataUsageId;
extern "C"  void XsdDate__ctor_m4205501685 (XsdDate_t2013238184 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XsdDate__ctor_m4205501685_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XsdAnySimpleType_t8663200_il2cpp_TypeInfo_var);
		XsdAnySimpleType__ctor_m711427083(__this, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t2590121 *)__this)->set_WhitespaceValue_0(2);
		return;
	}
}
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdDate::get_TokenizedType()
extern "C"  int32_t XsdDate_get_TokenizedType_m2312853278 (XsdDate_t2013238184 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(0);
	}
}
// System.Void Mono.Xml.Schema.XsdDateTime::.ctor()
extern Il2CppClass* XsdAnySimpleType_t8663200_il2cpp_TypeInfo_var;
extern const uint32_t XsdDateTime__ctor_m836503464_MetadataUsageId;
extern "C"  void XsdDateTime__ctor_m836503464 (XsdDateTime_t3288893205 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XsdDateTime__ctor_m836503464_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XsdAnySimpleType_t8663200_il2cpp_TypeInfo_var);
		XsdAnySimpleType__ctor_m711427083(__this, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t2590121 *)__this)->set_WhitespaceValue_0(2);
		return;
	}
}
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdDateTime::get_TokenizedType()
extern "C"  int32_t XsdDateTime_get_TokenizedType_m1885453521 (XsdDateTime_t3288893205 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(0);
	}
}
// System.Void Mono.Xml.Schema.XsdDecimal::.ctor()
extern Il2CppClass* XsdAnySimpleType_t8663200_il2cpp_TypeInfo_var;
extern const uint32_t XsdDecimal__ctor_m680772594_MetadataUsageId;
extern "C"  void XsdDecimal__ctor_m680772594 (XsdDecimal_t1712604697 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XsdDecimal__ctor_m680772594_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XsdAnySimpleType_t8663200_il2cpp_TypeInfo_var);
		XsdAnySimpleType__ctor_m711427083(__this, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t2590121 *)__this)->set_WhitespaceValue_0(2);
		return;
	}
}
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdDecimal::get_TokenizedType()
extern "C"  int32_t XsdDecimal_get_TokenizedType_m3768992089 (XsdDecimal_t1712604697 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(((int32_t)12));
	}
}
// System.Void Mono.Xml.Schema.XsdDouble::.ctor()
extern Il2CppClass* XsdAnySimpleType_t8663200_il2cpp_TypeInfo_var;
extern const uint32_t XsdDouble__ctor_m250327090_MetadataUsageId;
extern "C"  void XsdDouble__ctor_m250327090 (XsdDouble_t1156919691 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XsdDouble__ctor_m250327090_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XsdAnySimpleType_t8663200_il2cpp_TypeInfo_var);
		XsdAnySimpleType__ctor_m711427083(__this, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t2590121 *)__this)->set_WhitespaceValue_0(2);
		return;
	}
}
// System.Void Mono.Xml.Schema.XsdDuration::.ctor()
extern Il2CppClass* XsdAnySimpleType_t8663200_il2cpp_TypeInfo_var;
extern const uint32_t XsdDuration__ctor_m651334095_MetadataUsageId;
extern "C"  void XsdDuration__ctor_m651334095 (XsdDuration_t3799098638 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XsdDuration__ctor_m651334095_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XsdAnySimpleType_t8663200_il2cpp_TypeInfo_var);
		XsdAnySimpleType__ctor_m711427083(__this, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t2590121 *)__this)->set_WhitespaceValue_0(2);
		return;
	}
}
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdDuration::get_TokenizedType()
extern "C"  int32_t XsdDuration_get_TokenizedType_m2614051960 (XsdDuration_t3799098638 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(0);
	}
}
// System.Void Mono.Xml.Schema.XsdEntities::.ctor()
extern "C"  void XsdEntities__ctor_m61127234 (XsdEntities_t3688996667 * __this, const MethodInfo* method)
{
	{
		XsdName__ctor_m965229240(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdEntities::get_TokenizedType()
extern "C"  int32_t XsdEntities_get_TokenizedType_m1803638891 (XsdEntities_t3688996667 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(5);
	}
}
// System.Void Mono.Xml.Schema.XsdEntity::.ctor()
extern "C"  void XsdEntity__ctor_m2334407776 (XsdEntity_t1184602525 * __this, const MethodInfo* method)
{
	{
		XsdName__ctor_m965229240(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdEntity::get_TokenizedType()
extern "C"  int32_t XsdEntity_get_TokenizedType_m2100280777 (XsdEntity_t1184602525 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(4);
	}
}
// System.Void Mono.Xml.Schema.XsdFloat::.ctor()
extern Il2CppClass* XsdAnySimpleType_t8663200_il2cpp_TypeInfo_var;
extern const uint32_t XsdFloat__ctor_m181262663_MetadataUsageId;
extern "C"  void XsdFloat__ctor_m181262663 (XsdFloat_t2793549540 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XsdFloat__ctor_m181262663_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XsdAnySimpleType_t8663200_il2cpp_TypeInfo_var);
		XsdAnySimpleType__ctor_m711427083(__this, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t2590121 *)__this)->set_WhitespaceValue_0(2);
		return;
	}
}
// System.Void Mono.Xml.Schema.XsdGDay::.ctor()
extern Il2CppClass* XsdAnySimpleType_t8663200_il2cpp_TypeInfo_var;
extern const uint32_t XsdGDay__ctor_m4023895758_MetadataUsageId;
extern "C"  void XsdGDay__ctor_m4023895758 (XsdGDay_t2013299119 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XsdGDay__ctor_m4023895758_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XsdAnySimpleType_t8663200_il2cpp_TypeInfo_var);
		XsdAnySimpleType__ctor_m711427083(__this, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t2590121 *)__this)->set_WhitespaceValue_0(2);
		return;
	}
}
// System.Void Mono.Xml.Schema.XsdGMonth::.ctor()
extern Il2CppClass* XsdAnySimpleType_t8663200_il2cpp_TypeInfo_var;
extern const uint32_t XsdGMonth__ctor_m1716300458_MetadataUsageId;
extern "C"  void XsdGMonth__ctor_m1716300458 (XsdGMonth_t1211240467 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XsdGMonth__ctor_m1716300458_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XsdAnySimpleType_t8663200_il2cpp_TypeInfo_var);
		XsdAnySimpleType__ctor_m711427083(__this, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t2590121 *)__this)->set_WhitespaceValue_0(2);
		return;
	}
}
// System.Void Mono.Xml.Schema.XsdGMonthDay::.ctor()
extern Il2CppClass* XsdAnySimpleType_t8663200_il2cpp_TypeInfo_var;
extern const uint32_t XsdGMonthDay__ctor_m1969029376_MetadataUsageId;
extern "C"  void XsdGMonthDay__ctor_m1969029376 (XsdGMonthDay_t2202709899 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XsdGMonthDay__ctor_m1969029376_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XsdAnySimpleType_t8663200_il2cpp_TypeInfo_var);
		XsdAnySimpleType__ctor_m711427083(__this, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t2590121 *)__this)->set_WhitespaceValue_0(2);
		return;
	}
}
// System.Void Mono.Xml.Schema.XsdGYear::.ctor()
extern Il2CppClass* XsdAnySimpleType_t8663200_il2cpp_TypeInfo_var;
extern const uint32_t XsdGYear__ctor_m57593695_MetadataUsageId;
extern "C"  void XsdGYear__ctor_m57593695 (XsdGYear_t2793897420 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XsdGYear__ctor_m57593695_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XsdAnySimpleType_t8663200_il2cpp_TypeInfo_var);
		XsdAnySimpleType__ctor_m711427083(__this, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t2590121 *)__this)->set_WhitespaceValue_0(2);
		return;
	}
}
// System.Void Mono.Xml.Schema.XsdGYearMonth::.ctor()
extern Il2CppClass* XsdAnySimpleType_t8663200_il2cpp_TypeInfo_var;
extern const uint32_t XsdGYearMonth__ctor_m2329781447_MetadataUsageId;
extern "C"  void XsdGYearMonth__ctor_m2329781447 (XsdGYearMonth_t878671062 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XsdGYearMonth__ctor_m2329781447_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XsdAnySimpleType_t8663200_il2cpp_TypeInfo_var);
		XsdAnySimpleType__ctor_m711427083(__this, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t2590121 *)__this)->set_WhitespaceValue_0(2);
		return;
	}
}
// System.Void Mono.Xml.Schema.XsdHexBinary::.ctor()
extern Il2CppClass* XsdAnySimpleType_t8663200_il2cpp_TypeInfo_var;
extern const uint32_t XsdHexBinary__ctor_m3944764583_MetadataUsageId;
extern "C"  void XsdHexBinary__ctor_m3944764583 (XsdHexBinary_t1695962116 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XsdHexBinary__ctor_m3944764583_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XsdAnySimpleType_t8663200_il2cpp_TypeInfo_var);
		XsdAnySimpleType__ctor_m711427083(__this, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t2590121 *)__this)->set_WhitespaceValue_0(2);
		return;
	}
}
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdHexBinary::get_TokenizedType()
extern "C"  int32_t XsdHexBinary_get_TokenizedType_m1520534222 (XsdHexBinary_t1695962116 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(((int32_t)12));
	}
}
// System.Void Mono.Xml.Schema.XsdID::.ctor()
extern "C"  void XsdID__ctor_m1149632072 (XsdID_t2215259957 * __this, const MethodInfo* method)
{
	{
		XsdName__ctor_m965229240(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdID::get_TokenizedType()
extern "C"  int32_t XsdID_get_TokenizedType_m1588155441 (XsdID_t2215259957 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(1);
	}
}
// System.Void Mono.Xml.Schema.XsdIDRef::.ctor()
extern "C"  void XsdIDRef__ctor_m2220691851 (XsdIDRef_t2795100704 * __this, const MethodInfo* method)
{
	{
		XsdName__ctor_m965229240(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdIDRef::get_TokenizedType()
extern "C"  int32_t XsdIDRef_get_TokenizedType_m3332819250 (XsdIDRef_t2795100704 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(2);
	}
}
// System.Void Mono.Xml.Schema.XsdIDRefs::.ctor()
extern "C"  void XsdIDRefs__ctor_m969339240 (XsdIDRefs_t1259314069 * __this, const MethodInfo* method)
{
	{
		XsdName__ctor_m965229240(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdIDRefs::get_TokenizedType()
extern "C"  int32_t XsdIDRefs_get_TokenizedType_m1619207889 (XsdIDRefs_t1259314069 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(3);
	}
}
// System.Void Mono.Xml.Schema.XsdInt::.ctor()
extern "C"  void XsdInt__ctor_m3767165204 (XsdInt_t464121399 * __this, const MethodInfo* method)
{
	{
		XsdLong__ctor_m1169214951(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.Schema.XsdInteger::.ctor()
extern "C"  void XsdInteger__ctor_m3723735205 (XsdInteger_t2128393222 * __this, const MethodInfo* method)
{
	{
		XsdDecimal__ctor_m680772594(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.Schema.XsdLanguage::.ctor()
extern "C"  void XsdLanguage__ctor_m1673300107 (XsdLanguage_t4177521362 * __this, const MethodInfo* method)
{
	{
		XsdToken__ctor_m2878046794(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdLanguage::get_TokenizedType()
extern "C"  int32_t XsdLanguage_get_TokenizedType_m3551592756 (XsdLanguage_t4177521362 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(0);
	}
}
// System.Void Mono.Xml.Schema.XsdLong::.ctor()
extern "C"  void XsdLong__ctor_m1169214951 (XsdLong_t2013489782 * __this, const MethodInfo* method)
{
	{
		XsdInteger__ctor_m3723735205(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.Schema.XsdName::.ctor()
extern "C"  void XsdName__ctor_m965229240 (XsdName_t2013535877 * __this, const MethodInfo* method)
{
	{
		XsdToken__ctor_m2878046794(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdName::get_TokenizedType()
extern "C"  int32_t XsdName_get_TokenizedType_m2892179553 (XsdName_t2013535877 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(0);
	}
}
// System.Void Mono.Xml.Schema.XsdNCName::.ctor()
extern "C"  void XsdNCName__ctor_m4137434915 (XsdNCName_t1401413498 * __this, const MethodInfo* method)
{
	{
		XsdName__ctor_m965229240(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdNCName::get_TokenizedType()
extern "C"  int32_t XsdNCName_get_TokenizedType_m679654412 (XsdNCName_t1401413498 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(((int32_t)11));
	}
}
// System.Void Mono.Xml.Schema.XsdNegativeInteger::.ctor()
extern "C"  void XsdNegativeInteger__ctor_m3492042490 (XsdNegativeInteger_t2648572689 * __this, const MethodInfo* method)
{
	{
		XsdNonPositiveInteger__ctor_m3796642155(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.Schema.XsdNMToken::.ctor()
extern "C"  void XsdNMToken__ctor_m327823817 (XsdNMToken_t1296931426 * __this, const MethodInfo* method)
{
	{
		XsdToken__ctor_m2878046794(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdNMToken::get_TokenizedType()
extern "C"  int32_t XsdNMToken_get_TokenizedType_m611034288 (XsdNMToken_t1296931426 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(6);
	}
}
// System.Void Mono.Xml.Schema.XsdNMTokens::.ctor()
extern "C"  void XsdNMTokens__ctor_m2419972330 (XsdNMTokens_t2060706707 * __this, const MethodInfo* method)
{
	{
		XsdNMToken__ctor_m327823817(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdNMTokens::get_TokenizedType()
extern "C"  int32_t XsdNMTokens_get_TokenizedType_m3143219987 (XsdNMTokens_t2060706707 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(7);
	}
}
// System.Void Mono.Xml.Schema.XsdNonNegativeInteger::.ctor()
extern "C"  void XsdNonNegativeInteger__ctor_m1363977127 (XsdNonNegativeInteger_t906772726 * __this, const MethodInfo* method)
{
	{
		XsdInteger__ctor_m3723735205(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.Schema.XsdNonPositiveInteger::.ctor()
extern "C"  void XsdNonPositiveInteger__ctor_m3796642155 (XsdNonPositiveInteger_t673083058 * __this, const MethodInfo* method)
{
	{
		XsdInteger__ctor_m3723735205(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.Schema.XsdNormalizedString::.ctor()
extern "C"  void XsdNormalizedString__ctor_m1949672443 (XsdNormalizedString_t2753230946 * __this, const MethodInfo* method)
{
	{
		XsdString__ctor_m952800242(__this, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t2590121 *)__this)->set_WhitespaceValue_0(1);
		return;
	}
}
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdNormalizedString::get_TokenizedType()
extern "C"  int32_t XsdNormalizedString_get_TokenizedType_m70161316 (XsdNormalizedString_t2753230946 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(0);
	}
}
// System.Void Mono.Xml.Schema.XsdNotation::.ctor()
extern Il2CppClass* XsdAnySimpleType_t8663200_il2cpp_TypeInfo_var;
extern const uint32_t XsdNotation__ctor_m3082870561_MetadataUsageId;
extern "C"  void XsdNotation__ctor_m3082870561 (XsdNotation_t3074536316 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XsdNotation__ctor_m3082870561_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XsdAnySimpleType_t8663200_il2cpp_TypeInfo_var);
		XsdAnySimpleType__ctor_m711427083(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdNotation::get_TokenizedType()
extern "C"  int32_t XsdNotation_get_TokenizedType_m111814346 (XsdNotation_t3074536316 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(8);
	}
}
// System.Void Mono.Xml.Schema.XsdPositiveInteger::.ctor()
extern "C"  void XsdPositiveInteger__ctor_m1629740222 (XsdPositiveInteger_t2414883021 * __this, const MethodInfo* method)
{
	{
		XsdNonNegativeInteger__ctor_m1363977127(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.Schema.XsdQName::.ctor()
extern "C"  void XsdQName__ctor_m179205383 (XsdQName_t2802801444 * __this, const MethodInfo* method)
{
	{
		XsdName__ctor_m965229240(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdQName::get_TokenizedType()
extern "C"  int32_t XsdQName_get_TokenizedType_m2389889710 (XsdQName_t2802801444 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(((int32_t)10));
	}
}
// System.Void Mono.Xml.Schema.XsdShort::.ctor()
extern "C"  void XsdShort__ctor_m2889145127 (XsdShort_t2805436676 * __this, const MethodInfo* method)
{
	{
		XsdInt__ctor_m3767165204(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.Schema.XsdString::.ctor()
extern Il2CppClass* XsdAnySimpleType_t8663200_il2cpp_TypeInfo_var;
extern const uint32_t XsdString__ctor_m952800242_MetadataUsageId;
extern "C"  void XsdString__ctor_m952800242 (XsdString_t1590891979 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XsdString__ctor_m952800242_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XsdAnySimpleType_t8663200_il2cpp_TypeInfo_var);
		XsdAnySimpleType__ctor_m711427083(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdString::get_TokenizedType()
extern "C"  int32_t XsdString_get_TokenizedType_m1875200603 (XsdString_t1590891979 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(0);
	}
}
// System.Void Mono.Xml.Schema.XsdTime::.ctor()
extern Il2CppClass* XsdAnySimpleType_t8663200_il2cpp_TypeInfo_var;
extern const uint32_t XsdTime__ctor_m237472950_MetadataUsageId;
extern "C"  void XsdTime__ctor_m237472950 (XsdTime_t2013722311 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XsdTime__ctor_m237472950_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XsdAnySimpleType_t8663200_il2cpp_TypeInfo_var);
		XsdAnySimpleType__ctor_m711427083(__this, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t2590121 *)__this)->set_WhitespaceValue_0(2);
		return;
	}
}
// System.Void Mono.Xml.Schema.XsdTime::.cctor()
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* XsdTime_t2013722311_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2168509312;
extern Il2CppCodeGenString* _stringLiteral878311800;
extern Il2CppCodeGenString* _stringLiteral1457862126;
extern Il2CppCodeGenString* _stringLiteral2244053048;
extern Il2CppCodeGenString* _stringLiteral846167854;
extern Il2CppCodeGenString* _stringLiteral461399800;
extern Il2CppCodeGenString* _stringLiteral1418492014;
extern Il2CppCodeGenString* _stringLiteral1023579576;
extern Il2CppCodeGenString* _stringLiteral1457935802;
extern Il2CppCodeGenString* _stringLiteral846187714;
extern Il2CppCodeGenString* _stringLiteral461419660;
extern Il2CppCodeGenString* _stringLiteral1418511874;
extern Il2CppCodeGenString* _stringLiteral1023599436;
extern Il2CppCodeGenString* _stringLiteral1666215746;
extern Il2CppCodeGenString* _stringLiteral112484876;
extern Il2CppCodeGenString* _stringLiteral3486435458;
extern Il2CppCodeGenString* _stringLiteral2799279322;
extern Il2CppCodeGenString* _stringLiteral1457862114;
extern Il2CppCodeGenString* _stringLiteral2244053036;
extern Il2CppCodeGenString* _stringLiteral846167842;
extern Il2CppCodeGenString* _stringLiteral461399788;
extern Il2CppCodeGenString* _stringLiteral1418492002;
extern Il2CppCodeGenString* _stringLiteral1023579564;
extern Il2CppCodeGenString* _stringLiteral1666195874;
extern const uint32_t XsdTime__cctor_m2584597943_MetadataUsageId;
extern "C"  void XsdTime__cctor_m2584597943 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XsdTime__cctor_m2584597943_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringU5BU5D_t2956870243* L_0 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)((int32_t)24)));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral2168509312);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2168509312);
		StringU5BU5D_t2956870243* L_1 = L_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, _stringLiteral878311800);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral878311800);
		StringU5BU5D_t2956870243* L_2 = L_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 2);
		ArrayElementTypeCheck (L_2, _stringLiteral1457862126);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1457862126);
		StringU5BU5D_t2956870243* L_3 = L_2;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 3);
		ArrayElementTypeCheck (L_3, _stringLiteral2244053048);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral2244053048);
		StringU5BU5D_t2956870243* L_4 = L_3;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 4);
		ArrayElementTypeCheck (L_4, _stringLiteral846167854);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral846167854);
		StringU5BU5D_t2956870243* L_5 = L_4;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 5);
		ArrayElementTypeCheck (L_5, _stringLiteral461399800);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral461399800);
		StringU5BU5D_t2956870243* L_6 = L_5;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 6);
		ArrayElementTypeCheck (L_6, _stringLiteral1418492014);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral1418492014);
		StringU5BU5D_t2956870243* L_7 = L_6;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 7);
		ArrayElementTypeCheck (L_7, _stringLiteral1023579576);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(7), (String_t*)_stringLiteral1023579576);
		StringU5BU5D_t2956870243* L_8 = L_7;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 8);
		ArrayElementTypeCheck (L_8, _stringLiteral1457935802);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(8), (String_t*)_stringLiteral1457935802);
		StringU5BU5D_t2956870243* L_9 = L_8;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, ((int32_t)9));
		ArrayElementTypeCheck (L_9, _stringLiteral846187714);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (String_t*)_stringLiteral846187714);
		StringU5BU5D_t2956870243* L_10 = L_9;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, ((int32_t)10));
		ArrayElementTypeCheck (L_10, _stringLiteral461419660);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (String_t*)_stringLiteral461419660);
		StringU5BU5D_t2956870243* L_11 = L_10;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, ((int32_t)11));
		ArrayElementTypeCheck (L_11, _stringLiteral1418511874);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (String_t*)_stringLiteral1418511874);
		StringU5BU5D_t2956870243* L_12 = L_11;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, ((int32_t)12));
		ArrayElementTypeCheck (L_12, _stringLiteral1023599436);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (String_t*)_stringLiteral1023599436);
		StringU5BU5D_t2956870243* L_13 = L_12;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, ((int32_t)13));
		ArrayElementTypeCheck (L_13, _stringLiteral1666215746);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (String_t*)_stringLiteral1666215746);
		StringU5BU5D_t2956870243* L_14 = L_13;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, ((int32_t)14));
		ArrayElementTypeCheck (L_14, _stringLiteral112484876);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (String_t*)_stringLiteral112484876);
		StringU5BU5D_t2956870243* L_15 = L_14;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, ((int32_t)15));
		ArrayElementTypeCheck (L_15, _stringLiteral3486435458);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (String_t*)_stringLiteral3486435458);
		StringU5BU5D_t2956870243* L_16 = L_15;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, ((int32_t)16));
		ArrayElementTypeCheck (L_16, _stringLiteral2799279322);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)16)), (String_t*)_stringLiteral2799279322);
		StringU5BU5D_t2956870243* L_17 = L_16;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, ((int32_t)17));
		ArrayElementTypeCheck (L_17, _stringLiteral1457862114);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)17)), (String_t*)_stringLiteral1457862114);
		StringU5BU5D_t2956870243* L_18 = L_17;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, ((int32_t)18));
		ArrayElementTypeCheck (L_18, _stringLiteral2244053036);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)18)), (String_t*)_stringLiteral2244053036);
		StringU5BU5D_t2956870243* L_19 = L_18;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, ((int32_t)19));
		ArrayElementTypeCheck (L_19, _stringLiteral846167842);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)19)), (String_t*)_stringLiteral846167842);
		StringU5BU5D_t2956870243* L_20 = L_19;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, ((int32_t)20));
		ArrayElementTypeCheck (L_20, _stringLiteral461399788);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)20)), (String_t*)_stringLiteral461399788);
		StringU5BU5D_t2956870243* L_21 = L_20;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, ((int32_t)21));
		ArrayElementTypeCheck (L_21, _stringLiteral1418492002);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)21)), (String_t*)_stringLiteral1418492002);
		StringU5BU5D_t2956870243* L_22 = L_21;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, ((int32_t)22));
		ArrayElementTypeCheck (L_22, _stringLiteral1023579564);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)22)), (String_t*)_stringLiteral1023579564);
		StringU5BU5D_t2956870243* L_23 = L_22;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, ((int32_t)23));
		ArrayElementTypeCheck (L_23, _stringLiteral1666195874);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)23)), (String_t*)_stringLiteral1666195874);
		((XsdTime_t2013722311_StaticFields*)XsdTime_t2013722311_il2cpp_TypeInfo_var->static_fields)->set_timeFormats_61(L_23);
		return;
	}
}
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdTime::get_TokenizedType()
extern "C"  int32_t XsdTime_get_TokenizedType_m2777771359 (XsdTime_t2013722311 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(0);
	}
}
// System.Void Mono.Xml.Schema.XsdToken::.ctor()
extern "C"  void XsdToken__ctor_m2878046794 (XsdToken_t2806564481 * __this, const MethodInfo* method)
{
	{
		XsdNormalizedString__ctor_m1949672443(__this, /*hidden argument*/NULL);
		((XmlSchemaDatatype_t2590121 *)__this)->set_WhitespaceValue_0(2);
		return;
	}
}
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdToken::get_TokenizedType()
extern "C"  int32_t XsdToken_get_TokenizedType_m3748598641 (XsdToken_t2806564481 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(0);
	}
}
// System.Void Mono.Xml.Schema.XsdUnsignedByte::.ctor()
extern "C"  void XsdUnsignedByte__ctor_m471887846 (XsdUnsignedByte_t1376388247 * __this, const MethodInfo* method)
{
	{
		XsdUnsignedShort__ctor_m551853436(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.Schema.XsdUnsignedInt::.ctor()
extern "C"  void XsdUnsignedInt__ctor_m3112219817 (XsdUnsignedInt_t4045809666 * __this, const MethodInfo* method)
{
	{
		XsdUnsignedLong__ctor_m2340744434(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.Schema.XsdUnsignedLong::.ctor()
extern "C"  void XsdUnsignedLong__ctor_m2340744434 (XsdUnsignedLong_t1376676363 * __this, const MethodInfo* method)
{
	{
		XsdNonNegativeInteger__ctor_m1363977127(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.Schema.XsdUnsignedShort::.ctor()
extern "C"  void XsdUnsignedShort__ctor_m551853436 (XsdUnsignedShort_t244089871 * __this, const MethodInfo* method)
{
	{
		XsdUnsignedInt__ctor_m3112219817(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.XPath.Tokenizer::.ctor(System.String)
extern "C"  void Tokenizer__ctor_m3725140673 (Tokenizer_t3551440624 * __this, String_t* ___strInput0, const MethodInfo* method)
{
	{
		__this->set_m_iTokenPrev_4(((int32_t)258));
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___strInput0;
		__this->set_m_rgchInput_0(L_0);
		__this->set_m_ich_1(0);
		String_t* L_1 = ___strInput0;
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m2979997331(L_1, /*hidden argument*/NULL);
		__this->set_m_cch_2(L_2);
		Tokenizer_SkipWhitespace_m3223252775(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.XPath.Tokenizer::.cctor()
extern Il2CppClass* Hashtable_t3875263730_il2cpp_TypeInfo_var;
extern Il2CppClass* Tokenizer_t3551440624_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral96727;
extern Il2CppCodeGenString* _stringLiteral3555;
extern Il2CppCodeGenString* _stringLiteral99473;
extern Il2CppCodeGenString* _stringLiteral108290;
extern Il2CppCodeGenString* _stringLiteral3321137619;
extern Il2CppCodeGenString* _stringLiteral1566835452;
extern Il2CppCodeGenString* _stringLiteral13085340;
extern Il2CppCodeGenString* _stringLiteral94631196;
extern Il2CppCodeGenString* _stringLiteral3178663165;
extern Il2CppCodeGenString* _stringLiteral3547151398;
extern Il2CppCodeGenString* _stringLiteral765915793;
extern Il2CppCodeGenString* _stringLiteral2451430278;
extern Il2CppCodeGenString* _stringLiteral1252218203;
extern Il2CppCodeGenString* _stringLiteral3299543210;
extern Il2CppCodeGenString* _stringLiteral2914649283;
extern Il2CppCodeGenString* _stringLiteral341613496;
extern Il2CppCodeGenString* _stringLiteral3526476;
extern Il2CppCodeGenString* _stringLiteral950398559;
extern Il2CppCodeGenString* _stringLiteral3556653;
extern Il2CppCodeGenString* _stringLiteral3628607700;
extern Il2CppCodeGenString* _stringLiteral3386882;
extern const uint32_t Tokenizer__cctor_m2939925996_MetadataUsageId;
extern "C"  void Tokenizer__cctor_m2939925996 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Tokenizer__cctor_m2939925996_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		Hashtable_t3875263730 * L_0 = (Hashtable_t3875263730 *)il2cpp_codegen_object_new(Hashtable_t3875263730_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1514037738(L_0, /*hidden argument*/NULL);
		((Tokenizer_t3551440624_StaticFields*)Tokenizer_t3551440624_il2cpp_TypeInfo_var->static_fields)->set_s_mapTokens_8(L_0);
		ObjectU5BU5D_t11523773* L_1 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)((int32_t)42)));
		int32_t L_2 = ((int32_t)274);
		Il2CppObject * L_3 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t11523773* L_4 = L_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, _stringLiteral96727);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)_stringLiteral96727);
		ObjectU5BU5D_t11523773* L_5 = L_4;
		int32_t L_6 = ((int32_t)276);
		Il2CppObject * L_7 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 2);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_7);
		ObjectU5BU5D_t11523773* L_8 = L_5;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 3);
		ArrayElementTypeCheck (L_8, _stringLiteral3555);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)_stringLiteral3555);
		ObjectU5BU5D_t11523773* L_9 = L_8;
		int32_t L_10 = ((int32_t)278);
		Il2CppObject * L_11 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 4);
		ArrayElementTypeCheck (L_9, L_11);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)L_11);
		ObjectU5BU5D_t11523773* L_12 = L_9;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 5);
		ArrayElementTypeCheck (L_12, _stringLiteral99473);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)_stringLiteral99473);
		ObjectU5BU5D_t11523773* L_13 = L_12;
		int32_t L_14 = ((int32_t)280);
		Il2CppObject * L_15 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 6);
		ArrayElementTypeCheck (L_13, L_15);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)L_15);
		ObjectU5BU5D_t11523773* L_16 = L_13;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 7);
		ArrayElementTypeCheck (L_16, _stringLiteral108290);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(7), (Il2CppObject *)_stringLiteral108290);
		ObjectU5BU5D_t11523773* L_17 = L_16;
		int32_t L_18 = ((int32_t)296);
		Il2CppObject * L_19 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_18);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 8);
		ArrayElementTypeCheck (L_17, L_19);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)L_19);
		ObjectU5BU5D_t11523773* L_20 = L_17;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, ((int32_t)9));
		ArrayElementTypeCheck (L_20, _stringLiteral3321137619);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (Il2CppObject *)_stringLiteral3321137619);
		ObjectU5BU5D_t11523773* L_21 = L_20;
		int32_t L_22 = ((int32_t)298);
		Il2CppObject * L_23 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_22);
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, ((int32_t)10));
		ArrayElementTypeCheck (L_21, L_23);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (Il2CppObject *)L_23);
		ObjectU5BU5D_t11523773* L_24 = L_21;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, ((int32_t)11));
		ArrayElementTypeCheck (L_24, _stringLiteral1566835452);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (Il2CppObject *)_stringLiteral1566835452);
		ObjectU5BU5D_t11523773* L_25 = L_24;
		int32_t L_26 = ((int32_t)300);
		Il2CppObject * L_27 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_26);
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, ((int32_t)12));
		ArrayElementTypeCheck (L_25, L_27);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (Il2CppObject *)L_27);
		ObjectU5BU5D_t11523773* L_28 = L_25;
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, ((int32_t)13));
		ArrayElementTypeCheck (L_28, _stringLiteral13085340);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (Il2CppObject *)_stringLiteral13085340);
		ObjectU5BU5D_t11523773* L_29 = L_28;
		int32_t L_30 = ((int32_t)302);
		Il2CppObject * L_31 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_30);
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, ((int32_t)14));
		ArrayElementTypeCheck (L_29, L_31);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (Il2CppObject *)L_31);
		ObjectU5BU5D_t11523773* L_32 = L_29;
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, ((int32_t)15));
		ArrayElementTypeCheck (L_32, _stringLiteral94631196);
		(L_32)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (Il2CppObject *)_stringLiteral94631196);
		ObjectU5BU5D_t11523773* L_33 = L_32;
		int32_t L_34 = ((int32_t)304);
		Il2CppObject * L_35 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_34);
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, ((int32_t)16));
		ArrayElementTypeCheck (L_33, L_35);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)16)), (Il2CppObject *)L_35);
		ObjectU5BU5D_t11523773* L_36 = L_33;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, ((int32_t)17));
		ArrayElementTypeCheck (L_36, _stringLiteral3178663165);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)17)), (Il2CppObject *)_stringLiteral3178663165);
		ObjectU5BU5D_t11523773* L_37 = L_36;
		int32_t L_38 = ((int32_t)306);
		Il2CppObject * L_39 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_38);
		NullCheck(L_37);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_37, ((int32_t)18));
		ArrayElementTypeCheck (L_37, L_39);
		(L_37)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)18)), (Il2CppObject *)L_39);
		ObjectU5BU5D_t11523773* L_40 = L_37;
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, ((int32_t)19));
		ArrayElementTypeCheck (L_40, _stringLiteral3547151398);
		(L_40)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)19)), (Il2CppObject *)_stringLiteral3547151398);
		ObjectU5BU5D_t11523773* L_41 = L_40;
		int32_t L_42 = ((int32_t)308);
		Il2CppObject * L_43 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_42);
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, ((int32_t)20));
		ArrayElementTypeCheck (L_41, L_43);
		(L_41)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)20)), (Il2CppObject *)L_43);
		ObjectU5BU5D_t11523773* L_44 = L_41;
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, ((int32_t)21));
		ArrayElementTypeCheck (L_44, _stringLiteral765915793);
		(L_44)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)21)), (Il2CppObject *)_stringLiteral765915793);
		ObjectU5BU5D_t11523773* L_45 = L_44;
		int32_t L_46 = ((int32_t)310);
		Il2CppObject * L_47 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_46);
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, ((int32_t)22));
		ArrayElementTypeCheck (L_45, L_47);
		(L_45)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)22)), (Il2CppObject *)L_47);
		ObjectU5BU5D_t11523773* L_48 = L_45;
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, ((int32_t)23));
		ArrayElementTypeCheck (L_48, _stringLiteral2451430278);
		(L_48)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)23)), (Il2CppObject *)_stringLiteral2451430278);
		ObjectU5BU5D_t11523773* L_49 = L_48;
		int32_t L_50 = ((int32_t)312);
		Il2CppObject * L_51 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_50);
		NullCheck(L_49);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_49, ((int32_t)24));
		ArrayElementTypeCheck (L_49, L_51);
		(L_49)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)24)), (Il2CppObject *)L_51);
		ObjectU5BU5D_t11523773* L_52 = L_49;
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, ((int32_t)25));
		ArrayElementTypeCheck (L_52, _stringLiteral1252218203);
		(L_52)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)25)), (Il2CppObject *)_stringLiteral1252218203);
		ObjectU5BU5D_t11523773* L_53 = L_52;
		int32_t L_54 = ((int32_t)314);
		Il2CppObject * L_55 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_54);
		NullCheck(L_53);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_53, ((int32_t)26));
		ArrayElementTypeCheck (L_53, L_55);
		(L_53)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)26)), (Il2CppObject *)L_55);
		ObjectU5BU5D_t11523773* L_56 = L_53;
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, ((int32_t)27));
		ArrayElementTypeCheck (L_56, _stringLiteral3299543210);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)27)), (Il2CppObject *)_stringLiteral3299543210);
		ObjectU5BU5D_t11523773* L_57 = L_56;
		int32_t L_58 = ((int32_t)316);
		Il2CppObject * L_59 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_58);
		NullCheck(L_57);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_57, ((int32_t)28));
		ArrayElementTypeCheck (L_57, L_59);
		(L_57)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)28)), (Il2CppObject *)L_59);
		ObjectU5BU5D_t11523773* L_60 = L_57;
		NullCheck(L_60);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_60, ((int32_t)29));
		ArrayElementTypeCheck (L_60, _stringLiteral2914649283);
		(L_60)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)29)), (Il2CppObject *)_stringLiteral2914649283);
		ObjectU5BU5D_t11523773* L_61 = L_60;
		int32_t L_62 = ((int32_t)318);
		Il2CppObject * L_63 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_62);
		NullCheck(L_61);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_61, ((int32_t)30));
		ArrayElementTypeCheck (L_61, L_63);
		(L_61)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)30)), (Il2CppObject *)L_63);
		ObjectU5BU5D_t11523773* L_64 = L_61;
		NullCheck(L_64);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_64, ((int32_t)31));
		ArrayElementTypeCheck (L_64, _stringLiteral341613496);
		(L_64)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)31)), (Il2CppObject *)_stringLiteral341613496);
		ObjectU5BU5D_t11523773* L_65 = L_64;
		int32_t L_66 = ((int32_t)320);
		Il2CppObject * L_67 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_66);
		NullCheck(L_65);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_65, ((int32_t)32));
		ArrayElementTypeCheck (L_65, L_67);
		(L_65)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)32)), (Il2CppObject *)L_67);
		ObjectU5BU5D_t11523773* L_68 = L_65;
		NullCheck(L_68);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_68, ((int32_t)33));
		ArrayElementTypeCheck (L_68, _stringLiteral3526476);
		(L_68)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)33)), (Il2CppObject *)_stringLiteral3526476);
		ObjectU5BU5D_t11523773* L_69 = L_68;
		int32_t L_70 = ((int32_t)322);
		Il2CppObject * L_71 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_70);
		NullCheck(L_69);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_69, ((int32_t)34));
		ArrayElementTypeCheck (L_69, L_71);
		(L_69)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)34)), (Il2CppObject *)L_71);
		ObjectU5BU5D_t11523773* L_72 = L_69;
		NullCheck(L_72);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_72, ((int32_t)35));
		ArrayElementTypeCheck (L_72, _stringLiteral950398559);
		(L_72)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)35)), (Il2CppObject *)_stringLiteral950398559);
		ObjectU5BU5D_t11523773* L_73 = L_72;
		int32_t L_74 = ((int32_t)324);
		Il2CppObject * L_75 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_74);
		NullCheck(L_73);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_73, ((int32_t)36));
		ArrayElementTypeCheck (L_73, L_75);
		(L_73)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)36)), (Il2CppObject *)L_75);
		ObjectU5BU5D_t11523773* L_76 = L_73;
		NullCheck(L_76);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_76, ((int32_t)37));
		ArrayElementTypeCheck (L_76, _stringLiteral3556653);
		(L_76)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)37)), (Il2CppObject *)_stringLiteral3556653);
		ObjectU5BU5D_t11523773* L_77 = L_76;
		int32_t L_78 = ((int32_t)326);
		Il2CppObject * L_79 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_78);
		NullCheck(L_77);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_77, ((int32_t)38));
		ArrayElementTypeCheck (L_77, L_79);
		(L_77)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)38)), (Il2CppObject *)L_79);
		ObjectU5BU5D_t11523773* L_80 = L_77;
		NullCheck(L_80);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_80, ((int32_t)39));
		ArrayElementTypeCheck (L_80, _stringLiteral3628607700);
		(L_80)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)39)), (Il2CppObject *)_stringLiteral3628607700);
		ObjectU5BU5D_t11523773* L_81 = L_80;
		int32_t L_82 = ((int32_t)328);
		Il2CppObject * L_83 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_82);
		NullCheck(L_81);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_81, ((int32_t)40));
		ArrayElementTypeCheck (L_81, L_83);
		(L_81)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)40)), (Il2CppObject *)L_83);
		ObjectU5BU5D_t11523773* L_84 = L_81;
		NullCheck(L_84);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_84, ((int32_t)41));
		ArrayElementTypeCheck (L_84, _stringLiteral3386882);
		(L_84)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)41)), (Il2CppObject *)_stringLiteral3386882);
		((Tokenizer_t3551440624_StaticFields*)Tokenizer_t3551440624_il2cpp_TypeInfo_var->static_fields)->set_s_rgTokenMap_9(L_84);
		V_0 = 0;
		goto IL_0215;
	}

IL_01f7:
	{
		Hashtable_t3875263730 * L_85 = ((Tokenizer_t3551440624_StaticFields*)Tokenizer_t3551440624_il2cpp_TypeInfo_var->static_fields)->get_s_mapTokens_8();
		ObjectU5BU5D_t11523773* L_86 = ((Tokenizer_t3551440624_StaticFields*)Tokenizer_t3551440624_il2cpp_TypeInfo_var->static_fields)->get_s_rgTokenMap_9();
		int32_t L_87 = V_0;
		NullCheck(L_86);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_86, ((int32_t)((int32_t)L_87+(int32_t)1)));
		int32_t L_88 = ((int32_t)((int32_t)L_87+(int32_t)1));
		ObjectU5BU5D_t11523773* L_89 = ((Tokenizer_t3551440624_StaticFields*)Tokenizer_t3551440624_il2cpp_TypeInfo_var->static_fields)->get_s_rgTokenMap_9();
		int32_t L_90 = V_0;
		NullCheck(L_89);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_89, L_90);
		int32_t L_91 = L_90;
		NullCheck(L_85);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_85, ((L_86)->GetAt(static_cast<il2cpp_array_size_t>(L_88))), ((L_89)->GetAt(static_cast<il2cpp_array_size_t>(L_91))));
		int32_t L_92 = V_0;
		V_0 = ((int32_t)((int32_t)L_92+(int32_t)2));
	}

IL_0215:
	{
		int32_t L_93 = V_0;
		ObjectU5BU5D_t11523773* L_94 = ((Tokenizer_t3551440624_StaticFields*)Tokenizer_t3551440624_il2cpp_TypeInfo_var->static_fields)->get_s_rgTokenMap_9();
		NullCheck(L_94);
		if ((((int32_t)L_93) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_94)->max_length)))))))
		{
			goto IL_01f7;
		}
	}
	{
		return;
	}
}
// System.Char Mono.Xml.XPath.Tokenizer::Peek(System.Int32)
extern "C"  uint16_t Tokenizer_Peek_m3172346797 (Tokenizer_t3551440624 * __this, int32_t ___iOffset0, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_ich_1();
		int32_t L_1 = ___iOffset0;
		int32_t L_2 = __this->get_m_cch_2();
		if ((((int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1))) < ((int32_t)L_2)))
		{
			goto IL_0015;
		}
	}
	{
		return 0;
	}

IL_0015:
	{
		String_t* L_3 = __this->get_m_rgchInput_0();
		int32_t L_4 = __this->get_m_ich_1();
		int32_t L_5 = ___iOffset0;
		NullCheck(L_3);
		uint16_t L_6 = String_get_Chars_m3015341861(L_3, ((int32_t)((int32_t)L_4+(int32_t)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Char Mono.Xml.XPath.Tokenizer::Peek()
extern "C"  uint16_t Tokenizer_Peek_m4053934172 (Tokenizer_t3551440624 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = Tokenizer_Peek_m3172346797(__this, 0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Char Mono.Xml.XPath.Tokenizer::GetChar()
extern "C"  uint16_t Tokenizer_GetChar_m3293081869 (Tokenizer_t3551440624 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_ich_1();
		int32_t L_1 = __this->get_m_cch_2();
		if ((((int32_t)L_0) < ((int32_t)L_1)))
		{
			goto IL_0013;
		}
	}
	{
		return 0;
	}

IL_0013:
	{
		String_t* L_2 = __this->get_m_rgchInput_0();
		int32_t L_3 = __this->get_m_ich_1();
		int32_t L_4 = L_3;
		V_0 = L_4;
		__this->set_m_ich_1(((int32_t)((int32_t)L_4+(int32_t)1)));
		int32_t L_5 = V_0;
		NullCheck(L_2);
		uint16_t L_6 = String_get_Chars_m3015341861(L_2, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Char Mono.Xml.XPath.Tokenizer::PutBack()
extern Il2CppClass* XPathException_t2353341743_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1895083455;
extern const uint32_t Tokenizer_PutBack_m1991014231_MetadataUsageId;
extern "C"  uint16_t Tokenizer_PutBack_m1991014231 (Tokenizer_t3551440624 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Tokenizer_PutBack_m1991014231_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_ich_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		XPathException_t2353341743 * L_1 = (XPathException_t2353341743 *)il2cpp_codegen_object_new(XPathException_t2353341743_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_1, _stringLiteral1895083455, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		String_t* L_2 = __this->get_m_rgchInput_0();
		int32_t L_3 = __this->get_m_ich_1();
		int32_t L_4 = ((int32_t)((int32_t)L_3-(int32_t)1));
		V_0 = L_4;
		__this->set_m_ich_1(L_4);
		int32_t L_5 = V_0;
		NullCheck(L_2);
		uint16_t L_6 = String_get_Chars_m3015341861(L_2, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Boolean Mono.Xml.XPath.Tokenizer::SkipWhitespace()
extern Il2CppClass* Tokenizer_t3551440624_il2cpp_TypeInfo_var;
extern const uint32_t Tokenizer_SkipWhitespace_m3223252775_MetadataUsageId;
extern "C"  bool Tokenizer_SkipWhitespace_m3223252775 (Tokenizer_t3551440624 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Tokenizer_SkipWhitespace_m3223252775_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		uint16_t L_0 = Tokenizer_Peek_m4053934172(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Tokenizer_t3551440624_il2cpp_TypeInfo_var);
		bool L_1 = Tokenizer_IsWhitespace_m596496075(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		return (bool)0;
	}

IL_0012:
	{
		goto IL_001e;
	}

IL_0017:
	{
		Tokenizer_GetChar_m3293081869(__this, /*hidden argument*/NULL);
	}

IL_001e:
	{
		uint16_t L_2 = Tokenizer_Peek_m4053934172(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Tokenizer_t3551440624_il2cpp_TypeInfo_var);
		bool L_3 = Tokenizer_IsWhitespace_m596496075(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0017;
		}
	}
	{
		return (bool)1;
	}
}
// System.Int32 Mono.Xml.XPath.Tokenizer::ParseNumber()
extern Il2CppClass* StringBuilder_t3822575854_il2cpp_TypeInfo_var;
extern Il2CppClass* Tokenizer_t3551440624_il2cpp_TypeInfo_var;
extern Il2CppClass* NumberFormatInfo_t3411951076_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern const uint32_t Tokenizer_ParseNumber_m2376763193_MetadataUsageId;
extern "C"  int32_t Tokenizer_ParseNumber_m2376763193 (Tokenizer_t3551440624 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Tokenizer_ParseNumber_m2376763193_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t3822575854 * V_0 = NULL;
	{
		StringBuilder_t3822575854 * L_0 = (StringBuilder_t3822575854 *)il2cpp_codegen_object_new(StringBuilder_t3822575854_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m135953004(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0018;
	}

IL_000b:
	{
		StringBuilder_t3822575854 * L_1 = V_0;
		uint16_t L_2 = Tokenizer_GetChar_m3293081869(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		StringBuilder_Append_m2143093878(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0018:
	{
		uint16_t L_3 = Tokenizer_Peek_m4053934172(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Tokenizer_t3551440624_il2cpp_TypeInfo_var);
		bool L_4 = Tokenizer_IsDigit_m448102595(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_000b;
		}
	}
	{
		uint16_t L_5 = Tokenizer_Peek_m4053934172(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_5) == ((uint32_t)((int32_t)46)))))
		{
			goto IL_0064;
		}
	}
	{
		StringBuilder_t3822575854 * L_6 = V_0;
		uint16_t L_7 = Tokenizer_GetChar_m3293081869(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		StringBuilder_Append_m2143093878(L_6, L_7, /*hidden argument*/NULL);
		goto IL_0054;
	}

IL_0047:
	{
		StringBuilder_t3822575854 * L_8 = V_0;
		uint16_t L_9 = Tokenizer_GetChar_m3293081869(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		StringBuilder_Append_m2143093878(L_8, L_9, /*hidden argument*/NULL);
	}

IL_0054:
	{
		uint16_t L_10 = Tokenizer_Peek_m4053934172(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Tokenizer_t3551440624_il2cpp_TypeInfo_var);
		bool L_11 = Tokenizer_IsDigit_m448102595(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_0047;
		}
	}

IL_0064:
	{
		StringBuilder_t3822575854 * L_12 = V_0;
		NullCheck(L_12);
		String_t* L_13 = StringBuilder_ToString_m350379841(L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NumberFormatInfo_t3411951076_il2cpp_TypeInfo_var);
		NumberFormatInfo_t3411951076 * L_14 = NumberFormatInfo_get_InvariantInfo_m1900501910(NULL /*static, unused*/, /*hidden argument*/NULL);
		double L_15 = Double_Parse_m2930245738(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		double L_16 = L_15;
		Il2CppObject * L_17 = Box(Double_t534516614_il2cpp_TypeInfo_var, &L_16);
		__this->set_m_objToken_5(L_17);
		return ((int32_t)331);
	}
}
// System.Int32 Mono.Xml.XPath.Tokenizer::ParseLiteral()
extern Il2CppClass* StringBuilder_t3822575854_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t2778706699_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathException_t2353341743_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1823146261;
extern Il2CppCodeGenString* _stringLiteral2405292115;
extern const uint32_t Tokenizer_ParseLiteral_m2561279649_MetadataUsageId;
extern "C"  int32_t Tokenizer_ParseLiteral_m2561279649 (Tokenizer_t3551440624 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Tokenizer_ParseLiteral_m2561279649_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t3822575854 * V_0 = NULL;
	uint16_t V_1 = 0x0;
	uint16_t V_2 = 0x0;
	{
		StringBuilder_t3822575854 * L_0 = (StringBuilder_t3822575854 *)il2cpp_codegen_object_new(StringBuilder_t3822575854_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m135953004(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		uint16_t L_1 = Tokenizer_GetChar_m3293081869(__this, /*hidden argument*/NULL);
		V_1 = L_1;
		goto IL_0040;
	}

IL_0012:
	{
		uint16_t L_2 = V_2;
		if (L_2)
		{
			goto IL_0033;
		}
	}
	{
		uint16_t L_3 = V_1;
		uint16_t L_4 = L_3;
		Il2CppObject * L_5 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m2809334143(NULL /*static, unused*/, _stringLiteral1823146261, L_5, _stringLiteral2405292115, /*hidden argument*/NULL);
		XPathException_t2353341743 * L_7 = (XPathException_t2353341743 *)il2cpp_codegen_object_new(XPathException_t2353341743_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_7, L_6, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0033:
	{
		StringBuilder_t3822575854 * L_8 = V_0;
		uint16_t L_9 = Tokenizer_GetChar_m3293081869(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		StringBuilder_Append_m2143093878(L_8, L_9, /*hidden argument*/NULL);
	}

IL_0040:
	{
		uint16_t L_10 = Tokenizer_Peek_m4053934172(__this, /*hidden argument*/NULL);
		uint16_t L_11 = L_10;
		V_2 = L_11;
		uint16_t L_12 = V_1;
		if ((!(((uint32_t)L_11) == ((uint32_t)L_12))))
		{
			goto IL_0012;
		}
	}
	{
		Tokenizer_GetChar_m3293081869(__this, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_13 = V_0;
		NullCheck(L_13);
		String_t* L_14 = StringBuilder_ToString_m350379841(L_13, /*hidden argument*/NULL);
		__this->set_m_objToken_5(L_14);
		return ((int32_t)332);
	}
}
// System.String Mono.Xml.XPath.Tokenizer::ReadIdentifier()
extern Il2CppClass* StringBuilder_t3822575854_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t2778706699_il2cpp_TypeInfo_var;
extern const uint32_t Tokenizer_ReadIdentifier_m224120005_MetadataUsageId;
extern "C"  String_t* Tokenizer_ReadIdentifier_m224120005 (Tokenizer_t3551440624 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Tokenizer_ReadIdentifier_m224120005_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t3822575854 * V_0 = NULL;
	uint16_t V_1 = 0x0;
	{
		StringBuilder_t3822575854 * L_0 = (StringBuilder_t3822575854 *)il2cpp_codegen_object_new(StringBuilder_t3822575854_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m135953004(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		uint16_t L_1 = Tokenizer_Peek_m4053934172(__this, /*hidden argument*/NULL);
		V_1 = L_1;
		uint16_t L_2 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Char_t2778706699_il2cpp_TypeInfo_var);
		bool L_3 = Char_IsLetter_m1699036490(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0022;
		}
	}
	{
		uint16_t L_4 = V_1;
		if ((((int32_t)L_4) == ((int32_t)((int32_t)95))))
		{
			goto IL_0022;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_0022:
	{
		StringBuilder_t3822575854 * L_5 = V_0;
		uint16_t L_6 = Tokenizer_GetChar_m3293081869(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		StringBuilder_Append_m2143093878(L_5, L_6, /*hidden argument*/NULL);
		goto IL_0041;
	}

IL_0034:
	{
		StringBuilder_t3822575854 * L_7 = V_0;
		uint16_t L_8 = Tokenizer_GetChar_m3293081869(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		StringBuilder_Append_m2143093878(L_7, L_8, /*hidden argument*/NULL);
	}

IL_0041:
	{
		uint16_t L_9 = Tokenizer_Peek_m4053934172(__this, /*hidden argument*/NULL);
		uint16_t L_10 = L_9;
		V_1 = L_10;
		if ((((int32_t)L_10) == ((int32_t)((int32_t)95))))
		{
			goto IL_0034;
		}
	}
	{
		uint16_t L_11 = V_1;
		if ((((int32_t)L_11) == ((int32_t)((int32_t)45))))
		{
			goto IL_0034;
		}
	}
	{
		uint16_t L_12 = V_1;
		if ((((int32_t)L_12) == ((int32_t)((int32_t)46))))
		{
			goto IL_0034;
		}
	}
	{
		uint16_t L_13 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Char_t2778706699_il2cpp_TypeInfo_var);
		bool L_14 = Char_IsLetterOrDigit_m2290383044(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_0034;
		}
	}
	{
		Tokenizer_SkipWhitespace_m3223252775(__this, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_15 = V_0;
		NullCheck(L_15);
		String_t* L_16 = StringBuilder_ToString_m350379841(L_15, /*hidden argument*/NULL);
		return L_16;
	}
}
// System.Int32 Mono.Xml.XPath.Tokenizer::ParseIdentifier()
extern Il2CppClass* Tokenizer_t3551440624_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathException_t2353341743_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlQualifiedName_t176365656_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t2778706699_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2162601312;
extern Il2CppCodeGenString* _stringLiteral39;
extern Il2CppCodeGenString* _stringLiteral4093140345;
extern Il2CppCodeGenString* _stringLiteral58;
extern Il2CppCodeGenString* _stringLiteral521031395;
extern Il2CppCodeGenString* _stringLiteral3298764311;
extern const uint32_t Tokenizer_ParseIdentifier_m151998745_MetadataUsageId;
extern "C"  int32_t Tokenizer_ParseIdentifier_m151998745 (Tokenizer_t3551440624 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Tokenizer_ParseIdentifier_m151998745_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	int32_t V_2 = 0;
	uint16_t V_3 = 0x0;
	String_t* V_4 = NULL;
	int32_t G_B3_0 = 0;
	{
		String_t* L_0 = Tokenizer_ReadIdentifier_m224120005(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(Tokenizer_t3551440624_il2cpp_TypeInfo_var);
		Hashtable_t3875263730 * L_1 = ((Tokenizer_t3551440624_StaticFields*)Tokenizer_t3551440624_il2cpp_TypeInfo_var->static_fields)->get_s_mapTokens_8();
		String_t* L_2 = V_0;
		NullCheck(L_1);
		Il2CppObject * L_3 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_1, L_2);
		V_1 = L_3;
		Il2CppObject * L_4 = V_1;
		if (!L_4)
		{
			goto IL_0024;
		}
	}
	{
		Il2CppObject * L_5 = V_1;
		G_B3_0 = ((*(int32_t*)((int32_t*)UnBox (L_5, Int32_t2847414787_il2cpp_TypeInfo_var))));
		goto IL_0029;
	}

IL_0024:
	{
		G_B3_0 = ((int32_t)333);
	}

IL_0029:
	{
		V_2 = G_B3_0;
		String_t* L_6 = V_0;
		__this->set_m_objToken_5(L_6);
		uint16_t L_7 = Tokenizer_Peek_m4053934172(__this, /*hidden argument*/NULL);
		V_3 = L_7;
		uint16_t L_8 = V_3;
		if ((!(((uint32_t)L_8) == ((uint32_t)((int32_t)58)))))
		{
			goto IL_0119;
		}
	}
	{
		uint16_t L_9 = Tokenizer_Peek_m3172346797(__this, 1, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)58)))))
		{
			goto IL_0078;
		}
	}
	{
		Il2CppObject * L_10 = V_1;
		if (!L_10)
		{
			goto IL_0060;
		}
	}
	{
		int32_t L_11 = V_2;
		bool L_12 = Tokenizer_IsAxisName_m1626818866(__this, L_11, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_0076;
		}
	}

IL_0060:
	{
		String_t* L_13 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral2162601312, L_13, _stringLiteral39, /*hidden argument*/NULL);
		XPathException_t2353341743 * L_15 = (XPathException_t2353341743 *)il2cpp_codegen_object_new(XPathException_t2353341743_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_15, L_14, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_15);
	}

IL_0076:
	{
		int32_t L_16 = V_2;
		return L_16;
	}

IL_0078:
	{
		Tokenizer_GetChar_m3293081869(__this, /*hidden argument*/NULL);
		Tokenizer_SkipWhitespace_m3223252775(__this, /*hidden argument*/NULL);
		uint16_t L_17 = Tokenizer_Peek_m4053934172(__this, /*hidden argument*/NULL);
		V_3 = L_17;
		uint16_t L_18 = V_3;
		if ((!(((uint32_t)L_18) == ((uint32_t)((int32_t)42)))))
		{
			goto IL_00b3;
		}
	}
	{
		Tokenizer_GetChar_m3293081869(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_20 = V_0;
		XmlQualifiedName_t176365656 * L_21 = (XmlQualifiedName_t176365656 *)il2cpp_codegen_object_new(XmlQualifiedName_t176365656_il2cpp_TypeInfo_var);
		XmlQualifiedName__ctor_m2754617635(L_21, L_19, L_20, /*hidden argument*/NULL);
		__this->set_m_objToken_5(L_21);
		return ((int32_t)333);
	}

IL_00b3:
	{
		String_t* L_22 = Tokenizer_ReadIdentifier_m224120005(__this, /*hidden argument*/NULL);
		V_4 = L_22;
		String_t* L_23 = V_4;
		if (L_23)
		{
			goto IL_00f0;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_24 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 0);
		ArrayElementTypeCheck (L_24, _stringLiteral4093140345);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral4093140345);
		ObjectU5BU5D_t11523773* L_25 = L_24;
		String_t* L_26 = V_0;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 1);
		ArrayElementTypeCheck (L_25, L_26);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_26);
		ObjectU5BU5D_t11523773* L_27 = L_25;
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, 2);
		ArrayElementTypeCheck (L_27, _stringLiteral58);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral58);
		ObjectU5BU5D_t11523773* L_28 = L_27;
		uint16_t L_29 = V_3;
		uint16_t L_30 = L_29;
		Il2CppObject * L_31 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_30);
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, 3);
		ArrayElementTypeCheck (L_28, L_31);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_31);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m3016520001(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		XPathException_t2353341743 * L_33 = (XPathException_t2353341743 *)il2cpp_codegen_object_new(XPathException_t2353341743_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_33, L_32, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_33);
	}

IL_00f0:
	{
		uint16_t L_34 = Tokenizer_Peek_m4053934172(__this, /*hidden argument*/NULL);
		V_3 = L_34;
		String_t* L_35 = V_4;
		String_t* L_36 = V_0;
		XmlQualifiedName_t176365656 * L_37 = (XmlQualifiedName_t176365656 *)il2cpp_codegen_object_new(XmlQualifiedName_t176365656_il2cpp_TypeInfo_var);
		XmlQualifiedName__ctor_m2754617635(L_37, L_35, L_36, /*hidden argument*/NULL);
		__this->set_m_objToken_5(L_37);
		uint16_t L_38 = V_3;
		if ((!(((uint32_t)L_38) == ((uint32_t)((int32_t)40)))))
		{
			goto IL_0113;
		}
	}
	{
		return ((int32_t)269);
	}

IL_0113:
	{
		return ((int32_t)333);
	}

IL_0119:
	{
		bool L_39 = Tokenizer_get_IsFirstToken_m1142525831(__this, /*hidden argument*/NULL);
		if (L_39)
		{
			goto IL_0159;
		}
	}
	{
		bool L_40 = __this->get_m_fPrevWasOperator_6();
		if (L_40)
		{
			goto IL_0159;
		}
	}
	{
		Il2CppObject * L_41 = V_1;
		if (!L_41)
		{
			goto IL_0141;
		}
	}
	{
		int32_t L_42 = V_2;
		bool L_43 = Tokenizer_IsOperatorName_m3788670325(__this, L_42, /*hidden argument*/NULL);
		if (L_43)
		{
			goto IL_0157;
		}
	}

IL_0141:
	{
		String_t* L_44 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_45 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral521031395, L_44, _stringLiteral39, /*hidden argument*/NULL);
		XPathException_t2353341743 * L_46 = (XPathException_t2353341743 *)il2cpp_codegen_object_new(XPathException_t2353341743_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_46, L_45, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_46);
	}

IL_0157:
	{
		int32_t L_47 = V_2;
		return L_47;
	}

IL_0159:
	{
		uint16_t L_48 = V_3;
		if ((!(((uint32_t)L_48) == ((uint32_t)((int32_t)40)))))
		{
			goto IL_01a2;
		}
	}
	{
		Il2CppObject * L_49 = V_1;
		if (L_49)
		{
			goto IL_017e;
		}
	}
	{
		String_t* L_50 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_51 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		XmlQualifiedName_t176365656 * L_52 = (XmlQualifiedName_t176365656 *)il2cpp_codegen_object_new(XmlQualifiedName_t176365656_il2cpp_TypeInfo_var);
		XmlQualifiedName__ctor_m2754617635(L_52, L_50, L_51, /*hidden argument*/NULL);
		__this->set_m_objToken_5(L_52);
		return ((int32_t)269);
	}

IL_017e:
	{
		int32_t L_53 = V_2;
		bool L_54 = Tokenizer_IsNodeType_m3472117698(__this, L_53, /*hidden argument*/NULL);
		if (!L_54)
		{
			goto IL_018c;
		}
	}
	{
		int32_t L_55 = V_2;
		return L_55;
	}

IL_018c:
	{
		String_t* L_56 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_57 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral3298764311, L_56, _stringLiteral39, /*hidden argument*/NULL);
		XPathException_t2353341743 * L_58 = (XPathException_t2353341743 *)il2cpp_codegen_object_new(XPathException_t2353341743_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_58, L_57, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_58);
	}

IL_01a2:
	{
		String_t* L_59 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_60 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		XmlQualifiedName_t176365656 * L_61 = (XmlQualifiedName_t176365656 *)il2cpp_codegen_object_new(XmlQualifiedName_t176365656_il2cpp_TypeInfo_var);
		XmlQualifiedName__ctor_m2754617635(L_61, L_59, L_60, /*hidden argument*/NULL);
		__this->set_m_objToken_5(L_61);
		return ((int32_t)333);
	}
}
// System.Boolean Mono.Xml.XPath.Tokenizer::IsWhitespace(System.Char)
extern "C"  bool Tokenizer_IsWhitespace_m596496075 (Il2CppObject * __this /* static, unused */, uint16_t ___ch0, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		uint16_t L_0 = ___ch0;
		if ((((int32_t)L_0) == ((int32_t)((int32_t)32))))
		{
			goto IL_001f;
		}
	}
	{
		uint16_t L_1 = ___ch0;
		if ((((int32_t)L_1) == ((int32_t)((int32_t)9))))
		{
			goto IL_001f;
		}
	}
	{
		uint16_t L_2 = ___ch0;
		if ((((int32_t)L_2) == ((int32_t)((int32_t)10))))
		{
			goto IL_001f;
		}
	}
	{
		uint16_t L_3 = ___ch0;
		G_B5_0 = ((((int32_t)L_3) == ((int32_t)((int32_t)13)))? 1 : 0);
		goto IL_0020;
	}

IL_001f:
	{
		G_B5_0 = 1;
	}

IL_0020:
	{
		return (bool)G_B5_0;
	}
}
// System.Boolean Mono.Xml.XPath.Tokenizer::IsDigit(System.Char)
extern "C"  bool Tokenizer_IsDigit_m448102595 (Il2CppObject * __this /* static, unused */, uint16_t ___ch0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		uint16_t L_0 = ___ch0;
		if ((((int32_t)L_0) < ((int32_t)((int32_t)48))))
		{
			goto IL_0012;
		}
	}
	{
		uint16_t L_1 = ___ch0;
		G_B3_0 = ((((int32_t)((((int32_t)L_1) > ((int32_t)((int32_t)57)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0013;
	}

IL_0012:
	{
		G_B3_0 = 0;
	}

IL_0013:
	{
		return (bool)G_B3_0;
	}
}
// System.Int32 Mono.Xml.XPath.Tokenizer::ParseToken()
extern Il2CppClass* Tokenizer_t3551440624_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t2778706699_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathException_t2353341743_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2704140593;
extern Il2CppCodeGenString* _stringLiteral39;
extern const uint32_t Tokenizer_ParseToken_m1903029675_MetadataUsageId;
extern "C"  int32_t Tokenizer_ParseToken_m1903029675 (Tokenizer_t3551440624 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Tokenizer_ParseToken_m1903029675_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0x0;
	int32_t V_1 = 0;
	uint16_t V_2 = 0x0;
	{
		uint16_t L_0 = Tokenizer_Peek_m4053934172(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		uint16_t L_1 = V_0;
		V_2 = L_1;
		uint16_t L_2 = V_2;
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)33))) == 0)
		{
			goto IL_0260;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)33))) == 1)
		{
			goto IL_02f0;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)33))) == 2)
		{
			goto IL_0092;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)33))) == 3)
		{
			goto IL_0224;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)33))) == 4)
		{
			goto IL_0092;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)33))) == 5)
		{
			goto IL_0092;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)33))) == 6)
		{
			goto IL_02e9;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)33))) == 7)
		{
			goto IL_01ab;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)33))) == 8)
		{
			goto IL_01bf;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)33))) == 9)
		{
			goto IL_01f4;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)33))) == 10)
		{
			goto IL_01cc;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)33))) == 11)
		{
			goto IL_0162;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)33))) == 12)
		{
			goto IL_01e0;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)33))) == 13)
		{
			goto IL_00ef;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)33))) == 14)
		{
			goto IL_00c1;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)33))) == 15)
		{
			goto IL_0092;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)33))) == 16)
		{
			goto IL_0092;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)33))) == 17)
		{
			goto IL_0092;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)33))) == 18)
		{
			goto IL_0092;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)33))) == 19)
		{
			goto IL_0092;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)33))) == 20)
		{
			goto IL_0092;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)33))) == 21)
		{
			goto IL_0092;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)33))) == 22)
		{
			goto IL_0092;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)33))) == 23)
		{
			goto IL_0092;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)33))) == 24)
		{
			goto IL_0092;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)33))) == 25)
		{
			goto IL_0134;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)33))) == 26)
		{
			goto IL_0092;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)33))) == 27)
		{
			goto IL_02bb;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)33))) == 28)
		{
			goto IL_024c;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)33))) == 29)
		{
			goto IL_028d;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)33))) == 30)
		{
			goto IL_0092;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)33))) == 31)
		{
			goto IL_0176;
		}
	}

IL_0092:
	{
		uint16_t L_3 = V_2;
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)91))) == 0)
		{
			goto IL_018a;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)91))) == 1)
		{
			goto IL_00a7;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)91))) == 2)
		{
			goto IL_019e;
		}
	}

IL_00a7:
	{
		uint16_t L_4 = V_2;
		if ((((int32_t)L_4) == ((int32_t)0)))
		{
			goto IL_00bb;
		}
	}
	{
		uint16_t L_5 = V_2;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)124))))
		{
			goto IL_0238;
		}
	}
	{
		goto IL_02f7;
	}

IL_00bb:
	{
		return ((int32_t)258);
	}

IL_00c1:
	{
		__this->set_m_fThisIsOperator_7((bool)1);
		Tokenizer_GetChar_m3293081869(__this, /*hidden argument*/NULL);
		uint16_t L_6 = Tokenizer_Peek_m4053934172(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)((int32_t)47)))))
		{
			goto IL_00e9;
		}
	}
	{
		Tokenizer_GetChar_m3293081869(__this, /*hidden argument*/NULL);
		return ((int32_t)260);
	}

IL_00e9:
	{
		return ((int32_t)259);
	}

IL_00ef:
	{
		Tokenizer_GetChar_m3293081869(__this, /*hidden argument*/NULL);
		uint16_t L_7 = Tokenizer_Peek_m4053934172(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_7) == ((uint32_t)((int32_t)46)))))
		{
			goto IL_0110;
		}
	}
	{
		Tokenizer_GetChar_m3293081869(__this, /*hidden argument*/NULL);
		return ((int32_t)263);
	}

IL_0110:
	{
		uint16_t L_8 = Tokenizer_Peek_m4053934172(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Tokenizer_t3551440624_il2cpp_TypeInfo_var);
		bool L_9 = Tokenizer_IsDigit_m448102595(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_012e;
		}
	}
	{
		Tokenizer_PutBack_m1991014231(__this, /*hidden argument*/NULL);
		int32_t L_10 = Tokenizer_ParseNumber_m2376763193(__this, /*hidden argument*/NULL);
		return L_10;
	}

IL_012e:
	{
		return ((int32_t)262);
	}

IL_0134:
	{
		Tokenizer_GetChar_m3293081869(__this, /*hidden argument*/NULL);
		uint16_t L_11 = Tokenizer_Peek_m4053934172(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_11) == ((uint32_t)((int32_t)58)))))
		{
			goto IL_015c;
		}
	}
	{
		__this->set_m_fThisIsOperator_7((bool)1);
		Tokenizer_GetChar_m3293081869(__this, /*hidden argument*/NULL);
		return ((int32_t)265);
	}

IL_015c:
	{
		return ((int32_t)257);
	}

IL_0162:
	{
		__this->set_m_fThisIsOperator_7((bool)1);
		Tokenizer_GetChar_m3293081869(__this, /*hidden argument*/NULL);
		return ((int32_t)267);
	}

IL_0176:
	{
		__this->set_m_fThisIsOperator_7((bool)1);
		Tokenizer_GetChar_m3293081869(__this, /*hidden argument*/NULL);
		return ((int32_t)268);
	}

IL_018a:
	{
		__this->set_m_fThisIsOperator_7((bool)1);
		Tokenizer_GetChar_m3293081869(__this, /*hidden argument*/NULL);
		return ((int32_t)270);
	}

IL_019e:
	{
		Tokenizer_GetChar_m3293081869(__this, /*hidden argument*/NULL);
		return ((int32_t)271);
	}

IL_01ab:
	{
		__this->set_m_fThisIsOperator_7((bool)1);
		Tokenizer_GetChar_m3293081869(__this, /*hidden argument*/NULL);
		return ((int32_t)272);
	}

IL_01bf:
	{
		Tokenizer_GetChar_m3293081869(__this, /*hidden argument*/NULL);
		return ((int32_t)273);
	}

IL_01cc:
	{
		__this->set_m_fThisIsOperator_7((bool)1);
		Tokenizer_GetChar_m3293081869(__this, /*hidden argument*/NULL);
		return ((int32_t)282);
	}

IL_01e0:
	{
		__this->set_m_fThisIsOperator_7((bool)1);
		Tokenizer_GetChar_m3293081869(__this, /*hidden argument*/NULL);
		return ((int32_t)283);
	}

IL_01f4:
	{
		Tokenizer_GetChar_m3293081869(__this, /*hidden argument*/NULL);
		bool L_12 = Tokenizer_get_IsFirstToken_m1142525831(__this, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_021e;
		}
	}
	{
		bool L_13 = __this->get_m_fPrevWasOperator_6();
		if (L_13)
		{
			goto IL_021e;
		}
	}
	{
		__this->set_m_fThisIsOperator_7((bool)1);
		return ((int32_t)330);
	}

IL_021e:
	{
		return ((int32_t)284);
	}

IL_0224:
	{
		Tokenizer_GetChar_m3293081869(__this, /*hidden argument*/NULL);
		__this->set_m_fThisIsOperator_7((bool)1);
		return ((int32_t)285);
	}

IL_0238:
	{
		__this->set_m_fThisIsOperator_7((bool)1);
		Tokenizer_GetChar_m3293081869(__this, /*hidden argument*/NULL);
		return ((int32_t)286);
	}

IL_024c:
	{
		__this->set_m_fThisIsOperator_7((bool)1);
		Tokenizer_GetChar_m3293081869(__this, /*hidden argument*/NULL);
		return ((int32_t)287);
	}

IL_0260:
	{
		Tokenizer_GetChar_m3293081869(__this, /*hidden argument*/NULL);
		uint16_t L_14 = Tokenizer_Peek_m4053934172(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_14) == ((uint32_t)((int32_t)61)))))
		{
			goto IL_0288;
		}
	}
	{
		__this->set_m_fThisIsOperator_7((bool)1);
		Tokenizer_GetChar_m3293081869(__this, /*hidden argument*/NULL);
		return ((int32_t)288);
	}

IL_0288:
	{
		goto IL_033d;
	}

IL_028d:
	{
		__this->set_m_fThisIsOperator_7((bool)1);
		Tokenizer_GetChar_m3293081869(__this, /*hidden argument*/NULL);
		uint16_t L_15 = Tokenizer_Peek_m4053934172(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_15) == ((uint32_t)((int32_t)61)))))
		{
			goto IL_02b5;
		}
	}
	{
		Tokenizer_GetChar_m3293081869(__this, /*hidden argument*/NULL);
		return ((int32_t)292);
	}

IL_02b5:
	{
		return ((int32_t)295);
	}

IL_02bb:
	{
		__this->set_m_fThisIsOperator_7((bool)1);
		Tokenizer_GetChar_m3293081869(__this, /*hidden argument*/NULL);
		uint16_t L_16 = Tokenizer_Peek_m4053934172(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_16) == ((uint32_t)((int32_t)61)))))
		{
			goto IL_02e3;
		}
	}
	{
		Tokenizer_GetChar_m3293081869(__this, /*hidden argument*/NULL);
		return ((int32_t)290);
	}

IL_02e3:
	{
		return ((int32_t)294);
	}

IL_02e9:
	{
		int32_t L_17 = Tokenizer_ParseLiteral_m2561279649(__this, /*hidden argument*/NULL);
		return L_17;
	}

IL_02f0:
	{
		int32_t L_18 = Tokenizer_ParseLiteral_m2561279649(__this, /*hidden argument*/NULL);
		return L_18;
	}

IL_02f7:
	{
		uint16_t L_19 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Tokenizer_t3551440624_il2cpp_TypeInfo_var);
		bool L_20 = Tokenizer_IsDigit_m448102595(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_0309;
		}
	}
	{
		int32_t L_21 = Tokenizer_ParseNumber_m2376763193(__this, /*hidden argument*/NULL);
		return L_21;
	}

IL_0309:
	{
		uint16_t L_22 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Char_t2778706699_il2cpp_TypeInfo_var);
		bool L_23 = Char_IsLetter_m1699036490(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		if (L_23)
		{
			goto IL_031c;
		}
	}
	{
		uint16_t L_24 = V_0;
		if ((!(((uint32_t)L_24) == ((uint32_t)((int32_t)95)))))
		{
			goto IL_0338;
		}
	}

IL_031c:
	{
		int32_t L_25 = Tokenizer_ParseIdentifier_m151998745(__this, /*hidden argument*/NULL);
		V_1 = L_25;
		int32_t L_26 = V_1;
		bool L_27 = Tokenizer_IsOperatorName_m3788670325(__this, L_26, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_0336;
		}
	}
	{
		__this->set_m_fThisIsOperator_7((bool)1);
	}

IL_0336:
	{
		int32_t L_28 = V_1;
		return L_28;
	}

IL_0338:
	{
		goto IL_033d;
	}

IL_033d:
	{
		uint16_t L_29 = V_0;
		uint16_t L_30 = L_29;
		Il2CppObject * L_31 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_30);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m2809334143(NULL /*static, unused*/, _stringLiteral2704140593, L_31, _stringLiteral39, /*hidden argument*/NULL);
		XPathException_t2353341743 * L_33 = (XPathException_t2353341743 *)il2cpp_codegen_object_new(XPathException_t2353341743_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_33, L_32, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_33);
	}
}
// System.Boolean Mono.Xml.XPath.Tokenizer::advance()
extern "C"  bool Tokenizer_advance_m3323820185 (Tokenizer_t3551440624 * __this, const MethodInfo* method)
{
	{
		__this->set_m_fThisIsOperator_7((bool)0);
		__this->set_m_objToken_5(NULL);
		int32_t L_0 = Tokenizer_ParseToken_m1903029675(__this, /*hidden argument*/NULL);
		__this->set_m_iToken_3(L_0);
		Tokenizer_SkipWhitespace_m3223252775(__this, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_m_iToken_3();
		__this->set_m_iTokenPrev_4(L_1);
		bool L_2 = __this->get_m_fThisIsOperator_7();
		__this->set_m_fPrevWasOperator_6(L_2);
		int32_t L_3 = __this->get_m_iToken_3();
		return (bool)((((int32_t)((((int32_t)L_3) == ((int32_t)((int32_t)258)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 Mono.Xml.XPath.Tokenizer::token()
extern "C"  int32_t Tokenizer_token_m1034812214 (Tokenizer_t3551440624 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_iToken_3();
		return L_0;
	}
}
// System.Object Mono.Xml.XPath.Tokenizer::value()
extern "C"  Il2CppObject * Tokenizer_value_m3552967611 (Tokenizer_t3551440624 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_m_objToken_5();
		return L_0;
	}
}
// System.Boolean Mono.Xml.XPath.Tokenizer::get_IsFirstToken()
extern "C"  bool Tokenizer_get_IsFirstToken_m1142525831 (Tokenizer_t3551440624 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_iTokenPrev_4();
		return (bool)((((int32_t)L_0) == ((int32_t)((int32_t)258)))? 1 : 0);
	}
}
// System.Boolean Mono.Xml.XPath.Tokenizer::IsNodeType(System.Int32)
extern "C"  bool Tokenizer_IsNodeType_m3472117698 (Tokenizer_t3551440624 * __this, int32_t ___iToken0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___iToken0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)322))) == 0)
		{
			goto IL_002f;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)322))) == 1)
		{
			goto IL_0031;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)322))) == 2)
		{
			goto IL_002f;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)322))) == 3)
		{
			goto IL_0031;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)322))) == 4)
		{
			goto IL_002f;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)322))) == 5)
		{
			goto IL_0031;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)322))) == 6)
		{
			goto IL_002f;
		}
	}
	{
		goto IL_0031;
	}

IL_002f:
	{
		return (bool)1;
	}

IL_0031:
	{
		return (bool)0;
	}
}
// System.Boolean Mono.Xml.XPath.Tokenizer::IsOperatorName(System.Int32)
extern "C"  bool Tokenizer_IsOperatorName_m3788670325 (Tokenizer_t3551440624 * __this, int32_t ___iToken0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___iToken0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)274))) == 0)
		{
			goto IL_002f;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)274))) == 1)
		{
			goto IL_0031;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)274))) == 2)
		{
			goto IL_002f;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)274))) == 3)
		{
			goto IL_0031;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)274))) == 4)
		{
			goto IL_002f;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)274))) == 5)
		{
			goto IL_0031;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)274))) == 6)
		{
			goto IL_002f;
		}
	}
	{
		goto IL_0031;
	}

IL_002f:
	{
		return (bool)1;
	}

IL_0031:
	{
		return (bool)0;
	}
}
// System.Boolean Mono.Xml.XPath.Tokenizer::IsAxisName(System.Int32)
extern "C"  bool Tokenizer_IsAxisName_m1626818866 (Tokenizer_t3551440624 * __this, int32_t ___iToken0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___iToken0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)296))) == 0)
		{
			goto IL_0077;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)296))) == 1)
		{
			goto IL_0079;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)296))) == 2)
		{
			goto IL_0077;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)296))) == 3)
		{
			goto IL_0079;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)296))) == 4)
		{
			goto IL_0077;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)296))) == 5)
		{
			goto IL_0079;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)296))) == 6)
		{
			goto IL_0077;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)296))) == 7)
		{
			goto IL_0079;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)296))) == 8)
		{
			goto IL_0077;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)296))) == 9)
		{
			goto IL_0079;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)296))) == 10)
		{
			goto IL_0077;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)296))) == 11)
		{
			goto IL_0079;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)296))) == 12)
		{
			goto IL_0077;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)296))) == 13)
		{
			goto IL_0079;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)296))) == 14)
		{
			goto IL_0077;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)296))) == 15)
		{
			goto IL_0079;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)296))) == 16)
		{
			goto IL_0077;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)296))) == 17)
		{
			goto IL_0079;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)296))) == 18)
		{
			goto IL_0077;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)296))) == 19)
		{
			goto IL_0079;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)296))) == 20)
		{
			goto IL_0077;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)296))) == 21)
		{
			goto IL_0079;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)296))) == 22)
		{
			goto IL_0077;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)296))) == 23)
		{
			goto IL_0079;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)296))) == 24)
		{
			goto IL_0077;
		}
	}
	{
		goto IL_0079;
	}

IL_0077:
	{
		return (bool)1;
	}

IL_0079:
	{
		return (bool)0;
	}
}
// System.Void Mono.Xml.XPath.XmlDocumentEditableNavigator::.ctor(Mono.Xml.XPath.XPathEditableDocument)
extern Il2CppClass* XPathNavigator_t1624538935_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlDocumentEditableNavigator_t1066973242_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlDocumentNavigator_t569158157_il2cpp_TypeInfo_var;
extern const uint32_t XmlDocumentEditableNavigator__ctor_m2729288836_MetadataUsageId;
extern "C"  void XmlDocumentEditableNavigator__ctor_m2729288836 (XmlDocumentEditableNavigator_t1066973242 * __this, XPathEditableDocument_t469941845 * ___doc0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlDocumentEditableNavigator__ctor_m2729288836_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XPathNavigator_t1624538935_il2cpp_TypeInfo_var);
		XPathNavigator__ctor_m2215010048(__this, /*hidden argument*/NULL);
		XPathEditableDocument_t469941845 * L_0 = ___doc0;
		__this->set_document_3(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(XmlDocumentEditableNavigator_t1066973242_il2cpp_TypeInfo_var);
		bool L_1 = ((XmlDocumentEditableNavigator_t1066973242_StaticFields*)XmlDocumentEditableNavigator_t1066973242_il2cpp_TypeInfo_var->static_fields)->get_isXmlDocumentNavigatorImpl_2();
		if (!L_1)
		{
			goto IL_002d;
		}
	}
	{
		XPathEditableDocument_t469941845 * L_2 = ___doc0;
		NullCheck(L_2);
		XmlNode_t3592213601 * L_3 = XPathEditableDocument_get_Node_m560412385(L_2, /*hidden argument*/NULL);
		XmlDocumentNavigator_t569158157 * L_4 = (XmlDocumentNavigator_t569158157 *)il2cpp_codegen_object_new(XmlDocumentNavigator_t569158157_il2cpp_TypeInfo_var);
		XmlDocumentNavigator__ctor_m3471931459(L_4, L_3, /*hidden argument*/NULL);
		__this->set_navigator_4(L_4);
		goto IL_0039;
	}

IL_002d:
	{
		XPathEditableDocument_t469941845 * L_5 = ___doc0;
		NullCheck(L_5);
		XPathNavigator_t1624538935 * L_6 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(4 /* System.Xml.XPath.XPathNavigator Mono.Xml.XPath.XPathEditableDocument::CreateNavigator() */, L_5);
		__this->set_navigator_4(L_6);
	}

IL_0039:
	{
		return;
	}
}
// System.Void Mono.Xml.XPath.XmlDocumentEditableNavigator::.ctor(Mono.Xml.XPath.XmlDocumentEditableNavigator)
extern Il2CppClass* XPathNavigator_t1624538935_il2cpp_TypeInfo_var;
extern const uint32_t XmlDocumentEditableNavigator__ctor_m94457073_MetadataUsageId;
extern "C"  void XmlDocumentEditableNavigator__ctor_m94457073 (XmlDocumentEditableNavigator_t1066973242 * __this, XmlDocumentEditableNavigator_t1066973242 * ___nav0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlDocumentEditableNavigator__ctor_m94457073_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XPathNavigator_t1624538935_il2cpp_TypeInfo_var);
		XPathNavigator__ctor_m2215010048(__this, /*hidden argument*/NULL);
		XmlDocumentEditableNavigator_t1066973242 * L_0 = ___nav0;
		NullCheck(L_0);
		XPathEditableDocument_t469941845 * L_1 = L_0->get_document_3();
		__this->set_document_3(L_1);
		XmlDocumentEditableNavigator_t1066973242 * L_2 = ___nav0;
		NullCheck(L_2);
		XPathNavigator_t1624538935 * L_3 = L_2->get_navigator_4();
		NullCheck(L_3);
		XPathNavigator_t1624538935 * L_4 = VirtFuncInvoker0< XPathNavigator_t1624538935 * >::Invoke(14 /* System.Xml.XPath.XPathNavigator System.Xml.XPath.XPathNavigator::Clone() */, L_3);
		__this->set_navigator_4(L_4);
		return;
	}
}
// System.Void Mono.Xml.XPath.XmlDocumentEditableNavigator::.cctor()
extern const Il2CppType* XmlDocumentEditableNavigator_t1066973242_0_0_0_var;
extern const Il2CppType* XmlDocument_t3705263098_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlDocumentEditableNavigator_t1066973242_il2cpp_TypeInfo_var;
extern const uint32_t XmlDocumentEditableNavigator__cctor_m2314429568_MetadataUsageId;
extern "C"  void XmlDocumentEditableNavigator__cctor_m2314429568 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlDocumentEditableNavigator__cctor_m2314429568_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(XmlDocumentEditableNavigator_t1066973242_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		Assembly_t1882292308 * L_1 = VirtFuncInvoker0< Assembly_t1882292308 * >::Invoke(15 /* System.Reflection.Assembly System.Type::get_Assembly() */, L_0);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(XmlDocument_t3705263098_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_2);
		Assembly_t1882292308 * L_3 = VirtFuncInvoker0< Assembly_t1882292308 * >::Invoke(15 /* System.Reflection.Assembly System.Type::get_Assembly() */, L_2);
		((XmlDocumentEditableNavigator_t1066973242_StaticFields*)XmlDocumentEditableNavigator_t1066973242_il2cpp_TypeInfo_var->static_fields)->set_isXmlDocumentNavigatorImpl_2((bool)((((Il2CppObject*)(Assembly_t1882292308 *)L_1) == ((Il2CppObject*)(Assembly_t1882292308 *)L_3))? 1 : 0));
		return;
	}
}
// System.String Mono.Xml.XPath.XmlDocumentEditableNavigator::get_LocalName()
extern "C"  String_t* XmlDocumentEditableNavigator_get_LocalName_m2552684987 (XmlDocumentEditableNavigator_t1066973242 * __this, const MethodInfo* method)
{
	{
		XPathNavigator_t1624538935 * L_0 = __this->get_navigator_4();
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.Xml.XPath.XPathNavigator::get_LocalName() */, L_0);
		return L_1;
	}
}
// System.String Mono.Xml.XPath.XmlDocumentEditableNavigator::get_Name()
extern "C"  String_t* XmlDocumentEditableNavigator_get_Name_m1443563688 (XmlDocumentEditableNavigator_t1066973242 * __this, const MethodInfo* method)
{
	{
		XPathNavigator_t1624538935 * L_0 = __this->get_navigator_4();
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(10 /* System.String System.Xml.XPath.XPathNavigator::get_Name() */, L_0);
		return L_1;
	}
}
// System.String Mono.Xml.XPath.XmlDocumentEditableNavigator::get_NamespaceURI()
extern "C"  String_t* XmlDocumentEditableNavigator_get_NamespaceURI_m3195230894 (XmlDocumentEditableNavigator_t1066973242 * __this, const MethodInfo* method)
{
	{
		XPathNavigator_t1624538935 * L_0 = __this->get_navigator_4();
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(11 /* System.String System.Xml.XPath.XPathNavigator::get_NamespaceURI() */, L_0);
		return L_1;
	}
}
// System.Xml.XPath.XPathNodeType Mono.Xml.XPath.XmlDocumentEditableNavigator::get_NodeType()
extern "C"  int32_t XmlDocumentEditableNavigator_get_NodeType_m2717197373 (XmlDocumentEditableNavigator_t1066973242 * __this, const MethodInfo* method)
{
	{
		XPathNavigator_t1624538935 * L_0 = __this->get_navigator_4();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Xml.XPath.XPathNodeType System.Xml.XPath.XPathNavigator::get_NodeType() */, L_0);
		return L_1;
	}
}
// System.String Mono.Xml.XPath.XmlDocumentEditableNavigator::get_Value()
extern "C"  String_t* XmlDocumentEditableNavigator_get_Value_m310507990 (XmlDocumentEditableNavigator_t1066973242 * __this, const MethodInfo* method)
{
	{
		XPathNavigator_t1624538935 * L_0 = __this->get_navigator_4();
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String System.Xml.XPath.XPathItem::get_Value() */, L_0);
		return L_1;
	}
}
// System.String Mono.Xml.XPath.XmlDocumentEditableNavigator::get_XmlLang()
extern "C"  String_t* XmlDocumentEditableNavigator_get_XmlLang_m989037834 (XmlDocumentEditableNavigator_t1066973242 * __this, const MethodInfo* method)
{
	{
		XPathNavigator_t1624538935 * L_0 = __this->get_navigator_4();
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(13 /* System.String System.Xml.XPath.XPathNavigator::get_XmlLang() */, L_0);
		return L_1;
	}
}
// System.Boolean Mono.Xml.XPath.XmlDocumentEditableNavigator::get_HasChildren()
extern "C"  bool XmlDocumentEditableNavigator_get_HasChildren_m2696128707 (XmlDocumentEditableNavigator_t1066973242 * __this, const MethodInfo* method)
{
	{
		XPathNavigator_t1624538935 * L_0 = __this->get_navigator_4();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean System.Xml.XPath.XPathNavigator::get_HasChildren() */, L_0);
		return L_1;
	}
}
// System.Boolean Mono.Xml.XPath.XmlDocumentEditableNavigator::get_HasAttributes()
extern "C"  bool XmlDocumentEditableNavigator_get_HasAttributes_m4215613787 (XmlDocumentEditableNavigator_t1066973242 * __this, const MethodInfo* method)
{
	{
		XPathNavigator_t1624538935 * L_0 = __this->get_navigator_4();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Xml.XPath.XPathNavigator::get_HasAttributes() */, L_0);
		return L_1;
	}
}
// System.Xml.XPath.XPathNavigator Mono.Xml.XPath.XmlDocumentEditableNavigator::Clone()
extern Il2CppClass* XmlDocumentEditableNavigator_t1066973242_il2cpp_TypeInfo_var;
extern const uint32_t XmlDocumentEditableNavigator_Clone_m1441948730_MetadataUsageId;
extern "C"  XPathNavigator_t1624538935 * XmlDocumentEditableNavigator_Clone_m1441948730 (XmlDocumentEditableNavigator_t1066973242 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlDocumentEditableNavigator_Clone_m1441948730_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		XmlDocumentEditableNavigator_t1066973242 * L_0 = (XmlDocumentEditableNavigator_t1066973242 *)il2cpp_codegen_object_new(XmlDocumentEditableNavigator_t1066973242_il2cpp_TypeInfo_var);
		XmlDocumentEditableNavigator__ctor_m94457073(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Xml.XmlNode Mono.Xml.XPath.XmlDocumentEditableNavigator::GetNode()
extern Il2CppClass* IHasXmlNode_t3624981936_il2cpp_TypeInfo_var;
extern const uint32_t XmlDocumentEditableNavigator_GetNode_m964706321_MetadataUsageId;
extern "C"  XmlNode_t3592213601 * XmlDocumentEditableNavigator_GetNode_m964706321 (XmlDocumentEditableNavigator_t1066973242 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlDocumentEditableNavigator_GetNode_m964706321_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		XPathNavigator_t1624538935 * L_0 = __this->get_navigator_4();
		NullCheck(((Il2CppObject *)Castclass(L_0, IHasXmlNode_t3624981936_il2cpp_TypeInfo_var)));
		XmlNode_t3592213601 * L_1 = InterfaceFuncInvoker0< XmlNode_t3592213601 * >::Invoke(0 /* System.Xml.XmlNode System.Xml.IHasXmlNode::GetNode() */, IHasXmlNode_t3624981936_il2cpp_TypeInfo_var, ((Il2CppObject *)Castclass(L_0, IHasXmlNode_t3624981936_il2cpp_TypeInfo_var)));
		return L_1;
	}
}
// System.Boolean Mono.Xml.XPath.XmlDocumentEditableNavigator::IsSamePosition(System.Xml.XPath.XPathNavigator)
extern Il2CppClass* XmlDocumentEditableNavigator_t1066973242_il2cpp_TypeInfo_var;
extern const uint32_t XmlDocumentEditableNavigator_IsSamePosition_m2054489737_MetadataUsageId;
extern "C"  bool XmlDocumentEditableNavigator_IsSamePosition_m2054489737 (XmlDocumentEditableNavigator_t1066973242 * __this, XPathNavigator_t1624538935 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlDocumentEditableNavigator_IsSamePosition_m2054489737_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XmlDocumentEditableNavigator_t1066973242 * V_0 = NULL;
	{
		XPathNavigator_t1624538935 * L_0 = ___other0;
		V_0 = ((XmlDocumentEditableNavigator_t1066973242 *)IsInstClass(L_0, XmlDocumentEditableNavigator_t1066973242_il2cpp_TypeInfo_var));
		XmlDocumentEditableNavigator_t1066973242 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		XPathNavigator_t1624538935 * L_2 = __this->get_navigator_4();
		XmlDocumentEditableNavigator_t1066973242 * L_3 = V_0;
		NullCheck(L_3);
		XPathNavigator_t1624538935 * L_4 = L_3->get_navigator_4();
		NullCheck(L_2);
		bool L_5 = VirtFuncInvoker1< bool, XPathNavigator_t1624538935 * >::Invoke(18 /* System.Boolean System.Xml.XPath.XPathNavigator::IsSamePosition(System.Xml.XPath.XPathNavigator) */, L_2, L_4);
		return L_5;
	}

IL_001f:
	{
		XPathNavigator_t1624538935 * L_6 = __this->get_navigator_4();
		XmlDocumentEditableNavigator_t1066973242 * L_7 = V_0;
		NullCheck(L_6);
		bool L_8 = VirtFuncInvoker1< bool, XPathNavigator_t1624538935 * >::Invoke(18 /* System.Boolean System.Xml.XPath.XPathNavigator::IsSamePosition(System.Xml.XPath.XPathNavigator) */, L_6, L_7);
		return L_8;
	}
}
// System.Boolean Mono.Xml.XPath.XmlDocumentEditableNavigator::MoveTo(System.Xml.XPath.XPathNavigator)
extern Il2CppClass* XmlDocumentEditableNavigator_t1066973242_il2cpp_TypeInfo_var;
extern const uint32_t XmlDocumentEditableNavigator_MoveTo_m4020108982_MetadataUsageId;
extern "C"  bool XmlDocumentEditableNavigator_MoveTo_m4020108982 (XmlDocumentEditableNavigator_t1066973242 * __this, XPathNavigator_t1624538935 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlDocumentEditableNavigator_MoveTo_m4020108982_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XmlDocumentEditableNavigator_t1066973242 * V_0 = NULL;
	{
		XPathNavigator_t1624538935 * L_0 = ___other0;
		V_0 = ((XmlDocumentEditableNavigator_t1066973242 *)IsInstClass(L_0, XmlDocumentEditableNavigator_t1066973242_il2cpp_TypeInfo_var));
		XmlDocumentEditableNavigator_t1066973242 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		XPathNavigator_t1624538935 * L_2 = __this->get_navigator_4();
		XmlDocumentEditableNavigator_t1066973242 * L_3 = V_0;
		NullCheck(L_3);
		XPathNavigator_t1624538935 * L_4 = L_3->get_navigator_4();
		NullCheck(L_2);
		bool L_5 = VirtFuncInvoker1< bool, XPathNavigator_t1624538935 * >::Invoke(19 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveTo(System.Xml.XPath.XPathNavigator) */, L_2, L_4);
		return L_5;
	}

IL_001f:
	{
		XPathNavigator_t1624538935 * L_6 = __this->get_navigator_4();
		XmlDocumentEditableNavigator_t1066973242 * L_7 = V_0;
		NullCheck(L_6);
		bool L_8 = VirtFuncInvoker1< bool, XPathNavigator_t1624538935 * >::Invoke(19 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveTo(System.Xml.XPath.XPathNavigator) */, L_6, L_7);
		return L_8;
	}
}
// System.Boolean Mono.Xml.XPath.XmlDocumentEditableNavigator::MoveToFirstAttribute()
extern "C"  bool XmlDocumentEditableNavigator_MoveToFirstAttribute_m2891360487 (XmlDocumentEditableNavigator_t1066973242 * __this, const MethodInfo* method)
{
	{
		XPathNavigator_t1624538935 * L_0 = __this->get_navigator_4();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(24 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirstAttribute() */, L_0);
		return L_1;
	}
}
// System.Boolean Mono.Xml.XPath.XmlDocumentEditableNavigator::MoveToFirstChild()
extern "C"  bool XmlDocumentEditableNavigator_MoveToFirstChild_m2536638567 (XmlDocumentEditableNavigator_t1066973242 * __this, const MethodInfo* method)
{
	{
		XPathNavigator_t1624538935 * L_0 = __this->get_navigator_4();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(25 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirstChild() */, L_0);
		return L_1;
	}
}
// System.Boolean Mono.Xml.XPath.XmlDocumentEditableNavigator::MoveToFirstNamespace(System.Xml.XPath.XPathNamespaceScope)
extern "C"  bool XmlDocumentEditableNavigator_MoveToFirstNamespace_m296870135 (XmlDocumentEditableNavigator_t1066973242 * __this, int32_t ___scope0, const MethodInfo* method)
{
	{
		XPathNavigator_t1624538935 * L_0 = __this->get_navigator_4();
		int32_t L_1 = ___scope0;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, int32_t >::Invoke(26 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirstNamespace(System.Xml.XPath.XPathNamespaceScope) */, L_0, L_1);
		return L_2;
	}
}
// System.Boolean Mono.Xml.XPath.XmlDocumentEditableNavigator::MoveToId(System.String)
extern "C"  bool XmlDocumentEditableNavigator_MoveToId_m451508044 (XmlDocumentEditableNavigator_t1066973242 * __this, String_t* ___id0, const MethodInfo* method)
{
	{
		XPathNavigator_t1624538935 * L_0 = __this->get_navigator_4();
		String_t* L_1 = ___id0;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, String_t* >::Invoke(27 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToId(System.String) */, L_0, L_1);
		return L_2;
	}
}
// System.Boolean Mono.Xml.XPath.XmlDocumentEditableNavigator::MoveToNext()
extern "C"  bool XmlDocumentEditableNavigator_MoveToNext_m4016586958 (XmlDocumentEditableNavigator_t1066973242 * __this, const MethodInfo* method)
{
	{
		XPathNavigator_t1624538935 * L_0 = __this->get_navigator_4();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(28 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToNext() */, L_0);
		return L_1;
	}
}
// System.Boolean Mono.Xml.XPath.XmlDocumentEditableNavigator::MoveToNextAttribute()
extern "C"  bool XmlDocumentEditableNavigator_MoveToNextAttribute_m3261782864 (XmlDocumentEditableNavigator_t1066973242 * __this, const MethodInfo* method)
{
	{
		XPathNavigator_t1624538935 * L_0 = __this->get_navigator_4();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(29 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToNextAttribute() */, L_0);
		return L_1;
	}
}
// System.Boolean Mono.Xml.XPath.XmlDocumentEditableNavigator::MoveToNextNamespace(System.Xml.XPath.XPathNamespaceScope)
extern "C"  bool XmlDocumentEditableNavigator_MoveToNextNamespace_m2229981920 (XmlDocumentEditableNavigator_t1066973242 * __this, int32_t ___scope0, const MethodInfo* method)
{
	{
		XPathNavigator_t1624538935 * L_0 = __this->get_navigator_4();
		int32_t L_1 = ___scope0;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, int32_t >::Invoke(30 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToNextNamespace(System.Xml.XPath.XPathNamespaceScope) */, L_0, L_1);
		return L_2;
	}
}
// System.Boolean Mono.Xml.XPath.XmlDocumentEditableNavigator::MoveToParent()
extern "C"  bool XmlDocumentEditableNavigator_MoveToParent_m2811571781 (XmlDocumentEditableNavigator_t1066973242 * __this, const MethodInfo* method)
{
	{
		XPathNavigator_t1624538935 * L_0 = __this->get_navigator_4();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToParent() */, L_0);
		return L_1;
	}
}
// System.Void Mono.Xml.XPath.XmlDocumentEditableNavigator::MoveToRoot()
extern "C"  void XmlDocumentEditableNavigator_MoveToRoot_m4087425029 (XmlDocumentEditableNavigator_t1066973242 * __this, const MethodInfo* method)
{
	{
		XPathNavigator_t1624538935 * L_0 = __this->get_navigator_4();
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Xml.XPath.XPathNavigator::MoveToRoot() */, L_0);
		return;
	}
}
// System.Boolean Mono.Xml.XPath.XmlDocumentEditableNavigator::MoveToNamespace(System.String)
extern "C"  bool XmlDocumentEditableNavigator_MoveToNamespace_m4006976512 (XmlDocumentEditableNavigator_t1066973242 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		XPathNavigator_t1624538935 * L_0 = __this->get_navigator_4();
		String_t* L_1 = ___name0;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, String_t* >::Invoke(21 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToNamespace(System.String) */, L_0, L_1);
		return L_2;
	}
}
// System.Boolean Mono.Xml.XPath.XmlDocumentEditableNavigator::MoveToFirst()
extern "C"  bool XmlDocumentEditableNavigator_MoveToFirst_m1559067991 (XmlDocumentEditableNavigator_t1066973242 * __this, const MethodInfo* method)
{
	{
		XPathNavigator_t1624538935 * L_0 = __this->get_navigator_4();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(22 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToFirst() */, L_0);
		return L_1;
	}
}
// System.Boolean Mono.Xml.XPath.XmlDocumentEditableNavigator::MoveToAttribute(System.String,System.String)
extern "C"  bool XmlDocumentEditableNavigator_MoveToAttribute_m1216160411 (XmlDocumentEditableNavigator_t1066973242 * __this, String_t* ___localName0, String_t* ___namespaceURI1, const MethodInfo* method)
{
	{
		XPathNavigator_t1624538935 * L_0 = __this->get_navigator_4();
		String_t* L_1 = ___localName0;
		String_t* L_2 = ___namespaceURI1;
		NullCheck(L_0);
		bool L_3 = VirtFuncInvoker2< bool, String_t*, String_t* >::Invoke(20 /* System.Boolean System.Xml.XPath.XPathNavigator::MoveToAttribute(System.String,System.String) */, L_0, L_1, L_2);
		return L_3;
	}
}
// System.Boolean Mono.Xml.XPath.XmlDocumentEditableNavigator::IsDescendant(System.Xml.XPath.XPathNavigator)
extern Il2CppClass* XmlDocumentEditableNavigator_t1066973242_il2cpp_TypeInfo_var;
extern const uint32_t XmlDocumentEditableNavigator_IsDescendant_m1298640123_MetadataUsageId;
extern "C"  bool XmlDocumentEditableNavigator_IsDescendant_m1298640123 (XmlDocumentEditableNavigator_t1066973242 * __this, XPathNavigator_t1624538935 * ___nav0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlDocumentEditableNavigator_IsDescendant_m1298640123_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XmlDocumentEditableNavigator_t1066973242 * V_0 = NULL;
	{
		XPathNavigator_t1624538935 * L_0 = ___nav0;
		V_0 = ((XmlDocumentEditableNavigator_t1066973242 *)IsInstClass(L_0, XmlDocumentEditableNavigator_t1066973242_il2cpp_TypeInfo_var));
		XmlDocumentEditableNavigator_t1066973242 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		XPathNavigator_t1624538935 * L_2 = __this->get_navigator_4();
		XmlDocumentEditableNavigator_t1066973242 * L_3 = V_0;
		NullCheck(L_3);
		XPathNavigator_t1624538935 * L_4 = L_3->get_navigator_4();
		NullCheck(L_2);
		bool L_5 = VirtFuncInvoker1< bool, XPathNavigator_t1624538935 * >::Invoke(17 /* System.Boolean System.Xml.XPath.XPathNavigator::IsDescendant(System.Xml.XPath.XPathNavigator) */, L_2, L_4);
		return L_5;
	}

IL_001f:
	{
		XPathNavigator_t1624538935 * L_6 = __this->get_navigator_4();
		XPathNavigator_t1624538935 * L_7 = ___nav0;
		NullCheck(L_6);
		bool L_8 = VirtFuncInvoker1< bool, XPathNavigator_t1624538935 * >::Invoke(17 /* System.Boolean System.Xml.XPath.XPathNavigator::IsDescendant(System.Xml.XPath.XPathNavigator) */, L_6, L_7);
		return L_8;
	}
}
// System.Xml.XmlNodeOrder Mono.Xml.XPath.XmlDocumentEditableNavigator::ComparePosition(System.Xml.XPath.XPathNavigator)
extern Il2CppClass* XmlDocumentEditableNavigator_t1066973242_il2cpp_TypeInfo_var;
extern const uint32_t XmlDocumentEditableNavigator_ComparePosition_m205748080_MetadataUsageId;
extern "C"  int32_t XmlDocumentEditableNavigator_ComparePosition_m205748080 (XmlDocumentEditableNavigator_t1066973242 * __this, XPathNavigator_t1624538935 * ___nav0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlDocumentEditableNavigator_ComparePosition_m205748080_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XmlDocumentEditableNavigator_t1066973242 * V_0 = NULL;
	{
		XPathNavigator_t1624538935 * L_0 = ___nav0;
		V_0 = ((XmlDocumentEditableNavigator_t1066973242 *)IsInstClass(L_0, XmlDocumentEditableNavigator_t1066973242_il2cpp_TypeInfo_var));
		XmlDocumentEditableNavigator_t1066973242 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		XPathNavigator_t1624538935 * L_2 = __this->get_navigator_4();
		XmlDocumentEditableNavigator_t1066973242 * L_3 = V_0;
		NullCheck(L_3);
		XPathNavigator_t1624538935 * L_4 = L_3->get_navigator_4();
		NullCheck(L_2);
		int32_t L_5 = VirtFuncInvoker1< int32_t, XPathNavigator_t1624538935 * >::Invoke(15 /* System.Xml.XmlNodeOrder System.Xml.XPath.XPathNavigator::ComparePosition(System.Xml.XPath.XPathNavigator) */, L_2, L_4);
		return L_5;
	}

IL_001f:
	{
		XPathNavigator_t1624538935 * L_6 = __this->get_navigator_4();
		XPathNavigator_t1624538935 * L_7 = ___nav0;
		NullCheck(L_6);
		int32_t L_8 = VirtFuncInvoker1< int32_t, XPathNavigator_t1624538935 * >::Invoke(15 /* System.Xml.XmlNodeOrder System.Xml.XPath.XPathNavigator::ComparePosition(System.Xml.XPath.XPathNavigator) */, L_6, L_7);
		return L_8;
	}
}
// System.Void Mono.Xml.XPath.XPathEditableDocument::.ctor(System.Xml.XmlNode)
extern "C"  void XPathEditableDocument__ctor_m2910074809 (XPathEditableDocument_t469941845 * __this, XmlNode_t3592213601 * ___node0, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		XmlNode_t3592213601 * L_0 = ___node0;
		__this->set_node_0(L_0);
		return;
	}
}
// System.Xml.XmlNode Mono.Xml.XPath.XPathEditableDocument::get_Node()
extern "C"  XmlNode_t3592213601 * XPathEditableDocument_get_Node_m560412385 (XPathEditableDocument_t469941845 * __this, const MethodInfo* method)
{
	{
		XmlNode_t3592213601 * L_0 = __this->get_node_0();
		return L_0;
	}
}
// System.Xml.XPath.XPathNavigator Mono.Xml.XPath.XPathEditableDocument::CreateNavigator()
extern Il2CppClass* XmlDocumentEditableNavigator_t1066973242_il2cpp_TypeInfo_var;
extern const uint32_t XPathEditableDocument_CreateNavigator_m3544958723_MetadataUsageId;
extern "C"  XPathNavigator_t1624538935 * XPathEditableDocument_CreateNavigator_m3544958723 (XPathEditableDocument_t469941845 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathEditableDocument_CreateNavigator_m3544958723_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		XmlDocumentEditableNavigator_t1066973242 * L_0 = (XmlDocumentEditableNavigator_t1066973242 *)il2cpp_codegen_object_new(XmlDocumentEditableNavigator_t1066973242_il2cpp_TypeInfo_var);
		XmlDocumentEditableNavigator__ctor_m2729288836(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void Mono.Xml.XPath.XPathParser::.ctor(System.Xml.Xsl.IStaticXsltContext)
extern Il2CppClass* Console_t1097803980_il2cpp_TypeInfo_var;
extern Il2CppClass* TextWriter_t1689927879_il2cpp_TypeInfo_var;
extern const uint32_t XPathParser__ctor_m2662166670_MetadataUsageId;
extern "C"  void XPathParser__ctor_m2662166670 (XPathParser_t618877717 * __this, Il2CppObject * ___context0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathParser__ctor_m2662166670_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t1097803980_il2cpp_TypeInfo_var);
		TextWriter_t1689927879 * L_0 = Console_get_Out_m3780048827(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_ErrorOutput_1(L_0);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___context0;
		__this->set_Context_0(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(TextWriter_t1689927879_il2cpp_TypeInfo_var);
		TextWriter_t1689927879 * L_2 = ((TextWriter_t1689927879_StaticFields*)TextWriter_t1689927879_il2cpp_TypeInfo_var->static_fields)->get_Null_2();
		__this->set_ErrorOutput_1(L_2);
		return;
	}
}
// System.Void Mono.Xml.XPath.XPathParser::.cctor()
extern Il2CppClass* XPathParser_t618877717_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* Int16U5BU5D_t3675865332_il2cpp_TypeInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t3053238938____U24U24fieldU2D0_0_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t3053238938____U24U24fieldU2D1_1_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t3053238938____U24U24fieldU2D2_2_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t3053238938____U24U24fieldU2D3_3_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t3053238938____U24U24fieldU2D4_4_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t3053238938____U24U24fieldU2D5_5_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t3053238938____U24U24fieldU2D6_6_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t3053238938____U24U24fieldU2D7_7_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t3053238938____U24U24fieldU2D8_8_FieldInfo_var;
extern Il2CppCodeGenString* _stringLiteral2208659648;
extern Il2CppCodeGenString* _stringLiteral38634;
extern Il2CppCodeGenString* _stringLiteral38758;
extern Il2CppCodeGenString* _stringLiteral38789;
extern Il2CppCodeGenString* _stringLiteral38820;
extern Il2CppCodeGenString* _stringLiteral38851;
extern Il2CppCodeGenString* _stringLiteral38882;
extern Il2CppCodeGenString* _stringLiteral38913;
extern Il2CppCodeGenString* _stringLiteral38944;
extern Il2CppCodeGenString* _stringLiteral38975;
extern Il2CppCodeGenString* _stringLiteral39378;
extern Il2CppCodeGenString* _stringLiteral39409;
extern Il2CppCodeGenString* _stringLiteral39440;
extern Il2CppCodeGenString* _stringLiteral39502;
extern Il2CppCodeGenString* _stringLiteral40339;
extern Il2CppCodeGenString* _stringLiteral40401;
extern Il2CppCodeGenString* _stringLiteral41362;
extern Il2CppCodeGenString* _stringLiteral66247144;
extern Il2CppCodeGenString* _stringLiteral68828;
extern Il2CppCodeGenString* _stringLiteral78981469;
extern Il2CppCodeGenString* _stringLiteral2448425589;
extern Il2CppCodeGenString* _stringLiteral1059552;
extern Il2CppCodeGenString* _stringLiteral67881;
extern Il2CppCodeGenString* _stringLiteral2104361;
extern Il2CppCodeGenString* _stringLiteral1058560;
extern Il2CppCodeGenString* _stringLiteral1993453779;
extern Il2CppCodeGenString* _stringLiteral1070464;
extern Il2CppCodeGenString* _stringLiteral64305845;
extern Il2CppCodeGenString* _stringLiteral2099;
extern Il2CppCodeGenString* _stringLiteral904294034;
extern Il2CppCodeGenString* _stringLiteral3344544673;
extern Il2CppCodeGenString* _stringLiteral590478177;
extern Il2CppCodeGenString* _stringLiteral251377791;
extern Il2CppCodeGenString* _stringLiteral3486552643;
extern Il2CppCodeGenString* _stringLiteral64951;
extern Il2CppCodeGenString* _stringLiteral34398285;
extern Il2CppCodeGenString* _stringLiteral2531;
extern Il2CppCodeGenString* _stringLiteral1123133;
extern Il2CppCodeGenString* _stringLiteral67697;
extern Il2CppCodeGenString* _stringLiteral34483411;
extern Il2CppCodeGenString* _stringLiteral76514;
extern Il2CppCodeGenString* _stringLiteral34756738;
extern Il2CppCodeGenString* _stringLiteral2459034;
extern Il2CppCodeGenString* _stringLiteral73363536;
extern Il2CppCodeGenString* _stringLiteral438003570;
extern Il2CppCodeGenString* _stringLiteral2022079676;
extern Il2CppCodeGenString* _stringLiteral65523;
extern Il2CppCodeGenString* _stringLiteral2220;
extern Il2CppCodeGenString* _stringLiteral2487;
extern Il2CppCodeGenString* _stringLiteral1046532;
extern Il2CppCodeGenString* _stringLiteral2425;
extern Il2CppCodeGenString* _stringLiteral1072479;
extern Il2CppCodeGenString* _stringLiteral2270;
extern Il2CppCodeGenString* _stringLiteral1074401;
extern Il2CppCodeGenString* _stringLiteral2440;
extern Il2CppCodeGenString* _stringLiteral2285;
extern Il2CppCodeGenString* _stringLiteral4103764435;
extern Il2CppCodeGenString* _stringLiteral1784526541;
extern Il2CppCodeGenString* _stringLiteral3093547452;
extern Il2CppCodeGenString* _stringLiteral429421473;
extern Il2CppCodeGenString* _stringLiteral2799680124;
extern Il2CppCodeGenString* _stringLiteral3733808808;
extern Il2CppCodeGenString* _stringLiteral64093436;
extern Il2CppCodeGenString* _stringLiteral3043921192;
extern Il2CppCodeGenString* _stringLiteral3663755517;
extern Il2CppCodeGenString* _stringLiteral4143122787;
extern Il2CppCodeGenString* _stringLiteral2264202982;
extern Il2CppCodeGenString* _stringLiteral1572389210;
extern Il2CppCodeGenString* _stringLiteral3552510577;
extern Il2CppCodeGenString* _stringLiteral1301749075;
extern Il2CppCodeGenString* _stringLiteral3546176212;
extern Il2CppCodeGenString* _stringLiteral3138023362;
extern Il2CppCodeGenString* _stringLiteral4038812987;
extern Il2CppCodeGenString* _stringLiteral274586633;
extern Il2CppCodeGenString* _stringLiteral2352872618;
extern Il2CppCodeGenString* _stringLiteral2627600982;
extern Il2CppCodeGenString* _stringLiteral1406276771;
extern Il2CppCodeGenString* _stringLiteral3487977825;
extern Il2CppCodeGenString* _stringLiteral1436359430;
extern Il2CppCodeGenString* _stringLiteral3352811276;
extern Il2CppCodeGenString* _stringLiteral2541388;
extern Il2CppCodeGenString* _stringLiteral1082711924;
extern Il2CppCodeGenString* _stringLiteral1668381247;
extern Il2CppCodeGenString* _stringLiteral2368641989;
extern Il2CppCodeGenString* _stringLiteral2571565;
extern Il2CppCodeGenString* _stringLiteral1083647411;
extern Il2CppCodeGenString* _stringLiteral2864051522;
extern Il2CppCodeGenString* _stringLiteral2834426732;
extern Il2CppCodeGenString* _stringLiteral2401794;
extern Il2CppCodeGenString* _stringLiteral1078384510;
extern Il2CppCodeGenString* _stringLiteral1436456484;
extern Il2CppCodeGenString* _stringLiteral2313932617;
extern Il2CppCodeGenString* _stringLiteral900443279;
extern Il2CppCodeGenString* _stringLiteral77225596;
extern const uint32_t XPathParser__cctor_m2653475089_MetadataUsageId;
extern "C"  void XPathParser__cctor_m2653475089 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathParser__cctor_m2653475089_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((XPathParser_t618877717_StaticFields*)XPathParser_t618877717_il2cpp_TypeInfo_var->static_fields)->set_yyFinal_4(((int32_t)25));
		StringU5BU5D_t2956870243* L_0 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)((int32_t)334)));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral2208659648);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2208659648);
		StringU5BU5D_t2956870243* L_1 = L_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, ((int32_t)36));
		ArrayElementTypeCheck (L_1, _stringLiteral38634);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)36)), (String_t*)_stringLiteral38634);
		StringU5BU5D_t2956870243* L_2 = L_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, ((int32_t)40));
		ArrayElementTypeCheck (L_2, _stringLiteral38758);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)40)), (String_t*)_stringLiteral38758);
		StringU5BU5D_t2956870243* L_3 = L_2;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, ((int32_t)41));
		ArrayElementTypeCheck (L_3, _stringLiteral38789);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)41)), (String_t*)_stringLiteral38789);
		StringU5BU5D_t2956870243* L_4 = L_3;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, ((int32_t)42));
		ArrayElementTypeCheck (L_4, _stringLiteral38820);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)42)), (String_t*)_stringLiteral38820);
		StringU5BU5D_t2956870243* L_5 = L_4;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, ((int32_t)43));
		ArrayElementTypeCheck (L_5, _stringLiteral38851);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)43)), (String_t*)_stringLiteral38851);
		StringU5BU5D_t2956870243* L_6 = L_5;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, ((int32_t)44));
		ArrayElementTypeCheck (L_6, _stringLiteral38882);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)44)), (String_t*)_stringLiteral38882);
		StringU5BU5D_t2956870243* L_7 = L_6;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, ((int32_t)45));
		ArrayElementTypeCheck (L_7, _stringLiteral38913);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)45)), (String_t*)_stringLiteral38913);
		StringU5BU5D_t2956870243* L_8 = L_7;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, ((int32_t)46));
		ArrayElementTypeCheck (L_8, _stringLiteral38944);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)46)), (String_t*)_stringLiteral38944);
		StringU5BU5D_t2956870243* L_9 = L_8;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, ((int32_t)47));
		ArrayElementTypeCheck (L_9, _stringLiteral38975);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)47)), (String_t*)_stringLiteral38975);
		StringU5BU5D_t2956870243* L_10 = L_9;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, ((int32_t)60));
		ArrayElementTypeCheck (L_10, _stringLiteral39378);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)60)), (String_t*)_stringLiteral39378);
		StringU5BU5D_t2956870243* L_11 = L_10;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, ((int32_t)61));
		ArrayElementTypeCheck (L_11, _stringLiteral39409);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)61)), (String_t*)_stringLiteral39409);
		StringU5BU5D_t2956870243* L_12 = L_11;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, ((int32_t)62));
		ArrayElementTypeCheck (L_12, _stringLiteral39440);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)62)), (String_t*)_stringLiteral39440);
		StringU5BU5D_t2956870243* L_13 = L_12;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, ((int32_t)64));
		ArrayElementTypeCheck (L_13, _stringLiteral39502);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)64)), (String_t*)_stringLiteral39502);
		StringU5BU5D_t2956870243* L_14 = L_13;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, ((int32_t)91));
		ArrayElementTypeCheck (L_14, _stringLiteral40339);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)91)), (String_t*)_stringLiteral40339);
		StringU5BU5D_t2956870243* L_15 = L_14;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, ((int32_t)93));
		ArrayElementTypeCheck (L_15, _stringLiteral40401);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)93)), (String_t*)_stringLiteral40401);
		StringU5BU5D_t2956870243* L_16 = L_15;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, ((int32_t)124));
		ArrayElementTypeCheck (L_16, _stringLiteral41362);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)124)), (String_t*)_stringLiteral41362);
		StringU5BU5D_t2956870243* L_17 = L_16;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, ((int32_t)257));
		ArrayElementTypeCheck (L_17, _stringLiteral66247144);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)257)), (String_t*)_stringLiteral66247144);
		StringU5BU5D_t2956870243* L_18 = L_17;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, ((int32_t)258));
		ArrayElementTypeCheck (L_18, _stringLiteral68828);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)258)), (String_t*)_stringLiteral68828);
		StringU5BU5D_t2956870243* L_19 = L_18;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, ((int32_t)259));
		ArrayElementTypeCheck (L_19, _stringLiteral78981469);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)259)), (String_t*)_stringLiteral78981469);
		StringU5BU5D_t2956870243* L_20 = L_19;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, ((int32_t)260));
		ArrayElementTypeCheck (L_20, _stringLiteral2448425589);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)260)), (String_t*)_stringLiteral2448425589);
		StringU5BU5D_t2956870243* L_21 = L_20;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, ((int32_t)261));
		ArrayElementTypeCheck (L_21, _stringLiteral1059552);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)261)), (String_t*)_stringLiteral1059552);
		StringU5BU5D_t2956870243* L_22 = L_21;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, ((int32_t)262));
		ArrayElementTypeCheck (L_22, _stringLiteral67881);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)262)), (String_t*)_stringLiteral67881);
		StringU5BU5D_t2956870243* L_23 = L_22;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, ((int32_t)263));
		ArrayElementTypeCheck (L_23, _stringLiteral2104361);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)263)), (String_t*)_stringLiteral2104361);
		StringU5BU5D_t2956870243* L_24 = L_23;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, ((int32_t)264));
		ArrayElementTypeCheck (L_24, _stringLiteral1058560);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)264)), (String_t*)_stringLiteral1058560);
		StringU5BU5D_t2956870243* L_25 = L_24;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, ((int32_t)265));
		ArrayElementTypeCheck (L_25, _stringLiteral1993453779);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)265)), (String_t*)_stringLiteral1993453779);
		StringU5BU5D_t2956870243* L_26 = L_25;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, ((int32_t)266));
		ArrayElementTypeCheck (L_26, _stringLiteral1070464);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)266)), (String_t*)_stringLiteral1070464);
		StringU5BU5D_t2956870243* L_27 = L_26;
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, ((int32_t)267));
		ArrayElementTypeCheck (L_27, _stringLiteral64305845);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)267)), (String_t*)_stringLiteral64305845);
		StringU5BU5D_t2956870243* L_28 = L_27;
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, ((int32_t)268));
		ArrayElementTypeCheck (L_28, _stringLiteral2099);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)268)), (String_t*)_stringLiteral2099);
		StringU5BU5D_t2956870243* L_29 = L_28;
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, ((int32_t)269));
		ArrayElementTypeCheck (L_29, _stringLiteral904294034);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)269)), (String_t*)_stringLiteral904294034);
		StringU5BU5D_t2956870243* L_30 = L_29;
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, ((int32_t)270));
		ArrayElementTypeCheck (L_30, _stringLiteral3344544673);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)270)), (String_t*)_stringLiteral3344544673);
		StringU5BU5D_t2956870243* L_31 = L_30;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, ((int32_t)271));
		ArrayElementTypeCheck (L_31, _stringLiteral590478177);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)271)), (String_t*)_stringLiteral590478177);
		StringU5BU5D_t2956870243* L_32 = L_31;
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, ((int32_t)272));
		ArrayElementTypeCheck (L_32, _stringLiteral251377791);
		(L_32)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)272)), (String_t*)_stringLiteral251377791);
		StringU5BU5D_t2956870243* L_33 = L_32;
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, ((int32_t)273));
		ArrayElementTypeCheck (L_33, _stringLiteral3486552643);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)273)), (String_t*)_stringLiteral3486552643);
		StringU5BU5D_t2956870243* L_34 = L_33;
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, ((int32_t)274));
		ArrayElementTypeCheck (L_34, _stringLiteral64951);
		(L_34)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)274)), (String_t*)_stringLiteral64951);
		StringU5BU5D_t2956870243* L_35 = L_34;
		NullCheck(L_35);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_35, ((int32_t)275));
		ArrayElementTypeCheck (L_35, _stringLiteral34398285);
		(L_35)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)275)), (String_t*)_stringLiteral34398285);
		StringU5BU5D_t2956870243* L_36 = L_35;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, ((int32_t)276));
		ArrayElementTypeCheck (L_36, _stringLiteral2531);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)276)), (String_t*)_stringLiteral2531);
		StringU5BU5D_t2956870243* L_37 = L_36;
		NullCheck(L_37);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_37, ((int32_t)277));
		ArrayElementTypeCheck (L_37, _stringLiteral1123133);
		(L_37)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)277)), (String_t*)_stringLiteral1123133);
		StringU5BU5D_t2956870243* L_38 = L_37;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, ((int32_t)278));
		ArrayElementTypeCheck (L_38, _stringLiteral67697);
		(L_38)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)278)), (String_t*)_stringLiteral67697);
		StringU5BU5D_t2956870243* L_39 = L_38;
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, ((int32_t)279));
		ArrayElementTypeCheck (L_39, _stringLiteral34483411);
		(L_39)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)279)), (String_t*)_stringLiteral34483411);
		StringU5BU5D_t2956870243* L_40 = L_39;
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, ((int32_t)280));
		ArrayElementTypeCheck (L_40, _stringLiteral76514);
		(L_40)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)280)), (String_t*)_stringLiteral76514);
		StringU5BU5D_t2956870243* L_41 = L_40;
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, ((int32_t)281));
		ArrayElementTypeCheck (L_41, _stringLiteral34756738);
		(L_41)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)281)), (String_t*)_stringLiteral34756738);
		StringU5BU5D_t2956870243* L_42 = L_41;
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, ((int32_t)282));
		ArrayElementTypeCheck (L_42, _stringLiteral2459034);
		(L_42)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)282)), (String_t*)_stringLiteral2459034);
		StringU5BU5D_t2956870243* L_43 = L_42;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, ((int32_t)283));
		ArrayElementTypeCheck (L_43, _stringLiteral73363536);
		(L_43)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)283)), (String_t*)_stringLiteral73363536);
		StringU5BU5D_t2956870243* L_44 = L_43;
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, ((int32_t)284));
		ArrayElementTypeCheck (L_44, _stringLiteral438003570);
		(L_44)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)284)), (String_t*)_stringLiteral438003570);
		StringU5BU5D_t2956870243* L_45 = L_44;
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, ((int32_t)285));
		ArrayElementTypeCheck (L_45, _stringLiteral2022079676);
		(L_45)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)285)), (String_t*)_stringLiteral2022079676);
		StringU5BU5D_t2956870243* L_46 = L_45;
		NullCheck(L_46);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_46, ((int32_t)286));
		ArrayElementTypeCheck (L_46, _stringLiteral65523);
		(L_46)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)286)), (String_t*)_stringLiteral65523);
		StringU5BU5D_t2956870243* L_47 = L_46;
		NullCheck(L_47);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_47, ((int32_t)287));
		ArrayElementTypeCheck (L_47, _stringLiteral2220);
		(L_47)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)287)), (String_t*)_stringLiteral2220);
		StringU5BU5D_t2956870243* L_48 = L_47;
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, ((int32_t)288));
		ArrayElementTypeCheck (L_48, _stringLiteral2487);
		(L_48)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)288)), (String_t*)_stringLiteral2487);
		StringU5BU5D_t2956870243* L_49 = L_48;
		NullCheck(L_49);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_49, ((int32_t)289));
		ArrayElementTypeCheck (L_49, _stringLiteral1046532);
		(L_49)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)289)), (String_t*)_stringLiteral1046532);
		StringU5BU5D_t2956870243* L_50 = L_49;
		NullCheck(L_50);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_50, ((int32_t)290));
		ArrayElementTypeCheck (L_50, _stringLiteral2425);
		(L_50)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)290)), (String_t*)_stringLiteral2425);
		StringU5BU5D_t2956870243* L_51 = L_50;
		NullCheck(L_51);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_51, ((int32_t)291));
		ArrayElementTypeCheck (L_51, _stringLiteral1072479);
		(L_51)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)291)), (String_t*)_stringLiteral1072479);
		StringU5BU5D_t2956870243* L_52 = L_51;
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, ((int32_t)292));
		ArrayElementTypeCheck (L_52, _stringLiteral2270);
		(L_52)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)292)), (String_t*)_stringLiteral2270);
		StringU5BU5D_t2956870243* L_53 = L_52;
		NullCheck(L_53);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_53, ((int32_t)293));
		ArrayElementTypeCheck (L_53, _stringLiteral1074401);
		(L_53)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)293)), (String_t*)_stringLiteral1074401);
		StringU5BU5D_t2956870243* L_54 = L_53;
		NullCheck(L_54);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_54, ((int32_t)294));
		ArrayElementTypeCheck (L_54, _stringLiteral2440);
		(L_54)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)294)), (String_t*)_stringLiteral2440);
		StringU5BU5D_t2956870243* L_55 = L_54;
		NullCheck(L_55);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_55, ((int32_t)295));
		ArrayElementTypeCheck (L_55, _stringLiteral2285);
		(L_55)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)295)), (String_t*)_stringLiteral2285);
		StringU5BU5D_t2956870243* L_56 = L_55;
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, ((int32_t)296));
		ArrayElementTypeCheck (L_56, _stringLiteral4103764435);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)296)), (String_t*)_stringLiteral4103764435);
		StringU5BU5D_t2956870243* L_57 = L_56;
		NullCheck(L_57);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_57, ((int32_t)297));
		ArrayElementTypeCheck (L_57, _stringLiteral1784526541);
		(L_57)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)297)), (String_t*)_stringLiteral1784526541);
		StringU5BU5D_t2956870243* L_58 = L_57;
		NullCheck(L_58);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_58, ((int32_t)298));
		ArrayElementTypeCheck (L_58, _stringLiteral3093547452);
		(L_58)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)298)), (String_t*)_stringLiteral3093547452);
		StringU5BU5D_t2956870243* L_59 = L_58;
		NullCheck(L_59);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_59, ((int32_t)299));
		ArrayElementTypeCheck (L_59, _stringLiteral429421473);
		(L_59)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)299)), (String_t*)_stringLiteral429421473);
		StringU5BU5D_t2956870243* L_60 = L_59;
		NullCheck(L_60);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_60, ((int32_t)300));
		ArrayElementTypeCheck (L_60, _stringLiteral2799680124);
		(L_60)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)300)), (String_t*)_stringLiteral2799680124);
		StringU5BU5D_t2956870243* L_61 = L_60;
		NullCheck(L_61);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_61, ((int32_t)301));
		ArrayElementTypeCheck (L_61, _stringLiteral3733808808);
		(L_61)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)301)), (String_t*)_stringLiteral3733808808);
		StringU5BU5D_t2956870243* L_62 = L_61;
		NullCheck(L_62);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_62, ((int32_t)302));
		ArrayElementTypeCheck (L_62, _stringLiteral64093436);
		(L_62)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)302)), (String_t*)_stringLiteral64093436);
		StringU5BU5D_t2956870243* L_63 = L_62;
		NullCheck(L_63);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_63, ((int32_t)303));
		ArrayElementTypeCheck (L_63, _stringLiteral3043921192);
		(L_63)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)303)), (String_t*)_stringLiteral3043921192);
		StringU5BU5D_t2956870243* L_64 = L_63;
		NullCheck(L_64);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_64, ((int32_t)304));
		ArrayElementTypeCheck (L_64, _stringLiteral3663755517);
		(L_64)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)304)), (String_t*)_stringLiteral3663755517);
		StringU5BU5D_t2956870243* L_65 = L_64;
		NullCheck(L_65);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_65, ((int32_t)305));
		ArrayElementTypeCheck (L_65, _stringLiteral4143122787);
		(L_65)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)305)), (String_t*)_stringLiteral4143122787);
		StringU5BU5D_t2956870243* L_66 = L_65;
		NullCheck(L_66);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_66, ((int32_t)306));
		ArrayElementTypeCheck (L_66, _stringLiteral2264202982);
		(L_66)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)306)), (String_t*)_stringLiteral2264202982);
		StringU5BU5D_t2956870243* L_67 = L_66;
		NullCheck(L_67);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_67, ((int32_t)307));
		ArrayElementTypeCheck (L_67, _stringLiteral1572389210);
		(L_67)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)307)), (String_t*)_stringLiteral1572389210);
		StringU5BU5D_t2956870243* L_68 = L_67;
		NullCheck(L_68);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_68, ((int32_t)308));
		ArrayElementTypeCheck (L_68, _stringLiteral3552510577);
		(L_68)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)308)), (String_t*)_stringLiteral3552510577);
		StringU5BU5D_t2956870243* L_69 = L_68;
		NullCheck(L_69);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_69, ((int32_t)309));
		ArrayElementTypeCheck (L_69, _stringLiteral1301749075);
		(L_69)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)309)), (String_t*)_stringLiteral1301749075);
		StringU5BU5D_t2956870243* L_70 = L_69;
		NullCheck(L_70);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_70, ((int32_t)310));
		ArrayElementTypeCheck (L_70, _stringLiteral3546176212);
		(L_70)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)310)), (String_t*)_stringLiteral3546176212);
		StringU5BU5D_t2956870243* L_71 = L_70;
		NullCheck(L_71);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_71, ((int32_t)311));
		ArrayElementTypeCheck (L_71, _stringLiteral3138023362);
		(L_71)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)311)), (String_t*)_stringLiteral3138023362);
		StringU5BU5D_t2956870243* L_72 = L_71;
		NullCheck(L_72);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_72, ((int32_t)312));
		ArrayElementTypeCheck (L_72, _stringLiteral4038812987);
		(L_72)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)312)), (String_t*)_stringLiteral4038812987);
		StringU5BU5D_t2956870243* L_73 = L_72;
		NullCheck(L_73);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_73, ((int32_t)313));
		ArrayElementTypeCheck (L_73, _stringLiteral274586633);
		(L_73)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)313)), (String_t*)_stringLiteral274586633);
		StringU5BU5D_t2956870243* L_74 = L_73;
		NullCheck(L_74);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_74, ((int32_t)314));
		ArrayElementTypeCheck (L_74, _stringLiteral2352872618);
		(L_74)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)314)), (String_t*)_stringLiteral2352872618);
		StringU5BU5D_t2956870243* L_75 = L_74;
		NullCheck(L_75);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_75, ((int32_t)315));
		ArrayElementTypeCheck (L_75, _stringLiteral2627600982);
		(L_75)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)315)), (String_t*)_stringLiteral2627600982);
		StringU5BU5D_t2956870243* L_76 = L_75;
		NullCheck(L_76);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_76, ((int32_t)316));
		ArrayElementTypeCheck (L_76, _stringLiteral1406276771);
		(L_76)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)316)), (String_t*)_stringLiteral1406276771);
		StringU5BU5D_t2956870243* L_77 = L_76;
		NullCheck(L_77);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_77, ((int32_t)317));
		ArrayElementTypeCheck (L_77, _stringLiteral3487977825);
		(L_77)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)317)), (String_t*)_stringLiteral3487977825);
		StringU5BU5D_t2956870243* L_78 = L_77;
		NullCheck(L_78);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_78, ((int32_t)318));
		ArrayElementTypeCheck (L_78, _stringLiteral1436359430);
		(L_78)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)318)), (String_t*)_stringLiteral1436359430);
		StringU5BU5D_t2956870243* L_79 = L_78;
		NullCheck(L_79);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_79, ((int32_t)319));
		ArrayElementTypeCheck (L_79, _stringLiteral3352811276);
		(L_79)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)319)), (String_t*)_stringLiteral3352811276);
		StringU5BU5D_t2956870243* L_80 = L_79;
		NullCheck(L_80);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_80, ((int32_t)320));
		ArrayElementTypeCheck (L_80, _stringLiteral2541388);
		(L_80)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)320)), (String_t*)_stringLiteral2541388);
		StringU5BU5D_t2956870243* L_81 = L_80;
		NullCheck(L_81);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_81, ((int32_t)321));
		ArrayElementTypeCheck (L_81, _stringLiteral1082711924);
		(L_81)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)321)), (String_t*)_stringLiteral1082711924);
		StringU5BU5D_t2956870243* L_82 = L_81;
		NullCheck(L_82);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_82, ((int32_t)322));
		ArrayElementTypeCheck (L_82, _stringLiteral1668381247);
		(L_82)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)322)), (String_t*)_stringLiteral1668381247);
		StringU5BU5D_t2956870243* L_83 = L_82;
		NullCheck(L_83);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_83, ((int32_t)323));
		ArrayElementTypeCheck (L_83, _stringLiteral2368641989);
		(L_83)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)323)), (String_t*)_stringLiteral2368641989);
		StringU5BU5D_t2956870243* L_84 = L_83;
		NullCheck(L_84);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_84, ((int32_t)324));
		ArrayElementTypeCheck (L_84, _stringLiteral2571565);
		(L_84)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)324)), (String_t*)_stringLiteral2571565);
		StringU5BU5D_t2956870243* L_85 = L_84;
		NullCheck(L_85);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_85, ((int32_t)325));
		ArrayElementTypeCheck (L_85, _stringLiteral1083647411);
		(L_85)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)325)), (String_t*)_stringLiteral1083647411);
		StringU5BU5D_t2956870243* L_86 = L_85;
		NullCheck(L_86);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_86, ((int32_t)326));
		ArrayElementTypeCheck (L_86, _stringLiteral2864051522);
		(L_86)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)326)), (String_t*)_stringLiteral2864051522);
		StringU5BU5D_t2956870243* L_87 = L_86;
		NullCheck(L_87);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_87, ((int32_t)327));
		ArrayElementTypeCheck (L_87, _stringLiteral2834426732);
		(L_87)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)327)), (String_t*)_stringLiteral2834426732);
		StringU5BU5D_t2956870243* L_88 = L_87;
		NullCheck(L_88);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_88, ((int32_t)328));
		ArrayElementTypeCheck (L_88, _stringLiteral2401794);
		(L_88)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)328)), (String_t*)_stringLiteral2401794);
		StringU5BU5D_t2956870243* L_89 = L_88;
		NullCheck(L_89);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_89, ((int32_t)329));
		ArrayElementTypeCheck (L_89, _stringLiteral1078384510);
		(L_89)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)329)), (String_t*)_stringLiteral1078384510);
		StringU5BU5D_t2956870243* L_90 = L_89;
		NullCheck(L_90);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_90, ((int32_t)330));
		ArrayElementTypeCheck (L_90, _stringLiteral1436456484);
		(L_90)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)330)), (String_t*)_stringLiteral1436456484);
		StringU5BU5D_t2956870243* L_91 = L_90;
		NullCheck(L_91);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_91, ((int32_t)331));
		ArrayElementTypeCheck (L_91, _stringLiteral2313932617);
		(L_91)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)331)), (String_t*)_stringLiteral2313932617);
		StringU5BU5D_t2956870243* L_92 = L_91;
		NullCheck(L_92);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_92, ((int32_t)332));
		ArrayElementTypeCheck (L_92, _stringLiteral900443279);
		(L_92)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)332)), (String_t*)_stringLiteral900443279);
		StringU5BU5D_t2956870243* L_93 = L_92;
		NullCheck(L_93);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_93, ((int32_t)333));
		ArrayElementTypeCheck (L_93, _stringLiteral77225596);
		(L_93)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)333)), (String_t*)_stringLiteral77225596);
		((XPathParser_t618877717_StaticFields*)XPathParser_t618877717_il2cpp_TypeInfo_var->static_fields)->set_yyNames_5(L_93);
		Int16U5BU5D_t3675865332* L_94 = ((Int16U5BU5D_t3675865332*)SZArrayNew(Int16U5BU5D_t3675865332_il2cpp_TypeInfo_var, (uint32_t)((int32_t)104)));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_94, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t3053238938____U24U24fieldU2D0_0_FieldInfo_var), /*hidden argument*/NULL);
		((XPathParser_t618877717_StaticFields*)XPathParser_t618877717_il2cpp_TypeInfo_var->static_fields)->set_yyLhs_8(L_94);
		Int16U5BU5D_t3675865332* L_95 = ((Int16U5BU5D_t3675865332*)SZArrayNew(Int16U5BU5D_t3675865332_il2cpp_TypeInfo_var, (uint32_t)((int32_t)104)));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_95, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t3053238938____U24U24fieldU2D1_1_FieldInfo_var), /*hidden argument*/NULL);
		((XPathParser_t618877717_StaticFields*)XPathParser_t618877717_il2cpp_TypeInfo_var->static_fields)->set_yyLen_9(L_95);
		Int16U5BU5D_t3675865332* L_96 = ((Int16U5BU5D_t3675865332*)SZArrayNew(Int16U5BU5D_t3675865332_il2cpp_TypeInfo_var, (uint32_t)((int32_t)118)));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_96, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t3053238938____U24U24fieldU2D2_2_FieldInfo_var), /*hidden argument*/NULL);
		((XPathParser_t618877717_StaticFields*)XPathParser_t618877717_il2cpp_TypeInfo_var->static_fields)->set_yyDefRed_10(L_96);
		Int16U5BU5D_t3675865332* L_97 = ((Int16U5BU5D_t3675865332*)SZArrayNew(Int16U5BU5D_t3675865332_il2cpp_TypeInfo_var, (uint32_t)((int32_t)35)));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_97, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t3053238938____U24U24fieldU2D3_3_FieldInfo_var), /*hidden argument*/NULL);
		((XPathParser_t618877717_StaticFields*)XPathParser_t618877717_il2cpp_TypeInfo_var->static_fields)->set_yyDgoto_11(L_97);
		Int16U5BU5D_t3675865332* L_98 = ((Int16U5BU5D_t3675865332*)SZArrayNew(Int16U5BU5D_t3675865332_il2cpp_TypeInfo_var, (uint32_t)((int32_t)118)));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_98, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t3053238938____U24U24fieldU2D4_4_FieldInfo_var), /*hidden argument*/NULL);
		((XPathParser_t618877717_StaticFields*)XPathParser_t618877717_il2cpp_TypeInfo_var->static_fields)->set_yySindex_12(L_98);
		Int16U5BU5D_t3675865332* L_99 = ((Int16U5BU5D_t3675865332*)SZArrayNew(Int16U5BU5D_t3675865332_il2cpp_TypeInfo_var, (uint32_t)((int32_t)118)));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_99, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t3053238938____U24U24fieldU2D5_5_FieldInfo_var), /*hidden argument*/NULL);
		((XPathParser_t618877717_StaticFields*)XPathParser_t618877717_il2cpp_TypeInfo_var->static_fields)->set_yyRindex_13(L_99);
		Int16U5BU5D_t3675865332* L_100 = ((Int16U5BU5D_t3675865332*)SZArrayNew(Int16U5BU5D_t3675865332_il2cpp_TypeInfo_var, (uint32_t)((int32_t)35)));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_100, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t3053238938____U24U24fieldU2D6_6_FieldInfo_var), /*hidden argument*/NULL);
		((XPathParser_t618877717_StaticFields*)XPathParser_t618877717_il2cpp_TypeInfo_var->static_fields)->set_yyGindex_14(L_100);
		Int16U5BU5D_t3675865332* L_101 = ((Int16U5BU5D_t3675865332*)SZArrayNew(Int16U5BU5D_t3675865332_il2cpp_TypeInfo_var, (uint32_t)((int32_t)765)));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_101, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t3053238938____U24U24fieldU2D7_7_FieldInfo_var), /*hidden argument*/NULL);
		((XPathParser_t618877717_StaticFields*)XPathParser_t618877717_il2cpp_TypeInfo_var->static_fields)->set_yyTable_15(L_101);
		Int16U5BU5D_t3675865332* L_102 = ((Int16U5BU5D_t3675865332*)SZArrayNew(Int16U5BU5D_t3675865332_il2cpp_TypeInfo_var, (uint32_t)((int32_t)765)));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_102, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t3053238938____U24U24fieldU2D8_8_FieldInfo_var), /*hidden argument*/NULL);
		((XPathParser_t618877717_StaticFields*)XPathParser_t618877717_il2cpp_TypeInfo_var->static_fields)->set_yyCheck_16(L_102);
		return;
	}
}
// System.Xml.XPath.Expression Mono.Xml.XPath.XPathParser::Compile(System.String)
extern Il2CppClass* Tokenizer_t3551440624_il2cpp_TypeInfo_var;
extern Il2CppClass* Expression_t4217024437_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathException_t2353341743_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3091036757;
extern const uint32_t XPathParser_Compile_m3447021097_MetadataUsageId;
extern "C"  Expression_t4217024437 * XPathParser_Compile_m3447021097 (XPathParser_t618877717 * __this, String_t* ___xpath0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathParser_Compile_m3447021097_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Tokenizer_t3551440624 * V_0 = NULL;
	Exception_t1967233988 * V_1 = NULL;
	Expression_t4217024437 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_0 = ___xpath0;
			Tokenizer_t3551440624 * L_1 = (Tokenizer_t3551440624 *)il2cpp_codegen_object_new(Tokenizer_t3551440624_il2cpp_TypeInfo_var);
			Tokenizer__ctor_m3725140673(L_1, L_0, /*hidden argument*/NULL);
			V_0 = L_1;
			Tokenizer_t3551440624 * L_2 = V_0;
			Il2CppObject * L_3 = XPathParser_yyparse_m3024144012(__this, L_2, /*hidden argument*/NULL);
			V_2 = ((Expression_t4217024437 *)CastclassClass(L_3, Expression_t4217024437_il2cpp_TypeInfo_var));
			goto IL_003e;
		}

IL_0019:
		{
			; // IL_0019: leave IL_003e
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (XPathException_t2353341743_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001e;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0026;
		throw e;
	}

CATCH_001e:
	{ // begin catch(System.Xml.XPath.XPathException)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(__exception_local);
		}

IL_0021:
		{
			goto IL_003e;
		}
	} // end catch (depth: 1)

CATCH_0026:
	{ // begin catch(System.Exception)
		{
			V_1 = ((Exception_t1967233988 *)__exception_local);
			String_t* L_4 = ___xpath0;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_5 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3091036757, L_4, /*hidden argument*/NULL);
			Exception_t1967233988 * L_6 = V_1;
			XPathException_t2353341743 * L_7 = (XPathException_t2353341743 *)il2cpp_codegen_object_new(XPathException_t2353341743_il2cpp_TypeInfo_var);
			XPathException__ctor_m4004445308(L_7, L_5, L_6, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
		}

IL_0039:
		{
			goto IL_003e;
		}
	} // end catch (depth: 1)

IL_003e:
	{
		Expression_t4217024437 * L_8 = V_2;
		return L_8;
	}
}
// System.Xml.XPath.NodeSet Mono.Xml.XPath.XPathParser::CreateNodeTest(System.Xml.XPath.Axes,System.Object,System.Collections.ArrayList)
extern Il2CppClass* Expression_t4217024437_il2cpp_TypeInfo_var;
extern Il2CppClass* ExprFilter_t3320087274_il2cpp_TypeInfo_var;
extern const uint32_t XPathParser_CreateNodeTest_m3475400515_MetadataUsageId;
extern "C"  NodeSet_t3503134685 * XPathParser_CreateNodeTest_m3475400515 (XPathParser_t618877717 * __this, int32_t ___axis0, Il2CppObject * ___nodeTest1, ArrayList_t2121638921 * ___plist2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathParser_CreateNodeTest_m3475400515_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	NodeSet_t3503134685 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___axis0;
		Il2CppObject * L_1 = ___nodeTest1;
		NodeTest_t911751889 * L_2 = XPathParser_CreateNodeTest_m3760966820(__this, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		ArrayList_t2121638921 * L_3 = ___plist2;
		if (!L_3)
		{
			goto IL_0039;
		}
	}
	{
		V_1 = 0;
		goto IL_002d;
	}

IL_0016:
	{
		NodeSet_t3503134685 * L_4 = V_0;
		ArrayList_t2121638921 * L_5 = ___plist2;
		int32_t L_6 = V_1;
		NullCheck(L_5);
		Il2CppObject * L_7 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_5, L_6);
		ExprFilter_t3320087274 * L_8 = (ExprFilter_t3320087274 *)il2cpp_codegen_object_new(ExprFilter_t3320087274_il2cpp_TypeInfo_var);
		ExprFilter__ctor_m1987528211(L_8, L_4, ((Expression_t4217024437 *)CastclassClass(L_7, Expression_t4217024437_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_0 = L_8;
		int32_t L_9 = V_1;
		V_1 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_002d:
	{
		int32_t L_10 = V_1;
		ArrayList_t2121638921 * L_11 = ___plist2;
		NullCheck(L_11);
		int32_t L_12 = VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_11);
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_0016;
		}
	}

IL_0039:
	{
		NodeSet_t3503134685 * L_13 = V_0;
		return L_13;
	}
}
// System.Xml.XPath.NodeTest Mono.Xml.XPath.XPathParser::CreateNodeTest(System.Xml.XPath.Axes,System.Object)
extern Il2CppClass* XPathNodeType_t4070737174_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* NodeTypeTest_t2651955243_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlQualifiedName_t176365656_il2cpp_TypeInfo_var;
extern Il2CppClass* NodeNameTest_t873888508_il2cpp_TypeInfo_var;
extern const uint32_t XPathParser_CreateNodeTest_m3760966820_MetadataUsageId;
extern "C"  NodeTest_t911751889 * XPathParser_CreateNodeTest_m3760966820 (XPathParser_t618877717 * __this, int32_t ___axis0, Il2CppObject * ___test1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathParser_CreateNodeTest_m3760966820_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XmlQualifiedName_t176365656 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___test1;
		if (!((Il2CppObject *)IsInstSealed(L_0, XPathNodeType_t4070737174_il2cpp_TypeInfo_var)))
		{
			goto IL_0019;
		}
	}
	{
		int32_t L_1 = ___axis0;
		Il2CppObject * L_2 = ___test1;
		NodeTypeTest_t2651955243 * L_3 = (NodeTypeTest_t2651955243 *)il2cpp_codegen_object_new(NodeTypeTest_t2651955243_il2cpp_TypeInfo_var);
		NodeTypeTest__ctor_m2764187254(L_3, L_1, ((*(int32_t*)((int32_t*)UnBox (L_2, Int32_t2847414787_il2cpp_TypeInfo_var)))), (String_t*)NULL, /*hidden argument*/NULL);
		return L_3;
	}

IL_0019:
	{
		Il2CppObject * L_4 = ___test1;
		if (((String_t*)IsInstSealed(L_4, String_t_il2cpp_TypeInfo_var)))
		{
			goto IL_002a;
		}
	}
	{
		Il2CppObject * L_5 = ___test1;
		if (L_5)
		{
			goto IL_0038;
		}
	}

IL_002a:
	{
		int32_t L_6 = ___axis0;
		Il2CppObject * L_7 = ___test1;
		NodeTypeTest_t2651955243 * L_8 = (NodeTypeTest_t2651955243 *)il2cpp_codegen_object_new(NodeTypeTest_t2651955243_il2cpp_TypeInfo_var);
		NodeTypeTest__ctor_m2764187254(L_8, L_6, 7, ((String_t*)CastclassSealed(L_7, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_8;
	}

IL_0038:
	{
		Il2CppObject * L_9 = ___test1;
		V_0 = ((XmlQualifiedName_t176365656 *)CastclassClass(L_9, XmlQualifiedName_t176365656_il2cpp_TypeInfo_var));
		XmlQualifiedName_t176365656 * L_10 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(XmlQualifiedName_t176365656_il2cpp_TypeInfo_var);
		XmlQualifiedName_t176365656 * L_11 = ((XmlQualifiedName_t176365656_StaticFields*)XmlQualifiedName_t176365656_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		bool L_12 = XmlQualifiedName_op_Equality_m273752697(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0056;
		}
	}
	{
		int32_t L_13 = ___axis0;
		NodeTypeTest_t2651955243 * L_14 = (NodeTypeTest_t2651955243 *)il2cpp_codegen_object_new(NodeTypeTest_t2651955243_il2cpp_TypeInfo_var);
		NodeTypeTest__ctor_m423301530(L_14, L_13, /*hidden argument*/NULL);
		return L_14;
	}

IL_0056:
	{
		int32_t L_15 = ___axis0;
		XmlQualifiedName_t176365656 * L_16 = V_0;
		Il2CppObject * L_17 = __this->get_Context_0();
		NodeNameTest_t873888508 * L_18 = (NodeNameTest_t873888508 *)il2cpp_codegen_object_new(NodeNameTest_t873888508_il2cpp_TypeInfo_var);
		NodeNameTest__ctor_m98601429(L_18, L_15, L_16, L_17, /*hidden argument*/NULL);
		return L_18;
	}
}
// System.String Mono.Xml.XPath.XPathParser::yyname(System.Int32)
extern Il2CppClass* XPathParser_t618877717_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1591398658;
extern Il2CppCodeGenString* _stringLiteral2785235630;
extern const uint32_t XPathParser_yyname_m3001501735_MetadataUsageId;
extern "C"  String_t* XPathParser_yyname_m3001501735 (Il2CppObject * __this /* static, unused */, int32_t ___token0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathParser_yyname_m3001501735_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		int32_t L_0 = ___token0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_1 = ___token0;
		IL2CPP_RUNTIME_CLASS_INIT(XPathParser_t618877717_il2cpp_TypeInfo_var);
		StringU5BU5D_t2956870243* L_2 = ((XPathParser_t618877717_StaticFields*)XPathParser_t618877717_il2cpp_TypeInfo_var->static_fields)->get_yyNames_5();
		NullCheck(L_2);
		if ((((int32_t)L_1) <= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))))))
		{
			goto IL_001a;
		}
	}

IL_0014:
	{
		return _stringLiteral1591398658;
	}

IL_001a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XPathParser_t618877717_il2cpp_TypeInfo_var);
		StringU5BU5D_t2956870243* L_3 = ((XPathParser_t618877717_StaticFields*)XPathParser_t618877717_il2cpp_TypeInfo_var->static_fields)->get_yyNames_5();
		int32_t L_4 = ___token0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		String_t* L_6 = ((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5)));
		V_0 = L_6;
		if (!L_6)
		{
			goto IL_002a;
		}
	}
	{
		String_t* L_7 = V_0;
		return L_7;
	}

IL_002a:
	{
		return _stringLiteral2785235630;
	}
}
// System.Object Mono.Xml.XPath.XPathParser::yyDefault(System.Object)
extern "C"  Il2CppObject * XPathParser_yyDefault_m772388494 (XPathParser_t618877717 * __this, Il2CppObject * ___first0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___first0;
		return L_0;
	}
}
// System.Object Mono.Xml.XPath.XPathParser::yyparse(Mono.Xml.XPath.yyParser.yyInput)
extern Il2CppClass* Int32U5BU5D_t1809983122_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* yyDebug_t212154387_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathParser_t618877717_il2cpp_TypeInfo_var;
extern Il2CppClass* yyInput_t1890241104_il2cpp_TypeInfo_var;
extern Il2CppClass* yyUnexpectedEof_t2997709169_il2cpp_TypeInfo_var;
extern Il2CppClass* yyException_t1136059093_il2cpp_TypeInfo_var;
extern Il2CppClass* YYRules_t10764727_il2cpp_TypeInfo_var;
extern Il2CppClass* NodeSet_t3503134685_il2cpp_TypeInfo_var;
extern Il2CppClass* ExprUNION_t2208364215_il2cpp_TypeInfo_var;
extern Il2CppClass* ExprRoot_t2159432084_il2cpp_TypeInfo_var;
extern Il2CppClass* ExprSLASH_t2206450021_il2cpp_TypeInfo_var;
extern Il2CppClass* Expression_t4217024437_il2cpp_TypeInfo_var;
extern Il2CppClass* ExprSLASH2_t3664170439_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlQualifiedName_t176365656_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathException_t2353341743_il2cpp_TypeInfo_var;
extern Il2CppClass* ExprLiteral_t569227735_il2cpp_TypeInfo_var;
extern Il2CppClass* FunctionArguments_t3402201403_il2cpp_TypeInfo_var;
extern Il2CppClass* IStaticXsltContext_t2347050862_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* ArrayList_t2121638921_il2cpp_TypeInfo_var;
extern Il2CppClass* Axes_t4021066818_il2cpp_TypeInfo_var;
extern Il2CppClass* ExprOR_t1813727157_il2cpp_TypeInfo_var;
extern Il2CppClass* ExprAND_t79682687_il2cpp_TypeInfo_var;
extern Il2CppClass* ExprEQ_t1813726846_il2cpp_TypeInfo_var;
extern Il2CppClass* ExprNE_t1813727113_il2cpp_TypeInfo_var;
extern Il2CppClass* ExprLT_t1813727066_il2cpp_TypeInfo_var;
extern Il2CppClass* ExprGT_t1813726911_il2cpp_TypeInfo_var;
extern Il2CppClass* ExprLE_t1813727051_il2cpp_TypeInfo_var;
extern Il2CppClass* ExprGE_t1813726896_il2cpp_TypeInfo_var;
extern Il2CppClass* ExprPLUS_t2159338028_il2cpp_TypeInfo_var;
extern Il2CppClass* ExprMINUS_t2200832088_il2cpp_TypeInfo_var;
extern Il2CppClass* ExprMULT_t2159257026_il2cpp_TypeInfo_var;
extern Il2CppClass* ExprDIV_t79685433_il2cpp_TypeInfo_var;
extern Il2CppClass* ExprMOD_t79694250_il2cpp_TypeInfo_var;
extern Il2CppClass* ExprNEG_t79694904_il2cpp_TypeInfo_var;
extern Il2CppClass* XPathNodeType_t4070737174_il2cpp_TypeInfo_var;
extern Il2CppClass* NodeTypeTest_t2651955243_il2cpp_TypeInfo_var;
extern Il2CppClass* ExprFilter_t3320087274_il2cpp_TypeInfo_var;
extern Il2CppClass* ExprVariable_t1838989486_il2cpp_TypeInfo_var;
extern Il2CppClass* ExprParens_t3599155227_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern Il2CppClass* ExprNumber_t3560215227_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1611946891;
extern Il2CppCodeGenString* _stringLiteral729072086;
extern Il2CppCodeGenString* _stringLiteral3824180189;
extern Il2CppCodeGenString* _stringLiteral3355;
extern Il2CppCodeGenString* _stringLiteral683753632;
extern Il2CppCodeGenString* _stringLiteral106079;
extern Il2CppCodeGenString* _stringLiteral1408589050;
extern const uint32_t XPathParser_yyparse_m3024144012_MetadataUsageId;
extern "C"  Il2CppObject * XPathParser_yyparse_m3024144012 (XPathParser_t618877717 * __this, Il2CppObject * ___yyLex0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XPathParser_yyparse_m3024144012_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Int32U5BU5D_t1809983122* V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	ObjectU5BU5D_t11523773* V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	Int32U5BU5D_t1809983122* V_7 = NULL;
	ObjectU5BU5D_t11523773* V_8 = NULL;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	XmlQualifiedName_t176365656 * V_11 = NULL;
	XmlQualifiedName_t176365656 * V_12 = NULL;
	ArrayList_t2121638921 * V_13 = NULL;
	ArrayList_t2121638921 * V_14 = NULL;
	Expression_t4217024437 * V_15 = NULL;
	Expression_t4217024437 * V_16 = NULL;
	int32_t V_17 = 0;
	int32_t V_18 = 0;
	int32_t G_B13_0 = 0;
	XPathParser_t618877717 * G_B62_0 = NULL;
	XPathParser_t618877717 * G_B61_0 = NULL;
	Il2CppObject * G_B63_0 = NULL;
	XPathParser_t618877717 * G_B63_1 = NULL;
	int32_t G_B165_0 = 0;
	{
		int32_t L_0 = __this->get_yyMax_7();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0017;
		}
	}
	{
		__this->set_yyMax_7(((int32_t)256));
	}

IL_0017:
	{
		V_0 = 0;
		int32_t L_1 = __this->get_yyMax_7();
		V_1 = ((Int32U5BU5D_t1809983122*)SZArrayNew(Int32U5BU5D_t1809983122_il2cpp_TypeInfo_var, (uint32_t)L_1));
		V_2 = NULL;
		int32_t L_2 = __this->get_yyMax_7();
		V_3 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)L_2));
		V_4 = (-1);
		V_5 = 0;
		V_6 = 0;
		goto IL_0eb3;
	}

IL_0041:
	{
		int32_t L_3 = V_6;
		Int32U5BU5D_t1809983122* L_4 = V_1;
		NullCheck(L_4);
		if ((((int32_t)L_3) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length)))))))
		{
			goto IL_0085;
		}
	}
	{
		Int32U5BU5D_t1809983122* L_5 = V_1;
		NullCheck(L_5);
		int32_t L_6 = __this->get_yyMax_7();
		V_7 = ((Int32U5BU5D_t1809983122*)SZArrayNew(Int32U5BU5D_t1809983122_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length))))+(int32_t)L_6))));
		Int32U5BU5D_t1809983122* L_7 = V_1;
		Int32U5BU5D_t1809983122* L_8 = V_7;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_7);
		VirtActionInvoker2< Il2CppArray *, int32_t >::Invoke(9 /* System.Void System.Array::CopyTo(System.Array,System.Int32) */, (Il2CppArray *)(Il2CppArray *)L_7, (Il2CppArray *)(Il2CppArray *)L_8, 0);
		Int32U5BU5D_t1809983122* L_9 = V_7;
		V_1 = L_9;
		ObjectU5BU5D_t11523773* L_10 = V_3;
		NullCheck(L_10);
		int32_t L_11 = __this->get_yyMax_7();
		V_8 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length))))+(int32_t)L_11))));
		ObjectU5BU5D_t11523773* L_12 = V_3;
		ObjectU5BU5D_t11523773* L_13 = V_8;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_12);
		VirtActionInvoker2< Il2CppArray *, int32_t >::Invoke(9 /* System.Void System.Array::CopyTo(System.Array,System.Int32) */, (Il2CppArray *)(Il2CppArray *)L_12, (Il2CppArray *)(Il2CppArray *)L_13, 0);
		ObjectU5BU5D_t11523773* L_14 = V_8;
		V_3 = L_14;
	}

IL_0085:
	{
		Int32U5BU5D_t1809983122* L_15 = V_1;
		int32_t L_16 = V_6;
		int32_t L_17 = V_0;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(L_16), (int32_t)L_17);
		ObjectU5BU5D_t11523773* L_18 = V_3;
		int32_t L_19 = V_6;
		Il2CppObject * L_20 = V_2;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_19);
		ArrayElementTypeCheck (L_18, L_20);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(L_19), (Il2CppObject *)L_20);
		Il2CppObject * L_21 = __this->get_debug_3();
		if (!L_21)
		{
			goto IL_00a7;
		}
	}
	{
		Il2CppObject * L_22 = __this->get_debug_3();
		int32_t L_23 = V_0;
		Il2CppObject * L_24 = V_2;
		NullCheck(L_22);
		InterfaceActionInvoker2< int32_t, Il2CppObject * >::Invoke(0 /* System.Void Mono.Xml.XPath.yydebug.yyDebug::push(System.Int32,System.Object) */, yyDebug_t212154387_il2cpp_TypeInfo_var, L_22, L_23, L_24);
	}

IL_00a7:
	{
		goto IL_0ea3;
	}

IL_00ac:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XPathParser_t618877717_il2cpp_TypeInfo_var);
		Int16U5BU5D_t3675865332* L_25 = ((XPathParser_t618877717_StaticFields*)XPathParser_t618877717_il2cpp_TypeInfo_var->static_fields)->get_yyDefRed_10();
		int32_t L_26 = V_0;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, L_26);
		int32_t L_27 = L_26;
		int16_t L_28 = ((L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27)));
		V_9 = L_28;
		if (L_28)
		{
			goto IL_034b;
		}
	}
	{
		int32_t L_29 = V_4;
		if ((((int32_t)L_29) >= ((int32_t)0)))
		{
			goto IL_0102;
		}
	}
	{
		Il2CppObject * L_30 = ___yyLex0;
		NullCheck(L_30);
		bool L_31 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean Mono.Xml.XPath.yyParser.yyInput::advance() */, yyInput_t1890241104_il2cpp_TypeInfo_var, L_30);
		if (!L_31)
		{
			goto IL_00d9;
		}
	}
	{
		Il2CppObject * L_32 = ___yyLex0;
		NullCheck(L_32);
		int32_t L_33 = InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 Mono.Xml.XPath.yyParser.yyInput::token() */, yyInput_t1890241104_il2cpp_TypeInfo_var, L_32);
		G_B13_0 = L_33;
		goto IL_00da;
	}

IL_00d9:
	{
		G_B13_0 = 0;
	}

IL_00da:
	{
		V_4 = G_B13_0;
		Il2CppObject * L_34 = __this->get_debug_3();
		if (!L_34)
		{
			goto IL_0102;
		}
	}
	{
		Il2CppObject * L_35 = __this->get_debug_3();
		int32_t L_36 = V_0;
		int32_t L_37 = V_4;
		int32_t L_38 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(XPathParser_t618877717_il2cpp_TypeInfo_var);
		String_t* L_39 = XPathParser_yyname_m3001501735(NULL /*static, unused*/, L_38, /*hidden argument*/NULL);
		Il2CppObject * L_40 = ___yyLex0;
		NullCheck(L_40);
		Il2CppObject * L_41 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object Mono.Xml.XPath.yyParser.yyInput::value() */, yyInput_t1890241104_il2cpp_TypeInfo_var, L_40);
		NullCheck(L_35);
		InterfaceActionInvoker4< int32_t, int32_t, String_t*, Il2CppObject * >::Invoke(1 /* System.Void Mono.Xml.XPath.yydebug.yyDebug::lex(System.Int32,System.Int32,System.String,System.Object) */, yyDebug_t212154387_il2cpp_TypeInfo_var, L_35, L_36, L_37, L_39, L_41);
	}

IL_0102:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XPathParser_t618877717_il2cpp_TypeInfo_var);
		Int16U5BU5D_t3675865332* L_42 = ((XPathParser_t618877717_StaticFields*)XPathParser_t618877717_il2cpp_TypeInfo_var->static_fields)->get_yySindex_12();
		int32_t L_43 = V_0;
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, L_43);
		int32_t L_44 = L_43;
		int16_t L_45 = ((L_42)->GetAt(static_cast<il2cpp_array_size_t>(L_44)));
		V_9 = L_45;
		if (!L_45)
		{
			goto IL_0185;
		}
	}
	{
		int32_t L_46 = V_9;
		int32_t L_47 = V_4;
		int32_t L_48 = ((int32_t)((int32_t)L_46+(int32_t)L_47));
		V_9 = L_48;
		if ((((int32_t)L_48) < ((int32_t)0)))
		{
			goto IL_0185;
		}
	}
	{
		int32_t L_49 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(XPathParser_t618877717_il2cpp_TypeInfo_var);
		Int16U5BU5D_t3675865332* L_50 = ((XPathParser_t618877717_StaticFields*)XPathParser_t618877717_il2cpp_TypeInfo_var->static_fields)->get_yyTable_15();
		NullCheck(L_50);
		if ((((int32_t)L_49) >= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_50)->max_length)))))))
		{
			goto IL_0185;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XPathParser_t618877717_il2cpp_TypeInfo_var);
		Int16U5BU5D_t3675865332* L_51 = ((XPathParser_t618877717_StaticFields*)XPathParser_t618877717_il2cpp_TypeInfo_var->static_fields)->get_yyCheck_16();
		int32_t L_52 = V_9;
		NullCheck(L_51);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_51, L_52);
		int32_t L_53 = L_52;
		int32_t L_54 = V_4;
		if ((!(((uint32_t)((L_51)->GetAt(static_cast<il2cpp_array_size_t>(L_53)))) == ((uint32_t)L_54))))
		{
			goto IL_0185;
		}
	}
	{
		Il2CppObject * L_55 = __this->get_debug_3();
		if (!L_55)
		{
			goto IL_015f;
		}
	}
	{
		Il2CppObject * L_56 = __this->get_debug_3();
		int32_t L_57 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(XPathParser_t618877717_il2cpp_TypeInfo_var);
		Int16U5BU5D_t3675865332* L_58 = ((XPathParser_t618877717_StaticFields*)XPathParser_t618877717_il2cpp_TypeInfo_var->static_fields)->get_yyTable_15();
		int32_t L_59 = V_9;
		NullCheck(L_58);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_58, L_59);
		int32_t L_60 = L_59;
		int32_t L_61 = V_5;
		NullCheck(L_56);
		InterfaceActionInvoker3< int32_t, int32_t, int32_t >::Invoke(2 /* System.Void Mono.Xml.XPath.yydebug.yyDebug::shift(System.Int32,System.Int32,System.Int32) */, yyDebug_t212154387_il2cpp_TypeInfo_var, L_56, L_57, ((L_58)->GetAt(static_cast<il2cpp_array_size_t>(L_60))), ((int32_t)((int32_t)L_61-(int32_t)1)));
	}

IL_015f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XPathParser_t618877717_il2cpp_TypeInfo_var);
		Int16U5BU5D_t3675865332* L_62 = ((XPathParser_t618877717_StaticFields*)XPathParser_t618877717_il2cpp_TypeInfo_var->static_fields)->get_yyTable_15();
		int32_t L_63 = V_9;
		NullCheck(L_62);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_62, L_63);
		int32_t L_64 = L_63;
		V_0 = ((L_62)->GetAt(static_cast<il2cpp_array_size_t>(L_64)));
		Il2CppObject * L_65 = ___yyLex0;
		NullCheck(L_65);
		Il2CppObject * L_66 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object Mono.Xml.XPath.yyParser.yyInput::value() */, yyInput_t1890241104_il2cpp_TypeInfo_var, L_65);
		V_2 = L_66;
		V_4 = (-1);
		int32_t L_67 = V_5;
		if ((((int32_t)L_67) <= ((int32_t)0)))
		{
			goto IL_0180;
		}
	}
	{
		int32_t L_68 = V_5;
		V_5 = ((int32_t)((int32_t)L_68-(int32_t)1));
	}

IL_0180:
	{
		goto IL_0ea8;
	}

IL_0185:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XPathParser_t618877717_il2cpp_TypeInfo_var);
		Int16U5BU5D_t3675865332* L_69 = ((XPathParser_t618877717_StaticFields*)XPathParser_t618877717_il2cpp_TypeInfo_var->static_fields)->get_yyRindex_13();
		int32_t L_70 = V_0;
		NullCheck(L_69);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_69, L_70);
		int32_t L_71 = L_70;
		int16_t L_72 = ((L_69)->GetAt(static_cast<il2cpp_array_size_t>(L_71)));
		V_9 = L_72;
		if (!L_72)
		{
			goto IL_01ce;
		}
	}
	{
		int32_t L_73 = V_9;
		int32_t L_74 = V_4;
		int32_t L_75 = ((int32_t)((int32_t)L_73+(int32_t)L_74));
		V_9 = L_75;
		if ((((int32_t)L_75) < ((int32_t)0)))
		{
			goto IL_01ce;
		}
	}
	{
		int32_t L_76 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(XPathParser_t618877717_il2cpp_TypeInfo_var);
		Int16U5BU5D_t3675865332* L_77 = ((XPathParser_t618877717_StaticFields*)XPathParser_t618877717_il2cpp_TypeInfo_var->static_fields)->get_yyTable_15();
		NullCheck(L_77);
		if ((((int32_t)L_76) >= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_77)->max_length)))))))
		{
			goto IL_01ce;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XPathParser_t618877717_il2cpp_TypeInfo_var);
		Int16U5BU5D_t3675865332* L_78 = ((XPathParser_t618877717_StaticFields*)XPathParser_t618877717_il2cpp_TypeInfo_var->static_fields)->get_yyCheck_16();
		int32_t L_79 = V_9;
		NullCheck(L_78);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_78, L_79);
		int32_t L_80 = L_79;
		int32_t L_81 = V_4;
		if ((!(((uint32_t)((L_78)->GetAt(static_cast<il2cpp_array_size_t>(L_80)))) == ((uint32_t)L_81))))
		{
			goto IL_01ce;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XPathParser_t618877717_il2cpp_TypeInfo_var);
		Int16U5BU5D_t3675865332* L_82 = ((XPathParser_t618877717_StaticFields*)XPathParser_t618877717_il2cpp_TypeInfo_var->static_fields)->get_yyTable_15();
		int32_t L_83 = V_9;
		NullCheck(L_82);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_82, L_83);
		int32_t L_84 = L_83;
		V_9 = ((L_82)->GetAt(static_cast<il2cpp_array_size_t>(L_84)));
		goto IL_034b;
	}

IL_01ce:
	{
		int32_t L_85 = V_5;
		V_18 = L_85;
		int32_t L_86 = V_18;
		if (L_86 == 0)
		{
			goto IL_01ee;
		}
		if (L_86 == 1)
		{
			goto IL_022f;
		}
		if (L_86 == 2)
		{
			goto IL_022f;
		}
		if (L_86 == 3)
		{
			goto IL_02f5;
		}
	}
	{
		goto IL_034b;
	}

IL_01ee:
	{
		int32_t L_87 = V_0;
		__this->set_yyExpectingState_6(L_87);
		Il2CppObject * L_88 = __this->get_debug_3();
		if (!L_88)
		{
			goto IL_0210;
		}
	}
	{
		Il2CppObject * L_89 = __this->get_debug_3();
		NullCheck(L_89);
		InterfaceActionInvoker1< String_t* >::Invoke(8 /* System.Void Mono.Xml.XPath.yydebug.yyDebug::error(System.String) */, yyDebug_t212154387_il2cpp_TypeInfo_var, L_89, _stringLiteral1611946891);
	}

IL_0210:
	{
		int32_t L_90 = V_4;
		if (!L_90)
		{
			goto IL_0224;
		}
	}
	{
		int32_t L_91 = V_4;
		int32_t L_92 = __this->get_eof_token_2();
		if ((!(((uint32_t)L_91) == ((uint32_t)L_92))))
		{
			goto IL_022a;
		}
	}

IL_0224:
	{
		yyUnexpectedEof_t2997709169 * L_93 = (yyUnexpectedEof_t2997709169 *)il2cpp_codegen_object_new(yyUnexpectedEof_t2997709169_il2cpp_TypeInfo_var);
		yyUnexpectedEof__ctor_m1709160914(L_93, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_93);
	}

IL_022a:
	{
		goto IL_022f;
	}

IL_022f:
	{
		V_5 = 3;
	}

IL_0232:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XPathParser_t618877717_il2cpp_TypeInfo_var);
		Int16U5BU5D_t3675865332* L_94 = ((XPathParser_t618877717_StaticFields*)XPathParser_t618877717_il2cpp_TypeInfo_var->static_fields)->get_yySindex_12();
		Int32U5BU5D_t1809983122* L_95 = V_1;
		int32_t L_96 = V_6;
		NullCheck(L_95);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_95, L_96);
		int32_t L_97 = L_96;
		NullCheck(L_94);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_94, ((L_95)->GetAt(static_cast<il2cpp_array_size_t>(L_97))));
		int32_t L_98 = ((L_95)->GetAt(static_cast<il2cpp_array_size_t>(L_97)));
		int16_t L_99 = ((L_94)->GetAt(static_cast<il2cpp_array_size_t>(L_98)));
		V_9 = L_99;
		if (!L_99)
		{
			goto IL_02ad;
		}
	}
	{
		int32_t L_100 = V_9;
		int32_t L_101 = ((int32_t)((int32_t)L_100+(int32_t)((int32_t)256)));
		V_9 = L_101;
		if ((((int32_t)L_101) < ((int32_t)0)))
		{
			goto IL_02ad;
		}
	}
	{
		int32_t L_102 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(XPathParser_t618877717_il2cpp_TypeInfo_var);
		Int16U5BU5D_t3675865332* L_103 = ((XPathParser_t618877717_StaticFields*)XPathParser_t618877717_il2cpp_TypeInfo_var->static_fields)->get_yyTable_15();
		NullCheck(L_103);
		if ((((int32_t)L_102) >= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_103)->max_length)))))))
		{
			goto IL_02ad;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XPathParser_t618877717_il2cpp_TypeInfo_var);
		Int16U5BU5D_t3675865332* L_104 = ((XPathParser_t618877717_StaticFields*)XPathParser_t618877717_il2cpp_TypeInfo_var->static_fields)->get_yyCheck_16();
		int32_t L_105 = V_9;
		NullCheck(L_104);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_104, L_105);
		int32_t L_106 = L_105;
		if ((!(((uint32_t)((L_104)->GetAt(static_cast<il2cpp_array_size_t>(L_106)))) == ((uint32_t)((int32_t)256)))))
		{
			goto IL_02ad;
		}
	}
	{
		Il2CppObject * L_107 = __this->get_debug_3();
		if (!L_107)
		{
			goto IL_0298;
		}
	}
	{
		Il2CppObject * L_108 = __this->get_debug_3();
		Int32U5BU5D_t1809983122* L_109 = V_1;
		int32_t L_110 = V_6;
		NullCheck(L_109);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_109, L_110);
		int32_t L_111 = L_110;
		IL2CPP_RUNTIME_CLASS_INIT(XPathParser_t618877717_il2cpp_TypeInfo_var);
		Int16U5BU5D_t3675865332* L_112 = ((XPathParser_t618877717_StaticFields*)XPathParser_t618877717_il2cpp_TypeInfo_var->static_fields)->get_yyTable_15();
		int32_t L_113 = V_9;
		NullCheck(L_112);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_112, L_113);
		int32_t L_114 = L_113;
		NullCheck(L_108);
		InterfaceActionInvoker3< int32_t, int32_t, int32_t >::Invoke(2 /* System.Void Mono.Xml.XPath.yydebug.yyDebug::shift(System.Int32,System.Int32,System.Int32) */, yyDebug_t212154387_il2cpp_TypeInfo_var, L_108, ((L_109)->GetAt(static_cast<il2cpp_array_size_t>(L_111))), ((L_112)->GetAt(static_cast<il2cpp_array_size_t>(L_114))), 3);
	}

IL_0298:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XPathParser_t618877717_il2cpp_TypeInfo_var);
		Int16U5BU5D_t3675865332* L_115 = ((XPathParser_t618877717_StaticFields*)XPathParser_t618877717_il2cpp_TypeInfo_var->static_fields)->get_yyTable_15();
		int32_t L_116 = V_9;
		NullCheck(L_115);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_115, L_116);
		int32_t L_117 = L_116;
		V_0 = ((L_115)->GetAt(static_cast<il2cpp_array_size_t>(L_117)));
		Il2CppObject * L_118 = ___yyLex0;
		NullCheck(L_118);
		Il2CppObject * L_119 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object Mono.Xml.XPath.yyParser.yyInput::value() */, yyInput_t1890241104_il2cpp_TypeInfo_var, L_118);
		V_2 = L_119;
		goto IL_0ea8;
	}

IL_02ad:
	{
		Il2CppObject * L_120 = __this->get_debug_3();
		if (!L_120)
		{
			goto IL_02c7;
		}
	}
	{
		Il2CppObject * L_121 = __this->get_debug_3();
		Int32U5BU5D_t1809983122* L_122 = V_1;
		int32_t L_123 = V_6;
		NullCheck(L_122);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_122, L_123);
		int32_t L_124 = L_123;
		NullCheck(L_121);
		InterfaceActionInvoker1< int32_t >::Invoke(3 /* System.Void Mono.Xml.XPath.yydebug.yyDebug::pop(System.Int32) */, yyDebug_t212154387_il2cpp_TypeInfo_var, L_121, ((L_122)->GetAt(static_cast<il2cpp_array_size_t>(L_124))));
	}

IL_02c7:
	{
		int32_t L_125 = V_6;
		int32_t L_126 = ((int32_t)((int32_t)L_125-(int32_t)1));
		V_6 = L_126;
		if ((((int32_t)L_126) >= ((int32_t)0)))
		{
			goto IL_0232;
		}
	}
	{
		Il2CppObject * L_127 = __this->get_debug_3();
		if (!L_127)
		{
			goto IL_02ea;
		}
	}
	{
		Il2CppObject * L_128 = __this->get_debug_3();
		NullCheck(L_128);
		InterfaceActionInvoker0::Invoke(9 /* System.Void Mono.Xml.XPath.yydebug.yyDebug::reject() */, yyDebug_t212154387_il2cpp_TypeInfo_var, L_128);
	}

IL_02ea:
	{
		yyException_t1136059093 * L_129 = (yyException_t1136059093 *)il2cpp_codegen_object_new(yyException_t1136059093_il2cpp_TypeInfo_var);
		yyException__ctor_m2976596948(L_129, _stringLiteral729072086, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_129);
	}

IL_02f5:
	{
		int32_t L_130 = V_4;
		if (L_130)
		{
			goto IL_031d;
		}
	}
	{
		Il2CppObject * L_131 = __this->get_debug_3();
		if (!L_131)
		{
			goto IL_0312;
		}
	}
	{
		Il2CppObject * L_132 = __this->get_debug_3();
		NullCheck(L_132);
		InterfaceActionInvoker0::Invoke(9 /* System.Void Mono.Xml.XPath.yydebug.yyDebug::reject() */, yyDebug_t212154387_il2cpp_TypeInfo_var, L_132);
	}

IL_0312:
	{
		yyException_t1136059093 * L_133 = (yyException_t1136059093 *)il2cpp_codegen_object_new(yyException_t1136059093_il2cpp_TypeInfo_var);
		yyException__ctor_m2976596948(L_133, _stringLiteral3824180189, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_133);
	}

IL_031d:
	{
		Il2CppObject * L_134 = __this->get_debug_3();
		if (!L_134)
		{
			goto IL_0343;
		}
	}
	{
		Il2CppObject * L_135 = __this->get_debug_3();
		int32_t L_136 = V_0;
		int32_t L_137 = V_4;
		int32_t L_138 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(XPathParser_t618877717_il2cpp_TypeInfo_var);
		String_t* L_139 = XPathParser_yyname_m3001501735(NULL /*static, unused*/, L_138, /*hidden argument*/NULL);
		Il2CppObject * L_140 = ___yyLex0;
		NullCheck(L_140);
		Il2CppObject * L_141 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object Mono.Xml.XPath.yyParser.yyInput::value() */, yyInput_t1890241104_il2cpp_TypeInfo_var, L_140);
		NullCheck(L_135);
		InterfaceActionInvoker4< int32_t, int32_t, String_t*, Il2CppObject * >::Invoke(4 /* System.Void Mono.Xml.XPath.yydebug.yyDebug::discard(System.Int32,System.Int32,System.String,System.Object) */, yyDebug_t212154387_il2cpp_TypeInfo_var, L_135, L_136, L_137, L_139, L_141);
	}

IL_0343:
	{
		V_4 = (-1);
		goto IL_0e9e;
	}

IL_034b:
	{
		int32_t L_142 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(XPathParser_t618877717_il2cpp_TypeInfo_var);
		Int16U5BU5D_t3675865332* L_143 = ((XPathParser_t618877717_StaticFields*)XPathParser_t618877717_il2cpp_TypeInfo_var->static_fields)->get_yyLen_9();
		int32_t L_144 = V_9;
		NullCheck(L_143);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_143, L_144);
		int32_t L_145 = L_144;
		V_10 = ((int32_t)((int32_t)((int32_t)((int32_t)L_142+(int32_t)1))-(int32_t)((L_143)->GetAt(static_cast<il2cpp_array_size_t>(L_145)))));
		Il2CppObject * L_146 = __this->get_debug_3();
		if (!L_146)
		{
			goto IL_0388;
		}
	}
	{
		Il2CppObject * L_147 = __this->get_debug_3();
		int32_t L_148 = V_0;
		Int32U5BU5D_t1809983122* L_149 = V_1;
		int32_t L_150 = V_10;
		NullCheck(L_149);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_149, ((int32_t)((int32_t)L_150-(int32_t)1)));
		int32_t L_151 = ((int32_t)((int32_t)L_150-(int32_t)1));
		int32_t L_152 = V_9;
		int32_t L_153 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(YYRules_t10764727_il2cpp_TypeInfo_var);
		String_t* L_154 = YYRules_getRule_m3263407986(NULL /*static, unused*/, L_153, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(XPathParser_t618877717_il2cpp_TypeInfo_var);
		Int16U5BU5D_t3675865332* L_155 = ((XPathParser_t618877717_StaticFields*)XPathParser_t618877717_il2cpp_TypeInfo_var->static_fields)->get_yyLen_9();
		int32_t L_156 = V_9;
		NullCheck(L_155);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_155, L_156);
		int32_t L_157 = L_156;
		NullCheck(L_147);
		InterfaceActionInvoker5< int32_t, int32_t, int32_t, String_t*, int32_t >::Invoke(5 /* System.Void Mono.Xml.XPath.yydebug.yyDebug::reduce(System.Int32,System.Int32,System.Int32,System.String,System.Int32) */, yyDebug_t212154387_il2cpp_TypeInfo_var, L_147, L_148, ((L_149)->GetAt(static_cast<il2cpp_array_size_t>(L_151))), L_152, L_154, ((L_155)->GetAt(static_cast<il2cpp_array_size_t>(L_157))));
	}

IL_0388:
	{
		int32_t L_158 = V_10;
		int32_t L_159 = V_6;
		G_B61_0 = __this;
		if ((((int32_t)L_158) <= ((int32_t)L_159)))
		{
			G_B62_0 = __this;
			goto IL_0398;
		}
	}
	{
		G_B63_0 = NULL;
		G_B63_1 = G_B61_0;
		goto IL_039c;
	}

IL_0398:
	{
		ObjectU5BU5D_t11523773* L_160 = V_3;
		int32_t L_161 = V_10;
		NullCheck(L_160);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_160, L_161);
		int32_t L_162 = L_161;
		G_B63_0 = ((L_160)->GetAt(static_cast<il2cpp_array_size_t>(L_162)));
		G_B63_1 = G_B62_0;
	}

IL_039c:
	{
		NullCheck(G_B63_1);
		Il2CppObject * L_163 = XPathParser_yyDefault_m772388494(G_B63_1, G_B63_0, /*hidden argument*/NULL);
		V_2 = L_163;
		int32_t L_164 = V_9;
		V_18 = L_164;
		int32_t L_165 = V_18;
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 0)
		{
			goto IL_0544;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 1)
		{
			goto IL_0566;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 2)
		{
			goto IL_0571;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 3)
		{
			goto IL_0d77;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 4)
		{
			goto IL_058c;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 5)
		{
			goto IL_05ae;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 6)
		{
			goto IL_05d0;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 7)
		{
			goto IL_0d77;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 8)
		{
			goto IL_05eb;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 9)
		{
			goto IL_0660;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 10)
		{
			goto IL_0d77;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 11)
		{
			goto IL_06eb;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 12)
		{
			goto IL_070d;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 13)
		{
			goto IL_072f;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 14)
		{
			goto IL_0d77;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 15)
		{
			goto IL_0758;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 16)
		{
			goto IL_0764;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 17)
		{
			goto IL_0770;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 18)
		{
			goto IL_0777;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 19)
		{
			goto IL_0d77;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 20)
		{
			goto IL_0d77;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 21)
		{
			goto IL_07ad;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 22)
		{
			goto IL_0d77;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 23)
		{
			goto IL_07cf;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 24)
		{
			goto IL_0d77;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 25)
		{
			goto IL_07f1;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 26)
		{
			goto IL_0813;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 27)
		{
			goto IL_0d77;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 28)
		{
			goto IL_0835;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 29)
		{
			goto IL_0857;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 30)
		{
			goto IL_0879;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 31)
		{
			goto IL_089b;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 32)
		{
			goto IL_0d77;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 33)
		{
			goto IL_08bd;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 34)
		{
			goto IL_08df;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 35)
		{
			goto IL_0d77;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 36)
		{
			goto IL_0901;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 37)
		{
			goto IL_0923;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 38)
		{
			goto IL_0945;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 39)
		{
			goto IL_0d77;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 40)
		{
			goto IL_0967;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 41)
		{
			goto IL_0d77;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 42)
		{
			goto IL_097d;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 43)
		{
			goto IL_0d77;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 44)
		{
			goto IL_0d77;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 45)
		{
			goto IL_099f;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 46)
		{
			goto IL_09c1;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 47)
		{
			goto IL_0d77;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 48)
		{
			goto IL_0d77;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 49)
		{
			goto IL_09e3;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 50)
		{
			goto IL_09ee;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 51)
		{
			goto IL_0a09;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 52)
		{
			goto IL_0d77;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 53)
		{
			goto IL_0a24;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 54)
		{
			goto IL_0a46;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 55)
		{
			goto IL_0a68;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 56)
		{
			goto IL_0d77;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 57)
		{
			goto IL_0d77;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 58)
		{
			goto IL_0a91;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 59)
		{
			goto IL_0aa8;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 60)
		{
			goto IL_0ab9;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 61)
		{
			goto IL_0d77;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 62)
		{
			goto IL_0ac4;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 63)
		{
			goto IL_0ad3;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 64)
		{
			goto IL_0ae2;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 65)
		{
			goto IL_0ae9;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 66)
		{
			goto IL_0b1a;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 67)
		{
			goto IL_0d77;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 68)
		{
			goto IL_0b26;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 69)
		{
			goto IL_0b32;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 70)
		{
			goto IL_0b3e;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 71)
		{
			goto IL_0b4a;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 72)
		{
			goto IL_0b56;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 73)
		{
			goto IL_0b62;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 74)
		{
			goto IL_0d77;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 75)
		{
			goto IL_0b6f;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 76)
		{
			goto IL_0b90;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 77)
		{
			goto IL_0be2;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 78)
		{
			goto IL_0bf8;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 79)
		{
			goto IL_0c0e;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 80)
		{
			goto IL_0d77;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 81)
		{
			goto IL_0c24;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 82)
		{
			goto IL_0d77;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 83)
		{
			goto IL_0c89;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 84)
		{
			goto IL_0d77;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 85)
		{
			goto IL_0caa;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 86)
		{
			goto IL_0ccb;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 87)
		{
			goto IL_0cd7;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 88)
		{
			goto IL_0ce3;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 89)
		{
			goto IL_0cef;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 90)
		{
			goto IL_0cfb;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 91)
		{
			goto IL_0d07;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 92)
		{
			goto IL_0d13;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 93)
		{
			goto IL_0d1f;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 94)
		{
			goto IL_0d2b;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 95)
		{
			goto IL_0d37;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 96)
		{
			goto IL_0d43;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 97)
		{
			goto IL_0d50;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 98)
		{
			goto IL_0d5d;
		}
		if (((int32_t)((int32_t)L_165-(int32_t)2)) == 99)
		{
			goto IL_0d6a;
		}
	}
	{
		goto IL_0d77;
	}

IL_0544:
	{
		ObjectU5BU5D_t11523773* L_166 = V_3;
		int32_t L_167 = V_6;
		NullCheck(L_166);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_166, ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_167)));
		int32_t L_168 = ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_167));
		ObjectU5BU5D_t11523773* L_169 = V_3;
		int32_t L_170 = V_6;
		NullCheck(L_169);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_169, ((int32_t)((int32_t)0+(int32_t)L_170)));
		int32_t L_171 = ((int32_t)((int32_t)0+(int32_t)L_170));
		ExprUNION_t2208364215 * L_172 = (ExprUNION_t2208364215 *)il2cpp_codegen_object_new(ExprUNION_t2208364215_il2cpp_TypeInfo_var);
		ExprUNION__ctor_m2912512622(L_172, ((NodeSet_t3503134685 *)CastclassClass(((L_166)->GetAt(static_cast<il2cpp_array_size_t>(L_168))), NodeSet_t3503134685_il2cpp_TypeInfo_var)), ((NodeSet_t3503134685 *)CastclassClass(((L_169)->GetAt(static_cast<il2cpp_array_size_t>(L_171))), NodeSet_t3503134685_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_2 = L_172;
		goto IL_0d77;
	}

IL_0566:
	{
		ExprRoot_t2159432084 * L_173 = (ExprRoot_t2159432084 *)il2cpp_codegen_object_new(ExprRoot_t2159432084_il2cpp_TypeInfo_var);
		ExprRoot__ctor_m3406596739(L_173, /*hidden argument*/NULL);
		V_2 = L_173;
		goto IL_0d77;
	}

IL_0571:
	{
		ExprRoot_t2159432084 * L_174 = (ExprRoot_t2159432084 *)il2cpp_codegen_object_new(ExprRoot_t2159432084_il2cpp_TypeInfo_var);
		ExprRoot__ctor_m3406596739(L_174, /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_175 = V_3;
		int32_t L_176 = V_6;
		NullCheck(L_175);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_175, ((int32_t)((int32_t)0+(int32_t)L_176)));
		int32_t L_177 = ((int32_t)((int32_t)0+(int32_t)L_176));
		ExprSLASH_t2206450021 * L_178 = (ExprSLASH_t2206450021 *)il2cpp_codegen_object_new(ExprSLASH_t2206450021_il2cpp_TypeInfo_var);
		ExprSLASH__ctor_m1380016254(L_178, L_174, ((NodeSet_t3503134685 *)CastclassClass(((L_175)->GetAt(static_cast<il2cpp_array_size_t>(L_177))), NodeSet_t3503134685_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_2 = L_178;
		goto IL_0d77;
	}

IL_058c:
	{
		ObjectU5BU5D_t11523773* L_179 = V_3;
		int32_t L_180 = V_6;
		NullCheck(L_179);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_179, ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_180)));
		int32_t L_181 = ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_180));
		ObjectU5BU5D_t11523773* L_182 = V_3;
		int32_t L_183 = V_6;
		NullCheck(L_182);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_182, ((int32_t)((int32_t)0+(int32_t)L_183)));
		int32_t L_184 = ((int32_t)((int32_t)0+(int32_t)L_183));
		ExprSLASH_t2206450021 * L_185 = (ExprSLASH_t2206450021 *)il2cpp_codegen_object_new(ExprSLASH_t2206450021_il2cpp_TypeInfo_var);
		ExprSLASH__ctor_m1380016254(L_185, ((Expression_t4217024437 *)CastclassClass(((L_179)->GetAt(static_cast<il2cpp_array_size_t>(L_181))), Expression_t4217024437_il2cpp_TypeInfo_var)), ((NodeSet_t3503134685 *)CastclassClass(((L_182)->GetAt(static_cast<il2cpp_array_size_t>(L_184))), NodeSet_t3503134685_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_2 = L_185;
		goto IL_0d77;
	}

IL_05ae:
	{
		ObjectU5BU5D_t11523773* L_186 = V_3;
		int32_t L_187 = V_6;
		NullCheck(L_186);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_186, ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_187)));
		int32_t L_188 = ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_187));
		ObjectU5BU5D_t11523773* L_189 = V_3;
		int32_t L_190 = V_6;
		NullCheck(L_189);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_189, ((int32_t)((int32_t)0+(int32_t)L_190)));
		int32_t L_191 = ((int32_t)((int32_t)0+(int32_t)L_190));
		ExprSLASH2_t3664170439 * L_192 = (ExprSLASH2_t3664170439 *)il2cpp_codegen_object_new(ExprSLASH2_t3664170439_il2cpp_TypeInfo_var);
		ExprSLASH2__ctor_m1370991914(L_192, ((Expression_t4217024437 *)CastclassClass(((L_186)->GetAt(static_cast<il2cpp_array_size_t>(L_188))), Expression_t4217024437_il2cpp_TypeInfo_var)), ((NodeSet_t3503134685 *)CastclassClass(((L_189)->GetAt(static_cast<il2cpp_array_size_t>(L_191))), NodeSet_t3503134685_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_2 = L_192;
		goto IL_0d77;
	}

IL_05d0:
	{
		ExprRoot_t2159432084 * L_193 = (ExprRoot_t2159432084 *)il2cpp_codegen_object_new(ExprRoot_t2159432084_il2cpp_TypeInfo_var);
		ExprRoot__ctor_m3406596739(L_193, /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_194 = V_3;
		int32_t L_195 = V_6;
		NullCheck(L_194);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_194, ((int32_t)((int32_t)0+(int32_t)L_195)));
		int32_t L_196 = ((int32_t)((int32_t)0+(int32_t)L_195));
		ExprSLASH2_t3664170439 * L_197 = (ExprSLASH2_t3664170439 *)il2cpp_codegen_object_new(ExprSLASH2_t3664170439_il2cpp_TypeInfo_var);
		ExprSLASH2__ctor_m1370991914(L_197, L_193, ((NodeSet_t3503134685 *)CastclassClass(((L_194)->GetAt(static_cast<il2cpp_array_size_t>(L_196))), NodeSet_t3503134685_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_2 = L_197;
		goto IL_0d77;
	}

IL_05eb:
	{
		ObjectU5BU5D_t11523773* L_198 = V_3;
		int32_t L_199 = V_6;
		NullCheck(L_198);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_198, ((int32_t)((int32_t)((int32_t)-3)+(int32_t)L_199)));
		int32_t L_200 = ((int32_t)((int32_t)((int32_t)-3)+(int32_t)L_199));
		V_11 = ((XmlQualifiedName_t176365656 *)CastclassClass(((L_198)->GetAt(static_cast<il2cpp_array_size_t>(L_200))), XmlQualifiedName_t176365656_il2cpp_TypeInfo_var));
		XmlQualifiedName_t176365656 * L_201 = V_11;
		NullCheck(L_201);
		String_t* L_202 = XmlQualifiedName_get_Name_m607016698(L_201, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_203 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_202, _stringLiteral3355, /*hidden argument*/NULL);
		if (L_203)
		{
			goto IL_0625;
		}
	}
	{
		XmlQualifiedName_t176365656 * L_204 = V_11;
		NullCheck(L_204);
		String_t* L_205 = XmlQualifiedName_get_Namespace_m2987642414(L_204, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_206 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_207 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_205, L_206, /*hidden argument*/NULL);
		if (!L_207)
		{
			goto IL_0637;
		}
	}

IL_0625:
	{
		XmlQualifiedName_t176365656 * L_208 = V_11;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_209 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral683753632, L_208, /*hidden argument*/NULL);
		XPathException_t2353341743 * L_210 = (XPathException_t2353341743 *)il2cpp_codegen_object_new(XPathException_t2353341743_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_210, L_209, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_210);
	}

IL_0637:
	{
		XmlQualifiedName_t176365656 * L_211 = V_11;
		ObjectU5BU5D_t11523773* L_212 = V_3;
		int32_t L_213 = V_6;
		NullCheck(L_212);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_212, ((int32_t)((int32_t)(-1)+(int32_t)L_213)));
		int32_t L_214 = ((int32_t)((int32_t)(-1)+(int32_t)L_213));
		ExprLiteral_t569227735 * L_215 = (ExprLiteral_t569227735 *)il2cpp_codegen_object_new(ExprLiteral_t569227735_il2cpp_TypeInfo_var);
		ExprLiteral__ctor_m1598703472(L_215, ((String_t*)CastclassSealed(((L_212)->GetAt(static_cast<il2cpp_array_size_t>(L_214))), String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		FunctionArguments_t3402201403 * L_216 = (FunctionArguments_t3402201403 *)il2cpp_codegen_object_new(FunctionArguments_t3402201403_il2cpp_TypeInfo_var);
		FunctionArguments__ctor_m3611561738(L_216, L_215, (FunctionArguments_t3402201403 *)NULL, /*hidden argument*/NULL);
		Il2CppObject * L_217 = __this->get_Context_0();
		Expression_t4217024437 * L_218 = ExprFunctionCall_Factory_m1613910802(NULL /*static, unused*/, L_211, L_216, L_217, /*hidden argument*/NULL);
		V_2 = L_218;
		goto IL_0d77;
	}

IL_0660:
	{
		ObjectU5BU5D_t11523773* L_219 = V_3;
		int32_t L_220 = V_6;
		NullCheck(L_219);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_219, ((int32_t)((int32_t)((int32_t)-5)+(int32_t)L_220)));
		int32_t L_221 = ((int32_t)((int32_t)((int32_t)-5)+(int32_t)L_220));
		V_12 = ((XmlQualifiedName_t176365656 *)CastclassClass(((L_219)->GetAt(static_cast<il2cpp_array_size_t>(L_221))), XmlQualifiedName_t176365656_il2cpp_TypeInfo_var));
		XmlQualifiedName_t176365656 * L_222 = V_12;
		NullCheck(L_222);
		String_t* L_223 = XmlQualifiedName_get_Name_m607016698(L_222, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_224 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_223, _stringLiteral106079, /*hidden argument*/NULL);
		if (L_224)
		{
			goto IL_069a;
		}
	}
	{
		XmlQualifiedName_t176365656 * L_225 = V_12;
		NullCheck(L_225);
		String_t* L_226 = XmlQualifiedName_get_Namespace_m2987642414(L_225, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_227 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_228 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_226, L_227, /*hidden argument*/NULL);
		if (!L_228)
		{
			goto IL_06ac;
		}
	}

IL_069a:
	{
		XmlQualifiedName_t176365656 * L_229 = V_12;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_230 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral1408589050, L_229, /*hidden argument*/NULL);
		XPathException_t2353341743 * L_231 = (XPathException_t2353341743 *)il2cpp_codegen_object_new(XPathException_t2353341743_il2cpp_TypeInfo_var);
		XPathException__ctor_m2838559738(L_231, L_230, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_231);
	}

IL_06ac:
	{
		Il2CppObject * L_232 = __this->get_Context_0();
		XmlQualifiedName_t176365656 * L_233 = V_12;
		ObjectU5BU5D_t11523773* L_234 = V_3;
		int32_t L_235 = V_6;
		NullCheck(L_234);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_234, ((int32_t)((int32_t)((int32_t)-3)+(int32_t)L_235)));
		int32_t L_236 = ((int32_t)((int32_t)((int32_t)-3)+(int32_t)L_235));
		ExprLiteral_t569227735 * L_237 = (ExprLiteral_t569227735 *)il2cpp_codegen_object_new(ExprLiteral_t569227735_il2cpp_TypeInfo_var);
		ExprLiteral__ctor_m1598703472(L_237, ((String_t*)CastclassSealed(((L_234)->GetAt(static_cast<il2cpp_array_size_t>(L_236))), String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_238 = V_3;
		int32_t L_239 = V_6;
		NullCheck(L_238);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_238, ((int32_t)((int32_t)(-1)+(int32_t)L_239)));
		int32_t L_240 = ((int32_t)((int32_t)(-1)+(int32_t)L_239));
		ExprLiteral_t569227735 * L_241 = (ExprLiteral_t569227735 *)il2cpp_codegen_object_new(ExprLiteral_t569227735_il2cpp_TypeInfo_var);
		ExprLiteral__ctor_m1598703472(L_241, ((String_t*)CastclassSealed(((L_238)->GetAt(static_cast<il2cpp_array_size_t>(L_240))), String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		FunctionArguments_t3402201403 * L_242 = (FunctionArguments_t3402201403 *)il2cpp_codegen_object_new(FunctionArguments_t3402201403_il2cpp_TypeInfo_var);
		FunctionArguments__ctor_m3611561738(L_242, L_241, (FunctionArguments_t3402201403 *)NULL, /*hidden argument*/NULL);
		FunctionArguments_t3402201403 * L_243 = (FunctionArguments_t3402201403 *)il2cpp_codegen_object_new(FunctionArguments_t3402201403_il2cpp_TypeInfo_var);
		FunctionArguments__ctor_m3611561738(L_243, L_237, L_242, /*hidden argument*/NULL);
		NullCheck(L_232);
		Expression_t4217024437 * L_244 = InterfaceFuncInvoker2< Expression_t4217024437 *, XmlQualifiedName_t176365656 *, FunctionArguments_t3402201403 * >::Invoke(1 /* System.Xml.XPath.Expression System.Xml.Xsl.IStaticXsltContext::TryGetFunction(System.Xml.XmlQualifiedName,System.Xml.XPath.FunctionArguments) */, IStaticXsltContext_t2347050862_il2cpp_TypeInfo_var, L_232, L_233, L_243);
		V_2 = L_244;
		goto IL_0d77;
	}

IL_06eb:
	{
		ObjectU5BU5D_t11523773* L_245 = V_3;
		int32_t L_246 = V_6;
		NullCheck(L_245);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_245, ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_246)));
		int32_t L_247 = ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_246));
		ObjectU5BU5D_t11523773* L_248 = V_3;
		int32_t L_249 = V_6;
		NullCheck(L_248);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_248, ((int32_t)((int32_t)0+(int32_t)L_249)));
		int32_t L_250 = ((int32_t)((int32_t)0+(int32_t)L_249));
		ExprSLASH_t2206450021 * L_251 = (ExprSLASH_t2206450021 *)il2cpp_codegen_object_new(ExprSLASH_t2206450021_il2cpp_TypeInfo_var);
		ExprSLASH__ctor_m1380016254(L_251, ((Expression_t4217024437 *)CastclassClass(((L_245)->GetAt(static_cast<il2cpp_array_size_t>(L_247))), Expression_t4217024437_il2cpp_TypeInfo_var)), ((NodeSet_t3503134685 *)CastclassClass(((L_248)->GetAt(static_cast<il2cpp_array_size_t>(L_250))), NodeSet_t3503134685_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_2 = L_251;
		goto IL_0d77;
	}

IL_070d:
	{
		ObjectU5BU5D_t11523773* L_252 = V_3;
		int32_t L_253 = V_6;
		NullCheck(L_252);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_252, ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_253)));
		int32_t L_254 = ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_253));
		ObjectU5BU5D_t11523773* L_255 = V_3;
		int32_t L_256 = V_6;
		NullCheck(L_255);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_255, ((int32_t)((int32_t)0+(int32_t)L_256)));
		int32_t L_257 = ((int32_t)((int32_t)0+(int32_t)L_256));
		ExprSLASH2_t3664170439 * L_258 = (ExprSLASH2_t3664170439 *)il2cpp_codegen_object_new(ExprSLASH2_t3664170439_il2cpp_TypeInfo_var);
		ExprSLASH2__ctor_m1370991914(L_258, ((Expression_t4217024437 *)CastclassClass(((L_252)->GetAt(static_cast<il2cpp_array_size_t>(L_254))), Expression_t4217024437_il2cpp_TypeInfo_var)), ((NodeSet_t3503134685 *)CastclassClass(((L_255)->GetAt(static_cast<il2cpp_array_size_t>(L_257))), NodeSet_t3503134685_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_2 = L_258;
		goto IL_0d77;
	}

IL_072f:
	{
		ObjectU5BU5D_t11523773* L_259 = V_3;
		int32_t L_260 = V_6;
		NullCheck(L_259);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_259, ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_260)));
		int32_t L_261 = ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_260));
		ObjectU5BU5D_t11523773* L_262 = V_3;
		int32_t L_263 = V_6;
		NullCheck(L_262);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_262, ((int32_t)((int32_t)(-1)+(int32_t)L_263)));
		int32_t L_264 = ((int32_t)((int32_t)(-1)+(int32_t)L_263));
		ObjectU5BU5D_t11523773* L_265 = V_3;
		int32_t L_266 = V_6;
		NullCheck(L_265);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_265, ((int32_t)((int32_t)0+(int32_t)L_266)));
		int32_t L_267 = ((int32_t)((int32_t)0+(int32_t)L_266));
		NodeSet_t3503134685 * L_268 = XPathParser_CreateNodeTest_m3475400515(__this, ((*(int32_t*)((int32_t*)UnBox (((L_259)->GetAt(static_cast<il2cpp_array_size_t>(L_261))), Int32_t2847414787_il2cpp_TypeInfo_var)))), ((L_262)->GetAt(static_cast<il2cpp_array_size_t>(L_264))), ((ArrayList_t2121638921 *)CastclassClass(((L_265)->GetAt(static_cast<il2cpp_array_size_t>(L_267))), ArrayList_t2121638921_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_2 = L_268;
		goto IL_0d77;
	}

IL_0758:
	{
		int32_t L_269 = ((int32_t)3);
		Il2CppObject * L_270 = Box(Axes_t4021066818_il2cpp_TypeInfo_var, &L_269);
		V_2 = L_270;
		goto IL_0d77;
	}

IL_0764:
	{
		int32_t L_271 = ((int32_t)2);
		Il2CppObject * L_272 = Box(Axes_t4021066818_il2cpp_TypeInfo_var, &L_271);
		V_2 = L_272;
		goto IL_0d77;
	}

IL_0770:
	{
		V_2 = NULL;
		goto IL_0d77;
	}

IL_0777:
	{
		ObjectU5BU5D_t11523773* L_273 = V_3;
		int32_t L_274 = V_6;
		NullCheck(L_273);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_273, ((int32_t)((int32_t)(-1)+(int32_t)L_274)));
		int32_t L_275 = ((int32_t)((int32_t)(-1)+(int32_t)L_274));
		V_13 = ((ArrayList_t2121638921 *)CastclassClass(((L_273)->GetAt(static_cast<il2cpp_array_size_t>(L_275))), ArrayList_t2121638921_il2cpp_TypeInfo_var));
		ArrayList_t2121638921 * L_276 = V_13;
		if (L_276)
		{
			goto IL_0792;
		}
	}
	{
		ArrayList_t2121638921 * L_277 = (ArrayList_t2121638921 *)il2cpp_codegen_object_new(ArrayList_t2121638921_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1878432947(L_277, /*hidden argument*/NULL);
		V_13 = L_277;
	}

IL_0792:
	{
		ArrayList_t2121638921 * L_278 = V_13;
		ObjectU5BU5D_t11523773* L_279 = V_3;
		int32_t L_280 = V_6;
		NullCheck(L_279);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_279, ((int32_t)((int32_t)0+(int32_t)L_280)));
		int32_t L_281 = ((int32_t)((int32_t)0+(int32_t)L_280));
		NullCheck(L_278);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_278, ((Expression_t4217024437 *)CastclassClass(((L_279)->GetAt(static_cast<il2cpp_array_size_t>(L_281))), Expression_t4217024437_il2cpp_TypeInfo_var)));
		ArrayList_t2121638921 * L_282 = V_13;
		V_2 = L_282;
		goto IL_0d77;
	}

IL_07ad:
	{
		ObjectU5BU5D_t11523773* L_283 = V_3;
		int32_t L_284 = V_6;
		NullCheck(L_283);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_283, ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_284)));
		int32_t L_285 = ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_284));
		ObjectU5BU5D_t11523773* L_286 = V_3;
		int32_t L_287 = V_6;
		NullCheck(L_286);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_286, ((int32_t)((int32_t)0+(int32_t)L_287)));
		int32_t L_288 = ((int32_t)((int32_t)0+(int32_t)L_287));
		ExprOR_t1813727157 * L_289 = (ExprOR_t1813727157 *)il2cpp_codegen_object_new(ExprOR_t1813727157_il2cpp_TypeInfo_var);
		ExprOR__ctor_m1067593566(L_289, ((Expression_t4217024437 *)CastclassClass(((L_283)->GetAt(static_cast<il2cpp_array_size_t>(L_285))), Expression_t4217024437_il2cpp_TypeInfo_var)), ((Expression_t4217024437 *)CastclassClass(((L_286)->GetAt(static_cast<il2cpp_array_size_t>(L_288))), Expression_t4217024437_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_2 = L_289;
		goto IL_0d77;
	}

IL_07cf:
	{
		ObjectU5BU5D_t11523773* L_290 = V_3;
		int32_t L_291 = V_6;
		NullCheck(L_290);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_290, ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_291)));
		int32_t L_292 = ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_291));
		ObjectU5BU5D_t11523773* L_293 = V_3;
		int32_t L_294 = V_6;
		NullCheck(L_293);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_293, ((int32_t)((int32_t)0+(int32_t)L_294)));
		int32_t L_295 = ((int32_t)((int32_t)0+(int32_t)L_294));
		ExprAND_t79682687 * L_296 = (ExprAND_t79682687 *)il2cpp_codegen_object_new(ExprAND_t79682687_il2cpp_TypeInfo_var);
		ExprAND__ctor_m2970784246(L_296, ((Expression_t4217024437 *)CastclassClass(((L_290)->GetAt(static_cast<il2cpp_array_size_t>(L_292))), Expression_t4217024437_il2cpp_TypeInfo_var)), ((Expression_t4217024437 *)CastclassClass(((L_293)->GetAt(static_cast<il2cpp_array_size_t>(L_295))), Expression_t4217024437_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_2 = L_296;
		goto IL_0d77;
	}

IL_07f1:
	{
		ObjectU5BU5D_t11523773* L_297 = V_3;
		int32_t L_298 = V_6;
		NullCheck(L_297);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_297, ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_298)));
		int32_t L_299 = ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_298));
		ObjectU5BU5D_t11523773* L_300 = V_3;
		int32_t L_301 = V_6;
		NullCheck(L_300);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_300, ((int32_t)((int32_t)0+(int32_t)L_301)));
		int32_t L_302 = ((int32_t)((int32_t)0+(int32_t)L_301));
		ExprEQ_t1813726846 * L_303 = (ExprEQ_t1813726846 *)il2cpp_codegen_object_new(ExprEQ_t1813726846_il2cpp_TypeInfo_var);
		ExprEQ__ctor_m3423036967(L_303, ((Expression_t4217024437 *)CastclassClass(((L_297)->GetAt(static_cast<il2cpp_array_size_t>(L_299))), Expression_t4217024437_il2cpp_TypeInfo_var)), ((Expression_t4217024437 *)CastclassClass(((L_300)->GetAt(static_cast<il2cpp_array_size_t>(L_302))), Expression_t4217024437_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_2 = L_303;
		goto IL_0d77;
	}

IL_0813:
	{
		ObjectU5BU5D_t11523773* L_304 = V_3;
		int32_t L_305 = V_6;
		NullCheck(L_304);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_304, ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_305)));
		int32_t L_306 = ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_305));
		ObjectU5BU5D_t11523773* L_307 = V_3;
		int32_t L_308 = V_6;
		NullCheck(L_307);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_307, ((int32_t)((int32_t)0+(int32_t)L_308)));
		int32_t L_309 = ((int32_t)((int32_t)0+(int32_t)L_308));
		ExprNE_t1813727113 * L_310 = (ExprNE_t1813727113 *)il2cpp_codegen_object_new(ExprNE_t1813727113_il2cpp_TypeInfo_var);
		ExprNE__ctor_m3803811634(L_310, ((Expression_t4217024437 *)CastclassClass(((L_304)->GetAt(static_cast<il2cpp_array_size_t>(L_306))), Expression_t4217024437_il2cpp_TypeInfo_var)), ((Expression_t4217024437 *)CastclassClass(((L_307)->GetAt(static_cast<il2cpp_array_size_t>(L_309))), Expression_t4217024437_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_2 = L_310;
		goto IL_0d77;
	}

IL_0835:
	{
		ObjectU5BU5D_t11523773* L_311 = V_3;
		int32_t L_312 = V_6;
		NullCheck(L_311);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_311, ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_312)));
		int32_t L_313 = ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_312));
		ObjectU5BU5D_t11523773* L_314 = V_3;
		int32_t L_315 = V_6;
		NullCheck(L_314);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_314, ((int32_t)((int32_t)0+(int32_t)L_315)));
		int32_t L_316 = ((int32_t)((int32_t)0+(int32_t)L_315));
		ExprLT_t1813727066 * L_317 = (ExprLT_t1813727066 *)il2cpp_codegen_object_new(ExprLT_t1813727066_il2cpp_TypeInfo_var);
		ExprLT__ctor_m2626848515(L_317, ((Expression_t4217024437 *)CastclassClass(((L_311)->GetAt(static_cast<il2cpp_array_size_t>(L_313))), Expression_t4217024437_il2cpp_TypeInfo_var)), ((Expression_t4217024437 *)CastclassClass(((L_314)->GetAt(static_cast<il2cpp_array_size_t>(L_316))), Expression_t4217024437_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_2 = L_317;
		goto IL_0d77;
	}

IL_0857:
	{
		ObjectU5BU5D_t11523773* L_318 = V_3;
		int32_t L_319 = V_6;
		NullCheck(L_318);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_318, ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_319)));
		int32_t L_320 = ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_319));
		ObjectU5BU5D_t11523773* L_321 = V_3;
		int32_t L_322 = V_6;
		NullCheck(L_321);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_321, ((int32_t)((int32_t)0+(int32_t)L_322)));
		int32_t L_323 = ((int32_t)((int32_t)0+(int32_t)L_322));
		ExprGT_t1813726911 * L_324 = (ExprGT_t1813726911 *)il2cpp_codegen_object_new(ExprGT_t1813726911_il2cpp_TypeInfo_var);
		ExprGT__ctor_m2309283432(L_324, ((Expression_t4217024437 *)CastclassClass(((L_318)->GetAt(static_cast<il2cpp_array_size_t>(L_320))), Expression_t4217024437_il2cpp_TypeInfo_var)), ((Expression_t4217024437 *)CastclassClass(((L_321)->GetAt(static_cast<il2cpp_array_size_t>(L_323))), Expression_t4217024437_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_2 = L_324;
		goto IL_0d77;
	}

IL_0879:
	{
		ObjectU5BU5D_t11523773* L_325 = V_3;
		int32_t L_326 = V_6;
		NullCheck(L_325);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_325, ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_326)));
		int32_t L_327 = ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_326));
		ObjectU5BU5D_t11523773* L_328 = V_3;
		int32_t L_329 = V_6;
		NullCheck(L_328);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_328, ((int32_t)((int32_t)0+(int32_t)L_329)));
		int32_t L_330 = ((int32_t)((int32_t)0+(int32_t)L_329));
		ExprLE_t1813727051 * L_331 = (ExprLE_t1813727051 *)il2cpp_codegen_object_new(ExprLE_t1813727051_il2cpp_TypeInfo_var);
		ExprLE__ctor_m240811764(L_331, ((Expression_t4217024437 *)CastclassClass(((L_325)->GetAt(static_cast<il2cpp_array_size_t>(L_327))), Expression_t4217024437_il2cpp_TypeInfo_var)), ((Expression_t4217024437 *)CastclassClass(((L_328)->GetAt(static_cast<il2cpp_array_size_t>(L_330))), Expression_t4217024437_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_2 = L_331;
		goto IL_0d77;
	}

IL_089b:
	{
		ObjectU5BU5D_t11523773* L_332 = V_3;
		int32_t L_333 = V_6;
		NullCheck(L_332);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_332, ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_333)));
		int32_t L_334 = ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_333));
		ObjectU5BU5D_t11523773* L_335 = V_3;
		int32_t L_336 = V_6;
		NullCheck(L_335);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_335, ((int32_t)((int32_t)0+(int32_t)L_336)));
		int32_t L_337 = ((int32_t)((int32_t)0+(int32_t)L_336));
		ExprGE_t1813726896 * L_338 = (ExprGE_t1813726896 *)il2cpp_codegen_object_new(ExprGE_t1813726896_il2cpp_TypeInfo_var);
		ExprGE__ctor_m4218213977(L_338, ((Expression_t4217024437 *)CastclassClass(((L_332)->GetAt(static_cast<il2cpp_array_size_t>(L_334))), Expression_t4217024437_il2cpp_TypeInfo_var)), ((Expression_t4217024437 *)CastclassClass(((L_335)->GetAt(static_cast<il2cpp_array_size_t>(L_337))), Expression_t4217024437_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_2 = L_338;
		goto IL_0d77;
	}

IL_08bd:
	{
		ObjectU5BU5D_t11523773* L_339 = V_3;
		int32_t L_340 = V_6;
		NullCheck(L_339);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_339, ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_340)));
		int32_t L_341 = ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_340));
		ObjectU5BU5D_t11523773* L_342 = V_3;
		int32_t L_343 = V_6;
		NullCheck(L_342);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_342, ((int32_t)((int32_t)0+(int32_t)L_343)));
		int32_t L_344 = ((int32_t)((int32_t)0+(int32_t)L_343));
		ExprPLUS_t2159338028 * L_345 = (ExprPLUS_t2159338028 *)il2cpp_codegen_object_new(ExprPLUS_t2159338028_il2cpp_TypeInfo_var);
		ExprPLUS__ctor_m693128085(L_345, ((Expression_t4217024437 *)CastclassClass(((L_339)->GetAt(static_cast<il2cpp_array_size_t>(L_341))), Expression_t4217024437_il2cpp_TypeInfo_var)), ((Expression_t4217024437 *)CastclassClass(((L_342)->GetAt(static_cast<il2cpp_array_size_t>(L_344))), Expression_t4217024437_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_2 = L_345;
		goto IL_0d77;
	}

IL_08df:
	{
		ObjectU5BU5D_t11523773* L_346 = V_3;
		int32_t L_347 = V_6;
		NullCheck(L_346);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_346, ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_347)));
		int32_t L_348 = ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_347));
		ObjectU5BU5D_t11523773* L_349 = V_3;
		int32_t L_350 = V_6;
		NullCheck(L_349);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_349, ((int32_t)((int32_t)0+(int32_t)L_350)));
		int32_t L_351 = ((int32_t)((int32_t)0+(int32_t)L_350));
		ExprMINUS_t2200832088 * L_352 = (ExprMINUS_t2200832088 *)il2cpp_codegen_object_new(ExprMINUS_t2200832088_il2cpp_TypeInfo_var);
		ExprMINUS__ctor_m1636365327(L_352, ((Expression_t4217024437 *)CastclassClass(((L_346)->GetAt(static_cast<il2cpp_array_size_t>(L_348))), Expression_t4217024437_il2cpp_TypeInfo_var)), ((Expression_t4217024437 *)CastclassClass(((L_349)->GetAt(static_cast<il2cpp_array_size_t>(L_351))), Expression_t4217024437_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_2 = L_352;
		goto IL_0d77;
	}

IL_0901:
	{
		ObjectU5BU5D_t11523773* L_353 = V_3;
		int32_t L_354 = V_6;
		NullCheck(L_353);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_353, ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_354)));
		int32_t L_355 = ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_354));
		ObjectU5BU5D_t11523773* L_356 = V_3;
		int32_t L_357 = V_6;
		NullCheck(L_356);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_356, ((int32_t)((int32_t)0+(int32_t)L_357)));
		int32_t L_358 = ((int32_t)((int32_t)0+(int32_t)L_357));
		ExprMULT_t2159257026 * L_359 = (ExprMULT_t2159257026 *)il2cpp_codegen_object_new(ExprMULT_t2159257026_il2cpp_TypeInfo_var);
		ExprMULT__ctor_m2682740523(L_359, ((Expression_t4217024437 *)CastclassClass(((L_353)->GetAt(static_cast<il2cpp_array_size_t>(L_355))), Expression_t4217024437_il2cpp_TypeInfo_var)), ((Expression_t4217024437 *)CastclassClass(((L_356)->GetAt(static_cast<il2cpp_array_size_t>(L_358))), Expression_t4217024437_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_2 = L_359;
		goto IL_0d77;
	}

IL_0923:
	{
		ObjectU5BU5D_t11523773* L_360 = V_3;
		int32_t L_361 = V_6;
		NullCheck(L_360);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_360, ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_361)));
		int32_t L_362 = ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_361));
		ObjectU5BU5D_t11523773* L_363 = V_3;
		int32_t L_364 = V_6;
		NullCheck(L_363);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_363, ((int32_t)((int32_t)0+(int32_t)L_364)));
		int32_t L_365 = ((int32_t)((int32_t)0+(int32_t)L_364));
		ExprDIV_t79685433 * L_366 = (ExprDIV_t79685433 *)il2cpp_codegen_object_new(ExprDIV_t79685433_il2cpp_TypeInfo_var);
		ExprDIV__ctor_m2833239216(L_366, ((Expression_t4217024437 *)CastclassClass(((L_360)->GetAt(static_cast<il2cpp_array_size_t>(L_362))), Expression_t4217024437_il2cpp_TypeInfo_var)), ((Expression_t4217024437 *)CastclassClass(((L_363)->GetAt(static_cast<il2cpp_array_size_t>(L_365))), Expression_t4217024437_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_2 = L_366;
		goto IL_0d77;
	}

IL_0945:
	{
		ObjectU5BU5D_t11523773* L_367 = V_3;
		int32_t L_368 = V_6;
		NullCheck(L_367);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_367, ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_368)));
		int32_t L_369 = ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_368));
		ObjectU5BU5D_t11523773* L_370 = V_3;
		int32_t L_371 = V_6;
		NullCheck(L_370);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_370, ((int32_t)((int32_t)0+(int32_t)L_371)));
		int32_t L_372 = ((int32_t)((int32_t)0+(int32_t)L_371));
		ExprMOD_t79694250 * L_373 = (ExprMOD_t79694250 *)il2cpp_codegen_object_new(ExprMOD_t79694250_il2cpp_TypeInfo_var);
		ExprMOD__ctor_m1750329121(L_373, ((Expression_t4217024437 *)CastclassClass(((L_367)->GetAt(static_cast<il2cpp_array_size_t>(L_369))), Expression_t4217024437_il2cpp_TypeInfo_var)), ((Expression_t4217024437 *)CastclassClass(((L_370)->GetAt(static_cast<il2cpp_array_size_t>(L_372))), Expression_t4217024437_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_2 = L_373;
		goto IL_0d77;
	}

IL_0967:
	{
		ObjectU5BU5D_t11523773* L_374 = V_3;
		int32_t L_375 = V_6;
		NullCheck(L_374);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_374, ((int32_t)((int32_t)0+(int32_t)L_375)));
		int32_t L_376 = ((int32_t)((int32_t)0+(int32_t)L_375));
		ExprNEG_t79694904 * L_377 = (ExprNEG_t79694904 *)il2cpp_codegen_object_new(ExprNEG_t79694904_il2cpp_TypeInfo_var);
		ExprNEG__ctor_m3726946082(L_377, ((Expression_t4217024437 *)CastclassClass(((L_374)->GetAt(static_cast<il2cpp_array_size_t>(L_376))), Expression_t4217024437_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_2 = L_377;
		goto IL_0d77;
	}

IL_097d:
	{
		ObjectU5BU5D_t11523773* L_378 = V_3;
		int32_t L_379 = V_6;
		NullCheck(L_378);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_378, ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_379)));
		int32_t L_380 = ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_379));
		ObjectU5BU5D_t11523773* L_381 = V_3;
		int32_t L_382 = V_6;
		NullCheck(L_381);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_381, ((int32_t)((int32_t)0+(int32_t)L_382)));
		int32_t L_383 = ((int32_t)((int32_t)0+(int32_t)L_382));
		ExprUNION_t2208364215 * L_384 = (ExprUNION_t2208364215 *)il2cpp_codegen_object_new(ExprUNION_t2208364215_il2cpp_TypeInfo_var);
		ExprUNION__ctor_m2912512622(L_384, ((Expression_t4217024437 *)CastclassClass(((L_378)->GetAt(static_cast<il2cpp_array_size_t>(L_380))), Expression_t4217024437_il2cpp_TypeInfo_var)), ((Expression_t4217024437 *)CastclassClass(((L_381)->GetAt(static_cast<il2cpp_array_size_t>(L_383))), Expression_t4217024437_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_2 = L_384;
		goto IL_0d77;
	}

IL_099f:
	{
		ObjectU5BU5D_t11523773* L_385 = V_3;
		int32_t L_386 = V_6;
		NullCheck(L_385);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_385, ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_386)));
		int32_t L_387 = ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_386));
		ObjectU5BU5D_t11523773* L_388 = V_3;
		int32_t L_389 = V_6;
		NullCheck(L_388);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_388, ((int32_t)((int32_t)0+(int32_t)L_389)));
		int32_t L_390 = ((int32_t)((int32_t)0+(int32_t)L_389));
		ExprSLASH_t2206450021 * L_391 = (ExprSLASH_t2206450021 *)il2cpp_codegen_object_new(ExprSLASH_t2206450021_il2cpp_TypeInfo_var);
		ExprSLASH__ctor_m1380016254(L_391, ((Expression_t4217024437 *)CastclassClass(((L_385)->GetAt(static_cast<il2cpp_array_size_t>(L_387))), Expression_t4217024437_il2cpp_TypeInfo_var)), ((NodeSet_t3503134685 *)CastclassClass(((L_388)->GetAt(static_cast<il2cpp_array_size_t>(L_390))), NodeSet_t3503134685_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_2 = L_391;
		goto IL_0d77;
	}

IL_09c1:
	{
		ObjectU5BU5D_t11523773* L_392 = V_3;
		int32_t L_393 = V_6;
		NullCheck(L_392);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_392, ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_393)));
		int32_t L_394 = ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_393));
		ObjectU5BU5D_t11523773* L_395 = V_3;
		int32_t L_396 = V_6;
		NullCheck(L_395);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_395, ((int32_t)((int32_t)0+(int32_t)L_396)));
		int32_t L_397 = ((int32_t)((int32_t)0+(int32_t)L_396));
		ExprSLASH2_t3664170439 * L_398 = (ExprSLASH2_t3664170439 *)il2cpp_codegen_object_new(ExprSLASH2_t3664170439_il2cpp_TypeInfo_var);
		ExprSLASH2__ctor_m1370991914(L_398, ((Expression_t4217024437 *)CastclassClass(((L_392)->GetAt(static_cast<il2cpp_array_size_t>(L_394))), Expression_t4217024437_il2cpp_TypeInfo_var)), ((NodeSet_t3503134685 *)CastclassClass(((L_395)->GetAt(static_cast<il2cpp_array_size_t>(L_397))), NodeSet_t3503134685_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_2 = L_398;
		goto IL_0d77;
	}

IL_09e3:
	{
		ExprRoot_t2159432084 * L_399 = (ExprRoot_t2159432084 *)il2cpp_codegen_object_new(ExprRoot_t2159432084_il2cpp_TypeInfo_var);
		ExprRoot__ctor_m3406596739(L_399, /*hidden argument*/NULL);
		V_2 = L_399;
		goto IL_0d77;
	}

IL_09ee:
	{
		ExprRoot_t2159432084 * L_400 = (ExprRoot_t2159432084 *)il2cpp_codegen_object_new(ExprRoot_t2159432084_il2cpp_TypeInfo_var);
		ExprRoot__ctor_m3406596739(L_400, /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_401 = V_3;
		int32_t L_402 = V_6;
		NullCheck(L_401);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_401, ((int32_t)((int32_t)0+(int32_t)L_402)));
		int32_t L_403 = ((int32_t)((int32_t)0+(int32_t)L_402));
		ExprSLASH_t2206450021 * L_404 = (ExprSLASH_t2206450021 *)il2cpp_codegen_object_new(ExprSLASH_t2206450021_il2cpp_TypeInfo_var);
		ExprSLASH__ctor_m1380016254(L_404, L_400, ((NodeSet_t3503134685 *)CastclassClass(((L_401)->GetAt(static_cast<il2cpp_array_size_t>(L_403))), NodeSet_t3503134685_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_2 = L_404;
		goto IL_0d77;
	}

IL_0a09:
	{
		ExprRoot_t2159432084 * L_405 = (ExprRoot_t2159432084 *)il2cpp_codegen_object_new(ExprRoot_t2159432084_il2cpp_TypeInfo_var);
		ExprRoot__ctor_m3406596739(L_405, /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_406 = V_3;
		int32_t L_407 = V_6;
		NullCheck(L_406);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_406, ((int32_t)((int32_t)0+(int32_t)L_407)));
		int32_t L_408 = ((int32_t)((int32_t)0+(int32_t)L_407));
		ExprSLASH2_t3664170439 * L_409 = (ExprSLASH2_t3664170439 *)il2cpp_codegen_object_new(ExprSLASH2_t3664170439_il2cpp_TypeInfo_var);
		ExprSLASH2__ctor_m1370991914(L_409, L_405, ((NodeSet_t3503134685 *)CastclassClass(((L_406)->GetAt(static_cast<il2cpp_array_size_t>(L_408))), NodeSet_t3503134685_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_2 = L_409;
		goto IL_0d77;
	}

IL_0a24:
	{
		ObjectU5BU5D_t11523773* L_410 = V_3;
		int32_t L_411 = V_6;
		NullCheck(L_410);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_410, ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_411)));
		int32_t L_412 = ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_411));
		ObjectU5BU5D_t11523773* L_413 = V_3;
		int32_t L_414 = V_6;
		NullCheck(L_413);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_413, ((int32_t)((int32_t)0+(int32_t)L_414)));
		int32_t L_415 = ((int32_t)((int32_t)0+(int32_t)L_414));
		ExprSLASH_t2206450021 * L_416 = (ExprSLASH_t2206450021 *)il2cpp_codegen_object_new(ExprSLASH_t2206450021_il2cpp_TypeInfo_var);
		ExprSLASH__ctor_m1380016254(L_416, ((NodeSet_t3503134685 *)CastclassClass(((L_410)->GetAt(static_cast<il2cpp_array_size_t>(L_412))), NodeSet_t3503134685_il2cpp_TypeInfo_var)), ((NodeSet_t3503134685 *)CastclassClass(((L_413)->GetAt(static_cast<il2cpp_array_size_t>(L_415))), NodeSet_t3503134685_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_2 = L_416;
		goto IL_0d77;
	}

IL_0a46:
	{
		ObjectU5BU5D_t11523773* L_417 = V_3;
		int32_t L_418 = V_6;
		NullCheck(L_417);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_417, ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_418)));
		int32_t L_419 = ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_418));
		ObjectU5BU5D_t11523773* L_420 = V_3;
		int32_t L_421 = V_6;
		NullCheck(L_420);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_420, ((int32_t)((int32_t)0+(int32_t)L_421)));
		int32_t L_422 = ((int32_t)((int32_t)0+(int32_t)L_421));
		ExprSLASH2_t3664170439 * L_423 = (ExprSLASH2_t3664170439 *)il2cpp_codegen_object_new(ExprSLASH2_t3664170439_il2cpp_TypeInfo_var);
		ExprSLASH2__ctor_m1370991914(L_423, ((NodeSet_t3503134685 *)CastclassClass(((L_417)->GetAt(static_cast<il2cpp_array_size_t>(L_419))), NodeSet_t3503134685_il2cpp_TypeInfo_var)), ((NodeSet_t3503134685 *)CastclassClass(((L_420)->GetAt(static_cast<il2cpp_array_size_t>(L_422))), NodeSet_t3503134685_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_2 = L_423;
		goto IL_0d77;
	}

IL_0a68:
	{
		ObjectU5BU5D_t11523773* L_424 = V_3;
		int32_t L_425 = V_6;
		NullCheck(L_424);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_424, ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_425)));
		int32_t L_426 = ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_425));
		ObjectU5BU5D_t11523773* L_427 = V_3;
		int32_t L_428 = V_6;
		NullCheck(L_427);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_427, ((int32_t)((int32_t)(-1)+(int32_t)L_428)));
		int32_t L_429 = ((int32_t)((int32_t)(-1)+(int32_t)L_428));
		ObjectU5BU5D_t11523773* L_430 = V_3;
		int32_t L_431 = V_6;
		NullCheck(L_430);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_430, ((int32_t)((int32_t)0+(int32_t)L_431)));
		int32_t L_432 = ((int32_t)((int32_t)0+(int32_t)L_431));
		NodeSet_t3503134685 * L_433 = XPathParser_CreateNodeTest_m3475400515(__this, ((*(int32_t*)((int32_t*)UnBox (((L_424)->GetAt(static_cast<il2cpp_array_size_t>(L_426))), Int32_t2847414787_il2cpp_TypeInfo_var)))), ((L_427)->GetAt(static_cast<il2cpp_array_size_t>(L_429))), ((ArrayList_t2121638921 *)CastclassClass(((L_430)->GetAt(static_cast<il2cpp_array_size_t>(L_432))), ArrayList_t2121638921_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_2 = L_433;
		goto IL_0d77;
	}

IL_0a91:
	{
		ObjectU5BU5D_t11523773* L_434 = V_3;
		int32_t L_435 = V_6;
		NullCheck(L_434);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_434, ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_435)));
		int32_t L_436 = ((int32_t)((int32_t)((int32_t)-2)+(int32_t)L_435));
		int32_t L_437 = ((int32_t)((*(int32_t*)((int32_t*)UnBox (((L_434)->GetAt(static_cast<il2cpp_array_size_t>(L_436))), Int32_t2847414787_il2cpp_TypeInfo_var)))));
		Il2CppObject * L_438 = Box(XPathNodeType_t4070737174_il2cpp_TypeInfo_var, &L_437);
		V_2 = L_438;
		goto IL_0d77;
	}

IL_0aa8:
	{
		ObjectU5BU5D_t11523773* L_439 = V_3;
		int32_t L_440 = V_6;
		NullCheck(L_439);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_439, ((int32_t)((int32_t)(-1)+(int32_t)L_440)));
		int32_t L_441 = ((int32_t)((int32_t)(-1)+(int32_t)L_440));
		V_2 = ((String_t*)CastclassSealed(((L_439)->GetAt(static_cast<il2cpp_array_size_t>(L_441))), String_t_il2cpp_TypeInfo_var));
		goto IL_0d77;
	}

IL_0ab9:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XmlQualifiedName_t176365656_il2cpp_TypeInfo_var);
		XmlQualifiedName_t176365656 * L_442 = ((XmlQualifiedName_t176365656_StaticFields*)XmlQualifiedName_t176365656_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		V_2 = L_442;
		goto IL_0d77;
	}

IL_0ac4:
	{
		NodeTypeTest_t2651955243 * L_443 = (NodeTypeTest_t2651955243 *)il2cpp_codegen_object_new(NodeTypeTest_t2651955243_il2cpp_TypeInfo_var);
		NodeTypeTest__ctor_m2014120890(L_443, ((int32_t)12), ((int32_t)9), /*hidden argument*/NULL);
		V_2 = L_443;
		goto IL_0d77;
	}

IL_0ad3:
	{
		NodeTypeTest_t2651955243 * L_444 = (NodeTypeTest_t2651955243 *)il2cpp_codegen_object_new(NodeTypeTest_t2651955243_il2cpp_TypeInfo_var);
		NodeTypeTest__ctor_m2014120890(L_444, ((int32_t)9), ((int32_t)9), /*hidden argument*/NULL);
		V_2 = L_444;
		goto IL_0d77;
	}

IL_0ae2:
	{
		V_2 = NULL;
		goto IL_0d77;
	}

IL_0ae9:
	{
		ObjectU5BU5D_t11523773* L_445 = V_3;
		int32_t L_446 = V_6;
		NullCheck(L_445);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_445, ((int32_t)((int32_t)(-1)+(int32_t)L_446)));
		int32_t L_447 = ((int32_t)((int32_t)(-1)+(int32_t)L_446));
		V_14 = ((ArrayList_t2121638921 *)CastclassClass(((L_445)->GetAt(static_cast<il2cpp_array_size_t>(L_447))), ArrayList_t2121638921_il2cpp_TypeInfo_var));
		ArrayList_t2121638921 * L_448 = V_14;
		if (L_448)
		{
			goto IL_0b04;
		}
	}
	{
		ArrayList_t2121638921 * L_449 = (ArrayList_t2121638921 *)il2cpp_codegen_object_new(ArrayList_t2121638921_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1878432947(L_449, /*hidden argument*/NULL);
		V_14 = L_449;
	}

IL_0b04:
	{
		ArrayList_t2121638921 * L_450 = V_14;
		ObjectU5BU5D_t11523773* L_451 = V_3;
		int32_t L_452 = V_6;
		NullCheck(L_451);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_451, ((int32_t)((int32_t)0+(int32_t)L_452)));
		int32_t L_453 = ((int32_t)((int32_t)0+(int32_t)L_452));
		NullCheck(L_450);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_450, ((L_451)->GetAt(static_cast<il2cpp_array_size_t>(L_453))));
		ArrayList_t2121638921 * L_454 = V_14;
		V_2 = L_454;
		goto IL_0d77;
	}

IL_0b1a:
	{
		ObjectU5BU5D_t11523773* L_455 = V_3;
		int32_t L_456 = V_6;
		NullCheck(L_455);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_455, ((int32_t)((int32_t)(-1)+(int32_t)L_456)));
		int32_t L_457 = ((int32_t)((int32_t)(-1)+(int32_t)L_456));
		V_2 = ((L_455)->GetAt(static_cast<il2cpp_array_size_t>(L_457)));
		goto IL_0d77;
	}

IL_0b26:
	{
		int32_t L_458 = ((int32_t)3);
		Il2CppObject * L_459 = Box(Axes_t4021066818_il2cpp_TypeInfo_var, &L_458);
		V_2 = L_459;
		goto IL_0d77;
	}

IL_0b32:
	{
		int32_t L_460 = ((int32_t)2);
		Il2CppObject * L_461 = Box(Axes_t4021066818_il2cpp_TypeInfo_var, &L_460);
		V_2 = L_461;
		goto IL_0d77;
	}

IL_0b3e:
	{
		int32_t L_462 = ((int32_t)8);
		Il2CppObject * L_463 = Box(XPathNodeType_t4070737174_il2cpp_TypeInfo_var, &L_462);
		V_2 = L_463;
		goto IL_0d77;
	}

IL_0b4a:
	{
		int32_t L_464 = ((int32_t)4);
		Il2CppObject * L_465 = Box(XPathNodeType_t4070737174_il2cpp_TypeInfo_var, &L_464);
		V_2 = L_465;
		goto IL_0d77;
	}

IL_0b56:
	{
		int32_t L_466 = ((int32_t)7);
		Il2CppObject * L_467 = Box(XPathNodeType_t4070737174_il2cpp_TypeInfo_var, &L_466);
		V_2 = L_467;
		goto IL_0d77;
	}

IL_0b62:
	{
		int32_t L_468 = ((int32_t)((int32_t)9));
		Il2CppObject * L_469 = Box(XPathNodeType_t4070737174_il2cpp_TypeInfo_var, &L_468);
		V_2 = L_469;
		goto IL_0d77;
	}

IL_0b6f:
	{
		ObjectU5BU5D_t11523773* L_470 = V_3;
		int32_t L_471 = V_6;
		NullCheck(L_470);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_470, ((int32_t)((int32_t)(-1)+(int32_t)L_471)));
		int32_t L_472 = ((int32_t)((int32_t)(-1)+(int32_t)L_471));
		ObjectU5BU5D_t11523773* L_473 = V_3;
		int32_t L_474 = V_6;
		NullCheck(L_473);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_473, ((int32_t)((int32_t)0+(int32_t)L_474)));
		int32_t L_475 = ((int32_t)((int32_t)0+(int32_t)L_474));
		ExprFilter_t3320087274 * L_476 = (ExprFilter_t3320087274 *)il2cpp_codegen_object_new(ExprFilter_t3320087274_il2cpp_TypeInfo_var);
		ExprFilter__ctor_m1987528211(L_476, ((Expression_t4217024437 *)CastclassClass(((L_470)->GetAt(static_cast<il2cpp_array_size_t>(L_472))), Expression_t4217024437_il2cpp_TypeInfo_var)), ((Expression_t4217024437 *)CastclassClass(((L_473)->GetAt(static_cast<il2cpp_array_size_t>(L_475))), Expression_t4217024437_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_2 = L_476;
		goto IL_0d77;
	}

IL_0b90:
	{
		V_15 = (Expression_t4217024437 *)NULL;
		Il2CppObject * L_477 = __this->get_Context_0();
		if (!L_477)
		{
			goto IL_0bbb;
		}
	}
	{
		Il2CppObject * L_478 = __this->get_Context_0();
		ObjectU5BU5D_t11523773* L_479 = V_3;
		int32_t L_480 = V_6;
		NullCheck(L_479);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_479, ((int32_t)((int32_t)0+(int32_t)L_480)));
		int32_t L_481 = ((int32_t)((int32_t)0+(int32_t)L_480));
		NullCheck(((XmlQualifiedName_t176365656 *)CastclassClass(((L_479)->GetAt(static_cast<il2cpp_array_size_t>(L_481))), XmlQualifiedName_t176365656_il2cpp_TypeInfo_var)));
		String_t* L_482 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Xml.XmlQualifiedName::ToString() */, ((XmlQualifiedName_t176365656 *)CastclassClass(((L_479)->GetAt(static_cast<il2cpp_array_size_t>(L_481))), XmlQualifiedName_t176365656_il2cpp_TypeInfo_var)));
		NullCheck(L_478);
		Expression_t4217024437 * L_483 = InterfaceFuncInvoker1< Expression_t4217024437 *, String_t* >::Invoke(0 /* System.Xml.XPath.Expression System.Xml.Xsl.IStaticXsltContext::TryGetVariable(System.String) */, IStaticXsltContext_t2347050862_il2cpp_TypeInfo_var, L_478, L_482);
		V_15 = L_483;
	}

IL_0bbb:
	{
		Expression_t4217024437 * L_484 = V_15;
		if (L_484)
		{
			goto IL_0bda;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_485 = V_3;
		int32_t L_486 = V_6;
		NullCheck(L_485);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_485, ((int32_t)((int32_t)0+(int32_t)L_486)));
		int32_t L_487 = ((int32_t)((int32_t)0+(int32_t)L_486));
		Il2CppObject * L_488 = __this->get_Context_0();
		ExprVariable_t1838989486 * L_489 = (ExprVariable_t1838989486 *)il2cpp_codegen_object_new(ExprVariable_t1838989486_il2cpp_TypeInfo_var);
		ExprVariable__ctor_m2228197543(L_489, ((XmlQualifiedName_t176365656 *)CastclassClass(((L_485)->GetAt(static_cast<il2cpp_array_size_t>(L_487))), XmlQualifiedName_t176365656_il2cpp_TypeInfo_var)), L_488, /*hidden argument*/NULL);
		V_15 = L_489;
	}

IL_0bda:
	{
		Expression_t4217024437 * L_490 = V_15;
		V_2 = L_490;
		goto IL_0d77;
	}

IL_0be2:
	{
		ObjectU5BU5D_t11523773* L_491 = V_3;
		int32_t L_492 = V_6;
		NullCheck(L_491);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_491, ((int32_t)((int32_t)(-1)+(int32_t)L_492)));
		int32_t L_493 = ((int32_t)((int32_t)(-1)+(int32_t)L_492));
		ExprParens_t3599155227 * L_494 = (ExprParens_t3599155227 *)il2cpp_codegen_object_new(ExprParens_t3599155227_il2cpp_TypeInfo_var);
		ExprParens__ctor_m2196271671(L_494, ((Expression_t4217024437 *)CastclassClass(((L_491)->GetAt(static_cast<il2cpp_array_size_t>(L_493))), Expression_t4217024437_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_2 = L_494;
		goto IL_0d77;
	}

IL_0bf8:
	{
		ObjectU5BU5D_t11523773* L_495 = V_3;
		int32_t L_496 = V_6;
		NullCheck(L_495);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_495, ((int32_t)((int32_t)0+(int32_t)L_496)));
		int32_t L_497 = ((int32_t)((int32_t)0+(int32_t)L_496));
		ExprLiteral_t569227735 * L_498 = (ExprLiteral_t569227735 *)il2cpp_codegen_object_new(ExprLiteral_t569227735_il2cpp_TypeInfo_var);
		ExprLiteral__ctor_m1598703472(L_498, ((String_t*)CastclassSealed(((L_495)->GetAt(static_cast<il2cpp_array_size_t>(L_497))), String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_2 = L_498;
		goto IL_0d77;
	}

IL_0c0e:
	{
		ObjectU5BU5D_t11523773* L_499 = V_3;
		int32_t L_500 = V_6;
		NullCheck(L_499);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_499, ((int32_t)((int32_t)0+(int32_t)L_500)));
		int32_t L_501 = ((int32_t)((int32_t)0+(int32_t)L_500));
		ExprNumber_t3560215227 * L_502 = (ExprNumber_t3560215227 *)il2cpp_codegen_object_new(ExprNumber_t3560215227_il2cpp_TypeInfo_var);
		ExprNumber__ctor_m2500538694(L_502, ((*(double*)((double*)UnBox (((L_499)->GetAt(static_cast<il2cpp_array_size_t>(L_501))), Double_t534516614_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		V_2 = L_502;
		goto IL_0d77;
	}

IL_0c24:
	{
		V_16 = (Expression_t4217024437 *)NULL;
		Il2CppObject * L_503 = __this->get_Context_0();
		if (!L_503)
		{
			goto IL_0c56;
		}
	}
	{
		Il2CppObject * L_504 = __this->get_Context_0();
		ObjectU5BU5D_t11523773* L_505 = V_3;
		int32_t L_506 = V_6;
		NullCheck(L_505);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_505, ((int32_t)((int32_t)((int32_t)-3)+(int32_t)L_506)));
		int32_t L_507 = ((int32_t)((int32_t)((int32_t)-3)+(int32_t)L_506));
		ObjectU5BU5D_t11523773* L_508 = V_3;
		int32_t L_509 = V_6;
		NullCheck(L_508);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_508, ((int32_t)((int32_t)(-1)+(int32_t)L_509)));
		int32_t L_510 = ((int32_t)((int32_t)(-1)+(int32_t)L_509));
		NullCheck(L_504);
		Expression_t4217024437 * L_511 = InterfaceFuncInvoker2< Expression_t4217024437 *, XmlQualifiedName_t176365656 *, FunctionArguments_t3402201403 * >::Invoke(1 /* System.Xml.XPath.Expression System.Xml.Xsl.IStaticXsltContext::TryGetFunction(System.Xml.XmlQualifiedName,System.Xml.XPath.FunctionArguments) */, IStaticXsltContext_t2347050862_il2cpp_TypeInfo_var, L_504, ((XmlQualifiedName_t176365656 *)CastclassClass(((L_505)->GetAt(static_cast<il2cpp_array_size_t>(L_507))), XmlQualifiedName_t176365656_il2cpp_TypeInfo_var)), ((FunctionArguments_t3402201403 *)CastclassClass(((L_508)->GetAt(static_cast<il2cpp_array_size_t>(L_510))), FunctionArguments_t3402201403_il2cpp_TypeInfo_var)));
		V_16 = L_511;
	}

IL_0c56:
	{
		Expression_t4217024437 * L_512 = V_16;
		if (L_512)
		{
			goto IL_0c81;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_513 = V_3;
		int32_t L_514 = V_6;
		NullCheck(L_513);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_513, ((int32_t)((int32_t)((int32_t)-3)+(int32_t)L_514)));
		int32_t L_515 = ((int32_t)((int32_t)((int32_t)-3)+(int32_t)L_514));
		ObjectU5BU5D_t11523773* L_516 = V_3;
		int32_t L_517 = V_6;
		NullCheck(L_516);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_516, ((int32_t)((int32_t)(-1)+(int32_t)L_517)));
		int32_t L_518 = ((int32_t)((int32_t)(-1)+(int32_t)L_517));
		Il2CppObject * L_519 = __this->get_Context_0();
		Expression_t4217024437 * L_520 = ExprFunctionCall_Factory_m1613910802(NULL /*static, unused*/, ((XmlQualifiedName_t176365656 *)CastclassClass(((L_513)->GetAt(static_cast<il2cpp_array_size_t>(L_515))), XmlQualifiedName_t176365656_il2cpp_TypeInfo_var)), ((FunctionArguments_t3402201403 *)CastclassClass(((L_516)->GetAt(static_cast<il2cpp_array_size_t>(L_518))), FunctionArguments_t3402201403_il2cpp_TypeInfo_var)), L_519, /*hidden argument*/NULL);
		V_16 = L_520;
	}

IL_0c81:
	{
		Expression_t4217024437 * L_521 = V_16;
		V_2 = L_521;
		goto IL_0d77;
	}

IL_0c89:
	{
		ObjectU5BU5D_t11523773* L_522 = V_3;
		int32_t L_523 = V_6;
		NullCheck(L_522);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_522, ((int32_t)((int32_t)(-1)+(int32_t)L_523)));
		int32_t L_524 = ((int32_t)((int32_t)(-1)+(int32_t)L_523));
		ObjectU5BU5D_t11523773* L_525 = V_3;
		int32_t L_526 = V_6;
		NullCheck(L_525);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_525, ((int32_t)((int32_t)0+(int32_t)L_526)));
		int32_t L_527 = ((int32_t)((int32_t)0+(int32_t)L_526));
		FunctionArguments_t3402201403 * L_528 = (FunctionArguments_t3402201403 *)il2cpp_codegen_object_new(FunctionArguments_t3402201403_il2cpp_TypeInfo_var);
		FunctionArguments__ctor_m3611561738(L_528, ((Expression_t4217024437 *)CastclassClass(((L_522)->GetAt(static_cast<il2cpp_array_size_t>(L_524))), Expression_t4217024437_il2cpp_TypeInfo_var)), ((FunctionArguments_t3402201403 *)CastclassClass(((L_525)->GetAt(static_cast<il2cpp_array_size_t>(L_527))), FunctionArguments_t3402201403_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_2 = L_528;
		goto IL_0d77;
	}

IL_0caa:
	{
		ObjectU5BU5D_t11523773* L_529 = V_3;
		int32_t L_530 = V_6;
		NullCheck(L_529);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_529, ((int32_t)((int32_t)(-1)+(int32_t)L_530)));
		int32_t L_531 = ((int32_t)((int32_t)(-1)+(int32_t)L_530));
		ObjectU5BU5D_t11523773* L_532 = V_3;
		int32_t L_533 = V_6;
		NullCheck(L_532);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_532, ((int32_t)((int32_t)0+(int32_t)L_533)));
		int32_t L_534 = ((int32_t)((int32_t)0+(int32_t)L_533));
		FunctionArguments_t3402201403 * L_535 = (FunctionArguments_t3402201403 *)il2cpp_codegen_object_new(FunctionArguments_t3402201403_il2cpp_TypeInfo_var);
		FunctionArguments__ctor_m3611561738(L_535, ((Expression_t4217024437 *)CastclassClass(((L_529)->GetAt(static_cast<il2cpp_array_size_t>(L_531))), Expression_t4217024437_il2cpp_TypeInfo_var)), ((FunctionArguments_t3402201403 *)CastclassClass(((L_532)->GetAt(static_cast<il2cpp_array_size_t>(L_534))), FunctionArguments_t3402201403_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_2 = L_535;
		goto IL_0d77;
	}

IL_0ccb:
	{
		ObjectU5BU5D_t11523773* L_536 = V_3;
		int32_t L_537 = V_6;
		NullCheck(L_536);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_536, ((int32_t)((int32_t)(-1)+(int32_t)L_537)));
		int32_t L_538 = ((int32_t)((int32_t)(-1)+(int32_t)L_537));
		V_2 = ((L_536)->GetAt(static_cast<il2cpp_array_size_t>(L_538)));
		goto IL_0d77;
	}

IL_0cd7:
	{
		int32_t L_539 = ((int32_t)0);
		Il2CppObject * L_540 = Box(Axes_t4021066818_il2cpp_TypeInfo_var, &L_539);
		V_2 = L_540;
		goto IL_0d77;
	}

IL_0ce3:
	{
		int32_t L_541 = ((int32_t)1);
		Il2CppObject * L_542 = Box(Axes_t4021066818_il2cpp_TypeInfo_var, &L_541);
		V_2 = L_542;
		goto IL_0d77;
	}

IL_0cef:
	{
		int32_t L_543 = ((int32_t)2);
		Il2CppObject * L_544 = Box(Axes_t4021066818_il2cpp_TypeInfo_var, &L_543);
		V_2 = L_544;
		goto IL_0d77;
	}

IL_0cfb:
	{
		int32_t L_545 = ((int32_t)3);
		Il2CppObject * L_546 = Box(Axes_t4021066818_il2cpp_TypeInfo_var, &L_545);
		V_2 = L_546;
		goto IL_0d77;
	}

IL_0d07:
	{
		int32_t L_547 = ((int32_t)4);
		Il2CppObject * L_548 = Box(Axes_t4021066818_il2cpp_TypeInfo_var, &L_547);
		V_2 = L_548;
		goto IL_0d77;
	}

IL_0d13:
	{
		int32_t L_549 = ((int32_t)5);
		Il2CppObject * L_550 = Box(Axes_t4021066818_il2cpp_TypeInfo_var, &L_549);
		V_2 = L_550;
		goto IL_0d77;
	}

IL_0d1f:
	{
		int32_t L_551 = ((int32_t)6);
		Il2CppObject * L_552 = Box(Axes_t4021066818_il2cpp_TypeInfo_var, &L_551);
		V_2 = L_552;
		goto IL_0d77;
	}

IL_0d2b:
	{
		int32_t L_553 = ((int32_t)7);
		Il2CppObject * L_554 = Box(Axes_t4021066818_il2cpp_TypeInfo_var, &L_553);
		V_2 = L_554;
		goto IL_0d77;
	}

IL_0d37:
	{
		int32_t L_555 = ((int32_t)8);
		Il2CppObject * L_556 = Box(Axes_t4021066818_il2cpp_TypeInfo_var, &L_555);
		V_2 = L_556;
		goto IL_0d77;
	}

IL_0d43:
	{
		int32_t L_557 = ((int32_t)((int32_t)9));
		Il2CppObject * L_558 = Box(Axes_t4021066818_il2cpp_TypeInfo_var, &L_557);
		V_2 = L_558;
		goto IL_0d77;
	}

IL_0d50:
	{
		int32_t L_559 = ((int32_t)((int32_t)10));
		Il2CppObject * L_560 = Box(Axes_t4021066818_il2cpp_TypeInfo_var, &L_559);
		V_2 = L_560;
		goto IL_0d77;
	}

IL_0d5d:
	{
		int32_t L_561 = ((int32_t)((int32_t)11));
		Il2CppObject * L_562 = Box(Axes_t4021066818_il2cpp_TypeInfo_var, &L_561);
		V_2 = L_562;
		goto IL_0d77;
	}

IL_0d6a:
	{
		int32_t L_563 = ((int32_t)((int32_t)12));
		Il2CppObject * L_564 = Box(Axes_t4021066818_il2cpp_TypeInfo_var, &L_563);
		V_2 = L_564;
		goto IL_0d77;
	}

IL_0d77:
	{
		int32_t L_565 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(XPathParser_t618877717_il2cpp_TypeInfo_var);
		Int16U5BU5D_t3675865332* L_566 = ((XPathParser_t618877717_StaticFields*)XPathParser_t618877717_il2cpp_TypeInfo_var->static_fields)->get_yyLen_9();
		int32_t L_567 = V_9;
		NullCheck(L_566);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_566, L_567);
		int32_t L_568 = L_567;
		V_6 = ((int32_t)((int32_t)L_565-(int32_t)((L_566)->GetAt(static_cast<il2cpp_array_size_t>(L_568)))));
		Int32U5BU5D_t1809983122* L_569 = V_1;
		int32_t L_570 = V_6;
		NullCheck(L_569);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_569, L_570);
		int32_t L_571 = L_570;
		V_0 = ((L_569)->GetAt(static_cast<il2cpp_array_size_t>(L_571)));
		Int16U5BU5D_t3675865332* L_572 = ((XPathParser_t618877717_StaticFields*)XPathParser_t618877717_il2cpp_TypeInfo_var->static_fields)->get_yyLhs_8();
		int32_t L_573 = V_9;
		NullCheck(L_572);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_572, L_573);
		int32_t L_574 = L_573;
		V_17 = ((L_572)->GetAt(static_cast<il2cpp_array_size_t>(L_574)));
		int32_t L_575 = V_0;
		if (L_575)
		{
			goto IL_0e2e;
		}
	}
	{
		int32_t L_576 = V_17;
		if (L_576)
		{
			goto IL_0e2e;
		}
	}
	{
		Il2CppObject * L_577 = __this->get_debug_3();
		if (!L_577)
		{
			goto IL_0dbc;
		}
	}
	{
		Il2CppObject * L_578 = __this->get_debug_3();
		IL2CPP_RUNTIME_CLASS_INIT(XPathParser_t618877717_il2cpp_TypeInfo_var);
		int32_t L_579 = ((XPathParser_t618877717_StaticFields*)XPathParser_t618877717_il2cpp_TypeInfo_var->static_fields)->get_yyFinal_4();
		NullCheck(L_578);
		InterfaceActionInvoker2< int32_t, int32_t >::Invoke(6 /* System.Void Mono.Xml.XPath.yydebug.yyDebug::shift(System.Int32,System.Int32) */, yyDebug_t212154387_il2cpp_TypeInfo_var, L_578, 0, L_579);
	}

IL_0dbc:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XPathParser_t618877717_il2cpp_TypeInfo_var);
		int32_t L_580 = ((XPathParser_t618877717_StaticFields*)XPathParser_t618877717_il2cpp_TypeInfo_var->static_fields)->get_yyFinal_4();
		V_0 = L_580;
		int32_t L_581 = V_4;
		if ((((int32_t)L_581) >= ((int32_t)0)))
		{
			goto IL_0e09;
		}
	}
	{
		Il2CppObject * L_582 = ___yyLex0;
		NullCheck(L_582);
		bool L_583 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean Mono.Xml.XPath.yyParser.yyInput::advance() */, yyInput_t1890241104_il2cpp_TypeInfo_var, L_582);
		if (!L_583)
		{
			goto IL_0de0;
		}
	}
	{
		Il2CppObject * L_584 = ___yyLex0;
		NullCheck(L_584);
		int32_t L_585 = InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 Mono.Xml.XPath.yyParser.yyInput::token() */, yyInput_t1890241104_il2cpp_TypeInfo_var, L_584);
		G_B165_0 = L_585;
		goto IL_0de1;
	}

IL_0de0:
	{
		G_B165_0 = 0;
	}

IL_0de1:
	{
		V_4 = G_B165_0;
		Il2CppObject * L_586 = __this->get_debug_3();
		if (!L_586)
		{
			goto IL_0e09;
		}
	}
	{
		Il2CppObject * L_587 = __this->get_debug_3();
		int32_t L_588 = V_0;
		int32_t L_589 = V_4;
		int32_t L_590 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(XPathParser_t618877717_il2cpp_TypeInfo_var);
		String_t* L_591 = XPathParser_yyname_m3001501735(NULL /*static, unused*/, L_590, /*hidden argument*/NULL);
		Il2CppObject * L_592 = ___yyLex0;
		NullCheck(L_592);
		Il2CppObject * L_593 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object Mono.Xml.XPath.yyParser.yyInput::value() */, yyInput_t1890241104_il2cpp_TypeInfo_var, L_592);
		NullCheck(L_587);
		InterfaceActionInvoker4< int32_t, int32_t, String_t*, Il2CppObject * >::Invoke(1 /* System.Void Mono.Xml.XPath.yydebug.yyDebug::lex(System.Int32,System.Int32,System.String,System.Object) */, yyDebug_t212154387_il2cpp_TypeInfo_var, L_587, L_588, L_589, L_591, L_593);
	}

IL_0e09:
	{
		int32_t L_594 = V_4;
		if (L_594)
		{
			goto IL_0e29;
		}
	}
	{
		Il2CppObject * L_595 = __this->get_debug_3();
		if (!L_595)
		{
			goto IL_0e27;
		}
	}
	{
		Il2CppObject * L_596 = __this->get_debug_3();
		Il2CppObject * L_597 = V_2;
		NullCheck(L_596);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(7 /* System.Void Mono.Xml.XPath.yydebug.yyDebug::accept(System.Object) */, yyDebug_t212154387_il2cpp_TypeInfo_var, L_596, L_597);
	}

IL_0e27:
	{
		Il2CppObject * L_598 = V_2;
		return L_598;
	}

IL_0e29:
	{
		goto IL_0ea8;
	}

IL_0e2e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XPathParser_t618877717_il2cpp_TypeInfo_var);
		Int16U5BU5D_t3675865332* L_599 = ((XPathParser_t618877717_StaticFields*)XPathParser_t618877717_il2cpp_TypeInfo_var->static_fields)->get_yyGindex_14();
		int32_t L_600 = V_17;
		NullCheck(L_599);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_599, L_600);
		int32_t L_601 = L_600;
		int16_t L_602 = ((L_599)->GetAt(static_cast<il2cpp_array_size_t>(L_601)));
		V_9 = L_602;
		if (!L_602)
		{
			goto IL_0e75;
		}
	}
	{
		int32_t L_603 = V_9;
		int32_t L_604 = V_0;
		int32_t L_605 = ((int32_t)((int32_t)L_603+(int32_t)L_604));
		V_9 = L_605;
		if ((((int32_t)L_605) < ((int32_t)0)))
		{
			goto IL_0e75;
		}
	}
	{
		int32_t L_606 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(XPathParser_t618877717_il2cpp_TypeInfo_var);
		Int16U5BU5D_t3675865332* L_607 = ((XPathParser_t618877717_StaticFields*)XPathParser_t618877717_il2cpp_TypeInfo_var->static_fields)->get_yyTable_15();
		NullCheck(L_607);
		if ((((int32_t)L_606) >= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_607)->max_length)))))))
		{
			goto IL_0e75;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XPathParser_t618877717_il2cpp_TypeInfo_var);
		Int16U5BU5D_t3675865332* L_608 = ((XPathParser_t618877717_StaticFields*)XPathParser_t618877717_il2cpp_TypeInfo_var->static_fields)->get_yyCheck_16();
		int32_t L_609 = V_9;
		NullCheck(L_608);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_608, L_609);
		int32_t L_610 = L_609;
		int32_t L_611 = V_0;
		if ((!(((uint32_t)((L_608)->GetAt(static_cast<il2cpp_array_size_t>(L_610)))) == ((uint32_t)L_611))))
		{
			goto IL_0e75;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(XPathParser_t618877717_il2cpp_TypeInfo_var);
		Int16U5BU5D_t3675865332* L_612 = ((XPathParser_t618877717_StaticFields*)XPathParser_t618877717_il2cpp_TypeInfo_var->static_fields)->get_yyTable_15();
		int32_t L_613 = V_9;
		NullCheck(L_612);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_612, L_613);
		int32_t L_614 = L_613;
		V_0 = ((L_612)->GetAt(static_cast<il2cpp_array_size_t>(L_614)));
		goto IL_0e7e;
	}

IL_0e75:
	{
		IL2CPP_RUNTIME_CLASS_INIT(XPathParser_t618877717_il2cpp_TypeInfo_var);
		Int16U5BU5D_t3675865332* L_615 = ((XPathParser_t618877717_StaticFields*)XPathParser_t618877717_il2cpp_TypeInfo_var->static_fields)->get_yyDgoto_11();
		int32_t L_616 = V_17;
		NullCheck(L_615);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_615, L_616);
		int32_t L_617 = L_616;
		V_0 = ((L_615)->GetAt(static_cast<il2cpp_array_size_t>(L_617)));
	}

IL_0e7e:
	{
		Il2CppObject * L_618 = __this->get_debug_3();
		if (!L_618)
		{
			goto IL_0e99;
		}
	}
	{
		Il2CppObject * L_619 = __this->get_debug_3();
		Int32U5BU5D_t1809983122* L_620 = V_1;
		int32_t L_621 = V_6;
		NullCheck(L_620);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_620, L_621);
		int32_t L_622 = L_621;
		int32_t L_623 = V_0;
		NullCheck(L_619);
		InterfaceActionInvoker2< int32_t, int32_t >::Invoke(6 /* System.Void Mono.Xml.XPath.yydebug.yyDebug::shift(System.Int32,System.Int32) */, yyDebug_t212154387_il2cpp_TypeInfo_var, L_619, ((L_620)->GetAt(static_cast<il2cpp_array_size_t>(L_622))), L_623);
	}

IL_0e99:
	{
		goto IL_0ea8;
	}

IL_0e9e:
	{
		goto IL_0ea3;
	}

IL_0ea3:
	{
		goto IL_00ac;
	}

IL_0ea8:
	{
		goto IL_0ead;
	}

IL_0ead:
	{
		int32_t L_624 = V_6;
		V_6 = ((int32_t)((int32_t)L_624+(int32_t)1));
	}

IL_0eb3:
	{
		goto IL_0041;
	}
}
// System.Void Mono.Xml.XPath.XPathParser/YYRules::.cctor()
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* YYRules_t10764727_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral59836079;
extern Il2CppCodeGenString* _stringLiteral2386069248;
extern Il2CppCodeGenString* _stringLiteral1603771299;
extern Il2CppCodeGenString* _stringLiteral3488532589;
extern Il2CppCodeGenString* _stringLiteral1323250924;
extern Il2CppCodeGenString* _stringLiteral3835294396;
extern Il2CppCodeGenString* _stringLiteral1811800440;
extern Il2CppCodeGenString* _stringLiteral3005835992;
extern Il2CppCodeGenString* _stringLiteral745702884;
extern Il2CppCodeGenString* _stringLiteral150412335;
extern Il2CppCodeGenString* _stringLiteral1947290457;
extern Il2CppCodeGenString* _stringLiteral3740811485;
extern Il2CppCodeGenString* _stringLiteral77248221;
extern Il2CppCodeGenString* _stringLiteral397266585;
extern Il2CppCodeGenString* _stringLiteral859214593;
extern Il2CppCodeGenString* _stringLiteral449196710;
extern Il2CppCodeGenString* _stringLiteral3333176234;
extern Il2CppCodeGenString* _stringLiteral3735946749;
extern Il2CppCodeGenString* _stringLiteral1269949821;
extern Il2CppCodeGenString* _stringLiteral3180130006;
extern Il2CppCodeGenString* _stringLiteral1575477053;
extern Il2CppCodeGenString* _stringLiteral216582889;
extern Il2CppCodeGenString* _stringLiteral1176273630;
extern Il2CppCodeGenString* _stringLiteral2549436841;
extern Il2CppCodeGenString* _stringLiteral2265223849;
extern Il2CppCodeGenString* _stringLiteral2368578758;
extern Il2CppCodeGenString* _stringLiteral2684988499;
extern Il2CppCodeGenString* _stringLiteral648794262;
extern Il2CppCodeGenString* _stringLiteral1775122475;
extern Il2CppCodeGenString* _stringLiteral1336657131;
extern Il2CppCodeGenString* _stringLiteral1889315775;
extern Il2CppCodeGenString* _stringLiteral2941491322;
extern Il2CppCodeGenString* _stringLiteral882760558;
extern Il2CppCodeGenString* _stringLiteral1934936105;
extern Il2CppCodeGenString* _stringLiteral2228966384;
extern Il2CppCodeGenString* _stringLiteral1288572375;
extern Il2CppCodeGenString* _stringLiteral3251646485;
extern Il2CppCodeGenString* _stringLiteral1720796713;
extern Il2CppCodeGenString* _stringLiteral2889351266;
extern Il2CppCodeGenString* _stringLiteral35599035;
extern Il2CppCodeGenString* _stringLiteral449821676;
extern Il2CppCodeGenString* _stringLiteral3462657634;
extern Il2CppCodeGenString* _stringLiteral2141145714;
extern Il2CppCodeGenString* _stringLiteral2010406236;
extern Il2CppCodeGenString* _stringLiteral741969093;
extern Il2CppCodeGenString* _stringLiteral2251023558;
extern Il2CppCodeGenString* _stringLiteral1693593561;
extern Il2CppCodeGenString* _stringLiteral734462480;
extern Il2CppCodeGenString* _stringLiteral1858915210;
extern Il2CppCodeGenString* _stringLiteral3383971826;
extern Il2CppCodeGenString* _stringLiteral775600669;
extern Il2CppCodeGenString* _stringLiteral1460843912;
extern Il2CppCodeGenString* _stringLiteral3767895582;
extern Il2CppCodeGenString* _stringLiteral1406060860;
extern Il2CppCodeGenString* _stringLiteral3626130924;
extern Il2CppCodeGenString* _stringLiteral2041098921;
extern Il2CppCodeGenString* _stringLiteral3611780637;
extern Il2CppCodeGenString* _stringLiteral895068235;
extern Il2CppCodeGenString* _stringLiteral3971078859;
extern Il2CppCodeGenString* _stringLiteral2403568847;
extern Il2CppCodeGenString* _stringLiteral4178994452;
extern Il2CppCodeGenString* _stringLiteral1188291965;
extern Il2CppCodeGenString* _stringLiteral1305348795;
extern Il2CppCodeGenString* _stringLiteral1874765779;
extern Il2CppCodeGenString* _stringLiteral1649210184;
extern Il2CppCodeGenString* _stringLiteral3880875498;
extern Il2CppCodeGenString* _stringLiteral219342654;
extern Il2CppCodeGenString* _stringLiteral2018837325;
extern Il2CppCodeGenString* _stringLiteral2561598014;
extern Il2CppCodeGenString* _stringLiteral3882519893;
extern Il2CppCodeGenString* _stringLiteral3818076597;
extern Il2CppCodeGenString* _stringLiteral2977735031;
extern Il2CppCodeGenString* _stringLiteral2453314316;
extern Il2CppCodeGenString* _stringLiteral2977565260;
extern Il2CppCodeGenString* _stringLiteral2935673566;
extern Il2CppCodeGenString* _stringLiteral3639609053;
extern Il2CppCodeGenString* _stringLiteral320469127;
extern Il2CppCodeGenString* _stringLiteral3967948778;
extern Il2CppCodeGenString* _stringLiteral1260887232;
extern Il2CppCodeGenString* _stringLiteral940086520;
extern Il2CppCodeGenString* _stringLiteral4215574757;
extern Il2CppCodeGenString* _stringLiteral477897249;
extern Il2CppCodeGenString* _stringLiteral4113230325;
extern Il2CppCodeGenString* _stringLiteral3443968587;
extern Il2CppCodeGenString* _stringLiteral2350764261;
extern Il2CppCodeGenString* _stringLiteral3189266768;
extern Il2CppCodeGenString* _stringLiteral784582470;
extern Il2CppCodeGenString* _stringLiteral3321734573;
extern Il2CppCodeGenString* _stringLiteral934443926;
extern Il2CppCodeGenString* _stringLiteral31590882;
extern Il2CppCodeGenString* _stringLiteral2776624994;
extern Il2CppCodeGenString* _stringLiteral3752334935;
extern Il2CppCodeGenString* _stringLiteral1834918464;
extern Il2CppCodeGenString* _stringLiteral784421335;
extern Il2CppCodeGenString* _stringLiteral1038476346;
extern Il2CppCodeGenString* _stringLiteral1270723745;
extern Il2CppCodeGenString* _stringLiteral542004996;
extern Il2CppCodeGenString* _stringLiteral2933154825;
extern Il2CppCodeGenString* _stringLiteral3223626860;
extern Il2CppCodeGenString* _stringLiteral90042406;
extern Il2CppCodeGenString* _stringLiteral1832228201;
extern Il2CppCodeGenString* _stringLiteral1051944152;
extern const uint32_t YYRules__cctor_m1975363257_MetadataUsageId;
extern "C"  void YYRules__cctor_m1975363257 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (YYRules__cctor_m1975363257_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringU5BU5D_t2956870243* L_0 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)((int32_t)104)));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral59836079);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral59836079);
		StringU5BU5D_t2956870243* L_1 = L_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, _stringLiteral2386069248);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2386069248);
		StringU5BU5D_t2956870243* L_2 = L_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 2);
		ArrayElementTypeCheck (L_2, _stringLiteral1603771299);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1603771299);
		StringU5BU5D_t2956870243* L_3 = L_2;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 3);
		ArrayElementTypeCheck (L_3, _stringLiteral3488532589);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral3488532589);
		StringU5BU5D_t2956870243* L_4 = L_3;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 4);
		ArrayElementTypeCheck (L_4, _stringLiteral1323250924);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral1323250924);
		StringU5BU5D_t2956870243* L_5 = L_4;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 5);
		ArrayElementTypeCheck (L_5, _stringLiteral3835294396);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral3835294396);
		StringU5BU5D_t2956870243* L_6 = L_5;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 6);
		ArrayElementTypeCheck (L_6, _stringLiteral1811800440);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral1811800440);
		StringU5BU5D_t2956870243* L_7 = L_6;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 7);
		ArrayElementTypeCheck (L_7, _stringLiteral3005835992);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(7), (String_t*)_stringLiteral3005835992);
		StringU5BU5D_t2956870243* L_8 = L_7;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 8);
		ArrayElementTypeCheck (L_8, _stringLiteral745702884);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(8), (String_t*)_stringLiteral745702884);
		StringU5BU5D_t2956870243* L_9 = L_8;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, ((int32_t)9));
		ArrayElementTypeCheck (L_9, _stringLiteral150412335);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (String_t*)_stringLiteral150412335);
		StringU5BU5D_t2956870243* L_10 = L_9;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, ((int32_t)10));
		ArrayElementTypeCheck (L_10, _stringLiteral1947290457);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (String_t*)_stringLiteral1947290457);
		StringU5BU5D_t2956870243* L_11 = L_10;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, ((int32_t)11));
		ArrayElementTypeCheck (L_11, _stringLiteral3740811485);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (String_t*)_stringLiteral3740811485);
		StringU5BU5D_t2956870243* L_12 = L_11;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, ((int32_t)12));
		ArrayElementTypeCheck (L_12, _stringLiteral77248221);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (String_t*)_stringLiteral77248221);
		StringU5BU5D_t2956870243* L_13 = L_12;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, ((int32_t)13));
		ArrayElementTypeCheck (L_13, _stringLiteral397266585);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (String_t*)_stringLiteral397266585);
		StringU5BU5D_t2956870243* L_14 = L_13;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, ((int32_t)14));
		ArrayElementTypeCheck (L_14, _stringLiteral859214593);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (String_t*)_stringLiteral859214593);
		StringU5BU5D_t2956870243* L_15 = L_14;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, ((int32_t)15));
		ArrayElementTypeCheck (L_15, _stringLiteral449196710);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (String_t*)_stringLiteral449196710);
		StringU5BU5D_t2956870243* L_16 = L_15;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, ((int32_t)16));
		ArrayElementTypeCheck (L_16, _stringLiteral3333176234);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)16)), (String_t*)_stringLiteral3333176234);
		StringU5BU5D_t2956870243* L_17 = L_16;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, ((int32_t)17));
		ArrayElementTypeCheck (L_17, _stringLiteral3735946749);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)17)), (String_t*)_stringLiteral3735946749);
		StringU5BU5D_t2956870243* L_18 = L_17;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, ((int32_t)18));
		ArrayElementTypeCheck (L_18, _stringLiteral1269949821);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)18)), (String_t*)_stringLiteral1269949821);
		StringU5BU5D_t2956870243* L_19 = L_18;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, ((int32_t)19));
		ArrayElementTypeCheck (L_19, _stringLiteral3180130006);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)19)), (String_t*)_stringLiteral3180130006);
		StringU5BU5D_t2956870243* L_20 = L_19;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, ((int32_t)20));
		ArrayElementTypeCheck (L_20, _stringLiteral1575477053);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)20)), (String_t*)_stringLiteral1575477053);
		StringU5BU5D_t2956870243* L_21 = L_20;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, ((int32_t)21));
		ArrayElementTypeCheck (L_21, _stringLiteral216582889);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)21)), (String_t*)_stringLiteral216582889);
		StringU5BU5D_t2956870243* L_22 = L_21;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, ((int32_t)22));
		ArrayElementTypeCheck (L_22, _stringLiteral1176273630);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)22)), (String_t*)_stringLiteral1176273630);
		StringU5BU5D_t2956870243* L_23 = L_22;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, ((int32_t)23));
		ArrayElementTypeCheck (L_23, _stringLiteral2549436841);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)23)), (String_t*)_stringLiteral2549436841);
		StringU5BU5D_t2956870243* L_24 = L_23;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, ((int32_t)24));
		ArrayElementTypeCheck (L_24, _stringLiteral2265223849);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)24)), (String_t*)_stringLiteral2265223849);
		StringU5BU5D_t2956870243* L_25 = L_24;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, ((int32_t)25));
		ArrayElementTypeCheck (L_25, _stringLiteral2368578758);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)25)), (String_t*)_stringLiteral2368578758);
		StringU5BU5D_t2956870243* L_26 = L_25;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, ((int32_t)26));
		ArrayElementTypeCheck (L_26, _stringLiteral2684988499);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)26)), (String_t*)_stringLiteral2684988499);
		StringU5BU5D_t2956870243* L_27 = L_26;
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, ((int32_t)27));
		ArrayElementTypeCheck (L_27, _stringLiteral648794262);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)27)), (String_t*)_stringLiteral648794262);
		StringU5BU5D_t2956870243* L_28 = L_27;
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, ((int32_t)28));
		ArrayElementTypeCheck (L_28, _stringLiteral1775122475);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)28)), (String_t*)_stringLiteral1775122475);
		StringU5BU5D_t2956870243* L_29 = L_28;
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, ((int32_t)29));
		ArrayElementTypeCheck (L_29, _stringLiteral1336657131);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)29)), (String_t*)_stringLiteral1336657131);
		StringU5BU5D_t2956870243* L_30 = L_29;
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, ((int32_t)30));
		ArrayElementTypeCheck (L_30, _stringLiteral1889315775);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)30)), (String_t*)_stringLiteral1889315775);
		StringU5BU5D_t2956870243* L_31 = L_30;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, ((int32_t)31));
		ArrayElementTypeCheck (L_31, _stringLiteral2941491322);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)31)), (String_t*)_stringLiteral2941491322);
		StringU5BU5D_t2956870243* L_32 = L_31;
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, ((int32_t)32));
		ArrayElementTypeCheck (L_32, _stringLiteral882760558);
		(L_32)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)32)), (String_t*)_stringLiteral882760558);
		StringU5BU5D_t2956870243* L_33 = L_32;
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, ((int32_t)33));
		ArrayElementTypeCheck (L_33, _stringLiteral1934936105);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)33)), (String_t*)_stringLiteral1934936105);
		StringU5BU5D_t2956870243* L_34 = L_33;
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, ((int32_t)34));
		ArrayElementTypeCheck (L_34, _stringLiteral2228966384);
		(L_34)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)34)), (String_t*)_stringLiteral2228966384);
		StringU5BU5D_t2956870243* L_35 = L_34;
		NullCheck(L_35);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_35, ((int32_t)35));
		ArrayElementTypeCheck (L_35, _stringLiteral1288572375);
		(L_35)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)35)), (String_t*)_stringLiteral1288572375);
		StringU5BU5D_t2956870243* L_36 = L_35;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, ((int32_t)36));
		ArrayElementTypeCheck (L_36, _stringLiteral3251646485);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)36)), (String_t*)_stringLiteral3251646485);
		StringU5BU5D_t2956870243* L_37 = L_36;
		NullCheck(L_37);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_37, ((int32_t)37));
		ArrayElementTypeCheck (L_37, _stringLiteral1720796713);
		(L_37)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)37)), (String_t*)_stringLiteral1720796713);
		StringU5BU5D_t2956870243* L_38 = L_37;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, ((int32_t)38));
		ArrayElementTypeCheck (L_38, _stringLiteral2889351266);
		(L_38)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)38)), (String_t*)_stringLiteral2889351266);
		StringU5BU5D_t2956870243* L_39 = L_38;
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, ((int32_t)39));
		ArrayElementTypeCheck (L_39, _stringLiteral35599035);
		(L_39)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)39)), (String_t*)_stringLiteral35599035);
		StringU5BU5D_t2956870243* L_40 = L_39;
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, ((int32_t)40));
		ArrayElementTypeCheck (L_40, _stringLiteral449821676);
		(L_40)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)40)), (String_t*)_stringLiteral449821676);
		StringU5BU5D_t2956870243* L_41 = L_40;
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, ((int32_t)41));
		ArrayElementTypeCheck (L_41, _stringLiteral3462657634);
		(L_41)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)41)), (String_t*)_stringLiteral3462657634);
		StringU5BU5D_t2956870243* L_42 = L_41;
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, ((int32_t)42));
		ArrayElementTypeCheck (L_42, _stringLiteral2141145714);
		(L_42)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)42)), (String_t*)_stringLiteral2141145714);
		StringU5BU5D_t2956870243* L_43 = L_42;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, ((int32_t)43));
		ArrayElementTypeCheck (L_43, _stringLiteral2010406236);
		(L_43)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)43)), (String_t*)_stringLiteral2010406236);
		StringU5BU5D_t2956870243* L_44 = L_43;
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, ((int32_t)44));
		ArrayElementTypeCheck (L_44, _stringLiteral741969093);
		(L_44)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)44)), (String_t*)_stringLiteral741969093);
		StringU5BU5D_t2956870243* L_45 = L_44;
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, ((int32_t)45));
		ArrayElementTypeCheck (L_45, _stringLiteral2251023558);
		(L_45)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)45)), (String_t*)_stringLiteral2251023558);
		StringU5BU5D_t2956870243* L_46 = L_45;
		NullCheck(L_46);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_46, ((int32_t)46));
		ArrayElementTypeCheck (L_46, _stringLiteral1693593561);
		(L_46)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)46)), (String_t*)_stringLiteral1693593561);
		StringU5BU5D_t2956870243* L_47 = L_46;
		NullCheck(L_47);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_47, ((int32_t)47));
		ArrayElementTypeCheck (L_47, _stringLiteral734462480);
		(L_47)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)47)), (String_t*)_stringLiteral734462480);
		StringU5BU5D_t2956870243* L_48 = L_47;
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, ((int32_t)48));
		ArrayElementTypeCheck (L_48, _stringLiteral1858915210);
		(L_48)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)48)), (String_t*)_stringLiteral1858915210);
		StringU5BU5D_t2956870243* L_49 = L_48;
		NullCheck(L_49);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_49, ((int32_t)49));
		ArrayElementTypeCheck (L_49, _stringLiteral3383971826);
		(L_49)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)49)), (String_t*)_stringLiteral3383971826);
		StringU5BU5D_t2956870243* L_50 = L_49;
		NullCheck(L_50);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_50, ((int32_t)50));
		ArrayElementTypeCheck (L_50, _stringLiteral775600669);
		(L_50)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)50)), (String_t*)_stringLiteral775600669);
		StringU5BU5D_t2956870243* L_51 = L_50;
		NullCheck(L_51);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_51, ((int32_t)51));
		ArrayElementTypeCheck (L_51, _stringLiteral1460843912);
		(L_51)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)51)), (String_t*)_stringLiteral1460843912);
		StringU5BU5D_t2956870243* L_52 = L_51;
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, ((int32_t)52));
		ArrayElementTypeCheck (L_52, _stringLiteral3767895582);
		(L_52)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)52)), (String_t*)_stringLiteral3767895582);
		StringU5BU5D_t2956870243* L_53 = L_52;
		NullCheck(L_53);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_53, ((int32_t)53));
		ArrayElementTypeCheck (L_53, _stringLiteral1406060860);
		(L_53)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)53)), (String_t*)_stringLiteral1406060860);
		StringU5BU5D_t2956870243* L_54 = L_53;
		NullCheck(L_54);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_54, ((int32_t)54));
		ArrayElementTypeCheck (L_54, _stringLiteral3626130924);
		(L_54)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)54)), (String_t*)_stringLiteral3626130924);
		StringU5BU5D_t2956870243* L_55 = L_54;
		NullCheck(L_55);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_55, ((int32_t)55));
		ArrayElementTypeCheck (L_55, _stringLiteral2041098921);
		(L_55)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)55)), (String_t*)_stringLiteral2041098921);
		StringU5BU5D_t2956870243* L_56 = L_55;
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, ((int32_t)56));
		ArrayElementTypeCheck (L_56, _stringLiteral3611780637);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)56)), (String_t*)_stringLiteral3611780637);
		StringU5BU5D_t2956870243* L_57 = L_56;
		NullCheck(L_57);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_57, ((int32_t)57));
		ArrayElementTypeCheck (L_57, _stringLiteral895068235);
		(L_57)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)57)), (String_t*)_stringLiteral895068235);
		StringU5BU5D_t2956870243* L_58 = L_57;
		NullCheck(L_58);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_58, ((int32_t)58));
		ArrayElementTypeCheck (L_58, _stringLiteral3971078859);
		(L_58)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)58)), (String_t*)_stringLiteral3971078859);
		StringU5BU5D_t2956870243* L_59 = L_58;
		NullCheck(L_59);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_59, ((int32_t)59));
		ArrayElementTypeCheck (L_59, _stringLiteral2403568847);
		(L_59)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)59)), (String_t*)_stringLiteral2403568847);
		StringU5BU5D_t2956870243* L_60 = L_59;
		NullCheck(L_60);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_60, ((int32_t)60));
		ArrayElementTypeCheck (L_60, _stringLiteral4178994452);
		(L_60)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)60)), (String_t*)_stringLiteral4178994452);
		StringU5BU5D_t2956870243* L_61 = L_60;
		NullCheck(L_61);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_61, ((int32_t)61));
		ArrayElementTypeCheck (L_61, _stringLiteral1188291965);
		(L_61)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)61)), (String_t*)_stringLiteral1188291965);
		StringU5BU5D_t2956870243* L_62 = L_61;
		NullCheck(L_62);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_62, ((int32_t)62));
		ArrayElementTypeCheck (L_62, _stringLiteral1305348795);
		(L_62)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)62)), (String_t*)_stringLiteral1305348795);
		StringU5BU5D_t2956870243* L_63 = L_62;
		NullCheck(L_63);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_63, ((int32_t)63));
		ArrayElementTypeCheck (L_63, _stringLiteral1874765779);
		(L_63)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)63)), (String_t*)_stringLiteral1874765779);
		StringU5BU5D_t2956870243* L_64 = L_63;
		NullCheck(L_64);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_64, ((int32_t)64));
		ArrayElementTypeCheck (L_64, _stringLiteral1649210184);
		(L_64)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)64)), (String_t*)_stringLiteral1649210184);
		StringU5BU5D_t2956870243* L_65 = L_64;
		NullCheck(L_65);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_65, ((int32_t)65));
		ArrayElementTypeCheck (L_65, _stringLiteral3880875498);
		(L_65)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)65)), (String_t*)_stringLiteral3880875498);
		StringU5BU5D_t2956870243* L_66 = L_65;
		NullCheck(L_66);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_66, ((int32_t)66));
		ArrayElementTypeCheck (L_66, _stringLiteral3180130006);
		(L_66)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)66)), (String_t*)_stringLiteral3180130006);
		StringU5BU5D_t2956870243* L_67 = L_66;
		NullCheck(L_67);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_67, ((int32_t)67));
		ArrayElementTypeCheck (L_67, _stringLiteral1575477053);
		(L_67)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)67)), (String_t*)_stringLiteral1575477053);
		StringU5BU5D_t2956870243* L_68 = L_67;
		NullCheck(L_68);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_68, ((int32_t)68));
		ArrayElementTypeCheck (L_68, _stringLiteral219342654);
		(L_68)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)68)), (String_t*)_stringLiteral219342654);
		StringU5BU5D_t2956870243* L_69 = L_68;
		NullCheck(L_69);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_69, ((int32_t)69));
		ArrayElementTypeCheck (L_69, _stringLiteral2018837325);
		(L_69)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)69)), (String_t*)_stringLiteral2018837325);
		StringU5BU5D_t2956870243* L_70 = L_69;
		NullCheck(L_70);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_70, ((int32_t)70));
		ArrayElementTypeCheck (L_70, _stringLiteral2561598014);
		(L_70)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)70)), (String_t*)_stringLiteral2561598014);
		StringU5BU5D_t2956870243* L_71 = L_70;
		NullCheck(L_71);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_71, ((int32_t)71));
		ArrayElementTypeCheck (L_71, _stringLiteral3882519893);
		(L_71)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)71)), (String_t*)_stringLiteral3882519893);
		StringU5BU5D_t2956870243* L_72 = L_71;
		NullCheck(L_72);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_72, ((int32_t)72));
		ArrayElementTypeCheck (L_72, _stringLiteral3818076597);
		(L_72)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)72)), (String_t*)_stringLiteral3818076597);
		StringU5BU5D_t2956870243* L_73 = L_72;
		NullCheck(L_73);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_73, ((int32_t)73));
		ArrayElementTypeCheck (L_73, _stringLiteral2977735031);
		(L_73)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)73)), (String_t*)_stringLiteral2977735031);
		StringU5BU5D_t2956870243* L_74 = L_73;
		NullCheck(L_74);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_74, ((int32_t)74));
		ArrayElementTypeCheck (L_74, _stringLiteral2453314316);
		(L_74)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)74)), (String_t*)_stringLiteral2453314316);
		StringU5BU5D_t2956870243* L_75 = L_74;
		NullCheck(L_75);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_75, ((int32_t)75));
		ArrayElementTypeCheck (L_75, _stringLiteral2977565260);
		(L_75)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)75)), (String_t*)_stringLiteral2977565260);
		StringU5BU5D_t2956870243* L_76 = L_75;
		NullCheck(L_76);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_76, ((int32_t)76));
		ArrayElementTypeCheck (L_76, _stringLiteral2935673566);
		(L_76)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)76)), (String_t*)_stringLiteral2935673566);
		StringU5BU5D_t2956870243* L_77 = L_76;
		NullCheck(L_77);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_77, ((int32_t)77));
		ArrayElementTypeCheck (L_77, _stringLiteral3639609053);
		(L_77)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)77)), (String_t*)_stringLiteral3639609053);
		StringU5BU5D_t2956870243* L_78 = L_77;
		NullCheck(L_78);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_78, ((int32_t)78));
		ArrayElementTypeCheck (L_78, _stringLiteral320469127);
		(L_78)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)78)), (String_t*)_stringLiteral320469127);
		StringU5BU5D_t2956870243* L_79 = L_78;
		NullCheck(L_79);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_79, ((int32_t)79));
		ArrayElementTypeCheck (L_79, _stringLiteral3967948778);
		(L_79)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)79)), (String_t*)_stringLiteral3967948778);
		StringU5BU5D_t2956870243* L_80 = L_79;
		NullCheck(L_80);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_80, ((int32_t)80));
		ArrayElementTypeCheck (L_80, _stringLiteral1260887232);
		(L_80)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)80)), (String_t*)_stringLiteral1260887232);
		StringU5BU5D_t2956870243* L_81 = L_80;
		NullCheck(L_81);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_81, ((int32_t)81));
		ArrayElementTypeCheck (L_81, _stringLiteral940086520);
		(L_81)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)81)), (String_t*)_stringLiteral940086520);
		StringU5BU5D_t2956870243* L_82 = L_81;
		NullCheck(L_82);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_82, ((int32_t)82));
		ArrayElementTypeCheck (L_82, _stringLiteral4215574757);
		(L_82)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)82)), (String_t*)_stringLiteral4215574757);
		StringU5BU5D_t2956870243* L_83 = L_82;
		NullCheck(L_83);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_83, ((int32_t)83));
		ArrayElementTypeCheck (L_83, _stringLiteral477897249);
		(L_83)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)83)), (String_t*)_stringLiteral477897249);
		StringU5BU5D_t2956870243* L_84 = L_83;
		NullCheck(L_84);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_84, ((int32_t)84));
		ArrayElementTypeCheck (L_84, _stringLiteral4113230325);
		(L_84)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)84)), (String_t*)_stringLiteral4113230325);
		StringU5BU5D_t2956870243* L_85 = L_84;
		NullCheck(L_85);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_85, ((int32_t)85));
		ArrayElementTypeCheck (L_85, _stringLiteral3443968587);
		(L_85)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)85)), (String_t*)_stringLiteral3443968587);
		StringU5BU5D_t2956870243* L_86 = L_85;
		NullCheck(L_86);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_86, ((int32_t)86));
		ArrayElementTypeCheck (L_86, _stringLiteral2350764261);
		(L_86)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)86)), (String_t*)_stringLiteral2350764261);
		StringU5BU5D_t2956870243* L_87 = L_86;
		NullCheck(L_87);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_87, ((int32_t)87));
		ArrayElementTypeCheck (L_87, _stringLiteral3189266768);
		(L_87)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)87)), (String_t*)_stringLiteral3189266768);
		StringU5BU5D_t2956870243* L_88 = L_87;
		NullCheck(L_88);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_88, ((int32_t)88));
		ArrayElementTypeCheck (L_88, _stringLiteral784582470);
		(L_88)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)88)), (String_t*)_stringLiteral784582470);
		StringU5BU5D_t2956870243* L_89 = L_88;
		NullCheck(L_89);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_89, ((int32_t)89));
		ArrayElementTypeCheck (L_89, _stringLiteral3321734573);
		(L_89)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)89)), (String_t*)_stringLiteral3321734573);
		StringU5BU5D_t2956870243* L_90 = L_89;
		NullCheck(L_90);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_90, ((int32_t)90));
		ArrayElementTypeCheck (L_90, _stringLiteral934443926);
		(L_90)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)90)), (String_t*)_stringLiteral934443926);
		StringU5BU5D_t2956870243* L_91 = L_90;
		NullCheck(L_91);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_91, ((int32_t)91));
		ArrayElementTypeCheck (L_91, _stringLiteral31590882);
		(L_91)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)91)), (String_t*)_stringLiteral31590882);
		StringU5BU5D_t2956870243* L_92 = L_91;
		NullCheck(L_92);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_92, ((int32_t)92));
		ArrayElementTypeCheck (L_92, _stringLiteral2776624994);
		(L_92)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)92)), (String_t*)_stringLiteral2776624994);
		StringU5BU5D_t2956870243* L_93 = L_92;
		NullCheck(L_93);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_93, ((int32_t)93));
		ArrayElementTypeCheck (L_93, _stringLiteral3752334935);
		(L_93)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)93)), (String_t*)_stringLiteral3752334935);
		StringU5BU5D_t2956870243* L_94 = L_93;
		NullCheck(L_94);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_94, ((int32_t)94));
		ArrayElementTypeCheck (L_94, _stringLiteral1834918464);
		(L_94)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)94)), (String_t*)_stringLiteral1834918464);
		StringU5BU5D_t2956870243* L_95 = L_94;
		NullCheck(L_95);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_95, ((int32_t)95));
		ArrayElementTypeCheck (L_95, _stringLiteral784421335);
		(L_95)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)95)), (String_t*)_stringLiteral784421335);
		StringU5BU5D_t2956870243* L_96 = L_95;
		NullCheck(L_96);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_96, ((int32_t)96));
		ArrayElementTypeCheck (L_96, _stringLiteral1038476346);
		(L_96)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)96)), (String_t*)_stringLiteral1038476346);
		StringU5BU5D_t2956870243* L_97 = L_96;
		NullCheck(L_97);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_97, ((int32_t)97));
		ArrayElementTypeCheck (L_97, _stringLiteral1270723745);
		(L_97)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)97)), (String_t*)_stringLiteral1270723745);
		StringU5BU5D_t2956870243* L_98 = L_97;
		NullCheck(L_98);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_98, ((int32_t)98));
		ArrayElementTypeCheck (L_98, _stringLiteral542004996);
		(L_98)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)98)), (String_t*)_stringLiteral542004996);
		StringU5BU5D_t2956870243* L_99 = L_98;
		NullCheck(L_99);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_99, ((int32_t)99));
		ArrayElementTypeCheck (L_99, _stringLiteral2933154825);
		(L_99)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)99)), (String_t*)_stringLiteral2933154825);
		StringU5BU5D_t2956870243* L_100 = L_99;
		NullCheck(L_100);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_100, ((int32_t)100));
		ArrayElementTypeCheck (L_100, _stringLiteral3223626860);
		(L_100)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)100)), (String_t*)_stringLiteral3223626860);
		StringU5BU5D_t2956870243* L_101 = L_100;
		NullCheck(L_101);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_101, ((int32_t)101));
		ArrayElementTypeCheck (L_101, _stringLiteral90042406);
		(L_101)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)101)), (String_t*)_stringLiteral90042406);
		StringU5BU5D_t2956870243* L_102 = L_101;
		NullCheck(L_102);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_102, ((int32_t)102));
		ArrayElementTypeCheck (L_102, _stringLiteral1832228201);
		(L_102)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)102)), (String_t*)_stringLiteral1832228201);
		StringU5BU5D_t2956870243* L_103 = L_102;
		NullCheck(L_103);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_103, ((int32_t)103));
		ArrayElementTypeCheck (L_103, _stringLiteral1051944152);
		(L_103)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)103)), (String_t*)_stringLiteral1051944152);
		((YYRules_t10764727_StaticFields*)YYRules_t10764727_il2cpp_TypeInfo_var->static_fields)->set_yyRule_1(L_103);
		return;
	}
}
// System.String Mono.Xml.XPath.XPathParser/YYRules::getRule(System.Int32)
extern Il2CppClass* YYRules_t10764727_il2cpp_TypeInfo_var;
extern const uint32_t YYRules_getRule_m3263407986_MetadataUsageId;
extern "C"  String_t* YYRules_getRule_m3263407986 (Il2CppObject * __this /* static, unused */, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (YYRules_getRule_m3263407986_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(YYRules_t10764727_il2cpp_TypeInfo_var);
		StringU5BU5D_t2956870243* L_0 = ((YYRules_t10764727_StaticFields*)YYRules_t10764727_il2cpp_TypeInfo_var->static_fields)->get_yyRule_1();
		int32_t L_1 = ___index0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		return ((L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2)));
	}
}
// System.Void Mono.Xml.XPath.yyParser.yyException::.ctor(System.String)
extern "C"  void yyException__ctor_m2976596948 (yyException_t1136059093 * __this, String_t* ___message0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		Exception__ctor_m3870771296(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml.XPath.yyParser.yyUnexpectedEof::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t yyUnexpectedEof__ctor_m1709160914_MetadataUsageId;
extern "C"  void yyUnexpectedEof__ctor_m1709160914 (yyUnexpectedEof_t2997709169 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (yyUnexpectedEof__ctor_m1709160914_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		yyException__ctor_m2976596948(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml2.XmlTextReader::.ctor(System.IO.TextReader,System.Xml.XmlNameTable)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t XmlTextReader__ctor_m2112230387_MetadataUsageId;
extern "C"  void XmlTextReader__ctor_m2112230387 (XmlTextReader_t3066586409 * __this, TextReader_t1534522647 * ___input0, XmlNameTable_t3232213908 * ___nt1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader__ctor_m2112230387_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		TextReader_t1534522647 * L_1 = ___input0;
		XmlNameTable_t3232213908 * L_2 = ___nt1;
		XmlTextReader__ctor_m3279129655(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml2.XmlTextReader::.ctor(System.IO.Stream,System.Xml.XmlNodeType,System.Xml.XmlParserContext)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlStreamReader_t1190433730_il2cpp_TypeInfo_var;
extern const uint32_t XmlTextReader__ctor_m2476688333_MetadataUsageId;
extern "C"  void XmlTextReader__ctor_m2476688333 (XmlTextReader_t3066586409 * __this, Stream_t219029575 * ___xmlFragment0, int32_t ___fragType1, XmlParserContext_t3629084577 * ___context2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader__ctor_m2476688333_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XmlTextReader_t3066586409 * G_B2_0 = NULL;
	XmlTextReader_t3066586409 * G_B1_0 = NULL;
	String_t* G_B3_0 = NULL;
	XmlTextReader_t3066586409 * G_B3_1 = NULL;
	{
		XmlParserContext_t3629084577 * L_0 = ___context2;
		G_B1_0 = __this;
		if (!L_0)
		{
			G_B2_0 = __this;
			goto IL_0012;
		}
	}
	{
		XmlParserContext_t3629084577 * L_1 = ___context2;
		NullCheck(L_1);
		String_t* L_2 = XmlParserContext_get_BaseURI_m1880993733(L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		goto IL_0017;
	}

IL_0012:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
	}

IL_0017:
	{
		Stream_t219029575 * L_4 = ___xmlFragment0;
		XmlStreamReader_t1190433730 * L_5 = (XmlStreamReader_t1190433730 *)il2cpp_codegen_object_new(XmlStreamReader_t1190433730_il2cpp_TypeInfo_var);
		XmlStreamReader__ctor_m2508489112(L_5, L_4, /*hidden argument*/NULL);
		int32_t L_6 = ___fragType1;
		XmlParserContext_t3629084577 * L_7 = ___context2;
		NullCheck(G_B3_1);
		XmlTextReader__ctor_m4093855193(G_B3_1, G_B3_0, L_5, L_6, L_7, /*hidden argument*/NULL);
		__this->set_disallowReset_16((bool)1);
		return;
	}
}
// System.Void Mono.Xml2.XmlTextReader::.ctor(System.String,System.IO.Stream,System.Xml.XmlNameTable)
extern Il2CppClass* XmlStreamReader_t1190433730_il2cpp_TypeInfo_var;
extern const uint32_t XmlTextReader__ctor_m2102759687_MetadataUsageId;
extern "C"  void XmlTextReader__ctor_m2102759687 (XmlTextReader_t3066586409 * __this, String_t* ___url0, Stream_t219029575 * ___input1, XmlNameTable_t3232213908 * ___nt2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader__ctor_m2102759687_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___url0;
		Stream_t219029575 * L_1 = ___input1;
		XmlStreamReader_t1190433730 * L_2 = (XmlStreamReader_t1190433730 *)il2cpp_codegen_object_new(XmlStreamReader_t1190433730_il2cpp_TypeInfo_var);
		XmlStreamReader__ctor_m2508489112(L_2, L_1, /*hidden argument*/NULL);
		XmlNameTable_t3232213908 * L_3 = ___nt2;
		XmlTextReader__ctor_m3279129655(__this, L_0, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml2.XmlTextReader::.ctor(System.String,System.IO.TextReader,System.Xml.XmlNameTable)
extern "C"  void XmlTextReader__ctor_m3279129655 (XmlTextReader_t3066586409 * __this, String_t* ___url0, TextReader_t1534522647 * ___input1, XmlNameTable_t3232213908 * ___nt2, const MethodInfo* method)
{
	{
		String_t* L_0 = ___url0;
		TextReader_t1534522647 * L_1 = ___input1;
		XmlTextReader__ctor_m4093855193(__this, L_0, L_1, ((int32_t)9), (XmlParserContext_t3629084577 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml2.XmlTextReader::.ctor(System.String,System.Xml.XmlNodeType,System.Xml.XmlParserContext)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* StringReader_t2229325051_il2cpp_TypeInfo_var;
extern const uint32_t XmlTextReader__ctor_m4251487366_MetadataUsageId;
extern "C"  void XmlTextReader__ctor_m4251487366 (XmlTextReader_t3066586409 * __this, String_t* ___xmlFragment0, int32_t ___fragType1, XmlParserContext_t3629084577 * ___context2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader__ctor_m4251487366_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XmlTextReader_t3066586409 * G_B2_0 = NULL;
	XmlTextReader_t3066586409 * G_B1_0 = NULL;
	String_t* G_B3_0 = NULL;
	XmlTextReader_t3066586409 * G_B3_1 = NULL;
	{
		XmlParserContext_t3629084577 * L_0 = ___context2;
		G_B1_0 = __this;
		if (!L_0)
		{
			G_B2_0 = __this;
			goto IL_0012;
		}
	}
	{
		XmlParserContext_t3629084577 * L_1 = ___context2;
		NullCheck(L_1);
		String_t* L_2 = XmlParserContext_get_BaseURI_m1880993733(L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		goto IL_0017;
	}

IL_0012:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
	}

IL_0017:
	{
		String_t* L_4 = ___xmlFragment0;
		StringReader_t2229325051 * L_5 = (StringReader_t2229325051 *)il2cpp_codegen_object_new(StringReader_t2229325051_il2cpp_TypeInfo_var);
		StringReader__ctor_m1181104909(L_5, L_4, /*hidden argument*/NULL);
		int32_t L_6 = ___fragType1;
		XmlParserContext_t3629084577 * L_7 = ___context2;
		NullCheck(G_B3_1);
		XmlTextReader__ctor_m4093855193(G_B3_1, G_B3_0, L_5, L_6, L_7, /*hidden argument*/NULL);
		__this->set_disallowReset_16((bool)1);
		return;
	}
}
// System.Void Mono.Xml2.XmlTextReader::.ctor(System.String,System.IO.TextReader,System.Xml.XmlNodeType,System.Xml.XmlParserContext)
extern Il2CppClass* XmlAttributeTokenInfoU5BU5D_t2707514583_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlTokenInfoU5BU5D_t2413494705_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlUrlResolver_t215921638_il2cpp_TypeInfo_var;
extern Il2CppClass* DtdInputStateStack_t1263389581_il2cpp_TypeInfo_var;
extern const uint32_t XmlTextReader__ctor_m4093855193_MetadataUsageId;
extern "C"  void XmlTextReader__ctor_m4093855193 (XmlTextReader_t3066586409 * __this, String_t* ___url0, TextReader_t1534522647 * ___fragment1, int32_t ___fragType2, XmlParserContext_t3629084577 * ___context3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader__ctor_m4093855193_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_attributeTokens_7(((XmlAttributeTokenInfoU5BU5D_t2707514583*)SZArrayNew(XmlAttributeTokenInfoU5BU5D_t2707514583_il2cpp_TypeInfo_var, (uint32_t)((int32_t)10))));
		__this->set_attributeValueTokens_8(((XmlTokenInfoU5BU5D_t2413494705*)SZArrayNew(XmlTokenInfoU5BU5D_t2413494705_il2cpp_TypeInfo_var, (uint32_t)((int32_t)10))));
		__this->set_namespaces_44((bool)1);
		XmlUrlResolver_t215921638 * L_0 = (XmlUrlResolver_t215921638 *)il2cpp_codegen_object_new(XmlUrlResolver_t215921638_il2cpp_TypeInfo_var);
		XmlUrlResolver__ctor_m2727795693(L_0, /*hidden argument*/NULL);
		__this->set_resolver_46(L_0);
		__this->set_closeInput_50((bool)1);
		DtdInputStateStack_t1263389581 * L_1 = (DtdInputStateStack_t1263389581 *)il2cpp_codegen_object_new(DtdInputStateStack_t1263389581_il2cpp_TypeInfo_var);
		DtdInputStateStack__ctor_m2784489544(L_1, /*hidden argument*/NULL);
		__this->set_stateStack_54(L_1);
		XmlReader__ctor_m2228312417(__this, /*hidden argument*/NULL);
		String_t* L_2 = ___url0;
		XmlParserContext_t3629084577 * L_3 = ___context3;
		TextReader_t1534522647 * L_4 = ___fragment1;
		int32_t L_5 = ___fragType2;
		XmlTextReader_InitializeContext_m4274125212(__this, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Xml.XmlParserContext Mono.Xml2.XmlTextReader::Mono.Xml.IHasXmlParserContext.get_ParserContext()
extern "C"  XmlParserContext_t3629084577 * XmlTextReader_Mono_Xml_IHasXmlParserContext_get_ParserContext_m3669041097 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	{
		XmlParserContext_t3629084577 * L_0 = __this->get_parserContext_12();
		return L_0;
	}
}
// System.Int32 Mono.Xml2.XmlTextReader::get_AttributeCount()
extern "C"  int32_t XmlTextReader_get_AttributeCount_m1193113500 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_attributeCount_11();
		return L_0;
	}
}
// System.String Mono.Xml2.XmlTextReader::get_BaseURI()
extern "C"  String_t* XmlTextReader_get_BaseURI_m323459221 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	{
		XmlParserContext_t3629084577 * L_0 = __this->get_parserContext_12();
		NullCheck(L_0);
		String_t* L_1 = XmlParserContext_get_BaseURI_m1880993733(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean Mono.Xml2.XmlTextReader::get_CharacterChecking()
extern "C"  bool XmlTextReader_get_CharacterChecking_m3210588674 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_checkCharacters_48();
		return L_0;
	}
}
// System.Void Mono.Xml2.XmlTextReader::set_CharacterChecking(System.Boolean)
extern "C"  void XmlTextReader_set_CharacterChecking_m243948029 (XmlTextReader_t3066586409 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_checkCharacters_48(L_0);
		return;
	}
}
// System.Boolean Mono.Xml2.XmlTextReader::get_CloseInput()
extern "C"  bool XmlTextReader_get_CloseInput_m4285730133 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_closeInput_50();
		return L_0;
	}
}
// System.Void Mono.Xml2.XmlTextReader::set_CloseInput(System.Boolean)
extern "C"  void XmlTextReader_set_CloseInput_m534161544 (XmlTextReader_t3066586409 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_closeInput_50(L_0);
		return;
	}
}
// System.Int32 Mono.Xml2.XmlTextReader::get_Depth()
extern "C"  int32_t XmlTextReader_get_Depth_m958426748 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		XmlTokenInfo_t2571680784 * L_0 = __this->get_currentToken_4();
		NullCheck(L_0);
		int32_t L_1 = L_0->get_NodeType_12();
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0017;
		}
	}
	{
		G_B3_0 = 0;
		goto IL_0018;
	}

IL_0017:
	{
		G_B3_0 = (-1);
	}

IL_0018:
	{
		V_0 = G_B3_0;
		int32_t L_2 = __this->get_currentAttributeValue_10();
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_0030;
		}
	}
	{
		int32_t L_3 = V_0;
		int32_t L_4 = __this->get_elementDepth_18();
		return ((int32_t)((int32_t)((int32_t)((int32_t)L_3+(int32_t)L_4))+(int32_t)2));
	}

IL_0030:
	{
		int32_t L_5 = __this->get_currentAttribute_9();
		if ((((int32_t)L_5) < ((int32_t)0)))
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_6 = V_0;
		int32_t L_7 = __this->get_elementDepth_18();
		return ((int32_t)((int32_t)((int32_t)((int32_t)L_6+(int32_t)L_7))+(int32_t)1));
	}

IL_0047:
	{
		int32_t L_8 = __this->get_elementDepth_18();
		return L_8;
	}
}
// System.Xml.EntityHandling Mono.Xml2.XmlTextReader::get_EntityHandling()
extern "C"  int32_t XmlTextReader_get_EntityHandling_m33030270 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_entityHandling_51();
		return L_0;
	}
}
// System.Boolean Mono.Xml2.XmlTextReader::get_EOF()
extern "C"  bool XmlTextReader_get_EOF_m564876571 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_readState_15();
		return (bool)((((int32_t)L_0) == ((int32_t)3))? 1 : 0);
	}
}
// System.Boolean Mono.Xml2.XmlTextReader::get_IsDefault()
extern "C"  bool XmlTextReader_get_IsDefault_m2288721046 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean Mono.Xml2.XmlTextReader::get_IsEmptyElement()
extern "C"  bool XmlTextReader_get_IsEmptyElement_m2320835868 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	{
		XmlTokenInfo_t2571680784 * L_0 = __this->get_cursorToken_3();
		NullCheck(L_0);
		bool L_1 = L_0->get_IsEmptyElement_6();
		return L_1;
	}
}
// System.Int32 Mono.Xml2.XmlTextReader::get_LineNumber()
extern "C"  int32_t XmlTextReader_get_LineNumber_m3524737446 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_useProceedingLineInfo_38();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_1 = __this->get_line_34();
		return L_1;
	}

IL_0012:
	{
		XmlTokenInfo_t2571680784 * L_2 = __this->get_cursorToken_3();
		NullCheck(L_2);
		int32_t L_3 = L_2->get_LineNumber_8();
		return L_3;
	}
}
// System.Int32 Mono.Xml2.XmlTextReader::get_LinePosition()
extern "C"  int32_t XmlTextReader_get_LinePosition_m318081158 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_useProceedingLineInfo_38();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_1 = __this->get_column_35();
		return L_1;
	}

IL_0012:
	{
		XmlTokenInfo_t2571680784 * L_2 = __this->get_cursorToken_3();
		NullCheck(L_2);
		int32_t L_3 = L_2->get_LinePosition_9();
		return L_3;
	}
}
// System.String Mono.Xml2.XmlTextReader::get_LocalName()
extern "C"  String_t* XmlTextReader_get_LocalName_m1681504080 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	{
		XmlTokenInfo_t2571680784 * L_0 = __this->get_cursorToken_3();
		NullCheck(L_0);
		String_t* L_1 = L_0->get_LocalName_3();
		return L_1;
	}
}
// System.String Mono.Xml2.XmlTextReader::get_Name()
extern "C"  String_t* XmlTextReader_get_Name_m1653885427 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	{
		XmlTokenInfo_t2571680784 * L_0 = __this->get_cursorToken_3();
		NullCheck(L_0);
		String_t* L_1 = L_0->get_Name_2();
		return L_1;
	}
}
// System.String Mono.Xml2.XmlTextReader::get_NamespaceURI()
extern "C"  String_t* XmlTextReader_get_NamespaceURI_m37232889 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	{
		XmlTokenInfo_t2571680784 * L_0 = __this->get_cursorToken_3();
		NullCheck(L_0);
		String_t* L_1 = L_0->get_NamespaceURI_5();
		return L_1;
	}
}
// System.Xml.XmlNameTable Mono.Xml2.XmlTextReader::get_NameTable()
extern "C"  XmlNameTable_t3232213908 * XmlTextReader_get_NameTable_m3993144075 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	{
		XmlNameTable_t3232213908 * L_0 = __this->get_nameTable_13();
		return L_0;
	}
}
// System.Xml.XmlNodeType Mono.Xml2.XmlTextReader::get_NodeType()
extern "C"  int32_t XmlTextReader_get_NodeType_m3377542899 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	{
		XmlTokenInfo_t2571680784 * L_0 = __this->get_cursorToken_3();
		NullCheck(L_0);
		int32_t L_1 = L_0->get_NodeType_12();
		return L_1;
	}
}
// System.Boolean Mono.Xml2.XmlTextReader::get_Normalization()
extern "C"  bool XmlTextReader_get_Normalization_m717792252 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_normalization_47();
		return L_0;
	}
}
// System.Void Mono.Xml2.XmlTextReader::set_Normalization(System.Boolean)
extern "C"  void XmlTextReader_set_Normalization_m1554575479 (XmlTextReader_t3066586409 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_normalization_47(L_0);
		return;
	}
}
// System.String Mono.Xml2.XmlTextReader::get_Prefix()
extern "C"  String_t* XmlTextReader_get_Prefix_m1413213818 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	{
		XmlTokenInfo_t2571680784 * L_0 = __this->get_cursorToken_3();
		NullCheck(L_0);
		String_t* L_1 = L_0->get_Prefix_4();
		return L_1;
	}
}
// System.Xml.ReadState Mono.Xml2.XmlTextReader::get_ReadState()
extern "C"  int32_t XmlTextReader_get_ReadState_m734148670 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_readState_15();
		return L_0;
	}
}
// System.Xml.XmlReaderSettings Mono.Xml2.XmlTextReader::get_Settings()
extern "C"  XmlReaderSettings_t3693321125 * XmlTextReader_get_Settings_m2317908772 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	{
		XmlReaderSettings_t3693321125 * L_0 = XmlReader_get_Settings_m1880490585(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String Mono.Xml2.XmlTextReader::get_Value()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t XmlTextReader_get_Value_m2535514603_MetadataUsageId;
extern "C"  String_t* XmlTextReader_get_Value_m2535514603 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader_get_Value_m2535514603_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B3_0 = NULL;
	{
		XmlTokenInfo_t2571680784 * L_0 = __this->get_cursorToken_3();
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String Mono.Xml2.XmlTextReader/XmlTokenInfo::get_Value() */, L_0);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		XmlTokenInfo_t2571680784 * L_2 = __this->get_cursorToken_3();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String Mono.Xml2.XmlTextReader/XmlTokenInfo::get_Value() */, L_2);
		G_B3_0 = L_3;
		goto IL_0025;
	}

IL_0020:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_4;
	}

IL_0025:
	{
		return G_B3_0;
	}
}
// System.Xml.WhitespaceHandling Mono.Xml2.XmlTextReader::get_WhitespaceHandling()
extern "C"  int32_t XmlTextReader_get_WhitespaceHandling_m2742811570 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_whitespaceHandling_45();
		return L_0;
	}
}
// System.Void Mono.Xml2.XmlTextReader::set_WhitespaceHandling(System.Xml.WhitespaceHandling)
extern "C"  void XmlTextReader_set_WhitespaceHandling_m1022196025 (XmlTextReader_t3066586409 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_whitespaceHandling_45(L_0);
		return;
	}
}
// System.Void Mono.Xml2.XmlTextReader::set_XmlResolver(System.Xml.XmlResolver)
extern "C"  void XmlTextReader_set_XmlResolver_m2681221305 (XmlTextReader_t3066586409 * __this, XmlResolver_t2502213349 * ___value0, const MethodInfo* method)
{
	{
		XmlResolver_t2502213349 * L_0 = ___value0;
		__this->set_resolver_46(L_0);
		return;
	}
}
// System.Xml.XmlSpace Mono.Xml2.XmlTextReader::get_XmlSpace()
extern "C"  int32_t XmlTextReader_get_XmlSpace_m3700762060 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	{
		XmlParserContext_t3629084577 * L_0 = __this->get_parserContext_12();
		NullCheck(L_0);
		int32_t L_1 = XmlParserContext_get_XmlSpace_m4263973404(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void Mono.Xml2.XmlTextReader::Close()
extern "C"  void XmlTextReader_Close_m1957867768 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	{
		__this->set_readState_15(4);
		XmlTokenInfo_t2571680784 * L_0 = __this->get_cursorToken_3();
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(6 /* System.Void Mono.Xml2.XmlTextReader/XmlTokenInfo::Clear() */, L_0);
		XmlTokenInfo_t2571680784 * L_1 = __this->get_currentToken_4();
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(6 /* System.Void Mono.Xml2.XmlTextReader/XmlTokenInfo::Clear() */, L_1);
		__this->set_attributeCount_11(0);
		bool L_2 = __this->get_closeInput_50();
		if (!L_2)
		{
			goto IL_0045;
		}
	}
	{
		TextReader_t1534522647 * L_3 = __this->get_reader_28();
		if (!L_3)
		{
			goto IL_0045;
		}
	}
	{
		TextReader_t1534522647 * L_4 = __this->get_reader_28();
		NullCheck(L_4);
		VirtActionInvoker0::Invoke(5 /* System.Void System.IO.TextReader::Close() */, L_4);
	}

IL_0045:
	{
		return;
	}
}
// System.String Mono.Xml2.XmlTextReader::GetAttribute(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t XmlTextReader_GetAttribute_m2130398877_MetadataUsageId;
extern "C"  String_t* XmlTextReader_GetAttribute_m2130398877 (XmlTextReader_t3066586409 * __this, String_t* ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader_GetAttribute_m2130398877_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0031;
	}

IL_0007:
	{
		XmlAttributeTokenInfoU5BU5D_t2707514583* L_0 = __this->get_attributeTokens_7();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		NullCheck(((L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2))));
		String_t* L_3 = ((XmlTokenInfo_t2571680784 *)((L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2))))->get_Name_2();
		String_t* L_4 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002d;
		}
	}
	{
		XmlAttributeTokenInfoU5BU5D_t2707514583* L_6 = __this->get_attributeTokens_7();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		NullCheck(((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8))));
		String_t* L_9 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String Mono.Xml2.XmlTextReader/XmlAttributeTokenInfo::get_Value() */, ((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8))));
		return L_9;
	}

IL_002d:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0031:
	{
		int32_t L_11 = V_0;
		int32_t L_12 = __this->get_attributeCount_11();
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_0007;
		}
	}
	{
		return (String_t*)NULL;
	}
}
// System.Int32 Mono.Xml2.XmlTextReader::GetIndexOfQualifiedAttribute(System.String,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t XmlTextReader_GetIndexOfQualifiedAttribute_m328924107_MetadataUsageId;
extern "C"  int32_t XmlTextReader_GetIndexOfQualifiedAttribute_m328924107 (XmlTextReader_t3066586409 * __this, String_t* ___localName0, String_t* ___namespaceURI1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader_GetIndexOfQualifiedAttribute_m328924107_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	XmlAttributeTokenInfo_t922105698 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0038;
	}

IL_0007:
	{
		XmlAttributeTokenInfoU5BU5D_t2707514583* L_0 = __this->get_attributeTokens_7();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		V_1 = ((L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2)));
		XmlAttributeTokenInfo_t922105698 * L_3 = V_1;
		NullCheck(L_3);
		String_t* L_4 = ((XmlTokenInfo_t2571680784 *)L_3)->get_LocalName_3();
		String_t* L_5 = ___localName0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0034;
		}
	}
	{
		XmlAttributeTokenInfo_t922105698 * L_7 = V_1;
		NullCheck(L_7);
		String_t* L_8 = ((XmlTokenInfo_t2571680784 *)L_7)->get_NamespaceURI_5();
		String_t* L_9 = ___namespaceURI1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_10 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0034;
		}
	}
	{
		int32_t L_11 = V_0;
		return L_11;
	}

IL_0034:
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0038:
	{
		int32_t L_13 = V_0;
		int32_t L_14 = __this->get_attributeCount_11();
		if ((((int32_t)L_13) < ((int32_t)L_14)))
		{
			goto IL_0007;
		}
	}
	{
		return (-1);
	}
}
// System.String Mono.Xml2.XmlTextReader::GetAttribute(System.String,System.String)
extern "C"  String_t* XmlTextReader_GetAttribute_m841228313 (XmlTextReader_t3066586409 * __this, String_t* ___localName0, String_t* ___namespaceURI1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		String_t* L_0 = ___localName0;
		String_t* L_1 = ___namespaceURI1;
		int32_t L_2 = XmlTextReader_GetIndexOfQualifiedAttribute_m328924107(__this, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) >= ((int32_t)0)))
		{
			goto IL_0012;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_0012:
	{
		XmlAttributeTokenInfoU5BU5D_t2707514583* L_4 = __this->get_attributeTokens_7();
		int32_t L_5 = V_0;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		NullCheck(((L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6))));
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String Mono.Xml2.XmlTextReader/XmlAttributeTokenInfo::get_Value() */, ((L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6))));
		return L_7;
	}
}
// System.IO.TextReader Mono.Xml2.XmlTextReader::GetRemainder()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* StringReader_t2229325051_il2cpp_TypeInfo_var;
extern const uint32_t XmlTextReader_GetRemainder_m318592377_MetadataUsageId;
extern "C"  TextReader_t1534522647 * XmlTextReader_GetRemainder_m318592377 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader_GetRemainder_m318592377_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = __this->get_peekCharsLength_31();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		TextReader_t1534522647 * L_1 = __this->get_reader_28();
		return L_1;
	}

IL_0013:
	{
		CharU5BU5D_t3416858730* L_2 = __this->get_peekChars_29();
		int32_t L_3 = __this->get_peekCharsIndex_30();
		int32_t L_4 = __this->get_peekCharsLength_31();
		int32_t L_5 = __this->get_peekCharsIndex_30();
		String_t* L_6 = String_CreateString_m3402832113(NULL, L_2, L_3, ((int32_t)((int32_t)L_4-(int32_t)L_5)), /*hidden argument*/NULL);
		TextReader_t1534522647 * L_7 = __this->get_reader_28();
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(11 /* System.String System.IO.TextReader::ReadToEnd() */, L_7);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m138640077(NULL /*static, unused*/, L_6, L_8, /*hidden argument*/NULL);
		StringReader_t2229325051 * L_10 = (StringReader_t2229325051 *)il2cpp_codegen_object_new(StringReader_t2229325051_il2cpp_TypeInfo_var);
		StringReader__ctor_m1181104909(L_10, L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.String Mono.Xml2.XmlTextReader::LookupNamespace(System.String)
extern "C"  String_t* XmlTextReader_LookupNamespace_m3140960670 (XmlTextReader_t3066586409 * __this, String_t* ___prefix0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___prefix0;
		String_t* L_1 = XmlTextReader_LookupNamespace_m3699761695(__this, L_0, (bool)0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String Mono.Xml2.XmlTextReader::LookupNamespace(System.String,System.Boolean)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t XmlTextReader_LookupNamespace_m3699761695_MetadataUsageId;
extern "C"  String_t* XmlTextReader_LookupNamespace_m3699761695 (XmlTextReader_t3066586409 * __this, String_t* ___prefix0, bool ___atomizedNames1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader_LookupNamespace_m3699761695_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* G_B3_0 = NULL;
	{
		XmlNamespaceManager_t1861067185 * L_0 = __this->get_nsmgr_14();
		String_t* L_1 = ___prefix0;
		bool L_2 = ___atomizedNames1;
		NullCheck(L_0);
		String_t* L_3 = XmlNamespaceManager_LookupNamespace_m1482352649(L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		String_t* L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_6 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0024;
		}
	}
	{
		G_B3_0 = ((String_t*)(NULL));
		goto IL_0025;
	}

IL_0024:
	{
		String_t* L_7 = V_0;
		G_B3_0 = L_7;
	}

IL_0025:
	{
		return G_B3_0;
	}
}
// System.Void Mono.Xml2.XmlTextReader::MoveToAttribute(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3134748758;
extern const uint32_t XmlTextReader_MoveToAttribute_m2073234785_MetadataUsageId;
extern "C"  void XmlTextReader_MoveToAttribute_m2073234785 (XmlTextReader_t3066586409 * __this, int32_t ___i0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader_MoveToAttribute_m2073234785_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___i0;
		int32_t L_1 = __this->get_attributeCount_11();
		if ((((int32_t)L_0) < ((int32_t)L_1)))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, _stringLiteral3134748758, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___i0;
		__this->set_currentAttribute_9(L_3);
		__this->set_currentAttributeValue_10((-1));
		XmlAttributeTokenInfoU5BU5D_t2707514583* L_4 = __this->get_attributeTokens_7();
		int32_t L_5 = ___i0;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		__this->set_cursorToken_3(((L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6))));
		return;
	}
}
// System.Boolean Mono.Xml2.XmlTextReader::MoveToAttribute(System.String,System.String)
extern "C"  bool XmlTextReader_MoveToAttribute_m2467921062 (XmlTextReader_t3066586409 * __this, String_t* ___localName0, String_t* ___namespaceName1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		String_t* L_0 = ___localName0;
		String_t* L_1 = ___namespaceName1;
		int32_t L_2 = XmlTextReader_GetIndexOfQualifiedAttribute_m328924107(__this, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) >= ((int32_t)0)))
		{
			goto IL_0012;
		}
	}
	{
		return (bool)0;
	}

IL_0012:
	{
		int32_t L_4 = V_0;
		VirtActionInvoker1< int32_t >::Invoke(32 /* System.Void Mono.Xml2.XmlTextReader::MoveToAttribute(System.Int32) */, __this, L_4);
		return (bool)1;
	}
}
// System.Boolean Mono.Xml2.XmlTextReader::MoveToElement()
extern "C"  bool XmlTextReader_MoveToElement_m617221944 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	{
		XmlTokenInfo_t2571680784 * L_0 = __this->get_currentToken_4();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		XmlTokenInfo_t2571680784 * L_1 = __this->get_cursorToken_3();
		XmlTokenInfo_t2571680784 * L_2 = __this->get_currentToken_4();
		if ((!(((Il2CppObject*)(XmlTokenInfo_t2571680784 *)L_1) == ((Il2CppObject*)(XmlTokenInfo_t2571680784 *)L_2))))
		{
			goto IL_0020;
		}
	}
	{
		return (bool)0;
	}

IL_0020:
	{
		int32_t L_3 = __this->get_currentAttribute_9();
		if ((((int32_t)L_3) < ((int32_t)0)))
		{
			goto IL_0048;
		}
	}
	{
		__this->set_currentAttribute_9((-1));
		__this->set_currentAttributeValue_10((-1));
		XmlTokenInfo_t2571680784 * L_4 = __this->get_currentToken_4();
		__this->set_cursorToken_3(L_4);
		return (bool)1;
	}

IL_0048:
	{
		return (bool)0;
	}
}
// System.Boolean Mono.Xml2.XmlTextReader::MoveToFirstAttribute()
extern "C"  bool XmlTextReader_MoveToFirstAttribute_m3711573298 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_attributeCount_11();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		VirtFuncInvoker0< bool >::Invoke(35 /* System.Boolean Mono.Xml2.XmlTextReader::MoveToElement() */, __this);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(37 /* System.Boolean Mono.Xml2.XmlTextReader::MoveToNextAttribute() */, __this);
		return L_1;
	}
}
// System.Boolean Mono.Xml2.XmlTextReader::MoveToNextAttribute()
extern "C"  bool XmlTextReader_MoveToNextAttribute_m2456957349 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_currentAttribute_9();
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_1 = __this->get_attributeCount_11();
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		return (bool)0;
	}

IL_0018:
	{
		int32_t L_2 = __this->get_currentAttribute_9();
		int32_t L_3 = __this->get_attributeCount_11();
		if ((((int32_t)((int32_t)((int32_t)L_2+(int32_t)1))) >= ((int32_t)L_3)))
		{
			goto IL_0055;
		}
	}
	{
		int32_t L_4 = __this->get_currentAttribute_9();
		__this->set_currentAttribute_9(((int32_t)((int32_t)L_4+(int32_t)1)));
		__this->set_currentAttributeValue_10((-1));
		XmlAttributeTokenInfoU5BU5D_t2707514583* L_5 = __this->get_attributeTokens_7();
		int32_t L_6 = __this->get_currentAttribute_9();
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		__this->set_cursorToken_3(((L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7))));
		return (bool)1;
	}

IL_0055:
	{
		return (bool)0;
	}
}
// System.Boolean Mono.Xml2.XmlTextReader::Read()
extern Il2CppCodeGenString* _stringLiteral1927235010;
extern const uint32_t XmlTextReader_Read_m777518640_MetadataUsageId;
extern "C"  bool XmlTextReader_Read_m777518640 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader_Read_m777518640_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	{
		int32_t L_0 = __this->get_readState_15();
		if ((!(((uint32_t)L_0) == ((uint32_t)4))))
		{
			goto IL_000e;
		}
	}
	{
		return (bool)0;
	}

IL_000e:
	{
		int32_t L_1 = __this->get_peekCharsIndex_30();
		__this->set_curNodePeekIndex_32(L_1);
		__this->set_preserveCurrentTag_33((bool)1);
		__this->set_nestLevel_41(0);
		XmlTextReader_ClearValueBuffer_m495962918(__this, /*hidden argument*/NULL);
		int32_t L_2 = __this->get_startNodeType_39();
		if ((!(((uint32_t)L_2) == ((uint32_t)2))))
		{
			goto IL_007f;
		}
	}
	{
		int32_t L_3 = __this->get_currentAttribute_9();
		if (L_3)
		{
			goto IL_0047;
		}
	}
	{
		return (bool)0;
	}

IL_0047:
	{
		XmlTextReader_SkipTextDeclaration_m3278219726(__this, /*hidden argument*/NULL);
		XmlTextReader_ClearAttributes_m2951900356(__this, /*hidden argument*/NULL);
		XmlTextReader_IncrementAttributeToken_m639649356(__this, /*hidden argument*/NULL);
		XmlTextReader_ReadAttributeValueTokens_m3644060952(__this, ((int32_t)34), /*hidden argument*/NULL);
		XmlAttributeTokenInfoU5BU5D_t2707514583* L_4 = __this->get_attributeTokens_7();
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		int32_t L_5 = 0;
		__this->set_cursorToken_3(((L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_5))));
		__this->set_currentAttributeValue_10((-1));
		__this->set_readState_15(1);
		return (bool)1;
	}

IL_007f:
	{
		int32_t L_6 = __this->get_readState_15();
		if (L_6)
		{
			goto IL_009c;
		}
	}
	{
		int32_t L_7 = __this->get_currentState_40();
		if ((!(((uint32_t)L_7) == ((uint32_t)1))))
		{
			goto IL_009c;
		}
	}
	{
		XmlTextReader_SkipTextDeclaration_m3278219726(__this, /*hidden argument*/NULL);
	}

IL_009c:
	{
		XmlReaderBinarySupport_t2724083932 * L_8 = XmlReader_get_Binary_m2949076662(__this, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_00b2;
		}
	}
	{
		XmlReaderBinarySupport_t2724083932 * L_9 = XmlReader_get_Binary_m2949076662(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		XmlReaderBinarySupport_Reset_m2219159140(L_9, /*hidden argument*/NULL);
	}

IL_00b2:
	{
		V_0 = (bool)0;
		__this->set_readState_15(1);
		int32_t L_10 = __this->get_line_34();
		__this->set_currentLinkedNodeLineNumber_36(L_10);
		int32_t L_11 = __this->get_column_35();
		__this->set_currentLinkedNodeLinePosition_37(L_11);
		__this->set_useProceedingLineInfo_38((bool)1);
		XmlTokenInfo_t2571680784 * L_12 = __this->get_currentToken_4();
		__this->set_cursorToken_3(L_12);
		__this->set_attributeCount_11(0);
		int32_t L_13 = (-1);
		V_1 = L_13;
		__this->set_currentAttributeValue_10(L_13);
		int32_t L_14 = V_1;
		__this->set_currentAttribute_9(L_14);
		XmlTokenInfo_t2571680784 * L_15 = __this->get_currentToken_4();
		NullCheck(L_15);
		VirtActionInvoker0::Invoke(6 /* System.Void Mono.Xml2.XmlTextReader/XmlTokenInfo::Clear() */, L_15);
		bool L_16 = __this->get_depthUp_19();
		if (!L_16)
		{
			goto IL_0128;
		}
	}
	{
		int32_t L_17 = __this->get_depth_17();
		__this->set_depth_17(((int32_t)((int32_t)L_17+(int32_t)1)));
		__this->set_depthUp_19((bool)0);
	}

IL_0128:
	{
		bool L_18 = __this->get_readCharsInProgress_42();
		if (!L_18)
		{
			goto IL_0141;
		}
	}
	{
		__this->set_readCharsInProgress_42((bool)0);
		bool L_19 = XmlTextReader_ReadUntilEndTag_m2571482831(__this, /*hidden argument*/NULL);
		return L_19;
	}

IL_0141:
	{
		bool L_20 = XmlTextReader_ReadContent_m3354608395(__this, /*hidden argument*/NULL);
		V_0 = L_20;
		bool L_21 = V_0;
		if (L_21)
		{
			goto IL_0174;
		}
	}
	{
		int32_t L_22 = __this->get_startNodeType_39();
		if ((!(((uint32_t)L_22) == ((uint32_t)((int32_t)9)))))
		{
			goto IL_0174;
		}
	}
	{
		int32_t L_23 = __this->get_currentState_40();
		if ((((int32_t)L_23) == ((int32_t)((int32_t)15))))
		{
			goto IL_0174;
		}
	}
	{
		XmlException_t3490696160 * L_24 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral1927235010, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_24);
	}

IL_0174:
	{
		__this->set_useProceedingLineInfo_38((bool)0);
		bool L_25 = V_0;
		return L_25;
	}
}
// System.Boolean Mono.Xml2.XmlTextReader::ReadAttributeValue()
extern "C"  bool XmlTextReader_ReadAttributeValue_m2833253125 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	XmlAttributeTokenInfo_t922105698 * V_0 = NULL;
	{
		int32_t L_0 = __this->get_readState_15();
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = __this->get_startNodeType_39();
		if ((!(((uint32_t)L_1) == ((uint32_t)2))))
		{
			goto IL_001e;
		}
	}
	{
		VirtFuncInvoker0< bool >::Invoke(38 /* System.Boolean Mono.Xml2.XmlTextReader::Read() */, __this);
	}

IL_001e:
	{
		int32_t L_2 = __this->get_currentAttribute_9();
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_002c;
		}
	}
	{
		return (bool)0;
	}

IL_002c:
	{
		XmlAttributeTokenInfoU5BU5D_t2707514583* L_3 = __this->get_attributeTokens_7();
		int32_t L_4 = __this->get_currentAttribute_9();
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		V_0 = ((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5)));
		int32_t L_6 = __this->get_currentAttributeValue_10();
		if ((((int32_t)L_6) >= ((int32_t)0)))
		{
			goto IL_0054;
		}
	}
	{
		XmlAttributeTokenInfo_t922105698 * L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = L_7->get_ValueTokenStartIndex_13();
		__this->set_currentAttributeValue_10(((int32_t)((int32_t)L_8-(int32_t)1)));
	}

IL_0054:
	{
		int32_t L_9 = __this->get_currentAttributeValue_10();
		XmlAttributeTokenInfo_t922105698 * L_10 = V_0;
		NullCheck(L_10);
		int32_t L_11 = L_10->get_ValueTokenEndIndex_14();
		if ((((int32_t)L_9) >= ((int32_t)L_11)))
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_12 = __this->get_currentAttributeValue_10();
		__this->set_currentAttributeValue_10(((int32_t)((int32_t)L_12+(int32_t)1)));
		XmlTokenInfoU5BU5D_t2413494705* L_13 = __this->get_attributeValueTokens_8();
		int32_t L_14 = __this->get_currentAttributeValue_10();
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		int32_t L_15 = L_14;
		__this->set_cursorToken_3(((L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15))));
		return (bool)1;
	}

IL_0088:
	{
		return (bool)0;
	}
}
// System.Int32 Mono.Xml2.XmlTextReader::ReadChars(System.Char[],System.Int32,System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1772705448;
extern Il2CppCodeGenString* _stringLiteral2833160635;
extern Il2CppCodeGenString* _stringLiteral158277856;
extern const uint32_t XmlTextReader_ReadChars_m2077762994_MetadataUsageId;
extern "C"  int32_t XmlTextReader_ReadChars_m2077762994 (XmlTextReader_t3066586409 * __this, CharU5BU5D_t3416858730* ___buffer0, int32_t ___offset1, int32_t ___length2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader_ReadChars_m2077762994_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___offset1;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_1 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_1, _stringLiteral1772705448, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		int32_t L_2 = ___length2;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0024;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_3 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, _stringLiteral2833160635, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0024:
	{
		CharU5BU5D_t3416858730* L_4 = ___buffer0;
		NullCheck(L_4);
		int32_t L_5 = ___offset1;
		int32_t L_6 = ___length2;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length))))) >= ((int32_t)((int32_t)((int32_t)L_5+(int32_t)L_6)))))
		{
			goto IL_003a;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_7 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_7, _stringLiteral158277856, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_003a:
	{
		bool L_8 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean Mono.Xml2.XmlTextReader::get_IsEmptyElement() */, __this);
		if (!L_8)
		{
			goto IL_004e;
		}
	}
	{
		VirtFuncInvoker0< bool >::Invoke(38 /* System.Boolean Mono.Xml2.XmlTextReader::Read() */, __this);
		return 0;
	}

IL_004e:
	{
		bool L_9 = __this->get_readCharsInProgress_42();
		if (L_9)
		{
			goto IL_0067;
		}
	}
	{
		int32_t L_10 = VirtFuncInvoker0< int32_t >::Invoke(18 /* System.Xml.XmlNodeType Mono.Xml2.XmlTextReader::get_NodeType() */, __this);
		if ((((int32_t)L_10) == ((int32_t)1)))
		{
			goto IL_0067;
		}
	}
	{
		return 0;
	}

IL_0067:
	{
		__this->set_preserveCurrentTag_33((bool)0);
		__this->set_readCharsInProgress_42((bool)1);
		__this->set_useProceedingLineInfo_38((bool)1);
		CharU5BU5D_t3416858730* L_11 = ___buffer0;
		int32_t L_12 = ___offset1;
		int32_t L_13 = ___length2;
		int32_t L_14 = XmlTextReader_ReadCharsInternal_m2404589429(__this, L_11, L_12, L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Void Mono.Xml2.XmlTextReader::ResolveEntity()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral748624944;
extern const uint32_t XmlTextReader_ResolveEntity_m3399004975_MetadataUsageId;
extern "C"  void XmlTextReader_ResolveEntity_m3399004975 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader_ResolveEntity_m3399004975_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		InvalidOperationException_t2420574324 * L_0 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_0, _stringLiteral748624944, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Mono.Xml2.XmlTextReader::Skip()
extern "C"  void XmlTextReader_Skip_m2182684385 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	{
		XmlReader_Skip_m722576770(__this, /*hidden argument*/NULL);
		return;
	}
}
// Mono.Xml.DTDObjectModel Mono.Xml2.XmlTextReader::get_DTD()
extern "C"  DTDObjectModel_t709926554 * XmlTextReader_get_DTD_m3845091782 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	{
		XmlParserContext_t3629084577 * L_0 = __this->get_parserContext_12();
		NullCheck(L_0);
		DTDObjectModel_t709926554 * L_1 = XmlParserContext_get_Dtd_m613804278(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Xml.XmlResolver Mono.Xml2.XmlTextReader::get_Resolver()
extern "C"  XmlResolver_t2502213349 * XmlTextReader_get_Resolver_m3248241095 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	{
		XmlResolver_t2502213349 * L_0 = __this->get_resolver_46();
		return L_0;
	}
}
// System.Xml.XmlException Mono.Xml2.XmlTextReader::NotWFError(System.String)
extern Il2CppClass* XmlException_t3490696160_il2cpp_TypeInfo_var;
extern const uint32_t XmlTextReader_NotWFError_m304583423_MetadataUsageId;
extern "C"  XmlException_t3490696160 * XmlTextReader_NotWFError_m304583423 (XmlTextReader_t3066586409 * __this, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader_NotWFError_m304583423_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String Mono.Xml2.XmlTextReader::get_BaseURI() */, __this);
		String_t* L_1 = ___message0;
		XmlException_t3490696160 * L_2 = (XmlException_t3490696160 *)il2cpp_codegen_object_new(XmlException_t3490696160_il2cpp_TypeInfo_var);
		XmlException__ctor_m3778248785(L_2, __this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void Mono.Xml2.XmlTextReader::Init()
extern Il2CppClass* TagNameU5BU5D_t2465062056_il2cpp_TypeInfo_var;
extern Il2CppClass* StringBuilder_t3822575854_il2cpp_TypeInfo_var;
extern Il2CppClass* CharGetter_t2496392225_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t3416858730_il2cpp_TypeInfo_var;
extern const MethodInfo* XmlTextReader_ReadChars_m2077762994_MethodInfo_var;
extern const uint32_t XmlTextReader_Init_m1899167282_MetadataUsageId;
extern "C"  void XmlTextReader_Init_m1899167282 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader_Init_m1899167282_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		__this->set_allowMultipleRoot_23((bool)0);
		__this->set_elementNames_21(((TagNameU5BU5D_t2465062056*)SZArrayNew(TagNameU5BU5D_t2465062056_il2cpp_TypeInfo_var, (uint32_t)((int32_t)10))));
		StringBuilder_t3822575854 * L_0 = (StringBuilder_t3822575854 *)il2cpp_codegen_object_new(StringBuilder_t3822575854_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m135953004(L_0, /*hidden argument*/NULL);
		__this->set_valueBuffer_27(L_0);
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)XmlTextReader_ReadChars_m2077762994_MethodInfo_var);
		CharGetter_t2496392225 * L_2 = (CharGetter_t2496392225 *)il2cpp_codegen_object_new(CharGetter_t2496392225_il2cpp_TypeInfo_var);
		CharGetter__ctor_m3751756675(L_2, __this, L_1, /*hidden argument*/NULL);
		__this->set_binaryCharGetter_43(L_2);
		__this->set_checkCharacters_48((bool)1);
		XmlReaderSettings_t3693321125 * L_3 = VirtFuncInvoker0< XmlReaderSettings_t3693321125 * >::Invoke(22 /* System.Xml.XmlReaderSettings Mono.Xml2.XmlTextReader::get_Settings() */, __this);
		if (!L_3)
		{
			goto IL_0054;
		}
	}
	{
		XmlReaderSettings_t3693321125 * L_4 = VirtFuncInvoker0< XmlReaderSettings_t3693321125 * >::Invoke(22 /* System.Xml.XmlReaderSettings Mono.Xml2.XmlTextReader::get_Settings() */, __this);
		NullCheck(L_4);
		bool L_5 = XmlReaderSettings_get_CheckCharacters_m3292028349(L_4, /*hidden argument*/NULL);
		__this->set_checkCharacters_48(L_5);
	}

IL_0054:
	{
		__this->set_prohibitDtd_49((bool)0);
		__this->set_closeInput_50((bool)1);
		__this->set_entityHandling_51(2);
		__this->set_peekCharsIndex_30(0);
		CharU5BU5D_t3416858730* L_6 = __this->get_peekChars_29();
		if (L_6)
		{
			goto IL_008b;
		}
	}
	{
		__this->set_peekChars_29(((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)((int32_t)1024))));
	}

IL_008b:
	{
		__this->set_peekCharsLength_31((-1));
		__this->set_curNodePeekIndex_32((-1));
		__this->set_line_34(1);
		__this->set_column_35(1);
		int32_t L_7 = 0;
		V_0 = L_7;
		__this->set_currentLinkedNodeLinePosition_37(L_7);
		int32_t L_8 = V_0;
		__this->set_currentLinkedNodeLineNumber_36(L_8);
		XmlTextReader_Clear_m1948108813(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml2.XmlTextReader::Clear()
extern Il2CppClass* XmlTokenInfo_t2571680784_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t XmlTextReader_Clear_m1948108813_MetadataUsageId;
extern "C"  void XmlTextReader_Clear_m1948108813 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader_Clear_m1948108813_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		XmlTokenInfo_t2571680784 * L_0 = (XmlTokenInfo_t2571680784 *)il2cpp_codegen_object_new(XmlTokenInfo_t2571680784_il2cpp_TypeInfo_var);
		XmlTokenInfo__ctor_m2227356974(L_0, __this, /*hidden argument*/NULL);
		__this->set_currentToken_4(L_0);
		XmlTokenInfo_t2571680784 * L_1 = __this->get_currentToken_4();
		__this->set_cursorToken_3(L_1);
		__this->set_currentAttribute_9((-1));
		__this->set_currentAttributeValue_10((-1));
		__this->set_attributeCount_11(0);
		__this->set_readState_15(0);
		__this->set_depth_17(0);
		__this->set_elementDepth_18(0);
		__this->set_depthUp_19((bool)0);
		int32_t L_2 = 0;
		V_0 = (bool)L_2;
		__this->set_allowMultipleRoot_23((bool)L_2);
		bool L_3 = V_0;
		__this->set_popScope_20(L_3);
		__this->set_elementNameStackPos_22(0);
		__this->set_isStandalone_24((bool)0);
		__this->set_returnEntityReference_25((bool)0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_entityReferenceName_26(L_4);
		__this->set_useProceedingLineInfo_38((bool)0);
		__this->set_currentState_40(0);
		__this->set_readCharsInProgress_42((bool)0);
		return;
	}
}
// System.Void Mono.Xml2.XmlTextReader::InitializeContext(System.String,System.Xml.XmlParserContext,System.IO.TextReader,System.Xml.XmlNodeType)
extern Il2CppClass* NameTable_t2805661099_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlNamespaceManager_t1861067185_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlParserContext_t3629084577_il2cpp_TypeInfo_var;
extern Il2CppClass* Uri_t2776692961_il2cpp_TypeInfo_var;
extern Il2CppClass* StringReader_t2229325051_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlNodeType_t3966624571_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlException_t3490696160_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral34;
extern Il2CppCodeGenString* _stringLiteral1195861484;
extern Il2CppCodeGenString* _stringLiteral253268933;
extern const uint32_t XmlTextReader_InitializeContext_m4274125212_MetadataUsageId;
extern "C"  void XmlTextReader_InitializeContext_m4274125212 (XmlTextReader_t3066586409 * __this, String_t* ___url0, XmlParserContext_t3629084577 * ___context1, TextReader_t1534522647 * ___fragment2, int32_t ___fragType3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader_InitializeContext_m4274125212_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XmlNameTable_t3232213908 * V_0 = NULL;
	Uri_t2776692961 * V_1 = NULL;
	int32_t V_2 = 0;
	XmlTextReader_t3066586409 * G_B4_0 = NULL;
	XmlTextReader_t3066586409 * G_B3_0 = NULL;
	XmlNameTable_t3232213908 * G_B5_0 = NULL;
	XmlTextReader_t3066586409 * G_B5_1 = NULL;
	XmlTextReader_t3066586409 * G_B7_0 = NULL;
	XmlTextReader_t3066586409 * G_B6_0 = NULL;
	XmlNamespaceManager_t1861067185 * G_B8_0 = NULL;
	XmlTextReader_t3066586409 * G_B8_1 = NULL;
	{
		int32_t L_0 = ___fragType3;
		__this->set_startNodeType_39(L_0);
		XmlParserContext_t3629084577 * L_1 = ___context1;
		__this->set_parserContext_12(L_1);
		XmlParserContext_t3629084577 * L_2 = ___context1;
		if (L_2)
		{
			goto IL_0033;
		}
	}
	{
		NameTable_t2805661099 * L_3 = (NameTable_t2805661099 *)il2cpp_codegen_object_new(NameTable_t2805661099_il2cpp_TypeInfo_var);
		NameTable__ctor_m929780536(L_3, /*hidden argument*/NULL);
		V_0 = L_3;
		XmlNameTable_t3232213908 * L_4 = V_0;
		XmlNameTable_t3232213908 * L_5 = V_0;
		XmlNamespaceManager_t1861067185 * L_6 = (XmlNamespaceManager_t1861067185 *)il2cpp_codegen_object_new(XmlNamespaceManager_t1861067185_il2cpp_TypeInfo_var);
		XmlNamespaceManager__ctor_m1896995422(L_6, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		XmlParserContext_t3629084577 * L_8 = (XmlParserContext_t3629084577 *)il2cpp_codegen_object_new(XmlParserContext_t3629084577_il2cpp_TypeInfo_var);
		XmlParserContext__ctor_m857121144(L_8, L_4, L_6, L_7, 0, /*hidden argument*/NULL);
		__this->set_parserContext_12(L_8);
	}

IL_0033:
	{
		XmlParserContext_t3629084577 * L_9 = __this->get_parserContext_12();
		NullCheck(L_9);
		XmlNameTable_t3232213908 * L_10 = XmlParserContext_get_NameTable_m844308283(L_9, /*hidden argument*/NULL);
		__this->set_nameTable_13(L_10);
		XmlNameTable_t3232213908 * L_11 = __this->get_nameTable_13();
		G_B3_0 = __this;
		if (!L_11)
		{
			G_B4_0 = __this;
			goto IL_005b;
		}
	}
	{
		XmlNameTable_t3232213908 * L_12 = __this->get_nameTable_13();
		G_B5_0 = L_12;
		G_B5_1 = G_B3_0;
		goto IL_0060;
	}

IL_005b:
	{
		NameTable_t2805661099 * L_13 = (NameTable_t2805661099 *)il2cpp_codegen_object_new(NameTable_t2805661099_il2cpp_TypeInfo_var);
		NameTable__ctor_m929780536(L_13, /*hidden argument*/NULL);
		G_B5_0 = ((XmlNameTable_t3232213908 *)(L_13));
		G_B5_1 = G_B4_0;
	}

IL_0060:
	{
		NullCheck(G_B5_1);
		G_B5_1->set_nameTable_13(G_B5_0);
		XmlParserContext_t3629084577 * L_14 = __this->get_parserContext_12();
		NullCheck(L_14);
		XmlNamespaceManager_t1861067185 * L_15 = XmlParserContext_get_NamespaceManager_m3379582127(L_14, /*hidden argument*/NULL);
		__this->set_nsmgr_14(L_15);
		XmlNamespaceManager_t1861067185 * L_16 = __this->get_nsmgr_14();
		G_B6_0 = __this;
		if (!L_16)
		{
			G_B7_0 = __this;
			goto IL_008d;
		}
	}
	{
		XmlNamespaceManager_t1861067185 * L_17 = __this->get_nsmgr_14();
		G_B8_0 = L_17;
		G_B8_1 = G_B6_0;
		goto IL_0098;
	}

IL_008d:
	{
		XmlNameTable_t3232213908 * L_18 = __this->get_nameTable_13();
		XmlNamespaceManager_t1861067185 * L_19 = (XmlNamespaceManager_t1861067185 *)il2cpp_codegen_object_new(XmlNamespaceManager_t1861067185_il2cpp_TypeInfo_var);
		XmlNamespaceManager__ctor_m1896995422(L_19, L_18, /*hidden argument*/NULL);
		G_B8_0 = L_19;
		G_B8_1 = G_B7_0;
	}

IL_0098:
	{
		NullCheck(G_B8_1);
		G_B8_1->set_nsmgr_14(G_B8_0);
		String_t* L_20 = ___url0;
		if (!L_20)
		{
			goto IL_00c8;
		}
	}
	{
		String_t* L_21 = ___url0;
		NullCheck(L_21);
		int32_t L_22 = String_get_Length_m2979997331(L_21, /*hidden argument*/NULL);
		if ((((int32_t)L_22) <= ((int32_t)0)))
		{
			goto IL_00c8;
		}
	}
	{
		String_t* L_23 = ___url0;
		Uri_t2776692961 * L_24 = (Uri_t2776692961 *)il2cpp_codegen_object_new(Uri_t2776692961_il2cpp_TypeInfo_var);
		Uri__ctor_m2903273618(L_24, L_23, 0, /*hidden argument*/NULL);
		V_1 = L_24;
		XmlParserContext_t3629084577 * L_25 = __this->get_parserContext_12();
		Uri_t2776692961 * L_26 = V_1;
		NullCheck(L_26);
		String_t* L_27 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Uri::ToString() */, L_26);
		NullCheck(L_25);
		XmlParserContext_set_BaseURI_m663391444(L_25, L_27, /*hidden argument*/NULL);
	}

IL_00c8:
	{
		XmlTextReader_Init_m1899167282(__this, /*hidden argument*/NULL);
		TextReader_t1534522647 * L_28 = ___fragment2;
		__this->set_reader_28(L_28);
		int32_t L_29 = ___fragType3;
		V_2 = L_29;
		int32_t L_30 = V_2;
		if ((((int32_t)L_30) == ((int32_t)1)))
		{
			goto IL_0118;
		}
	}
	{
		int32_t L_31 = V_2;
		if ((((int32_t)L_31) == ((int32_t)2)))
		{
			goto IL_00f3;
		}
	}
	{
		int32_t L_32 = V_2;
		if ((((int32_t)L_32) == ((int32_t)((int32_t)9))))
		{
			goto IL_012b;
		}
	}
	{
		goto IL_0130;
	}

IL_00f3:
	{
		TextReader_t1534522647 * L_33 = ___fragment2;
		NullCheck(L_33);
		String_t* L_34 = VirtFuncInvoker0< String_t* >::Invoke(11 /* System.String System.IO.TextReader::ReadToEnd() */, L_33);
		NullCheck(L_34);
		String_t* L_35 = String_Replace_m2915759397(L_34, _stringLiteral34, _stringLiteral1195861484, /*hidden argument*/NULL);
		StringReader_t2229325051 * L_36 = (StringReader_t2229325051 *)il2cpp_codegen_object_new(StringReader_t2229325051_il2cpp_TypeInfo_var);
		StringReader__ctor_m1181104909(L_36, L_35, /*hidden argument*/NULL);
		__this->set_reader_28(L_36);
		goto IL_0147;
	}

IL_0118:
	{
		__this->set_currentState_40(1);
		__this->set_allowMultipleRoot_23((bool)1);
		goto IL_0147;
	}

IL_012b:
	{
		goto IL_0147;
	}

IL_0130:
	{
		int32_t L_37 = ___fragType3;
		int32_t L_38 = L_37;
		Il2CppObject * L_39 = Box(XmlNodeType_t3966624571_il2cpp_TypeInfo_var, &L_38);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_40 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral253268933, L_39, /*hidden argument*/NULL);
		XmlException_t3490696160 * L_41 = (XmlException_t3490696160 *)il2cpp_codegen_object_new(XmlException_t3490696160_il2cpp_TypeInfo_var);
		XmlException__ctor_m3226581679(L_41, L_40, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_41);
	}

IL_0147:
	{
		return;
	}
}
// System.Xml.ConformanceLevel Mono.Xml2.XmlTextReader::get_Conformance()
extern "C"  int32_t XmlTextReader_get_Conformance_m1190844346 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		bool L_0 = __this->get_allowMultipleRoot_23();
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		G_B3_0 = 1;
		goto IL_0012;
	}

IL_0011:
	{
		G_B3_0 = 2;
	}

IL_0012:
	{
		return (int32_t)(G_B3_0);
	}
}
// System.Void Mono.Xml2.XmlTextReader::set_Conformance(System.Xml.ConformanceLevel)
extern "C"  void XmlTextReader_set_Conformance_m2859698101 (XmlTextReader_t3066586409 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0015;
		}
	}
	{
		__this->set_currentState_40(1);
		__this->set_allowMultipleRoot_23((bool)1);
	}

IL_0015:
	{
		return;
	}
}
// System.Void Mono.Xml2.XmlTextReader::SetProperties(System.Xml.XmlNodeType,System.String,System.String,System.String,System.Boolean,System.String,System.Boolean)
extern "C"  void XmlTextReader_SetProperties_m3034003792 (XmlTextReader_t3066586409 * __this, int32_t ___nodeType0, String_t* ___name1, String_t* ___prefix2, String_t* ___localName3, bool ___isEmptyElement4, String_t* ___value5, bool ___clearAttributes6, const MethodInfo* method)
{
	{
		XmlTokenInfo_t2571680784 * L_0 = __this->get_currentToken_4();
		int32_t L_1 = ___nodeType0;
		String_t* L_2 = ___name1;
		String_t* L_3 = ___prefix2;
		String_t* L_4 = ___localName3;
		bool L_5 = ___isEmptyElement4;
		String_t* L_6 = ___value5;
		bool L_7 = ___clearAttributes6;
		XmlTextReader_SetTokenProperties_m237691509(__this, L_0, L_1, L_2, L_3, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		XmlTokenInfo_t2571680784 * L_8 = __this->get_currentToken_4();
		int32_t L_9 = __this->get_currentLinkedNodeLineNumber_36();
		NullCheck(L_8);
		L_8->set_LineNumber_8(L_9);
		XmlTokenInfo_t2571680784 * L_10 = __this->get_currentToken_4();
		int32_t L_11 = __this->get_currentLinkedNodeLinePosition_37();
		NullCheck(L_10);
		L_10->set_LinePosition_9(L_11);
		return;
	}
}
// System.Void Mono.Xml2.XmlTextReader::SetTokenProperties(Mono.Xml2.XmlTextReader/XmlTokenInfo,System.Xml.XmlNodeType,System.String,System.String,System.String,System.Boolean,System.String,System.Boolean)
extern "C"  void XmlTextReader_SetTokenProperties_m237691509 (XmlTextReader_t3066586409 * __this, XmlTokenInfo_t2571680784 * ___token0, int32_t ___nodeType1, String_t* ___name2, String_t* ___prefix3, String_t* ___localName4, bool ___isEmptyElement5, String_t* ___value6, bool ___clearAttributes7, const MethodInfo* method)
{
	{
		XmlTokenInfo_t2571680784 * L_0 = ___token0;
		int32_t L_1 = ___nodeType1;
		NullCheck(L_0);
		L_0->set_NodeType_12(L_1);
		XmlTokenInfo_t2571680784 * L_2 = ___token0;
		String_t* L_3 = ___name2;
		NullCheck(L_2);
		L_2->set_Name_2(L_3);
		XmlTokenInfo_t2571680784 * L_4 = ___token0;
		String_t* L_5 = ___prefix3;
		NullCheck(L_4);
		L_4->set_Prefix_4(L_5);
		XmlTokenInfo_t2571680784 * L_6 = ___token0;
		String_t* L_7 = ___localName4;
		NullCheck(L_6);
		L_6->set_LocalName_3(L_7);
		XmlTokenInfo_t2571680784 * L_8 = ___token0;
		bool L_9 = ___isEmptyElement5;
		NullCheck(L_8);
		L_8->set_IsEmptyElement_6(L_9);
		XmlTokenInfo_t2571680784 * L_10 = ___token0;
		String_t* L_11 = ___value6;
		NullCheck(L_10);
		VirtActionInvoker1< String_t* >::Invoke(5 /* System.Void Mono.Xml2.XmlTextReader/XmlTokenInfo::set_Value(System.String) */, L_10, L_11);
		int32_t L_12 = __this->get_depth_17();
		__this->set_elementDepth_18(L_12);
		bool L_13 = ___clearAttributes7;
		if (!L_13)
		{
			goto IL_0047;
		}
	}
	{
		XmlTextReader_ClearAttributes_m2951900356(__this, /*hidden argument*/NULL);
	}

IL_0047:
	{
		return;
	}
}
// System.Void Mono.Xml2.XmlTextReader::ClearAttributes()
extern "C"  void XmlTextReader_ClearAttributes_m2951900356 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	{
		__this->set_attributeCount_11(0);
		__this->set_currentAttribute_9((-1));
		__this->set_currentAttributeValue_10((-1));
		return;
	}
}
// System.Int32 Mono.Xml2.XmlTextReader::PeekSurrogate(System.Int32)
extern "C"  int32_t XmlTextReader_PeekSurrogate_m3783663408 (XmlTextReader_t3066586409 * __this, int32_t ___c0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = __this->get_peekCharsLength_31();
		int32_t L_1 = __this->get_peekCharsIndex_30();
		if ((((int32_t)L_0) > ((int32_t)((int32_t)((int32_t)L_1+(int32_t)1)))))
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_2 = ___c0;
		bool L_3 = XmlTextReader_ReadTextReader_m1148791089(__this, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_4 = ___c0;
		return L_4;
	}

IL_0021:
	{
		CharU5BU5D_t3416858730* L_5 = __this->get_peekChars_29();
		int32_t L_6 = __this->get_peekCharsIndex_30();
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		V_0 = ((L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7)));
		CharU5BU5D_t3416858730* L_8 = __this->get_peekChars_29();
		int32_t L_9 = __this->get_peekCharsIndex_30();
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, ((int32_t)((int32_t)L_9+(int32_t)1)));
		int32_t L_10 = ((int32_t)((int32_t)L_9+(int32_t)1));
		V_1 = ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10)));
		int32_t L_11 = V_0;
		if ((!(((uint32_t)((int32_t)((int32_t)L_11&(int32_t)((int32_t)64512)))) == ((uint32_t)((int32_t)55296)))))
		{
			goto IL_0061;
		}
	}
	{
		int32_t L_12 = V_1;
		if ((((int32_t)((int32_t)((int32_t)L_12&(int32_t)((int32_t)64512)))) == ((int32_t)((int32_t)56320))))
		{
			goto IL_0063;
		}
	}

IL_0061:
	{
		int32_t L_13 = V_0;
		return L_13;
	}

IL_0063:
	{
		int32_t L_14 = V_0;
		int32_t L_15 = V_1;
		return ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)65536)+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_14-(int32_t)((int32_t)55296)))*(int32_t)((int32_t)1024)))))+(int32_t)((int32_t)((int32_t)L_15-(int32_t)((int32_t)56320)))));
	}
}
// System.Int32 Mono.Xml2.XmlTextReader::PeekChar()
extern "C"  int32_t XmlTextReader_PeekChar_m254641297 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_peekCharsIndex_30();
		int32_t L_1 = __this->get_peekCharsLength_31();
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_0047;
		}
	}
	{
		CharU5BU5D_t3416858730* L_2 = __this->get_peekChars_29();
		int32_t L_3 = __this->get_peekCharsIndex_30();
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		V_0 = ((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4)));
		int32_t L_5 = V_0;
		if (L_5)
		{
			goto IL_0027;
		}
	}
	{
		return (-1);
	}

IL_0027:
	{
		int32_t L_6 = V_0;
		if ((((int32_t)L_6) < ((int32_t)((int32_t)55296))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_7 = V_0;
		if ((((int32_t)L_7) < ((int32_t)((int32_t)57343))))
		{
			goto IL_003f;
		}
	}

IL_003d:
	{
		int32_t L_8 = V_0;
		return L_8;
	}

IL_003f:
	{
		int32_t L_9 = V_0;
		int32_t L_10 = XmlTextReader_PeekSurrogate_m3783663408(__this, L_9, /*hidden argument*/NULL);
		return L_10;
	}

IL_0047:
	{
		bool L_11 = XmlTextReader_ReadTextReader_m1148791089(__this, (-1), /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_0055;
		}
	}
	{
		return (-1);
	}

IL_0055:
	{
		int32_t L_12 = XmlTextReader_PeekChar_m254641297(__this, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Int32 Mono.Xml2.XmlTextReader::ReadChar()
extern "C"  int32_t XmlTextReader_ReadChar_m3857716364 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = XmlTextReader_PeekChar_m254641297(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = __this->get_peekCharsIndex_30();
		__this->set_peekCharsIndex_30(((int32_t)((int32_t)L_1+(int32_t)1)));
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) < ((int32_t)((int32_t)65536))))
		{
			goto IL_002e;
		}
	}
	{
		int32_t L_3 = __this->get_peekCharsIndex_30();
		__this->set_peekCharsIndex_30(((int32_t)((int32_t)L_3+(int32_t)1)));
	}

IL_002e:
	{
		int32_t L_4 = V_0;
		if ((!(((uint32_t)L_4) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_0050;
		}
	}
	{
		int32_t L_5 = __this->get_line_34();
		__this->set_line_34(((int32_t)((int32_t)L_5+(int32_t)1)));
		__this->set_column_35(1);
		goto IL_0065;
	}

IL_0050:
	{
		int32_t L_6 = V_0;
		if ((((int32_t)L_6) == ((int32_t)(-1))))
		{
			goto IL_0065;
		}
	}
	{
		int32_t L_7 = __this->get_column_35();
		__this->set_column_35(((int32_t)((int32_t)L_7+(int32_t)1)));
	}

IL_0065:
	{
		int32_t L_8 = V_0;
		return L_8;
	}
}
// System.Void Mono.Xml2.XmlTextReader::Advance(System.Int32)
extern "C"  void XmlTextReader_Advance_m673767315 (XmlTextReader_t3066586409 * __this, int32_t ___ch0, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_peekCharsIndex_30();
		__this->set_peekCharsIndex_30(((int32_t)((int32_t)L_0+(int32_t)1)));
		int32_t L_1 = ___ch0;
		if ((((int32_t)L_1) < ((int32_t)((int32_t)65536))))
		{
			goto IL_0027;
		}
	}
	{
		int32_t L_2 = __this->get_peekCharsIndex_30();
		__this->set_peekCharsIndex_30(((int32_t)((int32_t)L_2+(int32_t)1)));
	}

IL_0027:
	{
		int32_t L_3 = ___ch0;
		if ((!(((uint32_t)L_3) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_0049;
		}
	}
	{
		int32_t L_4 = __this->get_line_34();
		__this->set_line_34(((int32_t)((int32_t)L_4+(int32_t)1)));
		__this->set_column_35(1);
		goto IL_005e;
	}

IL_0049:
	{
		int32_t L_5 = ___ch0;
		if ((((int32_t)L_5) == ((int32_t)(-1))))
		{
			goto IL_005e;
		}
	}
	{
		int32_t L_6 = __this->get_column_35();
		__this->set_column_35(((int32_t)((int32_t)L_6+(int32_t)1)));
	}

IL_005e:
	{
		return;
	}
}
// System.Boolean Mono.Xml2.XmlTextReader::ReadTextReader(System.Int32)
extern Il2CppClass* CharU5BU5D_t3416858730_il2cpp_TypeInfo_var;
extern const uint32_t XmlTextReader_ReadTextReader_m1148791089_MetadataUsageId;
extern "C"  bool XmlTextReader_ReadTextReader_m1148791089 (XmlTextReader_t3066586409 * __this, int32_t ___remained0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader_ReadTextReader_m1148791089_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	CharU5BU5D_t3416858730* V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = __this->get_peekCharsLength_31();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0036;
		}
	}
	{
		TextReader_t1534522647 * L_1 = __this->get_reader_28();
		CharU5BU5D_t3416858730* L_2 = __this->get_peekChars_29();
		CharU5BU5D_t3416858730* L_3 = __this->get_peekChars_29();
		NullCheck(L_3);
		NullCheck(L_1);
		int32_t L_4 = VirtFuncInvoker3< int32_t, CharU5BU5D_t3416858730*, int32_t, int32_t >::Invoke(9 /* System.Int32 System.IO.TextReader::Read(System.Char[],System.Int32,System.Int32) */, L_1, L_2, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))));
		__this->set_peekCharsLength_31(L_4);
		int32_t L_5 = __this->get_peekCharsLength_31();
		return (bool)((((int32_t)L_5) > ((int32_t)0))? 1 : 0);
	}

IL_0036:
	{
		int32_t L_6 = ___remained0;
		if ((((int32_t)L_6) < ((int32_t)0)))
		{
			goto IL_0043;
		}
	}
	{
		G_B5_0 = 1;
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		V_0 = G_B5_0;
		int32_t L_7 = __this->get_peekCharsLength_31();
		int32_t L_8 = __this->get_curNodePeekIndex_32();
		V_1 = ((int32_t)((int32_t)L_7-(int32_t)L_8));
		bool L_9 = __this->get_preserveCurrentTag_33();
		if (L_9)
		{
			goto IL_0071;
		}
	}
	{
		__this->set_curNodePeekIndex_32(0);
		__this->set_peekCharsIndex_30(0);
		goto IL_0101;
	}

IL_0071:
	{
		int32_t L_10 = __this->get_peekCharsLength_31();
		CharU5BU5D_t3416858730* L_11 = __this->get_peekChars_29();
		NullCheck(L_11);
		if ((((int32_t)L_10) >= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0089;
		}
	}
	{
		goto IL_0101;
	}

IL_0089:
	{
		int32_t L_12 = __this->get_curNodePeekIndex_32();
		int32_t L_13 = __this->get_peekCharsLength_31();
		if ((((int32_t)L_12) > ((int32_t)((int32_t)((int32_t)L_13>>(int32_t)1)))))
		{
			goto IL_00da;
		}
	}
	{
		CharU5BU5D_t3416858730* L_14 = __this->get_peekChars_29();
		NullCheck(L_14);
		V_2 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length))))*(int32_t)2))));
		CharU5BU5D_t3416858730* L_15 = __this->get_peekChars_29();
		int32_t L_16 = __this->get_curNodePeekIndex_32();
		CharU5BU5D_t3416858730* L_17 = V_2;
		int32_t L_18 = V_1;
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_15, L_16, (Il2CppArray *)(Il2CppArray *)L_17, 0, L_18, /*hidden argument*/NULL);
		CharU5BU5D_t3416858730* L_19 = V_2;
		__this->set_peekChars_29(L_19);
		__this->set_curNodePeekIndex_32(0);
		int32_t L_20 = V_1;
		__this->set_peekCharsIndex_30(L_20);
		goto IL_0101;
	}

IL_00da:
	{
		CharU5BU5D_t3416858730* L_21 = __this->get_peekChars_29();
		int32_t L_22 = __this->get_curNodePeekIndex_32();
		CharU5BU5D_t3416858730* L_23 = __this->get_peekChars_29();
		int32_t L_24 = V_1;
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_21, L_22, (Il2CppArray *)(Il2CppArray *)L_23, 0, L_24, /*hidden argument*/NULL);
		__this->set_curNodePeekIndex_32(0);
		int32_t L_25 = V_1;
		__this->set_peekCharsIndex_30(L_25);
	}

IL_0101:
	{
		int32_t L_26 = ___remained0;
		if ((((int32_t)L_26) < ((int32_t)0)))
		{
			goto IL_0117;
		}
	}
	{
		CharU5BU5D_t3416858730* L_27 = __this->get_peekChars_29();
		int32_t L_28 = __this->get_peekCharsIndex_30();
		int32_t L_29 = ___remained0;
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, L_28);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(L_28), (uint16_t)(((int32_t)((uint16_t)L_29))));
	}

IL_0117:
	{
		CharU5BU5D_t3416858730* L_30 = __this->get_peekChars_29();
		NullCheck(L_30);
		int32_t L_31 = __this->get_peekCharsIndex_30();
		int32_t L_32 = V_0;
		V_3 = ((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_30)->max_length))))-(int32_t)L_31))-(int32_t)L_32));
		int32_t L_33 = V_3;
		if ((((int32_t)L_33) <= ((int32_t)((int32_t)1024))))
		{
			goto IL_013a;
		}
	}
	{
		V_3 = ((int32_t)1024);
	}

IL_013a:
	{
		TextReader_t1534522647 * L_34 = __this->get_reader_28();
		CharU5BU5D_t3416858730* L_35 = __this->get_peekChars_29();
		int32_t L_36 = __this->get_peekCharsIndex_30();
		int32_t L_37 = V_0;
		int32_t L_38 = V_3;
		NullCheck(L_34);
		int32_t L_39 = VirtFuncInvoker3< int32_t, CharU5BU5D_t3416858730*, int32_t, int32_t >::Invoke(9 /* System.Int32 System.IO.TextReader::Read(System.Char[],System.Int32,System.Int32) */, L_34, L_35, ((int32_t)((int32_t)L_36+(int32_t)L_37)), L_38);
		V_4 = L_39;
		int32_t L_40 = V_0;
		int32_t L_41 = V_4;
		V_5 = ((int32_t)((int32_t)L_40+(int32_t)L_41));
		int32_t L_42 = __this->get_peekCharsIndex_30();
		int32_t L_43 = V_5;
		__this->set_peekCharsLength_31(((int32_t)((int32_t)L_42+(int32_t)L_43)));
		int32_t L_44 = V_5;
		return (bool)((((int32_t)((((int32_t)L_44) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean Mono.Xml2.XmlTextReader::ReadContent()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1090932143;
extern const uint32_t XmlTextReader_ReadContent_m3354608395_MetadataUsageId;
extern "C"  bool XmlTextReader_ReadContent_m3354608395 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader_ReadContent_m3354608395_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		bool L_0 = __this->get_popScope_20();
		if (!L_0)
		{
			goto IL_0029;
		}
	}
	{
		XmlNamespaceManager_t1861067185 * L_1 = __this->get_nsmgr_14();
		NullCheck(L_1);
		VirtFuncInvoker0< bool >::Invoke(12 /* System.Boolean System.Xml.XmlNamespaceManager::PopScope() */, L_1);
		XmlParserContext_t3629084577 * L_2 = __this->get_parserContext_12();
		NullCheck(L_2);
		XmlParserContext_PopScope_m706589621(L_2, /*hidden argument*/NULL);
		__this->set_popScope_20((bool)0);
	}

IL_0029:
	{
		bool L_3 = __this->get_returnEntityReference_25();
		if (!L_3)
		{
			goto IL_003f;
		}
	}
	{
		XmlTextReader_SetEntityReferenceProperties_m614061979(__this, /*hidden argument*/NULL);
		goto IL_0168;
	}

IL_003f:
	{
		int32_t L_4 = XmlTextReader_PeekChar_m254641297(__this, /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((!(((uint32_t)L_5) == ((uint32_t)(-1)))))
		{
			goto IL_009d;
		}
	}
	{
		__this->set_readState_15(3);
		XmlTextReader_ClearValueBuffer_m495962918(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_7 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_8 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		XmlTextReader_SetProperties_m3034003792(__this, 0, L_6, L_7, L_8, (bool)0, (String_t*)NULL, (bool)1, /*hidden argument*/NULL);
		int32_t L_9 = __this->get_depth_17();
		if ((((int32_t)L_9) <= ((int32_t)0)))
		{
			goto IL_009b;
		}
	}
	{
		int32_t L_10 = __this->get_depth_17();
		int32_t L_11 = L_10;
		Il2CppObject * L_12 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_11);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral1090932143, L_12, /*hidden argument*/NULL);
		XmlException_t3490696160 * L_14 = XmlTextReader_NotWFError_m304583423(__this, L_13, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_14);
	}

IL_009b:
	{
		return (bool)0;
	}

IL_009d:
	{
		int32_t L_15 = V_0;
		V_1 = L_15;
		int32_t L_16 = V_1;
		if (((int32_t)((int32_t)L_16-(int32_t)((int32_t)9))) == 0)
		{
			goto IL_0145;
		}
		if (((int32_t)((int32_t)L_16-(int32_t)((int32_t)9))) == 1)
		{
			goto IL_0145;
		}
		if (((int32_t)((int32_t)L_16-(int32_t)((int32_t)9))) == 2)
		{
			goto IL_00bc;
		}
		if (((int32_t)((int32_t)L_16-(int32_t)((int32_t)9))) == 3)
		{
			goto IL_00bc;
		}
		if (((int32_t)((int32_t)L_16-(int32_t)((int32_t)9))) == 4)
		{
			goto IL_0145;
		}
	}

IL_00bc:
	{
		int32_t L_17 = V_1;
		if ((((int32_t)L_17) == ((int32_t)((int32_t)32))))
		{
			goto IL_0145;
		}
	}
	{
		int32_t L_18 = V_1;
		if ((((int32_t)L_18) == ((int32_t)((int32_t)60))))
		{
			goto IL_00d1;
		}
	}
	{
		goto IL_015c;
	}

IL_00d1:
	{
		int32_t L_19 = V_0;
		XmlTextReader_Advance_m673767315(__this, L_19, /*hidden argument*/NULL);
		int32_t L_20 = XmlTextReader_PeekChar_m254641297(__this, /*hidden argument*/NULL);
		V_2 = L_20;
		int32_t L_21 = V_2;
		if ((((int32_t)L_21) == ((int32_t)((int32_t)33))))
		{
			goto IL_0122;
		}
	}
	{
		int32_t L_22 = V_2;
		if ((((int32_t)L_22) == ((int32_t)((int32_t)47))))
		{
			goto IL_00fc;
		}
	}
	{
		int32_t L_23 = V_2;
		if ((((int32_t)L_23) == ((int32_t)((int32_t)63))))
		{
			goto IL_010f;
		}
	}
	{
		goto IL_0135;
	}

IL_00fc:
	{
		XmlTextReader_Advance_m673767315(__this, ((int32_t)47), /*hidden argument*/NULL);
		XmlTextReader_ReadEndTag_m3759677719(__this, /*hidden argument*/NULL);
		goto IL_0140;
	}

IL_010f:
	{
		XmlTextReader_Advance_m673767315(__this, ((int32_t)63), /*hidden argument*/NULL);
		XmlTextReader_ReadProcessingInstruction_m4247906757(__this, /*hidden argument*/NULL);
		goto IL_0140;
	}

IL_0122:
	{
		XmlTextReader_Advance_m673767315(__this, ((int32_t)33), /*hidden argument*/NULL);
		XmlTextReader_ReadDeclaration_m3359070020(__this, /*hidden argument*/NULL);
		goto IL_0140;
	}

IL_0135:
	{
		XmlTextReader_ReadStartTag_m721786928(__this, /*hidden argument*/NULL);
		goto IL_0140;
	}

IL_0140:
	{
		goto IL_0168;
	}

IL_0145:
	{
		bool L_24 = XmlTextReader_ReadWhitespace_m885165773(__this, /*hidden argument*/NULL);
		if (L_24)
		{
			goto IL_0157;
		}
	}
	{
		bool L_25 = XmlTextReader_ReadContent_m3354608395(__this, /*hidden argument*/NULL);
		return L_25;
	}

IL_0157:
	{
		goto IL_0168;
	}

IL_015c:
	{
		XmlTextReader_ReadText_m502843516(__this, (bool)1, /*hidden argument*/NULL);
		goto IL_0168;
	}

IL_0168:
	{
		int32_t L_26 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Xml.ReadState Mono.Xml2.XmlTextReader::get_ReadState() */, __this);
		return (bool)((((int32_t)((((int32_t)L_26) == ((int32_t)3))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void Mono.Xml2.XmlTextReader::SetEntityReferenceProperties()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4074231567;
extern Il2CppCodeGenString* _stringLiteral3598558800;
extern const uint32_t XmlTextReader_SetEntityReferenceProperties_m614061979_MetadataUsageId;
extern "C"  void XmlTextReader_SetEntityReferenceProperties_m614061979 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader_SetEntityReferenceProperties_m614061979_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DTDEntityDeclaration_t2806387719 * V_0 = NULL;
	DTDEntityDeclaration_t2806387719 * G_B3_0 = NULL;
	{
		DTDObjectModel_t709926554 * L_0 = XmlTextReader_get_DTD_m3845091782(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		DTDObjectModel_t709926554 * L_1 = XmlTextReader_get_DTD_m3845091782(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		DTDEntityDeclarationCollection_t4226410245 * L_2 = DTDObjectModel_get_EntityDecls_m3251787732(L_1, /*hidden argument*/NULL);
		String_t* L_3 = __this->get_entityReferenceName_26();
		NullCheck(L_2);
		DTDEntityDeclaration_t2806387719 * L_4 = DTDEntityDeclarationCollection_get_Item_m2370831310(L_2, L_3, /*hidden argument*/NULL);
		G_B3_0 = L_4;
		goto IL_0027;
	}

IL_0026:
	{
		G_B3_0 = ((DTDEntityDeclaration_t2806387719 *)(NULL));
	}

IL_0027:
	{
		V_0 = G_B3_0;
		bool L_5 = __this->get_isStandalone_24();
		if (!L_5)
		{
			goto IL_005b;
		}
	}
	{
		DTDObjectModel_t709926554 * L_6 = XmlTextReader_get_DTD_m3845091782(__this, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_004f;
		}
	}
	{
		DTDEntityDeclaration_t2806387719 * L_7 = V_0;
		if (!L_7)
		{
			goto IL_004f;
		}
	}
	{
		DTDEntityDeclaration_t2806387719 * L_8 = V_0;
		NullCheck(L_8);
		bool L_9 = DTDNode_get_IsInternalSubset_m3131993229(L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_005b;
		}
	}

IL_004f:
	{
		XmlException_t3490696160 * L_10 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral4074231567, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
	}

IL_005b:
	{
		DTDEntityDeclaration_t2806387719 * L_11 = V_0;
		if (!L_11)
		{
			goto IL_0078;
		}
	}
	{
		DTDEntityDeclaration_t2806387719 * L_12 = V_0;
		NullCheck(L_12);
		String_t* L_13 = DTDEntityDeclaration_get_NotationName_m528268507(L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0078;
		}
	}
	{
		XmlException_t3490696160 * L_14 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral3598558800, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_14);
	}

IL_0078:
	{
		XmlTextReader_ClearValueBuffer_m495962918(__this, /*hidden argument*/NULL);
		String_t* L_15 = __this->get_entityReferenceName_26();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_17 = __this->get_entityReferenceName_26();
		XmlTextReader_SetProperties_m3034003792(__this, 5, L_15, L_16, L_17, (bool)0, (String_t*)NULL, (bool)1, /*hidden argument*/NULL);
		__this->set_returnEntityReference_25((bool)0);
		String_t* L_18 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_entityReferenceName_26(L_18);
		return;
	}
}
// System.Void Mono.Xml2.XmlTextReader::ReadStartTag()
extern Il2CppClass* XmlChar_t3591879093_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlTextReader_t3066586409_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t190145395_il2cpp_TypeInfo_var;
extern Il2CppClass* Uri_t2776692961_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1958628151_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1806928380;
extern Il2CppCodeGenString* _stringLiteral1965431777;
extern Il2CppCodeGenString* _stringLiteral114177052;
extern Il2CppCodeGenString* _stringLiteral2109719301;
extern Il2CppCodeGenString* _stringLiteral3598455478;
extern Il2CppCodeGenString* _stringLiteral1382260036;
extern Il2CppCodeGenString* _stringLiteral118807;
extern Il2CppCodeGenString* _stringLiteral3016401;
extern Il2CppCodeGenString* _stringLiteral3314158;
extern Il2CppCodeGenString* _stringLiteral109637894;
extern Il2CppCodeGenString* _stringLiteral3018305100;
extern Il2CppCodeGenString* _stringLiteral1544803905;
extern Il2CppCodeGenString* _stringLiteral137844567;
extern const uint32_t XmlTextReader_ReadStartTag_m721786928_MetadataUsageId;
extern "C"  void XmlTextReader_ReadStartTag_m721786928 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader_ReadStartTag_m721786928_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	bool V_3 = false;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	String_t* V_11 = NULL;
	String_t* V_12 = NULL;
	Uri_t2776692961 * V_13 = NULL;
	Uri_t2776692961 * V_14 = NULL;
	String_t* V_15 = NULL;
	Dictionary_2_t190145395 * V_16 = NULL;
	int32_t V_17 = 0;
	String_t* V_18 = NULL;
	int32_t V_19 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	Uri_t2776692961 * G_B59_0 = NULL;
	XmlParserContext_t3629084577 * G_B61_0 = NULL;
	XmlParserContext_t3629084577 * G_B60_0 = NULL;
	String_t* G_B62_0 = NULL;
	XmlParserContext_t3629084577 * G_B62_1 = NULL;
	{
		int32_t L_0 = __this->get_currentState_40();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)15)))))
		{
			goto IL_0019;
		}
	}
	{
		XmlException_t3490696160 * L_1 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral1806928380, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0019:
	{
		__this->set_currentState_40(1);
		XmlNamespaceManager_t1861067185 * L_2 = __this->get_nsmgr_14();
		NullCheck(L_2);
		VirtActionInvoker0::Invoke(13 /* System.Void System.Xml.XmlNamespaceManager::PushScope() */, L_2);
		int32_t L_3 = __this->get_line_34();
		__this->set_currentLinkedNodeLineNumber_36(L_3);
		int32_t L_4 = __this->get_column_35();
		__this->set_currentLinkedNodeLinePosition_37(L_4);
		String_t* L_5 = XmlTextReader_ReadName_m2367194790(__this, (&V_0), (&V_1), /*hidden argument*/NULL);
		V_2 = L_5;
		int32_t L_6 = __this->get_currentState_40();
		if ((!(((uint32_t)L_6) == ((uint32_t)((int32_t)15)))))
		{
			goto IL_0067;
		}
	}
	{
		XmlException_t3490696160 * L_7 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral1965431777, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0067:
	{
		V_3 = (bool)0;
		XmlTextReader_ClearAttributes_m2951900356(__this, /*hidden argument*/NULL);
		XmlTextReader_SkipWhitespace_m1854637366(__this, /*hidden argument*/NULL);
		int32_t L_8 = XmlTextReader_PeekChar_m254641297(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(XmlChar_t3591879093_il2cpp_TypeInfo_var);
		bool L_9 = XmlChar_IsFirstNameChar_m3587024924(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_008d;
		}
	}
	{
		XmlTextReader_ReadAttributes_m1335987046(__this, (bool)0, /*hidden argument*/NULL);
	}

IL_008d:
	{
		XmlTokenInfo_t2571680784 * L_10 = __this->get_currentToken_4();
		__this->set_cursorToken_3(L_10);
		V_4 = 0;
		goto IL_00b5;
	}

IL_00a1:
	{
		XmlAttributeTokenInfoU5BU5D_t2707514583* L_11 = __this->get_attributeTokens_7();
		int32_t L_12 = V_4;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		int32_t L_13 = L_12;
		NullCheck(((L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13))));
		XmlAttributeTokenInfo_FillXmlns_m2684533414(((L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13))), /*hidden argument*/NULL);
		int32_t L_14 = V_4;
		V_4 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_00b5:
	{
		int32_t L_15 = V_4;
		int32_t L_16 = __this->get_attributeCount_11();
		if ((((int32_t)L_15) < ((int32_t)L_16)))
		{
			goto IL_00a1;
		}
	}
	{
		V_5 = 0;
		goto IL_00de;
	}

IL_00ca:
	{
		XmlAttributeTokenInfoU5BU5D_t2707514583* L_17 = __this->get_attributeTokens_7();
		int32_t L_18 = V_5;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		int32_t L_19 = L_18;
		NullCheck(((L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19))));
		XmlAttributeTokenInfo_FillNamespace_m1012079525(((L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19))), /*hidden argument*/NULL);
		int32_t L_20 = V_5;
		V_5 = ((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_00de:
	{
		int32_t L_21 = V_5;
		int32_t L_22 = __this->get_attributeCount_11();
		if ((((int32_t)L_21) < ((int32_t)L_22)))
		{
			goto IL_00ca;
		}
	}
	{
		bool L_23 = __this->get_namespaces_44();
		if (!L_23)
		{
			goto IL_0157;
		}
	}
	{
		V_6 = 0;
		goto IL_014a;
	}

IL_00fe:
	{
		XmlAttributeTokenInfoU5BU5D_t2707514583* L_24 = __this->get_attributeTokens_7();
		int32_t L_25 = V_6;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, L_25);
		int32_t L_26 = L_25;
		NullCheck(((L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_26))));
		String_t* L_27 = ((XmlTokenInfo_t2571680784 *)((L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_26))))->get_Prefix_4();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_28 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_27, _stringLiteral114177052, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_0144;
		}
	}
	{
		XmlAttributeTokenInfoU5BU5D_t2707514583* L_29 = __this->get_attributeTokens_7();
		int32_t L_30 = V_6;
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, L_30);
		int32_t L_31 = L_30;
		NullCheck(((L_29)->GetAt(static_cast<il2cpp_array_size_t>(L_31))));
		String_t* L_32 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String Mono.Xml2.XmlTextReader/XmlAttributeTokenInfo::get_Value() */, ((L_29)->GetAt(static_cast<il2cpp_array_size_t>(L_31))));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_33 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_34 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_32, L_33, /*hidden argument*/NULL);
		if (!L_34)
		{
			goto IL_0144;
		}
	}
	{
		XmlException_t3490696160 * L_35 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral2109719301, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_35);
	}

IL_0144:
	{
		int32_t L_36 = V_6;
		V_6 = ((int32_t)((int32_t)L_36+(int32_t)1));
	}

IL_014a:
	{
		int32_t L_37 = V_6;
		int32_t L_38 = __this->get_attributeCount_11();
		if ((((int32_t)L_37) < ((int32_t)L_38)))
		{
			goto IL_00fe;
		}
	}

IL_0157:
	{
		V_7 = 0;
		goto IL_0201;
	}

IL_015f:
	{
		int32_t L_39 = V_7;
		V_8 = ((int32_t)((int32_t)L_39+(int32_t)1));
		goto IL_01ee;
	}

IL_016a:
	{
		XmlAttributeTokenInfoU5BU5D_t2707514583* L_40 = __this->get_attributeTokens_7();
		int32_t L_41 = V_7;
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, L_41);
		int32_t L_42 = L_41;
		NullCheck(((L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42))));
		String_t* L_43 = ((XmlTokenInfo_t2571680784 *)((L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42))))->get_Name_2();
		XmlAttributeTokenInfoU5BU5D_t2707514583* L_44 = __this->get_attributeTokens_7();
		int32_t L_45 = V_8;
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, L_45);
		int32_t L_46 = L_45;
		NullCheck(((L_44)->GetAt(static_cast<il2cpp_array_size_t>(L_46))));
		String_t* L_47 = ((XmlTokenInfo_t2571680784 *)((L_44)->GetAt(static_cast<il2cpp_array_size_t>(L_46))))->get_Name_2();
		bool L_48 = Object_ReferenceEquals_m3695130242(NULL /*static, unused*/, L_43, L_47, /*hidden argument*/NULL);
		if (L_48)
		{
			goto IL_01dc;
		}
	}
	{
		XmlAttributeTokenInfoU5BU5D_t2707514583* L_49 = __this->get_attributeTokens_7();
		int32_t L_50 = V_7;
		NullCheck(L_49);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_49, L_50);
		int32_t L_51 = L_50;
		NullCheck(((L_49)->GetAt(static_cast<il2cpp_array_size_t>(L_51))));
		String_t* L_52 = ((XmlTokenInfo_t2571680784 *)((L_49)->GetAt(static_cast<il2cpp_array_size_t>(L_51))))->get_LocalName_3();
		XmlAttributeTokenInfoU5BU5D_t2707514583* L_53 = __this->get_attributeTokens_7();
		int32_t L_54 = V_8;
		NullCheck(L_53);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_53, L_54);
		int32_t L_55 = L_54;
		NullCheck(((L_53)->GetAt(static_cast<il2cpp_array_size_t>(L_55))));
		String_t* L_56 = ((XmlTokenInfo_t2571680784 *)((L_53)->GetAt(static_cast<il2cpp_array_size_t>(L_55))))->get_LocalName_3();
		bool L_57 = Object_ReferenceEquals_m3695130242(NULL /*static, unused*/, L_52, L_56, /*hidden argument*/NULL);
		if (!L_57)
		{
			goto IL_01e8;
		}
	}
	{
		XmlAttributeTokenInfoU5BU5D_t2707514583* L_58 = __this->get_attributeTokens_7();
		int32_t L_59 = V_7;
		NullCheck(L_58);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_58, L_59);
		int32_t L_60 = L_59;
		NullCheck(((L_58)->GetAt(static_cast<il2cpp_array_size_t>(L_60))));
		String_t* L_61 = ((XmlTokenInfo_t2571680784 *)((L_58)->GetAt(static_cast<il2cpp_array_size_t>(L_60))))->get_NamespaceURI_5();
		XmlAttributeTokenInfoU5BU5D_t2707514583* L_62 = __this->get_attributeTokens_7();
		int32_t L_63 = V_8;
		NullCheck(L_62);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_62, L_63);
		int32_t L_64 = L_63;
		NullCheck(((L_62)->GetAt(static_cast<il2cpp_array_size_t>(L_64))));
		String_t* L_65 = ((XmlTokenInfo_t2571680784 *)((L_62)->GetAt(static_cast<il2cpp_array_size_t>(L_64))))->get_NamespaceURI_5();
		bool L_66 = Object_ReferenceEquals_m3695130242(NULL /*static, unused*/, L_61, L_65, /*hidden argument*/NULL);
		if (!L_66)
		{
			goto IL_01e8;
		}
	}

IL_01dc:
	{
		XmlException_t3490696160 * L_67 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral3598455478, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_67);
	}

IL_01e8:
	{
		int32_t L_68 = V_8;
		V_8 = ((int32_t)((int32_t)L_68+(int32_t)1));
	}

IL_01ee:
	{
		int32_t L_69 = V_8;
		int32_t L_70 = __this->get_attributeCount_11();
		if ((((int32_t)L_69) < ((int32_t)L_70)))
		{
			goto IL_016a;
		}
	}
	{
		int32_t L_71 = V_7;
		V_7 = ((int32_t)((int32_t)L_71+(int32_t)1));
	}

IL_0201:
	{
		int32_t L_72 = V_7;
		int32_t L_73 = __this->get_attributeCount_11();
		if ((((int32_t)L_72) < ((int32_t)L_73)))
		{
			goto IL_015f;
		}
	}
	{
		int32_t L_74 = XmlTextReader_PeekChar_m254641297(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_74) == ((uint32_t)((int32_t)47)))))
		{
			goto IL_0231;
		}
	}
	{
		XmlTextReader_Advance_m673767315(__this, ((int32_t)47), /*hidden argument*/NULL);
		V_3 = (bool)1;
		__this->set_popScope_20((bool)1);
		goto IL_0241;
	}

IL_0231:
	{
		__this->set_depthUp_19((bool)1);
		String_t* L_75 = V_2;
		String_t* L_76 = V_1;
		String_t* L_77 = V_0;
		XmlTextReader_PushElementName_m1658182381(__this, L_75, L_76, L_77, /*hidden argument*/NULL);
	}

IL_0241:
	{
		XmlParserContext_t3629084577 * L_78 = __this->get_parserContext_12();
		NullCheck(L_78);
		XmlParserContext_PushScope_m2997966602(L_78, /*hidden argument*/NULL);
		XmlTextReader_Expect_m2788845516(__this, ((int32_t)62), /*hidden argument*/NULL);
		String_t* L_79 = V_2;
		String_t* L_80 = V_0;
		String_t* L_81 = V_1;
		bool L_82 = V_3;
		XmlTextReader_SetProperties_m3034003792(__this, 1, L_79, L_80, L_81, L_82, (String_t*)NULL, (bool)0, /*hidden argument*/NULL);
		String_t* L_83 = V_0;
		NullCheck(L_83);
		int32_t L_84 = String_get_Length_m2979997331(L_83, /*hidden argument*/NULL);
		if ((((int32_t)L_84) <= ((int32_t)0)))
		{
			goto IL_0285;
		}
	}
	{
		XmlTokenInfo_t2571680784 * L_85 = __this->get_currentToken_4();
		String_t* L_86 = V_0;
		String_t* L_87 = XmlTextReader_LookupNamespace_m3699761695(__this, L_86, (bool)1, /*hidden argument*/NULL);
		NullCheck(L_85);
		L_85->set_NamespaceURI_5(L_87);
		goto IL_02a6;
	}

IL_0285:
	{
		bool L_88 = __this->get_namespaces_44();
		if (!L_88)
		{
			goto IL_02a6;
		}
	}
	{
		XmlTokenInfo_t2571680784 * L_89 = __this->get_currentToken_4();
		XmlNamespaceManager_t1861067185 * L_90 = __this->get_nsmgr_14();
		NullCheck(L_90);
		String_t* L_91 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Xml.XmlNamespaceManager::get_DefaultNamespace() */, L_90);
		NullCheck(L_89);
		L_89->set_NamespaceURI_5(L_91);
	}

IL_02a6:
	{
		bool L_92 = __this->get_namespaces_44();
		if (!L_92)
		{
			goto IL_0325;
		}
	}
	{
		String_t* L_93 = VirtFuncInvoker0< String_t* >::Invoke(16 /* System.String Mono.Xml2.XmlTextReader::get_NamespaceURI() */, __this);
		if (L_93)
		{
			goto IL_02d3;
		}
	}
	{
		String_t* L_94 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String Mono.Xml2.XmlTextReader::get_Prefix() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_95 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral1382260036, L_94, /*hidden argument*/NULL);
		XmlException_t3490696160 * L_96 = XmlTextReader_NotWFError_m304583423(__this, L_95, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_96);
	}

IL_02d3:
	try
	{ // begin try (depth: 1)
		{
			V_9 = 0;
			goto IL_030b;
		}

IL_02db:
		{
			int32_t L_97 = V_9;
			VirtActionInvoker1< int32_t >::Invoke(32 /* System.Void Mono.Xml2.XmlTextReader::MoveToAttribute(System.Int32) */, __this, L_97);
			String_t* L_98 = VirtFuncInvoker0< String_t* >::Invoke(16 /* System.String Mono.Xml2.XmlTextReader::get_NamespaceURI() */, __this);
			if (L_98)
			{
				goto IL_0305;
			}
		}

IL_02ee:
		{
			String_t* L_99 = VirtFuncInvoker0< String_t* >::Invoke(19 /* System.String Mono.Xml2.XmlTextReader::get_Prefix() */, __this);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_100 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral1382260036, L_99, /*hidden argument*/NULL);
			XmlException_t3490696160 * L_101 = XmlTextReader_NotWFError_m304583423(__this, L_100, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_101);
		}

IL_0305:
		{
			int32_t L_102 = V_9;
			V_9 = ((int32_t)((int32_t)L_102+(int32_t)1));
		}

IL_030b:
		{
			int32_t L_103 = V_9;
			int32_t L_104 = __this->get_attributeCount_11();
			if ((((int32_t)L_103) < ((int32_t)L_104)))
			{
				goto IL_02db;
			}
		}

IL_0318:
		{
			IL2CPP_LEAVE(0x325, FINALLY_031d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_031d;
	}

FINALLY_031d:
	{ // begin finally (depth: 1)
		VirtFuncInvoker0< bool >::Invoke(35 /* System.Boolean Mono.Xml2.XmlTextReader::MoveToElement() */, __this);
		IL2CPP_END_FINALLY(797)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(797)
	{
		IL2CPP_JUMP_TBL(0x325, IL_0325)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0325:
	{
		V_10 = 0;
		goto IL_0520;
	}

IL_032d:
	{
		XmlAttributeTokenInfoU5BU5D_t2707514583* L_105 = __this->get_attributeTokens_7();
		int32_t L_106 = V_10;
		NullCheck(L_105);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_105, L_106);
		int32_t L_107 = L_106;
		NullCheck(((L_105)->GetAt(static_cast<il2cpp_array_size_t>(L_107))));
		String_t* L_108 = ((XmlTokenInfo_t2571680784 *)((L_105)->GetAt(static_cast<il2cpp_array_size_t>(L_107))))->get_Prefix_4();
		bool L_109 = Object_ReferenceEquals_m3695130242(NULL /*static, unused*/, L_108, _stringLiteral118807, /*hidden argument*/NULL);
		if (L_109)
		{
			goto IL_034f;
		}
	}
	{
		goto IL_051a;
	}

IL_034f:
	{
		XmlAttributeTokenInfoU5BU5D_t2707514583* L_110 = __this->get_attributeTokens_7();
		int32_t L_111 = V_10;
		NullCheck(L_110);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_110, L_111);
		int32_t L_112 = L_111;
		NullCheck(((L_110)->GetAt(static_cast<il2cpp_array_size_t>(L_112))));
		String_t* L_113 = ((XmlTokenInfo_t2571680784 *)((L_110)->GetAt(static_cast<il2cpp_array_size_t>(L_112))))->get_LocalName_3();
		V_11 = L_113;
		XmlAttributeTokenInfoU5BU5D_t2707514583* L_114 = __this->get_attributeTokens_7();
		int32_t L_115 = V_10;
		NullCheck(L_114);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_114, L_115);
		int32_t L_116 = L_115;
		NullCheck(((L_114)->GetAt(static_cast<il2cpp_array_size_t>(L_116))));
		String_t* L_117 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String Mono.Xml2.XmlTextReader/XmlAttributeTokenInfo::get_Value() */, ((L_114)->GetAt(static_cast<il2cpp_array_size_t>(L_116))));
		V_12 = L_117;
		String_t* L_118 = V_11;
		V_15 = L_118;
		String_t* L_119 = V_15;
		if (!L_119)
		{
			goto IL_051a;
		}
	}
	{
		Dictionary_2_t190145395 * L_120 = ((XmlTextReader_t3066586409_StaticFields*)XmlTextReader_t3066586409_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24map52_56();
		if (L_120)
		{
			goto IL_03ba;
		}
	}
	{
		Dictionary_2_t190145395 * L_121 = (Dictionary_2_t190145395 *)il2cpp_codegen_object_new(Dictionary_2_t190145395_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1958628151(L_121, 3, /*hidden argument*/Dictionary_2__ctor_m1958628151_MethodInfo_var);
		V_16 = L_121;
		Dictionary_2_t190145395 * L_122 = V_16;
		NullCheck(L_122);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_122, _stringLiteral3016401, 0);
		Dictionary_2_t190145395 * L_123 = V_16;
		NullCheck(L_123);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_123, _stringLiteral3314158, 1);
		Dictionary_2_t190145395 * L_124 = V_16;
		NullCheck(L_124);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_124, _stringLiteral109637894, 2);
		Dictionary_2_t190145395 * L_125 = V_16;
		((XmlTextReader_t3066586409_StaticFields*)XmlTextReader_t3066586409_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__switchU24map52_56(L_125);
	}

IL_03ba:
	{
		Dictionary_2_t190145395 * L_126 = ((XmlTextReader_t3066586409_StaticFields*)XmlTextReader_t3066586409_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24map52_56();
		String_t* L_127 = V_15;
		NullCheck(L_126);
		bool L_128 = VirtFuncInvoker2< bool, String_t*, int32_t* >::Invoke(21 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::TryGetValue(!0,!1&) */, L_126, L_127, (&V_17));
		if (!L_128)
		{
			goto IL_051a;
		}
	}
	{
		int32_t L_129 = V_17;
		if (L_129 == 0)
		{
			goto IL_03e5;
		}
		if (L_129 == 1)
		{
			goto IL_0469;
		}
		if (L_129 == 2)
		{
			goto IL_047b;
		}
	}
	{
		goto IL_051a;
	}

IL_03e5:
	{
		XmlResolver_t2502213349 * L_130 = __this->get_resolver_46();
		if (!L_130)
		{
			goto IL_0457;
		}
	}
	{
		String_t* L_131 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String Mono.Xml2.XmlTextReader::get_BaseURI() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_132 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_133 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_131, L_132, /*hidden argument*/NULL);
		if (!L_133)
		{
			goto IL_0415;
		}
	}
	{
		String_t* L_134 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String Mono.Xml2.XmlTextReader::get_BaseURI() */, __this);
		Uri_t2776692961 * L_135 = (Uri_t2776692961 *)il2cpp_codegen_object_new(Uri_t2776692961_il2cpp_TypeInfo_var);
		Uri__ctor_m1721267859(L_135, L_134, /*hidden argument*/NULL);
		G_B59_0 = L_135;
		goto IL_0416;
	}

IL_0415:
	{
		G_B59_0 = ((Uri_t2776692961 *)(NULL));
	}

IL_0416:
	{
		V_13 = G_B59_0;
		XmlResolver_t2502213349 * L_136 = __this->get_resolver_46();
		Uri_t2776692961 * L_137 = V_13;
		String_t* L_138 = V_12;
		NullCheck(L_136);
		Uri_t2776692961 * L_139 = VirtFuncInvoker2< Uri_t2776692961 *, Uri_t2776692961 *, String_t* >::Invoke(5 /* System.Uri System.Xml.XmlResolver::ResolveUri(System.Uri,System.String) */, L_136, L_137, L_138);
		V_14 = L_139;
		XmlParserContext_t3629084577 * L_140 = __this->get_parserContext_12();
		Uri_t2776692961 * L_141 = V_14;
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t2776692961_il2cpp_TypeInfo_var);
		bool L_142 = Uri_op_Inequality_m2899852498(NULL /*static, unused*/, L_141, (Uri_t2776692961 *)NULL, /*hidden argument*/NULL);
		G_B60_0 = L_140;
		if (!L_142)
		{
			G_B61_0 = L_140;
			goto IL_0448;
		}
	}
	{
		Uri_t2776692961 * L_143 = V_14;
		NullCheck(L_143);
		String_t* L_144 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Uri::ToString() */, L_143);
		G_B62_0 = L_144;
		G_B62_1 = G_B60_0;
		goto IL_044d;
	}

IL_0448:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_145 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B62_0 = L_145;
		G_B62_1 = G_B61_0;
	}

IL_044d:
	{
		NullCheck(G_B62_1);
		XmlParserContext_set_BaseURI_m663391444(G_B62_1, G_B62_0, /*hidden argument*/NULL);
		goto IL_0464;
	}

IL_0457:
	{
		XmlParserContext_t3629084577 * L_146 = __this->get_parserContext_12();
		String_t* L_147 = V_12;
		NullCheck(L_146);
		XmlParserContext_set_BaseURI_m663391444(L_146, L_147, /*hidden argument*/NULL);
	}

IL_0464:
	{
		goto IL_051a;
	}

IL_0469:
	{
		XmlParserContext_t3629084577 * L_148 = __this->get_parserContext_12();
		String_t* L_149 = V_12;
		NullCheck(L_148);
		XmlParserContext_set_XmlLang_m1537642954(L_148, L_149, /*hidden argument*/NULL);
		goto IL_051a;
	}

IL_047b:
	{
		String_t* L_150 = V_12;
		V_18 = L_150;
		String_t* L_151 = V_18;
		if (!L_151)
		{
			goto IL_0502;
		}
	}
	{
		Dictionary_2_t190145395 * L_152 = ((XmlTextReader_t3066586409_StaticFields*)XmlTextReader_t3066586409_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24map51_55();
		if (L_152)
		{
			goto IL_04b9;
		}
	}
	{
		Dictionary_2_t190145395 * L_153 = (Dictionary_2_t190145395 *)il2cpp_codegen_object_new(Dictionary_2_t190145395_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1958628151(L_153, 2, /*hidden argument*/Dictionary_2__ctor_m1958628151_MethodInfo_var);
		V_16 = L_153;
		Dictionary_2_t190145395 * L_154 = V_16;
		NullCheck(L_154);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_154, _stringLiteral3018305100, 0);
		Dictionary_2_t190145395 * L_155 = V_16;
		NullCheck(L_155);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_155, _stringLiteral1544803905, 1);
		Dictionary_2_t190145395 * L_156 = V_16;
		((XmlTextReader_t3066586409_StaticFields*)XmlTextReader_t3066586409_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__switchU24map51_55(L_156);
	}

IL_04b9:
	{
		Dictionary_2_t190145395 * L_157 = ((XmlTextReader_t3066586409_StaticFields*)XmlTextReader_t3066586409_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24map51_55();
		String_t* L_158 = V_18;
		NullCheck(L_157);
		bool L_159 = VirtFuncInvoker2< bool, String_t*, int32_t* >::Invoke(21 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::TryGetValue(!0,!1&) */, L_157, L_158, (&V_19));
		if (!L_159)
		{
			goto IL_0502;
		}
	}
	{
		int32_t L_160 = V_19;
		if (!L_160)
		{
			goto IL_04e0;
		}
	}
	{
		int32_t L_161 = V_19;
		if ((((int32_t)L_161) == ((int32_t)1)))
		{
			goto IL_04f1;
		}
	}
	{
		goto IL_0502;
	}

IL_04e0:
	{
		XmlParserContext_t3629084577 * L_162 = __this->get_parserContext_12();
		NullCheck(L_162);
		XmlParserContext_set_XmlSpace_m3019056463(L_162, 2, /*hidden argument*/NULL);
		goto IL_0515;
	}

IL_04f1:
	{
		XmlParserContext_t3629084577 * L_163 = __this->get_parserContext_12();
		NullCheck(L_163);
		XmlParserContext_set_XmlSpace_m3019056463(L_163, 1, /*hidden argument*/NULL);
		goto IL_0515;
	}

IL_0502:
	{
		String_t* L_164 = V_12;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_165 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral137844567, L_164, /*hidden argument*/NULL);
		XmlException_t3490696160 * L_166 = XmlTextReader_NotWFError_m304583423(__this, L_165, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_166);
	}

IL_0515:
	{
		goto IL_051a;
	}

IL_051a:
	{
		int32_t L_167 = V_10;
		V_10 = ((int32_t)((int32_t)L_167+(int32_t)1));
	}

IL_0520:
	{
		int32_t L_168 = V_10;
		int32_t L_169 = __this->get_attributeCount_11();
		if ((((int32_t)L_168) < ((int32_t)L_169)))
		{
			goto IL_032d;
		}
	}
	{
		bool L_170 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean Mono.Xml2.XmlTextReader::get_IsEmptyElement() */, __this);
		if (!L_170)
		{
			goto IL_053e;
		}
	}
	{
		XmlTextReader_CheckCurrentStateUpdate_m2095539625(__this, /*hidden argument*/NULL);
	}

IL_053e:
	{
		return;
	}
}
// System.Void Mono.Xml2.XmlTextReader::PushElementName(System.String,System.String,System.String)
extern Il2CppClass* TagNameU5BU5D_t2465062056_il2cpp_TypeInfo_var;
extern const uint32_t XmlTextReader_PushElementName_m1658182381_MetadataUsageId;
extern "C"  void XmlTextReader_PushElementName_m1658182381 (XmlTextReader_t3066586409 * __this, String_t* ___name0, String_t* ___local1, String_t* ___prefix2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader_PushElementName_m1658182381_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TagNameU5BU5D_t2465062056* V_0 = NULL;
	int32_t V_1 = 0;
	{
		TagNameU5BU5D_t2465062056* L_0 = __this->get_elementNames_21();
		NullCheck(L_0);
		int32_t L_1 = __this->get_elementNameStackPos_22();
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((uint32_t)L_1))))
		{
			goto IL_003e;
		}
	}
	{
		TagNameU5BU5D_t2465062056* L_2 = __this->get_elementNames_21();
		NullCheck(L_2);
		V_0 = ((TagNameU5BU5D_t2465062056*)SZArrayNew(TagNameU5BU5D_t2465062056_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))*(int32_t)2))));
		TagNameU5BU5D_t2465062056* L_3 = __this->get_elementNames_21();
		TagNameU5BU5D_t2465062056* L_4 = V_0;
		int32_t L_5 = __this->get_elementNameStackPos_22();
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_3, 0, (Il2CppArray *)(Il2CppArray *)L_4, 0, L_5, /*hidden argument*/NULL);
		TagNameU5BU5D_t2465062056* L_6 = V_0;
		__this->set_elementNames_21(L_6);
	}

IL_003e:
	{
		TagNameU5BU5D_t2465062056* L_7 = __this->get_elementNames_21();
		int32_t L_8 = __this->get_elementNameStackPos_22();
		int32_t L_9 = L_8;
		V_1 = L_9;
		__this->set_elementNameStackPos_22(((int32_t)((int32_t)L_9+(int32_t)1)));
		int32_t L_10 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_10);
		String_t* L_11 = ___name0;
		String_t* L_12 = ___local1;
		String_t* L_13 = ___prefix2;
		TagName_t115468581  L_14;
		memset(&L_14, 0, sizeof(L_14));
		TagName__ctor_m1413447022(&L_14, L_11, L_12, L_13, /*hidden argument*/NULL);
		(*(TagName_t115468581 *)((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_10)))) = L_14;
		return;
	}
}
// System.Void Mono.Xml2.XmlTextReader::ReadEndTag()
extern Il2CppCodeGenString* _stringLiteral3908688669;
extern Il2CppCodeGenString* _stringLiteral1904138776;
extern const uint32_t XmlTextReader_ReadEndTag_m3759677719_MetadataUsageId;
extern "C"  void XmlTextReader_ReadEndTag_m3759677719 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader_ReadEndTag_m3759677719_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TagName_t115468581  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	{
		int32_t L_0 = __this->get_currentState_40();
		if ((((int32_t)L_0) == ((int32_t)1)))
		{
			goto IL_0018;
		}
	}
	{
		XmlException_t3490696160 * L_1 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral3908688669, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = __this->get_line_34();
		__this->set_currentLinkedNodeLineNumber_36(L_2);
		int32_t L_3 = __this->get_column_35();
		__this->set_currentLinkedNodeLinePosition_37(L_3);
		int32_t L_4 = __this->get_elementNameStackPos_22();
		if (L_4)
		{
			goto IL_0047;
		}
	}
	{
		XmlException_t3490696160 * L_5 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral1904138776, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0047:
	{
		TagNameU5BU5D_t2465062056* L_6 = __this->get_elementNames_21();
		int32_t L_7 = __this->get_elementNameStackPos_22();
		int32_t L_8 = ((int32_t)((int32_t)L_7-(int32_t)1));
		V_1 = L_8;
		__this->set_elementNameStackPos_22(L_8);
		int32_t L_9 = V_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_9);
		V_0 = (*(TagName_t115468581 *)((L_6)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_9))));
		String_t* L_10 = (&V_0)->get_Name_0();
		XmlTextReader_Expect_m1011563495(__this, L_10, /*hidden argument*/NULL);
		XmlTextReader_ExpectAfterWhitespace_m3276297213(__this, ((int32_t)62), /*hidden argument*/NULL);
		int32_t L_11 = __this->get_depth_17();
		__this->set_depth_17(((int32_t)((int32_t)L_11-(int32_t)1)));
		String_t* L_12 = (&V_0)->get_Name_0();
		String_t* L_13 = (&V_0)->get_Prefix_2();
		String_t* L_14 = (&V_0)->get_LocalName_1();
		XmlTextReader_SetProperties_m3034003792(__this, ((int32_t)15), L_12, L_13, L_14, (bool)0, (String_t*)NULL, (bool)1, /*hidden argument*/NULL);
		String_t* L_15 = (&V_0)->get_Prefix_2();
		NullCheck(L_15);
		int32_t L_16 = String_get_Length_m2979997331(L_15, /*hidden argument*/NULL);
		if ((((int32_t)L_16) <= ((int32_t)0)))
		{
			goto IL_00dc;
		}
	}
	{
		XmlTokenInfo_t2571680784 * L_17 = __this->get_currentToken_4();
		String_t* L_18 = (&V_0)->get_Prefix_2();
		String_t* L_19 = XmlTextReader_LookupNamespace_m3699761695(__this, L_18, (bool)1, /*hidden argument*/NULL);
		NullCheck(L_17);
		L_17->set_NamespaceURI_5(L_19);
		goto IL_00fd;
	}

IL_00dc:
	{
		bool L_20 = __this->get_namespaces_44();
		if (!L_20)
		{
			goto IL_00fd;
		}
	}
	{
		XmlTokenInfo_t2571680784 * L_21 = __this->get_currentToken_4();
		XmlNamespaceManager_t1861067185 * L_22 = __this->get_nsmgr_14();
		NullCheck(L_22);
		String_t* L_23 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Xml.XmlNamespaceManager::get_DefaultNamespace() */, L_22);
		NullCheck(L_21);
		L_21->set_NamespaceURI_5(L_23);
	}

IL_00fd:
	{
		__this->set_popScope_20((bool)1);
		XmlTextReader_CheckCurrentStateUpdate_m2095539625(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml2.XmlTextReader::CheckCurrentStateUpdate()
extern "C"  void XmlTextReader_CheckCurrentStateUpdate_m2095539625 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_depth_17();
		if (L_0)
		{
			goto IL_0036;
		}
	}
	{
		bool L_1 = __this->get_allowMultipleRoot_23();
		if (L_1)
		{
			goto IL_0036;
		}
	}
	{
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean Mono.Xml2.XmlTextReader::get_IsEmptyElement() */, __this);
		if (L_2)
		{
			goto IL_002e;
		}
	}
	{
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(18 /* System.Xml.XmlNodeType Mono.Xml2.XmlTextReader::get_NodeType() */, __this);
		if ((!(((uint32_t)L_3) == ((uint32_t)((int32_t)15)))))
		{
			goto IL_0036;
		}
	}

IL_002e:
	{
		__this->set_currentState_40(((int32_t)15));
	}

IL_0036:
	{
		return;
	}
}
// System.Void Mono.Xml2.XmlTextReader::AppendValueChar(System.Int32)
extern "C"  void XmlTextReader_AppendValueChar_m3744790334 (XmlTextReader_t3066586409 * __this, int32_t ___ch0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___ch0;
		if ((((int32_t)L_0) >= ((int32_t)((int32_t)65535))))
		{
			goto IL_001e;
		}
	}
	{
		StringBuilder_t3822575854 * L_1 = __this->get_valueBuffer_27();
		int32_t L_2 = ___ch0;
		NullCheck(L_1);
		StringBuilder_Append_m2143093878(L_1, (((int32_t)((uint16_t)L_2))), /*hidden argument*/NULL);
		goto IL_0025;
	}

IL_001e:
	{
		int32_t L_3 = ___ch0;
		XmlTextReader_AppendSurrogatePairValueChar_m704355490(__this, L_3, /*hidden argument*/NULL);
	}

IL_0025:
	{
		return;
	}
}
// System.Void Mono.Xml2.XmlTextReader::AppendSurrogatePairValueChar(System.Int32)
extern "C"  void XmlTextReader_AppendSurrogatePairValueChar_m704355490 (XmlTextReader_t3066586409 * __this, int32_t ___ch0, const MethodInfo* method)
{
	{
		StringBuilder_t3822575854 * L_0 = __this->get_valueBuffer_27();
		int32_t L_1 = ___ch0;
		NullCheck(L_0);
		StringBuilder_Append_m2143093878(L_0, (((int32_t)((uint16_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1-(int32_t)((int32_t)65536)))/(int32_t)((int32_t)1024)))+(int32_t)((int32_t)55296)))))), /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_2 = __this->get_valueBuffer_27();
		int32_t L_3 = ___ch0;
		NullCheck(L_2);
		StringBuilder_Append_m2143093878(L_2, (((int32_t)((uint16_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_3-(int32_t)((int32_t)65536)))%(int32_t)((int32_t)1024)))+(int32_t)((int32_t)56320)))))), /*hidden argument*/NULL);
		return;
	}
}
// System.String Mono.Xml2.XmlTextReader::CreateValueString()
extern Il2CppClass* CharU5BU5D_t3416858730_il2cpp_TypeInfo_var;
extern Il2CppClass* NameTable_t2805661099_il2cpp_TypeInfo_var;
extern const uint32_t XmlTextReader_CreateValueString_m2171097961_MetadataUsageId;
extern "C"  String_t* XmlTextReader_CreateValueString_m2171097961 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader_CreateValueString_m2171097961_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	String_t* G_B16_0 = NULL;
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(18 /* System.Xml.XmlNodeType Mono.Xml2.XmlTextReader::get_NodeType() */, __this);
		V_2 = L_0;
		int32_t L_1 = V_2;
		if ((((int32_t)L_1) == ((int32_t)((int32_t)13))))
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_2 = V_2;
		if ((((int32_t)L_2) == ((int32_t)((int32_t)14))))
		{
			goto IL_001c;
		}
	}
	{
		goto IL_00ad;
	}

IL_001c:
	{
		StringBuilder_t3822575854 * L_3 = __this->get_valueBuffer_27();
		NullCheck(L_3);
		int32_t L_4 = StringBuilder_get_Length_m2443133099(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		CharU5BU5D_t3416858730* L_5 = __this->get_whitespaceCache_53();
		if (L_5)
		{
			goto IL_0040;
		}
	}
	{
		__this->set_whitespaceCache_53(((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)((int32_t)32))));
	}

IL_0040:
	{
		int32_t L_6 = V_0;
		CharU5BU5D_t3416858730* L_7 = __this->get_whitespaceCache_53();
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_0053;
		}
	}
	{
		goto IL_00ad;
	}

IL_0053:
	{
		NameTable_t2805661099 * L_8 = __this->get_whitespacePool_52();
		if (L_8)
		{
			goto IL_0069;
		}
	}
	{
		NameTable_t2805661099 * L_9 = (NameTable_t2805661099 *)il2cpp_codegen_object_new(NameTable_t2805661099_il2cpp_TypeInfo_var);
		NameTable__ctor_m929780536(L_9, /*hidden argument*/NULL);
		__this->set_whitespacePool_52(L_9);
	}

IL_0069:
	{
		V_1 = 0;
		goto IL_0088;
	}

IL_0070:
	{
		CharU5BU5D_t3416858730* L_10 = __this->get_whitespaceCache_53();
		int32_t L_11 = V_1;
		StringBuilder_t3822575854 * L_12 = __this->get_valueBuffer_27();
		int32_t L_13 = V_1;
		NullCheck(L_12);
		uint16_t L_14 = StringBuilder_get_Chars_m1670994701(L_12, L_13, /*hidden argument*/NULL);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(L_11), (uint16_t)L_14);
		int32_t L_15 = V_1;
		V_1 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0088:
	{
		int32_t L_16 = V_1;
		int32_t L_17 = V_0;
		if ((((int32_t)L_16) < ((int32_t)L_17)))
		{
			goto IL_0070;
		}
	}
	{
		NameTable_t2805661099 * L_18 = __this->get_whitespacePool_52();
		CharU5BU5D_t3416858730* L_19 = __this->get_whitespaceCache_53();
		StringBuilder_t3822575854 * L_20 = __this->get_valueBuffer_27();
		NullCheck(L_20);
		int32_t L_21 = StringBuilder_get_Length_m2443133099(L_20, /*hidden argument*/NULL);
		NullCheck(L_18);
		String_t* L_22 = VirtFuncInvoker3< String_t*, CharU5BU5D_t3416858730*, int32_t, int32_t >::Invoke(5 /* System.String System.Xml.NameTable::Add(System.Char[],System.Int32,System.Int32) */, L_18, L_19, 0, L_21);
		return L_22;
	}

IL_00ad:
	{
		StringBuilder_t3822575854 * L_23 = __this->get_valueBuffer_27();
		NullCheck(L_23);
		int32_t L_24 = StringBuilder_get_Capacity_m884438143(L_23, /*hidden argument*/NULL);
		if ((((int32_t)L_24) >= ((int32_t)((int32_t)100))))
		{
			goto IL_00db;
		}
	}
	{
		StringBuilder_t3822575854 * L_25 = __this->get_valueBuffer_27();
		StringBuilder_t3822575854 * L_26 = __this->get_valueBuffer_27();
		NullCheck(L_26);
		int32_t L_27 = StringBuilder_get_Length_m2443133099(L_26, /*hidden argument*/NULL);
		NullCheck(L_25);
		String_t* L_28 = StringBuilder_ToString_m3621056261(L_25, 0, L_27, /*hidden argument*/NULL);
		G_B16_0 = L_28;
		goto IL_00e6;
	}

IL_00db:
	{
		StringBuilder_t3822575854 * L_29 = __this->get_valueBuffer_27();
		NullCheck(L_29);
		String_t* L_30 = StringBuilder_ToString_m350379841(L_29, /*hidden argument*/NULL);
		G_B16_0 = L_30;
	}

IL_00e6:
	{
		return G_B16_0;
	}
}
// System.Void Mono.Xml2.XmlTextReader::ClearValueBuffer()
extern "C"  void XmlTextReader_ClearValueBuffer_m495962918 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	{
		StringBuilder_t3822575854 * L_0 = __this->get_valueBuffer_27();
		NullCheck(L_0);
		StringBuilder_set_Length_m1952332172(L_0, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml2.XmlTextReader::ReadText(System.Boolean)
extern Il2CppClass* XmlChar_t3591879093_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3669187805;
extern Il2CppCodeGenString* _stringLiteral178728223;
extern Il2CppCodeGenString* _stringLiteral932431087;
extern const uint32_t XmlTextReader_ReadText_m502843516_MetadataUsageId;
extern "C"  void XmlTextReader_ReadText_m502843516 (XmlTextReader_t3066586409 * __this, bool ___notWhitespace0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader_ReadText_m502843516_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	int32_t V_2 = 0;
	int32_t G_B39_0 = 0;
	{
		int32_t L_0 = __this->get_currentState_40();
		if ((((int32_t)L_0) == ((int32_t)1)))
		{
			goto IL_0018;
		}
	}
	{
		XmlException_t3490696160 * L_1 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral3669187805, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		__this->set_preserveCurrentTag_33((bool)0);
		bool L_2 = ___notWhitespace0;
		if (!L_2)
		{
			goto IL_002b;
		}
	}
	{
		XmlTextReader_ClearValueBuffer_m495962918(__this, /*hidden argument*/NULL);
	}

IL_002b:
	{
		int32_t L_3 = XmlTextReader_PeekChar_m254641297(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		V_1 = (bool)0;
		goto IL_0129;
	}

IL_0039:
	{
		int32_t L_4 = V_0;
		if ((!(((uint32_t)L_4) == ((uint32_t)((int32_t)38)))))
		{
			goto IL_0065;
		}
	}
	{
		XmlTextReader_ReadChar_m3857716364(__this, /*hidden argument*/NULL);
		int32_t L_5 = XmlTextReader_ReadReference_m2020451886(__this, (bool)0, /*hidden argument*/NULL);
		V_0 = L_5;
		bool L_6 = __this->get_returnEntityReference_25();
		if (!L_6)
		{
			goto IL_0060;
		}
	}
	{
		goto IL_0138;
	}

IL_0060:
	{
		goto IL_00c4;
	}

IL_0065:
	{
		bool L_7 = __this->get_normalization_47();
		if (!L_7)
		{
			goto IL_009b;
		}
	}
	{
		int32_t L_8 = V_0;
		if ((!(((uint32_t)L_8) == ((uint32_t)((int32_t)13)))))
		{
			goto IL_009b;
		}
	}
	{
		XmlTextReader_ReadChar_m3857716364(__this, /*hidden argument*/NULL);
		int32_t L_9 = XmlTextReader_PeekChar_m254641297(__this, /*hidden argument*/NULL);
		V_0 = L_9;
		int32_t L_10 = V_0;
		if ((((int32_t)L_10) == ((int32_t)((int32_t)10))))
		{
			goto IL_0096;
		}
	}
	{
		XmlTextReader_AppendValueChar_m3744790334(__this, ((int32_t)10), /*hidden argument*/NULL);
	}

IL_0096:
	{
		goto IL_0129;
	}

IL_009b:
	{
		bool L_11 = XmlTextReader_get_CharacterChecking_m3210588674(__this, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_00bd;
		}
	}
	{
		int32_t L_12 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(XmlChar_t3591879093_il2cpp_TypeInfo_var);
		bool L_13 = XmlChar_IsInvalid_m3338941186(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_00bd;
		}
	}
	{
		XmlException_t3490696160 * L_14 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral178728223, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_14);
	}

IL_00bd:
	{
		int32_t L_15 = XmlTextReader_ReadChar_m3857716364(__this, /*hidden argument*/NULL);
		V_0 = L_15;
	}

IL_00c4:
	{
		int32_t L_16 = V_0;
		if ((((int32_t)L_16) >= ((int32_t)((int32_t)65535))))
		{
			goto IL_00e2;
		}
	}
	{
		StringBuilder_t3822575854 * L_17 = __this->get_valueBuffer_27();
		int32_t L_18 = V_0;
		NullCheck(L_17);
		StringBuilder_Append_m2143093878(L_17, (((int32_t)((uint16_t)L_18))), /*hidden argument*/NULL);
		goto IL_00e9;
	}

IL_00e2:
	{
		int32_t L_19 = V_0;
		XmlTextReader_AppendSurrogatePairValueChar_m704355490(__this, L_19, /*hidden argument*/NULL);
	}

IL_00e9:
	{
		int32_t L_20 = V_0;
		if ((!(((uint32_t)L_20) == ((uint32_t)((int32_t)93)))))
		{
			goto IL_0117;
		}
	}
	{
		bool L_21 = V_1;
		if (!L_21)
		{
			goto IL_0110;
		}
	}
	{
		int32_t L_22 = XmlTextReader_PeekChar_m254641297(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_22) == ((uint32_t)((int32_t)62)))))
		{
			goto IL_0110;
		}
	}
	{
		XmlException_t3490696160 * L_23 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral932431087, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_23);
	}

IL_0110:
	{
		V_1 = (bool)1;
		goto IL_011f;
	}

IL_0117:
	{
		bool L_24 = V_1;
		if (!L_24)
		{
			goto IL_011f;
		}
	}
	{
		V_1 = (bool)0;
	}

IL_011f:
	{
		int32_t L_25 = XmlTextReader_PeekChar_m254641297(__this, /*hidden argument*/NULL);
		V_0 = L_25;
		___notWhitespace0 = (bool)1;
	}

IL_0129:
	{
		int32_t L_26 = V_0;
		if ((((int32_t)L_26) == ((int32_t)((int32_t)60))))
		{
			goto IL_0138;
		}
	}
	{
		int32_t L_27 = V_0;
		if ((!(((uint32_t)L_27) == ((uint32_t)(-1)))))
		{
			goto IL_0039;
		}
	}

IL_0138:
	{
		bool L_28 = __this->get_returnEntityReference_25();
		if (!L_28)
		{
			goto IL_015e;
		}
	}
	{
		StringBuilder_t3822575854 * L_29 = __this->get_valueBuffer_27();
		NullCheck(L_29);
		int32_t L_30 = StringBuilder_get_Length_m2443133099(L_29, /*hidden argument*/NULL);
		if (L_30)
		{
			goto IL_015e;
		}
	}
	{
		XmlTextReader_SetEntityReferenceProperties_m614061979(__this, /*hidden argument*/NULL);
		goto IL_0199;
	}

IL_015e:
	{
		bool L_31 = ___notWhitespace0;
		if (!L_31)
		{
			goto IL_016a;
		}
	}
	{
		G_B39_0 = 3;
		goto IL_017f;
	}

IL_016a:
	{
		int32_t L_32 = VirtFuncInvoker0< int32_t >::Invoke(24 /* System.Xml.XmlSpace Mono.Xml2.XmlTextReader::get_XmlSpace() */, __this);
		if ((!(((uint32_t)L_32) == ((uint32_t)2))))
		{
			goto IL_017d;
		}
	}
	{
		G_B39_0 = ((int32_t)14);
		goto IL_017f;
	}

IL_017d:
	{
		G_B39_0 = ((int32_t)13);
	}

IL_017f:
	{
		V_2 = G_B39_0;
		int32_t L_33 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_34 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_35 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_36 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		XmlTextReader_SetProperties_m3034003792(__this, L_33, L_34, L_35, L_36, (bool)0, (String_t*)NULL, (bool)1, /*hidden argument*/NULL);
	}

IL_0199:
	{
		return;
	}
}
// System.Int32 Mono.Xml2.XmlTextReader::ReadReference(System.Boolean)
extern "C"  int32_t XmlTextReader_ReadReference_m2020451886 (XmlTextReader_t3066586409 * __this, bool ___ignoreEntityReferences0, const MethodInfo* method)
{
	{
		int32_t L_0 = XmlTextReader_PeekChar_m254641297(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)35)))))
		{
			goto IL_001c;
		}
	}
	{
		XmlTextReader_Advance_m673767315(__this, ((int32_t)35), /*hidden argument*/NULL);
		int32_t L_1 = XmlTextReader_ReadCharacterReference_m3777658776(__this, /*hidden argument*/NULL);
		return L_1;
	}

IL_001c:
	{
		bool L_2 = ___ignoreEntityReferences0;
		int32_t L_3 = XmlTextReader_ReadEntityReference_m1180838731(__this, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Int32 Mono.Xml2.XmlTextReader::ReadCharacterReference()
extern Il2CppClass* CultureInfo_t3603717042_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t2778706699_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlChar_t3591879093_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral967259696;
extern Il2CppCodeGenString* _stringLiteral2691180298;
extern Il2CppCodeGenString* _stringLiteral3856069867;
extern Il2CppCodeGenString* _stringLiteral966757701;
extern const uint32_t XmlTextReader_ReadCharacterReference_m3777658776_MetadataUsageId;
extern "C"  int32_t XmlTextReader_ReadCharacterReference_m3777658776 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader_ReadCharacterReference_m3777658776_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		int32_t L_0 = XmlTextReader_PeekChar_m254641297(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)120)))))
		{
			goto IL_00cd;
		}
	}
	{
		XmlTextReader_Advance_m673767315(__this, ((int32_t)120), /*hidden argument*/NULL);
		goto IL_00b2;
	}

IL_001c:
	{
		int32_t L_1 = V_1;
		XmlTextReader_Advance_m673767315(__this, L_1, /*hidden argument*/NULL);
		int32_t L_2 = V_1;
		if ((((int32_t)L_2) < ((int32_t)((int32_t)48))))
		{
			goto IL_0041;
		}
	}
	{
		int32_t L_3 = V_1;
		if ((((int32_t)L_3) > ((int32_t)((int32_t)57))))
		{
			goto IL_0041;
		}
	}
	{
		int32_t L_4 = V_0;
		int32_t L_5 = V_1;
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_4<<(int32_t)4))+(int32_t)L_5))-(int32_t)((int32_t)48)));
		goto IL_00b2;
	}

IL_0041:
	{
		int32_t L_6 = V_1;
		if ((((int32_t)L_6) < ((int32_t)((int32_t)65))))
		{
			goto IL_0062;
		}
	}
	{
		int32_t L_7 = V_1;
		if ((((int32_t)L_7) > ((int32_t)((int32_t)70))))
		{
			goto IL_0062;
		}
	}
	{
		int32_t L_8 = V_0;
		int32_t L_9 = V_1;
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_8<<(int32_t)4))+(int32_t)L_9))-(int32_t)((int32_t)65)))+(int32_t)((int32_t)10)));
		goto IL_00b2;
	}

IL_0062:
	{
		int32_t L_10 = V_1;
		if ((((int32_t)L_10) < ((int32_t)((int32_t)97))))
		{
			goto IL_0083;
		}
	}
	{
		int32_t L_11 = V_1;
		if ((((int32_t)L_11) > ((int32_t)((int32_t)102))))
		{
			goto IL_0083;
		}
	}
	{
		int32_t L_12 = V_0;
		int32_t L_13 = V_1;
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_12<<(int32_t)4))+(int32_t)L_13))-(int32_t)((int32_t)97)))+(int32_t)((int32_t)10)));
		goto IL_00b2;
	}

IL_0083:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3603717042_il2cpp_TypeInfo_var);
		CultureInfo_t3603717042 * L_14 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_15 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)2));
		int32_t L_16 = V_1;
		uint16_t L_17 = ((uint16_t)(((int32_t)((uint16_t)L_16))));
		Il2CppObject * L_18 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_17);
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 0);
		ArrayElementTypeCheck (L_15, L_18);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_18);
		ObjectU5BU5D_t11523773* L_19 = L_15;
		int32_t L_20 = V_1;
		int32_t L_21 = L_20;
		Il2CppObject * L_22 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_21);
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 1);
		ArrayElementTypeCheck (L_19, L_22);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = String_Format_m3351777162(NULL /*static, unused*/, L_14, _stringLiteral967259696, L_19, /*hidden argument*/NULL);
		XmlException_t3490696160 * L_24 = XmlTextReader_NotWFError_m304583423(__this, L_23, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_24);
	}

IL_00b2:
	{
		int32_t L_25 = XmlTextReader_PeekChar_m254641297(__this, /*hidden argument*/NULL);
		int32_t L_26 = L_25;
		V_1 = L_26;
		if ((((int32_t)L_26) == ((int32_t)((int32_t)59))))
		{
			goto IL_00c8;
		}
	}
	{
		int32_t L_27 = V_1;
		if ((!(((uint32_t)L_27) == ((uint32_t)(-1)))))
		{
			goto IL_001c;
		}
	}

IL_00c8:
	{
		goto IL_013d;
	}

IL_00cd:
	{
		goto IL_0127;
	}

IL_00d2:
	{
		int32_t L_28 = V_1;
		XmlTextReader_Advance_m673767315(__this, L_28, /*hidden argument*/NULL);
		int32_t L_29 = V_1;
		if ((((int32_t)L_29) < ((int32_t)((int32_t)48))))
		{
			goto IL_00f8;
		}
	}
	{
		int32_t L_30 = V_1;
		if ((((int32_t)L_30) > ((int32_t)((int32_t)57))))
		{
			goto IL_00f8;
		}
	}
	{
		int32_t L_31 = V_0;
		int32_t L_32 = V_1;
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_31*(int32_t)((int32_t)10)))+(int32_t)L_32))-(int32_t)((int32_t)48)));
		goto IL_0127;
	}

IL_00f8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3603717042_il2cpp_TypeInfo_var);
		CultureInfo_t3603717042 * L_33 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_34 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)2));
		int32_t L_35 = V_1;
		uint16_t L_36 = ((uint16_t)(((int32_t)((uint16_t)L_35))));
		Il2CppObject * L_37 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_36);
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, 0);
		ArrayElementTypeCheck (L_34, L_37);
		(L_34)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_37);
		ObjectU5BU5D_t11523773* L_38 = L_34;
		int32_t L_39 = V_1;
		int32_t L_40 = L_39;
		Il2CppObject * L_41 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_40);
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 1);
		ArrayElementTypeCheck (L_38, L_41);
		(L_38)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_41);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_42 = String_Format_m3351777162(NULL /*static, unused*/, L_33, _stringLiteral2691180298, L_38, /*hidden argument*/NULL);
		XmlException_t3490696160 * L_43 = XmlTextReader_NotWFError_m304583423(__this, L_42, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_43);
	}

IL_0127:
	{
		int32_t L_44 = XmlTextReader_PeekChar_m254641297(__this, /*hidden argument*/NULL);
		int32_t L_45 = L_44;
		V_1 = L_45;
		if ((((int32_t)L_45) == ((int32_t)((int32_t)59))))
		{
			goto IL_013d;
		}
	}
	{
		int32_t L_46 = V_1;
		if ((!(((uint32_t)L_46) == ((uint32_t)(-1)))))
		{
			goto IL_00d2;
		}
	}

IL_013d:
	{
		XmlTextReader_ReadChar_m3857716364(__this, /*hidden argument*/NULL);
		bool L_47 = XmlTextReader_get_CharacterChecking_m3210588674(__this, /*hidden argument*/NULL);
		if (!L_47)
		{
			goto IL_01a3;
		}
	}
	{
		bool L_48 = XmlTextReader_get_Normalization_m717792252(__this, /*hidden argument*/NULL);
		if (!L_48)
		{
			goto IL_01a3;
		}
	}
	{
		int32_t L_49 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(XmlChar_t3591879093_il2cpp_TypeInfo_var);
		bool L_50 = XmlChar_IsInvalid_m3338941186(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		if (!L_50)
		{
			goto IL_01a3;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_51 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_51);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_51, 0);
		ArrayElementTypeCheck (L_51, _stringLiteral3856069867);
		(L_51)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral3856069867);
		ObjectU5BU5D_t11523773* L_52 = L_51;
		bool L_53 = __this->get_normalization_47();
		bool L_54 = L_53;
		Il2CppObject * L_55 = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &L_54);
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, 1);
		ArrayElementTypeCheck (L_52, L_55);
		(L_52)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_55);
		ObjectU5BU5D_t11523773* L_56 = L_52;
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, 2);
		ArrayElementTypeCheck (L_56, _stringLiteral966757701);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral966757701);
		ObjectU5BU5D_t11523773* L_57 = L_56;
		bool L_58 = __this->get_checkCharacters_48();
		bool L_59 = L_58;
		Il2CppObject * L_60 = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &L_59);
		NullCheck(L_57);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_57, 3);
		ArrayElementTypeCheck (L_57, L_60);
		(L_57)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_60);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_61 = String_Concat_m3016520001(NULL /*static, unused*/, L_57, /*hidden argument*/NULL);
		XmlException_t3490696160 * L_62 = XmlTextReader_NotWFError_m304583423(__this, L_61, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_62);
	}

IL_01a3:
	{
		int32_t L_63 = V_0;
		return L_63;
	}
}
// System.Int32 Mono.Xml2.XmlTextReader::ReadEntityReference(System.Boolean)
extern Il2CppClass* XmlChar_t3591879093_il2cpp_TypeInfo_var;
extern const uint32_t XmlTextReader_ReadEntityReference_m1180838731_MetadataUsageId;
extern "C"  int32_t XmlTextReader_ReadEntityReference_m1180838731 (XmlTextReader_t3066586409 * __this, bool ___ignoreEntityReferences0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader_ReadEntityReference_m1180838731_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		String_t* L_0 = XmlTextReader_ReadName_m2874594848(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		XmlTextReader_Expect_m2788845516(__this, ((int32_t)59), /*hidden argument*/NULL);
		String_t* L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(XmlChar_t3591879093_il2cpp_TypeInfo_var);
		int32_t L_2 = XmlChar_GetPredefinedEntity_m4157892537(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = V_1;
		if ((((int32_t)L_3) < ((int32_t)0)))
		{
			goto IL_001f;
		}
	}
	{
		int32_t L_4 = V_1;
		return L_4;
	}

IL_001f:
	{
		bool L_5 = ___ignoreEntityReferences0;
		if (!L_5)
		{
			goto IL_005e;
		}
	}
	{
		XmlTextReader_AppendValueChar_m3744790334(__this, ((int32_t)38), /*hidden argument*/NULL);
		V_2 = 0;
		goto IL_0045;
	}

IL_0034:
	{
		String_t* L_6 = V_0;
		int32_t L_7 = V_2;
		NullCheck(L_6);
		uint16_t L_8 = String_get_Chars_m3015341861(L_6, L_7, /*hidden argument*/NULL);
		XmlTextReader_AppendValueChar_m3744790334(__this, L_8, /*hidden argument*/NULL);
		int32_t L_9 = V_2;
		V_2 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0045:
	{
		int32_t L_10 = V_2;
		String_t* L_11 = V_0;
		NullCheck(L_11);
		int32_t L_12 = String_get_Length_m2979997331(L_11, /*hidden argument*/NULL);
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_0034;
		}
	}
	{
		XmlTextReader_AppendValueChar_m3744790334(__this, ((int32_t)59), /*hidden argument*/NULL);
		goto IL_006c;
	}

IL_005e:
	{
		__this->set_returnEntityReference_25((bool)1);
		String_t* L_13 = V_0;
		__this->set_entityReferenceName_26(L_13);
	}

IL_006c:
	{
		return (-1);
	}
}
// System.Void Mono.Xml2.XmlTextReader::ReadAttributes(System.Boolean)
extern Il2CppCodeGenString* _stringLiteral587976122;
extern const uint32_t XmlTextReader_ReadAttributes_m1335987046_MetadataUsageId;
extern "C"  void XmlTextReader_ReadAttributes_m1335987046 (XmlTextReader_t3066586409 * __this, bool ___isXmlDecl0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader_ReadAttributes_m1335987046_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	{
		V_0 = (-1);
		V_1 = (bool)0;
		__this->set_currentAttribute_9((-1));
		__this->set_currentAttributeValue_10((-1));
	}

IL_0012:
	{
		bool L_0 = XmlTextReader_SkipWhitespace_m1854637366(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_002f;
		}
	}
	{
		bool L_1 = V_1;
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		XmlException_t3490696160 * L_2 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral587976122, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_002f:
	{
		XmlTextReader_IncrementAttributeToken_m639649356(__this, /*hidden argument*/NULL);
		XmlAttributeTokenInfo_t922105698 * L_3 = __this->get_currentAttributeToken_5();
		int32_t L_4 = __this->get_line_34();
		NullCheck(L_3);
		((XmlTokenInfo_t2571680784 *)L_3)->set_LineNumber_8(L_4);
		XmlAttributeTokenInfo_t922105698 * L_5 = __this->get_currentAttributeToken_5();
		int32_t L_6 = __this->get_column_35();
		NullCheck(L_5);
		((XmlTokenInfo_t2571680784 *)L_5)->set_LinePosition_9(L_6);
		XmlAttributeTokenInfo_t922105698 * L_7 = __this->get_currentAttributeToken_5();
		String_t* L_8 = XmlTextReader_ReadName_m2367194790(__this, (&V_2), (&V_3), /*hidden argument*/NULL);
		NullCheck(L_7);
		((XmlTokenInfo_t2571680784 *)L_7)->set_Name_2(L_8);
		XmlAttributeTokenInfo_t922105698 * L_9 = __this->get_currentAttributeToken_5();
		String_t* L_10 = V_2;
		NullCheck(L_9);
		((XmlTokenInfo_t2571680784 *)L_9)->set_Prefix_4(L_10);
		XmlAttributeTokenInfo_t922105698 * L_11 = __this->get_currentAttributeToken_5();
		String_t* L_12 = V_3;
		NullCheck(L_11);
		((XmlTokenInfo_t2571680784 *)L_11)->set_LocalName_3(L_12);
		XmlTextReader_ExpectAfterWhitespace_m3276297213(__this, ((int32_t)61), /*hidden argument*/NULL);
		XmlTextReader_SkipWhitespace_m1854637366(__this, /*hidden argument*/NULL);
		XmlTextReader_ReadAttributeValueTokens_m3644060952(__this, (-1), /*hidden argument*/NULL);
		bool L_13 = ___isXmlDecl0;
		if (!L_13)
		{
			goto IL_00ad;
		}
	}
	{
		XmlAttributeTokenInfo_t922105698 * L_14 = __this->get_currentAttributeToken_5();
		NullCheck(L_14);
		String_t* L_15 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String Mono.Xml2.XmlTextReader/XmlAttributeTokenInfo::get_Value() */, L_14);
		V_4 = L_15;
	}

IL_00ad:
	{
		int32_t L_16 = __this->get_attributeCount_11();
		__this->set_attributeCount_11(((int32_t)((int32_t)L_16+(int32_t)1)));
		bool L_17 = XmlTextReader_SkipWhitespace_m1854637366(__this, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_00c8;
		}
	}
	{
		V_1 = (bool)1;
	}

IL_00c8:
	{
		int32_t L_18 = XmlTextReader_PeekChar_m254641297(__this, /*hidden argument*/NULL);
		V_0 = L_18;
		bool L_19 = ___isXmlDecl0;
		if (!L_19)
		{
			goto IL_00e7;
		}
	}
	{
		int32_t L_20 = V_0;
		if ((!(((uint32_t)L_20) == ((uint32_t)((int32_t)63)))))
		{
			goto IL_00e2;
		}
	}
	{
		goto IL_0103;
	}

IL_00e2:
	{
		goto IL_00fc;
	}

IL_00e7:
	{
		int32_t L_21 = V_0;
		if ((((int32_t)L_21) == ((int32_t)((int32_t)47))))
		{
			goto IL_00f7;
		}
	}
	{
		int32_t L_22 = V_0;
		if ((!(((uint32_t)L_22) == ((uint32_t)((int32_t)62)))))
		{
			goto IL_00fc;
		}
	}

IL_00f7:
	{
		goto IL_0103;
	}

IL_00fc:
	{
		int32_t L_23 = V_0;
		if ((!(((uint32_t)L_23) == ((uint32_t)(-1)))))
		{
			goto IL_0012;
		}
	}

IL_0103:
	{
		__this->set_currentAttribute_9((-1));
		__this->set_currentAttributeValue_10((-1));
		return;
	}
}
// System.Void Mono.Xml2.XmlTextReader::AddAttributeWithValue(System.String,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t XmlTextReader_AddAttributeWithValue_m1675559726_MetadataUsageId;
extern "C"  void XmlTextReader_AddAttributeWithValue_m1675559726 (XmlTextReader_t3066586409 * __this, String_t* ___name0, String_t* ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader_AddAttributeWithValue_m1675559726_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XmlAttributeTokenInfo_t922105698 * V_0 = NULL;
	XmlTokenInfo_t2571680784 * V_1 = NULL;
	{
		XmlTextReader_IncrementAttributeToken_m639649356(__this, /*hidden argument*/NULL);
		XmlAttributeTokenInfoU5BU5D_t2707514583* L_0 = __this->get_attributeTokens_7();
		int32_t L_1 = __this->get_currentAttribute_9();
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		V_0 = ((L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2)));
		XmlAttributeTokenInfo_t922105698 * L_3 = V_0;
		XmlNameTable_t3232213908 * L_4 = VirtFuncInvoker0< XmlNameTable_t3232213908 * >::Invoke(17 /* System.Xml.XmlNameTable Mono.Xml2.XmlTextReader::get_NameTable() */, __this);
		String_t* L_5 = ___name0;
		NullCheck(L_4);
		String_t* L_6 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(4 /* System.String System.Xml.XmlNameTable::Add(System.String) */, L_4, L_5);
		NullCheck(L_3);
		((XmlTokenInfo_t2571680784 *)L_3)->set_Name_2(L_6);
		XmlAttributeTokenInfo_t922105698 * L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_7);
		((XmlTokenInfo_t2571680784 *)L_7)->set_Prefix_4(L_8);
		XmlAttributeTokenInfo_t922105698 * L_9 = V_0;
		String_t* L_10 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_9);
		((XmlTokenInfo_t2571680784 *)L_9)->set_NamespaceURI_5(L_10);
		XmlTextReader_IncrementAttributeValueToken_m3026969527(__this, /*hidden argument*/NULL);
		XmlTokenInfoU5BU5D_t2413494705* L_11 = __this->get_attributeValueTokens_8();
		int32_t L_12 = __this->get_currentAttributeValue_10();
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		int32_t L_13 = L_12;
		V_1 = ((L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13)));
		XmlTokenInfo_t2571680784 * L_14 = V_1;
		String_t* L_15 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_16 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_17 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_18 = ___value1;
		XmlTextReader_SetTokenProperties_m237691509(__this, L_14, 3, L_15, L_16, L_17, (bool)0, L_18, (bool)0, /*hidden argument*/NULL);
		XmlAttributeTokenInfo_t922105698 * L_19 = V_0;
		String_t* L_20 = ___value1;
		NullCheck(L_19);
		VirtActionInvoker1< String_t* >::Invoke(5 /* System.Void Mono.Xml2.XmlTextReader/XmlAttributeTokenInfo::set_Value(System.String) */, L_19, L_20);
		int32_t L_21 = __this->get_attributeCount_11();
		__this->set_attributeCount_11(((int32_t)((int32_t)L_21+(int32_t)1)));
		return;
	}
}
// System.Void Mono.Xml2.XmlTextReader::IncrementAttributeToken()
extern Il2CppClass* XmlAttributeTokenInfoU5BU5D_t2707514583_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlAttributeTokenInfo_t922105698_il2cpp_TypeInfo_var;
extern const uint32_t XmlTextReader_IncrementAttributeToken_m639649356_MetadataUsageId;
extern "C"  void XmlTextReader_IncrementAttributeToken_m639649356 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader_IncrementAttributeToken_m639649356_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XmlAttributeTokenInfoU5BU5D_t2707514583* V_0 = NULL;
	{
		int32_t L_0 = __this->get_currentAttribute_9();
		__this->set_currentAttribute_9(((int32_t)((int32_t)L_0+(int32_t)1)));
		XmlAttributeTokenInfoU5BU5D_t2707514583* L_1 = __this->get_attributeTokens_7();
		NullCheck(L_1);
		int32_t L_2 = __this->get_currentAttribute_9();
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))) == ((uint32_t)L_2))))
		{
			goto IL_0045;
		}
	}
	{
		XmlAttributeTokenInfoU5BU5D_t2707514583* L_3 = __this->get_attributeTokens_7();
		NullCheck(L_3);
		V_0 = ((XmlAttributeTokenInfoU5BU5D_t2707514583*)SZArrayNew(XmlAttributeTokenInfoU5BU5D_t2707514583_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))))*(int32_t)2))));
		XmlAttributeTokenInfoU5BU5D_t2707514583* L_4 = __this->get_attributeTokens_7();
		XmlAttributeTokenInfoU5BU5D_t2707514583* L_5 = V_0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_4);
		VirtActionInvoker2< Il2CppArray *, int32_t >::Invoke(9 /* System.Void System.Array::CopyTo(System.Array,System.Int32) */, (Il2CppArray *)(Il2CppArray *)L_4, (Il2CppArray *)(Il2CppArray *)L_5, 0);
		XmlAttributeTokenInfoU5BU5D_t2707514583* L_6 = V_0;
		__this->set_attributeTokens_7(L_6);
	}

IL_0045:
	{
		XmlAttributeTokenInfoU5BU5D_t2707514583* L_7 = __this->get_attributeTokens_7();
		int32_t L_8 = __this->get_currentAttribute_9();
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		if (((L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9))))
		{
			goto IL_006a;
		}
	}
	{
		XmlAttributeTokenInfoU5BU5D_t2707514583* L_10 = __this->get_attributeTokens_7();
		int32_t L_11 = __this->get_currentAttribute_9();
		XmlAttributeTokenInfo_t922105698 * L_12 = (XmlAttributeTokenInfo_t922105698 *)il2cpp_codegen_object_new(XmlAttributeTokenInfo_t922105698_il2cpp_TypeInfo_var);
		XmlAttributeTokenInfo__ctor_m1004432676(L_12, __this, /*hidden argument*/NULL);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		ArrayElementTypeCheck (L_10, L_12);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(L_11), (XmlAttributeTokenInfo_t922105698 *)L_12);
	}

IL_006a:
	{
		XmlAttributeTokenInfoU5BU5D_t2707514583* L_13 = __this->get_attributeTokens_7();
		int32_t L_14 = __this->get_currentAttribute_9();
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		int32_t L_15 = L_14;
		__this->set_currentAttributeToken_5(((L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15))));
		XmlAttributeTokenInfo_t922105698 * L_16 = __this->get_currentAttributeToken_5();
		NullCheck(L_16);
		VirtActionInvoker0::Invoke(6 /* System.Void Mono.Xml2.XmlTextReader/XmlAttributeTokenInfo::Clear() */, L_16);
		return;
	}
}
// System.Void Mono.Xml2.XmlTextReader::IncrementAttributeValueToken()
extern Il2CppClass* XmlTokenInfoU5BU5D_t2413494705_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlTokenInfo_t2571680784_il2cpp_TypeInfo_var;
extern const uint32_t XmlTextReader_IncrementAttributeValueToken_m3026969527_MetadataUsageId;
extern "C"  void XmlTextReader_IncrementAttributeValueToken_m3026969527 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader_IncrementAttributeValueToken_m3026969527_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XmlTokenInfoU5BU5D_t2413494705* V_0 = NULL;
	{
		int32_t L_0 = __this->get_currentAttributeValue_10();
		__this->set_currentAttributeValue_10(((int32_t)((int32_t)L_0+(int32_t)1)));
		XmlTokenInfoU5BU5D_t2413494705* L_1 = __this->get_attributeValueTokens_8();
		NullCheck(L_1);
		int32_t L_2 = __this->get_currentAttributeValue_10();
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))) == ((uint32_t)L_2))))
		{
			goto IL_0045;
		}
	}
	{
		XmlTokenInfoU5BU5D_t2413494705* L_3 = __this->get_attributeValueTokens_8();
		NullCheck(L_3);
		V_0 = ((XmlTokenInfoU5BU5D_t2413494705*)SZArrayNew(XmlTokenInfoU5BU5D_t2413494705_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))))*(int32_t)2))));
		XmlTokenInfoU5BU5D_t2413494705* L_4 = __this->get_attributeValueTokens_8();
		XmlTokenInfoU5BU5D_t2413494705* L_5 = V_0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_4);
		VirtActionInvoker2< Il2CppArray *, int32_t >::Invoke(9 /* System.Void System.Array::CopyTo(System.Array,System.Int32) */, (Il2CppArray *)(Il2CppArray *)L_4, (Il2CppArray *)(Il2CppArray *)L_5, 0);
		XmlTokenInfoU5BU5D_t2413494705* L_6 = V_0;
		__this->set_attributeValueTokens_8(L_6);
	}

IL_0045:
	{
		XmlTokenInfoU5BU5D_t2413494705* L_7 = __this->get_attributeValueTokens_8();
		int32_t L_8 = __this->get_currentAttributeValue_10();
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		if (((L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9))))
		{
			goto IL_006a;
		}
	}
	{
		XmlTokenInfoU5BU5D_t2413494705* L_10 = __this->get_attributeValueTokens_8();
		int32_t L_11 = __this->get_currentAttributeValue_10();
		XmlTokenInfo_t2571680784 * L_12 = (XmlTokenInfo_t2571680784 *)il2cpp_codegen_object_new(XmlTokenInfo_t2571680784_il2cpp_TypeInfo_var);
		XmlTokenInfo__ctor_m2227356974(L_12, __this, /*hidden argument*/NULL);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		ArrayElementTypeCheck (L_10, L_12);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(L_11), (XmlTokenInfo_t2571680784 *)L_12);
	}

IL_006a:
	{
		XmlTokenInfoU5BU5D_t2413494705* L_13 = __this->get_attributeValueTokens_8();
		int32_t L_14 = __this->get_currentAttributeValue_10();
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		int32_t L_15 = L_14;
		__this->set_currentAttributeValueToken_6(((L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15))));
		XmlTokenInfo_t2571680784 * L_16 = __this->get_currentAttributeValueToken_6();
		NullCheck(L_16);
		VirtActionInvoker0::Invoke(6 /* System.Void Mono.Xml2.XmlTextReader/XmlTokenInfo::Clear() */, L_16);
		return;
	}
}
// System.Void Mono.Xml2.XmlTextReader::ReadAttributeValueTokens(System.Int32)
extern Il2CppClass* XmlChar_t3591879093_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_1_t1355893759_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t4261813147_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral447355090;
extern Il2CppCodeGenString* _stringLiteral347082787;
extern Il2CppCodeGenString* _stringLiteral159631238;
extern Il2CppCodeGenString* _stringLiteral241322499;
extern const uint32_t XmlTextReader_ReadAttributeValueTokens_m3644060952_MetadataUsageId;
extern "C"  void XmlTextReader_ReadAttributeValueTokens_m3644060952 (XmlTextReader_t3066586409 * __this, int32_t ___dummyQuoteChar0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader_ReadAttributeValueTokens_m3644060952_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	bool V_3 = false;
	int32_t V_4 = 0;
	String_t* V_5 = NULL;
	int32_t V_6 = 0;
	String_t* V_7 = NULL;
	uint16_t V_8 = 0x0;
	Il2CppObject* V_9 = NULL;
	int32_t V_10 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = ___dummyQuoteChar0;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_1 = XmlTextReader_ReadChar_m3857716364(__this, /*hidden argument*/NULL);
		G_B3_0 = L_1;
		goto IL_0013;
	}

IL_0012:
	{
		int32_t L_2 = ___dummyQuoteChar0;
		G_B3_0 = L_2;
	}

IL_0013:
	{
		V_0 = G_B3_0;
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)39))))
		{
			goto IL_0030;
		}
	}
	{
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) == ((int32_t)((int32_t)34))))
		{
			goto IL_0030;
		}
	}
	{
		XmlException_t3490696160 * L_5 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral447355090, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0030:
	{
		XmlAttributeTokenInfo_t922105698 * L_6 = __this->get_currentAttributeToken_5();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		((XmlTokenInfo_t2571680784 *)L_6)->set_QuoteChar_7((((int32_t)((uint16_t)L_7))));
		XmlTextReader_IncrementAttributeValueToken_m3026969527(__this, /*hidden argument*/NULL);
		XmlAttributeTokenInfo_t922105698 * L_8 = __this->get_currentAttributeToken_5();
		int32_t L_9 = __this->get_currentAttributeValue_10();
		NullCheck(L_8);
		L_8->set_ValueTokenStartIndex_13(L_9);
		XmlTokenInfo_t2571680784 * L_10 = __this->get_currentAttributeValueToken_6();
		int32_t L_11 = __this->get_line_34();
		NullCheck(L_10);
		L_10->set_LineNumber_8(L_11);
		XmlTokenInfo_t2571680784 * L_12 = __this->get_currentAttributeValueToken_6();
		int32_t L_13 = __this->get_column_35();
		NullCheck(L_12);
		L_12->set_LinePosition_9(L_13);
		V_1 = (bool)0;
		V_2 = (bool)1;
		V_3 = (bool)1;
		V_4 = 0;
		XmlTokenInfo_t2571680784 * L_14 = __this->get_currentAttributeValueToken_6();
		StringBuilder_t3822575854 * L_15 = __this->get_valueBuffer_27();
		NullCheck(L_15);
		int32_t L_16 = StringBuilder_get_Length_m2443133099(L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		L_14->set_ValueBufferStart_10(L_16);
		goto IL_031f;
	}

IL_009a:
	{
		int32_t L_17 = XmlTextReader_ReadChar_m3857716364(__this, /*hidden argument*/NULL);
		V_4 = L_17;
		int32_t L_18 = V_4;
		int32_t L_19 = V_0;
		if ((!(((uint32_t)L_18) == ((uint32_t)L_19))))
		{
			goto IL_00af;
		}
	}
	{
		goto IL_0325;
	}

IL_00af:
	{
		bool L_20 = V_1;
		if (!L_20)
		{
			goto IL_00f7;
		}
	}
	{
		XmlTextReader_IncrementAttributeValueToken_m3026969527(__this, /*hidden argument*/NULL);
		XmlTokenInfo_t2571680784 * L_21 = __this->get_currentAttributeValueToken_6();
		StringBuilder_t3822575854 * L_22 = __this->get_valueBuffer_27();
		NullCheck(L_22);
		int32_t L_23 = StringBuilder_get_Length_m2443133099(L_22, /*hidden argument*/NULL);
		NullCheck(L_21);
		L_21->set_ValueBufferStart_10(L_23);
		XmlTokenInfo_t2571680784 * L_24 = __this->get_currentAttributeValueToken_6();
		int32_t L_25 = __this->get_line_34();
		NullCheck(L_24);
		L_24->set_LineNumber_8(L_25);
		XmlTokenInfo_t2571680784 * L_26 = __this->get_currentAttributeValueToken_6();
		int32_t L_27 = __this->get_column_35();
		NullCheck(L_26);
		L_26->set_LinePosition_9(L_27);
		V_1 = (bool)0;
		V_2 = (bool)1;
	}

IL_00f7:
	{
		int32_t L_28 = V_4;
		V_10 = L_28;
		int32_t L_29 = V_10;
		if (((int32_t)((int32_t)L_29-(int32_t)((int32_t)9))) == 0)
		{
			goto IL_0199;
		}
		if (((int32_t)((int32_t)L_29-(int32_t)((int32_t)9))) == 1)
		{
			goto IL_0199;
		}
		if (((int32_t)((int32_t)L_29-(int32_t)((int32_t)9))) == 2)
		{
			goto IL_0119;
		}
		if (((int32_t)((int32_t)L_29-(int32_t)((int32_t)9))) == 3)
		{
			goto IL_0119;
		}
		if (((int32_t)((int32_t)L_29-(int32_t)((int32_t)9))) == 4)
		{
			goto IL_015e;
		}
	}

IL_0119:
	{
		int32_t L_30 = V_10;
		if ((((int32_t)L_30) == ((int32_t)(-1))))
		{
			goto IL_0144;
		}
	}
	{
		int32_t L_31 = V_10;
		if ((((int32_t)L_31) == ((int32_t)((int32_t)38))))
		{
			goto IL_01b2;
		}
	}
	{
		int32_t L_32 = V_10;
		if ((((int32_t)L_32) == ((int32_t)((int32_t)60))))
		{
			goto IL_0138;
		}
	}
	{
		goto IL_02cd;
	}

IL_0138:
	{
		XmlException_t3490696160 * L_33 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral347082787, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_33);
	}

IL_0144:
	{
		int32_t L_34 = ___dummyQuoteChar0;
		if ((((int32_t)L_34) >= ((int32_t)0)))
		{
			goto IL_0157;
		}
	}
	{
		XmlException_t3490696160 * L_35 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral159631238, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_35);
	}

IL_0157:
	{
		V_3 = (bool)0;
		goto IL_031d;
	}

IL_015e:
	{
		bool L_36 = __this->get_normalization_47();
		if (L_36)
		{
			goto IL_016e;
		}
	}
	{
		goto IL_02cd;
	}

IL_016e:
	{
		int32_t L_37 = XmlTextReader_PeekChar_m254641297(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_37) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_0180;
		}
	}
	{
		goto IL_031f;
	}

IL_0180:
	{
		bool L_38 = __this->get_normalization_47();
		if (L_38)
		{
			goto IL_0190;
		}
	}
	{
		goto IL_02cd;
	}

IL_0190:
	{
		V_4 = ((int32_t)32);
		goto IL_02cd;
	}

IL_0199:
	{
		bool L_39 = __this->get_normalization_47();
		if (L_39)
		{
			goto IL_01a9;
		}
	}
	{
		goto IL_02cd;
	}

IL_01a9:
	{
		V_4 = ((int32_t)32);
		goto IL_02cd;
	}

IL_01b2:
	{
		int32_t L_40 = XmlTextReader_PeekChar_m254641297(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_40) == ((uint32_t)((int32_t)35)))))
		{
			goto IL_01dc;
		}
	}
	{
		XmlTextReader_Advance_m673767315(__this, ((int32_t)35), /*hidden argument*/NULL);
		int32_t L_41 = XmlTextReader_ReadCharacterReference_m3777658776(__this, /*hidden argument*/NULL);
		V_4 = L_41;
		int32_t L_42 = V_4;
		XmlTextReader_AppendValueChar_m3744790334(__this, L_42, /*hidden argument*/NULL);
		goto IL_031d;
	}

IL_01dc:
	{
		String_t* L_43 = XmlTextReader_ReadName_m2874594848(__this, /*hidden argument*/NULL);
		V_5 = L_43;
		XmlTextReader_Expect_m2788845516(__this, ((int32_t)59), /*hidden argument*/NULL);
		String_t* L_44 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(XmlChar_t3591879093_il2cpp_TypeInfo_var);
		int32_t L_45 = XmlChar_GetPredefinedEntity_m4157892537(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		V_6 = L_45;
		int32_t L_46 = V_6;
		if ((((int32_t)L_46) >= ((int32_t)0)))
		{
			goto IL_02c0;
		}
	}
	{
		String_t* L_47 = V_5;
		XmlTextReader_CheckAttributeEntityReferenceWFC_m1075790688(__this, L_47, /*hidden argument*/NULL);
		int32_t L_48 = __this->get_entityHandling_51();
		if ((!(((uint32_t)L_48) == ((uint32_t)1))))
		{
			goto IL_0262;
		}
	}
	{
		DTDObjectModel_t709926554 * L_49 = XmlTextReader_get_DTD_m3845091782(__this, /*hidden argument*/NULL);
		String_t* L_50 = V_5;
		NullCheck(L_49);
		String_t* L_51 = DTDObjectModel_GenerateEntityAttributeText_m1046632615(L_49, L_50, /*hidden argument*/NULL);
		V_7 = L_51;
		String_t* L_52 = V_7;
		NullCheck(L_52);
		Il2CppObject* L_53 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Char>::GetEnumerator() */, IEnumerable_1_t1355893759_il2cpp_TypeInfo_var, L_52);
		V_9 = L_53;
	}

IL_0229:
	try
	{ // begin try (depth: 1)
		{
			goto IL_023f;
		}

IL_022e:
		{
			Il2CppObject* L_54 = V_9;
			NullCheck(L_54);
			uint16_t L_55 = InterfaceFuncInvoker0< uint16_t >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Char>::get_Current() */, IEnumerator_1_t4261813147_il2cpp_TypeInfo_var, L_54);
			V_8 = L_55;
			uint16_t L_56 = V_8;
			XmlTextReader_AppendValueChar_m3744790334(__this, L_56, /*hidden argument*/NULL);
		}

IL_023f:
		{
			Il2CppObject* L_57 = V_9;
			NullCheck(L_57);
			bool L_58 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_57);
			if (L_58)
			{
				goto IL_022e;
			}
		}

IL_024b:
		{
			IL2CPP_LEAVE(0x25D, FINALLY_0250);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0250;
	}

FINALLY_0250:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_59 = V_9;
			if (L_59)
			{
				goto IL_0255;
			}
		}

IL_0254:
		{
			IL2CPP_END_FINALLY(592)
		}

IL_0255:
		{
			Il2CppObject* L_60 = V_9;
			NullCheck(L_60);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_60);
			IL2CPP_END_FINALLY(592)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(592)
	{
		IL2CPP_JUMP_TBL(0x25D, IL_025d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_025d:
	{
		goto IL_02bb;
	}

IL_0262:
	{
		XmlTokenInfo_t2571680784 * L_61 = __this->get_currentAttributeValueToken_6();
		StringBuilder_t3822575854 * L_62 = __this->get_valueBuffer_27();
		NullCheck(L_62);
		int32_t L_63 = StringBuilder_get_Length_m2443133099(L_62, /*hidden argument*/NULL);
		NullCheck(L_61);
		L_61->set_ValueBufferEnd_11(L_63);
		XmlTokenInfo_t2571680784 * L_64 = __this->get_currentAttributeValueToken_6();
		NullCheck(L_64);
		L_64->set_NodeType_12(3);
		bool L_65 = V_2;
		if (L_65)
		{
			goto IL_0290;
		}
	}
	{
		XmlTextReader_IncrementAttributeValueToken_m3026969527(__this, /*hidden argument*/NULL);
	}

IL_0290:
	{
		XmlTokenInfo_t2571680784 * L_66 = __this->get_currentAttributeValueToken_6();
		String_t* L_67 = V_5;
		NullCheck(L_66);
		L_66->set_Name_2(L_67);
		XmlTokenInfo_t2571680784 * L_68 = __this->get_currentAttributeValueToken_6();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_69 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_68);
		VirtActionInvoker1< String_t* >::Invoke(5 /* System.Void Mono.Xml2.XmlTextReader/XmlTokenInfo::set_Value(System.String) */, L_68, L_69);
		XmlTokenInfo_t2571680784 * L_70 = __this->get_currentAttributeValueToken_6();
		NullCheck(L_70);
		L_70->set_NodeType_12(5);
		V_1 = (bool)1;
	}

IL_02bb:
	{
		goto IL_02c8;
	}

IL_02c0:
	{
		int32_t L_71 = V_6;
		XmlTextReader_AppendValueChar_m3744790334(__this, L_71, /*hidden argument*/NULL);
	}

IL_02c8:
	{
		goto IL_031d;
	}

IL_02cd:
	{
		bool L_72 = XmlTextReader_get_CharacterChecking_m3210588674(__this, /*hidden argument*/NULL);
		if (!L_72)
		{
			goto IL_02f0;
		}
	}
	{
		int32_t L_73 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(XmlChar_t3591879093_il2cpp_TypeInfo_var);
		bool L_74 = XmlChar_IsInvalid_m3338941186(NULL /*static, unused*/, L_73, /*hidden argument*/NULL);
		if (!L_74)
		{
			goto IL_02f0;
		}
	}
	{
		XmlException_t3490696160 * L_75 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral241322499, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_75);
	}

IL_02f0:
	{
		int32_t L_76 = V_4;
		if ((((int32_t)L_76) >= ((int32_t)((int32_t)65535))))
		{
			goto IL_0310;
		}
	}
	{
		StringBuilder_t3822575854 * L_77 = __this->get_valueBuffer_27();
		int32_t L_78 = V_4;
		NullCheck(L_77);
		StringBuilder_Append_m2143093878(L_77, (((int32_t)((uint16_t)L_78))), /*hidden argument*/NULL);
		goto IL_0318;
	}

IL_0310:
	{
		int32_t L_79 = V_4;
		XmlTextReader_AppendSurrogatePairValueChar_m704355490(__this, L_79, /*hidden argument*/NULL);
	}

IL_0318:
	{
		goto IL_031d;
	}

IL_031d:
	{
		V_2 = (bool)0;
	}

IL_031f:
	{
		bool L_80 = V_3;
		if (L_80)
		{
			goto IL_009a;
		}
	}

IL_0325:
	{
		bool L_81 = V_1;
		if (L_81)
		{
			goto IL_034d;
		}
	}
	{
		XmlTokenInfo_t2571680784 * L_82 = __this->get_currentAttributeValueToken_6();
		StringBuilder_t3822575854 * L_83 = __this->get_valueBuffer_27();
		NullCheck(L_83);
		int32_t L_84 = StringBuilder_get_Length_m2443133099(L_83, /*hidden argument*/NULL);
		NullCheck(L_82);
		L_82->set_ValueBufferEnd_11(L_84);
		XmlTokenInfo_t2571680784 * L_85 = __this->get_currentAttributeValueToken_6();
		NullCheck(L_85);
		L_85->set_NodeType_12(3);
	}

IL_034d:
	{
		XmlAttributeTokenInfo_t922105698 * L_86 = __this->get_currentAttributeToken_5();
		int32_t L_87 = __this->get_currentAttributeValue_10();
		NullCheck(L_86);
		L_86->set_ValueTokenEndIndex_14(L_87);
		return;
	}
}
// System.Void Mono.Xml2.XmlTextReader::CheckAttributeEntityReferenceWFC(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2807658331;
extern Il2CppCodeGenString* _stringLiteral749461012;
extern Il2CppCodeGenString* _stringLiteral4064338322;
extern Il2CppCodeGenString* _stringLiteral3325449728;
extern const uint32_t XmlTextReader_CheckAttributeEntityReferenceWFC_m1075790688_MetadataUsageId;
extern "C"  void XmlTextReader_CheckAttributeEntityReferenceWFC_m1075790688 (XmlTextReader_t3066586409 * __this, String_t* ___entName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader_CheckAttributeEntityReferenceWFC_m1075790688_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DTDEntityDeclaration_t2806387719 * V_0 = NULL;
	DTDEntityDeclaration_t2806387719 * G_B3_0 = NULL;
	{
		DTDObjectModel_t709926554 * L_0 = XmlTextReader_get_DTD_m3845091782(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		G_B3_0 = ((DTDEntityDeclaration_t2806387719 *)(NULL));
		goto IL_0022;
	}

IL_0011:
	{
		DTDObjectModel_t709926554 * L_1 = XmlTextReader_get_DTD_m3845091782(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		DTDEntityDeclarationCollection_t4226410245 * L_2 = DTDObjectModel_get_EntityDecls_m3251787732(L_1, /*hidden argument*/NULL);
		String_t* L_3 = ___entName0;
		NullCheck(L_2);
		DTDEntityDeclaration_t2806387719 * L_4 = DTDEntityDeclarationCollection_get_Item_m2370831310(L_2, L_3, /*hidden argument*/NULL);
		G_B3_0 = L_4;
	}

IL_0022:
	{
		V_0 = G_B3_0;
		DTDEntityDeclaration_t2806387719 * L_5 = V_0;
		if (L_5)
		{
			goto IL_0064;
		}
	}
	{
		int32_t L_6 = __this->get_entityHandling_51();
		if ((((int32_t)L_6) == ((int32_t)1)))
		{
			goto IL_0051;
		}
	}
	{
		DTDObjectModel_t709926554 * L_7 = XmlTextReader_get_DTD_m3845091782(__this, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0063;
		}
	}
	{
		XmlResolver_t2502213349 * L_8 = __this->get_resolver_46();
		if (!L_8)
		{
			goto IL_0063;
		}
	}
	{
		DTDEntityDeclaration_t2806387719 * L_9 = V_0;
		if (L_9)
		{
			goto IL_0063;
		}
	}

IL_0051:
	{
		String_t* L_10 = ___entName0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral2807658331, L_10, /*hidden argument*/NULL);
		XmlException_t3490696160 * L_12 = XmlTextReader_NotWFError_m304583423(__this, L_11, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12);
	}

IL_0063:
	{
		return;
	}

IL_0064:
	{
		DTDEntityDeclaration_t2806387719 * L_13 = V_0;
		NullCheck(L_13);
		bool L_14 = DTDEntityDeclaration_get_HasExternalReference_m850645615(L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_007b;
		}
	}
	{
		XmlException_t3490696160 * L_15 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral749461012, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_15);
	}

IL_007b:
	{
		bool L_16 = __this->get_isStandalone_24();
		if (!L_16)
		{
			goto IL_009d;
		}
	}
	{
		DTDEntityDeclaration_t2806387719 * L_17 = V_0;
		NullCheck(L_17);
		bool L_18 = DTDNode_get_IsInternalSubset_m3131993229(L_17, /*hidden argument*/NULL);
		if (L_18)
		{
			goto IL_009d;
		}
	}
	{
		XmlException_t3490696160 * L_19 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral4064338322, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_19);
	}

IL_009d:
	{
		DTDEntityDeclaration_t2806387719 * L_20 = V_0;
		NullCheck(L_20);
		String_t* L_21 = DTDEntityDeclaration_get_EntityValue_m1737901506(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		int32_t L_22 = String_IndexOf_m2775210486(L_21, ((int32_t)60), /*hidden argument*/NULL);
		if ((((int32_t)L_22) < ((int32_t)0)))
		{
			goto IL_00bc;
		}
	}
	{
		XmlException_t3490696160 * L_23 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral3325449728, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_23);
	}

IL_00bc:
	{
		return;
	}
}
// System.Void Mono.Xml2.XmlTextReader::ReadProcessingInstruction()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* CultureInfo_t3603717042_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlChar_t3591879093_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral118807;
extern Il2CppCodeGenString* _stringLiteral67645141;
extern Il2CppCodeGenString* _stringLiteral2995707394;
extern Il2CppCodeGenString* _stringLiteral241322499;
extern const uint32_t XmlTextReader_ReadProcessingInstruction_m4247906757_MetadataUsageId;
extern "C"  void XmlTextReader_ReadProcessingInstruction_m4247906757 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader_ReadProcessingInstruction_m4247906757_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	{
		String_t* L_0 = XmlTextReader_ReadName_m2874594848(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_1, _stringLiteral118807, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_003d;
		}
	}
	{
		String_t* L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3603717042_il2cpp_TypeInfo_var);
		CultureInfo_t3603717042 * L_4 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		String_t* L_5 = String_ToLower_m2140020155(L_3, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_5, _stringLiteral118807, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003d;
		}
	}
	{
		XmlException_t3490696160 * L_7 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral67645141, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_003d:
	{
		bool L_8 = XmlTextReader_SkipWhitespace_m1854637366(__this, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0061;
		}
	}
	{
		int32_t L_9 = XmlTextReader_PeekChar_m254641297(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_9) == ((int32_t)((int32_t)63))))
		{
			goto IL_0061;
		}
	}
	{
		XmlException_t3490696160 * L_10 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral2995707394, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
	}

IL_0061:
	{
		XmlTextReader_ClearValueBuffer_m495962918(__this, /*hidden argument*/NULL);
		goto IL_00be;
	}

IL_006c:
	{
		int32_t L_11 = V_1;
		XmlTextReader_Advance_m673767315(__this, L_11, /*hidden argument*/NULL);
		int32_t L_12 = V_1;
		if ((!(((uint32_t)L_12) == ((uint32_t)((int32_t)63)))))
		{
			goto IL_0095;
		}
	}
	{
		int32_t L_13 = XmlTextReader_PeekChar_m254641297(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_13) == ((uint32_t)((int32_t)62)))))
		{
			goto IL_0095;
		}
	}
	{
		XmlTextReader_Advance_m673767315(__this, ((int32_t)62), /*hidden argument*/NULL);
		goto IL_00cc;
	}

IL_0095:
	{
		bool L_14 = XmlTextReader_get_CharacterChecking_m3210588674(__this, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00b7;
		}
	}
	{
		int32_t L_15 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(XmlChar_t3591879093_il2cpp_TypeInfo_var);
		bool L_16 = XmlChar_IsInvalid_m3338941186(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00b7;
		}
	}
	{
		XmlException_t3490696160 * L_17 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral241322499, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_17);
	}

IL_00b7:
	{
		int32_t L_18 = V_1;
		XmlTextReader_AppendValueChar_m3744790334(__this, L_18, /*hidden argument*/NULL);
	}

IL_00be:
	{
		int32_t L_19 = XmlTextReader_PeekChar_m254641297(__this, /*hidden argument*/NULL);
		int32_t L_20 = L_19;
		V_1 = L_20;
		if ((!(((uint32_t)L_20) == ((uint32_t)(-1)))))
		{
			goto IL_006c;
		}
	}

IL_00cc:
	{
		String_t* L_21 = V_0;
		bool L_22 = Object_ReferenceEquals_m3695130242(NULL /*static, unused*/, L_21, _stringLiteral118807, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_00e7;
		}
	}
	{
		XmlTextReader_VerifyXmlDeclaration_m4289163742(__this, /*hidden argument*/NULL);
		goto IL_010b;
	}

IL_00e7:
	{
		int32_t L_23 = __this->get_currentState_40();
		if (L_23)
		{
			goto IL_00fa;
		}
	}
	{
		__this->set_currentState_40(((int32_t)17));
	}

IL_00fa:
	{
		String_t* L_24 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_25 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_26 = V_0;
		XmlTextReader_SetProperties_m3034003792(__this, 7, L_24, L_25, L_26, (bool)0, (String_t*)NULL, (bool)1, /*hidden argument*/NULL);
	}

IL_010b:
	{
		return;
	}
}
// System.Void Mono.Xml2.XmlTextReader::VerifyXmlDeclaration()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlChar_t3591879093_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlStreamReader_t1190433730_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t180559927_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral485059713;
extern Il2CppCodeGenString* _stringLiteral351608024;
extern Il2CppCodeGenString* _stringLiteral48563;
extern Il2CppCodeGenString* _stringLiteral3885010282;
extern Il2CppCodeGenString* _stringLiteral1711222099;
extern Il2CppCodeGenString* _stringLiteral3083430319;
extern Il2CppCodeGenString* _stringLiteral3010322501;
extern Il2CppCodeGenString* _stringLiteral119527;
extern Il2CppCodeGenString* _stringLiteral3521;
extern Il2CppCodeGenString* _stringLiteral2446060078;
extern Il2CppCodeGenString* _stringLiteral3889747350;
extern Il2CppCodeGenString* _stringLiteral1316179793;
extern Il2CppCodeGenString* _stringLiteral118807;
extern const uint32_t XmlTextReader_VerifyXmlDeclaration_m4289163742_MetadataUsageId;
extern "C"  void XmlTextReader_VerifyXmlDeclaration_m4289163742 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader_VerifyXmlDeclaration_m4289163742_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	String_t* V_5 = NULL;
	int32_t V_6 = 0;
	{
		bool L_0 = __this->get_allowMultipleRoot_23();
		if (L_0)
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_1 = __this->get_currentState_40();
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		XmlException_t3490696160 * L_2 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral485059713, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0022:
	{
		__this->set_currentState_40(((int32_t)17));
		String_t* L_3 = XmlTextReader_CreateValueString_m2171097961(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		XmlTextReader_ClearAttributes_m2951900356(__this, /*hidden argument*/NULL);
		V_1 = 0;
		V_2 = (String_t*)NULL;
		V_3 = (String_t*)NULL;
		String_t* L_4 = V_0;
		XmlTextReader_ParseAttributeFromString_m132975569(__this, L_4, (&V_1), (&V_4), (&V_5), /*hidden argument*/NULL);
		String_t* L_5 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_5, _stringLiteral351608024, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_006c;
		}
	}
	{
		String_t* L_7 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_8 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_7, _stringLiteral48563, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0078;
		}
	}

IL_006c:
	{
		XmlException_t3490696160 * L_9 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral3885010282, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0078:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_4 = L_10;
		String_t* L_11 = V_0;
		bool L_12 = XmlTextReader_SkipWhitespaceInString_m1769166187(__this, L_11, (&V_1), /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_00a6;
		}
	}
	{
		int32_t L_13 = V_1;
		String_t* L_14 = V_0;
		NullCheck(L_14);
		int32_t L_15 = String_get_Length_m2979997331(L_14, /*hidden argument*/NULL);
		if ((((int32_t)L_13) >= ((int32_t)L_15)))
		{
			goto IL_00a6;
		}
	}
	{
		String_t* L_16 = V_0;
		XmlTextReader_ParseAttributeFromString_m132975569(__this, L_16, (&V_1), (&V_4), (&V_5), /*hidden argument*/NULL);
	}

IL_00a6:
	{
		String_t* L_17 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_18 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_17, _stringLiteral1711222099, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_0140;
		}
	}
	{
		String_t* L_19 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(XmlChar_t3591879093_il2cpp_TypeInfo_var);
		bool L_20 = XmlChar_IsValidIANAEncoding_m2400640270(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_00cf;
		}
	}
	{
		XmlException_t3490696160 * L_21 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral3083430319, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}

IL_00cf:
	{
		TextReader_t1534522647 * L_22 = __this->get_reader_28();
		if (!((XmlStreamReader_t1190433730 *)IsInstClass(L_22, XmlStreamReader_t1190433730_il2cpp_TypeInfo_var)))
		{
			goto IL_00ff;
		}
	}
	{
		XmlParserContext_t3629084577 * L_23 = __this->get_parserContext_12();
		TextReader_t1534522647 * L_24 = __this->get_reader_28();
		NullCheck(((XmlStreamReader_t1190433730 *)CastclassClass(L_24, XmlStreamReader_t1190433730_il2cpp_TypeInfo_var)));
		Encoding_t180559927 * L_25 = NonBlockingStreamReader_get_Encoding_m3739939814(((XmlStreamReader_t1190433730 *)CastclassClass(L_24, XmlStreamReader_t1190433730_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		NullCheck(L_23);
		XmlParserContext_set_Encoding_m493475469(L_23, L_25, /*hidden argument*/NULL);
		goto IL_010f;
	}

IL_00ff:
	{
		XmlParserContext_t3629084577 * L_26 = __this->get_parserContext_12();
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t180559927_il2cpp_TypeInfo_var);
		Encoding_t180559927 * L_27 = Encoding_get_Unicode_m2158134329(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_26);
		XmlParserContext_set_Encoding_m493475469(L_26, L_27, /*hidden argument*/NULL);
	}

IL_010f:
	{
		String_t* L_28 = V_5;
		V_2 = L_28;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_29 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_4 = L_29;
		String_t* L_30 = V_0;
		bool L_31 = XmlTextReader_SkipWhitespaceInString_m1769166187(__this, L_30, (&V_1), /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_0140;
		}
	}
	{
		int32_t L_32 = V_1;
		String_t* L_33 = V_0;
		NullCheck(L_33);
		int32_t L_34 = String_get_Length_m2979997331(L_33, /*hidden argument*/NULL);
		if ((((int32_t)L_32) >= ((int32_t)L_34)))
		{
			goto IL_0140;
		}
	}
	{
		String_t* L_35 = V_0;
		XmlTextReader_ParseAttributeFromString_m132975569(__this, L_35, (&V_1), (&V_4), (&V_5), /*hidden argument*/NULL);
	}

IL_0140:
	{
		String_t* L_36 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_37 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_36, _stringLiteral3010322501, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_01a3;
		}
	}
	{
		String_t* L_38 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_39 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_38, _stringLiteral119527, /*hidden argument*/NULL);
		__this->set_isStandalone_24(L_39);
		String_t* L_40 = V_5;
		bool L_41 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_40, _stringLiteral119527, /*hidden argument*/NULL);
		if (!L_41)
		{
			goto IL_0191;
		}
	}
	{
		String_t* L_42 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_43 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_42, _stringLiteral3521, /*hidden argument*/NULL);
		if (!L_43)
		{
			goto IL_0191;
		}
	}
	{
		XmlException_t3490696160 * L_44 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral2446060078, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_44);
	}

IL_0191:
	{
		String_t* L_45 = V_5;
		V_3 = L_45;
		String_t* L_46 = V_0;
		XmlTextReader_SkipWhitespaceInString_m1769166187(__this, L_46, (&V_1), /*hidden argument*/NULL);
		goto IL_01c2;
	}

IL_01a3:
	{
		String_t* L_47 = V_4;
		NullCheck(L_47);
		int32_t L_48 = String_get_Length_m2979997331(L_47, /*hidden argument*/NULL);
		if (!L_48)
		{
			goto IL_01c2;
		}
	}
	{
		String_t* L_49 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_50 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral3889747350, L_49, /*hidden argument*/NULL);
		XmlException_t3490696160 * L_51 = XmlTextReader_NotWFError_m304583423(__this, L_50, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_51);
	}

IL_01c2:
	{
		int32_t L_52 = V_1;
		String_t* L_53 = V_0;
		NullCheck(L_53);
		int32_t L_54 = String_get_Length_m2979997331(L_53, /*hidden argument*/NULL);
		if ((((int32_t)L_52) >= ((int32_t)L_54)))
		{
			goto IL_01da;
		}
	}
	{
		XmlException_t3490696160 * L_55 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral1316179793, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_55);
	}

IL_01da:
	{
		XmlTextReader_AddAttributeWithValue_m1675559726(__this, _stringLiteral351608024, _stringLiteral48563, /*hidden argument*/NULL);
		String_t* L_56 = V_2;
		if (!L_56)
		{
			goto IL_01fc;
		}
	}
	{
		String_t* L_57 = V_2;
		XmlTextReader_AddAttributeWithValue_m1675559726(__this, _stringLiteral1711222099, L_57, /*hidden argument*/NULL);
	}

IL_01fc:
	{
		String_t* L_58 = V_3;
		if (!L_58)
		{
			goto IL_020e;
		}
	}
	{
		String_t* L_59 = V_3;
		XmlTextReader_AddAttributeWithValue_m1675559726(__this, _stringLiteral3010322501, L_59, /*hidden argument*/NULL);
	}

IL_020e:
	{
		int32_t L_60 = (-1);
		V_6 = L_60;
		__this->set_currentAttributeValue_10(L_60);
		int32_t L_61 = V_6;
		__this->set_currentAttribute_9(L_61);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_62 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_63 = V_0;
		XmlTextReader_SetProperties_m3034003792(__this, ((int32_t)17), _stringLiteral118807, L_62, _stringLiteral118807, (bool)0, L_63, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Mono.Xml2.XmlTextReader::SkipWhitespaceInString(System.String,System.Int32&)
extern Il2CppClass* XmlChar_t3591879093_il2cpp_TypeInfo_var;
extern const uint32_t XmlTextReader_SkipWhitespaceInString_m1769166187_MetadataUsageId;
extern "C"  bool XmlTextReader_SkipWhitespaceInString_m1769166187 (XmlTextReader_t3066586409 * __this, String_t* ___text0, int32_t* ___idx1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader_SkipWhitespaceInString_m1769166187_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t* L_0 = ___idx1;
		V_0 = (*((int32_t*)L_0));
		goto IL_000e;
	}

IL_0008:
	{
		int32_t* L_1 = ___idx1;
		int32_t* L_2 = ___idx1;
		*((int32_t*)(L_1)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_2))+(int32_t)1));
	}

IL_000e:
	{
		int32_t* L_3 = ___idx1;
		String_t* L_4 = ___text0;
		NullCheck(L_4);
		int32_t L_5 = String_get_Length_m2979997331(L_4, /*hidden argument*/NULL);
		if ((((int32_t)(*((int32_t*)L_3))) >= ((int32_t)L_5)))
		{
			goto IL_002d;
		}
	}
	{
		String_t* L_6 = ___text0;
		int32_t* L_7 = ___idx1;
		NullCheck(L_6);
		uint16_t L_8 = String_get_Chars_m3015341861(L_6, (*((int32_t*)L_7)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(XmlChar_t3591879093_il2cpp_TypeInfo_var);
		bool L_9 = XmlChar_IsWhitespace_m1427848566(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0008;
		}
	}

IL_002d:
	{
		int32_t* L_10 = ___idx1;
		int32_t L_11 = V_0;
		return (bool)((((int32_t)((int32_t)((int32_t)(*((int32_t*)L_10))-(int32_t)L_11))) > ((int32_t)0))? 1 : 0);
	}
}
// System.Void Mono.Xml2.XmlTextReader::ParseAttributeFromString(System.String,System.Int32&,System.String&,System.String&)
extern Il2CppClass* XmlChar_t3591879093_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral548677155;
extern Il2CppCodeGenString* _stringLiteral3083698786;
extern const uint32_t XmlTextReader_ParseAttributeFromString_m132975569_MetadataUsageId;
extern "C"  void XmlTextReader_ParseAttributeFromString_m132975569 (XmlTextReader_t3066586409 * __this, String_t* ___src0, int32_t* ___idx1, String_t** ___name2, String_t** ___value3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader_ParseAttributeFromString_m132975569_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	uint16_t V_1 = 0x0;
	{
		goto IL_000b;
	}

IL_0005:
	{
		int32_t* L_0 = ___idx1;
		int32_t* L_1 = ___idx1;
		*((int32_t*)(L_0)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_1))+(int32_t)1));
	}

IL_000b:
	{
		int32_t* L_2 = ___idx1;
		String_t* L_3 = ___src0;
		NullCheck(L_3);
		int32_t L_4 = String_get_Length_m2979997331(L_3, /*hidden argument*/NULL);
		if ((((int32_t)(*((int32_t*)L_2))) >= ((int32_t)L_4)))
		{
			goto IL_002a;
		}
	}
	{
		String_t* L_5 = ___src0;
		int32_t* L_6 = ___idx1;
		NullCheck(L_5);
		uint16_t L_7 = String_get_Chars_m3015341861(L_5, (*((int32_t*)L_6)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(XmlChar_t3591879093_il2cpp_TypeInfo_var);
		bool L_8 = XmlChar_IsWhitespace_m1427848566(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0005;
		}
	}

IL_002a:
	{
		int32_t* L_9 = ___idx1;
		V_0 = (*((int32_t*)L_9));
		goto IL_0038;
	}

IL_0032:
	{
		int32_t* L_10 = ___idx1;
		int32_t* L_11 = ___idx1;
		*((int32_t*)(L_10)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_11))+(int32_t)1));
	}

IL_0038:
	{
		int32_t* L_12 = ___idx1;
		String_t* L_13 = ___src0;
		NullCheck(L_13);
		int32_t L_14 = String_get_Length_m2979997331(L_13, /*hidden argument*/NULL);
		if ((((int32_t)(*((int32_t*)L_12))) >= ((int32_t)L_14)))
		{
			goto IL_0057;
		}
	}
	{
		String_t* L_15 = ___src0;
		int32_t* L_16 = ___idx1;
		NullCheck(L_15);
		uint16_t L_17 = String_get_Chars_m3015341861(L_15, (*((int32_t*)L_16)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(XmlChar_t3591879093_il2cpp_TypeInfo_var);
		bool L_18 = XmlChar_IsNameChar_m1569088058(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		if (L_18)
		{
			goto IL_0032;
		}
	}

IL_0057:
	{
		String_t** L_19 = ___name2;
		String_t* L_20 = ___src0;
		int32_t L_21 = V_0;
		int32_t* L_22 = ___idx1;
		int32_t L_23 = V_0;
		NullCheck(L_20);
		String_t* L_24 = String_Substring_m675079568(L_20, L_21, ((int32_t)((int32_t)(*((int32_t*)L_22))-(int32_t)L_23)), /*hidden argument*/NULL);
		*((Il2CppObject **)(L_19)) = (Il2CppObject *)L_24;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_19), (Il2CppObject *)L_24);
		goto IL_006f;
	}

IL_0069:
	{
		int32_t* L_25 = ___idx1;
		int32_t* L_26 = ___idx1;
		*((int32_t*)(L_25)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_26))+(int32_t)1));
	}

IL_006f:
	{
		int32_t* L_27 = ___idx1;
		String_t* L_28 = ___src0;
		NullCheck(L_28);
		int32_t L_29 = String_get_Length_m2979997331(L_28, /*hidden argument*/NULL);
		if ((((int32_t)(*((int32_t*)L_27))) >= ((int32_t)L_29)))
		{
			goto IL_008e;
		}
	}
	{
		String_t* L_30 = ___src0;
		int32_t* L_31 = ___idx1;
		NullCheck(L_30);
		uint16_t L_32 = String_get_Chars_m3015341861(L_30, (*((int32_t*)L_31)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(XmlChar_t3591879093_il2cpp_TypeInfo_var);
		bool L_33 = XmlChar_IsWhitespace_m1427848566(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		if (L_33)
		{
			goto IL_0069;
		}
	}

IL_008e:
	{
		int32_t* L_34 = ___idx1;
		String_t* L_35 = ___src0;
		NullCheck(L_35);
		int32_t L_36 = String_get_Length_m2979997331(L_35, /*hidden argument*/NULL);
		if ((((int32_t)(*((int32_t*)L_34))) == ((int32_t)L_36)))
		{
			goto IL_00aa;
		}
	}
	{
		String_t* L_37 = ___src0;
		int32_t* L_38 = ___idx1;
		NullCheck(L_37);
		uint16_t L_39 = String_get_Chars_m3015341861(L_37, (*((int32_t*)L_38)), /*hidden argument*/NULL);
		if ((((int32_t)L_39) == ((int32_t)((int32_t)61))))
		{
			goto IL_00bd;
		}
	}

IL_00aa:
	{
		String_t** L_40 = ___name2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_41 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral548677155, (*((String_t**)L_40)), /*hidden argument*/NULL);
		XmlException_t3490696160 * L_42 = XmlTextReader_NotWFError_m304583423(__this, L_41, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_42);
	}

IL_00bd:
	{
		int32_t* L_43 = ___idx1;
		int32_t* L_44 = ___idx1;
		*((int32_t*)(L_43)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_44))+(int32_t)1));
		goto IL_00ce;
	}

IL_00c8:
	{
		int32_t* L_45 = ___idx1;
		int32_t* L_46 = ___idx1;
		*((int32_t*)(L_45)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_46))+(int32_t)1));
	}

IL_00ce:
	{
		int32_t* L_47 = ___idx1;
		String_t* L_48 = ___src0;
		NullCheck(L_48);
		int32_t L_49 = String_get_Length_m2979997331(L_48, /*hidden argument*/NULL);
		if ((((int32_t)(*((int32_t*)L_47))) >= ((int32_t)L_49)))
		{
			goto IL_00ed;
		}
	}
	{
		String_t* L_50 = ___src0;
		int32_t* L_51 = ___idx1;
		NullCheck(L_50);
		uint16_t L_52 = String_get_Chars_m3015341861(L_50, (*((int32_t*)L_51)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(XmlChar_t3591879093_il2cpp_TypeInfo_var);
		bool L_53 = XmlChar_IsWhitespace_m1427848566(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		if (L_53)
		{
			goto IL_00c8;
		}
	}

IL_00ed:
	{
		int32_t* L_54 = ___idx1;
		String_t* L_55 = ___src0;
		NullCheck(L_55);
		int32_t L_56 = String_get_Length_m2979997331(L_55, /*hidden argument*/NULL);
		if ((((int32_t)(*((int32_t*)L_54))) == ((int32_t)L_56)))
		{
			goto IL_0118;
		}
	}
	{
		String_t* L_57 = ___src0;
		int32_t* L_58 = ___idx1;
		NullCheck(L_57);
		uint16_t L_59 = String_get_Chars_m3015341861(L_57, (*((int32_t*)L_58)), /*hidden argument*/NULL);
		if ((((int32_t)L_59) == ((int32_t)((int32_t)34))))
		{
			goto IL_0124;
		}
	}
	{
		String_t* L_60 = ___src0;
		int32_t* L_61 = ___idx1;
		NullCheck(L_60);
		uint16_t L_62 = String_get_Chars_m3015341861(L_60, (*((int32_t*)L_61)), /*hidden argument*/NULL);
		if ((((int32_t)L_62) == ((int32_t)((int32_t)39))))
		{
			goto IL_0124;
		}
	}

IL_0118:
	{
		XmlException_t3490696160 * L_63 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral3083698786, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_63);
	}

IL_0124:
	{
		String_t* L_64 = ___src0;
		int32_t* L_65 = ___idx1;
		NullCheck(L_64);
		uint16_t L_66 = String_get_Chars_m3015341861(L_64, (*((int32_t*)L_65)), /*hidden argument*/NULL);
		V_1 = L_66;
		int32_t* L_67 = ___idx1;
		int32_t* L_68 = ___idx1;
		*((int32_t*)(L_67)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_68))+(int32_t)1));
		int32_t* L_69 = ___idx1;
		V_0 = (*((int32_t*)L_69));
		goto IL_0141;
	}

IL_013b:
	{
		int32_t* L_70 = ___idx1;
		int32_t* L_71 = ___idx1;
		*((int32_t*)(L_70)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_71))+(int32_t)1));
	}

IL_0141:
	{
		int32_t* L_72 = ___idx1;
		String_t* L_73 = ___src0;
		NullCheck(L_73);
		int32_t L_74 = String_get_Length_m2979997331(L_73, /*hidden argument*/NULL);
		if ((((int32_t)(*((int32_t*)L_72))) >= ((int32_t)L_74)))
		{
			goto IL_015c;
		}
	}
	{
		String_t* L_75 = ___src0;
		int32_t* L_76 = ___idx1;
		NullCheck(L_75);
		uint16_t L_77 = String_get_Chars_m3015341861(L_75, (*((int32_t*)L_76)), /*hidden argument*/NULL);
		uint16_t L_78 = V_1;
		if ((!(((uint32_t)L_77) == ((uint32_t)L_78))))
		{
			goto IL_013b;
		}
	}

IL_015c:
	{
		int32_t* L_79 = ___idx1;
		int32_t* L_80 = ___idx1;
		*((int32_t*)(L_79)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_80))+(int32_t)1));
		String_t** L_81 = ___value3;
		String_t* L_82 = ___src0;
		int32_t L_83 = V_0;
		int32_t* L_84 = ___idx1;
		int32_t L_85 = V_0;
		NullCheck(L_82);
		String_t* L_86 = String_Substring_m675079568(L_82, L_83, ((int32_t)((int32_t)((int32_t)((int32_t)(*((int32_t*)L_84))-(int32_t)L_85))-(int32_t)1)), /*hidden argument*/NULL);
		*((Il2CppObject **)(L_81)) = (Il2CppObject *)L_86;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_81), (Il2CppObject *)L_86);
		return;
	}
}
// System.Void Mono.Xml2.XmlTextReader::SkipTextDeclaration()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* CultureInfo_t3603717042_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t3416858730_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3683049;
extern Il2CppCodeGenString* _stringLiteral2694565034;
extern Il2CppCodeGenString* _stringLiteral351608024;
extern Il2CppCodeGenString* _stringLiteral1445239994;
extern Il2CppCodeGenString* _stringLiteral4063049545;
extern Il2CppCodeGenString* _stringLiteral48563;
extern Il2CppCodeGenString* _stringLiteral1711222099;
extern Il2CppCodeGenString* _stringLiteral1234965229;
extern Il2CppCodeGenString* _stringLiteral1536623702;
extern Il2CppCodeGenString* _stringLiteral2015;
extern const uint32_t XmlTextReader_SkipTextDeclaration_m3278219726_MetadataUsageId;
extern "C"  void XmlTextReader_SkipTextDeclaration_m3278219726 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader_SkipTextDeclaration_m3278219726_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	CharU5BU5D_t3416858730* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		int32_t L_0 = XmlTextReader_PeekChar_m254641297(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)((int32_t)60))))
		{
			goto IL_000e;
		}
	}
	{
		return;
	}

IL_000e:
	{
		XmlTextReader_ReadChar_m3857716364(__this, /*hidden argument*/NULL);
		int32_t L_1 = XmlTextReader_PeekChar_m254641297(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)((int32_t)63))))
		{
			goto IL_002a;
		}
	}
	{
		__this->set_peekCharsIndex_30(0);
		return;
	}

IL_002a:
	{
		XmlTextReader_ReadChar_m3857716364(__this, /*hidden argument*/NULL);
		goto IL_004e;
	}

IL_0036:
	{
		int32_t L_2 = XmlTextReader_PeekChar_m254641297(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0047;
		}
	}
	{
		goto IL_005a;
	}

IL_0047:
	{
		XmlTextReader_ReadChar_m3857716364(__this, /*hidden argument*/NULL);
	}

IL_004e:
	{
		int32_t L_3 = __this->get_peekCharsIndex_30();
		if ((((int32_t)L_3) < ((int32_t)6)))
		{
			goto IL_0036;
		}
	}

IL_005a:
	{
		CharU5BU5D_t3416858730* L_4 = __this->get_peekChars_29();
		String_t* L_5 = String_CreateString_m3402832113(NULL, L_4, 2, 4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_5, _stringLiteral3683049, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_00b0;
		}
	}
	{
		CharU5BU5D_t3416858730* L_7 = __this->get_peekChars_29();
		String_t* L_8 = String_CreateString_m3402832113(NULL, L_7, 2, 4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3603717042_il2cpp_TypeInfo_var);
		CultureInfo_t3603717042 * L_9 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		String_t* L_10 = String_ToLower_m2140020155(L_8, L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_11 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_10, _stringLiteral3683049, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_00a8;
		}
	}
	{
		XmlException_t3490696160 * L_12 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral2694565034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12);
	}

IL_00a8:
	{
		__this->set_peekCharsIndex_30(0);
		return;
	}

IL_00b0:
	{
		XmlTextReader_SkipWhitespace_m1854637366(__this, /*hidden argument*/NULL);
		int32_t L_13 = XmlTextReader_PeekChar_m254641297(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_13) == ((uint32_t)((int32_t)118)))))
		{
			goto IL_0199;
		}
	}
	{
		XmlTextReader_Expect_m1011563495(__this, _stringLiteral351608024, /*hidden argument*/NULL);
		XmlTextReader_ExpectAfterWhitespace_m3276297213(__this, ((int32_t)61), /*hidden argument*/NULL);
		XmlTextReader_SkipWhitespace_m1854637366(__this, /*hidden argument*/NULL);
		int32_t L_14 = XmlTextReader_ReadChar_m3857716364(__this, /*hidden argument*/NULL);
		V_0 = L_14;
		V_1 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)3));
		V_2 = 0;
		int32_t L_15 = V_0;
		V_4 = L_15;
		int32_t L_16 = V_4;
		if ((((int32_t)L_16) == ((int32_t)((int32_t)34))))
		{
			goto IL_0108;
		}
	}
	{
		int32_t L_17 = V_4;
		if ((((int32_t)L_17) == ((int32_t)((int32_t)39))))
		{
			goto IL_0108;
		}
	}
	{
		goto IL_018d;
	}

IL_0108:
	{
		goto IL_016e;
	}

IL_010d:
	{
		int32_t L_18 = XmlTextReader_PeekChar_m254641297(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_18) == ((uint32_t)(-1)))))
		{
			goto IL_0125;
		}
	}
	{
		XmlException_t3490696160 * L_19 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral1445239994, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_19);
	}

IL_0125:
	{
		int32_t L_20 = V_2;
		if ((!(((uint32_t)L_20) == ((uint32_t)3))))
		{
			goto IL_0138;
		}
	}
	{
		XmlException_t3490696160 * L_21 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral4063049545, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}

IL_0138:
	{
		CharU5BU5D_t3416858730* L_22 = V_1;
		int32_t L_23 = V_2;
		int32_t L_24 = XmlTextReader_ReadChar_m3857716364(__this, /*hidden argument*/NULL);
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(L_23), (uint16_t)(((int32_t)((uint16_t)L_24))));
		int32_t L_25 = V_2;
		V_2 = ((int32_t)((int32_t)L_25+(int32_t)1));
		int32_t L_26 = V_2;
		if ((!(((uint32_t)L_26) == ((uint32_t)3))))
		{
			goto IL_016e;
		}
	}
	{
		CharU5BU5D_t3416858730* L_27 = V_1;
		String_t* L_28 = String_CreateString_m578950865(NULL, L_27, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_29 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_28, _stringLiteral48563, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_016e;
		}
	}
	{
		XmlException_t3490696160 * L_30 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral4063049545, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_30);
	}

IL_016e:
	{
		int32_t L_31 = XmlTextReader_PeekChar_m254641297(__this, /*hidden argument*/NULL);
		int32_t L_32 = V_0;
		if ((!(((uint32_t)L_31) == ((uint32_t)L_32))))
		{
			goto IL_010d;
		}
	}
	{
		XmlTextReader_ReadChar_m3857716364(__this, /*hidden argument*/NULL);
		XmlTextReader_SkipWhitespace_m1854637366(__this, /*hidden argument*/NULL);
		goto IL_0199;
	}

IL_018d:
	{
		XmlException_t3490696160 * L_33 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral1445239994, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_33);
	}

IL_0199:
	{
		int32_t L_34 = XmlTextReader_PeekChar_m254641297(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_34) == ((uint32_t)((int32_t)101)))))
		{
			goto IL_022e;
		}
	}
	{
		XmlTextReader_Expect_m1011563495(__this, _stringLiteral1711222099, /*hidden argument*/NULL);
		XmlTextReader_ExpectAfterWhitespace_m3276297213(__this, ((int32_t)61), /*hidden argument*/NULL);
		XmlTextReader_SkipWhitespace_m1854637366(__this, /*hidden argument*/NULL);
		int32_t L_35 = XmlTextReader_ReadChar_m3857716364(__this, /*hidden argument*/NULL);
		V_3 = L_35;
		int32_t L_36 = V_3;
		V_4 = L_36;
		int32_t L_37 = V_4;
		if ((((int32_t)L_37) == ((int32_t)((int32_t)34))))
		{
			goto IL_01e1;
		}
	}
	{
		int32_t L_38 = V_4;
		if ((((int32_t)L_38) == ((int32_t)((int32_t)39))))
		{
			goto IL_01e1;
		}
	}
	{
		goto IL_021d;
	}

IL_01e1:
	{
		goto IL_01fe;
	}

IL_01e6:
	{
		int32_t L_39 = XmlTextReader_ReadChar_m3857716364(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_39) == ((uint32_t)(-1)))))
		{
			goto IL_01fe;
		}
	}
	{
		XmlException_t3490696160 * L_40 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral1234965229, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_40);
	}

IL_01fe:
	{
		int32_t L_41 = XmlTextReader_PeekChar_m254641297(__this, /*hidden argument*/NULL);
		int32_t L_42 = V_3;
		if ((!(((uint32_t)L_41) == ((uint32_t)L_42))))
		{
			goto IL_01e6;
		}
	}
	{
		XmlTextReader_ReadChar_m3857716364(__this, /*hidden argument*/NULL);
		XmlTextReader_SkipWhitespace_m1854637366(__this, /*hidden argument*/NULL);
		goto IL_0229;
	}

IL_021d:
	{
		XmlException_t3490696160 * L_43 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral1234965229, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_43);
	}

IL_0229:
	{
		goto IL_0245;
	}

IL_022e:
	{
		int32_t L_44 = XmlTextReader_get_Conformance_m1190844346(__this, /*hidden argument*/NULL);
		if (L_44)
		{
			goto IL_0245;
		}
	}
	{
		XmlException_t3490696160 * L_45 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral1536623702, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_45);
	}

IL_0245:
	{
		XmlTextReader_Expect_m1011563495(__this, _stringLiteral2015, /*hidden argument*/NULL);
		int32_t L_46 = __this->get_peekCharsIndex_30();
		__this->set_curNodePeekIndex_32(L_46);
		return;
	}
}
// System.Void Mono.Xml2.XmlTextReader::ReadDeclaration()
extern Il2CppCodeGenString* _stringLiteral1440;
extern Il2CppCodeGenString* _stringLiteral1982971790;
extern Il2CppCodeGenString* _stringLiteral2546877522;
extern Il2CppCodeGenString* _stringLiteral3999782502;
extern const uint32_t XmlTextReader_ReadDeclaration_m3359070020_MetadataUsageId;
extern "C"  void XmlTextReader_ReadDeclaration_m3359070020 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader_ReadDeclaration_m3359070020_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = XmlTextReader_PeekChar_m254641297(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		V_1 = L_1;
		int32_t L_2 = V_1;
		if ((((int32_t)L_2) == ((int32_t)((int32_t)45))))
		{
			goto IL_0026;
		}
	}
	{
		int32_t L_3 = V_1;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)68))))
		{
			goto IL_0059;
		}
	}
	{
		int32_t L_4 = V_1;
		if ((((int32_t)L_4) == ((int32_t)((int32_t)91))))
		{
			goto IL_003c;
		}
	}
	{
		goto IL_006f;
	}

IL_0026:
	{
		XmlTextReader_Expect_m1011563495(__this, _stringLiteral1440, /*hidden argument*/NULL);
		XmlTextReader_ReadComment_m1765947241(__this, /*hidden argument*/NULL);
		goto IL_007b;
	}

IL_003c:
	{
		XmlTextReader_ReadChar_m3857716364(__this, /*hidden argument*/NULL);
		XmlTextReader_Expect_m1011563495(__this, _stringLiteral1982971790, /*hidden argument*/NULL);
		XmlTextReader_ReadCDATA_m2376290903(__this, /*hidden argument*/NULL);
		goto IL_007b;
	}

IL_0059:
	{
		XmlTextReader_Expect_m1011563495(__this, _stringLiteral2546877522, /*hidden argument*/NULL);
		XmlTextReader_ReadDoctypeDecl_m1334696326(__this, /*hidden argument*/NULL);
		goto IL_007b;
	}

IL_006f:
	{
		XmlException_t3490696160 * L_5 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral3999782502, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_007b:
	{
		return;
	}
}
// System.Void Mono.Xml2.XmlTextReader::ReadComment()
extern Il2CppClass* XmlChar_t3591879093_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral555529373;
extern Il2CppCodeGenString* _stringLiteral178728223;
extern const uint32_t XmlTextReader_ReadComment_m1765947241_MetadataUsageId;
extern "C"  void XmlTextReader_ReadComment_m1765947241 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader_ReadComment_m1765947241_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_currentState_40();
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		__this->set_currentState_40(((int32_t)17));
	}

IL_0013:
	{
		__this->set_preserveCurrentTag_33((bool)0);
		XmlTextReader_ClearValueBuffer_m495962918(__this, /*hidden argument*/NULL);
		goto IL_008d;
	}

IL_0025:
	{
		int32_t L_1 = V_0;
		XmlTextReader_Advance_m673767315(__this, L_1, /*hidden argument*/NULL);
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)((int32_t)45)))))
		{
			goto IL_006f;
		}
	}
	{
		int32_t L_3 = XmlTextReader_PeekChar_m254641297(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)((int32_t)45)))))
		{
			goto IL_006f;
		}
	}
	{
		XmlTextReader_Advance_m673767315(__this, ((int32_t)45), /*hidden argument*/NULL);
		int32_t L_4 = XmlTextReader_PeekChar_m254641297(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_4) == ((int32_t)((int32_t)62))))
		{
			goto IL_0062;
		}
	}
	{
		XmlException_t3490696160 * L_5 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral555529373, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0062:
	{
		XmlTextReader_Advance_m673767315(__this, ((int32_t)62), /*hidden argument*/NULL);
		goto IL_009b;
	}

IL_006f:
	{
		int32_t L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(XmlChar_t3591879093_il2cpp_TypeInfo_var);
		bool L_7 = XmlChar_IsInvalid_m3338941186(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0086;
		}
	}
	{
		XmlException_t3490696160 * L_8 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral178728223, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_0086:
	{
		int32_t L_9 = V_0;
		XmlTextReader_AppendValueChar_m3744790334(__this, L_9, /*hidden argument*/NULL);
	}

IL_008d:
	{
		int32_t L_10 = XmlTextReader_PeekChar_m254641297(__this, /*hidden argument*/NULL);
		int32_t L_11 = L_10;
		V_0 = L_11;
		if ((!(((uint32_t)L_11) == ((uint32_t)(-1)))))
		{
			goto IL_0025;
		}
	}

IL_009b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_13 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_14 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		XmlTextReader_SetProperties_m3034003792(__this, 8, L_12, L_13, L_14, (bool)0, (String_t*)NULL, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml2.XmlTextReader::ReadCDATA()
extern Il2CppClass* XmlChar_t3591879093_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral329900736;
extern Il2CppCodeGenString* _stringLiteral241322499;
extern const uint32_t XmlTextReader_ReadCDATA_m2376290903_MetadataUsageId;
extern "C"  void XmlTextReader_ReadCDATA_m2376290903 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader_ReadCDATA_m2376290903_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	{
		int32_t L_0 = __this->get_currentState_40();
		if ((((int32_t)L_0) == ((int32_t)1)))
		{
			goto IL_0018;
		}
	}
	{
		XmlException_t3490696160 * L_1 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral329900736, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		__this->set_preserveCurrentTag_33((bool)0);
		XmlTextReader_ClearValueBuffer_m495962918(__this, /*hidden argument*/NULL);
		V_0 = (bool)0;
		V_1 = 0;
		goto IL_00ea;
	}

IL_002e:
	{
		bool L_2 = V_0;
		if (L_2)
		{
			goto IL_003b;
		}
	}
	{
		int32_t L_3 = XmlTextReader_ReadChar_m3857716364(__this, /*hidden argument*/NULL);
		V_1 = L_3;
	}

IL_003b:
	{
		V_0 = (bool)0;
		int32_t L_4 = V_1;
		if ((!(((uint32_t)L_4) == ((uint32_t)((int32_t)93)))))
		{
			goto IL_0074;
		}
	}
	{
		int32_t L_5 = XmlTextReader_PeekChar_m254641297(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_5) == ((uint32_t)((int32_t)93)))))
		{
			goto IL_0074;
		}
	}
	{
		int32_t L_6 = XmlTextReader_ReadChar_m3857716364(__this, /*hidden argument*/NULL);
		V_1 = L_6;
		int32_t L_7 = XmlTextReader_PeekChar_m254641297(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_7) == ((uint32_t)((int32_t)62)))))
		{
			goto IL_0072;
		}
	}
	{
		XmlTextReader_ReadChar_m3857716364(__this, /*hidden argument*/NULL);
		goto IL_00f6;
	}

IL_0072:
	{
		V_0 = (bool)1;
	}

IL_0074:
	{
		bool L_8 = __this->get_normalization_47();
		if (!L_8)
		{
			goto IL_00a3;
		}
	}
	{
		int32_t L_9 = V_1;
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)13)))))
		{
			goto IL_00a3;
		}
	}
	{
		int32_t L_10 = XmlTextReader_PeekChar_m254641297(__this, /*hidden argument*/NULL);
		V_1 = L_10;
		int32_t L_11 = V_1;
		if ((((int32_t)L_11) == ((int32_t)((int32_t)10))))
		{
			goto IL_009e;
		}
	}
	{
		XmlTextReader_AppendValueChar_m3744790334(__this, ((int32_t)10), /*hidden argument*/NULL);
	}

IL_009e:
	{
		goto IL_00ea;
	}

IL_00a3:
	{
		bool L_12 = XmlTextReader_get_CharacterChecking_m3210588674(__this, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_00c5;
		}
	}
	{
		int32_t L_13 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(XmlChar_t3591879093_il2cpp_TypeInfo_var);
		bool L_14 = XmlChar_IsInvalid_m3338941186(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00c5;
		}
	}
	{
		XmlException_t3490696160 * L_15 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral241322499, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_15);
	}

IL_00c5:
	{
		int32_t L_16 = V_1;
		if ((((int32_t)L_16) >= ((int32_t)((int32_t)65535))))
		{
			goto IL_00e3;
		}
	}
	{
		StringBuilder_t3822575854 * L_17 = __this->get_valueBuffer_27();
		int32_t L_18 = V_1;
		NullCheck(L_17);
		StringBuilder_Append_m2143093878(L_17, (((int32_t)((uint16_t)L_18))), /*hidden argument*/NULL);
		goto IL_00ea;
	}

IL_00e3:
	{
		int32_t L_19 = V_1;
		XmlTextReader_AppendSurrogatePairValueChar_m704355490(__this, L_19, /*hidden argument*/NULL);
	}

IL_00ea:
	{
		int32_t L_20 = XmlTextReader_PeekChar_m254641297(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_20) == ((uint32_t)(-1)))))
		{
			goto IL_002e;
		}
	}

IL_00f6:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_22 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_23 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		XmlTextReader_SetProperties_m3034003792(__this, 4, L_21, L_22, L_23, (bool)0, (String_t*)NULL, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml2.XmlTextReader::ReadDoctypeDecl()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3106665876;
extern Il2CppCodeGenString* _stringLiteral2407573267;
extern Il2CppCodeGenString* _stringLiteral3100517695;
extern Il2CppCodeGenString* _stringLiteral2370872937;
extern Il2CppCodeGenString* _stringLiteral2460968495;
extern const uint32_t XmlTextReader_ReadDoctypeDecl_m1334696326_MetadataUsageId;
extern "C"  void XmlTextReader_ReadDoctypeDecl_m1334696326 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader_ReadDoctypeDecl_m1334696326_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	{
		bool L_0 = __this->get_prohibitDtd_49();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		XmlException_t3490696160 * L_1 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral3106665876, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		int32_t L_2 = __this->get_currentState_40();
		V_5 = L_2;
		int32_t L_3 = V_5;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_003e;
		}
	}
	{
		int32_t L_4 = V_5;
		if ((((int32_t)L_4) == ((int32_t)((int32_t)10))))
		{
			goto IL_003e;
		}
	}
	{
		int32_t L_5 = V_5;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)15))))
		{
			goto IL_003e;
		}
	}
	{
		goto IL_004a;
	}

IL_003e:
	{
		XmlException_t3490696160 * L_6 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral2407573267, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_004a:
	{
		__this->set_currentState_40(((int32_t)10));
		V_0 = (String_t*)NULL;
		V_1 = (String_t*)NULL;
		V_2 = (String_t*)NULL;
		V_3 = 0;
		V_4 = 0;
		XmlTextReader_SkipWhitespace_m1854637366(__this, /*hidden argument*/NULL);
		String_t* L_7 = XmlTextReader_ReadName_m2874594848(__this, /*hidden argument*/NULL);
		V_0 = L_7;
		XmlTextReader_SkipWhitespace_m1854637366(__this, /*hidden argument*/NULL);
		int32_t L_8 = XmlTextReader_PeekChar_m254641297(__this, /*hidden argument*/NULL);
		V_6 = L_8;
		int32_t L_9 = V_6;
		if (((int32_t)((int32_t)L_9-(int32_t)((int32_t)80))) == 0)
		{
			goto IL_00a6;
		}
		if (((int32_t)((int32_t)L_9-(int32_t)((int32_t)80))) == 1)
		{
			goto IL_00d1;
		}
		if (((int32_t)((int32_t)L_9-(int32_t)((int32_t)80))) == 2)
		{
			goto IL_00d1;
		}
		if (((int32_t)((int32_t)L_9-(int32_t)((int32_t)80))) == 3)
		{
			goto IL_0099;
		}
	}
	{
		goto IL_00d1;
	}

IL_0099:
	{
		String_t* L_10 = XmlTextReader_ReadSystemLiteral_m287448164(__this, (bool)1, /*hidden argument*/NULL);
		V_2 = L_10;
		goto IL_00d1;
	}

IL_00a6:
	{
		String_t* L_11 = XmlTextReader_ReadPubidLiteral_m3836022092(__this, /*hidden argument*/NULL);
		V_1 = L_11;
		bool L_12 = XmlTextReader_SkipWhitespace_m1854637366(__this, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_00c4;
		}
	}
	{
		XmlException_t3490696160 * L_13 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral3100517695, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_13);
	}

IL_00c4:
	{
		String_t* L_14 = XmlTextReader_ReadSystemLiteral_m287448164(__this, (bool)0, /*hidden argument*/NULL);
		V_2 = L_14;
		goto IL_00d1;
	}

IL_00d1:
	{
		XmlTextReader_SkipWhitespace_m1854637366(__this, /*hidden argument*/NULL);
		int32_t L_15 = XmlTextReader_PeekChar_m254641297(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_15) == ((uint32_t)((int32_t)91)))))
		{
			goto IL_0118;
		}
	}
	{
		XmlTextReader_ReadChar_m3857716364(__this, /*hidden argument*/NULL);
		int32_t L_16 = VirtFuncInvoker0< int32_t >::Invoke(49 /* System.Int32 Mono.Xml2.XmlTextReader::get_LineNumber() */, __this);
		V_3 = L_16;
		int32_t L_17 = VirtFuncInvoker0< int32_t >::Invoke(50 /* System.Int32 Mono.Xml2.XmlTextReader::get_LinePosition() */, __this);
		V_4 = L_17;
		XmlTextReader_ClearValueBuffer_m495962918(__this, /*hidden argument*/NULL);
		XmlTextReader_ReadInternalSubset_m934179991(__this, /*hidden argument*/NULL);
		XmlParserContext_t3629084577 * L_18 = __this->get_parserContext_12();
		String_t* L_19 = XmlTextReader_CreateValueString_m2171097961(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		XmlParserContext_set_InternalSubset_m455417684(L_18, L_19, /*hidden argument*/NULL);
	}

IL_0118:
	{
		XmlTextReader_ExpectAfterWhitespace_m3276297213(__this, ((int32_t)62), /*hidden argument*/NULL);
		String_t* L_20 = V_0;
		String_t* L_21 = V_1;
		String_t* L_22 = V_2;
		XmlParserContext_t3629084577 * L_23 = __this->get_parserContext_12();
		NullCheck(L_23);
		String_t* L_24 = XmlParserContext_get_InternalSubset_m2001938359(L_23, /*hidden argument*/NULL);
		int32_t L_25 = V_3;
		int32_t L_26 = V_4;
		XmlTextReader_GenerateDTDObjectModel_m2497984484(__this, L_20, L_21, L_22, L_24, L_25, L_26, /*hidden argument*/NULL);
		String_t* L_27 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_28 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_29 = V_0;
		XmlParserContext_t3629084577 * L_30 = __this->get_parserContext_12();
		NullCheck(L_30);
		String_t* L_31 = XmlParserContext_get_InternalSubset_m2001938359(L_30, /*hidden argument*/NULL);
		XmlTextReader_SetProperties_m3034003792(__this, ((int32_t)10), L_27, L_28, L_29, (bool)0, L_31, (bool)1, /*hidden argument*/NULL);
		String_t* L_32 = V_1;
		if (!L_32)
		{
			goto IL_0166;
		}
	}
	{
		String_t* L_33 = V_1;
		XmlTextReader_AddAttributeWithValue_m1675559726(__this, _stringLiteral2370872937, L_33, /*hidden argument*/NULL);
	}

IL_0166:
	{
		String_t* L_34 = V_2;
		if (!L_34)
		{
			goto IL_0178;
		}
	}
	{
		String_t* L_35 = V_2;
		XmlTextReader_AddAttributeWithValue_m1675559726(__this, _stringLiteral2460968495, L_35, /*hidden argument*/NULL);
	}

IL_0178:
	{
		int32_t L_36 = (-1);
		V_6 = L_36;
		__this->set_currentAttributeValue_10(L_36);
		int32_t L_37 = V_6;
		__this->set_currentAttribute_9(L_37);
		return;
	}
}
// Mono.Xml.DTDObjectModel Mono.Xml2.XmlTextReader::GenerateDTDObjectModel(System.String,System.String,System.String,System.String)
extern "C"  DTDObjectModel_t709926554 * XmlTextReader_GenerateDTDObjectModel_m991330180 (XmlTextReader_t3066586409 * __this, String_t* ___name0, String_t* ___publicId1, String_t* ___systemId2, String_t* ___internalSubset3, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name0;
		String_t* L_1 = ___publicId1;
		String_t* L_2 = ___systemId2;
		String_t* L_3 = ___internalSubset3;
		DTDObjectModel_t709926554 * L_4 = XmlTextReader_GenerateDTDObjectModel_m2497984484(__this, L_0, L_1, L_2, L_3, 0, 0, /*hidden argument*/NULL);
		return L_4;
	}
}
// Mono.Xml.DTDObjectModel Mono.Xml2.XmlTextReader::GenerateDTDObjectModel(System.String,System.String,System.String,System.String,System.Int32,System.Int32)
extern Il2CppClass* DTDObjectModel_t709926554_il2cpp_TypeInfo_var;
extern Il2CppClass* DTDReader_t4257441119_il2cpp_TypeInfo_var;
extern const uint32_t XmlTextReader_GenerateDTDObjectModel_m2497984484_MetadataUsageId;
extern "C"  DTDObjectModel_t709926554 * XmlTextReader_GenerateDTDObjectModel_m2497984484 (XmlTextReader_t3066586409 * __this, String_t* ___name0, String_t* ___publicId1, String_t* ___systemId2, String_t* ___internalSubset3, int32_t ___intSubsetStartLine4, int32_t ___intSubsetStartColumn5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader_GenerateDTDObjectModel_m2497984484_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DTDReader_t4257441119 * V_0 = NULL;
	{
		XmlParserContext_t3629084577 * L_0 = __this->get_parserContext_12();
		XmlNameTable_t3232213908 * L_1 = VirtFuncInvoker0< XmlNameTable_t3232213908 * >::Invoke(17 /* System.Xml.XmlNameTable Mono.Xml2.XmlTextReader::get_NameTable() */, __this);
		DTDObjectModel_t709926554 * L_2 = (DTDObjectModel_t709926554 *)il2cpp_codegen_object_new(DTDObjectModel_t709926554_il2cpp_TypeInfo_var);
		DTDObjectModel__ctor_m3320301063(L_2, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		XmlParserContext_set_Dtd_m3742115955(L_0, L_2, /*hidden argument*/NULL);
		DTDObjectModel_t709926554 * L_3 = XmlTextReader_get_DTD_m3845091782(__this, /*hidden argument*/NULL);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String Mono.Xml2.XmlTextReader::get_BaseURI() */, __this);
		NullCheck(L_3);
		DTDObjectModel_set_BaseURI_m3388004765(L_3, L_4, /*hidden argument*/NULL);
		DTDObjectModel_t709926554 * L_5 = XmlTextReader_get_DTD_m3845091782(__this, /*hidden argument*/NULL);
		String_t* L_6 = ___name0;
		NullCheck(L_5);
		DTDObjectModel_set_Name_m1399978207(L_5, L_6, /*hidden argument*/NULL);
		DTDObjectModel_t709926554 * L_7 = XmlTextReader_get_DTD_m3845091782(__this, /*hidden argument*/NULL);
		String_t* L_8 = ___publicId1;
		NullCheck(L_7);
		DTDObjectModel_set_PublicId_m889242726(L_7, L_8, /*hidden argument*/NULL);
		DTDObjectModel_t709926554 * L_9 = XmlTextReader_get_DTD_m3845091782(__this, /*hidden argument*/NULL);
		String_t* L_10 = ___systemId2;
		NullCheck(L_9);
		DTDObjectModel_set_SystemId_m3085748576(L_9, L_10, /*hidden argument*/NULL);
		DTDObjectModel_t709926554 * L_11 = XmlTextReader_get_DTD_m3845091782(__this, /*hidden argument*/NULL);
		String_t* L_12 = ___internalSubset3;
		NullCheck(L_11);
		DTDObjectModel_set_InternalSubset_m2199752299(L_11, L_12, /*hidden argument*/NULL);
		DTDObjectModel_t709926554 * L_13 = XmlTextReader_get_DTD_m3845091782(__this, /*hidden argument*/NULL);
		XmlResolver_t2502213349 * L_14 = __this->get_resolver_46();
		NullCheck(L_13);
		DTDObjectModel_set_XmlResolver_m193476416(L_13, L_14, /*hidden argument*/NULL);
		DTDObjectModel_t709926554 * L_15 = XmlTextReader_get_DTD_m3845091782(__this, /*hidden argument*/NULL);
		bool L_16 = __this->get_isStandalone_24();
		NullCheck(L_15);
		DTDObjectModel_set_IsStandalone_m3307440830(L_15, L_16, /*hidden argument*/NULL);
		DTDObjectModel_t709926554 * L_17 = XmlTextReader_get_DTD_m3845091782(__this, /*hidden argument*/NULL);
		int32_t L_18 = __this->get_line_34();
		NullCheck(L_17);
		DTDObjectModel_set_LineNumber_m4288668902(L_17, L_18, /*hidden argument*/NULL);
		DTDObjectModel_t709926554 * L_19 = XmlTextReader_get_DTD_m3845091782(__this, /*hidden argument*/NULL);
		int32_t L_20 = __this->get_column_35();
		NullCheck(L_19);
		DTDObjectModel_set_LinePosition_m3330991110(L_19, L_20, /*hidden argument*/NULL);
		DTDObjectModel_t709926554 * L_21 = XmlTextReader_get_DTD_m3845091782(__this, /*hidden argument*/NULL);
		int32_t L_22 = ___intSubsetStartLine4;
		int32_t L_23 = ___intSubsetStartColumn5;
		DTDReader_t4257441119 * L_24 = (DTDReader_t4257441119 *)il2cpp_codegen_object_new(DTDReader_t4257441119_il2cpp_TypeInfo_var);
		DTDReader__ctor_m239668310(L_24, L_21, L_22, L_23, /*hidden argument*/NULL);
		V_0 = L_24;
		DTDReader_t4257441119 * L_25 = V_0;
		bool L_26 = __this->get_normalization_47();
		NullCheck(L_25);
		DTDReader_set_Normalization_m4032164889(L_25, L_26, /*hidden argument*/NULL);
		DTDReader_t4257441119 * L_27 = V_0;
		NullCheck(L_27);
		DTDObjectModel_t709926554 * L_28 = DTDReader_GenerateDTDObjectModel_m693103910(L_27, /*hidden argument*/NULL);
		return L_28;
	}
}
// Mono.Xml2.XmlTextReader/DtdInputState Mono.Xml2.XmlTextReader::get_State()
extern "C"  int32_t XmlTextReader_get_State_m4205986576 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	{
		DtdInputStateStack_t1263389581 * L_0 = __this->get_stateStack_54();
		NullCheck(L_0);
		int32_t L_1 = DtdInputStateStack_Peek_m2361625023(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 Mono.Xml2.XmlTextReader::ReadValueChar()
extern "C"  int32_t XmlTextReader_ReadValueChar_m579850003 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = XmlTextReader_ReadChar_m3857716364(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		XmlTextReader_AppendValueChar_m3744790334(__this, L_1, /*hidden argument*/NULL);
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Void Mono.Xml2.XmlTextReader::ExpectAndAppend(System.String)
extern "C"  void XmlTextReader_ExpectAndAppend_m2206132810 (XmlTextReader_t3066586409 * __this, String_t* ___s0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___s0;
		XmlTextReader_Expect_m1011563495(__this, L_0, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_1 = __this->get_valueBuffer_27();
		String_t* L_2 = ___s0;
		NullCheck(L_1);
		StringBuilder_Append_m3898090075(L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mono.Xml2.XmlTextReader::ReadInternalSubset()
extern Il2CppClass* Char_t2778706699_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2144508472;
extern Il2CppCodeGenString* _stringLiteral66085667;
extern Il2CppCodeGenString* _stringLiteral2575290;
extern Il2CppCodeGenString* _stringLiteral3684380182;
extern Il2CppCodeGenString* _stringLiteral2484761374;
extern Il2CppCodeGenString* _stringLiteral3860766736;
extern Il2CppCodeGenString* _stringLiteral45;
extern Il2CppCodeGenString* _stringLiteral3175819851;
extern Il2CppCodeGenString* _stringLiteral3973159514;
extern Il2CppCodeGenString* _stringLiteral62;
extern Il2CppCodeGenString* _stringLiteral3434247212;
extern const uint32_t XmlTextReader_ReadInternalSubset_m934179991_MetadataUsageId;
extern "C"  void XmlTextReader_ReadInternalSubset_m934179991 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader_ReadInternalSubset_m934179991_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	{
		V_0 = (bool)1;
		goto IL_03e5;
	}

IL_0007:
	{
		int32_t L_0 = XmlTextReader_ReadValueChar_m579850003(__this, /*hidden argument*/NULL);
		V_2 = L_0;
		int32_t L_1 = V_2;
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)34))) == 0)
		{
			goto IL_027f;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)34))) == 1)
		{
			goto IL_002f;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)34))) == 2)
		{
			goto IL_002f;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)34))) == 3)
		{
			goto IL_0397;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)34))) == 4)
		{
			goto IL_002f;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)34))) == 5)
		{
			goto IL_0238;
		}
	}

IL_002f:
	{
		int32_t L_2 = V_2;
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)60))) == 0)
		{
			goto IL_00ce;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)60))) == 1)
		{
			goto IL_0048;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)60))) == 2)
		{
			goto IL_02c7;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)60))) == 3)
		{
			goto IL_0331;
		}
	}

IL_0048:
	{
		int32_t L_3 = V_2;
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_00c2;
		}
	}
	{
		int32_t L_4 = V_2;
		if ((((int32_t)L_4) == ((int32_t)((int32_t)45))))
		{
			goto IL_035b;
		}
	}
	{
		int32_t L_5 = V_2;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)93))))
		{
			goto IL_0064;
		}
	}
	{
		goto IL_03e5;
	}

IL_0064:
	{
		int32_t L_6 = XmlTextReader_get_State_m4205986576(__this, /*hidden argument*/NULL);
		V_3 = L_6;
		int32_t L_7 = V_3;
		if (((int32_t)((int32_t)L_7-(int32_t)7)) == 0)
		{
			goto IL_00ac;
		}
		if (((int32_t)((int32_t)L_7-(int32_t)7)) == 1)
		{
			goto IL_00ac;
		}
		if (((int32_t)((int32_t)L_7-(int32_t)7)) == 2)
		{
			goto IL_00ac;
		}
	}
	{
		int32_t L_8 = V_3;
		if ((((int32_t)L_8) == ((int32_t)1)))
		{
			goto IL_008b;
		}
	}
	{
		goto IL_00b1;
	}

IL_008b:
	{
		StringBuilder_t3822575854 * L_9 = __this->get_valueBuffer_27();
		StringBuilder_t3822575854 * L_10 = __this->get_valueBuffer_27();
		NullCheck(L_10);
		int32_t L_11 = StringBuilder_get_Length_m2443133099(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		StringBuilder_Remove_m970775893(L_9, ((int32_t)((int32_t)L_11-(int32_t)1)), 1, /*hidden argument*/NULL);
		V_0 = (bool)0;
		goto IL_00bd;
	}

IL_00ac:
	{
		goto IL_03e5;
	}

IL_00b1:
	{
		XmlException_t3490696160 * L_12 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral2144508472, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12);
	}

IL_00bd:
	{
		goto IL_03e5;
	}

IL_00c2:
	{
		XmlException_t3490696160 * L_13 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral2144508472, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_13);
	}

IL_00ce:
	{
		int32_t L_14 = XmlTextReader_get_State_m4205986576(__this, /*hidden argument*/NULL);
		V_3 = L_14;
		int32_t L_15 = V_3;
		if (((int32_t)((int32_t)L_15-(int32_t)7)) == 0)
		{
			goto IL_00ee;
		}
		if (((int32_t)((int32_t)L_15-(int32_t)7)) == 1)
		{
			goto IL_00ee;
		}
		if (((int32_t)((int32_t)L_15-(int32_t)7)) == 2)
		{
			goto IL_00ee;
		}
	}
	{
		goto IL_00f3;
	}

IL_00ee:
	{
		goto IL_03e5;
	}

IL_00f3:
	{
		int32_t L_16 = XmlTextReader_ReadValueChar_m579850003(__this, /*hidden argument*/NULL);
		V_1 = L_16;
		int32_t L_17 = V_1;
		V_4 = L_17;
		int32_t L_18 = V_4;
		if ((((int32_t)L_18) == ((int32_t)((int32_t)33))))
		{
			goto IL_0125;
		}
	}
	{
		int32_t L_19 = V_4;
		if ((((int32_t)L_19) == ((int32_t)((int32_t)63))))
		{
			goto IL_0114;
		}
	}
	{
		goto IL_021b;
	}

IL_0114:
	{
		DtdInputStateStack_t1263389581 * L_20 = __this->get_stateStack_54();
		NullCheck(L_20);
		DtdInputStateStack_Push_m1703488689(L_20, 6, /*hidden argument*/NULL);
		goto IL_0233;
	}

IL_0125:
	{
		int32_t L_21 = XmlTextReader_ReadValueChar_m579850003(__this, /*hidden argument*/NULL);
		V_5 = L_21;
		int32_t L_22 = V_5;
		if ((((int32_t)L_22) == ((int32_t)((int32_t)45))))
		{
			goto IL_01fa;
		}
	}
	{
		int32_t L_23 = V_5;
		if ((((int32_t)L_23) == ((int32_t)((int32_t)65))))
		{
			goto IL_01c2;
		}
	}
	{
		int32_t L_24 = V_5;
		if ((((int32_t)L_24) == ((int32_t)((int32_t)69))))
		{
			goto IL_0156;
		}
	}
	{
		int32_t L_25 = V_5;
		if ((((int32_t)L_25) == ((int32_t)((int32_t)78))))
		{
			goto IL_01de;
		}
	}
	{
		goto IL_0216;
	}

IL_0156:
	{
		int32_t L_26 = XmlTextReader_ReadValueChar_m579850003(__this, /*hidden argument*/NULL);
		V_6 = L_26;
		int32_t L_27 = V_6;
		if (((int32_t)((int32_t)L_27-(int32_t)((int32_t)76))) == 0)
		{
			goto IL_0179;
		}
		if (((int32_t)((int32_t)L_27-(int32_t)((int32_t)76))) == 1)
		{
			goto IL_01b1;
		}
		if (((int32_t)((int32_t)L_27-(int32_t)((int32_t)76))) == 2)
		{
			goto IL_0195;
		}
	}
	{
		goto IL_01b1;
	}

IL_0179:
	{
		XmlTextReader_ExpectAndAppend_m2206132810(__this, _stringLiteral66085667, /*hidden argument*/NULL);
		DtdInputStateStack_t1263389581 * L_28 = __this->get_stateStack_54();
		NullCheck(L_28);
		DtdInputStateStack_Push_m1703488689(L_28, 2, /*hidden argument*/NULL);
		goto IL_01bd;
	}

IL_0195:
	{
		XmlTextReader_ExpectAndAppend_m2206132810(__this, _stringLiteral2575290, /*hidden argument*/NULL);
		DtdInputStateStack_t1263389581 * L_29 = __this->get_stateStack_54();
		NullCheck(L_29);
		DtdInputStateStack_Push_m1703488689(L_29, 4, /*hidden argument*/NULL);
		goto IL_01bd;
	}

IL_01b1:
	{
		XmlException_t3490696160 * L_30 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral3684380182, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_30);
	}

IL_01bd:
	{
		goto IL_0216;
	}

IL_01c2:
	{
		XmlTextReader_ExpectAndAppend_m2206132810(__this, _stringLiteral2484761374, /*hidden argument*/NULL);
		DtdInputStateStack_t1263389581 * L_31 = __this->get_stateStack_54();
		NullCheck(L_31);
		DtdInputStateStack_Push_m1703488689(L_31, 3, /*hidden argument*/NULL);
		goto IL_0216;
	}

IL_01de:
	{
		XmlTextReader_ExpectAndAppend_m2206132810(__this, _stringLiteral3860766736, /*hidden argument*/NULL);
		DtdInputStateStack_t1263389581 * L_32 = __this->get_stateStack_54();
		NullCheck(L_32);
		DtdInputStateStack_Push_m1703488689(L_32, 5, /*hidden argument*/NULL);
		goto IL_0216;
	}

IL_01fa:
	{
		XmlTextReader_ExpectAndAppend_m2206132810(__this, _stringLiteral45, /*hidden argument*/NULL);
		DtdInputStateStack_t1263389581 * L_33 = __this->get_stateStack_54();
		NullCheck(L_33);
		DtdInputStateStack_Push_m1703488689(L_33, 7, /*hidden argument*/NULL);
		goto IL_0216;
	}

IL_0216:
	{
		goto IL_0233;
	}

IL_021b:
	{
		int32_t L_34 = V_1;
		uint16_t L_35 = ((uint16_t)(((int32_t)((uint16_t)L_34))));
		Il2CppObject * L_36 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_35);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_37 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral3175819851, L_36, /*hidden argument*/NULL);
		XmlException_t3490696160 * L_38 = XmlTextReader_NotWFError_m304583423(__this, L_37, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_38);
	}

IL_0233:
	{
		goto IL_03e5;
	}

IL_0238:
	{
		int32_t L_39 = XmlTextReader_get_State_m4205986576(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_39) == ((uint32_t)8))))
		{
			goto IL_0255;
		}
	}
	{
		DtdInputStateStack_t1263389581 * L_40 = __this->get_stateStack_54();
		NullCheck(L_40);
		DtdInputStateStack_Pop_m3263076495(L_40, /*hidden argument*/NULL);
		goto IL_027a;
	}

IL_0255:
	{
		int32_t L_41 = XmlTextReader_get_State_m4205986576(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_41) == ((int32_t)((int32_t)9))))
		{
			goto IL_027a;
		}
	}
	{
		int32_t L_42 = XmlTextReader_get_State_m4205986576(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_42) == ((int32_t)7)))
		{
			goto IL_027a;
		}
	}
	{
		DtdInputStateStack_t1263389581 * L_43 = __this->get_stateStack_54();
		NullCheck(L_43);
		DtdInputStateStack_Push_m1703488689(L_43, 8, /*hidden argument*/NULL);
	}

IL_027a:
	{
		goto IL_03e5;
	}

IL_027f:
	{
		int32_t L_44 = XmlTextReader_get_State_m4205986576(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_44) == ((uint32_t)((int32_t)9)))))
		{
			goto IL_029d;
		}
	}
	{
		DtdInputStateStack_t1263389581 * L_45 = __this->get_stateStack_54();
		NullCheck(L_45);
		DtdInputStateStack_Pop_m3263076495(L_45, /*hidden argument*/NULL);
		goto IL_02c2;
	}

IL_029d:
	{
		int32_t L_46 = XmlTextReader_get_State_m4205986576(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_46) == ((int32_t)8)))
		{
			goto IL_02c2;
		}
	}
	{
		int32_t L_47 = XmlTextReader_get_State_m4205986576(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_47) == ((int32_t)7)))
		{
			goto IL_02c2;
		}
	}
	{
		DtdInputStateStack_t1263389581 * L_48 = __this->get_stateStack_54();
		NullCheck(L_48);
		DtdInputStateStack_Push_m1703488689(L_48, ((int32_t)9), /*hidden argument*/NULL);
	}

IL_02c2:
	{
		goto IL_03e5;
	}

IL_02c7:
	{
		int32_t L_49 = XmlTextReader_get_State_m4205986576(__this, /*hidden argument*/NULL);
		V_3 = L_49;
		int32_t L_50 = V_3;
		if (((int32_t)((int32_t)L_50-(int32_t)2)) == 0)
		{
			goto IL_02fb;
		}
		if (((int32_t)((int32_t)L_50-(int32_t)2)) == 1)
		{
			goto IL_0300;
		}
		if (((int32_t)((int32_t)L_50-(int32_t)2)) == 2)
		{
			goto IL_0305;
		}
		if (((int32_t)((int32_t)L_50-(int32_t)2)) == 3)
		{
			goto IL_030a;
		}
		if (((int32_t)((int32_t)L_50-(int32_t)2)) == 4)
		{
			goto IL_0320;
		}
		if (((int32_t)((int32_t)L_50-(int32_t)2)) == 5)
		{
			goto IL_031b;
		}
		if (((int32_t)((int32_t)L_50-(int32_t)2)) == 6)
		{
			goto IL_031b;
		}
		if (((int32_t)((int32_t)L_50-(int32_t)2)) == 7)
		{
			goto IL_031b;
		}
	}
	{
		goto IL_0320;
	}

IL_02fb:
	{
		goto IL_030a;
	}

IL_0300:
	{
		goto IL_030a;
	}

IL_0305:
	{
		goto IL_030a;
	}

IL_030a:
	{
		DtdInputStateStack_t1263389581 * L_51 = __this->get_stateStack_54();
		NullCheck(L_51);
		DtdInputStateStack_Pop_m3263076495(L_51, /*hidden argument*/NULL);
		goto IL_032c;
	}

IL_031b:
	{
		goto IL_03e5;
	}

IL_0320:
	{
		XmlException_t3490696160 * L_52 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral3973159514, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_52);
	}

IL_032c:
	{
		goto IL_03e5;
	}

IL_0331:
	{
		int32_t L_53 = XmlTextReader_get_State_m4205986576(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_53) == ((uint32_t)6))))
		{
			goto IL_0356;
		}
	}
	{
		int32_t L_54 = XmlTextReader_ReadValueChar_m579850003(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_54) == ((uint32_t)((int32_t)62)))))
		{
			goto IL_0356;
		}
	}
	{
		DtdInputStateStack_t1263389581 * L_55 = __this->get_stateStack_54();
		NullCheck(L_55);
		DtdInputStateStack_Pop_m3263076495(L_55, /*hidden argument*/NULL);
	}

IL_0356:
	{
		goto IL_03e5;
	}

IL_035b:
	{
		int32_t L_56 = XmlTextReader_get_State_m4205986576(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_56) == ((uint32_t)7))))
		{
			goto IL_0392;
		}
	}
	{
		int32_t L_57 = XmlTextReader_PeekChar_m254641297(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_57) == ((uint32_t)((int32_t)45)))))
		{
			goto IL_0392;
		}
	}
	{
		XmlTextReader_ReadValueChar_m579850003(__this, /*hidden argument*/NULL);
		XmlTextReader_ExpectAndAppend_m2206132810(__this, _stringLiteral62, /*hidden argument*/NULL);
		DtdInputStateStack_t1263389581 * L_58 = __this->get_stateStack_54();
		NullCheck(L_58);
		DtdInputStateStack_Pop_m3263076495(L_58, /*hidden argument*/NULL);
	}

IL_0392:
	{
		goto IL_03e5;
	}

IL_0397:
	{
		int32_t L_59 = XmlTextReader_get_State_m4205986576(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_59) == ((int32_t)1)))
		{
			goto IL_03e0;
		}
	}
	{
		int32_t L_60 = XmlTextReader_get_State_m4205986576(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_60) == ((int32_t)4)))
		{
			goto IL_03e0;
		}
	}
	{
		int32_t L_61 = XmlTextReader_get_State_m4205986576(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_61) == ((int32_t)7)))
		{
			goto IL_03e0;
		}
	}
	{
		int32_t L_62 = XmlTextReader_get_State_m4205986576(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_62) == ((int32_t)((int32_t)9))))
		{
			goto IL_03e0;
		}
	}
	{
		int32_t L_63 = XmlTextReader_get_State_m4205986576(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_63) == ((int32_t)8)))
		{
			goto IL_03e0;
		}
	}
	{
		XmlException_t3490696160 * L_64 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral3434247212, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_64);
	}

IL_03e0:
	{
		goto IL_03e5;
	}

IL_03e5:
	{
		bool L_65 = V_0;
		if (L_65)
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.String Mono.Xml2.XmlTextReader::ReadSystemLiteral(System.Boolean)
extern Il2CppCodeGenString* _stringLiteral2460968495;
extern Il2CppCodeGenString* _stringLiteral3085822955;
extern Il2CppCodeGenString* _stringLiteral1283542072;
extern const uint32_t XmlTextReader_ReadSystemLiteral_m287448164_MetadataUsageId;
extern "C"  String_t* XmlTextReader_ReadSystemLiteral_m287448164 (XmlTextReader_t3066586409 * __this, bool ___expectSYSTEM0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader_ReadSystemLiteral_m287448164_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		bool L_0 = ___expectSYSTEM0;
		if (!L_0)
		{
			goto IL_002d;
		}
	}
	{
		XmlTextReader_Expect_m1011563495(__this, _stringLiteral2460968495, /*hidden argument*/NULL);
		bool L_1 = XmlTextReader_SkipWhitespace_m1854637366(__this, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0028;
		}
	}
	{
		XmlException_t3490696160 * L_2 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral3085822955, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0028:
	{
		goto IL_0034;
	}

IL_002d:
	{
		XmlTextReader_SkipWhitespace_m1854637366(__this, /*hidden argument*/NULL);
	}

IL_0034:
	{
		int32_t L_3 = XmlTextReader_ReadChar_m3857716364(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		V_1 = 0;
		XmlTextReader_ClearValueBuffer_m495962918(__this, /*hidden argument*/NULL);
		goto IL_0070;
	}

IL_0048:
	{
		int32_t L_4 = XmlTextReader_ReadChar_m3857716364(__this, /*hidden argument*/NULL);
		V_1 = L_4;
		int32_t L_5 = V_1;
		if ((((int32_t)L_5) >= ((int32_t)0)))
		{
			goto IL_0062;
		}
	}
	{
		XmlException_t3490696160 * L_6 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral1283542072, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_0062:
	{
		int32_t L_7 = V_1;
		int32_t L_8 = V_0;
		if ((((int32_t)L_7) == ((int32_t)L_8)))
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_9 = V_1;
		XmlTextReader_AppendValueChar_m3744790334(__this, L_9, /*hidden argument*/NULL);
	}

IL_0070:
	{
		int32_t L_10 = V_1;
		int32_t L_11 = V_0;
		if ((!(((uint32_t)L_10) == ((uint32_t)L_11))))
		{
			goto IL_0048;
		}
	}
	{
		String_t* L_12 = XmlTextReader_CreateValueString_m2171097961(__this, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.String Mono.Xml2.XmlTextReader::ReadPubidLiteral()
extern Il2CppClass* XmlChar_t3591879093_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t2778706699_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2370872937;
extern Il2CppCodeGenString* _stringLiteral2403337637;
extern Il2CppCodeGenString* _stringLiteral1283542072;
extern Il2CppCodeGenString* _stringLiteral1168509637;
extern const uint32_t XmlTextReader_ReadPubidLiteral_m3836022092_MetadataUsageId;
extern "C"  String_t* XmlTextReader_ReadPubidLiteral_m3836022092 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader_ReadPubidLiteral_m3836022092_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		XmlTextReader_Expect_m1011563495(__this, _stringLiteral2370872937, /*hidden argument*/NULL);
		bool L_0 = XmlTextReader_SkipWhitespace_m1854637366(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0022;
		}
	}
	{
		XmlException_t3490696160 * L_1 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral2403337637, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0022:
	{
		int32_t L_2 = XmlTextReader_ReadChar_m3857716364(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		V_1 = 0;
		XmlTextReader_ClearValueBuffer_m495962918(__this, /*hidden argument*/NULL);
		goto IL_0088;
	}

IL_0036:
	{
		int32_t L_3 = XmlTextReader_ReadChar_m3857716364(__this, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = V_1;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_0050;
		}
	}
	{
		XmlException_t3490696160 * L_5 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral1283542072, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0050:
	{
		int32_t L_6 = V_1;
		int32_t L_7 = V_0;
		if ((((int32_t)L_6) == ((int32_t)L_7)))
		{
			goto IL_007a;
		}
	}
	{
		int32_t L_8 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(XmlChar_t3591879093_il2cpp_TypeInfo_var);
		bool L_9 = XmlChar_IsPubidChar_m2010542809(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_007a;
		}
	}
	{
		int32_t L_10 = V_1;
		uint16_t L_11 = ((uint16_t)(((int32_t)((uint16_t)L_10))));
		Il2CppObject * L_12 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_11);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral1168509637, L_12, /*hidden argument*/NULL);
		XmlException_t3490696160 * L_14 = XmlTextReader_NotWFError_m304583423(__this, L_13, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_14);
	}

IL_007a:
	{
		int32_t L_15 = V_1;
		int32_t L_16 = V_0;
		if ((((int32_t)L_15) == ((int32_t)L_16)))
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_17 = V_1;
		XmlTextReader_AppendValueChar_m3744790334(__this, L_17, /*hidden argument*/NULL);
	}

IL_0088:
	{
		int32_t L_18 = V_1;
		int32_t L_19 = V_0;
		if ((!(((uint32_t)L_18) == ((uint32_t)L_19))))
		{
			goto IL_0036;
		}
	}
	{
		String_t* L_20 = XmlTextReader_CreateValueString_m2171097961(__this, /*hidden argument*/NULL);
		return L_20;
	}
}
// System.String Mono.Xml2.XmlTextReader::ReadName()
extern "C"  String_t* XmlTextReader_ReadName_m2874594848 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	{
		String_t* L_0 = XmlTextReader_ReadName_m2367194790(__this, (&V_0), (&V_1), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String Mono.Xml2.XmlTextReader::ReadName(System.String&,System.String&)
extern Il2CppClass* XmlChar_t3591879093_il2cpp_TypeInfo_var;
extern Il2CppClass* CultureInfo_t3603717042_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t2778706699_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1130036125;
extern const uint32_t XmlTextReader_ReadName_m2367194790_MetadataUsageId;
extern "C"  String_t* XmlTextReader_ReadName_m2367194790 (XmlTextReader_t3066586409 * __this, String_t** ___prefix0, String_t** ___localName1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader_ReadName_m2367194790_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	String_t* V_6 = NULL;
	{
		bool L_0 = __this->get_preserveCurrentTag_33();
		V_0 = L_0;
		__this->set_preserveCurrentTag_33((bool)1);
		int32_t L_1 = __this->get_peekCharsIndex_30();
		int32_t L_2 = __this->get_curNodePeekIndex_32();
		V_1 = ((int32_t)((int32_t)L_1-(int32_t)L_2));
		int32_t L_3 = XmlTextReader_PeekChar_m254641297(__this, /*hidden argument*/NULL);
		V_2 = L_3;
		int32_t L_4 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(XmlChar_t3591879093_il2cpp_TypeInfo_var);
		bool L_5 = XmlChar_IsFirstNameChar_m3587024924(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_005d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3603717042_il2cpp_TypeInfo_var);
		CultureInfo_t3603717042 * L_6 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_7 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)2));
		int32_t L_8 = V_2;
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, L_10);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_10);
		ObjectU5BU5D_t11523773* L_11 = L_7;
		int32_t L_12 = V_2;
		uint16_t L_13 = ((uint16_t)(((int32_t)((uint16_t)L_12))));
		Il2CppObject * L_14 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_13);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 1);
		ArrayElementTypeCheck (L_11, L_14);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_14);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Format_m3351777162(NULL /*static, unused*/, L_6, _stringLiteral1130036125, L_11, /*hidden argument*/NULL);
		XmlException_t3490696160 * L_16 = XmlTextReader_NotWFError_m304583423(__this, L_15, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
	}

IL_005d:
	{
		int32_t L_17 = V_2;
		XmlTextReader_Advance_m673767315(__this, L_17, /*hidden argument*/NULL);
		V_3 = 1;
		V_4 = (-1);
		goto IL_0097;
	}

IL_006e:
	{
		int32_t L_18 = V_2;
		XmlTextReader_Advance_m673767315(__this, L_18, /*hidden argument*/NULL);
		int32_t L_19 = V_2;
		if ((!(((uint32_t)L_19) == ((uint32_t)((int32_t)58)))))
		{
			goto IL_0093;
		}
	}
	{
		bool L_20 = __this->get_namespaces_44();
		if (!L_20)
		{
			goto IL_0093;
		}
	}
	{
		int32_t L_21 = V_4;
		if ((((int32_t)L_21) >= ((int32_t)0)))
		{
			goto IL_0093;
		}
	}
	{
		int32_t L_22 = V_3;
		V_4 = L_22;
	}

IL_0093:
	{
		int32_t L_23 = V_3;
		V_3 = ((int32_t)((int32_t)L_23+(int32_t)1));
	}

IL_0097:
	{
		int32_t L_24 = XmlTextReader_PeekChar_m254641297(__this, /*hidden argument*/NULL);
		int32_t L_25 = L_24;
		V_2 = L_25;
		IL2CPP_RUNTIME_CLASS_INIT(XmlChar_t3591879093_il2cpp_TypeInfo_var);
		bool L_26 = XmlChar_IsNameChar_m1569088058(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		if (L_26)
		{
			goto IL_006e;
		}
	}
	{
		int32_t L_27 = __this->get_curNodePeekIndex_32();
		int32_t L_28 = V_1;
		V_5 = ((int32_t)((int32_t)L_27+(int32_t)L_28));
		XmlNameTable_t3232213908 * L_29 = VirtFuncInvoker0< XmlNameTable_t3232213908 * >::Invoke(17 /* System.Xml.XmlNameTable Mono.Xml2.XmlTextReader::get_NameTable() */, __this);
		CharU5BU5D_t3416858730* L_30 = __this->get_peekChars_29();
		int32_t L_31 = V_5;
		int32_t L_32 = V_3;
		NullCheck(L_29);
		String_t* L_33 = VirtFuncInvoker3< String_t*, CharU5BU5D_t3416858730*, int32_t, int32_t >::Invoke(5 /* System.String System.Xml.XmlNameTable::Add(System.Char[],System.Int32,System.Int32) */, L_29, L_30, L_31, L_32);
		V_6 = L_33;
		int32_t L_34 = V_4;
		if ((((int32_t)L_34) <= ((int32_t)0)))
		{
			goto IL_010d;
		}
	}
	{
		String_t** L_35 = ___prefix0;
		XmlNameTable_t3232213908 * L_36 = VirtFuncInvoker0< XmlNameTable_t3232213908 * >::Invoke(17 /* System.Xml.XmlNameTable Mono.Xml2.XmlTextReader::get_NameTable() */, __this);
		CharU5BU5D_t3416858730* L_37 = __this->get_peekChars_29();
		int32_t L_38 = V_5;
		int32_t L_39 = V_4;
		NullCheck(L_36);
		String_t* L_40 = VirtFuncInvoker3< String_t*, CharU5BU5D_t3416858730*, int32_t, int32_t >::Invoke(5 /* System.String System.Xml.XmlNameTable::Add(System.Char[],System.Int32,System.Int32) */, L_36, L_37, L_38, L_39);
		*((Il2CppObject **)(L_35)) = (Il2CppObject *)L_40;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_35), (Il2CppObject *)L_40);
		String_t** L_41 = ___localName1;
		XmlNameTable_t3232213908 * L_42 = VirtFuncInvoker0< XmlNameTable_t3232213908 * >::Invoke(17 /* System.Xml.XmlNameTable Mono.Xml2.XmlTextReader::get_NameTable() */, __this);
		CharU5BU5D_t3416858730* L_43 = __this->get_peekChars_29();
		int32_t L_44 = V_5;
		int32_t L_45 = V_4;
		int32_t L_46 = V_3;
		int32_t L_47 = V_4;
		NullCheck(L_42);
		String_t* L_48 = VirtFuncInvoker3< String_t*, CharU5BU5D_t3416858730*, int32_t, int32_t >::Invoke(5 /* System.String System.Xml.XmlNameTable::Add(System.Char[],System.Int32,System.Int32) */, L_42, L_43, ((int32_t)((int32_t)((int32_t)((int32_t)L_44+(int32_t)L_45))+(int32_t)1)), ((int32_t)((int32_t)((int32_t)((int32_t)L_46-(int32_t)L_47))-(int32_t)1)));
		*((Il2CppObject **)(L_41)) = (Il2CppObject *)L_48;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_41), (Il2CppObject *)L_48);
		goto IL_0118;
	}

IL_010d:
	{
		String_t** L_49 = ___prefix0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_50 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		*((Il2CppObject **)(L_49)) = (Il2CppObject *)L_50;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_49), (Il2CppObject *)L_50);
		String_t** L_51 = ___localName1;
		String_t* L_52 = V_6;
		*((Il2CppObject **)(L_51)) = (Il2CppObject *)L_52;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_51), (Il2CppObject *)L_52);
	}

IL_0118:
	{
		bool L_53 = V_0;
		__this->set_preserveCurrentTag_33(L_53);
		String_t* L_54 = V_6;
		return L_54;
	}
}
// System.Void Mono.Xml2.XmlTextReader::Expect(System.Int32)
extern Il2CppClass* CultureInfo_t3603717042_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t2778706699_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1855766747;
extern Il2CppCodeGenString* _stringLiteral68828;
extern const uint32_t XmlTextReader_Expect_m2788845516_MetadataUsageId;
extern "C"  void XmlTextReader_Expect_m2788845516 (XmlTextReader_t3066586409 * __this, int32_t ___expected0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader_Expect_m2788845516_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	ObjectU5BU5D_t11523773* G_B3_1 = NULL;
	ObjectU5BU5D_t11523773* G_B3_2 = NULL;
	String_t* G_B3_3 = NULL;
	CultureInfo_t3603717042 * G_B3_4 = NULL;
	XmlTextReader_t3066586409 * G_B3_5 = NULL;
	int32_t G_B2_0 = 0;
	ObjectU5BU5D_t11523773* G_B2_1 = NULL;
	ObjectU5BU5D_t11523773* G_B2_2 = NULL;
	String_t* G_B2_3 = NULL;
	CultureInfo_t3603717042 * G_B2_4 = NULL;
	XmlTextReader_t3066586409 * G_B2_5 = NULL;
	Il2CppObject * G_B4_0 = NULL;
	int32_t G_B4_1 = 0;
	ObjectU5BU5D_t11523773* G_B4_2 = NULL;
	ObjectU5BU5D_t11523773* G_B4_3 = NULL;
	String_t* G_B4_4 = NULL;
	CultureInfo_t3603717042 * G_B4_5 = NULL;
	XmlTextReader_t3066586409 * G_B4_6 = NULL;
	{
		int32_t L_0 = XmlTextReader_ReadChar_m3857716364(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		int32_t L_2 = ___expected0;
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_0061;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3603717042_il2cpp_TypeInfo_var);
		CultureInfo_t3603717042 * L_3 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_4 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)4));
		int32_t L_5 = ___expected0;
		uint16_t L_6 = ((uint16_t)(((int32_t)((uint16_t)L_5))));
		Il2CppObject * L_7 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_7);
		ObjectU5BU5D_t11523773* L_8 = L_4;
		int32_t L_9 = ___expected0;
		int32_t L_10 = L_9;
		Il2CppObject * L_11 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t11523773* L_12 = L_8;
		int32_t L_13 = V_0;
		G_B2_0 = 2;
		G_B2_1 = L_12;
		G_B2_2 = L_12;
		G_B2_3 = _stringLiteral1855766747;
		G_B2_4 = L_3;
		G_B2_5 = __this;
		if ((((int32_t)L_13) >= ((int32_t)0)))
		{
			G_B3_0 = 2;
			G_B3_1 = L_12;
			G_B3_2 = L_12;
			G_B3_3 = _stringLiteral1855766747;
			G_B3_4 = L_3;
			G_B3_5 = __this;
			goto IL_0045;
		}
	}
	{
		G_B4_0 = ((Il2CppObject *)(_stringLiteral68828));
		G_B4_1 = G_B2_0;
		G_B4_2 = G_B2_1;
		G_B4_3 = G_B2_2;
		G_B4_4 = G_B2_3;
		G_B4_5 = G_B2_4;
		G_B4_6 = G_B2_5;
		goto IL_004c;
	}

IL_0045:
	{
		int32_t L_14 = V_0;
		uint16_t L_15 = ((uint16_t)(((int32_t)((uint16_t)L_14))));
		Il2CppObject * L_16 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_15);
		G_B4_0 = L_16;
		G_B4_1 = G_B3_0;
		G_B4_2 = G_B3_1;
		G_B4_3 = G_B3_2;
		G_B4_4 = G_B3_3;
		G_B4_5 = G_B3_4;
		G_B4_6 = G_B3_5;
	}

IL_004c:
	{
		NullCheck(G_B4_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B4_2, G_B4_1);
		ArrayElementTypeCheck (G_B4_2, G_B4_0);
		(G_B4_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B4_1), (Il2CppObject *)G_B4_0);
		ObjectU5BU5D_t11523773* L_17 = G_B4_3;
		int32_t L_18 = V_0;
		int32_t L_19 = L_18;
		Il2CppObject * L_20 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_19);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 3);
		ArrayElementTypeCheck (L_17, L_20);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_20);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Format_m3351777162(NULL /*static, unused*/, G_B4_5, G_B4_4, L_17, /*hidden argument*/NULL);
		NullCheck(G_B4_6);
		XmlException_t3490696160 * L_22 = XmlTextReader_NotWFError_m304583423(G_B4_6, L_21, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_22);
	}

IL_0061:
	{
		return;
	}
}
// System.Void Mono.Xml2.XmlTextReader::Expect(System.String)
extern Il2CppClass* CultureInfo_t3603717042_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4253597076;
extern const uint32_t XmlTextReader_Expect_m1011563495_MetadataUsageId;
extern "C"  void XmlTextReader_Expect_m1011563495 (XmlTextReader_t3066586409 * __this, String_t* ___expected0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader_Expect_m1011563495_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_003d;
	}

IL_0007:
	{
		int32_t L_0 = XmlTextReader_ReadChar_m3857716364(__this, /*hidden argument*/NULL);
		String_t* L_1 = ___expected0;
		int32_t L_2 = V_0;
		NullCheck(L_1);
		uint16_t L_3 = String_get_Chars_m3015341861(L_1, L_2, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)L_3)))
		{
			goto IL_0039;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3603717042_il2cpp_TypeInfo_var);
		CultureInfo_t3603717042 * L_4 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_5 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1));
		String_t* L_6 = ___expected0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 0);
		ArrayElementTypeCheck (L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_6);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Format_m3351777162(NULL /*static, unused*/, L_4, _stringLiteral4253597076, L_5, /*hidden argument*/NULL);
		XmlException_t3490696160 * L_8 = XmlTextReader_NotWFError_m304583423(__this, L_7, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_0039:
	{
		int32_t L_9 = V_0;
		V_0 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_003d:
	{
		int32_t L_10 = V_0;
		String_t* L_11 = ___expected0;
		NullCheck(L_11);
		int32_t L_12 = String_get_Length_m2979997331(L_11, /*hidden argument*/NULL);
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void Mono.Xml2.XmlTextReader::ExpectAfterWhitespace(System.Char)
extern Il2CppClass* XmlChar_t3591879093_il2cpp_TypeInfo_var;
extern Il2CppClass* CultureInfo_t3603717042_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t2778706699_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral734441224;
extern Il2CppCodeGenString* _stringLiteral68828;
extern const uint32_t XmlTextReader_ExpectAfterWhitespace_m3276297213_MetadataUsageId;
extern "C"  void XmlTextReader_ExpectAfterWhitespace_m3276297213 (XmlTextReader_t3066586409 * __this, uint16_t ___c0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader_ExpectAfterWhitespace_m3276297213_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B6_0 = 0;
	ObjectU5BU5D_t11523773* G_B6_1 = NULL;
	ObjectU5BU5D_t11523773* G_B6_2 = NULL;
	String_t* G_B6_3 = NULL;
	CultureInfo_t3603717042 * G_B6_4 = NULL;
	XmlTextReader_t3066586409 * G_B6_5 = NULL;
	int32_t G_B5_0 = 0;
	ObjectU5BU5D_t11523773* G_B5_1 = NULL;
	ObjectU5BU5D_t11523773* G_B5_2 = NULL;
	String_t* G_B5_3 = NULL;
	CultureInfo_t3603717042 * G_B5_4 = NULL;
	XmlTextReader_t3066586409 * G_B5_5 = NULL;
	Il2CppObject * G_B7_0 = NULL;
	int32_t G_B7_1 = 0;
	ObjectU5BU5D_t11523773* G_B7_2 = NULL;
	ObjectU5BU5D_t11523773* G_B7_3 = NULL;
	String_t* G_B7_4 = NULL;
	CultureInfo_t3603717042 * G_B7_5 = NULL;
	XmlTextReader_t3066586409 * G_B7_6 = NULL;

IL_0000:
	{
		int32_t L_0 = XmlTextReader_ReadChar_m3857716364(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) >= ((int32_t)((int32_t)33))))
		{
			goto IL_001f;
		}
	}
	{
		int32_t L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(XmlChar_t3591879093_il2cpp_TypeInfo_var);
		bool L_3 = XmlChar_IsWhitespace_m1427848566(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001f;
		}
	}
	{
		goto IL_0000;
	}

IL_001f:
	{
		uint16_t L_4 = ___c0;
		int32_t L_5 = V_0;
		if ((((int32_t)L_4) == ((int32_t)L_5)))
		{
			goto IL_006f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3603717042_il2cpp_TypeInfo_var);
		CultureInfo_t3603717042 * L_6 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_7 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)3));
		uint16_t L_8 = ___c0;
		uint16_t L_9 = L_8;
		Il2CppObject * L_10 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, L_10);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_10);
		ObjectU5BU5D_t11523773* L_11 = L_7;
		int32_t L_12 = V_0;
		G_B5_0 = 1;
		G_B5_1 = L_11;
		G_B5_2 = L_11;
		G_B5_3 = _stringLiteral734441224;
		G_B5_4 = L_6;
		G_B5_5 = __this;
		if ((((int32_t)L_12) >= ((int32_t)0)))
		{
			G_B6_0 = 1;
			G_B6_1 = L_11;
			G_B6_2 = L_11;
			G_B6_3 = _stringLiteral734441224;
			G_B6_4 = L_6;
			G_B6_5 = __this;
			goto IL_0053;
		}
	}
	{
		G_B7_0 = ((Il2CppObject *)(_stringLiteral68828));
		G_B7_1 = G_B5_0;
		G_B7_2 = G_B5_1;
		G_B7_3 = G_B5_2;
		G_B7_4 = G_B5_3;
		G_B7_5 = G_B5_4;
		G_B7_6 = G_B5_5;
		goto IL_005a;
	}

IL_0053:
	{
		int32_t L_13 = V_0;
		uint16_t L_14 = ((uint16_t)(((int32_t)((uint16_t)L_13))));
		Il2CppObject * L_15 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_14);
		G_B7_0 = L_15;
		G_B7_1 = G_B6_0;
		G_B7_2 = G_B6_1;
		G_B7_3 = G_B6_2;
		G_B7_4 = G_B6_3;
		G_B7_5 = G_B6_4;
		G_B7_6 = G_B6_5;
	}

IL_005a:
	{
		NullCheck(G_B7_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B7_2, G_B7_1);
		ArrayElementTypeCheck (G_B7_2, G_B7_0);
		(G_B7_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B7_1), (Il2CppObject *)G_B7_0);
		ObjectU5BU5D_t11523773* L_16 = G_B7_3;
		int32_t L_17 = V_0;
		int32_t L_18 = L_17;
		Il2CppObject * L_19 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_18);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 2);
		ArrayElementTypeCheck (L_16, L_19);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_19);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = String_Format_m3351777162(NULL /*static, unused*/, G_B7_5, G_B7_4, L_16, /*hidden argument*/NULL);
		NullCheck(G_B7_6);
		XmlException_t3490696160 * L_21 = XmlTextReader_NotWFError_m304583423(G_B7_6, L_20, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}

IL_006f:
	{
		goto IL_0079;
	}
	// Dead block : IL_0074: br IL_0000

IL_0079:
	{
		return;
	}
}
// System.Boolean Mono.Xml2.XmlTextReader::SkipWhitespace()
extern "C"  bool XmlTextReader_SkipWhitespace_m1854637366 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = XmlTextReader_PeekChar_m254641297(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)((int32_t)32))))
		{
			goto IL_0026;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)((int32_t)9))))
		{
			goto IL_0026;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)10))))
		{
			goto IL_0026;
		}
	}
	{
		int32_t L_4 = V_0;
		G_B5_0 = ((((int32_t)L_4) == ((int32_t)((int32_t)13)))? 1 : 0);
		goto IL_0027;
	}

IL_0026:
	{
		G_B5_0 = 1;
	}

IL_0027:
	{
		V_1 = (bool)G_B5_0;
		bool L_5 = V_1;
		if (L_5)
		{
			goto IL_0030;
		}
	}
	{
		return (bool)0;
	}

IL_0030:
	{
		int32_t L_6 = V_0;
		XmlTextReader_Advance_m673767315(__this, L_6, /*hidden argument*/NULL);
		goto IL_0043;
	}

IL_003c:
	{
		int32_t L_7 = V_0;
		XmlTextReader_Advance_m673767315(__this, L_7, /*hidden argument*/NULL);
	}

IL_0043:
	{
		int32_t L_8 = XmlTextReader_PeekChar_m254641297(__this, /*hidden argument*/NULL);
		int32_t L_9 = L_8;
		V_0 = L_9;
		if ((((int32_t)L_9) == ((int32_t)((int32_t)32))))
		{
			goto IL_003c;
		}
	}
	{
		int32_t L_10 = V_0;
		if ((((int32_t)L_10) == ((int32_t)((int32_t)9))))
		{
			goto IL_003c;
		}
	}
	{
		int32_t L_11 = V_0;
		if ((((int32_t)L_11) == ((int32_t)((int32_t)10))))
		{
			goto IL_003c;
		}
	}
	{
		int32_t L_12 = V_0;
		if ((((int32_t)L_12) == ((int32_t)((int32_t)13))))
		{
			goto IL_003c;
		}
	}
	{
		bool L_13 = V_1;
		return L_13;
	}
}
// System.Boolean Mono.Xml2.XmlTextReader::ReadWhitespace()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t XmlTextReader_ReadWhitespace_m885165773_MetadataUsageId;
extern "C"  bool XmlTextReader_ReadWhitespace_m885165773 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader_ReadWhitespace_m885165773_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	bool V_3 = false;
	int32_t V_4 = 0;
	int32_t G_B11_0 = 0;
	int32_t G_B21_0 = 0;
	{
		int32_t L_0 = __this->get_currentState_40();
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		__this->set_currentState_40(((int32_t)17));
	}

IL_0013:
	{
		bool L_1 = __this->get_preserveCurrentTag_33();
		V_0 = L_1;
		__this->set_preserveCurrentTag_33((bool)1);
		int32_t L_2 = __this->get_peekCharsIndex_30();
		int32_t L_3 = __this->get_curNodePeekIndex_32();
		V_1 = ((int32_t)((int32_t)L_2-(int32_t)L_3));
		int32_t L_4 = XmlTextReader_PeekChar_m254641297(__this, /*hidden argument*/NULL);
		V_2 = L_4;
	}

IL_0036:
	{
		int32_t L_5 = V_2;
		XmlTextReader_Advance_m673767315(__this, L_5, /*hidden argument*/NULL);
		int32_t L_6 = XmlTextReader_PeekChar_m254641297(__this, /*hidden argument*/NULL);
		V_2 = L_6;
		int32_t L_7 = V_2;
		if ((((int32_t)L_7) == ((int32_t)((int32_t)32))))
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_8 = V_2;
		if ((((int32_t)L_8) == ((int32_t)((int32_t)9))))
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_9 = V_2;
		if ((((int32_t)L_9) == ((int32_t)((int32_t)10))))
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_10 = V_2;
		if ((((int32_t)L_10) == ((int32_t)((int32_t)13))))
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_11 = __this->get_currentState_40();
		if ((!(((uint32_t)L_11) == ((uint32_t)1))))
		{
			goto IL_0081;
		}
	}
	{
		int32_t L_12 = V_2;
		if ((((int32_t)L_12) == ((int32_t)(-1))))
		{
			goto IL_0081;
		}
	}
	{
		int32_t L_13 = V_2;
		G_B11_0 = ((((int32_t)((((int32_t)L_13) == ((int32_t)((int32_t)60)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0082;
	}

IL_0081:
	{
		G_B11_0 = 0;
	}

IL_0082:
	{
		V_3 = (bool)G_B11_0;
		bool L_14 = V_3;
		if (L_14)
		{
			goto IL_00af;
		}
	}
	{
		int32_t L_15 = __this->get_whitespaceHandling_45();
		if ((((int32_t)L_15) == ((int32_t)2)))
		{
			goto IL_00ad;
		}
	}
	{
		int32_t L_16 = __this->get_whitespaceHandling_45();
		if ((!(((uint32_t)L_16) == ((uint32_t)1))))
		{
			goto IL_00af;
		}
	}
	{
		int32_t L_17 = VirtFuncInvoker0< int32_t >::Invoke(24 /* System.Xml.XmlSpace Mono.Xml2.XmlTextReader::get_XmlSpace() */, __this);
		if ((((int32_t)L_17) == ((int32_t)2)))
		{
			goto IL_00af;
		}
	}

IL_00ad:
	{
		return (bool)0;
	}

IL_00af:
	{
		XmlTextReader_ClearValueBuffer_m495962918(__this, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_18 = __this->get_valueBuffer_27();
		CharU5BU5D_t3416858730* L_19 = __this->get_peekChars_29();
		int32_t L_20 = __this->get_curNodePeekIndex_32();
		int32_t L_21 = __this->get_peekCharsIndex_30();
		int32_t L_22 = __this->get_curNodePeekIndex_32();
		int32_t L_23 = V_1;
		NullCheck(L_18);
		StringBuilder_Append_m2623154804(L_18, L_19, L_20, ((int32_t)((int32_t)((int32_t)((int32_t)L_21-(int32_t)L_22))-(int32_t)L_23)), /*hidden argument*/NULL);
		bool L_24 = V_0;
		__this->set_preserveCurrentTag_33(L_24);
		bool L_25 = V_3;
		if (!L_25)
		{
			goto IL_00f5;
		}
	}
	{
		XmlTextReader_ReadText_m502843516(__this, (bool)0, /*hidden argument*/NULL);
		goto IL_0126;
	}

IL_00f5:
	{
		int32_t L_26 = VirtFuncInvoker0< int32_t >::Invoke(24 /* System.Xml.XmlSpace Mono.Xml2.XmlTextReader::get_XmlSpace() */, __this);
		if ((!(((uint32_t)L_26) == ((uint32_t)2))))
		{
			goto IL_0108;
		}
	}
	{
		G_B21_0 = ((int32_t)14);
		goto IL_010a;
	}

IL_0108:
	{
		G_B21_0 = ((int32_t)13);
	}

IL_010a:
	{
		V_4 = G_B21_0;
		int32_t L_27 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_28 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_29 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_30 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		XmlTextReader_SetProperties_m3034003792(__this, L_27, L_28, L_29, L_30, (bool)0, (String_t*)NULL, (bool)1, /*hidden argument*/NULL);
	}

IL_0126:
	{
		return (bool)1;
	}
}
// System.Int32 Mono.Xml2.XmlTextReader::ReadCharsInternal(System.Char[],System.Int32,System.Int32)
extern Il2CppCodeGenString* _stringLiteral519000108;
extern const uint32_t XmlTextReader_ReadCharsInternal_m2404589429_MetadataUsageId;
extern "C"  int32_t XmlTextReader_ReadCharsInternal_m2404589429 (XmlTextReader_t3066586409 * __this, CharU5BU5D_t3416858730* ___buffer0, int32_t ___offset1, int32_t ___length2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader_ReadCharsInternal_m2404589429_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		int32_t L_0 = ___offset1;
		V_0 = L_0;
		V_1 = 0;
		goto IL_0131;
	}

IL_0009:
	{
		int32_t L_1 = XmlTextReader_PeekChar_m254641297(__this, /*hidden argument*/NULL);
		V_2 = L_1;
		int32_t L_2 = V_2;
		V_3 = L_2;
		int32_t L_3 = V_3;
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0026;
		}
	}
	{
		int32_t L_4 = V_3;
		if ((((int32_t)L_4) == ((int32_t)((int32_t)60))))
		{
			goto IL_0032;
		}
	}
	{
		goto IL_00d2;
	}

IL_0026:
	{
		XmlException_t3490696160 * L_5 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral519000108, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0032:
	{
		int32_t L_6 = V_1;
		int32_t L_7 = ___length2;
		if ((!(((uint32_t)((int32_t)((int32_t)L_6+(int32_t)1))) == ((uint32_t)L_7))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_8 = V_1;
		return L_8;
	}

IL_003d:
	{
		int32_t L_9 = V_2;
		XmlTextReader_Advance_m673767315(__this, L_9, /*hidden argument*/NULL);
		int32_t L_10 = XmlTextReader_PeekChar_m254641297(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_10) == ((int32_t)((int32_t)47))))
		{
			goto IL_006d;
		}
	}
	{
		int32_t L_11 = __this->get_nestLevel_41();
		__this->set_nestLevel_41(((int32_t)((int32_t)L_11+(int32_t)1)));
		CharU5BU5D_t3416858730* L_12 = ___buffer0;
		int32_t L_13 = V_0;
		int32_t L_14 = L_13;
		V_0 = ((int32_t)((int32_t)L_14+(int32_t)1));
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_14);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(L_14), (uint16_t)((int32_t)60));
		goto IL_012d;
	}

IL_006d:
	{
		int32_t L_15 = __this->get_nestLevel_41();
		int32_t L_16 = L_15;
		V_4 = L_16;
		__this->set_nestLevel_41(((int32_t)((int32_t)L_16-(int32_t)1)));
		int32_t L_17 = V_4;
		if ((((int32_t)L_17) <= ((int32_t)0)))
		{
			goto IL_0094;
		}
	}
	{
		CharU5BU5D_t3416858730* L_18 = ___buffer0;
		int32_t L_19 = V_0;
		int32_t L_20 = L_19;
		V_0 = ((int32_t)((int32_t)L_20+(int32_t)1));
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_20);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(L_20), (uint16_t)((int32_t)60));
		goto IL_012d;
	}

IL_0094:
	{
		XmlTextReader_Expect_m2788845516(__this, ((int32_t)47), /*hidden argument*/NULL);
		bool L_21 = __this->get_depthUp_19();
		if (!L_21)
		{
			goto IL_00bc;
		}
	}
	{
		int32_t L_22 = __this->get_depth_17();
		__this->set_depth_17(((int32_t)((int32_t)L_22+(int32_t)1)));
		__this->set_depthUp_19((bool)0);
	}

IL_00bc:
	{
		XmlTextReader_ReadEndTag_m3759677719(__this, /*hidden argument*/NULL);
		__this->set_readCharsInProgress_42((bool)0);
		VirtFuncInvoker0< bool >::Invoke(38 /* System.Boolean Mono.Xml2.XmlTextReader::Read() */, __this);
		int32_t L_23 = V_1;
		return L_23;
	}

IL_00d2:
	{
		int32_t L_24 = V_2;
		XmlTextReader_Advance_m673767315(__this, L_24, /*hidden argument*/NULL);
		int32_t L_25 = V_2;
		if ((((int32_t)L_25) >= ((int32_t)((int32_t)65535))))
		{
			goto IL_00f2;
		}
	}
	{
		CharU5BU5D_t3416858730* L_26 = ___buffer0;
		int32_t L_27 = V_0;
		int32_t L_28 = L_27;
		V_0 = ((int32_t)((int32_t)L_28+(int32_t)1));
		int32_t L_29 = V_2;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, L_28);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(L_28), (uint16_t)(((int32_t)((uint16_t)L_29))));
		goto IL_0128;
	}

IL_00f2:
	{
		CharU5BU5D_t3416858730* L_30 = ___buffer0;
		int32_t L_31 = V_0;
		int32_t L_32 = L_31;
		V_0 = ((int32_t)((int32_t)L_32+(int32_t)1));
		int32_t L_33 = V_2;
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, L_32);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(L_32), (uint16_t)(((int32_t)((uint16_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_33-(int32_t)((int32_t)65536)))/(int32_t)((int32_t)1024)))+(int32_t)((int32_t)55296)))))));
		CharU5BU5D_t3416858730* L_34 = ___buffer0;
		int32_t L_35 = V_0;
		int32_t L_36 = L_35;
		V_0 = ((int32_t)((int32_t)L_36+(int32_t)1));
		int32_t L_37 = V_2;
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, L_36);
		(L_34)->SetAt(static_cast<il2cpp_array_size_t>(L_36), (uint16_t)(((int32_t)((uint16_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_37-(int32_t)((int32_t)65536)))%(int32_t)((int32_t)1024)))+(int32_t)((int32_t)56320)))))));
	}

IL_0128:
	{
		goto IL_012d;
	}

IL_012d:
	{
		int32_t L_38 = V_1;
		V_1 = ((int32_t)((int32_t)L_38+(int32_t)1));
	}

IL_0131:
	{
		int32_t L_39 = V_1;
		int32_t L_40 = ___length2;
		if ((((int32_t)L_39) < ((int32_t)L_40)))
		{
			goto IL_0009;
		}
	}
	{
		int32_t L_41 = ___length2;
		return L_41;
	}
}
// System.Boolean Mono.Xml2.XmlTextReader::ReadUntilEndTag()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral519000108;
extern const uint32_t XmlTextReader_ReadUntilEndTag_m2571482831_MetadataUsageId;
extern "C"  bool XmlTextReader_ReadUntilEndTag_m2571482831 (XmlTextReader_t3066586409 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTextReader_ReadUntilEndTag_m2571482831_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	String_t* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 Mono.Xml2.XmlTextReader::get_Depth() */, __this);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		__this->set_currentState_40(((int32_t)15));
	}

IL_0013:
	{
		int32_t L_1 = XmlTextReader_ReadChar_m3857716364(__this, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		V_2 = L_2;
		int32_t L_3 = V_2;
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0030;
		}
	}
	{
		int32_t L_4 = V_2;
		if ((((int32_t)L_4) == ((int32_t)((int32_t)60))))
		{
			goto IL_003c;
		}
	}
	{
		goto IL_00cb;
	}

IL_0030:
	{
		XmlException_t3490696160 * L_5 = XmlTextReader_NotWFError_m304583423(__this, _stringLiteral519000108, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_003c:
	{
		int32_t L_6 = XmlTextReader_PeekChar_m254641297(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_6) == ((int32_t)((int32_t)47))))
		{
			goto IL_005c;
		}
	}
	{
		int32_t L_7 = __this->get_nestLevel_41();
		__this->set_nestLevel_41(((int32_t)((int32_t)L_7+(int32_t)1)));
		goto IL_00cb;
	}

IL_005c:
	{
		int32_t L_8 = __this->get_nestLevel_41();
		int32_t L_9 = ((int32_t)((int32_t)L_8-(int32_t)1));
		V_3 = L_9;
		__this->set_nestLevel_41(L_9);
		int32_t L_10 = V_3;
		if ((((int32_t)L_10) <= ((int32_t)0)))
		{
			goto IL_0078;
		}
	}
	{
		goto IL_00cb;
	}

IL_0078:
	{
		XmlTextReader_ReadChar_m3857716364(__this, /*hidden argument*/NULL);
		String_t* L_11 = XmlTextReader_ReadName_m2874594848(__this, /*hidden argument*/NULL);
		V_1 = L_11;
		String_t* L_12 = V_1;
		TagNameU5BU5D_t2465062056* L_13 = __this->get_elementNames_21();
		int32_t L_14 = __this->get_elementNameStackPos_22();
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, ((int32_t)((int32_t)L_14-(int32_t)1)));
		String_t* L_15 = ((L_13)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_14-(int32_t)1)))))->get_Name_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_16 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_12, L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00ae;
		}
	}
	{
		goto IL_00cb;
	}

IL_00ae:
	{
		XmlTextReader_Expect_m2788845516(__this, ((int32_t)62), /*hidden argument*/NULL);
		int32_t L_17 = __this->get_depth_17();
		__this->set_depth_17(((int32_t)((int32_t)L_17-(int32_t)1)));
		bool L_18 = VirtFuncInvoker0< bool >::Invoke(38 /* System.Boolean Mono.Xml2.XmlTextReader::Read() */, __this);
		return L_18;
	}

IL_00cb:
	{
		goto IL_0013;
	}
}
// System.Void Mono.Xml2.XmlTextReader/DtdInputStateStack::.ctor()
extern Il2CppClass* Stack_t1623036922_il2cpp_TypeInfo_var;
extern const uint32_t DtdInputStateStack__ctor_m2784489544_MetadataUsageId;
extern "C"  void DtdInputStateStack__ctor_m2784489544 (DtdInputStateStack_t1263389581 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DtdInputStateStack__ctor_m2784489544_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Stack_t1623036922 * L_0 = (Stack_t1623036922 *)il2cpp_codegen_object_new(Stack_t1623036922_il2cpp_TypeInfo_var);
		Stack__ctor_m1821673314(L_0, /*hidden argument*/NULL);
		__this->set_intern_0(L_0);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		DtdInputStateStack_Push_m1703488689(__this, 1, /*hidden argument*/NULL);
		return;
	}
}
// Mono.Xml2.XmlTextReader/DtdInputState Mono.Xml2.XmlTextReader/DtdInputStateStack::Peek()
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t DtdInputStateStack_Peek_m2361625023_MetadataUsageId;
extern "C"  int32_t DtdInputStateStack_Peek_m2361625023 (DtdInputStateStack_t1263389581 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DtdInputStateStack_Peek_m2361625023_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Stack_t1623036922 * L_0 = __this->get_intern_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = VirtFuncInvoker0< Il2CppObject * >::Invoke(18 /* System.Object System.Collections.Stack::Peek() */, L_0);
		return (int32_t)(((*(int32_t*)((int32_t*)UnBox (L_1, Int32_t2847414787_il2cpp_TypeInfo_var)))));
	}
}
// Mono.Xml2.XmlTextReader/DtdInputState Mono.Xml2.XmlTextReader/DtdInputStateStack::Pop()
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t DtdInputStateStack_Pop_m3263076495_MetadataUsageId;
extern "C"  int32_t DtdInputStateStack_Pop_m3263076495 (DtdInputStateStack_t1263389581 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DtdInputStateStack_Pop_m3263076495_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Stack_t1623036922 * L_0 = __this->get_intern_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = VirtFuncInvoker0< Il2CppObject * >::Invoke(19 /* System.Object System.Collections.Stack::Pop() */, L_0);
		return (int32_t)(((*(int32_t*)((int32_t*)UnBox (L_1, Int32_t2847414787_il2cpp_TypeInfo_var)))));
	}
}
// System.Void Mono.Xml2.XmlTextReader/DtdInputStateStack::Push(Mono.Xml2.XmlTextReader/DtdInputState)
extern Il2CppClass* DtdInputState_t3379700667_il2cpp_TypeInfo_var;
extern const uint32_t DtdInputStateStack_Push_m1703488689_MetadataUsageId;
extern "C"  void DtdInputStateStack_Push_m1703488689 (DtdInputStateStack_t1263389581 * __this, int32_t ___val0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DtdInputStateStack_Push_m1703488689_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Stack_t1623036922 * L_0 = __this->get_intern_0();
		int32_t L_1 = ___val0;
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(DtdInputState_t3379700667_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		VirtActionInvoker1< Il2CppObject * >::Invoke(20 /* System.Void System.Collections.Stack::Push(System.Object) */, L_0, L_3);
		return;
	}
}
// System.Void Mono.Xml2.XmlTextReader/TagName::.ctor(System.String,System.String,System.String)
extern "C"  void TagName__ctor_m1413447022 (TagName_t115468581 * __this, String_t* ___n0, String_t* ___l1, String_t* ___p2, const MethodInfo* method)
{
	{
		String_t* L_0 = ___n0;
		__this->set_Name_0(L_0);
		String_t* L_1 = ___l1;
		__this->set_LocalName_1(L_1);
		String_t* L_2 = ___p2;
		__this->set_Prefix_2(L_2);
		return;
	}
}
// Conversion methods for marshalling of: Mono.Xml2.XmlTextReader/TagName
extern "C" void TagName_t115468581_marshal_pinvoke(const TagName_t115468581& unmarshaled, TagName_t115468581_marshaled_pinvoke& marshaled)
{
	marshaled.___Name_0 = il2cpp_codegen_marshal_string(unmarshaled.get_Name_0());
	marshaled.___LocalName_1 = il2cpp_codegen_marshal_string(unmarshaled.get_LocalName_1());
	marshaled.___Prefix_2 = il2cpp_codegen_marshal_string(unmarshaled.get_Prefix_2());
}
extern "C" void TagName_t115468581_marshal_pinvoke_back(const TagName_t115468581_marshaled_pinvoke& marshaled, TagName_t115468581& unmarshaled)
{
	unmarshaled.set_Name_0(il2cpp_codegen_marshal_string_result(marshaled.___Name_0));
	unmarshaled.set_LocalName_1(il2cpp_codegen_marshal_string_result(marshaled.___LocalName_1));
	unmarshaled.set_Prefix_2(il2cpp_codegen_marshal_string_result(marshaled.___Prefix_2));
}
// Conversion method for clean up from marshalling of: Mono.Xml2.XmlTextReader/TagName
extern "C" void TagName_t115468581_marshal_pinvoke_cleanup(TagName_t115468581_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___Name_0);
	marshaled.___Name_0 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___LocalName_1);
	marshaled.___LocalName_1 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___Prefix_2);
	marshaled.___Prefix_2 = NULL;
}
// Conversion methods for marshalling of: Mono.Xml2.XmlTextReader/TagName
extern "C" void TagName_t115468581_marshal_com(const TagName_t115468581& unmarshaled, TagName_t115468581_marshaled_com& marshaled)
{
	marshaled.___Name_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_Name_0());
	marshaled.___LocalName_1 = il2cpp_codegen_marshal_bstring(unmarshaled.get_LocalName_1());
	marshaled.___Prefix_2 = il2cpp_codegen_marshal_bstring(unmarshaled.get_Prefix_2());
}
extern "C" void TagName_t115468581_marshal_com_back(const TagName_t115468581_marshaled_com& marshaled, TagName_t115468581& unmarshaled)
{
	unmarshaled.set_Name_0(il2cpp_codegen_marshal_bstring_result(marshaled.___Name_0));
	unmarshaled.set_LocalName_1(il2cpp_codegen_marshal_bstring_result(marshaled.___LocalName_1));
	unmarshaled.set_Prefix_2(il2cpp_codegen_marshal_bstring_result(marshaled.___Prefix_2));
}
// Conversion method for clean up from marshalling of: Mono.Xml2.XmlTextReader/TagName
extern "C" void TagName_t115468581_marshal_com_cleanup(TagName_t115468581_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___Name_0);
	marshaled.___Name_0 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___LocalName_1);
	marshaled.___LocalName_1 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___Prefix_2);
	marshaled.___Prefix_2 = NULL;
}
// System.Void Mono.Xml2.XmlTextReader/XmlAttributeTokenInfo::.ctor(Mono.Xml2.XmlTextReader)
extern Il2CppClass* StringBuilder_t3822575854_il2cpp_TypeInfo_var;
extern const uint32_t XmlAttributeTokenInfo__ctor_m1004432676_MetadataUsageId;
extern "C"  void XmlAttributeTokenInfo__ctor_m1004432676 (XmlAttributeTokenInfo_t922105698 * __this, XmlTextReader_t3066586409 * ___reader0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlAttributeTokenInfo__ctor_m1004432676_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringBuilder_t3822575854 * L_0 = (StringBuilder_t3822575854 *)il2cpp_codegen_object_new(StringBuilder_t3822575854_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m135953004(L_0, /*hidden argument*/NULL);
		__this->set_tmpBuilder_16(L_0);
		XmlTextReader_t3066586409 * L_1 = ___reader0;
		XmlTokenInfo__ctor_m2227356974(__this, L_1, /*hidden argument*/NULL);
		((XmlTokenInfo_t2571680784 *)__this)->set_NodeType_12(2);
		return;
	}
}
// System.String Mono.Xml2.XmlTextReader/XmlAttributeTokenInfo::get_Value()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral38;
extern Il2CppCodeGenString* _stringLiteral59;
extern const uint32_t XmlAttributeTokenInfo_get_Value_m3945488568_MetadataUsageId;
extern "C"  String_t* XmlAttributeTokenInfo_get_Value_m3945488568 (XmlAttributeTokenInfo_t922105698 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlAttributeTokenInfo_get_Value_m3945488568_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XmlTokenInfo_t2571680784 * V_0 = NULL;
	int32_t V_1 = 0;
	XmlTokenInfo_t2571680784 * V_2 = NULL;
	{
		String_t* L_0 = __this->get_valueCache_15();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		String_t* L_1 = __this->get_valueCache_15();
		return L_1;
	}

IL_0012:
	{
		int32_t L_2 = __this->get_ValueTokenStartIndex_13();
		int32_t L_3 = __this->get_ValueTokenEndIndex_14();
		if ((!(((uint32_t)L_2) == ((uint32_t)L_3))))
		{
			goto IL_0075;
		}
	}
	{
		XmlTextReader_t3066586409 * L_4 = ((XmlTokenInfo_t2571680784 *)__this)->get_Reader_1();
		NullCheck(L_4);
		XmlTokenInfoU5BU5D_t2413494705* L_5 = L_4->get_attributeValueTokens_8();
		int32_t L_6 = __this->get_ValueTokenStartIndex_13();
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		V_0 = ((L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7)));
		XmlTokenInfo_t2571680784 * L_8 = V_0;
		NullCheck(L_8);
		int32_t L_9 = L_8->get_NodeType_12();
		if ((!(((uint32_t)L_9) == ((uint32_t)5))))
		{
			goto IL_0062;
		}
	}
	{
		XmlTokenInfo_t2571680784 * L_10 = V_0;
		NullCheck(L_10);
		String_t* L_11 = L_10->get_Name_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral38, L_11, _stringLiteral59, /*hidden argument*/NULL);
		__this->set_valueCache_15(L_12);
		goto IL_006e;
	}

IL_0062:
	{
		XmlTokenInfo_t2571680784 * L_13 = V_0;
		NullCheck(L_13);
		String_t* L_14 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String Mono.Xml2.XmlTextReader/XmlTokenInfo::get_Value() */, L_13);
		__this->set_valueCache_15(L_14);
	}

IL_006e:
	{
		String_t* L_15 = __this->get_valueCache_15();
		return L_15;
	}

IL_0075:
	{
		StringBuilder_t3822575854 * L_16 = __this->get_tmpBuilder_16();
		NullCheck(L_16);
		StringBuilder_set_Length_m1952332172(L_16, 0, /*hidden argument*/NULL);
		int32_t L_17 = __this->get_ValueTokenStartIndex_13();
		V_1 = L_17;
		goto IL_00f0;
	}

IL_008d:
	{
		XmlTextReader_t3066586409 * L_18 = ((XmlTokenInfo_t2571680784 *)__this)->get_Reader_1();
		NullCheck(L_18);
		XmlTokenInfoU5BU5D_t2413494705* L_19 = L_18->get_attributeValueTokens_8();
		int32_t L_20 = V_1;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, L_20);
		int32_t L_21 = L_20;
		V_2 = ((L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21)));
		XmlTokenInfo_t2571680784 * L_22 = V_2;
		NullCheck(L_22);
		int32_t L_23 = L_22->get_NodeType_12();
		if ((!(((uint32_t)L_23) == ((uint32_t)3))))
		{
			goto IL_00be;
		}
	}
	{
		StringBuilder_t3822575854 * L_24 = __this->get_tmpBuilder_16();
		XmlTokenInfo_t2571680784 * L_25 = V_2;
		NullCheck(L_25);
		String_t* L_26 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String Mono.Xml2.XmlTextReader/XmlTokenInfo::get_Value() */, L_25);
		NullCheck(L_24);
		StringBuilder_Append_m3898090075(L_24, L_26, /*hidden argument*/NULL);
		goto IL_00ec;
	}

IL_00be:
	{
		StringBuilder_t3822575854 * L_27 = __this->get_tmpBuilder_16();
		NullCheck(L_27);
		StringBuilder_Append_m2143093878(L_27, ((int32_t)38), /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_28 = __this->get_tmpBuilder_16();
		XmlTokenInfo_t2571680784 * L_29 = V_2;
		NullCheck(L_29);
		String_t* L_30 = L_29->get_Name_2();
		NullCheck(L_28);
		StringBuilder_Append_m3898090075(L_28, L_30, /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_31 = __this->get_tmpBuilder_16();
		NullCheck(L_31);
		StringBuilder_Append_m2143093878(L_31, ((int32_t)59), /*hidden argument*/NULL);
	}

IL_00ec:
	{
		int32_t L_32 = V_1;
		V_1 = ((int32_t)((int32_t)L_32+(int32_t)1));
	}

IL_00f0:
	{
		int32_t L_33 = V_1;
		int32_t L_34 = __this->get_ValueTokenEndIndex_14();
		if ((((int32_t)L_33) <= ((int32_t)L_34)))
		{
			goto IL_008d;
		}
	}
	{
		StringBuilder_t3822575854 * L_35 = __this->get_tmpBuilder_16();
		StringBuilder_t3822575854 * L_36 = __this->get_tmpBuilder_16();
		NullCheck(L_36);
		int32_t L_37 = StringBuilder_get_Length_m2443133099(L_36, /*hidden argument*/NULL);
		NullCheck(L_35);
		String_t* L_38 = StringBuilder_ToString_m3621056261(L_35, 0, L_37, /*hidden argument*/NULL);
		__this->set_valueCache_15(L_38);
		String_t* L_39 = __this->get_valueCache_15();
		return L_39;
	}
}
// System.Void Mono.Xml2.XmlTextReader/XmlAttributeTokenInfo::set_Value(System.String)
extern "C"  void XmlAttributeTokenInfo_set_Value_m2202117569 (XmlAttributeTokenInfo_t922105698 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_valueCache_15(L_0);
		return;
	}
}
// System.Void Mono.Xml2.XmlTextReader/XmlAttributeTokenInfo::Clear()
extern "C"  void XmlAttributeTokenInfo_Clear_m2942509466 (XmlAttributeTokenInfo_t922105698 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		XmlTokenInfo_Clear_m363498704(__this, /*hidden argument*/NULL);
		__this->set_valueCache_15((String_t*)NULL);
		((XmlTokenInfo_t2571680784 *)__this)->set_NodeType_12(2);
		int32_t L_0 = 0;
		V_0 = L_0;
		__this->set_ValueTokenEndIndex_14(L_0);
		int32_t L_1 = V_0;
		__this->set_ValueTokenStartIndex_13(L_1);
		return;
	}
}
// System.Void Mono.Xml2.XmlTextReader/XmlAttributeTokenInfo::FillXmlns()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral114177052;
extern const uint32_t XmlAttributeTokenInfo_FillXmlns_m2684533414_MetadataUsageId;
extern "C"  void XmlAttributeTokenInfo_FillXmlns_m2684533414 (XmlAttributeTokenInfo_t922105698 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlAttributeTokenInfo_FillXmlns_m2684533414_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ((XmlTokenInfo_t2571680784 *)__this)->get_Prefix_4();
		bool L_1 = Object_ReferenceEquals_m3695130242(NULL /*static, unused*/, L_0, _stringLiteral114177052, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0036;
		}
	}
	{
		XmlTextReader_t3066586409 * L_2 = ((XmlTokenInfo_t2571680784 *)__this)->get_Reader_1();
		NullCheck(L_2);
		XmlNamespaceManager_t1861067185 * L_3 = L_2->get_nsmgr_14();
		String_t* L_4 = ((XmlTokenInfo_t2571680784 *)__this)->get_LocalName_3();
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String Mono.Xml2.XmlTextReader/XmlAttributeTokenInfo::get_Value() */, __this);
		NullCheck(L_3);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(8 /* System.Void System.Xml.XmlNamespaceManager::AddNamespace(System.String,System.String) */, L_3, L_4, L_5);
		goto IL_0066;
	}

IL_0036:
	{
		String_t* L_6 = ((XmlTokenInfo_t2571680784 *)__this)->get_Name_2();
		bool L_7 = Object_ReferenceEquals_m3695130242(NULL /*static, unused*/, L_6, _stringLiteral114177052, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0066;
		}
	}
	{
		XmlTextReader_t3066586409 * L_8 = ((XmlTokenInfo_t2571680784 *)__this)->get_Reader_1();
		NullCheck(L_8);
		XmlNamespaceManager_t1861067185 * L_9 = L_8->get_nsmgr_14();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String Mono.Xml2.XmlTextReader/XmlAttributeTokenInfo::get_Value() */, __this);
		NullCheck(L_9);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(8 /* System.Void System.Xml.XmlNamespaceManager::AddNamespace(System.String,System.String) */, L_9, L_10, L_11);
	}

IL_0066:
	{
		return;
	}
}
// System.Void Mono.Xml2.XmlTextReader/XmlAttributeTokenInfo::FillNamespace()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral114177052;
extern Il2CppCodeGenString* _stringLiteral557947472;
extern const uint32_t XmlAttributeTokenInfo_FillNamespace_m1012079525_MetadataUsageId;
extern "C"  void XmlAttributeTokenInfo_FillNamespace_m1012079525 (XmlAttributeTokenInfo_t922105698 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlAttributeTokenInfo_FillNamespace_m1012079525_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ((XmlTokenInfo_t2571680784 *)__this)->get_Prefix_4();
		bool L_1 = Object_ReferenceEquals_m3695130242(NULL /*static, unused*/, L_0, _stringLiteral114177052, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_002a;
		}
	}
	{
		String_t* L_2 = ((XmlTokenInfo_t2571680784 *)__this)->get_Name_2();
		bool L_3 = Object_ReferenceEquals_m3695130242(NULL /*static, unused*/, L_2, _stringLiteral114177052, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003a;
		}
	}

IL_002a:
	{
		((XmlTokenInfo_t2571680784 *)__this)->set_NamespaceURI_5(_stringLiteral557947472);
		goto IL_0072;
	}

IL_003a:
	{
		String_t* L_4 = ((XmlTokenInfo_t2571680784 *)__this)->get_Prefix_4();
		NullCheck(L_4);
		int32_t L_5 = String_get_Length_m2979997331(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_005a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		((XmlTokenInfo_t2571680784 *)__this)->set_NamespaceURI_5(L_6);
		goto IL_0072;
	}

IL_005a:
	{
		XmlTextReader_t3066586409 * L_7 = ((XmlTokenInfo_t2571680784 *)__this)->get_Reader_1();
		String_t* L_8 = ((XmlTokenInfo_t2571680784 *)__this)->get_Prefix_4();
		NullCheck(L_7);
		String_t* L_9 = XmlTextReader_LookupNamespace_m3699761695(L_7, L_8, (bool)1, /*hidden argument*/NULL);
		((XmlTokenInfo_t2571680784 *)__this)->set_NamespaceURI_5(L_9);
	}

IL_0072:
	{
		return;
	}
}
// System.Void Mono.Xml2.XmlTextReader/XmlTokenInfo::.ctor(Mono.Xml2.XmlTextReader)
extern "C"  void XmlTokenInfo__ctor_m2227356974 (XmlTokenInfo_t2571680784 * __this, XmlTextReader_t3066586409 * ___xtr0, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		XmlTextReader_t3066586409 * L_0 = ___xtr0;
		__this->set_Reader_1(L_0);
		VirtActionInvoker0::Invoke(6 /* System.Void Mono.Xml2.XmlTextReader/XmlTokenInfo::Clear() */, __this);
		return;
	}
}
// System.String Mono.Xml2.XmlTextReader/XmlTokenInfo::get_Value()
extern "C"  String_t* XmlTokenInfo_get_Value_m663502792 (XmlTokenInfo_t2571680784 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		String_t* L_0 = __this->get_valueCache_0();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		String_t* L_1 = __this->get_valueCache_0();
		return L_1;
	}

IL_0012:
	{
		int32_t L_2 = __this->get_ValueBufferStart_10();
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_004e;
		}
	}
	{
		XmlTextReader_t3066586409 * L_3 = __this->get_Reader_1();
		NullCheck(L_3);
		StringBuilder_t3822575854 * L_4 = L_3->get_valueBuffer_27();
		int32_t L_5 = __this->get_ValueBufferStart_10();
		int32_t L_6 = __this->get_ValueBufferEnd_11();
		int32_t L_7 = __this->get_ValueBufferStart_10();
		NullCheck(L_4);
		String_t* L_8 = StringBuilder_ToString_m3621056261(L_4, L_5, ((int32_t)((int32_t)L_6-(int32_t)L_7)), /*hidden argument*/NULL);
		__this->set_valueCache_0(L_8);
		String_t* L_9 = __this->get_valueCache_0();
		return L_9;
	}

IL_004e:
	{
		int32_t L_10 = __this->get_NodeType_12();
		V_0 = L_10;
		int32_t L_11 = V_0;
		if (((int32_t)((int32_t)L_11-(int32_t)3)) == 0)
		{
			goto IL_0092;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)3)) == 1)
		{
			goto IL_0092;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)3)) == 2)
		{
			goto IL_00aa;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)3)) == 3)
		{
			goto IL_00aa;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)3)) == 4)
		{
			goto IL_0092;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)3)) == 5)
		{
			goto IL_0092;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)3)) == 6)
		{
			goto IL_00aa;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)3)) == 7)
		{
			goto IL_00aa;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)3)) == 8)
		{
			goto IL_00aa;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)3)) == 9)
		{
			goto IL_00aa;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)3)) == 10)
		{
			goto IL_0092;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)3)) == 11)
		{
			goto IL_0092;
		}
	}
	{
		goto IL_00aa;
	}

IL_0092:
	{
		XmlTextReader_t3066586409 * L_12 = __this->get_Reader_1();
		NullCheck(L_12);
		String_t* L_13 = XmlTextReader_CreateValueString_m2171097961(L_12, /*hidden argument*/NULL);
		__this->set_valueCache_0(L_13);
		String_t* L_14 = __this->get_valueCache_0();
		return L_14;
	}

IL_00aa:
	{
		return (String_t*)NULL;
	}
}
// System.Void Mono.Xml2.XmlTextReader/XmlTokenInfo::set_Value(System.String)
extern "C"  void XmlTokenInfo_set_Value_m69422411 (XmlTokenInfo_t2571680784 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_valueCache_0(L_0);
		return;
	}
}
// System.Void Mono.Xml2.XmlTextReader/XmlTokenInfo::Clear()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t XmlTokenInfo_Clear_m363498704_MetadataUsageId;
extern "C"  void XmlTokenInfo_Clear_m363498704 (XmlTokenInfo_t2571680784 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (XmlTokenInfo_Clear_m363498704_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	{
		__this->set_ValueBufferStart_10((-1));
		__this->set_valueCache_0((String_t*)NULL);
		__this->set_NodeType_12(0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_1 = L_0;
		V_0 = L_1;
		__this->set_NamespaceURI_5(L_1);
		String_t* L_2 = V_0;
		String_t* L_3 = L_2;
		V_0 = L_3;
		__this->set_Prefix_4(L_3);
		String_t* L_4 = V_0;
		String_t* L_5 = L_4;
		V_0 = L_5;
		__this->set_LocalName_3(L_5);
		String_t* L_6 = V_0;
		__this->set_Name_2(L_6);
		__this->set_IsEmptyElement_6((bool)0);
		__this->set_QuoteChar_7(((int32_t)34));
		int32_t L_7 = 0;
		V_1 = L_7;
		__this->set_LinePosition_9(L_7);
		int32_t L_8 = V_1;
		__this->set_LineNumber_8(L_8);
		return;
	}
}
// System.Void System.MonoTODOAttribute::.ctor()
extern "C"  void MonoTODOAttribute__ctor_m821822065 (MonoTODOAttribute_t1287393899 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.DTDReader::.ctor(Mono.Xml.DTDObjectModel,System.Int32,System.Int32)
extern "C"  void DTDReader__ctor_m239668310 (DTDReader_t4257441119 * __this, DTDObjectModel_t709926554 * ___dtd0, int32_t ___startLineNumber1, int32_t ___startLinePosition2, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		DTDObjectModel_t709926554 * L_0 = ___dtd0;
		__this->set_DTD_13(L_0);
		int32_t L_1 = ___startLineNumber1;
		__this->set_currentLinkedNodeLineNumber_6(L_1);
		int32_t L_2 = ___startLinePosition2;
		__this->set_currentLinkedNodeLinePosition_7(L_2);
		DTDReader_Init_m3143559120(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String System.Xml.DTDReader::get_BaseURI()
extern "C"  String_t* DTDReader_get_BaseURI_m1245597521 (DTDReader_t4257441119 * __this, const MethodInfo* method)
{
	{
		XmlParserInput_t3311178812 * L_0 = __this->get_currentInput_0();
		NullCheck(L_0);
		String_t* L_1 = XmlParserInput_get_BaseURI_m2378535050(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Xml.DTDReader::get_Normalization()
extern "C"  bool DTDReader_get_Normalization_m2363640846 (DTDReader_t4257441119 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_normalization_9();
		return L_0;
	}
}
// System.Void System.Xml.DTDReader::set_Normalization(System.Boolean)
extern "C"  void DTDReader_set_Normalization_m4032164889 (DTDReader_t4257441119 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_normalization_9(L_0);
		return;
	}
}
// System.Int32 System.Xml.DTDReader::get_LineNumber()
extern "C"  int32_t DTDReader_get_LineNumber_m3092317640 (DTDReader_t4257441119 * __this, const MethodInfo* method)
{
	{
		XmlParserInput_t3311178812 * L_0 = __this->get_currentInput_0();
		NullCheck(L_0);
		int32_t L_1 = XmlParserInput_get_LineNumber_m2726813009(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 System.Xml.DTDReader::get_LinePosition()
extern "C"  int32_t DTDReader_get_LinePosition_m1374475304 (DTDReader_t4257441119 * __this, const MethodInfo* method)
{
	{
		XmlParserInput_t3311178812 * L_0 = __this->get_currentInput_0();
		NullCheck(L_0);
		int32_t L_1 = XmlParserInput_get_LinePosition_m2311843185(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Xml.XmlException System.Xml.DTDReader::NotWFError(System.String)
extern Il2CppClass* XmlException_t3490696160_il2cpp_TypeInfo_var;
extern const uint32_t DTDReader_NotWFError_m440433271_MetadataUsageId;
extern "C"  XmlException_t3490696160 * DTDReader_NotWFError_m440433271 (DTDReader_t4257441119 * __this, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDReader_NotWFError_m440433271_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = DTDReader_get_BaseURI_m1245597521(__this, /*hidden argument*/NULL);
		String_t* L_1 = ___message0;
		XmlException_t3490696160 * L_2 = (XmlException_t3490696160 *)il2cpp_codegen_object_new(XmlException_t3490696160_il2cpp_TypeInfo_var);
		XmlException__ctor_m3778248785(L_2, __this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void System.Xml.DTDReader::Init()
extern Il2CppClass* Stack_t1623036922_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t3416858730_il2cpp_TypeInfo_var;
extern Il2CppClass* StringBuilder_t3822575854_il2cpp_TypeInfo_var;
extern const uint32_t DTDReader_Init_m3143559120_MetadataUsageId;
extern "C"  void DTDReader_Init_m3143559120 (DTDReader_t4257441119 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDReader_Init_m3143559120_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Stack_t1623036922 * L_0 = (Stack_t1623036922 *)il2cpp_codegen_object_new(Stack_t1623036922_il2cpp_TypeInfo_var);
		Stack__ctor_m1821673314(L_0, /*hidden argument*/NULL);
		__this->set_parserInputStack_1(L_0);
		__this->set_nameBuffer_2(((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)((int32_t)256))));
		__this->set_nameLength_3(0);
		__this->set_nameCapacity_4(((int32_t)256));
		StringBuilder_t3822575854 * L_1 = (StringBuilder_t3822575854 *)il2cpp_codegen_object_new(StringBuilder_t3822575854_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3624398269(L_1, ((int32_t)512), /*hidden argument*/NULL);
		__this->set_valueBuffer_5(L_1);
		return;
	}
}
// Mono.Xml.DTDObjectModel System.Xml.DTDReader::GenerateDTDObjectModel()
extern Il2CppClass* StringReader_t2229325051_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlParserInput_t3311178812_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArrayList_t2121638921_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_1_t310611086_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t3216530474_il2cpp_TypeInfo_var;
extern Il2CppClass* DTDEntityDeclaration_t2806387719_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1858383821;
extern const uint32_t DTDReader_GenerateDTDObjectModel_m693103910_MetadataUsageId;
extern "C"  DTDObjectModel_t709926554 * DTDReader_GenerateDTDObjectModel_m693103910 (DTDReader_t4257441119 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDReader_GenerateDTDObjectModel_m693103910_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	XmlParserInput_t3311178812 * V_2 = NULL;
	ArrayList_t2121638921 * V_3 = NULL;
	DTDEntityDeclaration_t2806387719 * V_4 = NULL;
	Il2CppObject* V_5 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Stack_t1623036922 * L_0 = __this->get_parserInputStack_1();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Stack::get_Count() */, L_0);
		V_0 = L_1;
		DTDObjectModel_t709926554 * L_2 = __this->get_DTD_13();
		NullCheck(L_2);
		String_t* L_3 = DTDObjectModel_get_InternalSubset_m746677696(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_00e4;
		}
	}
	{
		DTDObjectModel_t709926554 * L_4 = __this->get_DTD_13();
		NullCheck(L_4);
		String_t* L_5 = DTDObjectModel_get_InternalSubset_m746677696(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		int32_t L_6 = String_get_Length_m2979997331(L_5, /*hidden argument*/NULL);
		if ((((int32_t)L_6) <= ((int32_t)0)))
		{
			goto IL_00e4;
		}
	}
	{
		__this->set_processingInternalSubset_10((bool)1);
		XmlParserInput_t3311178812 * L_7 = __this->get_currentInput_0();
		V_2 = L_7;
		DTDObjectModel_t709926554 * L_8 = __this->get_DTD_13();
		NullCheck(L_8);
		String_t* L_9 = DTDObjectModel_get_InternalSubset_m746677696(L_8, /*hidden argument*/NULL);
		StringReader_t2229325051 * L_10 = (StringReader_t2229325051 *)il2cpp_codegen_object_new(StringReader_t2229325051_il2cpp_TypeInfo_var);
		StringReader__ctor_m1181104909(L_10, L_9, /*hidden argument*/NULL);
		DTDObjectModel_t709926554 * L_11 = __this->get_DTD_13();
		NullCheck(L_11);
		String_t* L_12 = DTDObjectModel_get_BaseURI_m398146012(L_11, /*hidden argument*/NULL);
		int32_t L_13 = __this->get_currentLinkedNodeLineNumber_6();
		int32_t L_14 = __this->get_currentLinkedNodeLinePosition_7();
		XmlParserInput_t3311178812 * L_15 = (XmlParserInput_t3311178812 *)il2cpp_codegen_object_new(XmlParserInput_t3311178812_il2cpp_TypeInfo_var);
		XmlParserInput__ctor_m3860804666(L_15, L_10, L_12, L_13, L_14, /*hidden argument*/NULL);
		__this->set_currentInput_0(L_15);
		XmlParserInput_t3311178812 * L_16 = __this->get_currentInput_0();
		NullCheck(L_16);
		XmlParserInput_set_AllowTextDecl_m3757679343(L_16, (bool)0, /*hidden argument*/NULL);
	}

IL_007e:
	{
		bool L_17 = DTDReader_ProcessDTDSubset_m2078640111(__this, /*hidden argument*/NULL);
		V_1 = L_17;
		int32_t L_18 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_18) == ((uint32_t)(-1)))))
		{
			goto IL_00a8;
		}
	}
	{
		Stack_t1623036922 * L_19 = __this->get_parserInputStack_1();
		NullCheck(L_19);
		int32_t L_20 = VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Stack::get_Count() */, L_19);
		if ((((int32_t)L_20) <= ((int32_t)0)))
		{
			goto IL_00a8;
		}
	}
	{
		DTDReader_PopParserInput_m2654799194(__this, /*hidden argument*/NULL);
	}

IL_00a8:
	{
		bool L_21 = V_1;
		if (L_21)
		{
			goto IL_007e;
		}
	}
	{
		Stack_t1623036922 * L_22 = __this->get_parserInputStack_1();
		NullCheck(L_22);
		int32_t L_23 = VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Stack::get_Count() */, L_22);
		int32_t L_24 = V_0;
		if ((((int32_t)L_23) > ((int32_t)L_24)))
		{
			goto IL_007e;
		}
	}
	{
		int32_t L_25 = __this->get_dtdIncludeSect_8();
		if (!L_25)
		{
			goto IL_00d6;
		}
	}
	{
		XmlException_t3490696160 * L_26 = DTDReader_NotWFError_m440433271(__this, _stringLiteral1858383821, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_26);
	}

IL_00d6:
	{
		XmlParserInput_t3311178812 * L_27 = V_2;
		__this->set_currentInput_0(L_27);
		__this->set_processingInternalSubset_10((bool)0);
	}

IL_00e4:
	{
		DTDObjectModel_t709926554 * L_28 = __this->get_DTD_13();
		NullCheck(L_28);
		String_t* L_29 = DTDObjectModel_get_SystemId_m893340523(L_28, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_018f;
		}
	}
	{
		DTDObjectModel_t709926554 * L_30 = __this->get_DTD_13();
		NullCheck(L_30);
		String_t* L_31 = DTDObjectModel_get_SystemId_m893340523(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_33 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_31, L_32, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_018f;
		}
	}
	{
		DTDObjectModel_t709926554 * L_34 = __this->get_DTD_13();
		NullCheck(L_34);
		XmlResolver_t2502213349 * L_35 = DTDObjectModel_get_Resolver_m1268564320(L_34, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_018f;
		}
	}
	{
		DTDObjectModel_t709926554 * L_36 = __this->get_DTD_13();
		NullCheck(L_36);
		String_t* L_37 = DTDObjectModel_get_SystemId_m893340523(L_36, /*hidden argument*/NULL);
		DTDReader_PushParserInput_m3654728911(__this, L_37, /*hidden argument*/NULL);
	}

IL_012f:
	{
		bool L_38 = DTDReader_ProcessDTDSubset_m2078640111(__this, /*hidden argument*/NULL);
		V_1 = L_38;
		int32_t L_39 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_39) == ((uint32_t)(-1)))))
		{
			goto IL_0159;
		}
	}
	{
		Stack_t1623036922 * L_40 = __this->get_parserInputStack_1();
		NullCheck(L_40);
		int32_t L_41 = VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Stack::get_Count() */, L_40);
		if ((((int32_t)L_41) <= ((int32_t)1)))
		{
			goto IL_0159;
		}
	}
	{
		DTDReader_PopParserInput_m2654799194(__this, /*hidden argument*/NULL);
	}

IL_0159:
	{
		bool L_42 = V_1;
		if (L_42)
		{
			goto IL_012f;
		}
	}
	{
		Stack_t1623036922 * L_43 = __this->get_parserInputStack_1();
		NullCheck(L_43);
		int32_t L_44 = VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Stack::get_Count() */, L_43);
		int32_t L_45 = V_0;
		if ((((int32_t)L_44) > ((int32_t)((int32_t)((int32_t)L_45+(int32_t)1)))))
		{
			goto IL_012f;
		}
	}
	{
		int32_t L_46 = __this->get_dtdIncludeSect_8();
		if (!L_46)
		{
			goto IL_0189;
		}
	}
	{
		XmlException_t3490696160 * L_47 = DTDReader_NotWFError_m440433271(__this, _stringLiteral1858383821, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_47);
	}

IL_0189:
	{
		DTDReader_PopParserInput_m2654799194(__this, /*hidden argument*/NULL);
	}

IL_018f:
	{
		ArrayList_t2121638921 * L_48 = (ArrayList_t2121638921 *)il2cpp_codegen_object_new(ArrayList_t2121638921_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1878432947(L_48, /*hidden argument*/NULL);
		V_3 = L_48;
		DTDObjectModel_t709926554 * L_49 = __this->get_DTD_13();
		NullCheck(L_49);
		DTDEntityDeclarationCollection_t4226410245 * L_50 = DTDObjectModel_get_EntityDecls_m3251787732(L_49, /*hidden argument*/NULL);
		NullCheck(L_50);
		Il2CppObject* L_51 = DictionaryBase_get_Values_m4015003527(L_50, /*hidden argument*/NULL);
		NullCheck(L_51);
		Il2CppObject* L_52 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<Mono.Xml.DTDNode>::GetEnumerator() */, IEnumerable_1_t310611086_il2cpp_TypeInfo_var, L_51);
		V_5 = L_52;
	}

IL_01ac:
	try
	{ // begin try (depth: 1)
		{
			goto IL_01d9;
		}

IL_01b1:
		{
			Il2CppObject* L_53 = V_5;
			NullCheck(L_53);
			DTDNode_t1733424026 * L_54 = InterfaceFuncInvoker0< DTDNode_t1733424026 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<Mono.Xml.DTDNode>::get_Current() */, IEnumerator_1_t3216530474_il2cpp_TypeInfo_var, L_53);
			V_4 = ((DTDEntityDeclaration_t2806387719 *)CastclassClass(L_54, DTDEntityDeclaration_t2806387719_il2cpp_TypeInfo_var));
			DTDEntityDeclaration_t2806387719 * L_55 = V_4;
			NullCheck(L_55);
			String_t* L_56 = DTDEntityDeclaration_get_NotationName_m528268507(L_55, /*hidden argument*/NULL);
			if (!L_56)
			{
				goto IL_01d9;
			}
		}

IL_01cb:
		{
			DTDEntityDeclaration_t2806387719 * L_57 = V_4;
			ArrayList_t2121638921 * L_58 = V_3;
			NullCheck(L_57);
			DTDEntityDeclaration_ScanEntityValue_m3595758284(L_57, L_58, /*hidden argument*/NULL);
			ArrayList_t2121638921 * L_59 = V_3;
			NullCheck(L_59);
			VirtActionInvoker0::Invoke(31 /* System.Void System.Collections.ArrayList::Clear() */, L_59);
		}

IL_01d9:
		{
			Il2CppObject* L_60 = V_5;
			NullCheck(L_60);
			bool L_61 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_60);
			if (L_61)
			{
				goto IL_01b1;
			}
		}

IL_01e5:
		{
			IL2CPP_LEAVE(0x1F7, FINALLY_01ea);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_01ea;
	}

FINALLY_01ea:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_62 = V_5;
			if (L_62)
			{
				goto IL_01ef;
			}
		}

IL_01ee:
		{
			IL2CPP_END_FINALLY(490)
		}

IL_01ef:
		{
			Il2CppObject* L_63 = V_5;
			NullCheck(L_63);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_63);
			IL2CPP_END_FINALLY(490)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(490)
	{
		IL2CPP_JUMP_TBL(0x1F7, IL_01f7)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_01f7:
	{
		DTDObjectModel_t709926554 * L_64 = __this->get_DTD_13();
		NullCheck(L_64);
		Hashtable_t3875263730 * L_65 = DTDObjectModel_get_ExternalResources_m2105766529(L_64, /*hidden argument*/NULL);
		NullCheck(L_65);
		VirtActionInvoker0::Invoke(34 /* System.Void System.Collections.Hashtable::Clear() */, L_65);
		DTDObjectModel_t709926554 * L_66 = __this->get_DTD_13();
		return L_66;
	}
}
// System.Boolean System.Xml.DTDReader::ProcessDTDSubset()
extern Il2CppClass* Char_t2778706699_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4010048345;
extern Il2CppCodeGenString* _stringLiteral486099368;
extern Il2CppCodeGenString* _stringLiteral1134819659;
extern Il2CppCodeGenString* _stringLiteral2945;
extern Il2CppCodeGenString* _stringLiteral3870066541;
extern const uint32_t DTDReader_ProcessDTDSubset_m2078640111_MetadataUsageId;
extern "C"  bool DTDReader_ProcessDTDSubset_m2078640111 (DTDReader_t4257441119 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDReader_ProcessDTDSubset_m2078640111_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	String_t* V_1 = NULL;
	DTDParameterEntityDeclaration_t2256150406 * V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
		int32_t L_0 = DTDReader_ReadChar_m1039540782(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		V_4 = L_1;
		int32_t L_2 = V_4;
		if ((((int32_t)L_2) == ((int32_t)(-1))))
		{
			goto IL_0039;
		}
	}
	{
		int32_t L_3 = V_4;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)37))))
		{
			goto IL_003b;
		}
	}
	{
		int32_t L_4 = V_4;
		if ((((int32_t)L_4) == ((int32_t)((int32_t)60))))
		{
			goto IL_00a8;
		}
	}
	{
		int32_t L_5 = V_4;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)93))))
		{
			goto IL_0110;
		}
	}
	{
		goto IL_014c;
	}

IL_0039:
	{
		return (bool)0;
	}

IL_003b:
	{
		bool L_6 = __this->get_processingInternalSubset_10();
		if (!L_6)
		{
			goto IL_0052;
		}
	}
	{
		DTDObjectModel_t709926554 * L_7 = __this->get_DTD_13();
		NullCheck(L_7);
		DTDObjectModel_set_InternalSubsetHasPEReference_m2420613834(L_7, (bool)1, /*hidden argument*/NULL);
	}

IL_0052:
	{
		String_t* L_8 = DTDReader_ReadName_m4241357028(__this, /*hidden argument*/NULL);
		V_1 = L_8;
		DTDReader_Expect_m1917199594(__this, ((int32_t)59), /*hidden argument*/NULL);
		String_t* L_9 = V_1;
		DTDParameterEntityDeclaration_t2256150406 * L_10 = DTDReader_GetPEDecl_m1992309660(__this, L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		DTDParameterEntityDeclaration_t2256150406 * L_11 = V_2;
		if (L_11)
		{
			goto IL_0074;
		}
	}
	{
		goto IL_016a;
	}

IL_0074:
	{
		XmlParserInput_t3311178812 * L_12 = __this->get_currentInput_0();
		DTDParameterEntityDeclaration_t2256150406 * L_13 = V_2;
		NullCheck(L_12);
		XmlParserInput_PushPEBuffer_m274373084(L_12, L_13, /*hidden argument*/NULL);
		goto IL_008c;
	}

IL_0085:
	{
		DTDReader_ProcessDTDSubset_m2078640111(__this, /*hidden argument*/NULL);
	}

IL_008c:
	{
		XmlParserInput_t3311178812 * L_14 = __this->get_currentInput_0();
		NullCheck(L_14);
		bool L_15 = XmlParserInput_get_HasPEBuffer_m4238977219(L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_0085;
		}
	}
	{
		DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
		goto IL_016a;
	}

IL_00a8:
	{
		int32_t L_16 = DTDReader_ReadChar_m1039540782(__this, /*hidden argument*/NULL);
		V_3 = L_16;
		int32_t L_17 = V_3;
		V_5 = L_17;
		int32_t L_18 = V_5;
		if ((((int32_t)L_18) == ((int32_t)(-1))))
		{
			goto IL_00e7;
		}
	}
	{
		int32_t L_19 = V_5;
		if ((((int32_t)L_19) == ((int32_t)((int32_t)33))))
		{
			goto IL_00dc;
		}
	}
	{
		int32_t L_20 = V_5;
		if ((((int32_t)L_20) == ((int32_t)((int32_t)63))))
		{
			goto IL_00d1;
		}
	}
	{
		goto IL_00f3;
	}

IL_00d1:
	{
		DTDReader_ReadProcessingInstruction_m155897575(__this, /*hidden argument*/NULL);
		goto IL_010b;
	}

IL_00dc:
	{
		DTDReader_CompileDeclaration_m3811200007(__this, /*hidden argument*/NULL);
		goto IL_010b;
	}

IL_00e7:
	{
		XmlException_t3490696160 * L_21 = DTDReader_NotWFError_m440433271(__this, _stringLiteral4010048345, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}

IL_00f3:
	{
		int32_t L_22 = V_3;
		uint16_t L_23 = ((uint16_t)(((int32_t)((uint16_t)L_22))));
		Il2CppObject * L_24 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_23);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_25 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral486099368, L_24, /*hidden argument*/NULL);
		XmlException_t3490696160 * L_26 = DTDReader_NotWFError_m440433271(__this, L_25, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_26);
	}

IL_010b:
	{
		goto IL_016a;
	}

IL_0110:
	{
		int32_t L_27 = __this->get_dtdIncludeSect_8();
		if (L_27)
		{
			goto IL_0127;
		}
	}
	{
		XmlException_t3490696160 * L_28 = DTDReader_NotWFError_m440433271(__this, _stringLiteral1134819659, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_28);
	}

IL_0127:
	{
		DTDReader_Expect_m4055310985(__this, _stringLiteral2945, /*hidden argument*/NULL);
		int32_t L_29 = __this->get_dtdIncludeSect_8();
		__this->set_dtdIncludeSect_8(((int32_t)((int32_t)L_29-(int32_t)1)));
		DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
		goto IL_016a;
	}

IL_014c:
	{
		int32_t L_30 = V_0;
		int32_t L_31 = L_30;
		Il2CppObject * L_32 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_31);
		int32_t L_33 = V_0;
		uint16_t L_34 = ((uint16_t)(((int32_t)((uint16_t)L_33))));
		Il2CppObject * L_35 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_34);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_36 = String_Format_m2398979370(NULL /*static, unused*/, _stringLiteral3870066541, L_32, L_35, /*hidden argument*/NULL);
		XmlException_t3490696160 * L_37 = DTDReader_NotWFError_m440433271(__this, L_36, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_37);
	}

IL_016a:
	{
		XmlParserInput_t3311178812 * L_38 = __this->get_currentInput_0();
		NullCheck(L_38);
		XmlParserInput_set_AllowTextDecl_m3757679343(L_38, (bool)0, /*hidden argument*/NULL);
		return (bool)1;
	}
}
// System.Void System.Xml.DTDReader::CompileDeclaration()
extern Il2CppClass* XmlChar_t3591879093_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2575290;
extern Il2CppCodeGenString* _stringLiteral3770693344;
extern Il2CppCodeGenString* _stringLiteral1487577660;
extern Il2CppCodeGenString* _stringLiteral66085667;
extern Il2CppCodeGenString* _stringLiteral2402286;
extern Il2CppCodeGenString* _stringLiteral2484761374;
extern Il2CppCodeGenString* _stringLiteral3860766736;
extern Il2CppCodeGenString* _stringLiteral64223885;
extern Il2CppCodeGenString* _stringLiteral2402228;
extern Il2CppCodeGenString* _stringLiteral101107768;
extern const uint32_t DTDReader_CompileDeclaration_m3811200007_MetadataUsageId;
extern "C"  void DTDReader_CompileDeclaration_m3811200007 (DTDReader_t4257441119 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDReader_CompileDeclaration_m3811200007_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DTDEntityDeclaration_t2806387719 * V_0 = NULL;
	DTDElementDeclaration_t95357814 * V_1 = NULL;
	DTDAttListDeclaration_t1258293171 * V_2 = NULL;
	DTDNotationDeclaration_t353707912 * V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		int32_t L_0 = DTDReader_ReadChar_m1039540782(__this, /*hidden argument*/NULL);
		V_4 = L_0;
		int32_t L_1 = V_4;
		if ((((int32_t)L_1) == ((int32_t)((int32_t)45))))
		{
			goto IL_003a;
		}
	}
	{
		int32_t L_2 = V_4;
		if ((((int32_t)L_2) == ((int32_t)((int32_t)65))))
		{
			goto IL_016c;
		}
	}
	{
		int32_t L_3 = V_4;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)69))))
		{
			goto IL_004d;
		}
	}
	{
		int32_t L_4 = V_4;
		if ((((int32_t)L_4) == ((int32_t)((int32_t)78))))
		{
			goto IL_019a;
		}
	}
	{
		int32_t L_5 = V_4;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)91))))
		{
			goto IL_01c8;
		}
	}
	{
		goto IL_023e;
	}

IL_003a:
	{
		DTDReader_Expect_m1917199594(__this, ((int32_t)45), /*hidden argument*/NULL);
		DTDReader_ReadComment_m2005393419(__this, /*hidden argument*/NULL);
		goto IL_024a;
	}

IL_004d:
	{
		int32_t L_6 = DTDReader_ReadChar_m1039540782(__this, /*hidden argument*/NULL);
		V_5 = L_6;
		int32_t L_7 = V_5;
		if (((int32_t)((int32_t)L_7-(int32_t)((int32_t)76))) == 0)
		{
			goto IL_012d;
		}
		if (((int32_t)((int32_t)L_7-(int32_t)((int32_t)76))) == 1)
		{
			goto IL_015b;
		}
		if (((int32_t)((int32_t)L_7-(int32_t)((int32_t)76))) == 2)
		{
			goto IL_0070;
		}
	}
	{
		goto IL_015b;
	}

IL_0070:
	{
		DTDReader_Expect_m4055310985(__this, _stringLiteral2575290, /*hidden argument*/NULL);
		bool L_8 = DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0092;
		}
	}
	{
		XmlException_t3490696160 * L_9 = DTDReader_NotWFError_m440433271(__this, _stringLiteral3770693344, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0092:
	{
		int32_t L_10 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_10) == ((uint32_t)((int32_t)37)))))
		{
			goto IL_00ef;
		}
	}
	{
		DTDReader_ReadChar_m1039540782(__this, /*hidden argument*/NULL);
		bool L_11 = DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_00bc;
		}
	}
	{
		DTDReader_ExpandPERef_m4144305510(__this, /*hidden argument*/NULL);
		goto IL_0092;
	}

IL_00bc:
	{
		DTDReader_TryExpandPERef_m232461585(__this, /*hidden argument*/NULL);
		int32_t L_12 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(XmlChar_t3591879093_il2cpp_TypeInfo_var);
		bool L_13 = XmlChar_IsNameChar_m1569088058(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_00de;
		}
	}
	{
		DTDReader_ReadParameterEntityDecl_m2554735394(__this, /*hidden argument*/NULL);
		goto IL_00ea;
	}

IL_00de:
	{
		XmlException_t3490696160 * L_14 = DTDReader_NotWFError_m440433271(__this, _stringLiteral1487577660, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_14);
	}

IL_00ea:
	{
		goto IL_0167;
	}

IL_00ef:
	{
		DTDEntityDeclaration_t2806387719 * L_15 = DTDReader_ReadEntityDecl_m131033009(__this, /*hidden argument*/NULL);
		V_0 = L_15;
		DTDObjectModel_t709926554 * L_16 = __this->get_DTD_13();
		NullCheck(L_16);
		DTDEntityDeclarationCollection_t4226410245 * L_17 = DTDObjectModel_get_EntityDecls_m3251787732(L_16, /*hidden argument*/NULL);
		DTDEntityDeclaration_t2806387719 * L_18 = V_0;
		NullCheck(L_18);
		String_t* L_19 = DTDEntityBase_get_Name_m1635306044(L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		DTDEntityDeclaration_t2806387719 * L_20 = DTDEntityDeclarationCollection_get_Item_m2370831310(L_17, L_19, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_0128;
		}
	}
	{
		DTDObjectModel_t709926554 * L_21 = __this->get_DTD_13();
		NullCheck(L_21);
		DTDEntityDeclarationCollection_t4226410245 * L_22 = DTDObjectModel_get_EntityDecls_m3251787732(L_21, /*hidden argument*/NULL);
		DTDEntityDeclaration_t2806387719 * L_23 = V_0;
		NullCheck(L_23);
		String_t* L_24 = DTDEntityBase_get_Name_m1635306044(L_23, /*hidden argument*/NULL);
		DTDEntityDeclaration_t2806387719 * L_25 = V_0;
		NullCheck(L_22);
		DTDEntityDeclarationCollection_Add_m889264620(L_22, L_24, L_25, /*hidden argument*/NULL);
	}

IL_0128:
	{
		goto IL_0167;
	}

IL_012d:
	{
		DTDReader_Expect_m4055310985(__this, _stringLiteral66085667, /*hidden argument*/NULL);
		DTDElementDeclaration_t95357814 * L_26 = DTDReader_ReadElementDecl_m983303313(__this, /*hidden argument*/NULL);
		V_1 = L_26;
		DTDObjectModel_t709926554 * L_27 = __this->get_DTD_13();
		NullCheck(L_27);
		DTDElementDeclarationCollection_t2951978932 * L_28 = DTDObjectModel_get_ElementDecls_m3999237198(L_27, /*hidden argument*/NULL);
		DTDElementDeclaration_t95357814 * L_29 = V_1;
		NullCheck(L_29);
		String_t* L_30 = DTDElementDeclaration_get_Name_m1111866310(L_29, /*hidden argument*/NULL);
		DTDElementDeclaration_t95357814 * L_31 = V_1;
		NullCheck(L_28);
		DTDElementDeclarationCollection_Add_m2137189962(L_28, L_30, L_31, /*hidden argument*/NULL);
		goto IL_0167;
	}

IL_015b:
	{
		XmlException_t3490696160 * L_32 = DTDReader_NotWFError_m440433271(__this, _stringLiteral2402286, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_32);
	}

IL_0167:
	{
		goto IL_024a;
	}

IL_016c:
	{
		DTDReader_Expect_m4055310985(__this, _stringLiteral2484761374, /*hidden argument*/NULL);
		DTDAttListDeclaration_t1258293171 * L_33 = DTDReader_ReadAttListDecl_m877842801(__this, /*hidden argument*/NULL);
		V_2 = L_33;
		DTDObjectModel_t709926554 * L_34 = __this->get_DTD_13();
		NullCheck(L_34);
		DTDAttListDeclarationCollection_t733492145 * L_35 = DTDObjectModel_get_AttListDecls_m3653137288(L_34, /*hidden argument*/NULL);
		DTDAttListDeclaration_t1258293171 * L_36 = V_2;
		NullCheck(L_36);
		String_t* L_37 = DTDAttListDeclaration_get_Name_m3744810115(L_36, /*hidden argument*/NULL);
		DTDAttListDeclaration_t1258293171 * L_38 = V_2;
		NullCheck(L_35);
		DTDAttListDeclarationCollection_Add_m1772263056(L_35, L_37, L_38, /*hidden argument*/NULL);
		goto IL_024a;
	}

IL_019a:
	{
		DTDReader_Expect_m4055310985(__this, _stringLiteral3860766736, /*hidden argument*/NULL);
		DTDNotationDeclaration_t353707912 * L_39 = DTDReader_ReadNotationDecl_m2258323887(__this, /*hidden argument*/NULL);
		V_3 = L_39;
		DTDObjectModel_t709926554 * L_40 = __this->get_DTD_13();
		NullCheck(L_40);
		DTDNotationDeclarationCollection_t3011202374 * L_41 = DTDObjectModel_get_NotationDecls_m2487965492(L_40, /*hidden argument*/NULL);
		DTDNotationDeclaration_t353707912 * L_42 = V_3;
		NullCheck(L_42);
		String_t* L_43 = DTDNotationDeclaration_get_Name_m1124239610(L_42, /*hidden argument*/NULL);
		DTDNotationDeclaration_t353707912 * L_44 = V_3;
		NullCheck(L_41);
		DTDNotationDeclarationCollection_Add_m754289612(L_41, L_43, L_44, /*hidden argument*/NULL);
		goto IL_024a;
	}

IL_01c8:
	{
		DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
		DTDReader_TryExpandPERef_m232461585(__this, /*hidden argument*/NULL);
		DTDReader_Expect_m1917199594(__this, ((int32_t)73), /*hidden argument*/NULL);
		int32_t L_45 = DTDReader_ReadChar_m1039540782(__this, /*hidden argument*/NULL);
		V_5 = L_45;
		int32_t L_46 = V_5;
		if ((((int32_t)L_46) == ((int32_t)((int32_t)71))))
		{
			goto IL_0223;
		}
	}
	{
		int32_t L_47 = V_5;
		if ((((int32_t)L_47) == ((int32_t)((int32_t)78))))
		{
			goto IL_01fd;
		}
	}
	{
		goto IL_0239;
	}

IL_01fd:
	{
		DTDReader_Expect_m4055310985(__this, _stringLiteral64223885, /*hidden argument*/NULL);
		DTDReader_ExpectAfterWhitespace_m2772157595(__this, ((int32_t)91), /*hidden argument*/NULL);
		int32_t L_48 = __this->get_dtdIncludeSect_8();
		__this->set_dtdIncludeSect_8(((int32_t)((int32_t)L_48+(int32_t)1)));
		goto IL_0239;
	}

IL_0223:
	{
		DTDReader_Expect_m4055310985(__this, _stringLiteral2402228, /*hidden argument*/NULL);
		DTDReader_ReadIgnoreSect_m3326521003(__this, /*hidden argument*/NULL);
		goto IL_0239;
	}

IL_0239:
	{
		goto IL_024a;
	}

IL_023e:
	{
		XmlException_t3490696160 * L_49 = DTDReader_NotWFError_m440433271(__this, _stringLiteral101107768, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_49);
	}

IL_024a:
	{
		return;
	}
}
// System.Void System.Xml.DTDReader::ReadIgnoreSect()
extern Il2CppCodeGenString* _stringLiteral3432849517;
extern Il2CppCodeGenString* _stringLiteral1554454807;
extern const uint32_t DTDReader_ReadIgnoreSect_m3326521003_MetadataUsageId;
extern "C"  void DTDReader_ReadIgnoreSect_m3326521003 (DTDReader_t4257441119 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDReader_ReadIgnoreSect_m3326521003_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		DTDReader_ExpectAfterWhitespace_m2772157595(__this, ((int32_t)91), /*hidden argument*/NULL);
		V_0 = 1;
		goto IL_00b4;
	}

IL_000f:
	{
		int32_t L_0 = DTDReader_ReadChar_m1039540782(__this, /*hidden argument*/NULL);
		V_1 = L_0;
		int32_t L_1 = V_1;
		if ((((int32_t)L_1) == ((int32_t)(-1))))
		{
			goto IL_0032;
		}
	}
	{
		int32_t L_2 = V_1;
		if ((((int32_t)L_2) == ((int32_t)((int32_t)60))))
		{
			goto IL_003e;
		}
	}
	{
		int32_t L_3 = V_1;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)93))))
		{
			goto IL_0079;
		}
	}
	{
		goto IL_00b4;
	}

IL_0032:
	{
		XmlException_t3490696160 * L_4 = DTDReader_NotWFError_m440433271(__this, _stringLiteral3432849517, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_003e:
	{
		int32_t L_5 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_5) == ((int32_t)((int32_t)33))))
		{
			goto IL_0050;
		}
	}
	{
		goto IL_00b4;
	}

IL_0050:
	{
		DTDReader_ReadChar_m1039540782(__this, /*hidden argument*/NULL);
		int32_t L_6 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_6) == ((int32_t)((int32_t)91))))
		{
			goto IL_0069;
		}
	}
	{
		goto IL_00b4;
	}

IL_0069:
	{
		DTDReader_ReadChar_m1039540782(__this, /*hidden argument*/NULL);
		int32_t L_7 = V_0;
		V_0 = ((int32_t)((int32_t)L_7+(int32_t)1));
		goto IL_00b4;
	}

IL_0079:
	{
		int32_t L_8 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_8) == ((int32_t)((int32_t)93))))
		{
			goto IL_008b;
		}
	}
	{
		goto IL_00b4;
	}

IL_008b:
	{
		DTDReader_ReadChar_m1039540782(__this, /*hidden argument*/NULL);
		int32_t L_9 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_9) == ((int32_t)((int32_t)62))))
		{
			goto IL_00a4;
		}
	}
	{
		goto IL_00b4;
	}

IL_00a4:
	{
		DTDReader_ReadChar_m1039540782(__this, /*hidden argument*/NULL);
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10-(int32_t)1));
		goto IL_00b4;
	}

IL_00b4:
	{
		int32_t L_11 = V_0;
		if ((((int32_t)L_11) > ((int32_t)0)))
		{
			goto IL_000f;
		}
	}
	{
		int32_t L_12 = V_0;
		if (!L_12)
		{
			goto IL_00cd;
		}
	}
	{
		XmlException_t3490696160 * L_13 = DTDReader_NotWFError_m440433271(__this, _stringLiteral1554454807, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_13);
	}

IL_00cd:
	{
		return;
	}
}
// Mono.Xml.DTDElementDeclaration System.Xml.DTDReader::ReadElementDecl()
extern Il2CppClass* DTDElementDeclaration_t95357814_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1107990466;
extern Il2CppCodeGenString* _stringLiteral2922080320;
extern const uint32_t DTDReader_ReadElementDecl_m983303313_MetadataUsageId;
extern "C"  DTDElementDeclaration_t95357814 * DTDReader_ReadElementDecl_m983303313 (DTDReader_t4257441119 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDReader_ReadElementDecl_m983303313_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DTDElementDeclaration_t95357814 * V_0 = NULL;
	{
		DTDObjectModel_t709926554 * L_0 = __this->get_DTD_13();
		DTDElementDeclaration_t95357814 * L_1 = (DTDElementDeclaration_t95357814 *)il2cpp_codegen_object_new(DTDElementDeclaration_t95357814_il2cpp_TypeInfo_var);
		DTDElementDeclaration__ctor_m3951886277(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		DTDElementDeclaration_t95357814 * L_2 = V_0;
		bool L_3 = __this->get_processingInternalSubset_10();
		NullCheck(L_2);
		DTDNode_set_IsInternalSubset_m3109582768(L_2, L_3, /*hidden argument*/NULL);
		bool L_4 = DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_002f;
		}
	}
	{
		XmlException_t3490696160 * L_5 = DTDReader_NotWFError_m440433271(__this, _stringLiteral1107990466, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_002f:
	{
		DTDReader_TryExpandPERef_m232461585(__this, /*hidden argument*/NULL);
		DTDElementDeclaration_t95357814 * L_6 = V_0;
		String_t* L_7 = DTDReader_ReadName_m4241357028(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		DTDElementDeclaration_set_Name_m3252098059(L_6, L_7, /*hidden argument*/NULL);
		bool L_8 = DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0059;
		}
	}
	{
		XmlException_t3490696160 * L_9 = DTDReader_NotWFError_m440433271(__this, _stringLiteral2922080320, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0059:
	{
		DTDReader_TryExpandPERef_m232461585(__this, /*hidden argument*/NULL);
		DTDElementDeclaration_t95357814 * L_10 = V_0;
		DTDReader_ReadContentSpec_m704165552(__this, L_10, /*hidden argument*/NULL);
		DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
		DTDReader_TryExpandPERef_m232461585(__this, /*hidden argument*/NULL);
		DTDReader_Expect_m1917199594(__this, ((int32_t)62), /*hidden argument*/NULL);
		DTDElementDeclaration_t95357814 * L_11 = V_0;
		return L_11;
	}
}
// System.Void System.Xml.DTDReader::ReadContentSpec(Mono.Xml.DTDElementDeclaration)
extern Il2CppClass* DTDContentModel_t1462496200_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2373480;
extern Il2CppCodeGenString* _stringLiteral2507;
extern Il2CppCodeGenString* _stringLiteral3352156672;
extern Il2CppCodeGenString* _stringLiteral4125296187;
extern Il2CppCodeGenString* _stringLiteral548941435;
extern Il2CppCodeGenString* _stringLiteral385535826;
extern const uint32_t DTDReader_ReadContentSpec_m704165552_MetadataUsageId;
extern "C"  void DTDReader_ReadContentSpec_m704165552 (DTDReader_t4257441119 * __this, DTDElementDeclaration_t95357814 * ___decl0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDReader_ReadContentSpec_m704165552_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DTDContentModel_t1462496200 * V_0 = NULL;
	DTDContentModel_t1462496200 * V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		DTDReader_TryExpandPERef_m232461585(__this, /*hidden argument*/NULL);
		int32_t L_0 = DTDReader_ReadChar_m1039540782(__this, /*hidden argument*/NULL);
		V_2 = L_0;
		int32_t L_1 = V_2;
		if ((((int32_t)L_1) == ((int32_t)((int32_t)40))))
		{
			goto IL_0059;
		}
	}
	{
		int32_t L_2 = V_2;
		if ((((int32_t)L_2) == ((int32_t)((int32_t)65))))
		{
			goto IL_0042;
		}
	}
	{
		int32_t L_3 = V_2;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)69))))
		{
			goto IL_002b;
		}
	}
	{
		goto IL_02cf;
	}

IL_002b:
	{
		DTDElementDeclaration_t95357814 * L_4 = ___decl0;
		NullCheck(L_4);
		DTDElementDeclaration_set_IsEmpty_m370233552(L_4, (bool)1, /*hidden argument*/NULL);
		DTDReader_Expect_m4055310985(__this, _stringLiteral2373480, /*hidden argument*/NULL);
		goto IL_02db;
	}

IL_0042:
	{
		DTDElementDeclaration_t95357814 * L_5 = ___decl0;
		NullCheck(L_5);
		DTDElementDeclaration_set_IsAny_m2296438319(L_5, (bool)1, /*hidden argument*/NULL);
		DTDReader_Expect_m4055310985(__this, _stringLiteral2507, /*hidden argument*/NULL);
		goto IL_02db;
	}

IL_0059:
	{
		DTDElementDeclaration_t95357814 * L_6 = ___decl0;
		NullCheck(L_6);
		DTDContentModel_t1462496200 * L_7 = DTDElementDeclaration_get_ContentModel_m3021018039(L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
		DTDReader_TryExpandPERef_m232461585(__this, /*hidden argument*/NULL);
		int32_t L_8 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_8) == ((uint32_t)((int32_t)35)))))
		{
			goto IL_016a;
		}
	}
	{
		DTDElementDeclaration_t95357814 * L_9 = ___decl0;
		NullCheck(L_9);
		DTDElementDeclaration_set_IsMixedContent_m3652418955(L_9, (bool)1, /*hidden argument*/NULL);
		DTDContentModel_t1462496200 * L_10 = V_0;
		NullCheck(L_10);
		DTDContentModel_set_Occurence_m1938639474(L_10, 2, /*hidden argument*/NULL);
		DTDContentModel_t1462496200 * L_11 = V_0;
		NullCheck(L_11);
		DTDContentModel_set_OrderType_m2020937195(L_11, 2, /*hidden argument*/NULL);
		DTDReader_Expect_m4055310985(__this, _stringLiteral3352156672, /*hidden argument*/NULL);
		DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
		DTDReader_TryExpandPERef_m232461585(__this, /*hidden argument*/NULL);
		goto IL_011d;
	}

IL_00ae:
	{
		DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
		int32_t L_12 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_12) == ((uint32_t)((int32_t)37)))))
		{
			goto IL_00ce;
		}
	}
	{
		DTDReader_TryExpandPERef_m232461585(__this, /*hidden argument*/NULL);
		goto IL_011d;
	}

IL_00ce:
	{
		DTDReader_Expect_m1917199594(__this, ((int32_t)124), /*hidden argument*/NULL);
		DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
		DTDReader_TryExpandPERef_m232461585(__this, /*hidden argument*/NULL);
		DTDObjectModel_t709926554 * L_13 = __this->get_DTD_13();
		DTDElementDeclaration_t95357814 * L_14 = ___decl0;
		NullCheck(L_14);
		String_t* L_15 = DTDElementDeclaration_get_Name_m1111866310(L_14, /*hidden argument*/NULL);
		DTDContentModel_t1462496200 * L_16 = (DTDContentModel_t1462496200 *)il2cpp_codegen_object_new(DTDContentModel_t1462496200_il2cpp_TypeInfo_var);
		DTDContentModel__ctor_m248936723(L_16, L_13, L_15, /*hidden argument*/NULL);
		V_1 = L_16;
		DTDContentModel_t1462496200 * L_17 = V_1;
		String_t* L_18 = DTDReader_ReadName_m4241357028(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		DTDContentModel_set_ElementName_m4279929463(L_17, L_18, /*hidden argument*/NULL);
		DTDContentModel_t1462496200 * L_19 = V_0;
		NullCheck(L_19);
		DTDContentModelCollection_t605667718 * L_20 = DTDContentModel_get_ChildModels_m1642723597(L_19, /*hidden argument*/NULL);
		DTDContentModel_t1462496200 * L_21 = V_1;
		DTDReader_AddContentModel_m2885863633(__this, L_20, L_21, /*hidden argument*/NULL);
		DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
		DTDReader_TryExpandPERef_m232461585(__this, /*hidden argument*/NULL);
	}

IL_011d:
	{
		int32_t L_22 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_22) == ((uint32_t)((int32_t)41)))))
		{
			goto IL_00ae;
		}
	}
	{
		DTDReader_Expect_m1917199594(__this, ((int32_t)41), /*hidden argument*/NULL);
		DTDContentModel_t1462496200 * L_23 = V_0;
		NullCheck(L_23);
		DTDContentModelCollection_t605667718 * L_24 = DTDContentModel_get_ChildModels_m1642723597(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		int32_t L_25 = DTDContentModelCollection_get_Count_m1800755431(L_24, /*hidden argument*/NULL);
		if ((((int32_t)L_25) <= ((int32_t)0)))
		{
			goto IL_0150;
		}
	}
	{
		DTDReader_Expect_m1917199594(__this, ((int32_t)42), /*hidden argument*/NULL);
		goto IL_0165;
	}

IL_0150:
	{
		int32_t L_26 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_26) == ((uint32_t)((int32_t)42)))))
		{
			goto IL_0165;
		}
	}
	{
		DTDReader_Expect_m1917199594(__this, ((int32_t)42), /*hidden argument*/NULL);
	}

IL_0165:
	{
		goto IL_02c3;
	}

IL_016a:
	{
		DTDContentModel_t1462496200 * L_27 = V_0;
		NullCheck(L_27);
		DTDContentModelCollection_t605667718 * L_28 = DTDContentModel_get_ChildModels_m1642723597(L_27, /*hidden argument*/NULL);
		DTDElementDeclaration_t95357814 * L_29 = ___decl0;
		DTDContentModel_t1462496200 * L_30 = DTDReader_ReadCP_m3931839010(__this, L_29, /*hidden argument*/NULL);
		NullCheck(L_28);
		DTDContentModelCollection_Add_m3413500450(L_28, L_30, /*hidden argument*/NULL);
		DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
	}

IL_0183:
	{
		int32_t L_31 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_31) == ((uint32_t)((int32_t)37)))))
		{
			goto IL_019c;
		}
	}
	{
		DTDReader_TryExpandPERef_m232461585(__this, /*hidden argument*/NULL);
		goto IL_0252;
	}

IL_019c:
	{
		int32_t L_32 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_32) == ((uint32_t)((int32_t)124)))))
		{
			goto IL_01f5;
		}
	}
	{
		DTDContentModel_t1462496200 * L_33 = V_0;
		NullCheck(L_33);
		int32_t L_34 = DTDContentModel_get_OrderType_m2382989904(L_33, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_34) == ((uint32_t)1))))
		{
			goto IL_01c1;
		}
	}
	{
		XmlException_t3490696160 * L_35 = DTDReader_NotWFError_m440433271(__this, _stringLiteral4125296187, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_35);
	}

IL_01c1:
	{
		DTDContentModel_t1462496200 * L_36 = V_0;
		NullCheck(L_36);
		DTDContentModel_set_OrderType_m2020937195(L_36, 2, /*hidden argument*/NULL);
		DTDReader_ReadChar_m1039540782(__this, /*hidden argument*/NULL);
		DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
		DTDContentModel_t1462496200 * L_37 = V_0;
		NullCheck(L_37);
		DTDContentModelCollection_t605667718 * L_38 = DTDContentModel_get_ChildModels_m1642723597(L_37, /*hidden argument*/NULL);
		DTDElementDeclaration_t95357814 * L_39 = ___decl0;
		DTDContentModel_t1462496200 * L_40 = DTDReader_ReadCP_m3931839010(__this, L_39, /*hidden argument*/NULL);
		DTDReader_AddContentModel_m2885863633(__this, L_38, L_40, /*hidden argument*/NULL);
		DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
		goto IL_0252;
	}

IL_01f5:
	{
		int32_t L_41 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_41) == ((uint32_t)((int32_t)44)))))
		{
			goto IL_024d;
		}
	}
	{
		DTDContentModel_t1462496200 * L_42 = V_0;
		NullCheck(L_42);
		int32_t L_43 = DTDContentModel_get_OrderType_m2382989904(L_42, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_43) == ((uint32_t)2))))
		{
			goto IL_021a;
		}
	}
	{
		XmlException_t3490696160 * L_44 = DTDReader_NotWFError_m440433271(__this, _stringLiteral548941435, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_44);
	}

IL_021a:
	{
		DTDContentModel_t1462496200 * L_45 = V_0;
		NullCheck(L_45);
		DTDContentModel_set_OrderType_m2020937195(L_45, 1, /*hidden argument*/NULL);
		DTDReader_ReadChar_m1039540782(__this, /*hidden argument*/NULL);
		DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
		DTDContentModel_t1462496200 * L_46 = V_0;
		NullCheck(L_46);
		DTDContentModelCollection_t605667718 * L_47 = DTDContentModel_get_ChildModels_m1642723597(L_46, /*hidden argument*/NULL);
		DTDElementDeclaration_t95357814 * L_48 = ___decl0;
		DTDContentModel_t1462496200 * L_49 = DTDReader_ReadCP_m3931839010(__this, L_48, /*hidden argument*/NULL);
		NullCheck(L_47);
		DTDContentModelCollection_Add_m3413500450(L_47, L_49, /*hidden argument*/NULL);
		DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
		goto IL_0252;
	}

IL_024d:
	{
		goto IL_0257;
	}

IL_0252:
	{
		goto IL_0183;
	}

IL_0257:
	{
		DTDReader_Expect_m1917199594(__this, ((int32_t)41), /*hidden argument*/NULL);
		int32_t L_50 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		V_3 = L_50;
		int32_t L_51 = V_3;
		if ((((int32_t)L_51) == ((int32_t)((int32_t)42))))
		{
			goto IL_0296;
		}
	}
	{
		int32_t L_52 = V_3;
		if ((((int32_t)L_52) == ((int32_t)((int32_t)43))))
		{
			goto IL_02a9;
		}
	}
	{
		int32_t L_53 = V_3;
		if ((((int32_t)L_53) == ((int32_t)((int32_t)63))))
		{
			goto IL_0283;
		}
	}
	{
		goto IL_02bc;
	}

IL_0283:
	{
		DTDContentModel_t1462496200 * L_54 = V_0;
		NullCheck(L_54);
		DTDContentModel_set_Occurence_m1938639474(L_54, 1, /*hidden argument*/NULL);
		DTDReader_ReadChar_m1039540782(__this, /*hidden argument*/NULL);
		goto IL_02bc;
	}

IL_0296:
	{
		DTDContentModel_t1462496200 * L_55 = V_0;
		NullCheck(L_55);
		DTDContentModel_set_Occurence_m1938639474(L_55, 2, /*hidden argument*/NULL);
		DTDReader_ReadChar_m1039540782(__this, /*hidden argument*/NULL);
		goto IL_02bc;
	}

IL_02a9:
	{
		DTDContentModel_t1462496200 * L_56 = V_0;
		NullCheck(L_56);
		DTDContentModel_set_Occurence_m1938639474(L_56, 3, /*hidden argument*/NULL);
		DTDReader_ReadChar_m1039540782(__this, /*hidden argument*/NULL);
		goto IL_02bc;
	}

IL_02bc:
	{
		DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
	}

IL_02c3:
	{
		DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
		goto IL_02db;
	}

IL_02cf:
	{
		XmlException_t3490696160 * L_57 = DTDReader_NotWFError_m440433271(__this, _stringLiteral385535826, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_57);
	}

IL_02db:
	{
		return;
	}
}
// Mono.Xml.DTDContentModel System.Xml.DTDReader::ReadCP(Mono.Xml.DTDElementDeclaration)
extern Il2CppClass* DTDContentModel_t1462496200_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4125296187;
extern Il2CppCodeGenString* _stringLiteral548941435;
extern const uint32_t DTDReader_ReadCP_m3931839010_MetadataUsageId;
extern "C"  DTDContentModel_t1462496200 * DTDReader_ReadCP_m3931839010 (DTDReader_t4257441119 * __this, DTDElementDeclaration_t95357814 * ___elem0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDReader_ReadCP_m3931839010_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DTDContentModel_t1462496200 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		V_0 = (DTDContentModel_t1462496200 *)NULL;
		DTDReader_TryExpandPERef_m232461585(__this, /*hidden argument*/NULL);
		int32_t L_0 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)40)))))
		{
			goto IL_0130;
		}
	}
	{
		DTDObjectModel_t709926554 * L_1 = __this->get_DTD_13();
		DTDElementDeclaration_t95357814 * L_2 = ___elem0;
		NullCheck(L_2);
		String_t* L_3 = DTDElementDeclaration_get_Name_m1111866310(L_2, /*hidden argument*/NULL);
		DTDContentModel_t1462496200 * L_4 = (DTDContentModel_t1462496200 *)il2cpp_codegen_object_new(DTDContentModel_t1462496200_il2cpp_TypeInfo_var);
		DTDContentModel__ctor_m248936723(L_4, L_1, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		DTDReader_ReadChar_m1039540782(__this, /*hidden argument*/NULL);
		DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
		DTDContentModel_t1462496200 * L_5 = V_0;
		NullCheck(L_5);
		DTDContentModelCollection_t605667718 * L_6 = DTDContentModel_get_ChildModels_m1642723597(L_5, /*hidden argument*/NULL);
		DTDElementDeclaration_t95357814 * L_7 = ___elem0;
		DTDContentModel_t1462496200 * L_8 = DTDReader_ReadCP_m3931839010(__this, L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		DTDContentModelCollection_Add_m3413500450(L_6, L_8, /*hidden argument*/NULL);
		DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
	}

IL_004f:
	{
		int32_t L_9 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)37)))))
		{
			goto IL_0068;
		}
	}
	{
		DTDReader_TryExpandPERef_m232461585(__this, /*hidden argument*/NULL);
		goto IL_011e;
	}

IL_0068:
	{
		int32_t L_10 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_10) == ((uint32_t)((int32_t)124)))))
		{
			goto IL_00c1;
		}
	}
	{
		DTDContentModel_t1462496200 * L_11 = V_0;
		NullCheck(L_11);
		int32_t L_12 = DTDContentModel_get_OrderType_m2382989904(L_11, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_12) == ((uint32_t)1))))
		{
			goto IL_008d;
		}
	}
	{
		XmlException_t3490696160 * L_13 = DTDReader_NotWFError_m440433271(__this, _stringLiteral4125296187, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_13);
	}

IL_008d:
	{
		DTDContentModel_t1462496200 * L_14 = V_0;
		NullCheck(L_14);
		DTDContentModel_set_OrderType_m2020937195(L_14, 2, /*hidden argument*/NULL);
		DTDReader_ReadChar_m1039540782(__this, /*hidden argument*/NULL);
		DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
		DTDContentModel_t1462496200 * L_15 = V_0;
		NullCheck(L_15);
		DTDContentModelCollection_t605667718 * L_16 = DTDContentModel_get_ChildModels_m1642723597(L_15, /*hidden argument*/NULL);
		DTDElementDeclaration_t95357814 * L_17 = ___elem0;
		DTDContentModel_t1462496200 * L_18 = DTDReader_ReadCP_m3931839010(__this, L_17, /*hidden argument*/NULL);
		DTDReader_AddContentModel_m2885863633(__this, L_16, L_18, /*hidden argument*/NULL);
		DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
		goto IL_011e;
	}

IL_00c1:
	{
		int32_t L_19 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_19) == ((uint32_t)((int32_t)44)))))
		{
			goto IL_0119;
		}
	}
	{
		DTDContentModel_t1462496200 * L_20 = V_0;
		NullCheck(L_20);
		int32_t L_21 = DTDContentModel_get_OrderType_m2382989904(L_20, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_21) == ((uint32_t)2))))
		{
			goto IL_00e6;
		}
	}
	{
		XmlException_t3490696160 * L_22 = DTDReader_NotWFError_m440433271(__this, _stringLiteral548941435, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_22);
	}

IL_00e6:
	{
		DTDContentModel_t1462496200 * L_23 = V_0;
		NullCheck(L_23);
		DTDContentModel_set_OrderType_m2020937195(L_23, 1, /*hidden argument*/NULL);
		DTDReader_ReadChar_m1039540782(__this, /*hidden argument*/NULL);
		DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
		DTDContentModel_t1462496200 * L_24 = V_0;
		NullCheck(L_24);
		DTDContentModelCollection_t605667718 * L_25 = DTDContentModel_get_ChildModels_m1642723597(L_24, /*hidden argument*/NULL);
		DTDElementDeclaration_t95357814 * L_26 = ___elem0;
		DTDContentModel_t1462496200 * L_27 = DTDReader_ReadCP_m3931839010(__this, L_26, /*hidden argument*/NULL);
		NullCheck(L_25);
		DTDContentModelCollection_Add_m3413500450(L_25, L_27, /*hidden argument*/NULL);
		DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
		goto IL_011e;
	}

IL_0119:
	{
		goto IL_0123;
	}

IL_011e:
	{
		goto IL_004f;
	}

IL_0123:
	{
		DTDReader_ExpectAfterWhitespace_m2772157595(__this, ((int32_t)41), /*hidden argument*/NULL);
		goto IL_0155;
	}

IL_0130:
	{
		DTDReader_TryExpandPERef_m232461585(__this, /*hidden argument*/NULL);
		DTDObjectModel_t709926554 * L_28 = __this->get_DTD_13();
		DTDElementDeclaration_t95357814 * L_29 = ___elem0;
		NullCheck(L_29);
		String_t* L_30 = DTDElementDeclaration_get_Name_m1111866310(L_29, /*hidden argument*/NULL);
		DTDContentModel_t1462496200 * L_31 = (DTDContentModel_t1462496200 *)il2cpp_codegen_object_new(DTDContentModel_t1462496200_il2cpp_TypeInfo_var);
		DTDContentModel__ctor_m248936723(L_31, L_28, L_30, /*hidden argument*/NULL);
		V_0 = L_31;
		DTDContentModel_t1462496200 * L_32 = V_0;
		String_t* L_33 = DTDReader_ReadName_m4241357028(__this, /*hidden argument*/NULL);
		NullCheck(L_32);
		DTDContentModel_set_ElementName_m4279929463(L_32, L_33, /*hidden argument*/NULL);
	}

IL_0155:
	{
		int32_t L_34 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		V_1 = L_34;
		int32_t L_35 = V_1;
		if ((((int32_t)L_35) == ((int32_t)((int32_t)42))))
		{
			goto IL_018c;
		}
	}
	{
		int32_t L_36 = V_1;
		if ((((int32_t)L_36) == ((int32_t)((int32_t)43))))
		{
			goto IL_019f;
		}
	}
	{
		int32_t L_37 = V_1;
		if ((((int32_t)L_37) == ((int32_t)((int32_t)63))))
		{
			goto IL_0179;
		}
	}
	{
		goto IL_01b2;
	}

IL_0179:
	{
		DTDContentModel_t1462496200 * L_38 = V_0;
		NullCheck(L_38);
		DTDContentModel_set_Occurence_m1938639474(L_38, 1, /*hidden argument*/NULL);
		DTDReader_ReadChar_m1039540782(__this, /*hidden argument*/NULL);
		goto IL_01b2;
	}

IL_018c:
	{
		DTDContentModel_t1462496200 * L_39 = V_0;
		NullCheck(L_39);
		DTDContentModel_set_Occurence_m1938639474(L_39, 2, /*hidden argument*/NULL);
		DTDReader_ReadChar_m1039540782(__this, /*hidden argument*/NULL);
		goto IL_01b2;
	}

IL_019f:
	{
		DTDContentModel_t1462496200 * L_40 = V_0;
		NullCheck(L_40);
		DTDContentModel_set_Occurence_m1938639474(L_40, 3, /*hidden argument*/NULL);
		DTDReader_ReadChar_m1039540782(__this, /*hidden argument*/NULL);
		goto IL_01b2;
	}

IL_01b2:
	{
		DTDContentModel_t1462496200 * L_41 = V_0;
		return L_41;
	}
}
// System.Void System.Xml.DTDReader::AddContentModel(Mono.Xml.DTDContentModelCollection,Mono.Xml.DTDContentModel)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlException_t3490696160_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3883849271;
extern const uint32_t DTDReader_AddContentModel_m2885863633_MetadataUsageId;
extern "C"  void DTDReader_AddContentModel_m2885863633 (DTDReader_t4257441119 * __this, DTDContentModelCollection_t605667718 * ___cmc0, DTDContentModel_t1462496200 * ___cm1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDReader_AddContentModel_m2885863633_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		DTDContentModel_t1462496200 * L_0 = ___cm1;
		NullCheck(L_0);
		String_t* L_1 = DTDContentModel_get_ElementName_m461002204(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0063;
		}
	}
	{
		V_0 = 0;
		goto IL_0057;
	}

IL_0012:
	{
		DTDContentModelCollection_t605667718 * L_2 = ___cmc0;
		int32_t L_3 = V_0;
		NullCheck(L_2);
		DTDContentModel_t1462496200 * L_4 = DTDContentModelCollection_get_Item_m3127043259(L_2, L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		String_t* L_5 = DTDContentModel_get_ElementName_m461002204(L_4, /*hidden argument*/NULL);
		DTDContentModel_t1462496200 * L_6 = ___cm1;
		NullCheck(L_6);
		String_t* L_7 = DTDContentModel_get_ElementName_m461002204(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_8 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_5, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0053;
		}
	}
	{
		int32_t L_9 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Xml.DTDReader::get_LineNumber() */, __this);
		int32_t L_10 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 System.Xml.DTDReader::get_LinePosition() */, __this);
		String_t* L_11 = DTDReader_get_BaseURI_m1245597521(__this, /*hidden argument*/NULL);
		XmlException_t3490696160 * L_12 = (XmlException_t3490696160 *)il2cpp_codegen_object_new(XmlException_t3490696160_il2cpp_TypeInfo_var);
		XmlException__ctor_m2245560573(L_12, _stringLiteral3883849271, L_9, L_10, NULL, L_11, (Exception_t1967233988 *)NULL, /*hidden argument*/NULL);
		DTDReader_HandleError_m2636292706(__this, L_12, /*hidden argument*/NULL);
		return;
	}

IL_0053:
	{
		int32_t L_13 = V_0;
		V_0 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0057:
	{
		int32_t L_14 = V_0;
		DTDContentModelCollection_t605667718 * L_15 = ___cmc0;
		NullCheck(L_15);
		int32_t L_16 = DTDContentModelCollection_get_Count_m1800755431(L_15, /*hidden argument*/NULL);
		if ((((int32_t)L_14) < ((int32_t)L_16)))
		{
			goto IL_0012;
		}
	}

IL_0063:
	{
		DTDContentModelCollection_t605667718 * L_17 = ___cmc0;
		DTDContentModel_t1462496200 * L_18 = ___cm1;
		NullCheck(L_17);
		DTDContentModelCollection_Add_m3413500450(L_17, L_18, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.DTDReader::ReadParameterEntityDecl()
extern Il2CppClass* DTDParameterEntityDeclaration_t2256150406_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlChar_t3591879093_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2343494230;
extern Il2CppCodeGenString* _stringLiteral3926763465;
extern Il2CppCodeGenString* _stringLiteral174159455;
extern Il2CppCodeGenString* _stringLiteral3890344584;
extern const uint32_t DTDReader_ReadParameterEntityDecl_m2554735394_MetadataUsageId;
extern "C"  void DTDReader_ReadParameterEntityDecl_m2554735394 (DTDReader_t4257441119 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDReader_ReadParameterEntityDecl_m2554735394_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DTDParameterEntityDeclaration_t2256150406 * V_0 = NULL;
	int32_t V_1 = 0;
	bool V_2 = false;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		DTDObjectModel_t709926554 * L_0 = __this->get_DTD_13();
		DTDParameterEntityDeclaration_t2256150406 * L_1 = (DTDParameterEntityDeclaration_t2256150406 *)il2cpp_codegen_object_new(DTDParameterEntityDeclaration_t2256150406_il2cpp_TypeInfo_var);
		DTDParameterEntityDeclaration__ctor_m3796965333(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		DTDParameterEntityDeclaration_t2256150406 * L_2 = V_0;
		String_t* L_3 = DTDReader_get_BaseURI_m1245597521(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(7 /* System.Void Mono.Xml.DTDNode::set_BaseURI(System.String) */, L_2, L_3);
		DTDParameterEntityDeclaration_t2256150406 * L_4 = V_0;
		DTDObjectModel_t709926554 * L_5 = __this->get_DTD_13();
		NullCheck(L_5);
		XmlResolver_t2502213349 * L_6 = DTDObjectModel_get_Resolver_m1268564320(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		DTDEntityBase_set_XmlResolver_m2788162230(L_4, L_6, /*hidden argument*/NULL);
		DTDParameterEntityDeclaration_t2256150406 * L_7 = V_0;
		String_t* L_8 = DTDReader_ReadName_m4241357028(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		DTDEntityBase_set_Name_m1787521109(L_7, L_8, /*hidden argument*/NULL);
		bool L_9 = DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_004c;
		}
	}
	{
		XmlException_t3490696160 * L_10 = DTDReader_NotWFError_m440433271(__this, _stringLiteral2343494230, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
	}

IL_004c:
	{
		int32_t L_11 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_11) == ((int32_t)((int32_t)83))))
		{
			goto IL_0066;
		}
	}
	{
		int32_t L_12 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_12) == ((uint32_t)((int32_t)80)))))
		{
			goto IL_009d;
		}
	}

IL_0066:
	{
		DTDReader_ReadExternalID_m1846433116(__this, /*hidden argument*/NULL);
		DTDParameterEntityDeclaration_t2256150406 * L_13 = V_0;
		String_t* L_14 = __this->get_cachedPublicId_11();
		NullCheck(L_13);
		DTDEntityBase_set_PublicId_m977897692(L_13, L_14, /*hidden argument*/NULL);
		DTDParameterEntityDeclaration_t2256150406 * L_15 = V_0;
		String_t* L_16 = __this->get_cachedSystemId_12();
		NullCheck(L_15);
		DTDEntityBase_set_SystemId_m3174403542(L_15, L_16, /*hidden argument*/NULL);
		DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
		DTDParameterEntityDeclaration_t2256150406 * L_17 = V_0;
		NullCheck(L_17);
		DTDEntityBase_Resolve_m3364394729(L_17, /*hidden argument*/NULL);
		DTDParameterEntityDeclaration_t2256150406 * L_18 = V_0;
		DTDReader_ResolveExternalEntityReplacementText_m1681961311(__this, L_18, /*hidden argument*/NULL);
		goto IL_0183;
	}

IL_009d:
	{
		DTDReader_TryExpandPERef_m232461585(__this, /*hidden argument*/NULL);
		int32_t L_19 = DTDReader_ReadChar_m1039540782(__this, /*hidden argument*/NULL);
		V_1 = L_19;
		int32_t L_20 = V_1;
		if ((((int32_t)L_20) == ((int32_t)((int32_t)39))))
		{
			goto IL_00c7;
		}
	}
	{
		int32_t L_21 = V_1;
		if ((((int32_t)L_21) == ((int32_t)((int32_t)34))))
		{
			goto IL_00c7;
		}
	}
	{
		XmlException_t3490696160 * L_22 = DTDReader_NotWFError_m440433271(__this, _stringLiteral3926763465, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_22);
	}

IL_00c7:
	{
		DTDReader_ClearValueBuffer_m4110694340(__this, /*hidden argument*/NULL);
		V_2 = (bool)1;
		goto IL_0164;
	}

IL_00d4:
	{
		int32_t L_23 = DTDReader_ReadChar_m1039540782(__this, /*hidden argument*/NULL);
		V_3 = L_23;
		int32_t L_24 = V_3;
		V_4 = L_24;
		int32_t L_25 = V_4;
		if ((((int32_t)L_25) == ((int32_t)(-1))))
		{
			goto IL_00fd;
		}
	}
	{
		int32_t L_26 = V_4;
		if ((((int32_t)L_26) == ((int32_t)((int32_t)34))))
		{
			goto IL_0109;
		}
	}
	{
		int32_t L_27 = V_4;
		if ((((int32_t)L_27) == ((int32_t)((int32_t)39))))
		{
			goto IL_0125;
		}
	}
	{
		goto IL_0141;
	}

IL_00fd:
	{
		XmlException_t3490696160 * L_28 = DTDReader_NotWFError_m440433271(__this, _stringLiteral174159455, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_28);
	}

IL_0109:
	{
		int32_t L_29 = V_1;
		if ((!(((uint32_t)L_29) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_0118;
		}
	}
	{
		V_2 = (bool)0;
		goto IL_0120;
	}

IL_0118:
	{
		DTDReader_AppendValueChar_m1219042272(__this, ((int32_t)34), /*hidden argument*/NULL);
	}

IL_0120:
	{
		goto IL_0164;
	}

IL_0125:
	{
		int32_t L_30 = V_1;
		if ((!(((uint32_t)L_30) == ((uint32_t)((int32_t)39)))))
		{
			goto IL_0134;
		}
	}
	{
		V_2 = (bool)0;
		goto IL_013c;
	}

IL_0134:
	{
		DTDReader_AppendValueChar_m1219042272(__this, ((int32_t)39), /*hidden argument*/NULL);
	}

IL_013c:
	{
		goto IL_0164;
	}

IL_0141:
	{
		int32_t L_31 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(XmlChar_t3591879093_il2cpp_TypeInfo_var);
		bool L_32 = XmlChar_IsInvalid_m3338941186(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_0158;
		}
	}
	{
		XmlException_t3490696160 * L_33 = DTDReader_NotWFError_m440433271(__this, _stringLiteral3890344584, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_33);
	}

IL_0158:
	{
		int32_t L_34 = V_3;
		DTDReader_AppendValueChar_m1219042272(__this, L_34, /*hidden argument*/NULL);
		goto IL_0164;
	}

IL_0164:
	{
		bool L_35 = V_2;
		if (L_35)
		{
			goto IL_00d4;
		}
	}
	{
		DTDParameterEntityDeclaration_t2256150406 * L_36 = V_0;
		String_t* L_37 = DTDReader_CreateValueString_m2937195813(__this, /*hidden argument*/NULL);
		NullCheck(L_36);
		DTDEntityBase_set_LiteralEntityValue_m256564993(L_36, L_37, /*hidden argument*/NULL);
		DTDReader_ClearValueBuffer_m4110694340(__this, /*hidden argument*/NULL);
		DTDParameterEntityDeclaration_t2256150406 * L_38 = V_0;
		DTDReader_ResolveInternalEntityReplacementText_m3467997101(__this, L_38, /*hidden argument*/NULL);
	}

IL_0183:
	{
		DTDReader_ExpectAfterWhitespace_m2772157595(__this, ((int32_t)62), /*hidden argument*/NULL);
		DTDObjectModel_t709926554 * L_39 = __this->get_DTD_13();
		NullCheck(L_39);
		DTDParameterEntityDeclarationCollection_t457236420 * L_40 = DTDObjectModel_get_PEDecls_m3945708037(L_39, /*hidden argument*/NULL);
		DTDParameterEntityDeclaration_t2256150406 * L_41 = V_0;
		NullCheck(L_41);
		String_t* L_42 = DTDEntityBase_get_Name_m1635306044(L_41, /*hidden argument*/NULL);
		NullCheck(L_40);
		DTDParameterEntityDeclaration_t2256150406 * L_43 = DTDParameterEntityDeclarationCollection_get_Item_m4053598104(L_40, L_42, /*hidden argument*/NULL);
		if (L_43)
		{
			goto IL_01bd;
		}
	}
	{
		DTDObjectModel_t709926554 * L_44 = __this->get_DTD_13();
		NullCheck(L_44);
		DTDParameterEntityDeclarationCollection_t457236420 * L_45 = DTDObjectModel_get_PEDecls_m3945708037(L_44, /*hidden argument*/NULL);
		DTDParameterEntityDeclaration_t2256150406 * L_46 = V_0;
		NullCheck(L_46);
		String_t* L_47 = DTDEntityBase_get_Name_m1635306044(L_46, /*hidden argument*/NULL);
		DTDParameterEntityDeclaration_t2256150406 * L_48 = V_0;
		NullCheck(L_45);
		DTDParameterEntityDeclarationCollection_Add_m3192507178(L_45, L_47, L_48, /*hidden argument*/NULL);
	}

IL_01bd:
	{
		return;
	}
}
// System.Void System.Xml.DTDReader::ResolveExternalEntityReplacementText(Mono.Xml.DTDEntityBase)
extern Il2CppClass* XmlTextReader_t3719122287_il2cpp_TypeInfo_var;
extern Il2CppClass* DTDEntityDeclaration_t2806387719_il2cpp_TypeInfo_var;
extern Il2CppClass* StringBuilder_t3822575854_il2cpp_TypeInfo_var;
extern const uint32_t DTDReader_ResolveExternalEntityReplacementText_m1681961311_MetadataUsageId;
extern "C"  void DTDReader_ResolveExternalEntityReplacementText_m1681961311 (DTDReader_t4257441119 * __this, DTDEntityBase_t1395379180 * ___decl0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDReader_ResolveExternalEntityReplacementText_m1681961311_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	XmlTextReader_t3719122287 * V_0 = NULL;
	StringBuilder_t3822575854 * V_1 = NULL;
	{
		DTDEntityBase_t1395379180 * L_0 = ___decl0;
		NullCheck(L_0);
		String_t* L_1 = DTDEntityBase_get_SystemId_m2816997275(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00b3;
		}
	}
	{
		DTDEntityBase_t1395379180 * L_2 = ___decl0;
		NullCheck(L_2);
		String_t* L_3 = DTDEntityBase_get_SystemId_m2816997275(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = String_get_Length_m2979997331(L_3, /*hidden argument*/NULL);
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_00b3;
		}
	}
	{
		DTDEntityBase_t1395379180 * L_5 = ___decl0;
		NullCheck(L_5);
		String_t* L_6 = DTDEntityBase_get_LiteralEntityValue_m3210771920(L_5, /*hidden argument*/NULL);
		XmlTextReader_t3719122287 * L_7 = (XmlTextReader_t3719122287 *)il2cpp_codegen_object_new(XmlTextReader_t3719122287_il2cpp_TypeInfo_var);
		XmlTextReader__ctor_m447354008(L_7, L_6, 1, (XmlParserContext_t3629084577 *)NULL, /*hidden argument*/NULL);
		V_0 = L_7;
		XmlTextReader_t3719122287 * L_8 = V_0;
		NullCheck(L_8);
		XmlTextReader_SkipTextDeclaration_m1645492832(L_8, /*hidden argument*/NULL);
		DTDEntityBase_t1395379180 * L_9 = ___decl0;
		if (!((DTDEntityDeclaration_t2806387719 *)IsInstClass(L_9, DTDEntityDeclaration_t2806387719_il2cpp_TypeInfo_var)))
		{
			goto IL_009d;
		}
	}
	{
		DTDObjectModel_t709926554 * L_10 = __this->get_DTD_13();
		NullCheck(L_10);
		DTDEntityDeclarationCollection_t4226410245 * L_11 = DTDObjectModel_get_EntityDecls_m3251787732(L_10, /*hidden argument*/NULL);
		DTDEntityBase_t1395379180 * L_12 = ___decl0;
		NullCheck(L_12);
		String_t* L_13 = DTDEntityBase_get_Name_m1635306044(L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		DTDEntityDeclaration_t2806387719 * L_14 = DTDEntityDeclarationCollection_get_Item_m2370831310(L_11, L_13, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_009d;
		}
	}
	{
		StringBuilder_t3822575854 * L_15 = (StringBuilder_t3822575854 *)il2cpp_codegen_object_new(StringBuilder_t3822575854_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m135953004(L_15, /*hidden argument*/NULL);
		V_1 = L_15;
		XmlTextReader_t3719122287 * L_16 = V_0;
		bool L_17 = DTDReader_get_Normalization_m2363640846(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		XmlTextReader_set_Normalization_m549008905(L_16, L_17, /*hidden argument*/NULL);
		XmlTextReader_t3719122287 * L_18 = V_0;
		NullCheck(L_18);
		VirtFuncInvoker0< bool >::Invoke(38 /* System.Boolean System.Xml.XmlTextReader::Read() */, L_18);
		goto IL_0081;
	}

IL_0074:
	{
		StringBuilder_t3822575854 * L_19 = V_1;
		XmlTextReader_t3719122287 * L_20 = V_0;
		NullCheck(L_20);
		String_t* L_21 = VirtFuncInvoker0< String_t* >::Invoke(42 /* System.String System.Xml.XmlReader::ReadOuterXml() */, L_20);
		NullCheck(L_19);
		StringBuilder_Append_m3898090075(L_19, L_21, /*hidden argument*/NULL);
	}

IL_0081:
	{
		XmlTextReader_t3719122287 * L_22 = V_0;
		NullCheck(L_22);
		bool L_23 = VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean System.Xml.XmlTextReader::get_EOF() */, L_22);
		if (!L_23)
		{
			goto IL_0074;
		}
	}
	{
		DTDEntityBase_t1395379180 * L_24 = ___decl0;
		StringBuilder_t3822575854 * L_25 = V_1;
		NullCheck(L_25);
		String_t* L_26 = StringBuilder_ToString_m350379841(L_25, /*hidden argument*/NULL);
		NullCheck(L_24);
		DTDEntityBase_set_ReplacementText_m1535392611(L_24, L_26, /*hidden argument*/NULL);
		goto IL_00ae;
	}

IL_009d:
	{
		DTDEntityBase_t1395379180 * L_27 = ___decl0;
		XmlTextReader_t3719122287 * L_28 = V_0;
		NullCheck(L_28);
		TextReader_t1534522647 * L_29 = XmlTextReader_GetRemainder_m2423594711(L_28, /*hidden argument*/NULL);
		NullCheck(L_29);
		String_t* L_30 = VirtFuncInvoker0< String_t* >::Invoke(11 /* System.String System.IO.TextReader::ReadToEnd() */, L_29);
		NullCheck(L_27);
		DTDEntityBase_set_ReplacementText_m1535392611(L_27, L_30, /*hidden argument*/NULL);
	}

IL_00ae:
	{
		goto IL_00bf;
	}

IL_00b3:
	{
		DTDEntityBase_t1395379180 * L_31 = ___decl0;
		DTDEntityBase_t1395379180 * L_32 = ___decl0;
		NullCheck(L_32);
		String_t* L_33 = DTDEntityBase_get_LiteralEntityValue_m3210771920(L_32, /*hidden argument*/NULL);
		NullCheck(L_31);
		DTDEntityBase_set_ReplacementText_m1535392611(L_31, L_33, /*hidden argument*/NULL);
	}

IL_00bf:
	{
		return;
	}
}
// System.Void System.Xml.DTDReader::ResolveInternalEntityReplacementText(Mono.Xml.DTDEntityBase)
extern Il2CppClass* XmlException_t3490696160_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlChar_t3591879093_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2484155688;
extern Il2CppCodeGenString* _stringLiteral3890344584;
extern Il2CppCodeGenString* _stringLiteral1590219389;
extern Il2CppCodeGenString* _stringLiteral2195838108;
extern const uint32_t DTDReader_ResolveInternalEntityReplacementText_m3467997101_MetadataUsageId;
extern "C"  void DTDReader_ResolveInternalEntityReplacementText_m3467997101 (DTDReader_t4257441119 * __this, DTDEntityBase_t1395379180 * ___decl0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDReader_ResolveInternalEntityReplacementText_m3467997101_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	String_t* V_5 = NULL;
	int32_t V_6 = 0;
	{
		DTDEntityBase_t1395379180 * L_0 = ___decl0;
		NullCheck(L_0);
		String_t* L_1 = DTDEntityBase_get_LiteralEntityValue_m3210771920(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m2979997331(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		DTDReader_ClearValueBuffer_m4110694340(__this, /*hidden argument*/NULL);
		V_2 = 0;
		goto IL_0185;
	}

IL_001b:
	{
		String_t* L_4 = V_0;
		int32_t L_5 = V_2;
		NullCheck(L_4);
		uint16_t L_6 = String_get_Chars_m3015341861(L_4, L_5, /*hidden argument*/NULL);
		V_3 = L_6;
		V_4 = 0;
		int32_t L_7 = V_3;
		V_6 = L_7;
		int32_t L_8 = V_6;
		if ((((int32_t)L_8) == ((int32_t)((int32_t)37))))
		{
			goto IL_0121;
		}
	}
	{
		int32_t L_9 = V_6;
		if ((((int32_t)L_9) == ((int32_t)((int32_t)38))))
		{
			goto IL_0040;
		}
	}
	{
		goto IL_0175;
	}

IL_0040:
	{
		int32_t L_10 = V_2;
		V_2 = ((int32_t)((int32_t)L_10+(int32_t)1));
		String_t* L_11 = V_0;
		int32_t L_12 = V_2;
		NullCheck(L_11);
		int32_t L_13 = String_IndexOf_m204546721(L_11, ((int32_t)59), L_12, /*hidden argument*/NULL);
		V_4 = L_13;
		int32_t L_14 = V_4;
		int32_t L_15 = V_2;
		if ((((int32_t)L_14) >= ((int32_t)((int32_t)((int32_t)L_15+(int32_t)1)))))
		{
			goto IL_006b;
		}
	}
	{
		DTDEntityBase_t1395379180 * L_16 = ___decl0;
		DTDEntityBase_t1395379180 * L_17 = ___decl0;
		NullCheck(L_17);
		String_t* L_18 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String Mono.Xml.DTDNode::get_BaseURI() */, L_17);
		XmlException_t3490696160 * L_19 = (XmlException_t3490696160 *)il2cpp_codegen_object_new(XmlException_t3490696160_il2cpp_TypeInfo_var);
		XmlException__ctor_m3778248785(L_19, L_16, L_18, _stringLiteral2484155688, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_19);
	}

IL_006b:
	{
		String_t* L_20 = V_0;
		int32_t L_21 = V_2;
		NullCheck(L_20);
		uint16_t L_22 = String_get_Chars_m3015341861(L_20, L_21, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_22) == ((uint32_t)((int32_t)35)))))
		{
			goto IL_00a6;
		}
	}
	{
		int32_t L_23 = V_2;
		V_2 = ((int32_t)((int32_t)L_23+(int32_t)1));
		DTDEntityBase_t1395379180 * L_24 = ___decl0;
		String_t* L_25 = V_0;
		int32_t L_26 = V_4;
		int32_t L_27 = DTDReader_GetCharacterReference_m1265404884(__this, L_24, L_25, (&V_2), L_26, /*hidden argument*/NULL);
		V_3 = L_27;
		int32_t L_28 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(XmlChar_t3591879093_il2cpp_TypeInfo_var);
		bool L_29 = XmlChar_IsInvalid_m3338941186(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_00a1;
		}
	}
	{
		XmlException_t3490696160 * L_30 = DTDReader_NotWFError_m440433271(__this, _stringLiteral3890344584, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_30);
	}

IL_00a1:
	{
		goto IL_00f8;
	}

IL_00a6:
	{
		String_t* L_31 = V_0;
		int32_t L_32 = V_2;
		int32_t L_33 = V_4;
		int32_t L_34 = V_2;
		NullCheck(L_31);
		String_t* L_35 = String_Substring_m675079568(L_31, L_32, ((int32_t)((int32_t)L_33-(int32_t)L_34)), /*hidden argument*/NULL);
		V_5 = L_35;
		String_t* L_36 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(XmlChar_t3591879093_il2cpp_TypeInfo_var);
		bool L_37 = XmlChar_IsName_m2902214767(NULL /*static, unused*/, L_36, /*hidden argument*/NULL);
		if (L_37)
		{
			goto IL_00d2;
		}
	}
	{
		String_t* L_38 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_39 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral1590219389, L_38, /*hidden argument*/NULL);
		XmlException_t3490696160 * L_40 = DTDReader_NotWFError_m440433271(__this, L_39, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_40);
	}

IL_00d2:
	{
		DTDReader_AppendValueChar_m1219042272(__this, ((int32_t)38), /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_41 = __this->get_valueBuffer_5();
		String_t* L_42 = V_5;
		NullCheck(L_41);
		StringBuilder_Append_m3898090075(L_41, L_42, /*hidden argument*/NULL);
		DTDReader_AppendValueChar_m1219042272(__this, ((int32_t)59), /*hidden argument*/NULL);
		int32_t L_43 = V_4;
		V_2 = L_43;
		goto IL_0181;
	}

IL_00f8:
	{
		int32_t L_44 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(XmlChar_t3591879093_il2cpp_TypeInfo_var);
		bool L_45 = XmlChar_IsInvalid_m3338941186(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		if (!L_45)
		{
			goto IL_0115;
		}
	}
	{
		DTDEntityBase_t1395379180 * L_46 = ___decl0;
		DTDEntityBase_t1395379180 * L_47 = ___decl0;
		NullCheck(L_47);
		String_t* L_48 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String Mono.Xml.DTDNode::get_BaseURI() */, L_47);
		XmlException_t3490696160 * L_49 = (XmlException_t3490696160 *)il2cpp_codegen_object_new(XmlException_t3490696160_il2cpp_TypeInfo_var);
		XmlException__ctor_m3778248785(L_49, L_46, L_48, _stringLiteral2195838108, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_49);
	}

IL_0115:
	{
		int32_t L_50 = V_3;
		DTDReader_AppendValueChar_m1219042272(__this, L_50, /*hidden argument*/NULL);
		goto IL_0181;
	}

IL_0121:
	{
		int32_t L_51 = V_2;
		V_2 = ((int32_t)((int32_t)L_51+(int32_t)1));
		String_t* L_52 = V_0;
		int32_t L_53 = V_2;
		NullCheck(L_52);
		int32_t L_54 = String_IndexOf_m204546721(L_52, ((int32_t)59), L_53, /*hidden argument*/NULL);
		V_4 = L_54;
		int32_t L_55 = V_4;
		int32_t L_56 = V_2;
		if ((((int32_t)L_55) >= ((int32_t)((int32_t)((int32_t)L_56+(int32_t)1)))))
		{
			goto IL_014c;
		}
	}
	{
		DTDEntityBase_t1395379180 * L_57 = ___decl0;
		DTDEntityBase_t1395379180 * L_58 = ___decl0;
		NullCheck(L_58);
		String_t* L_59 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String Mono.Xml.DTDNode::get_BaseURI() */, L_58);
		XmlException_t3490696160 * L_60 = (XmlException_t3490696160 *)il2cpp_codegen_object_new(XmlException_t3490696160_il2cpp_TypeInfo_var);
		XmlException__ctor_m3778248785(L_60, L_57, L_59, _stringLiteral2484155688, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_60);
	}

IL_014c:
	{
		String_t* L_61 = V_0;
		int32_t L_62 = V_2;
		int32_t L_63 = V_4;
		int32_t L_64 = V_2;
		NullCheck(L_61);
		String_t* L_65 = String_Substring_m675079568(L_61, L_62, ((int32_t)((int32_t)L_63-(int32_t)L_64)), /*hidden argument*/NULL);
		V_5 = L_65;
		StringBuilder_t3822575854 * L_66 = __this->get_valueBuffer_5();
		String_t* L_67 = V_5;
		String_t* L_68 = DTDReader_GetPEValue_m985148249(__this, L_67, /*hidden argument*/NULL);
		NullCheck(L_66);
		StringBuilder_Append_m3898090075(L_66, L_68, /*hidden argument*/NULL);
		int32_t L_69 = V_4;
		V_2 = L_69;
		goto IL_0181;
	}

IL_0175:
	{
		int32_t L_70 = V_3;
		DTDReader_AppendValueChar_m1219042272(__this, L_70, /*hidden argument*/NULL);
		goto IL_0181;
	}

IL_0181:
	{
		int32_t L_71 = V_2;
		V_2 = ((int32_t)((int32_t)L_71+(int32_t)1));
	}

IL_0185:
	{
		int32_t L_72 = V_2;
		int32_t L_73 = V_1;
		if ((((int32_t)L_72) < ((int32_t)L_73)))
		{
			goto IL_001b;
		}
	}
	{
		DTDEntityBase_t1395379180 * L_74 = ___decl0;
		String_t* L_75 = DTDReader_CreateValueString_m2937195813(__this, /*hidden argument*/NULL);
		NullCheck(L_74);
		DTDEntityBase_set_ReplacementText_m1535392611(L_74, L_75, /*hidden argument*/NULL);
		DTDReader_ClearValueBuffer_m4110694340(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 System.Xml.DTDReader::GetCharacterReference(Mono.Xml.DTDEntityBase,System.String,System.Int32&,System.Int32)
extern Il2CppClass* CultureInfo_t3603717042_il2cpp_TypeInfo_var;
extern Il2CppClass* FormatException_t2404802957_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlException_t3490696160_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2900025950;
extern const uint32_t DTDReader_GetCharacterReference_m1265404884_MetadataUsageId;
extern "C"  int32_t DTDReader_GetCharacterReference_m1265404884 (DTDReader_t4257441119 * __this, DTDEntityBase_t1395379180 * ___li0, String_t* ___value1, int32_t* ___index2, int32_t ___end3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDReader_GetCharacterReference_m1265404884_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = 0;
		String_t* L_0 = ___value1;
		int32_t* L_1 = ___index2;
		NullCheck(L_0);
		uint16_t L_2 = String_get_Chars_m3015341861(L_0, (*((int32_t*)L_1)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)((int32_t)120)))))
		{
			goto IL_0054;
		}
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		String_t* L_3 = ___value1;
		int32_t* L_4 = ___index2;
		int32_t L_5 = ___end3;
		int32_t* L_6 = ___index2;
		NullCheck(L_3);
		String_t* L_7 = String_Substring_m675079568(L_3, ((int32_t)((int32_t)(*((int32_t*)L_4))+(int32_t)1)), ((int32_t)((int32_t)((int32_t)((int32_t)L_5-(int32_t)(*((int32_t*)L_6))))-(int32_t)1)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3603717042_il2cpp_TypeInfo_var);
		CultureInfo_t3603717042 * L_8 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_9 = Int32_Parse_m2659730549(NULL /*static, unused*/, L_7, ((int32_t)515), L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_004f;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (FormatException_t2404802957_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0037;
		throw e;
	}

CATCH_0037:
	{ // begin catch(System.FormatException)
		{
			DTDEntityBase_t1395379180 * L_10 = ___li0;
			DTDEntityBase_t1395379180 * L_11 = ___li0;
			NullCheck(L_11);
			String_t* L_12 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String Mono.Xml.DTDNode::get_BaseURI() */, L_11);
			XmlException_t3490696160 * L_13 = (XmlException_t3490696160 *)il2cpp_codegen_object_new(XmlException_t3490696160_il2cpp_TypeInfo_var);
			XmlException__ctor_m3778248785(L_13, L_10, L_12, _stringLiteral2900025950, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_13);
		}

IL_004a:
		{
			goto IL_004f;
		}
	} // end catch (depth: 1)

IL_004f:
	{
		goto IL_0089;
	}

IL_0054:
	try
	{ // begin try (depth: 1)
		String_t* L_14 = ___value1;
		int32_t* L_15 = ___index2;
		int32_t L_16 = ___end3;
		int32_t* L_17 = ___index2;
		NullCheck(L_14);
		String_t* L_18 = String_Substring_m675079568(L_14, (*((int32_t*)L_15)), ((int32_t)((int32_t)L_16-(int32_t)(*((int32_t*)L_17)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3603717042_il2cpp_TypeInfo_var);
		CultureInfo_t3603717042 * L_19 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_20 = Int32_Parse_m981442986(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		V_0 = L_20;
		goto IL_0089;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (FormatException_t2404802957_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0071;
		throw e;
	}

CATCH_0071:
	{ // begin catch(System.FormatException)
		{
			DTDEntityBase_t1395379180 * L_21 = ___li0;
			DTDEntityBase_t1395379180 * L_22 = ___li0;
			NullCheck(L_22);
			String_t* L_23 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String Mono.Xml.DTDNode::get_BaseURI() */, L_22);
			XmlException_t3490696160 * L_24 = (XmlException_t3490696160 *)il2cpp_codegen_object_new(XmlException_t3490696160_il2cpp_TypeInfo_var);
			XmlException__ctor_m3778248785(L_24, L_21, L_23, _stringLiteral2900025950, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_24);
		}

IL_0084:
		{
			goto IL_0089;
		}
	} // end catch (depth: 1)

IL_0089:
	{
		int32_t* L_25 = ___index2;
		int32_t L_26 = ___end3;
		*((int32_t*)(L_25)) = (int32_t)L_26;
		int32_t L_27 = V_0;
		return L_27;
	}
}
// System.String System.Xml.DTDReader::GetPEValue(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t DTDReader_GetPEValue_m985148249_MetadataUsageId;
extern "C"  String_t* DTDReader_GetPEValue_m985148249 (DTDReader_t4257441119 * __this, String_t* ___peName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDReader_GetPEValue_m985148249_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DTDParameterEntityDeclaration_t2256150406 * V_0 = NULL;
	String_t* G_B3_0 = NULL;
	{
		String_t* L_0 = ___peName0;
		DTDParameterEntityDeclaration_t2256150406 * L_1 = DTDReader_GetPEDecl_m1992309660(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		DTDParameterEntityDeclaration_t2256150406 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_0019;
		}
	}
	{
		DTDParameterEntityDeclaration_t2256150406 * L_3 = V_0;
		NullCheck(L_3);
		String_t* L_4 = DTDEntityBase_get_ReplacementText_m3411513392(L_3, /*hidden argument*/NULL);
		G_B3_0 = L_4;
		goto IL_001e;
	}

IL_0019:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
	}

IL_001e:
	{
		return G_B3_0;
	}
}
// Mono.Xml.DTDParameterEntityDeclaration System.Xml.DTDReader::GetPEDecl(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlException_t3490696160_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral701453291;
extern Il2CppCodeGenString* _stringLiteral39;
extern Il2CppCodeGenString* _stringLiteral3906327673;
extern Il2CppCodeGenString* _stringLiteral1498509062;
extern Il2CppCodeGenString* _stringLiteral1090661113;
extern const uint32_t DTDReader_GetPEDecl_m1992309660_MetadataUsageId;
extern "C"  DTDParameterEntityDeclaration_t2256150406 * DTDReader_GetPEDecl_m1992309660 (DTDReader_t4257441119 * __this, String_t* ___peName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDReader_GetPEDecl_m1992309660_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DTDParameterEntityDeclaration_t2256150406 * V_0 = NULL;
	{
		DTDObjectModel_t709926554 * L_0 = __this->get_DTD_13();
		NullCheck(L_0);
		DTDParameterEntityDeclarationCollection_t457236420 * L_1 = DTDObjectModel_get_PEDecls_m3945708037(L_0, /*hidden argument*/NULL);
		String_t* L_2 = ___peName0;
		NullCheck(L_1);
		DTDParameterEntityDeclaration_t2256150406 * L_3 = DTDParameterEntityDeclarationCollection_get_Item_m4053598104(L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		DTDParameterEntityDeclaration_t2256150406 * L_4 = V_0;
		if (!L_4)
		{
			goto IL_003c;
		}
	}
	{
		DTDParameterEntityDeclaration_t2256150406 * L_5 = V_0;
		NullCheck(L_5);
		bool L_6 = DTDNode_get_IsInternalSubset_m3131993229(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003a;
		}
	}
	{
		String_t* L_7 = ___peName0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral701453291, L_7, _stringLiteral39, /*hidden argument*/NULL);
		XmlException_t3490696160 * L_9 = DTDReader_NotWFError_m440433271(__this, L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_003a:
	{
		DTDParameterEntityDeclaration_t2256150406 * L_10 = V_0;
		return L_10;
	}

IL_003c:
	{
		DTDObjectModel_t709926554 * L_11 = __this->get_DTD_13();
		NullCheck(L_11);
		String_t* L_12 = DTDObjectModel_get_SystemId_m893340523(L_11, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_005c;
		}
	}
	{
		DTDObjectModel_t709926554 * L_13 = __this->get_DTD_13();
		NullCheck(L_13);
		bool L_14 = DTDObjectModel_get_InternalSubsetHasPEReference_m603105815(L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_006c;
		}
	}

IL_005c:
	{
		DTDObjectModel_t709926554 * L_15 = __this->get_DTD_13();
		NullCheck(L_15);
		bool L_16 = DTDObjectModel_get_IsStandalone_m3806876939(L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_007e;
		}
	}

IL_006c:
	{
		String_t* L_17 = ___peName0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral3906327673, L_17, /*hidden argument*/NULL);
		XmlException_t3490696160 * L_19 = DTDReader_NotWFError_m440433271(__this, L_18, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_19);
	}

IL_007e:
	{
		String_t* L_20 = ___peName0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral1498509062, L_20, _stringLiteral1090661113, /*hidden argument*/NULL);
		XmlException_t3490696160 * L_22 = (XmlException_t3490696160 *)il2cpp_codegen_object_new(XmlException_t3490696160_il2cpp_TypeInfo_var);
		XmlException__ctor_m1015471719(L_22, L_21, (Exception_t1967233988 *)NULL, /*hidden argument*/NULL);
		DTDReader_HandleError_m2636292706(__this, L_22, /*hidden argument*/NULL);
		return (DTDParameterEntityDeclaration_t2256150406 *)NULL;
	}
}
// System.Boolean System.Xml.DTDReader::TryExpandPERef()
extern "C"  bool DTDReader_TryExpandPERef_m232461585 (DTDReader_t4257441119 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)((int32_t)37))))
		{
			goto IL_000f;
		}
	}
	{
		return (bool)0;
	}

IL_000f:
	{
		goto IL_0022;
	}

IL_0014:
	{
		DTDReader_TryExpandPERefSpaceKeep_m419925436(__this, /*hidden argument*/NULL);
		DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
	}

IL_0022:
	{
		int32_t L_1 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)((int32_t)37))))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)1;
	}
}
// System.Boolean System.Xml.DTDReader::TryExpandPERefSpaceKeep()
extern Il2CppCodeGenString* _stringLiteral493935949;
extern const uint32_t DTDReader_TryExpandPERefSpaceKeep_m419925436_MetadataUsageId;
extern "C"  bool DTDReader_TryExpandPERefSpaceKeep_m419925436 (DTDReader_t4257441119 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDReader_TryExpandPERefSpaceKeep_m419925436_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)37)))))
		{
			goto IL_0033;
		}
	}
	{
		bool L_1 = __this->get_processingInternalSubset_10();
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		XmlException_t3490696160 * L_2 = DTDReader_NotWFError_m440433271(__this, _stringLiteral493935949, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0024:
	{
		DTDReader_ReadChar_m1039540782(__this, /*hidden argument*/NULL);
		DTDReader_ExpandPERef_m4144305510(__this, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_0033:
	{
		return (bool)0;
	}
}
// System.Void System.Xml.DTDReader::ExpandPERef()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlException_t3490696160_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1498509062;
extern Il2CppCodeGenString* _stringLiteral1090661113;
extern const uint32_t DTDReader_ExpandPERef_m4144305510_MetadataUsageId;
extern "C"  void DTDReader_ExpandPERef_m4144305510 (DTDReader_t4257441119 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDReader_ExpandPERef_m4144305510_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	DTDParameterEntityDeclaration_t2256150406 * V_1 = NULL;
	{
		String_t* L_0 = DTDReader_ReadName_m4241357028(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		DTDReader_Expect_m1917199594(__this, ((int32_t)59), /*hidden argument*/NULL);
		DTDObjectModel_t709926554 * L_1 = __this->get_DTD_13();
		NullCheck(L_1);
		DTDParameterEntityDeclarationCollection_t457236420 * L_2 = DTDObjectModel_get_PEDecls_m3945708037(L_1, /*hidden argument*/NULL);
		String_t* L_3 = V_0;
		NullCheck(L_2);
		DTDParameterEntityDeclaration_t2256150406 * L_4 = DTDParameterEntityDeclarationCollection_get_Item_m4053598104(L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		DTDParameterEntityDeclaration_t2256150406 * L_5 = V_1;
		if (L_5)
		{
			goto IL_0044;
		}
	}
	{
		String_t* L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral1498509062, L_6, _stringLiteral1090661113, /*hidden argument*/NULL);
		XmlException_t3490696160 * L_8 = (XmlException_t3490696160 *)il2cpp_codegen_object_new(XmlException_t3490696160_il2cpp_TypeInfo_var);
		XmlException__ctor_m1015471719(L_8, L_7, (Exception_t1967233988 *)NULL, /*hidden argument*/NULL);
		DTDReader_HandleError_m2636292706(__this, L_8, /*hidden argument*/NULL);
		return;
	}

IL_0044:
	{
		XmlParserInput_t3311178812 * L_9 = __this->get_currentInput_0();
		DTDParameterEntityDeclaration_t2256150406 * L_10 = V_1;
		NullCheck(L_9);
		XmlParserInput_PushPEBuffer_m274373084(L_9, L_10, /*hidden argument*/NULL);
		return;
	}
}
// Mono.Xml.DTDEntityDeclaration System.Xml.DTDReader::ReadEntityDecl()
extern Il2CppClass* DTDEntityDeclaration_t2806387719_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1449031273;
extern Il2CppCodeGenString* _stringLiteral74125560;
extern Il2CppCodeGenString* _stringLiteral1661308296;
extern const uint32_t DTDReader_ReadEntityDecl_m131033009_MetadataUsageId;
extern "C"  DTDEntityDeclaration_t2806387719 * DTDReader_ReadEntityDecl_m131033009 (DTDReader_t4257441119 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDReader_ReadEntityDecl_m131033009_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DTDEntityDeclaration_t2806387719 * V_0 = NULL;
	{
		DTDObjectModel_t709926554 * L_0 = __this->get_DTD_13();
		DTDEntityDeclaration_t2806387719 * L_1 = (DTDEntityDeclaration_t2806387719 *)il2cpp_codegen_object_new(DTDEntityDeclaration_t2806387719_il2cpp_TypeInfo_var);
		DTDEntityDeclaration__ctor_m705712862(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		DTDEntityDeclaration_t2806387719 * L_2 = V_0;
		String_t* L_3 = DTDReader_get_BaseURI_m1245597521(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(7 /* System.Void Mono.Xml.DTDNode::set_BaseURI(System.String) */, L_2, L_3);
		DTDEntityDeclaration_t2806387719 * L_4 = V_0;
		DTDObjectModel_t709926554 * L_5 = __this->get_DTD_13();
		NullCheck(L_5);
		XmlResolver_t2502213349 * L_6 = DTDObjectModel_get_Resolver_m1268564320(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		DTDEntityBase_set_XmlResolver_m2788162230(L_4, L_6, /*hidden argument*/NULL);
		DTDEntityDeclaration_t2806387719 * L_7 = V_0;
		bool L_8 = __this->get_processingInternalSubset_10();
		NullCheck(L_7);
		DTDNode_set_IsInternalSubset_m3109582768(L_7, L_8, /*hidden argument*/NULL);
		DTDReader_TryExpandPERef_m232461585(__this, /*hidden argument*/NULL);
		DTDEntityDeclaration_t2806387719 * L_9 = V_0;
		String_t* L_10 = DTDReader_ReadName_m4241357028(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		DTDEntityBase_set_Name_m1787521109(L_9, L_10, /*hidden argument*/NULL);
		bool L_11 = DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_005f;
		}
	}
	{
		XmlException_t3490696160 * L_12 = DTDReader_NotWFError_m440433271(__this, _stringLiteral1449031273, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12);
	}

IL_005f:
	{
		DTDReader_TryExpandPERef_m232461585(__this, /*hidden argument*/NULL);
		int32_t L_13 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_13) == ((int32_t)((int32_t)83))))
		{
			goto IL_0080;
		}
	}
	{
		int32_t L_14 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_14) == ((uint32_t)((int32_t)80)))))
		{
			goto IL_011c;
		}
	}

IL_0080:
	{
		DTDReader_ReadExternalID_m1846433116(__this, /*hidden argument*/NULL);
		DTDEntityDeclaration_t2806387719 * L_15 = V_0;
		String_t* L_16 = __this->get_cachedPublicId_11();
		NullCheck(L_15);
		DTDEntityBase_set_PublicId_m977897692(L_15, L_16, /*hidden argument*/NULL);
		DTDEntityDeclaration_t2806387719 * L_17 = V_0;
		String_t* L_18 = __this->get_cachedSystemId_12();
		NullCheck(L_17);
		DTDEntityBase_set_SystemId_m3174403542(L_17, L_18, /*hidden argument*/NULL);
		bool L_19 = DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00e4;
		}
	}
	{
		int32_t L_20 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_20) == ((uint32_t)((int32_t)78)))))
		{
			goto IL_00e4;
		}
	}
	{
		DTDReader_Expect_m4055310985(__this, _stringLiteral74125560, /*hidden argument*/NULL);
		bool L_21 = DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
		if (L_21)
		{
			goto IL_00d8;
		}
	}
	{
		XmlException_t3490696160 * L_22 = DTDReader_NotWFError_m440433271(__this, _stringLiteral1661308296, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_22);
	}

IL_00d8:
	{
		DTDEntityDeclaration_t2806387719 * L_23 = V_0;
		String_t* L_24 = DTDReader_ReadName_m4241357028(__this, /*hidden argument*/NULL);
		NullCheck(L_23);
		DTDEntityDeclaration_set_NotationName_m2884802480(L_23, L_24, /*hidden argument*/NULL);
	}

IL_00e4:
	{
		DTDEntityDeclaration_t2806387719 * L_25 = V_0;
		NullCheck(L_25);
		String_t* L_26 = DTDEntityDeclaration_get_NotationName_m528268507(L_25, /*hidden argument*/NULL);
		if (L_26)
		{
			goto IL_0101;
		}
	}
	{
		DTDEntityDeclaration_t2806387719 * L_27 = V_0;
		NullCheck(L_27);
		DTDEntityBase_Resolve_m3364394729(L_27, /*hidden argument*/NULL);
		DTDEntityDeclaration_t2806387719 * L_28 = V_0;
		DTDReader_ResolveExternalEntityReplacementText_m1681961311(__this, L_28, /*hidden argument*/NULL);
		goto IL_0117;
	}

IL_0101:
	{
		DTDEntityDeclaration_t2806387719 * L_29 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_30 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_29);
		DTDEntityBase_set_LiteralEntityValue_m256564993(L_29, L_30, /*hidden argument*/NULL);
		DTDEntityDeclaration_t2806387719 * L_31 = V_0;
		String_t* L_32 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_31);
		DTDEntityBase_set_ReplacementText_m1535392611(L_31, L_32, /*hidden argument*/NULL);
	}

IL_0117:
	{
		goto IL_012a;
	}

IL_011c:
	{
		DTDEntityDeclaration_t2806387719 * L_33 = V_0;
		DTDReader_ReadEntityValueDecl_m3766537449(__this, L_33, /*hidden argument*/NULL);
		DTDEntityDeclaration_t2806387719 * L_34 = V_0;
		DTDReader_ResolveInternalEntityReplacementText_m3467997101(__this, L_34, /*hidden argument*/NULL);
	}

IL_012a:
	{
		DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
		DTDReader_TryExpandPERef_m232461585(__this, /*hidden argument*/NULL);
		DTDReader_Expect_m1917199594(__this, ((int32_t)62), /*hidden argument*/NULL);
		DTDEntityDeclaration_t2806387719 * L_35 = V_0;
		return L_35;
	}
}
// System.Void System.Xml.DTDReader::ReadEntityValueDecl(Mono.Xml.DTDEntityDeclaration)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlChar_t3591879093_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3926763465;
extern Il2CppCodeGenString* _stringLiteral1226183562;
extern Il2CppCodeGenString* _stringLiteral1963858809;
extern Il2CppCodeGenString* _stringLiteral2195838108;
extern const uint32_t DTDReader_ReadEntityValueDecl_m3766537449_MetadataUsageId;
extern "C"  void DTDReader_ReadEntityValueDecl_m3766537449 (DTDReader_t4257441119 * __this, DTDEntityDeclaration_t2806387719 * ___decl0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDReader_ReadEntityValueDecl_m3766537449_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	int32_t V_4 = 0;
	{
		DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
		int32_t L_0 = DTDReader_ReadChar_m1039540782(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)((int32_t)39))))
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)((int32_t)34))))
		{
			goto IL_002a;
		}
	}
	{
		XmlException_t3490696160 * L_3 = DTDReader_NotWFError_m440433271(__this, _stringLiteral3926763465, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002a:
	{
		DTDReader_ClearValueBuffer_m4110694340(__this, /*hidden argument*/NULL);
		goto IL_00d3;
	}

IL_0035:
	{
		int32_t L_4 = DTDReader_ReadChar_m1039540782(__this, /*hidden argument*/NULL);
		V_1 = L_4;
		int32_t L_5 = V_1;
		V_4 = L_5;
		int32_t L_6 = V_4;
		if ((((int32_t)L_6) == ((int32_t)(-1))))
		{
			goto IL_0099;
		}
	}
	{
		int32_t L_7 = V_4;
		if ((((int32_t)L_7) == ((int32_t)((int32_t)37))))
		{
			goto IL_0055;
		}
	}
	{
		goto IL_00a5;
	}

IL_0055:
	{
		String_t* L_8 = DTDReader_ReadName_m4241357028(__this, /*hidden argument*/NULL);
		V_2 = L_8;
		DTDReader_Expect_m1917199594(__this, ((int32_t)59), /*hidden argument*/NULL);
		DTDEntityDeclaration_t2806387719 * L_9 = ___decl0;
		NullCheck(L_9);
		bool L_10 = DTDNode_get_IsInternalSubset_m3131993229(L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0081;
		}
	}
	{
		String_t* L_11 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral1226183562, L_11, /*hidden argument*/NULL);
		XmlException_t3490696160 * L_13 = DTDReader_NotWFError_m440433271(__this, L_12, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_13);
	}

IL_0081:
	{
		StringBuilder_t3822575854 * L_14 = __this->get_valueBuffer_5();
		String_t* L_15 = V_2;
		String_t* L_16 = DTDReader_GetPEValue_m985148249(__this, L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		StringBuilder_Append_m3898090075(L_14, L_16, /*hidden argument*/NULL);
		goto IL_00d3;
	}

IL_0099:
	{
		XmlException_t3490696160 * L_17 = DTDReader_NotWFError_m440433271(__this, _stringLiteral1963858809, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_17);
	}

IL_00a5:
	{
		bool L_18 = __this->get_normalization_9();
		if (!L_18)
		{
			goto IL_00c7;
		}
	}
	{
		int32_t L_19 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(XmlChar_t3591879093_il2cpp_TypeInfo_var);
		bool L_20 = XmlChar_IsInvalid_m3338941186(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00c7;
		}
	}
	{
		XmlException_t3490696160 * L_21 = DTDReader_NotWFError_m440433271(__this, _stringLiteral2195838108, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}

IL_00c7:
	{
		int32_t L_22 = V_1;
		DTDReader_AppendValueChar_m1219042272(__this, L_22, /*hidden argument*/NULL);
		goto IL_00d3;
	}

IL_00d3:
	{
		int32_t L_23 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		int32_t L_24 = V_0;
		if ((!(((uint32_t)L_23) == ((uint32_t)L_24))))
		{
			goto IL_0035;
		}
	}
	{
		String_t* L_25 = DTDReader_CreateValueString_m2937195813(__this, /*hidden argument*/NULL);
		V_3 = L_25;
		DTDReader_ClearValueBuffer_m4110694340(__this, /*hidden argument*/NULL);
		int32_t L_26 = V_0;
		DTDReader_Expect_m1917199594(__this, L_26, /*hidden argument*/NULL);
		DTDEntityDeclaration_t2806387719 * L_27 = ___decl0;
		String_t* L_28 = V_3;
		NullCheck(L_27);
		DTDEntityBase_set_LiteralEntityValue_m256564993(L_27, L_28, /*hidden argument*/NULL);
		return;
	}
}
// Mono.Xml.DTDAttListDeclaration System.Xml.DTDReader::ReadAttListDecl()
extern Il2CppClass* DTDAttListDeclaration_t1258293171_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlException_t3490696160_il2cpp_TypeInfo_var;
extern Il2CppClass* ICollection_t3761522009_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlChar_t3591879093_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2095947991;
extern Il2CppCodeGenString* _stringLiteral44333264;
extern Il2CppCodeGenString* _stringLiteral2785353245;
extern const uint32_t DTDReader_ReadAttListDecl_m877842801_MetadataUsageId;
extern "C"  DTDAttListDeclaration_t1258293171 * DTDReader_ReadAttListDecl_m877842801 (DTDReader_t4257441119 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDReader_ReadAttListDecl_m877842801_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	DTDAttListDeclaration_t1258293171 * V_1 = NULL;
	DTDAttributeDefinition_t1053410431 * V_2 = NULL;
	int32_t V_3 = 0;
	DTDAttributeDefinition_t1053410431 * V_4 = NULL;
	{
		DTDReader_TryExpandPERefSpaceKeep_m419925436(__this, /*hidden argument*/NULL);
		bool L_0 = DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		XmlException_t3490696160 * L_1 = DTDReader_NotWFError_m440433271(__this, _stringLiteral2095947991, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001e:
	{
		DTDReader_TryExpandPERef_m232461585(__this, /*hidden argument*/NULL);
		String_t* L_2 = DTDReader_ReadName_m4241357028(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		DTDObjectModel_t709926554 * L_3 = __this->get_DTD_13();
		NullCheck(L_3);
		DTDAttListDeclarationCollection_t733492145 * L_4 = DTDObjectModel_get_AttListDecls_m3653137288(L_3, /*hidden argument*/NULL);
		String_t* L_5 = V_0;
		NullCheck(L_4);
		DTDAttListDeclaration_t1258293171 * L_6 = DTDAttListDeclarationCollection_get_Item_m920644408(L_4, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		DTDAttListDeclaration_t1258293171 * L_7 = V_1;
		if (L_7)
		{
			goto IL_0050;
		}
	}
	{
		DTDObjectModel_t709926554 * L_8 = __this->get_DTD_13();
		DTDAttListDeclaration_t1258293171 * L_9 = (DTDAttListDeclaration_t1258293171 *)il2cpp_codegen_object_new(DTDAttListDeclaration_t1258293171_il2cpp_TypeInfo_var);
		DTDAttListDeclaration__ctor_m1247135746(L_9, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
	}

IL_0050:
	{
		DTDAttListDeclaration_t1258293171 * L_10 = V_1;
		bool L_11 = __this->get_processingInternalSubset_10();
		NullCheck(L_10);
		DTDNode_set_IsInternalSubset_m3109582768(L_10, L_11, /*hidden argument*/NULL);
		DTDAttListDeclaration_t1258293171 * L_12 = V_1;
		String_t* L_13 = V_0;
		NullCheck(L_12);
		DTDAttListDeclaration_set_Name_m1965188718(L_12, L_13, /*hidden argument*/NULL);
		bool L_14 = DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_0087;
		}
	}
	{
		int32_t L_15 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_15) == ((int32_t)((int32_t)62))))
		{
			goto IL_0087;
		}
	}
	{
		XmlException_t3490696160 * L_16 = DTDReader_NotWFError_m440433271(__this, _stringLiteral44333264, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
	}

IL_0087:
	{
		DTDReader_TryExpandPERef_m232461585(__this, /*hidden argument*/NULL);
		goto IL_0131;
	}

IL_0093:
	{
		DTDAttributeDefinition_t1053410431 * L_17 = DTDReader_ReadAttributeDefinition_m4289497893(__this, /*hidden argument*/NULL);
		V_2 = L_17;
		DTDAttributeDefinition_t1053410431 * L_18 = V_2;
		NullCheck(L_18);
		XmlSchemaDatatype_t2590121 * L_19 = DTDAttributeDefinition_get_Datatype_m2869597951(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		int32_t L_20 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Xml.XmlTokenizedType System.Xml.Schema.XmlSchemaDatatype::get_TokenizedType() */, L_19);
		if ((!(((uint32_t)L_20) == ((uint32_t)1))))
		{
			goto IL_010b;
		}
	}
	{
		V_3 = 0;
		goto IL_00fa;
	}

IL_00b2:
	{
		DTDAttListDeclaration_t1258293171 * L_21 = V_1;
		int32_t L_22 = V_3;
		NullCheck(L_21);
		DTDAttributeDefinition_t1053410431 * L_23 = DTDAttListDeclaration_get_Item_m3277382479(L_21, L_22, /*hidden argument*/NULL);
		V_4 = L_23;
		DTDAttributeDefinition_t1053410431 * L_24 = V_4;
		NullCheck(L_24);
		XmlSchemaDatatype_t2590121 * L_25 = DTDAttributeDefinition_get_Datatype_m2869597951(L_24, /*hidden argument*/NULL);
		NullCheck(L_25);
		int32_t L_26 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Xml.XmlTokenizedType System.Xml.Schema.XmlSchemaDatatype::get_TokenizedType() */, L_25);
		if ((!(((uint32_t)L_26) == ((uint32_t)1))))
		{
			goto IL_00f6;
		}
	}
	{
		DTDAttributeDefinition_t1053410431 * L_27 = V_2;
		NullCheck(L_27);
		int32_t L_28 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 Mono.Xml.DTDNode::get_LineNumber() */, L_27);
		DTDAttributeDefinition_t1053410431 * L_29 = V_2;
		NullCheck(L_29);
		int32_t L_30 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 Mono.Xml.DTDNode::get_LinePosition() */, L_29);
		DTDAttributeDefinition_t1053410431 * L_31 = V_2;
		NullCheck(L_31);
		String_t* L_32 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String Mono.Xml.DTDNode::get_BaseURI() */, L_31);
		XmlException_t3490696160 * L_33 = (XmlException_t3490696160 *)il2cpp_codegen_object_new(XmlException_t3490696160_il2cpp_TypeInfo_var);
		XmlException__ctor_m2245560573(L_33, _stringLiteral2785353245, L_28, L_30, NULL, L_32, (Exception_t1967233988 *)NULL, /*hidden argument*/NULL);
		DTDReader_HandleError_m2636292706(__this, L_33, /*hidden argument*/NULL);
		goto IL_010b;
	}

IL_00f6:
	{
		int32_t L_34 = V_3;
		V_3 = ((int32_t)((int32_t)L_34+(int32_t)1));
	}

IL_00fa:
	{
		int32_t L_35 = V_3;
		DTDAttListDeclaration_t1258293171 * L_36 = V_1;
		NullCheck(L_36);
		Il2CppObject * L_37 = DTDAttListDeclaration_get_Definitions_m1730561771(L_36, /*hidden argument*/NULL);
		NullCheck(L_37);
		int32_t L_38 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.ICollection::get_Count() */, ICollection_t3761522009_il2cpp_TypeInfo_var, L_37);
		if ((((int32_t)L_35) < ((int32_t)L_38)))
		{
			goto IL_00b2;
		}
	}

IL_010b:
	{
		DTDAttListDeclaration_t1258293171 * L_39 = V_1;
		DTDAttributeDefinition_t1053410431 * L_40 = V_2;
		NullCheck(L_40);
		String_t* L_41 = DTDAttributeDefinition_get_Name_m1998089073(L_40, /*hidden argument*/NULL);
		NullCheck(L_39);
		DTDAttributeDefinition_t1053410431 * L_42 = DTDAttListDeclaration_get_Item_m3271307460(L_39, L_41, /*hidden argument*/NULL);
		if (L_42)
		{
			goto IL_0123;
		}
	}
	{
		DTDAttListDeclaration_t1258293171 * L_43 = V_1;
		DTDAttributeDefinition_t1053410431 * L_44 = V_2;
		NullCheck(L_43);
		DTDAttListDeclaration_Add_m3535383838(L_43, L_44, /*hidden argument*/NULL);
	}

IL_0123:
	{
		DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
		DTDReader_TryExpandPERef_m232461585(__this, /*hidden argument*/NULL);
	}

IL_0131:
	{
		int32_t L_45 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(XmlChar_t3591879093_il2cpp_TypeInfo_var);
		bool L_46 = XmlChar_IsNameChar_m1569088058(NULL /*static, unused*/, L_45, /*hidden argument*/NULL);
		if (L_46)
		{
			goto IL_0093;
		}
	}
	{
		DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
		DTDReader_TryExpandPERef_m232461585(__this, /*hidden argument*/NULL);
		DTDReader_Expect_m1917199594(__this, ((int32_t)62), /*hidden argument*/NULL);
		DTDAttListDeclaration_t1258293171 * L_47 = V_1;
		return L_47;
	}
}
// Mono.Xml.DTDAttributeDefinition System.Xml.DTDReader::ReadAttributeDefinition()
extern Il2CppClass* NotImplementedException_t1091014741_il2cpp_TypeInfo_var;
extern const uint32_t DTDReader_ReadAttributeDefinition_m4289497893_MetadataUsageId;
extern "C"  DTDAttributeDefinition_t1053410431 * DTDReader_ReadAttributeDefinition_m4289497893 (DTDReader_t4257441119 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDReader_ReadAttributeDefinition_m4289497893_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1091014741 * L_0 = (NotImplementedException_t1091014741 *)il2cpp_codegen_object_new(NotImplementedException_t1091014741_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m2063223793(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// Mono.Xml.DTDNotationDeclaration System.Xml.DTDReader::ReadNotationDecl()
extern Il2CppClass* DTDNotationDeclaration_t353707912_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3830013599;
extern Il2CppCodeGenString* _stringLiteral1872182591;
extern Il2CppCodeGenString* _stringLiteral2943446671;
extern const uint32_t DTDReader_ReadNotationDecl_m2258323887_MetadataUsageId;
extern "C"  DTDNotationDeclaration_t353707912 * DTDReader_ReadNotationDecl_m2258323887 (DTDReader_t4257441119 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDReader_ReadNotationDecl_m2258323887_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DTDNotationDeclaration_t353707912 * V_0 = NULL;
	bool V_1 = false;
	{
		DTDObjectModel_t709926554 * L_0 = __this->get_DTD_13();
		DTDNotationDeclaration_t353707912 * L_1 = (DTDNotationDeclaration_t353707912 *)il2cpp_codegen_object_new(DTDNotationDeclaration_t353707912_il2cpp_TypeInfo_var);
		DTDNotationDeclaration__ctor_m2368688479(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0023;
		}
	}
	{
		XmlException_t3490696160 * L_3 = DTDReader_NotWFError_m440433271(__this, _stringLiteral3830013599, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0023:
	{
		DTDReader_TryExpandPERef_m232461585(__this, /*hidden argument*/NULL);
		DTDNotationDeclaration_t353707912 * L_4 = V_0;
		String_t* L_5 = DTDReader_ReadName_m4241357028(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		DTDNotationDeclaration_set_Name_m1844837297(L_4, L_5, /*hidden argument*/NULL);
		DTDNotationDeclaration_t353707912 * L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_6);
		DTDNotationDeclaration_set_Prefix_m4201923082(L_6, L_7, /*hidden argument*/NULL);
		DTDNotationDeclaration_t353707912 * L_8 = V_0;
		DTDNotationDeclaration_t353707912 * L_9 = V_0;
		NullCheck(L_9);
		String_t* L_10 = DTDNotationDeclaration_get_Name_m1124239610(L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		DTDNotationDeclaration_set_LocalName_m4033785648(L_8, L_10, /*hidden argument*/NULL);
		DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
		int32_t L_11 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_11) == ((uint32_t)((int32_t)80)))))
		{
			goto IL_00b9;
		}
	}
	{
		DTDNotationDeclaration_t353707912 * L_12 = V_0;
		String_t* L_13 = DTDReader_ReadPubidLiteral_m1782524944(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		DTDNotationDeclaration_set_PublicId_m2504199736(L_12, L_13, /*hidden argument*/NULL);
		bool L_14 = DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
		V_1 = L_14;
		int32_t L_15 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_15) == ((int32_t)((int32_t)39))))
		{
			goto IL_008e;
		}
	}
	{
		int32_t L_16 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_16) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_00b4;
		}
	}

IL_008e:
	{
		bool L_17 = V_1;
		if (L_17)
		{
			goto IL_00a0;
		}
	}
	{
		XmlException_t3490696160 * L_18 = DTDReader_NotWFError_m440433271(__this, _stringLiteral1872182591, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_18);
	}

IL_00a0:
	{
		DTDNotationDeclaration_t353707912 * L_19 = V_0;
		String_t* L_20 = DTDReader_ReadSystemLiteral_m804658464(__this, (bool)0, /*hidden argument*/NULL);
		NullCheck(L_19);
		DTDNotationDeclaration_set_SystemId_m405738290(L_19, L_20, /*hidden argument*/NULL);
		DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
	}

IL_00b4:
	{
		goto IL_00da;
	}

IL_00b9:
	{
		int32_t L_21 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_21) == ((uint32_t)((int32_t)83)))))
		{
			goto IL_00da;
		}
	}
	{
		DTDNotationDeclaration_t353707912 * L_22 = V_0;
		String_t* L_23 = DTDReader_ReadSystemLiteral_m804658464(__this, (bool)1, /*hidden argument*/NULL);
		NullCheck(L_22);
		DTDNotationDeclaration_set_SystemId_m405738290(L_22, L_23, /*hidden argument*/NULL);
		DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
	}

IL_00da:
	{
		DTDNotationDeclaration_t353707912 * L_24 = V_0;
		NullCheck(L_24);
		String_t* L_25 = DTDNotationDeclaration_get_PublicId_m1745533587(L_24, /*hidden argument*/NULL);
		if (L_25)
		{
			goto IL_00fc;
		}
	}
	{
		DTDNotationDeclaration_t353707912 * L_26 = V_0;
		NullCheck(L_26);
		String_t* L_27 = DTDNotationDeclaration_get_SystemId_m483927897(L_26, /*hidden argument*/NULL);
		if (L_27)
		{
			goto IL_00fc;
		}
	}
	{
		XmlException_t3490696160 * L_28 = DTDReader_NotWFError_m440433271(__this, _stringLiteral2943446671, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_28);
	}

IL_00fc:
	{
		DTDReader_TryExpandPERef_m232461585(__this, /*hidden argument*/NULL);
		DTDReader_Expect_m1917199594(__this, ((int32_t)62), /*hidden argument*/NULL);
		DTDNotationDeclaration_t353707912 * L_29 = V_0;
		return L_29;
	}
}
// System.Void System.Xml.DTDReader::ReadExternalID()
extern Il2CppCodeGenString* _stringLiteral3100517695;
extern const uint32_t DTDReader_ReadExternalID_m1846433116_MetadataUsageId;
extern "C"  void DTDReader_ReadExternalID_m1846433116 (DTDReader_t4257441119 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDReader_ReadExternalID_m1846433116_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)80))) == 0)
		{
			goto IL_0037;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)80))) == 1)
		{
			goto IL_006c;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)80))) == 2)
		{
			goto IL_006c;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)80))) == 3)
		{
			goto IL_0025;
		}
	}
	{
		goto IL_006c;
	}

IL_0025:
	{
		String_t* L_2 = DTDReader_ReadSystemLiteral_m804658464(__this, (bool)1, /*hidden argument*/NULL);
		__this->set_cachedSystemId_12(L_2);
		goto IL_006c;
	}

IL_0037:
	{
		String_t* L_3 = DTDReader_ReadPubidLiteral_m1782524944(__this, /*hidden argument*/NULL);
		__this->set_cachedPublicId_11(L_3);
		bool L_4 = DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_005a;
		}
	}
	{
		XmlException_t3490696160 * L_5 = DTDReader_NotWFError_m440433271(__this, _stringLiteral3100517695, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_005a:
	{
		String_t* L_6 = DTDReader_ReadSystemLiteral_m804658464(__this, (bool)0, /*hidden argument*/NULL);
		__this->set_cachedSystemId_12(L_6);
		goto IL_006c;
	}

IL_006c:
	{
		return;
	}
}
// System.String System.Xml.DTDReader::ReadSystemLiteral(System.Boolean)
extern Il2CppCodeGenString* _stringLiteral2460968495;
extern Il2CppCodeGenString* _stringLiteral3085822955;
extern Il2CppCodeGenString* _stringLiteral1283542072;
extern const uint32_t DTDReader_ReadSystemLiteral_m804658464_MetadataUsageId;
extern "C"  String_t* DTDReader_ReadSystemLiteral_m804658464 (DTDReader_t4257441119 * __this, bool ___expectSYSTEM0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDReader_ReadSystemLiteral_m804658464_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		bool L_0 = ___expectSYSTEM0;
		if (!L_0)
		{
			goto IL_002d;
		}
	}
	{
		DTDReader_Expect_m4055310985(__this, _stringLiteral2460968495, /*hidden argument*/NULL);
		bool L_1 = DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0028;
		}
	}
	{
		XmlException_t3490696160 * L_2 = DTDReader_NotWFError_m440433271(__this, _stringLiteral3085822955, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0028:
	{
		goto IL_0034;
	}

IL_002d:
	{
		DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
	}

IL_0034:
	{
		int32_t L_3 = DTDReader_ReadChar_m1039540782(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		V_1 = 0;
		DTDReader_ClearValueBuffer_m4110694340(__this, /*hidden argument*/NULL);
		goto IL_0070;
	}

IL_0048:
	{
		int32_t L_4 = DTDReader_ReadChar_m1039540782(__this, /*hidden argument*/NULL);
		V_1 = L_4;
		int32_t L_5 = V_1;
		if ((((int32_t)L_5) >= ((int32_t)0)))
		{
			goto IL_0062;
		}
	}
	{
		XmlException_t3490696160 * L_6 = DTDReader_NotWFError_m440433271(__this, _stringLiteral1283542072, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_0062:
	{
		int32_t L_7 = V_1;
		int32_t L_8 = V_0;
		if ((((int32_t)L_7) == ((int32_t)L_8)))
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_9 = V_1;
		DTDReader_AppendValueChar_m1219042272(__this, L_9, /*hidden argument*/NULL);
	}

IL_0070:
	{
		int32_t L_10 = V_1;
		int32_t L_11 = V_0;
		if ((!(((uint32_t)L_10) == ((uint32_t)L_11))))
		{
			goto IL_0048;
		}
	}
	{
		String_t* L_12 = DTDReader_CreateValueString_m2937195813(__this, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.String System.Xml.DTDReader::ReadPubidLiteral()
extern Il2CppClass* XmlChar_t3591879093_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t2778706699_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2370872937;
extern Il2CppCodeGenString* _stringLiteral2403337637;
extern Il2CppCodeGenString* _stringLiteral1283542072;
extern Il2CppCodeGenString* _stringLiteral1168509637;
extern const uint32_t DTDReader_ReadPubidLiteral_m1782524944_MetadataUsageId;
extern "C"  String_t* DTDReader_ReadPubidLiteral_m1782524944 (DTDReader_t4257441119 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDReader_ReadPubidLiteral_m1782524944_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		DTDReader_Expect_m4055310985(__this, _stringLiteral2370872937, /*hidden argument*/NULL);
		bool L_0 = DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0022;
		}
	}
	{
		XmlException_t3490696160 * L_1 = DTDReader_NotWFError_m440433271(__this, _stringLiteral2403337637, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0022:
	{
		int32_t L_2 = DTDReader_ReadChar_m1039540782(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		V_1 = 0;
		DTDReader_ClearValueBuffer_m4110694340(__this, /*hidden argument*/NULL);
		goto IL_0088;
	}

IL_0036:
	{
		int32_t L_3 = DTDReader_ReadChar_m1039540782(__this, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = V_1;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_0050;
		}
	}
	{
		XmlException_t3490696160 * L_5 = DTDReader_NotWFError_m440433271(__this, _stringLiteral1283542072, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0050:
	{
		int32_t L_6 = V_1;
		int32_t L_7 = V_0;
		if ((((int32_t)L_6) == ((int32_t)L_7)))
		{
			goto IL_007a;
		}
	}
	{
		int32_t L_8 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(XmlChar_t3591879093_il2cpp_TypeInfo_var);
		bool L_9 = XmlChar_IsPubidChar_m2010542809(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_007a;
		}
	}
	{
		int32_t L_10 = V_1;
		uint16_t L_11 = ((uint16_t)(((int32_t)((uint16_t)L_10))));
		Il2CppObject * L_12 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_11);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral1168509637, L_12, /*hidden argument*/NULL);
		XmlException_t3490696160 * L_14 = DTDReader_NotWFError_m440433271(__this, L_13, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_14);
	}

IL_007a:
	{
		int32_t L_15 = V_1;
		int32_t L_16 = V_0;
		if ((((int32_t)L_15) == ((int32_t)L_16)))
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_17 = V_1;
		DTDReader_AppendValueChar_m1219042272(__this, L_17, /*hidden argument*/NULL);
	}

IL_0088:
	{
		int32_t L_18 = V_1;
		int32_t L_19 = V_0;
		if ((!(((uint32_t)L_18) == ((uint32_t)L_19))))
		{
			goto IL_0036;
		}
	}
	{
		String_t* L_20 = DTDReader_CreateValueString_m2937195813(__this, /*hidden argument*/NULL);
		return L_20;
	}
}
// System.String System.Xml.DTDReader::ReadName()
extern "C"  String_t* DTDReader_ReadName_m4241357028 (DTDReader_t4257441119 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = DTDReader_ReadNameOrNmToken_m632111980(__this, (bool)0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String System.Xml.DTDReader::ReadNameOrNmToken(System.Boolean)
extern Il2CppClass* XmlChar_t3591879093_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t2778706699_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2233264012;
extern Il2CppCodeGenString* _stringLiteral1130036125;
extern const uint32_t DTDReader_ReadNameOrNmToken_m632111980_MetadataUsageId;
extern "C"  String_t* DTDReader_ReadNameOrNmToken_m632111980 (DTDReader_t4257441119 * __this, bool ___isNameToken0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDReader_ReadNameOrNmToken_m632111980_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = ___isNameToken0;
		if (!L_1)
		{
			goto IL_003b;
		}
	}
	{
		int32_t L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(XmlChar_t3591879093_il2cpp_TypeInfo_var);
		bool L_3 = XmlChar_IsNameChar_m1569088058(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_4 = V_0;
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_5);
		int32_t L_7 = V_0;
		uint16_t L_8 = ((uint16_t)(((int32_t)((uint16_t)L_7))));
		Il2CppObject * L_9 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_8);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Format_m2398979370(NULL /*static, unused*/, _stringLiteral2233264012, L_6, L_9, /*hidden argument*/NULL);
		XmlException_t3490696160 * L_11 = DTDReader_NotWFError_m440433271(__this, L_10, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
	}

IL_0036:
	{
		goto IL_0064;
	}

IL_003b:
	{
		int32_t L_12 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(XmlChar_t3591879093_il2cpp_TypeInfo_var);
		bool L_13 = XmlChar_IsFirstNameChar_m3587024924(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_0064;
		}
	}
	{
		int32_t L_14 = V_0;
		int32_t L_15 = L_14;
		Il2CppObject * L_16 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_15);
		int32_t L_17 = V_0;
		uint16_t L_18 = ((uint16_t)(((int32_t)((uint16_t)L_17))));
		Il2CppObject * L_19 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_18);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = String_Format_m2398979370(NULL /*static, unused*/, _stringLiteral1130036125, L_16, L_19, /*hidden argument*/NULL);
		XmlException_t3490696160 * L_21 = DTDReader_NotWFError_m440433271(__this, L_20, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}

IL_0064:
	{
		__this->set_nameLength_3(0);
		int32_t L_22 = DTDReader_ReadChar_m1039540782(__this, /*hidden argument*/NULL);
		DTDReader_AppendNameChar_m3568759756(__this, L_22, /*hidden argument*/NULL);
		goto IL_0088;
	}

IL_007c:
	{
		int32_t L_23 = DTDReader_ReadChar_m1039540782(__this, /*hidden argument*/NULL);
		DTDReader_AppendNameChar_m3568759756(__this, L_23, /*hidden argument*/NULL);
	}

IL_0088:
	{
		int32_t L_24 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(XmlChar_t3591879093_il2cpp_TypeInfo_var);
		bool L_25 = XmlChar_IsNameChar_m1569088058(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		if (L_25)
		{
			goto IL_007c;
		}
	}
	{
		String_t* L_26 = DTDReader_CreateNameString_m1252119675(__this, /*hidden argument*/NULL);
		return L_26;
	}
}
// System.Void System.Xml.DTDReader::Expect(System.Int32)
extern Il2CppClass* CultureInfo_t3603717042_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t2778706699_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1855766747;
extern const uint32_t DTDReader_Expect_m1917199594_MetadataUsageId;
extern "C"  void DTDReader_Expect_m1917199594 (DTDReader_t4257441119 * __this, int32_t ___expected0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDReader_Expect_m1917199594_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = DTDReader_ReadChar_m1039540782(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		int32_t L_2 = ___expected0;
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3603717042_il2cpp_TypeInfo_var);
		CultureInfo_t3603717042 * L_3 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_4 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)4));
		int32_t L_5 = ___expected0;
		uint16_t L_6 = ((uint16_t)(((int32_t)((uint16_t)L_5))));
		Il2CppObject * L_7 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_7);
		ObjectU5BU5D_t11523773* L_8 = L_4;
		int32_t L_9 = ___expected0;
		int32_t L_10 = L_9;
		Il2CppObject * L_11 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t11523773* L_12 = L_8;
		int32_t L_13 = V_0;
		uint16_t L_14 = ((uint16_t)(((int32_t)((uint16_t)L_13))));
		Il2CppObject * L_15 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 2);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_15);
		ObjectU5BU5D_t11523773* L_16 = L_12;
		int32_t L_17 = V_0;
		int32_t L_18 = L_17;
		Il2CppObject * L_19 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_18);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 3);
		ArrayElementTypeCheck (L_16, L_19);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_19);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = String_Format_m3351777162(NULL /*static, unused*/, L_3, _stringLiteral1855766747, L_16, /*hidden argument*/NULL);
		XmlException_t3490696160 * L_21 = DTDReader_NotWFError_m440433271(__this, L_20, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}

IL_0050:
	{
		return;
	}
}
// System.Void System.Xml.DTDReader::Expect(System.String)
extern "C"  void DTDReader_Expect_m4055310985 (DTDReader_t4257441119 * __this, String_t* ___expected0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		String_t* L_0 = ___expected0;
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m2979997331(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		V_1 = 0;
		goto IL_001f;
	}

IL_000e:
	{
		String_t* L_2 = ___expected0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		uint16_t L_4 = String_get_Chars_m3015341861(L_2, L_3, /*hidden argument*/NULL);
		DTDReader_Expect_m1917199594(__this, L_4, /*hidden argument*/NULL);
		int32_t L_5 = V_1;
		V_1 = ((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_6 = V_1;
		int32_t L_7 = V_0;
		if ((((int32_t)L_6) < ((int32_t)L_7)))
		{
			goto IL_000e;
		}
	}
	{
		return;
	}
}
// System.Void System.Xml.DTDReader::ExpectAfterWhitespace(System.Char)
extern Il2CppClass* XmlChar_t3591879093_il2cpp_TypeInfo_var;
extern Il2CppClass* CultureInfo_t3603717042_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t2778706699_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral369862994;
extern const uint32_t DTDReader_ExpectAfterWhitespace_m2772157595_MetadataUsageId;
extern "C"  void DTDReader_ExpectAfterWhitespace_m2772157595 (DTDReader_t4257441119 * __this, uint16_t ___c0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDReader_ExpectAfterWhitespace_m2772157595_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;

IL_0000:
	{
		int32_t L_0 = DTDReader_ReadChar_m1039540782(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(XmlChar_t3591879093_il2cpp_TypeInfo_var);
		bool L_2 = XmlChar_IsWhitespace_m1427848566(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		goto IL_0000;
	}

IL_0017:
	{
		uint16_t L_3 = ___c0;
		int32_t L_4 = V_0;
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0056;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3603717042_il2cpp_TypeInfo_var);
		CultureInfo_t3603717042 * L_5 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_6 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)3));
		uint16_t L_7 = ___c0;
		uint16_t L_8 = L_7;
		Il2CppObject * L_9 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		ArrayElementTypeCheck (L_6, L_9);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_9);
		ObjectU5BU5D_t11523773* L_10 = L_6;
		int32_t L_11 = V_0;
		uint16_t L_12 = ((uint16_t)(((int32_t)((uint16_t)L_11))));
		Il2CppObject * L_13 = Box(Char_t2778706699_il2cpp_TypeInfo_var, &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 1);
		ArrayElementTypeCheck (L_10, L_13);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_13);
		ObjectU5BU5D_t11523773* L_14 = L_10;
		int32_t L_15 = V_0;
		int32_t L_16 = L_15;
		Il2CppObject * L_17 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_16);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 2);
		ArrayElementTypeCheck (L_14, L_17);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_17);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Format_m3351777162(NULL /*static, unused*/, L_5, _stringLiteral369862994, L_14, /*hidden argument*/NULL);
		XmlException_t3490696160 * L_19 = DTDReader_NotWFError_m440433271(__this, L_18, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_19);
	}

IL_0056:
	{
		goto IL_0060;
	}
	// Dead block : IL_005b: br IL_0000

IL_0060:
	{
		return;
	}
}
// System.Boolean System.Xml.DTDReader::SkipWhitespace()
extern Il2CppClass* XmlChar_t3591879093_il2cpp_TypeInfo_var;
extern const uint32_t DTDReader_SkipWhitespace_m232203876_MetadataUsageId;
extern "C"  bool DTDReader_SkipWhitespace_m232203876 (DTDReader_t4257441119 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDReader_SkipWhitespace_m232203876_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		int32_t L_0 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(XmlChar_t3591879093_il2cpp_TypeInfo_var);
		bool L_1 = XmlChar_IsWhitespace_m1427848566(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0018;
	}

IL_0011:
	{
		DTDReader_ReadChar_m1039540782(__this, /*hidden argument*/NULL);
	}

IL_0018:
	{
		int32_t L_2 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(XmlChar_t3591879093_il2cpp_TypeInfo_var);
		bool L_3 = XmlChar_IsWhitespace_m1427848566(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0011;
		}
	}
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// System.Int32 System.Xml.DTDReader::PeekChar()
extern "C"  int32_t DTDReader_PeekChar_m1731433011 (DTDReader_t4257441119 * __this, const MethodInfo* method)
{
	{
		XmlParserInput_t3311178812 * L_0 = __this->get_currentInput_0();
		NullCheck(L_0);
		int32_t L_1 = XmlParserInput_PeekChar_m455148668(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 System.Xml.DTDReader::ReadChar()
extern "C"  int32_t DTDReader_ReadChar_m1039540782 (DTDReader_t4257441119 * __this, const MethodInfo* method)
{
	{
		XmlParserInput_t3311178812 * L_0 = __this->get_currentInput_0();
		NullCheck(L_0);
		int32_t L_1 = XmlParserInput_ReadChar_m4058223735(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void System.Xml.DTDReader::ReadComment()
extern Il2CppClass* XmlChar_t3591879093_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral555529373;
extern Il2CppCodeGenString* _stringLiteral178728223;
extern const uint32_t DTDReader_ReadComment_m2005393419_MetadataUsageId;
extern "C"  void DTDReader_ReadComment_m2005393419 (DTDReader_t4257441119 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDReader_ReadComment_m2005393419_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		XmlParserInput_t3311178812 * L_0 = __this->get_currentInput_0();
		NullCheck(L_0);
		XmlParserInput_set_AllowTextDecl_m3757679343(L_0, (bool)0, /*hidden argument*/NULL);
		goto IL_0070;
	}

IL_0011:
	{
		int32_t L_1 = DTDReader_ReadChar_m1039540782(__this, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)((int32_t)45)))))
		{
			goto IL_0059;
		}
	}
	{
		int32_t L_3 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)((int32_t)45)))))
		{
			goto IL_0059;
		}
	}
	{
		DTDReader_ReadChar_m1039540782(__this, /*hidden argument*/NULL);
		int32_t L_4 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_4) == ((int32_t)((int32_t)62))))
		{
			goto IL_004d;
		}
	}
	{
		XmlException_t3490696160 * L_5 = DTDReader_NotWFError_m440433271(__this, _stringLiteral555529373, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_004d:
	{
		DTDReader_ReadChar_m1039540782(__this, /*hidden argument*/NULL);
		goto IL_007c;
	}

IL_0059:
	{
		int32_t L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(XmlChar_t3591879093_il2cpp_TypeInfo_var);
		bool L_7 = XmlChar_IsInvalid_m3338941186(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0070;
		}
	}
	{
		XmlException_t3490696160 * L_8 = DTDReader_NotWFError_m440433271(__this, _stringLiteral178728223, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_0070:
	{
		int32_t L_9 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_9) == ((uint32_t)(-1)))))
		{
			goto IL_0011;
		}
	}

IL_007c:
	{
		return;
	}
}
// System.Void System.Xml.DTDReader::ReadProcessingInstruction()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* CultureInfo_t3603717042_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral118807;
extern Il2CppCodeGenString* _stringLiteral67645141;
extern Il2CppCodeGenString* _stringLiteral2995707394;
extern const uint32_t DTDReader_ReadProcessingInstruction_m155897575_MetadataUsageId;
extern "C"  void DTDReader_ReadProcessingInstruction_m155897575 (DTDReader_t4257441119 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDReader_ReadProcessingInstruction_m155897575_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	{
		String_t* L_0 = DTDReader_ReadName_m4241357028(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_1, _stringLiteral118807, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		DTDReader_ReadTextDeclaration_m1514401081(__this, /*hidden argument*/NULL);
		return;
	}

IL_001e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3603717042_il2cpp_TypeInfo_var);
		CultureInfo_t3603717042 * L_3 = CultureInfo_get_InvariantCulture_m764001524(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		CompareInfo_t4023832425 * L_4 = VirtFuncInvoker0< CompareInfo_t4023832425 * >::Invoke(11 /* System.Globalization.CompareInfo System.Globalization.CultureInfo::get_CompareInfo() */, L_3);
		String_t* L_5 = V_0;
		NullCheck(L_4);
		int32_t L_6 = VirtFuncInvoker3< int32_t, String_t*, String_t*, int32_t >::Invoke(6 /* System.Int32 System.Globalization.CompareInfo::Compare(System.String,System.String,System.Globalization.CompareOptions) */, L_4, L_5, _stringLiteral118807, 1);
		if (L_6)
		{
			goto IL_0045;
		}
	}
	{
		XmlException_t3490696160 * L_7 = DTDReader_NotWFError_m440433271(__this, _stringLiteral67645141, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0045:
	{
		XmlParserInput_t3311178812 * L_8 = __this->get_currentInput_0();
		NullCheck(L_8);
		XmlParserInput_set_AllowTextDecl_m3757679343(L_8, (bool)0, /*hidden argument*/NULL);
		bool L_9 = DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0075;
		}
	}
	{
		int32_t L_10 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_10) == ((int32_t)((int32_t)63))))
		{
			goto IL_0075;
		}
	}
	{
		XmlException_t3490696160 * L_11 = DTDReader_NotWFError_m440433271(__this, _stringLiteral2995707394, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
	}

IL_0075:
	{
		goto IL_00a2;
	}

IL_007a:
	{
		int32_t L_12 = DTDReader_ReadChar_m1039540782(__this, /*hidden argument*/NULL);
		V_1 = L_12;
		int32_t L_13 = V_1;
		if ((!(((uint32_t)L_13) == ((uint32_t)((int32_t)63)))))
		{
			goto IL_00a2;
		}
	}
	{
		int32_t L_14 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_14) == ((uint32_t)((int32_t)62)))))
		{
			goto IL_00a2;
		}
	}
	{
		DTDReader_ReadChar_m1039540782(__this, /*hidden argument*/NULL);
		goto IL_00ae;
	}

IL_00a2:
	{
		int32_t L_15 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_15) == ((uint32_t)(-1)))))
		{
			goto IL_007a;
		}
	}

IL_00ae:
	{
		return;
	}
}
// System.Void System.Xml.DTDReader::ReadTextDeclaration()
extern Il2CppClass* CharU5BU5D_t3416858730_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1769504747;
extern Il2CppCodeGenString* _stringLiteral351608024;
extern Il2CppCodeGenString* _stringLiteral1445239994;
extern Il2CppCodeGenString* _stringLiteral4063049545;
extern Il2CppCodeGenString* _stringLiteral48563;
extern Il2CppCodeGenString* _stringLiteral1711222099;
extern Il2CppCodeGenString* _stringLiteral1234965229;
extern Il2CppCodeGenString* _stringLiteral1536623702;
extern Il2CppCodeGenString* _stringLiteral2015;
extern const uint32_t DTDReader_ReadTextDeclaration_m1514401081_MetadataUsageId;
extern "C"  void DTDReader_ReadTextDeclaration_m1514401081 (DTDReader_t4257441119 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDReader_ReadTextDeclaration_m1514401081_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	CharU5BU5D_t3416858730* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		XmlParserInput_t3311178812 * L_0 = __this->get_currentInput_0();
		NullCheck(L_0);
		bool L_1 = XmlParserInput_get_AllowTextDecl_m409413236(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		XmlException_t3490696160 * L_2 = DTDReader_NotWFError_m440433271(__this, _stringLiteral1769504747, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_001c:
	{
		XmlParserInput_t3311178812 * L_3 = __this->get_currentInput_0();
		NullCheck(L_3);
		XmlParserInput_set_AllowTextDecl_m3757679343(L_3, (bool)0, /*hidden argument*/NULL);
		DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
		int32_t L_4 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)((int32_t)118)))))
		{
			goto IL_0111;
		}
	}
	{
		DTDReader_Expect_m4055310985(__this, _stringLiteral351608024, /*hidden argument*/NULL);
		DTDReader_ExpectAfterWhitespace_m2772157595(__this, ((int32_t)61), /*hidden argument*/NULL);
		DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
		int32_t L_5 = DTDReader_ReadChar_m1039540782(__this, /*hidden argument*/NULL);
		V_0 = L_5;
		V_1 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)3));
		V_2 = 0;
		int32_t L_6 = V_0;
		V_4 = L_6;
		int32_t L_7 = V_4;
		if ((((int32_t)L_7) == ((int32_t)((int32_t)34))))
		{
			goto IL_0080;
		}
	}
	{
		int32_t L_8 = V_4;
		if ((((int32_t)L_8) == ((int32_t)((int32_t)39))))
		{
			goto IL_0080;
		}
	}
	{
		goto IL_0105;
	}

IL_0080:
	{
		goto IL_00e6;
	}

IL_0085:
	{
		int32_t L_9 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_9) == ((uint32_t)(-1)))))
		{
			goto IL_009d;
		}
	}
	{
		XmlException_t3490696160 * L_10 = DTDReader_NotWFError_m440433271(__this, _stringLiteral1445239994, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
	}

IL_009d:
	{
		int32_t L_11 = V_2;
		if ((!(((uint32_t)L_11) == ((uint32_t)3))))
		{
			goto IL_00b0;
		}
	}
	{
		XmlException_t3490696160 * L_12 = DTDReader_NotWFError_m440433271(__this, _stringLiteral4063049545, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12);
	}

IL_00b0:
	{
		CharU5BU5D_t3416858730* L_13 = V_1;
		int32_t L_14 = V_2;
		int32_t L_15 = DTDReader_ReadChar_m1039540782(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(L_14), (uint16_t)(((int32_t)((uint16_t)L_15))));
		int32_t L_16 = V_2;
		V_2 = ((int32_t)((int32_t)L_16+(int32_t)1));
		int32_t L_17 = V_2;
		if ((!(((uint32_t)L_17) == ((uint32_t)3))))
		{
			goto IL_00e6;
		}
	}
	{
		CharU5BU5D_t3416858730* L_18 = V_1;
		String_t* L_19 = String_CreateString_m578950865(NULL, L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_20 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_19, _stringLiteral48563, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00e6;
		}
	}
	{
		XmlException_t3490696160 * L_21 = DTDReader_NotWFError_m440433271(__this, _stringLiteral4063049545, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}

IL_00e6:
	{
		int32_t L_22 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		int32_t L_23 = V_0;
		if ((!(((uint32_t)L_22) == ((uint32_t)L_23))))
		{
			goto IL_0085;
		}
	}
	{
		DTDReader_ReadChar_m1039540782(__this, /*hidden argument*/NULL);
		DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
		goto IL_0111;
	}

IL_0105:
	{
		XmlException_t3490696160 * L_24 = DTDReader_NotWFError_m440433271(__this, _stringLiteral1445239994, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_24);
	}

IL_0111:
	{
		int32_t L_25 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_25) == ((uint32_t)((int32_t)101)))))
		{
			goto IL_01a6;
		}
	}
	{
		DTDReader_Expect_m4055310985(__this, _stringLiteral1711222099, /*hidden argument*/NULL);
		DTDReader_ExpectAfterWhitespace_m2772157595(__this, ((int32_t)61), /*hidden argument*/NULL);
		DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
		int32_t L_26 = DTDReader_ReadChar_m1039540782(__this, /*hidden argument*/NULL);
		V_3 = L_26;
		int32_t L_27 = V_3;
		V_4 = L_27;
		int32_t L_28 = V_4;
		if ((((int32_t)L_28) == ((int32_t)((int32_t)34))))
		{
			goto IL_0159;
		}
	}
	{
		int32_t L_29 = V_4;
		if ((((int32_t)L_29) == ((int32_t)((int32_t)39))))
		{
			goto IL_0159;
		}
	}
	{
		goto IL_0195;
	}

IL_0159:
	{
		goto IL_0176;
	}

IL_015e:
	{
		int32_t L_30 = DTDReader_ReadChar_m1039540782(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_30) == ((uint32_t)(-1)))))
		{
			goto IL_0176;
		}
	}
	{
		XmlException_t3490696160 * L_31 = DTDReader_NotWFError_m440433271(__this, _stringLiteral1234965229, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_31);
	}

IL_0176:
	{
		int32_t L_32 = DTDReader_PeekChar_m1731433011(__this, /*hidden argument*/NULL);
		int32_t L_33 = V_3;
		if ((!(((uint32_t)L_32) == ((uint32_t)L_33))))
		{
			goto IL_015e;
		}
	}
	{
		DTDReader_ReadChar_m1039540782(__this, /*hidden argument*/NULL);
		DTDReader_SkipWhitespace_m232203876(__this, /*hidden argument*/NULL);
		goto IL_01a1;
	}

IL_0195:
	{
		XmlException_t3490696160 * L_34 = DTDReader_NotWFError_m440433271(__this, _stringLiteral1234965229, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_34);
	}

IL_01a1:
	{
		goto IL_01b2;
	}

IL_01a6:
	{
		XmlException_t3490696160 * L_35 = DTDReader_NotWFError_m440433271(__this, _stringLiteral1536623702, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_35);
	}

IL_01b2:
	{
		DTDReader_Expect_m4055310985(__this, _stringLiteral2015, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.DTDReader::AppendNameChar(System.Int32)
extern "C"  void DTDReader_AppendNameChar_m3568759756 (DTDReader_t4257441119 * __this, int32_t ___ch0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		DTDReader_CheckNameCapacity_m4288055279(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___ch0;
		if ((((int32_t)L_0) >= ((int32_t)((int32_t)65535))))
		{
			goto IL_0030;
		}
	}
	{
		CharU5BU5D_t3416858730* L_1 = __this->get_nameBuffer_2();
		int32_t L_2 = __this->get_nameLength_3();
		int32_t L_3 = L_2;
		V_0 = L_3;
		__this->set_nameLength_3(((int32_t)((int32_t)L_3+(int32_t)1)));
		int32_t L_4 = V_0;
		int32_t L_5 = ___ch0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_4), (uint16_t)(((int32_t)((uint16_t)L_5))));
		goto IL_0084;
	}

IL_0030:
	{
		CharU5BU5D_t3416858730* L_6 = __this->get_nameBuffer_2();
		int32_t L_7 = __this->get_nameLength_3();
		int32_t L_8 = L_7;
		V_0 = L_8;
		__this->set_nameLength_3(((int32_t)((int32_t)L_8+(int32_t)1)));
		int32_t L_9 = V_0;
		int32_t L_10 = ___ch0;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_9);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_9), (uint16_t)(((int32_t)((uint16_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_10/(int32_t)((int32_t)65536)))+(int32_t)((int32_t)55296)))-(int32_t)1))))));
		DTDReader_CheckNameCapacity_m4288055279(__this, /*hidden argument*/NULL);
		CharU5BU5D_t3416858730* L_11 = __this->get_nameBuffer_2();
		int32_t L_12 = __this->get_nameLength_3();
		int32_t L_13 = L_12;
		V_0 = L_13;
		__this->set_nameLength_3(((int32_t)((int32_t)L_13+(int32_t)1)));
		int32_t L_14 = V_0;
		int32_t L_15 = ___ch0;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_14);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(L_14), (uint16_t)(((int32_t)((uint16_t)((int32_t)((int32_t)((int32_t)((int32_t)L_15%(int32_t)((int32_t)65536)))+(int32_t)((int32_t)56320)))))));
	}

IL_0084:
	{
		return;
	}
}
// System.Void System.Xml.DTDReader::CheckNameCapacity()
extern Il2CppClass* CharU5BU5D_t3416858730_il2cpp_TypeInfo_var;
extern const uint32_t DTDReader_CheckNameCapacity_m4288055279_MetadataUsageId;
extern "C"  void DTDReader_CheckNameCapacity_m4288055279 (DTDReader_t4257441119 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDReader_CheckNameCapacity_m4288055279_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	CharU5BU5D_t3416858730* V_0 = NULL;
	{
		int32_t L_0 = __this->get_nameLength_3();
		int32_t L_1 = __this->get_nameCapacity_4();
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0049;
		}
	}
	{
		int32_t L_2 = __this->get_nameCapacity_4();
		__this->set_nameCapacity_4(((int32_t)((int32_t)L_2*(int32_t)2)));
		CharU5BU5D_t3416858730* L_3 = __this->get_nameBuffer_2();
		V_0 = L_3;
		int32_t L_4 = __this->get_nameCapacity_4();
		__this->set_nameBuffer_2(((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)L_4)));
		CharU5BU5D_t3416858730* L_5 = V_0;
		CharU5BU5D_t3416858730* L_6 = __this->get_nameBuffer_2();
		int32_t L_7 = __this->get_nameLength_3();
		Array_Copy_m3799309042(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, (Il2CppArray *)(Il2CppArray *)L_6, L_7, /*hidden argument*/NULL);
	}

IL_0049:
	{
		return;
	}
}
// System.String System.Xml.DTDReader::CreateNameString()
extern "C"  String_t* DTDReader_CreateNameString_m1252119675 (DTDReader_t4257441119 * __this, const MethodInfo* method)
{
	{
		DTDObjectModel_t709926554 * L_0 = __this->get_DTD_13();
		NullCheck(L_0);
		XmlNameTable_t3232213908 * L_1 = DTDObjectModel_get_NameTable_m2752706194(L_0, /*hidden argument*/NULL);
		CharU5BU5D_t3416858730* L_2 = __this->get_nameBuffer_2();
		int32_t L_3 = __this->get_nameLength_3();
		NullCheck(L_1);
		String_t* L_4 = VirtFuncInvoker3< String_t*, CharU5BU5D_t3416858730*, int32_t, int32_t >::Invoke(5 /* System.String System.Xml.XmlNameTable::Add(System.Char[],System.Int32,System.Int32) */, L_1, L_2, 0, L_3);
		return L_4;
	}
}
// System.Void System.Xml.DTDReader::AppendValueChar(System.Int32)
extern Il2CppClass* XmlException_t3490696160_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1601962051;
extern const uint32_t DTDReader_AppendValueChar_m1219042272_MetadataUsageId;
extern "C"  void DTDReader_AppendValueChar_m1219042272 (DTDReader_t4257441119 * __this, int32_t ___ch0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDReader_AppendValueChar_m1219042272_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___ch0;
		if ((((int32_t)L_0) >= ((int32_t)((int32_t)65536))))
		{
			goto IL_001a;
		}
	}
	{
		StringBuilder_t3822575854 * L_1 = __this->get_valueBuffer_5();
		int32_t L_2 = ___ch0;
		NullCheck(L_1);
		StringBuilder_Append_m2143093878(L_1, (((int32_t)((uint16_t)L_2))), /*hidden argument*/NULL);
		return;
	}

IL_001a:
	{
		int32_t L_3 = ___ch0;
		if ((((int32_t)L_3) <= ((int32_t)((int32_t)1114111))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Xml.DTDReader::get_LineNumber() */, __this);
		int32_t L_5 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 System.Xml.DTDReader::get_LinePosition() */, __this);
		XmlException_t3490696160 * L_6 = (XmlException_t3490696160 *)il2cpp_codegen_object_new(XmlException_t3490696160_il2cpp_TypeInfo_var);
		XmlException__ctor_m1096610055(L_6, _stringLiteral1601962051, (Exception_t1967233988 *)NULL, L_4, L_5, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_003d:
	{
		int32_t L_7 = ___ch0;
		V_0 = ((int32_t)((int32_t)L_7-(int32_t)((int32_t)65536)));
		StringBuilder_t3822575854 * L_8 = __this->get_valueBuffer_5();
		int32_t L_9 = V_0;
		NullCheck(L_8);
		StringBuilder_Append_m2143093878(L_8, (((int32_t)((uint16_t)((int32_t)((int32_t)((int32_t)((int32_t)L_9>>(int32_t)((int32_t)10)))+(int32_t)((int32_t)55296)))))), /*hidden argument*/NULL);
		StringBuilder_t3822575854 * L_10 = __this->get_valueBuffer_5();
		int32_t L_11 = V_0;
		NullCheck(L_10);
		StringBuilder_Append_m2143093878(L_10, (((int32_t)((uint16_t)((int32_t)((int32_t)((int32_t)((int32_t)L_11&(int32_t)((int32_t)1023)))+(int32_t)((int32_t)56320)))))), /*hidden argument*/NULL);
		return;
	}
}
// System.String System.Xml.DTDReader::CreateValueString()
extern "C"  String_t* DTDReader_CreateValueString_m2937195813 (DTDReader_t4257441119 * __this, const MethodInfo* method)
{
	{
		StringBuilder_t3822575854 * L_0 = __this->get_valueBuffer_5();
		NullCheck(L_0);
		String_t* L_1 = StringBuilder_ToString_m350379841(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void System.Xml.DTDReader::ClearValueBuffer()
extern "C"  void DTDReader_ClearValueBuffer_m4110694340 (DTDReader_t4257441119 * __this, const MethodInfo* method)
{
	{
		StringBuilder_t3822575854 * L_0 = __this->get_valueBuffer_5();
		NullCheck(L_0);
		StringBuilder_set_Length_m1952332172(L_0, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.DTDReader::PushParserInput(System.String)
extern const Il2CppType* Stream_t219029575_0_0_0_var;
extern Il2CppClass* Uri_t2776692961_il2cpp_TypeInfo_var;
extern Il2CppClass* UriFormatException_t1145000641_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlParserInput_t3311178812_il2cpp_TypeInfo_var;
extern Il2CppClass* MemoryStream_t2881531048_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Stream_t219029575_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t58506160_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlStreamReader_t1190433730_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppClass* XmlException_t3490696160_il2cpp_TypeInfo_var;
extern Il2CppClass* StringReader_t2229325051_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2210596984;
extern Il2CppCodeGenString* _stringLiteral1067425733;
extern Il2CppCodeGenString* _stringLiteral1038;
extern const uint32_t DTDReader_PushParserInput_m3654728911_MetadataUsageId;
extern "C"  void DTDReader_PushParserInput_m3654728911 (DTDReader_t4257441119 * __this, String_t* ___url0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDReader_PushParserInput_m3654728911_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Uri_t2776692961 * V_0 = NULL;
	Uri_t2776692961 * V_1 = NULL;
	String_t* V_2 = NULL;
	XmlParserInput_t3311178812 * V_3 = NULL;
	ObjectU5BU5D_t11523773* V_4 = NULL;
	int32_t V_5 = 0;
	Stream_t219029575 * V_6 = NULL;
	MemoryStream_t2881531048 * V_7 = NULL;
	int32_t V_8 = 0;
	ByteU5BU5D_t58506160* V_9 = NULL;
	Exception_t1967233988 * V_10 = NULL;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	String_t* V_13 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	Uri_t2776692961 * G_B10_0 = NULL;
	String_t* G_B13_0 = NULL;
	int32_t G_B27_0 = 0;
	int32_t G_B30_0 = 0;
	String_t* G_B33_0 = NULL;
	{
		V_0 = (Uri_t2776692961 *)NULL;
	}

IL_0002:
	try
	{ // begin try (depth: 1)
		{
			DTDObjectModel_t709926554 * L_0 = __this->get_DTD_13();
			NullCheck(L_0);
			String_t* L_1 = DTDObjectModel_get_BaseURI_m398146012(L_0, /*hidden argument*/NULL);
			if (!L_1)
			{
				goto IL_0039;
			}
		}

IL_0012:
		{
			DTDObjectModel_t709926554 * L_2 = __this->get_DTD_13();
			NullCheck(L_2);
			String_t* L_3 = DTDObjectModel_get_BaseURI_m398146012(L_2, /*hidden argument*/NULL);
			NullCheck(L_3);
			int32_t L_4 = String_get_Length_m2979997331(L_3, /*hidden argument*/NULL);
			if ((((int32_t)L_4) <= ((int32_t)0)))
			{
				goto IL_0039;
			}
		}

IL_0028:
		{
			DTDObjectModel_t709926554 * L_5 = __this->get_DTD_13();
			NullCheck(L_5);
			String_t* L_6 = DTDObjectModel_get_BaseURI_m398146012(L_5, /*hidden argument*/NULL);
			Uri_t2776692961 * L_7 = (Uri_t2776692961 *)il2cpp_codegen_object_new(Uri_t2776692961_il2cpp_TypeInfo_var);
			Uri__ctor_m1721267859(L_7, L_6, /*hidden argument*/NULL);
			V_0 = L_7;
		}

IL_0039:
		{
			goto IL_0044;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (UriFormatException_t1145000641_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_003e;
		throw e;
	}

CATCH_003e:
	{ // begin catch(System.UriFormatException)
		goto IL_0044;
	} // end catch (depth: 1)

IL_0044:
	{
		String_t* L_8 = ___url0;
		if (!L_8)
		{
			goto IL_006d;
		}
	}
	{
		String_t* L_9 = ___url0;
		NullCheck(L_9);
		int32_t L_10 = String_get_Length_m2979997331(L_9, /*hidden argument*/NULL);
		if ((((int32_t)L_10) <= ((int32_t)0)))
		{
			goto IL_006d;
		}
	}
	{
		DTDObjectModel_t709926554 * L_11 = __this->get_DTD_13();
		NullCheck(L_11);
		XmlResolver_t2502213349 * L_12 = DTDObjectModel_get_Resolver_m1268564320(L_11, /*hidden argument*/NULL);
		Uri_t2776692961 * L_13 = V_0;
		String_t* L_14 = ___url0;
		NullCheck(L_12);
		Uri_t2776692961 * L_15 = VirtFuncInvoker2< Uri_t2776692961 *, Uri_t2776692961 *, String_t* >::Invoke(5 /* System.Uri System.Xml.XmlResolver::ResolveUri(System.Uri,System.String) */, L_12, L_13, L_14);
		G_B10_0 = L_15;
		goto IL_006e;
	}

IL_006d:
	{
		Uri_t2776692961 * L_16 = V_0;
		G_B10_0 = L_16;
	}

IL_006e:
	{
		V_1 = G_B10_0;
		Uri_t2776692961 * L_17 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t2776692961_il2cpp_TypeInfo_var);
		bool L_18 = Uri_op_Inequality_m2899852498(NULL /*static, unused*/, L_17, (Uri_t2776692961 *)NULL, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_0086;
		}
	}
	{
		Uri_t2776692961 * L_19 = V_1;
		NullCheck(L_19);
		String_t* L_20 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Uri::ToString() */, L_19);
		G_B13_0 = L_20;
		goto IL_008b;
	}

IL_0086:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B13_0 = L_21;
	}

IL_008b:
	{
		V_2 = G_B13_0;
		Stack_t1623036922 * L_22 = __this->get_parserInputStack_1();
		NullCheck(L_22);
		ObjectU5BU5D_t11523773* L_23 = VirtFuncInvoker0< ObjectU5BU5D_t11523773* >::Invoke(21 /* System.Object[] System.Collections.Stack::ToArray() */, L_22);
		V_4 = L_23;
		V_5 = 0;
		goto IL_00d5;
	}

IL_00a1:
	{
		ObjectU5BU5D_t11523773* L_24 = V_4;
		int32_t L_25 = V_5;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, L_25);
		int32_t L_26 = L_25;
		V_3 = ((XmlParserInput_t3311178812 *)CastclassClass(((L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_26))), XmlParserInput_t3311178812_il2cpp_TypeInfo_var));
		XmlParserInput_t3311178812 * L_27 = V_3;
		NullCheck(L_27);
		String_t* L_28 = XmlParserInput_get_BaseURI_m2378535050(L_27, /*hidden argument*/NULL);
		String_t* L_29 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_30 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_28, L_29, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_00cf;
		}
	}
	{
		String_t* L_31 = ___url0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral2210596984, L_31, /*hidden argument*/NULL);
		XmlException_t3490696160 * L_33 = DTDReader_NotWFError_m440433271(__this, L_32, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_33);
	}

IL_00cf:
	{
		int32_t L_34 = V_5;
		V_5 = ((int32_t)((int32_t)L_34+(int32_t)1));
	}

IL_00d5:
	{
		int32_t L_35 = V_5;
		ObjectU5BU5D_t11523773* L_36 = V_4;
		NullCheck(L_36);
		if ((((int32_t)L_35) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_36)->max_length)))))))
		{
			goto IL_00a1;
		}
	}
	{
		Stack_t1623036922 * L_37 = __this->get_parserInputStack_1();
		XmlParserInput_t3311178812 * L_38 = __this->get_currentInput_0();
		NullCheck(L_37);
		VirtActionInvoker1< Il2CppObject * >::Invoke(20 /* System.Void System.Collections.Stack::Push(System.Object) */, L_37, L_38);
		V_6 = (Stream_t219029575 *)NULL;
		MemoryStream_t2881531048 * L_39 = (MemoryStream_t2881531048 *)il2cpp_codegen_object_new(MemoryStream_t2881531048_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m3603177736(L_39, /*hidden argument*/NULL);
		V_7 = L_39;
	}

IL_00fb:
	try
	{ // begin try (depth: 1)
		{
			DTDObjectModel_t709926554 * L_40 = __this->get_DTD_13();
			NullCheck(L_40);
			XmlResolver_t2502213349 * L_41 = DTDObjectModel_get_Resolver_m1268564320(L_40, /*hidden argument*/NULL);
			Uri_t2776692961 * L_42 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
			Type_t * L_43 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Stream_t219029575_0_0_0_var), /*hidden argument*/NULL);
			NullCheck(L_41);
			Il2CppObject * L_44 = VirtFuncInvoker3< Il2CppObject *, Uri_t2776692961 *, String_t*, Type_t * >::Invoke(4 /* System.Object System.Xml.XmlResolver::GetEntity(System.Uri,System.String,System.Type) */, L_41, L_42, (String_t*)NULL, L_43);
			V_6 = ((Stream_t219029575 *)IsInstClass(L_44, Stream_t219029575_il2cpp_TypeInfo_var));
			V_9 = ((ByteU5BU5D_t58506160*)SZArrayNew(ByteU5BU5D_t58506160_il2cpp_TypeInfo_var, (uint32_t)((int32_t)4096)));
		}

IL_012a:
		{
			Stream_t219029575 * L_45 = V_6;
			ByteU5BU5D_t58506160* L_46 = V_9;
			ByteU5BU5D_t58506160* L_47 = V_9;
			NullCheck(L_47);
			NullCheck(L_45);
			int32_t L_48 = VirtFuncInvoker3< int32_t, ByteU5BU5D_t58506160*, int32_t, int32_t >::Invoke(19 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_45, L_46, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length)))));
			V_8 = L_48;
			MemoryStream_t2881531048 * L_49 = V_7;
			ByteU5BU5D_t58506160* L_50 = V_9;
			int32_t L_51 = V_8;
			NullCheck(L_49);
			VirtActionInvoker3< ByteU5BU5D_t58506160*, int32_t, int32_t >::Invoke(23 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_49, L_50, 0, L_51);
			int32_t L_52 = V_8;
			if ((((int32_t)L_52) > ((int32_t)0)))
			{
				goto IL_012a;
			}
		}

IL_014e:
		{
			Stream_t219029575 * L_53 = V_6;
			NullCheck(L_53);
			VirtActionInvoker0::Invoke(13 /* System.Void System.IO.Stream::Close() */, L_53);
			MemoryStream_t2881531048 * L_54 = V_7;
			NullCheck(L_54);
			VirtActionInvoker1< int64_t >::Invoke(11 /* System.Void System.IO.MemoryStream::set_Position(System.Int64) */, L_54, (((int64_t)((int64_t)0))));
			MemoryStream_t2881531048 * L_55 = V_7;
			XmlStreamReader_t1190433730 * L_56 = (XmlStreamReader_t1190433730 *)il2cpp_codegen_object_new(XmlStreamReader_t1190433730_il2cpp_TypeInfo_var);
			XmlStreamReader__ctor_m2508489112(L_56, L_55, /*hidden argument*/NULL);
			String_t* L_57 = V_2;
			XmlParserInput_t3311178812 * L_58 = (XmlParserInput_t3311178812 *)il2cpp_codegen_object_new(XmlParserInput_t3311178812_il2cpp_TypeInfo_var);
			XmlParserInput__ctor_m1070676826(L_58, L_56, L_57, /*hidden argument*/NULL);
			__this->set_currentInput_0(L_58);
			goto IL_0223;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0176;
		throw e;
	}

CATCH_0176:
	{ // begin catch(System.Exception)
		{
			V_10 = ((Exception_t1967233988 *)__exception_local);
			Stream_t219029575 * L_59 = V_6;
			if (!L_59)
			{
				goto IL_0186;
			}
		}

IL_017f:
		{
			Stream_t219029575 * L_60 = V_6;
			NullCheck(L_60);
			VirtActionInvoker0::Invoke(13 /* System.Void System.IO.Stream::Close() */, L_60);
		}

IL_0186:
		{
			XmlParserInput_t3311178812 * L_61 = __this->get_currentInput_0();
			if (L_61)
			{
				goto IL_0197;
			}
		}

IL_0191:
		{
			G_B27_0 = 0;
			goto IL_01a2;
		}

IL_0197:
		{
			XmlParserInput_t3311178812 * L_62 = __this->get_currentInput_0();
			NullCheck(L_62);
			int32_t L_63 = XmlParserInput_get_LineNumber_m2726813009(L_62, /*hidden argument*/NULL);
			G_B27_0 = L_63;
		}

IL_01a2:
		{
			V_11 = G_B27_0;
			XmlParserInput_t3311178812 * L_64 = __this->get_currentInput_0();
			if (L_64)
			{
				goto IL_01b5;
			}
		}

IL_01af:
		{
			G_B30_0 = 0;
			goto IL_01c0;
		}

IL_01b5:
		{
			XmlParserInput_t3311178812 * L_65 = __this->get_currentInput_0();
			NullCheck(L_65);
			int32_t L_66 = XmlParserInput_get_LinePosition_m2311843185(L_65, /*hidden argument*/NULL);
			G_B30_0 = L_66;
		}

IL_01c0:
		{
			V_12 = G_B30_0;
			XmlParserInput_t3311178812 * L_67 = __this->get_currentInput_0();
			if (L_67)
			{
				goto IL_01d7;
			}
		}

IL_01cd:
		{
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_68 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
			G_B33_0 = L_68;
			goto IL_01e2;
		}

IL_01d7:
		{
			XmlParserInput_t3311178812 * L_69 = __this->get_currentInput_0();
			NullCheck(L_69);
			String_t* L_70 = XmlParserInput_get_BaseURI_m2378535050(L_69, /*hidden argument*/NULL);
			G_B33_0 = L_70;
		}

IL_01e2:
		{
			V_13 = G_B33_0;
			String_t* L_71 = ___url0;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_72 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral1067425733, L_71, _stringLiteral1038, /*hidden argument*/NULL);
			int32_t L_73 = V_11;
			int32_t L_74 = V_12;
			String_t* L_75 = V_13;
			Exception_t1967233988 * L_76 = V_10;
			XmlException_t3490696160 * L_77 = (XmlException_t3490696160 *)il2cpp_codegen_object_new(XmlException_t3490696160_il2cpp_TypeInfo_var);
			XmlException__ctor_m2245560573(L_77, L_72, L_73, L_74, NULL, L_75, L_76, /*hidden argument*/NULL);
			DTDReader_HandleError_m2636292706(__this, L_77, /*hidden argument*/NULL);
			String_t* L_78 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
			StringReader_t2229325051 * L_79 = (StringReader_t2229325051 *)il2cpp_codegen_object_new(StringReader_t2229325051_il2cpp_TypeInfo_var);
			StringReader__ctor_m1181104909(L_79, L_78, /*hidden argument*/NULL);
			String_t* L_80 = V_2;
			XmlParserInput_t3311178812 * L_81 = (XmlParserInput_t3311178812 *)il2cpp_codegen_object_new(XmlParserInput_t3311178812_il2cpp_TypeInfo_var);
			XmlParserInput__ctor_m1070676826(L_81, L_79, L_80, /*hidden argument*/NULL);
			__this->set_currentInput_0(L_81);
			goto IL_0223;
		}
	} // end catch (depth: 1)

IL_0223:
	{
		return;
	}
}
// System.Void System.Xml.DTDReader::PopParserInput()
extern Il2CppClass* XmlParserInput_t3311178812_il2cpp_TypeInfo_var;
extern const uint32_t DTDReader_PopParserInput_m2654799194_MetadataUsageId;
extern "C"  void DTDReader_PopParserInput_m2654799194 (DTDReader_t4257441119 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DTDReader_PopParserInput_m2654799194_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		XmlParserInput_t3311178812 * L_0 = __this->get_currentInput_0();
		NullCheck(L_0);
		XmlParserInput_Close_m3764035949(L_0, /*hidden argument*/NULL);
		Stack_t1623036922 * L_1 = __this->get_parserInputStack_1();
		NullCheck(L_1);
		Il2CppObject * L_2 = VirtFuncInvoker0< Il2CppObject * >::Invoke(19 /* System.Object System.Collections.Stack::Pop() */, L_1);
		__this->set_currentInput_0(((XmlParserInput_t3311178812 *)IsInstClass(L_2, XmlParserInput_t3311178812_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void System.Xml.DTDReader::HandleError(System.Xml.XmlException)
extern "C"  void DTDReader_HandleError_m2636292706 (DTDReader_t4257441119 * __this, XmlException_t3490696160 * ___ex0, const MethodInfo* method)
{
	{
		DTDObjectModel_t709926554 * L_0 = __this->get_DTD_13();
		XmlException_t3490696160 * L_1 = ___ex0;
		NullCheck(L_0);
		DTDObjectModel_AddError_m2755935394(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Xml.MonoFIXAttribute::.ctor(System.String)
extern "C"  void MonoFIXAttribute__ctor_m3514877889 (MonoFIXAttribute_t792940146 * __this, String_t* ___comment0, const MethodInfo* method)
{
	{
		Attribute__ctor_m2985353781(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___comment0;
		__this->set_comment_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
