﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// <>__AnonType2`2<System.Object,System.Object>
struct U3CU3E__AnonType2_2_t489333003;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void <>__AnonType2`2<System.Object,System.Object>::.ctor(<hidden>__T,<showTable>__T)
extern "C"  void U3CU3E__AnonType2_2__ctor_m2429388114_gshared (U3CU3E__AnonType2_2_t489333003 * __this, Il2CppObject * ___hidden0, Il2CppObject * ___showTable1, const MethodInfo* method);
#define U3CU3E__AnonType2_2__ctor_m2429388114(__this, ___hidden0, ___showTable1, method) ((  void (*) (U3CU3E__AnonType2_2_t489333003 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))U3CU3E__AnonType2_2__ctor_m2429388114_gshared)(__this, ___hidden0, ___showTable1, method)
// <hidden>__T <>__AnonType2`2<System.Object,System.Object>::get_hidden()
extern "C"  Il2CppObject * U3CU3E__AnonType2_2_get_hidden_m3021139873_gshared (U3CU3E__AnonType2_2_t489333003 * __this, const MethodInfo* method);
#define U3CU3E__AnonType2_2_get_hidden_m3021139873(__this, method) ((  Il2CppObject * (*) (U3CU3E__AnonType2_2_t489333003 *, const MethodInfo*))U3CU3E__AnonType2_2_get_hidden_m3021139873_gshared)(__this, method)
// <showTable>__T <>__AnonType2`2<System.Object,System.Object>::get_showTable()
extern "C"  Il2CppObject * U3CU3E__AnonType2_2_get_showTable_m922775713_gshared (U3CU3E__AnonType2_2_t489333003 * __this, const MethodInfo* method);
#define U3CU3E__AnonType2_2_get_showTable_m922775713(__this, method) ((  Il2CppObject * (*) (U3CU3E__AnonType2_2_t489333003 *, const MethodInfo*))U3CU3E__AnonType2_2_get_showTable_m922775713_gshared)(__this, method)
// System.Boolean <>__AnonType2`2<System.Object,System.Object>::Equals(System.Object)
extern "C"  bool U3CU3E__AnonType2_2_Equals_m1236724810_gshared (U3CU3E__AnonType2_2_t489333003 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define U3CU3E__AnonType2_2_Equals_m1236724810(__this, ___obj0, method) ((  bool (*) (U3CU3E__AnonType2_2_t489333003 *, Il2CppObject *, const MethodInfo*))U3CU3E__AnonType2_2_Equals_m1236724810_gshared)(__this, ___obj0, method)
// System.Int32 <>__AnonType2`2<System.Object,System.Object>::GetHashCode()
extern "C"  int32_t U3CU3E__AnonType2_2_GetHashCode_m612527214_gshared (U3CU3E__AnonType2_2_t489333003 * __this, const MethodInfo* method);
#define U3CU3E__AnonType2_2_GetHashCode_m612527214(__this, method) ((  int32_t (*) (U3CU3E__AnonType2_2_t489333003 *, const MethodInfo*))U3CU3E__AnonType2_2_GetHashCode_m612527214_gshared)(__this, method)
// System.String <>__AnonType2`2<System.Object,System.Object>::ToString()
extern "C"  String_t* U3CU3E__AnonType2_2_ToString_m1423063270_gshared (U3CU3E__AnonType2_2_t489333003 * __this, const MethodInfo* method);
#define U3CU3E__AnonType2_2_ToString_m1423063270(__this, method) ((  String_t* (*) (U3CU3E__AnonType2_2_t489333003 *, const MethodInfo*))U3CU3E__AnonType2_2_ToString_m1423063270_gshared)(__this, method)
