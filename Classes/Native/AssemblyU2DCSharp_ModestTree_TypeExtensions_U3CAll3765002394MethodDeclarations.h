﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ModestTree.TypeExtensions/<AllAttributes>c__AnonStorey16D
struct U3CAllAttributesU3Ec__AnonStorey16D_t3765002394;
// System.Attribute
struct Attribute_t498693649;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Attribute498693649.h"

// System.Void ModestTree.TypeExtensions/<AllAttributes>c__AnonStorey16D::.ctor()
extern "C"  void U3CAllAttributesU3Ec__AnonStorey16D__ctor_m2989170470 (U3CAllAttributesU3Ec__AnonStorey16D_t3765002394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ModestTree.TypeExtensions/<AllAttributes>c__AnonStorey16D::<>m__28B(System.Attribute)
extern "C"  bool U3CAllAttributesU3Ec__AnonStorey16D_U3CU3Em__28B_m3317124830 (U3CAllAttributesU3Ec__AnonStorey16D_t3765002394 * __this, Attribute_t498693649 * ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
