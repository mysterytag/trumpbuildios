﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Examples.Sample05_ConvertFromCoroutine/<GetWWW>c__AnonStorey30
struct  U3CGetWWWU3Ec__AnonStorey30_t1804159459  : public Il2CppObject
{
public:
	// System.String UniRx.Examples.Sample05_ConvertFromCoroutine/<GetWWW>c__AnonStorey30::url
	String_t* ___url_0;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CGetWWWU3Ec__AnonStorey30_t1804159459, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier(&___url_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
