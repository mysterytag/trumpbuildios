﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen2116400401.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Nullable`1<UnityEngine.Vector3>::.ctor(T)
extern "C"  void Nullable_1__ctor_m2012110062_gshared (Nullable_1_t2116400401 * __this, Vector3_t3525329789  ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m2012110062(__this, ___value0, method) ((  void (*) (Nullable_1_t2116400401 *, Vector3_t3525329789 , const MethodInfo*))Nullable_1__ctor_m2012110062_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<UnityEngine.Vector3>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3543212479_gshared (Nullable_1_t2116400401 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m3543212479(__this, method) ((  bool (*) (Nullable_1_t2116400401 *, const MethodInfo*))Nullable_1_get_HasValue_m3543212479_gshared)(__this, method)
// T System.Nullable`1<UnityEngine.Vector3>::get_Value()
extern "C"  Vector3_t3525329789  Nullable_1_get_Value_m2002199377_gshared (Nullable_1_t2116400401 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m2002199377(__this, method) ((  Vector3_t3525329789  (*) (Nullable_1_t2116400401 *, const MethodInfo*))Nullable_1_get_Value_m2002199377_gshared)(__this, method)
// System.Boolean System.Nullable`1<UnityEngine.Vector3>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3083210417_gshared (Nullable_1_t2116400401 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m3083210417(__this, ___other0, method) ((  bool (*) (Nullable_1_t2116400401 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m3083210417_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<UnityEngine.Vector3>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2485111662_gshared (Nullable_1_t2116400401 * __this, Nullable_1_t2116400401  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m2485111662(__this, ___other0, method) ((  bool (*) (Nullable_1_t2116400401 *, Nullable_1_t2116400401 , const MethodInfo*))Nullable_1_Equals_m2485111662_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<UnityEngine.Vector3>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m3406313621_gshared (Nullable_1_t2116400401 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m3406313621(__this, method) ((  int32_t (*) (Nullable_1_t2116400401 *, const MethodInfo*))Nullable_1_GetHashCode_m3406313621_gshared)(__this, method)
// T System.Nullable`1<UnityEngine.Vector3>::GetValueOrDefault()
extern "C"  Vector3_t3525329789  Nullable_1_GetValueOrDefault_m2059098796_gshared (Nullable_1_t2116400401 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m2059098796(__this, method) ((  Vector3_t3525329789  (*) (Nullable_1_t2116400401 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m2059098796_gshared)(__this, method)
// System.String System.Nullable`1<UnityEngine.Vector3>::ToString()
extern "C"  String_t* Nullable_1_ToString_m3285229841_gshared (Nullable_1_t2116400401 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m3285229841(__this, method) ((  String_t* (*) (Nullable_1_t2116400401 *, const MethodInfo*))Nullable_1_ToString_m3285229841_gshared)(__this, method)
// System.Nullable`1<T> System.Nullable`1<UnityEngine.Vector3>::op_Implicit(T)
extern "C"  Nullable_1_t2116400401  Nullable_1_op_Implicit_m1679940177_gshared (Il2CppObject * __this /* static, unused */, Vector3_t3525329789  ___value0, const MethodInfo* method);
#define Nullable_1_op_Implicit_m1679940177(__this /* static, unused */, ___value0, method) ((  Nullable_1_t2116400401  (*) (Il2CppObject * /* static, unused */, Vector3_t3525329789 , const MethodInfo*))Nullable_1_op_Implicit_m1679940177_gshared)(__this /* static, unused */, ___value0, method)
