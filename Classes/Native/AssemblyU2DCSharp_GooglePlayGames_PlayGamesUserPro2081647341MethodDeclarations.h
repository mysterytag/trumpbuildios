﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GooglePlayGames.PlayGamesUserProfile
struct PlayGamesUserProfile_t2081647341;
// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t2509538522;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState3192499994.h"

// System.Void GooglePlayGames.PlayGamesUserProfile::.ctor()
extern "C"  void PlayGamesUserProfile__ctor_m1319405968 (PlayGamesUserProfile_t2081647341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.PlayGamesUserProfile::get_userName()
extern "C"  String_t* PlayGamesUserProfile_get_userName_m2379353398 (PlayGamesUserProfile_t2081647341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GooglePlayGames.PlayGamesUserProfile::get_id()
extern "C"  String_t* PlayGamesUserProfile_get_id_m613081947 (PlayGamesUserProfile_t2081647341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GooglePlayGames.PlayGamesUserProfile::get_isFriend()
extern "C"  bool PlayGamesUserProfile_get_isFriend_m43038265 (PlayGamesUserProfile_t2081647341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SocialPlatforms.UserState GooglePlayGames.PlayGamesUserProfile::get_state()
extern "C"  int32_t PlayGamesUserProfile_get_state_m361572247 (PlayGamesUserProfile_t2081647341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D GooglePlayGames.PlayGamesUserProfile::get_image()
extern "C"  Texture2D_t2509538522 * PlayGamesUserProfile_get_image_m3678310755 (PlayGamesUserProfile_t2081647341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
