﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.UploadStringCompletedEventHandler
struct UploadStringCompletedEventHandler_t2032366131;
// System.Object
struct Il2CppObject;
// System.Net.UploadStringCompletedEventArgs
struct UploadStringCompletedEventArgs_t4015562024;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "System_System_Net_UploadStringCompletedEventArgs4015562024.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Net.UploadStringCompletedEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void UploadStringCompletedEventHandler__ctor_m2024789823 (UploadStringCompletedEventHandler_t2032366131 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.UploadStringCompletedEventHandler::Invoke(System.Object,System.Net.UploadStringCompletedEventArgs)
extern "C"  void UploadStringCompletedEventHandler_Invoke_m828569081 (UploadStringCompletedEventHandler_t2032366131 * __this, Il2CppObject * ___sender0, UploadStringCompletedEventArgs_t4015562024 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_UploadStringCompletedEventHandler_t2032366131(Il2CppObject* delegate, Il2CppObject * ___sender0, UploadStringCompletedEventArgs_t4015562024 * ___e1);
// System.IAsyncResult System.Net.UploadStringCompletedEventHandler::BeginInvoke(System.Object,System.Net.UploadStringCompletedEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UploadStringCompletedEventHandler_BeginInvoke_m1061327070 (UploadStringCompletedEventHandler_t2032366131 * __this, Il2CppObject * ___sender0, UploadStringCompletedEventArgs_t4015562024 * ___e1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.UploadStringCompletedEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void UploadStringCompletedEventHandler_EndInvoke_m81182927 (UploadStringCompletedEventHandler_t2032366131 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
