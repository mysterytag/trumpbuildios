﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`1<System.Collections.IEnumerator>
struct Func_1_t1429988286;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/<SelectMany>c__AnonStorey4E`1<System.Object>
struct  U3CSelectManyU3Ec__AnonStorey4E_1_t4202698475  : public Il2CppObject
{
public:
	// System.Func`1<System.Collections.IEnumerator> UniRx.Observable/<SelectMany>c__AnonStorey4E`1::selector
	Func_1_t1429988286 * ___selector_0;

public:
	inline static int32_t get_offset_of_selector_0() { return static_cast<int32_t>(offsetof(U3CSelectManyU3Ec__AnonStorey4E_1_t4202698475, ___selector_0)); }
	inline Func_1_t1429988286 * get_selector_0() const { return ___selector_0; }
	inline Func_1_t1429988286 ** get_address_of_selector_0() { return &___selector_0; }
	inline void set_selector_0(Func_1_t1429988286 * value)
	{
		___selector_0 = value;
		Il2CppCodeGenWriteBarrier(&___selector_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
