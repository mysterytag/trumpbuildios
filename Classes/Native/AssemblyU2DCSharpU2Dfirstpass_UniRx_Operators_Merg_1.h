﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.MergeObservable`1/MergeOuterObserver<UniRx.Unit>
struct MergeOuterObserver_t3334261548;
// System.IDisposable
struct IDisposable_t1628921374;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4165376397.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.MergeObservable`1/MergeOuterObserver/Merge<UniRx.Unit>
struct  Merge_t3474609588  : public OperatorObserverBase_2_t4165376397
{
public:
	// UniRx.Operators.MergeObservable`1/MergeOuterObserver<T> UniRx.Operators.MergeObservable`1/MergeOuterObserver/Merge::parent
	MergeOuterObserver_t3334261548 * ___parent_2;
	// System.IDisposable UniRx.Operators.MergeObservable`1/MergeOuterObserver/Merge::cancel
	Il2CppObject * ___cancel_3;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(Merge_t3474609588, ___parent_2)); }
	inline MergeOuterObserver_t3334261548 * get_parent_2() const { return ___parent_2; }
	inline MergeOuterObserver_t3334261548 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(MergeOuterObserver_t3334261548 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_cancel_3() { return static_cast<int32_t>(offsetof(Merge_t3474609588, ___cancel_3)); }
	inline Il2CppObject * get_cancel_3() const { return ___cancel_3; }
	inline Il2CppObject ** get_address_of_cancel_3() { return &___cancel_3; }
	inline void set_cancel_3(Il2CppObject * value)
	{
		___cancel_3 = value;
		Il2CppCodeGenWriteBarrier(&___cancel_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
