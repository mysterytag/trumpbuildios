﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1/Subscription<System.Byte>
struct Subscription_t557940343;
// UniRx.Subject`1<System.Byte>
struct Subject_1_t421761145;
// UniRx.IObserver`1<System.Byte>
struct IObserver_1_t695725428;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Subject`1/Subscription<System.Byte>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern "C"  void Subscription__ctor_m1617373294_gshared (Subscription_t557940343 * __this, Subject_1_t421761145 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method);
#define Subscription__ctor_m1617373294(__this, ___parent0, ___unsubscribeTarget1, method) ((  void (*) (Subscription_t557940343 *, Subject_1_t421761145 *, Il2CppObject*, const MethodInfo*))Subscription__ctor_m1617373294_gshared)(__this, ___parent0, ___unsubscribeTarget1, method)
// System.Void UniRx.Subject`1/Subscription<System.Byte>::Dispose()
extern "C"  void Subscription_Dispose_m2238855368_gshared (Subscription_t557940343 * __this, const MethodInfo* method);
#define Subscription_Dispose_m2238855368(__this, method) ((  void (*) (Subscription_t557940343 *, const MethodInfo*))Subscription_Dispose_m2238855368_gshared)(__this, method)
