﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/UpdateUserTitleDisplayNameRequestCallback
struct UpdateUserTitleDisplayNameRequestCallback_t567940939;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.UpdateUserTitleDisplayNameRequest
struct UpdateUserTitleDisplayNameRequest_t2467685622;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UpdateUserT2467685622.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/UpdateUserTitleDisplayNameRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void UpdateUserTitleDisplayNameRequestCallback__ctor_m1910363738 (UpdateUserTitleDisplayNameRequestCallback_t567940939 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UpdateUserTitleDisplayNameRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.UpdateUserTitleDisplayNameRequest,System.Object)
extern "C"  void UpdateUserTitleDisplayNameRequestCallback_Invoke_m1883537091 (UpdateUserTitleDisplayNameRequestCallback_t567940939 * __this, String_t* ___urlPath0, int32_t ___callId1, UpdateUserTitleDisplayNameRequest_t2467685622 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_UpdateUserTitleDisplayNameRequestCallback_t567940939(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, UpdateUserTitleDisplayNameRequest_t2467685622 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/UpdateUserTitleDisplayNameRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.UpdateUserTitleDisplayNameRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UpdateUserTitleDisplayNameRequestCallback_BeginInvoke_m1876787070 (UpdateUserTitleDisplayNameRequestCallback_t567940939 * __this, String_t* ___urlPath0, int32_t ___callId1, UpdateUserTitleDisplayNameRequest_t2467685622 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UpdateUserTitleDisplayNameRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void UpdateUserTitleDisplayNameRequestCallback_EndInvoke_m2533262698 (UpdateUserTitleDisplayNameRequestCallback_t567940939 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
