﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_BinderGeneric_1_gen520705716MethodDeclarations.h"

// System.Void Zenject.BinderGeneric`1<UnityEngine.UI.Text>::.ctor(Zenject.DiContainer,System.String,Zenject.SingletonProviderMap)
#define BinderGeneric_1__ctor_m2916334790(__this, ___container0, ___identifier1, ___singletonMap2, method) ((  void (*) (BinderGeneric_1_t2970057494 *, DiContainer_t2383114449 *, String_t*, SingletonProviderMap_t1557411893 *, const MethodInfo*))BinderGeneric_1__ctor_m467851124_gshared)(__this, ___container0, ___identifier1, ___singletonMap2, method)
// Zenject.BindingConditionSetter Zenject.BinderGeneric`1<UnityEngine.UI.Text>::ToMethod(System.Func`2<Zenject.InjectContext,TContract>)
#define BinderGeneric_1_ToMethod_m2368489997(__this, ___method0, method) ((  BindingConditionSetter_t259147722 * (*) (BinderGeneric_1_t2970057494 *, Func_2_t775630079 *, const MethodInfo*))BinderGeneric_1_ToMethod_m4174284639_gshared)(__this, ___method0, method)
