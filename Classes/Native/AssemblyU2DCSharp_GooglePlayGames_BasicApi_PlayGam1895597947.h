﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GooglePlayGames.BasicApi.InvitationReceivedDelegate
struct InvitationReceivedDelegate_t716623425;
// GooglePlayGames.BasicApi.Multiplayer.MatchDelegate
struct MatchDelegate_t4292352560;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder
struct  Builder_t1895597948  : public Il2CppObject
{
public:
	// System.Boolean GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder::mEnableSaveGames
	bool ___mEnableSaveGames_0;
	// System.Boolean GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder::mEnableDeprecatedCloudSave
	bool ___mEnableDeprecatedCloudSave_1;
	// GooglePlayGames.BasicApi.InvitationReceivedDelegate GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder::mInvitationDelegate
	InvitationReceivedDelegate_t716623425 * ___mInvitationDelegate_2;
	// GooglePlayGames.BasicApi.Multiplayer.MatchDelegate GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder::mMatchDelegate
	MatchDelegate_t4292352560 * ___mMatchDelegate_3;

public:
	inline static int32_t get_offset_of_mEnableSaveGames_0() { return static_cast<int32_t>(offsetof(Builder_t1895597948, ___mEnableSaveGames_0)); }
	inline bool get_mEnableSaveGames_0() const { return ___mEnableSaveGames_0; }
	inline bool* get_address_of_mEnableSaveGames_0() { return &___mEnableSaveGames_0; }
	inline void set_mEnableSaveGames_0(bool value)
	{
		___mEnableSaveGames_0 = value;
	}

	inline static int32_t get_offset_of_mEnableDeprecatedCloudSave_1() { return static_cast<int32_t>(offsetof(Builder_t1895597948, ___mEnableDeprecatedCloudSave_1)); }
	inline bool get_mEnableDeprecatedCloudSave_1() const { return ___mEnableDeprecatedCloudSave_1; }
	inline bool* get_address_of_mEnableDeprecatedCloudSave_1() { return &___mEnableDeprecatedCloudSave_1; }
	inline void set_mEnableDeprecatedCloudSave_1(bool value)
	{
		___mEnableDeprecatedCloudSave_1 = value;
	}

	inline static int32_t get_offset_of_mInvitationDelegate_2() { return static_cast<int32_t>(offsetof(Builder_t1895597948, ___mInvitationDelegate_2)); }
	inline InvitationReceivedDelegate_t716623425 * get_mInvitationDelegate_2() const { return ___mInvitationDelegate_2; }
	inline InvitationReceivedDelegate_t716623425 ** get_address_of_mInvitationDelegate_2() { return &___mInvitationDelegate_2; }
	inline void set_mInvitationDelegate_2(InvitationReceivedDelegate_t716623425 * value)
	{
		___mInvitationDelegate_2 = value;
		Il2CppCodeGenWriteBarrier(&___mInvitationDelegate_2, value);
	}

	inline static int32_t get_offset_of_mMatchDelegate_3() { return static_cast<int32_t>(offsetof(Builder_t1895597948, ___mMatchDelegate_3)); }
	inline MatchDelegate_t4292352560 * get_mMatchDelegate_3() const { return ___mMatchDelegate_3; }
	inline MatchDelegate_t4292352560 ** get_address_of_mMatchDelegate_3() { return &___mMatchDelegate_3; }
	inline void set_mMatchDelegate_3(MatchDelegate_t4292352560 * value)
	{
		___mMatchDelegate_3 = value;
		Il2CppCodeGenWriteBarrier(&___mMatchDelegate_3, value);
	}
};

struct Builder_t1895597948_StaticFields
{
public:
	// GooglePlayGames.BasicApi.InvitationReceivedDelegate GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder::<>f__am$cache4
	InvitationReceivedDelegate_t716623425 * ___U3CU3Ef__amU24cache4_4;
	// GooglePlayGames.BasicApi.Multiplayer.MatchDelegate GooglePlayGames.BasicApi.PlayGamesClientConfiguration/Builder::<>f__am$cache5
	MatchDelegate_t4292352560 * ___U3CU3Ef__amU24cache5_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_4() { return static_cast<int32_t>(offsetof(Builder_t1895597948_StaticFields, ___U3CU3Ef__amU24cache4_4)); }
	inline InvitationReceivedDelegate_t716623425 * get_U3CU3Ef__amU24cache4_4() const { return ___U3CU3Ef__amU24cache4_4; }
	inline InvitationReceivedDelegate_t716623425 ** get_address_of_U3CU3Ef__amU24cache4_4() { return &___U3CU3Ef__amU24cache4_4; }
	inline void set_U3CU3Ef__amU24cache4_4(InvitationReceivedDelegate_t716623425 * value)
	{
		___U3CU3Ef__amU24cache4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_5() { return static_cast<int32_t>(offsetof(Builder_t1895597948_StaticFields, ___U3CU3Ef__amU24cache5_5)); }
	inline MatchDelegate_t4292352560 * get_U3CU3Ef__amU24cache5_5() const { return ___U3CU3Ef__amU24cache5_5; }
	inline MatchDelegate_t4292352560 ** get_address_of_U3CU3Ef__amU24cache5_5() { return &___U3CU3Ef__amU24cache5_5; }
	inline void set_U3CU3Ef__amU24cache5_5(MatchDelegate_t4292352560 * value)
	{
		___U3CU3Ef__amU24cache5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
