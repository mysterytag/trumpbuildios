﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// ReactiveUI/<SetContent>c__AnonStorey122`1<System.Object>
struct U3CSetContentU3Ec__AnonStorey122_1_t1018658799;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReactiveUI/<SetContent>c__AnonStorey122`1/<SetContent>c__AnonStorey123`1<System.Object>
struct  U3CSetContentU3Ec__AnonStorey123_1_t573633800  : public Il2CppObject
{
public:
	// T ReactiveUI/<SetContent>c__AnonStorey122`1/<SetContent>c__AnonStorey123`1::obj
	Il2CppObject * ___obj_0;
	// ReactiveUI/<SetContent>c__AnonStorey122`1<T> ReactiveUI/<SetContent>c__AnonStorey122`1/<SetContent>c__AnonStorey123`1::<>f__ref$290
	U3CSetContentU3Ec__AnonStorey122_1_t1018658799 * ___U3CU3Ef__refU24290_1;

public:
	inline static int32_t get_offset_of_obj_0() { return static_cast<int32_t>(offsetof(U3CSetContentU3Ec__AnonStorey123_1_t573633800, ___obj_0)); }
	inline Il2CppObject * get_obj_0() const { return ___obj_0; }
	inline Il2CppObject ** get_address_of_obj_0() { return &___obj_0; }
	inline void set_obj_0(Il2CppObject * value)
	{
		___obj_0 = value;
		Il2CppCodeGenWriteBarrier(&___obj_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24290_1() { return static_cast<int32_t>(offsetof(U3CSetContentU3Ec__AnonStorey123_1_t573633800, ___U3CU3Ef__refU24290_1)); }
	inline U3CSetContentU3Ec__AnonStorey122_1_t1018658799 * get_U3CU3Ef__refU24290_1() const { return ___U3CU3Ef__refU24290_1; }
	inline U3CSetContentU3Ec__AnonStorey122_1_t1018658799 ** get_address_of_U3CU3Ef__refU24290_1() { return &___U3CU3Ef__refU24290_1; }
	inline void set_U3CU3Ef__refU24290_1(U3CSetContentU3Ec__AnonStorey122_1_t1018658799 * value)
	{
		___U3CU3Ef__refU24290_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24290_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
