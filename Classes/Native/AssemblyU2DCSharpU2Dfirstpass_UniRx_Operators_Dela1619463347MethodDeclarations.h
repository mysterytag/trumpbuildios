﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.DelayFrameObservable`1/DelayFrame<System.Object>
struct DelayFrame_t1619463347;
// UniRx.Operators.DelayFrameObservable`1<System.Object>
struct DelayFrameObservable_1_t4117294156;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.DelayFrameObservable`1/DelayFrame<System.Object>::.ctor(UniRx.Operators.DelayFrameObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void DelayFrame__ctor_m2525345198_gshared (DelayFrame_t1619463347 * __this, DelayFrameObservable_1_t4117294156 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define DelayFrame__ctor_m2525345198(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (DelayFrame_t1619463347 *, DelayFrameObservable_1_t4117294156 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))DelayFrame__ctor_m2525345198_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.DelayFrameObservable`1/DelayFrame<System.Object>::Run()
extern "C"  Il2CppObject * DelayFrame_Run_m876846149_gshared (DelayFrame_t1619463347 * __this, const MethodInfo* method);
#define DelayFrame_Run_m876846149(__this, method) ((  Il2CppObject * (*) (DelayFrame_t1619463347 *, const MethodInfo*))DelayFrame_Run_m876846149_gshared)(__this, method)
// System.Collections.IEnumerator UniRx.Operators.DelayFrameObservable`1/DelayFrame<System.Object>::OnNextDelay(T)
extern "C"  Il2CppObject * DelayFrame_OnNextDelay_m2080628808_gshared (DelayFrame_t1619463347 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define DelayFrame_OnNextDelay_m2080628808(__this, ___value0, method) ((  Il2CppObject * (*) (DelayFrame_t1619463347 *, Il2CppObject *, const MethodInfo*))DelayFrame_OnNextDelay_m2080628808_gshared)(__this, ___value0, method)
// System.Collections.IEnumerator UniRx.Operators.DelayFrameObservable`1/DelayFrame<System.Object>::OnCompletedDelay()
extern "C"  Il2CppObject * DelayFrame_OnCompletedDelay_m158968724_gshared (DelayFrame_t1619463347 * __this, const MethodInfo* method);
#define DelayFrame_OnCompletedDelay_m158968724(__this, method) ((  Il2CppObject * (*) (DelayFrame_t1619463347 *, const MethodInfo*))DelayFrame_OnCompletedDelay_m158968724_gshared)(__this, method)
// System.Void UniRx.Operators.DelayFrameObservable`1/DelayFrame<System.Object>::OnNext(T)
extern "C"  void DelayFrame_OnNext_m3257379551_gshared (DelayFrame_t1619463347 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define DelayFrame_OnNext_m3257379551(__this, ___value0, method) ((  void (*) (DelayFrame_t1619463347 *, Il2CppObject *, const MethodInfo*))DelayFrame_OnNext_m3257379551_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.DelayFrameObservable`1/DelayFrame<System.Object>::OnError(System.Exception)
extern "C"  void DelayFrame_OnError_m2429560814_gshared (DelayFrame_t1619463347 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define DelayFrame_OnError_m2429560814(__this, ___error0, method) ((  void (*) (DelayFrame_t1619463347 *, Exception_t1967233988 *, const MethodInfo*))DelayFrame_OnError_m2429560814_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.DelayFrameObservable`1/DelayFrame<System.Object>::OnCompleted()
extern "C"  void DelayFrame_OnCompleted_m328781889_gshared (DelayFrame_t1619463347 * __this, const MethodInfo* method);
#define DelayFrame_OnCompleted_m328781889(__this, method) ((  void (*) (DelayFrame_t1619463347 *, const MethodInfo*))DelayFrame_OnCompleted_m328781889_gshared)(__this, method)
