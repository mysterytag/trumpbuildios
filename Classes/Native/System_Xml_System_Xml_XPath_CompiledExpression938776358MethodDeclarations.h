﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XPath.CompiledExpression
struct CompiledExpression_t938776358;
// System.String
struct String_t;
// System.Xml.XPath.Expression
struct Expression_t4217024437;
// System.Xml.XmlNamespaceManager
struct XmlNamespaceManager_t1861067185;
// System.Xml.IXmlNamespaceResolver
struct IXmlNamespaceResolver_t3696234203;
// System.Xml.XPath.XPathNodeIterator
struct XPathNodeIterator_t2394191562;
// System.Xml.XPath.BaseIterator
struct BaseIterator_t3696600956;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "System_Xml_System_Xml_XPath_Expression4217024437.h"
#include "System_Xml_System_Xml_XmlNamespaceManager1861067185.h"
#include "System_Xml_System_Xml_XPath_BaseIterator3696600956.h"

// System.Void System.Xml.XPath.CompiledExpression::.ctor(System.String,System.Xml.XPath.Expression)
extern "C"  void CompiledExpression__ctor_m2683771966 (CompiledExpression_t938776358 * __this, String_t* ___raw0, Expression_t4217024437 * ___expr1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XPath.CompiledExpression::SetContext(System.Xml.XmlNamespaceManager)
extern "C"  void CompiledExpression_SetContext_m1907071021 (CompiledExpression_t938776358 * __this, XmlNamespaceManager_t1861067185 * ___nsManager0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XPath.CompiledExpression::SetContext(System.Xml.IXmlNamespaceResolver)
extern "C"  void CompiledExpression_SetContext_m1282298115 (CompiledExpression_t938776358 * __this, Il2CppObject * ___nsResolver0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.IXmlNamespaceResolver System.Xml.XPath.CompiledExpression::get_NamespaceManager()
extern "C"  Il2CppObject * CompiledExpression_get_NamespaceManager_m652200026 (CompiledExpression_t938776358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XPath.XPathNodeIterator System.Xml.XPath.CompiledExpression::EvaluateNodeSet(System.Xml.XPath.BaseIterator)
extern "C"  XPathNodeIterator_t2394191562 * CompiledExpression_EvaluateNodeSet_m2426588395 (CompiledExpression_t938776358 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
