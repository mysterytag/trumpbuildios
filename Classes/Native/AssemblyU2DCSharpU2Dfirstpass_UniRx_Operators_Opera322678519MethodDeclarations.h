﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.OperatorObserverBase`2<System.Int64,UnityEngine.Vector2>
struct OperatorObserverBase_2_t322678519;
// UniRx.IObserver`1<UnityEngine.Vector2>
struct IObserver_1_t1442361395;
// System.IDisposable
struct IDisposable_t1628921374;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,UnityEngine.Vector2>::.ctor(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void OperatorObserverBase_2__ctor_m3493258426_gshared (OperatorObserverBase_2_t322678519 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define OperatorObserverBase_2__ctor_m3493258426(__this, ___observer0, ___cancel1, method) ((  void (*) (OperatorObserverBase_2_t322678519 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))OperatorObserverBase_2__ctor_m3493258426_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.OperatorObserverBase`2<System.Int64,UnityEngine.Vector2>::Dispose()
extern "C"  void OperatorObserverBase_2_Dispose_m2199592304_gshared (OperatorObserverBase_2_t322678519 * __this, const MethodInfo* method);
#define OperatorObserverBase_2_Dispose_m2199592304(__this, method) ((  void (*) (OperatorObserverBase_2_t322678519 *, const MethodInfo*))OperatorObserverBase_2_Dispose_m2199592304_gshared)(__this, method)
