﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.DoOnSubscribeObservable`1/DoOnSubscribe<System.Object>
struct DoOnSubscribe_t871059449;
// UniRx.Operators.DoOnSubscribeObservable`1<System.Object>
struct DoOnSubscribeObservable_1_t1431598738;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.DoOnSubscribeObservable`1/DoOnSubscribe<System.Object>::.ctor(UniRx.Operators.DoOnSubscribeObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void DoOnSubscribe__ctor_m346588380_gshared (DoOnSubscribe_t871059449 * __this, DoOnSubscribeObservable_1_t1431598738 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define DoOnSubscribe__ctor_m346588380(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (DoOnSubscribe_t871059449 *, DoOnSubscribeObservable_1_t1431598738 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))DoOnSubscribe__ctor_m346588380_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.DoOnSubscribeObservable`1/DoOnSubscribe<System.Object>::Run()
extern "C"  Il2CppObject * DoOnSubscribe_Run_m1872890543_gshared (DoOnSubscribe_t871059449 * __this, const MethodInfo* method);
#define DoOnSubscribe_Run_m1872890543(__this, method) ((  Il2CppObject * (*) (DoOnSubscribe_t871059449 *, const MethodInfo*))DoOnSubscribe_Run_m1872890543_gshared)(__this, method)
// System.Void UniRx.Operators.DoOnSubscribeObservable`1/DoOnSubscribe<System.Object>::OnNext(T)
extern "C"  void DoOnSubscribe_OnNext_m1060279305_gshared (DoOnSubscribe_t871059449 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define DoOnSubscribe_OnNext_m1060279305(__this, ___value0, method) ((  void (*) (DoOnSubscribe_t871059449 *, Il2CppObject *, const MethodInfo*))DoOnSubscribe_OnNext_m1060279305_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.DoOnSubscribeObservable`1/DoOnSubscribe<System.Object>::OnError(System.Exception)
extern "C"  void DoOnSubscribe_OnError_m2428992792_gshared (DoOnSubscribe_t871059449 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define DoOnSubscribe_OnError_m2428992792(__this, ___error0, method) ((  void (*) (DoOnSubscribe_t871059449 *, Exception_t1967233988 *, const MethodInfo*))DoOnSubscribe_OnError_m2428992792_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.DoOnSubscribeObservable`1/DoOnSubscribe<System.Object>::OnCompleted()
extern "C"  void DoOnSubscribe_OnCompleted_m3512145003_gshared (DoOnSubscribe_t871059449 * __this, const MethodInfo* method);
#define DoOnSubscribe_OnCompleted_m3512145003(__this, method) ((  void (*) (DoOnSubscribe_t871059449 *, const MethodInfo*))DoOnSubscribe_OnCompleted_m3512145003_gshared)(__this, method)
