﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData>
struct Subject_1_t848168958;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Triggers_Obser1883237895.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableScrollTrigger
struct  ObservableScrollTrigger_t1093057609  : public ObservableTriggerBase_t1883237895
{
public:
	// UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableScrollTrigger::onScroll
	Subject_1_t848168958 * ___onScroll_8;

public:
	inline static int32_t get_offset_of_onScroll_8() { return static_cast<int32_t>(offsetof(ObservableScrollTrigger_t1093057609, ___onScroll_8)); }
	inline Subject_1_t848168958 * get_onScroll_8() const { return ___onScroll_8; }
	inline Subject_1_t848168958 ** get_address_of_onScroll_8() { return &___onScroll_8; }
	inline void set_onScroll_8(Subject_1_t848168958 * value)
	{
		___onScroll_8 = value;
		Il2CppCodeGenWriteBarrier(&___onScroll_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
