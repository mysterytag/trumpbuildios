﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1/Subscription<UnityEngine.Vector2>
struct Subscription_t1304576310;
// UniRx.Subject`1<UnityEngine.Vector2>
struct Subject_1_t1168397112;
// UniRx.IObserver`1<UnityEngine.Vector2>
struct IObserver_1_t1442361395;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Subject`1/Subscription<UnityEngine.Vector2>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern "C"  void Subscription__ctor_m3227729597_gshared (Subscription_t1304576310 * __this, Subject_1_t1168397112 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method);
#define Subscription__ctor_m3227729597(__this, ___parent0, ___unsubscribeTarget1, method) ((  void (*) (Subscription_t1304576310 *, Subject_1_t1168397112 *, Il2CppObject*, const MethodInfo*))Subscription__ctor_m3227729597_gshared)(__this, ___parent0, ___unsubscribeTarget1, method)
// System.Void UniRx.Subject`1/Subscription<UnityEngine.Vector2>::Dispose()
extern "C"  void Subscription_Dispose_m2235871513_gshared (Subscription_t1304576310 * __this, const MethodInfo* method);
#define Subscription_Dispose_m2235871513(__this, method) ((  void (*) (Subscription_t1304576310 *, const MethodInfo*))Subscription_Dispose_m2235871513_gshared)(__this, method)
