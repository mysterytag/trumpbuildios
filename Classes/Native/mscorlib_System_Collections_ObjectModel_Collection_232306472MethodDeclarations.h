﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UniRx.Unit>
struct Collection_1_t232306472;
// System.Collections.Generic.IList`1<UniRx.Unit>
struct IList_1_t429811056;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// UniRx.Unit[]
struct UnitU5BU5D_t1267952403;
// System.Collections.Generic.IEnumerator`1<UniRx.Unit>
struct IEnumerator_1_t4041392486;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Unit>::.ctor()
extern "C"  void Collection_1__ctor_m2069262307_gshared (Collection_1_t232306472 * __this, const MethodInfo* method);
#define Collection_1__ctor_m2069262307(__this, method) ((  void (*) (Collection_1_t232306472 *, const MethodInfo*))Collection_1__ctor_m2069262307_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Unit>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1__ctor_m2599188754_gshared (Collection_1_t232306472 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1__ctor_m2599188754(__this, ___list0, method) ((  void (*) (Collection_1_t232306472 *, Il2CppObject*, const MethodInfo*))Collection_1__ctor_m2599188754_gshared)(__this, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UniRx.Unit>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1406663028_gshared (Collection_1_t232306472 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1406663028(__this, method) ((  bool (*) (Collection_1_t232306472 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1406663028_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Unit>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m939069121_gshared (Collection_1_t232306472 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m939069121(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t232306472 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m939069121_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UniRx.Unit>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m1220008976_gshared (Collection_1_t232306472 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m1220008976(__this, method) ((  Il2CppObject * (*) (Collection_1_t232306472 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m1220008976_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UniRx.Unit>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m1555307821_gshared (Collection_1_t232306472 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m1555307821(__this, ___value0, method) ((  int32_t (*) (Collection_1_t232306472 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m1555307821_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UniRx.Unit>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m2177936691_gshared (Collection_1_t232306472 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m2177936691(__this, ___value0, method) ((  bool (*) (Collection_1_t232306472 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m2177936691_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UniRx.Unit>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m756544773_gshared (Collection_1_t232306472 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m756544773(__this, ___value0, method) ((  int32_t (*) (Collection_1_t232306472 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m756544773_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Unit>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m3025010936_gshared (Collection_1_t232306472 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m3025010936(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t232306472 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m3025010936_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Unit>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m1543468336_gshared (Collection_1_t232306472 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m1543468336(__this, ___value0, method) ((  void (*) (Collection_1_t232306472 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m1543468336_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UniRx.Unit>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m3130964297_gshared (Collection_1_t232306472 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m3130964297(__this, method) ((  bool (*) (Collection_1_t232306472 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m3130964297_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UniRx.Unit>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m1242261755_gshared (Collection_1_t232306472 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m1242261755(__this, method) ((  Il2CppObject * (*) (Collection_1_t232306472 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1242261755_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UniRx.Unit>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m2690842338_gshared (Collection_1_t232306472 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m2690842338(__this, method) ((  bool (*) (Collection_1_t232306472 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m2690842338_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UniRx.Unit>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m540810199_gshared (Collection_1_t232306472 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m540810199(__this, method) ((  bool (*) (Collection_1_t232306472 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m540810199_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UniRx.Unit>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m878249922_gshared (Collection_1_t232306472 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m878249922(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t232306472 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m878249922_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Unit>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m4251569871_gshared (Collection_1_t232306472 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m4251569871(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t232306472 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m4251569871_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Unit>::Add(T)
extern "C"  void Collection_1_Add_m3104933948_gshared (Collection_1_t232306472 * __this, Unit_t2558286038  ___item0, const MethodInfo* method);
#define Collection_1_Add_m3104933948(__this, ___item0, method) ((  void (*) (Collection_1_t232306472 *, Unit_t2558286038 , const MethodInfo*))Collection_1_Add_m3104933948_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Unit>::Clear()
extern "C"  void Collection_1_Clear_m3770362894_gshared (Collection_1_t232306472 * __this, const MethodInfo* method);
#define Collection_1_Clear_m3770362894(__this, method) ((  void (*) (Collection_1_t232306472 *, const MethodInfo*))Collection_1_Clear_m3770362894_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Unit>::ClearItems()
extern "C"  void Collection_1_ClearItems_m3769617844_gshared (Collection_1_t232306472 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m3769617844(__this, method) ((  void (*) (Collection_1_t232306472 *, const MethodInfo*))Collection_1_ClearItems_m3769617844_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UniRx.Unit>::Contains(T)
extern "C"  bool Collection_1_Contains_m4289679360_gshared (Collection_1_t232306472 * __this, Unit_t2558286038  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m4289679360(__this, ___item0, method) ((  bool (*) (Collection_1_t232306472 *, Unit_t2558286038 , const MethodInfo*))Collection_1_Contains_m4289679360_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Unit>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m2077514028_gshared (Collection_1_t232306472 * __this, UnitU5BU5D_t1267952403* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m2077514028(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t232306472 *, UnitU5BU5D_t1267952403*, int32_t, const MethodInfo*))Collection_1_CopyTo_m2077514028_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UniRx.Unit>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m2435068247_gshared (Collection_1_t232306472 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m2435068247(__this, method) ((  Il2CppObject* (*) (Collection_1_t232306472 *, const MethodInfo*))Collection_1_GetEnumerator_m2435068247_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UniRx.Unit>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m1215920120_gshared (Collection_1_t232306472 * __this, Unit_t2558286038  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m1215920120(__this, ___item0, method) ((  int32_t (*) (Collection_1_t232306472 *, Unit_t2558286038 , const MethodInfo*))Collection_1_IndexOf_m1215920120_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Unit>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m3670985379_gshared (Collection_1_t232306472 * __this, int32_t ___index0, Unit_t2558286038  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m3670985379(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t232306472 *, int32_t, Unit_t2558286038 , const MethodInfo*))Collection_1_Insert_m3670985379_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Unit>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m1292496726_gshared (Collection_1_t232306472 * __this, int32_t ___index0, Unit_t2558286038  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m1292496726(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t232306472 *, int32_t, Unit_t2558286038 , const MethodInfo*))Collection_1_InsertItem_m1292496726_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UniRx.Unit>::Remove(T)
extern "C"  bool Collection_1_Remove_m477630459_gshared (Collection_1_t232306472 * __this, Unit_t2558286038  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m477630459(__this, ___item0, method) ((  bool (*) (Collection_1_t232306472 *, Unit_t2558286038 , const MethodInfo*))Collection_1_Remove_m477630459_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Unit>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m1544838249_gshared (Collection_1_t232306472 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m1544838249(__this, ___index0, method) ((  void (*) (Collection_1_t232306472 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m1544838249_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Unit>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m1430497865_gshared (Collection_1_t232306472 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m1430497865(__this, ___index0, method) ((  void (*) (Collection_1_t232306472 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m1430497865_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UniRx.Unit>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m3151092867_gshared (Collection_1_t232306472 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m3151092867(__this, method) ((  int32_t (*) (Collection_1_t232306472 *, const MethodInfo*))Collection_1_get_Count_m3151092867_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UniRx.Unit>::get_Item(System.Int32)
extern "C"  Unit_t2558286038  Collection_1_get_Item_m2102225295_gshared (Collection_1_t232306472 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m2102225295(__this, ___index0, method) ((  Unit_t2558286038  (*) (Collection_1_t232306472 *, int32_t, const MethodInfo*))Collection_1_get_Item_m2102225295_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Unit>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m3458395834_gshared (Collection_1_t232306472 * __this, int32_t ___index0, Unit_t2558286038  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m3458395834(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t232306472 *, int32_t, Unit_t2558286038 , const MethodInfo*))Collection_1_set_Item_m3458395834_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Unit>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m3447986815_gshared (Collection_1_t232306472 * __this, int32_t ___index0, Unit_t2558286038  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m3447986815(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t232306472 *, int32_t, Unit_t2558286038 , const MethodInfo*))Collection_1_SetItem_m3447986815_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UniRx.Unit>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m352789036_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m352789036(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m352789036_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<UniRx.Unit>::ConvertItem(System.Object)
extern "C"  Unit_t2558286038  Collection_1_ConvertItem_m831109358_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m831109358(__this /* static, unused */, ___item0, method) ((  Unit_t2558286038  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m831109358_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Unit>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m2979801324_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m2979801324(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m2979801324_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UniRx.Unit>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m2896667928_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m2896667928(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m2896667928_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UniRx.Unit>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m1458993287_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m1458993287(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m1458993287_gshared)(__this /* static, unused */, ___list0, method)
