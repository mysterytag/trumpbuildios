﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey99`3<System.Object,System.Object,System.Object>
struct U3CAsObservableU3Ec__AnonStorey99_3_t316980126;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey99`3<System.Object,System.Object,System.Object>::.ctor()
extern "C"  void U3CAsObservableU3Ec__AnonStorey99_3__ctor_m4188140622_gshared (U3CAsObservableU3Ec__AnonStorey99_3_t316980126 * __this, const MethodInfo* method);
#define U3CAsObservableU3Ec__AnonStorey99_3__ctor_m4188140622(__this, method) ((  void (*) (U3CAsObservableU3Ec__AnonStorey99_3_t316980126 *, const MethodInfo*))U3CAsObservableU3Ec__AnonStorey99_3__ctor_m4188140622_gshared)(__this, method)
// System.Void UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey99`3<System.Object,System.Object,System.Object>::<>m__D6(T0,T1,T2)
extern "C"  void U3CAsObservableU3Ec__AnonStorey99_3_U3CU3Em__D6_m1643197036_gshared (U3CAsObservableU3Ec__AnonStorey99_3_t316980126 * __this, Il2CppObject * ___t00, Il2CppObject * ___t11, Il2CppObject * ___t22, const MethodInfo* method);
#define U3CAsObservableU3Ec__AnonStorey99_3_U3CU3Em__D6_m1643197036(__this, ___t00, ___t11, ___t22, method) ((  void (*) (U3CAsObservableU3Ec__AnonStorey99_3_t316980126 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))U3CAsObservableU3Ec__AnonStorey99_3_U3CU3Em__D6_m1643197036_gshared)(__this, ___t00, ___t11, ___t22, method)
