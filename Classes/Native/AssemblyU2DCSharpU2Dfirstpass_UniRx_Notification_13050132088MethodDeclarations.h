﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Notification`1/OnCompletedNotification<System.Object>
struct OnCompletedNotification_t3050132088;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;
// UniRx.Notification`1<System.Object>
struct Notification_1_t38356375;
// System.String
struct String_t;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// System.Action`1<System.Exception>
struct Action_1_t2115686693;
// System.Action
struct Action_t437523947;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_NotificationKi3324123761.h"
#include "System_Core_System_Action437523947.h"

// System.Void UniRx.Notification`1/OnCompletedNotification<System.Object>::.ctor()
extern "C"  void OnCompletedNotification__ctor_m846116151_gshared (OnCompletedNotification_t3050132088 * __this, const MethodInfo* method);
#define OnCompletedNotification__ctor_m846116151(__this, method) ((  void (*) (OnCompletedNotification_t3050132088 *, const MethodInfo*))OnCompletedNotification__ctor_m846116151_gshared)(__this, method)
// T UniRx.Notification`1/OnCompletedNotification<System.Object>::get_Value()
extern "C"  Il2CppObject * OnCompletedNotification_get_Value_m4172696860_gshared (OnCompletedNotification_t3050132088 * __this, const MethodInfo* method);
#define OnCompletedNotification_get_Value_m4172696860(__this, method) ((  Il2CppObject * (*) (OnCompletedNotification_t3050132088 *, const MethodInfo*))OnCompletedNotification_get_Value_m4172696860_gshared)(__this, method)
// System.Exception UniRx.Notification`1/OnCompletedNotification<System.Object>::get_Exception()
extern "C"  Exception_t1967233988 * OnCompletedNotification_get_Exception_m195933264_gshared (OnCompletedNotification_t3050132088 * __this, const MethodInfo* method);
#define OnCompletedNotification_get_Exception_m195933264(__this, method) ((  Exception_t1967233988 * (*) (OnCompletedNotification_t3050132088 *, const MethodInfo*))OnCompletedNotification_get_Exception_m195933264_gshared)(__this, method)
// System.Boolean UniRx.Notification`1/OnCompletedNotification<System.Object>::get_HasValue()
extern "C"  bool OnCompletedNotification_get_HasValue_m1183486929_gshared (OnCompletedNotification_t3050132088 * __this, const MethodInfo* method);
#define OnCompletedNotification_get_HasValue_m1183486929(__this, method) ((  bool (*) (OnCompletedNotification_t3050132088 *, const MethodInfo*))OnCompletedNotification_get_HasValue_m1183486929_gshared)(__this, method)
// UniRx.NotificationKind UniRx.Notification`1/OnCompletedNotification<System.Object>::get_Kind()
extern "C"  int32_t OnCompletedNotification_get_Kind_m3114003516_gshared (OnCompletedNotification_t3050132088 * __this, const MethodInfo* method);
#define OnCompletedNotification_get_Kind_m3114003516(__this, method) ((  int32_t (*) (OnCompletedNotification_t3050132088 *, const MethodInfo*))OnCompletedNotification_get_Kind_m3114003516_gshared)(__this, method)
// System.Int32 UniRx.Notification`1/OnCompletedNotification<System.Object>::GetHashCode()
extern "C"  int32_t OnCompletedNotification_GetHashCode_m2285451036_gshared (OnCompletedNotification_t3050132088 * __this, const MethodInfo* method);
#define OnCompletedNotification_GetHashCode_m2285451036(__this, method) ((  int32_t (*) (OnCompletedNotification_t3050132088 *, const MethodInfo*))OnCompletedNotification_GetHashCode_m2285451036_gshared)(__this, method)
// System.Boolean UniRx.Notification`1/OnCompletedNotification<System.Object>::Equals(UniRx.Notification`1<T>)
extern "C"  bool OnCompletedNotification_Equals_m2016138576_gshared (OnCompletedNotification_t3050132088 * __this, Notification_1_t38356375 * ___other0, const MethodInfo* method);
#define OnCompletedNotification_Equals_m2016138576(__this, ___other0, method) ((  bool (*) (OnCompletedNotification_t3050132088 *, Notification_1_t38356375 *, const MethodInfo*))OnCompletedNotification_Equals_m2016138576_gshared)(__this, ___other0, method)
// System.String UniRx.Notification`1/OnCompletedNotification<System.Object>::ToString()
extern "C"  String_t* OnCompletedNotification_ToString_m2489518614_gshared (OnCompletedNotification_t3050132088 * __this, const MethodInfo* method);
#define OnCompletedNotification_ToString_m2489518614(__this, method) ((  String_t* (*) (OnCompletedNotification_t3050132088 *, const MethodInfo*))OnCompletedNotification_ToString_m2489518614_gshared)(__this, method)
// System.Void UniRx.Notification`1/OnCompletedNotification<System.Object>::Accept(UniRx.IObserver`1<T>)
extern "C"  void OnCompletedNotification_Accept_m1436023783_gshared (OnCompletedNotification_t3050132088 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define OnCompletedNotification_Accept_m1436023783(__this, ___observer0, method) ((  void (*) (OnCompletedNotification_t3050132088 *, Il2CppObject*, const MethodInfo*))OnCompletedNotification_Accept_m1436023783_gshared)(__this, ___observer0, method)
// System.Void UniRx.Notification`1/OnCompletedNotification<System.Object>::Accept(System.Action`1<T>,System.Action`1<System.Exception>,System.Action)
extern "C"  void OnCompletedNotification_Accept_m3744114624_gshared (OnCompletedNotification_t3050132088 * __this, Action_1_t985559125 * ___onNext0, Action_1_t2115686693 * ___onError1, Action_t437523947 * ___onCompleted2, const MethodInfo* method);
#define OnCompletedNotification_Accept_m3744114624(__this, ___onNext0, ___onError1, ___onCompleted2, method) ((  void (*) (OnCompletedNotification_t3050132088 *, Action_1_t985559125 *, Action_1_t2115686693 *, Action_t437523947 *, const MethodInfo*))OnCompletedNotification_Accept_m3744114624_gshared)(__this, ___onNext0, ___onError1, ___onCompleted2, method)
