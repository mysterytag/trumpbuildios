﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetCatalogItemsRequest
struct GetCatalogItemsRequest_t2824754786;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.GetCatalogItemsRequest::.ctor()
extern "C"  void GetCatalogItemsRequest__ctor_m2643214491 (GetCatalogItemsRequest_t2824754786 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GetCatalogItemsRequest::get_CatalogVersion()
extern "C"  String_t* GetCatalogItemsRequest_get_CatalogVersion_m3129163278 (GetCatalogItemsRequest_t2824754786 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetCatalogItemsRequest::set_CatalogVersion(System.String)
extern "C"  void GetCatalogItemsRequest_set_CatalogVersion_m532795101 (GetCatalogItemsRequest_t2824754786 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
