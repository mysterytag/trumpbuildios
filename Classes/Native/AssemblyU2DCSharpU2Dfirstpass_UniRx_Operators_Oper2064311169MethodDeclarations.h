﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper2908947767MethodDeclarations.h"

// System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.IObservable`1<UniRx.Unit>,UniRx.Unit>::.ctor(UniRx.IObserver`1<TResult>,System.IDisposable)
#define OperatorObserverBase_2__ctor_m1484004951(__this, ___observer0, ___cancel1, method) ((  void (*) (OperatorObserverBase_2_t2064311169 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))OperatorObserverBase_2__ctor_m476501194_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.OperatorObserverBase`2<UniRx.IObservable`1<UniRx.Unit>,UniRx.Unit>::Dispose()
#define OperatorObserverBase_2_Dispose_m1965330419(__this, method) ((  void (*) (OperatorObserverBase_2_t2064311169 *, const MethodInfo*))OperatorObserverBase_2_Dispose_m73684832_gshared)(__this, method)
