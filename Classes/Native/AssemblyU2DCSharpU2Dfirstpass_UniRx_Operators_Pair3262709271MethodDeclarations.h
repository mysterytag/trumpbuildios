﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.PairwiseObservable`2<System.Object,System.Object>
struct PairwiseObservable_2_t3262709271;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Func`3<System.Object,System.Object,System.Object>
struct Func_3_t1892209229;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.PairwiseObservable`2<System.Object,System.Object>::.ctor(UniRx.IObservable`1<T>,System.Func`3<T,T,TR>)
extern "C"  void PairwiseObservable_2__ctor_m653593912_gshared (PairwiseObservable_2_t3262709271 * __this, Il2CppObject* ___source0, Func_3_t1892209229 * ___selector1, const MethodInfo* method);
#define PairwiseObservable_2__ctor_m653593912(__this, ___source0, ___selector1, method) ((  void (*) (PairwiseObservable_2_t3262709271 *, Il2CppObject*, Func_3_t1892209229 *, const MethodInfo*))PairwiseObservable_2__ctor_m653593912_gshared)(__this, ___source0, ___selector1, method)
// System.IDisposable UniRx.Operators.PairwiseObservable`2<System.Object,System.Object>::SubscribeCore(UniRx.IObserver`1<TR>,System.IDisposable)
extern "C"  Il2CppObject * PairwiseObservable_2_SubscribeCore_m4053918239_gshared (PairwiseObservable_2_t3262709271 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define PairwiseObservable_2_SubscribeCore_m4053918239(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (PairwiseObservable_2_t3262709271 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))PairwiseObservable_2_SubscribeCore_m4053918239_gshared)(__this, ___observer0, ___cancel1, method)
