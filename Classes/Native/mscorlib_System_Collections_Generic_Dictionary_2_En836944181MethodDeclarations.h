﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En705561699MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.String>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1331191095(__this, ___dictionary0, method) ((  void (*) (Enumerator_t836944181 *, Dictionary_2_t1069916240 *, const MethodInfo*))Enumerator__ctor_m3517588453_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.String>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m637482068(__this, method) ((  Il2CppObject * (*) (Enumerator_t836944181 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3376064358_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.String>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3639836446(__this, method) ((  void (*) (Enumerator_t836944181 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2654939056_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.String>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m81676181(__this, method) ((  DictionaryEntry_t130027246  (*) (Enumerator_t836944181 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1741064359_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.String>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1474486832(__this, method) ((  Il2CppObject * (*) (Enumerator_t836944181 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2276212674_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.String>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m691074242(__this, method) ((  Il2CppObject * (*) (Enumerator_t836944181 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2350462420_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.String>::MoveNext()
#define Enumerator_MoveNext_m2520165710(__this, method) ((  bool (*) (Enumerator_t836944181 *, const MethodInfo*))Enumerator_MoveNext_m495617248_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.String>::get_Current()
#define Enumerator_get_Current_m1912370990(__this, method) ((  KeyValuePair_2_t558447538  (*) (Enumerator_t836944181 *, const MethodInfo*))Enumerator_get_Current_m2814877276_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.String>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1543410135(__this, method) ((  int32_t (*) (Enumerator_t836944181 *, const MethodInfo*))Enumerator_get_CurrentKey_m1612903401_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.String>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3638270679(__this, method) ((  String_t* (*) (Enumerator_t836944181 *, const MethodInfo*))Enumerator_get_CurrentValue_m1701822569_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.String>::Reset()
#define Enumerator_Reset_m2774603017(__this, method) ((  void (*) (Enumerator_t836944181 *, const MethodInfo*))Enumerator_Reset_m1068283575_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.String>::VerifyState()
#define Enumerator_VerifyState_m1235794514(__this, method) ((  void (*) (Enumerator_t836944181 *, const MethodInfo*))Enumerator_VerifyState_m2138300800_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.String>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m637458746(__this, method) ((  void (*) (Enumerator_t836944181 *, const MethodInfo*))Enumerator_VerifyCurrent_m362605800_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.String>::Dispose()
#define Enumerator_Dispose_m1742237593(__this, method) ((  void (*) (Enumerator_t836944181 *, const MethodInfo*))Enumerator_Dispose_m2646760903_gshared)(__this, method)
