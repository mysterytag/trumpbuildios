﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AdToApp.AndroidWrapper.AdToAppAndroidWrapper/<ShowBanner>c__AnonStorey2A
struct U3CShowBannerU3Ec__AnonStorey2A_t18990589;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper/<ShowBanner>c__AnonStorey2A::.ctor()
extern "C"  void U3CShowBannerU3Ec__AnonStorey2A__ctor_m2365810617 (U3CShowBannerU3Ec__AnonStorey2A_t18990589 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AndroidWrapper.AdToAppAndroidWrapper/<ShowBanner>c__AnonStorey2A::<>m__1(System.Object)
extern "C"  void U3CShowBannerU3Ec__AnonStorey2A_U3CU3Em__1_m940804003 (U3CShowBannerU3Ec__AnonStorey2A_t18990589 * __this, Il2CppObject * ___wrapperContext0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
