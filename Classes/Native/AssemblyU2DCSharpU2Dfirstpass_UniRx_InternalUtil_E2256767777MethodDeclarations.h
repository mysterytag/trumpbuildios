﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.EmptyObserver`1<System.Int32>
struct EmptyObserver_1_t2256767777;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Int32>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m1369830975_gshared (EmptyObserver_1_t2256767777 * __this, const MethodInfo* method);
#define EmptyObserver_1__ctor_m1369830975(__this, method) ((  void (*) (EmptyObserver_1_t2256767777 *, const MethodInfo*))EmptyObserver_1__ctor_m1369830975_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Int32>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m3327958350_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EmptyObserver_1__cctor_m3327958350(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EmptyObserver_1__cctor_m3327958350_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Int32>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m1953054345_gshared (EmptyObserver_1_t2256767777 * __this, const MethodInfo* method);
#define EmptyObserver_1_OnCompleted_m1953054345(__this, method) ((  void (*) (EmptyObserver_1_t2256767777 *, const MethodInfo*))EmptyObserver_1_OnCompleted_m1953054345_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Int32>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m2605017654_gshared (EmptyObserver_1_t2256767777 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define EmptyObserver_1_OnError_m2605017654(__this, ___error0, method) ((  void (*) (EmptyObserver_1_t2256767777 *, Exception_t1967233988 *, const MethodInfo*))EmptyObserver_1_OnError_m2605017654_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<System.Int32>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m683310375_gshared (EmptyObserver_1_t2256767777 * __this, int32_t ___value0, const MethodInfo* method);
#define EmptyObserver_1_OnNext_m683310375(__this, ___value0, method) ((  void (*) (EmptyObserver_1_t2256767777 *, int32_t, const MethodInfo*))EmptyObserver_1_OnNext_m683310375_gshared)(__this, ___value0, method)
