﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.UpdateSharedGroupDataResult
struct UpdateSharedGroupDataResult_t1620468680;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.UpdateSharedGroupDataResult::.ctor()
extern "C"  void UpdateSharedGroupDataResult__ctor_m3938722785 (UpdateSharedGroupDataResult_t1620468680 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
