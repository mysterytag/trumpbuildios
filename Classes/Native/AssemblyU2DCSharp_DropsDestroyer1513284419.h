﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GameController
struct GameController_t2782302542;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DropsDestroyer
struct  DropsDestroyer_t1513284419  : public MonoBehaviour_t3012272455
{
public:
	// GameController DropsDestroyer::GameController
	GameController_t2782302542 * ___GameController_2;

public:
	inline static int32_t get_offset_of_GameController_2() { return static_cast<int32_t>(offsetof(DropsDestroyer_t1513284419, ___GameController_2)); }
	inline GameController_t2782302542 * get_GameController_2() const { return ___GameController_2; }
	inline GameController_t2782302542 ** get_address_of_GameController_2() { return &___GameController_2; }
	inline void set_GameController_2(GameController_t2782302542 * value)
	{
		___GameController_2 = value;
		Il2CppCodeGenWriteBarrier(&___GameController_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
