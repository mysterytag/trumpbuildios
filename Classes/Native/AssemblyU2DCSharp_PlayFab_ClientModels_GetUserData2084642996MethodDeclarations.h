﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetUserDataRequest
struct GetUserDataRequest_t2084642996;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen1438485399.h"

// System.Void PlayFab.ClientModels.GetUserDataRequest::.ctor()
extern "C"  void GetUserDataRequest__ctor_m292591497 (GetUserDataRequest_t2084642996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.GetUserDataRequest::get_Keys()
extern "C"  List_1_t1765447871 * GetUserDataRequest_get_Keys_m2869316615 (GetUserDataRequest_t2084642996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetUserDataRequest::set_Keys(System.Collections.Generic.List`1<System.String>)
extern "C"  void GetUserDataRequest_set_Keys_m286878528 (GetUserDataRequest_t2084642996 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.GetUserDataRequest::get_PlayFabId()
extern "C"  String_t* GetUserDataRequest_get_PlayFabId_m3391920271 (GetUserDataRequest_t2084642996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetUserDataRequest::set_PlayFabId(System.String)
extern "C"  void GetUserDataRequest_set_PlayFabId_m2012234634 (GetUserDataRequest_t2084642996 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> PlayFab.ClientModels.GetUserDataRequest::get_IfChangedFromDataVersion()
extern "C"  Nullable_1_t1438485399  GetUserDataRequest_get_IfChangedFromDataVersion_m2814284290 (GetUserDataRequest_t2084642996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetUserDataRequest::set_IfChangedFromDataVersion(System.Nullable`1<System.Int32>)
extern "C"  void GetUserDataRequest_set_IfChangedFromDataVersion_m2790841449 (GetUserDataRequest_t2084642996 * __this, Nullable_1_t1438485399  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
