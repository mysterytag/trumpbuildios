﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ZipLatestFunc`4<System.Object,System.Object,System.Object,System.Object>
struct ZipLatestFunc_4_t1514349918;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void UniRx.Operators.ZipLatestFunc`4<System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void ZipLatestFunc_4__ctor_m3510557552_gshared (ZipLatestFunc_4_t1514349918 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define ZipLatestFunc_4__ctor_m3510557552(__this, ___object0, ___method1, method) ((  void (*) (ZipLatestFunc_4_t1514349918 *, Il2CppObject *, IntPtr_t, const MethodInfo*))ZipLatestFunc_4__ctor_m3510557552_gshared)(__this, ___object0, ___method1, method)
// TR UniRx.Operators.ZipLatestFunc`4<System.Object,System.Object,System.Object,System.Object>::Invoke(T1,T2,T3)
extern "C"  Il2CppObject * ZipLatestFunc_4_Invoke_m2540036115_gshared (ZipLatestFunc_4_t1514349918 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
#define ZipLatestFunc_4_Invoke_m2540036115(__this, ___arg10, ___arg21, ___arg32, method) ((  Il2CppObject * (*) (ZipLatestFunc_4_t1514349918 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))ZipLatestFunc_4_Invoke_m2540036115_gshared)(__this, ___arg10, ___arg21, ___arg32, method)
// System.IAsyncResult UniRx.Operators.ZipLatestFunc`4<System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ZipLatestFunc_4_BeginInvoke_m4019851107_gshared (ZipLatestFunc_4_t1514349918 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, AsyncCallback_t1363551830 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method);
#define ZipLatestFunc_4_BeginInvoke_m4019851107(__this, ___arg10, ___arg21, ___arg32, ___callback3, ___object4, method) ((  Il2CppObject * (*) (ZipLatestFunc_4_t1514349918 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))ZipLatestFunc_4_BeginInvoke_m4019851107_gshared)(__this, ___arg10, ___arg21, ___arg32, ___callback3, ___object4, method)
// TR UniRx.Operators.ZipLatestFunc`4<System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * ZipLatestFunc_4_EndInvoke_m165585703_gshared (ZipLatestFunc_4_t1514349918 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define ZipLatestFunc_4_EndInvoke_m165585703(__this, ___result0, method) ((  Il2CppObject * (*) (ZipLatestFunc_4_t1514349918 *, Il2CppObject *, const MethodInfo*))ZipLatestFunc_4_EndInvoke_m165585703_gshared)(__this, ___result0, method)
