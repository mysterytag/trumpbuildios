﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Sprite
struct Sprite_t4006040370;
// System.Action`1<DonateCell>
struct Action_1_t2946927090;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DonateButton
struct  DonateButton_t670753441  : public Il2CppObject
{
public:
	// System.String DonateButton::Name
	String_t* ___Name_0;
	// System.String DonateButton::TitleTranslateId
	String_t* ___TitleTranslateId_1;
	// System.String DonateButton::SoloTextTranslateId
	String_t* ___SoloTextTranslateId_2;
	// UnityEngine.Sprite DonateButton::SpriteIcon
	Sprite_t4006040370 * ___SpriteIcon_3;
	// System.Action`1<DonateCell> DonateButton::InitializeCell
	Action_1_t2946927090 * ___InitializeCell_4;
	// System.String DonateButton::DescriptionTranslateId
	String_t* ___DescriptionTranslateId_5;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(DonateButton_t670753441, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier(&___Name_0, value);
	}

	inline static int32_t get_offset_of_TitleTranslateId_1() { return static_cast<int32_t>(offsetof(DonateButton_t670753441, ___TitleTranslateId_1)); }
	inline String_t* get_TitleTranslateId_1() const { return ___TitleTranslateId_1; }
	inline String_t** get_address_of_TitleTranslateId_1() { return &___TitleTranslateId_1; }
	inline void set_TitleTranslateId_1(String_t* value)
	{
		___TitleTranslateId_1 = value;
		Il2CppCodeGenWriteBarrier(&___TitleTranslateId_1, value);
	}

	inline static int32_t get_offset_of_SoloTextTranslateId_2() { return static_cast<int32_t>(offsetof(DonateButton_t670753441, ___SoloTextTranslateId_2)); }
	inline String_t* get_SoloTextTranslateId_2() const { return ___SoloTextTranslateId_2; }
	inline String_t** get_address_of_SoloTextTranslateId_2() { return &___SoloTextTranslateId_2; }
	inline void set_SoloTextTranslateId_2(String_t* value)
	{
		___SoloTextTranslateId_2 = value;
		Il2CppCodeGenWriteBarrier(&___SoloTextTranslateId_2, value);
	}

	inline static int32_t get_offset_of_SpriteIcon_3() { return static_cast<int32_t>(offsetof(DonateButton_t670753441, ___SpriteIcon_3)); }
	inline Sprite_t4006040370 * get_SpriteIcon_3() const { return ___SpriteIcon_3; }
	inline Sprite_t4006040370 ** get_address_of_SpriteIcon_3() { return &___SpriteIcon_3; }
	inline void set_SpriteIcon_3(Sprite_t4006040370 * value)
	{
		___SpriteIcon_3 = value;
		Il2CppCodeGenWriteBarrier(&___SpriteIcon_3, value);
	}

	inline static int32_t get_offset_of_InitializeCell_4() { return static_cast<int32_t>(offsetof(DonateButton_t670753441, ___InitializeCell_4)); }
	inline Action_1_t2946927090 * get_InitializeCell_4() const { return ___InitializeCell_4; }
	inline Action_1_t2946927090 ** get_address_of_InitializeCell_4() { return &___InitializeCell_4; }
	inline void set_InitializeCell_4(Action_1_t2946927090 * value)
	{
		___InitializeCell_4 = value;
		Il2CppCodeGenWriteBarrier(&___InitializeCell_4, value);
	}

	inline static int32_t get_offset_of_DescriptionTranslateId_5() { return static_cast<int32_t>(offsetof(DonateButton_t670753441, ___DescriptionTranslateId_5)); }
	inline String_t* get_DescriptionTranslateId_5() const { return ___DescriptionTranslateId_5; }
	inline String_t** get_address_of_DescriptionTranslateId_5() { return &___DescriptionTranslateId_5; }
	inline void set_DescriptionTranslateId_5(String_t* value)
	{
		___DescriptionTranslateId_5 = value;
		Il2CppCodeGenWriteBarrier(&___DescriptionTranslateId_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
