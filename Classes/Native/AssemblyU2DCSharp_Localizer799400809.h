﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Localizer
struct  Localizer_t799400809  : public MonoBehaviour_t3012272455
{
public:
	// System.String Localizer::stringId
	String_t* ___stringId_2;

public:
	inline static int32_t get_offset_of_stringId_2() { return static_cast<int32_t>(offsetof(Localizer_t799400809, ___stringId_2)); }
	inline String_t* get_stringId_2() const { return ___stringId_2; }
	inline String_t** get_address_of_stringId_2() { return &___stringId_2; }
	inline void set_stringId_2(String_t* value)
	{
		___stringId_2 = value;
		Il2CppCodeGenWriteBarrier(&___stringId_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
