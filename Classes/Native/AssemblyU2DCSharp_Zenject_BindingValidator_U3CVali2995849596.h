﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Zenject.DiContainer
struct DiContainer_t2383114449;
// Zenject.InjectContext
struct InjectContext_t3456483891;
// System.Collections.Generic.List`1<Zenject.ProviderBase>
struct List_1_t2424453360;
// System.Collections.Generic.IEnumerator`1<Zenject.ZenjectResolveException>
struct IEnumerator_1_t2684159447;
// Zenject.ZenjectResolveException
struct ZenjectResolveException_t1201052999;
// Zenject.ProviderBase
struct ProviderBase_t1627494391;

#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat510236352.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.BindingValidator/<ValidateContract>c__Iterator49
struct  U3CValidateContractU3Ec__Iterator49_t2995849596  : public Il2CppObject
{
public:
	// Zenject.DiContainer Zenject.BindingValidator/<ValidateContract>c__Iterator49::container
	DiContainer_t2383114449 * ___container_0;
	// Zenject.InjectContext Zenject.BindingValidator/<ValidateContract>c__Iterator49::context
	InjectContext_t3456483891 * ___context_1;
	// System.Collections.Generic.List`1<Zenject.ProviderBase> Zenject.BindingValidator/<ValidateContract>c__Iterator49::<matches>__0
	List_1_t2424453360 * ___U3CmatchesU3E__0_2;
	// System.Collections.Generic.IEnumerator`1<Zenject.ZenjectResolveException> Zenject.BindingValidator/<ValidateContract>c__Iterator49::<$s_271>__1
	Il2CppObject* ___U3CU24s_271U3E__1_3;
	// Zenject.ZenjectResolveException Zenject.BindingValidator/<ValidateContract>c__Iterator49::<error>__2
	ZenjectResolveException_t1201052999 * ___U3CerrorU3E__2_4;
	// Zenject.InjectContext Zenject.BindingValidator/<ValidateContract>c__Iterator49::<subContext>__3
	InjectContext_t3456483891 * ___U3CsubContextU3E__3_5;
	// System.Collections.Generic.IEnumerator`1<Zenject.ZenjectResolveException> Zenject.BindingValidator/<ValidateContract>c__Iterator49::<$s_272>__4
	Il2CppObject* ___U3CU24s_272U3E__4_6;
	// Zenject.ZenjectResolveException Zenject.BindingValidator/<ValidateContract>c__Iterator49::<error>__5
	ZenjectResolveException_t1201052999 * ___U3CerrorU3E__5_7;
	// System.Collections.Generic.List`1/Enumerator<Zenject.ProviderBase> Zenject.BindingValidator/<ValidateContract>c__Iterator49::<$s_273>__6
	Enumerator_t510236352  ___U3CU24s_273U3E__6_8;
	// Zenject.ProviderBase Zenject.BindingValidator/<ValidateContract>c__Iterator49::<match>__7
	ProviderBase_t1627494391 * ___U3CmatchU3E__7_9;
	// System.Collections.Generic.IEnumerator`1<Zenject.ZenjectResolveException> Zenject.BindingValidator/<ValidateContract>c__Iterator49::<$s_274>__8
	Il2CppObject* ___U3CU24s_274U3E__8_10;
	// Zenject.ZenjectResolveException Zenject.BindingValidator/<ValidateContract>c__Iterator49::<error>__9
	ZenjectResolveException_t1201052999 * ___U3CerrorU3E__9_11;
	// System.Collections.Generic.IEnumerator`1<Zenject.ZenjectResolveException> Zenject.BindingValidator/<ValidateContract>c__Iterator49::<$s_275>__10
	Il2CppObject* ___U3CU24s_275U3E__10_12;
	// Zenject.ZenjectResolveException Zenject.BindingValidator/<ValidateContract>c__Iterator49::<error>__11
	ZenjectResolveException_t1201052999 * ___U3CerrorU3E__11_13;
	// System.Int32 Zenject.BindingValidator/<ValidateContract>c__Iterator49::$PC
	int32_t ___U24PC_14;
	// Zenject.ZenjectResolveException Zenject.BindingValidator/<ValidateContract>c__Iterator49::$current
	ZenjectResolveException_t1201052999 * ___U24current_15;
	// Zenject.DiContainer Zenject.BindingValidator/<ValidateContract>c__Iterator49::<$>container
	DiContainer_t2383114449 * ___U3CU24U3Econtainer_16;
	// Zenject.InjectContext Zenject.BindingValidator/<ValidateContract>c__Iterator49::<$>context
	InjectContext_t3456483891 * ___U3CU24U3Econtext_17;

public:
	inline static int32_t get_offset_of_container_0() { return static_cast<int32_t>(offsetof(U3CValidateContractU3Ec__Iterator49_t2995849596, ___container_0)); }
	inline DiContainer_t2383114449 * get_container_0() const { return ___container_0; }
	inline DiContainer_t2383114449 ** get_address_of_container_0() { return &___container_0; }
	inline void set_container_0(DiContainer_t2383114449 * value)
	{
		___container_0 = value;
		Il2CppCodeGenWriteBarrier(&___container_0, value);
	}

	inline static int32_t get_offset_of_context_1() { return static_cast<int32_t>(offsetof(U3CValidateContractU3Ec__Iterator49_t2995849596, ___context_1)); }
	inline InjectContext_t3456483891 * get_context_1() const { return ___context_1; }
	inline InjectContext_t3456483891 ** get_address_of_context_1() { return &___context_1; }
	inline void set_context_1(InjectContext_t3456483891 * value)
	{
		___context_1 = value;
		Il2CppCodeGenWriteBarrier(&___context_1, value);
	}

	inline static int32_t get_offset_of_U3CmatchesU3E__0_2() { return static_cast<int32_t>(offsetof(U3CValidateContractU3Ec__Iterator49_t2995849596, ___U3CmatchesU3E__0_2)); }
	inline List_1_t2424453360 * get_U3CmatchesU3E__0_2() const { return ___U3CmatchesU3E__0_2; }
	inline List_1_t2424453360 ** get_address_of_U3CmatchesU3E__0_2() { return &___U3CmatchesU3E__0_2; }
	inline void set_U3CmatchesU3E__0_2(List_1_t2424453360 * value)
	{
		___U3CmatchesU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmatchesU3E__0_2, value);
	}

	inline static int32_t get_offset_of_U3CU24s_271U3E__1_3() { return static_cast<int32_t>(offsetof(U3CValidateContractU3Ec__Iterator49_t2995849596, ___U3CU24s_271U3E__1_3)); }
	inline Il2CppObject* get_U3CU24s_271U3E__1_3() const { return ___U3CU24s_271U3E__1_3; }
	inline Il2CppObject** get_address_of_U3CU24s_271U3E__1_3() { return &___U3CU24s_271U3E__1_3; }
	inline void set_U3CU24s_271U3E__1_3(Il2CppObject* value)
	{
		___U3CU24s_271U3E__1_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_271U3E__1_3, value);
	}

	inline static int32_t get_offset_of_U3CerrorU3E__2_4() { return static_cast<int32_t>(offsetof(U3CValidateContractU3Ec__Iterator49_t2995849596, ___U3CerrorU3E__2_4)); }
	inline ZenjectResolveException_t1201052999 * get_U3CerrorU3E__2_4() const { return ___U3CerrorU3E__2_4; }
	inline ZenjectResolveException_t1201052999 ** get_address_of_U3CerrorU3E__2_4() { return &___U3CerrorU3E__2_4; }
	inline void set_U3CerrorU3E__2_4(ZenjectResolveException_t1201052999 * value)
	{
		___U3CerrorU3E__2_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CerrorU3E__2_4, value);
	}

	inline static int32_t get_offset_of_U3CsubContextU3E__3_5() { return static_cast<int32_t>(offsetof(U3CValidateContractU3Ec__Iterator49_t2995849596, ___U3CsubContextU3E__3_5)); }
	inline InjectContext_t3456483891 * get_U3CsubContextU3E__3_5() const { return ___U3CsubContextU3E__3_5; }
	inline InjectContext_t3456483891 ** get_address_of_U3CsubContextU3E__3_5() { return &___U3CsubContextU3E__3_5; }
	inline void set_U3CsubContextU3E__3_5(InjectContext_t3456483891 * value)
	{
		___U3CsubContextU3E__3_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CsubContextU3E__3_5, value);
	}

	inline static int32_t get_offset_of_U3CU24s_272U3E__4_6() { return static_cast<int32_t>(offsetof(U3CValidateContractU3Ec__Iterator49_t2995849596, ___U3CU24s_272U3E__4_6)); }
	inline Il2CppObject* get_U3CU24s_272U3E__4_6() const { return ___U3CU24s_272U3E__4_6; }
	inline Il2CppObject** get_address_of_U3CU24s_272U3E__4_6() { return &___U3CU24s_272U3E__4_6; }
	inline void set_U3CU24s_272U3E__4_6(Il2CppObject* value)
	{
		___U3CU24s_272U3E__4_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_272U3E__4_6, value);
	}

	inline static int32_t get_offset_of_U3CerrorU3E__5_7() { return static_cast<int32_t>(offsetof(U3CValidateContractU3Ec__Iterator49_t2995849596, ___U3CerrorU3E__5_7)); }
	inline ZenjectResolveException_t1201052999 * get_U3CerrorU3E__5_7() const { return ___U3CerrorU3E__5_7; }
	inline ZenjectResolveException_t1201052999 ** get_address_of_U3CerrorU3E__5_7() { return &___U3CerrorU3E__5_7; }
	inline void set_U3CerrorU3E__5_7(ZenjectResolveException_t1201052999 * value)
	{
		___U3CerrorU3E__5_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CerrorU3E__5_7, value);
	}

	inline static int32_t get_offset_of_U3CU24s_273U3E__6_8() { return static_cast<int32_t>(offsetof(U3CValidateContractU3Ec__Iterator49_t2995849596, ___U3CU24s_273U3E__6_8)); }
	inline Enumerator_t510236352  get_U3CU24s_273U3E__6_8() const { return ___U3CU24s_273U3E__6_8; }
	inline Enumerator_t510236352 * get_address_of_U3CU24s_273U3E__6_8() { return &___U3CU24s_273U3E__6_8; }
	inline void set_U3CU24s_273U3E__6_8(Enumerator_t510236352  value)
	{
		___U3CU24s_273U3E__6_8 = value;
	}

	inline static int32_t get_offset_of_U3CmatchU3E__7_9() { return static_cast<int32_t>(offsetof(U3CValidateContractU3Ec__Iterator49_t2995849596, ___U3CmatchU3E__7_9)); }
	inline ProviderBase_t1627494391 * get_U3CmatchU3E__7_9() const { return ___U3CmatchU3E__7_9; }
	inline ProviderBase_t1627494391 ** get_address_of_U3CmatchU3E__7_9() { return &___U3CmatchU3E__7_9; }
	inline void set_U3CmatchU3E__7_9(ProviderBase_t1627494391 * value)
	{
		___U3CmatchU3E__7_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmatchU3E__7_9, value);
	}

	inline static int32_t get_offset_of_U3CU24s_274U3E__8_10() { return static_cast<int32_t>(offsetof(U3CValidateContractU3Ec__Iterator49_t2995849596, ___U3CU24s_274U3E__8_10)); }
	inline Il2CppObject* get_U3CU24s_274U3E__8_10() const { return ___U3CU24s_274U3E__8_10; }
	inline Il2CppObject** get_address_of_U3CU24s_274U3E__8_10() { return &___U3CU24s_274U3E__8_10; }
	inline void set_U3CU24s_274U3E__8_10(Il2CppObject* value)
	{
		___U3CU24s_274U3E__8_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_274U3E__8_10, value);
	}

	inline static int32_t get_offset_of_U3CerrorU3E__9_11() { return static_cast<int32_t>(offsetof(U3CValidateContractU3Ec__Iterator49_t2995849596, ___U3CerrorU3E__9_11)); }
	inline ZenjectResolveException_t1201052999 * get_U3CerrorU3E__9_11() const { return ___U3CerrorU3E__9_11; }
	inline ZenjectResolveException_t1201052999 ** get_address_of_U3CerrorU3E__9_11() { return &___U3CerrorU3E__9_11; }
	inline void set_U3CerrorU3E__9_11(ZenjectResolveException_t1201052999 * value)
	{
		___U3CerrorU3E__9_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CerrorU3E__9_11, value);
	}

	inline static int32_t get_offset_of_U3CU24s_275U3E__10_12() { return static_cast<int32_t>(offsetof(U3CValidateContractU3Ec__Iterator49_t2995849596, ___U3CU24s_275U3E__10_12)); }
	inline Il2CppObject* get_U3CU24s_275U3E__10_12() const { return ___U3CU24s_275U3E__10_12; }
	inline Il2CppObject** get_address_of_U3CU24s_275U3E__10_12() { return &___U3CU24s_275U3E__10_12; }
	inline void set_U3CU24s_275U3E__10_12(Il2CppObject* value)
	{
		___U3CU24s_275U3E__10_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_275U3E__10_12, value);
	}

	inline static int32_t get_offset_of_U3CerrorU3E__11_13() { return static_cast<int32_t>(offsetof(U3CValidateContractU3Ec__Iterator49_t2995849596, ___U3CerrorU3E__11_13)); }
	inline ZenjectResolveException_t1201052999 * get_U3CerrorU3E__11_13() const { return ___U3CerrorU3E__11_13; }
	inline ZenjectResolveException_t1201052999 ** get_address_of_U3CerrorU3E__11_13() { return &___U3CerrorU3E__11_13; }
	inline void set_U3CerrorU3E__11_13(ZenjectResolveException_t1201052999 * value)
	{
		___U3CerrorU3E__11_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CerrorU3E__11_13, value);
	}

	inline static int32_t get_offset_of_U24PC_14() { return static_cast<int32_t>(offsetof(U3CValidateContractU3Ec__Iterator49_t2995849596, ___U24PC_14)); }
	inline int32_t get_U24PC_14() const { return ___U24PC_14; }
	inline int32_t* get_address_of_U24PC_14() { return &___U24PC_14; }
	inline void set_U24PC_14(int32_t value)
	{
		___U24PC_14 = value;
	}

	inline static int32_t get_offset_of_U24current_15() { return static_cast<int32_t>(offsetof(U3CValidateContractU3Ec__Iterator49_t2995849596, ___U24current_15)); }
	inline ZenjectResolveException_t1201052999 * get_U24current_15() const { return ___U24current_15; }
	inline ZenjectResolveException_t1201052999 ** get_address_of_U24current_15() { return &___U24current_15; }
	inline void set_U24current_15(ZenjectResolveException_t1201052999 * value)
	{
		___U24current_15 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_15, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Econtainer_16() { return static_cast<int32_t>(offsetof(U3CValidateContractU3Ec__Iterator49_t2995849596, ___U3CU24U3Econtainer_16)); }
	inline DiContainer_t2383114449 * get_U3CU24U3Econtainer_16() const { return ___U3CU24U3Econtainer_16; }
	inline DiContainer_t2383114449 ** get_address_of_U3CU24U3Econtainer_16() { return &___U3CU24U3Econtainer_16; }
	inline void set_U3CU24U3Econtainer_16(DiContainer_t2383114449 * value)
	{
		___U3CU24U3Econtainer_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Econtainer_16, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Econtext_17() { return static_cast<int32_t>(offsetof(U3CValidateContractU3Ec__Iterator49_t2995849596, ___U3CU24U3Econtext_17)); }
	inline InjectContext_t3456483891 * get_U3CU24U3Econtext_17() const { return ___U3CU24U3Econtext_17; }
	inline InjectContext_t3456483891 ** get_address_of_U3CU24U3Econtext_17() { return &___U3CU24U3Econtext_17; }
	inline void set_U3CU24U3Econtext_17(InjectContext_t3456483891 * value)
	{
		___U3CU24U3Econtext_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Econtext_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
