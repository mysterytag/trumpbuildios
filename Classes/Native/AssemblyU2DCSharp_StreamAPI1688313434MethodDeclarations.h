﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.IDisposable
struct IDisposable_t1628921374;
// IEmptyStream
struct IEmptyStream_t3684082468;
// System.Action
struct Action_t437523947;
// System.Func`1<System.Boolean>
struct Func_1_t1353786588;
// IEmptyStream[]
struct IEmptyStreamU5BU5D_t1849841613;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"

// System.IDisposable StreamAPI::LateListen(IEmptyStream,System.Action)
extern "C"  Il2CppObject * StreamAPI_LateListen_m34095296 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___stream0, Action_t437523947 * ___action1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// IEmptyStream StreamAPI::Where(IEmptyStream,System.Func`1<System.Boolean>)
extern "C"  Il2CppObject * StreamAPI_Where_m4270226730 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___stream0, Func_1_t1353786588 * ___o1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// IEmptyStream StreamAPI::MergeWith(IEmptyStream,IEmptyStream[])
extern "C"  Il2CppObject * StreamAPI_MergeWith_m2476496894 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___stream0, IEmptyStreamU5BU5D_t1849841613* ___others1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// IEmptyStream StreamAPI::FirstOnly(IEmptyStream)
extern "C"  Il2CppObject * StreamAPI_FirstOnly_m3252061600 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___stream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
