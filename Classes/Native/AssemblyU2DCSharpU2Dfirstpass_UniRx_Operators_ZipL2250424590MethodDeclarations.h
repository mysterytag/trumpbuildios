﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ZipLatestObservable`3/ZipLatest/LeftObserver<System.Object,System.Object,System.Object>
struct LeftObserver_t2250424591;
// UniRx.Operators.ZipLatestObservable`3/ZipLatest<System.Object,System.Object,System.Object>
struct ZipLatest_t2793383209;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.ZipLatestObservable`3/ZipLatest/LeftObserver<System.Object,System.Object,System.Object>::.ctor(UniRx.Operators.ZipLatestObservable`3/ZipLatest<TLeft,TRight,TResult>)
extern "C"  void LeftObserver__ctor_m4230301987_gshared (LeftObserver_t2250424591 * __this, ZipLatest_t2793383209 * ___parent0, const MethodInfo* method);
#define LeftObserver__ctor_m4230301987(__this, ___parent0, method) ((  void (*) (LeftObserver_t2250424591 *, ZipLatest_t2793383209 *, const MethodInfo*))LeftObserver__ctor_m4230301987_gshared)(__this, ___parent0, method)
// System.Void UniRx.Operators.ZipLatestObservable`3/ZipLatest/LeftObserver<System.Object,System.Object,System.Object>::OnNext(TLeft)
extern "C"  void LeftObserver_OnNext_m1283712940_gshared (LeftObserver_t2250424591 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define LeftObserver_OnNext_m1283712940(__this, ___value0, method) ((  void (*) (LeftObserver_t2250424591 *, Il2CppObject *, const MethodInfo*))LeftObserver_OnNext_m1283712940_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.ZipLatestObservable`3/ZipLatest/LeftObserver<System.Object,System.Object,System.Object>::OnError(System.Exception)
extern "C"  void LeftObserver_OnError_m2810200322_gshared (LeftObserver_t2250424591 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define LeftObserver_OnError_m2810200322(__this, ___error0, method) ((  void (*) (LeftObserver_t2250424591 *, Exception_t1967233988 *, const MethodInfo*))LeftObserver_OnError_m2810200322_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.ZipLatestObservable`3/ZipLatest/LeftObserver<System.Object,System.Object,System.Object>::OnCompleted()
extern "C"  void LeftObserver_OnCompleted_m2675369813_gshared (LeftObserver_t2250424591 * __this, const MethodInfo* method);
#define LeftObserver_OnCompleted_m2675369813(__this, method) ((  void (*) (LeftObserver_t2250424591 *, const MethodInfo*))LeftObserver_OnCompleted_m2675369813_gshared)(__this, method)
