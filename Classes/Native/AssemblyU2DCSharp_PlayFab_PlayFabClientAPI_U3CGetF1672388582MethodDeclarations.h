﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<GetFriendsList>c__AnonStoreyAE
struct U3CGetFriendsListU3Ec__AnonStoreyAE_t1672388582;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<GetFriendsList>c__AnonStoreyAE::.ctor()
extern "C"  void U3CGetFriendsListU3Ec__AnonStoreyAE__ctor_m819804813 (U3CGetFriendsListU3Ec__AnonStoreyAE_t1672388582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<GetFriendsList>c__AnonStoreyAE::<>m__9B(PlayFab.CallRequestContainer)
extern "C"  void U3CGetFriendsListU3Ec__AnonStoreyAE_U3CU3Em__9B_m498003476 (U3CGetFriendsListU3Ec__AnonStoreyAE_t1672388582 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
