﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.LinkCustomIDRequest
struct LinkCustomIDRequest_t3838085433;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.LinkCustomIDRequest::.ctor()
extern "C"  void LinkCustomIDRequest__ctor_m336790416 (LinkCustomIDRequest_t3838085433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LinkCustomIDRequest::get_CustomId()
extern "C"  String_t* LinkCustomIDRequest_get_CustomId_m2391187404 (LinkCustomIDRequest_t3838085433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LinkCustomIDRequest::set_CustomId(System.String)
extern "C"  void LinkCustomIDRequest_set_CustomId_m1580406789 (LinkCustomIDRequest_t3838085433 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
