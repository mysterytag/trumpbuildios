﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Rect>
struct DisposedObserver_1_t699886279;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "UnityEngine_UnityEngine_Rect1525428817.h"

// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Rect>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m73590925_gshared (DisposedObserver_1_t699886279 * __this, const MethodInfo* method);
#define DisposedObserver_1__ctor_m73590925(__this, method) ((  void (*) (DisposedObserver_1_t699886279 *, const MethodInfo*))DisposedObserver_1__ctor_m73590925_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Rect>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m1799222464_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define DisposedObserver_1__cctor_m1799222464(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))DisposedObserver_1__cctor_m1799222464_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Rect>::OnCompleted()
extern "C"  void DisposedObserver_1_OnCompleted_m3964583511_gshared (DisposedObserver_1_t699886279 * __this, const MethodInfo* method);
#define DisposedObserver_1_OnCompleted_m3964583511(__this, method) ((  void (*) (DisposedObserver_1_t699886279 *, const MethodInfo*))DisposedObserver_1_OnCompleted_m3964583511_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Rect>::OnError(System.Exception)
extern "C"  void DisposedObserver_1_OnError_m1364436228_gshared (DisposedObserver_1_t699886279 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define DisposedObserver_1_OnError_m1364436228(__this, ___error0, method) ((  void (*) (DisposedObserver_1_t699886279 *, Exception_t1967233988 *, const MethodInfo*))DisposedObserver_1_OnError_m1364436228_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UnityEngine.Rect>::OnNext(T)
extern "C"  void DisposedObserver_1_OnNext_m537138165_gshared (DisposedObserver_1_t699886279 * __this, Rect_t1525428817  ___value0, const MethodInfo* method);
#define DisposedObserver_1_OnNext_m537138165(__this, ___value0, method) ((  void (*) (DisposedObserver_1_t699886279 *, Rect_t1525428817 , const MethodInfo*))DisposedObserver_1_OnNext_m537138165_gshared)(__this, ___value0, method)
