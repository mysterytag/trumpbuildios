﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.UploadFileCompletedEventHandler
struct UploadFileCompletedEventHandler_t95467880;
// System.Object
struct Il2CppObject;
// System.Net.UploadFileCompletedEventArgs
struct UploadFileCompletedEventArgs_t2540349971;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "System_System_Net_UploadFileCompletedEventArgs2540349971.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Net.UploadFileCompletedEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void UploadFileCompletedEventHandler__ctor_m789301684 (UploadFileCompletedEventHandler_t95467880 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.UploadFileCompletedEventHandler::Invoke(System.Object,System.Net.UploadFileCompletedEventArgs)
extern "C"  void UploadFileCompletedEventHandler_Invoke_m3490991705 (UploadFileCompletedEventHandler_t95467880 * __this, Il2CppObject * ___sender0, UploadFileCompletedEventArgs_t2540349971 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_UploadFileCompletedEventHandler_t95467880(Il2CppObject* delegate, Il2CppObject * ___sender0, UploadFileCompletedEventArgs_t2540349971 * ___e1);
// System.IAsyncResult System.Net.UploadFileCompletedEventHandler::BeginInvoke(System.Object,System.Net.UploadFileCompletedEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UploadFileCompletedEventHandler_BeginInvoke_m4056259188 (UploadFileCompletedEventHandler_t95467880 * __this, Il2CppObject * ___sender0, UploadFileCompletedEventArgs_t2540349971 * ___e1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.UploadFileCompletedEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void UploadFileCompletedEventHandler_EndInvoke_m1334851012 (UploadFileCompletedEventHandler_t95467880 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
