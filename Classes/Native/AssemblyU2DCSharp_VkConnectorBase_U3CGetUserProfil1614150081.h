﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<VKProfileInfo>
struct Action_1_t3622102371;
// VkConnectorBase
struct VkConnectorBase_t2999232489;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VkConnectorBase/<GetUserProfile>c__AnonStoreyD8
struct  U3CGetUserProfileU3Ec__AnonStoreyD8_t1614150081  : public Il2CppObject
{
public:
	// System.Action`1<VKProfileInfo> VkConnectorBase/<GetUserProfile>c__AnonStoreyD8::doWithProfile
	Action_1_t3622102371 * ___doWithProfile_0;
	// VkConnectorBase VkConnectorBase/<GetUserProfile>c__AnonStoreyD8::<>f__this
	VkConnectorBase_t2999232489 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_doWithProfile_0() { return static_cast<int32_t>(offsetof(U3CGetUserProfileU3Ec__AnonStoreyD8_t1614150081, ___doWithProfile_0)); }
	inline Action_1_t3622102371 * get_doWithProfile_0() const { return ___doWithProfile_0; }
	inline Action_1_t3622102371 ** get_address_of_doWithProfile_0() { return &___doWithProfile_0; }
	inline void set_doWithProfile_0(Action_1_t3622102371 * value)
	{
		___doWithProfile_0 = value;
		Il2CppCodeGenWriteBarrier(&___doWithProfile_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CGetUserProfileU3Ec__AnonStoreyD8_t1614150081, ___U3CU3Ef__this_1)); }
	inline VkConnectorBase_t2999232489 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline VkConnectorBase_t2999232489 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(VkConnectorBase_t2999232489 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
