﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<GetUserStatistics>c__AnonStorey96
struct U3CGetUserStatisticsU3Ec__AnonStorey96_t1869467214;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<GetUserStatistics>c__AnonStorey96::.ctor()
extern "C"  void U3CGetUserStatisticsU3Ec__AnonStorey96__ctor_m1590793413 (U3CGetUserStatisticsU3Ec__AnonStorey96_t1869467214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<GetUserStatistics>c__AnonStorey96::<>m__83(PlayFab.CallRequestContainer)
extern "C"  void U3CGetUserStatisticsU3Ec__AnonStorey96_U3CU3Em__83_m270637726 (U3CGetUserStatisticsU3Ec__AnonStorey96_t1869467214 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
