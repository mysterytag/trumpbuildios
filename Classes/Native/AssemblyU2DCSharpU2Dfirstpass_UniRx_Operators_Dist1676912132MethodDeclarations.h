﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.DistinctUntilChangedObservable`1/DistinctUntilChanged<System.Boolean>
struct DistinctUntilChanged_t1676912132;
// UniRx.Operators.DistinctUntilChangedObservable`1<System.Boolean>
struct DistinctUntilChangedObservable_1_t3199977885;
// UniRx.IObserver`1<System.Boolean>
struct IObserver_1_t2423004244;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.DistinctUntilChangedObservable`1/DistinctUntilChanged<System.Boolean>::.ctor(UniRx.Operators.DistinctUntilChangedObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void DistinctUntilChanged__ctor_m3524137875_gshared (DistinctUntilChanged_t1676912132 * __this, DistinctUntilChangedObservable_1_t3199977885 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define DistinctUntilChanged__ctor_m3524137875(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (DistinctUntilChanged_t1676912132 *, DistinctUntilChangedObservable_1_t3199977885 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))DistinctUntilChanged__ctor_m3524137875_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.DistinctUntilChangedObservable`1/DistinctUntilChanged<System.Boolean>::OnNext(T)
extern "C"  void DistinctUntilChanged_OnNext_m1542520834_gshared (DistinctUntilChanged_t1676912132 * __this, bool ___value0, const MethodInfo* method);
#define DistinctUntilChanged_OnNext_m1542520834(__this, ___value0, method) ((  void (*) (DistinctUntilChanged_t1676912132 *, bool, const MethodInfo*))DistinctUntilChanged_OnNext_m1542520834_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.DistinctUntilChangedObservable`1/DistinctUntilChanged<System.Boolean>::OnError(System.Exception)
extern "C"  void DistinctUntilChanged_OnError_m3867727633_gshared (DistinctUntilChanged_t1676912132 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define DistinctUntilChanged_OnError_m3867727633(__this, ___error0, method) ((  void (*) (DistinctUntilChanged_t1676912132 *, Exception_t1967233988 *, const MethodInfo*))DistinctUntilChanged_OnError_m3867727633_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.DistinctUntilChangedObservable`1/DistinctUntilChanged<System.Boolean>::OnCompleted()
extern "C"  void DistinctUntilChanged_OnCompleted_m1352457188_gshared (DistinctUntilChanged_t1676912132 * __this, const MethodInfo* method);
#define DistinctUntilChanged_OnCompleted_m1352457188(__this, method) ((  void (*) (DistinctUntilChanged_t1676912132 *, const MethodInfo*))DistinctUntilChanged_OnCompleted_m1352457188_gshared)(__this, method)
