﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// <>__AnonType3`2<System.Boolean,System.Int32>
struct U3CU3E__AnonType3_2_t3095608592;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void <>__AnonType3`2<System.Boolean,System.Int32>::.ctor(<b>__T,<i>__T)
extern "C"  void U3CU3E__AnonType3_2__ctor_m4134975535_gshared (U3CU3E__AnonType3_2_t3095608592 * __this, bool ___b0, int32_t ___i1, const MethodInfo* method);
#define U3CU3E__AnonType3_2__ctor_m4134975535(__this, ___b0, ___i1, method) ((  void (*) (U3CU3E__AnonType3_2_t3095608592 *, bool, int32_t, const MethodInfo*))U3CU3E__AnonType3_2__ctor_m4134975535_gshared)(__this, ___b0, ___i1, method)
// <b>__T <>__AnonType3`2<System.Boolean,System.Int32>::get_b()
extern "C"  bool U3CU3E__AnonType3_2_get_b_m3615758246_gshared (U3CU3E__AnonType3_2_t3095608592 * __this, const MethodInfo* method);
#define U3CU3E__AnonType3_2_get_b_m3615758246(__this, method) ((  bool (*) (U3CU3E__AnonType3_2_t3095608592 *, const MethodInfo*))U3CU3E__AnonType3_2_get_b_m3615758246_gshared)(__this, method)
// <i>__T <>__AnonType3`2<System.Boolean,System.Int32>::get_i()
extern "C"  int32_t U3CU3E__AnonType3_2_get_i_m1283246900_gshared (U3CU3E__AnonType3_2_t3095608592 * __this, const MethodInfo* method);
#define U3CU3E__AnonType3_2_get_i_m1283246900(__this, method) ((  int32_t (*) (U3CU3E__AnonType3_2_t3095608592 *, const MethodInfo*))U3CU3E__AnonType3_2_get_i_m1283246900_gshared)(__this, method)
// System.Boolean <>__AnonType3`2<System.Boolean,System.Int32>::Equals(System.Object)
extern "C"  bool U3CU3E__AnonType3_2_Equals_m3411759213_gshared (U3CU3E__AnonType3_2_t3095608592 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define U3CU3E__AnonType3_2_Equals_m3411759213(__this, ___obj0, method) ((  bool (*) (U3CU3E__AnonType3_2_t3095608592 *, Il2CppObject *, const MethodInfo*))U3CU3E__AnonType3_2_Equals_m3411759213_gshared)(__this, ___obj0, method)
// System.Int32 <>__AnonType3`2<System.Boolean,System.Int32>::GetHashCode()
extern "C"  int32_t U3CU3E__AnonType3_2_GetHashCode_m485663633_gshared (U3CU3E__AnonType3_2_t3095608592 * __this, const MethodInfo* method);
#define U3CU3E__AnonType3_2_GetHashCode_m485663633(__this, method) ((  int32_t (*) (U3CU3E__AnonType3_2_t3095608592 *, const MethodInfo*))U3CU3E__AnonType3_2_GetHashCode_m485663633_gshared)(__this, method)
// System.String <>__AnonType3`2<System.Boolean,System.Int32>::ToString()
extern "C"  String_t* U3CU3E__AnonType3_2_ToString_m715328675_gshared (U3CU3E__AnonType3_2_t3095608592 * __this, const MethodInfo* method);
#define U3CU3E__AnonType3_2_ToString_m715328675(__this, method) ((  String_t* (*) (U3CU3E__AnonType3_2_t3095608592 *, const MethodInfo*))U3CU3E__AnonType3_2_ToString_m715328675_gshared)(__this, method)
