﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.ListObserver`1<System.Byte>
struct ListObserver_1_t3073311492;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<System.Byte>>
struct ImmutableList_1_t1755929927;
// System.Exception
struct Exception_t1967233988;
// UniRx.IObserver`1<System.Byte>
struct IObserver_1_t695725428;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.InternalUtil.ListObserver`1<System.Byte>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m2972945066_gshared (ListObserver_1_t3073311492 * __this, ImmutableList_1_t1755929927 * ___observers0, const MethodInfo* method);
#define ListObserver_1__ctor_m2972945066(__this, ___observers0, method) ((  void (*) (ListObserver_1_t3073311492 *, ImmutableList_1_t1755929927 *, const MethodInfo*))ListObserver_1__ctor_m2972945066_gshared)(__this, ___observers0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<System.Byte>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m3618018146_gshared (ListObserver_1_t3073311492 * __this, const MethodInfo* method);
#define ListObserver_1_OnCompleted_m3618018146(__this, method) ((  void (*) (ListObserver_1_t3073311492 *, const MethodInfo*))ListObserver_1_OnCompleted_m3618018146_gshared)(__this, method)
// System.Void UniRx.InternalUtil.ListObserver`1<System.Byte>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m909459855_gshared (ListObserver_1_t3073311492 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ListObserver_1_OnError_m909459855(__this, ___error0, method) ((  void (*) (ListObserver_1_t3073311492 *, Exception_t1967233988 *, const MethodInfo*))ListObserver_1_OnError_m909459855_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<System.Byte>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m2601395840_gshared (ListObserver_1_t3073311492 * __this, uint8_t ___value0, const MethodInfo* method);
#define ListObserver_1_OnNext_m2601395840(__this, ___value0, method) ((  void (*) (ListObserver_1_t3073311492 *, uint8_t, const MethodInfo*))ListObserver_1_OnNext_m2601395840_gshared)(__this, ___value0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<System.Byte>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m2677204562_gshared (ListObserver_1_t3073311492 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Add_m2677204562(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t3073311492 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Add_m2677204562_gshared)(__this, ___observer0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<System.Byte>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m3786832825_gshared (ListObserver_1_t3073311492 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Remove_m3786832825(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t3073311492 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Remove_m3786832825_gshared)(__this, ___observer0, method)
