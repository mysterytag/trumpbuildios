﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type
struct Type_t;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.String
struct String_t;
// Zenject.SingletonProviderMap
struct SingletonProviderMap_t1557411893;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.Binder
struct  Binder_t3872662847  : public Il2CppObject
{
public:
	// System.Type Zenject.Binder::_contractType
	Type_t * ____contractType_0;
	// Zenject.DiContainer Zenject.Binder::_container
	DiContainer_t2383114449 * ____container_1;
	// System.String Zenject.Binder::_bindIdentifier
	String_t* ____bindIdentifier_2;
	// Zenject.SingletonProviderMap Zenject.Binder::_singletonMap
	SingletonProviderMap_t1557411893 * ____singletonMap_3;

public:
	inline static int32_t get_offset_of__contractType_0() { return static_cast<int32_t>(offsetof(Binder_t3872662847, ____contractType_0)); }
	inline Type_t * get__contractType_0() const { return ____contractType_0; }
	inline Type_t ** get_address_of__contractType_0() { return &____contractType_0; }
	inline void set__contractType_0(Type_t * value)
	{
		____contractType_0 = value;
		Il2CppCodeGenWriteBarrier(&____contractType_0, value);
	}

	inline static int32_t get_offset_of__container_1() { return static_cast<int32_t>(offsetof(Binder_t3872662847, ____container_1)); }
	inline DiContainer_t2383114449 * get__container_1() const { return ____container_1; }
	inline DiContainer_t2383114449 ** get_address_of__container_1() { return &____container_1; }
	inline void set__container_1(DiContainer_t2383114449 * value)
	{
		____container_1 = value;
		Il2CppCodeGenWriteBarrier(&____container_1, value);
	}

	inline static int32_t get_offset_of__bindIdentifier_2() { return static_cast<int32_t>(offsetof(Binder_t3872662847, ____bindIdentifier_2)); }
	inline String_t* get__bindIdentifier_2() const { return ____bindIdentifier_2; }
	inline String_t** get_address_of__bindIdentifier_2() { return &____bindIdentifier_2; }
	inline void set__bindIdentifier_2(String_t* value)
	{
		____bindIdentifier_2 = value;
		Il2CppCodeGenWriteBarrier(&____bindIdentifier_2, value);
	}

	inline static int32_t get_offset_of__singletonMap_3() { return static_cast<int32_t>(offsetof(Binder_t3872662847, ____singletonMap_3)); }
	inline SingletonProviderMap_t1557411893 * get__singletonMap_3() const { return ____singletonMap_3; }
	inline SingletonProviderMap_t1557411893 ** get_address_of__singletonMap_3() { return &____singletonMap_3; }
	inline void set__singletonMap_3(SingletonProviderMap_t1557411893 * value)
	{
		____singletonMap_3 = value;
		Il2CppCodeGenWriteBarrier(&____singletonMap_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
