﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.CurrentGamesRequest
struct CurrentGamesRequest_t3387974807;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen212373688.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.CurrentGamesRequest::.ctor()
extern "C"  void CurrentGamesRequest__ctor_m1443172850 (CurrentGamesRequest_t3387974807 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<PlayFab.ClientModels.Region> PlayFab.ClientModels.CurrentGamesRequest::get_Region()
extern "C"  Nullable_1_t212373688  CurrentGamesRequest_get_Region_m75788213 (CurrentGamesRequest_t3387974807 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CurrentGamesRequest::set_Region(System.Nullable`1<PlayFab.ClientModels.Region>)
extern "C"  void CurrentGamesRequest_set_Region_m3072301612 (CurrentGamesRequest_t3387974807 * __this, Nullable_1_t212373688  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.CurrentGamesRequest::get_BuildVersion()
extern "C"  String_t* CurrentGamesRequest_get_BuildVersion_m3772793672 (CurrentGamesRequest_t3387974807 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CurrentGamesRequest::set_BuildVersion(System.String)
extern "C"  void CurrentGamesRequest_set_BuildVersion_m106086025 (CurrentGamesRequest_t3387974807 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.CurrentGamesRequest::get_GameMode()
extern "C"  String_t* CurrentGamesRequest_get_GameMode_m640173363 (CurrentGamesRequest_t3387974807 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CurrentGamesRequest::set_GameMode(System.String)
extern "C"  void CurrentGamesRequest_set_GameMode_m3345068542 (CurrentGamesRequest_t3387974807 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.CurrentGamesRequest::get_StatisticName()
extern "C"  String_t* CurrentGamesRequest_get_StatisticName_m989280159 (CurrentGamesRequest_t3387974807 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CurrentGamesRequest::set_StatisticName(System.String)
extern "C"  void CurrentGamesRequest_set_StatisticName_m2527718868 (CurrentGamesRequest_t3387974807 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
