﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ZipFunc`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct ZipFunc_7_t396393786;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void UniRx.Operators.ZipFunc`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void ZipFunc_7__ctor_m2125724862_gshared (ZipFunc_7_t396393786 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define ZipFunc_7__ctor_m2125724862(__this, ___object0, ___method1, method) ((  void (*) (ZipFunc_7_t396393786 *, Il2CppObject *, IntPtr_t, const MethodInfo*))ZipFunc_7__ctor_m2125724862_gshared)(__this, ___object0, ___method1, method)
// TR UniRx.Operators.ZipFunc`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Invoke(T1,T2,T3,T4,T5,T6)
extern "C"  Il2CppObject * ZipFunc_7_Invoke_m3456011300_gshared (ZipFunc_7_t396393786 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, Il2CppObject * ___arg65, const MethodInfo* method);
#define ZipFunc_7_Invoke_m3456011300(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, method) ((  Il2CppObject * (*) (ZipFunc_7_t396393786 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))ZipFunc_7_Invoke_m3456011300_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, method)
// System.IAsyncResult UniRx.Operators.ZipFunc`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,T3,T4,T5,T6,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ZipFunc_7_BeginInvoke_m3287376270_gshared (ZipFunc_7_t396393786 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, Il2CppObject * ___arg65, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method);
#define ZipFunc_7_BeginInvoke_m3287376270(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___callback6, ___object7, method) ((  Il2CppObject * (*) (ZipFunc_7_t396393786 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))ZipFunc_7_BeginInvoke_m3287376270_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___callback6, ___object7, method)
// TR UniRx.Operators.ZipFunc`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * ZipFunc_7_EndInvoke_m632624117_gshared (ZipFunc_7_t396393786 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define ZipFunc_7_EndInvoke_m632624117(__this, ___result0, method) ((  Il2CppObject * (*) (ZipFunc_7_t396393786 *, Il2CppObject *, const MethodInfo*))ZipFunc_7_EndInvoke_m632624117_gshared)(__this, ___result0, method)
