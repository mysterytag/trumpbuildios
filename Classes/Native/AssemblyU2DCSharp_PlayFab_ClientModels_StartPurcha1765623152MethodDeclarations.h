﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.StartPurchaseResult
struct StartPurchaseResult_t1765623152;
// System.String
struct String_t;
// System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>
struct List_1_t45083516;
// System.Collections.Generic.List`1<PlayFab.ClientModels.PaymentOption>
struct List_1_t2086233860;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t190145395;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.StartPurchaseResult::.ctor()
extern "C"  void StartPurchaseResult__ctor_m2171934009 (StartPurchaseResult_t1765623152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.StartPurchaseResult::get_OrderId()
extern "C"  String_t* StartPurchaseResult_get_OrderId_m1351194996 (StartPurchaseResult_t1765623152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.StartPurchaseResult::set_OrderId(System.String)
extern "C"  void StartPurchaseResult_set_OrderId_m283676383 (StartPurchaseResult_t1765623152 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem> PlayFab.ClientModels.StartPurchaseResult::get_Contents()
extern "C"  List_1_t45083516 * StartPurchaseResult_get_Contents_m1610386048 (StartPurchaseResult_t1765623152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.StartPurchaseResult::set_Contents(System.Collections.Generic.List`1<PlayFab.ClientModels.CartItem>)
extern "C"  void StartPurchaseResult_set_Contents_m269900343 (StartPurchaseResult_t1765623152 * __this, List_1_t45083516 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PlayFab.ClientModels.PaymentOption> PlayFab.ClientModels.StartPurchaseResult::get_PaymentOptions()
extern "C"  List_1_t2086233860 * StartPurchaseResult_get_PaymentOptions_m3124494844 (StartPurchaseResult_t1765623152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.StartPurchaseResult::set_PaymentOptions(System.Collections.Generic.List`1<PlayFab.ClientModels.PaymentOption>)
extern "C"  void StartPurchaseResult_set_PaymentOptions_m2813423247 (StartPurchaseResult_t1765623152 * __this, List_1_t2086233860 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32> PlayFab.ClientModels.StartPurchaseResult::get_VirtualCurrencyBalances()
extern "C"  Dictionary_2_t190145395 * StartPurchaseResult_get_VirtualCurrencyBalances_m922486076 (StartPurchaseResult_t1765623152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.StartPurchaseResult::set_VirtualCurrencyBalances(System.Collections.Generic.Dictionary`2<System.String,System.Int32>)
extern "C"  void StartPurchaseResult_set_VirtualCurrencyBalances_m2111379543 (StartPurchaseResult_t1765623152 * __this, Dictionary_2_t190145395 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
