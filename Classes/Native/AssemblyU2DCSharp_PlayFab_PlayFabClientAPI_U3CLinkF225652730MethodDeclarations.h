﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<LinkFacebookAccount>c__AnonStorey77
struct U3CLinkFacebookAccountU3Ec__AnonStorey77_t225652730;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<LinkFacebookAccount>c__AnonStorey77::.ctor()
extern "C"  void U3CLinkFacebookAccountU3Ec__AnonStorey77__ctor_m721674393 (U3CLinkFacebookAccountU3Ec__AnonStorey77_t225652730 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<LinkFacebookAccount>c__AnonStorey77::<>m__64(PlayFab.CallRequestContainer)
extern "C"  void U3CLinkFacebookAccountU3Ec__AnonStorey77_U3CU3Em__64_m2160903157 (U3CLinkFacebookAccountU3Ec__AnonStorey77_t225652730 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
