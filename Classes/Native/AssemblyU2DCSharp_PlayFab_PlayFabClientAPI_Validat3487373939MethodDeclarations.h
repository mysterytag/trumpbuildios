﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/ValidateIOSReceiptRequestCallback
struct ValidateIOSReceiptRequestCallback_t3487373939;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.ValidateIOSReceiptRequest
struct ValidateIOSReceiptRequest_t1279430686;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ValidateIOS1279430686.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/ValidateIOSReceiptRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void ValidateIOSReceiptRequestCallback__ctor_m1427649922 (ValidateIOSReceiptRequestCallback_t3487373939 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/ValidateIOSReceiptRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.ValidateIOSReceiptRequest,System.Object)
extern "C"  void ValidateIOSReceiptRequestCallback_Invoke_m236970611 (ValidateIOSReceiptRequestCallback_t3487373939 * __this, String_t* ___urlPath0, int32_t ___callId1, ValidateIOSReceiptRequest_t1279430686 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_ValidateIOSReceiptRequestCallback_t3487373939(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, ValidateIOSReceiptRequest_t1279430686 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/ValidateIOSReceiptRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.ValidateIOSReceiptRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ValidateIOSReceiptRequestCallback_BeginInvoke_m2174407550 (ValidateIOSReceiptRequestCallback_t3487373939 * __this, String_t* ___urlPath0, int32_t ___callId1, ValidateIOSReceiptRequest_t1279430686 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/ValidateIOSReceiptRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void ValidateIOSReceiptRequestCallback_EndInvoke_m2505488530 (ValidateIOSReceiptRequestCallback_t3487373939 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
