﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnonymousEmptyStream
struct AnonymousEmptyStream_t62006432;
// System.Func`3<System.Action,Priority,System.IDisposable>
struct Func_3_t1216273990;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action
struct Action_t437523947;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"

// System.Void AnonymousEmptyStream::.ctor(System.Func`3<System.Action,Priority,System.IDisposable>)
extern "C"  void AnonymousEmptyStream__ctor_m3388750898 (AnonymousEmptyStream_t62006432 * __this, Func_3_t1216273990 * ___subscribe0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable AnonymousEmptyStream::Listen(System.Action,Priority)
extern "C"  Il2CppObject * AnonymousEmptyStream_Listen_m2371592744 (AnonymousEmptyStream_t62006432 * __this, Action_t437523947 * ___observer0, int32_t ___p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
