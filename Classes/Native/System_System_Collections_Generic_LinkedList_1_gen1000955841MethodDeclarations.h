﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_LinkedList_1_gen2577235966MethodDeclarations.h"

// System.Void System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.IFixedTickable>>::.ctor()
#define LinkedList_1__ctor_m3626646430(__this, method) ((  void (*) (LinkedList_1_t1000955841 *, const MethodInfo*))LinkedList_1__ctor_m2955457271_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.IFixedTickable>>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define LinkedList_1__ctor_m692491231(__this, ___info0, ___context1, method) ((  void (*) (LinkedList_1_t1000955841 *, SerializationInfo_t2995724695 *, StreamingContext_t986364934 , const MethodInfo*))LinkedList_1__ctor_m3369579448_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.IFixedTickable>>::System.Collections.Generic.ICollection<T>.Add(T)
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2038707361(__this, ___value0, method) ((  void (*) (LinkedList_1_t1000955841 *, TaskInfo_t3555793591 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3576108392_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.IFixedTickable>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define LinkedList_1_System_Collections_ICollection_CopyTo_m1561036646(__this, ___array0, ___index1, method) ((  void (*) (LinkedList_1_t1000955841 *, Il2CppArray *, int32_t, const MethodInfo*))LinkedList_1_System_Collections_ICollection_CopyTo_m2331638317_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.IFixedTickable>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m779355688(__this, method) ((  Il2CppObject* (*) (LinkedList_1_t1000955841 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2027502985_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.IFixedTickable>>::System.Collections.IEnumerable.GetEnumerator()
#define LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m3162808801(__this, method) ((  Il2CppObject * (*) (LinkedList_1_t1000955841 *, const MethodInfo*))LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m51916412_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.IFixedTickable>>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4004468165(__this, method) ((  bool (*) (LinkedList_1_t1000955841 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2230847288_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.IFixedTickable>>::System.Collections.ICollection.get_IsSynchronized()
#define LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m1116284440(__this, method) ((  bool (*) (LinkedList_1_t1000955841 *, const MethodInfo*))LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m683991045_gshared)(__this, method)
// System.Object System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.IFixedTickable>>::System.Collections.ICollection.get_SyncRoot()
#define LinkedList_1_System_Collections_ICollection_get_SyncRoot_m1760369142(__this, method) ((  Il2CppObject * (*) (LinkedList_1_t1000955841 *, const MethodInfo*))LinkedList_1_System_Collections_ICollection_get_SyncRoot_m573420165_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.IFixedTickable>>::VerifyReferencedNode(System.Collections.Generic.LinkedListNode`1<T>)
#define LinkedList_1_VerifyReferencedNode_m310016845(__this, ___node0, method) ((  void (*) (LinkedList_1_t1000955841 *, LinkedListNode_1_t3257784211 *, const MethodInfo*))LinkedList_1_VerifyReferencedNode_m3939775124_gshared)(__this, ___node0, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.IFixedTickable>>::AddBefore(System.Collections.Generic.LinkedListNode`1<T>,T)
#define LinkedList_1_AddBefore_m198099353(__this, ___node0, ___value1, method) ((  LinkedListNode_1_t3257784211 * (*) (LinkedList_1_t1000955841 *, LinkedListNode_1_t3257784211 *, TaskInfo_t3555793591 *, const MethodInfo*))LinkedList_1_AddBefore_m1086189582_gshared)(__this, ___node0, ___value1, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.IFixedTickable>>::AddLast(T)
#define LinkedList_1_AddLast_m3122829657(__this, ___value0, method) ((  LinkedListNode_1_t3257784211 * (*) (LinkedList_1_t1000955841 *, TaskInfo_t3555793591 *, const MethodInfo*))LinkedList_1_AddLast_m4070107716_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.IFixedTickable>>::Clear()
#define LinkedList_1_Clear_m1032779721(__this, method) ((  void (*) (LinkedList_1_t1000955841 *, const MethodInfo*))LinkedList_1_Clear_m361590562_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.IFixedTickable>>::Contains(T)
#define LinkedList_1_Contains_m940177359(__this, ___value0, method) ((  bool (*) (LinkedList_1_t1000955841 *, TaskInfo_t3555793591 *, const MethodInfo*))LinkedList_1_Contains_m3484410556_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.IFixedTickable>>::CopyTo(T[],System.Int32)
#define LinkedList_1_CopyTo_m1145051857(__this, ___array0, ___index1, method) ((  void (*) (LinkedList_1_t1000955841 *, TaskInfoU5BU5D_t4242516110*, int32_t, const MethodInfo*))LinkedList_1_CopyTo_m3470139544_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.IFixedTickable>>::Find(T)
#define LinkedList_1_Find_m417231345(__this, ___value0, method) ((  LinkedListNode_1_t3257784211 * (*) (LinkedList_1_t1000955841 *, TaskInfo_t3555793591 *, const MethodInfo*))LinkedList_1_Find_m2643247334_gshared)(__this, ___value0, method)
// System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.IFixedTickable>>::GetEnumerator()
#define LinkedList_1_GetEnumerator_m4273938975(__this, method) ((  Enumerator_t2438535552  (*) (LinkedList_1_t1000955841 *, const MethodInfo*))LinkedList_1_GetEnumerator_m3713737734_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.IFixedTickable>>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define LinkedList_1_GetObjectData_m1994492988(__this, ___info0, ___context1, method) ((  void (*) (LinkedList_1_t1000955841 *, SerializationInfo_t2995724695 *, StreamingContext_t986364934 , const MethodInfo*))LinkedList_1_GetObjectData_m3974480661_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.IFixedTickable>>::OnDeserialization(System.Object)
#define LinkedList_1_OnDeserialization_m2707641256(__this, ___sender0, method) ((  void (*) (LinkedList_1_t1000955841 *, Il2CppObject *, const MethodInfo*))LinkedList_1_OnDeserialization_m3445006959_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.IFixedTickable>>::Remove(T)
#define LinkedList_1_Remove_m1604870026(__this, ___value0, method) ((  bool (*) (LinkedList_1_t1000955841 *, TaskInfo_t3555793591 *, const MethodInfo*))LinkedList_1_Remove_m3283493303_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.IFixedTickable>>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
#define LinkedList_1_Remove_m3280671645(__this, ___node0, method) ((  void (*) (LinkedList_1_t1000955841 *, LinkedListNode_1_t3257784211 *, const MethodInfo*))LinkedList_1_Remove_m4034790180_gshared)(__this, ___node0, method)
// System.Void System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.IFixedTickable>>::RemoveFirst()
#define LinkedList_1_RemoveFirst_m2540786184(__this, method) ((  void (*) (LinkedList_1_t1000955841 *, const MethodInfo*))LinkedList_1_RemoveFirst_m1652892321_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.IFixedTickable>>::RemoveLast()
#define LinkedList_1_RemoveLast_m2047491296(__this, method) ((  void (*) (LinkedList_1_t1000955841 *, const MethodInfo*))LinkedList_1_RemoveLast_m2573038887_gshared)(__this, method)
// System.Int32 System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.IFixedTickable>>::get_Count()
#define LinkedList_1_get_Count_m3467793618(__this, method) ((  int32_t (*) (LinkedList_1_t1000955841 *, const MethodInfo*))LinkedList_1_get_Count_m1368924491_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.IFixedTickable>>::get_First()
#define LinkedList_1_get_First_m3977739029(__this, method) ((  LinkedListNode_1_t3257784211 * (*) (LinkedList_1_t1000955841 *, const MethodInfo*))LinkedList_1_get_First_m3278587786_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<Zenject.IFixedTickable>>::get_Last()
#define LinkedList_1_get_Last_m3617865267(__this, method) ((  LinkedListNode_1_t3257784211 * (*) (LinkedList_1_t1000955841 *, const MethodInfo*))LinkedList_1_get_Last_m270176030_gshared)(__this, method)
