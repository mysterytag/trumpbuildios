﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.Internal.EventTest
struct EventTest_t3226111604;
// PlayFab.ClientModels.LoginResult
struct LoginResult_t2117559510;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginResult2117559510.h"

// System.Void PlayFab.Internal.EventTest::.ctor()
extern "C"  void EventTest__ctor_m2934902341 (EventTest_t3226111604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.Internal.EventTest::.cctor()
extern "C"  void EventTest__cctor_m305563144 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.Internal.EventTest::SetUp()
extern "C"  void EventTest_SetUp_m1469281984 (EventTest_t3226111604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.Internal.EventTest::WaitForApiCalls()
extern "C"  void EventTest_WaitForApiCalls_m90612786 (EventTest_t3226111604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.Internal.EventTest::TestInstCallbacks_GeneralOnly()
extern "C"  void EventTest_TestInstCallbacks_GeneralOnly_m1326841390 (EventTest_t3226111604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.Internal.EventTest::TestStaticCallbacks_GeneralOnly()
extern "C"  void EventTest_TestStaticCallbacks_GeneralOnly_m32804102 (EventTest_t3226111604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.Internal.EventTest::TestInstCallbacks_LocalCallback()
extern "C"  void EventTest_TestInstCallbacks_LocalCallback_m3802189066 (EventTest_t3226111604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.Internal.EventTest::TestStaticCallbacks_LocalCallback()
extern "C"  void EventTest_TestStaticCallbacks_LocalCallback_m1477903842 (EventTest_t3226111604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.Internal.EventTest::OnSuccessLocal(PlayFab.ClientModels.LoginResult)
extern "C"  void EventTest_OnSuccessLocal_m4258138046 (EventTest_t3226111604 * __this, LoginResult_t2117559510 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
