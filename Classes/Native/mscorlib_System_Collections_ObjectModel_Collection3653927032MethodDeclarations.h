﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<PlayFab.Internal.GMFB_327/testRegion>
struct Collection_1_t3653927032;
// System.Collections.Generic.IList`1<PlayFab.Internal.GMFB_327/testRegion>
struct IList_1_t3851431616;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// PlayFab.Internal.GMFB_327/testRegion[]
struct testRegionU5BU5D_t485442179;
// System.Collections.Generic.IEnumerator`1<PlayFab.Internal.GMFB_327/testRegion>
struct IEnumerator_1_t3168045750;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharp_PlayFab_Internal_GMFB_327_testRe1684939302.h"

// System.Void System.Collections.ObjectModel.Collection`1<PlayFab.Internal.GMFB_327/testRegion>::.ctor()
extern "C"  void Collection_1__ctor_m634573579_gshared (Collection_1_t3653927032 * __this, const MethodInfo* method);
#define Collection_1__ctor_m634573579(__this, method) ((  void (*) (Collection_1_t3653927032 *, const MethodInfo*))Collection_1__ctor_m634573579_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<PlayFab.Internal.GMFB_327/testRegion>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1__ctor_m896167658_gshared (Collection_1_t3653927032 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1__ctor_m896167658(__this, ___list0, method) ((  void (*) (Collection_1_t3653927032 *, Il2CppObject*, const MethodInfo*))Collection_1__ctor_m896167658_gshared)(__this, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m201655244_gshared (Collection_1_t3653927032 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m201655244(__this, method) ((  bool (*) (Collection_1_t3653927032 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m201655244_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m3053037721_gshared (Collection_1_t3653927032 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m3053037721(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3653927032 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m3053037721_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m2522504040_gshared (Collection_1_t3653927032 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m2522504040(__this, method) ((  Il2CppObject * (*) (Collection_1_t3653927032 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m2522504040_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m1595433301_gshared (Collection_1_t3653927032 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m1595433301(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3653927032 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m1595433301_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m897467787_gshared (Collection_1_t3653927032 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m897467787(__this, ___value0, method) ((  bool (*) (Collection_1_t3653927032 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m897467787_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m502129965_gshared (Collection_1_t3653927032 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m502129965(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3653927032 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m502129965_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m479895584_gshared (Collection_1_t3653927032 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m479895584(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3653927032 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m479895584_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m1322734856_gshared (Collection_1_t3653927032 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m1322734856(__this, ___value0, method) ((  void (*) (Collection_1_t3653927032 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m1322734856_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m286401009_gshared (Collection_1_t3653927032 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m286401009(__this, method) ((  bool (*) (Collection_1_t3653927032 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m286401009_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m1481999843_gshared (Collection_1_t3653927032 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m1481999843(__this, method) ((  Il2CppObject * (*) (Collection_1_t3653927032 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1481999843_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m2866788666_gshared (Collection_1_t3653927032 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m2866788666(__this, method) ((  bool (*) (Collection_1_t3653927032 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m2866788666_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m546485887_gshared (Collection_1_t3653927032 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m546485887(__this, method) ((  bool (*) (Collection_1_t3653927032 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m546485887_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m3633285802_gshared (Collection_1_t3653927032 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m3633285802(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t3653927032 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m3633285802_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<PlayFab.Internal.GMFB_327/testRegion>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m2232108023_gshared (Collection_1_t3653927032 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m2232108023(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3653927032 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m2232108023_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<PlayFab.Internal.GMFB_327/testRegion>::Add(T)
extern "C"  void Collection_1_Add_m426254356_gshared (Collection_1_t3653927032 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Add_m426254356(__this, ___item0, method) ((  void (*) (Collection_1_t3653927032 *, int32_t, const MethodInfo*))Collection_1_Add_m426254356_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<PlayFab.Internal.GMFB_327/testRegion>::Clear()
extern "C"  void Collection_1_Clear_m2335674166_gshared (Collection_1_t3653927032 * __this, const MethodInfo* method);
#define Collection_1_Clear_m2335674166(__this, method) ((  void (*) (Collection_1_t3653927032 *, const MethodInfo*))Collection_1_Clear_m2335674166_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<PlayFab.Internal.GMFB_327/testRegion>::ClearItems()
extern "C"  void Collection_1_ClearItems_m2545623948_gshared (Collection_1_t3653927032 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m2545623948(__this, method) ((  void (*) (Collection_1_t3653927032 *, const MethodInfo*))Collection_1_ClearItems_m2545623948_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<PlayFab.Internal.GMFB_327/testRegion>::Contains(T)
extern "C"  bool Collection_1_Contains_m402030760_gshared (Collection_1_t3653927032 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Contains_m402030760(__this, ___item0, method) ((  bool (*) (Collection_1_t3653927032 *, int32_t, const MethodInfo*))Collection_1_Contains_m402030760_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<PlayFab.Internal.GMFB_327/testRegion>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m3121982212_gshared (Collection_1_t3653927032 * __this, testRegionU5BU5D_t485442179* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m3121982212(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3653927032 *, testRegionU5BU5D_t485442179*, int32_t, const MethodInfo*))Collection_1_CopyTo_m3121982212_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<PlayFab.Internal.GMFB_327/testRegion>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m829096319_gshared (Collection_1_t3653927032 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m829096319(__this, method) ((  Il2CppObject* (*) (Collection_1_t3653927032 *, const MethodInfo*))Collection_1_GetEnumerator_m829096319_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<PlayFab.Internal.GMFB_327/testRegion>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m4080627408_gshared (Collection_1_t3653927032 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m4080627408(__this, ___item0, method) ((  int32_t (*) (Collection_1_t3653927032 *, int32_t, const MethodInfo*))Collection_1_IndexOf_m4080627408_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<PlayFab.Internal.GMFB_327/testRegion>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m275427963_gshared (Collection_1_t3653927032 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_Insert_m275427963(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3653927032 *, int32_t, int32_t, const MethodInfo*))Collection_1_Insert_m275427963_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<PlayFab.Internal.GMFB_327/testRegion>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m4074286_gshared (Collection_1_t3653927032 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m4074286(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3653927032 *, int32_t, int32_t, const MethodInfo*))Collection_1_InsertItem_m4074286_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<PlayFab.Internal.GMFB_327/testRegion>::Remove(T)
extern "C"  bool Collection_1_Remove_m312691363_gshared (Collection_1_t3653927032 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Remove_m312691363(__this, ___item0, method) ((  bool (*) (Collection_1_t3653927032 *, int32_t, const MethodInfo*))Collection_1_Remove_m312691363_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<PlayFab.Internal.GMFB_327/testRegion>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m2444248129_gshared (Collection_1_t3653927032 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m2444248129(__this, ___index0, method) ((  void (*) (Collection_1_t3653927032 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m2444248129_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<PlayFab.Internal.GMFB_327/testRegion>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m2474966049_gshared (Collection_1_t3653927032 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m2474966049(__this, ___index0, method) ((  void (*) (Collection_1_t3653927032 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m2474966049_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<PlayFab.Internal.GMFB_327/testRegion>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m1762705579_gshared (Collection_1_t3653927032 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m1762705579(__this, method) ((  int32_t (*) (Collection_1_t3653927032 *, const MethodInfo*))Collection_1_get_Count_m1762705579_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<PlayFab.Internal.GMFB_327/testRegion>::get_Item(System.Int32)
extern "C"  int32_t Collection_1_get_Item_m1042541095_gshared (Collection_1_t3653927032 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m1042541095(__this, ___index0, method) ((  int32_t (*) (Collection_1_t3653927032 *, int32_t, const MethodInfo*))Collection_1_get_Item_m1042541095_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<PlayFab.Internal.GMFB_327/testRegion>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m207896722_gshared (Collection_1_t3653927032 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m207896722(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3653927032 *, int32_t, int32_t, const MethodInfo*))Collection_1_set_Item_m207896722_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<PlayFab.Internal.GMFB_327/testRegion>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m1264922023_gshared (Collection_1_t3653927032 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m1264922023(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3653927032 *, int32_t, int32_t, const MethodInfo*))Collection_1_SetItem_m1264922023_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<PlayFab.Internal.GMFB_327/testRegion>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m3878915204_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m3878915204(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m3878915204_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<PlayFab.Internal.GMFB_327/testRegion>::ConvertItem(System.Object)
extern "C"  int32_t Collection_1_ConvertItem_m2877173126_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m2877173126(__this /* static, unused */, ___item0, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m2877173126_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<PlayFab.Internal.GMFB_327/testRegion>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m387106500_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m387106500(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m387106500_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<PlayFab.Internal.GMFB_327/testRegion>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m870548416_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m870548416(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m870548416_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<PlayFab.Internal.GMFB_327/testRegion>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m3583990495_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m3583990495(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m3583990495_gshared)(__this /* static, unused */, ___list0, method)
