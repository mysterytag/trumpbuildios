﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IconsProvider
struct IconsProvider_t3884200715;
// UnityEngine.Sprite
struct Sprite_t4006040370;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void IconsProvider::.ctor()
extern "C"  void IconsProvider__ctor_m1416063680 (IconsProvider_t3884200715 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Sprite IconsProvider::Get(System.String)
extern "C"  Sprite_t4006040370 * IconsProvider_Get_m1034217325 (IconsProvider_t3884200715 * __this, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
