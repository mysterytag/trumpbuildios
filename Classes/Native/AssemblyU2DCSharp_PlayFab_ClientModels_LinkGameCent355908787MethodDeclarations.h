﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.LinkGameCenterAccountRequest
struct LinkGameCenterAccountRequest_t355908787;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.LinkGameCenterAccountRequest::.ctor()
extern "C"  void LinkGameCenterAccountRequest__ctor_m3872169450 (LinkGameCenterAccountRequest_t355908787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.LinkGameCenterAccountRequest::get_GameCenterId()
extern "C"  String_t* LinkGameCenterAccountRequest_get_GameCenterId_m891929154 (LinkGameCenterAccountRequest_t355908787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.LinkGameCenterAccountRequest::set_GameCenterId(System.String)
extern "C"  void LinkGameCenterAccountRequest_set_GameCenterId_m1409317801 (LinkGameCenterAccountRequest_t355908787 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
