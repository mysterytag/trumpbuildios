﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen3886712006MethodDeclarations.h"

// System.Void UniRx.Subject`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>>::.ctor()
#define Subject_1__ctor_m1146164552(__this, method) ((  void (*) (Subject_1_t1411057834 *, const MethodInfo*))Subject_1__ctor_m2394553130_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>>::get_HasObservers()
#define Subject_1_get_HasObservers_m3327832644(__this, method) ((  bool (*) (Subject_1_t1411057834 *, const MethodInfo*))Subject_1_get_HasObservers_m3662622314_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>>::OnCompleted()
#define Subject_1_OnCompleted_m941175506(__this, method) ((  void (*) (Subject_1_t1411057834 *, const MethodInfo*))Subject_1_OnCompleted_m3133318964_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>>::OnError(System.Exception)
#define Subject_1_OnError_m906246399(__this, ___error0, method) ((  void (*) (Subject_1_t1411057834 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m2957519457_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>>::OnNext(T)
#define Subject_1_OnNext_m488242672(__this, ___value0, method) ((  void (*) (Subject_1_t1411057834 *, CollectionAddEvent_1_t3767990510 , const MethodInfo*))Subject_1_OnNext_m1893790546_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>>::Subscribe(UniRx.IObserver`1<T>)
#define Subject_1_Subscribe_m1262494855(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t1411057834 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m3412382719_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>>::Dispose()
#define Subject_1_Dispose_m1850789765(__this, method) ((  void (*) (Subject_1_t1411057834 *, const MethodInfo*))Subject_1_Dispose_m3256337639_gshared)(__this, method)
// System.Void UniRx.Subject`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>>::ThrowIfDisposed()
#define Subject_1_ThrowIfDisposed_m2327651342(__this, method) ((  void (*) (Subject_1_t1411057834 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m4176582512_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>>::IsRequiredSubscribeOnCurrentThread()
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m2449511995(__this, method) ((  bool (*) (Subject_1_t1411057834 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m530093281_gshared)(__this, method)
