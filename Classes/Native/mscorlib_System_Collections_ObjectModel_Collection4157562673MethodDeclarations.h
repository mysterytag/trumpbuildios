﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_Collection2338249549MethodDeclarations.h"

// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::.ctor()
#define Collection_1__ctor_m1316824143(__this, method) ((  void (*) (Collection_1_t4157562673 *, const MethodInfo*))Collection_1__ctor_m2802934255_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::.ctor(System.Collections.Generic.IList`1<T>)
#define Collection_1__ctor_m209737766(__this, ___list0, method) ((  void (*) (Collection_1_t4157562673 *, Il2CppObject*, const MethodInfo*))Collection_1__ctor_m3277856390_gshared)(__this, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1117403340(__this, method) ((  bool (*) (Collection_1_t4157562673 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1891404776_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Collection_1_System_Collections_ICollection_CopyTo_m1541053397(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t4157562673 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m4101468725_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::System.Collections.IEnumerable.GetEnumerator()
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m4213613328(__this, method) ((  Il2CppObject * (*) (Collection_1_t4157562673 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m1037225092_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::System.Collections.IList.Add(System.Object)
#define Collection_1_System_Collections_IList_Add_m3146002657(__this, ___value0, method) ((  int32_t (*) (Collection_1_t4157562673 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m3051129145_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::System.Collections.IList.Contains(System.Object)
#define Collection_1_System_Collections_IList_Contains_m371309195(__this, ___value0, method) ((  bool (*) (Collection_1_t4157562673 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m132120743_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::System.Collections.IList.IndexOf(System.Object)
#define Collection_1_System_Collections_IList_IndexOf_m3113160377(__this, ___value0, method) ((  int32_t (*) (Collection_1_t4157562673 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m2765323025_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::System.Collections.IList.Insert(System.Int32,System.Object)
#define Collection_1_System_Collections_IList_Insert_m973444964(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t4157562673 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m1905542404_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::System.Collections.IList.Remove(System.Object)
#define Collection_1_System_Collections_IList_Remove_m3088572228(__this, ___value0, method) ((  void (*) (Collection_1_t4157562673 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m908037540_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::System.Collections.ICollection.get_IsSynchronized()
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m2126417137(__this, method) ((  bool (*) (Collection_1_t4157562673 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m1813987669_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::System.Collections.ICollection.get_SyncRoot()
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m3637836829(__this, method) ((  Il2CppObject * (*) (Collection_1_t4157562673 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m708599303_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::System.Collections.IList.get_IsFixedSize()
#define Collection_1_System_Collections_IList_get_IsFixedSize_m3565088314(__this, method) ((  bool (*) (Collection_1_t4157562673 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m1803745622_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::System.Collections.IList.get_IsReadOnly()
#define Collection_1_System_Collections_IList_get_IsReadOnly_m1538843007(__this, method) ((  bool (*) (Collection_1_t4157562673 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m3837330147_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::System.Collections.IList.get_Item(System.Int32)
#define Collection_1_System_Collections_IList_get_Item_m923425380(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t4157562673 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m3424709070_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define Collection_1_System_Collections_IList_set_Item_m4086659643(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t4157562673 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m2184134619_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::Add(T)
#define Collection_1_Add_m1833735760(__this, ___item0, method) ((  void (*) (Collection_1_t4157562673 *, Tuple_2_t2188574943 , const MethodInfo*))Collection_1_Add_m2020222128_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::Clear()
#define Collection_1_Clear_m3017924730(__this, method) ((  void (*) (Collection_1_t4157562673 *, const MethodInfo*))Collection_1_Clear_m209067546_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::ClearItems()
#define Collection_1_ClearItems_m4125424840(__this, method) ((  void (*) (Collection_1_t4157562673 *, const MethodInfo*))Collection_1_ClearItems_m41304872_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::Contains(T)
#define Collection_1_Contains_m2192173992(__this, ___item0, method) ((  bool (*) (Collection_1_t4157562673 *, Tuple_2_t2188574943 , const MethodInfo*))Collection_1_Contains_m1031918604_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::CopyTo(T[],System.Int32)
#define Collection_1_CopyTo_m435653184(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t4157562673 *, Tuple_2U5BU5D_t2629098694*, int32_t, const MethodInfo*))Collection_1_CopyTo_m435306656_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::GetEnumerator()
#define Collection_1_GetEnumerator_m209261835(__this, method) ((  Il2CppObject* (*) (Collection_1_t4157562673 *, const MethodInfo*))Collection_1_GetEnumerator_m3801016163_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::IndexOf(T)
#define Collection_1_IndexOf_m2464697796(__this, ___item0, method) ((  int32_t (*) (Collection_1_t4157562673 *, Tuple_2_t2188574943 , const MethodInfo*))Collection_1_IndexOf_m4216597100_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::Insert(System.Int32,T)
#define Collection_1_Insert_m1349726391(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t4157562673 *, int32_t, Tuple_2_t2188574943 , const MethodInfo*))Collection_1_Insert_m4201119511_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::InsertItem(System.Int32,T)
#define Collection_1_InsertItem_m4012190570(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t4157562673 *, int32_t, Tuple_2_t2188574943 , const MethodInfo*))Collection_1_InsertItem_m3679177162_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::Remove(T)
#define Collection_1_Remove_m3644159395(__this, ___item0, method) ((  bool (*) (Collection_1_t4157562673 *, Tuple_2_t2188574943 , const MethodInfo*))Collection_1_Remove_m2297702151_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::RemoveAt(System.Int32)
#define Collection_1_RemoveAt_m3518546557(__this, ___index0, method) ((  void (*) (Collection_1_t4157562673 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m2074972381_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::RemoveItem(System.Int32)
#define Collection_1_RemoveItem_m4083604317(__this, ___index0, method) ((  void (*) (Collection_1_t4157562673 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m4083257789_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::get_Count()
#define Collection_1_get_Count_m3208495159(__this, method) ((  int32_t (*) (Collection_1_t4157562673 *, const MethodInfo*))Collection_1_get_Count_m1682798735_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::get_Item(System.Int32)
#define Collection_1_get_Item_m2520361345(__this, ___index0, method) ((  Tuple_2_t2188574943  (*) (Collection_1_t4157562673 *, int32_t, const MethodInfo*))Collection_1_get_Item_m2049497603_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::set_Item(System.Int32,T)
#define Collection_1_set_Item_m1816534990(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t4157562673 *, int32_t, Tuple_2_t2188574943 , const MethodInfo*))Collection_1_set_Item_m1816188462_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::SetItem(System.Int32,T)
#define Collection_1_SetItem_m208434923(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t4157562673 *, int32_t, Tuple_2_t2188574943 , const MethodInfo*))Collection_1_SetItem_m2702275723_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::IsValidItem(System.Object)
#define Collection_1_IsValidItem_m1474085252(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m36865184_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::ConvertItem(System.Object)
#define Collection_1_ConvertItem_m2039525344(__this /* static, unused */, ___item0, method) ((  Tuple_2_t2188574943  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m2039467874_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::CheckWritable(System.Collections.Generic.IList`1<T>)
#define Collection_1_CheckWritable_m305771520(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m27073120_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::IsSynchronized(System.Collections.Generic.IList`1<T>)
#define Collection_1_IsSynchronized_m3894488768(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m2513444_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UniRx.Tuple`2<System.String,UnityEngine.Sprite>>::IsFixedSize(System.Collections.Generic.IList`1<T>)
#define Collection_1_IsFixedSize_m456757215(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m2524888827_gshared)(__this /* static, unused */, ___list0, method)
