﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Tests/<TestReactive>c__AnonStorey147
struct U3CTestReactiveU3Ec__AnonStorey147_t1969170084;
// ICell`1<System.Int32>
struct ICell_1_t104078468;

#include "codegen/il2cpp-codegen.h"

// System.Void Tests/<TestReactive>c__AnonStorey147::.ctor()
extern "C"  void U3CTestReactiveU3Ec__AnonStorey147__ctor_m2504130153 (U3CTestReactiveU3Ec__AnonStorey147_t1969170084 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ICell`1<System.Int32> Tests/<TestReactive>c__AnonStorey147::<>m__1E4(System.Int32)
extern "C"  Il2CppObject* U3CTestReactiveU3Ec__AnonStorey147_U3CU3Em__1E4_m1218886149 (U3CTestReactiveU3Ec__AnonStorey147_t1969170084 * __this, int32_t ___v10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
