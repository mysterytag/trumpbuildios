﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ZergRush_ImmutableList_1_gen340490602MethodDeclarations.h"

// System.Void ZergRush.ImmutableList`1<System.Action`1<UniRx.CollectionAddEvent`1<SettingsButton>>>::.ctor()
#define ImmutableList_1__ctor_m3149056391(__this, method) ((  void (*) (ImmutableList_1_t2146509115 *, const MethodInfo*))ImmutableList_1__ctor_m99192906_gshared)(__this, method)
// System.Void ZergRush.ImmutableList`1<System.Action`1<UniRx.CollectionAddEvent`1<SettingsButton>>>::.ctor(T)
#define ImmutableList_1__ctor_m3131468983(__this, ___single0, method) ((  void (*) (ImmutableList_1_t2146509115 *, Action_1_t2643124933 *, const MethodInfo*))ImmutableList_1__ctor_m3074981460_gshared)(__this, ___single0, method)
// System.Void ZergRush.ImmutableList`1<System.Action`1<UniRx.CollectionAddEvent`1<SettingsButton>>>::.ctor(T[])
#define ImmutableList_1__ctor_m2864636437(__this, ___data0, method) ((  void (*) (ImmutableList_1_t2146509115 *, Action_1U5BU5D_t2847058056*, const MethodInfo*))ImmutableList_1__ctor_m119734386_gshared)(__this, ___data0, method)
// System.Collections.IEnumerator ZergRush.ImmutableList`1<System.Action`1<UniRx.CollectionAddEvent`1<SettingsButton>>>::System.Collections.IEnumerable.GetEnumerator()
#define ImmutableList_1_System_Collections_IEnumerable_GetEnumerator_m1968238074(__this, method) ((  Il2CppObject * (*) (ImmutableList_1_t2146509115 *, const MethodInfo*))ImmutableList_1_System_Collections_IEnumerable_GetEnumerator_m2591974359_gshared)(__this, method)
// ZergRush.ImmutableList`1<T> ZergRush.ImmutableList`1<System.Action`1<UniRx.CollectionAddEvent`1<SettingsButton>>>::Add(T)
#define ImmutableList_1_Add_m3136629808(__this, ___value0, method) ((  ImmutableList_1_t2146509115 * (*) (ImmutableList_1_t2146509115 *, Action_1_t2643124933 *, const MethodInfo*))ImmutableList_1_Add_m3924978445_gshared)(__this, ___value0, method)
// System.Collections.Generic.IEnumerator`1<T> ZergRush.ImmutableList`1<System.Action`1<UniRx.CollectionAddEvent`1<SettingsButton>>>::GetEnumerator()
#define ImmutableList_1_GetEnumerator_m2308878843(__this, method) ((  Il2CppObject* (*) (ImmutableList_1_t2146509115 *, const MethodInfo*))ImmutableList_1_GetEnumerator_m364779198_gshared)(__this, method)
// ZergRush.ImmutableList`1<T> ZergRush.ImmutableList`1<System.Action`1<UniRx.CollectionAddEvent`1<SettingsButton>>>::Remove(T)
#define ImmutableList_1_Remove_m279503109(__this, ___value0, method) ((  ImmutableList_1_t2146509115 * (*) (ImmutableList_1_t2146509115 *, Action_1_t2643124933 *, const MethodInfo*))ImmutableList_1_Remove_m1092573448_gshared)(__this, ___value0, method)
// ZergRush.ImmutableList`1<T> ZergRush.ImmutableList`1<System.Action`1<UniRx.CollectionAddEvent`1<SettingsButton>>>::Replace(System.Int32,T)
#define ImmutableList_1_Replace_m2289649450(__this, ___index0, ___value1, method) ((  ImmutableList_1_t2146509115 * (*) (ImmutableList_1_t2146509115 *, int32_t, Action_1_t2643124933 *, const MethodInfo*))ImmutableList_1_Replace_m2580545005_gshared)(__this, ___index0, ___value1, method)
// System.Int32 ZergRush.ImmutableList`1<System.Action`1<UniRx.CollectionAddEvent`1<SettingsButton>>>::IndexOf(T)
#define ImmutableList_1_IndexOf_m4225160482(__this, ___value0, method) ((  int32_t (*) (ImmutableList_1_t2146509115 *, Action_1_t2643124933 *, const MethodInfo*))ImmutableList_1_IndexOf_m1554103039_gshared)(__this, ___value0, method)
// System.Int32 ZergRush.ImmutableList`1<System.Action`1<UniRx.CollectionAddEvent`1<SettingsButton>>>::get_Count()
#define ImmutableList_1_get_Count_m1948263577(__this, method) ((  int32_t (*) (ImmutableList_1_t2146509115 *, const MethodInfo*))ImmutableList_1_get_Count_m749861468_gshared)(__this, method)
