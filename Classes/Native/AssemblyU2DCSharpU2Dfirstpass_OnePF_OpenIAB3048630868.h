﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// OnePF.IOpenIAB
struct IOpenIAB_t1424766443;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnePF.OpenIAB
struct  OpenIAB_t3048630868  : public Il2CppObject
{
public:

public:
};

struct OpenIAB_t3048630868_StaticFields
{
public:
	// OnePF.IOpenIAB OnePF.OpenIAB::_billing
	Il2CppObject * ____billing_0;

public:
	inline static int32_t get_offset_of__billing_0() { return static_cast<int32_t>(offsetof(OpenIAB_t3048630868_StaticFields, ____billing_0)); }
	inline Il2CppObject * get__billing_0() const { return ____billing_0; }
	inline Il2CppObject ** get_address_of__billing_0() { return &____billing_0; }
	inline void set__billing_0(Il2CppObject * value)
	{
		____billing_0 = value;
		Il2CppCodeGenWriteBarrier(&____billing_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
