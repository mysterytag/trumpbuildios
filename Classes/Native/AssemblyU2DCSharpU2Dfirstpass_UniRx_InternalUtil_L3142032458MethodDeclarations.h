﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.ListObserver`1<System.Int32>
struct ListObserver_1_t3142032458;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<System.Int32>>
struct ImmutableList_1_t1824650893;
// System.Exception
struct Exception_t1967233988;
// UniRx.IObserver`1<System.Int32>
struct IObserver_1_t764446394;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.InternalUtil.ListObserver`1<System.Int32>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m943900794_gshared (ListObserver_1_t3142032458 * __this, ImmutableList_1_t1824650893 * ___observers0, const MethodInfo* method);
#define ListObserver_1__ctor_m943900794(__this, ___observers0, method) ((  void (*) (ListObserver_1_t3142032458 *, ImmutableList_1_t1824650893 *, const MethodInfo*))ListObserver_1__ctor_m943900794_gshared)(__this, ___observers0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<System.Int32>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m1250157874_gshared (ListObserver_1_t3142032458 * __this, const MethodInfo* method);
#define ListObserver_1_OnCompleted_m1250157874(__this, method) ((  void (*) (ListObserver_1_t3142032458 *, const MethodInfo*))ListObserver_1_OnCompleted_m1250157874_gshared)(__this, method)
// System.Void UniRx.InternalUtil.ListObserver`1<System.Int32>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m2232834911_gshared (ListObserver_1_t3142032458 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ListObserver_1_OnError_m2232834911(__this, ___error0, method) ((  void (*) (ListObserver_1_t3142032458 *, Exception_t1967233988 *, const MethodInfo*))ListObserver_1_OnError_m2232834911_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<System.Int32>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m981262416_gshared (ListObserver_1_t3142032458 * __this, int32_t ___value0, const MethodInfo* method);
#define ListObserver_1_OnNext_m981262416(__this, ___value0, method) ((  void (*) (ListObserver_1_t3142032458 *, int32_t, const MethodInfo*))ListObserver_1_OnNext_m981262416_gshared)(__this, ___value0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<System.Int32>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m1209299248_gshared (ListObserver_1_t3142032458 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Add_m1209299248(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t3142032458 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Add_m1209299248_gshared)(__this, ___observer0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<System.Int32>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m481664027_gshared (ListObserver_1_t3142032458 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Remove_m481664027(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t3142032458 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Remove_m481664027_gshared)(__this, ___observer0, method)
