﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.CatalogItemBundleInfo
struct CatalogItemBundleInfo_t1005829164;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;
// System.Collections.Generic.Dictionary`2<System.String,System.UInt32>
struct Dictionary_2_t2623623230;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.CatalogItemBundleInfo::.ctor()
extern "C"  void CatalogItemBundleInfo__ctor_m1252803709 (CatalogItemBundleInfo_t1005829164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.CatalogItemBundleInfo::get_BundledItems()
extern "C"  List_1_t1765447871 * CatalogItemBundleInfo_get_BundledItems_m308254623 (CatalogItemBundleInfo_t1005829164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CatalogItemBundleInfo::set_BundledItems(System.Collections.Generic.List`1<System.String>)
extern "C"  void CatalogItemBundleInfo_set_BundledItems_m1385412374 (CatalogItemBundleInfo_t1005829164 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.CatalogItemBundleInfo::get_BundledResultTables()
extern "C"  List_1_t1765447871 * CatalogItemBundleInfo_get_BundledResultTables_m919145669 (CatalogItemBundleInfo_t1005829164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CatalogItemBundleInfo::set_BundledResultTables(System.Collections.Generic.List`1<System.String>)
extern "C"  void CatalogItemBundleInfo_set_BundledResultTables_m3766801494 (CatalogItemBundleInfo_t1005829164 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.UInt32> PlayFab.ClientModels.CatalogItemBundleInfo::get_BundledVirtualCurrencies()
extern "C"  Dictionary_2_t2623623230 * CatalogItemBundleInfo_get_BundledVirtualCurrencies_m2146316542 (CatalogItemBundleInfo_t1005829164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CatalogItemBundleInfo::set_BundledVirtualCurrencies(System.Collections.Generic.Dictionary`2<System.String,System.UInt32>)
extern "C"  void CatalogItemBundleInfo_set_BundledVirtualCurrencies_m2271108917 (CatalogItemBundleInfo_t1005829164 * __this, Dictionary_2_t2623623230 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
