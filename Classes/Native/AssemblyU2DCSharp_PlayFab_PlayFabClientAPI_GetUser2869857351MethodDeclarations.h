﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetUserReadOnlyDataRequestCallback
struct GetUserReadOnlyDataRequestCallback_t2869857351;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetUserDataRequest
struct GetUserDataRequest_t2084642996;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetUserData2084642996.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetUserReadOnlyDataRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetUserReadOnlyDataRequestCallback__ctor_m2333125046 (GetUserReadOnlyDataRequestCallback_t2869857351 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetUserReadOnlyDataRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetUserDataRequest,System.Object)
extern "C"  void GetUserReadOnlyDataRequestCallback_Invoke_m142763389 (GetUserReadOnlyDataRequestCallback_t2869857351 * __this, String_t* ___urlPath0, int32_t ___callId1, GetUserDataRequest_t2084642996 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetUserReadOnlyDataRequestCallback_t2869857351(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetUserDataRequest_t2084642996 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetUserReadOnlyDataRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetUserDataRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetUserReadOnlyDataRequestCallback_BeginInvoke_m1179519286 (GetUserReadOnlyDataRequestCallback_t2869857351 * __this, String_t* ___urlPath0, int32_t ___callId1, GetUserDataRequest_t2084642996 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetUserReadOnlyDataRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetUserReadOnlyDataRequestCallback_EndInvoke_m360277702 (GetUserReadOnlyDataRequestCallback_t2869857351 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
