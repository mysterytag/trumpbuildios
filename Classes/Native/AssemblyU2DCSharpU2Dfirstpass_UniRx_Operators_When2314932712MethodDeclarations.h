﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.WhenAllObservable`1<UniRx.Tuple`2<System.Object,System.Object>>
struct WhenAllObservable_1_t2314932712;
// UniRx.IObservable`1<UniRx.Tuple`2<System.Object,System.Object>>[]
struct IObservable_1U5BU5D_t1796868782;
// System.Collections.Generic.IEnumerable`1<UniRx.IObservable`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct IEnumerable_1_t3000214539;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Object>[]>
struct IObserver_1_t1814965617;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.WhenAllObservable`1<UniRx.Tuple`2<System.Object,System.Object>>::.ctor(UniRx.IObservable`1<T>[])
extern "C"  void WhenAllObservable_1__ctor_m358968886_gshared (WhenAllObservable_1_t2314932712 * __this, IObservable_1U5BU5D_t1796868782* ___sources0, const MethodInfo* method);
#define WhenAllObservable_1__ctor_m358968886(__this, ___sources0, method) ((  void (*) (WhenAllObservable_1_t2314932712 *, IObservable_1U5BU5D_t1796868782*, const MethodInfo*))WhenAllObservable_1__ctor_m358968886_gshared)(__this, ___sources0, method)
// System.Void UniRx.Operators.WhenAllObservable`1<UniRx.Tuple`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.IEnumerable`1<UniRx.IObservable`1<T>>)
extern "C"  void WhenAllObservable_1__ctor_m2432143589_gshared (WhenAllObservable_1_t2314932712 * __this, Il2CppObject* ___sources0, const MethodInfo* method);
#define WhenAllObservable_1__ctor_m2432143589(__this, ___sources0, method) ((  void (*) (WhenAllObservable_1_t2314932712 *, Il2CppObject*, const MethodInfo*))WhenAllObservable_1__ctor_m2432143589_gshared)(__this, ___sources0, method)
// System.IDisposable UniRx.Operators.WhenAllObservable`1<UniRx.Tuple`2<System.Object,System.Object>>::SubscribeCore(UniRx.IObserver`1<T[]>,System.IDisposable)
extern "C"  Il2CppObject * WhenAllObservable_1_SubscribeCore_m734246029_gshared (WhenAllObservable_1_t2314932712 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define WhenAllObservable_1_SubscribeCore_m734246029(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (WhenAllObservable_1_t2314932712 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))WhenAllObservable_1_SubscribeCore_m734246029_gshared)(__this, ___observer0, ___cancel1, method)
