﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.EmptyObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>
struct EmptyObserver_1_t2731641281;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_DictionaryRemo3322288291.h"

// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::.ctor()
extern "C"  void EmptyObserver_1__ctor_m3768066272_gshared (EmptyObserver_1_t2731641281 * __this, const MethodInfo* method);
#define EmptyObserver_1__ctor_m3768066272(__this, method) ((  void (*) (EmptyObserver_1_t2731641281 *, const MethodInfo*))EmptyObserver_1__ctor_m3768066272_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::.cctor()
extern "C"  void EmptyObserver_1__cctor_m363841229_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EmptyObserver_1__cctor_m363841229(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EmptyObserver_1__cctor_m363841229_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::OnCompleted()
extern "C"  void EmptyObserver_1_OnCompleted_m2422156394_gshared (EmptyObserver_1_t2731641281 * __this, const MethodInfo* method);
#define EmptyObserver_1_OnCompleted_m2422156394(__this, method) ((  void (*) (EmptyObserver_1_t2731641281 *, const MethodInfo*))EmptyObserver_1_OnCompleted_m2422156394_gshared)(__this, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::OnError(System.Exception)
extern "C"  void EmptyObserver_1_OnError_m1575521943_gshared (EmptyObserver_1_t2731641281 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define EmptyObserver_1_OnError_m1575521943(__this, ___error0, method) ((  void (*) (EmptyObserver_1_t2731641281 *, Exception_t1967233988 *, const MethodInfo*))EmptyObserver_1_OnError_m1575521943_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.EmptyObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::OnNext(T)
extern "C"  void EmptyObserver_1_OnNext_m3284960136_gshared (EmptyObserver_1_t2731641281 * __this, DictionaryRemoveEvent_2_t3322288291  ___value0, const MethodInfo* method);
#define EmptyObserver_1_OnNext_m3284960136(__this, ___value0, method) ((  void (*) (EmptyObserver_1_t2731641281 *, DictionaryRemoveEvent_2_t3322288291 , const MethodInfo*))EmptyObserver_1_OnNext_m3284960136_gshared)(__this, ___value0, method)
