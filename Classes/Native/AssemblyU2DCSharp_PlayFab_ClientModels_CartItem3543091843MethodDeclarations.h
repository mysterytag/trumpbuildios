﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.CartItem
struct CartItem_t3543091843;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.UInt32>
struct Dictionary_2_t2623623230;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.CartItem::.ctor()
extern "C"  void CartItem__ctor_m1216156442 (CartItem_t3543091843 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.CartItem::get_ItemId()
extern "C"  String_t* CartItem_get_ItemId_m1977638142 (CartItem_t3543091843 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CartItem::set_ItemId(System.String)
extern "C"  void CartItem_set_ItemId_m2360047021 (CartItem_t3543091843 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.CartItem::get_ItemClass()
extern "C"  String_t* CartItem_get_ItemClass_m1005424535 (CartItem_t3543091843 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CartItem::set_ItemClass(System.String)
extern "C"  void CartItem_set_ItemClass_m2441077058 (CartItem_t3543091843 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.CartItem::get_ItemInstanceId()
extern "C"  String_t* CartItem_get_ItemInstanceId_m3792392499 (CartItem_t3543091843 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CartItem::set_ItemInstanceId(System.String)
extern "C"  void CartItem_set_ItemInstanceId_m672843416 (CartItem_t3543091843 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.CartItem::get_DisplayName()
extern "C"  String_t* CartItem_get_DisplayName_m4174551455 (CartItem_t3543091843 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CartItem::set_DisplayName(System.String)
extern "C"  void CartItem_set_DisplayName_m825893498 (CartItem_t3543091843 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.CartItem::get_Description()
extern "C"  String_t* CartItem_get_Description_m2418483054 (CartItem_t3543091843 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CartItem::set_Description(System.String)
extern "C"  void CartItem_set_Description_m4269524235 (CartItem_t3543091843 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.UInt32> PlayFab.ClientModels.CartItem::get_VirtualCurrencyPrices()
extern "C"  Dictionary_2_t2623623230 * CartItem_get_VirtualCurrencyPrices_m2091401835 (CartItem_t3543091843 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CartItem::set_VirtualCurrencyPrices(System.Collections.Generic.Dictionary`2<System.String,System.UInt32>)
extern "C"  void CartItem_set_VirtualCurrencyPrices_m2407124922 (CartItem_t3543091843 * __this, Dictionary_2_t2623623230 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.UInt32> PlayFab.ClientModels.CartItem::get_RealCurrencyPrices()
extern "C"  Dictionary_2_t2623623230 * CartItem_get_RealCurrencyPrices_m1851834102 (CartItem_t3543091843 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CartItem::set_RealCurrencyPrices(System.Collections.Generic.Dictionary`2<System.String,System.UInt32>)
extern "C"  void CartItem_set_RealCurrencyPrices_m788490585 (CartItem_t3543091843 * __this, Dictionary_2_t2623623230 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.UInt32> PlayFab.ClientModels.CartItem::get_VCAmount()
extern "C"  Dictionary_2_t2623623230 * CartItem_get_VCAmount_m3461916482 (CartItem_t3543091843 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CartItem::set_VCAmount(System.Collections.Generic.Dictionary`2<System.String,System.UInt32>)
extern "C"  void CartItem_set_VCAmount_m2576381989 (CartItem_t3543091843 * __this, Dictionary_2_t2623623230 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
