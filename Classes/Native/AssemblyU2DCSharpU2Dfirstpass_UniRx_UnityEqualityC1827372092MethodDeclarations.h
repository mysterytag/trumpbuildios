﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.UnityEqualityComparer/BoundsEqualityComparer
struct BoundsEqualityComparer_t1827372092;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Bounds3518514978.h"

// System.Void UniRx.UnityEqualityComparer/BoundsEqualityComparer::.ctor()
extern "C"  void BoundsEqualityComparer__ctor_m3587935112 (BoundsEqualityComparer_t1827372092 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.UnityEqualityComparer/BoundsEqualityComparer::Equals(UnityEngine.Bounds,UnityEngine.Bounds)
extern "C"  bool BoundsEqualityComparer_Equals_m2064148303 (BoundsEqualityComparer_t1827372092 * __this, Bounds_t3518514978  ___self0, Bounds_t3518514978  ___vector1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UniRx.UnityEqualityComparer/BoundsEqualityComparer::GetHashCode(UnityEngine.Bounds)
extern "C"  int32_t BoundsEqualityComparer_GetHashCode_m3567165397 (BoundsEqualityComparer_t1827372092 * __this, Bounds_t3518514978  ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
