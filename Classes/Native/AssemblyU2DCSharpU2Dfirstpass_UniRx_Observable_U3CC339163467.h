﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.Object>
struct Action_1_t985559125;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/<CatchIgnore>c__AnonStorey3C`2<System.Object,System.Object>
struct  U3CCatchIgnoreU3Ec__AnonStorey3C_2_t339163467  : public Il2CppObject
{
public:
	// System.Action`1<TException> UniRx.Observable/<CatchIgnore>c__AnonStorey3C`2::errorAction
	Action_1_t985559125 * ___errorAction_0;

public:
	inline static int32_t get_offset_of_errorAction_0() { return static_cast<int32_t>(offsetof(U3CCatchIgnoreU3Ec__AnonStorey3C_2_t339163467, ___errorAction_0)); }
	inline Action_1_t985559125 * get_errorAction_0() const { return ___errorAction_0; }
	inline Action_1_t985559125 ** get_address_of_errorAction_0() { return &___errorAction_0; }
	inline void set_errorAction_0(Action_1_t985559125 * value)
	{
		___errorAction_0 = value;
		Il2CppCodeGenWriteBarrier(&___errorAction_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
