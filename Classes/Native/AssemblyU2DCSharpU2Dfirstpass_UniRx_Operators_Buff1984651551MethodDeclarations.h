﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.BufferObservable`2/Buffer<System.Int64,System.Int64>
struct Buffer_t1984651551;
// UniRx.Operators.BufferObservable`2<System.Int64,System.Int64>
struct BufferObservable_2_t550437669;
// UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Int64>>
struct IObserver_1_t2930938803;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.BufferObservable`2/Buffer<System.Int64,System.Int64>::.ctor(UniRx.Operators.BufferObservable`2<TSource,TWindowBoundary>,UniRx.IObserver`1<System.Collections.Generic.IList`1<TSource>>,System.IDisposable)
extern "C"  void Buffer__ctor_m2649861863_gshared (Buffer_t1984651551 * __this, BufferObservable_2_t550437669 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Buffer__ctor_m2649861863(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Buffer_t1984651551 *, BufferObservable_2_t550437669 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Buffer__ctor_m2649861863_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.BufferObservable`2/Buffer<System.Int64,System.Int64>::.cctor()
extern "C"  void Buffer__cctor_m1290824073_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Buffer__cctor_m1290824073(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Buffer__cctor_m1290824073_gshared)(__this /* static, unused */, method)
// System.IDisposable UniRx.Operators.BufferObservable`2/Buffer<System.Int64,System.Int64>::Run()
extern "C"  Il2CppObject * Buffer_Run_m2773953970_gshared (Buffer_t1984651551 * __this, const MethodInfo* method);
#define Buffer_Run_m2773953970(__this, method) ((  Il2CppObject * (*) (Buffer_t1984651551 *, const MethodInfo*))Buffer_Run_m2773953970_gshared)(__this, method)
// System.Void UniRx.Operators.BufferObservable`2/Buffer<System.Int64,System.Int64>::OnNext(TSource)
extern "C"  void Buffer_OnNext_m4291134769_gshared (Buffer_t1984651551 * __this, int64_t ___value0, const MethodInfo* method);
#define Buffer_OnNext_m4291134769(__this, ___value0, method) ((  void (*) (Buffer_t1984651551 *, int64_t, const MethodInfo*))Buffer_OnNext_m4291134769_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.BufferObservable`2/Buffer<System.Int64,System.Int64>::OnError(System.Exception)
extern "C"  void Buffer_OnError_m987904859_gshared (Buffer_t1984651551 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Buffer_OnError_m987904859(__this, ___error0, method) ((  void (*) (Buffer_t1984651551 *, Exception_t1967233988 *, const MethodInfo*))Buffer_OnError_m987904859_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.BufferObservable`2/Buffer<System.Int64,System.Int64>::OnCompleted()
extern "C"  void Buffer_OnCompleted_m2466438958_gshared (Buffer_t1984651551 * __this, const MethodInfo* method);
#define Buffer_OnCompleted_m2466438958(__this, method) ((  void (*) (Buffer_t1984651551 *, const MethodInfo*))Buffer_OnCompleted_m2466438958_gshared)(__this, method)
