﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ReactiveUI/<SetEnabled>c__AnonStorey125
struct U3CSetEnabledU3Ec__AnonStorey125_t340483856;

#include "codegen/il2cpp-codegen.h"

// System.Void ReactiveUI/<SetEnabled>c__AnonStorey125::.ctor()
extern "C"  void U3CSetEnabledU3Ec__AnonStorey125__ctor_m2298138937 (U3CSetEnabledU3Ec__AnonStorey125_t340483856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReactiveUI/<SetEnabled>c__AnonStorey125::<>m__1B5(System.Boolean)
extern "C"  void U3CSetEnabledU3Ec__AnonStorey125_U3CU3Em__1B5_m1422513659 (U3CSetEnabledU3Ec__AnonStorey125_t340483856 * __this, bool ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
