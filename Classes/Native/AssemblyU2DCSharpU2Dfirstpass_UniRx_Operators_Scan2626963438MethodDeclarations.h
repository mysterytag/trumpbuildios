﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ScanObservable`2/Scan<System.Object,System.Object>
struct Scan_t2626963438;
// UniRx.Operators.ScanObservable`2<System.Object,System.Object>
struct ScanObservable_2_t1879023476;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.ScanObservable`2/Scan<System.Object,System.Object>::.ctor(UniRx.Operators.ScanObservable`2<TSource,TAccumulate>,UniRx.IObserver`1<TAccumulate>,System.IDisposable)
extern "C"  void Scan__ctor_m57280148_gshared (Scan_t2626963438 * __this, ScanObservable_2_t1879023476 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Scan__ctor_m57280148(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Scan_t2626963438 *, ScanObservable_2_t1879023476 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Scan__ctor_m57280148_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.ScanObservable`2/Scan<System.Object,System.Object>::OnNext(TSource)
extern "C"  void Scan_OnNext_m658562997_gshared (Scan_t2626963438 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Scan_OnNext_m658562997(__this, ___value0, method) ((  void (*) (Scan_t2626963438 *, Il2CppObject *, const MethodInfo*))Scan_OnNext_m658562997_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.ScanObservable`2/Scan<System.Object,System.Object>::OnError(System.Exception)
extern "C"  void Scan_OnError_m2975940831_gshared (Scan_t2626963438 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Scan_OnError_m2975940831(__this, ___error0, method) ((  void (*) (Scan_t2626963438 *, Exception_t1967233988 *, const MethodInfo*))Scan_OnError_m2975940831_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.ScanObservable`2/Scan<System.Object,System.Object>::OnCompleted()
extern "C"  void Scan_OnCompleted_m2413497010_gshared (Scan_t2626963438 * __this, const MethodInfo* method);
#define Scan_OnCompleted_m2413497010(__this, method) ((  void (*) (Scan_t2626963438 *, const MethodInfo*))Scan_OnCompleted_m2413497010_gshared)(__this, method)
