﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<UnlinkCustomID>c__AnonStorey81
struct U3CUnlinkCustomIDU3Ec__AnonStorey81_t822976413;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<UnlinkCustomID>c__AnonStorey81::.ctor()
extern "C"  void U3CUnlinkCustomIDU3Ec__AnonStorey81__ctor_m1433204918 (U3CUnlinkCustomIDU3Ec__AnonStorey81_t822976413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<UnlinkCustomID>c__AnonStorey81::<>m__6E(PlayFab.CallRequestContainer)
extern "C"  void U3CUnlinkCustomIDU3Ec__AnonStorey81_U3CU3Em__6E_m3792689059 (U3CUnlinkCustomIDU3Ec__AnonStorey81_t822976413 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
