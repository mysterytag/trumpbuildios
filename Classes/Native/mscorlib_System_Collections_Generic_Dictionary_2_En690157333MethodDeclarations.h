﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1094945144MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,PlayFab.CallRequestContainer>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1400439833(__this, ___dictionary0, method) ((  void (*) (Enumerator_t690157333 *, Dictionary_2_t923129392 *, const MethodInfo*))Enumerator__ctor_m2377115088_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,PlayFab.CallRequestContainer>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m86448232(__this, method) ((  Il2CppObject * (*) (Enumerator_t690157333 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1037642267_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,PlayFab.CallRequestContainer>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m835325692(__this, method) ((  void (*) (Enumerator_t690157333 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2809374949_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,PlayFab.CallRequestContainer>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1586040005(__this, method) ((  DictionaryEntry_t130027246  (*) (Enumerator_t690157333 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2434214620_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,PlayFab.CallRequestContainer>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m747398980(__this, method) ((  Il2CppObject * (*) (Enumerator_t690157333 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3735627447_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,PlayFab.CallRequestContainer>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2039317718(__this, method) ((  Il2CppObject * (*) (Enumerator_t690157333 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m393753481_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,PlayFab.CallRequestContainer>::MoveNext()
#define Enumerator_MoveNext_m2067202408(__this, method) ((  bool (*) (Enumerator_t690157333 *, const MethodInfo*))Enumerator_MoveNext_m1213995029_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,PlayFab.CallRequestContainer>::get_Current()
#define Enumerator_get_Current_m2624111176(__this, method) ((  KeyValuePair_2_t411660690  (*) (Enumerator_t690157333 *, const MethodInfo*))Enumerator_get_Current_m1399860359_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,PlayFab.CallRequestContainer>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2367210421(__this, method) ((  int32_t (*) (Enumerator_t690157333 *, const MethodInfo*))Enumerator_get_CurrentKey_m1767398110_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,PlayFab.CallRequestContainer>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1024444313(__this, method) ((  CallRequestContainer_t432318609 * (*) (Enumerator_t690157333 *, const MethodInfo*))Enumerator_get_CurrentValue_m3384846750_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,PlayFab.CallRequestContainer>::Reset()
#define Enumerator_Reset_m1903462635(__this, method) ((  void (*) (Enumerator_t690157333 *, const MethodInfo*))Enumerator_Reset_m1080084514_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,PlayFab.CallRequestContainer>::VerifyState()
#define Enumerator_VerifyState_m2606382260(__this, method) ((  void (*) (Enumerator_t690157333 *, const MethodInfo*))Enumerator_VerifyState_m2404513451_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,PlayFab.CallRequestContainer>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m3512290076(__this, method) ((  void (*) (Enumerator_t690157333 *, const MethodInfo*))Enumerator_VerifyCurrent_m2789892947_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,PlayFab.CallRequestContainer>::Dispose()
#define Enumerator_Dispose_m2094953211(__this, method) ((  void (*) (Enumerator_t690157333 *, const MethodInfo*))Enumerator_Dispose_m1102561394_gshared)(__this, method)
