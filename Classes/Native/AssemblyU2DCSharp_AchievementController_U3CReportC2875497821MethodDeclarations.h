﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AchievementController/<ReportCoroutine>c__Iterator8
struct U3CReportCoroutineU3Ec__Iterator8_t2875497821;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void AchievementController/<ReportCoroutine>c__Iterator8::.ctor()
extern "C"  void U3CReportCoroutineU3Ec__Iterator8__ctor_m777067826 (U3CReportCoroutineU3Ec__Iterator8_t2875497821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AchievementController/<ReportCoroutine>c__Iterator8::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CReportCoroutineU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3638087136 (U3CReportCoroutineU3Ec__Iterator8_t2875497821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object AchievementController/<ReportCoroutine>c__Iterator8::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CReportCoroutineU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m4062351732 (U3CReportCoroutineU3Ec__Iterator8_t2875497821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AchievementController/<ReportCoroutine>c__Iterator8::MoveNext()
extern "C"  bool U3CReportCoroutineU3Ec__Iterator8_MoveNext_m3460578690 (U3CReportCoroutineU3Ec__Iterator8_t2875497821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AchievementController/<ReportCoroutine>c__Iterator8::Dispose()
extern "C"  void U3CReportCoroutineU3Ec__Iterator8_Dispose_m3631121647 (U3CReportCoroutineU3Ec__Iterator8_t2875497821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AchievementController/<ReportCoroutine>c__Iterator8::Reset()
extern "C"  void U3CReportCoroutineU3Ec__Iterator8_Reset_m2718468063 (U3CReportCoroutineU3Ec__Iterator8_t2875497821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
