﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Transaction
struct Transaction_t3809114814;
// System.Action
struct Action_t437523947;
// ITransactionable
struct ITransactionable_t1652082095;
// ICell
struct ICell_t69513547;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"
#include "mscorlib_System_String968488902.h"

// System.Void Transaction::.ctor()
extern "C"  void Transaction__ctor_m2311861869 (Transaction_t3809114814 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Transaction::.cctor()
extern "C"  void Transaction__cctor_m2466144992 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Transaction::AddDataAction(System.Int32,System.Action)
extern "C"  void Transaction_AddDataAction_m1712112276 (Il2CppObject * __this /* static, unused */, int32_t ___priority0, Action_t437523947 * ___act1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Transaction::RegisterPackedReaction(ITransactionable)
extern "C"  void Transaction_RegisterPackedReaction_m1156829996 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Transaction::Perform(System.Action)
extern "C"  void Transaction_Perform_m126662865 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Transaction::AddTouchedCell(ICell)
extern "C"  void Transaction_AddTouchedCell_m3715775313 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___cell0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Transaction::Test()
extern "C"  void Transaction_Test_m1164303689 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Transaction::TestCalculation()
extern "C"  void Transaction_TestCalculation_m4091949090 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Transaction::<Test>m__1EC(System.Int32)
extern "C"  void Transaction_U3CTestU3Em__1EC_m1888381402 (Il2CppObject * __this /* static, unused */, int32_t ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Transaction::<Test>m__1ED(System.Int32)
extern "C"  void Transaction_U3CTestU3Em__1ED_m3395933211 (Il2CppObject * __this /* static, unused */, int32_t ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Transaction::<Test>m__1EE(System.Int32)
extern "C"  void Transaction_U3CTestU3Em__1EE_m608517724 (Il2CppObject * __this /* static, unused */, int32_t ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Transaction::<Test>m__1F0(System.Int32)
extern "C"  void Transaction_U3CTestU3Em__1F0_m2799133926 (Il2CppObject * __this /* static, unused */, int32_t ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Transaction::<Test>m__1F1(System.Int32)
extern "C"  void Transaction_U3CTestU3Em__1F1_m11718439 (Il2CppObject * __this /* static, unused */, int32_t ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Transaction::<Test>m__1F2(System.Int32)
extern "C"  void Transaction_U3CTestU3Em__1F2_m1519270248 (Il2CppObject * __this /* static, unused */, int32_t ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Transaction::<Test>m__1F4(System.Int32,System.Int32)
extern "C"  int32_t Transaction_U3CTestU3Em__1F4_m2856407611 (Il2CppObject * __this /* static, unused */, int32_t ___v10, int32_t ___v21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Transaction::<Test>m__1F5(System.Int32)
extern "C"  void Transaction_U3CTestU3Em__1F5_m1746958379 (Il2CppObject * __this /* static, unused */, int32_t ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Transaction::<TestCalculation>m__1F8(System.String)
extern "C"  void Transaction_U3CTestCalculationU3Em__1F8_m2945634238 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
