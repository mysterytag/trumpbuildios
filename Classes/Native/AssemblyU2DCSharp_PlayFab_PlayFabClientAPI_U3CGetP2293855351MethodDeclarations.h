﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<GetPublisherData>c__AnonStoreyBD
struct U3CGetPublisherDataU3Ec__AnonStoreyBD_t2293855351;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<GetPublisherData>c__AnonStoreyBD::.ctor()
extern "C"  void U3CGetPublisherDataU3Ec__AnonStoreyBD__ctor_m3370315036 (U3CGetPublisherDataU3Ec__AnonStoreyBD_t2293855351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<GetPublisherData>c__AnonStoreyBD::<>m__AA(PlayFab.CallRequestContainer)
extern "C"  void U3CGetPublisherDataU3Ec__AnonStoreyBD_U3CU3Em__AA_m1423061786 (U3CGetPublisherDataU3Ec__AnonStoreyBD_t2293855351 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
