﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FbController
struct FbController_t3069175192;
// Facebook.Unity.ILoginResult
struct ILoginResult_t1998157500;
// Facebook.Unity.IGraphResult
struct IGraphResult_t2342947041;
// Facebook.Unity.IShareResult
struct IShareResult_t747644594;
// System.String
struct String_t;
// System.Uri
struct Uri_t2776692961;
// Facebook.Unity.IGroupCreateResult
struct IGroupCreateResult_t1652284654;
// Facebook.Unity.IGroupJoinResult
struct IGroupJoinResult_t1264024604;
// Facebook.Unity.IAppRequestResult
struct IAppRequestResult_t2121186099;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "System_System_Uri2776692961.h"

// System.Void FbController::.ctor()
extern "C"  void FbController__ctor_m3951351811 (FbController_t3069175192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FbController::Initialize()
extern "C"  void FbController_Initialize_m2001537137 (FbController_t3069175192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FbController::Awake()
extern "C"  void FbController_Awake_m4188957030 (FbController_t3069175192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FbController::InitCallback()
extern "C"  void FbController_InitCallback_m182958358 (FbController_t3069175192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FbController::OnHideUnity(System.Boolean)
extern "C"  void FbController_OnHideUnity_m4051576076 (FbController_t3069175192 * __this, bool ___isGameShown0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FbController::LogCheck()
extern "C"  void FbController_LogCheck_m3227753477 (FbController_t3069175192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FbController::AuthCallback(Facebook.Unity.ILoginResult)
extern "C"  void FbController_AuthCallback_m1014168166 (FbController_t3069175192 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FbController::GetProfileInfo()
extern "C"  void FbController_GetProfileInfo_m4228215138 (FbController_t3069175192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FbController::APICallbac(Facebook.Unity.IGraphResult)
extern "C"  void FbController_APICallbac_m16912866 (FbController_t3069175192 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FbController::Login()
extern "C"  void FbController_Login_m843052618 (FbController_t3069175192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FbController::ShareCallBack(Facebook.Unity.IShareResult)
extern "C"  void FbController_ShareCallBack_m3823362297 (FbController_t3069175192 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FbController::Shared_FB(System.String,System.String)
extern "C"  void FbController_Shared_FB_m2777845479 (FbController_t3069175192 * __this, String_t* ___title0, String_t* ___body1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FbController::AchivShareFB(System.String,System.String,System.String,System.Uri)
extern "C"  void FbController_AchivShareFB_m2795009672 (FbController_t3069175192 * __this, String_t* ___titl0, String_t* ___body1, String_t* ___desc2, Uri_t2776692961 * ___imgurl3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FbController::feedCallBack(Facebook.Unity.IShareResult)
extern "C"  void FbController_feedCallBack_m264831738 (FbController_t3069175192 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FbController::ShareCall(Facebook.Unity.IShareResult)
extern "C"  void FbController_ShareCall_m420074016 (FbController_t3069175192 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FbController::LogOutFB()
extern "C"  void FbController_LogOutFB_m1378024199 (FbController_t3069175192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FbController::GroupJoinCallBack(Facebook.Unity.IGroupCreateResult)
extern "C"  void FbController_GroupJoinCallBack_m2899330387 (FbController_t3069175192 * __this, Il2CppObject * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FbController::JoinGroupFB()
extern "C"  void FbController_JoinGroupFB_m2520743890 (FbController_t3069175192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FbController::InviteFrendsFB()
extern "C"  void FbController_InviteFrendsFB_m357508682 (FbController_t3069175192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FbController::Update()
extern "C"  void FbController_Update_m3959683946 (FbController_t3069175192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FbController::<LogCheck>m__14(System.Int64)
extern "C"  void FbController_U3CLogCheckU3Em__14_m3506570245 (FbController_t3069175192 * __this, int64_t ___l0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FbController::<AuthCallback>m__15(System.Int64)
extern "C"  void FbController_U3CAuthCallbackU3Em__15_m1769680815 (FbController_t3069175192 * __this, int64_t ___l0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FbController::<ShareCallBack>m__16(System.Int64)
extern "C"  void FbController_U3CShareCallBackU3Em__16_m1461457423 (FbController_t3069175192 * __this, int64_t ___l0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FbController::<ShareCallBack>m__17(System.Int64)
extern "C"  void FbController_U3CShareCallBackU3Em__17_m2969009232 (FbController_t3069175192 * __this, int64_t ___l0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FbController::<JoinGroupFB>m__18(Facebook.Unity.IGroupJoinResult)
extern "C"  void FbController_U3CJoinGroupFBU3Em__18_m1998385704 (FbController_t3069175192 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FbController::<InviteFrendsFB>m__19(Facebook.Unity.IAppRequestResult)
extern "C"  void FbController_U3CInviteFrendsFBU3Em__19_m584426090 (FbController_t3069175192 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
