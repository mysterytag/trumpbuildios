﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectManyObservable`2/SelectManyEnumerableObserverWithIndex<System.Object,UniRx.Unit>
struct SelectManyEnumerableObserverWithIndex_t1783110290;
// UniRx.Operators.SelectManyObservable`2<System.Object,UniRx.Unit>
struct SelectManyObservable_2_t2284173584;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyEnumerableObserverWithIndex<System.Object,UniRx.Unit>::.ctor(UniRx.Operators.SelectManyObservable`2<TSource,TResult>,UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void SelectManyEnumerableObserverWithIndex__ctor_m2995543886_gshared (SelectManyEnumerableObserverWithIndex_t1783110290 * __this, SelectManyObservable_2_t2284173584 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define SelectManyEnumerableObserverWithIndex__ctor_m2995543886(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (SelectManyEnumerableObserverWithIndex_t1783110290 *, SelectManyObservable_2_t2284173584 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SelectManyEnumerableObserverWithIndex__ctor_m2995543886_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.SelectManyObservable`2/SelectManyEnumerableObserverWithIndex<System.Object,UniRx.Unit>::Run()
extern "C"  Il2CppObject * SelectManyEnumerableObserverWithIndex_Run_m1983343770_gshared (SelectManyEnumerableObserverWithIndex_t1783110290 * __this, const MethodInfo* method);
#define SelectManyEnumerableObserverWithIndex_Run_m1983343770(__this, method) ((  Il2CppObject * (*) (SelectManyEnumerableObserverWithIndex_t1783110290 *, const MethodInfo*))SelectManyEnumerableObserverWithIndex_Run_m1983343770_gshared)(__this, method)
// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyEnumerableObserverWithIndex<System.Object,UniRx.Unit>::OnNext(TSource)
extern "C"  void SelectManyEnumerableObserverWithIndex_OnNext_m1573677913_gshared (SelectManyEnumerableObserverWithIndex_t1783110290 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define SelectManyEnumerableObserverWithIndex_OnNext_m1573677913(__this, ___value0, method) ((  void (*) (SelectManyEnumerableObserverWithIndex_t1783110290 *, Il2CppObject *, const MethodInfo*))SelectManyEnumerableObserverWithIndex_OnNext_m1573677913_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyEnumerableObserverWithIndex<System.Object,UniRx.Unit>::OnError(System.Exception)
extern "C"  void SelectManyEnumerableObserverWithIndex_OnError_m96309123_gshared (SelectManyEnumerableObserverWithIndex_t1783110290 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define SelectManyEnumerableObserverWithIndex_OnError_m96309123(__this, ___error0, method) ((  void (*) (SelectManyEnumerableObserverWithIndex_t1783110290 *, Exception_t1967233988 *, const MethodInfo*))SelectManyEnumerableObserverWithIndex_OnError_m96309123_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SelectManyObservable`2/SelectManyEnumerableObserverWithIndex<System.Object,UniRx.Unit>::OnCompleted()
extern "C"  void SelectManyEnumerableObserverWithIndex_OnCompleted_m1583165270_gshared (SelectManyEnumerableObserverWithIndex_t1783110290 * __this, const MethodInfo* method);
#define SelectManyEnumerableObserverWithIndex_OnCompleted_m1583165270(__this, method) ((  void (*) (SelectManyEnumerableObserverWithIndex_t1783110290 *, const MethodInfo*))SelectManyEnumerableObserverWithIndex_OnCompleted_m1583165270_gshared)(__this, method)
