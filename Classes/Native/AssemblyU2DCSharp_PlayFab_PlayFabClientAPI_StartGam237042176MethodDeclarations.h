﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/StartGameRequestCallback
struct StartGameRequestCallback_t237042176;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.StartGameRequest
struct StartGameRequest_t2370915499;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_StartGameRe2370915499.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/StartGameRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void StartGameRequestCallback__ctor_m3734481647 (StartGameRequestCallback_t237042176 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/StartGameRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.StartGameRequest,System.Object)
extern "C"  void StartGameRequestCallback_Invoke_m2037171679 (StartGameRequestCallback_t237042176 * __this, String_t* ___urlPath0, int32_t ___callId1, StartGameRequest_t2370915499 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_StartGameRequestCallback_t237042176(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, StartGameRequest_t2370915499 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/StartGameRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.StartGameRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * StartGameRequestCallback_BeginInvoke_m549142502 (StartGameRequestCallback_t237042176 * __this, String_t* ___urlPath0, int32_t ___callId1, StartGameRequest_t2370915499 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/StartGameRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void StartGameRequestCallback_EndInvoke_m3986418815 (StartGameRequestCallback_t237042176 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
