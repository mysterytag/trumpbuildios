﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.AppInviteResult
struct AppInviteResult_t2846493446;
// Facebook.Unity.ResultContainer
struct ResultContainer_t79372963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Facebook_Unity_ResultContainer79372963.h"

// System.Void Facebook.Unity.AppInviteResult::.ctor(Facebook.Unity.ResultContainer)
extern "C"  void AppInviteResult__ctor_m770325522 (AppInviteResult_t2846493446 * __this, ResultContainer_t79372963 * ___resultContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
