﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.ZenjectTypeInfo
struct ZenjectTypeInfo_t283213708;
// System.Type
struct Type_t;
// System.Collections.Generic.IEnumerable`1<Zenject.InjectableInfo>
struct IEnumerable_1_t4019864130;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t3542137334;
// Zenject.InjectableInfo
struct InjectableInfo_t1147709774;
// System.Reflection.ParameterInfo
struct ParameterInfo_t2610273829;
// System.Collections.Generic.List`1<Zenject.PostInjectableInfo>
struct List_1_t3877242631;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_System_Reflection_ConstructorInfo3542137334.h"
#include "mscorlib_System_Reflection_ParameterInfo2610273829.h"
#include "mscorlib_System_Reflection_MemberInfo2843033814.h"
#include "mscorlib_System_Reflection_MethodInfo3461221277.h"

// System.Void Zenject.TypeAnalyzer::.cctor()
extern "C"  void TypeAnalyzer__cctor_m403752817 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.ZenjectTypeInfo Zenject.TypeAnalyzer::GetInfo(System.Type)
extern "C"  ZenjectTypeInfo_t283213708 * TypeAnalyzer_GetInfo_m224667190 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.ZenjectTypeInfo Zenject.TypeAnalyzer::CreateTypeInfo(System.Type)
extern "C"  ZenjectTypeInfo_t283213708 * TypeAnalyzer_CreateTypeInfo_m444995314 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Zenject.InjectableInfo> Zenject.TypeAnalyzer::GetConstructorInjectables(System.Type,System.Reflection.ConstructorInfo)
extern "C"  Il2CppObject* TypeAnalyzer_GetConstructorInjectables_m1689975587 (Il2CppObject * __this /* static, unused */, Type_t * ___parentType0, ConstructorInfo_t3542137334 * ___constructorInfo1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.InjectableInfo Zenject.TypeAnalyzer::CreateInjectableInfoForParam(System.Type,System.Reflection.ParameterInfo)
extern "C"  InjectableInfo_t1147709774 * TypeAnalyzer_CreateInjectableInfoForParam_m3638764108 (Il2CppObject * __this /* static, unused */, Type_t * ___parentType0, ParameterInfo_t2610273829 * ___paramInfo1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Zenject.PostInjectableInfo> Zenject.TypeAnalyzer::GetPostInjectMethods(System.Type)
extern "C"  List_1_t3877242631 * TypeAnalyzer_GetPostInjectMethods_m3454744571 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Zenject.InjectableInfo> Zenject.TypeAnalyzer::GetPropertyInjectables(System.Type)
extern "C"  Il2CppObject* TypeAnalyzer_GetPropertyInjectables_m2870845588 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Zenject.InjectableInfo> Zenject.TypeAnalyzer::GetFieldInjectables(System.Type)
extern "C"  Il2CppObject* TypeAnalyzer_GetFieldInjectables_m2698805947 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.InjectableInfo Zenject.TypeAnalyzer::CreateForMember(System.Reflection.MemberInfo,System.Type)
extern "C"  InjectableInfo_t1147709774 * TypeAnalyzer_CreateForMember_m3555310243 (Il2CppObject * __this /* static, unused */, MemberInfo_t * ___memInfo0, Type_t * ___parentType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ConstructorInfo Zenject.TypeAnalyzer::GetInjectConstructor(System.Type)
extern "C"  ConstructorInfo_t3542137334 * TypeAnalyzer_GetInjectConstructor_m1524952239 (Il2CppObject * __this /* static, unused */, Type_t * ___parentType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.TypeAnalyzer::<GetPostInjectMethods>m__2B1(System.Reflection.MethodInfo)
extern "C"  bool TypeAnalyzer_U3CGetPostInjectMethodsU3Em__2B1_m1279773076 (Il2CppObject * __this /* static, unused */, MethodInfo_t * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.TypeAnalyzer::<GetInjectConstructor>m__2B6(System.Reflection.ConstructorInfo)
extern "C"  bool TypeAnalyzer_U3CGetInjectConstructorU3Em__2B6_m523941450 (Il2CppObject * __this /* static, unused */, ConstructorInfo_t3542137334 * ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
