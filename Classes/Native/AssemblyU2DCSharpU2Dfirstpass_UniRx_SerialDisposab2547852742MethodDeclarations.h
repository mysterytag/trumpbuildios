﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.SerialDisposable
struct SerialDisposable_t2547852742;
// System.IDisposable
struct IDisposable_t1628921374;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.SerialDisposable::.ctor()
extern "C"  void SerialDisposable__ctor_m2977852291 (SerialDisposable_t2547852742 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UniRx.SerialDisposable::get_IsDisposed()
extern "C"  bool SerialDisposable_get_IsDisposed_m623962869 (SerialDisposable_t2547852742 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable UniRx.SerialDisposable::get_Disposable()
extern "C"  Il2CppObject * SerialDisposable_get_Disposable_m4156835141 (SerialDisposable_t2547852742 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.SerialDisposable::set_Disposable(System.IDisposable)
extern "C"  void SerialDisposable_set_Disposable_m1438046228 (SerialDisposable_t2547852742 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.SerialDisposable::Dispose()
extern "C"  void SerialDisposable_Dispose_m1166115584 (SerialDisposable_t2547852742 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
