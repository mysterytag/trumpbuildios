﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.CompilerServices.CustomConstantAttribute
struct CustomConstantAttribute_t3826010445;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.CompilerServices.CustomConstantAttribute::.ctor()
extern "C"  void CustomConstantAttribute__ctor_m4235955465 (CustomConstantAttribute_t3826010445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
