﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey161
struct U3CRegisterDonateButtonsU3Ec__AnonStorey161_t2690654992;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey161::.ctor()
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey161__ctor_m2356069950 (U3CRegisterDonateButtonsU3Ec__AnonStorey161_t2690654992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey161::<>m__261(UniRx.Unit)
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey161_U3CU3Em__261_m258756556 (U3CRegisterDonateButtonsU3Ec__AnonStorey161_t2690654992 * __this, Unit_t2558286038  ___unit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey161::<>m__262(System.Boolean)
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey161_U3CU3Em__262_m1590565344 (U3CRegisterDonateButtonsU3Ec__AnonStorey161_t2690654992 * __this, bool ___connected0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
