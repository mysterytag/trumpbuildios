﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetGameServerRegionsRequestCallback
struct GetGameServerRegionsRequestCallback_t51967360;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GameServerRegionsRequest
struct GameServerRegionsRequest_t2476765941;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GameServerR2476765941.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetGameServerRegionsRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetGameServerRegionsRequestCallback__ctor_m3595819535 (GetGameServerRegionsRequestCallback_t51967360 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetGameServerRegionsRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GameServerRegionsRequest,System.Object)
extern "C"  void GetGameServerRegionsRequestCallback_Invoke_m2423352949 (GetGameServerRegionsRequestCallback_t51967360 * __this, String_t* ___urlPath0, int32_t ___callId1, GameServerRegionsRequest_t2476765941 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetGameServerRegionsRequestCallback_t51967360(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GameServerRegionsRequest_t2476765941 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetGameServerRegionsRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GameServerRegionsRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetGameServerRegionsRequestCallback_BeginInvoke_m2539355992 (GetGameServerRegionsRequestCallback_t51967360 * __this, String_t* ___urlPath0, int32_t ___callId1, GameServerRegionsRequest_t2476765941 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetGameServerRegionsRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetGameServerRegionsRequestCallback_EndInvoke_m403483551 (GetGameServerRegionsRequestCallback_t51967360 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
