﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Vector3>
struct ReactivePropertyObserver_t3761312358;
// UniRx.ReactiveProperty`1<UnityEngine.Vector3>
struct ReactiveProperty_1_t4133662827;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Vector3>::.ctor(UniRx.ReactiveProperty`1<T>)
extern "C"  void ReactivePropertyObserver__ctor_m172595289_gshared (ReactivePropertyObserver_t3761312358 * __this, ReactiveProperty_1_t4133662827 * ___parent0, const MethodInfo* method);
#define ReactivePropertyObserver__ctor_m172595289(__this, ___parent0, method) ((  void (*) (ReactivePropertyObserver_t3761312358 *, ReactiveProperty_1_t4133662827 *, const MethodInfo*))ReactivePropertyObserver__ctor_m172595289_gshared)(__this, ___parent0, method)
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Vector3>::OnNext(T)
extern "C"  void ReactivePropertyObserver_OnNext_m65627954_gshared (ReactivePropertyObserver_t3761312358 * __this, Vector3_t3525329789  ___value0, const MethodInfo* method);
#define ReactivePropertyObserver_OnNext_m65627954(__this, ___value0, method) ((  void (*) (ReactivePropertyObserver_t3761312358 *, Vector3_t3525329789 , const MethodInfo*))ReactivePropertyObserver_OnNext_m65627954_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Vector3>::OnError(System.Exception)
extern "C"  void ReactivePropertyObserver_OnError_m380427841_gshared (ReactivePropertyObserver_t3761312358 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ReactivePropertyObserver_OnError_m380427841(__this, ___error0, method) ((  void (*) (ReactivePropertyObserver_t3761312358 *, Exception_t1967233988 *, const MethodInfo*))ReactivePropertyObserver_OnError_m380427841_gshared)(__this, ___error0, method)
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Vector3>::OnCompleted()
extern "C"  void ReactivePropertyObserver_OnCompleted_m3937282836_gshared (ReactivePropertyObserver_t3761312358 * __this, const MethodInfo* method);
#define ReactivePropertyObserver_OnCompleted_m3937282836(__this, method) ((  void (*) (ReactivePropertyObserver_t3761312358 *, const MethodInfo*))ReactivePropertyObserver_OnCompleted_m3937282836_gshared)(__this, method)
