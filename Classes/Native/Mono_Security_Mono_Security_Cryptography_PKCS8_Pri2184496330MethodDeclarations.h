﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.Cryptography.PKCS8/PrivateKeyInfo
struct PrivateKeyInfo_t2184496331;
// System.Byte[]
struct ByteU5BU5D_t58506160;
// System.String
struct String_t;
// System.Security.Cryptography.RSA
struct RSA_t1557565273;
// System.Security.Cryptography.DSA
struct DSA_t1557551819;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Security_Cryptography_RSA1557565273.h"
#include "mscorlib_System_Security_Cryptography_DSAParameter2524359253.h"
#include "mscorlib_System_Security_Cryptography_DSA1557551819.h"

// System.Void Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::.ctor()
extern "C"  void PrivateKeyInfo__ctor_m362104835 (PrivateKeyInfo_t2184496331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::.ctor(System.Byte[])
extern "C"  void PrivateKeyInfo__ctor_m3331527846 (PrivateKeyInfo_t2184496331 * __this, ByteU5BU5D_t58506160* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::set_Algorithm(System.String)
extern "C"  void PrivateKeyInfo_set_Algorithm_m3850266735 (PrivateKeyInfo_t2184496331 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::get_PrivateKey()
extern "C"  ByteU5BU5D_t58506160* PrivateKeyInfo_get_PrivateKey_m2153221404 (PrivateKeyInfo_t2184496331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::set_PrivateKey(System.Byte[])
extern "C"  void PrivateKeyInfo_set_PrivateKey_m2448114063 (PrivateKeyInfo_t2184496331 * __this, ByteU5BU5D_t58506160* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::Decode(System.Byte[])
extern "C"  void PrivateKeyInfo_Decode_m2973591770 (PrivateKeyInfo_t2184496331 * __this, ByteU5BU5D_t58506160* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::GetBytes()
extern "C"  ByteU5BU5D_t58506160* PrivateKeyInfo_GetBytes_m3266878956 (PrivateKeyInfo_t2184496331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::RemoveLeadingZero(System.Byte[])
extern "C"  ByteU5BU5D_t58506160* PrivateKeyInfo_RemoveLeadingZero_m3776637556 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t58506160* ___bigInt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::Normalize(System.Byte[],System.Int32)
extern "C"  ByteU5BU5D_t58506160* PrivateKeyInfo_Normalize_m3523909062 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t58506160* ___bigInt0, int32_t ___length1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.RSA Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::DecodeRSA(System.Byte[])
extern "C"  RSA_t1557565273 * PrivateKeyInfo_DecodeRSA_m311172684 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t58506160* ___keypair0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::Encode(System.Security.Cryptography.RSA)
extern "C"  ByteU5BU5D_t58506160* PrivateKeyInfo_Encode_m3791041536 (Il2CppObject * __this /* static, unused */, RSA_t1557565273 * ___rsa0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.DSA Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::DecodeDSA(System.Byte[],System.Security.Cryptography.DSAParameters)
extern "C"  DSA_t1557551819 * PrivateKeyInfo_DecodeDSA_m1682427249 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t58506160* ___privateKey0, DSAParameters_t2524359253  ___dsaParameters1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.PKCS8/PrivateKeyInfo::Encode(System.Security.Cryptography.DSA)
extern "C"  ByteU5BU5D_t58506160* PrivateKeyInfo_Encode_m3790624462 (Il2CppObject * __this /* static, unused */, DSA_t1557551819 * ___dsa0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
