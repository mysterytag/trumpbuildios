﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/ReportPlayerRequestCallback
struct ReportPlayerRequestCallback_t1214398943;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.ReportPlayerClientRequest
struct ReportPlayerClientRequest_t3784712447;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_ReportPlaye3784712447.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/ReportPlayerRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void ReportPlayerRequestCallback__ctor_m2102293230 (ReportPlayerRequestCallback_t1214398943 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/ReportPlayerRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.ReportPlayerClientRequest,System.Object)
extern "C"  void ReportPlayerRequestCallback_Invoke_m3840429446 (ReportPlayerRequestCallback_t1214398943 * __this, String_t* ___urlPath0, int32_t ___callId1, ReportPlayerClientRequest_t3784712447 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_ReportPlayerRequestCallback_t1214398943(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, ReportPlayerClientRequest_t3784712447 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/ReportPlayerRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.ReportPlayerClientRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ReportPlayerRequestCallback_BeginInvoke_m2508655923 (ReportPlayerRequestCallback_t1214398943 * __this, String_t* ___urlPath0, int32_t ___callId1, ReportPlayerClientRequest_t3784712447 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/ReportPlayerRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void ReportPlayerRequestCallback_EndInvoke_m1421642750 (ReportPlayerRequestCallback_t1214398943 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
