﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.AsObservableObservable`1/AsObservable<System.Object>
struct AsObservable_t202062822;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.AsObservableObservable`1/AsObservable<System.Object>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void AsObservable__ctor_m2772916531_gshared (AsObservable_t202062822 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define AsObservable__ctor_m2772916531(__this, ___observer0, ___cancel1, method) ((  void (*) (AsObservable_t202062822 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))AsObservable__ctor_m2772916531_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.AsObservableObservable`1/AsObservable<System.Object>::OnNext(T)
extern "C"  void AsObservable_OnNext_m2074187391_gshared (AsObservable_t202062822 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define AsObservable_OnNext_m2074187391(__this, ___value0, method) ((  void (*) (AsObservable_t202062822 *, Il2CppObject *, const MethodInfo*))AsObservable_OnNext_m2074187391_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.AsObservableObservable`1/AsObservable<System.Object>::OnError(System.Exception)
extern "C"  void AsObservable_OnError_m2436813710_gshared (AsObservable_t202062822 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define AsObservable_OnError_m2436813710(__this, ___error0, method) ((  void (*) (AsObservable_t202062822 *, Exception_t1967233988 *, const MethodInfo*))AsObservable_OnError_m2436813710_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.AsObservableObservable`1/AsObservable<System.Object>::OnCompleted()
extern "C"  void AsObservable_OnCompleted_m1626598369_gshared (AsObservable_t202062822 * __this, const MethodInfo* method);
#define AsObservable_OnCompleted_m1626598369(__this, method) ((  void (*) (AsObservable_t202062822 *, const MethodInfo*))AsObservable_OnCompleted_m1626598369_gshared)(__this, method)
