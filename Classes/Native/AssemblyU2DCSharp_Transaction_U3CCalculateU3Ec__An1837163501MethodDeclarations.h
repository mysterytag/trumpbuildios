﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Transaction/<Calculate>c__AnonStorey149`1<System.Object>
struct U3CCalculateU3Ec__AnonStorey149_1_t1837163501;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"

// System.Void Transaction/<Calculate>c__AnonStorey149`1<System.Object>::.ctor()
extern "C"  void U3CCalculateU3Ec__AnonStorey149_1__ctor_m290250690_gshared (U3CCalculateU3Ec__AnonStorey149_1_t1837163501 * __this, const MethodInfo* method);
#define U3CCalculateU3Ec__AnonStorey149_1__ctor_m290250690(__this, method) ((  void (*) (U3CCalculateU3Ec__AnonStorey149_1_t1837163501 *, const MethodInfo*))U3CCalculateU3Ec__AnonStorey149_1__ctor_m290250690_gshared)(__this, method)
// System.IDisposable Transaction/<Calculate>c__AnonStorey149`1<System.Object>::<>m__1EA(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * U3CCalculateU3Ec__AnonStorey149_1_U3CU3Em__1EA_m2078672075_gshared (U3CCalculateU3Ec__AnonStorey149_1_t1837163501 * __this, Action_1_t985559125 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define U3CCalculateU3Ec__AnonStorey149_1_U3CU3Em__1EA_m2078672075(__this, ___reaction0, ___p1, method) ((  Il2CppObject * (*) (U3CCalculateU3Ec__AnonStorey149_1_t1837163501 *, Action_1_t985559125 *, int32_t, const MethodInfo*))U3CCalculateU3Ec__AnonStorey149_1_U3CU3Em__1EA_m2078672075_gshared)(__this, ___reaction0, ___p1, method)
// T Transaction/<Calculate>c__AnonStorey149`1<System.Object>::<>m__1EB()
extern "C"  Il2CppObject * U3CCalculateU3Ec__AnonStorey149_1_U3CU3Em__1EB_m1173453380_gshared (U3CCalculateU3Ec__AnonStorey149_1_t1837163501 * __this, const MethodInfo* method);
#define U3CCalculateU3Ec__AnonStorey149_1_U3CU3Em__1EB_m1173453380(__this, method) ((  Il2CppObject * (*) (U3CCalculateU3Ec__AnonStorey149_1_t1837163501 *, const MethodInfo*))U3CCalculateU3Ec__AnonStorey149_1_U3CU3Em__1EB_m1173453380_gshared)(__this, method)
