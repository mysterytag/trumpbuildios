﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.Dns/GetHostByNameCallback
struct GetHostByNameCallback_t2530953093;
// System.Object
struct Il2CppObject;
// System.Net.IPHostEntry
struct IPHostEntry_t4205702573;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Net.Dns/GetHostByNameCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetHostByNameCallback__ctor_m1526231925 (GetHostByNameCallback_t2530953093 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IPHostEntry System.Net.Dns/GetHostByNameCallback::Invoke(System.String)
extern "C"  IPHostEntry_t4205702573 * GetHostByNameCallback_Invoke_m1515139655 (GetHostByNameCallback_t2530953093 * __this, String_t* ___hostName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" IPHostEntry_t4205702573 * pinvoke_delegate_wrapper_GetHostByNameCallback_t2530953093(Il2CppObject* delegate, String_t* ___hostName0);
// System.IAsyncResult System.Net.Dns/GetHostByNameCallback::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetHostByNameCallback_BeginInvoke_m1591977432 (GetHostByNameCallback_t2530953093 * __this, String_t* ___hostName0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IPHostEntry System.Net.Dns/GetHostByNameCallback::EndInvoke(System.IAsyncResult)
extern "C"  IPHostEntry_t4205702573 * GetHostByNameCallback_EndInvoke_m3674681233 (GetHostByNameCallback_t2530953093 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
