﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.DisposablePrioritiesInstaller
struct DisposablePrioritiesInstaller_t471724603;
// System.Collections.Generic.List`1<System.Type>
struct List_1_t3576188904;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449.h"
#include "mscorlib_System_Type2779229935.h"

// System.Void Zenject.DisposablePrioritiesInstaller::.ctor(System.Collections.Generic.List`1<System.Type>)
extern "C"  void DisposablePrioritiesInstaller__ctor_m996211809 (DisposablePrioritiesInstaller_t471724603 * __this, List_1_t3576188904 * ___disposables0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.DisposablePrioritiesInstaller::InstallBindings()
extern "C"  void DisposablePrioritiesInstaller_InstallBindings_m1852522283 (DisposablePrioritiesInstaller_t471724603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.DisposablePrioritiesInstaller::BindPriority(Zenject.DiContainer,System.Type,System.Int32)
extern "C"  void DisposablePrioritiesInstaller_BindPriority_m2187886128 (Il2CppObject * __this /* static, unused */, DiContainer_t2383114449 * ___container0, Type_t * ___disposableType1, int32_t ___priorityCount2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
