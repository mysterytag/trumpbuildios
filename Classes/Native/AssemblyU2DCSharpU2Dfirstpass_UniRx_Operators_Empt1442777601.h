﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Opera154015849.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.EmptyObservable`1/Empty<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>
struct  Empty_t1442777601  : public OperatorObserverBase_2_t154015849
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
