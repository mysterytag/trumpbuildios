﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnalyticsController
struct AnalyticsController_t2265609122;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;
// System.Collections.Generic.Dictionary`2<System.Int32,System.String>
struct Dictionary_2_t1459299685;
// System.Collections.Generic.Dictionary`2<System.Double,System.String>
struct Dictionary_2_t697597558;
// System.Action`1<System.Single>
struct Action_1_t1106661726;
// VKProfileInfo
struct VKProfileInfo_t3473649666;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_TimeSpan763862892.h"
#include "AssemblyU2DCSharp_AnalyticsController_AdvertState3882078461.h"
#include "AssemblyU2DCSharp_AnalyticsController_CustomDimens4016050750.h"
#include "AssemblyU2DCSharp_VKProfileInfo3473649666.h"

// System.Void AnalyticsController::.ctor()
extern "C"  void AnalyticsController__ctor_m4204697097 (AnalyticsController_t2265609122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController::.cctor()
extern "C"  void AnalyticsController__cctor_m1014494916 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController::ClearSessionData()
extern "C"  void AnalyticsController_ClearSessionData_m2478968110 (AnalyticsController_t2265609122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController::Awake()
extern "C"  void AnalyticsController_Awake_m147335020 (AnalyticsController_t2265609122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AnalyticsController::Start()
extern "C"  Il2CppObject * AnalyticsController_Start_m2967007569 (AnalyticsController_t2265609122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController::LocalyticsInit()
extern "C"  void AnalyticsController_LocalyticsInit_m311821726 (AnalyticsController_t2265609122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController::GameLoadComplete()
extern "C"  void AnalyticsController_GameLoadComplete_m3830391852 (AnalyticsController_t2265609122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController::CheckSession()
extern "C"  void AnalyticsController_CheckSession_m4032794889 (AnalyticsController_t2265609122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController::HandleOnLocalyticsDidTagEvent(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>,System.Int64)
extern "C"  void AnalyticsController_HandleOnLocalyticsDidTagEvent_m1735288919 (AnalyticsController_t2265609122 * __this, String_t* ___eventName0, Dictionary_2_t2606186806 * ___attributes1, int64_t ___customerValueIncrease2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController::HandleOnLocalyticsSessionDidOpen(System.Boolean,System.Boolean,System.Boolean)
extern "C"  void AnalyticsController_HandleOnLocalyticsSessionDidOpen_m4104200863 (AnalyticsController_t2265609122 * __this, bool ___isFirst0, bool ___isUpgrade1, bool ___isResume2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController::HandleOnLocalyticsSessionWillOpen(System.Boolean,System.Boolean,System.Boolean)
extern "C"  void AnalyticsController_HandleOnLocalyticsSessionWillOpen_m3240241974 (AnalyticsController_t2265609122 * __this, bool ___isFirst0, bool ___isUpgrade1, bool ___isResume2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController::HandleOnLocalyticsSessionWillClose()
extern "C"  void AnalyticsController_HandleOnLocalyticsSessionWillClose_m821744677 (AnalyticsController_t2265609122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> AnalyticsController::StandartStatData()
extern "C"  Dictionary_2_t2606186806 * AnalyticsController_StandartStatData_m1642007736 (AnalyticsController_t2265609122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController::OnApplicationPause(System.Boolean)
extern "C"  void AnalyticsController_OnApplicationPause_m1059574615 (AnalyticsController_t2265609122 * __this, bool ___pause0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController::SendSessionEnd()
extern "C"  void AnalyticsController_SendSessionEnd_m2278500968 (AnalyticsController_t2265609122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController::SendInterstitialRequestes(System.Int32,System.Int32)
extern "C"  void AnalyticsController_SendInterstitialRequestes_m1125272630 (AnalyticsController_t2265609122 * __this, int32_t ___value0, int32_t ___wasShown1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController::SendStinkyResourceEvent(System.Boolean,System.Double,System.String,System.String)
extern "C"  void AnalyticsController_SendStinkyResourceEvent_m3545396994 (AnalyticsController_t2265609122 * __this, bool ___sink0, double ___value1, String_t* ___type2, String_t* ___name3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController::AddStinkyAttribute(System.Collections.Generic.Dictionary`2<System.String,System.String>,System.String,System.Double)
extern "C"  void AnalyticsController_AddStinkyAttribute_m3622317355 (AnalyticsController_t2265609122 * __this, Dictionary_2_t2606186806 * ___dict0, String_t* ___name1, double ___number2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController::SendMoneyDecEvent(System.String,System.Double,System.Int32,System.Int32)
extern "C"  void AnalyticsController_SendMoneyDecEvent_m1541129255 (AnalyticsController_t2265609122 * __this, String_t* ___category0, double ___money1, int32_t ___upgrade_num2, int32_t ___level3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController::SendTutorEvent(System.Int32,System.TimeSpan)
extern "C"  void AnalyticsController_SendTutorEvent_m2989345902 (AnalyticsController_t2265609122 * __this, int32_t ___step0, TimeSpan_t763862892  ___time1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController::SendVideoEvent(System.String,AnalyticsController/AdvertState)
extern "C"  void AnalyticsController_SendVideoEvent_m1144229372 (AnalyticsController_t2265609122 * __this, String_t* ___source0, int32_t ___status1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController::CrystalsInc(System.String,System.String,System.String)
extern "C"  void AnalyticsController_CrystalsInc_m1111385970 (AnalyticsController_t2265609122 * __this, String_t* ___category0, String_t* ___name1, String_t* ___CrystalCount2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController::CrystalDec(System.String,System.String,System.String,System.String,System.String,System.Double)
extern "C"  void AnalyticsController_CrystalDec_m3296302343 (AnalyticsController_t2265609122 * __this, String_t* ___type0, String_t* ___name1, String_t* ___category2, String_t* ___upgrNum3, String_t* ___level4, double ___crystals5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController::SendInterstitialEvent(System.String,System.String,AnalyticsController/AdvertState)
extern "C"  void AnalyticsController_SendInterstitialEvent_m1419620845 (AnalyticsController_t2265609122 * __this, String_t* ___source0, String_t* ___type1, int32_t ___state2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController::SendBannerEvent(System.String,AnalyticsController/AdvertState)
extern "C"  void AnalyticsController_SendBannerEvent_m2294473713 (AnalyticsController_t2265609122 * __this, String_t* ___source0, int32_t ___status1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController::WatchVKLogin()
extern "C"  void AnalyticsController_WatchVKLogin_m3230919904 (AnalyticsController_t2265609122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController::WatchFbLogin()
extern "C"  void AnalyticsController_WatchFbLogin_m3515352281 (AnalyticsController_t2265609122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AnalyticsController::DimFromDict(System.Int32,System.Collections.Generic.Dictionary`2<System.Int32,System.String>)
extern "C"  String_t* AnalyticsController_DimFromDict_m3630285929 (Il2CppObject * __this /* static, unused */, int32_t ___number0, Dictionary_2_t1459299685 * ___dict1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AnalyticsController::DimFromDict(System.Double,System.Collections.Generic.Dictionary`2<System.Double,System.String>)
extern "C"  String_t* AnalyticsController_DimFromDict_m2785456935 (Il2CppObject * __this /* static, unused */, double ___number0, Dictionary_2_t697597558 * ___dict1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController::UpdateVk()
extern "C"  void AnalyticsController_UpdateVk_m1068559097 (AnalyticsController_t2265609122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController::updateFB()
extern "C"  void AnalyticsController_updateFB_m3369545472 (AnalyticsController_t2265609122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController::UpdateCustomDimensions()
extern "C"  void AnalyticsController_UpdateCustomDimensions_m904778466 (AnalyticsController_t2265609122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController::FriendInvetedChanged()
extern "C"  void AnalyticsController_FriendInvetedChanged_m3359179854 (AnalyticsController_t2265609122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController::ProfileSend()
extern "C"  void AnalyticsController_ProfileSend_m3413387064 (AnalyticsController_t2265609122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController::RevenueChanged()
extern "C"  void AnalyticsController_RevenueChanged_m1312807571 (AnalyticsController_t2265609122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController::PaymentCountChanged()
extern "C"  void AnalyticsController_PaymentCountChanged_m2376487346 (AnalyticsController_t2265609122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController::SendAchievementUnlocked(System.String)
extern "C"  void AnalyticsController_SendAchievementUnlocked_m151234801 (AnalyticsController_t2265609122 * __this, String_t* ___achievementName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController::SetCustomDim(AnalyticsController/CustomDimensions,System.String)
extern "C"  void AnalyticsController_SetCustomDim_m3180549185 (AnalyticsController_t2265609122 * __this, int32_t ___dim0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController::SendUserProfile(System.String,System.String,System.String)
extern "C"  void AnalyticsController_SendUserProfile_m520982333 (AnalyticsController_t2265609122 * __this, String_t* ___first_name0, String_t* ___last_name1, String_t* ___age2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController::SendDonateEvent(System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void AnalyticsController_SendDonateEvent_m953927876 (AnalyticsController_t2265609122 * __this, String_t* ___offer0, String_t* ___revenue1, String_t* ___currency2, String_t* ___curCrystal3, String_t* ___origin4, String_t* ___bonusType5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AnalyticsController::MakeCurrenciesRequest(System.String)
extern "C"  String_t* AnalyticsController_MakeCurrenciesRequest_m3654821222 (AnalyticsController_t2265609122 * __this, String_t* ___currency0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController::FriendInvited()
extern "C"  void AnalyticsController_FriendInvited_m484482020 (AnalyticsController_t2265609122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AnalyticsController::ConvertCurrencyCoro(System.String,System.Single,System.Action`1<System.Single>)
extern "C"  Il2CppObject * AnalyticsController_ConvertCurrencyCoro_m3521133614 (AnalyticsController_t2265609122 * __this, String_t* ___currency0, float ___revenue1, Action_1_t1106661726 * ___callback2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AnalyticsController::SendAdsRevenue(System.String,System.Single,System.String)
extern "C"  Il2CppObject * AnalyticsController_SendAdsRevenue_m1307772924 (AnalyticsController_t2265609122 * __this, String_t* ___name0, float ___revenueUSD1, String_t* ___Cur2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController::SendLocalyticsEvent(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>,System.Int64)
extern "C"  void AnalyticsController_SendLocalyticsEvent_m1189364029 (AnalyticsController_t2265609122 * __this, String_t* ___name0, Dictionary_2_t2606186806 * ___dict1, int64_t ___clv2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AnalyticsController::SendRevenue(System.String,System.String,System.String,System.String,System.Boolean,System.String,System.String)
extern "C"  Il2CppObject * AnalyticsController_SendRevenue_m2168808994 (AnalyticsController_t2265609122 * __this, String_t* ___offer0, String_t* ___curCristal1, String_t* ___revenue2, String_t* ___currency3, bool ___storeDebuff4, String_t* ___origin5, String_t* ___bonusType6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController::SendEarnEvent()
extern "C"  void AnalyticsController_SendEarnEvent_m3563498913 (AnalyticsController_t2265609122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController::SendSessionStartEvent()
extern "C"  void AnalyticsController_SendSessionStartEvent_m1850459533 (AnalyticsController_t2265609122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController::SendInstallEvent()
extern "C"  void AnalyticsController_SendInstallEvent_m3565083330 (AnalyticsController_t2265609122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController::SendGameStartOver()
extern "C"  void AnalyticsController_SendGameStartOver_m1466523907 (AnalyticsController_t2265609122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController::onConversationDataReceived(System.String)
extern "C"  void AnalyticsController_onConversationDataReceived_m2075607034 (AnalyticsController_t2265609122 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController::SetProfile(System.String,System.Int64)
extern "C"  void AnalyticsController_SetProfile_m418011608 (AnalyticsController_t2265609122 * __this, String_t* ___field0, int64_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController::SetProfile(System.String,System.String)
extern "C"  void AnalyticsController_SetProfile_m530065084 (AnalyticsController_t2265609122 * __this, String_t* ___field0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController::onConversationDataError(System.String)
extern "C"  void AnalyticsController_onConversationDataError_m735611519 (AnalyticsController_t2265609122 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController::<SendSessionEnd>m__127(System.Int64)
extern "C"  void AnalyticsController_U3CSendSessionEndU3Em__127_m4270557067 (AnalyticsController_t2265609122 * __this, int64_t ___l0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AnalyticsController::<WatchVKLogin>m__128(System.Boolean)
extern "C"  bool AnalyticsController_U3CWatchVKLoginU3Em__128_m707451461 (Il2CppObject * __this /* static, unused */, bool ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController::<WatchVKLogin>m__129(System.Boolean)
extern "C"  void AnalyticsController_U3CWatchVKLoginU3Em__129_m657630330 (AnalyticsController_t2265609122 * __this, bool ____0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnalyticsController::<WatchVKLogin>m__12A(VKProfileInfo)
extern "C"  void AnalyticsController_U3CWatchVKLoginU3Em__12A_m1455200645 (AnalyticsController_t2265609122 * __this, VKProfileInfo_t3473649666 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
