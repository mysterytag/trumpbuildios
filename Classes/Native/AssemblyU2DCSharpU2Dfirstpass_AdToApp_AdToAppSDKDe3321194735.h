﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// AdToApp.AdToAppSDKDelegate
struct AdToAppSDKDelegate_t2853395885;

#include "AssemblyU2DCSharpU2Dfirstpass_AdToApp_AndroidWrappe382012998.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdToApp.AdToAppSDKDelegate/AndroidInterstitialListener
struct  AndroidInterstitialListener_t3321194735  : public ATAInterstitialAdListener_t382012998
{
public:
	// AdToApp.AdToAppSDKDelegate AdToApp.AdToAppSDKDelegate/AndroidInterstitialListener::_sdkDelegate
	AdToAppSDKDelegate_t2853395885 * ____sdkDelegate_0;

public:
	inline static int32_t get_offset_of__sdkDelegate_0() { return static_cast<int32_t>(offsetof(AndroidInterstitialListener_t3321194735, ____sdkDelegate_0)); }
	inline AdToAppSDKDelegate_t2853395885 * get__sdkDelegate_0() const { return ____sdkDelegate_0; }
	inline AdToAppSDKDelegate_t2853395885 ** get_address_of__sdkDelegate_0() { return &____sdkDelegate_0; }
	inline void set__sdkDelegate_0(AdToAppSDKDelegate_t2853395885 * value)
	{
		____sdkDelegate_0 = value;
		Il2CppCodeGenWriteBarrier(&____sdkDelegate_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
