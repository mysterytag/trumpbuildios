﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void BarygaOpenIABManager/GooglePlayPurchaseGuard::.cctor()
extern "C"  void GooglePlayPurchaseGuard__cctor_m3394018919 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BarygaOpenIABManager/GooglePlayPurchaseGuard::Verify(System.String,System.String)
extern "C"  bool GooglePlayPurchaseGuard_Verify_m79220659 (Il2CppObject * __this /* static, unused */, String_t* ___purchaseJson0, String_t* ___base64Signature1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
