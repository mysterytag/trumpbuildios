﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Zenject.CompositionRoot
struct CompositionRoot_t1939928321;

#include "AssemblyU2DCSharp_Zenject_Installer3915807453.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.StandardUnityInstaller
struct  StandardUnityInstaller_t3603054405  : public Installer_t3915807453
{
public:
	// Zenject.CompositionRoot Zenject.StandardUnityInstaller::_root
	CompositionRoot_t1939928321 * ____root_1;

public:
	inline static int32_t get_offset_of__root_1() { return static_cast<int32_t>(offsetof(StandardUnityInstaller_t3603054405, ____root_1)); }
	inline CompositionRoot_t1939928321 * get__root_1() const { return ____root_1; }
	inline CompositionRoot_t1939928321 ** get_address_of__root_1() { return &____root_1; }
	inline void set__root_1(CompositionRoot_t1939928321 * value)
	{
		____root_1 = value;
		Il2CppCodeGenWriteBarrier(&____root_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
