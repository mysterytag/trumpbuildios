﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ZipLatestObservable`7/ZipLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct ZipLatest_t4230324089;
// UniRx.Operators.ZipLatestObservable`7<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct ZipLatestObservable_7_t932604128;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.ZipLatestObservable`7/ZipLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.Int32,UniRx.Operators.ZipLatestObservable`7<T1,T2,T3,T4,T5,T6,TR>,UniRx.IObserver`1<TR>,System.IDisposable)
extern "C"  void ZipLatest__ctor_m2644855870_gshared (ZipLatest_t4230324089 * __this, int32_t ___length0, ZipLatestObservable_7_t932604128 * ___parent1, Il2CppObject* ___observer2, Il2CppObject * ___cancel3, const MethodInfo* method);
#define ZipLatest__ctor_m2644855870(__this, ___length0, ___parent1, ___observer2, ___cancel3, method) ((  void (*) (ZipLatest_t4230324089 *, int32_t, ZipLatestObservable_7_t932604128 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))ZipLatest__ctor_m2644855870_gshared)(__this, ___length0, ___parent1, ___observer2, ___cancel3, method)
// System.IDisposable UniRx.Operators.ZipLatestObservable`7/ZipLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Run()
extern "C"  Il2CppObject * ZipLatest_Run_m963794225_gshared (ZipLatest_t4230324089 * __this, const MethodInfo* method);
#define ZipLatest_Run_m963794225(__this, method) ((  Il2CppObject * (*) (ZipLatest_t4230324089 *, const MethodInfo*))ZipLatest_Run_m963794225_gshared)(__this, method)
// TR UniRx.Operators.ZipLatestObservable`7/ZipLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::GetResult()
extern "C"  Il2CppObject * ZipLatest_GetResult_m3506258701_gshared (ZipLatest_t4230324089 * __this, const MethodInfo* method);
#define ZipLatest_GetResult_m3506258701(__this, method) ((  Il2CppObject * (*) (ZipLatest_t4230324089 *, const MethodInfo*))ZipLatest_GetResult_m3506258701_gshared)(__this, method)
// System.Void UniRx.Operators.ZipLatestObservable`7/ZipLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::OnNext(TR)
extern "C"  void ZipLatest_OnNext_m2318934389_gshared (ZipLatest_t4230324089 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ZipLatest_OnNext_m2318934389(__this, ___value0, method) ((  void (*) (ZipLatest_t4230324089 *, Il2CppObject *, const MethodInfo*))ZipLatest_OnNext_m2318934389_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.ZipLatestObservable`7/ZipLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::OnError(System.Exception)
extern "C"  void ZipLatest_OnError_m2210343450_gshared (ZipLatest_t4230324089 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ZipLatest_OnError_m2210343450(__this, ___error0, method) ((  void (*) (ZipLatest_t4230324089 *, Exception_t1967233988 *, const MethodInfo*))ZipLatest_OnError_m2210343450_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.ZipLatestObservable`7/ZipLatest<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::OnCompleted()
extern "C"  void ZipLatest_OnCompleted_m213431405_gshared (ZipLatest_t4230324089 * __this, const MethodInfo* method);
#define ZipLatest_OnCompleted_m213431405(__this, method) ((  void (*) (ZipLatest_t4230324089 *, const MethodInfo*))ZipLatest_OnCompleted_m213431405_gshared)(__this, method)
