﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ObserveExtensions/<ObserveEveryValueChanged>c__AnonStorey91`2<System.Object,System.Object>
struct U3CObserveEveryValueChangedU3Ec__AnonStorey91_2_t1463036810;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560.h"

// System.Void UniRx.ObserveExtensions/<ObserveEveryValueChanged>c__AnonStorey91`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CObserveEveryValueChangedU3Ec__AnonStorey91_2__ctor_m3111246175_gshared (U3CObserveEveryValueChangedU3Ec__AnonStorey91_2_t1463036810 * __this, const MethodInfo* method);
#define U3CObserveEveryValueChangedU3Ec__AnonStorey91_2__ctor_m3111246175(__this, method) ((  void (*) (U3CObserveEveryValueChangedU3Ec__AnonStorey91_2_t1463036810 *, const MethodInfo*))U3CObserveEveryValueChangedU3Ec__AnonStorey91_2__ctor_m3111246175_gshared)(__this, method)
// System.Collections.IEnumerator UniRx.ObserveExtensions/<ObserveEveryValueChanged>c__AnonStorey91`2<System.Object,System.Object>::<>m__BD(UniRx.IObserver`1<TProperty>,UniRx.CancellationToken)
extern "C"  Il2CppObject * U3CObserveEveryValueChangedU3Ec__AnonStorey91_2_U3CU3Em__BD_m397551855_gshared (U3CObserveEveryValueChangedU3Ec__AnonStorey91_2_t1463036810 * __this, Il2CppObject* ___observer0, CancellationToken_t1439151560  ___cancellationToken1, const MethodInfo* method);
#define U3CObserveEveryValueChangedU3Ec__AnonStorey91_2_U3CU3Em__BD_m397551855(__this, ___observer0, ___cancellationToken1, method) ((  Il2CppObject * (*) (U3CObserveEveryValueChangedU3Ec__AnonStorey91_2_t1463036810 *, Il2CppObject*, CancellationToken_t1439151560 , const MethodInfo*))U3CObserveEveryValueChangedU3Ec__AnonStorey91_2_U3CU3Em__BD_m397551855_gshared)(__this, ___observer0, ___cancellationToken1, method)
