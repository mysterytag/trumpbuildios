﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/LinkGameCenterAccountRequestCallback
struct LinkGameCenterAccountRequestCallback_t1941561864;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.LinkGameCenterAccountRequest
struct LinkGameCenterAccountRequest_t355908787;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkGameCent355908787.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/LinkGameCenterAccountRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void LinkGameCenterAccountRequestCallback__ctor_m1751246583 (LinkGameCenterAccountRequestCallback_t1941561864 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LinkGameCenterAccountRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.LinkGameCenterAccountRequest,System.Object)
extern "C"  void LinkGameCenterAccountRequestCallback_Invoke_m1246454879 (LinkGameCenterAccountRequestCallback_t1941561864 * __this, String_t* ___urlPath0, int32_t ___callId1, LinkGameCenterAccountRequest_t355908787 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_LinkGameCenterAccountRequestCallback_t1941561864(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, LinkGameCenterAccountRequest_t355908787 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/LinkGameCenterAccountRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.LinkGameCenterAccountRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LinkGameCenterAccountRequestCallback_BeginInvoke_m1372581878 (LinkGameCenterAccountRequestCallback_t1941561864 * __this, String_t* ___urlPath0, int32_t ___callId1, LinkGameCenterAccountRequest_t355908787 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LinkGameCenterAccountRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void LinkGameCenterAccountRequestCallback_EndInvoke_m531983495 (LinkGameCenterAccountRequestCallback_t1941561864 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
