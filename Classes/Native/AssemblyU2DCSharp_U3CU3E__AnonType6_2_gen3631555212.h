﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <>__AnonType6`2<System.Double,System.Int32>
struct  U3CU3E__AnonType6_2_t3631555212  : public Il2CppObject
{
public:
	// <money>__T <>__AnonType6`2::<money>
	double ___U3CmoneyU3E_0;
	// <level>__T <>__AnonType6`2::<level>
	int32_t ___U3ClevelU3E_1;

public:
	inline static int32_t get_offset_of_U3CmoneyU3E_0() { return static_cast<int32_t>(offsetof(U3CU3E__AnonType6_2_t3631555212, ___U3CmoneyU3E_0)); }
	inline double get_U3CmoneyU3E_0() const { return ___U3CmoneyU3E_0; }
	inline double* get_address_of_U3CmoneyU3E_0() { return &___U3CmoneyU3E_0; }
	inline void set_U3CmoneyU3E_0(double value)
	{
		___U3CmoneyU3E_0 = value;
	}

	inline static int32_t get_offset_of_U3ClevelU3E_1() { return static_cast<int32_t>(offsetof(U3CU3E__AnonType6_2_t3631555212, ___U3ClevelU3E_1)); }
	inline int32_t get_U3ClevelU3E_1() const { return ___U3ClevelU3E_1; }
	inline int32_t* get_address_of_U3ClevelU3E_1() { return &___U3ClevelU3E_1; }
	inline void set_U3ClevelU3E_1(int32_t value)
	{
		___U3ClevelU3E_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
