﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23312956448MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,OnePF.Purchase>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m393269856(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1286424775 *, String_t*, Purchase_t160195573 *, const MethodInfo*))KeyValuePair_2__ctor_m4168265535_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,OnePF.Purchase>::get_Key()
#define KeyValuePair_2_get_Key_m4008284001(__this, method) ((  String_t* (*) (KeyValuePair_2_t1286424775 *, const MethodInfo*))KeyValuePair_2_get_Key_m3256475977_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,OnePF.Purchase>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m721193929(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1286424775 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m1278074762_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,OnePF.Purchase>::get_Value()
#define KeyValuePair_2_get_Value_m1591893074(__this, method) ((  Purchase_t160195573 * (*) (KeyValuePair_2_t1286424775 *, const MethodInfo*))KeyValuePair_2_get_Value_m3899079597_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,OnePF.Purchase>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m4079670089(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1286424775 *, Purchase_t160195573 *, const MethodInfo*))KeyValuePair_2_set_Value_m2954518154_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,OnePF.Purchase>::ToString()
#define KeyValuePair_2_ToString_m4222434809(__this, method) ((  String_t* (*) (KeyValuePair_2_t1286424775 *, const MethodInfo*))KeyValuePair_2_ToString_m1313859518_gshared)(__this, method)
