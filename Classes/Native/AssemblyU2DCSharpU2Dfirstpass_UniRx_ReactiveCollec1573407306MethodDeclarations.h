﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ReactiveCollection`1<UniRx.Tuple`2<System.Object,System.Object>>
struct ReactiveCollection_1_t1573407306;
// System.Collections.Generic.IEnumerable`1<UniRx.Tuple`2<System.Object,System.Object>>
struct IEnumerable_1_t3241416175;
// System.Collections.Generic.List`1<UniRx.Tuple`2<System.Object,System.Object>>
struct List_1_t1166220788;
// UniRx.IObservable`1<System.Int32>
struct IObservable_1_t2606213151;
// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;
// UniRx.IObservable`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct IObservable_1_t1707475750;
// UniRx.IObservable`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct IObservable_1_t2207387610;
// UniRx.IObservable`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct IObservable_1_t3828558783;
// UniRx.IObservable`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct IObservable_1_t1132704299;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen369261819.h"

// System.Void UniRx.ReactiveCollection`1<UniRx.Tuple`2<System.Object,System.Object>>::.ctor()
extern "C"  void ReactiveCollection_1__ctor_m717548565_gshared (ReactiveCollection_1_t1573407306 * __this, const MethodInfo* method);
#define ReactiveCollection_1__ctor_m717548565(__this, method) ((  void (*) (ReactiveCollection_1_t1573407306 *, const MethodInfo*))ReactiveCollection_1__ctor_m717548565_gshared)(__this, method)
// System.Void UniRx.ReactiveCollection`1<UniRx.Tuple`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void ReactiveCollection_1__ctor_m4220134730_gshared (ReactiveCollection_1_t1573407306 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define ReactiveCollection_1__ctor_m4220134730(__this, ___collection0, method) ((  void (*) (ReactiveCollection_1_t1573407306 *, Il2CppObject*, const MethodInfo*))ReactiveCollection_1__ctor_m4220134730_gshared)(__this, ___collection0, method)
// System.Void UniRx.ReactiveCollection`1<UniRx.Tuple`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void ReactiveCollection_1__ctor_m1910574573_gshared (ReactiveCollection_1_t1573407306 * __this, List_1_t1166220788 * ___list0, const MethodInfo* method);
#define ReactiveCollection_1__ctor_m1910574573(__this, ___list0, method) ((  void (*) (ReactiveCollection_1_t1573407306 *, List_1_t1166220788 *, const MethodInfo*))ReactiveCollection_1__ctor_m1910574573_gshared)(__this, ___list0, method)
// System.Void UniRx.ReactiveCollection`1<UniRx.Tuple`2<System.Object,System.Object>>::ClearItems()
extern "C"  void ReactiveCollection_1_ClearItems_m2487296194_gshared (ReactiveCollection_1_t1573407306 * __this, const MethodInfo* method);
#define ReactiveCollection_1_ClearItems_m2487296194(__this, method) ((  void (*) (ReactiveCollection_1_t1573407306 *, const MethodInfo*))ReactiveCollection_1_ClearItems_m2487296194_gshared)(__this, method)
// System.Void UniRx.ReactiveCollection`1<UniRx.Tuple`2<System.Object,System.Object>>::InsertItem(System.Int32,T)
extern "C"  void ReactiveCollection_1_InsertItem_m4232958436_gshared (ReactiveCollection_1_t1573407306 * __this, int32_t ___index0, Tuple_2_t369261819  ___item1, const MethodInfo* method);
#define ReactiveCollection_1_InsertItem_m4232958436(__this, ___index0, ___item1, method) ((  void (*) (ReactiveCollection_1_t1573407306 *, int32_t, Tuple_2_t369261819 , const MethodInfo*))ReactiveCollection_1_InsertItem_m4232958436_gshared)(__this, ___index0, ___item1, method)
// System.Void UniRx.ReactiveCollection`1<UniRx.Tuple`2<System.Object,System.Object>>::Move(System.Int32,System.Int32)
extern "C"  void ReactiveCollection_1_Move_m697532678_gshared (ReactiveCollection_1_t1573407306 * __this, int32_t ___oldIndex0, int32_t ___newIndex1, const MethodInfo* method);
#define ReactiveCollection_1_Move_m697532678(__this, ___oldIndex0, ___newIndex1, method) ((  void (*) (ReactiveCollection_1_t1573407306 *, int32_t, int32_t, const MethodInfo*))ReactiveCollection_1_Move_m697532678_gshared)(__this, ___oldIndex0, ___newIndex1, method)
// System.Void UniRx.ReactiveCollection`1<UniRx.Tuple`2<System.Object,System.Object>>::MoveItem(System.Int32,System.Int32)
extern "C"  void ReactiveCollection_1_MoveItem_m1629479283_gshared (ReactiveCollection_1_t1573407306 * __this, int32_t ___oldIndex0, int32_t ___newIndex1, const MethodInfo* method);
#define ReactiveCollection_1_MoveItem_m1629479283(__this, ___oldIndex0, ___newIndex1, method) ((  void (*) (ReactiveCollection_1_t1573407306 *, int32_t, int32_t, const MethodInfo*))ReactiveCollection_1_MoveItem_m1629479283_gshared)(__this, ___oldIndex0, ___newIndex1, method)
// System.Void UniRx.ReactiveCollection`1<UniRx.Tuple`2<System.Object,System.Object>>::RemoveItem(System.Int32)
extern "C"  void ReactiveCollection_1_RemoveItem_m3181041751_gshared (ReactiveCollection_1_t1573407306 * __this, int32_t ___index0, const MethodInfo* method);
#define ReactiveCollection_1_RemoveItem_m3181041751(__this, ___index0, method) ((  void (*) (ReactiveCollection_1_t1573407306 *, int32_t, const MethodInfo*))ReactiveCollection_1_RemoveItem_m3181041751_gshared)(__this, ___index0, method)
// System.Void UniRx.ReactiveCollection`1<UniRx.Tuple`2<System.Object,System.Object>>::SetItem(System.Int32,T)
extern "C"  void ReactiveCollection_1_SetItem_m3920097969_gshared (ReactiveCollection_1_t1573407306 * __this, int32_t ___index0, Tuple_2_t369261819  ___item1, const MethodInfo* method);
#define ReactiveCollection_1_SetItem_m3920097969(__this, ___index0, ___item1, method) ((  void (*) (ReactiveCollection_1_t1573407306 *, int32_t, Tuple_2_t369261819 , const MethodInfo*))ReactiveCollection_1_SetItem_m3920097969_gshared)(__this, ___index0, ___item1, method)
// UniRx.IObservable`1<System.Int32> UniRx.ReactiveCollection`1<UniRx.Tuple`2<System.Object,System.Object>>::ObserveCountChanged(System.Boolean)
extern "C"  Il2CppObject* ReactiveCollection_1_ObserveCountChanged_m4252337840_gshared (ReactiveCollection_1_t1573407306 * __this, bool ___notifyCurrentCount0, const MethodInfo* method);
#define ReactiveCollection_1_ObserveCountChanged_m4252337840(__this, ___notifyCurrentCount0, method) ((  Il2CppObject* (*) (ReactiveCollection_1_t1573407306 *, bool, const MethodInfo*))ReactiveCollection_1_ObserveCountChanged_m4252337840_gshared)(__this, ___notifyCurrentCount0, method)
// UniRx.IObservable`1<UniRx.Unit> UniRx.ReactiveCollection`1<UniRx.Tuple`2<System.Object,System.Object>>::ObserveReset()
extern "C"  Il2CppObject* ReactiveCollection_1_ObserveReset_m1369659178_gshared (ReactiveCollection_1_t1573407306 * __this, const MethodInfo* method);
#define ReactiveCollection_1_ObserveReset_m1369659178(__this, method) ((  Il2CppObject* (*) (ReactiveCollection_1_t1573407306 *, const MethodInfo*))ReactiveCollection_1_ObserveReset_m1369659178_gshared)(__this, method)
// UniRx.IObservable`1<UniRx.CollectionAddEvent`1<T>> UniRx.ReactiveCollection`1<UniRx.Tuple`2<System.Object,System.Object>>::ObserveAdd()
extern "C"  Il2CppObject* ReactiveCollection_1_ObserveAdd_m3081882730_gshared (ReactiveCollection_1_t1573407306 * __this, const MethodInfo* method);
#define ReactiveCollection_1_ObserveAdd_m3081882730(__this, method) ((  Il2CppObject* (*) (ReactiveCollection_1_t1573407306 *, const MethodInfo*))ReactiveCollection_1_ObserveAdd_m3081882730_gshared)(__this, method)
// UniRx.IObservable`1<UniRx.CollectionMoveEvent`1<T>> UniRx.ReactiveCollection`1<UniRx.Tuple`2<System.Object,System.Object>>::ObserveMove()
extern "C"  Il2CppObject* ReactiveCollection_1_ObserveMove_m3790976434_gshared (ReactiveCollection_1_t1573407306 * __this, const MethodInfo* method);
#define ReactiveCollection_1_ObserveMove_m3790976434(__this, method) ((  Il2CppObject* (*) (ReactiveCollection_1_t1573407306 *, const MethodInfo*))ReactiveCollection_1_ObserveMove_m3790976434_gshared)(__this, method)
// UniRx.IObservable`1<UniRx.CollectionRemoveEvent`1<T>> UniRx.ReactiveCollection`1<UniRx.Tuple`2<System.Object,System.Object>>::ObserveRemove()
extern "C"  Il2CppObject* ReactiveCollection_1_ObserveRemove_m3547014418_gshared (ReactiveCollection_1_t1573407306 * __this, const MethodInfo* method);
#define ReactiveCollection_1_ObserveRemove_m3547014418(__this, method) ((  Il2CppObject* (*) (ReactiveCollection_1_t1573407306 *, const MethodInfo*))ReactiveCollection_1_ObserveRemove_m3547014418_gshared)(__this, method)
// UniRx.IObservable`1<UniRx.CollectionReplaceEvent`1<T>> UniRx.ReactiveCollection`1<UniRx.Tuple`2<System.Object,System.Object>>::ObserveReplace()
extern "C"  Il2CppObject* ReactiveCollection_1_ObserveReplace_m504154192_gshared (ReactiveCollection_1_t1573407306 * __this, const MethodInfo* method);
#define ReactiveCollection_1_ObserveReplace_m504154192(__this, method) ((  Il2CppObject* (*) (ReactiveCollection_1_t1573407306 *, const MethodInfo*))ReactiveCollection_1_ObserveReplace_m504154192_gshared)(__this, method)
// System.Void UniRx.ReactiveCollection`1<UniRx.Tuple`2<System.Object,System.Object>>::Dispose()
extern "C"  void ReactiveCollection_1_Dispose_m2267686674_gshared (ReactiveCollection_1_t1573407306 * __this, const MethodInfo* method);
#define ReactiveCollection_1_Dispose_m2267686674(__this, method) ((  void (*) (ReactiveCollection_1_t1573407306 *, const MethodInfo*))ReactiveCollection_1_Dispose_m2267686674_gshared)(__this, method)
// System.Int32 UniRx.ReactiveCollection`1<UniRx.Tuple`2<System.Object,System.Object>>::<ObserveCountChanged>m__C3()
extern "C"  int32_t ReactiveCollection_1_U3CObserveCountChangedU3Em__C3_m2530644681_gshared (ReactiveCollection_1_t1573407306 * __this, const MethodInfo* method);
#define ReactiveCollection_1_U3CObserveCountChangedU3Em__C3_m2530644681(__this, method) ((  int32_t (*) (ReactiveCollection_1_t1573407306 *, const MethodInfo*))ReactiveCollection_1_U3CObserveCountChangedU3Em__C3_m2530644681_gshared)(__this, method)
