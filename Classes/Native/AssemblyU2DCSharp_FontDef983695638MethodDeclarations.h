﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FontDef
struct FontDef_t983695638;

#include "codegen/il2cpp-codegen.h"

// System.Void FontDef::.ctor()
extern "C"  void FontDef__ctor_m2508719893 (FontDef_t983695638 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
