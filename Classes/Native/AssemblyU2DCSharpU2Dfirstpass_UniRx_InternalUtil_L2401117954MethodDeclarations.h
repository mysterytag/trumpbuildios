﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.ListObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>
struct ListObserver_1_t2401117954;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>>
struct ImmutableList_1_t1083736389;
// System.Exception
struct Exception_t1967233988;
// UniRx.IObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>
struct IObserver_1_t23531890;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen2106500283.h"

// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m830657531_gshared (ListObserver_1_t2401117954 * __this, ImmutableList_1_t1083736389 * ___observers0, const MethodInfo* method);
#define ListObserver_1__ctor_m830657531(__this, ___observers0, method) ((  void (*) (ListObserver_1_t2401117954 *, ImmutableList_1_t1083736389 *, const MethodInfo*))ListObserver_1__ctor_m830657531_gshared)(__this, ___observers0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m3639853811_gshared (ListObserver_1_t2401117954 * __this, const MethodInfo* method);
#define ListObserver_1_OnCompleted_m3639853811(__this, method) ((  void (*) (ListObserver_1_t2401117954 *, const MethodInfo*))ListObserver_1_OnCompleted_m3639853811_gshared)(__this, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m3530203040_gshared (ListObserver_1_t2401117954 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ListObserver_1_OnError_m3530203040(__this, ___error0, method) ((  void (*) (ListObserver_1_t2401117954 *, Exception_t1967233988 *, const MethodInfo*))ListObserver_1_OnError_m3530203040_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m2872821393_gshared (ListObserver_1_t2401117954 * __this, Tuple_2_t2106500283  ___value0, const MethodInfo* method);
#define ListObserver_1_OnNext_m2872821393(__this, ___value0, method) ((  void (*) (ListObserver_1_t2401117954 *, Tuple_2_t2106500283 , const MethodInfo*))ListObserver_1_OnNext_m2872821393_gshared)(__this, ___value0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m3001986659_gshared (ListObserver_1_t2401117954 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Add_m3001986659(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t2401117954 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Add_m3001986659_gshared)(__this, ___observer0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m2808966664_gshared (ListObserver_1_t2401117954 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Remove_m2808966664(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t2401117954 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Remove_m2808966664_gshared)(__this, ___observer0, method)
