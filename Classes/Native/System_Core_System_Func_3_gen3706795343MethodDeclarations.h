﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Func`3<System.Int64,UniRx.Unit,System.Int64>
struct Func_3_t3706795343;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Func`3<System.Int64,UniRx.Unit,System.Int64>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_3__ctor_m33277251_gshared (Func_3_t3706795343 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Func_3__ctor_m33277251(__this, ___object0, ___method1, method) ((  void (*) (Func_3_t3706795343 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_3__ctor_m33277251_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`3<System.Int64,UniRx.Unit,System.Int64>::Invoke(T1,T2)
extern "C"  int64_t Func_3_Invoke_m2334658926_gshared (Func_3_t3706795343 * __this, int64_t ___arg10, Unit_t2558286038  ___arg21, const MethodInfo* method);
#define Func_3_Invoke_m2334658926(__this, ___arg10, ___arg21, method) ((  int64_t (*) (Func_3_t3706795343 *, int64_t, Unit_t2558286038 , const MethodInfo*))Func_3_Invoke_m2334658926_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Func`3<System.Int64,UniRx.Unit,System.Int64>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_3_BeginInvoke_m939526131_gshared (Func_3_t3706795343 * __this, int64_t ___arg10, Unit_t2558286038  ___arg21, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Func_3_BeginInvoke_m939526131(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Func_3_t3706795343 *, int64_t, Unit_t2558286038 , AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_3_BeginInvoke_m939526131_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// TResult System.Func`3<System.Int64,UniRx.Unit,System.Int64>::EndInvoke(System.IAsyncResult)
extern "C"  int64_t Func_3_EndInvoke_m2301153069_gshared (Func_3_t3706795343 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Func_3_EndInvoke_m2301153069(__this, ___result0, method) ((  int64_t (*) (Func_3_t3706795343 *, Il2CppObject *, const MethodInfo*))Func_3_EndInvoke_m2301153069_gshared)(__this, ___result0, method)
