﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnePF.OpenIAB_WP8
struct  OpenIAB_WP8_t446638612  : public Il2CppObject
{
public:

public:
};

struct OpenIAB_WP8_t446638612_StaticFields
{
public:
	// System.String OnePF.OpenIAB_WP8::STORE
	String_t* ___STORE_0;

public:
	inline static int32_t get_offset_of_STORE_0() { return static_cast<int32_t>(offsetof(OpenIAB_WP8_t446638612_StaticFields, ___STORE_0)); }
	inline String_t* get_STORE_0() const { return ___STORE_0; }
	inline String_t** get_address_of_STORE_0() { return &___STORE_0; }
	inline void set_STORE_0(String_t* value)
	{
		___STORE_0 = value;
		Il2CppCodeGenWriteBarrier(&___STORE_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
