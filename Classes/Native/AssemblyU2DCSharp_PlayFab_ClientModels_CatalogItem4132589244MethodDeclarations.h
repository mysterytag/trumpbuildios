﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.CatalogItem
struct CatalogItem_t4132589244;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.UInt32>
struct Dictionary_2_t2623623230;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;
// PlayFab.ClientModels.CatalogItemConsumableInfo
struct CatalogItemConsumableInfo_t2956282765;
// PlayFab.ClientModels.CatalogItemContainerInfo
struct CatalogItemContainerInfo_t284142995;
// PlayFab.ClientModels.CatalogItemBundleInfo
struct CatalogItemBundleInfo_t1005829164;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_CatalogItem2956282765.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_CatalogItemC284142995.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_CatalogItem1005829164.h"

// System.Void PlayFab.ClientModels.CatalogItem::.ctor()
extern "C"  void CatalogItem__ctor_m2174105453 (CatalogItem_t4132589244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.CatalogItem::get_ItemId()
extern "C"  String_t* CatalogItem_get_ItemId_m3475438449 (CatalogItem_t4132589244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CatalogItem::set_ItemId(System.String)
extern "C"  void CatalogItem_set_ItemId_m3045149504 (CatalogItem_t4132589244 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.CatalogItem::get_ItemClass()
extern "C"  String_t* CatalogItem_get_ItemClass_m1559132228 (CatalogItem_t4132589244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CatalogItem::set_ItemClass(System.String)
extern "C"  void CatalogItem_set_ItemClass_m2644557519 (CatalogItem_t4132589244 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.CatalogItem::get_CatalogVersion()
extern "C"  String_t* CatalogItem_get_CatalogVersion_m3810864162 (CatalogItem_t4132589244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CatalogItem::set_CatalogVersion(System.String)
extern "C"  void CatalogItem_set_CatalogVersion_m2233729839 (CatalogItem_t4132589244 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.CatalogItem::get_DisplayName()
extern "C"  String_t* CatalogItem_get_DisplayName_m3711699724 (CatalogItem_t4132589244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CatalogItem::set_DisplayName(System.String)
extern "C"  void CatalogItem_set_DisplayName_m3097088199 (CatalogItem_t4132589244 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.CatalogItem::get_Description()
extern "C"  String_t* CatalogItem_get_Description_m1955631323 (CatalogItem_t4132589244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CatalogItem::set_Description(System.String)
extern "C"  void CatalogItem_set_Description_m2245751640 (CatalogItem_t4132589244 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.UInt32> PlayFab.ClientModels.CatalogItem::get_VirtualCurrencyPrices()
extern "C"  Dictionary_2_t2623623230 * CatalogItem_get_VirtualCurrencyPrices_m2816134290 (CatalogItem_t4132589244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CatalogItem::set_VirtualCurrencyPrices(System.Collections.Generic.Dictionary`2<System.String,System.UInt32>)
extern "C"  void CatalogItem_set_VirtualCurrencyPrices_m2865484557 (CatalogItem_t4132589244 * __this, Dictionary_2_t2623623230 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.UInt32> PlayFab.ClientModels.CatalogItem::get_RealCurrencyPrices()
extern "C"  Dictionary_2_t2623623230 * CatalogItem_get_RealCurrencyPrices_m3052650031 (CatalogItem_t4132589244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CatalogItem::set_RealCurrencyPrices(System.Collections.Generic.Dictionary`2<System.String,System.UInt32>)
extern "C"  void CatalogItem_set_RealCurrencyPrices_m1479368422 (CatalogItem_t4132589244 * __this, Dictionary_2_t2623623230 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.CatalogItem::get_Tags()
extern "C"  List_1_t1765447871 * CatalogItem_get_Tags_m762111114 (CatalogItem_t4132589244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CatalogItem::set_Tags(System.Collections.Generic.List`1<System.String>)
extern "C"  void CatalogItem_set_Tags_m3396385729 (CatalogItem_t4132589244 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.CatalogItem::get_CustomData()
extern "C"  String_t* CatalogItem_get_CustomData_m4003440222 (CatalogItem_t4132589244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CatalogItem::set_CustomData(System.String)
extern "C"  void CatalogItem_set_CustomData_m3866094067 (CatalogItem_t4132589244 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayFab.ClientModels.CatalogItemConsumableInfo PlayFab.ClientModels.CatalogItem::get_Consumable()
extern "C"  CatalogItemConsumableInfo_t2956282765 * CatalogItem_get_Consumable_m937351617 (CatalogItem_t4132589244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CatalogItem::set_Consumable(PlayFab.ClientModels.CatalogItemConsumableInfo)
extern "C"  void CatalogItem_set_Consumable_m2864583224 (CatalogItem_t4132589244 * __this, CatalogItemConsumableInfo_t2956282765 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayFab.ClientModels.CatalogItemContainerInfo PlayFab.ClientModels.CatalogItem::get_Container()
extern "C"  CatalogItemContainerInfo_t284142995 * CatalogItem_get_Container_m3277564209 (CatalogItem_t4132589244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CatalogItem::set_Container(PlayFab.ClientModels.CatalogItemContainerInfo)
extern "C"  void CatalogItem_set_Container_m2475862850 (CatalogItem_t4132589244 * __this, CatalogItemContainerInfo_t284142995 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayFab.ClientModels.CatalogItemBundleInfo PlayFab.ClientModels.CatalogItem::get_Bundle()
extern "C"  CatalogItemBundleInfo_t1005829164 * CatalogItem_get_Bundle_m4190914977 (CatalogItem_t4132589244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CatalogItem::set_Bundle(PlayFab.ClientModels.CatalogItemBundleInfo)
extern "C"  void CatalogItem_set_Bundle_m1182022808 (CatalogItem_t4132589244 * __this, CatalogItemBundleInfo_t1005829164 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayFab.ClientModels.CatalogItem::get_CanBecomeCharacter()
extern "C"  bool CatalogItem_get_CanBecomeCharacter_m3742623846 (CatalogItem_t4132589244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CatalogItem::set_CanBecomeCharacter(System.Boolean)
extern "C"  void CatalogItem_set_CanBecomeCharacter_m1912467485 (CatalogItem_t4132589244 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayFab.ClientModels.CatalogItem::get_IsStackable()
extern "C"  bool CatalogItem_get_IsStackable_m1538119558 (CatalogItem_t4132589244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CatalogItem::set_IsStackable(System.Boolean)
extern "C"  void CatalogItem_set_IsStackable_m1760282045 (CatalogItem_t4132589244 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayFab.ClientModels.CatalogItem::get_IsTradable()
extern "C"  bool CatalogItem_get_IsTradable_m191111865 (CatalogItem_t4132589244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CatalogItem::set_IsTradable(System.Boolean)
extern "C"  void CatalogItem_set_IsTradable_m984407344 (CatalogItem_t4132589244 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.CatalogItem::get_ItemImageUrl()
extern "C"  String_t* CatalogItem_get_ItemImageUrl_m1223217770 (CatalogItem_t4132589244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.CatalogItem::set_ItemImageUrl(System.String)
extern "C"  void CatalogItem_set_ItemImageUrl_m2020329895 (CatalogItem_t4132589244 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
