﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SampleFrameObservable`1<System.Object>
struct SampleFrameObservable_1_t1335449437;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_FrameCountType167759182.h"

// System.Void UniRx.Operators.SampleFrameObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Int32,UniRx.FrameCountType)
extern "C"  void SampleFrameObservable_1__ctor_m1945853965_gshared (SampleFrameObservable_1_t1335449437 * __this, Il2CppObject* ___source0, int32_t ___frameCount1, int32_t ___frameCountType2, const MethodInfo* method);
#define SampleFrameObservable_1__ctor_m1945853965(__this, ___source0, ___frameCount1, ___frameCountType2, method) ((  void (*) (SampleFrameObservable_1_t1335449437 *, Il2CppObject*, int32_t, int32_t, const MethodInfo*))SampleFrameObservable_1__ctor_m1945853965_gshared)(__this, ___source0, ___frameCount1, ___frameCountType2, method)
// System.IDisposable UniRx.Operators.SampleFrameObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * SampleFrameObservable_1_SubscribeCore_m1245030859_gshared (SampleFrameObservable_1_t1335449437 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define SampleFrameObservable_1_SubscribeCore_m1245030859(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (SampleFrameObservable_1_t1335449437 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SampleFrameObservable_1_SubscribeCore_m1245030859_gshared)(__this, ___observer0, ___cancel1, method)
