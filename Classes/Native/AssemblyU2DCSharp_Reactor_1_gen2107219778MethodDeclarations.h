﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Reactor`1<System.Int64>
struct Reactor_1_t2107219778;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<System.Int64>
struct Action_1_t2995867587;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"

// System.Void Reactor`1<System.Int64>::.ctor()
extern "C"  void Reactor_1__ctor_m3572774116_gshared (Reactor_1_t2107219778 * __this, const MethodInfo* method);
#define Reactor_1__ctor_m3572774116(__this, method) ((  void (*) (Reactor_1_t2107219778 *, const MethodInfo*))Reactor_1__ctor_m3572774116_gshared)(__this, method)
// System.Void Reactor`1<System.Int64>::React(T)
extern "C"  void Reactor_1_React_m2918510013_gshared (Reactor_1_t2107219778 * __this, int64_t ___t0, const MethodInfo* method);
#define Reactor_1_React_m2918510013(__this, ___t0, method) ((  void (*) (Reactor_1_t2107219778 *, int64_t, const MethodInfo*))Reactor_1_React_m2918510013_gshared)(__this, ___t0, method)
// System.Void Reactor`1<System.Int64>::TransactionUnpackReact(T,System.Int32)
extern "C"  void Reactor_1_TransactionUnpackReact_m1211823880_gshared (Reactor_1_t2107219778 * __this, int64_t ___t0, int32_t ___i1, const MethodInfo* method);
#define Reactor_1_TransactionUnpackReact_m1211823880(__this, ___t0, ___i1, method) ((  void (*) (Reactor_1_t2107219778 *, int64_t, int32_t, const MethodInfo*))Reactor_1_TransactionUnpackReact_m1211823880_gshared)(__this, ___t0, ___i1, method)
// System.Boolean Reactor`1<System.Int64>::Empty()
extern "C"  bool Reactor_1_Empty_m984096451_gshared (Reactor_1_t2107219778 * __this, const MethodInfo* method);
#define Reactor_1_Empty_m984096451(__this, method) ((  bool (*) (Reactor_1_t2107219778 *, const MethodInfo*))Reactor_1_Empty_m984096451_gshared)(__this, method)
// System.IDisposable Reactor`1<System.Int64>::AddReaction(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * Reactor_1_AddReaction_m959058531_gshared (Reactor_1_t2107219778 * __this, Action_1_t2995867587 * ___reaction0, int32_t ___priority1, const MethodInfo* method);
#define Reactor_1_AddReaction_m959058531(__this, ___reaction0, ___priority1, method) ((  Il2CppObject * (*) (Reactor_1_t2107219778 *, Action_1_t2995867587 *, int32_t, const MethodInfo*))Reactor_1_AddReaction_m959058531_gshared)(__this, ___reaction0, ___priority1, method)
// System.Void Reactor`1<System.Int64>::UpgradeToMultyPriority()
extern "C"  void Reactor_1_UpgradeToMultyPriority_m4158857782_gshared (Reactor_1_t2107219778 * __this, const MethodInfo* method);
#define Reactor_1_UpgradeToMultyPriority_m4158857782(__this, method) ((  void (*) (Reactor_1_t2107219778 *, const MethodInfo*))Reactor_1_UpgradeToMultyPriority_m4158857782_gshared)(__this, method)
// System.Void Reactor`1<System.Int64>::AddToMultipriority(System.Action`1<T>,Priority)
extern "C"  void Reactor_1_AddToMultipriority_m3972978753_gshared (Reactor_1_t2107219778 * __this, Action_1_t2995867587 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define Reactor_1_AddToMultipriority_m3972978753(__this, ___reaction0, ___p1, method) ((  void (*) (Reactor_1_t2107219778 *, Action_1_t2995867587 *, int32_t, const MethodInfo*))Reactor_1_AddToMultipriority_m3972978753_gshared)(__this, ___reaction0, ___p1, method)
// System.Void Reactor`1<System.Int64>::RemoveReaction(System.Action`1<T>,Priority)
extern "C"  void Reactor_1_RemoveReaction_m586430901_gshared (Reactor_1_t2107219778 * __this, Action_1_t2995867587 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define Reactor_1_RemoveReaction_m586430901(__this, ___reaction0, ___p1, method) ((  void (*) (Reactor_1_t2107219778 *, Action_1_t2995867587 *, int32_t, const MethodInfo*))Reactor_1_RemoveReaction_m586430901_gshared)(__this, ___reaction0, ___p1, method)
// System.Void Reactor`1<System.Int64>::Clear()
extern "C"  void Reactor_1_Clear_m978907407_gshared (Reactor_1_t2107219778 * __this, const MethodInfo* method);
#define Reactor_1_Clear_m978907407(__this, method) ((  void (*) (Reactor_1_t2107219778 *, const MethodInfo*))Reactor_1_Clear_m978907407_gshared)(__this, method)
