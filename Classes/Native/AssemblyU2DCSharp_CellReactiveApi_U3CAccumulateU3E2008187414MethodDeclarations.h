﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/<Accumulate>c__AnonStoreyFB`2<System.Object,System.Object>
struct U3CAccumulateU3Ec__AnonStoreyFB_2_t2008187414;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"

// System.Void CellReactiveApi/<Accumulate>c__AnonStoreyFB`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CAccumulateU3Ec__AnonStoreyFB_2__ctor_m357651446_gshared (U3CAccumulateU3Ec__AnonStoreyFB_2_t2008187414 * __this, const MethodInfo* method);
#define U3CAccumulateU3Ec__AnonStoreyFB_2__ctor_m357651446(__this, method) ((  void (*) (U3CAccumulateU3Ec__AnonStoreyFB_2_t2008187414 *, const MethodInfo*))U3CAccumulateU3Ec__AnonStoreyFB_2__ctor_m357651446_gshared)(__this, method)
// System.IDisposable CellReactiveApi/<Accumulate>c__AnonStoreyFB`2<System.Object,System.Object>::<>m__171(System.Action`1<T>,Priority)
extern "C"  Il2CppObject * U3CAccumulateU3Ec__AnonStoreyFB_2_U3CU3Em__171_m1385176897_gshared (U3CAccumulateU3Ec__AnonStoreyFB_2_t2008187414 * __this, Action_1_t985559125 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define U3CAccumulateU3Ec__AnonStoreyFB_2_U3CU3Em__171_m1385176897(__this, ___reaction0, ___p1, method) ((  Il2CppObject * (*) (U3CAccumulateU3Ec__AnonStoreyFB_2_t2008187414 *, Action_1_t985559125 *, int32_t, const MethodInfo*))U3CAccumulateU3Ec__AnonStoreyFB_2_U3CU3Em__171_m1385176897_gshared)(__this, ___reaction0, ___p1, method)
// T CellReactiveApi/<Accumulate>c__AnonStoreyFB`2<System.Object,System.Object>::<>m__172()
extern "C"  Il2CppObject * U3CAccumulateU3Ec__AnonStoreyFB_2_U3CU3Em__172_m1414540942_gshared (U3CAccumulateU3Ec__AnonStoreyFB_2_t2008187414 * __this, const MethodInfo* method);
#define U3CAccumulateU3Ec__AnonStoreyFB_2_U3CU3Em__172_m1414540942(__this, method) ((  Il2CppObject * (*) (U3CAccumulateU3Ec__AnonStoreyFB_2_t2008187414 *, const MethodInfo*))U3CAccumulateU3Ec__AnonStoreyFB_2_U3CU3Em__172_m1414540942_gshared)(__this, method)
