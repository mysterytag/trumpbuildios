﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ReactiveUI/<SetFill>c__AnonStorey127
struct U3CSetFillU3Ec__AnonStorey127_t1032993156;

#include "codegen/il2cpp-codegen.h"

// System.Void ReactiveUI/<SetFill>c__AnonStorey127::.ctor()
extern "C"  void U3CSetFillU3Ec__AnonStorey127__ctor_m1393578873 (U3CSetFillU3Ec__AnonStorey127_t1032993156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReactiveUI/<SetFill>c__AnonStorey127::<>m__1B7(System.Single)
extern "C"  void U3CSetFillU3Ec__AnonStorey127_U3CU3Em__1B7_m671383205 (U3CSetFillU3Ec__AnonStorey127_t1032993156 * __this, float ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
