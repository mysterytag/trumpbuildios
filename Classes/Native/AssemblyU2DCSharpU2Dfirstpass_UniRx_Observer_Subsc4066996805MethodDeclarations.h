﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observer/Subscribe`1<System.Single>
struct Subscribe_1_t4066996805;
// System.Action`1<System.Single>
struct Action_1_t1106661726;
// System.Action`1<System.Exception>
struct Action_1_t2115686693;
// System.Action
struct Action_t437523947;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Observer/Subscribe`1<System.Single>::.ctor(System.Action`1<T>,System.Action`1<System.Exception>,System.Action)
extern "C"  void Subscribe_1__ctor_m1906112009_gshared (Subscribe_1_t4066996805 * __this, Action_1_t1106661726 * ___onNext0, Action_1_t2115686693 * ___onError1, Action_t437523947 * ___onCompleted2, const MethodInfo* method);
#define Subscribe_1__ctor_m1906112009(__this, ___onNext0, ___onError1, ___onCompleted2, method) ((  void (*) (Subscribe_1_t4066996805 *, Action_1_t1106661726 *, Action_1_t2115686693 *, Action_t437523947 *, const MethodInfo*))Subscribe_1__ctor_m1906112009_gshared)(__this, ___onNext0, ___onError1, ___onCompleted2, method)
// System.Void UniRx.Observer/Subscribe`1<System.Single>::OnNext(T)
extern "C"  void Subscribe_1_OnNext_m1874519270_gshared (Subscribe_1_t4066996805 * __this, float ___value0, const MethodInfo* method);
#define Subscribe_1_OnNext_m1874519270(__this, ___value0, method) ((  void (*) (Subscribe_1_t4066996805 *, float, const MethodInfo*))Subscribe_1_OnNext_m1874519270_gshared)(__this, ___value0, method)
// System.Void UniRx.Observer/Subscribe`1<System.Single>::OnError(System.Exception)
extern "C"  void Subscribe_1_OnError_m1767142389_gshared (Subscribe_1_t4066996805 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Subscribe_1_OnError_m1767142389(__this, ___error0, method) ((  void (*) (Subscribe_1_t4066996805 *, Exception_t1967233988 *, const MethodInfo*))Subscribe_1_OnError_m1767142389_gshared)(__this, ___error0, method)
// System.Void UniRx.Observer/Subscribe`1<System.Single>::OnCompleted()
extern "C"  void Subscribe_1_OnCompleted_m4049710792_gshared (Subscribe_1_t4066996805 * __this, const MethodInfo* method);
#define Subscribe_1_OnCompleted_m4049710792(__this, method) ((  void (*) (Subscribe_1_t4066996805 *, const MethodInfo*))Subscribe_1_OnCompleted_m4049710792_gshared)(__this, method)
