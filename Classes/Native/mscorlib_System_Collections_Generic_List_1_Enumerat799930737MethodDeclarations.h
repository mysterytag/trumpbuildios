﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4014815677MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<SubstanceBase`2/Intrusion<System.Boolean,System.Boolean>>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1156050966(__this, ___l0, method) ((  void (*) (Enumerator_t799930737 *, List_1_t2714147745 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SubstanceBase`2/Intrusion<System.Boolean,System.Boolean>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m235597436(__this, method) ((  void (*) (Enumerator_t799930737 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<SubstanceBase`2/Intrusion<System.Boolean,System.Boolean>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1346327410(__this, method) ((  Il2CppObject * (*) (Enumerator_t799930737 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SubstanceBase`2/Intrusion<System.Boolean,System.Boolean>>::Dispose()
#define Enumerator_Dispose_m3659096955(__this, method) ((  void (*) (Enumerator_t799930737 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SubstanceBase`2/Intrusion<System.Boolean,System.Boolean>>::VerifyState()
#define Enumerator_VerifyState_m145288500(__this, method) ((  void (*) (Enumerator_t799930737 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<SubstanceBase`2/Intrusion<System.Boolean,System.Boolean>>::MoveNext()
#define Enumerator_MoveNext_m3919966252(__this, method) ((  bool (*) (Enumerator_t799930737 *, const MethodInfo*))Enumerator_MoveNext_m302222300_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<SubstanceBase`2/Intrusion<System.Boolean,System.Boolean>>::get_Current()
#define Enumerator_get_Current_m1259252685(__this, method) ((  Intrusion_t1917188776 * (*) (Enumerator_t799930737 *, const MethodInfo*))Enumerator_get_Current_m1113061044_gshared)(__this, method)
