﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CellUtils.SingleAssignmentDisposable
struct SingleAssignmentDisposable_t1832432170;
// CellReactiveApi/<WhenOnce>c__AnonStorey107`1<System.Object>
struct U3CWhenOnceU3Ec__AnonStorey107_1_t2731099226;
// CellReactiveApi/<WhenOnce>c__AnonStorey107`1/<WhenOnce>c__AnonStorey108`1<System.Object>
struct U3CWhenOnceU3Ec__AnonStorey108_1_t2286074227;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CellReactiveApi/<WhenOnce>c__AnonStorey107`1/<WhenOnce>c__AnonStorey109`1<System.Object>
struct  U3CWhenOnceU3Ec__AnonStorey109_1_t1841049228  : public Il2CppObject
{
public:
	// CellUtils.SingleAssignmentDisposable CellReactiveApi/<WhenOnce>c__AnonStorey107`1/<WhenOnce>c__AnonStorey109`1::disp
	SingleAssignmentDisposable_t1832432170 * ___disp_0;
	// CellReactiveApi/<WhenOnce>c__AnonStorey107`1<T> CellReactiveApi/<WhenOnce>c__AnonStorey107`1/<WhenOnce>c__AnonStorey109`1::<>f__ref$263
	U3CWhenOnceU3Ec__AnonStorey107_1_t2731099226 * ___U3CU3Ef__refU24263_1;
	// CellReactiveApi/<WhenOnce>c__AnonStorey107`1/<WhenOnce>c__AnonStorey108`1<T> CellReactiveApi/<WhenOnce>c__AnonStorey107`1/<WhenOnce>c__AnonStorey109`1::<>f__ref$264
	U3CWhenOnceU3Ec__AnonStorey108_1_t2286074227 * ___U3CU3Ef__refU24264_2;

public:
	inline static int32_t get_offset_of_disp_0() { return static_cast<int32_t>(offsetof(U3CWhenOnceU3Ec__AnonStorey109_1_t1841049228, ___disp_0)); }
	inline SingleAssignmentDisposable_t1832432170 * get_disp_0() const { return ___disp_0; }
	inline SingleAssignmentDisposable_t1832432170 ** get_address_of_disp_0() { return &___disp_0; }
	inline void set_disp_0(SingleAssignmentDisposable_t1832432170 * value)
	{
		___disp_0 = value;
		Il2CppCodeGenWriteBarrier(&___disp_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24263_1() { return static_cast<int32_t>(offsetof(U3CWhenOnceU3Ec__AnonStorey109_1_t1841049228, ___U3CU3Ef__refU24263_1)); }
	inline U3CWhenOnceU3Ec__AnonStorey107_1_t2731099226 * get_U3CU3Ef__refU24263_1() const { return ___U3CU3Ef__refU24263_1; }
	inline U3CWhenOnceU3Ec__AnonStorey107_1_t2731099226 ** get_address_of_U3CU3Ef__refU24263_1() { return &___U3CU3Ef__refU24263_1; }
	inline void set_U3CU3Ef__refU24263_1(U3CWhenOnceU3Ec__AnonStorey107_1_t2731099226 * value)
	{
		___U3CU3Ef__refU24263_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24263_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24264_2() { return static_cast<int32_t>(offsetof(U3CWhenOnceU3Ec__AnonStorey109_1_t1841049228, ___U3CU3Ef__refU24264_2)); }
	inline U3CWhenOnceU3Ec__AnonStorey108_1_t2286074227 * get_U3CU3Ef__refU24264_2() const { return ___U3CU3Ef__refU24264_2; }
	inline U3CWhenOnceU3Ec__AnonStorey108_1_t2286074227 ** get_address_of_U3CU3Ef__refU24264_2() { return &___U3CU3Ef__refU24264_2; }
	inline void set_U3CU3Ef__refU24264_2(U3CWhenOnceU3Ec__AnonStorey108_1_t2286074227 * value)
	{
		___U3CU3Ef__refU24264_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24264_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
