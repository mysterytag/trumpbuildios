﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.UnlinkAndroidDeviceIDRequest
struct UnlinkAndroidDeviceIDRequest_t1293232914;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.UnlinkAndroidDeviceIDRequest::.ctor()
extern "C"  void UnlinkAndroidDeviceIDRequest__ctor_m3733645931 (UnlinkAndroidDeviceIDRequest_t1293232914 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.UnlinkAndroidDeviceIDRequest::get_AndroidDeviceId()
extern "C"  String_t* UnlinkAndroidDeviceIDRequest_get_AndroidDeviceId_m1098042787 (UnlinkAndroidDeviceIDRequest_t1293232914 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UnlinkAndroidDeviceIDRequest::set_AndroidDeviceId(System.String)
extern "C"  void UnlinkAndroidDeviceIDRequest_set_AndroidDeviceId_m3113869814 (UnlinkAndroidDeviceIDRequest_t1293232914 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
