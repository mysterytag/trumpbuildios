﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<StartGame>c__AnonStoreyB7
struct U3CStartGameU3Ec__AnonStoreyB7_t2490831510;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<StartGame>c__AnonStoreyB7::.ctor()
extern "C"  void U3CStartGameU3Ec__AnonStoreyB7__ctor_m496267645 (U3CStartGameU3Ec__AnonStoreyB7_t2490831510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<StartGame>c__AnonStoreyB7::<>m__A4(PlayFab.CallRequestContainer)
extern "C"  void U3CStartGameU3Ec__AnonStoreyB7_U3CU3Em__A4_m23489646 (U3CStartGameU3Ec__AnonStoreyB7_t2490831510 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
