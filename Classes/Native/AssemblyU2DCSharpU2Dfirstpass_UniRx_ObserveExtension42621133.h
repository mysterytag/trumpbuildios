﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.IEqualityComparer`1<System.Double>
struct IEqualityComparer_1_t2858783265;
// UnityEngine.YieldInstruction
struct YieldInstruction_t3557331758;
struct YieldInstruction_t3557331758_marshaled_pinvoke;
// UnityEngine.Object
struct Object_t3878351788;
struct Object_t3878351788_marshaled_pinvoke;
// System.Object
struct Il2CppObject;
// System.Func`2<System.Object,System.Double>
struct Func_2_t1833193546;
// UniRx.IObserver`1<System.Double>
struct IObserver_1_t2746515517;
// System.Exception
struct Exception_t1967233988;

#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_FrameCountType167759182.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CancellationTo1439151560.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ObserveExtensions/<PublishUnityObjectValueChanged>c__Iterator25`2<System.Object,System.Double>
struct  U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t42621133  : public Il2CppObject
{
public:
	// System.Collections.Generic.IEqualityComparer`1<TProperty> UniRx.ObserveExtensions/<PublishUnityObjectValueChanged>c__Iterator25`2::<comparer>__0
	Il2CppObject* ___U3CcomparerU3E__0_0;
	// System.Boolean UniRx.ObserveExtensions/<PublishUnityObjectValueChanged>c__Iterator25`2::<isFirst>__1
	bool ___U3CisFirstU3E__1_1;
	// TProperty UniRx.ObserveExtensions/<PublishUnityObjectValueChanged>c__Iterator25`2::<currentValue>__2
	double ___U3CcurrentValueU3E__2_2;
	// TProperty UniRx.ObserveExtensions/<PublishUnityObjectValueChanged>c__Iterator25`2::<prevValue>__3
	double ___U3CprevValueU3E__3_3;
	// UniRx.FrameCountType UniRx.ObserveExtensions/<PublishUnityObjectValueChanged>c__Iterator25`2::frameCountType
	int32_t ___frameCountType_4;
	// UnityEngine.YieldInstruction UniRx.ObserveExtensions/<PublishUnityObjectValueChanged>c__Iterator25`2::<yieldInstruction>__4
	YieldInstruction_t3557331758 * ___U3CyieldInstructionU3E__4_5;
	// UnityEngine.Object UniRx.ObserveExtensions/<PublishUnityObjectValueChanged>c__Iterator25`2::unityObject
	Object_t3878351788 * ___unityObject_6;
	// TSource UniRx.ObserveExtensions/<PublishUnityObjectValueChanged>c__Iterator25`2::<source>__5
	Il2CppObject * ___U3CsourceU3E__5_7;
	// UniRx.CancellationToken UniRx.ObserveExtensions/<PublishUnityObjectValueChanged>c__Iterator25`2::cancellationToken
	CancellationToken_t1439151560  ___cancellationToken_8;
	// System.Func`2<TSource,TProperty> UniRx.ObserveExtensions/<PublishUnityObjectValueChanged>c__Iterator25`2::propertySelector
	Func_2_t1833193546 * ___propertySelector_9;
	// UniRx.IObserver`1<TProperty> UniRx.ObserveExtensions/<PublishUnityObjectValueChanged>c__Iterator25`2::observer
	Il2CppObject* ___observer_10;
	// System.Exception UniRx.ObserveExtensions/<PublishUnityObjectValueChanged>c__Iterator25`2::<ex>__6
	Exception_t1967233988 * ___U3CexU3E__6_11;
	// System.Int32 UniRx.ObserveExtensions/<PublishUnityObjectValueChanged>c__Iterator25`2::$PC
	int32_t ___U24PC_12;
	// System.Object UniRx.ObserveExtensions/<PublishUnityObjectValueChanged>c__Iterator25`2::$current
	Il2CppObject * ___U24current_13;
	// UniRx.FrameCountType UniRx.ObserveExtensions/<PublishUnityObjectValueChanged>c__Iterator25`2::<$>frameCountType
	int32_t ___U3CU24U3EframeCountType_14;
	// UnityEngine.Object UniRx.ObserveExtensions/<PublishUnityObjectValueChanged>c__Iterator25`2::<$>unityObject
	Object_t3878351788 * ___U3CU24U3EunityObject_15;
	// UniRx.CancellationToken UniRx.ObserveExtensions/<PublishUnityObjectValueChanged>c__Iterator25`2::<$>cancellationToken
	CancellationToken_t1439151560  ___U3CU24U3EcancellationToken_16;
	// System.Func`2<TSource,TProperty> UniRx.ObserveExtensions/<PublishUnityObjectValueChanged>c__Iterator25`2::<$>propertySelector
	Func_2_t1833193546 * ___U3CU24U3EpropertySelector_17;
	// UniRx.IObserver`1<TProperty> UniRx.ObserveExtensions/<PublishUnityObjectValueChanged>c__Iterator25`2::<$>observer
	Il2CppObject* ___U3CU24U3Eobserver_18;

public:
	inline static int32_t get_offset_of_U3CcomparerU3E__0_0() { return static_cast<int32_t>(offsetof(U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t42621133, ___U3CcomparerU3E__0_0)); }
	inline Il2CppObject* get_U3CcomparerU3E__0_0() const { return ___U3CcomparerU3E__0_0; }
	inline Il2CppObject** get_address_of_U3CcomparerU3E__0_0() { return &___U3CcomparerU3E__0_0; }
	inline void set_U3CcomparerU3E__0_0(Il2CppObject* value)
	{
		___U3CcomparerU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcomparerU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CisFirstU3E__1_1() { return static_cast<int32_t>(offsetof(U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t42621133, ___U3CisFirstU3E__1_1)); }
	inline bool get_U3CisFirstU3E__1_1() const { return ___U3CisFirstU3E__1_1; }
	inline bool* get_address_of_U3CisFirstU3E__1_1() { return &___U3CisFirstU3E__1_1; }
	inline void set_U3CisFirstU3E__1_1(bool value)
	{
		___U3CisFirstU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CcurrentValueU3E__2_2() { return static_cast<int32_t>(offsetof(U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t42621133, ___U3CcurrentValueU3E__2_2)); }
	inline double get_U3CcurrentValueU3E__2_2() const { return ___U3CcurrentValueU3E__2_2; }
	inline double* get_address_of_U3CcurrentValueU3E__2_2() { return &___U3CcurrentValueU3E__2_2; }
	inline void set_U3CcurrentValueU3E__2_2(double value)
	{
		___U3CcurrentValueU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U3CprevValueU3E__3_3() { return static_cast<int32_t>(offsetof(U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t42621133, ___U3CprevValueU3E__3_3)); }
	inline double get_U3CprevValueU3E__3_3() const { return ___U3CprevValueU3E__3_3; }
	inline double* get_address_of_U3CprevValueU3E__3_3() { return &___U3CprevValueU3E__3_3; }
	inline void set_U3CprevValueU3E__3_3(double value)
	{
		___U3CprevValueU3E__3_3 = value;
	}

	inline static int32_t get_offset_of_frameCountType_4() { return static_cast<int32_t>(offsetof(U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t42621133, ___frameCountType_4)); }
	inline int32_t get_frameCountType_4() const { return ___frameCountType_4; }
	inline int32_t* get_address_of_frameCountType_4() { return &___frameCountType_4; }
	inline void set_frameCountType_4(int32_t value)
	{
		___frameCountType_4 = value;
	}

	inline static int32_t get_offset_of_U3CyieldInstructionU3E__4_5() { return static_cast<int32_t>(offsetof(U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t42621133, ___U3CyieldInstructionU3E__4_5)); }
	inline YieldInstruction_t3557331758 * get_U3CyieldInstructionU3E__4_5() const { return ___U3CyieldInstructionU3E__4_5; }
	inline YieldInstruction_t3557331758 ** get_address_of_U3CyieldInstructionU3E__4_5() { return &___U3CyieldInstructionU3E__4_5; }
	inline void set_U3CyieldInstructionU3E__4_5(YieldInstruction_t3557331758 * value)
	{
		___U3CyieldInstructionU3E__4_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CyieldInstructionU3E__4_5, value);
	}

	inline static int32_t get_offset_of_unityObject_6() { return static_cast<int32_t>(offsetof(U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t42621133, ___unityObject_6)); }
	inline Object_t3878351788 * get_unityObject_6() const { return ___unityObject_6; }
	inline Object_t3878351788 ** get_address_of_unityObject_6() { return &___unityObject_6; }
	inline void set_unityObject_6(Object_t3878351788 * value)
	{
		___unityObject_6 = value;
		Il2CppCodeGenWriteBarrier(&___unityObject_6, value);
	}

	inline static int32_t get_offset_of_U3CsourceU3E__5_7() { return static_cast<int32_t>(offsetof(U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t42621133, ___U3CsourceU3E__5_7)); }
	inline Il2CppObject * get_U3CsourceU3E__5_7() const { return ___U3CsourceU3E__5_7; }
	inline Il2CppObject ** get_address_of_U3CsourceU3E__5_7() { return &___U3CsourceU3E__5_7; }
	inline void set_U3CsourceU3E__5_7(Il2CppObject * value)
	{
		___U3CsourceU3E__5_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CsourceU3E__5_7, value);
	}

	inline static int32_t get_offset_of_cancellationToken_8() { return static_cast<int32_t>(offsetof(U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t42621133, ___cancellationToken_8)); }
	inline CancellationToken_t1439151560  get_cancellationToken_8() const { return ___cancellationToken_8; }
	inline CancellationToken_t1439151560 * get_address_of_cancellationToken_8() { return &___cancellationToken_8; }
	inline void set_cancellationToken_8(CancellationToken_t1439151560  value)
	{
		___cancellationToken_8 = value;
	}

	inline static int32_t get_offset_of_propertySelector_9() { return static_cast<int32_t>(offsetof(U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t42621133, ___propertySelector_9)); }
	inline Func_2_t1833193546 * get_propertySelector_9() const { return ___propertySelector_9; }
	inline Func_2_t1833193546 ** get_address_of_propertySelector_9() { return &___propertySelector_9; }
	inline void set_propertySelector_9(Func_2_t1833193546 * value)
	{
		___propertySelector_9 = value;
		Il2CppCodeGenWriteBarrier(&___propertySelector_9, value);
	}

	inline static int32_t get_offset_of_observer_10() { return static_cast<int32_t>(offsetof(U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t42621133, ___observer_10)); }
	inline Il2CppObject* get_observer_10() const { return ___observer_10; }
	inline Il2CppObject** get_address_of_observer_10() { return &___observer_10; }
	inline void set_observer_10(Il2CppObject* value)
	{
		___observer_10 = value;
		Il2CppCodeGenWriteBarrier(&___observer_10, value);
	}

	inline static int32_t get_offset_of_U3CexU3E__6_11() { return static_cast<int32_t>(offsetof(U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t42621133, ___U3CexU3E__6_11)); }
	inline Exception_t1967233988 * get_U3CexU3E__6_11() const { return ___U3CexU3E__6_11; }
	inline Exception_t1967233988 ** get_address_of_U3CexU3E__6_11() { return &___U3CexU3E__6_11; }
	inline void set_U3CexU3E__6_11(Exception_t1967233988 * value)
	{
		___U3CexU3E__6_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CexU3E__6_11, value);
	}

	inline static int32_t get_offset_of_U24PC_12() { return static_cast<int32_t>(offsetof(U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t42621133, ___U24PC_12)); }
	inline int32_t get_U24PC_12() const { return ___U24PC_12; }
	inline int32_t* get_address_of_U24PC_12() { return &___U24PC_12; }
	inline void set_U24PC_12(int32_t value)
	{
		___U24PC_12 = value;
	}

	inline static int32_t get_offset_of_U24current_13() { return static_cast<int32_t>(offsetof(U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t42621133, ___U24current_13)); }
	inline Il2CppObject * get_U24current_13() const { return ___U24current_13; }
	inline Il2CppObject ** get_address_of_U24current_13() { return &___U24current_13; }
	inline void set_U24current_13(Il2CppObject * value)
	{
		___U24current_13 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_13, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EframeCountType_14() { return static_cast<int32_t>(offsetof(U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t42621133, ___U3CU24U3EframeCountType_14)); }
	inline int32_t get_U3CU24U3EframeCountType_14() const { return ___U3CU24U3EframeCountType_14; }
	inline int32_t* get_address_of_U3CU24U3EframeCountType_14() { return &___U3CU24U3EframeCountType_14; }
	inline void set_U3CU24U3EframeCountType_14(int32_t value)
	{
		___U3CU24U3EframeCountType_14 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3EunityObject_15() { return static_cast<int32_t>(offsetof(U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t42621133, ___U3CU24U3EunityObject_15)); }
	inline Object_t3878351788 * get_U3CU24U3EunityObject_15() const { return ___U3CU24U3EunityObject_15; }
	inline Object_t3878351788 ** get_address_of_U3CU24U3EunityObject_15() { return &___U3CU24U3EunityObject_15; }
	inline void set_U3CU24U3EunityObject_15(Object_t3878351788 * value)
	{
		___U3CU24U3EunityObject_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3EunityObject_15, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EcancellationToken_16() { return static_cast<int32_t>(offsetof(U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t42621133, ___U3CU24U3EcancellationToken_16)); }
	inline CancellationToken_t1439151560  get_U3CU24U3EcancellationToken_16() const { return ___U3CU24U3EcancellationToken_16; }
	inline CancellationToken_t1439151560 * get_address_of_U3CU24U3EcancellationToken_16() { return &___U3CU24U3EcancellationToken_16; }
	inline void set_U3CU24U3EcancellationToken_16(CancellationToken_t1439151560  value)
	{
		___U3CU24U3EcancellationToken_16 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3EpropertySelector_17() { return static_cast<int32_t>(offsetof(U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t42621133, ___U3CU24U3EpropertySelector_17)); }
	inline Func_2_t1833193546 * get_U3CU24U3EpropertySelector_17() const { return ___U3CU24U3EpropertySelector_17; }
	inline Func_2_t1833193546 ** get_address_of_U3CU24U3EpropertySelector_17() { return &___U3CU24U3EpropertySelector_17; }
	inline void set_U3CU24U3EpropertySelector_17(Func_2_t1833193546 * value)
	{
		___U3CU24U3EpropertySelector_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3EpropertySelector_17, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eobserver_18() { return static_cast<int32_t>(offsetof(U3CPublishUnityObjectValueChangedU3Ec__Iterator25_2_t42621133, ___U3CU24U3Eobserver_18)); }
	inline Il2CppObject* get_U3CU24U3Eobserver_18() const { return ___U3CU24U3Eobserver_18; }
	inline Il2CppObject** get_address_of_U3CU24U3Eobserver_18() { return &___U3CU24U3Eobserver_18; }
	inline void set_U3CU24U3Eobserver_18(Il2CppObject* value)
	{
		___U3CU24U3Eobserver_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eobserver_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
