﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observer/Subscribe_`1<UniRx.Unit>
struct Subscribe__1_t1008250993;
// System.Action`1<System.Exception>
struct Action_1_t2115686693;
// System.Action
struct Action_t437523947;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Observer/Subscribe_`1<UniRx.Unit>::.ctor(System.Action`1<System.Exception>,System.Action)
extern "C"  void Subscribe__1__ctor_m2876855847_gshared (Subscribe__1_t1008250993 * __this, Action_1_t2115686693 * ___onError0, Action_t437523947 * ___onCompleted1, const MethodInfo* method);
#define Subscribe__1__ctor_m2876855847(__this, ___onError0, ___onCompleted1, method) ((  void (*) (Subscribe__1_t1008250993 *, Action_1_t2115686693 *, Action_t437523947 *, const MethodInfo*))Subscribe__1__ctor_m2876855847_gshared)(__this, ___onError0, ___onCompleted1, method)
// System.Void UniRx.Observer/Subscribe_`1<UniRx.Unit>::OnNext(T)
extern "C"  void Subscribe__1_OnNext_m2285041170_gshared (Subscribe__1_t1008250993 * __this, Unit_t2558286038  ___value0, const MethodInfo* method);
#define Subscribe__1_OnNext_m2285041170(__this, ___value0, method) ((  void (*) (Subscribe__1_t1008250993 *, Unit_t2558286038 , const MethodInfo*))Subscribe__1_OnNext_m2285041170_gshared)(__this, ___value0, method)
// System.Void UniRx.Observer/Subscribe_`1<UniRx.Unit>::OnError(System.Exception)
extern "C"  void Subscribe__1_OnError_m3065818401_gshared (Subscribe__1_t1008250993 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Subscribe__1_OnError_m3065818401(__this, ___error0, method) ((  void (*) (Subscribe__1_t1008250993 *, Exception_t1967233988 *, const MethodInfo*))Subscribe__1_OnError_m3065818401_gshared)(__this, ___error0, method)
// System.Void UniRx.Observer/Subscribe_`1<UniRx.Unit>::OnCompleted()
extern "C"  void Subscribe__1_OnCompleted_m4292168180_gshared (Subscribe__1_t1008250993 * __this, const MethodInfo* method);
#define Subscribe__1_OnCompleted_m4292168180(__this, method) ((  void (*) (Subscribe__1_t1008250993 *, const MethodInfo*))Subscribe__1_OnCompleted_m4292168180_gshared)(__this, method)
