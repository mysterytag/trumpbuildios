﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1/Subscription<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>
struct Subscription_t4180714101;
// UniRx.Subject`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>
struct Subject_1_t4044534903;
// UniRx.IObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>
struct IObserver_1_t23531890;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Subject`1/Subscription<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern "C"  void Subscription__ctor_m1798832381_gshared (Subscription_t4180714101 * __this, Subject_1_t4044534903 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method);
#define Subscription__ctor_m1798832381(__this, ___parent0, ___unsubscribeTarget1, method) ((  void (*) (Subscription_t4180714101 *, Subject_1_t4044534903 *, Il2CppObject*, const MethodInfo*))Subscription__ctor_m1798832381_gshared)(__this, ___parent0, ___unsubscribeTarget1, method)
// System.Void UniRx.Subject`1/Subscription<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>::Dispose()
extern "C"  void Subscription_Dispose_m3143010009_gshared (Subscription_t4180714101 * __this, const MethodInfo* method);
#define Subscription_Dispose_m3143010009(__this, method) ((  void (*) (Subscription_t4180714101 *, const MethodInfo*))Subscription_Dispose_m3143010009_gshared)(__this, method)
