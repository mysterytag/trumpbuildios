﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GameAnalyticsSDK.GA_MiniJSON
struct GA_MiniJSON_t4072382662;
// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameAnalyticsSDK.GA_MiniJSON
struct  GA_MiniJSON_t4072382662  : public Il2CppObject
{
public:
	// System.Int32 GameAnalyticsSDK.GA_MiniJSON::lastErrorIndex
	int32_t ___lastErrorIndex_14;
	// System.String GameAnalyticsSDK.GA_MiniJSON::lastDecode
	String_t* ___lastDecode_15;

public:
	inline static int32_t get_offset_of_lastErrorIndex_14() { return static_cast<int32_t>(offsetof(GA_MiniJSON_t4072382662, ___lastErrorIndex_14)); }
	inline int32_t get_lastErrorIndex_14() const { return ___lastErrorIndex_14; }
	inline int32_t* get_address_of_lastErrorIndex_14() { return &___lastErrorIndex_14; }
	inline void set_lastErrorIndex_14(int32_t value)
	{
		___lastErrorIndex_14 = value;
	}

	inline static int32_t get_offset_of_lastDecode_15() { return static_cast<int32_t>(offsetof(GA_MiniJSON_t4072382662, ___lastDecode_15)); }
	inline String_t* get_lastDecode_15() const { return ___lastDecode_15; }
	inline String_t** get_address_of_lastDecode_15() { return &___lastDecode_15; }
	inline void set_lastDecode_15(String_t* value)
	{
		___lastDecode_15 = value;
		Il2CppCodeGenWriteBarrier(&___lastDecode_15, value);
	}
};

struct GA_MiniJSON_t4072382662_StaticFields
{
public:
	// GameAnalyticsSDK.GA_MiniJSON GameAnalyticsSDK.GA_MiniJSON::instance
	GA_MiniJSON_t4072382662 * ___instance_13;

public:
	inline static int32_t get_offset_of_instance_13() { return static_cast<int32_t>(offsetof(GA_MiniJSON_t4072382662_StaticFields, ___instance_13)); }
	inline GA_MiniJSON_t4072382662 * get_instance_13() const { return ___instance_13; }
	inline GA_MiniJSON_t4072382662 ** get_address_of_instance_13() { return &___instance_13; }
	inline void set_instance_13(GA_MiniJSON_t4072382662 * value)
	{
		___instance_13 = value;
		Il2CppCodeGenWriteBarrier(&___instance_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
