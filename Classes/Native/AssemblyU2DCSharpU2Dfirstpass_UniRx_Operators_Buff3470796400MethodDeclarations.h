﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.BufferObservable`1/BufferTC<System.Object>
struct BufferTC_t3470796400;
// UniRx.Operators.BufferObservable`1<System.Object>
struct BufferObservable_1_t574294898;
// UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Object>>
struct IObserver_1_t920630341;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<System.TimeSpan>
struct Action_1_t912315597;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.BufferObservable`1/BufferTC<System.Object>::.ctor(UniRx.Operators.BufferObservable`1<T>,UniRx.IObserver`1<System.Collections.Generic.IList`1<T>>,System.IDisposable)
extern "C"  void BufferTC__ctor_m257145004_gshared (BufferTC_t3470796400 * __this, BufferObservable_1_t574294898 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define BufferTC__ctor_m257145004(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (BufferTC_t3470796400 *, BufferObservable_1_t574294898 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))BufferTC__ctor_m257145004_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.BufferObservable`1/BufferTC<System.Object>::.cctor()
extern "C"  void BufferTC__cctor_m2847470407_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define BufferTC__cctor_m2847470407(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))BufferTC__cctor_m2847470407_gshared)(__this /* static, unused */, method)
// System.IDisposable UniRx.Operators.BufferObservable`1/BufferTC<System.Object>::Run()
extern "C"  Il2CppObject * BufferTC_Run_m3712505588_gshared (BufferTC_t3470796400 * __this, const MethodInfo* method);
#define BufferTC_Run_m3712505588(__this, method) ((  Il2CppObject * (*) (BufferTC_t3470796400 *, const MethodInfo*))BufferTC_Run_m3712505588_gshared)(__this, method)
// System.Void UniRx.Operators.BufferObservable`1/BufferTC<System.Object>::CreateTimer()
extern "C"  void BufferTC_CreateTimer_m40636781_gshared (BufferTC_t3470796400 * __this, const MethodInfo* method);
#define BufferTC_CreateTimer_m40636781(__this, method) ((  void (*) (BufferTC_t3470796400 *, const MethodInfo*))BufferTC_CreateTimer_m40636781_gshared)(__this, method)
// System.Void UniRx.Operators.BufferObservable`1/BufferTC<System.Object>::OnNextTick(System.Int64)
extern "C"  void BufferTC_OnNextTick_m3470066015_gshared (BufferTC_t3470796400 * __this, int64_t ___currentTimerId0, const MethodInfo* method);
#define BufferTC_OnNextTick_m3470066015(__this, ___currentTimerId0, method) ((  void (*) (BufferTC_t3470796400 *, int64_t, const MethodInfo*))BufferTC_OnNextTick_m3470066015_gshared)(__this, ___currentTimerId0, method)
// System.Void UniRx.Operators.BufferObservable`1/BufferTC<System.Object>::OnNextRecursive(System.Int64,System.Action`1<System.TimeSpan>)
extern "C"  void BufferTC_OnNextRecursive_m2259042514_gshared (BufferTC_t3470796400 * __this, int64_t ___currentTimerId0, Action_1_t912315597 * ___self1, const MethodInfo* method);
#define BufferTC_OnNextRecursive_m2259042514(__this, ___currentTimerId0, ___self1, method) ((  void (*) (BufferTC_t3470796400 *, int64_t, Action_1_t912315597 *, const MethodInfo*))BufferTC_OnNextRecursive_m2259042514_gshared)(__this, ___currentTimerId0, ___self1, method)
// System.Void UniRx.Operators.BufferObservable`1/BufferTC<System.Object>::OnNext(T)
extern "C"  void BufferTC_OnNext_m2968053326_gshared (BufferTC_t3470796400 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define BufferTC_OnNext_m2968053326(__this, ___value0, method) ((  void (*) (BufferTC_t3470796400 *, Il2CppObject *, const MethodInfo*))BufferTC_OnNext_m2968053326_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.BufferObservable`1/BufferTC<System.Object>::OnError(System.Exception)
extern "C"  void BufferTC_OnError_m4251100509_gshared (BufferTC_t3470796400 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define BufferTC_OnError_m4251100509(__this, ___error0, method) ((  void (*) (BufferTC_t3470796400 *, Exception_t1967233988 *, const MethodInfo*))BufferTC_OnError_m4251100509_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.BufferObservable`1/BufferTC<System.Object>::OnCompleted()
extern "C"  void BufferTC_OnCompleted_m4284529712_gshared (BufferTC_t3470796400 * __this, const MethodInfo* method);
#define BufferTC_OnCompleted_m4284529712(__this, method) ((  void (*) (BufferTC_t3470796400 *, const MethodInfo*))BufferTC_OnCompleted_m4284529712_gshared)(__this, method)
