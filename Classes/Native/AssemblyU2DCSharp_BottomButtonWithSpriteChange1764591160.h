﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GuiController
struct GuiController_t1495593751;
// UnityEngine.Sprite
struct Sprite_t4006040370;
// UniRx.BoolReactiveProperty
struct BoolReactiveProperty_t3047538250;
// UnityEngine.UI.Image
struct Image_t3354615620;
// TutorialPlayer
struct TutorialPlayer_t4281139199;
// System.Func`2<System.Boolean,System.Boolean>
struct Func_2_t1008118516;

#include "AssemblyU2DCSharp_BottomButton1932555613.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BottomButtonWithSpriteChange
struct  BottomButtonWithSpriteChange_t1764591160  : public BottomButton_t1932555613
{
public:
	// GuiController BottomButtonWithSpriteChange::GuiController
	GuiController_t1495593751 * ___GuiController_3;
	// UnityEngine.Sprite BottomButtonWithSpriteChange::DefaultSprite
	Sprite_t4006040370 * ___DefaultSprite_4;
	// UnityEngine.Sprite BottomButtonWithSpriteChange::PressedSprite
	Sprite_t4006040370 * ___PressedSprite_5;
	// UniRx.BoolReactiveProperty BottomButtonWithSpriteChange::Pressed
	BoolReactiveProperty_t3047538250 * ___Pressed_6;
	// UnityEngine.UI.Image BottomButtonWithSpriteChange::_image
	Image_t3354615620 * ____image_7;
	// TutorialPlayer BottomButtonWithSpriteChange::TutorialPlayer
	TutorialPlayer_t4281139199 * ___TutorialPlayer_8;

public:
	inline static int32_t get_offset_of_GuiController_3() { return static_cast<int32_t>(offsetof(BottomButtonWithSpriteChange_t1764591160, ___GuiController_3)); }
	inline GuiController_t1495593751 * get_GuiController_3() const { return ___GuiController_3; }
	inline GuiController_t1495593751 ** get_address_of_GuiController_3() { return &___GuiController_3; }
	inline void set_GuiController_3(GuiController_t1495593751 * value)
	{
		___GuiController_3 = value;
		Il2CppCodeGenWriteBarrier(&___GuiController_3, value);
	}

	inline static int32_t get_offset_of_DefaultSprite_4() { return static_cast<int32_t>(offsetof(BottomButtonWithSpriteChange_t1764591160, ___DefaultSprite_4)); }
	inline Sprite_t4006040370 * get_DefaultSprite_4() const { return ___DefaultSprite_4; }
	inline Sprite_t4006040370 ** get_address_of_DefaultSprite_4() { return &___DefaultSprite_4; }
	inline void set_DefaultSprite_4(Sprite_t4006040370 * value)
	{
		___DefaultSprite_4 = value;
		Il2CppCodeGenWriteBarrier(&___DefaultSprite_4, value);
	}

	inline static int32_t get_offset_of_PressedSprite_5() { return static_cast<int32_t>(offsetof(BottomButtonWithSpriteChange_t1764591160, ___PressedSprite_5)); }
	inline Sprite_t4006040370 * get_PressedSprite_5() const { return ___PressedSprite_5; }
	inline Sprite_t4006040370 ** get_address_of_PressedSprite_5() { return &___PressedSprite_5; }
	inline void set_PressedSprite_5(Sprite_t4006040370 * value)
	{
		___PressedSprite_5 = value;
		Il2CppCodeGenWriteBarrier(&___PressedSprite_5, value);
	}

	inline static int32_t get_offset_of_Pressed_6() { return static_cast<int32_t>(offsetof(BottomButtonWithSpriteChange_t1764591160, ___Pressed_6)); }
	inline BoolReactiveProperty_t3047538250 * get_Pressed_6() const { return ___Pressed_6; }
	inline BoolReactiveProperty_t3047538250 ** get_address_of_Pressed_6() { return &___Pressed_6; }
	inline void set_Pressed_6(BoolReactiveProperty_t3047538250 * value)
	{
		___Pressed_6 = value;
		Il2CppCodeGenWriteBarrier(&___Pressed_6, value);
	}

	inline static int32_t get_offset_of__image_7() { return static_cast<int32_t>(offsetof(BottomButtonWithSpriteChange_t1764591160, ____image_7)); }
	inline Image_t3354615620 * get__image_7() const { return ____image_7; }
	inline Image_t3354615620 ** get_address_of__image_7() { return &____image_7; }
	inline void set__image_7(Image_t3354615620 * value)
	{
		____image_7 = value;
		Il2CppCodeGenWriteBarrier(&____image_7, value);
	}

	inline static int32_t get_offset_of_TutorialPlayer_8() { return static_cast<int32_t>(offsetof(BottomButtonWithSpriteChange_t1764591160, ___TutorialPlayer_8)); }
	inline TutorialPlayer_t4281139199 * get_TutorialPlayer_8() const { return ___TutorialPlayer_8; }
	inline TutorialPlayer_t4281139199 ** get_address_of_TutorialPlayer_8() { return &___TutorialPlayer_8; }
	inline void set_TutorialPlayer_8(TutorialPlayer_t4281139199 * value)
	{
		___TutorialPlayer_8 = value;
		Il2CppCodeGenWriteBarrier(&___TutorialPlayer_8, value);
	}
};

struct BottomButtonWithSpriteChange_t1764591160_StaticFields
{
public:
	// System.Func`2<System.Boolean,System.Boolean> BottomButtonWithSpriteChange::<>f__am$cache6
	Func_2_t1008118516 * ___U3CU3Ef__amU24cache6_9;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_9() { return static_cast<int32_t>(offsetof(BottomButtonWithSpriteChange_t1764591160_StaticFields, ___U3CU3Ef__amU24cache6_9)); }
	inline Func_2_t1008118516 * get_U3CU3Ef__amU24cache6_9() const { return ___U3CU3Ef__amU24cache6_9; }
	inline Func_2_t1008118516 ** get_address_of_U3CU3Ef__amU24cache6_9() { return &___U3CU3Ef__amU24cache6_9; }
	inline void set_U3CU3Ef__amU24cache6_9(Func_2_t1008118516 * value)
	{
		___U3CU3Ef__amU24cache6_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
