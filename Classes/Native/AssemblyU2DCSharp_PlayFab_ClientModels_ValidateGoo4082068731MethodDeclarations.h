﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.ValidateGooglePlayPurchaseRequest
struct ValidateGooglePlayPurchaseRequest_t4082068731;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Nullable_1_gen3871963234.h"

// System.Void PlayFab.ClientModels.ValidateGooglePlayPurchaseRequest::.ctor()
extern "C"  void ValidateGooglePlayPurchaseRequest__ctor_m1651301262 (ValidateGooglePlayPurchaseRequest_t4082068731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.ValidateGooglePlayPurchaseRequest::get_ReceiptJson()
extern "C"  String_t* ValidateGooglePlayPurchaseRequest_get_ReceiptJson_m3959318080 (ValidateGooglePlayPurchaseRequest_t4082068731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ValidateGooglePlayPurchaseRequest::set_ReceiptJson(System.String)
extern "C"  void ValidateGooglePlayPurchaseRequest_set_ReceiptJson_m2850936403 (ValidateGooglePlayPurchaseRequest_t4082068731 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.ValidateGooglePlayPurchaseRequest::get_Signature()
extern "C"  String_t* ValidateGooglePlayPurchaseRequest_get_Signature_m4028366200 (ValidateGooglePlayPurchaseRequest_t4082068731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ValidateGooglePlayPurchaseRequest::set_Signature(System.String)
extern "C"  void ValidateGooglePlayPurchaseRequest_set_Signature_m3388205659 (ValidateGooglePlayPurchaseRequest_t4082068731 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.ValidateGooglePlayPurchaseRequest::get_CurrencyCode()
extern "C"  String_t* ValidateGooglePlayPurchaseRequest_get_CurrencyCode_m3350969600 (ValidateGooglePlayPurchaseRequest_t4082068731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ValidateGooglePlayPurchaseRequest::set_CurrencyCode(System.String)
extern "C"  void ValidateGooglePlayPurchaseRequest_set_CurrencyCode_m397229713 (ValidateGooglePlayPurchaseRequest_t4082068731 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.UInt32> PlayFab.ClientModels.ValidateGooglePlayPurchaseRequest::get_PurchasePrice()
extern "C"  Nullable_1_t3871963234  ValidateGooglePlayPurchaseRequest_get_PurchasePrice_m252542783 (ValidateGooglePlayPurchaseRequest_t4082068731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.ValidateGooglePlayPurchaseRequest::set_PurchasePrice(System.Nullable`1<System.UInt32>)
extern "C"  void ValidateGooglePlayPurchaseRequest_set_PurchasePrice_m739233958 (ValidateGooglePlayPurchaseRequest_t4082068731 * __this, Nullable_1_t3871963234  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
