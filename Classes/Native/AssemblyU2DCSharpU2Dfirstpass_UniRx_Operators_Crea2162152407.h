﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`2<UniRx.IObserver`1<System.Single>,System.IDisposable>
struct Func_2_t3827670562;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Operat22353992.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.CreateObservable`1<System.Single>
struct  CreateObservable_1_t2162152407  : public OperatorObservableBase_1_t22353992
{
public:
	// System.Func`2<UniRx.IObserver`1<T>,System.IDisposable> UniRx.Operators.CreateObservable`1::subscribe
	Func_2_t3827670562 * ___subscribe_1;

public:
	inline static int32_t get_offset_of_subscribe_1() { return static_cast<int32_t>(offsetof(CreateObservable_1_t2162152407, ___subscribe_1)); }
	inline Func_2_t3827670562 * get_subscribe_1() const { return ___subscribe_1; }
	inline Func_2_t3827670562 ** get_address_of_subscribe_1() { return &___subscribe_1; }
	inline void set_subscribe_1(Func_2_t3827670562 * value)
	{
		___subscribe_1 = value;
		Il2CppCodeGenWriteBarrier(&___subscribe_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
