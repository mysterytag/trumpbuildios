﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Type
struct Type_t;

#include "mscorlib_System_Attribute498693649.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Prime31.P31DeserializeableFieldAttribute
struct  P31DeserializeableFieldAttribute_t1330016988  : public Attribute_t498693649
{
public:
	// System.String Prime31.P31DeserializeableFieldAttribute::key
	String_t* ___key_0;
	// System.Boolean Prime31.P31DeserializeableFieldAttribute::isCollection
	bool ___isCollection_1;
	// System.Type Prime31.P31DeserializeableFieldAttribute::type
	Type_t * ___type_2;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(P31DeserializeableFieldAttribute_t1330016988, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier(&___key_0, value);
	}

	inline static int32_t get_offset_of_isCollection_1() { return static_cast<int32_t>(offsetof(P31DeserializeableFieldAttribute_t1330016988, ___isCollection_1)); }
	inline bool get_isCollection_1() const { return ___isCollection_1; }
	inline bool* get_address_of_isCollection_1() { return &___isCollection_1; }
	inline void set_isCollection_1(bool value)
	{
		___isCollection_1 = value;
	}

	inline static int32_t get_offset_of_type_2() { return static_cast<int32_t>(offsetof(P31DeserializeableFieldAttribute_t1330016988, ___type_2)); }
	inline Type_t * get_type_2() const { return ___type_2; }
	inline Type_t ** get_address_of_type_2() { return &___type_2; }
	inline void set_type_2(Type_t * value)
	{
		___type_2 = value;
		Il2CppCodeGenWriteBarrier(&___type_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
