﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3591453091MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Zenject.PrefabSingletonId,Zenject.PrefabSingletonLazyCreator>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3322373000(__this, ___dictionary0, method) ((  void (*) (Enumerator_t3655640518 *, Dictionary_2_t3888612577 *, const MethodInfo*))Enumerator__ctor_m3920831137_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Zenject.PrefabSingletonId,Zenject.PrefabSingletonLazyCreator>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1014527523(__this, method) ((  Il2CppObject * (*) (Enumerator_t3655640518 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3262087712_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Zenject.PrefabSingletonId,Zenject.PrefabSingletonLazyCreator>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m213518381(__this, method) ((  void (*) (Enumerator_t3655640518 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2959141748_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Zenject.PrefabSingletonId,Zenject.PrefabSingletonLazyCreator>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3587983972(__this, method) ((  DictionaryEntry_t130027246  (*) (Enumerator_t3655640518 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2279524093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Zenject.PrefabSingletonId,Zenject.PrefabSingletonLazyCreator>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m718359743(__this, method) ((  Il2CppObject * (*) (Enumerator_t3655640518 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1201448700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Zenject.PrefabSingletonId,Zenject.PrefabSingletonLazyCreator>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m4197382033(__this, method) ((  Il2CppObject * (*) (Enumerator_t3655640518 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m294434446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Zenject.PrefabSingletonId,Zenject.PrefabSingletonLazyCreator>::MoveNext()
#define Enumerator_MoveNext_m1437995741(__this, method) ((  bool (*) (Enumerator_t3655640518 *, const MethodInfo*))Enumerator_MoveNext_m217327200_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Zenject.PrefabSingletonId,Zenject.PrefabSingletonLazyCreator>::get_Current()
#define Enumerator_get_Current_m1011348287(__this, method) ((  KeyValuePair_2_t3377143875  (*) (Enumerator_t3655640518 *, const MethodInfo*))Enumerator_get_Current_m4240003024_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Zenject.PrefabSingletonId,Zenject.PrefabSingletonLazyCreator>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2721665062(__this, method) ((  PrefabSingletonId_t3643845047 * (*) (Enumerator_t3655640518 *, const MethodInfo*))Enumerator_get_CurrentKey_m3062159917_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Zenject.PrefabSingletonId,Zenject.PrefabSingletonLazyCreator>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2069889382(__this, method) ((  PrefabSingletonLazyCreator_t3719004230 * (*) (Enumerator_t3655640518 *, const MethodInfo*))Enumerator_get_CurrentValue_m592783249_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Zenject.PrefabSingletonId,Zenject.PrefabSingletonLazyCreator>::Reset()
#define Enumerator_Reset_m2092642778(__this, method) ((  void (*) (Enumerator_t3655640518 *, const MethodInfo*))Enumerator_Reset_m3001375603_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Zenject.PrefabSingletonId,Zenject.PrefabSingletonLazyCreator>::VerifyState()
#define Enumerator_VerifyState_m334771811(__this, method) ((  void (*) (Enumerator_t3655640518 *, const MethodInfo*))Enumerator_VerifyState_m4290054460_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Zenject.PrefabSingletonId,Zenject.PrefabSingletonLazyCreator>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m2338034955(__this, method) ((  void (*) (Enumerator_t3655640518 *, const MethodInfo*))Enumerator_VerifyCurrent_m2318603684_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Zenject.PrefabSingletonId,Zenject.PrefabSingletonLazyCreator>::Dispose()
#define Enumerator_Dispose_m3508444202(__this, method) ((  void (*) (Enumerator_t3655640518 *, const MethodInfo*))Enumerator_Dispose_m627360643_gshared)(__this, method)
