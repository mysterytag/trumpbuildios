﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.InitializableManager/InitializableInfo
struct InitializableInfo_t3830632317;
// Zenject.IInitializable
struct IInitializable_t617891835;

#include "codegen/il2cpp-codegen.h"

// System.Void Zenject.InitializableManager/InitializableInfo::.ctor(Zenject.IInitializable,System.Int32)
extern "C"  void InitializableInfo__ctor_m4117725644 (InitializableInfo_t3830632317 * __this, Il2CppObject * ___initializable0, int32_t ___priority1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
