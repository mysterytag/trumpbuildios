﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MoneyCounter
struct MoneyCounter_t199543100;

#include "codegen/il2cpp-codegen.h"

// System.Void MoneyCounter::.ctor()
extern "C"  void MoneyCounter__ctor_m2271988959 (MoneyCounter_t199543100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoneyCounter::SetCountersNumber(System.Int32,System.Int32)
extern "C"  void MoneyCounter_SetCountersNumber_m3722464103 (MoneyCounter_t199543100 * __this, int32_t ___quantity0, int32_t ___chars1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoneyCounter::UpdateCounter()
extern "C"  void MoneyCounter_UpdateCounter_m3651466800 (MoneyCounter_t199543100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MoneyCounter::CheckSymbol(System.Char)
extern "C"  bool MoneyCounter_CheckSymbol_m4024240308 (MoneyCounter_t199543100 * __this, uint16_t ___symbol0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
