﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Func`2<UniRx.Unit,System.Object>
struct Func_2_t3392211982;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Func`2<UniRx.Unit,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m2719813104_gshared (Func_2_t3392211982 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Func_2__ctor_m2719813104(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t3392211982 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m2719813104_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<UniRx.Unit,System.Object>::Invoke(T)
extern "C"  Il2CppObject * Func_2_Invoke_m2321944950_gshared (Func_2_t3392211982 * __this, Unit_t2558286038  ___arg10, const MethodInfo* method);
#define Func_2_Invoke_m2321944950(__this, ___arg10, method) ((  Il2CppObject * (*) (Func_2_t3392211982 *, Unit_t2558286038 , const MethodInfo*))Func_2_Invoke_m2321944950_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<UniRx.Unit,System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_2_BeginInvoke_m3501888937_gshared (Func_2_t3392211982 * __this, Unit_t2558286038  ___arg10, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method);
#define Func_2_BeginInvoke_m3501888937(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t3392211982 *, Unit_t2558286038 , AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m3501888937_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<UniRx.Unit,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Func_2_EndInvoke_m859232286_gshared (Func_2_t3392211982 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Func_2_EndInvoke_m859232286(__this, ___result0, method) ((  Il2CppObject * (*) (Func_2_t3392211982 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m859232286_gshared)(__this, ___result0, method)
