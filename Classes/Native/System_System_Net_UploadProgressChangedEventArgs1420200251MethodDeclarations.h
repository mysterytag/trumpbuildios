﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.UploadProgressChangedEventArgs
struct UploadProgressChangedEventArgs_t1420200251;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Net.UploadProgressChangedEventArgs::.ctor(System.Int64,System.Int64,System.Int64,System.Int64,System.Int32,System.Object)
extern "C"  void UploadProgressChangedEventArgs__ctor_m206641896 (UploadProgressChangedEventArgs_t1420200251 * __this, int64_t ___bytesReceived0, int64_t ___totalBytesToReceive1, int64_t ___bytesSent2, int64_t ___totalBytesToSend3, int32_t ___progressPercentage4, Il2CppObject * ___userState5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.UploadProgressChangedEventArgs::get_BytesReceived()
extern "C"  int64_t UploadProgressChangedEventArgs_get_BytesReceived_m2292973787 (UploadProgressChangedEventArgs_t1420200251 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.UploadProgressChangedEventArgs::get_TotalBytesToReceive()
extern "C"  int64_t UploadProgressChangedEventArgs_get_TotalBytesToReceive_m665615824 (UploadProgressChangedEventArgs_t1420200251 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.UploadProgressChangedEventArgs::get_BytesSent()
extern "C"  int64_t UploadProgressChangedEventArgs_get_BytesSent_m597252434 (UploadProgressChangedEventArgs_t1420200251 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Net.UploadProgressChangedEventArgs::get_TotalBytesToSend()
extern "C"  int64_t UploadProgressChangedEventArgs_get_TotalBytesToSend_m1249230685 (UploadProgressChangedEventArgs_t1420200251 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
