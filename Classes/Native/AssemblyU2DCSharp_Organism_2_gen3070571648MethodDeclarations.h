﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Organism_2_gen948289219MethodDeclarations.h"

// System.Void Organism`2<IOrganism,System.Object>::.ctor()
#define Organism_2__ctor_m1656428386(__this, method) ((  void (*) (Organism_2_t3070571648 *, const MethodInfo*))Organism_2__ctor_m4082911749_gshared)(__this, method)
// TCarrier Organism`2<IOrganism,System.Object>::get_carrier()
#define Organism_2_get_carrier_m1413555624(__this, method) ((  Il2CppObject * (*) (Organism_2_t3070571648 *, const MethodInfo*))Organism_2_get_carrier_m4029902219_gshared)(__this, method)
// System.Void Organism`2<IOrganism,System.Object>::set_carrier(TCarrier)
#define Organism_2_set_carrier_m1007007031(__this, ___value0, method) ((  void (*) (Organism_2_t3070571648 *, Il2CppObject *, const MethodInfo*))Organism_2_set_carrier_m2958823066_gshared)(__this, ___value0, method)
// TRoot Organism`2<IOrganism,System.Object>::get_root()
#define Organism_2_get_root_m1487033744(__this, method) ((  Il2CppObject * (*) (Organism_2_t3070571648 *, const MethodInfo*))Organism_2_get_root_m2383393421_gshared)(__this, method)
// System.Void Organism`2<IOrganism,System.Object>::set_root(TRoot)
#define Organism_2_set_root_m365987547(__this, ___value0, method) ((  void (*) (Organism_2_t3070571648 *, Il2CppObject *, const MethodInfo*))Organism_2_set_root_m984770174_gshared)(__this, ___value0, method)
// IOnceEmptyStream Organism`2<IOrganism,System.Object>::get_fellAsleep()
#define Organism_2_get_fellAsleep_m50682438(__this, method) ((  Il2CppObject * (*) (Organism_2_t3070571648 *, const MethodInfo*))Organism_2_get_fellAsleep_m2050669571_gshared)(__this, method)
// System.Void Organism`2<IOrganism,System.Object>::DisposeWhenAsleep(System.IDisposable)
#define Organism_2_DisposeWhenAsleep_m3237155589(__this, ___disposable0, method) ((  void (*) (Organism_2_t3070571648 *, Il2CppObject *, const MethodInfo*))Organism_2_DisposeWhenAsleep_m153634408_gshared)(__this, ___disposable0, method)
// System.Boolean Organism`2<IOrganism,System.Object>::get_isAlive()
#define Organism_2_get_isAlive_m491029102(__this, method) ((  bool (*) (Organism_2_t3070571648 *, const MethodInfo*))Organism_2_get_isAlive_m434369745_gshared)(__this, method)
// System.Boolean Organism`2<IOrganism,System.Object>::get_primal()
#define Organism_2_get_primal_m3745366120(__this, method) ((  bool (*) (Organism_2_t3070571648 *, const MethodInfo*))Organism_2_get_primal_m1803875749_gshared)(__this, method)
// System.Void Organism`2<IOrganism,System.Object>::Setup(IOrganism,IRoot)
#define Organism_2_Setup_m3047540027(__this, ___c0, ___r1, method) ((  void (*) (Organism_2_t3070571648 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Organism_2_Setup_m3424294968_gshared)(__this, ___c0, ___r1, method)
// System.Void Organism`2<IOrganism,System.Object>::WakeUp()
#define Organism_2_WakeUp_m502719745(__this, method) ((  void (*) (Organism_2_t3070571648 *, const MethodInfo*))Organism_2_WakeUp_m2709259966_gshared)(__this, method)
// System.Void Organism`2<IOrganism,System.Object>::Spread()
#define Organism_2_Spread_m2749816341(__this, method) ((  void (*) (Organism_2_t3070571648 *, const MethodInfo*))Organism_2_Spread_m661389266_gshared)(__this, method)
// System.Void Organism`2<IOrganism,System.Object>::Collect(System.IDisposable)
#define Organism_2_Collect_m1443876096(__this, ___tail0, method) ((  void (*) (Organism_2_t3070571648 *, Il2CppObject *, const MethodInfo*))Organism_2_Collect_m831016995_gshared)(__this, ___tail0, method)
// System.Void Organism`2<IOrganism,System.Object>::set_collect(System.IDisposable)
#define Organism_2_set_collect_m2824908611(__this, ___value0, method) ((  void (*) (Organism_2_t3070571648 *, Il2CppObject *, const MethodInfo*))Organism_2_set_collect_m1070393574_gshared)(__this, ___value0, method)
// System.Void Organism`2<IOrganism,System.Object>::ToSleep()
#define Organism_2_ToSleep_m2846017244(__this, method) ((  void (*) (Organism_2_t3070571648 *, const MethodInfo*))Organism_2_ToSleep_m2529287359_gshared)(__this, method)
// System.Void Organism`2<IOrganism,System.Object>::Put(IOrganism)
#define Organism_2_Put_m2812605096(__this, ___organism0, method) ((  void (*) (Organism_2_t3070571648 *, Il2CppObject *, const MethodInfo*))Organism_2_Put_m1862734501_gshared)(__this, ___organism0, method)
// System.Void Organism`2<IOrganism,System.Object>::Take(IOrganism)
#define Organism_2_Take_m4016239374(__this, ___organism0, method) ((  void (*) (Organism_2_t3070571648 *, Il2CppObject *, const MethodInfo*))Organism_2_Take_m340054705_gshared)(__this, ___organism0, method)
// IOrganism Organism`2<IOrganism,System.Object>::ChangeRoot(TRoot)
#define Organism_2_ChangeRoot_m1990193408(__this, ___r0, method) ((  Il2CppObject * (*) (Organism_2_t3070571648 *, Il2CppObject *, const MethodInfo*))Organism_2_ChangeRoot_m2085091299_gshared)(__this, ___r0, method)
// System.Void Organism`2<IOrganism,System.Object>::RegisterChildren()
#define Organism_2_RegisterChildren_m1567059300(__this, method) ((  void (*) (Organism_2_t3070571648 *, const MethodInfo*))Organism_2_RegisterChildren_m1720665825_gshared)(__this, method)
// System.Void Organism`2<IOrganism,System.Object>::<WakeUp>m__1AD(IOrganism)
#define Organism_2_U3CWakeUpU3Em__1AD_m4233065099(__this /* static, unused */, ___child0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Organism_2_U3CWakeUpU3Em__1AD_m1095000686_gshared)(__this /* static, unused */, ___child0, method)
// System.Void Organism`2<IOrganism,System.Object>::<Spread>m__1AE(IOrganism)
#define Organism_2_U3CSpreadU3Em__1AE_m1378117886(__this /* static, unused */, ___child0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Organism_2_U3CSpreadU3Em__1AE_m2535020769_gshared)(__this /* static, unused */, ___child0, method)
// System.Void Organism`2<IOrganism,System.Object>::<ToSleep>m__1AF(IOrganism)
#define Organism_2_U3CToSleepU3Em__1AF_m1710793904(__this /* static, unused */, ___child0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Organism_2_U3CToSleepU3Em__1AF_m3215044909_gshared)(__this /* static, unused */, ___child0, method)
// System.Void Organism`2<IOrganism,System.Object>::<ToSleep>m__1B0(System.IDisposable)
#define Organism_2_U3CToSleepU3Em__1B0_m2580728422(__this /* static, unused */, ___d0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Organism_2_U3CToSleepU3Em__1B0_m3346233993_gshared)(__this /* static, unused */, ___d0, method)
