﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.UnlinkCustomIDRequest
struct UnlinkCustomIDRequest_t1348755968;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.UnlinkCustomIDRequest::.ctor()
extern "C"  void UnlinkCustomIDRequest__ctor_m3606506793 (UnlinkCustomIDRequest_t1348755968 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.UnlinkCustomIDRequest::get_CustomId()
extern "C"  String_t* UnlinkCustomIDRequest_get_CustomId_m2856692563 (UnlinkCustomIDRequest_t1348755968 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.UnlinkCustomIDRequest::set_CustomId(System.String)
extern "C"  void UnlinkCustomIDRequest_set_CustomId_m2423470878 (UnlinkCustomIDRequest_t1348755968 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
