﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.StringReactiveProperty
struct StringReactiveProperty_t1288121713;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void UniRx.StringReactiveProperty::.ctor()
extern "C"  void StringReactiveProperty__ctor_m2744792376 (StringReactiveProperty_t1288121713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.StringReactiveProperty::.ctor(System.String)
extern "C"  void StringReactiveProperty__ctor_m2957616330 (StringReactiveProperty_t1288121713 * __this, String_t* ___initialValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
