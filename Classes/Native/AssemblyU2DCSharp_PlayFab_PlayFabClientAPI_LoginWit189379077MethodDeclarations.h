﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/LoginWithIOSDeviceIDRequestCallback
struct LoginWithIOSDeviceIDRequestCallback_t189379077;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.LoginWithIOSDeviceIDRequest
struct LoginWithIOSDeviceIDRequest_t318822832;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginWithIOS318822832.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/LoginWithIOSDeviceIDRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void LoginWithIOSDeviceIDRequestCallback__ctor_m4228347924 (LoginWithIOSDeviceIDRequestCallback_t189379077 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LoginWithIOSDeviceIDRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.LoginWithIOSDeviceIDRequest,System.Object)
extern "C"  void LoginWithIOSDeviceIDRequestCallback_Invoke_m3562183503 (LoginWithIOSDeviceIDRequestCallback_t189379077 * __this, String_t* ___urlPath0, int32_t ___callId1, LoginWithIOSDeviceIDRequest_t318822832 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_LoginWithIOSDeviceIDRequestCallback_t189379077(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, LoginWithIOSDeviceIDRequest_t318822832 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/LoginWithIOSDeviceIDRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.LoginWithIOSDeviceIDRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LoginWithIOSDeviceIDRequestCallback_BeginInvoke_m3849458046 (LoginWithIOSDeviceIDRequestCallback_t189379077 * __this, String_t* ___urlPath0, int32_t ___callId1, LoginWithIOSDeviceIDRequest_t318822832 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LoginWithIOSDeviceIDRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void LoginWithIOSDeviceIDRequestCallback_EndInvoke_m794956836 (LoginWithIOSDeviceIDRequestCallback_t189379077 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
