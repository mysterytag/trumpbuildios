﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.BufferObservable`1/BufferTS<System.Object>
struct BufferTS_t71212032;
// UniRx.Operators.BufferObservable`1<System.Object>
struct BufferObservable_1_t574294898;
// UniRx.IObserver`1<System.Collections.Generic.IList`1<System.Object>>
struct IObserver_1_t920630341;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.BufferObservable`1/BufferTS<System.Object>::.ctor(UniRx.Operators.BufferObservable`1<T>,UniRx.IObserver`1<System.Collections.Generic.IList`1<T>>,System.IDisposable)
extern "C"  void BufferTS__ctor_m349948572_gshared (BufferTS_t71212032 * __this, BufferObservable_1_t574294898 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define BufferTS__ctor_m349948572(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (BufferTS_t71212032 *, BufferObservable_1_t574294898 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))BufferTS__ctor_m349948572_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.BufferObservable`1/BufferTS<System.Object>::Run()
extern "C"  Il2CppObject * BufferTS_Run_m2153900804_gshared (BufferTS_t71212032 * __this, const MethodInfo* method);
#define BufferTS_Run_m2153900804(__this, method) ((  Il2CppObject * (*) (BufferTS_t71212032 *, const MethodInfo*))BufferTS_Run_m2153900804_gshared)(__this, method)
// System.Void UniRx.Operators.BufferObservable`1/BufferTS<System.Object>::CreateTimer()
extern "C"  void BufferTS_CreateTimer_m1358468477_gshared (BufferTS_t71212032 * __this, const MethodInfo* method);
#define BufferTS_CreateTimer_m1358468477(__this, method) ((  void (*) (BufferTS_t71212032 *, const MethodInfo*))BufferTS_CreateTimer_m1358468477_gshared)(__this, method)
// System.Void UniRx.Operators.BufferObservable`1/BufferTS<System.Object>::OnNext(T)
extern "C"  void BufferTS_OnNext_m1174008414_gshared (BufferTS_t71212032 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define BufferTS_OnNext_m1174008414(__this, ___value0, method) ((  void (*) (BufferTS_t71212032 *, Il2CppObject *, const MethodInfo*))BufferTS_OnNext_m1174008414_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.BufferObservable`1/BufferTS<System.Object>::OnError(System.Exception)
extern "C"  void BufferTS_OnError_m1366921581_gshared (BufferTS_t71212032 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define BufferTS_OnError_m1366921581(__this, ___error0, method) ((  void (*) (BufferTS_t71212032 *, Exception_t1967233988 *, const MethodInfo*))BufferTS_OnError_m1366921581_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.BufferObservable`1/BufferTS<System.Object>::OnCompleted()
extern "C"  void BufferTS_OnCompleted_m1307394112_gshared (BufferTS_t71212032 * __this, const MethodInfo* method);
#define BufferTS_OnCompleted_m1307394112(__this, method) ((  void (*) (BufferTS_t71212032 *, const MethodInfo*))BufferTS_OnCompleted_m1307394112_gshared)(__this, method)
