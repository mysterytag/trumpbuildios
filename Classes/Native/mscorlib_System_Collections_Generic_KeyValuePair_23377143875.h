﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Zenject.PrefabSingletonId
struct PrefabSingletonId_t3643845047;
// Zenject.PrefabSingletonLazyCreator
struct PrefabSingletonLazyCreator_t3719004230;

#include "mscorlib_System_ValueType4014882752.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<Zenject.PrefabSingletonId,Zenject.PrefabSingletonLazyCreator>
struct  KeyValuePair_2_t3377143875 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	PrefabSingletonId_t3643845047 * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	PrefabSingletonLazyCreator_t3719004230 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3377143875, ___key_0)); }
	inline PrefabSingletonId_t3643845047 * get_key_0() const { return ___key_0; }
	inline PrefabSingletonId_t3643845047 ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(PrefabSingletonId_t3643845047 * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier(&___key_0, value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3377143875, ___value_1)); }
	inline PrefabSingletonLazyCreator_t3719004230 * get_value_1() const { return ___value_1; }
	inline PrefabSingletonLazyCreator_t3719004230 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(PrefabSingletonLazyCreator_t3719004230 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier(&___value_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
