﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"

extern const Il2CppMethodPointer* native_delegate_wrapper_DeflateStream_UnmanagedRead_m404803083_indirect;
extern const Il2CppMethodPointer* native_delegate_wrapper_DeflateStream_UnmanagedWrite_m223973422_indirect;
extern const Il2CppMethodPointer* native_delegate_wrapper_Localytics_ReceiveAnalyticsMessage_m3745756772_indirect;
extern const Il2CppMethodPointer* native_delegate_wrapper_Localytics_ReceiveMessagingMessage_m864780130_indirect;
extern const Il2CppMethodPointer * g_PInvokeWrapperPointers[4] = 
{
	(Il2CppMethodPointer*)&native_delegate_wrapper_DeflateStream_UnmanagedRead_m404803083_indirect,
	(Il2CppMethodPointer*)&native_delegate_wrapper_DeflateStream_UnmanagedWrite_m223973422_indirect,
	(Il2CppMethodPointer*)&native_delegate_wrapper_Localytics_ReceiveAnalyticsMessage_m3745756772_indirect,
	(Il2CppMethodPointer*)&native_delegate_wrapper_Localytics_ReceiveMessagingMessage_m864780130_indirect,
};
