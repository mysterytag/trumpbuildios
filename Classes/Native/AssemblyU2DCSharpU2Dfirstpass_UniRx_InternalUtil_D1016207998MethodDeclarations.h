﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionReplaceEvent`1<System.Object>>
struct DisposedObserver_1_t1016207998;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionRepl1841750536.h"

// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionReplaceEvent`1<System.Object>>::.ctor()
extern "C"  void DisposedObserver_1__ctor_m548762823_gshared (DisposedObserver_1_t1016207998 * __this, const MethodInfo* method);
#define DisposedObserver_1__ctor_m548762823(__this, method) ((  void (*) (DisposedObserver_1_t1016207998 *, const MethodInfo*))DisposedObserver_1__ctor_m548762823_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionReplaceEvent`1<System.Object>>::.cctor()
extern "C"  void DisposedObserver_1__cctor_m3644649414_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define DisposedObserver_1__cctor_m3644649414(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))DisposedObserver_1__cctor_m3644649414_gshared)(__this /* static, unused */, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionReplaceEvent`1<System.Object>>::OnCompleted()
extern "C"  void DisposedObserver_1_OnCompleted_m3887183633_gshared (DisposedObserver_1_t1016207998 * __this, const MethodInfo* method);
#define DisposedObserver_1_OnCompleted_m3887183633(__this, method) ((  void (*) (DisposedObserver_1_t1016207998 *, const MethodInfo*))DisposedObserver_1_OnCompleted_m3887183633_gshared)(__this, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionReplaceEvent`1<System.Object>>::OnError(System.Exception)
extern "C"  void DisposedObserver_1_OnError_m2201310910_gshared (DisposedObserver_1_t1016207998 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define DisposedObserver_1_OnError_m2201310910(__this, ___error0, method) ((  void (*) (DisposedObserver_1_t1016207998 *, Exception_t1967233988 *, const MethodInfo*))DisposedObserver_1_OnError_m2201310910_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.DisposedObserver`1<UniRx.CollectionReplaceEvent`1<System.Object>>::OnNext(T)
extern "C"  void DisposedObserver_1_OnNext_m1910798767_gshared (DisposedObserver_1_t1016207998 * __this, CollectionReplaceEvent_1_t1841750536  ___value0, const MethodInfo* method);
#define DisposedObserver_1_OnNext_m1910798767(__this, ___value0, method) ((  void (*) (DisposedObserver_1_t1016207998 *, CollectionReplaceEvent_1_t1841750536 , const MethodInfo*))DisposedObserver_1_OnNext_m1910798767_gshared)(__this, ___value0, method)
