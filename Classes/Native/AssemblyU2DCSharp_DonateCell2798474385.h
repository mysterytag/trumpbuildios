﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Image
struct Image_t3354615620;
// UnityEngine.UI.Text
struct Text_t3286458198;

#include "AssemblyU2DCSharp_TableViewCellBase2057844710.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DonateCell
struct  DonateCell_t2798474385  : public TableViewCellBase_t2057844710
{
public:
	// UnityEngine.UI.Image DonateCell::Back
	Image_t3354615620 * ___Back_13;
	// UnityEngine.UI.Text DonateCell::SoloText
	Text_t3286458198 * ___SoloText_14;
	// UnityEngine.UI.Text DonateCell::TitleText
	Text_t3286458198 * ___TitleText_15;
	// UnityEngine.UI.Text DonateCell::DescriptionText
	Text_t3286458198 * ___DescriptionText_16;
	// UnityEngine.UI.Text DonateCell::CostText
	Text_t3286458198 * ___CostText_17;
	// UnityEngine.UI.Image DonateCell::NewGame
	Image_t3354615620 * ___NewGame_18;
	// UnityEngine.UI.Image DonateCell::Sound
	Image_t3354615620 * ___Sound_19;
	// UnityEngine.UI.Image DonateCell::MiddleIcon
	Image_t3354615620 * ___MiddleIcon_20;

public:
	inline static int32_t get_offset_of_Back_13() { return static_cast<int32_t>(offsetof(DonateCell_t2798474385, ___Back_13)); }
	inline Image_t3354615620 * get_Back_13() const { return ___Back_13; }
	inline Image_t3354615620 ** get_address_of_Back_13() { return &___Back_13; }
	inline void set_Back_13(Image_t3354615620 * value)
	{
		___Back_13 = value;
		Il2CppCodeGenWriteBarrier(&___Back_13, value);
	}

	inline static int32_t get_offset_of_SoloText_14() { return static_cast<int32_t>(offsetof(DonateCell_t2798474385, ___SoloText_14)); }
	inline Text_t3286458198 * get_SoloText_14() const { return ___SoloText_14; }
	inline Text_t3286458198 ** get_address_of_SoloText_14() { return &___SoloText_14; }
	inline void set_SoloText_14(Text_t3286458198 * value)
	{
		___SoloText_14 = value;
		Il2CppCodeGenWriteBarrier(&___SoloText_14, value);
	}

	inline static int32_t get_offset_of_TitleText_15() { return static_cast<int32_t>(offsetof(DonateCell_t2798474385, ___TitleText_15)); }
	inline Text_t3286458198 * get_TitleText_15() const { return ___TitleText_15; }
	inline Text_t3286458198 ** get_address_of_TitleText_15() { return &___TitleText_15; }
	inline void set_TitleText_15(Text_t3286458198 * value)
	{
		___TitleText_15 = value;
		Il2CppCodeGenWriteBarrier(&___TitleText_15, value);
	}

	inline static int32_t get_offset_of_DescriptionText_16() { return static_cast<int32_t>(offsetof(DonateCell_t2798474385, ___DescriptionText_16)); }
	inline Text_t3286458198 * get_DescriptionText_16() const { return ___DescriptionText_16; }
	inline Text_t3286458198 ** get_address_of_DescriptionText_16() { return &___DescriptionText_16; }
	inline void set_DescriptionText_16(Text_t3286458198 * value)
	{
		___DescriptionText_16 = value;
		Il2CppCodeGenWriteBarrier(&___DescriptionText_16, value);
	}

	inline static int32_t get_offset_of_CostText_17() { return static_cast<int32_t>(offsetof(DonateCell_t2798474385, ___CostText_17)); }
	inline Text_t3286458198 * get_CostText_17() const { return ___CostText_17; }
	inline Text_t3286458198 ** get_address_of_CostText_17() { return &___CostText_17; }
	inline void set_CostText_17(Text_t3286458198 * value)
	{
		___CostText_17 = value;
		Il2CppCodeGenWriteBarrier(&___CostText_17, value);
	}

	inline static int32_t get_offset_of_NewGame_18() { return static_cast<int32_t>(offsetof(DonateCell_t2798474385, ___NewGame_18)); }
	inline Image_t3354615620 * get_NewGame_18() const { return ___NewGame_18; }
	inline Image_t3354615620 ** get_address_of_NewGame_18() { return &___NewGame_18; }
	inline void set_NewGame_18(Image_t3354615620 * value)
	{
		___NewGame_18 = value;
		Il2CppCodeGenWriteBarrier(&___NewGame_18, value);
	}

	inline static int32_t get_offset_of_Sound_19() { return static_cast<int32_t>(offsetof(DonateCell_t2798474385, ___Sound_19)); }
	inline Image_t3354615620 * get_Sound_19() const { return ___Sound_19; }
	inline Image_t3354615620 ** get_address_of_Sound_19() { return &___Sound_19; }
	inline void set_Sound_19(Image_t3354615620 * value)
	{
		___Sound_19 = value;
		Il2CppCodeGenWriteBarrier(&___Sound_19, value);
	}

	inline static int32_t get_offset_of_MiddleIcon_20() { return static_cast<int32_t>(offsetof(DonateCell_t2798474385, ___MiddleIcon_20)); }
	inline Image_t3354615620 * get_MiddleIcon_20() const { return ___MiddleIcon_20; }
	inline Image_t3354615620 ** get_address_of_MiddleIcon_20() { return &___MiddleIcon_20; }
	inline void set_MiddleIcon_20(Image_t3354615620 * value)
	{
		___MiddleIcon_20 = value;
		Il2CppCodeGenWriteBarrier(&___MiddleIcon_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
