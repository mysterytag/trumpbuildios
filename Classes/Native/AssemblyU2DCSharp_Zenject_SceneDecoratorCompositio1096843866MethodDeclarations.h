﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.SceneDecoratorCompositionRoot/<Init>c__Iterator50
struct U3CInitU3Ec__Iterator50_t1096843866;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Zenject.SceneDecoratorCompositionRoot/<Init>c__Iterator50::.ctor()
extern "C"  void U3CInitU3Ec__Iterator50__ctor_m1909821954 (U3CInitU3Ec__Iterator50_t1096843866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Zenject.SceneDecoratorCompositionRoot/<Init>c__Iterator50::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CInitU3Ec__Iterator50_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1492704976 (U3CInitU3Ec__Iterator50_t1096843866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Zenject.SceneDecoratorCompositionRoot/<Init>c__Iterator50::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CInitU3Ec__Iterator50_System_Collections_IEnumerator_get_Current_m4041376356 (U3CInitU3Ec__Iterator50_t1096843866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.SceneDecoratorCompositionRoot/<Init>c__Iterator50::MoveNext()
extern "C"  bool U3CInitU3Ec__Iterator50_MoveNext_m3420556722 (U3CInitU3Ec__Iterator50_t1096843866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.SceneDecoratorCompositionRoot/<Init>c__Iterator50::Dispose()
extern "C"  void U3CInitU3Ec__Iterator50_Dispose_m1286145471 (U3CInitU3Ec__Iterator50_t1096843866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.SceneDecoratorCompositionRoot/<Init>c__Iterator50::Reset()
extern "C"  void U3CInitU3Ec__Iterator50_Reset_m3851222191 (U3CInitU3Ec__Iterator50_t1096843866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
