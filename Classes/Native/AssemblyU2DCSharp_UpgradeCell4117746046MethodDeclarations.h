﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UpgradeCell
struct UpgradeCell_t4117746046;

#include "codegen/il2cpp-codegen.h"

// System.Void UpgradeCell::.ctor()
extern "C"  void UpgradeCell__ctor_m1008498605 (UpgradeCell_t4117746046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UpgradeCell::.cctor()
extern "C"  void UpgradeCell__cctor_m716589472 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UpgradeCell::Awake()
extern "C"  void UpgradeCell_Awake_m1246103824 (UpgradeCell_t4117746046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UpgradeCell::<Awake>m__27F()
extern "C"  void UpgradeCell_U3CAwakeU3Em__27F_m2054477332 (UpgradeCell_t4117746046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
