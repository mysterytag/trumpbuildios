﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.ASN1
struct ASN1_t1254135647;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "Mono_Security_Mono_Security_ASN11254135646.h"

// Mono.Security.ASN1 Mono.Security.PKCS7::AlgorithmIdentifier(System.String)
extern "C"  ASN1_t1254135647 * PKCS7_AlgorithmIdentifier_m2230694184 (Il2CppObject * __this /* static, unused */, String_t* ___oid0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.ASN1 Mono.Security.PKCS7::AlgorithmIdentifier(System.String,Mono.Security.ASN1)
extern "C"  ASN1_t1254135647 * PKCS7_AlgorithmIdentifier_m4280840358 (Il2CppObject * __this /* static, unused */, String_t* ___oid0, ASN1_t1254135647 * ___parameters1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
