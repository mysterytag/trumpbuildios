﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/RunCloudScriptRequestCallback
struct RunCloudScriptRequestCallback_t2578940735;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.RunCloudScriptRequest
struct RunCloudScriptRequest_t180913386;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_RunCloudScri180913386.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/RunCloudScriptRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void RunCloudScriptRequestCallback__ctor_m610996302 (RunCloudScriptRequestCallback_t2578940735 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/RunCloudScriptRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.RunCloudScriptRequest,System.Object)
extern "C"  void RunCloudScriptRequestCallback_Invoke_m721757147 (RunCloudScriptRequestCallback_t2578940735 * __this, String_t* ___urlPath0, int32_t ___callId1, RunCloudScriptRequest_t180913386 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_RunCloudScriptRequestCallback_t2578940735(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, RunCloudScriptRequest_t180913386 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/RunCloudScriptRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.RunCloudScriptRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * RunCloudScriptRequestCallback_BeginInvoke_m3764821118 (RunCloudScriptRequestCallback_t2578940735 * __this, String_t* ___urlPath0, int32_t ___callId1, RunCloudScriptRequest_t180913386 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/RunCloudScriptRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void RunCloudScriptRequestCallback_EndInvoke_m1786698078 (RunCloudScriptRequestCallback_t2578940735 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
