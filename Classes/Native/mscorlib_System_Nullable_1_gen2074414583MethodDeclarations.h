﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen2074414583.h"
#include "AssemblyU2DCSharp_SponsorPay_SPUserGender3483343971.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Nullable`1<SponsorPay.SPUserGender>::.ctor(T)
extern "C"  void Nullable_1__ctor_m318086284_gshared (Nullable_1_t2074414583 * __this, int32_t ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m318086284(__this, ___value0, method) ((  void (*) (Nullable_1_t2074414583 *, int32_t, const MethodInfo*))Nullable_1__ctor_m318086284_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<SponsorPay.SPUserGender>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m863809844_gshared (Nullable_1_t2074414583 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m863809844(__this, method) ((  bool (*) (Nullable_1_t2074414583 *, const MethodInfo*))Nullable_1_get_HasValue_m863809844_gshared)(__this, method)
// T System.Nullable`1<SponsorPay.SPUserGender>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m2727128153_gshared (Nullable_1_t2074414583 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m2727128153(__this, method) ((  int32_t (*) (Nullable_1_t2074414583 *, const MethodInfo*))Nullable_1_get_Value_m2727128153_gshared)(__this, method)
// System.Boolean System.Nullable`1<SponsorPay.SPUserGender>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m2944519873_gshared (Nullable_1_t2074414583 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m2944519873(__this, ___other0, method) ((  bool (*) (Nullable_1_t2074414583 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m2944519873_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<SponsorPay.SPUserGender>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3820935518_gshared (Nullable_1_t2074414583 * __this, Nullable_1_t2074414583  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m3820935518(__this, ___other0, method) ((  bool (*) (Nullable_1_t2074414583 *, Nullable_1_t2074414583 , const MethodInfo*))Nullable_1_Equals_m3820935518_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<SponsorPay.SPUserGender>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m250414501_gshared (Nullable_1_t2074414583 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m250414501(__this, method) ((  int32_t (*) (Nullable_1_t2074414583 *, const MethodInfo*))Nullable_1_GetHashCode_m250414501_gshared)(__this, method)
// T System.Nullable`1<SponsorPay.SPUserGender>::GetValueOrDefault()
extern "C"  int32_t Nullable_1_GetValueOrDefault_m3799735220_gshared (Nullable_1_t2074414583 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m3799735220(__this, method) ((  int32_t (*) (Nullable_1_t2074414583 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m3799735220_gshared)(__this, method)
// System.String System.Nullable`1<SponsorPay.SPUserGender>::ToString()
extern "C"  String_t* Nullable_1_ToString_m496505473_gshared (Nullable_1_t2074414583 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m496505473(__this, method) ((  String_t* (*) (Nullable_1_t2074414583 *, const MethodInfo*))Nullable_1_ToString_m496505473_gshared)(__this, method)
// System.Nullable`1<T> System.Nullable`1<SponsorPay.SPUserGender>::op_Implicit(T)
extern "C"  Nullable_1_t2074414583  Nullable_1_op_Implicit_m2460643905_gshared (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method);
#define Nullable_1_op_Implicit_m2460643905(__this /* static, unused */, ___value0, method) ((  Nullable_1_t2074414583  (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))Nullable_1_op_Implicit_m2460643905_gshared)(__this /* static, unused */, ___value0, method)
