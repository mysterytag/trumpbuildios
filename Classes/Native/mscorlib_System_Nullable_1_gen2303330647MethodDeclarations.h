﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen2303330647.h"
#include "mscorlib_System_DateTimeOffset3712260035.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Nullable`1<System.DateTimeOffset>::.ctor(T)
extern "C"  void Nullable_1__ctor_m522950596_gshared (Nullable_1_t2303330647 * __this, DateTimeOffset_t3712260035  ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m522950596(__this, ___value0, method) ((  void (*) (Nullable_1_t2303330647 *, DateTimeOffset_t3712260035 , const MethodInfo*))Nullable_1__ctor_m522950596_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<System.DateTimeOffset>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m2074669485_gshared (Nullable_1_t2303330647 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m2074669485(__this, method) ((  bool (*) (Nullable_1_t2303330647 *, const MethodInfo*))Nullable_1_get_HasValue_m2074669485_gshared)(__this, method)
// T System.Nullable`1<System.DateTimeOffset>::get_Value()
extern "C"  DateTimeOffset_t3712260035  Nullable_1_get_Value_m4125167011_gshared (Nullable_1_t2303330647 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m4125167011(__this, method) ((  DateTimeOffset_t3712260035  (*) (Nullable_1_t2303330647 *, const MethodInfo*))Nullable_1_get_Value_m4125167011_gshared)(__this, method)
// System.Boolean System.Nullable`1<System.DateTimeOffset>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1572111781_gshared (Nullable_1_t2303330647 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m1572111781(__this, ___other0, method) ((  bool (*) (Nullable_1_t2303330647 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m1572111781_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<System.DateTimeOffset>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3552532986_gshared (Nullable_1_t2303330647 * __this, Nullable_1_t2303330647  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m3552532986(__this, ___other0, method) ((  bool (*) (Nullable_1_t2303330647 *, Nullable_1_t2303330647 , const MethodInfo*))Nullable_1_Equals_m3552532986_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<System.DateTimeOffset>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m2941497865_gshared (Nullable_1_t2303330647 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m2941497865(__this, method) ((  int32_t (*) (Nullable_1_t2303330647 *, const MethodInfo*))Nullable_1_GetHashCode_m2941497865_gshared)(__this, method)
// T System.Nullable`1<System.DateTimeOffset>::GetValueOrDefault()
extern "C"  DateTimeOffset_t3712260035  Nullable_1_GetValueOrDefault_m959858776_gshared (Nullable_1_t2303330647 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m959858776(__this, method) ((  DateTimeOffset_t3712260035  (*) (Nullable_1_t2303330647 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m959858776_gshared)(__this, method)
// System.String System.Nullable`1<System.DateTimeOffset>::ToString()
extern "C"  String_t* Nullable_1_ToString_m2837501533_gshared (Nullable_1_t2303330647 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m2837501533(__this, method) ((  String_t* (*) (Nullable_1_t2303330647 *, const MethodInfo*))Nullable_1_ToString_m2837501533_gshared)(__this, method)
// System.Nullable`1<T> System.Nullable`1<System.DateTimeOffset>::op_Implicit(T)
extern "C"  Nullable_1_t2303330647  Nullable_1_op_Implicit_m1865326813_gshared (Il2CppObject * __this /* static, unused */, DateTimeOffset_t3712260035  ___value0, const MethodInfo* method);
#define Nullable_1_op_Implicit_m1865326813(__this /* static, unused */, ___value0, method) ((  Nullable_1_t2303330647  (*) (Il2CppObject * /* static, unused */, DateTimeOffset_t3712260035 , const MethodInfo*))Nullable_1_op_Implicit_m1865326813_gshared)(__this /* static, unused */, ___value0, method)
