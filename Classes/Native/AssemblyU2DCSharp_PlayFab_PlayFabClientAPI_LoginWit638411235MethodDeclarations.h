﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/LoginWithAndroidDeviceIDRequestCallback
struct LoginWithAndroidDeviceIDRequestCallback_t638411235;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.LoginWithAndroidDeviceIDRequest
struct LoginWithAndroidDeviceIDRequest_t522798990;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginWithAnd522798990.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/LoginWithAndroidDeviceIDRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void LoginWithAndroidDeviceIDRequestCallback__ctor_m1428629746 (LoginWithAndroidDeviceIDRequestCallback_t638411235 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LoginWithAndroidDeviceIDRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.LoginWithAndroidDeviceIDRequest,System.Object)
extern "C"  void LoginWithAndroidDeviceIDRequestCallback_Invoke_m1909883923 (LoginWithAndroidDeviceIDRequestCallback_t638411235 * __this, String_t* ___urlPath0, int32_t ___callId1, LoginWithAndroidDeviceIDRequest_t522798990 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_LoginWithAndroidDeviceIDRequestCallback_t638411235(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, LoginWithAndroidDeviceIDRequest_t522798990 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/LoginWithAndroidDeviceIDRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.LoginWithAndroidDeviceIDRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LoginWithAndroidDeviceIDRequestCallback_BeginInvoke_m327267518 (LoginWithAndroidDeviceIDRequestCallback_t638411235 * __this, String_t* ___urlPath0, int32_t ___callId1, LoginWithAndroidDeviceIDRequest_t522798990 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LoginWithAndroidDeviceIDRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void LoginWithAndroidDeviceIDRequestCallback_EndInvoke_m4199080962 (LoginWithAndroidDeviceIDRequestCallback_t638411235 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
