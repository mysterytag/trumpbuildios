﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.SelectObservable`2<System.Int64,System.Boolean>
struct SelectObservable_2_t691402772;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1303321368.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.SelectObservable`2/Select_<System.Int64,System.Boolean>
struct  Select__t3288963811  : public OperatorObserverBase_2_t1303321368
{
public:
	// UniRx.Operators.SelectObservable`2<T,TR> UniRx.Operators.SelectObservable`2/Select_::parent
	SelectObservable_2_t691402772 * ___parent_2;
	// System.Int32 UniRx.Operators.SelectObservable`2/Select_::index
	int32_t ___index_3;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(Select__t3288963811, ___parent_2)); }
	inline SelectObservable_2_t691402772 * get_parent_2() const { return ___parent_2; }
	inline SelectObservable_2_t691402772 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(SelectObservable_2_t691402772 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(Select__t3288963811, ___index_3)); }
	inline int32_t get_index_3() const { return ___index_3; }
	inline int32_t* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(int32_t value)
	{
		___index_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
