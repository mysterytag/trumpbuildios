﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Zenject.IInitializable
struct IInitializable_t617891835;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InitializableManager/InitializableInfo
struct  InitializableInfo_t3830632317  : public Il2CppObject
{
public:
	// Zenject.IInitializable Zenject.InitializableManager/InitializableInfo::Initializable
	Il2CppObject * ___Initializable_0;
	// System.Int32 Zenject.InitializableManager/InitializableInfo::Priority
	int32_t ___Priority_1;

public:
	inline static int32_t get_offset_of_Initializable_0() { return static_cast<int32_t>(offsetof(InitializableInfo_t3830632317, ___Initializable_0)); }
	inline Il2CppObject * get_Initializable_0() const { return ___Initializable_0; }
	inline Il2CppObject ** get_address_of_Initializable_0() { return &___Initializable_0; }
	inline void set_Initializable_0(Il2CppObject * value)
	{
		___Initializable_0 = value;
		Il2CppCodeGenWriteBarrier(&___Initializable_0, value);
	}

	inline static int32_t get_offset_of_Priority_1() { return static_cast<int32_t>(offsetof(InitializableInfo_t3830632317, ___Priority_1)); }
	inline int32_t get_Priority_1() const { return ___Priority_1; }
	inline int32_t* get_address_of_Priority_1() { return &___Priority_1; }
	inline void set_Priority_1(int32_t value)
	{
		___Priority_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
