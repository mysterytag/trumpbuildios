﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<LoginWithXbox>c__AnonStorey6A
struct U3CLoginWithXboxU3Ec__AnonStorey6A_t2946184826;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<LoginWithXbox>c__AnonStorey6A::.ctor()
extern "C"  void U3CLoginWithXboxU3Ec__AnonStorey6A__ctor_m3065677337 (U3CLoginWithXboxU3Ec__AnonStorey6A_t2946184826 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<LoginWithXbox>c__AnonStorey6A::<>m__57(PlayFab.CallRequestContainer)
extern "C"  void U3CLoginWithXboxU3Ec__AnonStorey6A_U3CU3Em__57_m1577247321 (U3CLoginWithXboxU3Ec__AnonStorey6A_t2946184826 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
