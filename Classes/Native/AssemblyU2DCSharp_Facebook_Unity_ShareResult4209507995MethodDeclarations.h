﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Facebook.Unity.ShareResult
struct ShareResult_t4209507995;
// Facebook.Unity.ResultContainer
struct ResultContainer_t79372963;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Facebook_Unity_ResultContainer79372963.h"
#include "mscorlib_System_String968488902.h"

// System.Void Facebook.Unity.ShareResult::.ctor(Facebook.Unity.ResultContainer)
extern "C"  void ShareResult__ctor_m3030347613 (ShareResult_t4209507995 * __this, ResultContainer_t79372963 * ___resultContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.ShareResult::get_PostId()
extern "C"  String_t* ShareResult_get_PostId_m2293539755 (ShareResult_t4209507995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Facebook.Unity.ShareResult::set_PostId(System.String)
extern "C"  void ShareResult_set_PostId_m99241862 (ShareResult_t4209507995 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.ShareResult::get_PostIDKey()
extern "C"  String_t* ShareResult_get_PostIDKey_m1621191862 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Facebook.Unity.ShareResult::ToString()
extern "C"  String_t* ShareResult_ToString_m1300431539 (ShareResult_t4209507995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
