﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZergRush.ReactiveCollectionExtensions/<ProcessEach>c__AnonStorey11C`1<System.Object>
struct U3CProcessEachU3Ec__AnonStorey11C_1_t2501101944;
// CollectionReplaceEvent`1<System.Object>
struct CollectionReplaceEvent_1_t2497372070;
// CollectionRemoveEvent`1<System.Object>
struct CollectionRemoveEvent_1_t898259258;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionAddE2416521987.h"

// System.Void ZergRush.ReactiveCollectionExtensions/<ProcessEach>c__AnonStorey11C`1<System.Object>::.ctor()
extern "C"  void U3CProcessEachU3Ec__AnonStorey11C_1__ctor_m3438604360_gshared (U3CProcessEachU3Ec__AnonStorey11C_1_t2501101944 * __this, const MethodInfo* method);
#define U3CProcessEachU3Ec__AnonStorey11C_1__ctor_m3438604360(__this, method) ((  void (*) (U3CProcessEachU3Ec__AnonStorey11C_1_t2501101944 *, const MethodInfo*))U3CProcessEachU3Ec__AnonStorey11C_1__ctor_m3438604360_gshared)(__this, method)
// System.Void ZergRush.ReactiveCollectionExtensions/<ProcessEach>c__AnonStorey11C`1<System.Object>::<>m__1A5(UniRx.Unit)
extern "C"  void U3CProcessEachU3Ec__AnonStorey11C_1_U3CU3Em__1A5_m2736562074_gshared (U3CProcessEachU3Ec__AnonStorey11C_1_t2501101944 * __this, Unit_t2558286038  ___unit0, const MethodInfo* method);
#define U3CProcessEachU3Ec__AnonStorey11C_1_U3CU3Em__1A5_m2736562074(__this, ___unit0, method) ((  void (*) (U3CProcessEachU3Ec__AnonStorey11C_1_t2501101944 *, Unit_t2558286038 , const MethodInfo*))U3CProcessEachU3Ec__AnonStorey11C_1_U3CU3Em__1A5_m2736562074_gshared)(__this, ___unit0, method)
// System.Void ZergRush.ReactiveCollectionExtensions/<ProcessEach>c__AnonStorey11C`1<System.Object>::<>m__1A6(UniRx.CollectionAddEvent`1<T>)
extern "C"  void U3CProcessEachU3Ec__AnonStorey11C_1_U3CU3Em__1A6_m2066254805_gshared (U3CProcessEachU3Ec__AnonStorey11C_1_t2501101944 * __this, CollectionAddEvent_1_t2416521987  ___add0, const MethodInfo* method);
#define U3CProcessEachU3Ec__AnonStorey11C_1_U3CU3Em__1A6_m2066254805(__this, ___add0, method) ((  void (*) (U3CProcessEachU3Ec__AnonStorey11C_1_t2501101944 *, CollectionAddEvent_1_t2416521987 , const MethodInfo*))U3CProcessEachU3Ec__AnonStorey11C_1_U3CU3Em__1A6_m2066254805_gshared)(__this, ___add0, method)
// System.Void ZergRush.ReactiveCollectionExtensions/<ProcessEach>c__AnonStorey11C`1<System.Object>::<>m__1A7(CollectionReplaceEvent`1<T>)
extern "C"  void U3CProcessEachU3Ec__AnonStorey11C_1_U3CU3Em__1A7_m2666331465_gshared (U3CProcessEachU3Ec__AnonStorey11C_1_t2501101944 * __this, CollectionReplaceEvent_1_t2497372070 * ___replaceEvent0, const MethodInfo* method);
#define U3CProcessEachU3Ec__AnonStorey11C_1_U3CU3Em__1A7_m2666331465(__this, ___replaceEvent0, method) ((  void (*) (U3CProcessEachU3Ec__AnonStorey11C_1_t2501101944 *, CollectionReplaceEvent_1_t2497372070 *, const MethodInfo*))U3CProcessEachU3Ec__AnonStorey11C_1_U3CU3Em__1A7_m2666331465_gshared)(__this, ___replaceEvent0, method)
// System.Void ZergRush.ReactiveCollectionExtensions/<ProcessEach>c__AnonStorey11C`1<System.Object>::<>m__1A8(CollectionRemoveEvent`1<T>)
extern "C"  void U3CProcessEachU3Ec__AnonStorey11C_1_U3CU3Em__1A8_m2901653052_gshared (U3CProcessEachU3Ec__AnonStorey11C_1_t2501101944 * __this, CollectionRemoveEvent_1_t898259258 * ___remove0, const MethodInfo* method);
#define U3CProcessEachU3Ec__AnonStorey11C_1_U3CU3Em__1A8_m2901653052(__this, ___remove0, method) ((  void (*) (U3CProcessEachU3Ec__AnonStorey11C_1_t2501101944 *, CollectionRemoveEvent_1_t898259258 *, const MethodInfo*))U3CProcessEachU3Ec__AnonStorey11C_1_U3CU3Em__1A8_m2901653052_gshared)(__this, ___remove0, method)
