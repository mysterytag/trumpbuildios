﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.BinderUntyped
struct BinderUntyped_t2430239676;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// System.Type
struct Type_t;
// System.String
struct String_t;
// Zenject.SingletonProviderMap
struct SingletonProviderMap_t1557411893;
// Zenject.BindingConditionSetter
struct BindingConditionSetter_t259147722;
// System.Func`2<Zenject.InjectContext,System.Object>
struct Func_2_t2621245597;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_Zenject_SingletonProviderMap1557411893.h"

// System.Void Zenject.BinderUntyped::.ctor(Zenject.DiContainer,System.Type,System.String,Zenject.SingletonProviderMap)
extern "C"  void BinderUntyped__ctor_m907613062 (BinderUntyped_t2430239676 * __this, DiContainer_t2383114449 * ___container0, Type_t * ___contractType1, String_t* ___identifier2, SingletonProviderMap_t1557411893 * ___singletonMap3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.BindingConditionSetter Zenject.BinderUntyped::ToMethod(System.Type,System.Func`2<Zenject.InjectContext,System.Object>)
extern "C"  BindingConditionSetter_t259147722 * BinderUntyped_ToMethod_m1993396013 (BinderUntyped_t2430239676 * __this, Type_t * ___returnType0, Func_2_t2621245597 * ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
