﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// UniRx.ReactiveProperty`1/ReactivePropertyObserver<System.Int32>
struct ReactivePropertyObserver_t3083397356;
// UniRx.ReactiveProperty`1<System.Int32>
struct ReactiveProperty_1_t3455747825;
// System.Exception
struct Exception_t1967233988;
// UniRx.ReactiveProperty`1/ReactivePropertyObserver<System.Int64>
struct ReactivePropertyObserver_t3083397451;
// UniRx.ReactiveProperty`1<System.Int64>
struct ReactiveProperty_1_t3455747920;
// UniRx.ReactiveProperty`1/ReactivePropertyObserver<System.Object>
struct ReactivePropertyObserver_t1073088989;
// UniRx.ReactiveProperty`1<System.Object>
struct ReactiveProperty_1_t1445439458;
// System.Object
struct Il2CppObject;
// UniRx.ReactiveProperty`1/ReactivePropertyObserver<System.Single>
struct ReactivePropertyObserver_t1194191590;
// UniRx.ReactiveProperty`1<System.Single>
struct ReactiveProperty_1_t1566542059;
// UniRx.ReactiveProperty`1/ReactivePropertyObserver<TableButtons/StatkaPoTableViev>
struct ReactivePropertyObserver_t3744378386;
// UniRx.ReactiveProperty`1<TableButtons/StatkaPoTableViev>
struct ReactiveProperty_1_t4116728855;
// UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Bounds>
struct ReactivePropertyObserver_t3754497547;
// UniRx.ReactiveProperty`1<UnityEngine.Bounds>
struct ReactiveProperty_1_t4126848016;
// UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Color>
struct ReactivePropertyObserver_t1824158329;
// UniRx.ReactiveProperty`1<UnityEngine.Color>
struct ReactiveProperty_1_t2196508798;
// UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Quaternion>
struct ReactivePropertyObserver_t2127698548;
// UniRx.ReactiveProperty`1<UnityEngine.Quaternion>
struct ReactiveProperty_1_t2500049017;
// UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Rect>
struct ReactivePropertyObserver_t1761411386;
// UniRx.ReactiveProperty`1<UnityEngine.Rect>
struct ReactiveProperty_1_t2133761855;
// UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Vector2>
struct ReactivePropertyObserver_t3761312357;
// UniRx.ReactiveProperty`1<UnityEngine.Vector2>
struct ReactiveProperty_1_t4133662826;
// UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Vector3>
struct ReactivePropertyObserver_t3761312358;
// UniRx.ReactiveProperty`1<UnityEngine.Vector3>
struct ReactiveProperty_1_t4133662827;
// UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Vector4>
struct ReactivePropertyObserver_t3761312359;
// UniRx.ReactiveProperty`1<UnityEngine.Vector4>
struct ReactiveProperty_1_t4133662828;
// UniRx.ReactiveProperty`1<System.Boolean>
struct ReactiveProperty_1_t819338379;
// UniRx.IObservable`1<System.Boolean>
struct IObservable_1_t4264771001;
// System.Collections.Generic.IEqualityComparer`1<System.Boolean>
struct IEqualityComparer_1_t2535271992;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Boolean>
struct IObserver_1_t2423004244;
// System.String
struct String_t;
// UniRx.ReactiveProperty`1<System.Byte>
struct ReactiveProperty_1_t3387026859;
// UniRx.IObservable`1<System.Byte>
struct IObservable_1_t2537492185;
// System.Collections.Generic.IEqualityComparer`1<System.Byte>
struct IEqualityComparer_1_t807993176;
// UniRx.IObserver`1<System.Byte>
struct IObserver_1_t695725428;
// UniRx.ReactiveProperty`1<System.Double>
struct ReactiveProperty_1_t1142849652;
// UniRx.IObservable`1<System.Double>
struct IObservable_1_t293314978;
// System.Collections.Generic.IEqualityComparer`1<System.Double>
struct IEqualityComparer_1_t2858783265;
// UniRx.IObserver`1<System.Double>
struct IObserver_1_t2746515517;
// UniRx.IObservable`1<System.Int32>
struct IObservable_1_t2606213151;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t876714142;
// UniRx.IObserver`1<System.Int32>
struct IObserver_1_t764446394;
// UniRx.IObservable`1<System.Int64>
struct IObservable_1_t2606213246;
// System.Collections.Generic.IEqualityComparer`1<System.Int64>
struct IEqualityComparer_1_t876714237;
// UniRx.IObserver`1<System.Int64>
struct IObserver_1_t764446489;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t3161373071;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// UniRx.IObservable`1<System.Single>
struct IObservable_1_t717007385;
// System.Collections.Generic.IEqualityComparer`1<System.Single>
struct IEqualityComparer_1_t3282475672;
// UniRx.IObserver`1<System.Single>
struct IObserver_1_t3170207924;
// UniRx.IObservable`1<TableButtons/StatkaPoTableViev>
struct IObservable_1_t3267194181;
// System.Collections.Generic.IEqualityComparer`1<TableButtons/StatkaPoTableViev>
struct IEqualityComparer_1_t1537695172;
// UniRx.IObserver`1<TableButtons/StatkaPoTableViev>
struct IObserver_1_t1425427424;
// UniRx.IObservable`1<UnityEngine.Bounds>
struct IObservable_1_t3277313342;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Bounds>
struct IEqualityComparer_1_t1547814333;
// UniRx.IObserver`1<UnityEngine.Bounds>
struct IObserver_1_t1435546585;
// UniRx.IObservable`1<UnityEngine.Color>
struct IObservable_1_t1346974124;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Color>
struct IEqualityComparer_1_t3912442411;
// UniRx.IObserver`1<UnityEngine.Color>
struct IObserver_1_t3800174663;
// UniRx.IObservable`1<UnityEngine.Quaternion>
struct IObservable_1_t1650514343;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Quaternion>
struct IEqualityComparer_1_t4215982630;
// UniRx.IObserver`1<UnityEngine.Quaternion>
struct IObserver_1_t4103714882;
// UniRx.IObservable`1<UnityEngine.Rect>
struct IObservable_1_t1284227181;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Rect>
struct IEqualityComparer_1_t3849695468;
// UniRx.IObserver`1<UnityEngine.Rect>
struct IObserver_1_t3737427720;
// UniRx.IObservable`1<UnityEngine.Vector2>
struct IObservable_1_t3284128152;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Vector2>
struct IEqualityComparer_1_t1554629143;
// UniRx.IObserver`1<UnityEngine.Vector2>
struct IObserver_1_t1442361395;
// UniRx.IObservable`1<UnityEngine.Vector3>
struct IObservable_1_t3284128153;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Vector3>
struct IEqualityComparer_1_t1554629144;
// UniRx.IObserver`1<UnityEngine.Vector3>
struct IObserver_1_t1442361396;
// UniRx.IObservable`1<UnityEngine.Vector4>
struct IObservable_1_t3284128154;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Vector4>
struct IEqualityComparer_1_t1554629145;
// UniRx.IObserver`1<UnityEngine.Vector4>
struct IObserver_1_t1442361397;
// UniRx.ReadOnlyReactiveProperty`1/ReadOnlyReactivePropertyObserver<System.Boolean>
struct ReadOnlyReactivePropertyObserver_t3498060216;
// UniRx.ReadOnlyReactiveProperty`1<System.Boolean>
struct ReadOnlyReactiveProperty_1_t3000407357;
// UniRx.ReadOnlyReactiveProperty`1/ReadOnlyReactivePropertyObserver<System.Object>
struct ReadOnlyReactivePropertyObserver_t4124161295;
// UniRx.ReadOnlyReactiveProperty`1<System.Object>
struct ReadOnlyReactiveProperty_1_t3626508436;
// UniRx.ReadOnlyReactiveProperty`1/ReadOnlyReactivePropertyObserver<UnityEngine.Color>
struct ReadOnlyReactivePropertyObserver_t580263339;
// UniRx.ReadOnlyReactiveProperty`1<UnityEngine.Color>
struct ReadOnlyReactiveProperty_1_t82610480;
// UniRx.ReplaySubject`1/Subscription<System.Object>
struct Subscription_t2911320241;
// UniRx.ReplaySubject`1<System.Object>
struct ReplaySubject_1_t1910820897;
// UniRx.IScheduler
struct IScheduler_t2938318244;
// UniRx.ScheduledNotifier`1/<Report>c__AnonStorey58<System.Object>
struct U3CReportU3Ec__AnonStorey58_t3448642837;
// UniRx.ScheduledNotifier`1/<Report>c__AnonStorey58<System.Single>
struct U3CReportU3Ec__AnonStorey58_t3569745438;
// UniRx.ScheduledNotifier`1/<Report>c__AnonStorey59<System.Object>
struct U3CReportU3Ec__AnonStorey59_t820249710;
// UniRx.ScheduledNotifier`1/<Report>c__AnonStorey59<System.Single>
struct U3CReportU3Ec__AnonStorey59_t941352311;
// UniRx.ScheduledNotifier`1/<Report>c__AnonStorey5A<System.Object>
struct U3CReportU3Ec__AnonStorey5A_t1267941174;
// UniRx.ScheduledNotifier`1/<Report>c__AnonStorey5A<System.Single>
struct U3CReportU3Ec__AnonStorey5A_t1389043775;
// UniRx.ScheduledNotifier`1<System.Object>
struct ScheduledNotifier_1_t2357123231;
// UniRx.ScheduledNotifier`1<System.Single>
struct ScheduledNotifier_1_t2478225832;
// UniRx.Scheduler/ThreadPoolScheduler/<ScheduleQueueing>c__AnonStorey7A`1<System.Object>
struct U3CScheduleQueueingU3Ec__AnonStorey7A_1_t1746579521;
// UniRx.Scheduler/ThreadPoolScheduler/<ScheduleQueueing>c__AnonStorey7A`1<UniRx.Unit>
struct U3CScheduleQueueingU3Ec__AnonStorey7A_1_t3467759139;
// UniRx.Subject`1/Subscription<System.Boolean>
struct Subscription_t2285219159;
// UniRx.Subject`1<System.Boolean>
struct Subject_1_t2149039961;
// UniRx.Subject`1/Subscription<System.Byte>
struct Subscription_t557940343;
// UniRx.Subject`1<System.Byte>
struct Subject_1_t421761145;
// UniRx.Subject`1/Subscription<System.Double>
struct Subscription_t2608730432;
// UniRx.Subject`1<System.Double>
struct Subject_1_t2472551234;
// UniRx.Subject`1/Subscription<System.Int32>
struct Subscription_t626661309;
// UniRx.Subject`1<System.Int32>
struct Subject_1_t490482111;
// UniRx.Subject`1/Subscription<System.Int64>
struct Subscription_t626661404;
// UniRx.Subject`1<System.Int64>
struct Subject_1_t490482206;
// UniRx.Subject`1/Subscription<System.Object>
struct Subscription_t2911320239;
// UniRx.Subject`1<System.Object>
struct Subject_1_t2775141040;
// UniRx.Subject`1/Subscription<System.Single>
struct Subscription_t3032422839;
// UniRx.Subject`1<System.Single>
struct Subject_1_t2896243641;
// UniRx.Subject`1/Subscription<TableButtons/StatkaPoTableViev>
struct Subscription_t1287642339;
// UniRx.Subject`1<TableButtons/StatkaPoTableViev>
struct Subject_1_t1151463141;
// UniRx.Subject`1/Subscription<UniRx.CollectionAddEvent`1<System.Object>>
struct Subscription_t195768509;
// UniRx.Subject`1<UniRx.CollectionAddEvent`1<System.Object>>
struct Subject_1_t59589311;
// UniRx.IObserver`1<UniRx.CollectionAddEvent`1<System.Object>>
struct IObserver_1_t333553594;
// UniRx.Subject`1/Subscription<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct Subscription_t4022891204;
// UniRx.Subject`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct Subject_1_t3886712006;
// UniRx.IObserver`1<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct IObserver_1_t4160676289;
// UniRx.Subject`1/Subscription<UniRx.CollectionMoveEvent`1<System.Object>>
struct Subscription_t695680369;
// UniRx.Subject`1<UniRx.CollectionMoveEvent`1<System.Object>>
struct Subject_1_t559501171;
// UniRx.IObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>
struct IObserver_1_t833465454;
// UniRx.Subject`1/Subscription<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct Subscription_t227835768;
// UniRx.Subject`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct Subject_1_t91656570;
// UniRx.IObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct IObserver_1_t365620853;
// UniRx.Subject`1/Subscription<UniRx.CollectionRemoveEvent`1<System.Object>>
struct Subscription_t2316851542;
// UniRx.Subject`1<UniRx.CollectionRemoveEvent`1<System.Object>>
struct Subject_1_t2180672344;
// UniRx.IObserver`1<UniRx.CollectionRemoveEvent`1<System.Object>>
struct IObserver_1_t2454636627;
// UniRx.Subject`1/Subscription<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct Subscription_t1849006941;
// UniRx.Subject`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct Subject_1_t1712827743;
// UniRx.IObserver`1<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct IObserver_1_t1986792026;
// UniRx.Subject`1/Subscription<UniRx.CollectionReplaceEvent`1<System.Object>>
struct Subscription_t3915964354;
// UniRx.Subject`1<UniRx.CollectionReplaceEvent`1<System.Object>>
struct Subject_1_t3779785156;
// UniRx.IObserver`1<UniRx.CollectionReplaceEvent`1<System.Object>>
struct IObserver_1_t4053749439;
// UniRx.Subject`1/Subscription<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct Subscription_t3448119753;
// UniRx.Subject`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct Subject_1_t3311940555;
// UniRx.IObserver`1<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct IObserver_1_t3585904838;
// UniRx.Subject`1/Subscription<UniRx.CountChangedStatus>
struct Subscription_t1550474243;
// UniRx.Subject`1<UniRx.CountChangedStatus>
struct Subject_1_t1414295045;
// UniRx.IObserver`1<UniRx.CountChangedStatus>
struct IObserver_1_t1688259328;
// UniRx.Subject`1/Subscription<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>
struct Subscription_t2325194826;
// UniRx.Subject`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>
struct Subject_1_t2189015628;
// UniRx.IObserver`1<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>
struct IObserver_1_t2462979911;
// UniRx.Subject`1/Subscription<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>
struct Subscription_t1101534813;
// UniRx.Subject`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>
struct Subject_1_t965355615;
// UniRx.IObserver`1<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>
struct IObserver_1_t1239319898;
// UniRx.Subject`1/Subscription<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>
struct Subscription_t1558173585;
// UniRx.Subject`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>
struct Subject_1_t1421994387;
// UniRx.IObserver`1<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>
struct IObserver_1_t1695958670;
// UniRx.Subject`1/Subscription<UniRx.Tuple`2<System.Object,System.Int32>>
struct Subscription_t158816708;
// UniRx.Subject`1<UniRx.Tuple`2<System.Object,System.Int32>>
struct Subject_1_t22637510;
// UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Int32>>
struct IObserver_1_t296601793;
// UniRx.Subject`1/Subscription<UniRx.Tuple`2<System.Object,System.Object>>
struct Subscription_t2443475637;
// UniRx.Subject`1<UniRx.Tuple`2<System.Object,System.Object>>
struct Subject_1_t2307296439;
// UniRx.IObserver`1<UniRx.Tuple`2<System.Object,System.Object>>
struct IObserver_1_t2581260722;
// UniRx.Subject`1/Subscription<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>
struct Subscription_t4180714101;
// UniRx.Subject`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>
struct Subject_1_t4044534903;
// UniRx.IObserver`1<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>
struct IObserver_1_t23531890;
// UniRx.Subject`1/Subscription<UniRx.Unit>
struct Subscription_t337532561;
// UniRx.Subject`1<UniRx.Unit>
struct Subject_1_t201353362;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;
// UniRx.Subject`1/Subscription<UnityEngine.Bounds>
struct Subscription_t1297761500;
// UniRx.Subject`1<UnityEngine.Bounds>
struct Subject_1_t1161582302;
// UniRx.Subject`1/Subscription<UnityEngine.Color>
struct Subscription_t3662389578;
// UniRx.Subject`1<UnityEngine.Color>
struct Subject_1_t3526210380;
// UniRx.Subject`1/Subscription<UnityEngine.MasterServerEvent>
struct Subscription_t57054012;
// UniRx.Subject`1<UnityEngine.MasterServerEvent>
struct Subject_1_t4215842110;
// UniRx.IObserver`1<UnityEngine.MasterServerEvent>
struct IObserver_1_t194839097;
// UniRx.Subject`1/Subscription<UnityEngine.NetworkConnectionError>
struct Subscription_t3098019811;
// UniRx.Subject`1<UnityEngine.NetworkConnectionError>
struct Subject_1_t2961840613;
// UniRx.IObserver`1<UnityEngine.NetworkConnectionError>
struct IObserver_1_t3235804896;
// UniRx.Subject`1/Subscription<UnityEngine.NetworkDisconnection>
struct Subscription_t2412974213;
// UniRx.Subject`1<UnityEngine.NetworkDisconnection>
struct Subject_1_t2276795015;
// UniRx.IObserver`1<UnityEngine.NetworkDisconnection>
struct IObserver_1_t2550759298;
// UniRx.Subject`1/Subscription<UnityEngine.NetworkMessageInfo>
struct Subscription_t353591406;
// UniRx.Subject`1<UnityEngine.NetworkMessageInfo>
struct Subject_1_t217412208;
// UniRx.IObserver`1<UnityEngine.NetworkMessageInfo>
struct IObserver_1_t491376491;
// UniRx.Subject`1/Subscription<UnityEngine.NetworkPlayer>
struct Subscription_t3355351190;
// UniRx.Subject`1<UnityEngine.NetworkPlayer>
struct Subject_1_t3219171992;
// UniRx.IObserver`1<UnityEngine.NetworkPlayer>
struct IObserver_1_t3493136275;
// UniRx.Subject`1/Subscription<UnityEngine.Quaternion>
struct Subscription_t3965929797;
// UniRx.Subject`1<UnityEngine.Quaternion>
struct Subject_1_t3829750599;
// UniRx.Subject`1/Subscription<UnityEngine.Rect>
struct Subscription_t3599642635;
// UniRx.Subject`1<UnityEngine.Rect>
struct Subject_1_t3463463437;
// UniRx.Subject`1/Subscription<UnityEngine.Vector2>
struct Subscription_t1304576310;
// UniRx.Subject`1<UnityEngine.Vector2>
struct Subject_1_t1168397112;
// UniRx.Subject`1/Subscription<UnityEngine.Vector3>
struct Subscription_t1304576311;
// UniRx.Subject`1<UnityEngine.Vector3>
struct Subject_1_t1168397113;
// UniRx.Subject`1/Subscription<UnityEngine.Vector4>
struct Subscription_t1304576312;
// UniRx.Subject`1<UnityEngine.Vector4>
struct Subject_1_t1168397114;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper3083397356.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper3083397356MethodDeclarations.h"
#include "mscorlib_System_Void2779279689.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper3455747825.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "mscorlib_System_Int322847414787.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper3455747825MethodDeclarations.h"
#include "mscorlib_System_Exception1967233988.h"
#include "mscorlib_System_Threading_Interlocked3007803305MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen490482111.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen490482111MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper3083397451.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper3083397451MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper3455747920.h"
#include "mscorlib_System_Int642847414882.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper3455747920MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen490482206.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen490482206MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper1073088989.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper1073088989MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper1445439458.h"
#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper1445439458MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen2775141040.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen2775141040MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper1194191590.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper1194191590MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper1566542059.h"
#include "mscorlib_System_Single958209021.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper1566542059MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen2896243641.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen2896243641MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper3744378386.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper3744378386MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper4116728855.h"
#include "AssemblyU2DCSharp_TableButtons_StatkaPoTableViev3508395817.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper4116728855MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen1151463141.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen1151463141MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper3754497547.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper3754497547MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper4126848016.h"
#include "UnityEngine_UnityEngine_Bounds3518514978.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper4126848016MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen1161582302.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen1161582302MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper1824158329.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper1824158329MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper2196508798.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper2196508798MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen3526210380.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen3526210380MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper2127698548.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper2127698548MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper2500049017.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper2500049017MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen3829750599.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen3829750599MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper1761411386.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper1761411386MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper2133761855.h"
#include "UnityEngine_UnityEngine_Rect1525428817.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper2133761855MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen3463463437.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen3463463437MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper3761312357.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper3761312357MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper4133662826.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper4133662826MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen1168397112.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen1168397112MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper3761312358.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper3761312358MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper4133662827.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper4133662827MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen1168397113.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen1168397113MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper3761312359.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper3761312359MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper4133662828.h"
#include "UnityEngine_UnityEngine_Vector43525329790.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper4133662828MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen1168397114.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen1168397114MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactivePropert819338379.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactivePropert819338379MethodDeclarations.h"
#include "mscorlib_System_Boolean211005341.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen2149039961.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen2149039961MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactivePropert446987910.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactivePropert446987910MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_UnityEqualityC3801418734MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_UnityEqualityC3801418734.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Disposable3736388786.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Disposable3736388786MethodDeclarations.h"
#include "mscorlib_System_GC2776609905MethodDeclarations.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Boolean211005341MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper3387026859.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper3387026859MethodDeclarations.h"
#include "mscorlib_System_Byte2778693821.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen421761145.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen421761145MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper3014676390.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper3014676390MethodDeclarations.h"
#include "mscorlib_System_Byte2778693821MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper1142849652.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactiveProper1142849652MethodDeclarations.h"
#include "mscorlib_System_Double534516614.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen2472551234.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen2472551234MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactivePropert770499183.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReactivePropert770499183MethodDeclarations.h"
#include "mscorlib_System_Double534516614MethodDeclarations.h"
#include "mscorlib_System_Int322847414787MethodDeclarations.h"
#include "mscorlib_System_Int642847414882MethodDeclarations.h"
#include "mscorlib_System_Single958209021MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Bounds3518514978MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color1588175760MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect1525428817MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector23525329788MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector33525329789MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector43525329790MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReadOnlyReacti3498060216.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReadOnlyReacti3498060216MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReadOnlyReacti3000407357.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReadOnlyReacti4124161295.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReadOnlyReacti4124161295MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReadOnlyReacti3626508436.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReadOnlyReactiv580263339.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReadOnlyReactiv580263339MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReadOnlyReactive82610480.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReadOnlyReacti3000407357MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReadOnlyReacti3626508436MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReadOnlyReactive82610480MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReplaySubject_2911320238.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReplaySubject_2911320238MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReplaySubject_1910820897.h"
#include "mscorlib_System_Threading_Monitor2071304733MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L1131724091.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L1131724091MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Em246459410.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Em246459410MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ReplaySubject_1910820897MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Scheduler_Defa3564383257MethodDeclarations.h"
#include "mscorlib_System_TimeSpan763862892.h"
#include "mscorlib_System_TimeSpan763862892MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException3479058991MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException3214793280MethodDeclarations.h"
#include "System_System_Collections_Generic_Queue_1_gen2416153082.h"
#include "System_System_Collections_Generic_Queue_1_gen2416153082MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException3479058991.h"
#include "mscorlib_System_ArgumentNullException3214793280.h"
#include "mscorlib_System_DateTimeOffset3712260035.h"
#include "mscorlib_System_DateTimeOffset3712260035MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Scheduler103934925MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_TimeInterval_1_708065542.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_TimeInterval_1_708065542MethodDeclarations.h"
#include "System_System_Collections_Generic_Queue_1_Enumerat3885774799.h"
#include "Assembly-CSharp-firstpass_ArrayTypes.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I4109309822.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I4109309822MethodDeclarations.h"
#include "System_System_Collections_Generic_Queue_1_Enumerat3885774799MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Dis11563882.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Dis11563882MethodDeclarations.h"
#include "mscorlib_System_ObjectDisposedException973246880MethodDeclarations.h"
#include "mscorlib_System_String968488902MethodDeclarations.h"
#include "mscorlib_System_ObjectDisposedException973246880.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ScheduledNotif3448642837.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ScheduledNotif3448642837MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ScheduledNotif2357123231.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ScheduledNotif3569745438.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ScheduledNotif3569745438MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ScheduledNotif2478225832.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ScheduledNotifi820249710.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ScheduledNotifi820249710MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ScheduledNotifi941352311.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ScheduledNotifi941352311MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ScheduledNotif1267941174.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ScheduledNotif1267941174MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ScheduledNotif1389043775.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ScheduledNotif1389043775MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ScheduledNotif2357123231MethodDeclarations.h"
#include "System_Core_System_Action437523947MethodDeclarations.h"
#include "System_Core_System_Action437523947.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_ScheduledNotif2478225832MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Scheduler_Igno1329991843.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Scheduler_Igno1329991843MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen985559125MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen985559125.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_3_gen37583746.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_3_gen37583746MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Scheduler_Igno3051171461.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Scheduler_Igno3051171461MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_3_gen3015191994.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_3_gen3015191994MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2706738743.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "mscorlib_System_Action_1_gen2706738743MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Scheduler_Main1329991843.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Scheduler_Main1329991843MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Scheduler_Main3051171461.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Scheduler_Main3051171461MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Scheduler_Thre1746579521.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Scheduler_Thre1746579521MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Scheduler_Thre3467759139.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Scheduler_Thre3467759139MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Stubs_1_gen1554195722.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Stubs_1_gen1554195722MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen359458046.h"
#include "mscorlib_System_Action_1_gen359458046MethodDeclarations.h"
#include "System_Core_System_Func_2_gen1008118516.h"
#include "System_Core_System_Func_2_gen1008118516MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Stubs_1_gen1877706995.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Stubs_1_gen1877706995MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen682969319.h"
#include "mscorlib_System_Action_1_gen682969319MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2869950768.h"
#include "System_Core_System_Func_2_gen2869950768MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Stubs_1_gen4190605263.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Stubs_1_gen4190605263MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2995867587.h"
#include "mscorlib_System_Action_1_gen2995867587MethodDeclarations.h"
#include "System_Core_System_Func_2_gen592778816.h"
#include "System_Core_System_Func_2_gen592778816MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Stubs_1_gen2180296801.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Stubs_1_gen2180296801MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2135783352.h"
#include "System_Core_System_Func_2_gen2135783352MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Stubs_1_gen2301399402.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Stubs_1_gen2301399402MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen1106661726.h"
#include "mscorlib_System_Action_1_gen1106661726MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3464793460.h"
#include "System_Core_System_Func_2_gen3464793460MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Stubs_1_gen1712452200.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Stubs_1_gen1712452200MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen517714524.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen369261819.h"
#include "mscorlib_System_Action_1_gen517714524MethodDeclarations.h"
#include "System_Core_System_Func_2_gen1259381692.h"
#include "System_Core_System_Func_2_gen1259381692MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Stubs_1_gen3901476419.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Stubs_1_gen3901476419MethodDeclarations.h"
#include "System_Core_System_Func_2_gen818424304.h"
#include "System_Core_System_Func_2_gen818424304MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Stubs_1_gen2931366141.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Stubs_1_gen2931366141MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen1736628465.h"
#include "mscorlib_System_Action_1_gen1736628465MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2057442760.h"
#include "System_Core_System_Func_2_gen2057442760MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs2285219159.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs2285219159MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Li505623012.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Li505623012MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E3915325627.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E3915325627MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subsc557940343.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subsc557940343MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L3073311492.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L3073311492MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E2188046811.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E2188046811MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs2608730432.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs2608730432MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Li829134285.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Li829134285MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E4238836900.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E4238836900MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subsc626661309.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subsc626661309MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L3142032458.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L3142032458MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E2256767777.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E2256767777MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subsc626661404.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subsc626661404MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L3142032553.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L3142032553MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E2256767872.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E2256767872MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs2911320238.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs2911320238MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs3032422839.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs3032422839MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L1252826692.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L1252826692MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Em367562011.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Em367562011MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs1287642339.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs1287642339MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L3803013488.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L3803013488MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E2917748807.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E2917748807MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subsc195768509.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subsc195768509MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen59589311.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L2711139658.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L2711139658MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1825874977.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1825874977MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs4022891204.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs4022891204MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen3886712006.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L2243295057.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L2243295057MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1358030376.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1358030376MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subsc695680369.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subsc695680369MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen559501171.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L3211051518.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L3211051518MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E2325786837.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E2325786837MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subsc227835768.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subsc227835768MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen91656570.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L2743206917.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L2743206917MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1857942236.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1857942236MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs2316851542.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs2316851542MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen2180672344.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Li537255395.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Li537255395MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E3946958010.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E3946958010MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs1849006941.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs1849006941MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen1712827743.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Lis69410794.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Lis69410794MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E3479113409.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E3479113409MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs3915964354.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs3915964354MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen3779785156.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L2136368207.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L2136368207MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1251103526.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1251103526MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs3448119753.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs3448119753MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen3311940555.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L1668523606.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L1668523606MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Em783258925.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Em783258925MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs1550474243.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs1550474243MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen1414295045.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L4065845392.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L4065845392MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E3180580711.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E3180580711MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs2325194826.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs2325194826MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen2189015628.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Li545598679.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Li545598679MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E3955301294.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E3955301294MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs1101534813.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs1101534813MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen965355615.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L3616905962.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L3616905962MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E2731641281.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E2731641281MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs1558173585.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs1558173585MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen1421994387.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L4073544734.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L4073544734MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E3188280053.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E3188280053MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subsc158816708.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subsc158816708MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen22637510.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L2674187857.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L2674187857MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1788923176.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1788923176MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs2443475637.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs2443475637MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen2307296439.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Li663879490.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Li663879490MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E4073582105.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E4073582105MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs4180714101.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs4180714101MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen4044534903.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L2401117954.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L2401117954MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1515853273.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1515853273MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subsc337532560.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subsc337532560MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen201353362.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L2852903709.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L2852903709MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1967639028.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1967639028MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs1297761500.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs1297761500MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L3813132649.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L3813132649MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E2927867968.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E2927867968MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs3662389578.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs3662389578MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L1882793431.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L1882793431MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Em997528750.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Em997528750MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subscr57054012.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subscr57054012MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen4215842110.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L2572425161.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L2572425161MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1687160480.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1687160480MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs3098019811.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs3098019811MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen2961840613.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L1318423664.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L1318423664MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Em433158983.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Em433158983MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs2412974213.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs2412974213MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen2276795015.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Li633378066.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Li633378066MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E4043080681.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E4043080681MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subsc353591406.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subsc353591406MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen217412208.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L2868962555.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L2868962555MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1983697874.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1983697874MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs3355351190.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs3355351190MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_gen3219171992.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L1575755043.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L1575755043MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Em690490362.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Em690490362MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs3965929797.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs3965929797MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L2186333650.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L2186333650MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1301068969.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E1301068969MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs3599642635.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs3599642635MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L1820046488.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L1820046488MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Em934781807.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Em934781807MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs1304576310.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs1304576310MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L3819947459.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L3819947459MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E2934682778.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E2934682778MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs1304576311.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs1304576311MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L3819947460.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L3819947460MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E2934682779.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E2934682779MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs1304576312.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Subject_1_Subs1304576312MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L3819947461.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_L3819947461MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E2934682780.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_E2934682780MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I3483208743.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I3483208743MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D3680430099.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D3680430099MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I1755929927.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I1755929927MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D1953151283.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D1953151283MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I3806720016.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I3806720016MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D4003941372.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D4003941372MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I1824650893.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I1824650893MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D2021872249.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D2021872249MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I1824650988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I1824650988MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D2021872344.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_D2021872344MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I4230412423.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_I4230412423MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Di132666483.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_InternalUtil_Di132666483MethodDeclarations.h"

// System.Collections.Generic.IEqualityComparer`1<!!0> UniRx.UnityEqualityComparer::GetDefault<System.Boolean>()
extern "C"  Il2CppObject* UnityEqualityComparer_GetDefault_TisBoolean_t211005341_m2699202742_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define UnityEqualityComparer_GetDefault_TisBoolean_t211005341_m2699202742(__this /* static, unused */, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))UnityEqualityComparer_GetDefault_TisBoolean_t211005341_m2699202742_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEqualityComparer`1<!!0> UniRx.UnityEqualityComparer::GetDefault<System.Byte>()
extern "C"  Il2CppObject* UnityEqualityComparer_GetDefault_TisByte_t2778693821_m2016033784_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define UnityEqualityComparer_GetDefault_TisByte_t2778693821_m2016033784(__this /* static, unused */, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))UnityEqualityComparer_GetDefault_TisByte_t2778693821_m2016033784_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEqualityComparer`1<!!0> UniRx.UnityEqualityComparer::GetDefault<System.Double>()
extern "C"  Il2CppObject* UnityEqualityComparer_GetDefault_TisDouble_t534516614_m1655977039_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define UnityEqualityComparer_GetDefault_TisDouble_t534516614_m1655977039(__this /* static, unused */, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))UnityEqualityComparer_GetDefault_TisDouble_t534516614_m1655977039_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEqualityComparer`1<!!0> UniRx.UnityEqualityComparer::GetDefault<System.Int32>()
extern "C"  Il2CppObject* UnityEqualityComparer_GetDefault_TisInt32_t2847414787_m463157328_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define UnityEqualityComparer_GetDefault_TisInt32_t2847414787_m463157328(__this /* static, unused */, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))UnityEqualityComparer_GetDefault_TisInt32_t2847414787_m463157328_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEqualityComparer`1<!!0> UniRx.UnityEqualityComparer::GetDefault<System.Int64>()
extern "C"  Il2CppObject* UnityEqualityComparer_GetDefault_TisInt64_t2847414882_m465987473_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define UnityEqualityComparer_GetDefault_TisInt64_t2847414882_m465987473(__this /* static, unused */, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))UnityEqualityComparer_GetDefault_TisInt64_t2847414882_m465987473_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEqualityComparer`1<!!0> UniRx.UnityEqualityComparer::GetDefault<System.Object>()
extern "C"  Il2CppObject* UnityEqualityComparer_GetDefault_TisIl2CppObject_m972533281_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define UnityEqualityComparer_GetDefault_TisIl2CppObject_m972533281(__this /* static, unused */, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))UnityEqualityComparer_GetDefault_TisIl2CppObject_m972533281_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEqualityComparer`1<!!0> UniRx.UnityEqualityComparer::GetDefault<System.Single>()
extern "C"  Il2CppObject* UnityEqualityComparer_GetDefault_TisSingle_t958209021_m967591032_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define UnityEqualityComparer_GetDefault_TisSingle_t958209021_m967591032(__this /* static, unused */, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))UnityEqualityComparer_GetDefault_TisSingle_t958209021_m967591032_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEqualityComparer`1<!!0> UniRx.UnityEqualityComparer::GetDefault<TableButtons/StatkaPoTableViev>()
extern "C"  Il2CppObject* UnityEqualityComparer_GetDefault_TisStatkaPoTableViev_t3508395817_m3673930130_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define UnityEqualityComparer_GetDefault_TisStatkaPoTableViev_t3508395817_m3673930130(__this /* static, unused */, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))UnityEqualityComparer_GetDefault_TisStatkaPoTableViev_t3508395817_m3673930130_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEqualityComparer`1<!!0> UniRx.UnityEqualityComparer::GetDefault<UnityEngine.Bounds>()
extern "C"  Il2CppObject* UnityEqualityComparer_GetDefault_TisBounds_t3518514978_m3175276179_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define UnityEqualityComparer_GetDefault_TisBounds_t3518514978_m3175276179(__this /* static, unused */, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))UnityEqualityComparer_GetDefault_TisBounds_t3518514978_m3175276179_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEqualityComparer`1<!!0> UniRx.UnityEqualityComparer::GetDefault<UnityEngine.Color>()
extern "C"  Il2CppObject* UnityEqualityComparer_GetDefault_TisColor_t1588175760_m1034675891_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define UnityEqualityComparer_GetDefault_TisColor_t1588175760_m1034675891(__this /* static, unused */, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))UnityEqualityComparer_GetDefault_TisColor_t1588175760_m1034675891_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEqualityComparer`1<!!0> UniRx.UnityEqualityComparer::GetDefault<UnityEngine.Quaternion>()
extern "C"  Il2CppObject* UnityEqualityComparer_GetDefault_TisQuaternion_t1891715979_m1458833674_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define UnityEqualityComparer_GetDefault_TisQuaternion_t1891715979_m1458833674(__this /* static, unused */, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))UnityEqualityComparer_GetDefault_TisQuaternion_t1891715979_m1458833674_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEqualityComparer`1<!!0> UniRx.UnityEqualityComparer::GetDefault<UnityEngine.Rect>()
extern "C"  Il2CppObject* UnityEqualityComparer_GetDefault_TisRect_t1525428817_m1967640388_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define UnityEqualityComparer_GetDefault_TisRect_t1525428817_m1967640388(__this /* static, unused */, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))UnityEqualityComparer_GetDefault_TisRect_t1525428817_m1967640388_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEqualityComparer`1<!!0> UniRx.UnityEqualityComparer::GetDefault<UnityEngine.Vector2>()
extern "C"  Il2CppObject* UnityEqualityComparer_GetDefault_TisVector2_t3525329788_m946587015_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define UnityEqualityComparer_GetDefault_TisVector2_t3525329788_m946587015(__this /* static, unused */, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))UnityEqualityComparer_GetDefault_TisVector2_t3525329788_m946587015_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEqualityComparer`1<!!0> UniRx.UnityEqualityComparer::GetDefault<UnityEngine.Vector3>()
extern "C"  Il2CppObject* UnityEqualityComparer_GetDefault_TisVector3_t3525329789_m946616806_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define UnityEqualityComparer_GetDefault_TisVector3_t3525329789_m946616806(__this /* static, unused */, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))UnityEqualityComparer_GetDefault_TisVector3_t3525329789_m946616806_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEqualityComparer`1<!!0> UniRx.UnityEqualityComparer::GetDefault<UnityEngine.Vector4>()
extern "C"  Il2CppObject* UnityEqualityComparer_GetDefault_TisVector4_t3525329790_m946646597_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define UnityEqualityComparer_GetDefault_TisVector4_t3525329790_m946646597(__this /* static, unused */, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))UnityEqualityComparer_GetDefault_TisVector4_t3525329790_m946646597_gshared)(__this /* static, unused */, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<System.Int32>::.ctor(UniRx.ReactiveProperty`1<T>)
extern "C"  void ReactivePropertyObserver__ctor_m523330523_gshared (ReactivePropertyObserver_t3083397356 * __this, ReactiveProperty_1_t3455747825 * ___parent0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ReactiveProperty_1_t3455747825 * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<System.Int32>::OnNext(T)
extern "C"  void ReactivePropertyObserver_OnNext_m4087905648_gshared (ReactivePropertyObserver_t3083397356 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		ReactiveProperty_1_t3455747825 * L_0 = (ReactiveProperty_1_t3455747825 *)__this->get_parent_0();
		int32_t L_1 = ___value0;
		NullCheck((ReactiveProperty_1_t3455747825 *)L_0);
		VirtActionInvoker1< int32_t >::Invoke(7 /* System.Void UniRx.ReactiveProperty`1<System.Int32>::set_Value(T) */, (ReactiveProperty_1_t3455747825 *)L_0, (int32_t)L_1);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<System.Int32>::OnError(System.Exception)
extern "C"  void ReactivePropertyObserver_OnError_m3399727231_gshared (ReactivePropertyObserver_t3083397356 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_1();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0022;
		}
	}
	{
		ReactiveProperty_1_t3455747825 * L_2 = (ReactiveProperty_1_t3455747825 *)__this->get_parent_0();
		NullCheck(L_2);
		Subject_1_t490482111 * L_3 = (Subject_1_t490482111 *)L_2->get_publisher_4();
		Exception_t1967233988 * L_4 = ___error0;
		NullCheck((Subject_1_t490482111 *)L_3);
		((  void (*) (Subject_1_t490482111 *, Exception_t1967233988 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Subject_1_t490482111 *)L_3, (Exception_t1967233988 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
	}

IL_0022:
	{
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<System.Int32>::OnCompleted()
extern "C"  void ReactivePropertyObserver_OnCompleted_m475787858_gshared (ReactivePropertyObserver_t3083397356 * __this, const MethodInfo* method)
{
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_1();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0021;
		}
	}
	{
		ReactiveProperty_1_t3455747825 * L_2 = (ReactiveProperty_1_t3455747825 *)__this->get_parent_0();
		NullCheck(L_2);
		Subject_1_t490482111 * L_3 = (Subject_1_t490482111 *)L_2->get_publisher_4();
		NullCheck((Subject_1_t490482111 *)L_3);
		((  void (*) (Subject_1_t490482111 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Subject_1_t490482111 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
	}

IL_0021:
	{
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<System.Int64>::.ctor(UniRx.ReactiveProperty`1<T>)
extern "C"  void ReactivePropertyObserver__ctor_m1020887772_gshared (ReactivePropertyObserver_t3083397451 * __this, ReactiveProperty_1_t3455747920 * ___parent0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ReactiveProperty_1_t3455747920 * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<System.Int64>::OnNext(T)
extern "C"  void ReactivePropertyObserver_OnNext_m1984423759_gshared (ReactivePropertyObserver_t3083397451 * __this, int64_t ___value0, const MethodInfo* method)
{
	{
		ReactiveProperty_1_t3455747920 * L_0 = (ReactiveProperty_1_t3455747920 *)__this->get_parent_0();
		int64_t L_1 = ___value0;
		NullCheck((ReactiveProperty_1_t3455747920 *)L_0);
		VirtActionInvoker1< int64_t >::Invoke(7 /* System.Void UniRx.ReactiveProperty`1<System.Int64>::set_Value(T) */, (ReactiveProperty_1_t3455747920 *)L_0, (int64_t)L_1);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<System.Int64>::OnError(System.Exception)
extern "C"  void ReactivePropertyObserver_OnError_m524911710_gshared (ReactivePropertyObserver_t3083397451 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_1();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0022;
		}
	}
	{
		ReactiveProperty_1_t3455747920 * L_2 = (ReactiveProperty_1_t3455747920 *)__this->get_parent_0();
		NullCheck(L_2);
		Subject_1_t490482206 * L_3 = (Subject_1_t490482206 *)L_2->get_publisher_4();
		Exception_t1967233988 * L_4 = ___error0;
		NullCheck((Subject_1_t490482206 *)L_3);
		((  void (*) (Subject_1_t490482206 *, Exception_t1967233988 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Subject_1_t490482206 *)L_3, (Exception_t1967233988 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
	}

IL_0022:
	{
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<System.Int64>::OnCompleted()
extern "C"  void ReactivePropertyObserver_OnCompleted_m191190193_gshared (ReactivePropertyObserver_t3083397451 * __this, const MethodInfo* method)
{
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_1();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0021;
		}
	}
	{
		ReactiveProperty_1_t3455747920 * L_2 = (ReactiveProperty_1_t3455747920 *)__this->get_parent_0();
		NullCheck(L_2);
		Subject_1_t490482206 * L_3 = (Subject_1_t490482206 *)L_2->get_publisher_4();
		NullCheck((Subject_1_t490482206 *)L_3);
		((  void (*) (Subject_1_t490482206 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Subject_1_t490482206 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
	}

IL_0021:
	{
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<System.Object>::.ctor(UniRx.ReactiveProperty`1<T>)
extern "C"  void ReactivePropertyObserver__ctor_m3636290772_gshared (ReactivePropertyObserver_t1073088989 * __this, ReactiveProperty_1_t1445439458 * ___parent0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ReactiveProperty_1_t1445439458 * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<System.Object>::OnNext(T)
extern "C"  void ReactivePropertyObserver_OnNext_m3640032855_gshared (ReactivePropertyObserver_t1073088989 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		ReactiveProperty_1_t1445439458 * L_0 = (ReactiveProperty_1_t1445439458 *)__this->get_parent_0();
		Il2CppObject * L_1 = ___value0;
		NullCheck((ReactiveProperty_1_t1445439458 *)L_0);
		VirtActionInvoker1< Il2CppObject * >::Invoke(7 /* System.Void UniRx.ReactiveProperty`1<System.Object>::set_Value(T) */, (ReactiveProperty_1_t1445439458 *)L_0, (Il2CppObject *)L_1);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<System.Object>::OnError(System.Exception)
extern "C"  void ReactivePropertyObserver_OnError_m490277734_gshared (ReactivePropertyObserver_t1073088989 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_1();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0022;
		}
	}
	{
		ReactiveProperty_1_t1445439458 * L_2 = (ReactiveProperty_1_t1445439458 *)__this->get_parent_0();
		NullCheck(L_2);
		Subject_1_t2775141040 * L_3 = (Subject_1_t2775141040 *)L_2->get_publisher_4();
		Exception_t1967233988 * L_4 = ___error0;
		NullCheck((Subject_1_t2775141040 *)L_3);
		((  void (*) (Subject_1_t2775141040 *, Exception_t1967233988 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Subject_1_t2775141040 *)L_3, (Exception_t1967233988 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
	}

IL_0022:
	{
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<System.Object>::OnCompleted()
extern "C"  void ReactivePropertyObserver_OnCompleted_m3076597689_gshared (ReactivePropertyObserver_t1073088989 * __this, const MethodInfo* method)
{
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_1();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0021;
		}
	}
	{
		ReactiveProperty_1_t1445439458 * L_2 = (ReactiveProperty_1_t1445439458 *)__this->get_parent_0();
		NullCheck(L_2);
		Subject_1_t2775141040 * L_3 = (Subject_1_t2775141040 *)L_2->get_publisher_4();
		NullCheck((Subject_1_t2775141040 *)L_3);
		((  void (*) (Subject_1_t2775141040 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Subject_1_t2775141040 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
	}

IL_0021:
	{
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<System.Single>::.ctor(UniRx.ReactiveProperty`1<T>)
extern "C"  void ReactivePropertyObserver__ctor_m3749029227_gshared (ReactivePropertyObserver_t1194191590 * __this, ReactiveProperty_1_t1566542059 * ___parent0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ReactiveProperty_1_t1566542059 * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<System.Single>::OnNext(T)
extern "C"  void ReactivePropertyObserver_OnNext_m1358961120_gshared (ReactivePropertyObserver_t1194191590 * __this, float ___value0, const MethodInfo* method)
{
	{
		ReactiveProperty_1_t1566542059 * L_0 = (ReactiveProperty_1_t1566542059 *)__this->get_parent_0();
		float L_1 = ___value0;
		NullCheck((ReactiveProperty_1_t1566542059 *)L_0);
		VirtActionInvoker1< float >::Invoke(7 /* System.Void UniRx.ReactiveProperty`1<System.Single>::set_Value(T) */, (ReactiveProperty_1_t1566542059 *)L_0, (float)L_1);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<System.Single>::OnError(System.Exception)
extern "C"  void ReactivePropertyObserver_OnError_m1737782511_gshared (ReactivePropertyObserver_t1194191590 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_1();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0022;
		}
	}
	{
		ReactiveProperty_1_t1566542059 * L_2 = (ReactiveProperty_1_t1566542059 *)__this->get_parent_0();
		NullCheck(L_2);
		Subject_1_t2896243641 * L_3 = (Subject_1_t2896243641 *)L_2->get_publisher_4();
		Exception_t1967233988 * L_4 = ___error0;
		NullCheck((Subject_1_t2896243641 *)L_3);
		((  void (*) (Subject_1_t2896243641 *, Exception_t1967233988 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Subject_1_t2896243641 *)L_3, (Exception_t1967233988 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
	}

IL_0022:
	{
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<System.Single>::OnCompleted()
extern "C"  void ReactivePropertyObserver_OnCompleted_m2460997314_gshared (ReactivePropertyObserver_t1194191590 * __this, const MethodInfo* method)
{
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_1();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0021;
		}
	}
	{
		ReactiveProperty_1_t1566542059 * L_2 = (ReactiveProperty_1_t1566542059 *)__this->get_parent_0();
		NullCheck(L_2);
		Subject_1_t2896243641 * L_3 = (Subject_1_t2896243641 *)L_2->get_publisher_4();
		NullCheck((Subject_1_t2896243641 *)L_3);
		((  void (*) (Subject_1_t2896243641 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Subject_1_t2896243641 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
	}

IL_0021:
	{
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<TableButtons/StatkaPoTableViev>::.ctor(UniRx.ReactiveProperty`1<T>)
extern "C"  void ReactivePropertyObserver__ctor_m949315997_gshared (ReactivePropertyObserver_t3744378386 * __this, ReactiveProperty_1_t4116728855 * ___parent0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ReactiveProperty_1_t4116728855 * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<TableButtons/StatkaPoTableViev>::OnNext(T)
extern "C"  void ReactivePropertyObserver_OnNext_m4031271278_gshared (ReactivePropertyObserver_t3744378386 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		ReactiveProperty_1_t4116728855 * L_0 = (ReactiveProperty_1_t4116728855 *)__this->get_parent_0();
		int32_t L_1 = ___value0;
		NullCheck((ReactiveProperty_1_t4116728855 *)L_0);
		VirtActionInvoker1< int32_t >::Invoke(7 /* System.Void UniRx.ReactiveProperty`1<TableButtons/StatkaPoTableViev>::set_Value(T) */, (ReactiveProperty_1_t4116728855 *)L_0, (int32_t)L_1);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<TableButtons/StatkaPoTableViev>::OnError(System.Exception)
extern "C"  void ReactivePropertyObserver_OnError_m3016216701_gshared (ReactivePropertyObserver_t3744378386 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_1();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0022;
		}
	}
	{
		ReactiveProperty_1_t4116728855 * L_2 = (ReactiveProperty_1_t4116728855 *)__this->get_parent_0();
		NullCheck(L_2);
		Subject_1_t1151463141 * L_3 = (Subject_1_t1151463141 *)L_2->get_publisher_4();
		Exception_t1967233988 * L_4 = ___error0;
		NullCheck((Subject_1_t1151463141 *)L_3);
		((  void (*) (Subject_1_t1151463141 *, Exception_t1967233988 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Subject_1_t1151463141 *)L_3, (Exception_t1967233988 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
	}

IL_0022:
	{
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<TableButtons/StatkaPoTableViev>::OnCompleted()
extern "C"  void ReactivePropertyObserver_OnCompleted_m1557501776_gshared (ReactivePropertyObserver_t3744378386 * __this, const MethodInfo* method)
{
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_1();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0021;
		}
	}
	{
		ReactiveProperty_1_t4116728855 * L_2 = (ReactiveProperty_1_t4116728855 *)__this->get_parent_0();
		NullCheck(L_2);
		Subject_1_t1151463141 * L_3 = (Subject_1_t1151463141 *)L_2->get_publisher_4();
		NullCheck((Subject_1_t1151463141 *)L_3);
		((  void (*) (Subject_1_t1151463141 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Subject_1_t1151463141 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
	}

IL_0021:
	{
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Bounds>::.ctor(UniRx.ReactiveProperty`1<T>)
extern "C"  void ReactivePropertyObserver__ctor_m4114333790_gshared (ReactivePropertyObserver_t3754497547 * __this, ReactiveProperty_1_t4126848016 * ___parent0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ReactiveProperty_1_t4126848016 * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Bounds>::OnNext(T)
extern "C"  void ReactivePropertyObserver_OnNext_m1946176397_gshared (ReactivePropertyObserver_t3754497547 * __this, Bounds_t3518514978  ___value0, const MethodInfo* method)
{
	{
		ReactiveProperty_1_t4126848016 * L_0 = (ReactiveProperty_1_t4126848016 *)__this->get_parent_0();
		Bounds_t3518514978  L_1 = ___value0;
		NullCheck((ReactiveProperty_1_t4126848016 *)L_0);
		VirtActionInvoker1< Bounds_t3518514978  >::Invoke(7 /* System.Void UniRx.ReactiveProperty`1<UnityEngine.Bounds>::set_Value(T) */, (ReactiveProperty_1_t4126848016 *)L_0, (Bounds_t3518514978 )L_1);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Bounds>::OnError(System.Exception)
extern "C"  void ReactivePropertyObserver_OnError_m2430446748_gshared (ReactivePropertyObserver_t3754497547 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_1();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0022;
		}
	}
	{
		ReactiveProperty_1_t4126848016 * L_2 = (ReactiveProperty_1_t4126848016 *)__this->get_parent_0();
		NullCheck(L_2);
		Subject_1_t1161582302 * L_3 = (Subject_1_t1161582302 *)L_2->get_publisher_4();
		Exception_t1967233988 * L_4 = ___error0;
		NullCheck((Subject_1_t1161582302 *)L_3);
		((  void (*) (Subject_1_t1161582302 *, Exception_t1967233988 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Subject_1_t1161582302 *)L_3, (Exception_t1967233988 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
	}

IL_0022:
	{
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Bounds>::OnCompleted()
extern "C"  void ReactivePropertyObserver_OnCompleted_m4055198191_gshared (ReactivePropertyObserver_t3754497547 * __this, const MethodInfo* method)
{
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_1();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0021;
		}
	}
	{
		ReactiveProperty_1_t4126848016 * L_2 = (ReactiveProperty_1_t4126848016 *)__this->get_parent_0();
		NullCheck(L_2);
		Subject_1_t1161582302 * L_3 = (Subject_1_t1161582302 *)L_2->get_publisher_4();
		NullCheck((Subject_1_t1161582302 *)L_3);
		((  void (*) (Subject_1_t1161582302 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Subject_1_t1161582302 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
	}

IL_0021:
	{
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Color>::.ctor(UniRx.ReactiveProperty`1<T>)
extern "C"  void ReactivePropertyObserver__ctor_m910925542_gshared (ReactivePropertyObserver_t1824158329 * __this, ReactiveProperty_1_t2196508798 * ___parent0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ReactiveProperty_1_t2196508798 * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Color>::OnNext(T)
extern "C"  void ReactivePropertyObserver_OnNext_m3654968325_gshared (ReactivePropertyObserver_t1824158329 * __this, Color_t1588175760  ___value0, const MethodInfo* method)
{
	{
		ReactiveProperty_1_t2196508798 * L_0 = (ReactiveProperty_1_t2196508798 *)__this->get_parent_0();
		Color_t1588175760  L_1 = ___value0;
		NullCheck((ReactiveProperty_1_t2196508798 *)L_0);
		VirtActionInvoker1< Color_t1588175760  >::Invoke(7 /* System.Void UniRx.ReactiveProperty`1<UnityEngine.Color>::set_Value(T) */, (ReactiveProperty_1_t2196508798 *)L_0, (Color_t1588175760 )L_1);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Color>::OnError(System.Exception)
extern "C"  void ReactivePropertyObserver_OnError_m3207058708_gshared (ReactivePropertyObserver_t1824158329 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_1();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0022;
		}
	}
	{
		ReactiveProperty_1_t2196508798 * L_2 = (ReactiveProperty_1_t2196508798 *)__this->get_parent_0();
		NullCheck(L_2);
		Subject_1_t3526210380 * L_3 = (Subject_1_t3526210380 *)L_2->get_publisher_4();
		Exception_t1967233988 * L_4 = ___error0;
		NullCheck((Subject_1_t3526210380 *)L_3);
		((  void (*) (Subject_1_t3526210380 *, Exception_t1967233988 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Subject_1_t3526210380 *)L_3, (Exception_t1967233988 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
	}

IL_0022:
	{
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Color>::OnCompleted()
extern "C"  void ReactivePropertyObserver_OnCompleted_m861832807_gshared (ReactivePropertyObserver_t1824158329 * __this, const MethodInfo* method)
{
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_1();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0021;
		}
	}
	{
		ReactiveProperty_1_t2196508798 * L_2 = (ReactiveProperty_1_t2196508798 *)__this->get_parent_0();
		NullCheck(L_2);
		Subject_1_t3526210380 * L_3 = (Subject_1_t3526210380 *)L_2->get_publisher_4();
		NullCheck((Subject_1_t3526210380 *)L_3);
		((  void (*) (Subject_1_t3526210380 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Subject_1_t3526210380 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
	}

IL_0021:
	{
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Quaternion>::.ctor(UniRx.ReactiveProperty`1<T>)
extern "C"  void ReactivePropertyObserver__ctor_m240166677_gshared (ReactivePropertyObserver_t2127698548 * __this, ReactiveProperty_1_t2500049017 * ___parent0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ReactiveProperty_1_t2500049017 * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Quaternion>::OnNext(T)
extern "C"  void ReactivePropertyObserver_OnNext_m414948598_gshared (ReactivePropertyObserver_t2127698548 * __this, Quaternion_t1891715979  ___value0, const MethodInfo* method)
{
	{
		ReactiveProperty_1_t2500049017 * L_0 = (ReactiveProperty_1_t2500049017 *)__this->get_parent_0();
		Quaternion_t1891715979  L_1 = ___value0;
		NullCheck((ReactiveProperty_1_t2500049017 *)L_0);
		VirtActionInvoker1< Quaternion_t1891715979  >::Invoke(7 /* System.Void UniRx.ReactiveProperty`1<UnityEngine.Quaternion>::set_Value(T) */, (ReactiveProperty_1_t2500049017 *)L_0, (Quaternion_t1891715979 )L_1);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Quaternion>::OnError(System.Exception)
extern "C"  void ReactivePropertyObserver_OnError_m881003525_gshared (ReactivePropertyObserver_t2127698548 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_1();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0022;
		}
	}
	{
		ReactiveProperty_1_t2500049017 * L_2 = (ReactiveProperty_1_t2500049017 *)__this->get_parent_0();
		NullCheck(L_2);
		Subject_1_t3829750599 * L_3 = (Subject_1_t3829750599 *)L_2->get_publisher_4();
		Exception_t1967233988 * L_4 = ___error0;
		NullCheck((Subject_1_t3829750599 *)L_3);
		((  void (*) (Subject_1_t3829750599 *, Exception_t1967233988 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Subject_1_t3829750599 *)L_3, (Exception_t1967233988 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
	}

IL_0022:
	{
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Quaternion>::OnCompleted()
extern "C"  void ReactivePropertyObserver_OnCompleted_m1009245912_gshared (ReactivePropertyObserver_t2127698548 * __this, const MethodInfo* method)
{
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_1();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0021;
		}
	}
	{
		ReactiveProperty_1_t2500049017 * L_2 = (ReactiveProperty_1_t2500049017 *)__this->get_parent_0();
		NullCheck(L_2);
		Subject_1_t3829750599 * L_3 = (Subject_1_t3829750599 *)L_2->get_publisher_4();
		NullCheck((Subject_1_t3829750599 *)L_3);
		((  void (*) (Subject_1_t3829750599 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Subject_1_t3829750599 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
	}

IL_0021:
	{
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Rect>::.ctor(UniRx.ReactiveProperty`1<T>)
extern "C"  void ReactivePropertyObserver__ctor_m1486346191_gshared (ReactivePropertyObserver_t1761411386 * __this, ReactiveProperty_1_t2133761855 * ___parent0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ReactiveProperty_1_t2133761855 * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Rect>::OnNext(T)
extern "C"  void ReactivePropertyObserver_OnNext_m739417852_gshared (ReactivePropertyObserver_t1761411386 * __this, Rect_t1525428817  ___value0, const MethodInfo* method)
{
	{
		ReactiveProperty_1_t2133761855 * L_0 = (ReactiveProperty_1_t2133761855 *)__this->get_parent_0();
		Rect_t1525428817  L_1 = ___value0;
		NullCheck((ReactiveProperty_1_t2133761855 *)L_0);
		VirtActionInvoker1< Rect_t1525428817  >::Invoke(7 /* System.Void UniRx.ReactiveProperty`1<UnityEngine.Rect>::set_Value(T) */, (ReactiveProperty_1_t2133761855 *)L_0, (Rect_t1525428817 )L_1);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Rect>::OnError(System.Exception)
extern "C"  void ReactivePropertyObserver_OnError_m4052828683_gshared (ReactivePropertyObserver_t1761411386 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_1();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0022;
		}
	}
	{
		ReactiveProperty_1_t2133761855 * L_2 = (ReactiveProperty_1_t2133761855 *)__this->get_parent_0();
		NullCheck(L_2);
		Subject_1_t3463463437 * L_3 = (Subject_1_t3463463437 *)L_2->get_publisher_4();
		Exception_t1967233988 * L_4 = ___error0;
		NullCheck((Subject_1_t3463463437 *)L_3);
		((  void (*) (Subject_1_t3463463437 *, Exception_t1967233988 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Subject_1_t3463463437 *)L_3, (Exception_t1967233988 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
	}

IL_0022:
	{
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Rect>::OnCompleted()
extern "C"  void ReactivePropertyObserver_OnCompleted_m3900861918_gshared (ReactivePropertyObserver_t1761411386 * __this, const MethodInfo* method)
{
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_1();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0021;
		}
	}
	{
		ReactiveProperty_1_t2133761855 * L_2 = (ReactiveProperty_1_t2133761855 *)__this->get_parent_0();
		NullCheck(L_2);
		Subject_1_t3463463437 * L_3 = (Subject_1_t3463463437 *)L_2->get_publisher_4();
		NullCheck((Subject_1_t3463463437 *)L_3);
		((  void (*) (Subject_1_t3463463437 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Subject_1_t3463463437 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
	}

IL_0021:
	{
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Vector2>::.ctor(UniRx.ReactiveProperty`1<T>)
extern "C"  void ReactivePropertyObserver__ctor_m2834758586_gshared (ReactivePropertyObserver_t3761312357 * __this, ReactiveProperty_1_t4133662826 * ___parent0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ReactiveProperty_1_t4133662826 * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Vector2>::OnNext(T)
extern "C"  void ReactivePropertyObserver_OnNext_m359030961_gshared (ReactivePropertyObserver_t3761312357 * __this, Vector2_t3525329788  ___value0, const MethodInfo* method)
{
	{
		ReactiveProperty_1_t4133662826 * L_0 = (ReactiveProperty_1_t4133662826 *)__this->get_parent_0();
		Vector2_t3525329788  L_1 = ___value0;
		NullCheck((ReactiveProperty_1_t4133662826 *)L_0);
		VirtActionInvoker1< Vector2_t3525329788  >::Invoke(7 /* System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector2>::set_Value(T) */, (ReactiveProperty_1_t4133662826 *)L_0, (Vector2_t3525329788 )L_1);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Vector2>::OnError(System.Exception)
extern "C"  void ReactivePropertyObserver_OnError_m3982293440_gshared (ReactivePropertyObserver_t3761312357 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_1();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0022;
		}
	}
	{
		ReactiveProperty_1_t4133662826 * L_2 = (ReactiveProperty_1_t4133662826 *)__this->get_parent_0();
		NullCheck(L_2);
		Subject_1_t1168397112 * L_3 = (Subject_1_t1168397112 *)L_2->get_publisher_4();
		Exception_t1967233988 * L_4 = ___error0;
		NullCheck((Subject_1_t1168397112 *)L_3);
		((  void (*) (Subject_1_t1168397112 *, Exception_t1967233988 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Subject_1_t1168397112 *)L_3, (Exception_t1967233988 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
	}

IL_0022:
	{
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Vector2>::OnCompleted()
extern "C"  void ReactivePropertyObserver_OnCompleted_m2583973139_gshared (ReactivePropertyObserver_t3761312357 * __this, const MethodInfo* method)
{
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_1();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0021;
		}
	}
	{
		ReactiveProperty_1_t4133662826 * L_2 = (ReactiveProperty_1_t4133662826 *)__this->get_parent_0();
		NullCheck(L_2);
		Subject_1_t1168397112 * L_3 = (Subject_1_t1168397112 *)L_2->get_publisher_4();
		NullCheck((Subject_1_t1168397112 *)L_3);
		((  void (*) (Subject_1_t1168397112 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Subject_1_t1168397112 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
	}

IL_0021:
	{
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Vector3>::.ctor(UniRx.ReactiveProperty`1<T>)
extern "C"  void ReactivePropertyObserver__ctor_m172595289_gshared (ReactivePropertyObserver_t3761312358 * __this, ReactiveProperty_1_t4133662827 * ___parent0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ReactiveProperty_1_t4133662827 * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Vector3>::OnNext(T)
extern "C"  void ReactivePropertyObserver_OnNext_m65627954_gshared (ReactivePropertyObserver_t3761312358 * __this, Vector3_t3525329789  ___value0, const MethodInfo* method)
{
	{
		ReactiveProperty_1_t4133662827 * L_0 = (ReactiveProperty_1_t4133662827 *)__this->get_parent_0();
		Vector3_t3525329789  L_1 = ___value0;
		NullCheck((ReactiveProperty_1_t4133662827 *)L_0);
		VirtActionInvoker1< Vector3_t3525329789  >::Invoke(7 /* System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector3>::set_Value(T) */, (ReactiveProperty_1_t4133662827 *)L_0, (Vector3_t3525329789 )L_1);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Vector3>::OnError(System.Exception)
extern "C"  void ReactivePropertyObserver_OnError_m380427841_gshared (ReactivePropertyObserver_t3761312358 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_1();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0022;
		}
	}
	{
		ReactiveProperty_1_t4133662827 * L_2 = (ReactiveProperty_1_t4133662827 *)__this->get_parent_0();
		NullCheck(L_2);
		Subject_1_t1168397113 * L_3 = (Subject_1_t1168397113 *)L_2->get_publisher_4();
		Exception_t1967233988 * L_4 = ___error0;
		NullCheck((Subject_1_t1168397113 *)L_3);
		((  void (*) (Subject_1_t1168397113 *, Exception_t1967233988 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Subject_1_t1168397113 *)L_3, (Exception_t1967233988 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
	}

IL_0022:
	{
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Vector3>::OnCompleted()
extern "C"  void ReactivePropertyObserver_OnCompleted_m3937282836_gshared (ReactivePropertyObserver_t3761312358 * __this, const MethodInfo* method)
{
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_1();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0021;
		}
	}
	{
		ReactiveProperty_1_t4133662827 * L_2 = (ReactiveProperty_1_t4133662827 *)__this->get_parent_0();
		NullCheck(L_2);
		Subject_1_t1168397113 * L_3 = (Subject_1_t1168397113 *)L_2->get_publisher_4();
		NullCheck((Subject_1_t1168397113 *)L_3);
		((  void (*) (Subject_1_t1168397113 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Subject_1_t1168397113 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
	}

IL_0021:
	{
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Vector4>::.ctor(UniRx.ReactiveProperty`1<T>)
extern "C"  void ReactivePropertyObserver__ctor_m1805399288_gshared (ReactivePropertyObserver_t3761312359 * __this, ReactiveProperty_1_t4133662828 * ___parent0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ReactiveProperty_1_t4133662828 * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Vector4>::OnNext(T)
extern "C"  void ReactivePropertyObserver_OnNext_m4067192243_gshared (ReactivePropertyObserver_t3761312359 * __this, Vector4_t3525329790  ___value0, const MethodInfo* method)
{
	{
		ReactiveProperty_1_t4133662828 * L_0 = (ReactiveProperty_1_t4133662828 *)__this->get_parent_0();
		Vector4_t3525329790  L_1 = ___value0;
		NullCheck((ReactiveProperty_1_t4133662828 *)L_0);
		VirtActionInvoker1< Vector4_t3525329790  >::Invoke(7 /* System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector4>::set_Value(T) */, (ReactiveProperty_1_t4133662828 *)L_0, (Vector4_t3525329790 )L_1);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Vector4>::OnError(System.Exception)
extern "C"  void ReactivePropertyObserver_OnError_m1073529538_gshared (ReactivePropertyObserver_t3761312359 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_1();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0022;
		}
	}
	{
		ReactiveProperty_1_t4133662828 * L_2 = (ReactiveProperty_1_t4133662828 *)__this->get_parent_0();
		NullCheck(L_2);
		Subject_1_t1168397114 * L_3 = (Subject_1_t1168397114 *)L_2->get_publisher_4();
		Exception_t1967233988 * L_4 = ___error0;
		NullCheck((Subject_1_t1168397114 *)L_3);
		((  void (*) (Subject_1_t1168397114 *, Exception_t1967233988 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Subject_1_t1168397114 *)L_3, (Exception_t1967233988 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
	}

IL_0022:
	{
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1/ReactivePropertyObserver<UnityEngine.Vector4>::OnCompleted()
extern "C"  void ReactivePropertyObserver_OnCompleted_m995625237_gshared (ReactivePropertyObserver_t3761312359 * __this, const MethodInfo* method)
{
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_1();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0021;
		}
	}
	{
		ReactiveProperty_1_t4133662828 * L_2 = (ReactiveProperty_1_t4133662828 *)__this->get_parent_0();
		NullCheck(L_2);
		Subject_1_t1168397114 * L_3 = (Subject_1_t1168397114 *)L_2->get_publisher_4();
		NullCheck((Subject_1_t1168397114 *)L_3);
		((  void (*) (Subject_1_t1168397114 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Subject_1_t1168397114 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
	}

IL_0021:
	{
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Boolean>::.ctor()
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m3742808319_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m3742808319_gshared (ReactiveProperty_1_t819338379 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m3742808319_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		Initobj (Boolean_t211005341_il2cpp_TypeInfo_var, (&V_0));
		bool L_0 = V_0;
		NullCheck((ReactiveProperty_1_t819338379 *)__this);
		((  void (*) (ReactiveProperty_1_t819338379 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ReactiveProperty_1_t819338379 *)__this, (bool)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Boolean>::.ctor(T)
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m62942271_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m62942271_gshared (ReactiveProperty_1_t819338379 * __this, bool ___initialValue0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m62942271_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		Initobj (Boolean_t211005341_il2cpp_TypeInfo_var, (&V_0));
		bool L_0 = V_0;
		__this->set_value_3(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		bool L_1 = ___initialValue0;
		NullCheck((ReactiveProperty_1_t819338379 *)__this);
		VirtActionInvoker1< bool >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<System.Boolean>::SetValue(T) */, (ReactiveProperty_1_t819338379 *)__this, (bool)L_1);
		__this->set_canPublishValueOnSubscribe_1((bool)1);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Boolean>::.ctor(UniRx.IObservable`1<T>)
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m1217349022_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m1217349022_gshared (ReactiveProperty_1_t819338379 * __this, Il2CppObject* ___source0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m1217349022_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		Initobj (Boolean_t211005341_il2cpp_TypeInfo_var, (&V_0));
		bool L_0 = V_0;
		__this->set_value_3(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		__this->set_canPublishValueOnSubscribe_1((bool)0);
		Subject_1_t2149039961 * L_1 = (Subject_1_t2149039961 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Subject_1_t2149039961 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_publisher_4(L_1);
		Il2CppObject* L_2 = ___source0;
		ReactivePropertyObserver_t446987910 * L_3 = (ReactivePropertyObserver_t446987910 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (ReactivePropertyObserver_t446987910 *, ReactiveProperty_1_t819338379 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_3, (ReactiveProperty_1_t819338379 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_4 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Boolean>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_2, (Il2CppObject*)L_3);
		__this->set_sourceConnection_5(L_4);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Boolean>::.ctor(UniRx.IObservable`1<T>,T)
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m1641311158_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m1641311158_gshared (ReactiveProperty_1_t819338379 * __this, Il2CppObject* ___source0, bool ___initialValue1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m1641311158_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		Initobj (Boolean_t211005341_il2cpp_TypeInfo_var, (&V_0));
		bool L_0 = V_0;
		__this->set_value_3(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		__this->set_canPublishValueOnSubscribe_1((bool)0);
		bool L_1 = ___initialValue1;
		NullCheck((ReactiveProperty_1_t819338379 *)__this);
		VirtActionInvoker1< bool >::Invoke(7 /* System.Void UniRx.ReactiveProperty`1<System.Boolean>::set_Value(T) */, (ReactiveProperty_1_t819338379 *)__this, (bool)L_1);
		Subject_1_t2149039961 * L_2 = (Subject_1_t2149039961 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Subject_1_t2149039961 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_publisher_4(L_2);
		Il2CppObject* L_3 = ___source0;
		ReactivePropertyObserver_t446987910 * L_4 = (ReactivePropertyObserver_t446987910 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (ReactivePropertyObserver_t446987910 *, ReactiveProperty_1_t819338379 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_4, (ReactiveProperty_1_t819338379 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((Il2CppObject*)L_3);
		Il2CppObject * L_5 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Boolean>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_3, (Il2CppObject*)L_4);
		__this->set_sourceConnection_5(L_5);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Boolean>::.cctor()
extern Il2CppClass* UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__cctor_m3875811982_MetadataUsageId;
extern "C"  void ReactiveProperty_1__cctor_m3875811982_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__cctor_m3875811982_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		((ReactiveProperty_1_t819338379_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->static_fields)->set_defaultEqualityComparer_0(L_0);
		return;
	}
}
// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1<System.Boolean>::get_EqualityComparer()
extern "C"  Il2CppObject* ReactiveProperty_1_get_EqualityComparer_m2360157460_gshared (ReactiveProperty_1_t819338379 * __this, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		Il2CppObject* L_0 = ((ReactiveProperty_1_t819338379_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->static_fields)->get_defaultEqualityComparer_0();
		return L_0;
	}
}
// T UniRx.ReactiveProperty`1<System.Boolean>::get_Value()
extern "C"  bool ReactiveProperty_1_get_Value_m3883642950_gshared (ReactiveProperty_1_t819338379 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_value_3();
		return L_0;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Boolean>::set_Value(T)
extern "C"  void ReactiveProperty_1_set_Value_m2843367693_gshared (ReactiveProperty_1_t819338379 * __this, bool ___value0, const MethodInfo* method)
{
	Subject_1_t2149039961 * V_0 = NULL;
	Subject_1_t2149039961 * V_1 = NULL;
	{
		bool L_0 = (bool)__this->get_canPublishValueOnSubscribe_1();
		if (L_0)
		{
			goto IL_003f;
		}
	}
	{
		__this->set_canPublishValueOnSubscribe_1((bool)1);
		bool L_1 = ___value0;
		NullCheck((ReactiveProperty_1_t819338379 *)__this);
		VirtActionInvoker1< bool >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<System.Boolean>::SetValue(T) */, (ReactiveProperty_1_t819338379 *)__this, (bool)L_1);
		bool L_2 = (bool)__this->get_isDisposed_2();
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		return;
	}

IL_0025:
	{
		Subject_1_t2149039961 * L_3 = (Subject_1_t2149039961 *)__this->get_publisher_4();
		V_0 = (Subject_1_t2149039961 *)L_3;
		Subject_1_t2149039961 * L_4 = V_0;
		if (!L_4)
		{
			goto IL_003e;
		}
	}
	{
		Subject_1_t2149039961 * L_5 = V_0;
		bool L_6 = (bool)__this->get_value_3();
		NullCheck((Subject_1_t2149039961 *)L_5);
		((  void (*) (Subject_1_t2149039961 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Subject_1_t2149039961 *)L_5, (bool)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
	}

IL_003e:
	{
		return;
	}

IL_003f:
	{
		NullCheck((ReactiveProperty_1_t819338379 *)__this);
		Il2CppObject* L_7 = VirtFuncInvoker0< Il2CppObject* >::Invoke(10 /* System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1<System.Boolean>::get_EqualityComparer() */, (ReactiveProperty_1_t819338379 *)__this);
		bool L_8 = (bool)__this->get_value_3();
		bool L_9 = ___value0;
		NullCheck((Il2CppObject*)L_7);
		bool L_10 = InterfaceFuncInvoker2< bool, bool, bool >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Boolean>::Equals(!0,!0) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12), (Il2CppObject*)L_7, (bool)L_8, (bool)L_9);
		if (L_10)
		{
			goto IL_0082;
		}
	}
	{
		bool L_11 = ___value0;
		NullCheck((ReactiveProperty_1_t819338379 *)__this);
		VirtActionInvoker1< bool >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<System.Boolean>::SetValue(T) */, (ReactiveProperty_1_t819338379 *)__this, (bool)L_11);
		bool L_12 = (bool)__this->get_isDisposed_2();
		if (!L_12)
		{
			goto IL_0069;
		}
	}
	{
		return;
	}

IL_0069:
	{
		Subject_1_t2149039961 * L_13 = (Subject_1_t2149039961 *)__this->get_publisher_4();
		V_1 = (Subject_1_t2149039961 *)L_13;
		Subject_1_t2149039961 * L_14 = V_1;
		if (!L_14)
		{
			goto IL_0082;
		}
	}
	{
		Subject_1_t2149039961 * L_15 = V_1;
		bool L_16 = (bool)__this->get_value_3();
		NullCheck((Subject_1_t2149039961 *)L_15);
		((  void (*) (Subject_1_t2149039961 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Subject_1_t2149039961 *)L_15, (bool)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
	}

IL_0082:
	{
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Boolean>::SetValue(T)
extern "C"  void ReactiveProperty_1_SetValue_m3502544650_gshared (ReactiveProperty_1_t819338379 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_value_3(L_0);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Boolean>::SetValueAndForceNotify(T)
extern "C"  void ReactiveProperty_1_SetValueAndForceNotify_m264733517_gshared (ReactiveProperty_1_t819338379 * __this, bool ___value0, const MethodInfo* method)
{
	Subject_1_t2149039961 * V_0 = NULL;
	{
		bool L_0 = ___value0;
		NullCheck((ReactiveProperty_1_t819338379 *)__this);
		VirtActionInvoker1< bool >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<System.Boolean>::SetValue(T) */, (ReactiveProperty_1_t819338379 *)__this, (bool)L_0);
		bool L_1 = (bool)__this->get_isDisposed_2();
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		return;
	}

IL_0013:
	{
		Subject_1_t2149039961 * L_2 = (Subject_1_t2149039961 *)__this->get_publisher_4();
		V_0 = (Subject_1_t2149039961 *)L_2;
		Subject_1_t2149039961 * L_3 = V_0;
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		Subject_1_t2149039961 * L_4 = V_0;
		bool L_5 = (bool)__this->get_value_3();
		NullCheck((Subject_1_t2149039961 *)L_4);
		((  void (*) (Subject_1_t2149039961 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Subject_1_t2149039961 *)L_4, (bool)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
	}

IL_002c:
	{
		return;
	}
}
// System.IDisposable UniRx.ReactiveProperty`1<System.Boolean>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1_Subscribe_m1226681278_MetadataUsageId;
extern "C"  Il2CppObject * ReactiveProperty_1_Subscribe_m1226681278_gshared (ReactiveProperty_1_t819338379 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1_Subscribe_m1226681278_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t2149039961 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		bool L_0 = (bool)__this->get_isDisposed_2();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Il2CppObject* L_1 = ___observer0;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Boolean>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_2 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_2;
	}

IL_0017:
	{
		Subject_1_t2149039961 * L_3 = (Subject_1_t2149039961 *)__this->get_publisher_4();
		if (L_3)
		{
			goto IL_002d;
		}
	}
	{
		Subject_1_t2149039961 * L_4 = (Subject_1_t2149039961 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Subject_1_t2149039961 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_publisher_4(L_4);
	}

IL_002d:
	{
		Subject_1_t2149039961 * L_5 = (Subject_1_t2149039961 *)__this->get_publisher_4();
		V_0 = (Subject_1_t2149039961 *)L_5;
		Subject_1_t2149039961 * L_6 = V_0;
		if (!L_6)
		{
			goto IL_005b;
		}
	}
	{
		Subject_1_t2149039961 * L_7 = V_0;
		Il2CppObject* L_8 = ___observer0;
		NullCheck((Subject_1_t2149039961 *)L_7);
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (Subject_1_t2149039961 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)((Subject_1_t2149039961 *)L_7, (Il2CppObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		V_1 = (Il2CppObject *)L_9;
		bool L_10 = (bool)__this->get_canPublishValueOnSubscribe_1();
		if (!L_10)
		{
			goto IL_0059;
		}
	}
	{
		Il2CppObject* L_11 = ___observer0;
		bool L_12 = (bool)__this->get_value_3();
		NullCheck((Il2CppObject*)L_11);
		InterfaceActionInvoker1< bool >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Boolean>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_11, (bool)L_12);
	}

IL_0059:
	{
		Il2CppObject * L_13 = V_1;
		return L_13;
	}

IL_005b:
	{
		Il2CppObject* L_14 = ___observer0;
		NullCheck((Il2CppObject*)L_14);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Boolean>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_14);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_15 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_15;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Boolean>::Dispose()
extern "C"  void ReactiveProperty_1_Dispose_m1849450876_gshared (ReactiveProperty_1_t819338379 * __this, const MethodInfo* method)
{
	{
		NullCheck((ReactiveProperty_1_t819338379 *)__this);
		VirtActionInvoker1< bool >::Invoke(12 /* System.Void UniRx.ReactiveProperty`1<System.Boolean>::Dispose(System.Boolean) */, (ReactiveProperty_1_t819338379 *)__this, (bool)1);
		GC_SuppressFinalize_m1160635446(NULL /*static, unused*/, (Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Boolean>::Dispose(System.Boolean)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1_Dispose_m2167197171_MetadataUsageId;
extern "C"  void ReactiveProperty_1_Dispose_m2167197171_gshared (ReactiveProperty_1_t819338379 * __this, bool ___disposing0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1_Dispose_m2167197171_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Subject_1_t2149039961 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (bool)__this->get_isDisposed_2();
		if (L_0)
		{
			goto IL_0052;
		}
	}
	{
		__this->set_isDisposed_2((bool)1);
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_sourceConnection_5();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		Il2CppObject * L_3 = V_0;
		NullCheck((Il2CppObject *)L_3);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
		__this->set_sourceConnection_5((Il2CppObject *)NULL);
	}

IL_002c:
	{
		Subject_1_t2149039961 * L_4 = (Subject_1_t2149039961 *)__this->get_publisher_4();
		V_1 = (Subject_1_t2149039961 *)L_4;
		Subject_1_t2149039961 * L_5 = V_1;
		if (!L_5)
		{
			goto IL_0052;
		}
	}

IL_0039:
	try
	{ // begin try (depth: 1)
		Subject_1_t2149039961 * L_6 = V_1;
		NullCheck((Subject_1_t2149039961 *)L_6);
		((  void (*) (Subject_1_t2149039961 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)((Subject_1_t2149039961 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		IL2CPP_LEAVE(0x52, FINALLY_0044);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0044;
	}

FINALLY_0044:
	{ // begin finally (depth: 1)
		Subject_1_t2149039961 * L_7 = V_1;
		NullCheck((Subject_1_t2149039961 *)L_7);
		((  void (*) (Subject_1_t2149039961 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((Subject_1_t2149039961 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		__this->set_publisher_4((Subject_1_t2149039961 *)NULL);
		IL2CPP_END_FINALLY(68)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(68)
	{
		IL2CPP_JUMP_TBL(0x52, IL_0052)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0052:
	{
		return;
	}
}
// System.String UniRx.ReactiveProperty`1<System.Boolean>::ToString()
extern Il2CppCodeGenString* _stringLiteral3392903;
extern const uint32_t ReactiveProperty_1_ToString_m4110379380_MetadataUsageId;
extern "C"  String_t* ReactiveProperty_1_ToString_m4110379380_gshared (ReactiveProperty_1_t819338379 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1_ToString_m4110379380_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B3_0 = NULL;
	{
		bool L_0 = (bool)__this->get_value_3();
		goto IL_001a;
	}
	{
		G_B3_0 = _stringLiteral3392903;
		goto IL_002b;
	}

IL_001a:
	{
		bool* L_1 = (bool*)__this->get_address_of_value_3();
		String_t* L_2 = Boolean_ToString_m2512358154((bool*)L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_002b:
	{
		return G_B3_0;
	}
}
// System.Boolean UniRx.ReactiveProperty`1<System.Boolean>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m952260580_gshared (ReactiveProperty_1_t819338379 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Byte>::.ctor()
extern Il2CppClass* Byte_t2778693821_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m1302869371_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m1302869371_gshared (ReactiveProperty_1_t3387026859 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m1302869371_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint8_t V_0 = 0x0;
	{
		Initobj (Byte_t2778693821_il2cpp_TypeInfo_var, (&V_0));
		uint8_t L_0 = V_0;
		NullCheck((ReactiveProperty_1_t3387026859 *)__this);
		((  void (*) (ReactiveProperty_1_t3387026859 *, uint8_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ReactiveProperty_1_t3387026859 *)__this, (uint8_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Byte>::.ctor(T)
extern Il2CppClass* Byte_t2778693821_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m1734246211_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m1734246211_gshared (ReactiveProperty_1_t3387026859 * __this, uint8_t ___initialValue0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m1734246211_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint8_t V_0 = 0x0;
	{
		Initobj (Byte_t2778693821_il2cpp_TypeInfo_var, (&V_0));
		uint8_t L_0 = V_0;
		__this->set_value_3(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		uint8_t L_1 = ___initialValue0;
		NullCheck((ReactiveProperty_1_t3387026859 *)__this);
		VirtActionInvoker1< uint8_t >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<System.Byte>::SetValue(T) */, (ReactiveProperty_1_t3387026859 *)__this, (uint8_t)L_1);
		__this->set_canPublishValueOnSubscribe_1((bool)1);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Byte>::.ctor(UniRx.IObservable`1<T>)
extern Il2CppClass* Byte_t2778693821_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m4204920090_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m4204920090_gshared (ReactiveProperty_1_t3387026859 * __this, Il2CppObject* ___source0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m4204920090_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint8_t V_0 = 0x0;
	{
		Initobj (Byte_t2778693821_il2cpp_TypeInfo_var, (&V_0));
		uint8_t L_0 = V_0;
		__this->set_value_3(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		__this->set_canPublishValueOnSubscribe_1((bool)0);
		Subject_1_t421761145 * L_1 = (Subject_1_t421761145 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Subject_1_t421761145 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_publisher_4(L_1);
		Il2CppObject* L_2 = ___source0;
		ReactivePropertyObserver_t3014676390 * L_3 = (ReactivePropertyObserver_t3014676390 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (ReactivePropertyObserver_t3014676390 *, ReactiveProperty_1_t3387026859 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_3, (ReactiveProperty_1_t3387026859 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_4 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Byte>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_2, (Il2CppObject*)L_3);
		__this->set_sourceConnection_5(L_4);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Byte>::.ctor(UniRx.IObservable`1<T>,T)
extern Il2CppClass* Byte_t2778693821_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m3658953778_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m3658953778_gshared (ReactiveProperty_1_t3387026859 * __this, Il2CppObject* ___source0, uint8_t ___initialValue1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m3658953778_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint8_t V_0 = 0x0;
	{
		Initobj (Byte_t2778693821_il2cpp_TypeInfo_var, (&V_0));
		uint8_t L_0 = V_0;
		__this->set_value_3(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		__this->set_canPublishValueOnSubscribe_1((bool)0);
		uint8_t L_1 = ___initialValue1;
		NullCheck((ReactiveProperty_1_t3387026859 *)__this);
		VirtActionInvoker1< uint8_t >::Invoke(7 /* System.Void UniRx.ReactiveProperty`1<System.Byte>::set_Value(T) */, (ReactiveProperty_1_t3387026859 *)__this, (uint8_t)L_1);
		Subject_1_t421761145 * L_2 = (Subject_1_t421761145 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Subject_1_t421761145 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_publisher_4(L_2);
		Il2CppObject* L_3 = ___source0;
		ReactivePropertyObserver_t3014676390 * L_4 = (ReactivePropertyObserver_t3014676390 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (ReactivePropertyObserver_t3014676390 *, ReactiveProperty_1_t3387026859 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_4, (ReactiveProperty_1_t3387026859 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((Il2CppObject*)L_3);
		Il2CppObject * L_5 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Byte>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_3, (Il2CppObject*)L_4);
		__this->set_sourceConnection_5(L_5);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Byte>::.cctor()
extern Il2CppClass* UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__cctor_m1252148626_MetadataUsageId;
extern "C"  void ReactiveProperty_1__cctor_m1252148626_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__cctor_m1252148626_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		((ReactiveProperty_1_t3387026859_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->static_fields)->set_defaultEqualityComparer_0(L_0);
		return;
	}
}
// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1<System.Byte>::get_EqualityComparer()
extern "C"  Il2CppObject* ReactiveProperty_1_get_EqualityComparer_m332684858_gshared (ReactiveProperty_1_t3387026859 * __this, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		Il2CppObject* L_0 = ((ReactiveProperty_1_t3387026859_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->static_fields)->get_defaultEqualityComparer_0();
		return L_0;
	}
}
// T UniRx.ReactiveProperty`1<System.Byte>::get_Value()
extern "C"  uint8_t ReactiveProperty_1_get_Value_m1384232096_gshared (ReactiveProperty_1_t3387026859 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = (uint8_t)__this->get_value_3();
		return L_0;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Byte>::set_Value(T)
extern "C"  void ReactiveProperty_1_set_Value_m437209617_gshared (ReactiveProperty_1_t3387026859 * __this, uint8_t ___value0, const MethodInfo* method)
{
	Subject_1_t421761145 * V_0 = NULL;
	Subject_1_t421761145 * V_1 = NULL;
	{
		bool L_0 = (bool)__this->get_canPublishValueOnSubscribe_1();
		if (L_0)
		{
			goto IL_003f;
		}
	}
	{
		__this->set_canPublishValueOnSubscribe_1((bool)1);
		uint8_t L_1 = ___value0;
		NullCheck((ReactiveProperty_1_t3387026859 *)__this);
		VirtActionInvoker1< uint8_t >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<System.Byte>::SetValue(T) */, (ReactiveProperty_1_t3387026859 *)__this, (uint8_t)L_1);
		bool L_2 = (bool)__this->get_isDisposed_2();
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		return;
	}

IL_0025:
	{
		Subject_1_t421761145 * L_3 = (Subject_1_t421761145 *)__this->get_publisher_4();
		V_0 = (Subject_1_t421761145 *)L_3;
		Subject_1_t421761145 * L_4 = V_0;
		if (!L_4)
		{
			goto IL_003e;
		}
	}
	{
		Subject_1_t421761145 * L_5 = V_0;
		uint8_t L_6 = (uint8_t)__this->get_value_3();
		NullCheck((Subject_1_t421761145 *)L_5);
		((  void (*) (Subject_1_t421761145 *, uint8_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Subject_1_t421761145 *)L_5, (uint8_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
	}

IL_003e:
	{
		return;
	}

IL_003f:
	{
		NullCheck((ReactiveProperty_1_t3387026859 *)__this);
		Il2CppObject* L_7 = VirtFuncInvoker0< Il2CppObject* >::Invoke(10 /* System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1<System.Byte>::get_EqualityComparer() */, (ReactiveProperty_1_t3387026859 *)__this);
		uint8_t L_8 = (uint8_t)__this->get_value_3();
		uint8_t L_9 = ___value0;
		NullCheck((Il2CppObject*)L_7);
		bool L_10 = InterfaceFuncInvoker2< bool, uint8_t, uint8_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Byte>::Equals(!0,!0) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12), (Il2CppObject*)L_7, (uint8_t)L_8, (uint8_t)L_9);
		if (L_10)
		{
			goto IL_0082;
		}
	}
	{
		uint8_t L_11 = ___value0;
		NullCheck((ReactiveProperty_1_t3387026859 *)__this);
		VirtActionInvoker1< uint8_t >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<System.Byte>::SetValue(T) */, (ReactiveProperty_1_t3387026859 *)__this, (uint8_t)L_11);
		bool L_12 = (bool)__this->get_isDisposed_2();
		if (!L_12)
		{
			goto IL_0069;
		}
	}
	{
		return;
	}

IL_0069:
	{
		Subject_1_t421761145 * L_13 = (Subject_1_t421761145 *)__this->get_publisher_4();
		V_1 = (Subject_1_t421761145 *)L_13;
		Subject_1_t421761145 * L_14 = V_1;
		if (!L_14)
		{
			goto IL_0082;
		}
	}
	{
		Subject_1_t421761145 * L_15 = V_1;
		uint8_t L_16 = (uint8_t)__this->get_value_3();
		NullCheck((Subject_1_t421761145 *)L_15);
		((  void (*) (Subject_1_t421761145 *, uint8_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Subject_1_t421761145 *)L_15, (uint8_t)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
	}

IL_0082:
	{
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Byte>::SetValue(T)
extern "C"  void ReactiveProperty_1_SetValue_m1762358662_gshared (ReactiveProperty_1_t3387026859 * __this, uint8_t ___value0, const MethodInfo* method)
{
	{
		uint8_t L_0 = ___value0;
		__this->set_value_3(L_0);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Byte>::SetValueAndForceNotify(T)
extern "C"  void ReactiveProperty_1_SetValueAndForceNotify_m2180602057_gshared (ReactiveProperty_1_t3387026859 * __this, uint8_t ___value0, const MethodInfo* method)
{
	Subject_1_t421761145 * V_0 = NULL;
	{
		uint8_t L_0 = ___value0;
		NullCheck((ReactiveProperty_1_t3387026859 *)__this);
		VirtActionInvoker1< uint8_t >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<System.Byte>::SetValue(T) */, (ReactiveProperty_1_t3387026859 *)__this, (uint8_t)L_0);
		bool L_1 = (bool)__this->get_isDisposed_2();
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		return;
	}

IL_0013:
	{
		Subject_1_t421761145 * L_2 = (Subject_1_t421761145 *)__this->get_publisher_4();
		V_0 = (Subject_1_t421761145 *)L_2;
		Subject_1_t421761145 * L_3 = V_0;
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		Subject_1_t421761145 * L_4 = V_0;
		uint8_t L_5 = (uint8_t)__this->get_value_3();
		NullCheck((Subject_1_t421761145 *)L_4);
		((  void (*) (Subject_1_t421761145 *, uint8_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Subject_1_t421761145 *)L_4, (uint8_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
	}

IL_002c:
	{
		return;
	}
}
// System.IDisposable UniRx.ReactiveProperty`1<System.Byte>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1_Subscribe_m764395536_MetadataUsageId;
extern "C"  Il2CppObject * ReactiveProperty_1_Subscribe_m764395536_gshared (ReactiveProperty_1_t3387026859 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1_Subscribe_m764395536_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t421761145 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		bool L_0 = (bool)__this->get_isDisposed_2();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Il2CppObject* L_1 = ___observer0;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Byte>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_2 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_2;
	}

IL_0017:
	{
		Subject_1_t421761145 * L_3 = (Subject_1_t421761145 *)__this->get_publisher_4();
		if (L_3)
		{
			goto IL_002d;
		}
	}
	{
		Subject_1_t421761145 * L_4 = (Subject_1_t421761145 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Subject_1_t421761145 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_publisher_4(L_4);
	}

IL_002d:
	{
		Subject_1_t421761145 * L_5 = (Subject_1_t421761145 *)__this->get_publisher_4();
		V_0 = (Subject_1_t421761145 *)L_5;
		Subject_1_t421761145 * L_6 = V_0;
		if (!L_6)
		{
			goto IL_005b;
		}
	}
	{
		Subject_1_t421761145 * L_7 = V_0;
		Il2CppObject* L_8 = ___observer0;
		NullCheck((Subject_1_t421761145 *)L_7);
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (Subject_1_t421761145 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)((Subject_1_t421761145 *)L_7, (Il2CppObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		V_1 = (Il2CppObject *)L_9;
		bool L_10 = (bool)__this->get_canPublishValueOnSubscribe_1();
		if (!L_10)
		{
			goto IL_0059;
		}
	}
	{
		Il2CppObject* L_11 = ___observer0;
		uint8_t L_12 = (uint8_t)__this->get_value_3();
		NullCheck((Il2CppObject*)L_11);
		InterfaceActionInvoker1< uint8_t >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Byte>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_11, (uint8_t)L_12);
	}

IL_0059:
	{
		Il2CppObject * L_13 = V_1;
		return L_13;
	}

IL_005b:
	{
		Il2CppObject* L_14 = ___observer0;
		NullCheck((Il2CppObject*)L_14);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Byte>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_14);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_15 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_15;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Byte>::Dispose()
extern "C"  void ReactiveProperty_1_Dispose_m2120265464_gshared (ReactiveProperty_1_t3387026859 * __this, const MethodInfo* method)
{
	{
		NullCheck((ReactiveProperty_1_t3387026859 *)__this);
		VirtActionInvoker1< bool >::Invoke(12 /* System.Void UniRx.ReactiveProperty`1<System.Byte>::Dispose(System.Boolean) */, (ReactiveProperty_1_t3387026859 *)__this, (bool)1);
		GC_SuppressFinalize_m1160635446(NULL /*static, unused*/, (Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Byte>::Dispose(System.Boolean)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1_Dispose_m126734959_MetadataUsageId;
extern "C"  void ReactiveProperty_1_Dispose_m126734959_gshared (ReactiveProperty_1_t3387026859 * __this, bool ___disposing0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1_Dispose_m126734959_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Subject_1_t421761145 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (bool)__this->get_isDisposed_2();
		if (L_0)
		{
			goto IL_0052;
		}
	}
	{
		__this->set_isDisposed_2((bool)1);
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_sourceConnection_5();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		Il2CppObject * L_3 = V_0;
		NullCheck((Il2CppObject *)L_3);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
		__this->set_sourceConnection_5((Il2CppObject *)NULL);
	}

IL_002c:
	{
		Subject_1_t421761145 * L_4 = (Subject_1_t421761145 *)__this->get_publisher_4();
		V_1 = (Subject_1_t421761145 *)L_4;
		Subject_1_t421761145 * L_5 = V_1;
		if (!L_5)
		{
			goto IL_0052;
		}
	}

IL_0039:
	try
	{ // begin try (depth: 1)
		Subject_1_t421761145 * L_6 = V_1;
		NullCheck((Subject_1_t421761145 *)L_6);
		((  void (*) (Subject_1_t421761145 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)((Subject_1_t421761145 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		IL2CPP_LEAVE(0x52, FINALLY_0044);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0044;
	}

FINALLY_0044:
	{ // begin finally (depth: 1)
		Subject_1_t421761145 * L_7 = V_1;
		NullCheck((Subject_1_t421761145 *)L_7);
		((  void (*) (Subject_1_t421761145 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((Subject_1_t421761145 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		__this->set_publisher_4((Subject_1_t421761145 *)NULL);
		IL2CPP_END_FINALLY(68)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(68)
	{
		IL2CPP_JUMP_TBL(0x52, IL_0052)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0052:
	{
		return;
	}
}
// System.String UniRx.ReactiveProperty`1<System.Byte>::ToString()
extern Il2CppCodeGenString* _stringLiteral3392903;
extern const uint32_t ReactiveProperty_1_ToString_m3932696338_MetadataUsageId;
extern "C"  String_t* ReactiveProperty_1_ToString_m3932696338_gshared (ReactiveProperty_1_t3387026859 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1_ToString_m3932696338_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B3_0 = NULL;
	{
		uint8_t L_0 = (uint8_t)__this->get_value_3();
		goto IL_001a;
	}
	{
		G_B3_0 = _stringLiteral3392903;
		goto IL_002b;
	}

IL_001a:
	{
		uint8_t* L_1 = (uint8_t*)__this->get_address_of_value_3();
		String_t* L_2 = Byte_ToString_m961894880((uint8_t*)L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_002b:
	{
		return G_B3_0;
	}
}
// System.Boolean UniRx.ReactiveProperty`1<System.Byte>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m3230296368_gshared (ReactiveProperty_1_t3387026859 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Double>::.ctor()
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m3586573700_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m3586573700_gshared (ReactiveProperty_1_t1142849652 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m3586573700_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	double V_0 = 0.0;
	{
		Initobj (Double_t534516614_il2cpp_TypeInfo_var, (&V_0));
		double L_0 = V_0;
		NullCheck((ReactiveProperty_1_t1142849652 *)__this);
		((  void (*) (ReactiveProperty_1_t1142849652 *, double, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ReactiveProperty_1_t1142849652 *)__this, (double)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Double>::.ctor(T)
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m3809603674_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m3809603674_gshared (ReactiveProperty_1_t1142849652 * __this, double ___initialValue0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m3809603674_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	double V_0 = 0.0;
	{
		Initobj (Double_t534516614_il2cpp_TypeInfo_var, (&V_0));
		double L_0 = V_0;
		__this->set_value_3(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		double L_1 = ___initialValue0;
		NullCheck((ReactiveProperty_1_t1142849652 *)__this);
		VirtActionInvoker1< double >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<System.Double>::SetValue(T) */, (ReactiveProperty_1_t1142849652 *)__this, (double)L_1);
		__this->set_canPublishValueOnSubscribe_1((bool)1);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Double>::.ctor(UniRx.IObservable`1<T>)
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m4197642339_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m4197642339_gshared (ReactiveProperty_1_t1142849652 * __this, Il2CppObject* ___source0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m4197642339_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	double V_0 = 0.0;
	{
		Initobj (Double_t534516614_il2cpp_TypeInfo_var, (&V_0));
		double L_0 = V_0;
		__this->set_value_3(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		__this->set_canPublishValueOnSubscribe_1((bool)0);
		Subject_1_t2472551234 * L_1 = (Subject_1_t2472551234 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Subject_1_t2472551234 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_publisher_4(L_1);
		Il2CppObject* L_2 = ___source0;
		ReactivePropertyObserver_t770499183 * L_3 = (ReactivePropertyObserver_t770499183 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (ReactivePropertyObserver_t770499183 *, ReactiveProperty_1_t1142849652 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_3, (ReactiveProperty_1_t1142849652 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_4 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Double>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_2, (Il2CppObject*)L_3);
		__this->set_sourceConnection_5(L_4);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Double>::.ctor(UniRx.IObservable`1<T>,T)
extern Il2CppClass* Double_t534516614_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m960002363_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m960002363_gshared (ReactiveProperty_1_t1142849652 * __this, Il2CppObject* ___source0, double ___initialValue1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m960002363_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	double V_0 = 0.0;
	{
		Initobj (Double_t534516614_il2cpp_TypeInfo_var, (&V_0));
		double L_0 = V_0;
		__this->set_value_3(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		__this->set_canPublishValueOnSubscribe_1((bool)0);
		double L_1 = ___initialValue1;
		NullCheck((ReactiveProperty_1_t1142849652 *)__this);
		VirtActionInvoker1< double >::Invoke(7 /* System.Void UniRx.ReactiveProperty`1<System.Double>::set_Value(T) */, (ReactiveProperty_1_t1142849652 *)__this, (double)L_1);
		Subject_1_t2472551234 * L_2 = (Subject_1_t2472551234 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Subject_1_t2472551234 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_publisher_4(L_2);
		Il2CppObject* L_3 = ___source0;
		ReactivePropertyObserver_t770499183 * L_4 = (ReactivePropertyObserver_t770499183 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (ReactivePropertyObserver_t770499183 *, ReactiveProperty_1_t1142849652 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_4, (ReactiveProperty_1_t1142849652 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((Il2CppObject*)L_3);
		Il2CppObject * L_5 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Double>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_3, (Il2CppObject*)L_4);
		__this->set_sourceConnection_5(L_5);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Double>::.cctor()
extern Il2CppClass* UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__cctor_m3327506089_MetadataUsageId;
extern "C"  void ReactiveProperty_1__cctor_m3327506089_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__cctor_m3327506089_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		((ReactiveProperty_1_t1142849652_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->static_fields)->set_defaultEqualityComparer_0(L_0);
		return;
	}
}
// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1<System.Double>::get_EqualityComparer()
extern "C"  Il2CppObject* ReactiveProperty_1_get_EqualityComparer_m3271061201_gshared (ReactiveProperty_1_t1142849652 * __this, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		Il2CppObject* L_0 = ((ReactiveProperty_1_t1142849652_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->static_fields)->get_defaultEqualityComparer_0();
		return L_0;
	}
}
// T UniRx.ReactiveProperty`1<System.Double>::get_Value()
extern "C"  double ReactiveProperty_1_get_Value_m1320871017_gshared (ReactiveProperty_1_t1142849652 * __this, const MethodInfo* method)
{
	{
		double L_0 = (double)__this->get_value_3();
		return L_0;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Double>::set_Value(T)
extern "C"  void ReactiveProperty_1_set_Value_m3185989544_gshared (ReactiveProperty_1_t1142849652 * __this, double ___value0, const MethodInfo* method)
{
	Subject_1_t2472551234 * V_0 = NULL;
	Subject_1_t2472551234 * V_1 = NULL;
	{
		bool L_0 = (bool)__this->get_canPublishValueOnSubscribe_1();
		if (L_0)
		{
			goto IL_003f;
		}
	}
	{
		__this->set_canPublishValueOnSubscribe_1((bool)1);
		double L_1 = ___value0;
		NullCheck((ReactiveProperty_1_t1142849652 *)__this);
		VirtActionInvoker1< double >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<System.Double>::SetValue(T) */, (ReactiveProperty_1_t1142849652 *)__this, (double)L_1);
		bool L_2 = (bool)__this->get_isDisposed_2();
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		return;
	}

IL_0025:
	{
		Subject_1_t2472551234 * L_3 = (Subject_1_t2472551234 *)__this->get_publisher_4();
		V_0 = (Subject_1_t2472551234 *)L_3;
		Subject_1_t2472551234 * L_4 = V_0;
		if (!L_4)
		{
			goto IL_003e;
		}
	}
	{
		Subject_1_t2472551234 * L_5 = V_0;
		double L_6 = (double)__this->get_value_3();
		NullCheck((Subject_1_t2472551234 *)L_5);
		((  void (*) (Subject_1_t2472551234 *, double, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Subject_1_t2472551234 *)L_5, (double)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
	}

IL_003e:
	{
		return;
	}

IL_003f:
	{
		NullCheck((ReactiveProperty_1_t1142849652 *)__this);
		Il2CppObject* L_7 = VirtFuncInvoker0< Il2CppObject* >::Invoke(10 /* System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1<System.Double>::get_EqualityComparer() */, (ReactiveProperty_1_t1142849652 *)__this);
		double L_8 = (double)__this->get_value_3();
		double L_9 = ___value0;
		NullCheck((Il2CppObject*)L_7);
		bool L_10 = InterfaceFuncInvoker2< bool, double, double >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Double>::Equals(!0,!0) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12), (Il2CppObject*)L_7, (double)L_8, (double)L_9);
		if (L_10)
		{
			goto IL_0082;
		}
	}
	{
		double L_11 = ___value0;
		NullCheck((ReactiveProperty_1_t1142849652 *)__this);
		VirtActionInvoker1< double >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<System.Double>::SetValue(T) */, (ReactiveProperty_1_t1142849652 *)__this, (double)L_11);
		bool L_12 = (bool)__this->get_isDisposed_2();
		if (!L_12)
		{
			goto IL_0069;
		}
	}
	{
		return;
	}

IL_0069:
	{
		Subject_1_t2472551234 * L_13 = (Subject_1_t2472551234 *)__this->get_publisher_4();
		V_1 = (Subject_1_t2472551234 *)L_13;
		Subject_1_t2472551234 * L_14 = V_1;
		if (!L_14)
		{
			goto IL_0082;
		}
	}
	{
		Subject_1_t2472551234 * L_15 = V_1;
		double L_16 = (double)__this->get_value_3();
		NullCheck((Subject_1_t2472551234 *)L_15);
		((  void (*) (Subject_1_t2472551234 *, double, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Subject_1_t2472551234 *)L_15, (double)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
	}

IL_0082:
	{
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Double>::SetValue(T)
extern "C"  void ReactiveProperty_1_SetValue_m2682312975_gshared (ReactiveProperty_1_t1142849652 * __this, double ___value0, const MethodInfo* method)
{
	{
		double L_0 = ___value0;
		__this->set_value_3(L_0);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Double>::SetValueAndForceNotify(T)
extern "C"  void ReactiveProperty_1_SetValueAndForceNotify_m2425444498_gshared (ReactiveProperty_1_t1142849652 * __this, double ___value0, const MethodInfo* method)
{
	Subject_1_t2472551234 * V_0 = NULL;
	{
		double L_0 = ___value0;
		NullCheck((ReactiveProperty_1_t1142849652 *)__this);
		VirtActionInvoker1< double >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<System.Double>::SetValue(T) */, (ReactiveProperty_1_t1142849652 *)__this, (double)L_0);
		bool L_1 = (bool)__this->get_isDisposed_2();
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		return;
	}

IL_0013:
	{
		Subject_1_t2472551234 * L_2 = (Subject_1_t2472551234 *)__this->get_publisher_4();
		V_0 = (Subject_1_t2472551234 *)L_2;
		Subject_1_t2472551234 * L_3 = V_0;
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		Subject_1_t2472551234 * L_4 = V_0;
		double L_5 = (double)__this->get_value_3();
		NullCheck((Subject_1_t2472551234 *)L_4);
		((  void (*) (Subject_1_t2472551234 *, double, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Subject_1_t2472551234 *)L_4, (double)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
	}

IL_002c:
	{
		return;
	}
}
// System.IDisposable UniRx.ReactiveProperty`1<System.Double>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1_Subscribe_m3002610265_MetadataUsageId;
extern "C"  Il2CppObject * ReactiveProperty_1_Subscribe_m3002610265_gshared (ReactiveProperty_1_t1142849652 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1_Subscribe_m3002610265_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t2472551234 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		bool L_0 = (bool)__this->get_isDisposed_2();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Il2CppObject* L_1 = ___observer0;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Double>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_2 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_2;
	}

IL_0017:
	{
		Subject_1_t2472551234 * L_3 = (Subject_1_t2472551234 *)__this->get_publisher_4();
		if (L_3)
		{
			goto IL_002d;
		}
	}
	{
		Subject_1_t2472551234 * L_4 = (Subject_1_t2472551234 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Subject_1_t2472551234 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_publisher_4(L_4);
	}

IL_002d:
	{
		Subject_1_t2472551234 * L_5 = (Subject_1_t2472551234 *)__this->get_publisher_4();
		V_0 = (Subject_1_t2472551234 *)L_5;
		Subject_1_t2472551234 * L_6 = V_0;
		if (!L_6)
		{
			goto IL_005b;
		}
	}
	{
		Subject_1_t2472551234 * L_7 = V_0;
		Il2CppObject* L_8 = ___observer0;
		NullCheck((Subject_1_t2472551234 *)L_7);
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (Subject_1_t2472551234 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)((Subject_1_t2472551234 *)L_7, (Il2CppObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		V_1 = (Il2CppObject *)L_9;
		bool L_10 = (bool)__this->get_canPublishValueOnSubscribe_1();
		if (!L_10)
		{
			goto IL_0059;
		}
	}
	{
		Il2CppObject* L_11 = ___observer0;
		double L_12 = (double)__this->get_value_3();
		NullCheck((Il2CppObject*)L_11);
		InterfaceActionInvoker1< double >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Double>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_11, (double)L_12);
	}

IL_0059:
	{
		Il2CppObject * L_13 = V_1;
		return L_13;
	}

IL_005b:
	{
		Il2CppObject* L_14 = ___observer0;
		NullCheck((Il2CppObject*)L_14);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Double>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_14);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_15 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_15;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Double>::Dispose()
extern "C"  void ReactiveProperty_1_Dispose_m2031837377_gshared (ReactiveProperty_1_t1142849652 * __this, const MethodInfo* method)
{
	{
		NullCheck((ReactiveProperty_1_t1142849652 *)__this);
		VirtActionInvoker1< bool >::Invoke(12 /* System.Void UniRx.ReactiveProperty`1<System.Double>::Dispose(System.Boolean) */, (ReactiveProperty_1_t1142849652 *)__this, (bool)1);
		GC_SuppressFinalize_m1160635446(NULL /*static, unused*/, (Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Double>::Dispose(System.Boolean)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1_Dispose_m4135923832_MetadataUsageId;
extern "C"  void ReactiveProperty_1_Dispose_m4135923832_gshared (ReactiveProperty_1_t1142849652 * __this, bool ___disposing0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1_Dispose_m4135923832_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Subject_1_t2472551234 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (bool)__this->get_isDisposed_2();
		if (L_0)
		{
			goto IL_0052;
		}
	}
	{
		__this->set_isDisposed_2((bool)1);
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_sourceConnection_5();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		Il2CppObject * L_3 = V_0;
		NullCheck((Il2CppObject *)L_3);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
		__this->set_sourceConnection_5((Il2CppObject *)NULL);
	}

IL_002c:
	{
		Subject_1_t2472551234 * L_4 = (Subject_1_t2472551234 *)__this->get_publisher_4();
		V_1 = (Subject_1_t2472551234 *)L_4;
		Subject_1_t2472551234 * L_5 = V_1;
		if (!L_5)
		{
			goto IL_0052;
		}
	}

IL_0039:
	try
	{ // begin try (depth: 1)
		Subject_1_t2472551234 * L_6 = V_1;
		NullCheck((Subject_1_t2472551234 *)L_6);
		((  void (*) (Subject_1_t2472551234 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)((Subject_1_t2472551234 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		IL2CPP_LEAVE(0x52, FINALLY_0044);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0044;
	}

FINALLY_0044:
	{ // begin finally (depth: 1)
		Subject_1_t2472551234 * L_7 = V_1;
		NullCheck((Subject_1_t2472551234 *)L_7);
		((  void (*) (Subject_1_t2472551234 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((Subject_1_t2472551234 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		__this->set_publisher_4((Subject_1_t2472551234 *)NULL);
		IL2CPP_END_FINALLY(68)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(68)
	{
		IL2CPP_JUMP_TBL(0x52, IL_0052)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0052:
	{
		return;
	}
}
// System.String UniRx.ReactiveProperty`1<System.Double>::ToString()
extern Il2CppCodeGenString* _stringLiteral3392903;
extern const uint32_t ReactiveProperty_1_ToString_m2259213097_MetadataUsageId;
extern "C"  String_t* ReactiveProperty_1_ToString_m2259213097_gshared (ReactiveProperty_1_t1142849652 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1_ToString_m2259213097_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B3_0 = NULL;
	{
		double L_0 = (double)__this->get_value_3();
		goto IL_001a;
	}
	{
		G_B3_0 = _stringLiteral3392903;
		goto IL_002b;
	}

IL_001a:
	{
		double* L_1 = (double*)__this->get_address_of_value_3();
		String_t* L_2 = Double_ToString_m3380246633((double*)L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_002b:
	{
		return G_B3_0;
	}
}
// System.Boolean UniRx.ReactiveProperty`1<System.Double>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m3187226823_gshared (ReactiveProperty_1_t1142849652 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Int32>::.ctor()
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m1270444133_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m1270444133_gshared (ReactiveProperty_1_t3455747825 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m1270444133_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		Initobj (Int32_t2847414787_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_0 = V_0;
		NullCheck((ReactiveProperty_1_t3455747825 *)__this);
		((  void (*) (ReactiveProperty_1_t3455747825 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ReactiveProperty_1_t3455747825 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Int32>::.ctor(T)
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m729063833_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m729063833_gshared (ReactiveProperty_1_t3455747825 * __this, int32_t ___initialValue0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m729063833_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		Initobj (Int32_t2847414787_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_0 = V_0;
		__this->set_value_3(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_1 = ___initialValue0;
		NullCheck((ReactiveProperty_1_t3455747825 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<System.Int32>::SetValue(T) */, (ReactiveProperty_1_t3455747825 *)__this, (int32_t)L_1);
		__this->set_canPublishValueOnSubscribe_1((bool)1);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Int32>::.ctor(UniRx.IObservable`1<T>)
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m3020016772_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m3020016772_gshared (ReactiveProperty_1_t3455747825 * __this, Il2CppObject* ___source0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m3020016772_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		Initobj (Int32_t2847414787_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_0 = V_0;
		__this->set_value_3(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		__this->set_canPublishValueOnSubscribe_1((bool)0);
		Subject_1_t490482111 * L_1 = (Subject_1_t490482111 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Subject_1_t490482111 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_publisher_4(L_1);
		Il2CppObject* L_2 = ___source0;
		ReactivePropertyObserver_t3083397356 * L_3 = (ReactivePropertyObserver_t3083397356 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (ReactivePropertyObserver_t3083397356 *, ReactiveProperty_1_t3455747825 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_3, (ReactiveProperty_1_t3455747825 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_4 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Int32>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_2, (Il2CppObject*)L_3);
		__this->set_sourceConnection_5(L_4);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Int32>::.ctor(UniRx.IObservable`1<T>,T)
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m3133198620_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m3133198620_gshared (ReactiveProperty_1_t3455747825 * __this, Il2CppObject* ___source0, int32_t ___initialValue1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m3133198620_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		Initobj (Int32_t2847414787_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_0 = V_0;
		__this->set_value_3(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		__this->set_canPublishValueOnSubscribe_1((bool)0);
		int32_t L_1 = ___initialValue1;
		NullCheck((ReactiveProperty_1_t3455747825 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(7 /* System.Void UniRx.ReactiveProperty`1<System.Int32>::set_Value(T) */, (ReactiveProperty_1_t3455747825 *)__this, (int32_t)L_1);
		Subject_1_t490482111 * L_2 = (Subject_1_t490482111 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Subject_1_t490482111 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_publisher_4(L_2);
		Il2CppObject* L_3 = ___source0;
		ReactivePropertyObserver_t3083397356 * L_4 = (ReactivePropertyObserver_t3083397356 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (ReactivePropertyObserver_t3083397356 *, ReactiveProperty_1_t3455747825 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_4, (ReactiveProperty_1_t3455747825 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((Il2CppObject*)L_3);
		Il2CppObject * L_5 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Int32>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_3, (Il2CppObject*)L_4);
		__this->set_sourceConnection_5(L_5);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Int32>::.cctor()
extern Il2CppClass* UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__cctor_m246966248_MetadataUsageId;
extern "C"  void ReactiveProperty_1__cctor_m246966248_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__cctor_m246966248_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		((ReactiveProperty_1_t3455747825_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->static_fields)->set_defaultEqualityComparer_0(L_0);
		return;
	}
}
// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1<System.Int32>::get_EqualityComparer()
extern "C"  Il2CppObject* ReactiveProperty_1_get_EqualityComparer_m3877458094_gshared (ReactiveProperty_1_t3455747825 * __this, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		Il2CppObject* L_0 = ((ReactiveProperty_1_t3455747825_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->static_fields)->get_defaultEqualityComparer_0();
		return L_0;
	}
}
// T UniRx.ReactiveProperty`1<System.Int32>::get_Value()
extern "C"  int32_t ReactiveProperty_1_get_Value_m1685227756_gshared (ReactiveProperty_1_t3455747825 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_3();
		return L_0;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Int32>::set_Value(T)
extern "C"  void ReactiveProperty_1_set_Value_m3338686823_gshared (ReactiveProperty_1_t3455747825 * __this, int32_t ___value0, const MethodInfo* method)
{
	Subject_1_t490482111 * V_0 = NULL;
	Subject_1_t490482111 * V_1 = NULL;
	{
		bool L_0 = (bool)__this->get_canPublishValueOnSubscribe_1();
		if (L_0)
		{
			goto IL_003f;
		}
	}
	{
		__this->set_canPublishValueOnSubscribe_1((bool)1);
		int32_t L_1 = ___value0;
		NullCheck((ReactiveProperty_1_t3455747825 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<System.Int32>::SetValue(T) */, (ReactiveProperty_1_t3455747825 *)__this, (int32_t)L_1);
		bool L_2 = (bool)__this->get_isDisposed_2();
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		return;
	}

IL_0025:
	{
		Subject_1_t490482111 * L_3 = (Subject_1_t490482111 *)__this->get_publisher_4();
		V_0 = (Subject_1_t490482111 *)L_3;
		Subject_1_t490482111 * L_4 = V_0;
		if (!L_4)
		{
			goto IL_003e;
		}
	}
	{
		Subject_1_t490482111 * L_5 = V_0;
		int32_t L_6 = (int32_t)__this->get_value_3();
		NullCheck((Subject_1_t490482111 *)L_5);
		((  void (*) (Subject_1_t490482111 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Subject_1_t490482111 *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
	}

IL_003e:
	{
		return;
	}

IL_003f:
	{
		NullCheck((ReactiveProperty_1_t3455747825 *)__this);
		Il2CppObject* L_7 = VirtFuncInvoker0< Il2CppObject* >::Invoke(10 /* System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1<System.Int32>::get_EqualityComparer() */, (ReactiveProperty_1_t3455747825 *)__this);
		int32_t L_8 = (int32_t)__this->get_value_3();
		int32_t L_9 = ___value0;
		NullCheck((Il2CppObject*)L_7);
		bool L_10 = InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Int32>::Equals(!0,!0) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12), (Il2CppObject*)L_7, (int32_t)L_8, (int32_t)L_9);
		if (L_10)
		{
			goto IL_0082;
		}
	}
	{
		int32_t L_11 = ___value0;
		NullCheck((ReactiveProperty_1_t3455747825 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<System.Int32>::SetValue(T) */, (ReactiveProperty_1_t3455747825 *)__this, (int32_t)L_11);
		bool L_12 = (bool)__this->get_isDisposed_2();
		if (!L_12)
		{
			goto IL_0069;
		}
	}
	{
		return;
	}

IL_0069:
	{
		Subject_1_t490482111 * L_13 = (Subject_1_t490482111 *)__this->get_publisher_4();
		V_1 = (Subject_1_t490482111 *)L_13;
		Subject_1_t490482111 * L_14 = V_1;
		if (!L_14)
		{
			goto IL_0082;
		}
	}
	{
		Subject_1_t490482111 * L_15 = V_1;
		int32_t L_16 = (int32_t)__this->get_value_3();
		NullCheck((Subject_1_t490482111 *)L_15);
		((  void (*) (Subject_1_t490482111 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Subject_1_t490482111 *)L_15, (int32_t)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
	}

IL_0082:
	{
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Int32>::SetValue(T)
extern "C"  void ReactiveProperty_1_SetValue_m886123376_gshared (ReactiveProperty_1_t3455747825 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_value_3(L_0);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Int32>::SetValueAndForceNotify(T)
extern "C"  void ReactiveProperty_1_SetValueAndForceNotify_m363687219_gshared (ReactiveProperty_1_t3455747825 * __this, int32_t ___value0, const MethodInfo* method)
{
	Subject_1_t490482111 * V_0 = NULL;
	{
		int32_t L_0 = ___value0;
		NullCheck((ReactiveProperty_1_t3455747825 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<System.Int32>::SetValue(T) */, (ReactiveProperty_1_t3455747825 *)__this, (int32_t)L_0);
		bool L_1 = (bool)__this->get_isDisposed_2();
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		return;
	}

IL_0013:
	{
		Subject_1_t490482111 * L_2 = (Subject_1_t490482111 *)__this->get_publisher_4();
		V_0 = (Subject_1_t490482111 *)L_2;
		Subject_1_t490482111 * L_3 = V_0;
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		Subject_1_t490482111 * L_4 = V_0;
		int32_t L_5 = (int32_t)__this->get_value_3();
		NullCheck((Subject_1_t490482111 *)L_4);
		((  void (*) (Subject_1_t490482111 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Subject_1_t490482111 *)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
	}

IL_002c:
	{
		return;
	}
}
// System.IDisposable UniRx.ReactiveProperty`1<System.Int32>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1_Subscribe_m4285240932_MetadataUsageId;
extern "C"  Il2CppObject * ReactiveProperty_1_Subscribe_m4285240932_gshared (ReactiveProperty_1_t3455747825 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1_Subscribe_m4285240932_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t490482111 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		bool L_0 = (bool)__this->get_isDisposed_2();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Il2CppObject* L_1 = ___observer0;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Int32>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_2 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_2;
	}

IL_0017:
	{
		Subject_1_t490482111 * L_3 = (Subject_1_t490482111 *)__this->get_publisher_4();
		if (L_3)
		{
			goto IL_002d;
		}
	}
	{
		Subject_1_t490482111 * L_4 = (Subject_1_t490482111 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Subject_1_t490482111 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_publisher_4(L_4);
	}

IL_002d:
	{
		Subject_1_t490482111 * L_5 = (Subject_1_t490482111 *)__this->get_publisher_4();
		V_0 = (Subject_1_t490482111 *)L_5;
		Subject_1_t490482111 * L_6 = V_0;
		if (!L_6)
		{
			goto IL_005b;
		}
	}
	{
		Subject_1_t490482111 * L_7 = V_0;
		Il2CppObject* L_8 = ___observer0;
		NullCheck((Subject_1_t490482111 *)L_7);
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (Subject_1_t490482111 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)((Subject_1_t490482111 *)L_7, (Il2CppObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		V_1 = (Il2CppObject *)L_9;
		bool L_10 = (bool)__this->get_canPublishValueOnSubscribe_1();
		if (!L_10)
		{
			goto IL_0059;
		}
	}
	{
		Il2CppObject* L_11 = ___observer0;
		int32_t L_12 = (int32_t)__this->get_value_3();
		NullCheck((Il2CppObject*)L_11);
		InterfaceActionInvoker1< int32_t >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Int32>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_11, (int32_t)L_12);
	}

IL_0059:
	{
		Il2CppObject * L_13 = V_1;
		return L_13;
	}

IL_005b:
	{
		Il2CppObject* L_14 = ___observer0;
		NullCheck((Il2CppObject*)L_14);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Int32>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_14);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_15 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_15;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Int32>::Dispose()
extern "C"  void ReactiveProperty_1_Dispose_m1024382818_gshared (ReactiveProperty_1_t3455747825 * __this, const MethodInfo* method)
{
	{
		NullCheck((ReactiveProperty_1_t3455747825 *)__this);
		VirtActionInvoker1< bool >::Invoke(12 /* System.Void UniRx.ReactiveProperty`1<System.Int32>::Dispose(System.Boolean) */, (ReactiveProperty_1_t3455747825 *)__this, (bool)1);
		GC_SuppressFinalize_m1160635446(NULL /*static, unused*/, (Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Int32>::Dispose(System.Boolean)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1_Dispose_m4214225241_MetadataUsageId;
extern "C"  void ReactiveProperty_1_Dispose_m4214225241_gshared (ReactiveProperty_1_t3455747825 * __this, bool ___disposing0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1_Dispose_m4214225241_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Subject_1_t490482111 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (bool)__this->get_isDisposed_2();
		if (L_0)
		{
			goto IL_0052;
		}
	}
	{
		__this->set_isDisposed_2((bool)1);
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_sourceConnection_5();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		Il2CppObject * L_3 = V_0;
		NullCheck((Il2CppObject *)L_3);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
		__this->set_sourceConnection_5((Il2CppObject *)NULL);
	}

IL_002c:
	{
		Subject_1_t490482111 * L_4 = (Subject_1_t490482111 *)__this->get_publisher_4();
		V_1 = (Subject_1_t490482111 *)L_4;
		Subject_1_t490482111 * L_5 = V_1;
		if (!L_5)
		{
			goto IL_0052;
		}
	}

IL_0039:
	try
	{ // begin try (depth: 1)
		Subject_1_t490482111 * L_6 = V_1;
		NullCheck((Subject_1_t490482111 *)L_6);
		((  void (*) (Subject_1_t490482111 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)((Subject_1_t490482111 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		IL2CPP_LEAVE(0x52, FINALLY_0044);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0044;
	}

FINALLY_0044:
	{ // begin finally (depth: 1)
		Subject_1_t490482111 * L_7 = V_1;
		NullCheck((Subject_1_t490482111 *)L_7);
		((  void (*) (Subject_1_t490482111 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((Subject_1_t490482111 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		__this->set_publisher_4((Subject_1_t490482111 *)NULL);
		IL2CPP_END_FINALLY(68)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(68)
	{
		IL2CPP_JUMP_TBL(0x52, IL_0052)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0052:
	{
		return;
	}
}
// System.String UniRx.ReactiveProperty`1<System.Int32>::ToString()
extern Il2CppCodeGenString* _stringLiteral3392903;
extern const uint32_t ReactiveProperty_1_ToString_m1266433294_MetadataUsageId;
extern "C"  String_t* ReactiveProperty_1_ToString_m1266433294_gshared (ReactiveProperty_1_t3455747825 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1_ToString_m1266433294_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B3_0 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get_value_3();
		goto IL_001a;
	}
	{
		G_B3_0 = _stringLiteral3392903;
		goto IL_002b;
	}

IL_001a:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_3();
		String_t* L_2 = Int32_ToString_m1286526384((int32_t*)L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_002b:
	{
		return G_B3_0;
	}
}
// System.Boolean UniRx.ReactiveProperty`1<System.Int32>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m2531383614_gshared (ReactiveProperty_1_t3455747825 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Int64>::.ctor()
extern Il2CppClass* Int64_t2847414882_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m2358756868_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m2358756868_gshared (ReactiveProperty_1_t3455747920 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m2358756868_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int64_t V_0 = 0;
	{
		Initobj (Int64_t2847414882_il2cpp_TypeInfo_var, (&V_0));
		int64_t L_0 = V_0;
		NullCheck((ReactiveProperty_1_t3455747920 *)__this);
		((  void (*) (ReactiveProperty_1_t3455747920 *, int64_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ReactiveProperty_1_t3455747920 *)__this, (int64_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Int64>::.ctor(T)
extern Il2CppClass* Int64_t2847414882_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m107020250_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m107020250_gshared (ReactiveProperty_1_t3455747920 * __this, int64_t ___initialValue0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m107020250_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int64_t V_0 = 0;
	{
		Initobj (Int64_t2847414882_il2cpp_TypeInfo_var, (&V_0));
		int64_t L_0 = V_0;
		__this->set_value_3(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		int64_t L_1 = ___initialValue0;
		NullCheck((ReactiveProperty_1_t3455747920 *)__this);
		VirtActionInvoker1< int64_t >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<System.Int64>::SetValue(T) */, (ReactiveProperty_1_t3455747920 *)__this, (int64_t)L_1);
		__this->set_canPublishValueOnSubscribe_1((bool)1);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Int64>::.ctor(UniRx.IObservable`1<T>)
extern Il2CppClass* Int64_t2847414882_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m1729138915_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m1729138915_gshared (ReactiveProperty_1_t3455747920 * __this, Il2CppObject* ___source0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m1729138915_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int64_t V_0 = 0;
	{
		Initobj (Int64_t2847414882_il2cpp_TypeInfo_var, (&V_0));
		int64_t L_0 = V_0;
		__this->set_value_3(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		__this->set_canPublishValueOnSubscribe_1((bool)0);
		Subject_1_t490482206 * L_1 = (Subject_1_t490482206 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Subject_1_t490482206 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_publisher_4(L_1);
		Il2CppObject* L_2 = ___source0;
		ReactivePropertyObserver_t3083397451 * L_3 = (ReactivePropertyObserver_t3083397451 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (ReactivePropertyObserver_t3083397451 *, ReactiveProperty_1_t3455747920 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_3, (ReactiveProperty_1_t3455747920 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_4 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Int64>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_2, (Il2CppObject*)L_3);
		__this->set_sourceConnection_5(L_4);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Int64>::.ctor(UniRx.IObservable`1<T>,T)
extern Il2CppClass* Int64_t2847414882_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m3845126587_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m3845126587_gshared (ReactiveProperty_1_t3455747920 * __this, Il2CppObject* ___source0, int64_t ___initialValue1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m3845126587_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int64_t V_0 = 0;
	{
		Initobj (Int64_t2847414882_il2cpp_TypeInfo_var, (&V_0));
		int64_t L_0 = V_0;
		__this->set_value_3(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		__this->set_canPublishValueOnSubscribe_1((bool)0);
		int64_t L_1 = ___initialValue1;
		NullCheck((ReactiveProperty_1_t3455747920 *)__this);
		VirtActionInvoker1< int64_t >::Invoke(7 /* System.Void UniRx.ReactiveProperty`1<System.Int64>::set_Value(T) */, (ReactiveProperty_1_t3455747920 *)__this, (int64_t)L_1);
		Subject_1_t490482206 * L_2 = (Subject_1_t490482206 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Subject_1_t490482206 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_publisher_4(L_2);
		Il2CppObject* L_3 = ___source0;
		ReactivePropertyObserver_t3083397451 * L_4 = (ReactivePropertyObserver_t3083397451 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (ReactivePropertyObserver_t3083397451 *, ReactiveProperty_1_t3455747920 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_4, (ReactiveProperty_1_t3455747920 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((Il2CppObject*)L_3);
		Il2CppObject * L_5 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Int64>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_3, (Il2CppObject*)L_4);
		__this->set_sourceConnection_5(L_5);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Int64>::.cctor()
extern Il2CppClass* UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__cctor_m3919889961_MetadataUsageId;
extern "C"  void ReactiveProperty_1__cctor_m3919889961_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__cctor_m3919889961_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		((ReactiveProperty_1_t3455747920_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->static_fields)->set_defaultEqualityComparer_0(L_0);
		return;
	}
}
// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1<System.Int64>::get_EqualityComparer()
extern "C"  Il2CppObject* ReactiveProperty_1_get_EqualityComparer_m2529372463_gshared (ReactiveProperty_1_t3455747920 * __this, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		Il2CppObject* L_0 = ((ReactiveProperty_1_t3455747920_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->static_fields)->get_defaultEqualityComparer_0();
		return L_0;
	}
}
// T UniRx.ReactiveProperty`1<System.Int64>::get_Value()
extern "C"  int64_t ReactiveProperty_1_get_Value_m3168728843_gshared (ReactiveProperty_1_t3455747920 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = (int64_t)__this->get_value_3();
		return L_0;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Int64>::set_Value(T)
extern "C"  void ReactiveProperty_1_set_Value_m2082580264_gshared (ReactiveProperty_1_t3455747920 * __this, int64_t ___value0, const MethodInfo* method)
{
	Subject_1_t490482206 * V_0 = NULL;
	Subject_1_t490482206 * V_1 = NULL;
	{
		bool L_0 = (bool)__this->get_canPublishValueOnSubscribe_1();
		if (L_0)
		{
			goto IL_003f;
		}
	}
	{
		__this->set_canPublishValueOnSubscribe_1((bool)1);
		int64_t L_1 = ___value0;
		NullCheck((ReactiveProperty_1_t3455747920 *)__this);
		VirtActionInvoker1< int64_t >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<System.Int64>::SetValue(T) */, (ReactiveProperty_1_t3455747920 *)__this, (int64_t)L_1);
		bool L_2 = (bool)__this->get_isDisposed_2();
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		return;
	}

IL_0025:
	{
		Subject_1_t490482206 * L_3 = (Subject_1_t490482206 *)__this->get_publisher_4();
		V_0 = (Subject_1_t490482206 *)L_3;
		Subject_1_t490482206 * L_4 = V_0;
		if (!L_4)
		{
			goto IL_003e;
		}
	}
	{
		Subject_1_t490482206 * L_5 = V_0;
		int64_t L_6 = (int64_t)__this->get_value_3();
		NullCheck((Subject_1_t490482206 *)L_5);
		((  void (*) (Subject_1_t490482206 *, int64_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Subject_1_t490482206 *)L_5, (int64_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
	}

IL_003e:
	{
		return;
	}

IL_003f:
	{
		NullCheck((ReactiveProperty_1_t3455747920 *)__this);
		Il2CppObject* L_7 = VirtFuncInvoker0< Il2CppObject* >::Invoke(10 /* System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1<System.Int64>::get_EqualityComparer() */, (ReactiveProperty_1_t3455747920 *)__this);
		int64_t L_8 = (int64_t)__this->get_value_3();
		int64_t L_9 = ___value0;
		NullCheck((Il2CppObject*)L_7);
		bool L_10 = InterfaceFuncInvoker2< bool, int64_t, int64_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Int64>::Equals(!0,!0) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12), (Il2CppObject*)L_7, (int64_t)L_8, (int64_t)L_9);
		if (L_10)
		{
			goto IL_0082;
		}
	}
	{
		int64_t L_11 = ___value0;
		NullCheck((ReactiveProperty_1_t3455747920 *)__this);
		VirtActionInvoker1< int64_t >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<System.Int64>::SetValue(T) */, (ReactiveProperty_1_t3455747920 *)__this, (int64_t)L_11);
		bool L_12 = (bool)__this->get_isDisposed_2();
		if (!L_12)
		{
			goto IL_0069;
		}
	}
	{
		return;
	}

IL_0069:
	{
		Subject_1_t490482206 * L_13 = (Subject_1_t490482206 *)__this->get_publisher_4();
		V_1 = (Subject_1_t490482206 *)L_13;
		Subject_1_t490482206 * L_14 = V_1;
		if (!L_14)
		{
			goto IL_0082;
		}
	}
	{
		Subject_1_t490482206 * L_15 = V_1;
		int64_t L_16 = (int64_t)__this->get_value_3();
		NullCheck((Subject_1_t490482206 *)L_15);
		((  void (*) (Subject_1_t490482206 *, int64_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Subject_1_t490482206 *)L_15, (int64_t)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
	}

IL_0082:
	{
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Int64>::SetValue(T)
extern "C"  void ReactiveProperty_1_SetValue_m2369624463_gshared (ReactiveProperty_1_t3455747920 * __this, int64_t ___value0, const MethodInfo* method)
{
	{
		int64_t L_0 = ___value0;
		__this->set_value_3(L_0);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Int64>::SetValueAndForceNotify(T)
extern "C"  void ReactiveProperty_1_SetValueAndForceNotify_m1783838994_gshared (ReactiveProperty_1_t3455747920 * __this, int64_t ___value0, const MethodInfo* method)
{
	Subject_1_t490482206 * V_0 = NULL;
	{
		int64_t L_0 = ___value0;
		NullCheck((ReactiveProperty_1_t3455747920 *)__this);
		VirtActionInvoker1< int64_t >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<System.Int64>::SetValue(T) */, (ReactiveProperty_1_t3455747920 *)__this, (int64_t)L_0);
		bool L_1 = (bool)__this->get_isDisposed_2();
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		return;
	}

IL_0013:
	{
		Subject_1_t490482206 * L_2 = (Subject_1_t490482206 *)__this->get_publisher_4();
		V_0 = (Subject_1_t490482206 *)L_2;
		Subject_1_t490482206 * L_3 = V_0;
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		Subject_1_t490482206 * L_4 = V_0;
		int64_t L_5 = (int64_t)__this->get_value_3();
		NullCheck((Subject_1_t490482206 *)L_4);
		((  void (*) (Subject_1_t490482206 *, int64_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Subject_1_t490482206 *)L_4, (int64_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
	}

IL_002c:
	{
		return;
	}
}
// System.IDisposable UniRx.ReactiveProperty`1<System.Int64>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1_Subscribe_m702201603_MetadataUsageId;
extern "C"  Il2CppObject * ReactiveProperty_1_Subscribe_m702201603_gshared (ReactiveProperty_1_t3455747920 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1_Subscribe_m702201603_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t490482206 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		bool L_0 = (bool)__this->get_isDisposed_2();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Il2CppObject* L_1 = ___observer0;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Int64>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_2 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_2;
	}

IL_0017:
	{
		Subject_1_t490482206 * L_3 = (Subject_1_t490482206 *)__this->get_publisher_4();
		if (L_3)
		{
			goto IL_002d;
		}
	}
	{
		Subject_1_t490482206 * L_4 = (Subject_1_t490482206 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Subject_1_t490482206 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_publisher_4(L_4);
	}

IL_002d:
	{
		Subject_1_t490482206 * L_5 = (Subject_1_t490482206 *)__this->get_publisher_4();
		V_0 = (Subject_1_t490482206 *)L_5;
		Subject_1_t490482206 * L_6 = V_0;
		if (!L_6)
		{
			goto IL_005b;
		}
	}
	{
		Subject_1_t490482206 * L_7 = V_0;
		Il2CppObject* L_8 = ___observer0;
		NullCheck((Subject_1_t490482206 *)L_7);
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (Subject_1_t490482206 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)((Subject_1_t490482206 *)L_7, (Il2CppObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		V_1 = (Il2CppObject *)L_9;
		bool L_10 = (bool)__this->get_canPublishValueOnSubscribe_1();
		if (!L_10)
		{
			goto IL_0059;
		}
	}
	{
		Il2CppObject* L_11 = ___observer0;
		int64_t L_12 = (int64_t)__this->get_value_3();
		NullCheck((Il2CppObject*)L_11);
		InterfaceActionInvoker1< int64_t >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Int64>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_11, (int64_t)L_12);
	}

IL_0059:
	{
		Il2CppObject * L_13 = V_1;
		return L_13;
	}

IL_005b:
	{
		Il2CppObject* L_14 = ___observer0;
		NullCheck((Il2CppObject*)L_14);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Int64>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_14);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_15 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_15;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Int64>::Dispose()
extern "C"  void ReactiveProperty_1_Dispose_m3215868225_gshared (ReactiveProperty_1_t3455747920 * __this, const MethodInfo* method)
{
	{
		NullCheck((ReactiveProperty_1_t3455747920 *)__this);
		VirtActionInvoker1< bool >::Invoke(12 /* System.Void UniRx.ReactiveProperty`1<System.Int64>::Dispose(System.Boolean) */, (ReactiveProperty_1_t3455747920 *)__this, (bool)1);
		GC_SuppressFinalize_m1160635446(NULL /*static, unused*/, (Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Int64>::Dispose(System.Boolean)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1_Dispose_m1078276344_MetadataUsageId;
extern "C"  void ReactiveProperty_1_Dispose_m1078276344_gshared (ReactiveProperty_1_t3455747920 * __this, bool ___disposing0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1_Dispose_m1078276344_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Subject_1_t490482206 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (bool)__this->get_isDisposed_2();
		if (L_0)
		{
			goto IL_0052;
		}
	}
	{
		__this->set_isDisposed_2((bool)1);
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_sourceConnection_5();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		Il2CppObject * L_3 = V_0;
		NullCheck((Il2CppObject *)L_3);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
		__this->set_sourceConnection_5((Il2CppObject *)NULL);
	}

IL_002c:
	{
		Subject_1_t490482206 * L_4 = (Subject_1_t490482206 *)__this->get_publisher_4();
		V_1 = (Subject_1_t490482206 *)L_4;
		Subject_1_t490482206 * L_5 = V_1;
		if (!L_5)
		{
			goto IL_0052;
		}
	}

IL_0039:
	try
	{ // begin try (depth: 1)
		Subject_1_t490482206 * L_6 = V_1;
		NullCheck((Subject_1_t490482206 *)L_6);
		((  void (*) (Subject_1_t490482206 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)((Subject_1_t490482206 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		IL2CPP_LEAVE(0x52, FINALLY_0044);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0044;
	}

FINALLY_0044:
	{ // begin finally (depth: 1)
		Subject_1_t490482206 * L_7 = V_1;
		NullCheck((Subject_1_t490482206 *)L_7);
		((  void (*) (Subject_1_t490482206 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((Subject_1_t490482206 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		__this->set_publisher_4((Subject_1_t490482206 *)NULL);
		IL2CPP_END_FINALLY(68)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(68)
	{
		IL2CPP_JUMP_TBL(0x52, IL_0052)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0052:
	{
		return;
	}
}
// System.String UniRx.ReactiveProperty`1<System.Int64>::ToString()
extern Il2CppCodeGenString* _stringLiteral3392903;
extern const uint32_t ReactiveProperty_1_ToString_m483004175_MetadataUsageId;
extern "C"  String_t* ReactiveProperty_1_ToString_m483004175_gshared (ReactiveProperty_1_t3455747920 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1_ToString_m483004175_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B3_0 = NULL;
	{
		int64_t L_0 = (int64_t)__this->get_value_3();
		goto IL_001a;
	}
	{
		G_B3_0 = _stringLiteral3392903;
		goto IL_002b;
	}

IL_001a:
	{
		int64_t* L_1 = (int64_t*)__this->get_address_of_value_3();
		String_t* L_2 = Int64_ToString_m3478011791((int64_t*)L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_002b:
	{
		return G_B3_0;
	}
}
// System.Boolean UniRx.ReactiveProperty`1<System.Int64>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m3942530047_gshared (ReactiveProperty_1_t3455747920 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Object>::.ctor()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m2182085490_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m2182085490_gshared (ReactiveProperty_1_t1445439458 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m2182085490_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_0 = V_0;
		NullCheck((ReactiveProperty_1_t1445439458 *)__this);
		((  void (*) (ReactiveProperty_1_t1445439458 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ReactiveProperty_1_t1445439458 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Object>::.ctor(T)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m3220142124_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m3220142124_gshared (ReactiveProperty_1_t1445439458 * __this, Il2CppObject * ___initialValue0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m3220142124_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_0 = V_0;
		__this->set_value_3(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___initialValue0;
		NullCheck((ReactiveProperty_1_t1445439458 *)__this);
		VirtActionInvoker1< Il2CppObject * >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<System.Object>::SetValue(T) */, (ReactiveProperty_1_t1445439458 *)__this, (Il2CppObject *)L_1);
		__this->set_canPublishValueOnSubscribe_1((bool)1);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Object>::.ctor(UniRx.IObservable`1<T>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m2559762385_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m2559762385_gshared (ReactiveProperty_1_t1445439458 * __this, Il2CppObject* ___source0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m2559762385_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_0 = V_0;
		__this->set_value_3(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		__this->set_canPublishValueOnSubscribe_1((bool)0);
		Subject_1_t2775141040 * L_1 = (Subject_1_t2775141040 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Subject_1_t2775141040 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_publisher_4(L_1);
		Il2CppObject* L_2 = ___source0;
		ReactivePropertyObserver_t1073088989 * L_3 = (ReactivePropertyObserver_t1073088989 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (ReactivePropertyObserver_t1073088989 *, ReactiveProperty_1_t1445439458 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_3, (ReactiveProperty_1_t1445439458 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_4 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_2, (Il2CppObject*)L_3);
		__this->set_sourceConnection_5(L_4);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Object>::.ctor(UniRx.IObservable`1<T>,T)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m3210364201_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m3210364201_gshared (ReactiveProperty_1_t1445439458 * __this, Il2CppObject* ___source0, Il2CppObject * ___initialValue1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m3210364201_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_0 = V_0;
		__this->set_value_3(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		__this->set_canPublishValueOnSubscribe_1((bool)0);
		Il2CppObject * L_1 = ___initialValue1;
		NullCheck((ReactiveProperty_1_t1445439458 *)__this);
		VirtActionInvoker1< Il2CppObject * >::Invoke(7 /* System.Void UniRx.ReactiveProperty`1<System.Object>::set_Value(T) */, (ReactiveProperty_1_t1445439458 *)__this, (Il2CppObject *)L_1);
		Subject_1_t2775141040 * L_2 = (Subject_1_t2775141040 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Subject_1_t2775141040 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_publisher_4(L_2);
		Il2CppObject* L_3 = ___source0;
		ReactivePropertyObserver_t1073088989 * L_4 = (ReactivePropertyObserver_t1073088989 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (ReactivePropertyObserver_t1073088989 *, ReactiveProperty_1_t1445439458 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_4, (ReactiveProperty_1_t1445439458 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((Il2CppObject*)L_3);
		Il2CppObject * L_5 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_3, (Il2CppObject*)L_4);
		__this->set_sourceConnection_5(L_5);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Object>::.cctor()
extern Il2CppClass* UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__cctor_m2738044539_MetadataUsageId;
extern "C"  void ReactiveProperty_1__cctor_m2738044539_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__cctor_m2738044539_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		((ReactiveProperty_1_t1445439458_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->static_fields)->set_defaultEqualityComparer_0(L_0);
		return;
	}
}
// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1<System.Object>::get_EqualityComparer()
extern "C"  Il2CppObject* ReactiveProperty_1_get_EqualityComparer_m3290095395_gshared (ReactiveProperty_1_t1445439458 * __this, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		Il2CppObject* L_0 = ((ReactiveProperty_1_t1445439458_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->static_fields)->get_defaultEqualityComparer_0();
		return L_0;
	}
}
// T UniRx.ReactiveProperty`1<System.Object>::get_Value()
extern "C"  Il2CppObject * ReactiveProperty_1_get_Value_m2793108311_gshared (ReactiveProperty_1_t1445439458 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_3();
		return L_0;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Object>::set_Value(T)
extern "C"  void ReactiveProperty_1_set_Value_m1580705402_gshared (ReactiveProperty_1_t1445439458 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	Subject_1_t2775141040 * V_0 = NULL;
	Subject_1_t2775141040 * V_1 = NULL;
	{
		bool L_0 = (bool)__this->get_canPublishValueOnSubscribe_1();
		if (L_0)
		{
			goto IL_003f;
		}
	}
	{
		__this->set_canPublishValueOnSubscribe_1((bool)1);
		Il2CppObject * L_1 = ___value0;
		NullCheck((ReactiveProperty_1_t1445439458 *)__this);
		VirtActionInvoker1< Il2CppObject * >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<System.Object>::SetValue(T) */, (ReactiveProperty_1_t1445439458 *)__this, (Il2CppObject *)L_1);
		bool L_2 = (bool)__this->get_isDisposed_2();
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		return;
	}

IL_0025:
	{
		Subject_1_t2775141040 * L_3 = (Subject_1_t2775141040 *)__this->get_publisher_4();
		V_0 = (Subject_1_t2775141040 *)L_3;
		Subject_1_t2775141040 * L_4 = V_0;
		if (!L_4)
		{
			goto IL_003e;
		}
	}
	{
		Subject_1_t2775141040 * L_5 = V_0;
		Il2CppObject * L_6 = (Il2CppObject *)__this->get_value_3();
		NullCheck((Subject_1_t2775141040 *)L_5);
		((  void (*) (Subject_1_t2775141040 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Subject_1_t2775141040 *)L_5, (Il2CppObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
	}

IL_003e:
	{
		return;
	}

IL_003f:
	{
		NullCheck((ReactiveProperty_1_t1445439458 *)__this);
		Il2CppObject* L_7 = VirtFuncInvoker0< Il2CppObject* >::Invoke(10 /* System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1<System.Object>::get_EqualityComparer() */, (ReactiveProperty_1_t1445439458 *)__this);
		Il2CppObject * L_8 = (Il2CppObject *)__this->get_value_3();
		Il2CppObject * L_9 = ___value0;
		NullCheck((Il2CppObject*)L_7);
		bool L_10 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Object>::Equals(!0,!0) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12), (Il2CppObject*)L_7, (Il2CppObject *)L_8, (Il2CppObject *)L_9);
		if (L_10)
		{
			goto IL_0082;
		}
	}
	{
		Il2CppObject * L_11 = ___value0;
		NullCheck((ReactiveProperty_1_t1445439458 *)__this);
		VirtActionInvoker1< Il2CppObject * >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<System.Object>::SetValue(T) */, (ReactiveProperty_1_t1445439458 *)__this, (Il2CppObject *)L_11);
		bool L_12 = (bool)__this->get_isDisposed_2();
		if (!L_12)
		{
			goto IL_0069;
		}
	}
	{
		return;
	}

IL_0069:
	{
		Subject_1_t2775141040 * L_13 = (Subject_1_t2775141040 *)__this->get_publisher_4();
		V_1 = (Subject_1_t2775141040 *)L_13;
		Subject_1_t2775141040 * L_14 = V_1;
		if (!L_14)
		{
			goto IL_0082;
		}
	}
	{
		Subject_1_t2775141040 * L_15 = V_1;
		Il2CppObject * L_16 = (Il2CppObject *)__this->get_value_3();
		NullCheck((Subject_1_t2775141040 *)L_15);
		((  void (*) (Subject_1_t2775141040 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Subject_1_t2775141040 *)L_15, (Il2CppObject *)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
	}

IL_0082:
	{
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Object>::SetValue(T)
extern "C"  void ReactiveProperty_1_SetValue_m4154550269_gshared (ReactiveProperty_1_t1445439458 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_value_3(L_0);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Object>::SetValueAndForceNotify(T)
extern "C"  void ReactiveProperty_1_SetValueAndForceNotify_m2537434880_gshared (ReactiveProperty_1_t1445439458 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	Subject_1_t2775141040 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___value0;
		NullCheck((ReactiveProperty_1_t1445439458 *)__this);
		VirtActionInvoker1< Il2CppObject * >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<System.Object>::SetValue(T) */, (ReactiveProperty_1_t1445439458 *)__this, (Il2CppObject *)L_0);
		bool L_1 = (bool)__this->get_isDisposed_2();
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		return;
	}

IL_0013:
	{
		Subject_1_t2775141040 * L_2 = (Subject_1_t2775141040 *)__this->get_publisher_4();
		V_0 = (Subject_1_t2775141040 *)L_2;
		Subject_1_t2775141040 * L_3 = V_0;
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		Subject_1_t2775141040 * L_4 = V_0;
		Il2CppObject * L_5 = (Il2CppObject *)__this->get_value_3();
		NullCheck((Subject_1_t2775141040 *)L_4);
		((  void (*) (Subject_1_t2775141040 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Subject_1_t2775141040 *)L_4, (Il2CppObject *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
	}

IL_002c:
	{
		return;
	}
}
// System.IDisposable UniRx.ReactiveProperty`1<System.Object>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1_Subscribe_m958004807_MetadataUsageId;
extern "C"  Il2CppObject * ReactiveProperty_1_Subscribe_m958004807_gshared (ReactiveProperty_1_t1445439458 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1_Subscribe_m958004807_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t2775141040 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		bool L_0 = (bool)__this->get_isDisposed_2();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Il2CppObject* L_1 = ___observer0;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_2 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_2;
	}

IL_0017:
	{
		Subject_1_t2775141040 * L_3 = (Subject_1_t2775141040 *)__this->get_publisher_4();
		if (L_3)
		{
			goto IL_002d;
		}
	}
	{
		Subject_1_t2775141040 * L_4 = (Subject_1_t2775141040 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Subject_1_t2775141040 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_publisher_4(L_4);
	}

IL_002d:
	{
		Subject_1_t2775141040 * L_5 = (Subject_1_t2775141040 *)__this->get_publisher_4();
		V_0 = (Subject_1_t2775141040 *)L_5;
		Subject_1_t2775141040 * L_6 = V_0;
		if (!L_6)
		{
			goto IL_005b;
		}
	}
	{
		Subject_1_t2775141040 * L_7 = V_0;
		Il2CppObject* L_8 = ___observer0;
		NullCheck((Subject_1_t2775141040 *)L_7);
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (Subject_1_t2775141040 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)((Subject_1_t2775141040 *)L_7, (Il2CppObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		V_1 = (Il2CppObject *)L_9;
		bool L_10 = (bool)__this->get_canPublishValueOnSubscribe_1();
		if (!L_10)
		{
			goto IL_0059;
		}
	}
	{
		Il2CppObject* L_11 = ___observer0;
		Il2CppObject * L_12 = (Il2CppObject *)__this->get_value_3();
		NullCheck((Il2CppObject*)L_11);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_11, (Il2CppObject *)L_12);
	}

IL_0059:
	{
		Il2CppObject * L_13 = V_1;
		return L_13;
	}

IL_005b:
	{
		Il2CppObject* L_14 = ___observer0;
		NullCheck((Il2CppObject*)L_14);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_14);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_15 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_15;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Object>::Dispose()
extern "C"  void ReactiveProperty_1_Dispose_m938398511_gshared (ReactiveProperty_1_t1445439458 * __this, const MethodInfo* method)
{
	{
		NullCheck((ReactiveProperty_1_t1445439458 *)__this);
		VirtActionInvoker1< bool >::Invoke(12 /* System.Void UniRx.ReactiveProperty`1<System.Object>::Dispose(System.Boolean) */, (ReactiveProperty_1_t1445439458 *)__this, (bool)1);
		GC_SuppressFinalize_m1160635446(NULL /*static, unused*/, (Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Object>::Dispose(System.Boolean)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1_Dispose_m431016550_MetadataUsageId;
extern "C"  void ReactiveProperty_1_Dispose_m431016550_gshared (ReactiveProperty_1_t1445439458 * __this, bool ___disposing0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1_Dispose_m431016550_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Subject_1_t2775141040 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (bool)__this->get_isDisposed_2();
		if (L_0)
		{
			goto IL_0052;
		}
	}
	{
		__this->set_isDisposed_2((bool)1);
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_sourceConnection_5();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		Il2CppObject * L_3 = V_0;
		NullCheck((Il2CppObject *)L_3);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
		__this->set_sourceConnection_5((Il2CppObject *)NULL);
	}

IL_002c:
	{
		Subject_1_t2775141040 * L_4 = (Subject_1_t2775141040 *)__this->get_publisher_4();
		V_1 = (Subject_1_t2775141040 *)L_4;
		Subject_1_t2775141040 * L_5 = V_1;
		if (!L_5)
		{
			goto IL_0052;
		}
	}

IL_0039:
	try
	{ // begin try (depth: 1)
		Subject_1_t2775141040 * L_6 = V_1;
		NullCheck((Subject_1_t2775141040 *)L_6);
		((  void (*) (Subject_1_t2775141040 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)((Subject_1_t2775141040 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		IL2CPP_LEAVE(0x52, FINALLY_0044);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0044;
	}

FINALLY_0044:
	{ // begin finally (depth: 1)
		Subject_1_t2775141040 * L_7 = V_1;
		NullCheck((Subject_1_t2775141040 *)L_7);
		((  void (*) (Subject_1_t2775141040 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((Subject_1_t2775141040 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		__this->set_publisher_4((Subject_1_t2775141040 *)NULL);
		IL2CPP_END_FINALLY(68)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(68)
	{
		IL2CPP_JUMP_TBL(0x52, IL_0052)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0052:
	{
		return;
	}
}
// System.String UniRx.ReactiveProperty`1<System.Object>::ToString()
extern Il2CppCodeGenString* _stringLiteral3392903;
extern const uint32_t ReactiveProperty_1_ToString_m2722346619_MetadataUsageId;
extern "C"  String_t* ReactiveProperty_1_ToString_m2722346619_gshared (ReactiveProperty_1_t1445439458 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1_ToString_m2722346619_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B3_0 = NULL;
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_3();
		if (L_0)
		{
			goto IL_001a;
		}
	}
	{
		G_B3_0 = _stringLiteral3392903;
		goto IL_002b;
	}

IL_001a:
	{
		Il2CppObject ** L_1 = (Il2CppObject **)__this->get_address_of_value_3();
		NullCheck((Il2CppObject *)(*L_1));
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*L_1));
		G_B3_0 = L_2;
	}

IL_002b:
	{
		return G_B3_0;
	}
}
// System.Boolean UniRx.ReactiveProperty`1<System.Object>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m3797922201_gshared (ReactiveProperty_1_t1445439458 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Single>::.ctor()
extern Il2CppClass* Single_t958209021_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m1840047419_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m1840047419_gshared (ReactiveProperty_1_t1566542059 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m1840047419_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		Initobj (Single_t958209021_il2cpp_TypeInfo_var, (&V_0));
		float L_0 = V_0;
		NullCheck((ReactiveProperty_1_t1566542059 *)__this);
		((  void (*) (ReactiveProperty_1_t1566542059 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ReactiveProperty_1_t1566542059 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Single>::.ctor(T)
extern Il2CppClass* Single_t958209021_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m1206896515_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m1206896515_gshared (ReactiveProperty_1_t1566542059 * __this, float ___initialValue0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m1206896515_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		Initobj (Single_t958209021_il2cpp_TypeInfo_var, (&V_0));
		float L_0 = V_0;
		__this->set_value_3(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		float L_1 = ___initialValue0;
		NullCheck((ReactiveProperty_1_t1566542059 *)__this);
		VirtActionInvoker1< float >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<System.Single>::SetValue(T) */, (ReactiveProperty_1_t1566542059 *)__this, (float)L_1);
		__this->set_canPublishValueOnSubscribe_1((bool)1);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Single>::.ctor(UniRx.IObservable`1<T>)
extern Il2CppClass* Single_t958209021_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m211573978_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m211573978_gshared (ReactiveProperty_1_t1566542059 * __this, Il2CppObject* ___source0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m211573978_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		Initobj (Single_t958209021_il2cpp_TypeInfo_var, (&V_0));
		float L_0 = V_0;
		__this->set_value_3(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		__this->set_canPublishValueOnSubscribe_1((bool)0);
		Subject_1_t2896243641 * L_1 = (Subject_1_t2896243641 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Subject_1_t2896243641 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_publisher_4(L_1);
		Il2CppObject* L_2 = ___source0;
		ReactivePropertyObserver_t1194191590 * L_3 = (ReactivePropertyObserver_t1194191590 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (ReactivePropertyObserver_t1194191590 *, ReactiveProperty_1_t1566542059 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_3, (ReactiveProperty_1_t1566542059 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_4 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Single>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_2, (Il2CppObject*)L_3);
		__this->set_sourceConnection_5(L_4);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Single>::.ctor(UniRx.IObservable`1<T>,T)
extern Il2CppClass* Single_t958209021_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m1459135474_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m1459135474_gshared (ReactiveProperty_1_t1566542059 * __this, Il2CppObject* ___source0, float ___initialValue1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m1459135474_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		Initobj (Single_t958209021_il2cpp_TypeInfo_var, (&V_0));
		float L_0 = V_0;
		__this->set_value_3(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		__this->set_canPublishValueOnSubscribe_1((bool)0);
		float L_1 = ___initialValue1;
		NullCheck((ReactiveProperty_1_t1566542059 *)__this);
		VirtActionInvoker1< float >::Invoke(7 /* System.Void UniRx.ReactiveProperty`1<System.Single>::set_Value(T) */, (ReactiveProperty_1_t1566542059 *)__this, (float)L_1);
		Subject_1_t2896243641 * L_2 = (Subject_1_t2896243641 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Subject_1_t2896243641 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_publisher_4(L_2);
		Il2CppObject* L_3 = ___source0;
		ReactivePropertyObserver_t1194191590 * L_4 = (ReactivePropertyObserver_t1194191590 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (ReactivePropertyObserver_t1194191590 *, ReactiveProperty_1_t1566542059 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_4, (ReactiveProperty_1_t1566542059 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((Il2CppObject*)L_3);
		Il2CppObject * L_5 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Single>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_3, (Il2CppObject*)L_4);
		__this->set_sourceConnection_5(L_5);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Single>::.cctor()
extern Il2CppClass* UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__cctor_m724798930_MetadataUsageId;
extern "C"  void ReactiveProperty_1__cctor_m724798930_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__cctor_m724798930_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		((ReactiveProperty_1_t1566542059_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->static_fields)->set_defaultEqualityComparer_0(L_0);
		return;
	}
}
// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1<System.Single>::get_EqualityComparer()
extern "C"  Il2CppObject* ReactiveProperty_1_get_EqualityComparer_m2339768890_gshared (ReactiveProperty_1_t1566542059 * __this, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		Il2CppObject* L_0 = ((ReactiveProperty_1_t1566542059_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->static_fields)->get_defaultEqualityComparer_0();
		return L_0;
	}
}
// T UniRx.ReactiveProperty`1<System.Single>::get_Value()
extern "C"  float ReactiveProperty_1_get_Value_m1116491936_gshared (ReactiveProperty_1_t1566542059 * __this, const MethodInfo* method)
{
	{
		float L_0 = (float)__this->get_value_3();
		return L_0;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Single>::set_Value(T)
extern "C"  void ReactiveProperty_1_set_Value_m1145205329_gshared (ReactiveProperty_1_t1566542059 * __this, float ___value0, const MethodInfo* method)
{
	Subject_1_t2896243641 * V_0 = NULL;
	Subject_1_t2896243641 * V_1 = NULL;
	{
		bool L_0 = (bool)__this->get_canPublishValueOnSubscribe_1();
		if (L_0)
		{
			goto IL_003f;
		}
	}
	{
		__this->set_canPublishValueOnSubscribe_1((bool)1);
		float L_1 = ___value0;
		NullCheck((ReactiveProperty_1_t1566542059 *)__this);
		VirtActionInvoker1< float >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<System.Single>::SetValue(T) */, (ReactiveProperty_1_t1566542059 *)__this, (float)L_1);
		bool L_2 = (bool)__this->get_isDisposed_2();
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		return;
	}

IL_0025:
	{
		Subject_1_t2896243641 * L_3 = (Subject_1_t2896243641 *)__this->get_publisher_4();
		V_0 = (Subject_1_t2896243641 *)L_3;
		Subject_1_t2896243641 * L_4 = V_0;
		if (!L_4)
		{
			goto IL_003e;
		}
	}
	{
		Subject_1_t2896243641 * L_5 = V_0;
		float L_6 = (float)__this->get_value_3();
		NullCheck((Subject_1_t2896243641 *)L_5);
		((  void (*) (Subject_1_t2896243641 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Subject_1_t2896243641 *)L_5, (float)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
	}

IL_003e:
	{
		return;
	}

IL_003f:
	{
		NullCheck((ReactiveProperty_1_t1566542059 *)__this);
		Il2CppObject* L_7 = VirtFuncInvoker0< Il2CppObject* >::Invoke(10 /* System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1<System.Single>::get_EqualityComparer() */, (ReactiveProperty_1_t1566542059 *)__this);
		float L_8 = (float)__this->get_value_3();
		float L_9 = ___value0;
		NullCheck((Il2CppObject*)L_7);
		bool L_10 = InterfaceFuncInvoker2< bool, float, float >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Single>::Equals(!0,!0) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12), (Il2CppObject*)L_7, (float)L_8, (float)L_9);
		if (L_10)
		{
			goto IL_0082;
		}
	}
	{
		float L_11 = ___value0;
		NullCheck((ReactiveProperty_1_t1566542059 *)__this);
		VirtActionInvoker1< float >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<System.Single>::SetValue(T) */, (ReactiveProperty_1_t1566542059 *)__this, (float)L_11);
		bool L_12 = (bool)__this->get_isDisposed_2();
		if (!L_12)
		{
			goto IL_0069;
		}
	}
	{
		return;
	}

IL_0069:
	{
		Subject_1_t2896243641 * L_13 = (Subject_1_t2896243641 *)__this->get_publisher_4();
		V_1 = (Subject_1_t2896243641 *)L_13;
		Subject_1_t2896243641 * L_14 = V_1;
		if (!L_14)
		{
			goto IL_0082;
		}
	}
	{
		Subject_1_t2896243641 * L_15 = V_1;
		float L_16 = (float)__this->get_value_3();
		NullCheck((Subject_1_t2896243641 *)L_15);
		((  void (*) (Subject_1_t2896243641 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Subject_1_t2896243641 *)L_15, (float)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
	}

IL_0082:
	{
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Single>::SetValue(T)
extern "C"  void ReactiveProperty_1_SetValue_m2477933894_gshared (ReactiveProperty_1_t1566542059 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_value_3(L_0);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Single>::SetValueAndForceNotify(T)
extern "C"  void ReactiveProperty_1_SetValueAndForceNotify_m3784939657_gshared (ReactiveProperty_1_t1566542059 * __this, float ___value0, const MethodInfo* method)
{
	Subject_1_t2896243641 * V_0 = NULL;
	{
		float L_0 = ___value0;
		NullCheck((ReactiveProperty_1_t1566542059 *)__this);
		VirtActionInvoker1< float >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<System.Single>::SetValue(T) */, (ReactiveProperty_1_t1566542059 *)__this, (float)L_0);
		bool L_1 = (bool)__this->get_isDisposed_2();
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		return;
	}

IL_0013:
	{
		Subject_1_t2896243641 * L_2 = (Subject_1_t2896243641 *)__this->get_publisher_4();
		V_0 = (Subject_1_t2896243641 *)L_2;
		Subject_1_t2896243641 * L_3 = V_0;
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		Subject_1_t2896243641 * L_4 = V_0;
		float L_5 = (float)__this->get_value_3();
		NullCheck((Subject_1_t2896243641 *)L_4);
		((  void (*) (Subject_1_t2896243641 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Subject_1_t2896243641 *)L_4, (float)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
	}

IL_002c:
	{
		return;
	}
}
// System.IDisposable UniRx.ReactiveProperty`1<System.Single>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1_Subscribe_m3501743376_MetadataUsageId;
extern "C"  Il2CppObject * ReactiveProperty_1_Subscribe_m3501743376_gshared (ReactiveProperty_1_t1566542059 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1_Subscribe_m3501743376_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t2896243641 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		bool L_0 = (bool)__this->get_isDisposed_2();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Il2CppObject* L_1 = ___observer0;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Single>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_2 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_2;
	}

IL_0017:
	{
		Subject_1_t2896243641 * L_3 = (Subject_1_t2896243641 *)__this->get_publisher_4();
		if (L_3)
		{
			goto IL_002d;
		}
	}
	{
		Subject_1_t2896243641 * L_4 = (Subject_1_t2896243641 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Subject_1_t2896243641 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_publisher_4(L_4);
	}

IL_002d:
	{
		Subject_1_t2896243641 * L_5 = (Subject_1_t2896243641 *)__this->get_publisher_4();
		V_0 = (Subject_1_t2896243641 *)L_5;
		Subject_1_t2896243641 * L_6 = V_0;
		if (!L_6)
		{
			goto IL_005b;
		}
	}
	{
		Subject_1_t2896243641 * L_7 = V_0;
		Il2CppObject* L_8 = ___observer0;
		NullCheck((Subject_1_t2896243641 *)L_7);
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (Subject_1_t2896243641 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)((Subject_1_t2896243641 *)L_7, (Il2CppObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		V_1 = (Il2CppObject *)L_9;
		bool L_10 = (bool)__this->get_canPublishValueOnSubscribe_1();
		if (!L_10)
		{
			goto IL_0059;
		}
	}
	{
		Il2CppObject* L_11 = ___observer0;
		float L_12 = (float)__this->get_value_3();
		NullCheck((Il2CppObject*)L_11);
		InterfaceActionInvoker1< float >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Single>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_11, (float)L_12);
	}

IL_0059:
	{
		Il2CppObject * L_13 = V_1;
		return L_13;
	}

IL_005b:
	{
		Il2CppObject* L_14 = ___observer0;
		NullCheck((Il2CppObject*)L_14);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Single>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_14);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_15 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_15;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Single>::Dispose()
extern "C"  void ReactiveProperty_1_Dispose_m2952294072_gshared (ReactiveProperty_1_t1566542059 * __this, const MethodInfo* method)
{
	{
		NullCheck((ReactiveProperty_1_t1566542059 *)__this);
		VirtActionInvoker1< bool >::Invoke(12 /* System.Void UniRx.ReactiveProperty`1<System.Single>::Dispose(System.Boolean) */, (ReactiveProperty_1_t1566542059 *)__this, (bool)1);
		GC_SuppressFinalize_m1160635446(NULL /*static, unused*/, (Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<System.Single>::Dispose(System.Boolean)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1_Dispose_m1035665967_MetadataUsageId;
extern "C"  void ReactiveProperty_1_Dispose_m1035665967_gshared (ReactiveProperty_1_t1566542059 * __this, bool ___disposing0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1_Dispose_m1035665967_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Subject_1_t2896243641 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (bool)__this->get_isDisposed_2();
		if (L_0)
		{
			goto IL_0052;
		}
	}
	{
		__this->set_isDisposed_2((bool)1);
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_sourceConnection_5();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		Il2CppObject * L_3 = V_0;
		NullCheck((Il2CppObject *)L_3);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
		__this->set_sourceConnection_5((Il2CppObject *)NULL);
	}

IL_002c:
	{
		Subject_1_t2896243641 * L_4 = (Subject_1_t2896243641 *)__this->get_publisher_4();
		V_1 = (Subject_1_t2896243641 *)L_4;
		Subject_1_t2896243641 * L_5 = V_1;
		if (!L_5)
		{
			goto IL_0052;
		}
	}

IL_0039:
	try
	{ // begin try (depth: 1)
		Subject_1_t2896243641 * L_6 = V_1;
		NullCheck((Subject_1_t2896243641 *)L_6);
		((  void (*) (Subject_1_t2896243641 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)((Subject_1_t2896243641 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		IL2CPP_LEAVE(0x52, FINALLY_0044);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0044;
	}

FINALLY_0044:
	{ // begin finally (depth: 1)
		Subject_1_t2896243641 * L_7 = V_1;
		NullCheck((Subject_1_t2896243641 *)L_7);
		((  void (*) (Subject_1_t2896243641 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((Subject_1_t2896243641 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		__this->set_publisher_4((Subject_1_t2896243641 *)NULL);
		IL2CPP_END_FINALLY(68)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(68)
	{
		IL2CPP_JUMP_TBL(0x52, IL_0052)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0052:
	{
		return;
	}
}
// System.String UniRx.ReactiveProperty`1<System.Single>::ToString()
extern Il2CppCodeGenString* _stringLiteral3392903;
extern const uint32_t ReactiveProperty_1_ToString_m728599570_MetadataUsageId;
extern "C"  String_t* ReactiveProperty_1_ToString_m728599570_gshared (ReactiveProperty_1_t1566542059 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1_ToString_m728599570_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B3_0 = NULL;
	{
		float L_0 = (float)__this->get_value_3();
		goto IL_001a;
	}
	{
		G_B3_0 = _stringLiteral3392903;
		goto IL_002b;
	}

IL_001a:
	{
		float* L_1 = (float*)__this->get_address_of_value_3();
		String_t* L_2 = Single_ToString_m5736032((float*)L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_002b:
	{
		return G_B3_0;
	}
}
// System.Boolean UniRx.ReactiveProperty`1<System.Single>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m470427760_gshared (ReactiveProperty_1_t1566542059 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void UniRx.ReactiveProperty`1<TableButtons/StatkaPoTableViev>::.ctor()
extern Il2CppClass* StatkaPoTableViev_t3508395817_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m1349893103_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m1349893103_gshared (ReactiveProperty_1_t4116728855 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m1349893103_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		Initobj (StatkaPoTableViev_t3508395817_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_0 = V_0;
		NullCheck((ReactiveProperty_1_t4116728855 *)__this);
		((  void (*) (ReactiveProperty_1_t4116728855 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ReactiveProperty_1_t4116728855 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<TableButtons/StatkaPoTableViev>::.ctor(T)
extern Il2CppClass* StatkaPoTableViev_t3508395817_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m3938220827_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m3938220827_gshared (ReactiveProperty_1_t4116728855 * __this, int32_t ___initialValue0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m3938220827_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		Initobj (StatkaPoTableViev_t3508395817_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_0 = V_0;
		__this->set_value_3(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_1 = ___initialValue0;
		NullCheck((ReactiveProperty_1_t4116728855 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<TableButtons/StatkaPoTableViev>::SetValue(T) */, (ReactiveProperty_1_t4116728855 *)__this, (int32_t)L_1);
		__this->set_canPublishValueOnSubscribe_1((bool)1);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<TableButtons/StatkaPoTableViev>::.ctor(UniRx.IObservable`1<T>)
extern Il2CppClass* StatkaPoTableViev_t3508395817_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m3258210370_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m3258210370_gshared (ReactiveProperty_1_t4116728855 * __this, Il2CppObject* ___source0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m3258210370_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		Initobj (StatkaPoTableViev_t3508395817_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_0 = V_0;
		__this->set_value_3(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		__this->set_canPublishValueOnSubscribe_1((bool)0);
		Subject_1_t1151463141 * L_1 = (Subject_1_t1151463141 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Subject_1_t1151463141 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_publisher_4(L_1);
		Il2CppObject* L_2 = ___source0;
		ReactivePropertyObserver_t3744378386 * L_3 = (ReactivePropertyObserver_t3744378386 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (ReactivePropertyObserver_t3744378386 *, ReactiveProperty_1_t4116728855 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_3, (ReactiveProperty_1_t4116728855 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_4 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<TableButtons/StatkaPoTableViev>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_2, (Il2CppObject*)L_3);
		__this->set_sourceConnection_5(L_4);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<TableButtons/StatkaPoTableViev>::.ctor(UniRx.IObservable`1<T>,T)
extern Il2CppClass* StatkaPoTableViev_t3508395817_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m109012314_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m109012314_gshared (ReactiveProperty_1_t4116728855 * __this, Il2CppObject* ___source0, int32_t ___initialValue1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m109012314_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		Initobj (StatkaPoTableViev_t3508395817_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_0 = V_0;
		__this->set_value_3(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		__this->set_canPublishValueOnSubscribe_1((bool)0);
		int32_t L_1 = ___initialValue1;
		NullCheck((ReactiveProperty_1_t4116728855 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(7 /* System.Void UniRx.ReactiveProperty`1<TableButtons/StatkaPoTableViev>::set_Value(T) */, (ReactiveProperty_1_t4116728855 *)__this, (int32_t)L_1);
		Subject_1_t1151463141 * L_2 = (Subject_1_t1151463141 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Subject_1_t1151463141 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_publisher_4(L_2);
		Il2CppObject* L_3 = ___source0;
		ReactivePropertyObserver_t3744378386 * L_4 = (ReactivePropertyObserver_t3744378386 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (ReactivePropertyObserver_t3744378386 *, ReactiveProperty_1_t4116728855 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_4, (ReactiveProperty_1_t4116728855 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((Il2CppObject*)L_3);
		Il2CppObject * L_5 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<TableButtons/StatkaPoTableViev>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_3, (Il2CppObject*)L_4);
		__this->set_sourceConnection_5(L_5);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<TableButtons/StatkaPoTableViev>::.cctor()
extern Il2CppClass* UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__cctor_m3456123242_MetadataUsageId;
extern "C"  void ReactiveProperty_1__cctor_m3456123242_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__cctor_m3456123242_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		((ReactiveProperty_1_t4116728855_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->static_fields)->set_defaultEqualityComparer_0(L_0);
		return;
	}
}
// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1<TableButtons/StatkaPoTableViev>::get_EqualityComparer()
extern "C"  Il2CppObject* ReactiveProperty_1_get_EqualityComparer_m1026221296_gshared (ReactiveProperty_1_t4116728855 * __this, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		Il2CppObject* L_0 = ((ReactiveProperty_1_t4116728855_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->static_fields)->get_defaultEqualityComparer_0();
		return L_0;
	}
}
// T UniRx.ReactiveProperty`1<TableButtons/StatkaPoTableViev>::get_Value()
extern "C"  int32_t ReactiveProperty_1_get_Value_m1445003754_gshared (ReactiveProperty_1_t4116728855 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_3();
		return L_0;
	}
}
// System.Void UniRx.ReactiveProperty`1<TableButtons/StatkaPoTableViev>::set_Value(T)
extern "C"  void ReactiveProperty_1_set_Value_m2212207081_gshared (ReactiveProperty_1_t4116728855 * __this, int32_t ___value0, const MethodInfo* method)
{
	Subject_1_t1151463141 * V_0 = NULL;
	Subject_1_t1151463141 * V_1 = NULL;
	{
		bool L_0 = (bool)__this->get_canPublishValueOnSubscribe_1();
		if (L_0)
		{
			goto IL_003f;
		}
	}
	{
		__this->set_canPublishValueOnSubscribe_1((bool)1);
		int32_t L_1 = ___value0;
		NullCheck((ReactiveProperty_1_t4116728855 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<TableButtons/StatkaPoTableViev>::SetValue(T) */, (ReactiveProperty_1_t4116728855 *)__this, (int32_t)L_1);
		bool L_2 = (bool)__this->get_isDisposed_2();
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		return;
	}

IL_0025:
	{
		Subject_1_t1151463141 * L_3 = (Subject_1_t1151463141 *)__this->get_publisher_4();
		V_0 = (Subject_1_t1151463141 *)L_3;
		Subject_1_t1151463141 * L_4 = V_0;
		if (!L_4)
		{
			goto IL_003e;
		}
	}
	{
		Subject_1_t1151463141 * L_5 = V_0;
		int32_t L_6 = (int32_t)__this->get_value_3();
		NullCheck((Subject_1_t1151463141 *)L_5);
		((  void (*) (Subject_1_t1151463141 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Subject_1_t1151463141 *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
	}

IL_003e:
	{
		return;
	}

IL_003f:
	{
		NullCheck((ReactiveProperty_1_t4116728855 *)__this);
		Il2CppObject* L_7 = VirtFuncInvoker0< Il2CppObject* >::Invoke(10 /* System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1<TableButtons/StatkaPoTableViev>::get_EqualityComparer() */, (ReactiveProperty_1_t4116728855 *)__this);
		int32_t L_8 = (int32_t)__this->get_value_3();
		int32_t L_9 = ___value0;
		NullCheck((Il2CppObject*)L_7);
		bool L_10 = InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<TableButtons/StatkaPoTableViev>::Equals(!0,!0) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12), (Il2CppObject*)L_7, (int32_t)L_8, (int32_t)L_9);
		if (L_10)
		{
			goto IL_0082;
		}
	}
	{
		int32_t L_11 = ___value0;
		NullCheck((ReactiveProperty_1_t4116728855 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<TableButtons/StatkaPoTableViev>::SetValue(T) */, (ReactiveProperty_1_t4116728855 *)__this, (int32_t)L_11);
		bool L_12 = (bool)__this->get_isDisposed_2();
		if (!L_12)
		{
			goto IL_0069;
		}
	}
	{
		return;
	}

IL_0069:
	{
		Subject_1_t1151463141 * L_13 = (Subject_1_t1151463141 *)__this->get_publisher_4();
		V_1 = (Subject_1_t1151463141 *)L_13;
		Subject_1_t1151463141 * L_14 = V_1;
		if (!L_14)
		{
			goto IL_0082;
		}
	}
	{
		Subject_1_t1151463141 * L_15 = V_1;
		int32_t L_16 = (int32_t)__this->get_value_3();
		NullCheck((Subject_1_t1151463141 *)L_15);
		((  void (*) (Subject_1_t1151463141 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Subject_1_t1151463141 *)L_15, (int32_t)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
	}

IL_0082:
	{
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<TableButtons/StatkaPoTableViev>::SetValue(T)
extern "C"  void ReactiveProperty_1_SetValue_m3205089966_gshared (ReactiveProperty_1_t4116728855 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_value_3(L_0);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<TableButtons/StatkaPoTableViev>::SetValueAndForceNotify(T)
extern "C"  void ReactiveProperty_1_SetValueAndForceNotify_m1745324017_gshared (ReactiveProperty_1_t4116728855 * __this, int32_t ___value0, const MethodInfo* method)
{
	Subject_1_t1151463141 * V_0 = NULL;
	{
		int32_t L_0 = ___value0;
		NullCheck((ReactiveProperty_1_t4116728855 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<TableButtons/StatkaPoTableViev>::SetValue(T) */, (ReactiveProperty_1_t4116728855 *)__this, (int32_t)L_0);
		bool L_1 = (bool)__this->get_isDisposed_2();
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		return;
	}

IL_0013:
	{
		Subject_1_t1151463141 * L_2 = (Subject_1_t1151463141 *)__this->get_publisher_4();
		V_0 = (Subject_1_t1151463141 *)L_2;
		Subject_1_t1151463141 * L_3 = V_0;
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		Subject_1_t1151463141 * L_4 = V_0;
		int32_t L_5 = (int32_t)__this->get_value_3();
		NullCheck((Subject_1_t1151463141 *)L_4);
		((  void (*) (Subject_1_t1151463141 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Subject_1_t1151463141 *)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
	}

IL_002c:
	{
		return;
	}
}
// System.IDisposable UniRx.ReactiveProperty`1<TableButtons/StatkaPoTableViev>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1_Subscribe_m837270370_MetadataUsageId;
extern "C"  Il2CppObject * ReactiveProperty_1_Subscribe_m837270370_gshared (ReactiveProperty_1_t4116728855 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1_Subscribe_m837270370_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t1151463141 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		bool L_0 = (bool)__this->get_isDisposed_2();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Il2CppObject* L_1 = ___observer0;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<TableButtons/StatkaPoTableViev>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_2 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_2;
	}

IL_0017:
	{
		Subject_1_t1151463141 * L_3 = (Subject_1_t1151463141 *)__this->get_publisher_4();
		if (L_3)
		{
			goto IL_002d;
		}
	}
	{
		Subject_1_t1151463141 * L_4 = (Subject_1_t1151463141 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Subject_1_t1151463141 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_publisher_4(L_4);
	}

IL_002d:
	{
		Subject_1_t1151463141 * L_5 = (Subject_1_t1151463141 *)__this->get_publisher_4();
		V_0 = (Subject_1_t1151463141 *)L_5;
		Subject_1_t1151463141 * L_6 = V_0;
		if (!L_6)
		{
			goto IL_005b;
		}
	}
	{
		Subject_1_t1151463141 * L_7 = V_0;
		Il2CppObject* L_8 = ___observer0;
		NullCheck((Subject_1_t1151463141 *)L_7);
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (Subject_1_t1151463141 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)((Subject_1_t1151463141 *)L_7, (Il2CppObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		V_1 = (Il2CppObject *)L_9;
		bool L_10 = (bool)__this->get_canPublishValueOnSubscribe_1();
		if (!L_10)
		{
			goto IL_0059;
		}
	}
	{
		Il2CppObject* L_11 = ___observer0;
		int32_t L_12 = (int32_t)__this->get_value_3();
		NullCheck((Il2CppObject*)L_11);
		InterfaceActionInvoker1< int32_t >::Invoke(2 /* System.Void UniRx.IObserver`1<TableButtons/StatkaPoTableViev>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_11, (int32_t)L_12);
	}

IL_0059:
	{
		Il2CppObject * L_13 = V_1;
		return L_13;
	}

IL_005b:
	{
		Il2CppObject* L_14 = ___observer0;
		NullCheck((Il2CppObject*)L_14);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<TableButtons/StatkaPoTableViev>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_14);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_15 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_15;
	}
}
// System.Void UniRx.ReactiveProperty`1<TableButtons/StatkaPoTableViev>::Dispose()
extern "C"  void ReactiveProperty_1_Dispose_m1724001824_gshared (ReactiveProperty_1_t4116728855 * __this, const MethodInfo* method)
{
	{
		NullCheck((ReactiveProperty_1_t4116728855 *)__this);
		VirtActionInvoker1< bool >::Invoke(12 /* System.Void UniRx.ReactiveProperty`1<TableButtons/StatkaPoTableViev>::Dispose(System.Boolean) */, (ReactiveProperty_1_t4116728855 *)__this, (bool)1);
		GC_SuppressFinalize_m1160635446(NULL /*static, unused*/, (Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<TableButtons/StatkaPoTableViev>::Dispose(System.Boolean)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1_Dispose_m372091799_MetadataUsageId;
extern "C"  void ReactiveProperty_1_Dispose_m372091799_gshared (ReactiveProperty_1_t4116728855 * __this, bool ___disposing0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1_Dispose_m372091799_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Subject_1_t1151463141 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (bool)__this->get_isDisposed_2();
		if (L_0)
		{
			goto IL_0052;
		}
	}
	{
		__this->set_isDisposed_2((bool)1);
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_sourceConnection_5();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		Il2CppObject * L_3 = V_0;
		NullCheck((Il2CppObject *)L_3);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
		__this->set_sourceConnection_5((Il2CppObject *)NULL);
	}

IL_002c:
	{
		Subject_1_t1151463141 * L_4 = (Subject_1_t1151463141 *)__this->get_publisher_4();
		V_1 = (Subject_1_t1151463141 *)L_4;
		Subject_1_t1151463141 * L_5 = V_1;
		if (!L_5)
		{
			goto IL_0052;
		}
	}

IL_0039:
	try
	{ // begin try (depth: 1)
		Subject_1_t1151463141 * L_6 = V_1;
		NullCheck((Subject_1_t1151463141 *)L_6);
		((  void (*) (Subject_1_t1151463141 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)((Subject_1_t1151463141 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		IL2CPP_LEAVE(0x52, FINALLY_0044);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0044;
	}

FINALLY_0044:
	{ // begin finally (depth: 1)
		Subject_1_t1151463141 * L_7 = V_1;
		NullCheck((Subject_1_t1151463141 *)L_7);
		((  void (*) (Subject_1_t1151463141 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((Subject_1_t1151463141 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		__this->set_publisher_4((Subject_1_t1151463141 *)NULL);
		IL2CPP_END_FINALLY(68)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(68)
	{
		IL2CPP_JUMP_TBL(0x52, IL_0052)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0052:
	{
		return;
	}
}
// System.String UniRx.ReactiveProperty`1<TableButtons/StatkaPoTableViev>::ToString()
extern Il2CppCodeGenString* _stringLiteral3392903;
extern const uint32_t ReactiveProperty_1_ToString_m2953153360_MetadataUsageId;
extern "C"  String_t* ReactiveProperty_1_ToString_m2953153360_gshared (ReactiveProperty_1_t4116728855 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1_ToString_m2953153360_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B3_0 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get_value_3();
		goto IL_001a;
	}
	{
		G_B3_0 = _stringLiteral3392903;
		goto IL_002b;
	}

IL_001a:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_3();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18), L_1);
		NullCheck((Il2CppObject *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_2);
		G_B3_0 = L_3;
	}

IL_002b:
	{
		return G_B3_0;
	}
}
// System.Boolean UniRx.ReactiveProperty`1<TableButtons/StatkaPoTableViev>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m3543207104_gshared (ReactiveProperty_1_t4116728855 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Bounds>::.ctor()
extern Il2CppClass* Bounds_t3518514978_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m3808573442_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m3808573442_gshared (ReactiveProperty_1_t4126848016 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m3808573442_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Bounds_t3518514978  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Bounds_t3518514978_il2cpp_TypeInfo_var, (&V_0));
		Bounds_t3518514978  L_0 = V_0;
		NullCheck((ReactiveProperty_1_t4126848016 *)__this);
		((  void (*) (ReactiveProperty_1_t4126848016 *, Bounds_t3518514978 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ReactiveProperty_1_t4126848016 *)__this, (Bounds_t3518514978 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Bounds>::.ctor(T)
extern Il2CppClass* Bounds_t3518514978_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m2101661084_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m2101661084_gshared (ReactiveProperty_1_t4126848016 * __this, Bounds_t3518514978  ___initialValue0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m2101661084_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Bounds_t3518514978  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Bounds_t3518514978_il2cpp_TypeInfo_var, (&V_0));
		Bounds_t3518514978  L_0 = V_0;
		__this->set_value_3(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Bounds_t3518514978  L_1 = ___initialValue0;
		NullCheck((ReactiveProperty_1_t4126848016 *)__this);
		VirtActionInvoker1< Bounds_t3518514978  >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<UnityEngine.Bounds>::SetValue(T) */, (ReactiveProperty_1_t4126848016 *)__this, (Bounds_t3518514978 )L_1);
		__this->set_canPublishValueOnSubscribe_1((bool)1);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Bounds>::.ctor(UniRx.IObservable`1<T>)
extern Il2CppClass* Bounds_t3518514978_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m869683297_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m869683297_gshared (ReactiveProperty_1_t4126848016 * __this, Il2CppObject* ___source0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m869683297_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Bounds_t3518514978  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Bounds_t3518514978_il2cpp_TypeInfo_var, (&V_0));
		Bounds_t3518514978  L_0 = V_0;
		__this->set_value_3(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		__this->set_canPublishValueOnSubscribe_1((bool)0);
		Subject_1_t1161582302 * L_1 = (Subject_1_t1161582302 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Subject_1_t1161582302 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_publisher_4(L_1);
		Il2CppObject* L_2 = ___source0;
		ReactivePropertyObserver_t3754497547 * L_3 = (ReactivePropertyObserver_t3754497547 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (ReactivePropertyObserver_t3754497547 *, ReactiveProperty_1_t4126848016 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_3, (ReactiveProperty_1_t4126848016 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_4 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<UnityEngine.Bounds>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_2, (Il2CppObject*)L_3);
		__this->set_sourceConnection_5(L_4);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Bounds>::.ctor(UniRx.IObservable`1<T>,T)
extern Il2CppClass* Bounds_t3518514978_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m2541998521_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m2541998521_gshared (ReactiveProperty_1_t4126848016 * __this, Il2CppObject* ___source0, Bounds_t3518514978  ___initialValue1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m2541998521_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Bounds_t3518514978  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Bounds_t3518514978_il2cpp_TypeInfo_var, (&V_0));
		Bounds_t3518514978  L_0 = V_0;
		__this->set_value_3(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		__this->set_canPublishValueOnSubscribe_1((bool)0);
		Bounds_t3518514978  L_1 = ___initialValue1;
		NullCheck((ReactiveProperty_1_t4126848016 *)__this);
		VirtActionInvoker1< Bounds_t3518514978  >::Invoke(7 /* System.Void UniRx.ReactiveProperty`1<UnityEngine.Bounds>::set_Value(T) */, (ReactiveProperty_1_t4126848016 *)__this, (Bounds_t3518514978 )L_1);
		Subject_1_t1161582302 * L_2 = (Subject_1_t1161582302 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Subject_1_t1161582302 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_publisher_4(L_2);
		Il2CppObject* L_3 = ___source0;
		ReactivePropertyObserver_t3754497547 * L_4 = (ReactivePropertyObserver_t3754497547 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (ReactivePropertyObserver_t3754497547 *, ReactiveProperty_1_t4126848016 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_4, (ReactiveProperty_1_t4126848016 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((Il2CppObject*)L_3);
		Il2CppObject * L_5 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<UnityEngine.Bounds>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_3, (Il2CppObject*)L_4);
		__this->set_sourceConnection_5(L_5);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Bounds>::.cctor()
extern Il2CppClass* UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__cctor_m1619563499_MetadataUsageId;
extern "C"  void ReactiveProperty_1__cctor_m1619563499_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__cctor_m1619563499_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		((ReactiveProperty_1_t4126848016_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->static_fields)->set_defaultEqualityComparer_0(L_0);
		return;
	}
}
// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1<UnityEngine.Bounds>::get_EqualityComparer()
extern "C"  Il2CppObject* ReactiveProperty_1_get_EqualityComparer_m3430053937_gshared (ReactiveProperty_1_t4126848016 * __this, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		Il2CppObject* L_0 = ((ReactiveProperty_1_t4126848016_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->static_fields)->get_defaultEqualityComparer_0();
		return L_0;
	}
}
// T UniRx.ReactiveProperty`1<UnityEngine.Bounds>::get_Value()
extern "C"  Bounds_t3518514978  ReactiveProperty_1_get_Value_m513908041_gshared (ReactiveProperty_1_t4126848016 * __this, const MethodInfo* method)
{
	{
		Bounds_t3518514978  L_0 = (Bounds_t3518514978 )__this->get_value_3();
		return L_0;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Bounds>::set_Value(T)
extern "C"  void ReactiveProperty_1_set_Value_m486851562_gshared (ReactiveProperty_1_t4126848016 * __this, Bounds_t3518514978  ___value0, const MethodInfo* method)
{
	Subject_1_t1161582302 * V_0 = NULL;
	Subject_1_t1161582302 * V_1 = NULL;
	{
		bool L_0 = (bool)__this->get_canPublishValueOnSubscribe_1();
		if (L_0)
		{
			goto IL_003f;
		}
	}
	{
		__this->set_canPublishValueOnSubscribe_1((bool)1);
		Bounds_t3518514978  L_1 = ___value0;
		NullCheck((ReactiveProperty_1_t4126848016 *)__this);
		VirtActionInvoker1< Bounds_t3518514978  >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<UnityEngine.Bounds>::SetValue(T) */, (ReactiveProperty_1_t4126848016 *)__this, (Bounds_t3518514978 )L_1);
		bool L_2 = (bool)__this->get_isDisposed_2();
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		return;
	}

IL_0025:
	{
		Subject_1_t1161582302 * L_3 = (Subject_1_t1161582302 *)__this->get_publisher_4();
		V_0 = (Subject_1_t1161582302 *)L_3;
		Subject_1_t1161582302 * L_4 = V_0;
		if (!L_4)
		{
			goto IL_003e;
		}
	}
	{
		Subject_1_t1161582302 * L_5 = V_0;
		Bounds_t3518514978  L_6 = (Bounds_t3518514978 )__this->get_value_3();
		NullCheck((Subject_1_t1161582302 *)L_5);
		((  void (*) (Subject_1_t1161582302 *, Bounds_t3518514978 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Subject_1_t1161582302 *)L_5, (Bounds_t3518514978 )L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
	}

IL_003e:
	{
		return;
	}

IL_003f:
	{
		NullCheck((ReactiveProperty_1_t4126848016 *)__this);
		Il2CppObject* L_7 = VirtFuncInvoker0< Il2CppObject* >::Invoke(10 /* System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1<UnityEngine.Bounds>::get_EqualityComparer() */, (ReactiveProperty_1_t4126848016 *)__this);
		Bounds_t3518514978  L_8 = (Bounds_t3518514978 )__this->get_value_3();
		Bounds_t3518514978  L_9 = ___value0;
		NullCheck((Il2CppObject*)L_7);
		bool L_10 = InterfaceFuncInvoker2< bool, Bounds_t3518514978 , Bounds_t3518514978  >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<UnityEngine.Bounds>::Equals(!0,!0) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12), (Il2CppObject*)L_7, (Bounds_t3518514978 )L_8, (Bounds_t3518514978 )L_9);
		if (L_10)
		{
			goto IL_0082;
		}
	}
	{
		Bounds_t3518514978  L_11 = ___value0;
		NullCheck((ReactiveProperty_1_t4126848016 *)__this);
		VirtActionInvoker1< Bounds_t3518514978  >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<UnityEngine.Bounds>::SetValue(T) */, (ReactiveProperty_1_t4126848016 *)__this, (Bounds_t3518514978 )L_11);
		bool L_12 = (bool)__this->get_isDisposed_2();
		if (!L_12)
		{
			goto IL_0069;
		}
	}
	{
		return;
	}

IL_0069:
	{
		Subject_1_t1161582302 * L_13 = (Subject_1_t1161582302 *)__this->get_publisher_4();
		V_1 = (Subject_1_t1161582302 *)L_13;
		Subject_1_t1161582302 * L_14 = V_1;
		if (!L_14)
		{
			goto IL_0082;
		}
	}
	{
		Subject_1_t1161582302 * L_15 = V_1;
		Bounds_t3518514978  L_16 = (Bounds_t3518514978 )__this->get_value_3();
		NullCheck((Subject_1_t1161582302 *)L_15);
		((  void (*) (Subject_1_t1161582302 *, Bounds_t3518514978 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Subject_1_t1161582302 *)L_15, (Bounds_t3518514978 )L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
	}

IL_0082:
	{
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Bounds>::SetValue(T)
extern "C"  void ReactiveProperty_1_SetValue_m3842169997_gshared (ReactiveProperty_1_t4126848016 * __this, Bounds_t3518514978  ___value0, const MethodInfo* method)
{
	{
		Bounds_t3518514978  L_0 = ___value0;
		__this->set_value_3(L_0);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Bounds>::SetValueAndForceNotify(T)
extern "C"  void ReactiveProperty_1_SetValueAndForceNotify_m4268779408_gshared (ReactiveProperty_1_t4126848016 * __this, Bounds_t3518514978  ___value0, const MethodInfo* method)
{
	Subject_1_t1161582302 * V_0 = NULL;
	{
		Bounds_t3518514978  L_0 = ___value0;
		NullCheck((ReactiveProperty_1_t4126848016 *)__this);
		VirtActionInvoker1< Bounds_t3518514978  >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<UnityEngine.Bounds>::SetValue(T) */, (ReactiveProperty_1_t4126848016 *)__this, (Bounds_t3518514978 )L_0);
		bool L_1 = (bool)__this->get_isDisposed_2();
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		return;
	}

IL_0013:
	{
		Subject_1_t1161582302 * L_2 = (Subject_1_t1161582302 *)__this->get_publisher_4();
		V_0 = (Subject_1_t1161582302 *)L_2;
		Subject_1_t1161582302 * L_3 = V_0;
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		Subject_1_t1161582302 * L_4 = V_0;
		Bounds_t3518514978  L_5 = (Bounds_t3518514978 )__this->get_value_3();
		NullCheck((Subject_1_t1161582302 *)L_4);
		((  void (*) (Subject_1_t1161582302 *, Bounds_t3518514978 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Subject_1_t1161582302 *)L_4, (Bounds_t3518514978 )L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
	}

IL_002c:
	{
		return;
	}
}
// System.IDisposable UniRx.ReactiveProperty`1<UnityEngine.Bounds>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1_Subscribe_m3642423105_MetadataUsageId;
extern "C"  Il2CppObject * ReactiveProperty_1_Subscribe_m3642423105_gshared (ReactiveProperty_1_t4126848016 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1_Subscribe_m3642423105_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t1161582302 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		bool L_0 = (bool)__this->get_isDisposed_2();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Il2CppObject* L_1 = ___observer0;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UnityEngine.Bounds>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_2 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_2;
	}

IL_0017:
	{
		Subject_1_t1161582302 * L_3 = (Subject_1_t1161582302 *)__this->get_publisher_4();
		if (L_3)
		{
			goto IL_002d;
		}
	}
	{
		Subject_1_t1161582302 * L_4 = (Subject_1_t1161582302 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Subject_1_t1161582302 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_publisher_4(L_4);
	}

IL_002d:
	{
		Subject_1_t1161582302 * L_5 = (Subject_1_t1161582302 *)__this->get_publisher_4();
		V_0 = (Subject_1_t1161582302 *)L_5;
		Subject_1_t1161582302 * L_6 = V_0;
		if (!L_6)
		{
			goto IL_005b;
		}
	}
	{
		Subject_1_t1161582302 * L_7 = V_0;
		Il2CppObject* L_8 = ___observer0;
		NullCheck((Subject_1_t1161582302 *)L_7);
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (Subject_1_t1161582302 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)((Subject_1_t1161582302 *)L_7, (Il2CppObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		V_1 = (Il2CppObject *)L_9;
		bool L_10 = (bool)__this->get_canPublishValueOnSubscribe_1();
		if (!L_10)
		{
			goto IL_0059;
		}
	}
	{
		Il2CppObject* L_11 = ___observer0;
		Bounds_t3518514978  L_12 = (Bounds_t3518514978 )__this->get_value_3();
		NullCheck((Il2CppObject*)L_11);
		InterfaceActionInvoker1< Bounds_t3518514978  >::Invoke(2 /* System.Void UniRx.IObserver`1<UnityEngine.Bounds>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_11, (Bounds_t3518514978 )L_12);
	}

IL_0059:
	{
		Il2CppObject * L_13 = V_1;
		return L_13;
	}

IL_005b:
	{
		Il2CppObject* L_14 = ___observer0;
		NullCheck((Il2CppObject*)L_14);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UnityEngine.Bounds>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_14);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_15 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_15;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Bounds>::Dispose()
extern "C"  void ReactiveProperty_1_Dispose_m625224639_gshared (ReactiveProperty_1_t4126848016 * __this, const MethodInfo* method)
{
	{
		NullCheck((ReactiveProperty_1_t4126848016 *)__this);
		VirtActionInvoker1< bool >::Invoke(12 /* System.Void UniRx.ReactiveProperty`1<UnityEngine.Bounds>::Dispose(System.Boolean) */, (ReactiveProperty_1_t4126848016 *)__this, (bool)1);
		GC_SuppressFinalize_m1160635446(NULL /*static, unused*/, (Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Bounds>::Dispose(System.Boolean)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1_Dispose_m2251810550_MetadataUsageId;
extern "C"  void ReactiveProperty_1_Dispose_m2251810550_gshared (ReactiveProperty_1_t4126848016 * __this, bool ___disposing0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1_Dispose_m2251810550_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Subject_1_t1161582302 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (bool)__this->get_isDisposed_2();
		if (L_0)
		{
			goto IL_0052;
		}
	}
	{
		__this->set_isDisposed_2((bool)1);
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_sourceConnection_5();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		Il2CppObject * L_3 = V_0;
		NullCheck((Il2CppObject *)L_3);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
		__this->set_sourceConnection_5((Il2CppObject *)NULL);
	}

IL_002c:
	{
		Subject_1_t1161582302 * L_4 = (Subject_1_t1161582302 *)__this->get_publisher_4();
		V_1 = (Subject_1_t1161582302 *)L_4;
		Subject_1_t1161582302 * L_5 = V_1;
		if (!L_5)
		{
			goto IL_0052;
		}
	}

IL_0039:
	try
	{ // begin try (depth: 1)
		Subject_1_t1161582302 * L_6 = V_1;
		NullCheck((Subject_1_t1161582302 *)L_6);
		((  void (*) (Subject_1_t1161582302 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)((Subject_1_t1161582302 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		IL2CPP_LEAVE(0x52, FINALLY_0044);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0044;
	}

FINALLY_0044:
	{ // begin finally (depth: 1)
		Subject_1_t1161582302 * L_7 = V_1;
		NullCheck((Subject_1_t1161582302 *)L_7);
		((  void (*) (Subject_1_t1161582302 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((Subject_1_t1161582302 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		__this->set_publisher_4((Subject_1_t1161582302 *)NULL);
		IL2CPP_END_FINALLY(68)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(68)
	{
		IL2CPP_JUMP_TBL(0x52, IL_0052)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0052:
	{
		return;
	}
}
// System.String UniRx.ReactiveProperty`1<UnityEngine.Bounds>::ToString()
extern Il2CppCodeGenString* _stringLiteral3392903;
extern const uint32_t ReactiveProperty_1_ToString_m125066769_MetadataUsageId;
extern "C"  String_t* ReactiveProperty_1_ToString_m125066769_gshared (ReactiveProperty_1_t4126848016 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1_ToString_m125066769_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B3_0 = NULL;
	{
		Bounds_t3518514978  L_0 = (Bounds_t3518514978 )__this->get_value_3();
		goto IL_001a;
	}
	{
		G_B3_0 = _stringLiteral3392903;
		goto IL_002b;
	}

IL_001a:
	{
		Bounds_t3518514978 * L_1 = (Bounds_t3518514978 *)__this->get_address_of_value_3();
		String_t* L_2 = Bounds_ToString_m1795228795((Bounds_t3518514978 *)L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_002b:
	{
		return G_B3_0;
	}
}
// System.Boolean UniRx.ReactiveProperty`1<UnityEngine.Bounds>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m564760001_gshared (ReactiveProperty_1_t4126848016 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Color>::.ctor()
extern Il2CppClass* Color_t1588175760_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m1209776928_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m1209776928_gshared (ReactiveProperty_1_t2196508798 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m1209776928_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Color_t1588175760  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Color_t1588175760_il2cpp_TypeInfo_var, (&V_0));
		Color_t1588175760  L_0 = V_0;
		NullCheck((ReactiveProperty_1_t2196508798 *)__this);
		((  void (*) (ReactiveProperty_1_t2196508798 *, Color_t1588175760 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ReactiveProperty_1_t2196508798 *)__this, (Color_t1588175760 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Color>::.ctor(T)
extern Il2CppClass* Color_t1588175760_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m3143347774_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m3143347774_gshared (ReactiveProperty_1_t2196508798 * __this, Color_t1588175760  ___initialValue0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m3143347774_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Color_t1588175760  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Color_t1588175760_il2cpp_TypeInfo_var, (&V_0));
		Color_t1588175760  L_0 = V_0;
		__this->set_value_3(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Color_t1588175760  L_1 = ___initialValue0;
		NullCheck((ReactiveProperty_1_t2196508798 *)__this);
		VirtActionInvoker1< Color_t1588175760  >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<UnityEngine.Color>::SetValue(T) */, (ReactiveProperty_1_t2196508798 *)__this, (Color_t1588175760 )L_1);
		__this->set_canPublishValueOnSubscribe_1((bool)1);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Color>::.ctor(UniRx.IObservable`1<T>)
extern Il2CppClass* Color_t1588175760_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m3815475455_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m3815475455_gshared (ReactiveProperty_1_t2196508798 * __this, Il2CppObject* ___source0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m3815475455_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Color_t1588175760  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Color_t1588175760_il2cpp_TypeInfo_var, (&V_0));
		Color_t1588175760  L_0 = V_0;
		__this->set_value_3(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		__this->set_canPublishValueOnSubscribe_1((bool)0);
		Subject_1_t3526210380 * L_1 = (Subject_1_t3526210380 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Subject_1_t3526210380 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_publisher_4(L_1);
		Il2CppObject* L_2 = ___source0;
		ReactivePropertyObserver_t1824158329 * L_3 = (ReactivePropertyObserver_t1824158329 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (ReactivePropertyObserver_t1824158329 *, ReactiveProperty_1_t2196508798 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_3, (ReactiveProperty_1_t2196508798 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_4 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<UnityEngine.Color>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_2, (Il2CppObject*)L_3);
		__this->set_sourceConnection_5(L_4);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Color>::.ctor(UniRx.IObservable`1<T>,T)
extern Il2CppClass* Color_t1588175760_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m3064814295_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m3064814295_gshared (ReactiveProperty_1_t2196508798 * __this, Il2CppObject* ___source0, Color_t1588175760  ___initialValue1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m3064814295_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Color_t1588175760  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Color_t1588175760_il2cpp_TypeInfo_var, (&V_0));
		Color_t1588175760  L_0 = V_0;
		__this->set_value_3(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		__this->set_canPublishValueOnSubscribe_1((bool)0);
		Color_t1588175760  L_1 = ___initialValue1;
		NullCheck((ReactiveProperty_1_t2196508798 *)__this);
		VirtActionInvoker1< Color_t1588175760  >::Invoke(7 /* System.Void UniRx.ReactiveProperty`1<UnityEngine.Color>::set_Value(T) */, (ReactiveProperty_1_t2196508798 *)__this, (Color_t1588175760 )L_1);
		Subject_1_t3526210380 * L_2 = (Subject_1_t3526210380 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Subject_1_t3526210380 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_publisher_4(L_2);
		Il2CppObject* L_3 = ___source0;
		ReactivePropertyObserver_t1824158329 * L_4 = (ReactivePropertyObserver_t1824158329 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (ReactivePropertyObserver_t1824158329 *, ReactiveProperty_1_t2196508798 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_4, (ReactiveProperty_1_t2196508798 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((Il2CppObject*)L_3);
		Il2CppObject * L_5 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<UnityEngine.Color>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_3, (Il2CppObject*)L_4);
		__this->set_sourceConnection_5(L_5);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Color>::.cctor()
extern Il2CppClass* UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__cctor_m2661250189_MetadataUsageId;
extern "C"  void ReactiveProperty_1__cctor_m2661250189_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__cctor_m2661250189_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		((ReactiveProperty_1_t2196508798_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->static_fields)->set_defaultEqualityComparer_0(L_0);
		return;
	}
}
// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1<UnityEngine.Color>::get_EqualityComparer()
extern "C"  Il2CppObject* ReactiveProperty_1_get_EqualityComparer_m357557557_gshared (ReactiveProperty_1_t2196508798 * __this, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		Il2CppObject* L_0 = ((ReactiveProperty_1_t2196508798_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->static_fields)->get_defaultEqualityComparer_0();
		return L_0;
	}
}
// T UniRx.ReactiveProperty`1<UnityEngine.Color>::get_Value()
extern "C"  Color_t1588175760  ReactiveProperty_1_get_Value_m2447971461_gshared (ReactiveProperty_1_t2196508798 * __this, const MethodInfo* method)
{
	{
		Color_t1588175760  L_0 = (Color_t1588175760 )__this->get_value_3();
		return L_0;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Color>::set_Value(T)
extern "C"  void ReactiveProperty_1_set_Value_m3180757900_gshared (ReactiveProperty_1_t2196508798 * __this, Color_t1588175760  ___value0, const MethodInfo* method)
{
	Subject_1_t3526210380 * V_0 = NULL;
	Subject_1_t3526210380 * V_1 = NULL;
	{
		bool L_0 = (bool)__this->get_canPublishValueOnSubscribe_1();
		if (L_0)
		{
			goto IL_003f;
		}
	}
	{
		__this->set_canPublishValueOnSubscribe_1((bool)1);
		Color_t1588175760  L_1 = ___value0;
		NullCheck((ReactiveProperty_1_t2196508798 *)__this);
		VirtActionInvoker1< Color_t1588175760  >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<UnityEngine.Color>::SetValue(T) */, (ReactiveProperty_1_t2196508798 *)__this, (Color_t1588175760 )L_1);
		bool L_2 = (bool)__this->get_isDisposed_2();
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		return;
	}

IL_0025:
	{
		Subject_1_t3526210380 * L_3 = (Subject_1_t3526210380 *)__this->get_publisher_4();
		V_0 = (Subject_1_t3526210380 *)L_3;
		Subject_1_t3526210380 * L_4 = V_0;
		if (!L_4)
		{
			goto IL_003e;
		}
	}
	{
		Subject_1_t3526210380 * L_5 = V_0;
		Color_t1588175760  L_6 = (Color_t1588175760 )__this->get_value_3();
		NullCheck((Subject_1_t3526210380 *)L_5);
		((  void (*) (Subject_1_t3526210380 *, Color_t1588175760 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Subject_1_t3526210380 *)L_5, (Color_t1588175760 )L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
	}

IL_003e:
	{
		return;
	}

IL_003f:
	{
		NullCheck((ReactiveProperty_1_t2196508798 *)__this);
		Il2CppObject* L_7 = VirtFuncInvoker0< Il2CppObject* >::Invoke(10 /* System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1<UnityEngine.Color>::get_EqualityComparer() */, (ReactiveProperty_1_t2196508798 *)__this);
		Color_t1588175760  L_8 = (Color_t1588175760 )__this->get_value_3();
		Color_t1588175760  L_9 = ___value0;
		NullCheck((Il2CppObject*)L_7);
		bool L_10 = InterfaceFuncInvoker2< bool, Color_t1588175760 , Color_t1588175760  >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<UnityEngine.Color>::Equals(!0,!0) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12), (Il2CppObject*)L_7, (Color_t1588175760 )L_8, (Color_t1588175760 )L_9);
		if (L_10)
		{
			goto IL_0082;
		}
	}
	{
		Color_t1588175760  L_11 = ___value0;
		NullCheck((ReactiveProperty_1_t2196508798 *)__this);
		VirtActionInvoker1< Color_t1588175760  >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<UnityEngine.Color>::SetValue(T) */, (ReactiveProperty_1_t2196508798 *)__this, (Color_t1588175760 )L_11);
		bool L_12 = (bool)__this->get_isDisposed_2();
		if (!L_12)
		{
			goto IL_0069;
		}
	}
	{
		return;
	}

IL_0069:
	{
		Subject_1_t3526210380 * L_13 = (Subject_1_t3526210380 *)__this->get_publisher_4();
		V_1 = (Subject_1_t3526210380 *)L_13;
		Subject_1_t3526210380 * L_14 = V_1;
		if (!L_14)
		{
			goto IL_0082;
		}
	}
	{
		Subject_1_t3526210380 * L_15 = V_1;
		Color_t1588175760  L_16 = (Color_t1588175760 )__this->get_value_3();
		NullCheck((Subject_1_t3526210380 *)L_15);
		((  void (*) (Subject_1_t3526210380 *, Color_t1588175760 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Subject_1_t3526210380 *)L_15, (Color_t1588175760 )L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
	}

IL_0082:
	{
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Color>::SetValue(T)
extern "C"  void ReactiveProperty_1_SetValue_m1296670891_gshared (ReactiveProperty_1_t2196508798 * __this, Color_t1588175760  ___value0, const MethodInfo* method)
{
	{
		Color_t1588175760  L_0 = ___value0;
		__this->set_value_3(L_0);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Color>::SetValueAndForceNotify(T)
extern "C"  void ReactiveProperty_1_SetValueAndForceNotify_m4182155054_gshared (ReactiveProperty_1_t2196508798 * __this, Color_t1588175760  ___value0, const MethodInfo* method)
{
	Subject_1_t3526210380 * V_0 = NULL;
	{
		Color_t1588175760  L_0 = ___value0;
		NullCheck((ReactiveProperty_1_t2196508798 *)__this);
		VirtActionInvoker1< Color_t1588175760  >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<UnityEngine.Color>::SetValue(T) */, (ReactiveProperty_1_t2196508798 *)__this, (Color_t1588175760 )L_0);
		bool L_1 = (bool)__this->get_isDisposed_2();
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		return;
	}

IL_0013:
	{
		Subject_1_t3526210380 * L_2 = (Subject_1_t3526210380 *)__this->get_publisher_4();
		V_0 = (Subject_1_t3526210380 *)L_2;
		Subject_1_t3526210380 * L_3 = V_0;
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		Subject_1_t3526210380 * L_4 = V_0;
		Color_t1588175760  L_5 = (Color_t1588175760 )__this->get_value_3();
		NullCheck((Subject_1_t3526210380 *)L_4);
		((  void (*) (Subject_1_t3526210380 *, Color_t1588175760 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Subject_1_t3526210380 *)L_4, (Color_t1588175760 )L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
	}

IL_002c:
	{
		return;
	}
}
// System.IDisposable UniRx.ReactiveProperty`1<UnityEngine.Color>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1_Subscribe_m168590965_MetadataUsageId;
extern "C"  Il2CppObject * ReactiveProperty_1_Subscribe_m168590965_gshared (ReactiveProperty_1_t2196508798 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1_Subscribe_m168590965_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t3526210380 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		bool L_0 = (bool)__this->get_isDisposed_2();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Il2CppObject* L_1 = ___observer0;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UnityEngine.Color>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_2 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_2;
	}

IL_0017:
	{
		Subject_1_t3526210380 * L_3 = (Subject_1_t3526210380 *)__this->get_publisher_4();
		if (L_3)
		{
			goto IL_002d;
		}
	}
	{
		Subject_1_t3526210380 * L_4 = (Subject_1_t3526210380 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Subject_1_t3526210380 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_publisher_4(L_4);
	}

IL_002d:
	{
		Subject_1_t3526210380 * L_5 = (Subject_1_t3526210380 *)__this->get_publisher_4();
		V_0 = (Subject_1_t3526210380 *)L_5;
		Subject_1_t3526210380 * L_6 = V_0;
		if (!L_6)
		{
			goto IL_005b;
		}
	}
	{
		Subject_1_t3526210380 * L_7 = V_0;
		Il2CppObject* L_8 = ___observer0;
		NullCheck((Subject_1_t3526210380 *)L_7);
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (Subject_1_t3526210380 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)((Subject_1_t3526210380 *)L_7, (Il2CppObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		V_1 = (Il2CppObject *)L_9;
		bool L_10 = (bool)__this->get_canPublishValueOnSubscribe_1();
		if (!L_10)
		{
			goto IL_0059;
		}
	}
	{
		Il2CppObject* L_11 = ___observer0;
		Color_t1588175760  L_12 = (Color_t1588175760 )__this->get_value_3();
		NullCheck((Il2CppObject*)L_11);
		InterfaceActionInvoker1< Color_t1588175760  >::Invoke(2 /* System.Void UniRx.IObserver`1<UnityEngine.Color>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_11, (Color_t1588175760 )L_12);
	}

IL_0059:
	{
		Il2CppObject * L_13 = V_1;
		return L_13;
	}

IL_005b:
	{
		Il2CppObject* L_14 = ___observer0;
		NullCheck((Il2CppObject*)L_14);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UnityEngine.Color>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_14);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_15 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_15;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Color>::Dispose()
extern "C"  void ReactiveProperty_1_Dispose_m2852740957_gshared (ReactiveProperty_1_t2196508798 * __this, const MethodInfo* method)
{
	{
		NullCheck((ReactiveProperty_1_t2196508798 *)__this);
		VirtActionInvoker1< bool >::Invoke(12 /* System.Void UniRx.ReactiveProperty`1<UnityEngine.Color>::Dispose(System.Boolean) */, (ReactiveProperty_1_t2196508798 *)__this, (bool)1);
		GC_SuppressFinalize_m1160635446(NULL /*static, unused*/, (Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Color>::Dispose(System.Boolean)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1_Dispose_m4191383060_MetadataUsageId;
extern "C"  void ReactiveProperty_1_Dispose_m4191383060_gshared (ReactiveProperty_1_t2196508798 * __this, bool ___disposing0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1_Dispose_m4191383060_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Subject_1_t3526210380 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (bool)__this->get_isDisposed_2();
		if (L_0)
		{
			goto IL_0052;
		}
	}
	{
		__this->set_isDisposed_2((bool)1);
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_sourceConnection_5();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		Il2CppObject * L_3 = V_0;
		NullCheck((Il2CppObject *)L_3);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
		__this->set_sourceConnection_5((Il2CppObject *)NULL);
	}

IL_002c:
	{
		Subject_1_t3526210380 * L_4 = (Subject_1_t3526210380 *)__this->get_publisher_4();
		V_1 = (Subject_1_t3526210380 *)L_4;
		Subject_1_t3526210380 * L_5 = V_1;
		if (!L_5)
		{
			goto IL_0052;
		}
	}

IL_0039:
	try
	{ // begin try (depth: 1)
		Subject_1_t3526210380 * L_6 = V_1;
		NullCheck((Subject_1_t3526210380 *)L_6);
		((  void (*) (Subject_1_t3526210380 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)((Subject_1_t3526210380 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		IL2CPP_LEAVE(0x52, FINALLY_0044);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0044;
	}

FINALLY_0044:
	{ // begin finally (depth: 1)
		Subject_1_t3526210380 * L_7 = V_1;
		NullCheck((Subject_1_t3526210380 *)L_7);
		((  void (*) (Subject_1_t3526210380 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((Subject_1_t3526210380 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		__this->set_publisher_4((Subject_1_t3526210380 *)NULL);
		IL2CPP_END_FINALLY(68)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(68)
	{
		IL2CPP_JUMP_TBL(0x52, IL_0052)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0052:
	{
		return;
	}
}
// System.String UniRx.ReactiveProperty`1<UnityEngine.Color>::ToString()
extern Il2CppCodeGenString* _stringLiteral3392903;
extern const uint32_t ReactiveProperty_1_ToString_m1231972749_MetadataUsageId;
extern "C"  String_t* ReactiveProperty_1_ToString_m1231972749_gshared (ReactiveProperty_1_t2196508798 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1_ToString_m1231972749_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B3_0 = NULL;
	{
		Color_t1588175760  L_0 = (Color_t1588175760 )__this->get_value_3();
		goto IL_001a;
	}
	{
		G_B3_0 = _stringLiteral3392903;
		goto IL_002b;
	}

IL_001a:
	{
		Color_t1588175760 * L_1 = (Color_t1588175760 *)__this->get_address_of_value_3();
		String_t* L_2 = Color_ToString_m2277845527((Color_t1588175760 *)L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_002b:
	{
		return G_B3_0;
	}
}
// System.Boolean UniRx.ReactiveProperty`1<UnityEngine.Color>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m2357356203_gshared (ReactiveProperty_1_t2196508798 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Quaternion>::.ctor()
extern Il2CppClass* Quaternion_t1891715979_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m79599915_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m79599915_gshared (ReactiveProperty_1_t2500049017 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m79599915_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Quaternion_t1891715979  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Quaternion_t1891715979_il2cpp_TypeInfo_var, (&V_0));
		Quaternion_t1891715979  L_0 = V_0;
		NullCheck((ReactiveProperty_1_t2500049017 *)__this);
		((  void (*) (ReactiveProperty_1_t2500049017 *, Quaternion_t1891715979 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ReactiveProperty_1_t2500049017 *)__this, (Quaternion_t1891715979 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Quaternion>::.ctor(T)
extern Il2CppClass* Quaternion_t1891715979_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m2467598739_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m2467598739_gshared (ReactiveProperty_1_t2500049017 * __this, Quaternion_t1891715979  ___initialValue0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m2467598739_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Quaternion_t1891715979  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Quaternion_t1891715979_il2cpp_TypeInfo_var, (&V_0));
		Quaternion_t1891715979  L_0 = V_0;
		__this->set_value_3(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Quaternion_t1891715979  L_1 = ___initialValue0;
		NullCheck((ReactiveProperty_1_t2500049017 *)__this);
		VirtActionInvoker1< Quaternion_t1891715979  >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<UnityEngine.Quaternion>::SetValue(T) */, (ReactiveProperty_1_t2500049017 *)__this, (Quaternion_t1891715979 )L_1);
		__this->set_canPublishValueOnSubscribe_1((bool)1);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Quaternion>::.ctor(UniRx.IObservable`1<T>)
extern Il2CppClass* Quaternion_t1891715979_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m3449126090_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m3449126090_gshared (ReactiveProperty_1_t2500049017 * __this, Il2CppObject* ___source0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m3449126090_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Quaternion_t1891715979  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Quaternion_t1891715979_il2cpp_TypeInfo_var, (&V_0));
		Quaternion_t1891715979  L_0 = V_0;
		__this->set_value_3(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		__this->set_canPublishValueOnSubscribe_1((bool)0);
		Subject_1_t3829750599 * L_1 = (Subject_1_t3829750599 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Subject_1_t3829750599 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_publisher_4(L_1);
		Il2CppObject* L_2 = ___source0;
		ReactivePropertyObserver_t2127698548 * L_3 = (ReactivePropertyObserver_t2127698548 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (ReactivePropertyObserver_t2127698548 *, ReactiveProperty_1_t2500049017 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_3, (ReactiveProperty_1_t2500049017 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_4 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<UnityEngine.Quaternion>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_2, (Il2CppObject*)L_3);
		__this->set_sourceConnection_5(L_4);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Quaternion>::.ctor(UniRx.IObservable`1<T>,T)
extern Il2CppClass* Quaternion_t1891715979_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m3190392802_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m3190392802_gshared (ReactiveProperty_1_t2500049017 * __this, Il2CppObject* ___source0, Quaternion_t1891715979  ___initialValue1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m3190392802_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Quaternion_t1891715979  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Quaternion_t1891715979_il2cpp_TypeInfo_var, (&V_0));
		Quaternion_t1891715979  L_0 = V_0;
		__this->set_value_3(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		__this->set_canPublishValueOnSubscribe_1((bool)0);
		Quaternion_t1891715979  L_1 = ___initialValue1;
		NullCheck((ReactiveProperty_1_t2500049017 *)__this);
		VirtActionInvoker1< Quaternion_t1891715979  >::Invoke(7 /* System.Void UniRx.ReactiveProperty`1<UnityEngine.Quaternion>::set_Value(T) */, (ReactiveProperty_1_t2500049017 *)__this, (Quaternion_t1891715979 )L_1);
		Subject_1_t3829750599 * L_2 = (Subject_1_t3829750599 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Subject_1_t3829750599 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_publisher_4(L_2);
		Il2CppObject* L_3 = ___source0;
		ReactivePropertyObserver_t2127698548 * L_4 = (ReactivePropertyObserver_t2127698548 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (ReactivePropertyObserver_t2127698548 *, ReactiveProperty_1_t2500049017 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_4, (ReactiveProperty_1_t2500049017 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((Il2CppObject*)L_3);
		Il2CppObject * L_5 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<UnityEngine.Quaternion>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_3, (Il2CppObject*)L_4);
		__this->set_sourceConnection_5(L_5);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Quaternion>::.cctor()
extern Il2CppClass* UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__cctor_m1985501154_MetadataUsageId;
extern "C"  void ReactiveProperty_1__cctor_m1985501154_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__cctor_m1985501154_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		((ReactiveProperty_1_t2500049017_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->static_fields)->set_defaultEqualityComparer_0(L_0);
		return;
	}
}
// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1<UnityEngine.Quaternion>::get_EqualityComparer()
extern "C"  Il2CppObject* ReactiveProperty_1_get_EqualityComparer_m1655527528_gshared (ReactiveProperty_1_t2500049017 * __this, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		Il2CppObject* L_0 = ((ReactiveProperty_1_t2500049017_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->static_fields)->get_defaultEqualityComparer_0();
		return L_0;
	}
}
// T UniRx.ReactiveProperty`1<UnityEngine.Quaternion>::get_Value()
extern "C"  Quaternion_t1891715979  ReactiveProperty_1_get_Value_m3947833714_gshared (ReactiveProperty_1_t2500049017 * __this, const MethodInfo* method)
{
	{
		Quaternion_t1891715979  L_0 = (Quaternion_t1891715979 )__this->get_value_3();
		return L_0;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Quaternion>::set_Value(T)
extern "C"  void ReactiveProperty_1_set_Value_m2094249057_gshared (ReactiveProperty_1_t2500049017 * __this, Quaternion_t1891715979  ___value0, const MethodInfo* method)
{
	Subject_1_t3829750599 * V_0 = NULL;
	Subject_1_t3829750599 * V_1 = NULL;
	{
		bool L_0 = (bool)__this->get_canPublishValueOnSubscribe_1();
		if (L_0)
		{
			goto IL_003f;
		}
	}
	{
		__this->set_canPublishValueOnSubscribe_1((bool)1);
		Quaternion_t1891715979  L_1 = ___value0;
		NullCheck((ReactiveProperty_1_t2500049017 *)__this);
		VirtActionInvoker1< Quaternion_t1891715979  >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<UnityEngine.Quaternion>::SetValue(T) */, (ReactiveProperty_1_t2500049017 *)__this, (Quaternion_t1891715979 )L_1);
		bool L_2 = (bool)__this->get_isDisposed_2();
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		return;
	}

IL_0025:
	{
		Subject_1_t3829750599 * L_3 = (Subject_1_t3829750599 *)__this->get_publisher_4();
		V_0 = (Subject_1_t3829750599 *)L_3;
		Subject_1_t3829750599 * L_4 = V_0;
		if (!L_4)
		{
			goto IL_003e;
		}
	}
	{
		Subject_1_t3829750599 * L_5 = V_0;
		Quaternion_t1891715979  L_6 = (Quaternion_t1891715979 )__this->get_value_3();
		NullCheck((Subject_1_t3829750599 *)L_5);
		((  void (*) (Subject_1_t3829750599 *, Quaternion_t1891715979 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Subject_1_t3829750599 *)L_5, (Quaternion_t1891715979 )L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
	}

IL_003e:
	{
		return;
	}

IL_003f:
	{
		NullCheck((ReactiveProperty_1_t2500049017 *)__this);
		Il2CppObject* L_7 = VirtFuncInvoker0< Il2CppObject* >::Invoke(10 /* System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1<UnityEngine.Quaternion>::get_EqualityComparer() */, (ReactiveProperty_1_t2500049017 *)__this);
		Quaternion_t1891715979  L_8 = (Quaternion_t1891715979 )__this->get_value_3();
		Quaternion_t1891715979  L_9 = ___value0;
		NullCheck((Il2CppObject*)L_7);
		bool L_10 = InterfaceFuncInvoker2< bool, Quaternion_t1891715979 , Quaternion_t1891715979  >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<UnityEngine.Quaternion>::Equals(!0,!0) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12), (Il2CppObject*)L_7, (Quaternion_t1891715979 )L_8, (Quaternion_t1891715979 )L_9);
		if (L_10)
		{
			goto IL_0082;
		}
	}
	{
		Quaternion_t1891715979  L_11 = ___value0;
		NullCheck((ReactiveProperty_1_t2500049017 *)__this);
		VirtActionInvoker1< Quaternion_t1891715979  >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<UnityEngine.Quaternion>::SetValue(T) */, (ReactiveProperty_1_t2500049017 *)__this, (Quaternion_t1891715979 )L_11);
		bool L_12 = (bool)__this->get_isDisposed_2();
		if (!L_12)
		{
			goto IL_0069;
		}
	}
	{
		return;
	}

IL_0069:
	{
		Subject_1_t3829750599 * L_13 = (Subject_1_t3829750599 *)__this->get_publisher_4();
		V_1 = (Subject_1_t3829750599 *)L_13;
		Subject_1_t3829750599 * L_14 = V_1;
		if (!L_14)
		{
			goto IL_0082;
		}
	}
	{
		Subject_1_t3829750599 * L_15 = V_1;
		Quaternion_t1891715979  L_16 = (Quaternion_t1891715979 )__this->get_value_3();
		NullCheck((Subject_1_t3829750599 *)L_15);
		((  void (*) (Subject_1_t3829750599 *, Quaternion_t1891715979 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Subject_1_t3829750599 *)L_15, (Quaternion_t1891715979 )L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
	}

IL_0082:
	{
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Quaternion>::SetValue(T)
extern "C"  void ReactiveProperty_1_SetValue_m568885558_gshared (ReactiveProperty_1_t2500049017 * __this, Quaternion_t1891715979  ___value0, const MethodInfo* method)
{
	{
		Quaternion_t1891715979  L_0 = ___value0;
		__this->set_value_3(L_0);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Quaternion>::SetValueAndForceNotify(T)
extern "C"  void ReactiveProperty_1_SetValueAndForceNotify_m679489657_gshared (ReactiveProperty_1_t2500049017 * __this, Quaternion_t1891715979  ___value0, const MethodInfo* method)
{
	Subject_1_t3829750599 * V_0 = NULL;
	{
		Quaternion_t1891715979  L_0 = ___value0;
		NullCheck((ReactiveProperty_1_t2500049017 *)__this);
		VirtActionInvoker1< Quaternion_t1891715979  >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<UnityEngine.Quaternion>::SetValue(T) */, (ReactiveProperty_1_t2500049017 *)__this, (Quaternion_t1891715979 )L_0);
		bool L_1 = (bool)__this->get_isDisposed_2();
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		return;
	}

IL_0013:
	{
		Subject_1_t3829750599 * L_2 = (Subject_1_t3829750599 *)__this->get_publisher_4();
		V_0 = (Subject_1_t3829750599 *)L_2;
		Subject_1_t3829750599 * L_3 = V_0;
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		Subject_1_t3829750599 * L_4 = V_0;
		Quaternion_t1891715979  L_5 = (Quaternion_t1891715979 )__this->get_value_3();
		NullCheck((Subject_1_t3829750599 *)L_4);
		((  void (*) (Subject_1_t3829750599 *, Quaternion_t1891715979 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Subject_1_t3829750599 *)L_4, (Quaternion_t1891715979 )L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
	}

IL_002c:
	{
		return;
	}
}
// System.IDisposable UniRx.ReactiveProperty`1<UnityEngine.Quaternion>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1_Subscribe_m228578026_MetadataUsageId;
extern "C"  Il2CppObject * ReactiveProperty_1_Subscribe_m228578026_gshared (ReactiveProperty_1_t2500049017 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1_Subscribe_m228578026_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t3829750599 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		bool L_0 = (bool)__this->get_isDisposed_2();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Il2CppObject* L_1 = ___observer0;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UnityEngine.Quaternion>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_2 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_2;
	}

IL_0017:
	{
		Subject_1_t3829750599 * L_3 = (Subject_1_t3829750599 *)__this->get_publisher_4();
		if (L_3)
		{
			goto IL_002d;
		}
	}
	{
		Subject_1_t3829750599 * L_4 = (Subject_1_t3829750599 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Subject_1_t3829750599 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_publisher_4(L_4);
	}

IL_002d:
	{
		Subject_1_t3829750599 * L_5 = (Subject_1_t3829750599 *)__this->get_publisher_4();
		V_0 = (Subject_1_t3829750599 *)L_5;
		Subject_1_t3829750599 * L_6 = V_0;
		if (!L_6)
		{
			goto IL_005b;
		}
	}
	{
		Subject_1_t3829750599 * L_7 = V_0;
		Il2CppObject* L_8 = ___observer0;
		NullCheck((Subject_1_t3829750599 *)L_7);
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (Subject_1_t3829750599 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)((Subject_1_t3829750599 *)L_7, (Il2CppObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		V_1 = (Il2CppObject *)L_9;
		bool L_10 = (bool)__this->get_canPublishValueOnSubscribe_1();
		if (!L_10)
		{
			goto IL_0059;
		}
	}
	{
		Il2CppObject* L_11 = ___observer0;
		Quaternion_t1891715979  L_12 = (Quaternion_t1891715979 )__this->get_value_3();
		NullCheck((Il2CppObject*)L_11);
		InterfaceActionInvoker1< Quaternion_t1891715979  >::Invoke(2 /* System.Void UniRx.IObserver`1<UnityEngine.Quaternion>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_11, (Quaternion_t1891715979 )L_12);
	}

IL_0059:
	{
		Il2CppObject * L_13 = V_1;
		return L_13;
	}

IL_005b:
	{
		Il2CppObject* L_14 = ___observer0;
		NullCheck((Il2CppObject*)L_14);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UnityEngine.Quaternion>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_14);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_15 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_15;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Quaternion>::Dispose()
extern "C"  void ReactiveProperty_1_Dispose_m3379357352_gshared (ReactiveProperty_1_t2500049017 * __this, const MethodInfo* method)
{
	{
		NullCheck((ReactiveProperty_1_t2500049017 *)__this);
		VirtActionInvoker1< bool >::Invoke(12 /* System.Void UniRx.ReactiveProperty`1<UnityEngine.Quaternion>::Dispose(System.Boolean) */, (ReactiveProperty_1_t2500049017 *)__this, (bool)1);
		GC_SuppressFinalize_m1160635446(NULL /*static, unused*/, (Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Quaternion>::Dispose(System.Boolean)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1_Dispose_m4165391903_MetadataUsageId;
extern "C"  void ReactiveProperty_1_Dispose_m4165391903_gshared (ReactiveProperty_1_t2500049017 * __this, bool ___disposing0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1_Dispose_m4165391903_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Subject_1_t3829750599 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (bool)__this->get_isDisposed_2();
		if (L_0)
		{
			goto IL_0052;
		}
	}
	{
		__this->set_isDisposed_2((bool)1);
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_sourceConnection_5();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		Il2CppObject * L_3 = V_0;
		NullCheck((Il2CppObject *)L_3);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
		__this->set_sourceConnection_5((Il2CppObject *)NULL);
	}

IL_002c:
	{
		Subject_1_t3829750599 * L_4 = (Subject_1_t3829750599 *)__this->get_publisher_4();
		V_1 = (Subject_1_t3829750599 *)L_4;
		Subject_1_t3829750599 * L_5 = V_1;
		if (!L_5)
		{
			goto IL_0052;
		}
	}

IL_0039:
	try
	{ // begin try (depth: 1)
		Subject_1_t3829750599 * L_6 = V_1;
		NullCheck((Subject_1_t3829750599 *)L_6);
		((  void (*) (Subject_1_t3829750599 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)((Subject_1_t3829750599 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		IL2CPP_LEAVE(0x52, FINALLY_0044);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0044;
	}

FINALLY_0044:
	{ // begin finally (depth: 1)
		Subject_1_t3829750599 * L_7 = V_1;
		NullCheck((Subject_1_t3829750599 *)L_7);
		((  void (*) (Subject_1_t3829750599 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((Subject_1_t3829750599 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		__this->set_publisher_4((Subject_1_t3829750599 *)NULL);
		IL2CPP_END_FINALLY(68)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(68)
	{
		IL2CPP_JUMP_TBL(0x52, IL_0052)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0052:
	{
		return;
	}
}
// System.String UniRx.ReactiveProperty`1<UnityEngine.Quaternion>::ToString()
extern Il2CppCodeGenString* _stringLiteral3392903;
extern const uint32_t ReactiveProperty_1_ToString_m1564948168_MetadataUsageId;
extern "C"  String_t* ReactiveProperty_1_ToString_m1564948168_gshared (ReactiveProperty_1_t2500049017 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1_ToString_m1564948168_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B3_0 = NULL;
	{
		Quaternion_t1891715979  L_0 = (Quaternion_t1891715979 )__this->get_value_3();
		goto IL_001a;
	}
	{
		G_B3_0 = _stringLiteral3392903;
		goto IL_002b;
	}

IL_001a:
	{
		Quaternion_t1891715979 * L_1 = (Quaternion_t1891715979 *)__this->get_address_of_value_3();
		String_t* L_2 = Quaternion_ToString_m1793285860((Quaternion_t1891715979 *)L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_002b:
	{
		return G_B3_0;
	}
}
// System.Boolean UniRx.ReactiveProperty`1<UnityEngine.Quaternion>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m1735105336_gshared (ReactiveProperty_1_t2500049017 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Rect>::.ctor()
extern Il2CppClass* Rect_t1525428817_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m2303028849_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m2303028849_gshared (ReactiveProperty_1_t2133761855 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m2303028849_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t1525428817  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Rect_t1525428817_il2cpp_TypeInfo_var, (&V_0));
		Rect_t1525428817  L_0 = V_0;
		NullCheck((ReactiveProperty_1_t2133761855 *)__this);
		((  void (*) (ReactiveProperty_1_t2133761855 *, Rect_t1525428817 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ReactiveProperty_1_t2133761855 *)__this, (Rect_t1525428817 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Rect>::.ctor(T)
extern Il2CppClass* Rect_t1525428817_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m2674418957_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m2674418957_gshared (ReactiveProperty_1_t2133761855 * __this, Rect_t1525428817  ___initialValue0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m2674418957_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t1525428817  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Rect_t1525428817_il2cpp_TypeInfo_var, (&V_0));
		Rect_t1525428817  L_0 = V_0;
		__this->set_value_3(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Rect_t1525428817  L_1 = ___initialValue0;
		NullCheck((ReactiveProperty_1_t2133761855 *)__this);
		VirtActionInvoker1< Rect_t1525428817  >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<UnityEngine.Rect>::SetValue(T) */, (ReactiveProperty_1_t2133761855 *)__this, (Rect_t1525428817 )L_1);
		__this->set_canPublishValueOnSubscribe_1((bool)1);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Rect>::.ctor(UniRx.IObservable`1<T>)
extern Il2CppClass* Rect_t1525428817_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m397310352_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m397310352_gshared (ReactiveProperty_1_t2133761855 * __this, Il2CppObject* ___source0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m397310352_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t1525428817  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Rect_t1525428817_il2cpp_TypeInfo_var, (&V_0));
		Rect_t1525428817  L_0 = V_0;
		__this->set_value_3(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		__this->set_canPublishValueOnSubscribe_1((bool)0);
		Subject_1_t3463463437 * L_1 = (Subject_1_t3463463437 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Subject_1_t3463463437 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_publisher_4(L_1);
		Il2CppObject* L_2 = ___source0;
		ReactivePropertyObserver_t1761411386 * L_3 = (ReactivePropertyObserver_t1761411386 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (ReactivePropertyObserver_t1761411386 *, ReactiveProperty_1_t2133761855 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_3, (ReactiveProperty_1_t2133761855 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_4 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<UnityEngine.Rect>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_2, (Il2CppObject*)L_3);
		__this->set_sourceConnection_5(L_4);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Rect>::.ctor(UniRx.IObservable`1<T>,T)
extern Il2CppClass* Rect_t1525428817_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m3858131752_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m3858131752_gshared (ReactiveProperty_1_t2133761855 * __this, Il2CppObject* ___source0, Rect_t1525428817  ___initialValue1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m3858131752_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t1525428817  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Rect_t1525428817_il2cpp_TypeInfo_var, (&V_0));
		Rect_t1525428817  L_0 = V_0;
		__this->set_value_3(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		__this->set_canPublishValueOnSubscribe_1((bool)0);
		Rect_t1525428817  L_1 = ___initialValue1;
		NullCheck((ReactiveProperty_1_t2133761855 *)__this);
		VirtActionInvoker1< Rect_t1525428817  >::Invoke(7 /* System.Void UniRx.ReactiveProperty`1<UnityEngine.Rect>::set_Value(T) */, (ReactiveProperty_1_t2133761855 *)__this, (Rect_t1525428817 )L_1);
		Subject_1_t3463463437 * L_2 = (Subject_1_t3463463437 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Subject_1_t3463463437 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_publisher_4(L_2);
		Il2CppObject* L_3 = ___source0;
		ReactivePropertyObserver_t1761411386 * L_4 = (ReactivePropertyObserver_t1761411386 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (ReactivePropertyObserver_t1761411386 *, ReactiveProperty_1_t2133761855 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_4, (ReactiveProperty_1_t2133761855 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((Il2CppObject*)L_3);
		Il2CppObject * L_5 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<UnityEngine.Rect>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_3, (Il2CppObject*)L_4);
		__this->set_sourceConnection_5(L_5);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Rect>::.cctor()
extern Il2CppClass* UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__cctor_m2192321372_MetadataUsageId;
extern "C"  void ReactiveProperty_1__cctor_m2192321372_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__cctor_m2192321372_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		((ReactiveProperty_1_t2133761855_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->static_fields)->set_defaultEqualityComparer_0(L_0);
		return;
	}
}
// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1<UnityEngine.Rect>::get_EqualityComparer()
extern "C"  Il2CppObject* ReactiveProperty_1_get_EqualityComparer_m1558262178_gshared (ReactiveProperty_1_t2133761855 * __this, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		Il2CppObject* L_0 = ((ReactiveProperty_1_t2133761855_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->static_fields)->get_defaultEqualityComparer_0();
		return L_0;
	}
}
// T UniRx.ReactiveProperty`1<UnityEngine.Rect>::get_Value()
extern "C"  Rect_t1525428817  ReactiveProperty_1_get_Value_m1076111480_gshared (ReactiveProperty_1_t2133761855 * __this, const MethodInfo* method)
{
	{
		Rect_t1525428817  L_0 = (Rect_t1525428817 )__this->get_value_3();
		return L_0;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Rect>::set_Value(T)
extern "C"  void ReactiveProperty_1_set_Value_m3418176219_gshared (ReactiveProperty_1_t2133761855 * __this, Rect_t1525428817  ___value0, const MethodInfo* method)
{
	Subject_1_t3463463437 * V_0 = NULL;
	Subject_1_t3463463437 * V_1 = NULL;
	{
		bool L_0 = (bool)__this->get_canPublishValueOnSubscribe_1();
		if (L_0)
		{
			goto IL_003f;
		}
	}
	{
		__this->set_canPublishValueOnSubscribe_1((bool)1);
		Rect_t1525428817  L_1 = ___value0;
		NullCheck((ReactiveProperty_1_t2133761855 *)__this);
		VirtActionInvoker1< Rect_t1525428817  >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<UnityEngine.Rect>::SetValue(T) */, (ReactiveProperty_1_t2133761855 *)__this, (Rect_t1525428817 )L_1);
		bool L_2 = (bool)__this->get_isDisposed_2();
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		return;
	}

IL_0025:
	{
		Subject_1_t3463463437 * L_3 = (Subject_1_t3463463437 *)__this->get_publisher_4();
		V_0 = (Subject_1_t3463463437 *)L_3;
		Subject_1_t3463463437 * L_4 = V_0;
		if (!L_4)
		{
			goto IL_003e;
		}
	}
	{
		Subject_1_t3463463437 * L_5 = V_0;
		Rect_t1525428817  L_6 = (Rect_t1525428817 )__this->get_value_3();
		NullCheck((Subject_1_t3463463437 *)L_5);
		((  void (*) (Subject_1_t3463463437 *, Rect_t1525428817 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Subject_1_t3463463437 *)L_5, (Rect_t1525428817 )L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
	}

IL_003e:
	{
		return;
	}

IL_003f:
	{
		NullCheck((ReactiveProperty_1_t2133761855 *)__this);
		Il2CppObject* L_7 = VirtFuncInvoker0< Il2CppObject* >::Invoke(10 /* System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1<UnityEngine.Rect>::get_EqualityComparer() */, (ReactiveProperty_1_t2133761855 *)__this);
		Rect_t1525428817  L_8 = (Rect_t1525428817 )__this->get_value_3();
		Rect_t1525428817  L_9 = ___value0;
		NullCheck((Il2CppObject*)L_7);
		bool L_10 = InterfaceFuncInvoker2< bool, Rect_t1525428817 , Rect_t1525428817  >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<UnityEngine.Rect>::Equals(!0,!0) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12), (Il2CppObject*)L_7, (Rect_t1525428817 )L_8, (Rect_t1525428817 )L_9);
		if (L_10)
		{
			goto IL_0082;
		}
	}
	{
		Rect_t1525428817  L_11 = ___value0;
		NullCheck((ReactiveProperty_1_t2133761855 *)__this);
		VirtActionInvoker1< Rect_t1525428817  >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<UnityEngine.Rect>::SetValue(T) */, (ReactiveProperty_1_t2133761855 *)__this, (Rect_t1525428817 )L_11);
		bool L_12 = (bool)__this->get_isDisposed_2();
		if (!L_12)
		{
			goto IL_0069;
		}
	}
	{
		return;
	}

IL_0069:
	{
		Subject_1_t3463463437 * L_13 = (Subject_1_t3463463437 *)__this->get_publisher_4();
		V_1 = (Subject_1_t3463463437 *)L_13;
		Subject_1_t3463463437 * L_14 = V_1;
		if (!L_14)
		{
			goto IL_0082;
		}
	}
	{
		Subject_1_t3463463437 * L_15 = V_1;
		Rect_t1525428817  L_16 = (Rect_t1525428817 )__this->get_value_3();
		NullCheck((Subject_1_t3463463437 *)L_15);
		((  void (*) (Subject_1_t3463463437 *, Rect_t1525428817 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Subject_1_t3463463437 *)L_15, (Rect_t1525428817 )L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
	}

IL_0082:
	{
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Rect>::SetValue(T)
extern "C"  void ReactiveProperty_1_SetValue_m2966897532_gshared (ReactiveProperty_1_t2133761855 * __this, Rect_t1525428817  ___value0, const MethodInfo* method)
{
	{
		Rect_t1525428817  L_0 = ___value0;
		__this->set_value_3(L_0);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Rect>::SetValueAndForceNotify(T)
extern "C"  void ReactiveProperty_1_SetValueAndForceNotify_m3614605375_gshared (ReactiveProperty_1_t2133761855 * __this, Rect_t1525428817  ___value0, const MethodInfo* method)
{
	Subject_1_t3463463437 * V_0 = NULL;
	{
		Rect_t1525428817  L_0 = ___value0;
		NullCheck((ReactiveProperty_1_t2133761855 *)__this);
		VirtActionInvoker1< Rect_t1525428817  >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<UnityEngine.Rect>::SetValue(T) */, (ReactiveProperty_1_t2133761855 *)__this, (Rect_t1525428817 )L_0);
		bool L_1 = (bool)__this->get_isDisposed_2();
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		return;
	}

IL_0013:
	{
		Subject_1_t3463463437 * L_2 = (Subject_1_t3463463437 *)__this->get_publisher_4();
		V_0 = (Subject_1_t3463463437 *)L_2;
		Subject_1_t3463463437 * L_3 = V_0;
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		Subject_1_t3463463437 * L_4 = V_0;
		Rect_t1525428817  L_5 = (Rect_t1525428817 )__this->get_value_3();
		NullCheck((Subject_1_t3463463437 *)L_4);
		((  void (*) (Subject_1_t3463463437 *, Rect_t1525428817 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Subject_1_t3463463437 *)L_4, (Rect_t1525428817 )L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
	}

IL_002c:
	{
		return;
	}
}
// System.IDisposable UniRx.ReactiveProperty`1<UnityEngine.Rect>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1_Subscribe_m1110078960_MetadataUsageId;
extern "C"  Il2CppObject * ReactiveProperty_1_Subscribe_m1110078960_gshared (ReactiveProperty_1_t2133761855 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1_Subscribe_m1110078960_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t3463463437 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		bool L_0 = (bool)__this->get_isDisposed_2();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Il2CppObject* L_1 = ___observer0;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UnityEngine.Rect>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_2 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_2;
	}

IL_0017:
	{
		Subject_1_t3463463437 * L_3 = (Subject_1_t3463463437 *)__this->get_publisher_4();
		if (L_3)
		{
			goto IL_002d;
		}
	}
	{
		Subject_1_t3463463437 * L_4 = (Subject_1_t3463463437 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Subject_1_t3463463437 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_publisher_4(L_4);
	}

IL_002d:
	{
		Subject_1_t3463463437 * L_5 = (Subject_1_t3463463437 *)__this->get_publisher_4();
		V_0 = (Subject_1_t3463463437 *)L_5;
		Subject_1_t3463463437 * L_6 = V_0;
		if (!L_6)
		{
			goto IL_005b;
		}
	}
	{
		Subject_1_t3463463437 * L_7 = V_0;
		Il2CppObject* L_8 = ___observer0;
		NullCheck((Subject_1_t3463463437 *)L_7);
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (Subject_1_t3463463437 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)((Subject_1_t3463463437 *)L_7, (Il2CppObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		V_1 = (Il2CppObject *)L_9;
		bool L_10 = (bool)__this->get_canPublishValueOnSubscribe_1();
		if (!L_10)
		{
			goto IL_0059;
		}
	}
	{
		Il2CppObject* L_11 = ___observer0;
		Rect_t1525428817  L_12 = (Rect_t1525428817 )__this->get_value_3();
		NullCheck((Il2CppObject*)L_11);
		InterfaceActionInvoker1< Rect_t1525428817  >::Invoke(2 /* System.Void UniRx.IObserver`1<UnityEngine.Rect>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_11, (Rect_t1525428817 )L_12);
	}

IL_0059:
	{
		Il2CppObject * L_13 = V_1;
		return L_13;
	}

IL_005b:
	{
		Il2CppObject* L_14 = ___observer0;
		NullCheck((Il2CppObject*)L_14);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UnityEngine.Rect>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_14);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_15 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_15;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Rect>::Dispose()
extern "C"  void ReactiveProperty_1_Dispose_m1200849518_gshared (ReactiveProperty_1_t2133761855 * __this, const MethodInfo* method)
{
	{
		NullCheck((ReactiveProperty_1_t2133761855 *)__this);
		VirtActionInvoker1< bool >::Invoke(12 /* System.Void UniRx.ReactiveProperty`1<UnityEngine.Rect>::Dispose(System.Boolean) */, (ReactiveProperty_1_t2133761855 *)__this, (bool)1);
		GC_SuppressFinalize_m1160635446(NULL /*static, unused*/, (Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Rect>::Dispose(System.Boolean)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1_Dispose_m351690597_MetadataUsageId;
extern "C"  void ReactiveProperty_1_Dispose_m351690597_gshared (ReactiveProperty_1_t2133761855 * __this, bool ___disposing0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1_Dispose_m351690597_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Subject_1_t3463463437 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (bool)__this->get_isDisposed_2();
		if (L_0)
		{
			goto IL_0052;
		}
	}
	{
		__this->set_isDisposed_2((bool)1);
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_sourceConnection_5();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		Il2CppObject * L_3 = V_0;
		NullCheck((Il2CppObject *)L_3);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
		__this->set_sourceConnection_5((Il2CppObject *)NULL);
	}

IL_002c:
	{
		Subject_1_t3463463437 * L_4 = (Subject_1_t3463463437 *)__this->get_publisher_4();
		V_1 = (Subject_1_t3463463437 *)L_4;
		Subject_1_t3463463437 * L_5 = V_1;
		if (!L_5)
		{
			goto IL_0052;
		}
	}

IL_0039:
	try
	{ // begin try (depth: 1)
		Subject_1_t3463463437 * L_6 = V_1;
		NullCheck((Subject_1_t3463463437 *)L_6);
		((  void (*) (Subject_1_t3463463437 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)((Subject_1_t3463463437 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		IL2CPP_LEAVE(0x52, FINALLY_0044);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0044;
	}

FINALLY_0044:
	{ // begin finally (depth: 1)
		Subject_1_t3463463437 * L_7 = V_1;
		NullCheck((Subject_1_t3463463437 *)L_7);
		((  void (*) (Subject_1_t3463463437 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((Subject_1_t3463463437 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		__this->set_publisher_4((Subject_1_t3463463437 *)NULL);
		IL2CPP_END_FINALLY(68)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(68)
	{
		IL2CPP_JUMP_TBL(0x52, IL_0052)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0052:
	{
		return;
	}
}
// System.String UniRx.ReactiveProperty`1<UnityEngine.Rect>::ToString()
extern Il2CppCodeGenString* _stringLiteral3392903;
extern const uint32_t ReactiveProperty_1_ToString_m3111913986_MetadataUsageId;
extern "C"  String_t* ReactiveProperty_1_ToString_m3111913986_gshared (ReactiveProperty_1_t2133761855 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1_ToString_m3111913986_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B3_0 = NULL;
	{
		Rect_t1525428817  L_0 = (Rect_t1525428817 )__this->get_value_3();
		goto IL_001a;
	}
	{
		G_B3_0 = _stringLiteral3392903;
		goto IL_002b;
	}

IL_001a:
	{
		Rect_t1525428817 * L_1 = (Rect_t1525428817 *)__this->get_address_of_value_3();
		String_t* L_2 = Rect_ToString_m2093687658((Rect_t1525428817 *)L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_002b:
	{
		return G_B3_0;
	}
}
// System.Boolean UniRx.ReactiveProperty`1<UnityEngine.Rect>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m4081809586_gshared (ReactiveProperty_1_t2133761855 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector2>::.ctor()
extern Il2CppClass* Vector2_t3525329788_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m3508624908_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m3508624908_gshared (ReactiveProperty_1_t4133662826 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m3508624908_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t3525329788  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Vector2_t3525329788_il2cpp_TypeInfo_var, (&V_0));
		Vector2_t3525329788  L_0 = V_0;
		NullCheck((ReactiveProperty_1_t4133662826 *)__this);
		((  void (*) (ReactiveProperty_1_t4133662826 *, Vector2_t3525329788 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ReactiveProperty_1_t4133662826 *)__this, (Vector2_t3525329788 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector2>::.ctor(T)
extern Il2CppClass* Vector2_t3525329788_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m1393191122_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m1393191122_gshared (ReactiveProperty_1_t4133662826 * __this, Vector2_t3525329788  ___initialValue0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m1393191122_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t3525329788  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Vector2_t3525329788_il2cpp_TypeInfo_var, (&V_0));
		Vector2_t3525329788  L_0 = V_0;
		__this->set_value_3(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Vector2_t3525329788  L_1 = ___initialValue0;
		NullCheck((ReactiveProperty_1_t4133662826 *)__this);
		VirtActionInvoker1< Vector2_t3525329788  >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector2>::SetValue(T) */, (ReactiveProperty_1_t4133662826 *)__this, (Vector2_t3525329788 )L_1);
		__this->set_canPublishValueOnSubscribe_1((bool)1);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector2>::.ctor(UniRx.IObservable`1<T>)
extern Il2CppClass* Vector2_t3525329788_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m1935995627_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m1935995627_gshared (ReactiveProperty_1_t4133662826 * __this, Il2CppObject* ___source0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m1935995627_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t3525329788  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Vector2_t3525329788_il2cpp_TypeInfo_var, (&V_0));
		Vector2_t3525329788  L_0 = V_0;
		__this->set_value_3(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		__this->set_canPublishValueOnSubscribe_1((bool)0);
		Subject_1_t1168397112 * L_1 = (Subject_1_t1168397112 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Subject_1_t1168397112 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_publisher_4(L_1);
		Il2CppObject* L_2 = ___source0;
		ReactivePropertyObserver_t3761312357 * L_3 = (ReactivePropertyObserver_t3761312357 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (ReactivePropertyObserver_t3761312357 *, ReactiveProperty_1_t4133662826 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_3, (ReactiveProperty_1_t4133662826 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_4 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<UnityEngine.Vector2>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_2, (Il2CppObject*)L_3);
		__this->set_sourceConnection_5(L_4);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector2>::.ctor(UniRx.IObservable`1<T>,T)
extern Il2CppClass* Vector2_t3525329788_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m770963907_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m770963907_gshared (ReactiveProperty_1_t4133662826 * __this, Il2CppObject* ___source0, Vector2_t3525329788  ___initialValue1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m770963907_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t3525329788  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Vector2_t3525329788_il2cpp_TypeInfo_var, (&V_0));
		Vector2_t3525329788  L_0 = V_0;
		__this->set_value_3(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		__this->set_canPublishValueOnSubscribe_1((bool)0);
		Vector2_t3525329788  L_1 = ___initialValue1;
		NullCheck((ReactiveProperty_1_t4133662826 *)__this);
		VirtActionInvoker1< Vector2_t3525329788  >::Invoke(7 /* System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector2>::set_Value(T) */, (ReactiveProperty_1_t4133662826 *)__this, (Vector2_t3525329788 )L_1);
		Subject_1_t1168397112 * L_2 = (Subject_1_t1168397112 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Subject_1_t1168397112 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_publisher_4(L_2);
		Il2CppObject* L_3 = ___source0;
		ReactivePropertyObserver_t3761312357 * L_4 = (ReactivePropertyObserver_t3761312357 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (ReactivePropertyObserver_t3761312357 *, ReactiveProperty_1_t4133662826 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_4, (ReactiveProperty_1_t4133662826 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((Il2CppObject*)L_3);
		Il2CppObject * L_5 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<UnityEngine.Vector2>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_3, (Il2CppObject*)L_4);
		__this->set_sourceConnection_5(L_5);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector2>::.cctor()
extern Il2CppClass* UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__cctor_m911093537_MetadataUsageId;
extern "C"  void ReactiveProperty_1__cctor_m911093537_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__cctor_m911093537_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		((ReactiveProperty_1_t4133662826_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->static_fields)->set_defaultEqualityComparer_0(L_0);
		return;
	}
}
// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1<UnityEngine.Vector2>::get_EqualityComparer()
extern "C"  Il2CppObject* ReactiveProperty_1_get_EqualityComparer_m333484169_gshared (ReactiveProperty_1_t4133662826 * __this, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		Il2CppObject* L_0 = ((ReactiveProperty_1_t4133662826_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->static_fields)->get_defaultEqualityComparer_0();
		return L_0;
	}
}
// T UniRx.ReactiveProperty`1<UnityEngine.Vector2>::get_Value()
extern "C"  Vector2_t3525329788  ReactiveProperty_1_get_Value_m2822045105_gshared (ReactiveProperty_1_t4133662826 * __this, const MethodInfo* method)
{
	{
		Vector2_t3525329788  L_0 = (Vector2_t3525329788 )__this->get_value_3();
		return L_0;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector2>::set_Value(T)
extern "C"  void ReactiveProperty_1_set_Value_m327013408_gshared (ReactiveProperty_1_t4133662826 * __this, Vector2_t3525329788  ___value0, const MethodInfo* method)
{
	Subject_1_t1168397112 * V_0 = NULL;
	Subject_1_t1168397112 * V_1 = NULL;
	{
		bool L_0 = (bool)__this->get_canPublishValueOnSubscribe_1();
		if (L_0)
		{
			goto IL_003f;
		}
	}
	{
		__this->set_canPublishValueOnSubscribe_1((bool)1);
		Vector2_t3525329788  L_1 = ___value0;
		NullCheck((ReactiveProperty_1_t4133662826 *)__this);
		VirtActionInvoker1< Vector2_t3525329788  >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector2>::SetValue(T) */, (ReactiveProperty_1_t4133662826 *)__this, (Vector2_t3525329788 )L_1);
		bool L_2 = (bool)__this->get_isDisposed_2();
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		return;
	}

IL_0025:
	{
		Subject_1_t1168397112 * L_3 = (Subject_1_t1168397112 *)__this->get_publisher_4();
		V_0 = (Subject_1_t1168397112 *)L_3;
		Subject_1_t1168397112 * L_4 = V_0;
		if (!L_4)
		{
			goto IL_003e;
		}
	}
	{
		Subject_1_t1168397112 * L_5 = V_0;
		Vector2_t3525329788  L_6 = (Vector2_t3525329788 )__this->get_value_3();
		NullCheck((Subject_1_t1168397112 *)L_5);
		((  void (*) (Subject_1_t1168397112 *, Vector2_t3525329788 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Subject_1_t1168397112 *)L_5, (Vector2_t3525329788 )L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
	}

IL_003e:
	{
		return;
	}

IL_003f:
	{
		NullCheck((ReactiveProperty_1_t4133662826 *)__this);
		Il2CppObject* L_7 = VirtFuncInvoker0< Il2CppObject* >::Invoke(10 /* System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1<UnityEngine.Vector2>::get_EqualityComparer() */, (ReactiveProperty_1_t4133662826 *)__this);
		Vector2_t3525329788  L_8 = (Vector2_t3525329788 )__this->get_value_3();
		Vector2_t3525329788  L_9 = ___value0;
		NullCheck((Il2CppObject*)L_7);
		bool L_10 = InterfaceFuncInvoker2< bool, Vector2_t3525329788 , Vector2_t3525329788  >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<UnityEngine.Vector2>::Equals(!0,!0) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12), (Il2CppObject*)L_7, (Vector2_t3525329788 )L_8, (Vector2_t3525329788 )L_9);
		if (L_10)
		{
			goto IL_0082;
		}
	}
	{
		Vector2_t3525329788  L_11 = ___value0;
		NullCheck((ReactiveProperty_1_t4133662826 *)__this);
		VirtActionInvoker1< Vector2_t3525329788  >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector2>::SetValue(T) */, (ReactiveProperty_1_t4133662826 *)__this, (Vector2_t3525329788 )L_11);
		bool L_12 = (bool)__this->get_isDisposed_2();
		if (!L_12)
		{
			goto IL_0069;
		}
	}
	{
		return;
	}

IL_0069:
	{
		Subject_1_t1168397112 * L_13 = (Subject_1_t1168397112 *)__this->get_publisher_4();
		V_1 = (Subject_1_t1168397112 *)L_13;
		Subject_1_t1168397112 * L_14 = V_1;
		if (!L_14)
		{
			goto IL_0082;
		}
	}
	{
		Subject_1_t1168397112 * L_15 = V_1;
		Vector2_t3525329788  L_16 = (Vector2_t3525329788 )__this->get_value_3();
		NullCheck((Subject_1_t1168397112 *)L_15);
		((  void (*) (Subject_1_t1168397112 *, Vector2_t3525329788 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Subject_1_t1168397112 *)L_15, (Vector2_t3525329788 )L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
	}

IL_0082:
	{
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector2>::SetValue(T)
extern "C"  void ReactiveProperty_1_SetValue_m3282824599_gshared (ReactiveProperty_1_t4133662826 * __this, Vector2_t3525329788  ___value0, const MethodInfo* method)
{
	{
		Vector2_t3525329788  L_0 = ___value0;
		__this->set_value_3(L_0);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector2>::SetValueAndForceNotify(T)
extern "C"  void ReactiveProperty_1_SetValueAndForceNotify_m2516180762_gshared (ReactiveProperty_1_t4133662826 * __this, Vector2_t3525329788  ___value0, const MethodInfo* method)
{
	Subject_1_t1168397112 * V_0 = NULL;
	{
		Vector2_t3525329788  L_0 = ___value0;
		NullCheck((ReactiveProperty_1_t4133662826 *)__this);
		VirtActionInvoker1< Vector2_t3525329788  >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector2>::SetValue(T) */, (ReactiveProperty_1_t4133662826 *)__this, (Vector2_t3525329788 )L_0);
		bool L_1 = (bool)__this->get_isDisposed_2();
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		return;
	}

IL_0013:
	{
		Subject_1_t1168397112 * L_2 = (Subject_1_t1168397112 *)__this->get_publisher_4();
		V_0 = (Subject_1_t1168397112 *)L_2;
		Subject_1_t1168397112 * L_3 = V_0;
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		Subject_1_t1168397112 * L_4 = V_0;
		Vector2_t3525329788  L_5 = (Vector2_t3525329788 )__this->get_value_3();
		NullCheck((Subject_1_t1168397112 *)L_4);
		((  void (*) (Subject_1_t1168397112 *, Vector2_t3525329788 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Subject_1_t1168397112 *)L_4, (Vector2_t3525329788 )L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
	}

IL_002c:
	{
		return;
	}
}
// System.IDisposable UniRx.ReactiveProperty`1<UnityEngine.Vector2>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1_Subscribe_m1213422241_MetadataUsageId;
extern "C"  Il2CppObject * ReactiveProperty_1_Subscribe_m1213422241_gshared (ReactiveProperty_1_t4133662826 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1_Subscribe_m1213422241_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t1168397112 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		bool L_0 = (bool)__this->get_isDisposed_2();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Il2CppObject* L_1 = ___observer0;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UnityEngine.Vector2>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_2 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_2;
	}

IL_0017:
	{
		Subject_1_t1168397112 * L_3 = (Subject_1_t1168397112 *)__this->get_publisher_4();
		if (L_3)
		{
			goto IL_002d;
		}
	}
	{
		Subject_1_t1168397112 * L_4 = (Subject_1_t1168397112 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Subject_1_t1168397112 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_publisher_4(L_4);
	}

IL_002d:
	{
		Subject_1_t1168397112 * L_5 = (Subject_1_t1168397112 *)__this->get_publisher_4();
		V_0 = (Subject_1_t1168397112 *)L_5;
		Subject_1_t1168397112 * L_6 = V_0;
		if (!L_6)
		{
			goto IL_005b;
		}
	}
	{
		Subject_1_t1168397112 * L_7 = V_0;
		Il2CppObject* L_8 = ___observer0;
		NullCheck((Subject_1_t1168397112 *)L_7);
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (Subject_1_t1168397112 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)((Subject_1_t1168397112 *)L_7, (Il2CppObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		V_1 = (Il2CppObject *)L_9;
		bool L_10 = (bool)__this->get_canPublishValueOnSubscribe_1();
		if (!L_10)
		{
			goto IL_0059;
		}
	}
	{
		Il2CppObject* L_11 = ___observer0;
		Vector2_t3525329788  L_12 = (Vector2_t3525329788 )__this->get_value_3();
		NullCheck((Il2CppObject*)L_11);
		InterfaceActionInvoker1< Vector2_t3525329788  >::Invoke(2 /* System.Void UniRx.IObserver`1<UnityEngine.Vector2>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_11, (Vector2_t3525329788 )L_12);
	}

IL_0059:
	{
		Il2CppObject * L_13 = V_1;
		return L_13;
	}

IL_005b:
	{
		Il2CppObject* L_14 = ___observer0;
		NullCheck((Il2CppObject*)L_14);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UnityEngine.Vector2>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_14);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_15 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_15;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector2>::Dispose()
extern "C"  void ReactiveProperty_1_Dispose_m137492297_gshared (ReactiveProperty_1_t4133662826 * __this, const MethodInfo* method)
{
	{
		NullCheck((ReactiveProperty_1_t4133662826 *)__this);
		VirtActionInvoker1< bool >::Invoke(12 /* System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector2>::Dispose(System.Boolean) */, (ReactiveProperty_1_t4133662826 *)__this, (bool)1);
		GC_SuppressFinalize_m1160635446(NULL /*static, unused*/, (Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector2>::Dispose(System.Boolean)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1_Dispose_m3018701056_MetadataUsageId;
extern "C"  void ReactiveProperty_1_Dispose_m3018701056_gshared (ReactiveProperty_1_t4133662826 * __this, bool ___disposing0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1_Dispose_m3018701056_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Subject_1_t1168397112 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (bool)__this->get_isDisposed_2();
		if (L_0)
		{
			goto IL_0052;
		}
	}
	{
		__this->set_isDisposed_2((bool)1);
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_sourceConnection_5();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		Il2CppObject * L_3 = V_0;
		NullCheck((Il2CppObject *)L_3);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
		__this->set_sourceConnection_5((Il2CppObject *)NULL);
	}

IL_002c:
	{
		Subject_1_t1168397112 * L_4 = (Subject_1_t1168397112 *)__this->get_publisher_4();
		V_1 = (Subject_1_t1168397112 *)L_4;
		Subject_1_t1168397112 * L_5 = V_1;
		if (!L_5)
		{
			goto IL_0052;
		}
	}

IL_0039:
	try
	{ // begin try (depth: 1)
		Subject_1_t1168397112 * L_6 = V_1;
		NullCheck((Subject_1_t1168397112 *)L_6);
		((  void (*) (Subject_1_t1168397112 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)((Subject_1_t1168397112 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		IL2CPP_LEAVE(0x52, FINALLY_0044);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0044;
	}

FINALLY_0044:
	{ // begin finally (depth: 1)
		Subject_1_t1168397112 * L_7 = V_1;
		NullCheck((Subject_1_t1168397112 *)L_7);
		((  void (*) (Subject_1_t1168397112 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((Subject_1_t1168397112 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		__this->set_publisher_4((Subject_1_t1168397112 *)NULL);
		IL2CPP_END_FINALLY(68)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(68)
	{
		IL2CPP_JUMP_TBL(0x52, IL_0052)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0052:
	{
		return;
	}
}
// System.String UniRx.ReactiveProperty`1<UnityEngine.Vector2>::ToString()
extern Il2CppCodeGenString* _stringLiteral3392903;
extern const uint32_t ReactiveProperty_1_ToString_m3980354529_MetadataUsageId;
extern "C"  String_t* ReactiveProperty_1_ToString_m3980354529_gshared (ReactiveProperty_1_t4133662826 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1_ToString_m3980354529_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B3_0 = NULL;
	{
		Vector2_t3525329788  L_0 = (Vector2_t3525329788 )__this->get_value_3();
		goto IL_001a;
	}
	{
		G_B3_0 = _stringLiteral3392903;
		goto IL_002b;
	}

IL_001a:
	{
		Vector2_t3525329788 * L_1 = (Vector2_t3525329788 *)__this->get_address_of_value_3();
		String_t* L_2 = Vector2_ToString_m3859776067((Vector2_t3525329788 *)L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_002b:
	{
		return G_B3_0;
	}
}
// System.Boolean UniRx.ReactiveProperty`1<UnityEngine.Vector2>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m955929151_gshared (ReactiveProperty_1_t4133662826 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector3>::.ctor()
extern Il2CppClass* Vector3_t3525329789_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m1711673549_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m1711673549_gshared (ReactiveProperty_1_t4133662827 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m1711673549_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t3525329789  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Vector3_t3525329789_il2cpp_TypeInfo_var, (&V_0));
		Vector3_t3525329789  L_0 = V_0;
		NullCheck((ReactiveProperty_1_t4133662827 *)__this);
		((  void (*) (ReactiveProperty_1_t4133662827 *, Vector3_t3525329789 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ReactiveProperty_1_t4133662827 *)__this, (Vector3_t3525329789 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector3>::.ctor(T)
extern Il2CppClass* Vector3_t3525329789_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m1522273841_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m1522273841_gshared (ReactiveProperty_1_t4133662827 * __this, Vector3_t3525329789  ___initialValue0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m1522273841_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t3525329789  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Vector3_t3525329789_il2cpp_TypeInfo_var, (&V_0));
		Vector3_t3525329789  L_0 = V_0;
		__this->set_value_3(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Vector3_t3525329789  L_1 = ___initialValue0;
		NullCheck((ReactiveProperty_1_t4133662827 *)__this);
		VirtActionInvoker1< Vector3_t3525329789  >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector3>::SetValue(T) */, (ReactiveProperty_1_t4133662827 *)__this, (Vector3_t3525329789 )L_1);
		__this->set_canPublishValueOnSubscribe_1((bool)1);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector3>::.ctor(UniRx.IObservable`1<T>)
extern Il2CppClass* Vector3_t3525329789_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m4047285996_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m4047285996_gshared (ReactiveProperty_1_t4133662827 * __this, Il2CppObject* ___source0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m4047285996_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t3525329789  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Vector3_t3525329789_il2cpp_TypeInfo_var, (&V_0));
		Vector3_t3525329789  L_0 = V_0;
		__this->set_value_3(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		__this->set_canPublishValueOnSubscribe_1((bool)0);
		Subject_1_t1168397113 * L_1 = (Subject_1_t1168397113 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Subject_1_t1168397113 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_publisher_4(L_1);
		Il2CppObject* L_2 = ___source0;
		ReactivePropertyObserver_t3761312358 * L_3 = (ReactivePropertyObserver_t3761312358 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (ReactivePropertyObserver_t3761312358 *, ReactiveProperty_1_t4133662827 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_3, (ReactiveProperty_1_t4133662827 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_4 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<UnityEngine.Vector3>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_2, (Il2CppObject*)L_3);
		__this->set_sourceConnection_5(L_4);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector3>::.ctor(UniRx.IObservable`1<T>,T)
extern Il2CppClass* Vector3_t3525329789_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m2496444804_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m2496444804_gshared (ReactiveProperty_1_t4133662827 * __this, Il2CppObject* ___source0, Vector3_t3525329789  ___initialValue1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m2496444804_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t3525329789  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Vector3_t3525329789_il2cpp_TypeInfo_var, (&V_0));
		Vector3_t3525329789  L_0 = V_0;
		__this->set_value_3(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		__this->set_canPublishValueOnSubscribe_1((bool)0);
		Vector3_t3525329789  L_1 = ___initialValue1;
		NullCheck((ReactiveProperty_1_t4133662827 *)__this);
		VirtActionInvoker1< Vector3_t3525329789  >::Invoke(7 /* System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector3>::set_Value(T) */, (ReactiveProperty_1_t4133662827 *)__this, (Vector3_t3525329789 )L_1);
		Subject_1_t1168397113 * L_2 = (Subject_1_t1168397113 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Subject_1_t1168397113 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_publisher_4(L_2);
		Il2CppObject* L_3 = ___source0;
		ReactivePropertyObserver_t3761312358 * L_4 = (ReactivePropertyObserver_t3761312358 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (ReactivePropertyObserver_t3761312358 *, ReactiveProperty_1_t4133662827 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_4, (ReactiveProperty_1_t4133662827 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((Il2CppObject*)L_3);
		Il2CppObject * L_5 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<UnityEngine.Vector3>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_3, (Il2CppObject*)L_4);
		__this->set_sourceConnection_5(L_5);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector3>::.cctor()
extern Il2CppClass* UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__cctor_m1040176256_MetadataUsageId;
extern "C"  void ReactiveProperty_1__cctor_m1040176256_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__cctor_m1040176256_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		((ReactiveProperty_1_t4133662827_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->static_fields)->set_defaultEqualityComparer_0(L_0);
		return;
	}
}
// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1<UnityEngine.Vector3>::get_EqualityComparer()
extern "C"  Il2CppObject* ReactiveProperty_1_get_EqualityComparer_m364503976_gshared (ReactiveProperty_1_t4133662827 * __this, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		Il2CppObject* L_0 = ((ReactiveProperty_1_t4133662827_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->static_fields)->get_defaultEqualityComparer_0();
		return L_0;
	}
}
// T UniRx.ReactiveProperty`1<UnityEngine.Vector3>::get_Value()
extern "C"  Vector3_t3525329789  ReactiveProperty_1_get_Value_m34629618_gshared (ReactiveProperty_1_t4133662827 * __this, const MethodInfo* method)
{
	{
		Vector3_t3525329789  L_0 = (Vector3_t3525329789 )__this->get_value_3();
		return L_0;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector3>::set_Value(T)
extern "C"  void ReactiveProperty_1_set_Value_m4111446527_gshared (ReactiveProperty_1_t4133662827 * __this, Vector3_t3525329789  ___value0, const MethodInfo* method)
{
	Subject_1_t1168397113 * V_0 = NULL;
	Subject_1_t1168397113 * V_1 = NULL;
	{
		bool L_0 = (bool)__this->get_canPublishValueOnSubscribe_1();
		if (L_0)
		{
			goto IL_003f;
		}
	}
	{
		__this->set_canPublishValueOnSubscribe_1((bool)1);
		Vector3_t3525329789  L_1 = ___value0;
		NullCheck((ReactiveProperty_1_t4133662827 *)__this);
		VirtActionInvoker1< Vector3_t3525329789  >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector3>::SetValue(T) */, (ReactiveProperty_1_t4133662827 *)__this, (Vector3_t3525329789 )L_1);
		bool L_2 = (bool)__this->get_isDisposed_2();
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		return;
	}

IL_0025:
	{
		Subject_1_t1168397113 * L_3 = (Subject_1_t1168397113 *)__this->get_publisher_4();
		V_0 = (Subject_1_t1168397113 *)L_3;
		Subject_1_t1168397113 * L_4 = V_0;
		if (!L_4)
		{
			goto IL_003e;
		}
	}
	{
		Subject_1_t1168397113 * L_5 = V_0;
		Vector3_t3525329789  L_6 = (Vector3_t3525329789 )__this->get_value_3();
		NullCheck((Subject_1_t1168397113 *)L_5);
		((  void (*) (Subject_1_t1168397113 *, Vector3_t3525329789 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Subject_1_t1168397113 *)L_5, (Vector3_t3525329789 )L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
	}

IL_003e:
	{
		return;
	}

IL_003f:
	{
		NullCheck((ReactiveProperty_1_t4133662827 *)__this);
		Il2CppObject* L_7 = VirtFuncInvoker0< Il2CppObject* >::Invoke(10 /* System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1<UnityEngine.Vector3>::get_EqualityComparer() */, (ReactiveProperty_1_t4133662827 *)__this);
		Vector3_t3525329789  L_8 = (Vector3_t3525329789 )__this->get_value_3();
		Vector3_t3525329789  L_9 = ___value0;
		NullCheck((Il2CppObject*)L_7);
		bool L_10 = InterfaceFuncInvoker2< bool, Vector3_t3525329789 , Vector3_t3525329789  >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<UnityEngine.Vector3>::Equals(!0,!0) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12), (Il2CppObject*)L_7, (Vector3_t3525329789 )L_8, (Vector3_t3525329789 )L_9);
		if (L_10)
		{
			goto IL_0082;
		}
	}
	{
		Vector3_t3525329789  L_11 = ___value0;
		NullCheck((ReactiveProperty_1_t4133662827 *)__this);
		VirtActionInvoker1< Vector3_t3525329789  >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector3>::SetValue(T) */, (ReactiveProperty_1_t4133662827 *)__this, (Vector3_t3525329789 )L_11);
		bool L_12 = (bool)__this->get_isDisposed_2();
		if (!L_12)
		{
			goto IL_0069;
		}
	}
	{
		return;
	}

IL_0069:
	{
		Subject_1_t1168397113 * L_13 = (Subject_1_t1168397113 *)__this->get_publisher_4();
		V_1 = (Subject_1_t1168397113 *)L_13;
		Subject_1_t1168397113 * L_14 = V_1;
		if (!L_14)
		{
			goto IL_0082;
		}
	}
	{
		Subject_1_t1168397113 * L_15 = V_1;
		Vector3_t3525329789  L_16 = (Vector3_t3525329789 )__this->get_value_3();
		NullCheck((Subject_1_t1168397113 *)L_15);
		((  void (*) (Subject_1_t1168397113 *, Vector3_t3525329789 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Subject_1_t1168397113 *)L_15, (Vector3_t3525329789 )L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
	}

IL_0082:
	{
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector3>::SetValue(T)
extern "C"  void ReactiveProperty_1_SetValue_m495409112_gshared (ReactiveProperty_1_t4133662827 * __this, Vector3_t3525329789  ___value0, const MethodInfo* method)
{
	{
		Vector3_t3525329789  L_0 = ___value0;
		__this->set_value_3(L_0);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector3>::SetValueAndForceNotify(T)
extern "C"  void ReactiveProperty_1_SetValueAndForceNotify_m3209282459_gshared (ReactiveProperty_1_t4133662827 * __this, Vector3_t3525329789  ___value0, const MethodInfo* method)
{
	Subject_1_t1168397113 * V_0 = NULL;
	{
		Vector3_t3525329789  L_0 = ___value0;
		NullCheck((ReactiveProperty_1_t4133662827 *)__this);
		VirtActionInvoker1< Vector3_t3525329789  >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector3>::SetValue(T) */, (ReactiveProperty_1_t4133662827 *)__this, (Vector3_t3525329789 )L_0);
		bool L_1 = (bool)__this->get_isDisposed_2();
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		return;
	}

IL_0013:
	{
		Subject_1_t1168397113 * L_2 = (Subject_1_t1168397113 *)__this->get_publisher_4();
		V_0 = (Subject_1_t1168397113 *)L_2;
		Subject_1_t1168397113 * L_3 = V_0;
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		Subject_1_t1168397113 * L_4 = V_0;
		Vector3_t3525329789  L_5 = (Vector3_t3525329789 )__this->get_value_3();
		NullCheck((Subject_1_t1168397113 *)L_4);
		((  void (*) (Subject_1_t1168397113 *, Vector3_t3525329789 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Subject_1_t1168397113 *)L_4, (Vector3_t3525329789 )L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
	}

IL_002c:
	{
		return;
	}
}
// System.IDisposable UniRx.ReactiveProperty`1<UnityEngine.Vector3>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1_Subscribe_m2938903138_MetadataUsageId;
extern "C"  Il2CppObject * ReactiveProperty_1_Subscribe_m2938903138_gshared (ReactiveProperty_1_t4133662827 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1_Subscribe_m2938903138_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t1168397113 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		bool L_0 = (bool)__this->get_isDisposed_2();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Il2CppObject* L_1 = ___observer0;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UnityEngine.Vector3>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_2 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_2;
	}

IL_0017:
	{
		Subject_1_t1168397113 * L_3 = (Subject_1_t1168397113 *)__this->get_publisher_4();
		if (L_3)
		{
			goto IL_002d;
		}
	}
	{
		Subject_1_t1168397113 * L_4 = (Subject_1_t1168397113 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Subject_1_t1168397113 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_publisher_4(L_4);
	}

IL_002d:
	{
		Subject_1_t1168397113 * L_5 = (Subject_1_t1168397113 *)__this->get_publisher_4();
		V_0 = (Subject_1_t1168397113 *)L_5;
		Subject_1_t1168397113 * L_6 = V_0;
		if (!L_6)
		{
			goto IL_005b;
		}
	}
	{
		Subject_1_t1168397113 * L_7 = V_0;
		Il2CppObject* L_8 = ___observer0;
		NullCheck((Subject_1_t1168397113 *)L_7);
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (Subject_1_t1168397113 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)((Subject_1_t1168397113 *)L_7, (Il2CppObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		V_1 = (Il2CppObject *)L_9;
		bool L_10 = (bool)__this->get_canPublishValueOnSubscribe_1();
		if (!L_10)
		{
			goto IL_0059;
		}
	}
	{
		Il2CppObject* L_11 = ___observer0;
		Vector3_t3525329789  L_12 = (Vector3_t3525329789 )__this->get_value_3();
		NullCheck((Il2CppObject*)L_11);
		InterfaceActionInvoker1< Vector3_t3525329789  >::Invoke(2 /* System.Void UniRx.IObserver`1<UnityEngine.Vector3>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_11, (Vector3_t3525329789 )L_12);
	}

IL_0059:
	{
		Il2CppObject * L_13 = V_1;
		return L_13;
	}

IL_005b:
	{
		Il2CppObject* L_14 = ___observer0;
		NullCheck((Il2CppObject*)L_14);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UnityEngine.Vector3>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_14);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_15 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_15;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector3>::Dispose()
extern "C"  void ReactiveProperty_1_Dispose_m4139056586_gshared (ReactiveProperty_1_t4133662827 * __this, const MethodInfo* method)
{
	{
		NullCheck((ReactiveProperty_1_t4133662827 *)__this);
		VirtActionInvoker1< bool >::Invoke(12 /* System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector3>::Dispose(System.Boolean) */, (ReactiveProperty_1_t4133662827 *)__this, (bool)1);
		GC_SuppressFinalize_m1160635446(NULL /*static, unused*/, (Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector3>::Dispose(System.Boolean)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1_Dispose_m3980315073_MetadataUsageId;
extern "C"  void ReactiveProperty_1_Dispose_m3980315073_gshared (ReactiveProperty_1_t4133662827 * __this, bool ___disposing0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1_Dispose_m3980315073_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Subject_1_t1168397113 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (bool)__this->get_isDisposed_2();
		if (L_0)
		{
			goto IL_0052;
		}
	}
	{
		__this->set_isDisposed_2((bool)1);
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_sourceConnection_5();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		Il2CppObject * L_3 = V_0;
		NullCheck((Il2CppObject *)L_3);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
		__this->set_sourceConnection_5((Il2CppObject *)NULL);
	}

IL_002c:
	{
		Subject_1_t1168397113 * L_4 = (Subject_1_t1168397113 *)__this->get_publisher_4();
		V_1 = (Subject_1_t1168397113 *)L_4;
		Subject_1_t1168397113 * L_5 = V_1;
		if (!L_5)
		{
			goto IL_0052;
		}
	}

IL_0039:
	try
	{ // begin try (depth: 1)
		Subject_1_t1168397113 * L_6 = V_1;
		NullCheck((Subject_1_t1168397113 *)L_6);
		((  void (*) (Subject_1_t1168397113 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)((Subject_1_t1168397113 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		IL2CPP_LEAVE(0x52, FINALLY_0044);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0044;
	}

FINALLY_0044:
	{ // begin finally (depth: 1)
		Subject_1_t1168397113 * L_7 = V_1;
		NullCheck((Subject_1_t1168397113 *)L_7);
		((  void (*) (Subject_1_t1168397113 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((Subject_1_t1168397113 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		__this->set_publisher_4((Subject_1_t1168397113 *)NULL);
		IL2CPP_END_FINALLY(68)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(68)
	{
		IL2CPP_JUMP_TBL(0x52, IL_0052)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0052:
	{
		return;
	}
}
// System.String UniRx.ReactiveProperty`1<UnityEngine.Vector3>::ToString()
extern Il2CppCodeGenString* _stringLiteral3392903;
extern const uint32_t ReactiveProperty_1_ToString_m3474795904_MetadataUsageId;
extern "C"  String_t* ReactiveProperty_1_ToString_m3474795904_gshared (ReactiveProperty_1_t4133662827 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1_ToString_m3474795904_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B3_0 = NULL;
	{
		Vector3_t3525329789  L_0 = (Vector3_t3525329789 )__this->get_value_3();
		goto IL_001a;
	}
	{
		G_B3_0 = _stringLiteral3392903;
		goto IL_002b;
	}

IL_001a:
	{
		Vector3_t3525329789 * L_1 = (Vector3_t3525329789 *)__this->get_address_of_value_3();
		String_t* L_2 = Vector3_ToString_m3566373060((Vector3_t3525329789 *)L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_002b:
	{
		return G_B3_0;
	}
}
// System.Boolean UniRx.ReactiveProperty`1<UnityEngine.Vector3>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m2417509150_gshared (ReactiveProperty_1_t4133662827 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector4>::.ctor()
extern Il2CppClass* Vector4_t3525329790_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m4209689486_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m4209689486_gshared (ReactiveProperty_1_t4133662828 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m4209689486_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector4_t3525329790  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Vector4_t3525329790_il2cpp_TypeInfo_var, (&V_0));
		Vector4_t3525329790  L_0 = V_0;
		NullCheck((ReactiveProperty_1_t4133662828 *)__this);
		((  void (*) (ReactiveProperty_1_t4133662828 *, Vector4_t3525329790 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ReactiveProperty_1_t4133662828 *)__this, (Vector4_t3525329790 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector4>::.ctor(T)
extern Il2CppClass* Vector4_t3525329790_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m1651356560_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m1651356560_gshared (ReactiveProperty_1_t4133662828 * __this, Vector4_t3525329790  ___initialValue0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m1651356560_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector4_t3525329790  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Vector4_t3525329790_il2cpp_TypeInfo_var, (&V_0));
		Vector4_t3525329790  L_0 = V_0;
		__this->set_value_3(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Vector4_t3525329790  L_1 = ___initialValue0;
		NullCheck((ReactiveProperty_1_t4133662828 *)__this);
		VirtActionInvoker1< Vector4_t3525329790  >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector4>::SetValue(T) */, (ReactiveProperty_1_t4133662828 *)__this, (Vector4_t3525329790 )L_1);
		__this->set_canPublishValueOnSubscribe_1((bool)1);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector4>::.ctor(UniRx.IObservable`1<T>)
extern Il2CppClass* Vector4_t3525329790_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m1863609069_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m1863609069_gshared (ReactiveProperty_1_t4133662828 * __this, Il2CppObject* ___source0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m1863609069_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector4_t3525329790  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Vector4_t3525329790_il2cpp_TypeInfo_var, (&V_0));
		Vector4_t3525329790  L_0 = V_0;
		__this->set_value_3(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		__this->set_canPublishValueOnSubscribe_1((bool)0);
		Subject_1_t1168397114 * L_1 = (Subject_1_t1168397114 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Subject_1_t1168397114 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_publisher_4(L_1);
		Il2CppObject* L_2 = ___source0;
		ReactivePropertyObserver_t3761312359 * L_3 = (ReactivePropertyObserver_t3761312359 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (ReactivePropertyObserver_t3761312359 *, ReactiveProperty_1_t4133662828 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_3, (ReactiveProperty_1_t4133662828 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_4 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<UnityEngine.Vector4>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_2, (Il2CppObject*)L_3);
		__this->set_sourceConnection_5(L_4);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector4>::.ctor(UniRx.IObservable`1<T>,T)
extern Il2CppClass* Vector4_t3525329790_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__ctor_m4221925701_MetadataUsageId;
extern "C"  void ReactiveProperty_1__ctor_m4221925701_gshared (ReactiveProperty_1_t4133662828 * __this, Il2CppObject* ___source0, Vector4_t3525329790  ___initialValue1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__ctor_m4221925701_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector4_t3525329790  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Vector4_t3525329790_il2cpp_TypeInfo_var, (&V_0));
		Vector4_t3525329790  L_0 = V_0;
		__this->set_value_3(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		__this->set_canPublishValueOnSubscribe_1((bool)0);
		Vector4_t3525329790  L_1 = ___initialValue1;
		NullCheck((ReactiveProperty_1_t4133662828 *)__this);
		VirtActionInvoker1< Vector4_t3525329790  >::Invoke(7 /* System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector4>::set_Value(T) */, (ReactiveProperty_1_t4133662828 *)__this, (Vector4_t3525329790 )L_1);
		Subject_1_t1168397114 * L_2 = (Subject_1_t1168397114 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Subject_1_t1168397114 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_publisher_4(L_2);
		Il2CppObject* L_3 = ___source0;
		ReactivePropertyObserver_t3761312359 * L_4 = (ReactivePropertyObserver_t3761312359 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (ReactivePropertyObserver_t3761312359 *, ReactiveProperty_1_t4133662828 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_4, (ReactiveProperty_1_t4133662828 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((Il2CppObject*)L_3);
		Il2CppObject * L_5 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<UnityEngine.Vector4>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_3, (Il2CppObject*)L_4);
		__this->set_sourceConnection_5(L_5);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector4>::.cctor()
extern Il2CppClass* UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1__cctor_m1169258975_MetadataUsageId;
extern "C"  void ReactiveProperty_1__cctor_m1169258975_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1__cctor_m1169258975_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityEqualityComparer_t3801418734_il2cpp_TypeInfo_var);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		((ReactiveProperty_1_t4133662828_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->static_fields)->set_defaultEqualityComparer_0(L_0);
		return;
	}
}
// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1<UnityEngine.Vector4>::get_EqualityComparer()
extern "C"  Il2CppObject* ReactiveProperty_1_get_EqualityComparer_m395523783_gshared (ReactiveProperty_1_t4133662828 * __this, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		Il2CppObject* L_0 = ((ReactiveProperty_1_t4133662828_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->static_fields)->get_defaultEqualityComparer_0();
		return L_0;
	}
}
// T UniRx.ReactiveProperty`1<UnityEngine.Vector4>::get_Value()
extern "C"  Vector4_t3525329790  ReactiveProperty_1_get_Value_m1542181427_gshared (ReactiveProperty_1_t4133662828 * __this, const MethodInfo* method)
{
	{
		Vector4_t3525329790  L_0 = (Vector4_t3525329790 )__this->get_value_3();
		return L_0;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector4>::set_Value(T)
extern "C"  void ReactiveProperty_1_set_Value_m3600912350_gshared (ReactiveProperty_1_t4133662828 * __this, Vector4_t3525329790  ___value0, const MethodInfo* method)
{
	Subject_1_t1168397114 * V_0 = NULL;
	Subject_1_t1168397114 * V_1 = NULL;
	{
		bool L_0 = (bool)__this->get_canPublishValueOnSubscribe_1();
		if (L_0)
		{
			goto IL_003f;
		}
	}
	{
		__this->set_canPublishValueOnSubscribe_1((bool)1);
		Vector4_t3525329790  L_1 = ___value0;
		NullCheck((ReactiveProperty_1_t4133662828 *)__this);
		VirtActionInvoker1< Vector4_t3525329790  >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector4>::SetValue(T) */, (ReactiveProperty_1_t4133662828 *)__this, (Vector4_t3525329790 )L_1);
		bool L_2 = (bool)__this->get_isDisposed_2();
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		return;
	}

IL_0025:
	{
		Subject_1_t1168397114 * L_3 = (Subject_1_t1168397114 *)__this->get_publisher_4();
		V_0 = (Subject_1_t1168397114 *)L_3;
		Subject_1_t1168397114 * L_4 = V_0;
		if (!L_4)
		{
			goto IL_003e;
		}
	}
	{
		Subject_1_t1168397114 * L_5 = V_0;
		Vector4_t3525329790  L_6 = (Vector4_t3525329790 )__this->get_value_3();
		NullCheck((Subject_1_t1168397114 *)L_5);
		((  void (*) (Subject_1_t1168397114 *, Vector4_t3525329790 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Subject_1_t1168397114 *)L_5, (Vector4_t3525329790 )L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
	}

IL_003e:
	{
		return;
	}

IL_003f:
	{
		NullCheck((ReactiveProperty_1_t4133662828 *)__this);
		Il2CppObject* L_7 = VirtFuncInvoker0< Il2CppObject* >::Invoke(10 /* System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1<UnityEngine.Vector4>::get_EqualityComparer() */, (ReactiveProperty_1_t4133662828 *)__this);
		Vector4_t3525329790  L_8 = (Vector4_t3525329790 )__this->get_value_3();
		Vector4_t3525329790  L_9 = ___value0;
		NullCheck((Il2CppObject*)L_7);
		bool L_10 = InterfaceFuncInvoker2< bool, Vector4_t3525329790 , Vector4_t3525329790  >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<UnityEngine.Vector4>::Equals(!0,!0) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12), (Il2CppObject*)L_7, (Vector4_t3525329790 )L_8, (Vector4_t3525329790 )L_9);
		if (L_10)
		{
			goto IL_0082;
		}
	}
	{
		Vector4_t3525329790  L_11 = ___value0;
		NullCheck((ReactiveProperty_1_t4133662828 *)__this);
		VirtActionInvoker1< Vector4_t3525329790  >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector4>::SetValue(T) */, (ReactiveProperty_1_t4133662828 *)__this, (Vector4_t3525329790 )L_11);
		bool L_12 = (bool)__this->get_isDisposed_2();
		if (!L_12)
		{
			goto IL_0069;
		}
	}
	{
		return;
	}

IL_0069:
	{
		Subject_1_t1168397114 * L_13 = (Subject_1_t1168397114 *)__this->get_publisher_4();
		V_1 = (Subject_1_t1168397114 *)L_13;
		Subject_1_t1168397114 * L_14 = V_1;
		if (!L_14)
		{
			goto IL_0082;
		}
	}
	{
		Subject_1_t1168397114 * L_15 = V_1;
		Vector4_t3525329790  L_16 = (Vector4_t3525329790 )__this->get_value_3();
		NullCheck((Subject_1_t1168397114 *)L_15);
		((  void (*) (Subject_1_t1168397114 *, Vector4_t3525329790 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Subject_1_t1168397114 *)L_15, (Vector4_t3525329790 )L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
	}

IL_0082:
	{
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector4>::SetValue(T)
extern "C"  void ReactiveProperty_1_SetValue_m2002960921_gshared (ReactiveProperty_1_t4133662828 * __this, Vector4_t3525329790  ___value0, const MethodInfo* method)
{
	{
		Vector4_t3525329790  L_0 = ___value0;
		__this->set_value_3(L_0);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector4>::SetValueAndForceNotify(T)
extern "C"  void ReactiveProperty_1_SetValueAndForceNotify_m3902384156_gshared (ReactiveProperty_1_t4133662828 * __this, Vector4_t3525329790  ___value0, const MethodInfo* method)
{
	Subject_1_t1168397114 * V_0 = NULL;
	{
		Vector4_t3525329790  L_0 = ___value0;
		NullCheck((ReactiveProperty_1_t4133662828 *)__this);
		VirtActionInvoker1< Vector4_t3525329790  >::Invoke(11 /* System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector4>::SetValue(T) */, (ReactiveProperty_1_t4133662828 *)__this, (Vector4_t3525329790 )L_0);
		bool L_1 = (bool)__this->get_isDisposed_2();
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		return;
	}

IL_0013:
	{
		Subject_1_t1168397114 * L_2 = (Subject_1_t1168397114 *)__this->get_publisher_4();
		V_0 = (Subject_1_t1168397114 *)L_2;
		Subject_1_t1168397114 * L_3 = V_0;
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		Subject_1_t1168397114 * L_4 = V_0;
		Vector4_t3525329790  L_5 = (Vector4_t3525329790 )__this->get_value_3();
		NullCheck((Subject_1_t1168397114 *)L_4);
		((  void (*) (Subject_1_t1168397114 *, Vector4_t3525329790 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Subject_1_t1168397114 *)L_4, (Vector4_t3525329790 )L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
	}

IL_002c:
	{
		return;
	}
}
// System.IDisposable UniRx.ReactiveProperty`1<UnityEngine.Vector4>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1_Subscribe_m369416739_MetadataUsageId;
extern "C"  Il2CppObject * ReactiveProperty_1_Subscribe_m369416739_gshared (ReactiveProperty_1_t4133662828 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1_Subscribe_m369416739_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t1168397114 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		bool L_0 = (bool)__this->get_isDisposed_2();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Il2CppObject* L_1 = ___observer0;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UnityEngine.Vector4>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_2 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_2;
	}

IL_0017:
	{
		Subject_1_t1168397114 * L_3 = (Subject_1_t1168397114 *)__this->get_publisher_4();
		if (L_3)
		{
			goto IL_002d;
		}
	}
	{
		Subject_1_t1168397114 * L_4 = (Subject_1_t1168397114 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Subject_1_t1168397114 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_publisher_4(L_4);
	}

IL_002d:
	{
		Subject_1_t1168397114 * L_5 = (Subject_1_t1168397114 *)__this->get_publisher_4();
		V_0 = (Subject_1_t1168397114 *)L_5;
		Subject_1_t1168397114 * L_6 = V_0;
		if (!L_6)
		{
			goto IL_005b;
		}
	}
	{
		Subject_1_t1168397114 * L_7 = V_0;
		Il2CppObject* L_8 = ___observer0;
		NullCheck((Subject_1_t1168397114 *)L_7);
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (Subject_1_t1168397114 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)((Subject_1_t1168397114 *)L_7, (Il2CppObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		V_1 = (Il2CppObject *)L_9;
		bool L_10 = (bool)__this->get_canPublishValueOnSubscribe_1();
		if (!L_10)
		{
			goto IL_0059;
		}
	}
	{
		Il2CppObject* L_11 = ___observer0;
		Vector4_t3525329790  L_12 = (Vector4_t3525329790 )__this->get_value_3();
		NullCheck((Il2CppObject*)L_11);
		InterfaceActionInvoker1< Vector4_t3525329790  >::Invoke(2 /* System.Void UniRx.IObserver`1<UnityEngine.Vector4>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_11, (Vector4_t3525329790 )L_12);
	}

IL_0059:
	{
		Il2CppObject * L_13 = V_1;
		return L_13;
	}

IL_005b:
	{
		Il2CppObject* L_14 = ___observer0;
		NullCheck((Il2CppObject*)L_14);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UnityEngine.Vector4>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (Il2CppObject*)L_14);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_15 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_15;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector4>::Dispose()
extern "C"  void ReactiveProperty_1_Dispose_m3845653579_gshared (ReactiveProperty_1_t4133662828 * __this, const MethodInfo* method)
{
	{
		NullCheck((ReactiveProperty_1_t4133662828 *)__this);
		VirtActionInvoker1< bool >::Invoke(12 /* System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector4>::Dispose(System.Boolean) */, (ReactiveProperty_1_t4133662828 *)__this, (bool)1);
		GC_SuppressFinalize_m1160635446(NULL /*static, unused*/, (Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Vector4>::Dispose(System.Boolean)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t ReactiveProperty_1_Dispose_m646961794_MetadataUsageId;
extern "C"  void ReactiveProperty_1_Dispose_m646961794_gshared (ReactiveProperty_1_t4133662828 * __this, bool ___disposing0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1_Dispose_m646961794_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Subject_1_t1168397114 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (bool)__this->get_isDisposed_2();
		if (L_0)
		{
			goto IL_0052;
		}
	}
	{
		__this->set_isDisposed_2((bool)1);
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_sourceConnection_5();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		Il2CppObject * L_3 = V_0;
		NullCheck((Il2CppObject *)L_3);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
		__this->set_sourceConnection_5((Il2CppObject *)NULL);
	}

IL_002c:
	{
		Subject_1_t1168397114 * L_4 = (Subject_1_t1168397114 *)__this->get_publisher_4();
		V_1 = (Subject_1_t1168397114 *)L_4;
		Subject_1_t1168397114 * L_5 = V_1;
		if (!L_5)
		{
			goto IL_0052;
		}
	}

IL_0039:
	try
	{ // begin try (depth: 1)
		Subject_1_t1168397114 * L_6 = V_1;
		NullCheck((Subject_1_t1168397114 *)L_6);
		((  void (*) (Subject_1_t1168397114 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)((Subject_1_t1168397114 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		IL2CPP_LEAVE(0x52, FINALLY_0044);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0044;
	}

FINALLY_0044:
	{ // begin finally (depth: 1)
		Subject_1_t1168397114 * L_7 = V_1;
		NullCheck((Subject_1_t1168397114 *)L_7);
		((  void (*) (Subject_1_t1168397114 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((Subject_1_t1168397114 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		__this->set_publisher_4((Subject_1_t1168397114 *)NULL);
		IL2CPP_END_FINALLY(68)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(68)
	{
		IL2CPP_JUMP_TBL(0x52, IL_0052)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0052:
	{
		return;
	}
}
// System.String UniRx.ReactiveProperty`1<UnityEngine.Vector4>::ToString()
extern Il2CppCodeGenString* _stringLiteral3392903;
extern const uint32_t ReactiveProperty_1_ToString_m2969237279_MetadataUsageId;
extern "C"  String_t* ReactiveProperty_1_ToString_m2969237279_gshared (ReactiveProperty_1_t4133662828 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReactiveProperty_1_ToString_m2969237279_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B3_0 = NULL;
	{
		Vector4_t3525329790  L_0 = (Vector4_t3525329790 )__this->get_value_3();
		goto IL_001a;
	}
	{
		G_B3_0 = _stringLiteral3392903;
		goto IL_002b;
	}

IL_001a:
	{
		Vector4_t3525329790 * L_1 = (Vector4_t3525329790 *)__this->get_address_of_value_3();
		String_t* L_2 = Vector4_ToString_m3272970053((Vector4_t3525329790 *)L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_002b:
	{
		return G_B3_0;
	}
}
// System.Boolean UniRx.ReactiveProperty`1<UnityEngine.Vector4>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m3879089149_gshared (ReactiveProperty_1_t4133662828 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void UniRx.ReadOnlyReactiveProperty`1/ReadOnlyReactivePropertyObserver<System.Boolean>::.ctor(UniRx.ReadOnlyReactiveProperty`1<T>)
extern "C"  void ReadOnlyReactivePropertyObserver__ctor_m4256084099_gshared (ReadOnlyReactivePropertyObserver_t3498060216 * __this, ReadOnlyReactiveProperty_1_t3000407357 * ___parent0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ReadOnlyReactiveProperty_1_t3000407357 * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		return;
	}
}
// System.Void UniRx.ReadOnlyReactiveProperty`1/ReadOnlyReactivePropertyObserver<System.Boolean>::OnNext(T)
extern "C"  void ReadOnlyReactivePropertyObserver_OnNext_m2074741770_gshared (ReadOnlyReactivePropertyObserver_t3498060216 * __this, bool ___value0, const MethodInfo* method)
{
	{
		ReadOnlyReactiveProperty_1_t3000407357 * L_0 = (ReadOnlyReactiveProperty_1_t3000407357 *)__this->get_parent_0();
		bool L_1 = ___value0;
		NullCheck(L_0);
		L_0->set_value_2(L_1);
		ReadOnlyReactiveProperty_1_t3000407357 * L_2 = (ReadOnlyReactiveProperty_1_t3000407357 *)__this->get_parent_0();
		NullCheck(L_2);
		L_2->set_canPublishValueOnSubscribe_0((bool)1);
		ReadOnlyReactiveProperty_1_t3000407357 * L_3 = (ReadOnlyReactiveProperty_1_t3000407357 *)__this->get_parent_0();
		NullCheck(L_3);
		Subject_1_t2149039961 * L_4 = (Subject_1_t2149039961 *)L_3->get_publisher_3();
		bool L_5 = ___value0;
		NullCheck((Subject_1_t2149039961 *)L_4);
		((  void (*) (Subject_1_t2149039961 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Subject_1_t2149039961 *)L_4, (bool)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.ReadOnlyReactiveProperty`1/ReadOnlyReactivePropertyObserver<System.Boolean>::OnError(System.Exception)
extern "C"  void ReadOnlyReactivePropertyObserver_OnError_m4026061593_gshared (ReadOnlyReactivePropertyObserver_t3498060216 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_1();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0022;
		}
	}
	{
		ReadOnlyReactiveProperty_1_t3000407357 * L_2 = (ReadOnlyReactiveProperty_1_t3000407357 *)__this->get_parent_0();
		NullCheck(L_2);
		Subject_1_t2149039961 * L_3 = (Subject_1_t2149039961 *)L_2->get_publisher_3();
		Exception_t1967233988 * L_4 = ___error0;
		NullCheck((Subject_1_t2149039961 *)L_3);
		((  void (*) (Subject_1_t2149039961 *, Exception_t1967233988 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Subject_1_t2149039961 *)L_3, (Exception_t1967233988 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
	}

IL_0022:
	{
		return;
	}
}
// System.Void UniRx.ReadOnlyReactiveProperty`1/ReadOnlyReactivePropertyObserver<System.Boolean>::OnCompleted()
extern "C"  void ReadOnlyReactivePropertyObserver_OnCompleted_m2506138604_gshared (ReadOnlyReactivePropertyObserver_t3498060216 * __this, const MethodInfo* method)
{
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_1();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0021;
		}
	}
	{
		ReadOnlyReactiveProperty_1_t3000407357 * L_2 = (ReadOnlyReactiveProperty_1_t3000407357 *)__this->get_parent_0();
		NullCheck(L_2);
		Subject_1_t2149039961 * L_3 = (Subject_1_t2149039961 *)L_2->get_publisher_3();
		NullCheck((Subject_1_t2149039961 *)L_3);
		((  void (*) (Subject_1_t2149039961 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Subject_1_t2149039961 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
	}

IL_0021:
	{
		return;
	}
}
// System.Void UniRx.ReadOnlyReactiveProperty`1/ReadOnlyReactivePropertyObserver<System.Object>::.ctor(UniRx.ReadOnlyReactiveProperty`1<T>)
extern "C"  void ReadOnlyReactivePropertyObserver__ctor_m855956758_gshared (ReadOnlyReactivePropertyObserver_t4124161295 * __this, ReadOnlyReactiveProperty_1_t3626508436 * ___parent0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ReadOnlyReactiveProperty_1_t3626508436 * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		return;
	}
}
// System.Void UniRx.ReadOnlyReactiveProperty`1/ReadOnlyReactivePropertyObserver<System.Object>::OnNext(T)
extern "C"  void ReadOnlyReactivePropertyObserver_OnNext_m458355991_gshared (ReadOnlyReactivePropertyObserver_t4124161295 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		ReadOnlyReactiveProperty_1_t3626508436 * L_0 = (ReadOnlyReactiveProperty_1_t3626508436 *)__this->get_parent_0();
		Il2CppObject * L_1 = ___value0;
		NullCheck(L_0);
		L_0->set_value_2(L_1);
		ReadOnlyReactiveProperty_1_t3626508436 * L_2 = (ReadOnlyReactiveProperty_1_t3626508436 *)__this->get_parent_0();
		NullCheck(L_2);
		L_2->set_canPublishValueOnSubscribe_0((bool)1);
		ReadOnlyReactiveProperty_1_t3626508436 * L_3 = (ReadOnlyReactiveProperty_1_t3626508436 *)__this->get_parent_0();
		NullCheck(L_3);
		Subject_1_t2775141040 * L_4 = (Subject_1_t2775141040 *)L_3->get_publisher_3();
		Il2CppObject * L_5 = ___value0;
		NullCheck((Subject_1_t2775141040 *)L_4);
		((  void (*) (Subject_1_t2775141040 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Subject_1_t2775141040 *)L_4, (Il2CppObject *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.ReadOnlyReactiveProperty`1/ReadOnlyReactivePropertyObserver<System.Object>::OnError(System.Exception)
extern "C"  void ReadOnlyReactivePropertyObserver_OnError_m357499430_gshared (ReadOnlyReactivePropertyObserver_t4124161295 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_1();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0022;
		}
	}
	{
		ReadOnlyReactiveProperty_1_t3626508436 * L_2 = (ReadOnlyReactiveProperty_1_t3626508436 *)__this->get_parent_0();
		NullCheck(L_2);
		Subject_1_t2775141040 * L_3 = (Subject_1_t2775141040 *)L_2->get_publisher_3();
		Exception_t1967233988 * L_4 = ___error0;
		NullCheck((Subject_1_t2775141040 *)L_3);
		((  void (*) (Subject_1_t2775141040 *, Exception_t1967233988 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Subject_1_t2775141040 *)L_3, (Exception_t1967233988 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
	}

IL_0022:
	{
		return;
	}
}
// System.Void UniRx.ReadOnlyReactiveProperty`1/ReadOnlyReactivePropertyObserver<System.Object>::OnCompleted()
extern "C"  void ReadOnlyReactivePropertyObserver_OnCompleted_m3718463097_gshared (ReadOnlyReactivePropertyObserver_t4124161295 * __this, const MethodInfo* method)
{
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_1();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0021;
		}
	}
	{
		ReadOnlyReactiveProperty_1_t3626508436 * L_2 = (ReadOnlyReactiveProperty_1_t3626508436 *)__this->get_parent_0();
		NullCheck(L_2);
		Subject_1_t2775141040 * L_3 = (Subject_1_t2775141040 *)L_2->get_publisher_3();
		NullCheck((Subject_1_t2775141040 *)L_3);
		((  void (*) (Subject_1_t2775141040 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Subject_1_t2775141040 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
	}

IL_0021:
	{
		return;
	}
}
// System.Void UniRx.ReadOnlyReactiveProperty`1/ReadOnlyReactivePropertyObserver<UnityEngine.Color>::.ctor(UniRx.ReadOnlyReactiveProperty`1<T>)
extern "C"  void ReadOnlyReactivePropertyObserver__ctor_m2616608040_gshared (ReadOnlyReactivePropertyObserver_t580263339 * __this, ReadOnlyReactiveProperty_1_t82610480 * ___parent0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ReadOnlyReactiveProperty_1_t82610480 * L_0 = ___parent0;
		__this->set_parent_0(L_0);
		return;
	}
}
// System.Void UniRx.ReadOnlyReactiveProperty`1/ReadOnlyReactivePropertyObserver<UnityEngine.Color>::OnNext(T)
extern "C"  void ReadOnlyReactivePropertyObserver_OnNext_m1866437_gshared (ReadOnlyReactivePropertyObserver_t580263339 * __this, Color_t1588175760  ___value0, const MethodInfo* method)
{
	{
		ReadOnlyReactiveProperty_1_t82610480 * L_0 = (ReadOnlyReactiveProperty_1_t82610480 *)__this->get_parent_0();
		Color_t1588175760  L_1 = ___value0;
		NullCheck(L_0);
		L_0->set_value_2(L_1);
		ReadOnlyReactiveProperty_1_t82610480 * L_2 = (ReadOnlyReactiveProperty_1_t82610480 *)__this->get_parent_0();
		NullCheck(L_2);
		L_2->set_canPublishValueOnSubscribe_0((bool)1);
		ReadOnlyReactiveProperty_1_t82610480 * L_3 = (ReadOnlyReactiveProperty_1_t82610480 *)__this->get_parent_0();
		NullCheck(L_3);
		Subject_1_t3526210380 * L_4 = (Subject_1_t3526210380 *)L_3->get_publisher_3();
		Color_t1588175760  L_5 = ___value0;
		NullCheck((Subject_1_t3526210380 *)L_4);
		((  void (*) (Subject_1_t3526210380 *, Color_t1588175760 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Subject_1_t3526210380 *)L_4, (Color_t1588175760 )L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.ReadOnlyReactiveProperty`1/ReadOnlyReactivePropertyObserver<UnityEngine.Color>::OnError(System.Exception)
extern "C"  void ReadOnlyReactivePropertyObserver_OnError_m971271124_gshared (ReadOnlyReactivePropertyObserver_t580263339 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_1();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0022;
		}
	}
	{
		ReadOnlyReactiveProperty_1_t82610480 * L_2 = (ReadOnlyReactiveProperty_1_t82610480 *)__this->get_parent_0();
		NullCheck(L_2);
		Subject_1_t3526210380 * L_3 = (Subject_1_t3526210380 *)L_2->get_publisher_3();
		Exception_t1967233988 * L_4 = ___error0;
		NullCheck((Subject_1_t3526210380 *)L_3);
		((  void (*) (Subject_1_t3526210380 *, Exception_t1967233988 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Subject_1_t3526210380 *)L_3, (Exception_t1967233988 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
	}

IL_0022:
	{
		return;
	}
}
// System.Void UniRx.ReadOnlyReactiveProperty`1/ReadOnlyReactivePropertyObserver<UnityEngine.Color>::OnCompleted()
extern "C"  void ReadOnlyReactivePropertyObserver_OnCompleted_m2838969639_gshared (ReadOnlyReactivePropertyObserver_t580263339 * __this, const MethodInfo* method)
{
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_isStopped_1();
		int32_t L_1 = Interlocked_Increment_m2829412809(NULL /*static, unused*/, (int32_t*)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0021;
		}
	}
	{
		ReadOnlyReactiveProperty_1_t82610480 * L_2 = (ReadOnlyReactiveProperty_1_t82610480 *)__this->get_parent_0();
		NullCheck(L_2);
		Subject_1_t3526210380 * L_3 = (Subject_1_t3526210380 *)L_2->get_publisher_3();
		NullCheck((Subject_1_t3526210380 *)L_3);
		((  void (*) (Subject_1_t3526210380 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Subject_1_t3526210380 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
	}

IL_0021:
	{
		return;
	}
}
// System.Void UniRx.ReadOnlyReactiveProperty`1<System.Boolean>::.ctor(UniRx.IObservable`1<T>)
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyReactiveProperty_1__ctor_m2005416796_MetadataUsageId;
extern "C"  void ReadOnlyReactiveProperty_1__ctor_m2005416796_gshared (ReadOnlyReactiveProperty_1_t3000407357 * __this, Il2CppObject* ___source0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyReactiveProperty_1__ctor_m2005416796_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		Initobj (Boolean_t211005341_il2cpp_TypeInfo_var, (&V_0));
		bool L_0 = V_0;
		__this->set_value_2(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Subject_1_t2149039961 * L_1 = (Subject_1_t2149039961 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (Subject_1_t2149039961 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->set_publisher_3(L_1);
		Il2CppObject* L_2 = ___source0;
		ReadOnlyReactivePropertyObserver_t3498060216 * L_3 = (ReadOnlyReactivePropertyObserver_t3498060216 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (ReadOnlyReactivePropertyObserver_t3498060216 *, ReadOnlyReactiveProperty_1_t3000407357 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_3, (ReadOnlyReactiveProperty_1_t3000407357 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_4 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Boolean>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_2, (Il2CppObject*)L_3);
		__this->set_sourceConnection_4(L_4);
		return;
	}
}
// System.Void UniRx.ReadOnlyReactiveProperty`1<System.Boolean>::.ctor(UniRx.IObservable`1<T>,T)
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyReactiveProperty_1__ctor_m3060197876_MetadataUsageId;
extern "C"  void ReadOnlyReactiveProperty_1__ctor_m3060197876_gshared (ReadOnlyReactiveProperty_1_t3000407357 * __this, Il2CppObject* ___source0, bool ___initialValue1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyReactiveProperty_1__ctor_m3060197876_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		Initobj (Boolean_t211005341_il2cpp_TypeInfo_var, (&V_0));
		bool L_0 = V_0;
		__this->set_value_2(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		bool L_1 = ___initialValue1;
		__this->set_value_2(L_1);
		Subject_1_t2149039961 * L_2 = (Subject_1_t2149039961 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (Subject_1_t2149039961 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->set_publisher_3(L_2);
		Il2CppObject* L_3 = ___source0;
		ReadOnlyReactivePropertyObserver_t3498060216 * L_4 = (ReadOnlyReactivePropertyObserver_t3498060216 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (ReadOnlyReactivePropertyObserver_t3498060216 *, ReadOnlyReactiveProperty_1_t3000407357 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_4, (ReadOnlyReactiveProperty_1_t3000407357 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((Il2CppObject*)L_3);
		Il2CppObject * L_5 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Boolean>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_3, (Il2CppObject*)L_4);
		__this->set_sourceConnection_4(L_5);
		return;
	}
}
// T UniRx.ReadOnlyReactiveProperty`1<System.Boolean>::get_Value()
extern "C"  bool ReadOnlyReactiveProperty_1_get_Value_m3402680452_gshared (ReadOnlyReactiveProperty_1_t3000407357 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_value_2();
		return L_0;
	}
}
// System.IDisposable UniRx.ReadOnlyReactiveProperty`1<System.Boolean>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyReactiveProperty_1_Subscribe_m98383100_MetadataUsageId;
extern "C"  Il2CppObject * ReadOnlyReactiveProperty_1_Subscribe_m98383100_gshared (ReadOnlyReactiveProperty_1_t3000407357 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyReactiveProperty_1_Subscribe_m98383100_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t2149039961 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		bool L_0 = (bool)__this->get_isDisposed_1();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Il2CppObject* L_1 = ___observer0;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Boolean>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_2 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_2;
	}

IL_0017:
	{
		Subject_1_t2149039961 * L_3 = (Subject_1_t2149039961 *)__this->get_publisher_3();
		if (L_3)
		{
			goto IL_002d;
		}
	}
	{
		Subject_1_t2149039961 * L_4 = (Subject_1_t2149039961 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (Subject_1_t2149039961 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->set_publisher_3(L_4);
	}

IL_002d:
	{
		Subject_1_t2149039961 * L_5 = (Subject_1_t2149039961 *)__this->get_publisher_3();
		V_0 = (Subject_1_t2149039961 *)L_5;
		Subject_1_t2149039961 * L_6 = V_0;
		if (!L_6)
		{
			goto IL_005b;
		}
	}
	{
		Subject_1_t2149039961 * L_7 = V_0;
		Il2CppObject* L_8 = ___observer0;
		NullCheck((Subject_1_t2149039961 *)L_7);
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (Subject_1_t2149039961 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Subject_1_t2149039961 *)L_7, (Il2CppObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		V_1 = (Il2CppObject *)L_9;
		bool L_10 = (bool)__this->get_canPublishValueOnSubscribe_0();
		if (!L_10)
		{
			goto IL_0059;
		}
	}
	{
		Il2CppObject* L_11 = ___observer0;
		bool L_12 = (bool)__this->get_value_2();
		NullCheck((Il2CppObject*)L_11);
		InterfaceActionInvoker1< bool >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Boolean>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_11, (bool)L_12);
	}

IL_0059:
	{
		Il2CppObject * L_13 = V_1;
		return L_13;
	}

IL_005b:
	{
		Il2CppObject* L_14 = ___observer0;
		NullCheck((Il2CppObject*)L_14);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Boolean>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_14);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_15 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_15;
	}
}
// System.Void UniRx.ReadOnlyReactiveProperty`1<System.Boolean>::Dispose()
extern "C"  void ReadOnlyReactiveProperty_1_Dispose_m3901694522_gshared (ReadOnlyReactiveProperty_1_t3000407357 * __this, const MethodInfo* method)
{
	{
		NullCheck((ReadOnlyReactiveProperty_1_t3000407357 *)__this);
		VirtActionInvoker1< bool >::Invoke(8 /* System.Void UniRx.ReadOnlyReactiveProperty`1<System.Boolean>::Dispose(System.Boolean) */, (ReadOnlyReactiveProperty_1_t3000407357 *)__this, (bool)1);
		GC_SuppressFinalize_m1160635446(NULL /*static, unused*/, (Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.ReadOnlyReactiveProperty`1<System.Boolean>::Dispose(System.Boolean)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyReactiveProperty_1_Dispose_m3297274417_MetadataUsageId;
extern "C"  void ReadOnlyReactiveProperty_1_Dispose_m3297274417_gshared (ReadOnlyReactiveProperty_1_t3000407357 * __this, bool ___disposing0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyReactiveProperty_1_Dispose_m3297274417_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Subject_1_t2149039961 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (bool)__this->get_isDisposed_1();
		if (L_0)
		{
			goto IL_0052;
		}
	}
	{
		__this->set_isDisposed_1((bool)1);
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_sourceConnection_4();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		Il2CppObject * L_3 = V_0;
		NullCheck((Il2CppObject *)L_3);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
		__this->set_sourceConnection_4((Il2CppObject *)NULL);
	}

IL_002c:
	{
		Subject_1_t2149039961 * L_4 = (Subject_1_t2149039961 *)__this->get_publisher_3();
		V_1 = (Subject_1_t2149039961 *)L_4;
		Subject_1_t2149039961 * L_5 = V_1;
		if (!L_5)
		{
			goto IL_0052;
		}
	}

IL_0039:
	try
	{ // begin try (depth: 1)
		Subject_1_t2149039961 * L_6 = V_1;
		NullCheck((Subject_1_t2149039961 *)L_6);
		((  void (*) (Subject_1_t2149039961 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((Subject_1_t2149039961 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		IL2CPP_LEAVE(0x52, FINALLY_0044);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0044;
	}

FINALLY_0044:
	{ // begin finally (depth: 1)
		Subject_1_t2149039961 * L_7 = V_1;
		NullCheck((Subject_1_t2149039961 *)L_7);
		((  void (*) (Subject_1_t2149039961 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Subject_1_t2149039961 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_publisher_3((Subject_1_t2149039961 *)NULL);
		IL2CPP_END_FINALLY(68)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(68)
	{
		IL2CPP_JUMP_TBL(0x52, IL_0052)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0052:
	{
		return;
	}
}
// System.String UniRx.ReadOnlyReactiveProperty`1<System.Boolean>::ToString()
extern Il2CppCodeGenString* _stringLiteral3392903;
extern const uint32_t ReadOnlyReactiveProperty_1_ToString_m577531254_MetadataUsageId;
extern "C"  String_t* ReadOnlyReactiveProperty_1_ToString_m577531254_gshared (ReadOnlyReactiveProperty_1_t3000407357 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyReactiveProperty_1_ToString_m577531254_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B3_0 = NULL;
	{
		bool L_0 = (bool)__this->get_value_2();
		goto IL_001a;
	}
	{
		G_B3_0 = _stringLiteral3392903;
		goto IL_002b;
	}

IL_001a:
	{
		bool* L_1 = (bool*)__this->get_address_of_value_2();
		String_t* L_2 = Boolean_ToString_m2512358154((bool*)L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_002b:
	{
		return G_B3_0;
	}
}
// System.Boolean UniRx.ReadOnlyReactiveProperty`1<System.Boolean>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool ReadOnlyReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m1727181926_gshared (ReadOnlyReactiveProperty_1_t3000407357 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void UniRx.ReadOnlyReactiveProperty`1<System.Object>::.ctor(UniRx.IObservable`1<T>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyReactiveProperty_1__ctor_m3555015251_MetadataUsageId;
extern "C"  void ReadOnlyReactiveProperty_1__ctor_m3555015251_gshared (ReadOnlyReactiveProperty_1_t3626508436 * __this, Il2CppObject* ___source0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyReactiveProperty_1__ctor_m3555015251_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_0 = V_0;
		__this->set_value_2(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Subject_1_t2775141040 * L_1 = (Subject_1_t2775141040 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (Subject_1_t2775141040 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->set_publisher_3(L_1);
		Il2CppObject* L_2 = ___source0;
		ReadOnlyReactivePropertyObserver_t4124161295 * L_3 = (ReadOnlyReactivePropertyObserver_t4124161295 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (ReadOnlyReactivePropertyObserver_t4124161295 *, ReadOnlyReactiveProperty_1_t3626508436 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_3, (ReadOnlyReactiveProperty_1_t3626508436 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_4 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_2, (Il2CppObject*)L_3);
		__this->set_sourceConnection_4(L_4);
		return;
	}
}
// System.Void UniRx.ReadOnlyReactiveProperty`1<System.Object>::.ctor(UniRx.IObservable`1<T>,T)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyReactiveProperty_1__ctor_m1870661419_MetadataUsageId;
extern "C"  void ReadOnlyReactiveProperty_1__ctor_m1870661419_gshared (ReadOnlyReactiveProperty_1_t3626508436 * __this, Il2CppObject* ___source0, Il2CppObject * ___initialValue1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyReactiveProperty_1__ctor_m1870661419_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_0 = V_0;
		__this->set_value_2(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___initialValue1;
		__this->set_value_2(L_1);
		Subject_1_t2775141040 * L_2 = (Subject_1_t2775141040 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (Subject_1_t2775141040 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->set_publisher_3(L_2);
		Il2CppObject* L_3 = ___source0;
		ReadOnlyReactivePropertyObserver_t4124161295 * L_4 = (ReadOnlyReactivePropertyObserver_t4124161295 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (ReadOnlyReactivePropertyObserver_t4124161295 *, ReadOnlyReactiveProperty_1_t3626508436 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_4, (ReadOnlyReactiveProperty_1_t3626508436 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((Il2CppObject*)L_3);
		Il2CppObject * L_5 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<System.Object>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_3, (Il2CppObject*)L_4);
		__this->set_sourceConnection_4(L_5);
		return;
	}
}
// T UniRx.ReadOnlyReactiveProperty`1<System.Object>::get_Value()
extern "C"  Il2CppObject * ReadOnlyReactiveProperty_1_get_Value_m4163066713_gshared (ReadOnlyReactiveProperty_1_t3626508436 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_2();
		return L_0;
	}
}
// System.IDisposable UniRx.ReadOnlyReactiveProperty`1<System.Object>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyReactiveProperty_1_Subscribe_m2445628745_MetadataUsageId;
extern "C"  Il2CppObject * ReadOnlyReactiveProperty_1_Subscribe_m2445628745_gshared (ReadOnlyReactiveProperty_1_t3626508436 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyReactiveProperty_1_Subscribe_m2445628745_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t2775141040 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		bool L_0 = (bool)__this->get_isDisposed_1();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Il2CppObject* L_1 = ___observer0;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_2 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_2;
	}

IL_0017:
	{
		Subject_1_t2775141040 * L_3 = (Subject_1_t2775141040 *)__this->get_publisher_3();
		if (L_3)
		{
			goto IL_002d;
		}
	}
	{
		Subject_1_t2775141040 * L_4 = (Subject_1_t2775141040 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (Subject_1_t2775141040 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->set_publisher_3(L_4);
	}

IL_002d:
	{
		Subject_1_t2775141040 * L_5 = (Subject_1_t2775141040 *)__this->get_publisher_3();
		V_0 = (Subject_1_t2775141040 *)L_5;
		Subject_1_t2775141040 * L_6 = V_0;
		if (!L_6)
		{
			goto IL_005b;
		}
	}
	{
		Subject_1_t2775141040 * L_7 = V_0;
		Il2CppObject* L_8 = ___observer0;
		NullCheck((Subject_1_t2775141040 *)L_7);
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (Subject_1_t2775141040 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Subject_1_t2775141040 *)L_7, (Il2CppObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		V_1 = (Il2CppObject *)L_9;
		bool L_10 = (bool)__this->get_canPublishValueOnSubscribe_0();
		if (!L_10)
		{
			goto IL_0059;
		}
	}
	{
		Il2CppObject* L_11 = ___observer0;
		Il2CppObject * L_12 = (Il2CppObject *)__this->get_value_2();
		NullCheck((Il2CppObject*)L_11);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_11, (Il2CppObject *)L_12);
	}

IL_0059:
	{
		Il2CppObject * L_13 = V_1;
		return L_13;
	}

IL_005b:
	{
		Il2CppObject* L_14 = ___observer0;
		NullCheck((Il2CppObject*)L_14);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_14);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_15 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_15;
	}
}
// System.Void UniRx.ReadOnlyReactiveProperty`1<System.Object>::Dispose()
extern "C"  void ReadOnlyReactiveProperty_1_Dispose_m3221357233_gshared (ReadOnlyReactiveProperty_1_t3626508436 * __this, const MethodInfo* method)
{
	{
		NullCheck((ReadOnlyReactiveProperty_1_t3626508436 *)__this);
		VirtActionInvoker1< bool >::Invoke(8 /* System.Void UniRx.ReadOnlyReactiveProperty`1<System.Object>::Dispose(System.Boolean) */, (ReadOnlyReactiveProperty_1_t3626508436 *)__this, (bool)1);
		GC_SuppressFinalize_m1160635446(NULL /*static, unused*/, (Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.ReadOnlyReactiveProperty`1<System.Object>::Dispose(System.Boolean)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyReactiveProperty_1_Dispose_m1852943976_MetadataUsageId;
extern "C"  void ReadOnlyReactiveProperty_1_Dispose_m1852943976_gshared (ReadOnlyReactiveProperty_1_t3626508436 * __this, bool ___disposing0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyReactiveProperty_1_Dispose_m1852943976_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Subject_1_t2775141040 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (bool)__this->get_isDisposed_1();
		if (L_0)
		{
			goto IL_0052;
		}
	}
	{
		__this->set_isDisposed_1((bool)1);
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_sourceConnection_4();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		Il2CppObject * L_3 = V_0;
		NullCheck((Il2CppObject *)L_3);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
		__this->set_sourceConnection_4((Il2CppObject *)NULL);
	}

IL_002c:
	{
		Subject_1_t2775141040 * L_4 = (Subject_1_t2775141040 *)__this->get_publisher_3();
		V_1 = (Subject_1_t2775141040 *)L_4;
		Subject_1_t2775141040 * L_5 = V_1;
		if (!L_5)
		{
			goto IL_0052;
		}
	}

IL_0039:
	try
	{ // begin try (depth: 1)
		Subject_1_t2775141040 * L_6 = V_1;
		NullCheck((Subject_1_t2775141040 *)L_6);
		((  void (*) (Subject_1_t2775141040 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((Subject_1_t2775141040 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		IL2CPP_LEAVE(0x52, FINALLY_0044);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0044;
	}

FINALLY_0044:
	{ // begin finally (depth: 1)
		Subject_1_t2775141040 * L_7 = V_1;
		NullCheck((Subject_1_t2775141040 *)L_7);
		((  void (*) (Subject_1_t2775141040 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Subject_1_t2775141040 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_publisher_3((Subject_1_t2775141040 *)NULL);
		IL2CPP_END_FINALLY(68)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(68)
	{
		IL2CPP_JUMP_TBL(0x52, IL_0052)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0052:
	{
		return;
	}
}
// System.String UniRx.ReadOnlyReactiveProperty`1<System.Object>::ToString()
extern Il2CppCodeGenString* _stringLiteral3392903;
extern const uint32_t ReadOnlyReactiveProperty_1_ToString_m3439667769_MetadataUsageId;
extern "C"  String_t* ReadOnlyReactiveProperty_1_ToString_m3439667769_gshared (ReadOnlyReactiveProperty_1_t3626508436 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyReactiveProperty_1_ToString_m3439667769_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B3_0 = NULL;
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_2();
		if (L_0)
		{
			goto IL_001a;
		}
	}
	{
		G_B3_0 = _stringLiteral3392903;
		goto IL_002b;
	}

IL_001a:
	{
		Il2CppObject ** L_1 = (Il2CppObject **)__this->get_address_of_value_2();
		NullCheck((Il2CppObject *)(*L_1));
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*L_1));
		G_B3_0 = L_2;
	}

IL_002b:
	{
		return G_B3_0;
	}
}
// System.Boolean UniRx.ReadOnlyReactiveProperty`1<System.Object>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool ReadOnlyReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m2991635671_gshared (ReadOnlyReactiveProperty_1_t3626508436 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void UniRx.ReadOnlyReactiveProperty`1<UnityEngine.Color>::.ctor(UniRx.IObservable`1<T>)
extern Il2CppClass* Color_t1588175760_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyReactiveProperty_1__ctor_m556323457_MetadataUsageId;
extern "C"  void ReadOnlyReactiveProperty_1__ctor_m556323457_gshared (ReadOnlyReactiveProperty_1_t82610480 * __this, Il2CppObject* ___source0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyReactiveProperty_1__ctor_m556323457_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Color_t1588175760  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Color_t1588175760_il2cpp_TypeInfo_var, (&V_0));
		Color_t1588175760  L_0 = V_0;
		__this->set_value_2(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Subject_1_t3526210380 * L_1 = (Subject_1_t3526210380 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (Subject_1_t3526210380 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->set_publisher_3(L_1);
		Il2CppObject* L_2 = ___source0;
		ReadOnlyReactivePropertyObserver_t580263339 * L_3 = (ReadOnlyReactivePropertyObserver_t580263339 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (ReadOnlyReactivePropertyObserver_t580263339 *, ReadOnlyReactiveProperty_1_t82610480 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_3, (ReadOnlyReactiveProperty_1_t82610480 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject * L_4 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<UnityEngine.Color>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_2, (Il2CppObject*)L_3);
		__this->set_sourceConnection_4(L_4);
		return;
	}
}
// System.Void UniRx.ReadOnlyReactiveProperty`1<UnityEngine.Color>::.ctor(UniRx.IObservable`1<T>,T)
extern Il2CppClass* Color_t1588175760_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyReactiveProperty_1__ctor_m2050903001_MetadataUsageId;
extern "C"  void ReadOnlyReactiveProperty_1__ctor_m2050903001_gshared (ReadOnlyReactiveProperty_1_t82610480 * __this, Il2CppObject* ___source0, Color_t1588175760  ___initialValue1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyReactiveProperty_1__ctor_m2050903001_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Color_t1588175760  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Color_t1588175760_il2cpp_TypeInfo_var, (&V_0));
		Color_t1588175760  L_0 = V_0;
		__this->set_value_2(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Color_t1588175760  L_1 = ___initialValue1;
		__this->set_value_2(L_1);
		Subject_1_t3526210380 * L_2 = (Subject_1_t3526210380 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (Subject_1_t3526210380 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->set_publisher_3(L_2);
		Il2CppObject* L_3 = ___source0;
		ReadOnlyReactivePropertyObserver_t580263339 * L_4 = (ReadOnlyReactivePropertyObserver_t580263339 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (ReadOnlyReactivePropertyObserver_t580263339 *, ReadOnlyReactiveProperty_1_t82610480 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_4, (ReadOnlyReactiveProperty_1_t82610480 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((Il2CppObject*)L_3);
		Il2CppObject * L_5 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject* >::Invoke(0 /* System.IDisposable UniRx.IObservable`1<UnityEngine.Color>::Subscribe(UniRx.IObserver`1<T>) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_3, (Il2CppObject*)L_4);
		__this->set_sourceConnection_4(L_5);
		return;
	}
}
// T UniRx.ReadOnlyReactiveProperty`1<UnityEngine.Color>::get_Value()
extern "C"  Color_t1588175760  ReadOnlyReactiveProperty_1_get_Value_m2105092999_gshared (ReadOnlyReactiveProperty_1_t82610480 * __this, const MethodInfo* method)
{
	{
		Color_t1588175760  L_0 = (Color_t1588175760 )__this->get_value_2();
		return L_0;
	}
}
// System.IDisposable UniRx.ReadOnlyReactiveProperty`1<UnityEngine.Color>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyReactiveProperty_1_Subscribe_m3746595959_MetadataUsageId;
extern "C"  Il2CppObject * ReadOnlyReactiveProperty_1_Subscribe_m3746595959_gshared (ReadOnlyReactiveProperty_1_t82610480 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyReactiveProperty_1_Subscribe_m3746595959_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Subject_1_t3526210380 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		bool L_0 = (bool)__this->get_isDisposed_1();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Il2CppObject* L_1 = ___observer0;
		NullCheck((Il2CppObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UnityEngine.Color>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_2 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_2;
	}

IL_0017:
	{
		Subject_1_t3526210380 * L_3 = (Subject_1_t3526210380 *)__this->get_publisher_3();
		if (L_3)
		{
			goto IL_002d;
		}
	}
	{
		Subject_1_t3526210380 * L_4 = (Subject_1_t3526210380 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (Subject_1_t3526210380 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->set_publisher_3(L_4);
	}

IL_002d:
	{
		Subject_1_t3526210380 * L_5 = (Subject_1_t3526210380 *)__this->get_publisher_3();
		V_0 = (Subject_1_t3526210380 *)L_5;
		Subject_1_t3526210380 * L_6 = V_0;
		if (!L_6)
		{
			goto IL_005b;
		}
	}
	{
		Subject_1_t3526210380 * L_7 = V_0;
		Il2CppObject* L_8 = ___observer0;
		NullCheck((Subject_1_t3526210380 *)L_7);
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (Subject_1_t3526210380 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Subject_1_t3526210380 *)L_7, (Il2CppObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		V_1 = (Il2CppObject *)L_9;
		bool L_10 = (bool)__this->get_canPublishValueOnSubscribe_0();
		if (!L_10)
		{
			goto IL_0059;
		}
	}
	{
		Il2CppObject* L_11 = ___observer0;
		Color_t1588175760  L_12 = (Color_t1588175760 )__this->get_value_2();
		NullCheck((Il2CppObject*)L_11);
		InterfaceActionInvoker1< Color_t1588175760  >::Invoke(2 /* System.Void UniRx.IObserver`1<UnityEngine.Color>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_11, (Color_t1588175760 )L_12);
	}

IL_0059:
	{
		Il2CppObject * L_13 = V_1;
		return L_13;
	}

IL_005b:
	{
		Il2CppObject* L_14 = ___observer0;
		NullCheck((Il2CppObject*)L_14);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<UnityEngine.Color>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_14);
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_15 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_15;
	}
}
// System.Void UniRx.ReadOnlyReactiveProperty`1<UnityEngine.Color>::Dispose()
extern "C"  void ReadOnlyReactiveProperty_1_Dispose_m2383740383_gshared (ReadOnlyReactiveProperty_1_t82610480 * __this, const MethodInfo* method)
{
	{
		NullCheck((ReadOnlyReactiveProperty_1_t82610480 *)__this);
		VirtActionInvoker1< bool >::Invoke(8 /* System.Void UniRx.ReadOnlyReactiveProperty`1<UnityEngine.Color>::Dispose(System.Boolean) */, (ReadOnlyReactiveProperty_1_t82610480 *)__this, (bool)1);
		GC_SuppressFinalize_m1160635446(NULL /*static, unused*/, (Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.ReadOnlyReactiveProperty`1<UnityEngine.Color>::Dispose(System.Boolean)
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyReactiveProperty_1_Dispose_m2073985302_MetadataUsageId;
extern "C"  void ReadOnlyReactiveProperty_1_Dispose_m2073985302_gshared (ReadOnlyReactiveProperty_1_t82610480 * __this, bool ___disposing0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyReactiveProperty_1_Dispose_m2073985302_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Subject_1_t3526210380 * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (bool)__this->get_isDisposed_1();
		if (L_0)
		{
			goto IL_0052;
		}
	}
	{
		__this->set_isDisposed_1((bool)1);
		Il2CppObject * L_1 = (Il2CppObject *)__this->get_sourceConnection_4();
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		Il2CppObject * L_3 = V_0;
		NullCheck((Il2CppObject *)L_3);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
		__this->set_sourceConnection_4((Il2CppObject *)NULL);
	}

IL_002c:
	{
		Subject_1_t3526210380 * L_4 = (Subject_1_t3526210380 *)__this->get_publisher_3();
		V_1 = (Subject_1_t3526210380 *)L_4;
		Subject_1_t3526210380 * L_5 = V_1;
		if (!L_5)
		{
			goto IL_0052;
		}
	}

IL_0039:
	try
	{ // begin try (depth: 1)
		Subject_1_t3526210380 * L_6 = V_1;
		NullCheck((Subject_1_t3526210380 *)L_6);
		((  void (*) (Subject_1_t3526210380 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((Subject_1_t3526210380 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		IL2CPP_LEAVE(0x52, FINALLY_0044);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0044;
	}

FINALLY_0044:
	{ // begin finally (depth: 1)
		Subject_1_t3526210380 * L_7 = V_1;
		NullCheck((Subject_1_t3526210380 *)L_7);
		((  void (*) (Subject_1_t3526210380 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Subject_1_t3526210380 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_publisher_3((Subject_1_t3526210380 *)NULL);
		IL2CPP_END_FINALLY(68)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(68)
	{
		IL2CPP_JUMP_TBL(0x52, IL_0052)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0052:
	{
		return;
	}
}
// System.String UniRx.ReadOnlyReactiveProperty`1<UnityEngine.Color>::ToString()
extern Il2CppCodeGenString* _stringLiteral3392903;
extern const uint32_t ReadOnlyReactiveProperty_1_ToString_m2327039563_MetadataUsageId;
extern "C"  String_t* ReadOnlyReactiveProperty_1_ToString_m2327039563_gshared (ReadOnlyReactiveProperty_1_t82610480 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyReactiveProperty_1_ToString_m2327039563_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B3_0 = NULL;
	{
		Color_t1588175760  L_0 = (Color_t1588175760 )__this->get_value_2();
		goto IL_001a;
	}
	{
		G_B3_0 = _stringLiteral3392903;
		goto IL_002b;
	}

IL_001a:
	{
		Color_t1588175760 * L_1 = (Color_t1588175760 *)__this->get_address_of_value_2();
		String_t* L_2 = Color_ToString_m2277845527((Color_t1588175760 *)L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_002b:
	{
		return G_B3_0;
	}
}
// System.Boolean UniRx.ReadOnlyReactiveProperty`1<UnityEngine.Color>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool ReadOnlyReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m2589958889_gshared (ReadOnlyReactiveProperty_1_t82610480 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void UniRx.ReplaySubject`1/Subscription<System.Object>::.ctor(UniRx.ReplaySubject`1<T>,UniRx.IObserver`1<T>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Subscription__ctor_m2334232119_MetadataUsageId;
extern "C"  void Subscription__ctor_m2334232119_gshared (Subscription_t2911320241 * __this, ReplaySubject_1_t1910820897 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscription__ctor_m2334232119_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ReplaySubject_1_t1910820897 * L_1 = ___parent0;
		__this->set_parent_1(L_1);
		Il2CppObject* L_2 = ___unsubscribeTarget1;
		__this->set_unsubscribeTarget_2(L_2);
		return;
	}
}
// System.Void UniRx.ReplaySubject`1/Subscription<System.Object>::Dispose()
extern "C"  void Subscription_Dispose_m2149913094_gshared (Subscription_t2911320241 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ListObserver_1_t1131724091 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			ReplaySubject_1_t1910820897 * L_2 = (ReplaySubject_1_t1910820897 *)__this->get_parent_1();
			if (!L_2)
			{
				goto IL_0087;
			}
		}

IL_0018:
		{
			ReplaySubject_1_t1910820897 * L_3 = (ReplaySubject_1_t1910820897 *)__this->get_parent_1();
			NullCheck(L_3);
			Il2CppObject * L_4 = (Il2CppObject *)L_3->get_observerLock_0();
			V_1 = (Il2CppObject *)L_4;
			Il2CppObject * L_5 = V_1;
			Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		}

IL_002a:
		try
		{ // begin try (depth: 2)
			{
				ReplaySubject_1_t1910820897 * L_6 = (ReplaySubject_1_t1910820897 *)__this->get_parent_1();
				NullCheck(L_6);
				Il2CppObject* L_7 = (Il2CppObject*)L_6->get_outObserver_4();
				V_2 = (ListObserver_1_t1131724091 *)((ListObserver_1_t1131724091 *)IsInst(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
				ListObserver_1_t1131724091 * L_8 = V_2;
				if (!L_8)
				{
					goto IL_005d;
				}
			}

IL_0041:
			{
				ReplaySubject_1_t1910820897 * L_9 = (ReplaySubject_1_t1910820897 *)__this->get_parent_1();
				ListObserver_1_t1131724091 * L_10 = V_2;
				Il2CppObject* L_11 = (Il2CppObject*)__this->get_unsubscribeTarget_2();
				NullCheck((ListObserver_1_t1131724091 *)L_10);
				Il2CppObject* L_12 = ((  Il2CppObject* (*) (ListObserver_1_t1131724091 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ListObserver_1_t1131724091 *)L_10, (Il2CppObject*)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
				NullCheck(L_9);
				L_9->set_outObserver_4(L_12);
				goto IL_006d;
			}

IL_005d:
			{
				ReplaySubject_1_t1910820897 * L_13 = (ReplaySubject_1_t1910820897 *)__this->get_parent_1();
				IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
				EmptyObserver_1_t246459410 * L_14 = ((EmptyObserver_1_t246459410_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_Instance_0();
				NullCheck(L_13);
				L_13->set_outObserver_4(L_14);
			}

IL_006d:
			{
				__this->set_unsubscribeTarget_2((Il2CppObject*)NULL);
				__this->set_parent_1((ReplaySubject_1_t1910820897 *)NULL);
				IL2CPP_LEAVE(0x87, FINALLY_0080);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0080;
		}

FINALLY_0080:
		{ // begin finally (depth: 2)
			Il2CppObject * L_15 = V_1;
			Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_15, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(128)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(128)
		{
			IL2CPP_JUMP_TBL(0x87, IL_0087)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0087:
		{
			IL2CPP_LEAVE(0x93, FINALLY_008c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008c;
	}

FINALLY_008c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_16 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(140)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(140)
	{
		IL2CPP_JUMP_TBL(0x93, IL_0093)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0093:
	{
		return;
	}
}
// System.Void UniRx.ReplaySubject`1<System.Object>::.ctor()
extern Il2CppClass* TimeSpan_t763862892_il2cpp_TypeInfo_var;
extern const uint32_t ReplaySubject_1__ctor_m2541553777_MetadataUsageId;
extern "C"  void ReplaySubject_1__ctor_m2541553777_gshared (ReplaySubject_1_t1910820897 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReplaySubject_1__ctor_m2541553777_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t763862892_il2cpp_TypeInfo_var);
		TimeSpan_t763862892  L_0 = ((TimeSpan_t763862892_StaticFields*)TimeSpan_t763862892_il2cpp_TypeInfo_var->static_fields)->get_MaxValue_5();
		Il2CppObject * L_1 = DefaultSchedulers_get_Iteration_m2327009079(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck((ReplaySubject_1_t1910820897 *)__this);
		((  void (*) (ReplaySubject_1_t1910820897 *, int32_t, TimeSpan_t763862892 , Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ReplaySubject_1_t1910820897 *)__this, (int32_t)((int32_t)2147483647LL), (TimeSpan_t763862892 )L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.ReplaySubject`1<System.Object>::.ctor(UniRx.IScheduler)
extern Il2CppClass* TimeSpan_t763862892_il2cpp_TypeInfo_var;
extern const uint32_t ReplaySubject_1__ctor_m3619462535_MetadataUsageId;
extern "C"  void ReplaySubject_1__ctor_m3619462535_gshared (ReplaySubject_1_t1910820897 * __this, Il2CppObject * ___scheduler0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReplaySubject_1__ctor_m3619462535_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t763862892_il2cpp_TypeInfo_var);
		TimeSpan_t763862892  L_0 = ((TimeSpan_t763862892_StaticFields*)TimeSpan_t763862892_il2cpp_TypeInfo_var->static_fields)->get_MaxValue_5();
		Il2CppObject * L_1 = ___scheduler0;
		NullCheck((ReplaySubject_1_t1910820897 *)__this);
		((  void (*) (ReplaySubject_1_t1910820897 *, int32_t, TimeSpan_t763862892 , Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ReplaySubject_1_t1910820897 *)__this, (int32_t)((int32_t)2147483647LL), (TimeSpan_t763862892 )L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.ReplaySubject`1<System.Object>::.ctor(System.Int32)
extern Il2CppClass* TimeSpan_t763862892_il2cpp_TypeInfo_var;
extern const uint32_t ReplaySubject_1__ctor_m3130301506_MetadataUsageId;
extern "C"  void ReplaySubject_1__ctor_m3130301506_gshared (ReplaySubject_1_t1910820897 * __this, int32_t ___bufferSize0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReplaySubject_1__ctor_m3130301506_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___bufferSize0;
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t763862892_il2cpp_TypeInfo_var);
		TimeSpan_t763862892  L_1 = ((TimeSpan_t763862892_StaticFields*)TimeSpan_t763862892_il2cpp_TypeInfo_var->static_fields)->get_MaxValue_5();
		Il2CppObject * L_2 = DefaultSchedulers_get_Iteration_m2327009079(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck((ReplaySubject_1_t1910820897 *)__this);
		((  void (*) (ReplaySubject_1_t1910820897 *, int32_t, TimeSpan_t763862892 , Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ReplaySubject_1_t1910820897 *)__this, (int32_t)L_0, (TimeSpan_t763862892 )L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.ReplaySubject`1<System.Object>::.ctor(System.Int32,UniRx.IScheduler)
extern Il2CppClass* TimeSpan_t763862892_il2cpp_TypeInfo_var;
extern const uint32_t ReplaySubject_1__ctor_m4237425562_MetadataUsageId;
extern "C"  void ReplaySubject_1__ctor_m4237425562_gshared (ReplaySubject_1_t1910820897 * __this, int32_t ___bufferSize0, Il2CppObject * ___scheduler1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReplaySubject_1__ctor_m4237425562_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___bufferSize0;
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t763862892_il2cpp_TypeInfo_var);
		TimeSpan_t763862892  L_1 = ((TimeSpan_t763862892_StaticFields*)TimeSpan_t763862892_il2cpp_TypeInfo_var->static_fields)->get_MaxValue_5();
		Il2CppObject * L_2 = ___scheduler1;
		NullCheck((ReplaySubject_1_t1910820897 *)__this);
		((  void (*) (ReplaySubject_1_t1910820897 *, int32_t, TimeSpan_t763862892 , Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ReplaySubject_1_t1910820897 *)__this, (int32_t)L_0, (TimeSpan_t763862892 )L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.ReplaySubject`1<System.Object>::.ctor(System.TimeSpan)
extern "C"  void ReplaySubject_1__ctor_m3497541899_gshared (ReplaySubject_1_t1910820897 * __this, TimeSpan_t763862892  ___window0, const MethodInfo* method)
{
	{
		TimeSpan_t763862892  L_0 = ___window0;
		Il2CppObject * L_1 = DefaultSchedulers_get_Iteration_m2327009079(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck((ReplaySubject_1_t1910820897 *)__this);
		((  void (*) (ReplaySubject_1_t1910820897 *, int32_t, TimeSpan_t763862892 , Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ReplaySubject_1_t1910820897 *)__this, (int32_t)((int32_t)2147483647LL), (TimeSpan_t763862892 )L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.ReplaySubject`1<System.Object>::.ctor(System.TimeSpan,UniRx.IScheduler)
extern "C"  void ReplaySubject_1__ctor_m3564462577_gshared (ReplaySubject_1_t1910820897 * __this, TimeSpan_t763862892  ___window0, Il2CppObject * ___scheduler1, const MethodInfo* method)
{
	{
		TimeSpan_t763862892  L_0 = ___window0;
		Il2CppObject * L_1 = ___scheduler1;
		NullCheck((ReplaySubject_1_t1910820897 *)__this);
		((  void (*) (ReplaySubject_1_t1910820897 *, int32_t, TimeSpan_t763862892 , Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ReplaySubject_1_t1910820897 *)__this, (int32_t)((int32_t)2147483647LL), (TimeSpan_t763862892 )L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.ReplaySubject`1<System.Object>::.ctor(System.Int32,System.TimeSpan,UniRx.IScheduler)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppClass* TimeSpan_t763862892_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1906231393;
extern Il2CppCodeGenString* _stringLiteral3507215344;
extern Il2CppCodeGenString* _stringLiteral4134256827;
extern const uint32_t ReplaySubject_1__ctor_m191511044_MetadataUsageId;
extern "C"  void ReplaySubject_1__ctor_m191511044_gshared (ReplaySubject_1_t1910820897 * __this, int32_t ___bufferSize0, TimeSpan_t763862892  ___window1, Il2CppObject * ___scheduler2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReplaySubject_1__ctor_m191511044_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_observerLock_0(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		EmptyObserver_1_t246459410 * L_1 = ((EmptyObserver_1_t246459410_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->get_Instance_0();
		__this->set_outObserver_4(L_1);
		Queue_1_t2416153082 * L_2 = (Queue_1_t2416153082 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Queue_1_t2416153082 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->set_queue_9(L_2);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_3 = ___bufferSize0;
		if ((((int32_t)L_3) >= ((int32_t)0)))
		{
			goto IL_0039;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_4 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_4, (String_t*)_stringLiteral1906231393, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0039:
	{
		TimeSpan_t763862892  L_5 = ___window1;
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t763862892_il2cpp_TypeInfo_var);
		TimeSpan_t763862892  L_6 = ((TimeSpan_t763862892_StaticFields*)TimeSpan_t763862892_il2cpp_TypeInfo_var->static_fields)->get_Zero_7();
		bool L_7 = TimeSpan_op_LessThan_m4265983228(NULL /*static, unused*/, (TimeSpan_t763862892 )L_5, (TimeSpan_t763862892 )L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0054;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_8 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_8, (String_t*)_stringLiteral3507215344, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_0054:
	{
		Il2CppObject * L_9 = ___scheduler2;
		if (L_9)
		{
			goto IL_0065;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_10 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_10, (String_t*)_stringLiteral4134256827, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
	}

IL_0065:
	{
		int32_t L_11 = ___bufferSize0;
		__this->set_bufferSize_5(L_11);
		TimeSpan_t763862892  L_12 = ___window1;
		__this->set_window_6(L_12);
		Il2CppObject * L_13 = ___scheduler2;
		__this->set_scheduler_8(L_13);
		Il2CppObject * L_14 = ___scheduler2;
		NullCheck((Il2CppObject *)L_14);
		DateTimeOffset_t3712260035  L_15 = InterfaceFuncInvoker0< DateTimeOffset_t3712260035  >::Invoke(0 /* System.DateTimeOffset UniRx.IScheduler::get_Now() */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_14);
		__this->set_startTime_7(L_15);
		return;
	}
}
// System.Void UniRx.ReplaySubject`1<System.Object>::Trim()
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTimeOffset_t3712260035_il2cpp_TypeInfo_var;
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern const uint32_t ReplaySubject_1_Trim_m767772245_MetadataUsageId;
extern "C"  void ReplaySubject_1_Trim_m767772245_gshared (ReplaySubject_1_t1910820897 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReplaySubject_1_Trim_m767772245_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TimeSpan_t763862892  V_0;
	memset(&V_0, 0, sizeof(V_0));
	TimeInterval_1_t708065542  V_1;
	memset(&V_1, 0, sizeof(V_1));
	TimeSpan_t763862892  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_scheduler_8();
		NullCheck((Il2CppObject *)L_0);
		DateTimeOffset_t3712260035  L_1 = InterfaceFuncInvoker0< DateTimeOffset_t3712260035  >::Invoke(0 /* System.DateTimeOffset UniRx.IScheduler::get_Now() */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		DateTimeOffset_t3712260035  L_2 = (DateTimeOffset_t3712260035 )__this->get_startTime_7();
		IL2CPP_RUNTIME_CLASS_INIT(DateTimeOffset_t3712260035_il2cpp_TypeInfo_var);
		TimeSpan_t763862892  L_3 = DateTimeOffset_op_Subtraction_m3815865550(NULL /*static, unused*/, (DateTimeOffset_t3712260035 )L_1, (DateTimeOffset_t3712260035 )L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		TimeSpan_t763862892  L_4 = Scheduler_Normalize_m3379699520(NULL /*static, unused*/, (TimeSpan_t763862892 )L_3, /*hidden argument*/NULL);
		V_0 = (TimeSpan_t763862892 )L_4;
		goto IL_002d;
	}

IL_0021:
	{
		Queue_1_t2416153082 * L_5 = (Queue_1_t2416153082 *)__this->get_queue_9();
		NullCheck((Queue_1_t2416153082 *)L_5);
		((  TimeInterval_1_t708065542  (*) (Queue_1_t2416153082 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Queue_1_t2416153082 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
	}

IL_002d:
	{
		Queue_1_t2416153082 * L_6 = (Queue_1_t2416153082 *)__this->get_queue_9();
		NullCheck((Queue_1_t2416153082 *)L_6);
		int32_t L_7 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 System.Collections.Generic.Queue`1<UniRx.TimeInterval`1<System.Object>>::get_Count() */, (Queue_1_t2416153082 *)L_6);
		int32_t L_8 = (int32_t)__this->get_bufferSize_5();
		if ((((int32_t)L_7) > ((int32_t)L_8)))
		{
			goto IL_0021;
		}
	}
	{
		goto IL_0054;
	}

IL_0048:
	{
		Queue_1_t2416153082 * L_9 = (Queue_1_t2416153082 *)__this->get_queue_9();
		NullCheck((Queue_1_t2416153082 *)L_9);
		((  TimeInterval_1_t708065542  (*) (Queue_1_t2416153082 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Queue_1_t2416153082 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
	}

IL_0054:
	{
		Queue_1_t2416153082 * L_10 = (Queue_1_t2416153082 *)__this->get_queue_9();
		NullCheck((Queue_1_t2416153082 *)L_10);
		int32_t L_11 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 System.Collections.Generic.Queue`1<UniRx.TimeInterval`1<System.Object>>::get_Count() */, (Queue_1_t2416153082 *)L_10);
		if ((((int32_t)L_11) <= ((int32_t)0)))
		{
			goto IL_0093;
		}
	}
	{
		Queue_1_t2416153082 * L_12 = (Queue_1_t2416153082 *)__this->get_queue_9();
		NullCheck((Queue_1_t2416153082 *)L_12);
		TimeInterval_1_t708065542  L_13 = ((  TimeInterval_1_t708065542  (*) (Queue_1_t2416153082 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Queue_1_t2416153082 *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		V_1 = (TimeInterval_1_t708065542 )L_13;
		TimeSpan_t763862892  L_14 = ((  TimeSpan_t763862892  (*) (TimeInterval_1_t708065542 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((TimeInterval_1_t708065542 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		TimeSpan_t763862892  L_15 = TimeSpan_Subtract_m1410255071((TimeSpan_t763862892 *)(&V_0), (TimeSpan_t763862892 )L_14, /*hidden argument*/NULL);
		V_2 = (TimeSpan_t763862892 )L_15;
		TimeSpan_t763862892  L_16 = (TimeSpan_t763862892 )__this->get_window_6();
		int32_t L_17 = TimeSpan_CompareTo_m2960988804((TimeSpan_t763862892 *)(&V_2), (TimeSpan_t763862892 )L_16, /*hidden argument*/NULL);
		if ((((int32_t)L_17) > ((int32_t)0)))
		{
			goto IL_0048;
		}
	}

IL_0093:
	{
		return;
	}
}
// System.Void UniRx.ReplaySubject`1<System.Object>::OnCompleted()
extern "C"  void ReplaySubject_1_OnCompleted_m1109549883_gshared (ReplaySubject_1_t1910820897 * __this, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_observerLock_0();
		V_1 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			NullCheck((ReplaySubject_1_t1910820897 *)__this);
			((  void (*) (ReplaySubject_1_t1910820897 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((ReplaySubject_1_t1910820897 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
			bool L_2 = (bool)__this->get_isStopped_1();
			if (!L_2)
			{
				goto IL_0023;
			}
		}

IL_001e:
		{
			IL2CPP_LEAVE(0x54, FINALLY_0047);
		}

IL_0023:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_outObserver_4();
			V_0 = (Il2CppObject*)L_3;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
			EmptyObserver_1_t246459410 * L_4 = ((EmptyObserver_1_t246459410_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->get_Instance_0();
			__this->set_outObserver_4(L_4);
			__this->set_isStopped_1((bool)1);
			NullCheck((ReplaySubject_1_t1910820897 *)__this);
			((  void (*) (ReplaySubject_1_t1910820897 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((ReplaySubject_1_t1910820897 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
			IL2CPP_LEAVE(0x4E, FINALLY_0047);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0047;
	}

FINALLY_0047:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(71)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(71)
	{
		IL2CPP_JUMP_TBL(0x54, IL_0054)
		IL2CPP_JUMP_TBL(0x4E, IL_004e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_004e:
	{
		Il2CppObject* L_6 = V_0;
		NullCheck((Il2CppObject*)L_6);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10), (Il2CppObject*)L_6);
	}

IL_0054:
	{
		return;
	}
}
// System.Void UniRx.ReplaySubject`1<System.Object>::OnError(System.Exception)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral96784904;
extern const uint32_t ReplaySubject_1_OnError_m3749909480_MetadataUsageId;
extern "C"  void ReplaySubject_1_OnError_m3749909480_gshared (ReplaySubject_1_t1910820897 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReplaySubject_1_OnError_m3749909480_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Exception_t1967233988 * L_0 = ___error0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral96784904, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_observerLock_0();
		V_1 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
	}

IL_001e:
	try
	{ // begin try (depth: 1)
		{
			NullCheck((ReplaySubject_1_t1910820897 *)__this);
			((  void (*) (ReplaySubject_1_t1910820897 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((ReplaySubject_1_t1910820897 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
			bool L_4 = (bool)__this->get_isStopped_1();
			if (!L_4)
			{
				goto IL_0034;
			}
		}

IL_002f:
		{
			IL2CPP_LEAVE(0x6D, FINALLY_005f);
		}

IL_0034:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_outObserver_4();
			V_0 = (Il2CppObject*)L_5;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
			EmptyObserver_1_t246459410 * L_6 = ((EmptyObserver_1_t246459410_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->get_Instance_0();
			__this->set_outObserver_4(L_6);
			__this->set_isStopped_1((bool)1);
			Exception_t1967233988 * L_7 = ___error0;
			__this->set_lastError_3(L_7);
			NullCheck((ReplaySubject_1_t1910820897 *)__this);
			((  void (*) (ReplaySubject_1_t1910820897 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((ReplaySubject_1_t1910820897 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
			IL2CPP_LEAVE(0x66, FINALLY_005f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_005f;
	}

FINALLY_005f:
	{ // begin finally (depth: 1)
		Il2CppObject * L_8 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(95)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(95)
	{
		IL2CPP_JUMP_TBL(0x6D, IL_006d)
		IL2CPP_JUMP_TBL(0x66, IL_0066)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0066:
	{
		Il2CppObject* L_9 = V_0;
		Exception_t1967233988 * L_10 = ___error0;
		NullCheck((Il2CppObject*)L_9);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10), (Il2CppObject*)L_9, (Exception_t1967233988 *)L_10);
	}

IL_006d:
	{
		return;
	}
}
// System.Void UniRx.ReplaySubject`1<System.Object>::OnNext(T)
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTimeOffset_t3712260035_il2cpp_TypeInfo_var;
extern const uint32_t ReplaySubject_1_OnNext_m1427491545_MetadataUsageId;
extern "C"  void ReplaySubject_1_OnNext_m1427491545_gshared (ReplaySubject_1_t1910820897 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReplaySubject_1_OnNext_m1427491545_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_observerLock_0();
		V_1 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			NullCheck((ReplaySubject_1_t1910820897 *)__this);
			((  void (*) (ReplaySubject_1_t1910820897 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((ReplaySubject_1_t1910820897 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
			bool L_2 = (bool)__this->get_isStopped_1();
			if (!L_2)
			{
				goto IL_0023;
			}
		}

IL_001e:
		{
			IL2CPP_LEAVE(0x6A, FINALLY_005c);
		}

IL_0023:
		{
			Queue_1_t2416153082 * L_3 = (Queue_1_t2416153082 *)__this->get_queue_9();
			Il2CppObject * L_4 = ___value0;
			Il2CppObject * L_5 = (Il2CppObject *)__this->get_scheduler_8();
			NullCheck((Il2CppObject *)L_5);
			DateTimeOffset_t3712260035  L_6 = InterfaceFuncInvoker0< DateTimeOffset_t3712260035  >::Invoke(0 /* System.DateTimeOffset UniRx.IScheduler::get_Now() */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
			DateTimeOffset_t3712260035  L_7 = (DateTimeOffset_t3712260035 )__this->get_startTime_7();
			IL2CPP_RUNTIME_CLASS_INIT(DateTimeOffset_t3712260035_il2cpp_TypeInfo_var);
			TimeSpan_t763862892  L_8 = DateTimeOffset_op_Subtraction_m3815865550(NULL /*static, unused*/, (DateTimeOffset_t3712260035 )L_6, (DateTimeOffset_t3712260035 )L_7, /*hidden argument*/NULL);
			TimeInterval_1_t708065542  L_9;
			memset(&L_9, 0, sizeof(L_9));
			((  void (*) (TimeInterval_1_t708065542 *, Il2CppObject *, TimeSpan_t763862892 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)(&L_9, (Il2CppObject *)L_4, (TimeSpan_t763862892 )L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
			NullCheck((Queue_1_t2416153082 *)L_3);
			((  void (*) (Queue_1_t2416153082 *, TimeInterval_1_t708065542 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->method)((Queue_1_t2416153082 *)L_3, (TimeInterval_1_t708065542 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
			NullCheck((ReplaySubject_1_t1910820897 *)__this);
			((  void (*) (ReplaySubject_1_t1910820897 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((ReplaySubject_1_t1910820897 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
			Il2CppObject* L_10 = (Il2CppObject*)__this->get_outObserver_4();
			V_0 = (Il2CppObject*)L_10;
			IL2CPP_LEAVE(0x63, FINALLY_005c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_005c;
	}

FINALLY_005c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_11 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_11, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(92)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(92)
	{
		IL2CPP_JUMP_TBL(0x6A, IL_006a)
		IL2CPP_JUMP_TBL(0x63, IL_0063)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0063:
	{
		Il2CppObject* L_12 = V_0;
		Il2CppObject * L_13 = ___value0;
		NullCheck((Il2CppObject*)L_12);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10), (Il2CppObject*)L_12, (Il2CppObject *)L_13);
	}

IL_006a:
	{
		return;
	}
}
// System.IDisposable UniRx.ReplaySubject`1<System.Object>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral348607190;
extern const uint32_t ReplaySubject_1_Subscribe_m3366756272_MetadataUsageId;
extern "C"  Il2CppObject * ReplaySubject_1_Subscribe_m3366756272_gshared (ReplaySubject_1_t1910820897 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReplaySubject_1_Subscribe_m3366756272_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	Subscription_t2911320241 * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	ListObserver_1_t1131724091 * V_3 = NULL;
	Il2CppObject* V_4 = NULL;
	TimeInterval_1_t708065542  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Enumerator_t3885774799  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___observer0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral348607190, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		V_0 = (Exception_t1967233988 *)NULL;
		V_1 = (Subscription_t2911320241 *)NULL;
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_observerLock_0();
		V_2 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_2;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
	}

IL_0022:
	try
	{ // begin try (depth: 1)
		{
			NullCheck((ReplaySubject_1_t1910820897 *)__this);
			((  void (*) (ReplaySubject_1_t1910820897 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((ReplaySubject_1_t1910820897 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
			bool L_4 = (bool)__this->get_isStopped_1();
			if (L_4)
			{
				goto IL_009e;
			}
		}

IL_0033:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_outObserver_4();
			V_3 = (ListObserver_1_t1131724091 *)((ListObserver_1_t1131724091 *)IsInst(L_5, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)));
			ListObserver_1_t1131724091 * L_6 = V_3;
			if (!L_6)
			{
				goto IL_0057;
			}
		}

IL_0045:
		{
			ListObserver_1_t1131724091 * L_7 = V_3;
			Il2CppObject* L_8 = ___observer0;
			NullCheck((ListObserver_1_t1131724091 *)L_7);
			Il2CppObject* L_9 = ((  Il2CppObject* (*) (ListObserver_1_t1131724091 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->method)((ListObserver_1_t1131724091 *)L_7, (Il2CppObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
			__this->set_outObserver_4(L_9);
			goto IL_0096;
		}

IL_0057:
		{
			Il2CppObject* L_10 = (Il2CppObject*)__this->get_outObserver_4();
			V_4 = (Il2CppObject*)L_10;
			Il2CppObject* L_11 = V_4;
			if (!((EmptyObserver_1_t246459410 *)IsInst(L_11, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16))))
			{
				goto IL_0077;
			}
		}

IL_006b:
		{
			Il2CppObject* L_12 = ___observer0;
			__this->set_outObserver_4(L_12);
			goto IL_0096;
		}

IL_0077:
		{
			IObserver_1U5BU5D_t3998655818* L_13 = (IObserver_1U5BU5D_t3998655818*)((IObserver_1U5BU5D_t3998655818*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17), (uint32_t)2));
			Il2CppObject* L_14 = V_4;
			NullCheck(L_13);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 0);
			ArrayElementTypeCheck (L_13, L_14);
			(L_13)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject*)L_14);
			IObserver_1U5BU5D_t3998655818* L_15 = (IObserver_1U5BU5D_t3998655818*)L_13;
			Il2CppObject* L_16 = ___observer0;
			NullCheck(L_15);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 1);
			ArrayElementTypeCheck (L_15, L_16);
			(L_15)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject*)L_16);
			ImmutableList_1_t4109309822 * L_17 = (ImmutableList_1_t4109309822 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
			((  void (*) (ImmutableList_1_t4109309822 *, IObserver_1U5BU5D_t3998655818*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19)->method)(L_17, (IObserver_1U5BU5D_t3998655818*)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
			ListObserver_1_t1131724091 * L_18 = (ListObserver_1_t1131724091 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
			((  void (*) (ListObserver_1_t1131724091 *, ImmutableList_1_t4109309822 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20)->method)(L_18, (ImmutableList_1_t4109309822 *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20));
			__this->set_outObserver_4(L_18);
		}

IL_0096:
		{
			Il2CppObject* L_19 = ___observer0;
			Subscription_t2911320241 * L_20 = (Subscription_t2911320241 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21));
			((  void (*) (Subscription_t2911320241 *, ReplaySubject_1_t1910820897 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22)->method)(L_20, (ReplaySubject_1_t1910820897 *)__this, (Il2CppObject*)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22));
			V_1 = (Subscription_t2911320241 *)L_20;
		}

IL_009e:
		{
			Exception_t1967233988 * L_21 = (Exception_t1967233988 *)__this->get_lastError_3();
			V_0 = (Exception_t1967233988 *)L_21;
			NullCheck((ReplaySubject_1_t1910820897 *)__this);
			((  void (*) (ReplaySubject_1_t1910820897 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((ReplaySubject_1_t1910820897 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
			Queue_1_t2416153082 * L_22 = (Queue_1_t2416153082 *)__this->get_queue_9();
			NullCheck((Queue_1_t2416153082 *)L_22);
			Enumerator_t3885774799  L_23 = ((  Enumerator_t3885774799  (*) (Queue_1_t2416153082 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23)->method)((Queue_1_t2416153082 *)L_22, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23));
			V_6 = (Enumerator_t3885774799 )L_23;
		}

IL_00b8:
		try
		{ // begin try (depth: 2)
			{
				goto IL_00d3;
			}

IL_00bd:
			{
				TimeInterval_1_t708065542  L_24 = ((  TimeInterval_1_t708065542  (*) (Enumerator_t3885774799 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24)->method)((Enumerator_t3885774799 *)(&V_6), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
				V_5 = (TimeInterval_1_t708065542 )L_24;
				Il2CppObject* L_25 = ___observer0;
				Il2CppObject * L_26 = ((  Il2CppObject * (*) (TimeInterval_1_t708065542 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25)->method)((TimeInterval_1_t708065542 *)(&V_5), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25));
				NullCheck((Il2CppObject*)L_25);
				InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10), (Il2CppObject*)L_25, (Il2CppObject *)L_26);
			}

IL_00d3:
			{
				bool L_27 = ((  bool (*) (Enumerator_t3885774799 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26)->method)((Enumerator_t3885774799 *)(&V_6), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26));
				if (L_27)
				{
					goto IL_00bd;
				}
			}

IL_00df:
			{
				IL2CPP_LEAVE(0xF1, FINALLY_00e4);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_00e4;
		}

FINALLY_00e4:
		{ // begin finally (depth: 2)
			Enumerator_t3885774799  L_28 = V_6;
			Enumerator_t3885774799  L_29 = L_28;
			Il2CppObject * L_30 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27), &L_29);
			NullCheck((Il2CppObject *)L_30);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_30);
			IL2CPP_END_FINALLY(228)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(228)
		{
			IL2CPP_JUMP_TBL(0xF1, IL_00f1)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_00f1:
		{
			IL2CPP_LEAVE(0xFD, FINALLY_00f6);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00f6;
	}

FINALLY_00f6:
	{ // begin finally (depth: 1)
		Il2CppObject * L_31 = V_2;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_31, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(246)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(246)
	{
		IL2CPP_JUMP_TBL(0xFD, IL_00fd)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00fd:
	{
		Subscription_t2911320241 * L_32 = V_1;
		if (!L_32)
		{
			goto IL_0105;
		}
	}
	{
		Subscription_t2911320241 * L_33 = V_1;
		return L_33;
	}

IL_0105:
	{
		Exception_t1967233988 * L_34 = V_0;
		if (!L_34)
		{
			goto IL_0117;
		}
	}
	{
		Il2CppObject* L_35 = ___observer0;
		Exception_t1967233988 * L_36 = V_0;
		NullCheck((Il2CppObject*)L_35);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10), (Il2CppObject*)L_35, (Exception_t1967233988 *)L_36);
		goto IL_011d;
	}

IL_0117:
	{
		Il2CppObject* L_37 = ___observer0;
		NullCheck((Il2CppObject*)L_37);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10), (Il2CppObject*)L_37);
	}

IL_011d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_38 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_38;
	}
}
// System.Void UniRx.ReplaySubject`1<System.Object>::Dispose()
extern "C"  void ReplaySubject_1_Dispose_m2790038638_gshared (ReplaySubject_1_t1910820897 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_observerLock_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		__this->set_isDisposed_2((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28));
		DisposedObserver_1_t11563882 * L_2 = ((DisposedObserver_1_t11563882_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28)->static_fields)->get_Instance_0();
		__this->set_outObserver_4(L_2);
		__this->set_lastError_3((Exception_t1967233988 *)NULL);
		__this->set_queue_9((Queue_1_t2416153082 *)NULL);
		IL2CPP_LEAVE(0x39, FINALLY_0032);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0032;
	}

FINALLY_0032:
	{ // begin finally (depth: 1)
		Il2CppObject * L_3 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(50)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(50)
	{
		IL2CPP_JUMP_TBL(0x39, IL_0039)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0039:
	{
		return;
	}
}
// System.Void UniRx.ReplaySubject`1<System.Object>::ThrowIfDisposed()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t ReplaySubject_1_ThrowIfDisposed_m309721079_MetadataUsageId;
extern "C"  void ReplaySubject_1_ThrowIfDisposed_m309721079_gshared (ReplaySubject_1_t1910820897 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReplaySubject_1_ThrowIfDisposed_m309721079_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_isDisposed_2();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_2 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0016:
	{
		return;
	}
}
// System.Boolean UniRx.ReplaySubject`1<System.Object>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool ReplaySubject_1_IsRequiredSubscribeOnCurrentThread_m3711271858_gshared (ReplaySubject_1_t1910820897 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void UniRx.ScheduledNotifier`1/<Report>c__AnonStorey58<System.Object>::.ctor()
extern "C"  void U3CReportU3Ec__AnonStorey58__ctor_m2356660748_gshared (U3CReportU3Ec__AnonStorey58_t3448642837 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.ScheduledNotifier`1/<Report>c__AnonStorey58<System.Object>::<>m__6C()
extern "C"  void U3CReportU3Ec__AnonStorey58_U3CU3Em__6C_m3611421026_gshared (U3CReportU3Ec__AnonStorey58_t3448642837 * __this, const MethodInfo* method)
{
	{
		ScheduledNotifier_1_t2357123231 * L_0 = (ScheduledNotifier_1_t2357123231 *)__this->get_U3CU3Ef__this_1();
		NullCheck(L_0);
		Subject_1_t2775141040 * L_1 = (Subject_1_t2775141040 *)L_0->get_trigger_1();
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_value_0();
		NullCheck((Subject_1_t2775141040 *)L_1);
		((  void (*) (Subject_1_t2775141040 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Subject_1_t2775141040 *)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.ScheduledNotifier`1/<Report>c__AnonStorey58<System.Single>::.ctor()
extern "C"  void U3CReportU3Ec__AnonStorey58__ctor_m2014622677_gshared (U3CReportU3Ec__AnonStorey58_t3569745438 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.ScheduledNotifier`1/<Report>c__AnonStorey58<System.Single>::<>m__6C()
extern "C"  void U3CReportU3Ec__AnonStorey58_U3CU3Em__6C_m1330349291_gshared (U3CReportU3Ec__AnonStorey58_t3569745438 * __this, const MethodInfo* method)
{
	{
		ScheduledNotifier_1_t2478225832 * L_0 = (ScheduledNotifier_1_t2478225832 *)__this->get_U3CU3Ef__this_1();
		NullCheck(L_0);
		Subject_1_t2896243641 * L_1 = (Subject_1_t2896243641 *)L_0->get_trigger_1();
		float L_2 = (float)__this->get_value_0();
		NullCheck((Subject_1_t2896243641 *)L_1);
		((  void (*) (Subject_1_t2896243641 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Subject_1_t2896243641 *)L_1, (float)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.ScheduledNotifier`1/<Report>c__AnonStorey59<System.Object>::.ctor()
extern "C"  void U3CReportU3Ec__AnonStorey59__ctor_m279451405_gshared (U3CReportU3Ec__AnonStorey59_t820249710 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.ScheduledNotifier`1/<Report>c__AnonStorey59<System.Object>::<>m__6D()
extern "C"  void U3CReportU3Ec__AnonStorey59_U3CU3Em__6D_m278068708_gshared (U3CReportU3Ec__AnonStorey59_t820249710 * __this, const MethodInfo* method)
{
	{
		ScheduledNotifier_1_t2357123231 * L_0 = (ScheduledNotifier_1_t2357123231 *)__this->get_U3CU3Ef__this_1();
		NullCheck(L_0);
		Subject_1_t2775141040 * L_1 = (Subject_1_t2775141040 *)L_0->get_trigger_1();
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_value_0();
		NullCheck((Subject_1_t2775141040 *)L_1);
		((  void (*) (Subject_1_t2775141040 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Subject_1_t2775141040 *)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.ScheduledNotifier`1/<Report>c__AnonStorey59<System.Single>::.ctor()
extern "C"  void U3CReportU3Ec__AnonStorey59__ctor_m4232380630_gshared (U3CReportU3Ec__AnonStorey59_t941352311 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.ScheduledNotifier`1/<Report>c__AnonStorey59<System.Single>::<>m__6D()
extern "C"  void U3CReportU3Ec__AnonStorey59_U3CU3Em__6D_m2291964269_gshared (U3CReportU3Ec__AnonStorey59_t941352311 * __this, const MethodInfo* method)
{
	{
		ScheduledNotifier_1_t2478225832 * L_0 = (ScheduledNotifier_1_t2478225832 *)__this->get_U3CU3Ef__this_1();
		NullCheck(L_0);
		Subject_1_t2896243641 * L_1 = (Subject_1_t2896243641 *)L_0->get_trigger_1();
		float L_2 = (float)__this->get_value_0();
		NullCheck((Subject_1_t2896243641 *)L_1);
		((  void (*) (Subject_1_t2896243641 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Subject_1_t2896243641 *)L_1, (float)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.ScheduledNotifier`1/<Report>c__AnonStorey5A<System.Object>::.ctor()
extern "C"  void U3CReportU3Ec__AnonStorey5A__ctor_m841645845_gshared (U3CReportU3Ec__AnonStorey5A_t1267941174 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.ScheduledNotifier`1/<Report>c__AnonStorey5A<System.Object>::<>m__6E()
extern "C"  void U3CReportU3Ec__AnonStorey5A_U3CU3Em__6E_m3676014509_gshared (U3CReportU3Ec__AnonStorey5A_t1267941174 * __this, const MethodInfo* method)
{
	{
		ScheduledNotifier_1_t2357123231 * L_0 = (ScheduledNotifier_1_t2357123231 *)__this->get_U3CU3Ef__this_1();
		NullCheck(L_0);
		Subject_1_t2775141040 * L_1 = (Subject_1_t2775141040 *)L_0->get_trigger_1();
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_value_0();
		NullCheck((Subject_1_t2775141040 *)L_1);
		((  void (*) (Subject_1_t2775141040 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Subject_1_t2775141040 *)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.ScheduledNotifier`1/<Report>c__AnonStorey5A<System.Single>::.ctor()
extern "C"  void U3CReportU3Ec__AnonStorey5A__ctor_m499607774_gshared (U3CReportU3Ec__AnonStorey5A_t1389043775 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.ScheduledNotifier`1/<Report>c__AnonStorey5A<System.Single>::<>m__6E()
extern "C"  void U3CReportU3Ec__AnonStorey5A_U3CU3Em__6E_m1394942774_gshared (U3CReportU3Ec__AnonStorey5A_t1389043775 * __this, const MethodInfo* method)
{
	{
		ScheduledNotifier_1_t2478225832 * L_0 = (ScheduledNotifier_1_t2478225832 *)__this->get_U3CU3Ef__this_1();
		NullCheck(L_0);
		Subject_1_t2896243641 * L_1 = (Subject_1_t2896243641 *)L_0->get_trigger_1();
		float L_2 = (float)__this->get_value_0();
		NullCheck((Subject_1_t2896243641 *)L_1);
		((  void (*) (Subject_1_t2896243641 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Subject_1_t2896243641 *)L_1, (float)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void UniRx.ScheduledNotifier`1<System.Object>::.ctor()
extern "C"  void ScheduledNotifier_1__ctor_m526978847_gshared (ScheduledNotifier_1_t2357123231 * __this, const MethodInfo* method)
{
	{
		Subject_1_t2775141040 * L_0 = (Subject_1_t2775141040 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (Subject_1_t2775141040 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->set_trigger_1(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_1 = DefaultSchedulers_get_ConstantTimeOperations_m1411475237(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_scheduler_0(L_1);
		return;
	}
}
// System.Void UniRx.ScheduledNotifier`1<System.Object>::.ctor(UniRx.IScheduler)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4134256827;
extern const uint32_t ScheduledNotifier_1__ctor_m3595667509_MetadataUsageId;
extern "C"  void ScheduledNotifier_1__ctor_m3595667509_gshared (ScheduledNotifier_1_t2357123231 * __this, Il2CppObject * ___scheduler0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScheduledNotifier_1__ctor_m3595667509_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t2775141040 * L_0 = (Subject_1_t2775141040 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (Subject_1_t2775141040 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->set_trigger_1(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___scheduler0;
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_2 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_2, (String_t*)_stringLiteral4134256827, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0022:
	{
		Il2CppObject * L_3 = ___scheduler0;
		__this->set_scheduler_0(L_3);
		return;
	}
}
// System.Void UniRx.ScheduledNotifier`1<System.Object>::Report(T)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t ScheduledNotifier_1_Report_m3209518853_MetadataUsageId;
extern "C"  void ScheduledNotifier_1_Report_m3209518853_gshared (ScheduledNotifier_1_t2357123231 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScheduledNotifier_1_Report_m3209518853_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CReportU3Ec__AnonStorey58_t3448642837 * V_0 = NULL;
	{
		U3CReportU3Ec__AnonStorey58_t3448642837 * L_0 = (U3CReportU3Ec__AnonStorey58_t3448642837 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (U3CReportU3Ec__AnonStorey58_t3448642837 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (U3CReportU3Ec__AnonStorey58_t3448642837 *)L_0;
		U3CReportU3Ec__AnonStorey58_t3448642837 * L_1 = V_0;
		Il2CppObject * L_2 = ___value0;
		NullCheck(L_1);
		L_1->set_value_0(L_2);
		U3CReportU3Ec__AnonStorey58_t3448642837 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_1(__this);
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_scheduler_0();
		U3CReportU3Ec__AnonStorey58_t3448642837 * L_5 = V_0;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Action_t437523947 * L_7 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_7, (Il2CppObject *)L_5, (IntPtr_t)L_6, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_4);
		InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_4, (Action_t437523947 *)L_7);
		return;
	}
}
// System.IDisposable UniRx.ScheduledNotifier`1<System.Object>::Report(T,System.TimeSpan)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t ScheduledNotifier_1_Report_m1320187968_MetadataUsageId;
extern "C"  Il2CppObject * ScheduledNotifier_1_Report_m1320187968_gshared (ScheduledNotifier_1_t2357123231 * __this, Il2CppObject * ___value0, TimeSpan_t763862892  ___dueTime1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScheduledNotifier_1_Report_m1320187968_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	U3CReportU3Ec__AnonStorey59_t820249710 * V_1 = NULL;
	{
		U3CReportU3Ec__AnonStorey59_t820249710 * L_0 = (U3CReportU3Ec__AnonStorey59_t820249710 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (U3CReportU3Ec__AnonStorey59_t820249710 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		V_1 = (U3CReportU3Ec__AnonStorey59_t820249710 *)L_0;
		U3CReportU3Ec__AnonStorey59_t820249710 * L_1 = V_1;
		Il2CppObject * L_2 = ___value0;
		NullCheck(L_1);
		L_1->set_value_0(L_2);
		U3CReportU3Ec__AnonStorey59_t820249710 * L_3 = V_1;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_1(__this);
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_scheduler_0();
		TimeSpan_t763862892  L_5 = ___dueTime1;
		U3CReportU3Ec__AnonStorey59_t820249710 * L_6 = V_1;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		Action_t437523947 * L_8 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_8, (Il2CppObject *)L_6, (IntPtr_t)L_7, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_4);
		Il2CppObject * L_9 = InterfaceFuncInvoker2< Il2CppObject *, TimeSpan_t763862892 , Action_t437523947 * >::Invoke(2 /* System.IDisposable UniRx.IScheduler::Schedule(System.TimeSpan,System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_4, (TimeSpan_t763862892 )L_5, (Action_t437523947 *)L_8);
		V_0 = (Il2CppObject *)L_9;
		Il2CppObject * L_10 = V_0;
		return L_10;
	}
}
// System.IDisposable UniRx.ScheduledNotifier`1<System.Object>::Report(T,System.DateTimeOffset)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern const uint32_t ScheduledNotifier_1_Report_m1244727369_MetadataUsageId;
extern "C"  Il2CppObject * ScheduledNotifier_1_Report_m1244727369_gshared (ScheduledNotifier_1_t2357123231 * __this, Il2CppObject * ___value0, DateTimeOffset_t3712260035  ___dueTime1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScheduledNotifier_1_Report_m1244727369_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	U3CReportU3Ec__AnonStorey5A_t1267941174 * V_1 = NULL;
	{
		U3CReportU3Ec__AnonStorey5A_t1267941174 * L_0 = (U3CReportU3Ec__AnonStorey5A_t1267941174 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		((  void (*) (U3CReportU3Ec__AnonStorey5A_t1267941174 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		V_1 = (U3CReportU3Ec__AnonStorey5A_t1267941174 *)L_0;
		U3CReportU3Ec__AnonStorey5A_t1267941174 * L_1 = V_1;
		Il2CppObject * L_2 = ___value0;
		NullCheck(L_1);
		L_1->set_value_0(L_2);
		U3CReportU3Ec__AnonStorey5A_t1267941174 * L_3 = V_1;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_1(__this);
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_scheduler_0();
		DateTimeOffset_t3712260035  L_5 = ___dueTime1;
		U3CReportU3Ec__AnonStorey5A_t1267941174 * L_6 = V_1;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		Action_t437523947 * L_8 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_8, (Il2CppObject *)L_6, (IntPtr_t)L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_9 = Scheduler_Schedule_m96532664(NULL /*static, unused*/, (Il2CppObject *)L_4, (DateTimeOffset_t3712260035 )L_5, (Action_t437523947 *)L_8, /*hidden argument*/NULL);
		V_0 = (Il2CppObject *)L_9;
		Il2CppObject * L_10 = V_0;
		return L_10;
	}
}
// System.IDisposable UniRx.ScheduledNotifier`1<System.Object>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral348607190;
extern const uint32_t ScheduledNotifier_1_Subscribe_m4082606558_MetadataUsageId;
extern "C"  Il2CppObject * ScheduledNotifier_1_Subscribe_m4082606558_gshared (ScheduledNotifier_1_t2357123231 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScheduledNotifier_1_Subscribe_m4082606558_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___observer0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral348607190, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Subject_1_t2775141040 * L_2 = (Subject_1_t2775141040 *)__this->get_trigger_1();
		Il2CppObject* L_3 = ___observer0;
		NullCheck((Subject_1_t2775141040 *)L_2);
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (Subject_1_t2775141040 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)((Subject_1_t2775141040 *)L_2, (Il2CppObject*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		return L_4;
	}
}
// System.Void UniRx.ScheduledNotifier`1<System.Single>::.ctor()
extern "C"  void ScheduledNotifier_1__ctor_m184940776_gshared (ScheduledNotifier_1_t2478225832 * __this, const MethodInfo* method)
{
	{
		Subject_1_t2896243641 * L_0 = (Subject_1_t2896243641 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (Subject_1_t2896243641 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->set_trigger_1(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_1 = DefaultSchedulers_get_ConstantTimeOperations_m1411475237(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_scheduler_0(L_1);
		return;
	}
}
// System.Void UniRx.ScheduledNotifier`1<System.Single>::.ctor(UniRx.IScheduler)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4134256827;
extern const uint32_t ScheduledNotifier_1__ctor_m4200316926_MetadataUsageId;
extern "C"  void ScheduledNotifier_1__ctor_m4200316926_gshared (ScheduledNotifier_1_t2478225832 * __this, Il2CppObject * ___scheduler0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScheduledNotifier_1__ctor_m4200316926_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Subject_1_t2896243641 * L_0 = (Subject_1_t2896243641 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (Subject_1_t2896243641 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		__this->set_trigger_1(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___scheduler0;
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_2 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_2, (String_t*)_stringLiteral4134256827, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0022:
	{
		Il2CppObject * L_3 = ___scheduler0;
		__this->set_scheduler_0(L_3);
		return;
	}
}
// System.Void UniRx.ScheduledNotifier`1<System.Single>::Report(T)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t ScheduledNotifier_1_Report_m928447118_MetadataUsageId;
extern "C"  void ScheduledNotifier_1_Report_m928447118_gshared (ScheduledNotifier_1_t2478225832 * __this, float ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScheduledNotifier_1_Report_m928447118_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CReportU3Ec__AnonStorey58_t3569745438 * V_0 = NULL;
	{
		U3CReportU3Ec__AnonStorey58_t3569745438 * L_0 = (U3CReportU3Ec__AnonStorey58_t3569745438 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (U3CReportU3Ec__AnonStorey58_t3569745438 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (U3CReportU3Ec__AnonStorey58_t3569745438 *)L_0;
		U3CReportU3Ec__AnonStorey58_t3569745438 * L_1 = V_0;
		float L_2 = ___value0;
		NullCheck(L_1);
		L_1->set_value_0(L_2);
		U3CReportU3Ec__AnonStorey58_t3569745438 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_1(__this);
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_scheduler_0();
		U3CReportU3Ec__AnonStorey58_t3569745438 * L_5 = V_0;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Action_t437523947 * L_7 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_7, (Il2CppObject *)L_5, (IntPtr_t)L_6, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_4);
		InterfaceFuncInvoker1< Il2CppObject *, Action_t437523947 * >::Invoke(1 /* System.IDisposable UniRx.IScheduler::Schedule(System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_4, (Action_t437523947 *)L_7);
		return;
	}
}
// System.IDisposable UniRx.ScheduledNotifier`1<System.Single>::Report(T,System.TimeSpan)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* IScheduler_t2938318244_il2cpp_TypeInfo_var;
extern const uint32_t ScheduledNotifier_1_Report_m2567692745_MetadataUsageId;
extern "C"  Il2CppObject * ScheduledNotifier_1_Report_m2567692745_gshared (ScheduledNotifier_1_t2478225832 * __this, float ___value0, TimeSpan_t763862892  ___dueTime1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScheduledNotifier_1_Report_m2567692745_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	U3CReportU3Ec__AnonStorey59_t941352311 * V_1 = NULL;
	{
		U3CReportU3Ec__AnonStorey59_t941352311 * L_0 = (U3CReportU3Ec__AnonStorey59_t941352311 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (U3CReportU3Ec__AnonStorey59_t941352311 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		V_1 = (U3CReportU3Ec__AnonStorey59_t941352311 *)L_0;
		U3CReportU3Ec__AnonStorey59_t941352311 * L_1 = V_1;
		float L_2 = ___value0;
		NullCheck(L_1);
		L_1->set_value_0(L_2);
		U3CReportU3Ec__AnonStorey59_t941352311 * L_3 = V_1;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_1(__this);
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_scheduler_0();
		TimeSpan_t763862892  L_5 = ___dueTime1;
		U3CReportU3Ec__AnonStorey59_t941352311 * L_6 = V_1;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		Action_t437523947 * L_8 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_8, (Il2CppObject *)L_6, (IntPtr_t)L_7, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_4);
		Il2CppObject * L_9 = InterfaceFuncInvoker2< Il2CppObject *, TimeSpan_t763862892 , Action_t437523947 * >::Invoke(2 /* System.IDisposable UniRx.IScheduler::Schedule(System.TimeSpan,System.Action) */, IScheduler_t2938318244_il2cpp_TypeInfo_var, (Il2CppObject *)L_4, (TimeSpan_t763862892 )L_5, (Action_t437523947 *)L_8);
		V_0 = (Il2CppObject *)L_9;
		Il2CppObject * L_10 = V_0;
		return L_10;
	}
}
// System.IDisposable UniRx.ScheduledNotifier`1<System.Single>::Report(T,System.DateTimeOffset)
extern Il2CppClass* Action_t437523947_il2cpp_TypeInfo_var;
extern Il2CppClass* Scheduler_t103934925_il2cpp_TypeInfo_var;
extern const uint32_t ScheduledNotifier_1_Report_m3788465938_MetadataUsageId;
extern "C"  Il2CppObject * ScheduledNotifier_1_Report_m3788465938_gshared (ScheduledNotifier_1_t2478225832 * __this, float ___value0, DateTimeOffset_t3712260035  ___dueTime1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScheduledNotifier_1_Report_m3788465938_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	U3CReportU3Ec__AnonStorey5A_t1389043775 * V_1 = NULL;
	{
		U3CReportU3Ec__AnonStorey5A_t1389043775 * L_0 = (U3CReportU3Ec__AnonStorey5A_t1389043775 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		((  void (*) (U3CReportU3Ec__AnonStorey5A_t1389043775 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		V_1 = (U3CReportU3Ec__AnonStorey5A_t1389043775 *)L_0;
		U3CReportU3Ec__AnonStorey5A_t1389043775 * L_1 = V_1;
		float L_2 = ___value0;
		NullCheck(L_1);
		L_1->set_value_0(L_2);
		U3CReportU3Ec__AnonStorey5A_t1389043775 * L_3 = V_1;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_1(__this);
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_scheduler_0();
		DateTimeOffset_t3712260035  L_5 = ___dueTime1;
		U3CReportU3Ec__AnonStorey5A_t1389043775 * L_6 = V_1;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		Action_t437523947 * L_8 = (Action_t437523947 *)il2cpp_codegen_object_new(Action_t437523947_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_8, (Il2CppObject *)L_6, (IntPtr_t)L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Scheduler_t103934925_il2cpp_TypeInfo_var);
		Il2CppObject * L_9 = Scheduler_Schedule_m96532664(NULL /*static, unused*/, (Il2CppObject *)L_4, (DateTimeOffset_t3712260035 )L_5, (Action_t437523947 *)L_8, /*hidden argument*/NULL);
		V_0 = (Il2CppObject *)L_9;
		Il2CppObject * L_10 = V_0;
		return L_10;
	}
}
// System.IDisposable UniRx.ScheduledNotifier`1<System.Single>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral348607190;
extern const uint32_t ScheduledNotifier_1_Subscribe_m2331377831_MetadataUsageId;
extern "C"  Il2CppObject * ScheduledNotifier_1_Subscribe_m2331377831_gshared (ScheduledNotifier_1_t2478225832 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScheduledNotifier_1_Subscribe_m2331377831_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___observer0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral348607190, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Subject_1_t2896243641 * L_2 = (Subject_1_t2896243641 *)__this->get_trigger_1();
		Il2CppObject* L_3 = ___observer0;
		NullCheck((Subject_1_t2896243641 *)L_2);
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (Subject_1_t2896243641 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)((Subject_1_t2896243641 *)L_2, (Il2CppObject*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		return L_4;
	}
}
// System.Void UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler/QueuedAction`1<System.Object>::.cctor()
extern Il2CppClass* Action_1_t985559125_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1__ctor_m1498188969_MethodInfo_var;
extern const uint32_t QueuedAction_1__cctor_m1598591082_MetadataUsageId;
extern "C"  void QueuedAction_1__cctor_m1598591082_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (QueuedAction_1__cctor_m1598591082_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Action_1_t985559125 * L_1 = (Action_1_t985559125 *)il2cpp_codegen_object_new(Action_1_t985559125_il2cpp_TypeInfo_var);
		Action_1__ctor_m1498188969(L_1, (Il2CppObject *)NULL, (IntPtr_t)L_0, /*hidden argument*/Action_1__ctor_m1498188969_MethodInfo_var);
		((QueuedAction_1_t1329991844_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->set_Instance_0(L_1);
		return;
	}
}
// System.Void UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler/QueuedAction`1<System.Object>::Invoke(System.Object)
extern Il2CppClass* ICancelable_t4109686575_il2cpp_TypeInfo_var;
extern const uint32_t QueuedAction_1_Invoke_m3731350139_MetadataUsageId;
extern "C"  void QueuedAction_1_Invoke_m3731350139_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___state0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (QueuedAction_1_Invoke_m3731350139_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Tuple_3_t37583746  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Il2CppObject * L_0 = ___state0;
		V_0 = (Tuple_3_t37583746 )((*(Tuple_3_t37583746 *)((Tuple_3_t37583746 *)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)))));
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (Tuple_3_t37583746 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Tuple_3_t37583746 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((Il2CppObject *)L_1);
		bool L_2 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean UniRx.ICancelable::get_IsDisposed() */, ICancelable_t4109686575_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
		if (L_2)
		{
			goto IL_002b;
		}
	}
	{
		Action_1_t985559125 * L_3 = ((  Action_1_t985559125 * (*) (Tuple_3_t37583746 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Tuple_3_t37583746 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (Tuple_3_t37583746 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Tuple_3_t37583746 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((Action_1_t985559125 *)L_3);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Action_1_t985559125 *)L_3, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
	}

IL_002b:
	{
		return;
	}
}
// System.Void UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler/QueuedAction`1<UniRx.Unit>::.cctor()
extern Il2CppClass* Action_1_t985559125_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1__ctor_m1498188969_MethodInfo_var;
extern const uint32_t QueuedAction_1__cctor_m345052844_MetadataUsageId;
extern "C"  void QueuedAction_1__cctor_m345052844_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (QueuedAction_1__cctor_m345052844_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Action_1_t985559125 * L_1 = (Action_1_t985559125 *)il2cpp_codegen_object_new(Action_1_t985559125_il2cpp_TypeInfo_var);
		Action_1__ctor_m1498188969(L_1, (Il2CppObject *)NULL, (IntPtr_t)L_0, /*hidden argument*/Action_1__ctor_m1498188969_MethodInfo_var);
		((QueuedAction_1_t3051171462_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->set_Instance_0(L_1);
		return;
	}
}
// System.Void UniRx.Scheduler/IgnoreTimeScaleMainThreadScheduler/QueuedAction`1<UniRx.Unit>::Invoke(System.Object)
extern Il2CppClass* ICancelable_t4109686575_il2cpp_TypeInfo_var;
extern const uint32_t QueuedAction_1_Invoke_m1356273529_MetadataUsageId;
extern "C"  void QueuedAction_1_Invoke_m1356273529_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___state0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (QueuedAction_1_Invoke_m1356273529_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Tuple_3_t3015191994  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Il2CppObject * L_0 = ___state0;
		V_0 = (Tuple_3_t3015191994 )((*(Tuple_3_t3015191994 *)((Tuple_3_t3015191994 *)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)))));
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (Tuple_3_t3015191994 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Tuple_3_t3015191994 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((Il2CppObject *)L_1);
		bool L_2 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean UniRx.ICancelable::get_IsDisposed() */, ICancelable_t4109686575_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
		if (L_2)
		{
			goto IL_002b;
		}
	}
	{
		Action_1_t2706738743 * L_3 = ((  Action_1_t2706738743 * (*) (Tuple_3_t3015191994 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Tuple_3_t3015191994 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Unit_t2558286038  L_4 = ((  Unit_t2558286038  (*) (Tuple_3_t3015191994 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Tuple_3_t3015191994 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((Action_1_t2706738743 *)L_3);
		((  void (*) (Action_1_t2706738743 *, Unit_t2558286038 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Action_1_t2706738743 *)L_3, (Unit_t2558286038 )L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
	}

IL_002b:
	{
		return;
	}
}
// System.Void UniRx.Scheduler/MainThreadScheduler/QueuedAction`1<System.Object>::.cctor()
extern Il2CppClass* Action_1_t985559125_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1__ctor_m1498188969_MethodInfo_var;
extern const uint32_t QueuedAction_1__cctor_m1522584373_MetadataUsageId;
extern "C"  void QueuedAction_1__cctor_m1522584373_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (QueuedAction_1__cctor_m1522584373_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Action_1_t985559125 * L_1 = (Action_1_t985559125 *)il2cpp_codegen_object_new(Action_1_t985559125_il2cpp_TypeInfo_var);
		Action_1__ctor_m1498188969(L_1, (Il2CppObject *)NULL, (IntPtr_t)L_0, /*hidden argument*/Action_1__ctor_m1498188969_MethodInfo_var);
		((QueuedAction_1_t1329991843_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->set_Instance_0(L_1);
		return;
	}
}
// System.Void UniRx.Scheduler/MainThreadScheduler/QueuedAction`1<System.Object>::Invoke(System.Object)
extern Il2CppClass* ICancelable_t4109686575_il2cpp_TypeInfo_var;
extern const uint32_t QueuedAction_1_Invoke_m2692532624_MetadataUsageId;
extern "C"  void QueuedAction_1_Invoke_m2692532624_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___state0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (QueuedAction_1_Invoke_m2692532624_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Tuple_3_t37583746  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Il2CppObject * L_0 = ___state0;
		V_0 = (Tuple_3_t37583746 )((*(Tuple_3_t37583746 *)((Tuple_3_t37583746 *)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)))));
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (Tuple_3_t37583746 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Tuple_3_t37583746 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((Il2CppObject *)L_1);
		bool L_2 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean UniRx.ICancelable::get_IsDisposed() */, ICancelable_t4109686575_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
		if (L_2)
		{
			goto IL_002b;
		}
	}
	{
		Action_1_t985559125 * L_3 = ((  Action_1_t985559125 * (*) (Tuple_3_t37583746 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Tuple_3_t37583746 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (Tuple_3_t37583746 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Tuple_3_t37583746 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((Action_1_t985559125 *)L_3);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Action_1_t985559125 *)L_3, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
	}

IL_002b:
	{
		return;
	}
}
// System.Void UniRx.Scheduler/MainThreadScheduler/QueuedAction`1<UniRx.Unit>::.cctor()
extern Il2CppClass* Action_1_t985559125_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1__ctor_m1498188969_MethodInfo_var;
extern const uint32_t QueuedAction_1__cctor_m4097361857_MetadataUsageId;
extern "C"  void QueuedAction_1__cctor_m4097361857_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (QueuedAction_1__cctor_m4097361857_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Action_1_t985559125 * L_1 = (Action_1_t985559125 *)il2cpp_codegen_object_new(Action_1_t985559125_il2cpp_TypeInfo_var);
		Action_1__ctor_m1498188969(L_1, (Il2CppObject *)NULL, (IntPtr_t)L_0, /*hidden argument*/Action_1__ctor_m1498188969_MethodInfo_var);
		((QueuedAction_1_t3051171461_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->set_Instance_0(L_1);
		return;
	}
}
// System.Void UniRx.Scheduler/MainThreadScheduler/QueuedAction`1<UniRx.Unit>::Invoke(System.Object)
extern Il2CppClass* ICancelable_t4109686575_il2cpp_TypeInfo_var;
extern const uint32_t QueuedAction_1_Invoke_m942182532_MetadataUsageId;
extern "C"  void QueuedAction_1_Invoke_m942182532_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___state0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (QueuedAction_1_Invoke_m942182532_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Tuple_3_t3015191994  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Il2CppObject * L_0 = ___state0;
		V_0 = (Tuple_3_t3015191994 )((*(Tuple_3_t3015191994 *)((Tuple_3_t3015191994 *)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)))));
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (Tuple_3_t3015191994 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Tuple_3_t3015191994 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		NullCheck((Il2CppObject *)L_1);
		bool L_2 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean UniRx.ICancelable::get_IsDisposed() */, ICancelable_t4109686575_il2cpp_TypeInfo_var, (Il2CppObject *)L_1);
		if (L_2)
		{
			goto IL_002b;
		}
	}
	{
		Action_1_t2706738743 * L_3 = ((  Action_1_t2706738743 * (*) (Tuple_3_t3015191994 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Tuple_3_t3015191994 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Unit_t2558286038  L_4 = ((  Unit_t2558286038  (*) (Tuple_3_t3015191994 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Tuple_3_t3015191994 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((Action_1_t2706738743 *)L_3);
		((  void (*) (Action_1_t2706738743 *, Unit_t2558286038 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Action_1_t2706738743 *)L_3, (Unit_t2558286038 )L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
	}

IL_002b:
	{
		return;
	}
}
// System.Void UniRx.Scheduler/ThreadPoolScheduler/<ScheduleQueueing>c__AnonStorey7A`1<System.Object>::.ctor()
extern "C"  void U3CScheduleQueueingU3Ec__AnonStorey7A_1__ctor_m1683445557_gshared (U3CScheduleQueueingU3Ec__AnonStorey7A_1_t1746579521 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Scheduler/ThreadPoolScheduler/<ScheduleQueueing>c__AnonStorey7A`1<System.Object>::<>m__A1(System.Object)
extern Il2CppClass* ICancelable_t4109686575_il2cpp_TypeInfo_var;
extern const uint32_t U3CScheduleQueueingU3Ec__AnonStorey7A_1_U3CU3Em__A1_m2453861254_MetadataUsageId;
extern "C"  void U3CScheduleQueueingU3Ec__AnonStorey7A_1_U3CU3Em__A1_m2453861254_gshared (U3CScheduleQueueingU3Ec__AnonStorey7A_1_t1746579521 * __this, Il2CppObject * ___callBackState0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CScheduleQueueingU3Ec__AnonStorey7A_1_U3CU3Em__A1_m2453861254_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_cancel_0();
		NullCheck((Il2CppObject *)L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean UniRx.ICancelable::get_IsDisposed() */, ICancelable_t4109686575_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		if (L_1)
		{
			goto IL_0021;
		}
	}
	{
		Action_1_t985559125 * L_2 = (Action_1_t985559125 *)__this->get_action_1();
		Il2CppObject * L_3 = ___callBackState0;
		NullCheck((Action_1_t985559125 *)L_2);
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Action_1_t985559125 *)L_2, (Il2CppObject *)((Il2CppObject *)Castclass(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
	}

IL_0021:
	{
		return;
	}
}
// System.Void UniRx.Scheduler/ThreadPoolScheduler/<ScheduleQueueing>c__AnonStorey7A`1<UniRx.Unit>::.ctor()
extern "C"  void U3CScheduleQueueingU3Ec__AnonStorey7A_1__ctor_m91826895_gshared (U3CScheduleQueueingU3Ec__AnonStorey7A_1_t3467759139 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UniRx.Scheduler/ThreadPoolScheduler/<ScheduleQueueing>c__AnonStorey7A`1<UniRx.Unit>::<>m__A1(System.Object)
extern Il2CppClass* ICancelable_t4109686575_il2cpp_TypeInfo_var;
extern const uint32_t U3CScheduleQueueingU3Ec__AnonStorey7A_1_U3CU3Em__A1_m1453340844_MetadataUsageId;
extern "C"  void U3CScheduleQueueingU3Ec__AnonStorey7A_1_U3CU3Em__A1_m1453340844_gshared (U3CScheduleQueueingU3Ec__AnonStorey7A_1_t3467759139 * __this, Il2CppObject * ___callBackState0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CScheduleQueueingU3Ec__AnonStorey7A_1_U3CU3Em__A1_m1453340844_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_cancel_0();
		NullCheck((Il2CppObject *)L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean UniRx.ICancelable::get_IsDisposed() */, ICancelable_t4109686575_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		if (L_1)
		{
			goto IL_0021;
		}
	}
	{
		Action_1_t2706738743 * L_2 = (Action_1_t2706738743 *)__this->get_action_1();
		Il2CppObject * L_3 = ___callBackState0;
		NullCheck((Action_1_t2706738743 *)L_2);
		((  void (*) (Action_1_t2706738743 *, Unit_t2558286038 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Action_1_t2706738743 *)L_2, (Unit_t2558286038 )((*(Unit_t2558286038 *)((Unit_t2558286038 *)UnBox (L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
	}

IL_0021:
	{
		return;
	}
}
// System.Void UniRx.Stubs`1<System.Boolean>::.cctor()
extern "C"  void Stubs_1__cctor_m1340283405_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Action_1_t359458046 * L_0 = ((Stubs_1_t1554195722_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache2_2();
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Action_1_t359458046 * L_2 = (Action_1_t359458046 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Action_1_t359458046 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Il2CppObject *)NULL, (IntPtr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((Stubs_1_t1554195722_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_U3CU3Ef__amU24cache2_2(L_2);
	}

IL_0018:
	{
		Action_1_t359458046 * L_3 = ((Stubs_1_t1554195722_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache2_2();
		((Stubs_1_t1554195722_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_Ignore_0(L_3);
		Func_2_t1008118516 * L_4 = ((Stubs_1_t1554195722_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache3_3();
		if (L_4)
		{
			goto IL_003a;
		}
	}
	{
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Func_2_t1008118516 * L_6 = (Func_2_t1008118516 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (Func_2_t1008118516 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_6, (Il2CppObject *)NULL, (IntPtr_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((Stubs_1_t1554195722_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_U3CU3Ef__amU24cache3_3(L_6);
	}

IL_003a:
	{
		Func_2_t1008118516 * L_7 = ((Stubs_1_t1554195722_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache3_3();
		((Stubs_1_t1554195722_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_Identity_1(L_7);
		return;
	}
}
// System.Void UniRx.Stubs`1<System.Boolean>::<Ignore>m__71(T)
extern "C"  void Stubs_1_U3CIgnoreU3Em__71_m1046516233_gshared (Il2CppObject * __this /* static, unused */, bool ___t0, const MethodInfo* method)
{
	{
		return;
	}
}
// T UniRx.Stubs`1<System.Boolean>::<Identity>m__72(T)
extern "C"  bool Stubs_1_U3CIdentityU3Em__72_m3768037021_gshared (Il2CppObject * __this /* static, unused */, bool ___t0, const MethodInfo* method)
{
	{
		bool L_0 = ___t0;
		return L_0;
	}
}
// System.Void UniRx.Stubs`1<System.Double>::.cctor()
extern "C"  void Stubs_1__cctor_m474768202_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Action_1_t682969319 * L_0 = ((Stubs_1_t1877706995_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache2_2();
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Action_1_t682969319 * L_2 = (Action_1_t682969319 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Action_1_t682969319 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Il2CppObject *)NULL, (IntPtr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((Stubs_1_t1877706995_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_U3CU3Ef__amU24cache2_2(L_2);
	}

IL_0018:
	{
		Action_1_t682969319 * L_3 = ((Stubs_1_t1877706995_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache2_2();
		((Stubs_1_t1877706995_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_Ignore_0(L_3);
		Func_2_t2869950768 * L_4 = ((Stubs_1_t1877706995_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache3_3();
		if (L_4)
		{
			goto IL_003a;
		}
	}
	{
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Func_2_t2869950768 * L_6 = (Func_2_t2869950768 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (Func_2_t2869950768 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_6, (Il2CppObject *)NULL, (IntPtr_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((Stubs_1_t1877706995_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_U3CU3Ef__amU24cache3_3(L_6);
	}

IL_003a:
	{
		Func_2_t2869950768 * L_7 = ((Stubs_1_t1877706995_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache3_3();
		((Stubs_1_t1877706995_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_Identity_1(L_7);
		return;
	}
}
// System.Void UniRx.Stubs`1<System.Double>::<Ignore>m__71(T)
extern "C"  void Stubs_1_U3CIgnoreU3Em__71_m1774145094_gshared (Il2CppObject * __this /* static, unused */, double ___t0, const MethodInfo* method)
{
	{
		return;
	}
}
// T UniRx.Stubs`1<System.Double>::<Identity>m__72(T)
extern "C"  double Stubs_1_U3CIdentityU3Em__72_m91553976_gshared (Il2CppObject * __this /* static, unused */, double ___t0, const MethodInfo* method)
{
	{
		double L_0 = ___t0;
		return L_0;
	}
}
// System.Void UniRx.Stubs`1<System.Int64>::.cctor()
extern "C"  void Stubs_1__cctor_m1472561512_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Action_1_t2995867587 * L_0 = ((Stubs_1_t4190605263_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache2_2();
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Action_1_t2995867587 * L_2 = (Action_1_t2995867587 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Action_1_t2995867587 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Il2CppObject *)NULL, (IntPtr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((Stubs_1_t4190605263_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_U3CU3Ef__amU24cache2_2(L_2);
	}

IL_0018:
	{
		Action_1_t2995867587 * L_3 = ((Stubs_1_t4190605263_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache2_2();
		((Stubs_1_t4190605263_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_Ignore_0(L_3);
		Func_2_t592778816 * L_4 = ((Stubs_1_t4190605263_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache3_3();
		if (L_4)
		{
			goto IL_003a;
		}
	}
	{
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Func_2_t592778816 * L_6 = (Func_2_t592778816 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (Func_2_t592778816 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_6, (Il2CppObject *)NULL, (IntPtr_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((Stubs_1_t4190605263_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_U3CU3Ef__amU24cache3_3(L_6);
	}

IL_003a:
	{
		Func_2_t592778816 * L_7 = ((Stubs_1_t4190605263_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache3_3();
		((Stubs_1_t4190605263_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_Identity_1(L_7);
		return;
	}
}
// System.Void UniRx.Stubs`1<System.Int64>::<Ignore>m__71(T)
extern "C"  void Stubs_1_U3CIgnoreU3Em__71_m2168417892_gshared (Il2CppObject * __this /* static, unused */, int64_t ___t0, const MethodInfo* method)
{
	{
		return;
	}
}
// T UniRx.Stubs`1<System.Int64>::<Identity>m__72(T)
extern "C"  int64_t Stubs_1_U3CIdentityU3Em__72_m107431032_gshared (Il2CppObject * __this /* static, unused */, int64_t ___t0, const MethodInfo* method)
{
	{
		int64_t L_0 = ___t0;
		return L_0;
	}
}
// System.Void UniRx.Stubs`1<System.Object>::.cctor()
extern "C"  void Stubs_1__cctor_m4180273948_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Action_1_t985559125 * L_0 = ((Stubs_1_t2180296801_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache2_2();
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Action_1_t985559125 * L_2 = (Action_1_t985559125 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Action_1_t985559125 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Il2CppObject *)NULL, (IntPtr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((Stubs_1_t2180296801_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_U3CU3Ef__amU24cache2_2(L_2);
	}

IL_0018:
	{
		Action_1_t985559125 * L_3 = ((Stubs_1_t2180296801_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache2_2();
		((Stubs_1_t2180296801_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_Ignore_0(L_3);
		Func_2_t2135783352 * L_4 = ((Stubs_1_t2180296801_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache3_3();
		if (L_4)
		{
			goto IL_003a;
		}
	}
	{
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Func_2_t2135783352 * L_6 = (Func_2_t2135783352 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (Func_2_t2135783352 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_6, (Il2CppObject *)NULL, (IntPtr_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((Stubs_1_t2180296801_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_U3CU3Ef__amU24cache3_3(L_6);
	}

IL_003a:
	{
		Func_2_t2135783352 * L_7 = ((Stubs_1_t2180296801_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache3_3();
		((Stubs_1_t2180296801_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_Identity_1(L_7);
		return;
	}
}
// System.Void UniRx.Stubs`1<System.Object>::<Ignore>m__71(T)
extern "C"  void Stubs_1_U3CIgnoreU3Em__71_m3494437912_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___t0, const MethodInfo* method)
{
	{
		return;
	}
}
// T UniRx.Stubs`1<System.Object>::<Identity>m__72(T)
extern "C"  Il2CppObject * Stubs_1_U3CIdentityU3Em__72_m4025510410_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___t0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___t0;
		return L_0;
	}
}
// System.Void UniRx.Stubs`1<System.Single>::.cctor()
extern "C"  void Stubs_1__cctor_m2167028339_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Action_1_t1106661726 * L_0 = ((Stubs_1_t2301399402_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache2_2();
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Action_1_t1106661726 * L_2 = (Action_1_t1106661726 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Action_1_t1106661726 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Il2CppObject *)NULL, (IntPtr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((Stubs_1_t2301399402_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_U3CU3Ef__amU24cache2_2(L_2);
	}

IL_0018:
	{
		Action_1_t1106661726 * L_3 = ((Stubs_1_t2301399402_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache2_2();
		((Stubs_1_t2301399402_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_Ignore_0(L_3);
		Func_2_t3464793460 * L_4 = ((Stubs_1_t2301399402_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache3_3();
		if (L_4)
		{
			goto IL_003a;
		}
	}
	{
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Func_2_t3464793460 * L_6 = (Func_2_t3464793460 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (Func_2_t3464793460 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_6, (Il2CppObject *)NULL, (IntPtr_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((Stubs_1_t2301399402_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_U3CU3Ef__amU24cache3_3(L_6);
	}

IL_003a:
	{
		Func_2_t3464793460 * L_7 = ((Stubs_1_t2301399402_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache3_3();
		((Stubs_1_t2301399402_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_Identity_1(L_7);
		return;
	}
}
// System.Void UniRx.Stubs`1<System.Single>::<Ignore>m__71(T)
extern "C"  void Stubs_1_U3CIgnoreU3Em__71_m3654020207_gshared (Il2CppObject * __this /* static, unused */, float ___t0, const MethodInfo* method)
{
	{
		return;
	}
}
// T UniRx.Stubs`1<System.Single>::<Identity>m__72(T)
extern "C"  float Stubs_1_U3CIdentityU3Em__72_m2765273249_gshared (Il2CppObject * __this /* static, unused */, float ___t0, const MethodInfo* method)
{
	{
		float L_0 = ___t0;
		return L_0;
	}
}
// System.Void UniRx.Stubs`1<UniRx.Tuple`2<System.Object,System.Object>>::.cctor()
extern "C"  void Stubs_1__cctor_m3681425582_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Action_1_t517714524 * L_0 = ((Stubs_1_t1712452200_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache2_2();
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Action_1_t517714524 * L_2 = (Action_1_t517714524 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Action_1_t517714524 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Il2CppObject *)NULL, (IntPtr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((Stubs_1_t1712452200_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_U3CU3Ef__amU24cache2_2(L_2);
	}

IL_0018:
	{
		Action_1_t517714524 * L_3 = ((Stubs_1_t1712452200_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache2_2();
		((Stubs_1_t1712452200_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_Ignore_0(L_3);
		Func_2_t1259381692 * L_4 = ((Stubs_1_t1712452200_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache3_3();
		if (L_4)
		{
			goto IL_003a;
		}
	}
	{
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Func_2_t1259381692 * L_6 = (Func_2_t1259381692 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (Func_2_t1259381692 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_6, (Il2CppObject *)NULL, (IntPtr_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((Stubs_1_t1712452200_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_U3CU3Ef__amU24cache3_3(L_6);
	}

IL_003a:
	{
		Func_2_t1259381692 * L_7 = ((Stubs_1_t1712452200_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache3_3();
		((Stubs_1_t1712452200_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_Identity_1(L_7);
		return;
	}
}
// System.Void UniRx.Stubs`1<UniRx.Tuple`2<System.Object,System.Object>>::<Ignore>m__71(T)
extern "C"  void Stubs_1_U3CIgnoreU3Em__71_m3306643370_gshared (Il2CppObject * __this /* static, unused */, Tuple_2_t369261819  ___t0, const MethodInfo* method)
{
	{
		return;
	}
}
// T UniRx.Stubs`1<UniRx.Tuple`2<System.Object,System.Object>>::<Identity>m__72(T)
extern "C"  Tuple_2_t369261819  Stubs_1_U3CIdentityU3Em__72_m684371070_gshared (Il2CppObject * __this /* static, unused */, Tuple_2_t369261819  ___t0, const MethodInfo* method)
{
	{
		Tuple_2_t369261819  L_0 = ___t0;
		return L_0;
	}
}
// System.Void UniRx.Stubs`1<UniRx.Unit>::.cctor()
extern "C"  void Stubs_1__cctor_m2773682490_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Action_1_t2706738743 * L_0 = ((Stubs_1_t3901476419_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache2_2();
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Action_1_t2706738743 * L_2 = (Action_1_t2706738743 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Action_1_t2706738743 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Il2CppObject *)NULL, (IntPtr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((Stubs_1_t3901476419_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_U3CU3Ef__amU24cache2_2(L_2);
	}

IL_0018:
	{
		Action_1_t2706738743 * L_3 = ((Stubs_1_t3901476419_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache2_2();
		((Stubs_1_t3901476419_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_Ignore_0(L_3);
		Func_2_t818424304 * L_4 = ((Stubs_1_t3901476419_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache3_3();
		if (L_4)
		{
			goto IL_003a;
		}
	}
	{
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Func_2_t818424304 * L_6 = (Func_2_t818424304 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (Func_2_t818424304 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_6, (Il2CppObject *)NULL, (IntPtr_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((Stubs_1_t3901476419_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_U3CU3Ef__amU24cache3_3(L_6);
	}

IL_003a:
	{
		Func_2_t818424304 * L_7 = ((Stubs_1_t3901476419_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache3_3();
		((Stubs_1_t3901476419_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_Identity_1(L_7);
		return;
	}
}
// System.Void UniRx.Stubs`1<UniRx.Unit>::<Ignore>m__71(T)
extern "C"  void Stubs_1_U3CIgnoreU3Em__71_m2826503222_gshared (Il2CppObject * __this /* static, unused */, Unit_t2558286038  ___t0, const MethodInfo* method)
{
	{
		return;
	}
}
// T UniRx.Stubs`1<UniRx.Unit>::<Identity>m__72(T)
extern "C"  Unit_t2558286038  Stubs_1_U3CIdentityU3Em__72_m3683983882_gshared (Il2CppObject * __this /* static, unused */, Unit_t2558286038  ___t0, const MethodInfo* method)
{
	{
		Unit_t2558286038  L_0 = ___t0;
		return L_0;
	}
}
// System.Void UniRx.Stubs`1<UnityEngine.Color>::.cctor()
extern "C"  void Stubs_1__cctor_m2319247534_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Action_1_t1736628465 * L_0 = ((Stubs_1_t2931366141_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache2_2();
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Action_1_t1736628465 * L_2 = (Action_1_t1736628465 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (Action_1_t1736628465 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Il2CppObject *)NULL, (IntPtr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((Stubs_1_t2931366141_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_U3CU3Ef__amU24cache2_2(L_2);
	}

IL_0018:
	{
		Action_1_t1736628465 * L_3 = ((Stubs_1_t2931366141_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache2_2();
		((Stubs_1_t2931366141_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_Ignore_0(L_3);
		Func_2_t2057442760 * L_4 = ((Stubs_1_t2931366141_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache3_3();
		if (L_4)
		{
			goto IL_003a;
		}
	}
	{
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Func_2_t2057442760 * L_6 = (Func_2_t2057442760 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (Func_2_t2057442760 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(L_6, (Il2CppObject *)NULL, (IntPtr_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((Stubs_1_t2931366141_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_U3CU3Ef__amU24cache3_3(L_6);
	}

IL_003a:
	{
		Func_2_t2057442760 * L_7 = ((Stubs_1_t2931366141_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_U3CU3Ef__amU24cache3_3();
		((Stubs_1_t2931366141_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_Identity_1(L_7);
		return;
	}
}
// System.Void UniRx.Stubs`1<UnityEngine.Color>::<Ignore>m__71(T)
extern "C"  void Stubs_1_U3CIgnoreU3Em__71_m1612066730_gshared (Il2CppObject * __this /* static, unused */, Color_t1588175760  ___t0, const MethodInfo* method)
{
	{
		return;
	}
}
// T UniRx.Stubs`1<UnityEngine.Color>::<Identity>m__72(T)
extern "C"  Color_t1588175760  Stubs_1_U3CIdentityU3Em__72_m3211182492_gshared (Il2CppObject * __this /* static, unused */, Color_t1588175760  ___t0, const MethodInfo* method)
{
	{
		Color_t1588175760  L_0 = ___t0;
		return L_0;
	}
}
// System.Void UniRx.Subject`1/Subscription<System.Boolean>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Subscription__ctor_m1067686410_MetadataUsageId;
extern "C"  void Subscription__ctor_m1067686410_gshared (Subscription_t2285219159 * __this, Subject_1_t2149039961 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscription__ctor_m1067686410_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Subject_1_t2149039961 * L_1 = ___parent0;
		__this->set_parent_1(L_1);
		Il2CppObject* L_2 = ___unsubscribeTarget1;
		__this->set_unsubscribeTarget_2(L_2);
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<System.Boolean>::Dispose()
extern "C"  void Subscription_Dispose_m3196332_gshared (Subscription_t2285219159 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ListObserver_1_t505623012 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Subject_1_t2149039961 * L_2 = (Subject_1_t2149039961 *)__this->get_parent_1();
			if (!L_2)
			{
				goto IL_0087;
			}
		}

IL_0018:
		{
			Subject_1_t2149039961 * L_3 = (Subject_1_t2149039961 *)__this->get_parent_1();
			NullCheck(L_3);
			Il2CppObject * L_4 = (Il2CppObject *)L_3->get_observerLock_0();
			V_1 = (Il2CppObject *)L_4;
			Il2CppObject * L_5 = V_1;
			Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		}

IL_002a:
		try
		{ // begin try (depth: 2)
			{
				Subject_1_t2149039961 * L_6 = (Subject_1_t2149039961 *)__this->get_parent_1();
				NullCheck(L_6);
				Il2CppObject* L_7 = (Il2CppObject*)L_6->get_outObserver_4();
				V_2 = (ListObserver_1_t505623012 *)((ListObserver_1_t505623012 *)IsInst(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
				ListObserver_1_t505623012 * L_8 = V_2;
				if (!L_8)
				{
					goto IL_005d;
				}
			}

IL_0041:
			{
				Subject_1_t2149039961 * L_9 = (Subject_1_t2149039961 *)__this->get_parent_1();
				ListObserver_1_t505623012 * L_10 = V_2;
				Il2CppObject* L_11 = (Il2CppObject*)__this->get_unsubscribeTarget_2();
				NullCheck((ListObserver_1_t505623012 *)L_10);
				Il2CppObject* L_12 = ((  Il2CppObject* (*) (ListObserver_1_t505623012 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ListObserver_1_t505623012 *)L_10, (Il2CppObject*)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
				NullCheck(L_9);
				L_9->set_outObserver_4(L_12);
				goto IL_006d;
			}

IL_005d:
			{
				Subject_1_t2149039961 * L_13 = (Subject_1_t2149039961 *)__this->get_parent_1();
				IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
				EmptyObserver_1_t3915325627 * L_14 = ((EmptyObserver_1_t3915325627_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_Instance_0();
				NullCheck(L_13);
				L_13->set_outObserver_4(L_14);
			}

IL_006d:
			{
				__this->set_unsubscribeTarget_2((Il2CppObject*)NULL);
				__this->set_parent_1((Subject_1_t2149039961 *)NULL);
				IL2CPP_LEAVE(0x87, FINALLY_0080);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0080;
		}

FINALLY_0080:
		{ // begin finally (depth: 2)
			Il2CppObject * L_15 = V_1;
			Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_15, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(128)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(128)
		{
			IL2CPP_JUMP_TBL(0x87, IL_0087)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0087:
		{
			IL2CPP_LEAVE(0x93, FINALLY_008c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008c;
	}

FINALLY_008c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_16 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(140)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(140)
	{
		IL2CPP_JUMP_TBL(0x93, IL_0093)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0093:
	{
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<System.Byte>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Subscription__ctor_m1617373294_MetadataUsageId;
extern "C"  void Subscription__ctor_m1617373294_gshared (Subscription_t557940343 * __this, Subject_1_t421761145 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscription__ctor_m1617373294_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Subject_1_t421761145 * L_1 = ___parent0;
		__this->set_parent_1(L_1);
		Il2CppObject* L_2 = ___unsubscribeTarget1;
		__this->set_unsubscribeTarget_2(L_2);
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<System.Byte>::Dispose()
extern "C"  void Subscription_Dispose_m2238855368_gshared (Subscription_t557940343 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ListObserver_1_t3073311492 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Subject_1_t421761145 * L_2 = (Subject_1_t421761145 *)__this->get_parent_1();
			if (!L_2)
			{
				goto IL_0087;
			}
		}

IL_0018:
		{
			Subject_1_t421761145 * L_3 = (Subject_1_t421761145 *)__this->get_parent_1();
			NullCheck(L_3);
			Il2CppObject * L_4 = (Il2CppObject *)L_3->get_observerLock_0();
			V_1 = (Il2CppObject *)L_4;
			Il2CppObject * L_5 = V_1;
			Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		}

IL_002a:
		try
		{ // begin try (depth: 2)
			{
				Subject_1_t421761145 * L_6 = (Subject_1_t421761145 *)__this->get_parent_1();
				NullCheck(L_6);
				Il2CppObject* L_7 = (Il2CppObject*)L_6->get_outObserver_4();
				V_2 = (ListObserver_1_t3073311492 *)((ListObserver_1_t3073311492 *)IsInst(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
				ListObserver_1_t3073311492 * L_8 = V_2;
				if (!L_8)
				{
					goto IL_005d;
				}
			}

IL_0041:
			{
				Subject_1_t421761145 * L_9 = (Subject_1_t421761145 *)__this->get_parent_1();
				ListObserver_1_t3073311492 * L_10 = V_2;
				Il2CppObject* L_11 = (Il2CppObject*)__this->get_unsubscribeTarget_2();
				NullCheck((ListObserver_1_t3073311492 *)L_10);
				Il2CppObject* L_12 = ((  Il2CppObject* (*) (ListObserver_1_t3073311492 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ListObserver_1_t3073311492 *)L_10, (Il2CppObject*)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
				NullCheck(L_9);
				L_9->set_outObserver_4(L_12);
				goto IL_006d;
			}

IL_005d:
			{
				Subject_1_t421761145 * L_13 = (Subject_1_t421761145 *)__this->get_parent_1();
				IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
				EmptyObserver_1_t2188046811 * L_14 = ((EmptyObserver_1_t2188046811_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_Instance_0();
				NullCheck(L_13);
				L_13->set_outObserver_4(L_14);
			}

IL_006d:
			{
				__this->set_unsubscribeTarget_2((Il2CppObject*)NULL);
				__this->set_parent_1((Subject_1_t421761145 *)NULL);
				IL2CPP_LEAVE(0x87, FINALLY_0080);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0080;
		}

FINALLY_0080:
		{ // begin finally (depth: 2)
			Il2CppObject * L_15 = V_1;
			Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_15, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(128)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(128)
		{
			IL2CPP_JUMP_TBL(0x87, IL_0087)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0087:
		{
			IL2CPP_LEAVE(0x93, FINALLY_008c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008c;
	}

FINALLY_008c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_16 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(140)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(140)
	{
		IL2CPP_JUMP_TBL(0x93, IL_0093)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0093:
	{
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<System.Double>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Subscription__ctor_m3617506373_MetadataUsageId;
extern "C"  void Subscription__ctor_m3617506373_gshared (Subscription_t2608730432 * __this, Subject_1_t2472551234 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscription__ctor_m3617506373_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Subject_1_t2472551234 * L_1 = ___parent0;
		__this->set_parent_1(L_1);
		Il2CppObject* L_2 = ___unsubscribeTarget1;
		__this->set_unsubscribeTarget_2(L_2);
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<System.Double>::Dispose()
extern "C"  void Subscription_Dispose_m32618129_gshared (Subscription_t2608730432 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ListObserver_1_t829134285 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Subject_1_t2472551234 * L_2 = (Subject_1_t2472551234 *)__this->get_parent_1();
			if (!L_2)
			{
				goto IL_0087;
			}
		}

IL_0018:
		{
			Subject_1_t2472551234 * L_3 = (Subject_1_t2472551234 *)__this->get_parent_1();
			NullCheck(L_3);
			Il2CppObject * L_4 = (Il2CppObject *)L_3->get_observerLock_0();
			V_1 = (Il2CppObject *)L_4;
			Il2CppObject * L_5 = V_1;
			Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		}

IL_002a:
		try
		{ // begin try (depth: 2)
			{
				Subject_1_t2472551234 * L_6 = (Subject_1_t2472551234 *)__this->get_parent_1();
				NullCheck(L_6);
				Il2CppObject* L_7 = (Il2CppObject*)L_6->get_outObserver_4();
				V_2 = (ListObserver_1_t829134285 *)((ListObserver_1_t829134285 *)IsInst(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
				ListObserver_1_t829134285 * L_8 = V_2;
				if (!L_8)
				{
					goto IL_005d;
				}
			}

IL_0041:
			{
				Subject_1_t2472551234 * L_9 = (Subject_1_t2472551234 *)__this->get_parent_1();
				ListObserver_1_t829134285 * L_10 = V_2;
				Il2CppObject* L_11 = (Il2CppObject*)__this->get_unsubscribeTarget_2();
				NullCheck((ListObserver_1_t829134285 *)L_10);
				Il2CppObject* L_12 = ((  Il2CppObject* (*) (ListObserver_1_t829134285 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ListObserver_1_t829134285 *)L_10, (Il2CppObject*)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
				NullCheck(L_9);
				L_9->set_outObserver_4(L_12);
				goto IL_006d;
			}

IL_005d:
			{
				Subject_1_t2472551234 * L_13 = (Subject_1_t2472551234 *)__this->get_parent_1();
				IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
				EmptyObserver_1_t4238836900 * L_14 = ((EmptyObserver_1_t4238836900_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_Instance_0();
				NullCheck(L_13);
				L_13->set_outObserver_4(L_14);
			}

IL_006d:
			{
				__this->set_unsubscribeTarget_2((Il2CppObject*)NULL);
				__this->set_parent_1((Subject_1_t2472551234 *)NULL);
				IL2CPP_LEAVE(0x87, FINALLY_0080);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0080;
		}

FINALLY_0080:
		{ // begin finally (depth: 2)
			Il2CppObject * L_15 = V_1;
			Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_15, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(128)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(128)
		{
			IL2CPP_JUMP_TBL(0x87, IL_0087)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0087:
		{
			IL2CPP_LEAVE(0x93, FINALLY_008c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008c;
	}

FINALLY_008c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_16 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(140)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(140)
	{
		IL2CPP_JUMP_TBL(0x93, IL_0093)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0093:
	{
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<System.Int32>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Subscription__ctor_m169029604_MetadataUsageId;
extern "C"  void Subscription__ctor_m169029604_gshared (Subscription_t626661309 * __this, Subject_1_t490482111 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscription__ctor_m169029604_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Subject_1_t490482111 * L_1 = ___parent0;
		__this->set_parent_1(L_1);
		Il2CppObject* L_2 = ___unsubscribeTarget1;
		__this->set_unsubscribeTarget_2(L_2);
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<System.Int32>::Dispose()
extern "C"  void Subscription_Dispose_m405702546_gshared (Subscription_t626661309 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ListObserver_1_t3142032458 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Subject_1_t490482111 * L_2 = (Subject_1_t490482111 *)__this->get_parent_1();
			if (!L_2)
			{
				goto IL_0087;
			}
		}

IL_0018:
		{
			Subject_1_t490482111 * L_3 = (Subject_1_t490482111 *)__this->get_parent_1();
			NullCheck(L_3);
			Il2CppObject * L_4 = (Il2CppObject *)L_3->get_observerLock_0();
			V_1 = (Il2CppObject *)L_4;
			Il2CppObject * L_5 = V_1;
			Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		}

IL_002a:
		try
		{ // begin try (depth: 2)
			{
				Subject_1_t490482111 * L_6 = (Subject_1_t490482111 *)__this->get_parent_1();
				NullCheck(L_6);
				Il2CppObject* L_7 = (Il2CppObject*)L_6->get_outObserver_4();
				V_2 = (ListObserver_1_t3142032458 *)((ListObserver_1_t3142032458 *)IsInst(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
				ListObserver_1_t3142032458 * L_8 = V_2;
				if (!L_8)
				{
					goto IL_005d;
				}
			}

IL_0041:
			{
				Subject_1_t490482111 * L_9 = (Subject_1_t490482111 *)__this->get_parent_1();
				ListObserver_1_t3142032458 * L_10 = V_2;
				Il2CppObject* L_11 = (Il2CppObject*)__this->get_unsubscribeTarget_2();
				NullCheck((ListObserver_1_t3142032458 *)L_10);
				Il2CppObject* L_12 = ((  Il2CppObject* (*) (ListObserver_1_t3142032458 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ListObserver_1_t3142032458 *)L_10, (Il2CppObject*)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
				NullCheck(L_9);
				L_9->set_outObserver_4(L_12);
				goto IL_006d;
			}

IL_005d:
			{
				Subject_1_t490482111 * L_13 = (Subject_1_t490482111 *)__this->get_parent_1();
				IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
				EmptyObserver_1_t2256767777 * L_14 = ((EmptyObserver_1_t2256767777_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_Instance_0();
				NullCheck(L_13);
				L_13->set_outObserver_4(L_14);
			}

IL_006d:
			{
				__this->set_unsubscribeTarget_2((Il2CppObject*)NULL);
				__this->set_parent_1((Subject_1_t490482111 *)NULL);
				IL2CPP_LEAVE(0x87, FINALLY_0080);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0080;
		}

FINALLY_0080:
		{ // begin finally (depth: 2)
			Il2CppObject * L_15 = V_1;
			Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_15, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(128)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(128)
		{
			IL2CPP_JUMP_TBL(0x87, IL_0087)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0087:
		{
			IL2CPP_LEAVE(0x93, FINALLY_008c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008c;
	}

FINALLY_008c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_16 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(140)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(140)
	{
		IL2CPP_JUMP_TBL(0x93, IL_0093)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0093:
	{
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<System.Int64>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Subscription__ctor_m3087521125_MetadataUsageId;
extern "C"  void Subscription__ctor_m3087521125_gshared (Subscription_t626661404 * __this, Subject_1_t490482206 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscription__ctor_m3087521125_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Subject_1_t490482206 * L_1 = ___parent0;
		__this->set_parent_1(L_1);
		Il2CppObject* L_2 = ___unsubscribeTarget1;
		__this->set_unsubscribeTarget_2(L_2);
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<System.Int64>::Dispose()
extern "C"  void Subscription_Dispose_m2597187953_gshared (Subscription_t626661404 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ListObserver_1_t3142032553 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Subject_1_t490482206 * L_2 = (Subject_1_t490482206 *)__this->get_parent_1();
			if (!L_2)
			{
				goto IL_0087;
			}
		}

IL_0018:
		{
			Subject_1_t490482206 * L_3 = (Subject_1_t490482206 *)__this->get_parent_1();
			NullCheck(L_3);
			Il2CppObject * L_4 = (Il2CppObject *)L_3->get_observerLock_0();
			V_1 = (Il2CppObject *)L_4;
			Il2CppObject * L_5 = V_1;
			Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		}

IL_002a:
		try
		{ // begin try (depth: 2)
			{
				Subject_1_t490482206 * L_6 = (Subject_1_t490482206 *)__this->get_parent_1();
				NullCheck(L_6);
				Il2CppObject* L_7 = (Il2CppObject*)L_6->get_outObserver_4();
				V_2 = (ListObserver_1_t3142032553 *)((ListObserver_1_t3142032553 *)IsInst(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
				ListObserver_1_t3142032553 * L_8 = V_2;
				if (!L_8)
				{
					goto IL_005d;
				}
			}

IL_0041:
			{
				Subject_1_t490482206 * L_9 = (Subject_1_t490482206 *)__this->get_parent_1();
				ListObserver_1_t3142032553 * L_10 = V_2;
				Il2CppObject* L_11 = (Il2CppObject*)__this->get_unsubscribeTarget_2();
				NullCheck((ListObserver_1_t3142032553 *)L_10);
				Il2CppObject* L_12 = ((  Il2CppObject* (*) (ListObserver_1_t3142032553 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ListObserver_1_t3142032553 *)L_10, (Il2CppObject*)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
				NullCheck(L_9);
				L_9->set_outObserver_4(L_12);
				goto IL_006d;
			}

IL_005d:
			{
				Subject_1_t490482206 * L_13 = (Subject_1_t490482206 *)__this->get_parent_1();
				IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
				EmptyObserver_1_t2256767872 * L_14 = ((EmptyObserver_1_t2256767872_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_Instance_0();
				NullCheck(L_13);
				L_13->set_outObserver_4(L_14);
			}

IL_006d:
			{
				__this->set_unsubscribeTarget_2((Il2CppObject*)NULL);
				__this->set_parent_1((Subject_1_t490482206 *)NULL);
				IL2CPP_LEAVE(0x87, FINALLY_0080);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0080;
		}

FINALLY_0080:
		{ // begin finally (depth: 2)
			Il2CppObject * L_15 = V_1;
			Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_15, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(128)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(128)
		{
			IL2CPP_JUMP_TBL(0x87, IL_0087)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0087:
		{
			IL2CPP_LEAVE(0x93, FINALLY_008c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008c;
	}

FINALLY_008c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_16 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(140)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(140)
	{
		IL2CPP_JUMP_TBL(0x93, IL_0093)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0093:
	{
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<System.Object>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Subscription__ctor_m2347359895_MetadataUsageId;
extern "C"  void Subscription__ctor_m2347359895_gshared (Subscription_t2911320239 * __this, Subject_1_t2775141040 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscription__ctor_m2347359895_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Subject_1_t2775141040 * L_1 = ___parent0;
		__this->set_parent_1(L_1);
		Il2CppObject* L_2 = ___unsubscribeTarget1;
		__this->set_unsubscribeTarget_2(L_2);
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<System.Object>::Dispose()
extern "C"  void Subscription_Dispose_m3234146559_gshared (Subscription_t2911320239 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ListObserver_1_t1131724091 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Subject_1_t2775141040 * L_2 = (Subject_1_t2775141040 *)__this->get_parent_1();
			if (!L_2)
			{
				goto IL_0087;
			}
		}

IL_0018:
		{
			Subject_1_t2775141040 * L_3 = (Subject_1_t2775141040 *)__this->get_parent_1();
			NullCheck(L_3);
			Il2CppObject * L_4 = (Il2CppObject *)L_3->get_observerLock_0();
			V_1 = (Il2CppObject *)L_4;
			Il2CppObject * L_5 = V_1;
			Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		}

IL_002a:
		try
		{ // begin try (depth: 2)
			{
				Subject_1_t2775141040 * L_6 = (Subject_1_t2775141040 *)__this->get_parent_1();
				NullCheck(L_6);
				Il2CppObject* L_7 = (Il2CppObject*)L_6->get_outObserver_4();
				V_2 = (ListObserver_1_t1131724091 *)((ListObserver_1_t1131724091 *)IsInst(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
				ListObserver_1_t1131724091 * L_8 = V_2;
				if (!L_8)
				{
					goto IL_005d;
				}
			}

IL_0041:
			{
				Subject_1_t2775141040 * L_9 = (Subject_1_t2775141040 *)__this->get_parent_1();
				ListObserver_1_t1131724091 * L_10 = V_2;
				Il2CppObject* L_11 = (Il2CppObject*)__this->get_unsubscribeTarget_2();
				NullCheck((ListObserver_1_t1131724091 *)L_10);
				Il2CppObject* L_12 = ((  Il2CppObject* (*) (ListObserver_1_t1131724091 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ListObserver_1_t1131724091 *)L_10, (Il2CppObject*)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
				NullCheck(L_9);
				L_9->set_outObserver_4(L_12);
				goto IL_006d;
			}

IL_005d:
			{
				Subject_1_t2775141040 * L_13 = (Subject_1_t2775141040 *)__this->get_parent_1();
				IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
				EmptyObserver_1_t246459410 * L_14 = ((EmptyObserver_1_t246459410_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_Instance_0();
				NullCheck(L_13);
				L_13->set_outObserver_4(L_14);
			}

IL_006d:
			{
				__this->set_unsubscribeTarget_2((Il2CppObject*)NULL);
				__this->set_parent_1((Subject_1_t2775141040 *)NULL);
				IL2CPP_LEAVE(0x87, FINALLY_0080);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0080;
		}

FINALLY_0080:
		{ // begin finally (depth: 2)
			Il2CppObject * L_15 = V_1;
			Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_15, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(128)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(128)
		{
			IL2CPP_JUMP_TBL(0x87, IL_0087)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0087:
		{
			IL2CPP_LEAVE(0x93, FINALLY_008c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008c;
	}

FINALLY_008c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_16 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(140)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(140)
	{
		IL2CPP_JUMP_TBL(0x93, IL_0093)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0093:
	{
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<System.Single>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Subscription__ctor_m2945119406_MetadataUsageId;
extern "C"  void Subscription__ctor_m2945119406_gshared (Subscription_t3032422839 * __this, Subject_1_t2896243641 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscription__ctor_m2945119406_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Subject_1_t2896243641 * L_1 = ___parent0;
		__this->set_parent_1(L_1);
		Il2CppObject* L_2 = ___unsubscribeTarget1;
		__this->set_unsubscribeTarget_2(L_2);
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<System.Single>::Dispose()
extern "C"  void Subscription_Dispose_m953074824_gshared (Subscription_t3032422839 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ListObserver_1_t1252826692 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Subject_1_t2896243641 * L_2 = (Subject_1_t2896243641 *)__this->get_parent_1();
			if (!L_2)
			{
				goto IL_0087;
			}
		}

IL_0018:
		{
			Subject_1_t2896243641 * L_3 = (Subject_1_t2896243641 *)__this->get_parent_1();
			NullCheck(L_3);
			Il2CppObject * L_4 = (Il2CppObject *)L_3->get_observerLock_0();
			V_1 = (Il2CppObject *)L_4;
			Il2CppObject * L_5 = V_1;
			Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		}

IL_002a:
		try
		{ // begin try (depth: 2)
			{
				Subject_1_t2896243641 * L_6 = (Subject_1_t2896243641 *)__this->get_parent_1();
				NullCheck(L_6);
				Il2CppObject* L_7 = (Il2CppObject*)L_6->get_outObserver_4();
				V_2 = (ListObserver_1_t1252826692 *)((ListObserver_1_t1252826692 *)IsInst(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
				ListObserver_1_t1252826692 * L_8 = V_2;
				if (!L_8)
				{
					goto IL_005d;
				}
			}

IL_0041:
			{
				Subject_1_t2896243641 * L_9 = (Subject_1_t2896243641 *)__this->get_parent_1();
				ListObserver_1_t1252826692 * L_10 = V_2;
				Il2CppObject* L_11 = (Il2CppObject*)__this->get_unsubscribeTarget_2();
				NullCheck((ListObserver_1_t1252826692 *)L_10);
				Il2CppObject* L_12 = ((  Il2CppObject* (*) (ListObserver_1_t1252826692 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ListObserver_1_t1252826692 *)L_10, (Il2CppObject*)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
				NullCheck(L_9);
				L_9->set_outObserver_4(L_12);
				goto IL_006d;
			}

IL_005d:
			{
				Subject_1_t2896243641 * L_13 = (Subject_1_t2896243641 *)__this->get_parent_1();
				IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
				EmptyObserver_1_t367562011 * L_14 = ((EmptyObserver_1_t367562011_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_Instance_0();
				NullCheck(L_13);
				L_13->set_outObserver_4(L_14);
			}

IL_006d:
			{
				__this->set_unsubscribeTarget_2((Il2CppObject*)NULL);
				__this->set_parent_1((Subject_1_t2896243641 *)NULL);
				IL2CPP_LEAVE(0x87, FINALLY_0080);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0080;
		}

FINALLY_0080:
		{ // begin finally (depth: 2)
			Il2CppObject * L_15 = V_1;
			Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_15, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(128)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(128)
		{
			IL2CPP_JUMP_TBL(0x87, IL_0087)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0087:
		{
			IL2CPP_LEAVE(0x93, FINALLY_008c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008c;
	}

FINALLY_008c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_16 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(140)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(140)
	{
		IL2CPP_JUMP_TBL(0x93, IL_0093)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0093:
	{
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<TableButtons/StatkaPoTableViev>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Subscription__ctor_m1508639718_MetadataUsageId;
extern "C"  void Subscription__ctor_m1508639718_gshared (Subscription_t1287642339 * __this, Subject_1_t1151463141 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscription__ctor_m1508639718_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Subject_1_t1151463141 * L_1 = ___parent0;
		__this->set_parent_1(L_1);
		Il2CppObject* L_2 = ___unsubscribeTarget1;
		__this->set_unsubscribeTarget_2(L_2);
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<TableButtons/StatkaPoTableViev>::Dispose()
extern "C"  void Subscription_Dispose_m189280848_gshared (Subscription_t1287642339 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ListObserver_1_t3803013488 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Subject_1_t1151463141 * L_2 = (Subject_1_t1151463141 *)__this->get_parent_1();
			if (!L_2)
			{
				goto IL_0087;
			}
		}

IL_0018:
		{
			Subject_1_t1151463141 * L_3 = (Subject_1_t1151463141 *)__this->get_parent_1();
			NullCheck(L_3);
			Il2CppObject * L_4 = (Il2CppObject *)L_3->get_observerLock_0();
			V_1 = (Il2CppObject *)L_4;
			Il2CppObject * L_5 = V_1;
			Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		}

IL_002a:
		try
		{ // begin try (depth: 2)
			{
				Subject_1_t1151463141 * L_6 = (Subject_1_t1151463141 *)__this->get_parent_1();
				NullCheck(L_6);
				Il2CppObject* L_7 = (Il2CppObject*)L_6->get_outObserver_4();
				V_2 = (ListObserver_1_t3803013488 *)((ListObserver_1_t3803013488 *)IsInst(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
				ListObserver_1_t3803013488 * L_8 = V_2;
				if (!L_8)
				{
					goto IL_005d;
				}
			}

IL_0041:
			{
				Subject_1_t1151463141 * L_9 = (Subject_1_t1151463141 *)__this->get_parent_1();
				ListObserver_1_t3803013488 * L_10 = V_2;
				Il2CppObject* L_11 = (Il2CppObject*)__this->get_unsubscribeTarget_2();
				NullCheck((ListObserver_1_t3803013488 *)L_10);
				Il2CppObject* L_12 = ((  Il2CppObject* (*) (ListObserver_1_t3803013488 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ListObserver_1_t3803013488 *)L_10, (Il2CppObject*)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
				NullCheck(L_9);
				L_9->set_outObserver_4(L_12);
				goto IL_006d;
			}

IL_005d:
			{
				Subject_1_t1151463141 * L_13 = (Subject_1_t1151463141 *)__this->get_parent_1();
				IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
				EmptyObserver_1_t2917748807 * L_14 = ((EmptyObserver_1_t2917748807_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_Instance_0();
				NullCheck(L_13);
				L_13->set_outObserver_4(L_14);
			}

IL_006d:
			{
				__this->set_unsubscribeTarget_2((Il2CppObject*)NULL);
				__this->set_parent_1((Subject_1_t1151463141 *)NULL);
				IL2CPP_LEAVE(0x87, FINALLY_0080);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0080;
		}

FINALLY_0080:
		{ // begin finally (depth: 2)
			Il2CppObject * L_15 = V_1;
			Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_15, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(128)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(128)
		{
			IL2CPP_JUMP_TBL(0x87, IL_0087)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0087:
		{
			IL2CPP_LEAVE(0x93, FINALLY_008c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008c;
	}

FINALLY_008c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_16 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(140)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(140)
	{
		IL2CPP_JUMP_TBL(0x93, IL_0093)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0093:
	{
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UniRx.CollectionAddEvent`1<System.Object>>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Subscription__ctor_m1851991161_MetadataUsageId;
extern "C"  void Subscription__ctor_m1851991161_gshared (Subscription_t195768509 * __this, Subject_1_t59589311 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscription__ctor_m1851991161_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Subject_1_t59589311 * L_1 = ___parent0;
		__this->set_parent_1(L_1);
		Il2CppObject* L_2 = ___unsubscribeTarget1;
		__this->set_unsubscribeTarget_2(L_2);
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UniRx.CollectionAddEvent`1<System.Object>>::Dispose()
extern "C"  void Subscription_Dispose_m3028760029_gshared (Subscription_t195768509 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ListObserver_1_t2711139658 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Subject_1_t59589311 * L_2 = (Subject_1_t59589311 *)__this->get_parent_1();
			if (!L_2)
			{
				goto IL_0087;
			}
		}

IL_0018:
		{
			Subject_1_t59589311 * L_3 = (Subject_1_t59589311 *)__this->get_parent_1();
			NullCheck(L_3);
			Il2CppObject * L_4 = (Il2CppObject *)L_3->get_observerLock_0();
			V_1 = (Il2CppObject *)L_4;
			Il2CppObject * L_5 = V_1;
			Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		}

IL_002a:
		try
		{ // begin try (depth: 2)
			{
				Subject_1_t59589311 * L_6 = (Subject_1_t59589311 *)__this->get_parent_1();
				NullCheck(L_6);
				Il2CppObject* L_7 = (Il2CppObject*)L_6->get_outObserver_4();
				V_2 = (ListObserver_1_t2711139658 *)((ListObserver_1_t2711139658 *)IsInst(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
				ListObserver_1_t2711139658 * L_8 = V_2;
				if (!L_8)
				{
					goto IL_005d;
				}
			}

IL_0041:
			{
				Subject_1_t59589311 * L_9 = (Subject_1_t59589311 *)__this->get_parent_1();
				ListObserver_1_t2711139658 * L_10 = V_2;
				Il2CppObject* L_11 = (Il2CppObject*)__this->get_unsubscribeTarget_2();
				NullCheck((ListObserver_1_t2711139658 *)L_10);
				Il2CppObject* L_12 = ((  Il2CppObject* (*) (ListObserver_1_t2711139658 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ListObserver_1_t2711139658 *)L_10, (Il2CppObject*)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
				NullCheck(L_9);
				L_9->set_outObserver_4(L_12);
				goto IL_006d;
			}

IL_005d:
			{
				Subject_1_t59589311 * L_13 = (Subject_1_t59589311 *)__this->get_parent_1();
				IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
				EmptyObserver_1_t1825874977 * L_14 = ((EmptyObserver_1_t1825874977_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_Instance_0();
				NullCheck(L_13);
				L_13->set_outObserver_4(L_14);
			}

IL_006d:
			{
				__this->set_unsubscribeTarget_2((Il2CppObject*)NULL);
				__this->set_parent_1((Subject_1_t59589311 *)NULL);
				IL2CPP_LEAVE(0x87, FINALLY_0080);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0080;
		}

FINALLY_0080:
		{ // begin finally (depth: 2)
			Il2CppObject * L_15 = V_1;
			Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_15, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(128)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(128)
		{
			IL2CPP_JUMP_TBL(0x87, IL_0087)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0087:
		{
			IL2CPP_LEAVE(0x93, FINALLY_008c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008c;
	}

FINALLY_008c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_16 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(140)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(140)
	{
		IL2CPP_JUMP_TBL(0x93, IL_0093)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0093:
	{
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Subscription__ctor_m3286311513_MetadataUsageId;
extern "C"  void Subscription__ctor_m3286311513_gshared (Subscription_t4022891204 * __this, Subject_1_t3886712006 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscription__ctor_m3286311513_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Subject_1_t3886712006 * L_1 = ___parent0;
		__this->set_parent_1(L_1);
		Il2CppObject* L_2 = ___unsubscribeTarget1;
		__this->set_unsubscribeTarget_2(L_2);
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UniRx.CollectionAddEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::Dispose()
extern "C"  void Subscription_Dispose_m836406269_gshared (Subscription_t4022891204 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ListObserver_1_t2243295057 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Subject_1_t3886712006 * L_2 = (Subject_1_t3886712006 *)__this->get_parent_1();
			if (!L_2)
			{
				goto IL_0087;
			}
		}

IL_0018:
		{
			Subject_1_t3886712006 * L_3 = (Subject_1_t3886712006 *)__this->get_parent_1();
			NullCheck(L_3);
			Il2CppObject * L_4 = (Il2CppObject *)L_3->get_observerLock_0();
			V_1 = (Il2CppObject *)L_4;
			Il2CppObject * L_5 = V_1;
			Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		}

IL_002a:
		try
		{ // begin try (depth: 2)
			{
				Subject_1_t3886712006 * L_6 = (Subject_1_t3886712006 *)__this->get_parent_1();
				NullCheck(L_6);
				Il2CppObject* L_7 = (Il2CppObject*)L_6->get_outObserver_4();
				V_2 = (ListObserver_1_t2243295057 *)((ListObserver_1_t2243295057 *)IsInst(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
				ListObserver_1_t2243295057 * L_8 = V_2;
				if (!L_8)
				{
					goto IL_005d;
				}
			}

IL_0041:
			{
				Subject_1_t3886712006 * L_9 = (Subject_1_t3886712006 *)__this->get_parent_1();
				ListObserver_1_t2243295057 * L_10 = V_2;
				Il2CppObject* L_11 = (Il2CppObject*)__this->get_unsubscribeTarget_2();
				NullCheck((ListObserver_1_t2243295057 *)L_10);
				Il2CppObject* L_12 = ((  Il2CppObject* (*) (ListObserver_1_t2243295057 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ListObserver_1_t2243295057 *)L_10, (Il2CppObject*)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
				NullCheck(L_9);
				L_9->set_outObserver_4(L_12);
				goto IL_006d;
			}

IL_005d:
			{
				Subject_1_t3886712006 * L_13 = (Subject_1_t3886712006 *)__this->get_parent_1();
				IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
				EmptyObserver_1_t1358030376 * L_14 = ((EmptyObserver_1_t1358030376_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_Instance_0();
				NullCheck(L_13);
				L_13->set_outObserver_4(L_14);
			}

IL_006d:
			{
				__this->set_unsubscribeTarget_2((Il2CppObject*)NULL);
				__this->set_parent_1((Subject_1_t3886712006 *)NULL);
				IL2CPP_LEAVE(0x87, FINALLY_0080);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0080;
		}

FINALLY_0080:
		{ // begin finally (depth: 2)
			Il2CppObject * L_15 = V_1;
			Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_15, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(128)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(128)
		{
			IL2CPP_JUMP_TBL(0x87, IL_0087)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0087:
		{
			IL2CPP_LEAVE(0x93, FINALLY_008c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008c;
	}

FINALLY_008c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_16 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(140)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(140)
	{
		IL2CPP_JUMP_TBL(0x93, IL_0093)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0093:
	{
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UniRx.CollectionMoveEvent`1<System.Object>>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Subscription__ctor_m1098239611_MetadataUsageId;
extern "C"  void Subscription__ctor_m1098239611_gshared (Subscription_t695680369 * __this, Subject_1_t559501171 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscription__ctor_m1098239611_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Subject_1_t559501171 * L_1 = ___parent0;
		__this->set_parent_1(L_1);
		Il2CppObject* L_2 = ___unsubscribeTarget1;
		__this->set_unsubscribeTarget_2(L_2);
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UniRx.CollectionMoveEvent`1<System.Object>>::Dispose()
extern "C"  void Subscription_Dispose_m1564560027_gshared (Subscription_t695680369 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ListObserver_1_t3211051518 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Subject_1_t559501171 * L_2 = (Subject_1_t559501171 *)__this->get_parent_1();
			if (!L_2)
			{
				goto IL_0087;
			}
		}

IL_0018:
		{
			Subject_1_t559501171 * L_3 = (Subject_1_t559501171 *)__this->get_parent_1();
			NullCheck(L_3);
			Il2CppObject * L_4 = (Il2CppObject *)L_3->get_observerLock_0();
			V_1 = (Il2CppObject *)L_4;
			Il2CppObject * L_5 = V_1;
			Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		}

IL_002a:
		try
		{ // begin try (depth: 2)
			{
				Subject_1_t559501171 * L_6 = (Subject_1_t559501171 *)__this->get_parent_1();
				NullCheck(L_6);
				Il2CppObject* L_7 = (Il2CppObject*)L_6->get_outObserver_4();
				V_2 = (ListObserver_1_t3211051518 *)((ListObserver_1_t3211051518 *)IsInst(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
				ListObserver_1_t3211051518 * L_8 = V_2;
				if (!L_8)
				{
					goto IL_005d;
				}
			}

IL_0041:
			{
				Subject_1_t559501171 * L_9 = (Subject_1_t559501171 *)__this->get_parent_1();
				ListObserver_1_t3211051518 * L_10 = V_2;
				Il2CppObject* L_11 = (Il2CppObject*)__this->get_unsubscribeTarget_2();
				NullCheck((ListObserver_1_t3211051518 *)L_10);
				Il2CppObject* L_12 = ((  Il2CppObject* (*) (ListObserver_1_t3211051518 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ListObserver_1_t3211051518 *)L_10, (Il2CppObject*)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
				NullCheck(L_9);
				L_9->set_outObserver_4(L_12);
				goto IL_006d;
			}

IL_005d:
			{
				Subject_1_t559501171 * L_13 = (Subject_1_t559501171 *)__this->get_parent_1();
				IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
				EmptyObserver_1_t2325786837 * L_14 = ((EmptyObserver_1_t2325786837_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_Instance_0();
				NullCheck(L_13);
				L_13->set_outObserver_4(L_14);
			}

IL_006d:
			{
				__this->set_unsubscribeTarget_2((Il2CppObject*)NULL);
				__this->set_parent_1((Subject_1_t559501171 *)NULL);
				IL2CPP_LEAVE(0x87, FINALLY_0080);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0080;
		}

FINALLY_0080:
		{ // begin finally (depth: 2)
			Il2CppObject * L_15 = V_1;
			Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_15, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(128)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(128)
		{
			IL2CPP_JUMP_TBL(0x87, IL_0087)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0087:
		{
			IL2CPP_LEAVE(0x93, FINALLY_008c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008c;
	}

FINALLY_008c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_16 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(140)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(140)
	{
		IL2CPP_JUMP_TBL(0x93, IL_0093)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0093:
	{
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Subscription__ctor_m2371882903_MetadataUsageId;
extern "C"  void Subscription__ctor_m2371882903_gshared (Subscription_t227835768 * __this, Subject_1_t91656570 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscription__ctor_m2371882903_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Subject_1_t91656570 * L_1 = ___parent0;
		__this->set_parent_1(L_1);
		Il2CppObject* L_2 = ___unsubscribeTarget1;
		__this->set_unsubscribeTarget_2(L_2);
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::Dispose()
extern "C"  void Subscription_Dispose_m955275263_gshared (Subscription_t227835768 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ListObserver_1_t2743206917 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Subject_1_t91656570 * L_2 = (Subject_1_t91656570 *)__this->get_parent_1();
			if (!L_2)
			{
				goto IL_0087;
			}
		}

IL_0018:
		{
			Subject_1_t91656570 * L_3 = (Subject_1_t91656570 *)__this->get_parent_1();
			NullCheck(L_3);
			Il2CppObject * L_4 = (Il2CppObject *)L_3->get_observerLock_0();
			V_1 = (Il2CppObject *)L_4;
			Il2CppObject * L_5 = V_1;
			Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		}

IL_002a:
		try
		{ // begin try (depth: 2)
			{
				Subject_1_t91656570 * L_6 = (Subject_1_t91656570 *)__this->get_parent_1();
				NullCheck(L_6);
				Il2CppObject* L_7 = (Il2CppObject*)L_6->get_outObserver_4();
				V_2 = (ListObserver_1_t2743206917 *)((ListObserver_1_t2743206917 *)IsInst(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
				ListObserver_1_t2743206917 * L_8 = V_2;
				if (!L_8)
				{
					goto IL_005d;
				}
			}

IL_0041:
			{
				Subject_1_t91656570 * L_9 = (Subject_1_t91656570 *)__this->get_parent_1();
				ListObserver_1_t2743206917 * L_10 = V_2;
				Il2CppObject* L_11 = (Il2CppObject*)__this->get_unsubscribeTarget_2();
				NullCheck((ListObserver_1_t2743206917 *)L_10);
				Il2CppObject* L_12 = ((  Il2CppObject* (*) (ListObserver_1_t2743206917 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ListObserver_1_t2743206917 *)L_10, (Il2CppObject*)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
				NullCheck(L_9);
				L_9->set_outObserver_4(L_12);
				goto IL_006d;
			}

IL_005d:
			{
				Subject_1_t91656570 * L_13 = (Subject_1_t91656570 *)__this->get_parent_1();
				IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
				EmptyObserver_1_t1857942236 * L_14 = ((EmptyObserver_1_t1857942236_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_Instance_0();
				NullCheck(L_13);
				L_13->set_outObserver_4(L_14);
			}

IL_006d:
			{
				__this->set_unsubscribeTarget_2((Il2CppObject*)NULL);
				__this->set_parent_1((Subject_1_t91656570 *)NULL);
				IL2CPP_LEAVE(0x87, FINALLY_0080);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0080;
		}

FINALLY_0080:
		{ // begin finally (depth: 2)
			Il2CppObject * L_15 = V_1;
			Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_15, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(128)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(128)
		{
			IL2CPP_JUMP_TBL(0x87, IL_0087)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0087:
		{
			IL2CPP_LEAVE(0x93, FINALLY_008c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008c;
	}

FINALLY_008c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_16 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(140)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(140)
	{
		IL2CPP_JUMP_TBL(0x93, IL_0093)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0093:
	{
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UniRx.CollectionRemoveEvent`1<System.Object>>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Subscription__ctor_m1285516328_MetadataUsageId;
extern "C"  void Subscription__ctor_m1285516328_gshared (Subscription_t2316851542 * __this, Subject_1_t2180672344 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscription__ctor_m1285516328_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Subject_1_t2180672344 * L_1 = ___parent0;
		__this->set_parent_1(L_1);
		Il2CppObject* L_2 = ___unsubscribeTarget1;
		__this->set_unsubscribeTarget_2(L_2);
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UniRx.CollectionRemoveEvent`1<System.Object>>::Dispose()
extern "C"  void Subscription_Dispose_m2278203598_gshared (Subscription_t2316851542 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ListObserver_1_t537255395 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Subject_1_t2180672344 * L_2 = (Subject_1_t2180672344 *)__this->get_parent_1();
			if (!L_2)
			{
				goto IL_0087;
			}
		}

IL_0018:
		{
			Subject_1_t2180672344 * L_3 = (Subject_1_t2180672344 *)__this->get_parent_1();
			NullCheck(L_3);
			Il2CppObject * L_4 = (Il2CppObject *)L_3->get_observerLock_0();
			V_1 = (Il2CppObject *)L_4;
			Il2CppObject * L_5 = V_1;
			Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		}

IL_002a:
		try
		{ // begin try (depth: 2)
			{
				Subject_1_t2180672344 * L_6 = (Subject_1_t2180672344 *)__this->get_parent_1();
				NullCheck(L_6);
				Il2CppObject* L_7 = (Il2CppObject*)L_6->get_outObserver_4();
				V_2 = (ListObserver_1_t537255395 *)((ListObserver_1_t537255395 *)IsInst(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
				ListObserver_1_t537255395 * L_8 = V_2;
				if (!L_8)
				{
					goto IL_005d;
				}
			}

IL_0041:
			{
				Subject_1_t2180672344 * L_9 = (Subject_1_t2180672344 *)__this->get_parent_1();
				ListObserver_1_t537255395 * L_10 = V_2;
				Il2CppObject* L_11 = (Il2CppObject*)__this->get_unsubscribeTarget_2();
				NullCheck((ListObserver_1_t537255395 *)L_10);
				Il2CppObject* L_12 = ((  Il2CppObject* (*) (ListObserver_1_t537255395 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ListObserver_1_t537255395 *)L_10, (Il2CppObject*)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
				NullCheck(L_9);
				L_9->set_outObserver_4(L_12);
				goto IL_006d;
			}

IL_005d:
			{
				Subject_1_t2180672344 * L_13 = (Subject_1_t2180672344 *)__this->get_parent_1();
				IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
				EmptyObserver_1_t3946958010 * L_14 = ((EmptyObserver_1_t3946958010_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_Instance_0();
				NullCheck(L_13);
				L_13->set_outObserver_4(L_14);
			}

IL_006d:
			{
				__this->set_unsubscribeTarget_2((Il2CppObject*)NULL);
				__this->set_parent_1((Subject_1_t2180672344 *)NULL);
				IL2CPP_LEAVE(0x87, FINALLY_0080);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0080;
		}

FINALLY_0080:
		{ // begin finally (depth: 2)
			Il2CppObject * L_15 = V_1;
			Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_15, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(128)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(128)
		{
			IL2CPP_JUMP_TBL(0x87, IL_0087)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0087:
		{
			IL2CPP_LEAVE(0x93, FINALLY_008c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008c;
	}

FINALLY_008c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_16 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(140)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(140)
	{
		IL2CPP_JUMP_TBL(0x93, IL_0093)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0093:
	{
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Subscription__ctor_m952601866_MetadataUsageId;
extern "C"  void Subscription__ctor_m952601866_gshared (Subscription_t1849006941 * __this, Subject_1_t1712827743 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscription__ctor_m952601866_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Subject_1_t1712827743 * L_1 = ___parent0;
		__this->set_parent_1(L_1);
		Il2CppObject* L_2 = ___unsubscribeTarget1;
		__this->set_unsubscribeTarget_2(L_2);
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UniRx.CollectionRemoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::Dispose()
extern "C"  void Subscription_Dispose_m1259664044_gshared (Subscription_t1849006941 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ListObserver_1_t69410794 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Subject_1_t1712827743 * L_2 = (Subject_1_t1712827743 *)__this->get_parent_1();
			if (!L_2)
			{
				goto IL_0087;
			}
		}

IL_0018:
		{
			Subject_1_t1712827743 * L_3 = (Subject_1_t1712827743 *)__this->get_parent_1();
			NullCheck(L_3);
			Il2CppObject * L_4 = (Il2CppObject *)L_3->get_observerLock_0();
			V_1 = (Il2CppObject *)L_4;
			Il2CppObject * L_5 = V_1;
			Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		}

IL_002a:
		try
		{ // begin try (depth: 2)
			{
				Subject_1_t1712827743 * L_6 = (Subject_1_t1712827743 *)__this->get_parent_1();
				NullCheck(L_6);
				Il2CppObject* L_7 = (Il2CppObject*)L_6->get_outObserver_4();
				V_2 = (ListObserver_1_t69410794 *)((ListObserver_1_t69410794 *)IsInst(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
				ListObserver_1_t69410794 * L_8 = V_2;
				if (!L_8)
				{
					goto IL_005d;
				}
			}

IL_0041:
			{
				Subject_1_t1712827743 * L_9 = (Subject_1_t1712827743 *)__this->get_parent_1();
				ListObserver_1_t69410794 * L_10 = V_2;
				Il2CppObject* L_11 = (Il2CppObject*)__this->get_unsubscribeTarget_2();
				NullCheck((ListObserver_1_t69410794 *)L_10);
				Il2CppObject* L_12 = ((  Il2CppObject* (*) (ListObserver_1_t69410794 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ListObserver_1_t69410794 *)L_10, (Il2CppObject*)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
				NullCheck(L_9);
				L_9->set_outObserver_4(L_12);
				goto IL_006d;
			}

IL_005d:
			{
				Subject_1_t1712827743 * L_13 = (Subject_1_t1712827743 *)__this->get_parent_1();
				IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
				EmptyObserver_1_t3479113409 * L_14 = ((EmptyObserver_1_t3479113409_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_Instance_0();
				NullCheck(L_13);
				L_13->set_outObserver_4(L_14);
			}

IL_006d:
			{
				__this->set_unsubscribeTarget_2((Il2CppObject*)NULL);
				__this->set_parent_1((Subject_1_t1712827743 *)NULL);
				IL2CPP_LEAVE(0x87, FINALLY_0080);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0080;
		}

FINALLY_0080:
		{ // begin finally (depth: 2)
			Il2CppObject * L_15 = V_1;
			Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_15, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(128)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(128)
		{
			IL2CPP_JUMP_TBL(0x87, IL_0087)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0087:
		{
			IL2CPP_LEAVE(0x93, FINALLY_008c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008c;
	}

FINALLY_008c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_16 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(140)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(140)
	{
		IL2CPP_JUMP_TBL(0x93, IL_0093)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0093:
	{
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UniRx.CollectionReplaceEvent`1<System.Object>>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Subscription__ctor_m247878534_MetadataUsageId;
extern "C"  void Subscription__ctor_m247878534_gshared (Subscription_t3915964354 * __this, Subject_1_t3779785156 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscription__ctor_m247878534_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Subject_1_t3779785156 * L_1 = ___parent0;
		__this->set_parent_1(L_1);
		Il2CppObject* L_2 = ___unsubscribeTarget1;
		__this->set_unsubscribeTarget_2(L_2);
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UniRx.CollectionReplaceEvent`1<System.Object>>::Dispose()
extern "C"  void Subscription_Dispose_m262938288_gshared (Subscription_t3915964354 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ListObserver_1_t2136368207 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Subject_1_t3779785156 * L_2 = (Subject_1_t3779785156 *)__this->get_parent_1();
			if (!L_2)
			{
				goto IL_0087;
			}
		}

IL_0018:
		{
			Subject_1_t3779785156 * L_3 = (Subject_1_t3779785156 *)__this->get_parent_1();
			NullCheck(L_3);
			Il2CppObject * L_4 = (Il2CppObject *)L_3->get_observerLock_0();
			V_1 = (Il2CppObject *)L_4;
			Il2CppObject * L_5 = V_1;
			Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		}

IL_002a:
		try
		{ // begin try (depth: 2)
			{
				Subject_1_t3779785156 * L_6 = (Subject_1_t3779785156 *)__this->get_parent_1();
				NullCheck(L_6);
				Il2CppObject* L_7 = (Il2CppObject*)L_6->get_outObserver_4();
				V_2 = (ListObserver_1_t2136368207 *)((ListObserver_1_t2136368207 *)IsInst(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
				ListObserver_1_t2136368207 * L_8 = V_2;
				if (!L_8)
				{
					goto IL_005d;
				}
			}

IL_0041:
			{
				Subject_1_t3779785156 * L_9 = (Subject_1_t3779785156 *)__this->get_parent_1();
				ListObserver_1_t2136368207 * L_10 = V_2;
				Il2CppObject* L_11 = (Il2CppObject*)__this->get_unsubscribeTarget_2();
				NullCheck((ListObserver_1_t2136368207 *)L_10);
				Il2CppObject* L_12 = ((  Il2CppObject* (*) (ListObserver_1_t2136368207 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ListObserver_1_t2136368207 *)L_10, (Il2CppObject*)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
				NullCheck(L_9);
				L_9->set_outObserver_4(L_12);
				goto IL_006d;
			}

IL_005d:
			{
				Subject_1_t3779785156 * L_13 = (Subject_1_t3779785156 *)__this->get_parent_1();
				IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
				EmptyObserver_1_t1251103526 * L_14 = ((EmptyObserver_1_t1251103526_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_Instance_0();
				NullCheck(L_13);
				L_13->set_outObserver_4(L_14);
			}

IL_006d:
			{
				__this->set_unsubscribeTarget_2((Il2CppObject*)NULL);
				__this->set_parent_1((Subject_1_t3779785156 *)NULL);
				IL2CPP_LEAVE(0x87, FINALLY_0080);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0080;
		}

FINALLY_0080:
		{ // begin finally (depth: 2)
			Il2CppObject * L_15 = V_1;
			Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_15, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(128)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(128)
		{
			IL2CPP_JUMP_TBL(0x87, IL_0087)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0087:
		{
			IL2CPP_LEAVE(0x93, FINALLY_008c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008c;
	}

FINALLY_008c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_16 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(140)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(140)
	{
		IL2CPP_JUMP_TBL(0x93, IL_0093)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0093:
	{
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Subscription__ctor_m1051003500_MetadataUsageId;
extern "C"  void Subscription__ctor_m1051003500_gshared (Subscription_t3448119753 * __this, Subject_1_t3311940555 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscription__ctor_m1051003500_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Subject_1_t3311940555 * L_1 = ___parent0;
		__this->set_parent_1(L_1);
		Il2CppObject* L_2 = ___unsubscribeTarget1;
		__this->set_unsubscribeTarget_2(L_2);
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UniRx.CollectionReplaceEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::Dispose()
extern "C"  void Subscription_Dispose_m3809482762_gshared (Subscription_t3448119753 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ListObserver_1_t1668523606 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Subject_1_t3311940555 * L_2 = (Subject_1_t3311940555 *)__this->get_parent_1();
			if (!L_2)
			{
				goto IL_0087;
			}
		}

IL_0018:
		{
			Subject_1_t3311940555 * L_3 = (Subject_1_t3311940555 *)__this->get_parent_1();
			NullCheck(L_3);
			Il2CppObject * L_4 = (Il2CppObject *)L_3->get_observerLock_0();
			V_1 = (Il2CppObject *)L_4;
			Il2CppObject * L_5 = V_1;
			Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		}

IL_002a:
		try
		{ // begin try (depth: 2)
			{
				Subject_1_t3311940555 * L_6 = (Subject_1_t3311940555 *)__this->get_parent_1();
				NullCheck(L_6);
				Il2CppObject* L_7 = (Il2CppObject*)L_6->get_outObserver_4();
				V_2 = (ListObserver_1_t1668523606 *)((ListObserver_1_t1668523606 *)IsInst(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
				ListObserver_1_t1668523606 * L_8 = V_2;
				if (!L_8)
				{
					goto IL_005d;
				}
			}

IL_0041:
			{
				Subject_1_t3311940555 * L_9 = (Subject_1_t3311940555 *)__this->get_parent_1();
				ListObserver_1_t1668523606 * L_10 = V_2;
				Il2CppObject* L_11 = (Il2CppObject*)__this->get_unsubscribeTarget_2();
				NullCheck((ListObserver_1_t1668523606 *)L_10);
				Il2CppObject* L_12 = ((  Il2CppObject* (*) (ListObserver_1_t1668523606 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ListObserver_1_t1668523606 *)L_10, (Il2CppObject*)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
				NullCheck(L_9);
				L_9->set_outObserver_4(L_12);
				goto IL_006d;
			}

IL_005d:
			{
				Subject_1_t3311940555 * L_13 = (Subject_1_t3311940555 *)__this->get_parent_1();
				IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
				EmptyObserver_1_t783258925 * L_14 = ((EmptyObserver_1_t783258925_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_Instance_0();
				NullCheck(L_13);
				L_13->set_outObserver_4(L_14);
			}

IL_006d:
			{
				__this->set_unsubscribeTarget_2((Il2CppObject*)NULL);
				__this->set_parent_1((Subject_1_t3311940555 *)NULL);
				IL2CPP_LEAVE(0x87, FINALLY_0080);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0080;
		}

FINALLY_0080:
		{ // begin finally (depth: 2)
			Il2CppObject * L_15 = V_1;
			Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_15, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(128)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(128)
		{
			IL2CPP_JUMP_TBL(0x87, IL_0087)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0087:
		{
			IL2CPP_LEAVE(0x93, FINALLY_008c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008c;
	}

FINALLY_008c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_16 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(140)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(140)
	{
		IL2CPP_JUMP_TBL(0x93, IL_0093)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0093:
	{
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UniRx.CountChangedStatus>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Subscription__ctor_m3005987492_MetadataUsageId;
extern "C"  void Subscription__ctor_m3005987492_gshared (Subscription_t1550474243 * __this, Subject_1_t1414295045 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscription__ctor_m3005987492_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Subject_1_t1414295045 * L_1 = ___parent0;
		__this->set_parent_1(L_1);
		Il2CppObject* L_2 = ___unsubscribeTarget1;
		__this->set_unsubscribeTarget_2(L_2);
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UniRx.CountChangedStatus>::Dispose()
extern "C"  void Subscription_Dispose_m2136949970_gshared (Subscription_t1550474243 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ListObserver_1_t4065845392 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Subject_1_t1414295045 * L_2 = (Subject_1_t1414295045 *)__this->get_parent_1();
			if (!L_2)
			{
				goto IL_0087;
			}
		}

IL_0018:
		{
			Subject_1_t1414295045 * L_3 = (Subject_1_t1414295045 *)__this->get_parent_1();
			NullCheck(L_3);
			Il2CppObject * L_4 = (Il2CppObject *)L_3->get_observerLock_0();
			V_1 = (Il2CppObject *)L_4;
			Il2CppObject * L_5 = V_1;
			Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		}

IL_002a:
		try
		{ // begin try (depth: 2)
			{
				Subject_1_t1414295045 * L_6 = (Subject_1_t1414295045 *)__this->get_parent_1();
				NullCheck(L_6);
				Il2CppObject* L_7 = (Il2CppObject*)L_6->get_outObserver_4();
				V_2 = (ListObserver_1_t4065845392 *)((ListObserver_1_t4065845392 *)IsInst(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
				ListObserver_1_t4065845392 * L_8 = V_2;
				if (!L_8)
				{
					goto IL_005d;
				}
			}

IL_0041:
			{
				Subject_1_t1414295045 * L_9 = (Subject_1_t1414295045 *)__this->get_parent_1();
				ListObserver_1_t4065845392 * L_10 = V_2;
				Il2CppObject* L_11 = (Il2CppObject*)__this->get_unsubscribeTarget_2();
				NullCheck((ListObserver_1_t4065845392 *)L_10);
				Il2CppObject* L_12 = ((  Il2CppObject* (*) (ListObserver_1_t4065845392 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ListObserver_1_t4065845392 *)L_10, (Il2CppObject*)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
				NullCheck(L_9);
				L_9->set_outObserver_4(L_12);
				goto IL_006d;
			}

IL_005d:
			{
				Subject_1_t1414295045 * L_13 = (Subject_1_t1414295045 *)__this->get_parent_1();
				IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
				EmptyObserver_1_t3180580711 * L_14 = ((EmptyObserver_1_t3180580711_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_Instance_0();
				NullCheck(L_13);
				L_13->set_outObserver_4(L_14);
			}

IL_006d:
			{
				__this->set_unsubscribeTarget_2((Il2CppObject*)NULL);
				__this->set_parent_1((Subject_1_t1414295045 *)NULL);
				IL2CPP_LEAVE(0x87, FINALLY_0080);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0080;
		}

FINALLY_0080:
		{ // begin finally (depth: 2)
			Il2CppObject * L_15 = V_1;
			Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_15, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(128)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(128)
		{
			IL2CPP_JUMP_TBL(0x87, IL_0087)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0087:
		{
			IL2CPP_LEAVE(0x93, FINALLY_008c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008c;
	}

FINALLY_008c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_16 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(140)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(140)
	{
		IL2CPP_JUMP_TBL(0x93, IL_0093)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0093:
	{
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Subscription__ctor_m3220022820_MetadataUsageId;
extern "C"  void Subscription__ctor_m3220022820_gshared (Subscription_t2325194826 * __this, Subject_1_t2189015628 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscription__ctor_m3220022820_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Subject_1_t2189015628 * L_1 = ___parent0;
		__this->set_parent_1(L_1);
		Il2CppObject* L_2 = ___unsubscribeTarget1;
		__this->set_unsubscribeTarget_2(L_2);
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UniRx.DictionaryAddEvent`2<System.Object,System.Object>>::Dispose()
extern "C"  void Subscription_Dispose_m3047827794_gshared (Subscription_t2325194826 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ListObserver_1_t545598679 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Subject_1_t2189015628 * L_2 = (Subject_1_t2189015628 *)__this->get_parent_1();
			if (!L_2)
			{
				goto IL_0087;
			}
		}

IL_0018:
		{
			Subject_1_t2189015628 * L_3 = (Subject_1_t2189015628 *)__this->get_parent_1();
			NullCheck(L_3);
			Il2CppObject * L_4 = (Il2CppObject *)L_3->get_observerLock_0();
			V_1 = (Il2CppObject *)L_4;
			Il2CppObject * L_5 = V_1;
			Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		}

IL_002a:
		try
		{ // begin try (depth: 2)
			{
				Subject_1_t2189015628 * L_6 = (Subject_1_t2189015628 *)__this->get_parent_1();
				NullCheck(L_6);
				Il2CppObject* L_7 = (Il2CppObject*)L_6->get_outObserver_4();
				V_2 = (ListObserver_1_t545598679 *)((ListObserver_1_t545598679 *)IsInst(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
				ListObserver_1_t545598679 * L_8 = V_2;
				if (!L_8)
				{
					goto IL_005d;
				}
			}

IL_0041:
			{
				Subject_1_t2189015628 * L_9 = (Subject_1_t2189015628 *)__this->get_parent_1();
				ListObserver_1_t545598679 * L_10 = V_2;
				Il2CppObject* L_11 = (Il2CppObject*)__this->get_unsubscribeTarget_2();
				NullCheck((ListObserver_1_t545598679 *)L_10);
				Il2CppObject* L_12 = ((  Il2CppObject* (*) (ListObserver_1_t545598679 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ListObserver_1_t545598679 *)L_10, (Il2CppObject*)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
				NullCheck(L_9);
				L_9->set_outObserver_4(L_12);
				goto IL_006d;
			}

IL_005d:
			{
				Subject_1_t2189015628 * L_13 = (Subject_1_t2189015628 *)__this->get_parent_1();
				IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
				EmptyObserver_1_t3955301294 * L_14 = ((EmptyObserver_1_t3955301294_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_Instance_0();
				NullCheck(L_13);
				L_13->set_outObserver_4(L_14);
			}

IL_006d:
			{
				__this->set_unsubscribeTarget_2((Il2CppObject*)NULL);
				__this->set_parent_1((Subject_1_t2189015628 *)NULL);
				IL2CPP_LEAVE(0x87, FINALLY_0080);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0080;
		}

FINALLY_0080:
		{ // begin finally (depth: 2)
			Il2CppObject * L_15 = V_1;
			Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_15, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(128)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(128)
		{
			IL2CPP_JUMP_TBL(0x87, IL_0087)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0087:
		{
			IL2CPP_LEAVE(0x93, FINALLY_008c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008c;
	}

FINALLY_008c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_16 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(140)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(140)
	{
		IL2CPP_JUMP_TBL(0x93, IL_0093)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0093:
	{
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Subscription__ctor_m1808152739_MetadataUsageId;
extern "C"  void Subscription__ctor_m1808152739_gshared (Subscription_t1101534813 * __this, Subject_1_t965355615 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscription__ctor_m1808152739_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Subject_1_t965355615 * L_1 = ___parent0;
		__this->set_parent_1(L_1);
		Il2CppObject* L_2 = ___unsubscribeTarget1;
		__this->set_unsubscribeTarget_2(L_2);
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UniRx.DictionaryRemoveEvent`2<System.Object,System.Object>>::Dispose()
extern "C"  void Subscription_Dispose_m1023051635_gshared (Subscription_t1101534813 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ListObserver_1_t3616905962 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Subject_1_t965355615 * L_2 = (Subject_1_t965355615 *)__this->get_parent_1();
			if (!L_2)
			{
				goto IL_0087;
			}
		}

IL_0018:
		{
			Subject_1_t965355615 * L_3 = (Subject_1_t965355615 *)__this->get_parent_1();
			NullCheck(L_3);
			Il2CppObject * L_4 = (Il2CppObject *)L_3->get_observerLock_0();
			V_1 = (Il2CppObject *)L_4;
			Il2CppObject * L_5 = V_1;
			Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		}

IL_002a:
		try
		{ // begin try (depth: 2)
			{
				Subject_1_t965355615 * L_6 = (Subject_1_t965355615 *)__this->get_parent_1();
				NullCheck(L_6);
				Il2CppObject* L_7 = (Il2CppObject*)L_6->get_outObserver_4();
				V_2 = (ListObserver_1_t3616905962 *)((ListObserver_1_t3616905962 *)IsInst(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
				ListObserver_1_t3616905962 * L_8 = V_2;
				if (!L_8)
				{
					goto IL_005d;
				}
			}

IL_0041:
			{
				Subject_1_t965355615 * L_9 = (Subject_1_t965355615 *)__this->get_parent_1();
				ListObserver_1_t3616905962 * L_10 = V_2;
				Il2CppObject* L_11 = (Il2CppObject*)__this->get_unsubscribeTarget_2();
				NullCheck((ListObserver_1_t3616905962 *)L_10);
				Il2CppObject* L_12 = ((  Il2CppObject* (*) (ListObserver_1_t3616905962 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ListObserver_1_t3616905962 *)L_10, (Il2CppObject*)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
				NullCheck(L_9);
				L_9->set_outObserver_4(L_12);
				goto IL_006d;
			}

IL_005d:
			{
				Subject_1_t965355615 * L_13 = (Subject_1_t965355615 *)__this->get_parent_1();
				IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
				EmptyObserver_1_t2731641281 * L_14 = ((EmptyObserver_1_t2731641281_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_Instance_0();
				NullCheck(L_13);
				L_13->set_outObserver_4(L_14);
			}

IL_006d:
			{
				__this->set_unsubscribeTarget_2((Il2CppObject*)NULL);
				__this->set_parent_1((Subject_1_t965355615 *)NULL);
				IL2CPP_LEAVE(0x87, FINALLY_0080);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0080;
		}

FINALLY_0080:
		{ // begin finally (depth: 2)
			Il2CppObject * L_15 = V_1;
			Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_15, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(128)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(128)
		{
			IL2CPP_JUMP_TBL(0x87, IL_0087)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0087:
		{
			IL2CPP_LEAVE(0x93, FINALLY_008c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008c;
	}

FINALLY_008c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_16 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(140)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(140)
	{
		IL2CPP_JUMP_TBL(0x93, IL_0093)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0093:
	{
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Subscription__ctor_m334052977_MetadataUsageId;
extern "C"  void Subscription__ctor_m334052977_gshared (Subscription_t1558173585 * __this, Subject_1_t1421994387 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscription__ctor_m334052977_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Subject_1_t1421994387 * L_1 = ___parent0;
		__this->set_parent_1(L_1);
		Il2CppObject* L_2 = ___unsubscribeTarget1;
		__this->set_unsubscribeTarget_2(L_2);
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UniRx.DictionaryReplaceEvent`2<System.Object,System.Object>>::Dispose()
extern "C"  void Subscription_Dispose_m341106917_gshared (Subscription_t1558173585 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ListObserver_1_t4073544734 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Subject_1_t1421994387 * L_2 = (Subject_1_t1421994387 *)__this->get_parent_1();
			if (!L_2)
			{
				goto IL_0087;
			}
		}

IL_0018:
		{
			Subject_1_t1421994387 * L_3 = (Subject_1_t1421994387 *)__this->get_parent_1();
			NullCheck(L_3);
			Il2CppObject * L_4 = (Il2CppObject *)L_3->get_observerLock_0();
			V_1 = (Il2CppObject *)L_4;
			Il2CppObject * L_5 = V_1;
			Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		}

IL_002a:
		try
		{ // begin try (depth: 2)
			{
				Subject_1_t1421994387 * L_6 = (Subject_1_t1421994387 *)__this->get_parent_1();
				NullCheck(L_6);
				Il2CppObject* L_7 = (Il2CppObject*)L_6->get_outObserver_4();
				V_2 = (ListObserver_1_t4073544734 *)((ListObserver_1_t4073544734 *)IsInst(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
				ListObserver_1_t4073544734 * L_8 = V_2;
				if (!L_8)
				{
					goto IL_005d;
				}
			}

IL_0041:
			{
				Subject_1_t1421994387 * L_9 = (Subject_1_t1421994387 *)__this->get_parent_1();
				ListObserver_1_t4073544734 * L_10 = V_2;
				Il2CppObject* L_11 = (Il2CppObject*)__this->get_unsubscribeTarget_2();
				NullCheck((ListObserver_1_t4073544734 *)L_10);
				Il2CppObject* L_12 = ((  Il2CppObject* (*) (ListObserver_1_t4073544734 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ListObserver_1_t4073544734 *)L_10, (Il2CppObject*)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
				NullCheck(L_9);
				L_9->set_outObserver_4(L_12);
				goto IL_006d;
			}

IL_005d:
			{
				Subject_1_t1421994387 * L_13 = (Subject_1_t1421994387 *)__this->get_parent_1();
				IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
				EmptyObserver_1_t3188280053 * L_14 = ((EmptyObserver_1_t3188280053_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_Instance_0();
				NullCheck(L_13);
				L_13->set_outObserver_4(L_14);
			}

IL_006d:
			{
				__this->set_unsubscribeTarget_2((Il2CppObject*)NULL);
				__this->set_parent_1((Subject_1_t1421994387 *)NULL);
				IL2CPP_LEAVE(0x87, FINALLY_0080);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0080;
		}

FINALLY_0080:
		{ // begin finally (depth: 2)
			Il2CppObject * L_15 = V_1;
			Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_15, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(128)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(128)
		{
			IL2CPP_JUMP_TBL(0x87, IL_0087)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0087:
		{
			IL2CPP_LEAVE(0x93, FINALLY_008c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008c;
	}

FINALLY_008c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_16 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(140)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(140)
	{
		IL2CPP_JUMP_TBL(0x93, IL_0093)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0093:
	{
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UniRx.Tuple`2<System.Object,System.Int32>>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Subscription__ctor_m2532258126_MetadataUsageId;
extern "C"  void Subscription__ctor_m2532258126_gshared (Subscription_t158816708 * __this, Subject_1_t22637510 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscription__ctor_m2532258126_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Subject_1_t22637510 * L_1 = ___parent0;
		__this->set_parent_1(L_1);
		Il2CppObject* L_2 = ___unsubscribeTarget1;
		__this->set_unsubscribeTarget_2(L_2);
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UniRx.Tuple`2<System.Object,System.Int32>>::Dispose()
extern "C"  void Subscription_Dispose_m167027688_gshared (Subscription_t158816708 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ListObserver_1_t2674187857 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Subject_1_t22637510 * L_2 = (Subject_1_t22637510 *)__this->get_parent_1();
			if (!L_2)
			{
				goto IL_0087;
			}
		}

IL_0018:
		{
			Subject_1_t22637510 * L_3 = (Subject_1_t22637510 *)__this->get_parent_1();
			NullCheck(L_3);
			Il2CppObject * L_4 = (Il2CppObject *)L_3->get_observerLock_0();
			V_1 = (Il2CppObject *)L_4;
			Il2CppObject * L_5 = V_1;
			Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		}

IL_002a:
		try
		{ // begin try (depth: 2)
			{
				Subject_1_t22637510 * L_6 = (Subject_1_t22637510 *)__this->get_parent_1();
				NullCheck(L_6);
				Il2CppObject* L_7 = (Il2CppObject*)L_6->get_outObserver_4();
				V_2 = (ListObserver_1_t2674187857 *)((ListObserver_1_t2674187857 *)IsInst(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
				ListObserver_1_t2674187857 * L_8 = V_2;
				if (!L_8)
				{
					goto IL_005d;
				}
			}

IL_0041:
			{
				Subject_1_t22637510 * L_9 = (Subject_1_t22637510 *)__this->get_parent_1();
				ListObserver_1_t2674187857 * L_10 = V_2;
				Il2CppObject* L_11 = (Il2CppObject*)__this->get_unsubscribeTarget_2();
				NullCheck((ListObserver_1_t2674187857 *)L_10);
				Il2CppObject* L_12 = ((  Il2CppObject* (*) (ListObserver_1_t2674187857 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ListObserver_1_t2674187857 *)L_10, (Il2CppObject*)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
				NullCheck(L_9);
				L_9->set_outObserver_4(L_12);
				goto IL_006d;
			}

IL_005d:
			{
				Subject_1_t22637510 * L_13 = (Subject_1_t22637510 *)__this->get_parent_1();
				IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
				EmptyObserver_1_t1788923176 * L_14 = ((EmptyObserver_1_t1788923176_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_Instance_0();
				NullCheck(L_13);
				L_13->set_outObserver_4(L_14);
			}

IL_006d:
			{
				__this->set_unsubscribeTarget_2((Il2CppObject*)NULL);
				__this->set_parent_1((Subject_1_t22637510 *)NULL);
				IL2CPP_LEAVE(0x87, FINALLY_0080);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0080;
		}

FINALLY_0080:
		{ // begin finally (depth: 2)
			Il2CppObject * L_15 = V_1;
			Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_15, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(128)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(128)
		{
			IL2CPP_JUMP_TBL(0x87, IL_0087)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0087:
		{
			IL2CPP_LEAVE(0x93, FINALLY_008c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008c;
	}

FINALLY_008c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_16 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(140)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(140)
	{
		IL2CPP_JUMP_TBL(0x93, IL_0093)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0093:
	{
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UniRx.Tuple`2<System.Object,System.Object>>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Subscription__ctor_m838108267_MetadataUsageId;
extern "C"  void Subscription__ctor_m838108267_gshared (Subscription_t2443475637 * __this, Subject_1_t2307296439 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscription__ctor_m838108267_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Subject_1_t2307296439 * L_1 = ___parent0;
		__this->set_parent_1(L_1);
		Il2CppObject* L_2 = ___unsubscribeTarget1;
		__this->set_unsubscribeTarget_2(L_2);
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UniRx.Tuple`2<System.Object,System.Object>>::Dispose()
extern "C"  void Subscription_Dispose_m1188617899_gshared (Subscription_t2443475637 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ListObserver_1_t663879490 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Subject_1_t2307296439 * L_2 = (Subject_1_t2307296439 *)__this->get_parent_1();
			if (!L_2)
			{
				goto IL_0087;
			}
		}

IL_0018:
		{
			Subject_1_t2307296439 * L_3 = (Subject_1_t2307296439 *)__this->get_parent_1();
			NullCheck(L_3);
			Il2CppObject * L_4 = (Il2CppObject *)L_3->get_observerLock_0();
			V_1 = (Il2CppObject *)L_4;
			Il2CppObject * L_5 = V_1;
			Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		}

IL_002a:
		try
		{ // begin try (depth: 2)
			{
				Subject_1_t2307296439 * L_6 = (Subject_1_t2307296439 *)__this->get_parent_1();
				NullCheck(L_6);
				Il2CppObject* L_7 = (Il2CppObject*)L_6->get_outObserver_4();
				V_2 = (ListObserver_1_t663879490 *)((ListObserver_1_t663879490 *)IsInst(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
				ListObserver_1_t663879490 * L_8 = V_2;
				if (!L_8)
				{
					goto IL_005d;
				}
			}

IL_0041:
			{
				Subject_1_t2307296439 * L_9 = (Subject_1_t2307296439 *)__this->get_parent_1();
				ListObserver_1_t663879490 * L_10 = V_2;
				Il2CppObject* L_11 = (Il2CppObject*)__this->get_unsubscribeTarget_2();
				NullCheck((ListObserver_1_t663879490 *)L_10);
				Il2CppObject* L_12 = ((  Il2CppObject* (*) (ListObserver_1_t663879490 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ListObserver_1_t663879490 *)L_10, (Il2CppObject*)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
				NullCheck(L_9);
				L_9->set_outObserver_4(L_12);
				goto IL_006d;
			}

IL_005d:
			{
				Subject_1_t2307296439 * L_13 = (Subject_1_t2307296439 *)__this->get_parent_1();
				IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
				EmptyObserver_1_t4073582105 * L_14 = ((EmptyObserver_1_t4073582105_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_Instance_0();
				NullCheck(L_13);
				L_13->set_outObserver_4(L_14);
			}

IL_006d:
			{
				__this->set_unsubscribeTarget_2((Il2CppObject*)NULL);
				__this->set_parent_1((Subject_1_t2307296439 *)NULL);
				IL2CPP_LEAVE(0x87, FINALLY_0080);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0080;
		}

FINALLY_0080:
		{ // begin finally (depth: 2)
			Il2CppObject * L_15 = V_1;
			Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_15, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(128)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(128)
		{
			IL2CPP_JUMP_TBL(0x87, IL_0087)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0087:
		{
			IL2CPP_LEAVE(0x93, FINALLY_008c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008c;
	}

FINALLY_008c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_16 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(140)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(140)
	{
		IL2CPP_JUMP_TBL(0x93, IL_0093)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0093:
	{
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Subscription__ctor_m1798832381_MetadataUsageId;
extern "C"  void Subscription__ctor_m1798832381_gshared (Subscription_t4180714101 * __this, Subject_1_t4044534903 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscription__ctor_m1798832381_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Subject_1_t4044534903 * L_1 = ___parent0;
		__this->set_parent_1(L_1);
		Il2CppObject* L_2 = ___unsubscribeTarget1;
		__this->set_unsubscribeTarget_2(L_2);
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UniRx.Tuple`2<System.Object,UnityEngine.NetworkMessageInfo>>::Dispose()
extern "C"  void Subscription_Dispose_m3143010009_gshared (Subscription_t4180714101 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ListObserver_1_t2401117954 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Subject_1_t4044534903 * L_2 = (Subject_1_t4044534903 *)__this->get_parent_1();
			if (!L_2)
			{
				goto IL_0087;
			}
		}

IL_0018:
		{
			Subject_1_t4044534903 * L_3 = (Subject_1_t4044534903 *)__this->get_parent_1();
			NullCheck(L_3);
			Il2CppObject * L_4 = (Il2CppObject *)L_3->get_observerLock_0();
			V_1 = (Il2CppObject *)L_4;
			Il2CppObject * L_5 = V_1;
			Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		}

IL_002a:
		try
		{ // begin try (depth: 2)
			{
				Subject_1_t4044534903 * L_6 = (Subject_1_t4044534903 *)__this->get_parent_1();
				NullCheck(L_6);
				Il2CppObject* L_7 = (Il2CppObject*)L_6->get_outObserver_4();
				V_2 = (ListObserver_1_t2401117954 *)((ListObserver_1_t2401117954 *)IsInst(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
				ListObserver_1_t2401117954 * L_8 = V_2;
				if (!L_8)
				{
					goto IL_005d;
				}
			}

IL_0041:
			{
				Subject_1_t4044534903 * L_9 = (Subject_1_t4044534903 *)__this->get_parent_1();
				ListObserver_1_t2401117954 * L_10 = V_2;
				Il2CppObject* L_11 = (Il2CppObject*)__this->get_unsubscribeTarget_2();
				NullCheck((ListObserver_1_t2401117954 *)L_10);
				Il2CppObject* L_12 = ((  Il2CppObject* (*) (ListObserver_1_t2401117954 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ListObserver_1_t2401117954 *)L_10, (Il2CppObject*)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
				NullCheck(L_9);
				L_9->set_outObserver_4(L_12);
				goto IL_006d;
			}

IL_005d:
			{
				Subject_1_t4044534903 * L_13 = (Subject_1_t4044534903 *)__this->get_parent_1();
				IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
				EmptyObserver_1_t1515853273 * L_14 = ((EmptyObserver_1_t1515853273_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_Instance_0();
				NullCheck(L_13);
				L_13->set_outObserver_4(L_14);
			}

IL_006d:
			{
				__this->set_unsubscribeTarget_2((Il2CppObject*)NULL);
				__this->set_parent_1((Subject_1_t4044534903 *)NULL);
				IL2CPP_LEAVE(0x87, FINALLY_0080);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0080;
		}

FINALLY_0080:
		{ // begin finally (depth: 2)
			Il2CppObject * L_15 = V_1;
			Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_15, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(128)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(128)
		{
			IL2CPP_JUMP_TBL(0x87, IL_0087)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0087:
		{
			IL2CPP_LEAVE(0x93, FINALLY_008c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008c;
	}

FINALLY_008c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_16 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(140)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(140)
	{
		IL2CPP_JUMP_TBL(0x93, IL_0093)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0093:
	{
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UniRx.Unit>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Subscription__ctor_m3804311543_MetadataUsageId;
extern "C"  void Subscription__ctor_m3804311543_gshared (Subscription_t337532561 * __this, Subject_1_t201353362 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscription__ctor_m3804311543_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Subject_1_t201353362 * L_1 = ___parent0;
		__this->set_parent_1(L_1);
		Il2CppObject* L_2 = ___unsubscribeTarget1;
		__this->set_unsubscribeTarget_2(L_2);
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UniRx.Unit>::Dispose()
extern "C"  void Subscription_Dispose_m1294662559_gshared (Subscription_t337532561 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ListObserver_1_t2852903709 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Subject_1_t201353362 * L_2 = (Subject_1_t201353362 *)__this->get_parent_1();
			if (!L_2)
			{
				goto IL_0087;
			}
		}

IL_0018:
		{
			Subject_1_t201353362 * L_3 = (Subject_1_t201353362 *)__this->get_parent_1();
			NullCheck(L_3);
			Il2CppObject * L_4 = (Il2CppObject *)L_3->get_observerLock_0();
			V_1 = (Il2CppObject *)L_4;
			Il2CppObject * L_5 = V_1;
			Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		}

IL_002a:
		try
		{ // begin try (depth: 2)
			{
				Subject_1_t201353362 * L_6 = (Subject_1_t201353362 *)__this->get_parent_1();
				NullCheck(L_6);
				Il2CppObject* L_7 = (Il2CppObject*)L_6->get_outObserver_4();
				V_2 = (ListObserver_1_t2852903709 *)((ListObserver_1_t2852903709 *)IsInst(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
				ListObserver_1_t2852903709 * L_8 = V_2;
				if (!L_8)
				{
					goto IL_005d;
				}
			}

IL_0041:
			{
				Subject_1_t201353362 * L_9 = (Subject_1_t201353362 *)__this->get_parent_1();
				ListObserver_1_t2852903709 * L_10 = V_2;
				Il2CppObject* L_11 = (Il2CppObject*)__this->get_unsubscribeTarget_2();
				NullCheck((ListObserver_1_t2852903709 *)L_10);
				Il2CppObject* L_12 = ((  Il2CppObject* (*) (ListObserver_1_t2852903709 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ListObserver_1_t2852903709 *)L_10, (Il2CppObject*)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
				NullCheck(L_9);
				L_9->set_outObserver_4(L_12);
				goto IL_006d;
			}

IL_005d:
			{
				Subject_1_t201353362 * L_13 = (Subject_1_t201353362 *)__this->get_parent_1();
				IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
				EmptyObserver_1_t1967639028 * L_14 = ((EmptyObserver_1_t1967639028_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_Instance_0();
				NullCheck(L_13);
				L_13->set_outObserver_4(L_14);
			}

IL_006d:
			{
				__this->set_unsubscribeTarget_2((Il2CppObject*)NULL);
				__this->set_parent_1((Subject_1_t201353362 *)NULL);
				IL2CPP_LEAVE(0x87, FINALLY_0080);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0080;
		}

FINALLY_0080:
		{ // begin finally (depth: 2)
			Il2CppObject * L_15 = V_1;
			Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_15, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(128)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(128)
		{
			IL2CPP_JUMP_TBL(0x87, IL_0087)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0087:
		{
			IL2CPP_LEAVE(0x93, FINALLY_008c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008c;
	}

FINALLY_008c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_16 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(140)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(140)
	{
		IL2CPP_JUMP_TBL(0x93, IL_0093)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0093:
	{
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UnityEngine.Bounds>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Subscription__ctor_m440378791_MetadataUsageId;
extern "C"  void Subscription__ctor_m440378791_gshared (Subscription_t1297761500 * __this, Subject_1_t1161582302 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscription__ctor_m440378791_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Subject_1_t1161582302 * L_1 = ___parent0;
		__this->set_parent_1(L_1);
		Il2CppObject* L_2 = ___unsubscribeTarget1;
		__this->set_unsubscribeTarget_2(L_2);
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UnityEngine.Bounds>::Dispose()
extern "C"  void Subscription_Dispose_m554366959_gshared (Subscription_t1297761500 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ListObserver_1_t3813132649 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Subject_1_t1161582302 * L_2 = (Subject_1_t1161582302 *)__this->get_parent_1();
			if (!L_2)
			{
				goto IL_0087;
			}
		}

IL_0018:
		{
			Subject_1_t1161582302 * L_3 = (Subject_1_t1161582302 *)__this->get_parent_1();
			NullCheck(L_3);
			Il2CppObject * L_4 = (Il2CppObject *)L_3->get_observerLock_0();
			V_1 = (Il2CppObject *)L_4;
			Il2CppObject * L_5 = V_1;
			Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		}

IL_002a:
		try
		{ // begin try (depth: 2)
			{
				Subject_1_t1161582302 * L_6 = (Subject_1_t1161582302 *)__this->get_parent_1();
				NullCheck(L_6);
				Il2CppObject* L_7 = (Il2CppObject*)L_6->get_outObserver_4();
				V_2 = (ListObserver_1_t3813132649 *)((ListObserver_1_t3813132649 *)IsInst(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
				ListObserver_1_t3813132649 * L_8 = V_2;
				if (!L_8)
				{
					goto IL_005d;
				}
			}

IL_0041:
			{
				Subject_1_t1161582302 * L_9 = (Subject_1_t1161582302 *)__this->get_parent_1();
				ListObserver_1_t3813132649 * L_10 = V_2;
				Il2CppObject* L_11 = (Il2CppObject*)__this->get_unsubscribeTarget_2();
				NullCheck((ListObserver_1_t3813132649 *)L_10);
				Il2CppObject* L_12 = ((  Il2CppObject* (*) (ListObserver_1_t3813132649 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ListObserver_1_t3813132649 *)L_10, (Il2CppObject*)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
				NullCheck(L_9);
				L_9->set_outObserver_4(L_12);
				goto IL_006d;
			}

IL_005d:
			{
				Subject_1_t1161582302 * L_13 = (Subject_1_t1161582302 *)__this->get_parent_1();
				IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
				EmptyObserver_1_t2927867968 * L_14 = ((EmptyObserver_1_t2927867968_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_Instance_0();
				NullCheck(L_13);
				L_13->set_outObserver_4(L_14);
			}

IL_006d:
			{
				__this->set_unsubscribeTarget_2((Il2CppObject*)NULL);
				__this->set_parent_1((Subject_1_t1161582302 *)NULL);
				IL2CPP_LEAVE(0x87, FINALLY_0080);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0080;
		}

FINALLY_0080:
		{ // begin finally (depth: 2)
			Il2CppObject * L_15 = V_1;
			Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_15, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(128)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(128)
		{
			IL2CPP_JUMP_TBL(0x87, IL_0087)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0087:
		{
			IL2CPP_LEAVE(0x93, FINALLY_008c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008c;
	}

FINALLY_008c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_16 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(140)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(140)
	{
		IL2CPP_JUMP_TBL(0x93, IL_0093)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0093:
	{
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UnityEngine.Color>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Subscription__ctor_m2202885929_MetadataUsageId;
extern "C"  void Subscription__ctor_m2202885929_gshared (Subscription_t3662389578 * __this, Subject_1_t3526210380 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscription__ctor_m2202885929_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Subject_1_t3526210380 * L_1 = ___parent0;
		__this->set_parent_1(L_1);
		Il2CppObject* L_2 = ___unsubscribeTarget1;
		__this->set_unsubscribeTarget_2(L_2);
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UnityEngine.Color>::Dispose()
extern "C"  void Subscription_Dispose_m2434813229_gshared (Subscription_t3662389578 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ListObserver_1_t1882793431 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Subject_1_t3526210380 * L_2 = (Subject_1_t3526210380 *)__this->get_parent_1();
			if (!L_2)
			{
				goto IL_0087;
			}
		}

IL_0018:
		{
			Subject_1_t3526210380 * L_3 = (Subject_1_t3526210380 *)__this->get_parent_1();
			NullCheck(L_3);
			Il2CppObject * L_4 = (Il2CppObject *)L_3->get_observerLock_0();
			V_1 = (Il2CppObject *)L_4;
			Il2CppObject * L_5 = V_1;
			Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		}

IL_002a:
		try
		{ // begin try (depth: 2)
			{
				Subject_1_t3526210380 * L_6 = (Subject_1_t3526210380 *)__this->get_parent_1();
				NullCheck(L_6);
				Il2CppObject* L_7 = (Il2CppObject*)L_6->get_outObserver_4();
				V_2 = (ListObserver_1_t1882793431 *)((ListObserver_1_t1882793431 *)IsInst(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
				ListObserver_1_t1882793431 * L_8 = V_2;
				if (!L_8)
				{
					goto IL_005d;
				}
			}

IL_0041:
			{
				Subject_1_t3526210380 * L_9 = (Subject_1_t3526210380 *)__this->get_parent_1();
				ListObserver_1_t1882793431 * L_10 = V_2;
				Il2CppObject* L_11 = (Il2CppObject*)__this->get_unsubscribeTarget_2();
				NullCheck((ListObserver_1_t1882793431 *)L_10);
				Il2CppObject* L_12 = ((  Il2CppObject* (*) (ListObserver_1_t1882793431 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ListObserver_1_t1882793431 *)L_10, (Il2CppObject*)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
				NullCheck(L_9);
				L_9->set_outObserver_4(L_12);
				goto IL_006d;
			}

IL_005d:
			{
				Subject_1_t3526210380 * L_13 = (Subject_1_t3526210380 *)__this->get_parent_1();
				IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
				EmptyObserver_1_t997528750 * L_14 = ((EmptyObserver_1_t997528750_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_Instance_0();
				NullCheck(L_13);
				L_13->set_outObserver_4(L_14);
			}

IL_006d:
			{
				__this->set_unsubscribeTarget_2((Il2CppObject*)NULL);
				__this->set_parent_1((Subject_1_t3526210380 *)NULL);
				IL2CPP_LEAVE(0x87, FINALLY_0080);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0080;
		}

FINALLY_0080:
		{ // begin finally (depth: 2)
			Il2CppObject * L_15 = V_1;
			Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_15, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(128)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(128)
		{
			IL2CPP_JUMP_TBL(0x87, IL_0087)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0087:
		{
			IL2CPP_LEAVE(0x93, FINALLY_008c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008c;
	}

FINALLY_008c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_16 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(140)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(140)
	{
		IL2CPP_JUMP_TBL(0x93, IL_0093)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0093:
	{
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UnityEngine.MasterServerEvent>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Subscription__ctor_m2652697463_MetadataUsageId;
extern "C"  void Subscription__ctor_m2652697463_gshared (Subscription_t57054012 * __this, Subject_1_t4215842110 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscription__ctor_m2652697463_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Subject_1_t4215842110 * L_1 = ___parent0;
		__this->set_parent_1(L_1);
		Il2CppObject* L_2 = ___unsubscribeTarget1;
		__this->set_unsubscribeTarget_2(L_2);
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UnityEngine.MasterServerEvent>::Dispose()
extern "C"  void Subscription_Dispose_m1756694559_gshared (Subscription_t57054012 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ListObserver_1_t2572425161 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Subject_1_t4215842110 * L_2 = (Subject_1_t4215842110 *)__this->get_parent_1();
			if (!L_2)
			{
				goto IL_0087;
			}
		}

IL_0018:
		{
			Subject_1_t4215842110 * L_3 = (Subject_1_t4215842110 *)__this->get_parent_1();
			NullCheck(L_3);
			Il2CppObject * L_4 = (Il2CppObject *)L_3->get_observerLock_0();
			V_1 = (Il2CppObject *)L_4;
			Il2CppObject * L_5 = V_1;
			Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		}

IL_002a:
		try
		{ // begin try (depth: 2)
			{
				Subject_1_t4215842110 * L_6 = (Subject_1_t4215842110 *)__this->get_parent_1();
				NullCheck(L_6);
				Il2CppObject* L_7 = (Il2CppObject*)L_6->get_outObserver_4();
				V_2 = (ListObserver_1_t2572425161 *)((ListObserver_1_t2572425161 *)IsInst(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
				ListObserver_1_t2572425161 * L_8 = V_2;
				if (!L_8)
				{
					goto IL_005d;
				}
			}

IL_0041:
			{
				Subject_1_t4215842110 * L_9 = (Subject_1_t4215842110 *)__this->get_parent_1();
				ListObserver_1_t2572425161 * L_10 = V_2;
				Il2CppObject* L_11 = (Il2CppObject*)__this->get_unsubscribeTarget_2();
				NullCheck((ListObserver_1_t2572425161 *)L_10);
				Il2CppObject* L_12 = ((  Il2CppObject* (*) (ListObserver_1_t2572425161 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ListObserver_1_t2572425161 *)L_10, (Il2CppObject*)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
				NullCheck(L_9);
				L_9->set_outObserver_4(L_12);
				goto IL_006d;
			}

IL_005d:
			{
				Subject_1_t4215842110 * L_13 = (Subject_1_t4215842110 *)__this->get_parent_1();
				IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
				EmptyObserver_1_t1687160480 * L_14 = ((EmptyObserver_1_t1687160480_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_Instance_0();
				NullCheck(L_13);
				L_13->set_outObserver_4(L_14);
			}

IL_006d:
			{
				__this->set_unsubscribeTarget_2((Il2CppObject*)NULL);
				__this->set_parent_1((Subject_1_t4215842110 *)NULL);
				IL2CPP_LEAVE(0x87, FINALLY_0080);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0080;
		}

FINALLY_0080:
		{ // begin finally (depth: 2)
			Il2CppObject * L_15 = V_1;
			Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_15, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(128)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(128)
		{
			IL2CPP_JUMP_TBL(0x87, IL_0087)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0087:
		{
			IL2CPP_LEAVE(0x93, FINALLY_008c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008c;
	}

FINALLY_008c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_16 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(140)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(140)
	{
		IL2CPP_JUMP_TBL(0x93, IL_0093)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0093:
	{
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UnityEngine.NetworkConnectionError>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Subscription__ctor_m782499200_MetadataUsageId;
extern "C"  void Subscription__ctor_m782499200_gshared (Subscription_t3098019811 * __this, Subject_1_t2961840613 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscription__ctor_m782499200_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Subject_1_t2961840613 * L_1 = ___parent0;
		__this->set_parent_1(L_1);
		Il2CppObject* L_2 = ___unsubscribeTarget1;
		__this->set_unsubscribeTarget_2(L_2);
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UnityEngine.NetworkConnectionError>::Dispose()
extern "C"  void Subscription_Dispose_m1524578422_gshared (Subscription_t3098019811 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ListObserver_1_t1318423664 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Subject_1_t2961840613 * L_2 = (Subject_1_t2961840613 *)__this->get_parent_1();
			if (!L_2)
			{
				goto IL_0087;
			}
		}

IL_0018:
		{
			Subject_1_t2961840613 * L_3 = (Subject_1_t2961840613 *)__this->get_parent_1();
			NullCheck(L_3);
			Il2CppObject * L_4 = (Il2CppObject *)L_3->get_observerLock_0();
			V_1 = (Il2CppObject *)L_4;
			Il2CppObject * L_5 = V_1;
			Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		}

IL_002a:
		try
		{ // begin try (depth: 2)
			{
				Subject_1_t2961840613 * L_6 = (Subject_1_t2961840613 *)__this->get_parent_1();
				NullCheck(L_6);
				Il2CppObject* L_7 = (Il2CppObject*)L_6->get_outObserver_4();
				V_2 = (ListObserver_1_t1318423664 *)((ListObserver_1_t1318423664 *)IsInst(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
				ListObserver_1_t1318423664 * L_8 = V_2;
				if (!L_8)
				{
					goto IL_005d;
				}
			}

IL_0041:
			{
				Subject_1_t2961840613 * L_9 = (Subject_1_t2961840613 *)__this->get_parent_1();
				ListObserver_1_t1318423664 * L_10 = V_2;
				Il2CppObject* L_11 = (Il2CppObject*)__this->get_unsubscribeTarget_2();
				NullCheck((ListObserver_1_t1318423664 *)L_10);
				Il2CppObject* L_12 = ((  Il2CppObject* (*) (ListObserver_1_t1318423664 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ListObserver_1_t1318423664 *)L_10, (Il2CppObject*)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
				NullCheck(L_9);
				L_9->set_outObserver_4(L_12);
				goto IL_006d;
			}

IL_005d:
			{
				Subject_1_t2961840613 * L_13 = (Subject_1_t2961840613 *)__this->get_parent_1();
				IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
				EmptyObserver_1_t433158983 * L_14 = ((EmptyObserver_1_t433158983_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_Instance_0();
				NullCheck(L_13);
				L_13->set_outObserver_4(L_14);
			}

IL_006d:
			{
				__this->set_unsubscribeTarget_2((Il2CppObject*)NULL);
				__this->set_parent_1((Subject_1_t2961840613 *)NULL);
				IL2CPP_LEAVE(0x87, FINALLY_0080);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0080;
		}

FINALLY_0080:
		{ // begin finally (depth: 2)
			Il2CppObject * L_15 = V_1;
			Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_15, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(128)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(128)
		{
			IL2CPP_JUMP_TBL(0x87, IL_0087)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0087:
		{
			IL2CPP_LEAVE(0x93, FINALLY_008c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008c;
	}

FINALLY_008c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_16 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(140)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(140)
	{
		IL2CPP_JUMP_TBL(0x93, IL_0093)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0093:
	{
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UnityEngine.NetworkDisconnection>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Subscription__ctor_m3858878238_MetadataUsageId;
extern "C"  void Subscription__ctor_m3858878238_gshared (Subscription_t2412974213 * __this, Subject_1_t2276795015 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscription__ctor_m3858878238_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Subject_1_t2276795015 * L_1 = ___parent0;
		__this->set_parent_1(L_1);
		Il2CppObject* L_2 = ___unsubscribeTarget1;
		__this->set_unsubscribeTarget_2(L_2);
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UnityEngine.NetworkDisconnection>::Dispose()
extern "C"  void Subscription_Dispose_m2635555864_gshared (Subscription_t2412974213 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ListObserver_1_t633378066 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Subject_1_t2276795015 * L_2 = (Subject_1_t2276795015 *)__this->get_parent_1();
			if (!L_2)
			{
				goto IL_0087;
			}
		}

IL_0018:
		{
			Subject_1_t2276795015 * L_3 = (Subject_1_t2276795015 *)__this->get_parent_1();
			NullCheck(L_3);
			Il2CppObject * L_4 = (Il2CppObject *)L_3->get_observerLock_0();
			V_1 = (Il2CppObject *)L_4;
			Il2CppObject * L_5 = V_1;
			Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		}

IL_002a:
		try
		{ // begin try (depth: 2)
			{
				Subject_1_t2276795015 * L_6 = (Subject_1_t2276795015 *)__this->get_parent_1();
				NullCheck(L_6);
				Il2CppObject* L_7 = (Il2CppObject*)L_6->get_outObserver_4();
				V_2 = (ListObserver_1_t633378066 *)((ListObserver_1_t633378066 *)IsInst(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
				ListObserver_1_t633378066 * L_8 = V_2;
				if (!L_8)
				{
					goto IL_005d;
				}
			}

IL_0041:
			{
				Subject_1_t2276795015 * L_9 = (Subject_1_t2276795015 *)__this->get_parent_1();
				ListObserver_1_t633378066 * L_10 = V_2;
				Il2CppObject* L_11 = (Il2CppObject*)__this->get_unsubscribeTarget_2();
				NullCheck((ListObserver_1_t633378066 *)L_10);
				Il2CppObject* L_12 = ((  Il2CppObject* (*) (ListObserver_1_t633378066 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ListObserver_1_t633378066 *)L_10, (Il2CppObject*)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
				NullCheck(L_9);
				L_9->set_outObserver_4(L_12);
				goto IL_006d;
			}

IL_005d:
			{
				Subject_1_t2276795015 * L_13 = (Subject_1_t2276795015 *)__this->get_parent_1();
				IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
				EmptyObserver_1_t4043080681 * L_14 = ((EmptyObserver_1_t4043080681_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_Instance_0();
				NullCheck(L_13);
				L_13->set_outObserver_4(L_14);
			}

IL_006d:
			{
				__this->set_unsubscribeTarget_2((Il2CppObject*)NULL);
				__this->set_parent_1((Subject_1_t2276795015 *)NULL);
				IL2CPP_LEAVE(0x87, FINALLY_0080);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0080;
		}

FINALLY_0080:
		{ // begin finally (depth: 2)
			Il2CppObject * L_15 = V_1;
			Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_15, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(128)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(128)
		{
			IL2CPP_JUMP_TBL(0x87, IL_0087)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0087:
		{
			IL2CPP_LEAVE(0x93, FINALLY_008c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008c;
	}

FINALLY_008c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_16 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(140)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(140)
	{
		IL2CPP_JUMP_TBL(0x93, IL_0093)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0093:
	{
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UnityEngine.NetworkMessageInfo>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Subscription__ctor_m1995492437_MetadataUsageId;
extern "C"  void Subscription__ctor_m1995492437_gshared (Subscription_t353591406 * __this, Subject_1_t217412208 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscription__ctor_m1995492437_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Subject_1_t217412208 * L_1 = ___parent0;
		__this->set_parent_1(L_1);
		Il2CppObject* L_2 = ___unsubscribeTarget1;
		__this->set_unsubscribeTarget_2(L_2);
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UnityEngine.NetworkMessageInfo>::Dispose()
extern "C"  void Subscription_Dispose_m3474105473_gshared (Subscription_t353591406 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ListObserver_1_t2868962555 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Subject_1_t217412208 * L_2 = (Subject_1_t217412208 *)__this->get_parent_1();
			if (!L_2)
			{
				goto IL_0087;
			}
		}

IL_0018:
		{
			Subject_1_t217412208 * L_3 = (Subject_1_t217412208 *)__this->get_parent_1();
			NullCheck(L_3);
			Il2CppObject * L_4 = (Il2CppObject *)L_3->get_observerLock_0();
			V_1 = (Il2CppObject *)L_4;
			Il2CppObject * L_5 = V_1;
			Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		}

IL_002a:
		try
		{ // begin try (depth: 2)
			{
				Subject_1_t217412208 * L_6 = (Subject_1_t217412208 *)__this->get_parent_1();
				NullCheck(L_6);
				Il2CppObject* L_7 = (Il2CppObject*)L_6->get_outObserver_4();
				V_2 = (ListObserver_1_t2868962555 *)((ListObserver_1_t2868962555 *)IsInst(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
				ListObserver_1_t2868962555 * L_8 = V_2;
				if (!L_8)
				{
					goto IL_005d;
				}
			}

IL_0041:
			{
				Subject_1_t217412208 * L_9 = (Subject_1_t217412208 *)__this->get_parent_1();
				ListObserver_1_t2868962555 * L_10 = V_2;
				Il2CppObject* L_11 = (Il2CppObject*)__this->get_unsubscribeTarget_2();
				NullCheck((ListObserver_1_t2868962555 *)L_10);
				Il2CppObject* L_12 = ((  Il2CppObject* (*) (ListObserver_1_t2868962555 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ListObserver_1_t2868962555 *)L_10, (Il2CppObject*)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
				NullCheck(L_9);
				L_9->set_outObserver_4(L_12);
				goto IL_006d;
			}

IL_005d:
			{
				Subject_1_t217412208 * L_13 = (Subject_1_t217412208 *)__this->get_parent_1();
				IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
				EmptyObserver_1_t1983697874 * L_14 = ((EmptyObserver_1_t1983697874_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_Instance_0();
				NullCheck(L_13);
				L_13->set_outObserver_4(L_14);
			}

IL_006d:
			{
				__this->set_unsubscribeTarget_2((Il2CppObject*)NULL);
				__this->set_parent_1((Subject_1_t217412208 *)NULL);
				IL2CPP_LEAVE(0x87, FINALLY_0080);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0080;
		}

FINALLY_0080:
		{ // begin finally (depth: 2)
			Il2CppObject * L_15 = V_1;
			Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_15, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(128)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(128)
		{
			IL2CPP_JUMP_TBL(0x87, IL_0087)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0087:
		{
			IL2CPP_LEAVE(0x93, FINALLY_008c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008c;
	}

FINALLY_008c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_16 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(140)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(140)
	{
		IL2CPP_JUMP_TBL(0x93, IL_0093)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0093:
	{
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UnityEngine.NetworkPlayer>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Subscription__ctor_m3055518045_MetadataUsageId;
extern "C"  void Subscription__ctor_m3055518045_gshared (Subscription_t3355351190 * __this, Subject_1_t3219171992 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscription__ctor_m3055518045_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Subject_1_t3219171992 * L_1 = ___parent0;
		__this->set_parent_1(L_1);
		Il2CppObject* L_2 = ___unsubscribeTarget1;
		__this->set_unsubscribeTarget_2(L_2);
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UnityEngine.NetworkPlayer>::Dispose()
extern "C"  void Subscription_Dispose_m3054422649_gshared (Subscription_t3355351190 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ListObserver_1_t1575755043 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Subject_1_t3219171992 * L_2 = (Subject_1_t3219171992 *)__this->get_parent_1();
			if (!L_2)
			{
				goto IL_0087;
			}
		}

IL_0018:
		{
			Subject_1_t3219171992 * L_3 = (Subject_1_t3219171992 *)__this->get_parent_1();
			NullCheck(L_3);
			Il2CppObject * L_4 = (Il2CppObject *)L_3->get_observerLock_0();
			V_1 = (Il2CppObject *)L_4;
			Il2CppObject * L_5 = V_1;
			Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		}

IL_002a:
		try
		{ // begin try (depth: 2)
			{
				Subject_1_t3219171992 * L_6 = (Subject_1_t3219171992 *)__this->get_parent_1();
				NullCheck(L_6);
				Il2CppObject* L_7 = (Il2CppObject*)L_6->get_outObserver_4();
				V_2 = (ListObserver_1_t1575755043 *)((ListObserver_1_t1575755043 *)IsInst(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
				ListObserver_1_t1575755043 * L_8 = V_2;
				if (!L_8)
				{
					goto IL_005d;
				}
			}

IL_0041:
			{
				Subject_1_t3219171992 * L_9 = (Subject_1_t3219171992 *)__this->get_parent_1();
				ListObserver_1_t1575755043 * L_10 = V_2;
				Il2CppObject* L_11 = (Il2CppObject*)__this->get_unsubscribeTarget_2();
				NullCheck((ListObserver_1_t1575755043 *)L_10);
				Il2CppObject* L_12 = ((  Il2CppObject* (*) (ListObserver_1_t1575755043 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ListObserver_1_t1575755043 *)L_10, (Il2CppObject*)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
				NullCheck(L_9);
				L_9->set_outObserver_4(L_12);
				goto IL_006d;
			}

IL_005d:
			{
				Subject_1_t3219171992 * L_13 = (Subject_1_t3219171992 *)__this->get_parent_1();
				IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
				EmptyObserver_1_t690490362 * L_14 = ((EmptyObserver_1_t690490362_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_Instance_0();
				NullCheck(L_13);
				L_13->set_outObserver_4(L_14);
			}

IL_006d:
			{
				__this->set_unsubscribeTarget_2((Il2CppObject*)NULL);
				__this->set_parent_1((Subject_1_t3219171992 *)NULL);
				IL2CPP_LEAVE(0x87, FINALLY_0080);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0080;
		}

FINALLY_0080:
		{ // begin finally (depth: 2)
			Il2CppObject * L_15 = V_1;
			Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_15, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(128)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(128)
		{
			IL2CPP_JUMP_TBL(0x87, IL_0087)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0087:
		{
			IL2CPP_LEAVE(0x93, FINALLY_008c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008c;
	}

FINALLY_008c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_16 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(140)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(140)
	{
		IL2CPP_JUMP_TBL(0x93, IL_0093)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0093:
	{
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UnityEngine.Quaternion>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Subscription__ctor_m1897287774_MetadataUsageId;
extern "C"  void Subscription__ctor_m1897287774_gshared (Subscription_t3965929797 * __this, Subject_1_t3829750599 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscription__ctor_m1897287774_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Subject_1_t3829750599 * L_1 = ___parent0;
		__this->set_parent_1(L_1);
		Il2CppObject* L_2 = ___unsubscribeTarget1;
		__this->set_unsubscribeTarget_2(L_2);
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UnityEngine.Quaternion>::Dispose()
extern "C"  void Subscription_Dispose_m2945587928_gshared (Subscription_t3965929797 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ListObserver_1_t2186333650 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Subject_1_t3829750599 * L_2 = (Subject_1_t3829750599 *)__this->get_parent_1();
			if (!L_2)
			{
				goto IL_0087;
			}
		}

IL_0018:
		{
			Subject_1_t3829750599 * L_3 = (Subject_1_t3829750599 *)__this->get_parent_1();
			NullCheck(L_3);
			Il2CppObject * L_4 = (Il2CppObject *)L_3->get_observerLock_0();
			V_1 = (Il2CppObject *)L_4;
			Il2CppObject * L_5 = V_1;
			Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		}

IL_002a:
		try
		{ // begin try (depth: 2)
			{
				Subject_1_t3829750599 * L_6 = (Subject_1_t3829750599 *)__this->get_parent_1();
				NullCheck(L_6);
				Il2CppObject* L_7 = (Il2CppObject*)L_6->get_outObserver_4();
				V_2 = (ListObserver_1_t2186333650 *)((ListObserver_1_t2186333650 *)IsInst(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
				ListObserver_1_t2186333650 * L_8 = V_2;
				if (!L_8)
				{
					goto IL_005d;
				}
			}

IL_0041:
			{
				Subject_1_t3829750599 * L_9 = (Subject_1_t3829750599 *)__this->get_parent_1();
				ListObserver_1_t2186333650 * L_10 = V_2;
				Il2CppObject* L_11 = (Il2CppObject*)__this->get_unsubscribeTarget_2();
				NullCheck((ListObserver_1_t2186333650 *)L_10);
				Il2CppObject* L_12 = ((  Il2CppObject* (*) (ListObserver_1_t2186333650 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ListObserver_1_t2186333650 *)L_10, (Il2CppObject*)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
				NullCheck(L_9);
				L_9->set_outObserver_4(L_12);
				goto IL_006d;
			}

IL_005d:
			{
				Subject_1_t3829750599 * L_13 = (Subject_1_t3829750599 *)__this->get_parent_1();
				IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
				EmptyObserver_1_t1301068969 * L_14 = ((EmptyObserver_1_t1301068969_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_Instance_0();
				NullCheck(L_13);
				L_13->set_outObserver_4(L_14);
			}

IL_006d:
			{
				__this->set_unsubscribeTarget_2((Il2CppObject*)NULL);
				__this->set_parent_1((Subject_1_t3829750599 *)NULL);
				IL2CPP_LEAVE(0x87, FINALLY_0080);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0080;
		}

FINALLY_0080:
		{ // begin finally (depth: 2)
			Il2CppObject * L_15 = V_1;
			Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_15, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(128)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(128)
		{
			IL2CPP_JUMP_TBL(0x87, IL_0087)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0087:
		{
			IL2CPP_LEAVE(0x93, FINALLY_008c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008c;
	}

FINALLY_008c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_16 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(140)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(140)
	{
		IL2CPP_JUMP_TBL(0x93, IL_0093)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0093:
	{
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UnityEngine.Rect>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Subscription__ctor_m2757842008_MetadataUsageId;
extern "C"  void Subscription__ctor_m2757842008_gshared (Subscription_t3599642635 * __this, Subject_1_t3463463437 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscription__ctor_m2757842008_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Subject_1_t3463463437 * L_1 = ___parent0;
		__this->set_parent_1(L_1);
		Il2CppObject* L_2 = ___unsubscribeTarget1;
		__this->set_unsubscribeTarget_2(L_2);
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UnityEngine.Rect>::Dispose()
extern "C"  void Subscription_Dispose_m771725982_gshared (Subscription_t3599642635 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ListObserver_1_t1820046488 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Subject_1_t3463463437 * L_2 = (Subject_1_t3463463437 *)__this->get_parent_1();
			if (!L_2)
			{
				goto IL_0087;
			}
		}

IL_0018:
		{
			Subject_1_t3463463437 * L_3 = (Subject_1_t3463463437 *)__this->get_parent_1();
			NullCheck(L_3);
			Il2CppObject * L_4 = (Il2CppObject *)L_3->get_observerLock_0();
			V_1 = (Il2CppObject *)L_4;
			Il2CppObject * L_5 = V_1;
			Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		}

IL_002a:
		try
		{ // begin try (depth: 2)
			{
				Subject_1_t3463463437 * L_6 = (Subject_1_t3463463437 *)__this->get_parent_1();
				NullCheck(L_6);
				Il2CppObject* L_7 = (Il2CppObject*)L_6->get_outObserver_4();
				V_2 = (ListObserver_1_t1820046488 *)((ListObserver_1_t1820046488 *)IsInst(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
				ListObserver_1_t1820046488 * L_8 = V_2;
				if (!L_8)
				{
					goto IL_005d;
				}
			}

IL_0041:
			{
				Subject_1_t3463463437 * L_9 = (Subject_1_t3463463437 *)__this->get_parent_1();
				ListObserver_1_t1820046488 * L_10 = V_2;
				Il2CppObject* L_11 = (Il2CppObject*)__this->get_unsubscribeTarget_2();
				NullCheck((ListObserver_1_t1820046488 *)L_10);
				Il2CppObject* L_12 = ((  Il2CppObject* (*) (ListObserver_1_t1820046488 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ListObserver_1_t1820046488 *)L_10, (Il2CppObject*)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
				NullCheck(L_9);
				L_9->set_outObserver_4(L_12);
				goto IL_006d;
			}

IL_005d:
			{
				Subject_1_t3463463437 * L_13 = (Subject_1_t3463463437 *)__this->get_parent_1();
				IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
				EmptyObserver_1_t934781807 * L_14 = ((EmptyObserver_1_t934781807_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_Instance_0();
				NullCheck(L_13);
				L_13->set_outObserver_4(L_14);
			}

IL_006d:
			{
				__this->set_unsubscribeTarget_2((Il2CppObject*)NULL);
				__this->set_parent_1((Subject_1_t3463463437 *)NULL);
				IL2CPP_LEAVE(0x87, FINALLY_0080);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0080;
		}

FINALLY_0080:
		{ // begin finally (depth: 2)
			Il2CppObject * L_15 = V_1;
			Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_15, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(128)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(128)
		{
			IL2CPP_JUMP_TBL(0x87, IL_0087)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0087:
		{
			IL2CPP_LEAVE(0x93, FINALLY_008c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008c;
	}

FINALLY_008c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_16 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(140)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(140)
	{
		IL2CPP_JUMP_TBL(0x93, IL_0093)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0093:
	{
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UnityEngine.Vector2>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Subscription__ctor_m3227729597_MetadataUsageId;
extern "C"  void Subscription__ctor_m3227729597_gshared (Subscription_t1304576310 * __this, Subject_1_t1168397112 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscription__ctor_m3227729597_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Subject_1_t1168397112 * L_1 = ___parent0;
		__this->set_parent_1(L_1);
		Il2CppObject* L_2 = ___unsubscribeTarget1;
		__this->set_unsubscribeTarget_2(L_2);
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UnityEngine.Vector2>::Dispose()
extern "C"  void Subscription_Dispose_m2235871513_gshared (Subscription_t1304576310 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ListObserver_1_t3819947459 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Subject_1_t1168397112 * L_2 = (Subject_1_t1168397112 *)__this->get_parent_1();
			if (!L_2)
			{
				goto IL_0087;
			}
		}

IL_0018:
		{
			Subject_1_t1168397112 * L_3 = (Subject_1_t1168397112 *)__this->get_parent_1();
			NullCheck(L_3);
			Il2CppObject * L_4 = (Il2CppObject *)L_3->get_observerLock_0();
			V_1 = (Il2CppObject *)L_4;
			Il2CppObject * L_5 = V_1;
			Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		}

IL_002a:
		try
		{ // begin try (depth: 2)
			{
				Subject_1_t1168397112 * L_6 = (Subject_1_t1168397112 *)__this->get_parent_1();
				NullCheck(L_6);
				Il2CppObject* L_7 = (Il2CppObject*)L_6->get_outObserver_4();
				V_2 = (ListObserver_1_t3819947459 *)((ListObserver_1_t3819947459 *)IsInst(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
				ListObserver_1_t3819947459 * L_8 = V_2;
				if (!L_8)
				{
					goto IL_005d;
				}
			}

IL_0041:
			{
				Subject_1_t1168397112 * L_9 = (Subject_1_t1168397112 *)__this->get_parent_1();
				ListObserver_1_t3819947459 * L_10 = V_2;
				Il2CppObject* L_11 = (Il2CppObject*)__this->get_unsubscribeTarget_2();
				NullCheck((ListObserver_1_t3819947459 *)L_10);
				Il2CppObject* L_12 = ((  Il2CppObject* (*) (ListObserver_1_t3819947459 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ListObserver_1_t3819947459 *)L_10, (Il2CppObject*)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
				NullCheck(L_9);
				L_9->set_outObserver_4(L_12);
				goto IL_006d;
			}

IL_005d:
			{
				Subject_1_t1168397112 * L_13 = (Subject_1_t1168397112 *)__this->get_parent_1();
				IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
				EmptyObserver_1_t2934682778 * L_14 = ((EmptyObserver_1_t2934682778_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_Instance_0();
				NullCheck(L_13);
				L_13->set_outObserver_4(L_14);
			}

IL_006d:
			{
				__this->set_unsubscribeTarget_2((Il2CppObject*)NULL);
				__this->set_parent_1((Subject_1_t1168397112 *)NULL);
				IL2CPP_LEAVE(0x87, FINALLY_0080);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0080;
		}

FINALLY_0080:
		{ // begin finally (depth: 2)
			Il2CppObject * L_15 = V_1;
			Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_15, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(128)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(128)
		{
			IL2CPP_JUMP_TBL(0x87, IL_0087)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0087:
		{
			IL2CPP_LEAVE(0x93, FINALLY_008c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008c;
	}

FINALLY_008c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_16 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(140)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(140)
	{
		IL2CPP_JUMP_TBL(0x93, IL_0093)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0093:
	{
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UnityEngine.Vector3>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Subscription__ctor_m2987189468_MetadataUsageId;
extern "C"  void Subscription__ctor_m2987189468_gshared (Subscription_t1304576311 * __this, Subject_1_t1168397113 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscription__ctor_m2987189468_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Subject_1_t1168397113 * L_1 = ___parent0;
		__this->set_parent_1(L_1);
		Il2CppObject* L_2 = ___unsubscribeTarget1;
		__this->set_unsubscribeTarget_2(L_2);
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UnityEngine.Vector3>::Dispose()
extern "C"  void Subscription_Dispose_m1942468506_gshared (Subscription_t1304576311 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ListObserver_1_t3819947460 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Subject_1_t1168397113 * L_2 = (Subject_1_t1168397113 *)__this->get_parent_1();
			if (!L_2)
			{
				goto IL_0087;
			}
		}

IL_0018:
		{
			Subject_1_t1168397113 * L_3 = (Subject_1_t1168397113 *)__this->get_parent_1();
			NullCheck(L_3);
			Il2CppObject * L_4 = (Il2CppObject *)L_3->get_observerLock_0();
			V_1 = (Il2CppObject *)L_4;
			Il2CppObject * L_5 = V_1;
			Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		}

IL_002a:
		try
		{ // begin try (depth: 2)
			{
				Subject_1_t1168397113 * L_6 = (Subject_1_t1168397113 *)__this->get_parent_1();
				NullCheck(L_6);
				Il2CppObject* L_7 = (Il2CppObject*)L_6->get_outObserver_4();
				V_2 = (ListObserver_1_t3819947460 *)((ListObserver_1_t3819947460 *)IsInst(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
				ListObserver_1_t3819947460 * L_8 = V_2;
				if (!L_8)
				{
					goto IL_005d;
				}
			}

IL_0041:
			{
				Subject_1_t1168397113 * L_9 = (Subject_1_t1168397113 *)__this->get_parent_1();
				ListObserver_1_t3819947460 * L_10 = V_2;
				Il2CppObject* L_11 = (Il2CppObject*)__this->get_unsubscribeTarget_2();
				NullCheck((ListObserver_1_t3819947460 *)L_10);
				Il2CppObject* L_12 = ((  Il2CppObject* (*) (ListObserver_1_t3819947460 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ListObserver_1_t3819947460 *)L_10, (Il2CppObject*)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
				NullCheck(L_9);
				L_9->set_outObserver_4(L_12);
				goto IL_006d;
			}

IL_005d:
			{
				Subject_1_t1168397113 * L_13 = (Subject_1_t1168397113 *)__this->get_parent_1();
				IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
				EmptyObserver_1_t2934682779 * L_14 = ((EmptyObserver_1_t2934682779_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_Instance_0();
				NullCheck(L_13);
				L_13->set_outObserver_4(L_14);
			}

IL_006d:
			{
				__this->set_unsubscribeTarget_2((Il2CppObject*)NULL);
				__this->set_parent_1((Subject_1_t1168397113 *)NULL);
				IL2CPP_LEAVE(0x87, FINALLY_0080);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0080;
		}

FINALLY_0080:
		{ // begin finally (depth: 2)
			Il2CppObject * L_15 = V_1;
			Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_15, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(128)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(128)
		{
			IL2CPP_JUMP_TBL(0x87, IL_0087)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0087:
		{
			IL2CPP_LEAVE(0x93, FINALLY_008c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008c;
	}

FINALLY_008c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_16 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(140)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(140)
	{
		IL2CPP_JUMP_TBL(0x93, IL_0093)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0093:
	{
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UnityEngine.Vector4>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Subscription__ctor_m2746649339_MetadataUsageId;
extern "C"  void Subscription__ctor_m2746649339_gshared (Subscription_t1304576312 * __this, Subject_1_t1168397114 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subscription__ctor_m2746649339_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_gate_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Subject_1_t1168397114 * L_1 = ___parent0;
		__this->set_parent_1(L_1);
		Il2CppObject* L_2 = ___unsubscribeTarget1;
		__this->set_unsubscribeTarget_2(L_2);
		return;
	}
}
// System.Void UniRx.Subject`1/Subscription<UnityEngine.Vector4>::Dispose()
extern "C"  void Subscription_Dispose_m1649065499_gshared (Subscription_t1304576312 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ListObserver_1_t3819947461 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_gate_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Subject_1_t1168397114 * L_2 = (Subject_1_t1168397114 *)__this->get_parent_1();
			if (!L_2)
			{
				goto IL_0087;
			}
		}

IL_0018:
		{
			Subject_1_t1168397114 * L_3 = (Subject_1_t1168397114 *)__this->get_parent_1();
			NullCheck(L_3);
			Il2CppObject * L_4 = (Il2CppObject *)L_3->get_observerLock_0();
			V_1 = (Il2CppObject *)L_4;
			Il2CppObject * L_5 = V_1;
			Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		}

IL_002a:
		try
		{ // begin try (depth: 2)
			{
				Subject_1_t1168397114 * L_6 = (Subject_1_t1168397114 *)__this->get_parent_1();
				NullCheck(L_6);
				Il2CppObject* L_7 = (Il2CppObject*)L_6->get_outObserver_4();
				V_2 = (ListObserver_1_t3819947461 *)((ListObserver_1_t3819947461 *)IsInst(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
				ListObserver_1_t3819947461 * L_8 = V_2;
				if (!L_8)
				{
					goto IL_005d;
				}
			}

IL_0041:
			{
				Subject_1_t1168397114 * L_9 = (Subject_1_t1168397114 *)__this->get_parent_1();
				ListObserver_1_t3819947461 * L_10 = V_2;
				Il2CppObject* L_11 = (Il2CppObject*)__this->get_unsubscribeTarget_2();
				NullCheck((ListObserver_1_t3819947461 *)L_10);
				Il2CppObject* L_12 = ((  Il2CppObject* (*) (ListObserver_1_t3819947461 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ListObserver_1_t3819947461 *)L_10, (Il2CppObject*)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
				NullCheck(L_9);
				L_9->set_outObserver_4(L_12);
				goto IL_006d;
			}

IL_005d:
			{
				Subject_1_t1168397114 * L_13 = (Subject_1_t1168397114 *)__this->get_parent_1();
				IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
				EmptyObserver_1_t2934682780 * L_14 = ((EmptyObserver_1_t2934682780_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_Instance_0();
				NullCheck(L_13);
				L_13->set_outObserver_4(L_14);
			}

IL_006d:
			{
				__this->set_unsubscribeTarget_2((Il2CppObject*)NULL);
				__this->set_parent_1((Subject_1_t1168397114 *)NULL);
				IL2CPP_LEAVE(0x87, FINALLY_0080);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
			goto FINALLY_0080;
		}

FINALLY_0080:
		{ // begin finally (depth: 2)
			Il2CppObject * L_15 = V_1;
			Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_15, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(128)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(128)
		{
			IL2CPP_JUMP_TBL(0x87, IL_0087)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
		}

IL_0087:
		{
			IL2CPP_LEAVE(0x93, FINALLY_008c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008c;
	}

FINALLY_008c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_16 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(140)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(140)
	{
		IL2CPP_JUMP_TBL(0x93, IL_0093)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0093:
	{
		return;
	}
}
// System.Void UniRx.Subject`1<System.Boolean>::.ctor()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Subject_1__ctor_m886839961_MetadataUsageId;
extern "C"  void Subject_1__ctor_m886839961_gshared (Subject_1_t2149039961 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subject_1__ctor_m886839961_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_observerLock_0(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		EmptyObserver_1_t3915325627 * L_1 = ((EmptyObserver_1_t3915325627_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
		__this->set_outObserver_4(L_1);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UniRx.Subject`1<System.Boolean>::get_HasObservers()
extern "C"  bool Subject_1_get_HasObservers_m3612237723_gshared (Subject_1_t2149039961 * __this, const MethodInfo* method)
{
	int32_t G_B4_0 = 0;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_outObserver_4();
		if (((EmptyObserver_1_t3915325627 *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1))))
		{
			goto IL_0026;
		}
	}
	{
		bool L_1 = (bool)__this->get_isStopped_1();
		if (L_1)
		{
			goto IL_0026;
		}
	}
	{
		bool L_2 = (bool)__this->get_isDisposed_2();
		G_B4_0 = ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_0027;
	}

IL_0026:
	{
		G_B4_0 = 0;
	}

IL_0027:
	{
		return (bool)G_B4_0;
	}
}
// System.Void UniRx.Subject`1<System.Boolean>::OnCompleted()
extern "C"  void Subject_1_OnCompleted_m2422552419_gshared (Subject_1_t2149039961 * __this, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_observerLock_0();
		V_1 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			NullCheck((Subject_1_t2149039961 *)__this);
			((  void (*) (Subject_1_t2149039961 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Subject_1_t2149039961 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			bool L_2 = (bool)__this->get_isStopped_1();
			if (!L_2)
			{
				goto IL_0023;
			}
		}

IL_001e:
		{
			IL2CPP_LEAVE(0x4E, FINALLY_0041);
		}

IL_0023:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_outObserver_4();
			V_0 = (Il2CppObject*)L_3;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
			EmptyObserver_1_t3915325627 * L_4 = ((EmptyObserver_1_t3915325627_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
			__this->set_outObserver_4(L_4);
			__this->set_isStopped_1((bool)1);
			IL2CPP_LEAVE(0x48, FINALLY_0041);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0041;
	}

FINALLY_0041:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(65)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(65)
	{
		IL2CPP_JUMP_TBL(0x4E, IL_004e)
		IL2CPP_JUMP_TBL(0x48, IL_0048)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0048:
	{
		Il2CppObject* L_6 = V_0;
		NullCheck((Il2CppObject*)L_6);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Boolean>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_6);
	}

IL_004e:
	{
		return;
	}
}
// System.Void UniRx.Subject`1<System.Boolean>::OnError(System.Exception)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral96784904;
extern const uint32_t Subject_1_OnError_m2694900752_MetadataUsageId;
extern "C"  void Subject_1_OnError_m2694900752_gshared (Subject_1_t2149039961 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subject_1_OnError_m2694900752_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Exception_t1967233988 * L_0 = ___error0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral96784904, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_observerLock_0();
		V_1 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
	}

IL_001e:
	try
	{ // begin try (depth: 1)
		{
			NullCheck((Subject_1_t2149039961 *)__this);
			((  void (*) (Subject_1_t2149039961 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Subject_1_t2149039961 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			bool L_4 = (bool)__this->get_isStopped_1();
			if (!L_4)
			{
				goto IL_0034;
			}
		}

IL_002f:
		{
			IL2CPP_LEAVE(0x67, FINALLY_0059);
		}

IL_0034:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_outObserver_4();
			V_0 = (Il2CppObject*)L_5;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
			EmptyObserver_1_t3915325627 * L_6 = ((EmptyObserver_1_t3915325627_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
			__this->set_outObserver_4(L_6);
			__this->set_isStopped_1((bool)1);
			Exception_t1967233988 * L_7 = ___error0;
			__this->set_lastError_3(L_7);
			IL2CPP_LEAVE(0x60, FINALLY_0059);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0059;
	}

FINALLY_0059:
	{ // begin finally (depth: 1)
		Il2CppObject * L_8 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(89)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(89)
	{
		IL2CPP_JUMP_TBL(0x67, IL_0067)
		IL2CPP_JUMP_TBL(0x60, IL_0060)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0060:
	{
		Il2CppObject* L_9 = V_0;
		Exception_t1967233988 * L_10 = ___error0;
		NullCheck((Il2CppObject*)L_9);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Boolean>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_9, (Exception_t1967233988 *)L_10);
	}

IL_0067:
	{
		return;
	}
}
// System.Void UniRx.Subject`1<System.Boolean>::OnNext(T)
extern "C"  void Subject_1_OnNext_m385413889_gshared (Subject_1_t2149039961 * __this, bool ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_outObserver_4();
		bool L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< bool >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Boolean>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0, (bool)L_1);
		return;
	}
}
// System.IDisposable UniRx.Subject`1<System.Boolean>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral348607190;
extern const uint32_t Subject_1_Subscribe_m3869918062_MetadataUsageId;
extern "C"  Il2CppObject * Subject_1_Subscribe_m3869918062_gshared (Subject_1_t2149039961 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subject_1_Subscribe_m3869918062_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ListObserver_1_t505623012 * V_2 = NULL;
	Il2CppObject* V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___observer0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral348607190, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		V_0 = (Exception_t1967233988 *)NULL;
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_observerLock_0();
		V_1 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
	}

IL_0020:
	try
	{ // begin try (depth: 1)
		{
			NullCheck((Subject_1_t2149039961 *)__this);
			((  void (*) (Subject_1_t2149039961 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Subject_1_t2149039961 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			bool L_4 = (bool)__this->get_isStopped_1();
			if (L_4)
			{
				goto IL_009f;
			}
		}

IL_0031:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_outObserver_4();
			V_2 = (ListObserver_1_t505623012 *)((ListObserver_1_t505623012 *)IsInst(L_5, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)));
			ListObserver_1_t505623012 * L_6 = V_2;
			if (!L_6)
			{
				goto IL_0055;
			}
		}

IL_0043:
		{
			ListObserver_1_t505623012 * L_7 = V_2;
			Il2CppObject* L_8 = ___observer0;
			NullCheck((ListObserver_1_t505623012 *)L_7);
			Il2CppObject* L_9 = ((  Il2CppObject* (*) (ListObserver_1_t505623012 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((ListObserver_1_t505623012 *)L_7, (Il2CppObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			__this->set_outObserver_4(L_9);
			goto IL_0091;
		}

IL_0055:
		{
			Il2CppObject* L_10 = (Il2CppObject*)__this->get_outObserver_4();
			V_3 = (Il2CppObject*)L_10;
			Il2CppObject* L_11 = V_3;
			if (!((EmptyObserver_1_t3915325627 *)IsInst(L_11, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1))))
			{
				goto IL_0073;
			}
		}

IL_0067:
		{
			Il2CppObject* L_12 = ___observer0;
			__this->set_outObserver_4(L_12);
			goto IL_0091;
		}

IL_0073:
		{
			IObserver_1U5BU5D_t3497092061* L_13 = (IObserver_1U5BU5D_t3497092061*)((IObserver_1U5BU5D_t3497092061*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (uint32_t)2));
			Il2CppObject* L_14 = V_3;
			NullCheck(L_13);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 0);
			ArrayElementTypeCheck (L_13, L_14);
			(L_13)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject*)L_14);
			IObserver_1U5BU5D_t3497092061* L_15 = (IObserver_1U5BU5D_t3497092061*)L_13;
			Il2CppObject* L_16 = ___observer0;
			NullCheck(L_15);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 1);
			ArrayElementTypeCheck (L_15, L_16);
			(L_15)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject*)L_16);
			ImmutableList_1_t3483208743 * L_17 = (ImmutableList_1_t3483208743 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
			((  void (*) (ImmutableList_1_t3483208743 *, IObserver_1U5BU5D_t3497092061*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_17, (IObserver_1U5BU5D_t3497092061*)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
			ListObserver_1_t505623012 * L_18 = (ListObserver_1_t505623012 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			((  void (*) (ListObserver_1_t505623012 *, ImmutableList_1_t3483208743 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(L_18, (ImmutableList_1_t3483208743 *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
			__this->set_outObserver_4(L_18);
		}

IL_0091:
		{
			Il2CppObject* L_19 = ___observer0;
			Subscription_t2285219159 * L_20 = (Subscription_t2285219159 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
			((  void (*) (Subscription_t2285219159 *, Subject_1_t2149039961 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_20, (Subject_1_t2149039961 *)__this, (Il2CppObject*)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
			V_4 = (Il2CppObject *)L_20;
			IL2CPP_LEAVE(0xD0, FINALLY_00ab);
		}

IL_009f:
		{
			Exception_t1967233988 * L_21 = (Exception_t1967233988 *)__this->get_lastError_3();
			V_0 = (Exception_t1967233988 *)L_21;
			IL2CPP_LEAVE(0xB2, FINALLY_00ab);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00ab;
	}

FINALLY_00ab:
	{ // begin finally (depth: 1)
		Il2CppObject * L_22 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_22, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(171)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(171)
	{
		IL2CPP_JUMP_TBL(0xD0, IL_00d0)
		IL2CPP_JUMP_TBL(0xB2, IL_00b2)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00b2:
	{
		Exception_t1967233988 * L_23 = V_0;
		if (!L_23)
		{
			goto IL_00c4;
		}
	}
	{
		Il2CppObject* L_24 = ___observer0;
		Exception_t1967233988 * L_25 = V_0;
		NullCheck((Il2CppObject*)L_24);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Boolean>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_24, (Exception_t1967233988 *)L_25);
		goto IL_00ca;
	}

IL_00c4:
	{
		Il2CppObject* L_26 = ___observer0;
		NullCheck((Il2CppObject*)L_26);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Boolean>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_26);
	}

IL_00ca:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_27 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_27;
	}

IL_00d0:
	{
		Il2CppObject * L_28 = V_4;
		return L_28;
	}
}
// System.Void UniRx.Subject`1<System.Boolean>::Dispose()
extern "C"  void Subject_1_Dispose_m1747960982_gshared (Subject_1_t2149039961 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_observerLock_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		__this->set_isDisposed_2((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		DisposedObserver_1_t3680430099 * L_2 = ((DisposedObserver_1_t3680430099_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->static_fields)->get_Instance_0();
		__this->set_outObserver_4(L_2);
		IL2CPP_LEAVE(0x2B, FINALLY_0024);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0024;
	}

FINALLY_0024:
	{ // begin finally (depth: 1)
		Il2CppObject * L_3 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(36)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(36)
	{
		IL2CPP_JUMP_TBL(0x2B, IL_002b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002b:
	{
		return;
	}
}
// System.Void UniRx.Subject`1<System.Boolean>::ThrowIfDisposed()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t Subject_1_ThrowIfDisposed_m492992543_MetadataUsageId;
extern "C"  void Subject_1_ThrowIfDisposed_m492992543_gshared (Subject_1_t2149039961 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subject_1_ThrowIfDisposed_m492992543_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_isDisposed_2();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_2 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0016:
	{
		return;
	}
}
// System.Boolean UniRx.Subject`1<System.Boolean>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool Subject_1_IsRequiredSubscribeOnCurrentThread_m1965702098_gshared (Subject_1_t2149039961 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void UniRx.Subject`1<System.Byte>::.ctor()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Subject_1__ctor_m2689976865_MetadataUsageId;
extern "C"  void Subject_1__ctor_m2689976865_gshared (Subject_1_t421761145 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subject_1__ctor_m2689976865_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_observerLock_0(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		EmptyObserver_1_t2188046811 * L_1 = ((EmptyObserver_1_t2188046811_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
		__this->set_outObserver_4(L_1);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UniRx.Subject`1<System.Byte>::get_HasObservers()
extern "C"  bool Subject_1_get_HasObservers_m1412271051_gshared (Subject_1_t421761145 * __this, const MethodInfo* method)
{
	int32_t G_B4_0 = 0;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_outObserver_4();
		if (((EmptyObserver_1_t2188046811 *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1))))
		{
			goto IL_0026;
		}
	}
	{
		bool L_1 = (bool)__this->get_isStopped_1();
		if (L_1)
		{
			goto IL_0026;
		}
	}
	{
		bool L_2 = (bool)__this->get_isDisposed_2();
		G_B4_0 = ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_0027;
	}

IL_0026:
	{
		G_B4_0 = 0;
	}

IL_0027:
	{
		return (bool)G_B4_0;
	}
}
// System.Void UniRx.Subject`1<System.Byte>::OnCompleted()
extern "C"  void Subject_1_OnCompleted_m971972843_gshared (Subject_1_t421761145 * __this, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_observerLock_0();
		V_1 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			NullCheck((Subject_1_t421761145 *)__this);
			((  void (*) (Subject_1_t421761145 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Subject_1_t421761145 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			bool L_2 = (bool)__this->get_isStopped_1();
			if (!L_2)
			{
				goto IL_0023;
			}
		}

IL_001e:
		{
			IL2CPP_LEAVE(0x4E, FINALLY_0041);
		}

IL_0023:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_outObserver_4();
			V_0 = (Il2CppObject*)L_3;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
			EmptyObserver_1_t2188046811 * L_4 = ((EmptyObserver_1_t2188046811_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
			__this->set_outObserver_4(L_4);
			__this->set_isStopped_1((bool)1);
			IL2CPP_LEAVE(0x48, FINALLY_0041);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0041;
	}

FINALLY_0041:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(65)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(65)
	{
		IL2CPP_JUMP_TBL(0x4E, IL_004e)
		IL2CPP_JUMP_TBL(0x48, IL_0048)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0048:
	{
		Il2CppObject* L_6 = V_0;
		NullCheck((Il2CppObject*)L_6);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Byte>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_6);
	}

IL_004e:
	{
		return;
	}
}
// System.Void UniRx.Subject`1<System.Byte>::OnError(System.Exception)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral96784904;
extern const uint32_t Subject_1_OnError_m993053080_MetadataUsageId;
extern "C"  void Subject_1_OnError_m993053080_gshared (Subject_1_t421761145 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subject_1_OnError_m993053080_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Exception_t1967233988 * L_0 = ___error0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral96784904, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_observerLock_0();
		V_1 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
	}

IL_001e:
	try
	{ // begin try (depth: 1)
		{
			NullCheck((Subject_1_t421761145 *)__this);
			((  void (*) (Subject_1_t421761145 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Subject_1_t421761145 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			bool L_4 = (bool)__this->get_isStopped_1();
			if (!L_4)
			{
				goto IL_0034;
			}
		}

IL_002f:
		{
			IL2CPP_LEAVE(0x67, FINALLY_0059);
		}

IL_0034:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_outObserver_4();
			V_0 = (Il2CppObject*)L_5;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
			EmptyObserver_1_t2188046811 * L_6 = ((EmptyObserver_1_t2188046811_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
			__this->set_outObserver_4(L_6);
			__this->set_isStopped_1((bool)1);
			Exception_t1967233988 * L_7 = ___error0;
			__this->set_lastError_3(L_7);
			IL2CPP_LEAVE(0x60, FINALLY_0059);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0059;
	}

FINALLY_0059:
	{ // begin finally (depth: 1)
		Il2CppObject * L_8 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(89)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(89)
	{
		IL2CPP_JUMP_TBL(0x67, IL_0067)
		IL2CPP_JUMP_TBL(0x60, IL_0060)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0060:
	{
		Il2CppObject* L_9 = V_0;
		Exception_t1967233988 * L_10 = ___error0;
		NullCheck((Il2CppObject*)L_9);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Byte>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_9, (Exception_t1967233988 *)L_10);
	}

IL_0067:
	{
		return;
	}
}
// System.Void UniRx.Subject`1<System.Byte>::OnNext(T)
extern "C"  void Subject_1_OnNext_m2328158345_gshared (Subject_1_t421761145 * __this, uint8_t ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_outObserver_4();
		uint8_t L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< uint8_t >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Byte>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0, (uint8_t)L_1);
		return;
	}
}
// System.IDisposable UniRx.Subject`1<System.Byte>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral348607190;
extern const uint32_t Subject_1_Subscribe_m1101121120_MetadataUsageId;
extern "C"  Il2CppObject * Subject_1_Subscribe_m1101121120_gshared (Subject_1_t421761145 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subject_1_Subscribe_m1101121120_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ListObserver_1_t3073311492 * V_2 = NULL;
	Il2CppObject* V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___observer0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral348607190, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		V_0 = (Exception_t1967233988 *)NULL;
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_observerLock_0();
		V_1 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
	}

IL_0020:
	try
	{ // begin try (depth: 1)
		{
			NullCheck((Subject_1_t421761145 *)__this);
			((  void (*) (Subject_1_t421761145 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Subject_1_t421761145 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			bool L_4 = (bool)__this->get_isStopped_1();
			if (L_4)
			{
				goto IL_009f;
			}
		}

IL_0031:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_outObserver_4();
			V_2 = (ListObserver_1_t3073311492 *)((ListObserver_1_t3073311492 *)IsInst(L_5, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)));
			ListObserver_1_t3073311492 * L_6 = V_2;
			if (!L_6)
			{
				goto IL_0055;
			}
		}

IL_0043:
		{
			ListObserver_1_t3073311492 * L_7 = V_2;
			Il2CppObject* L_8 = ___observer0;
			NullCheck((ListObserver_1_t3073311492 *)L_7);
			Il2CppObject* L_9 = ((  Il2CppObject* (*) (ListObserver_1_t3073311492 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((ListObserver_1_t3073311492 *)L_7, (Il2CppObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			__this->set_outObserver_4(L_9);
			goto IL_0091;
		}

IL_0055:
		{
			Il2CppObject* L_10 = (Il2CppObject*)__this->get_outObserver_4();
			V_3 = (Il2CppObject*)L_10;
			Il2CppObject* L_11 = V_3;
			if (!((EmptyObserver_1_t2188046811 *)IsInst(L_11, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1))))
			{
				goto IL_0073;
			}
		}

IL_0067:
		{
			Il2CppObject* L_12 = ___observer0;
			__this->set_outObserver_4(L_12);
			goto IL_0091;
		}

IL_0073:
		{
			IObserver_1U5BU5D_t4045638205* L_13 = (IObserver_1U5BU5D_t4045638205*)((IObserver_1U5BU5D_t4045638205*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (uint32_t)2));
			Il2CppObject* L_14 = V_3;
			NullCheck(L_13);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 0);
			ArrayElementTypeCheck (L_13, L_14);
			(L_13)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject*)L_14);
			IObserver_1U5BU5D_t4045638205* L_15 = (IObserver_1U5BU5D_t4045638205*)L_13;
			Il2CppObject* L_16 = ___observer0;
			NullCheck(L_15);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 1);
			ArrayElementTypeCheck (L_15, L_16);
			(L_15)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject*)L_16);
			ImmutableList_1_t1755929927 * L_17 = (ImmutableList_1_t1755929927 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
			((  void (*) (ImmutableList_1_t1755929927 *, IObserver_1U5BU5D_t4045638205*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_17, (IObserver_1U5BU5D_t4045638205*)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
			ListObserver_1_t3073311492 * L_18 = (ListObserver_1_t3073311492 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			((  void (*) (ListObserver_1_t3073311492 *, ImmutableList_1_t1755929927 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(L_18, (ImmutableList_1_t1755929927 *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
			__this->set_outObserver_4(L_18);
		}

IL_0091:
		{
			Il2CppObject* L_19 = ___observer0;
			Subscription_t557940343 * L_20 = (Subscription_t557940343 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
			((  void (*) (Subscription_t557940343 *, Subject_1_t421761145 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_20, (Subject_1_t421761145 *)__this, (Il2CppObject*)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
			V_4 = (Il2CppObject *)L_20;
			IL2CPP_LEAVE(0xD0, FINALLY_00ab);
		}

IL_009f:
		{
			Exception_t1967233988 * L_21 = (Exception_t1967233988 *)__this->get_lastError_3();
			V_0 = (Exception_t1967233988 *)L_21;
			IL2CPP_LEAVE(0xB2, FINALLY_00ab);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00ab;
	}

FINALLY_00ab:
	{ // begin finally (depth: 1)
		Il2CppObject * L_22 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_22, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(171)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(171)
	{
		IL2CPP_JUMP_TBL(0xD0, IL_00d0)
		IL2CPP_JUMP_TBL(0xB2, IL_00b2)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00b2:
	{
		Exception_t1967233988 * L_23 = V_0;
		if (!L_23)
		{
			goto IL_00c4;
		}
	}
	{
		Il2CppObject* L_24 = ___observer0;
		Exception_t1967233988 * L_25 = V_0;
		NullCheck((Il2CppObject*)L_24);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Byte>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_24, (Exception_t1967233988 *)L_25);
		goto IL_00ca;
	}

IL_00c4:
	{
		Il2CppObject* L_26 = ___observer0;
		NullCheck((Il2CppObject*)L_26);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Byte>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_26);
	}

IL_00ca:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_27 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_27;
	}

IL_00d0:
	{
		Il2CppObject * L_28 = V_4;
		return L_28;
	}
}
// System.Void UniRx.Subject`1<System.Byte>::Dispose()
extern "C"  void Subject_1_Dispose_m3690705438_gshared (Subject_1_t421761145 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_observerLock_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		__this->set_isDisposed_2((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		DisposedObserver_1_t1953151283 * L_2 = ((DisposedObserver_1_t1953151283_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->static_fields)->get_Instance_0();
		__this->set_outObserver_4(L_2);
		IL2CPP_LEAVE(0x2B, FINALLY_0024);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0024;
	}

FINALLY_0024:
	{ // begin finally (depth: 1)
		Il2CppObject * L_3 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(36)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(36)
	{
		IL2CPP_JUMP_TBL(0x2B, IL_002b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002b:
	{
		return;
	}
}
// System.Void UniRx.Subject`1<System.Byte>::ThrowIfDisposed()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t Subject_1_ThrowIfDisposed_m3041680807_MetadataUsageId;
extern "C"  void Subject_1_ThrowIfDisposed_m3041680807_gshared (Subject_1_t421761145 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subject_1_ThrowIfDisposed_m3041680807_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_isDisposed_2();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_2 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0016:
	{
		return;
	}
}
// System.Boolean UniRx.Subject`1<System.Byte>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool Subject_1_IsRequiredSubscribeOnCurrentThread_m4043881474_gshared (Subject_1_t421761145 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void UniRx.Subject`1<System.Double>::.ctor()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Subject_1__ctor_m862046378_MetadataUsageId;
extern "C"  void Subject_1__ctor_m862046378_gshared (Subject_1_t2472551234 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subject_1__ctor_m862046378_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_observerLock_0(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		EmptyObserver_1_t4238836900 * L_1 = ((EmptyObserver_1_t4238836900_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
		__this->set_outObserver_4(L_1);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UniRx.Subject`1<System.Double>::get_HasObservers()
extern "C"  bool Subject_1_get_HasObservers_m3034142370_gshared (Subject_1_t2472551234 * __this, const MethodInfo* method)
{
	int32_t G_B4_0 = 0;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_outObserver_4();
		if (((EmptyObserver_1_t4238836900 *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1))))
		{
			goto IL_0026;
		}
	}
	{
		bool L_1 = (bool)__this->get_isStopped_1();
		if (L_1)
		{
			goto IL_0026;
		}
	}
	{
		bool L_2 = (bool)__this->get_isDisposed_2();
		G_B4_0 = ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_0027;
	}

IL_0026:
	{
		G_B4_0 = 0;
	}

IL_0027:
	{
		return (bool)G_B4_0;
	}
}
// System.Void UniRx.Subject`1<System.Double>::OnCompleted()
extern "C"  void Subject_1_OnCompleted_m3602535604_gshared (Subject_1_t2472551234 * __this, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_observerLock_0();
		V_1 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			NullCheck((Subject_1_t2472551234 *)__this);
			((  void (*) (Subject_1_t2472551234 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Subject_1_t2472551234 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			bool L_2 = (bool)__this->get_isStopped_1();
			if (!L_2)
			{
				goto IL_0023;
			}
		}

IL_001e:
		{
			IL2CPP_LEAVE(0x4E, FINALLY_0041);
		}

IL_0023:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_outObserver_4();
			V_0 = (Il2CppObject*)L_3;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
			EmptyObserver_1_t4238836900 * L_4 = ((EmptyObserver_1_t4238836900_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
			__this->set_outObserver_4(L_4);
			__this->set_isStopped_1((bool)1);
			IL2CPP_LEAVE(0x48, FINALLY_0041);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0041;
	}

FINALLY_0041:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(65)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(65)
	{
		IL2CPP_JUMP_TBL(0x4E, IL_004e)
		IL2CPP_JUMP_TBL(0x48, IL_0048)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0048:
	{
		Il2CppObject* L_6 = V_0;
		NullCheck((Il2CppObject*)L_6);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Double>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_6);
	}

IL_004e:
	{
		return;
	}
}
// System.Void UniRx.Subject`1<System.Double>::OnError(System.Exception)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral96784904;
extern const uint32_t Subject_1_OnError_m1588041697_MetadataUsageId;
extern "C"  void Subject_1_OnError_m1588041697_gshared (Subject_1_t2472551234 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subject_1_OnError_m1588041697_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Exception_t1967233988 * L_0 = ___error0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral96784904, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_observerLock_0();
		V_1 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
	}

IL_001e:
	try
	{ // begin try (depth: 1)
		{
			NullCheck((Subject_1_t2472551234 *)__this);
			((  void (*) (Subject_1_t2472551234 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Subject_1_t2472551234 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			bool L_4 = (bool)__this->get_isStopped_1();
			if (!L_4)
			{
				goto IL_0034;
			}
		}

IL_002f:
		{
			IL2CPP_LEAVE(0x67, FINALLY_0059);
		}

IL_0034:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_outObserver_4();
			V_0 = (Il2CppObject*)L_5;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
			EmptyObserver_1_t4238836900 * L_6 = ((EmptyObserver_1_t4238836900_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
			__this->set_outObserver_4(L_6);
			__this->set_isStopped_1((bool)1);
			Exception_t1967233988 * L_7 = ___error0;
			__this->set_lastError_3(L_7);
			IL2CPP_LEAVE(0x60, FINALLY_0059);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0059;
	}

FINALLY_0059:
	{ // begin finally (depth: 1)
		Il2CppObject * L_8 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(89)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(89)
	{
		IL2CPP_JUMP_TBL(0x67, IL_0067)
		IL2CPP_JUMP_TBL(0x60, IL_0060)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0060:
	{
		Il2CppObject* L_9 = V_0;
		Exception_t1967233988 * L_10 = ___error0;
		NullCheck((Il2CppObject*)L_9);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Double>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_9, (Exception_t1967233988 *)L_10);
	}

IL_0067:
	{
		return;
	}
}
// System.Void UniRx.Subject`1<System.Double>::OnNext(T)
extern "C"  void Subject_1_OnNext_m2328584402_gshared (Subject_1_t2472551234 * __this, double ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_outObserver_4();
		double L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< double >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Double>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0, (double)L_1);
		return;
	}
}
// System.IDisposable UniRx.Subject`1<System.Double>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral348607190;
extern const uint32_t Subject_1_Subscribe_m178381993_MetadataUsageId;
extern "C"  Il2CppObject * Subject_1_Subscribe_m178381993_gshared (Subject_1_t2472551234 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subject_1_Subscribe_m178381993_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ListObserver_1_t829134285 * V_2 = NULL;
	Il2CppObject* V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___observer0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral348607190, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		V_0 = (Exception_t1967233988 *)NULL;
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_observerLock_0();
		V_1 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
	}

IL_0020:
	try
	{ // begin try (depth: 1)
		{
			NullCheck((Subject_1_t2472551234 *)__this);
			((  void (*) (Subject_1_t2472551234 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Subject_1_t2472551234 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			bool L_4 = (bool)__this->get_isStopped_1();
			if (L_4)
			{
				goto IL_009f;
			}
		}

IL_0031:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_outObserver_4();
			V_2 = (ListObserver_1_t829134285 *)((ListObserver_1_t829134285 *)IsInst(L_5, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)));
			ListObserver_1_t829134285 * L_6 = V_2;
			if (!L_6)
			{
				goto IL_0055;
			}
		}

IL_0043:
		{
			ListObserver_1_t829134285 * L_7 = V_2;
			Il2CppObject* L_8 = ___observer0;
			NullCheck((ListObserver_1_t829134285 *)L_7);
			Il2CppObject* L_9 = ((  Il2CppObject* (*) (ListObserver_1_t829134285 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((ListObserver_1_t829134285 *)L_7, (Il2CppObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			__this->set_outObserver_4(L_9);
			goto IL_0091;
		}

IL_0055:
		{
			Il2CppObject* L_10 = (Il2CppObject*)__this->get_outObserver_4();
			V_3 = (Il2CppObject*)L_10;
			Il2CppObject* L_11 = V_3;
			if (!((EmptyObserver_1_t4238836900 *)IsInst(L_11, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1))))
			{
				goto IL_0073;
			}
		}

IL_0067:
		{
			Il2CppObject* L_12 = ___observer0;
			__this->set_outObserver_4(L_12);
			goto IL_0091;
		}

IL_0073:
		{
			IObserver_1U5BU5D_t740445744* L_13 = (IObserver_1U5BU5D_t740445744*)((IObserver_1U5BU5D_t740445744*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (uint32_t)2));
			Il2CppObject* L_14 = V_3;
			NullCheck(L_13);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 0);
			ArrayElementTypeCheck (L_13, L_14);
			(L_13)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject*)L_14);
			IObserver_1U5BU5D_t740445744* L_15 = (IObserver_1U5BU5D_t740445744*)L_13;
			Il2CppObject* L_16 = ___observer0;
			NullCheck(L_15);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 1);
			ArrayElementTypeCheck (L_15, L_16);
			(L_15)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject*)L_16);
			ImmutableList_1_t3806720016 * L_17 = (ImmutableList_1_t3806720016 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
			((  void (*) (ImmutableList_1_t3806720016 *, IObserver_1U5BU5D_t740445744*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_17, (IObserver_1U5BU5D_t740445744*)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
			ListObserver_1_t829134285 * L_18 = (ListObserver_1_t829134285 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			((  void (*) (ListObserver_1_t829134285 *, ImmutableList_1_t3806720016 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(L_18, (ImmutableList_1_t3806720016 *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
			__this->set_outObserver_4(L_18);
		}

IL_0091:
		{
			Il2CppObject* L_19 = ___observer0;
			Subscription_t2608730432 * L_20 = (Subscription_t2608730432 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
			((  void (*) (Subscription_t2608730432 *, Subject_1_t2472551234 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_20, (Subject_1_t2472551234 *)__this, (Il2CppObject*)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
			V_4 = (Il2CppObject *)L_20;
			IL2CPP_LEAVE(0xD0, FINALLY_00ab);
		}

IL_009f:
		{
			Exception_t1967233988 * L_21 = (Exception_t1967233988 *)__this->get_lastError_3();
			V_0 = (Exception_t1967233988 *)L_21;
			IL2CPP_LEAVE(0xB2, FINALLY_00ab);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00ab;
	}

FINALLY_00ab:
	{ // begin finally (depth: 1)
		Il2CppObject * L_22 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_22, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(171)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(171)
	{
		IL2CPP_JUMP_TBL(0xD0, IL_00d0)
		IL2CPP_JUMP_TBL(0xB2, IL_00b2)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00b2:
	{
		Exception_t1967233988 * L_23 = V_0;
		if (!L_23)
		{
			goto IL_00c4;
		}
	}
	{
		Il2CppObject* L_24 = ___observer0;
		Exception_t1967233988 * L_25 = V_0;
		NullCheck((Il2CppObject*)L_24);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Double>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_24, (Exception_t1967233988 *)L_25);
		goto IL_00ca;
	}

IL_00c4:
	{
		Il2CppObject* L_26 = ___observer0;
		NullCheck((Il2CppObject*)L_26);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Double>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_26);
	}

IL_00ca:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_27 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_27;
	}

IL_00d0:
	{
		Il2CppObject * L_28 = V_4;
		return L_28;
	}
}
// System.Void UniRx.Subject`1<System.Double>::Dispose()
extern "C"  void Subject_1_Dispose_m3691131495_gshared (Subject_1_t2472551234 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_observerLock_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		__this->set_isDisposed_2((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		DisposedObserver_1_t4003941372 * L_2 = ((DisposedObserver_1_t4003941372_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->static_fields)->get_Instance_0();
		__this->set_outObserver_4(L_2);
		IL2CPP_LEAVE(0x2B, FINALLY_0024);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0024;
	}

FINALLY_0024:
	{ // begin finally (depth: 1)
		Il2CppObject * L_3 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(36)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(36)
	{
		IL2CPP_JUMP_TBL(0x2B, IL_002b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002b:
	{
		return;
	}
}
// System.Void UniRx.Subject`1<System.Double>::ThrowIfDisposed()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t Subject_1_ThrowIfDisposed_m3461776624_MetadataUsageId;
extern "C"  void Subject_1_ThrowIfDisposed_m3461776624_gshared (Subject_1_t2472551234 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subject_1_ThrowIfDisposed_m3461776624_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_isDisposed_2();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_2 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0016:
	{
		return;
	}
}
// System.Boolean UniRx.Subject`1<System.Double>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool Subject_1_IsRequiredSubscribeOnCurrentThread_m3358465817_gshared (Subject_1_t2472551234 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void UniRx.Subject`1<System.Int32>::.ctor()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Subject_1__ctor_m1321103487_MetadataUsageId;
extern "C"  void Subject_1__ctor_m1321103487_gshared (Subject_1_t490482111 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subject_1__ctor_m1321103487_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_observerLock_0(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		EmptyObserver_1_t2256767777 * L_1 = ((EmptyObserver_1_t2256767777_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
		__this->set_outObserver_4(L_1);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UniRx.Subject`1<System.Int32>::get_HasObservers()
extern "C"  bool Subject_1_get_HasObservers_m1275029237_gshared (Subject_1_t490482111 * __this, const MethodInfo* method)
{
	int32_t G_B4_0 = 0;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_outObserver_4();
		if (((EmptyObserver_1_t2256767777 *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1))))
		{
			goto IL_0026;
		}
	}
	{
		bool L_1 = (bool)__this->get_isStopped_1();
		if (L_1)
		{
			goto IL_0026;
		}
	}
	{
		bool L_2 = (bool)__this->get_isDisposed_2();
		G_B4_0 = ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_0027;
	}

IL_0026:
	{
		G_B4_0 = 0;
	}

IL_0027:
	{
		return (bool)G_B4_0;
	}
}
// System.Void UniRx.Subject`1<System.Int32>::OnCompleted()
extern "C"  void Subject_1_OnCompleted_m827132105_gshared (Subject_1_t490482111 * __this, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_observerLock_0();
		V_1 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			NullCheck((Subject_1_t490482111 *)__this);
			((  void (*) (Subject_1_t490482111 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Subject_1_t490482111 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			bool L_2 = (bool)__this->get_isStopped_1();
			if (!L_2)
			{
				goto IL_0023;
			}
		}

IL_001e:
		{
			IL2CPP_LEAVE(0x4E, FINALLY_0041);
		}

IL_0023:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_outObserver_4();
			V_0 = (Il2CppObject*)L_3;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
			EmptyObserver_1_t2256767777 * L_4 = ((EmptyObserver_1_t2256767777_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
			__this->set_outObserver_4(L_4);
			__this->set_isStopped_1((bool)1);
			IL2CPP_LEAVE(0x48, FINALLY_0041);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0041;
	}

FINALLY_0041:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(65)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(65)
	{
		IL2CPP_JUMP_TBL(0x4E, IL_004e)
		IL2CPP_JUMP_TBL(0x48, IL_0048)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0048:
	{
		Il2CppObject* L_6 = V_0;
		NullCheck((Il2CppObject*)L_6);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Int32>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_6);
	}

IL_004e:
	{
		return;
	}
}
// System.Void UniRx.Subject`1<System.Int32>::OnError(System.Exception)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral96784904;
extern const uint32_t Subject_1_OnError_m529257590_MetadataUsageId;
extern "C"  void Subject_1_OnError_m529257590_gshared (Subject_1_t490482111 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subject_1_OnError_m529257590_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Exception_t1967233988 * L_0 = ___error0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral96784904, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_observerLock_0();
		V_1 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
	}

IL_001e:
	try
	{ // begin try (depth: 1)
		{
			NullCheck((Subject_1_t490482111 *)__this);
			((  void (*) (Subject_1_t490482111 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Subject_1_t490482111 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			bool L_4 = (bool)__this->get_isStopped_1();
			if (!L_4)
			{
				goto IL_0034;
			}
		}

IL_002f:
		{
			IL2CPP_LEAVE(0x67, FINALLY_0059);
		}

IL_0034:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_outObserver_4();
			V_0 = (Il2CppObject*)L_5;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
			EmptyObserver_1_t2256767777 * L_6 = ((EmptyObserver_1_t2256767777_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
			__this->set_outObserver_4(L_6);
			__this->set_isStopped_1((bool)1);
			Exception_t1967233988 * L_7 = ___error0;
			__this->set_lastError_3(L_7);
			IL2CPP_LEAVE(0x60, FINALLY_0059);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0059;
	}

FINALLY_0059:
	{ // begin finally (depth: 1)
		Il2CppObject * L_8 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(89)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(89)
	{
		IL2CPP_JUMP_TBL(0x67, IL_0067)
		IL2CPP_JUMP_TBL(0x60, IL_0060)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0060:
	{
		Il2CppObject* L_9 = V_0;
		Exception_t1967233988 * L_10 = ___error0;
		NullCheck((Il2CppObject*)L_9);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Int32>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_9, (Exception_t1967233988 *)L_10);
	}

IL_0067:
	{
		return;
	}
}
// System.Void UniRx.Subject`1<System.Int32>::OnNext(T)
extern "C"  void Subject_1_OnNext_m1100834663_gshared (Subject_1_t490482111 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_outObserver_4();
		int32_t L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< int32_t >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Int32>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0, (int32_t)L_1);
		return;
	}
}
// System.IDisposable UniRx.Subject`1<System.Int32>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral348607190;
extern const uint32_t Subject_1_Subscribe_m1838832148_MetadataUsageId;
extern "C"  Il2CppObject * Subject_1_Subscribe_m1838832148_gshared (Subject_1_t490482111 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subject_1_Subscribe_m1838832148_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ListObserver_1_t3142032458 * V_2 = NULL;
	Il2CppObject* V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___observer0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral348607190, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		V_0 = (Exception_t1967233988 *)NULL;
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_observerLock_0();
		V_1 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
	}

IL_0020:
	try
	{ // begin try (depth: 1)
		{
			NullCheck((Subject_1_t490482111 *)__this);
			((  void (*) (Subject_1_t490482111 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Subject_1_t490482111 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			bool L_4 = (bool)__this->get_isStopped_1();
			if (L_4)
			{
				goto IL_009f;
			}
		}

IL_0031:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_outObserver_4();
			V_2 = (ListObserver_1_t3142032458 *)((ListObserver_1_t3142032458 *)IsInst(L_5, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)));
			ListObserver_1_t3142032458 * L_6 = V_2;
			if (!L_6)
			{
				goto IL_0055;
			}
		}

IL_0043:
		{
			ListObserver_1_t3142032458 * L_7 = V_2;
			Il2CppObject* L_8 = ___observer0;
			NullCheck((ListObserver_1_t3142032458 *)L_7);
			Il2CppObject* L_9 = ((  Il2CppObject* (*) (ListObserver_1_t3142032458 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((ListObserver_1_t3142032458 *)L_7, (Il2CppObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			__this->set_outObserver_4(L_9);
			goto IL_0091;
		}

IL_0055:
		{
			Il2CppObject* L_10 = (Il2CppObject*)__this->get_outObserver_4();
			V_3 = (Il2CppObject*)L_10;
			Il2CppObject* L_11 = V_3;
			if (!((EmptyObserver_1_t2256767777 *)IsInst(L_11, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1))))
			{
				goto IL_0073;
			}
		}

IL_0067:
		{
			Il2CppObject* L_12 = ___observer0;
			__this->set_outObserver_4(L_12);
			goto IL_0091;
		}

IL_0073:
		{
			IObserver_1U5BU5D_t1502147871* L_13 = (IObserver_1U5BU5D_t1502147871*)((IObserver_1U5BU5D_t1502147871*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (uint32_t)2));
			Il2CppObject* L_14 = V_3;
			NullCheck(L_13);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 0);
			ArrayElementTypeCheck (L_13, L_14);
			(L_13)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject*)L_14);
			IObserver_1U5BU5D_t1502147871* L_15 = (IObserver_1U5BU5D_t1502147871*)L_13;
			Il2CppObject* L_16 = ___observer0;
			NullCheck(L_15);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 1);
			ArrayElementTypeCheck (L_15, L_16);
			(L_15)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject*)L_16);
			ImmutableList_1_t1824650893 * L_17 = (ImmutableList_1_t1824650893 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
			((  void (*) (ImmutableList_1_t1824650893 *, IObserver_1U5BU5D_t1502147871*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_17, (IObserver_1U5BU5D_t1502147871*)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
			ListObserver_1_t3142032458 * L_18 = (ListObserver_1_t3142032458 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			((  void (*) (ListObserver_1_t3142032458 *, ImmutableList_1_t1824650893 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(L_18, (ImmutableList_1_t1824650893 *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
			__this->set_outObserver_4(L_18);
		}

IL_0091:
		{
			Il2CppObject* L_19 = ___observer0;
			Subscription_t626661309 * L_20 = (Subscription_t626661309 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
			((  void (*) (Subscription_t626661309 *, Subject_1_t490482111 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_20, (Subject_1_t490482111 *)__this, (Il2CppObject*)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
			V_4 = (Il2CppObject *)L_20;
			IL2CPP_LEAVE(0xD0, FINALLY_00ab);
		}

IL_009f:
		{
			Exception_t1967233988 * L_21 = (Exception_t1967233988 *)__this->get_lastError_3();
			V_0 = (Exception_t1967233988 *)L_21;
			IL2CPP_LEAVE(0xB2, FINALLY_00ab);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00ab;
	}

FINALLY_00ab:
	{ // begin finally (depth: 1)
		Il2CppObject * L_22 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_22, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(171)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(171)
	{
		IL2CPP_JUMP_TBL(0xD0, IL_00d0)
		IL2CPP_JUMP_TBL(0xB2, IL_00b2)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00b2:
	{
		Exception_t1967233988 * L_23 = V_0;
		if (!L_23)
		{
			goto IL_00c4;
		}
	}
	{
		Il2CppObject* L_24 = ___observer0;
		Exception_t1967233988 * L_25 = V_0;
		NullCheck((Il2CppObject*)L_24);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Int32>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_24, (Exception_t1967233988 *)L_25);
		goto IL_00ca;
	}

IL_00c4:
	{
		Il2CppObject* L_26 = ___observer0;
		NullCheck((Il2CppObject*)L_26);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Int32>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_26);
	}

IL_00ca:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_27 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_27;
	}

IL_00d0:
	{
		Il2CppObject * L_28 = V_4;
		return L_28;
	}
}
// System.Void UniRx.Subject`1<System.Int32>::Dispose()
extern "C"  void Subject_1_Dispose_m2463381756_gshared (Subject_1_t490482111 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_observerLock_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		__this->set_isDisposed_2((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		DisposedObserver_1_t2021872249 * L_2 = ((DisposedObserver_1_t2021872249_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->static_fields)->get_Instance_0();
		__this->set_outObserver_4(L_2);
		IL2CPP_LEAVE(0x2B, FINALLY_0024);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0024;
	}

FINALLY_0024:
	{ // begin finally (depth: 1)
		Il2CppObject * L_3 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(36)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(36)
	{
		IL2CPP_JUMP_TBL(0x2B, IL_002b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002b:
	{
		return;
	}
}
// System.Void UniRx.Subject`1<System.Int32>::ThrowIfDisposed()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t Subject_1_ThrowIfDisposed_m2039948933_MetadataUsageId;
extern "C"  void Subject_1_ThrowIfDisposed_m2039948933_gshared (Subject_1_t490482111 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subject_1_ThrowIfDisposed_m2039948933_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_isDisposed_2();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_2 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0016:
	{
		return;
	}
}
// System.Boolean UniRx.Subject`1<System.Int32>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool Subject_1_IsRequiredSubscribeOnCurrentThread_m1982718124_gshared (Subject_1_t490482111 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void UniRx.Subject`1<System.Int64>::.ctor()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Subject_1__ctor_m2409416222_MetadataUsageId;
extern "C"  void Subject_1__ctor_m2409416222_gshared (Subject_1_t490482206 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subject_1__ctor_m2409416222_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_observerLock_0(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		EmptyObserver_1_t2256767872 * L_1 = ((EmptyObserver_1_t2256767872_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
		__this->set_outObserver_4(L_1);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UniRx.Subject`1<System.Int64>::get_HasObservers()
extern "C"  bool Subject_1_get_HasObservers_m933210102_gshared (Subject_1_t490482206 * __this, const MethodInfo* method)
{
	int32_t G_B4_0 = 0;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_outObserver_4();
		if (((EmptyObserver_1_t2256767872 *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1))))
		{
			goto IL_0026;
		}
	}
	{
		bool L_1 = (bool)__this->get_isStopped_1();
		if (L_1)
		{
			goto IL_0026;
		}
	}
	{
		bool L_2 = (bool)__this->get_isDisposed_2();
		G_B4_0 = ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_0027;
	}

IL_0026:
	{
		G_B4_0 = 0;
	}

IL_0027:
	{
		return (bool)G_B4_0;
	}
}
// System.Void UniRx.Subject`1<System.Int64>::OnCompleted()
extern "C"  void Subject_1_OnCompleted_m542534440_gshared (Subject_1_t490482206 * __this, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_observerLock_0();
		V_1 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			NullCheck((Subject_1_t490482206 *)__this);
			((  void (*) (Subject_1_t490482206 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Subject_1_t490482206 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			bool L_2 = (bool)__this->get_isStopped_1();
			if (!L_2)
			{
				goto IL_0023;
			}
		}

IL_001e:
		{
			IL2CPP_LEAVE(0x4E, FINALLY_0041);
		}

IL_0023:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_outObserver_4();
			V_0 = (Il2CppObject*)L_3;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
			EmptyObserver_1_t2256767872 * L_4 = ((EmptyObserver_1_t2256767872_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
			__this->set_outObserver_4(L_4);
			__this->set_isStopped_1((bool)1);
			IL2CPP_LEAVE(0x48, FINALLY_0041);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0041;
	}

FINALLY_0041:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(65)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(65)
	{
		IL2CPP_JUMP_TBL(0x4E, IL_004e)
		IL2CPP_JUMP_TBL(0x48, IL_0048)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0048:
	{
		Il2CppObject* L_6 = V_0;
		NullCheck((Il2CppObject*)L_6);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Int64>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_6);
	}

IL_004e:
	{
		return;
	}
}
// System.Void UniRx.Subject`1<System.Int64>::OnError(System.Exception)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral96784904;
extern const uint32_t Subject_1_OnError_m1949409365_MetadataUsageId;
extern "C"  void Subject_1_OnError_m1949409365_gshared (Subject_1_t490482206 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subject_1_OnError_m1949409365_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Exception_t1967233988 * L_0 = ___error0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral96784904, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_observerLock_0();
		V_1 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
	}

IL_001e:
	try
	{ // begin try (depth: 1)
		{
			NullCheck((Subject_1_t490482206 *)__this);
			((  void (*) (Subject_1_t490482206 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Subject_1_t490482206 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			bool L_4 = (bool)__this->get_isStopped_1();
			if (!L_4)
			{
				goto IL_0034;
			}
		}

IL_002f:
		{
			IL2CPP_LEAVE(0x67, FINALLY_0059);
		}

IL_0034:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_outObserver_4();
			V_0 = (Il2CppObject*)L_5;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
			EmptyObserver_1_t2256767872 * L_6 = ((EmptyObserver_1_t2256767872_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
			__this->set_outObserver_4(L_6);
			__this->set_isStopped_1((bool)1);
			Exception_t1967233988 * L_7 = ___error0;
			__this->set_lastError_3(L_7);
			IL2CPP_LEAVE(0x60, FINALLY_0059);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0059;
	}

FINALLY_0059:
	{ // begin finally (depth: 1)
		Il2CppObject * L_8 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(89)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(89)
	{
		IL2CPP_JUMP_TBL(0x67, IL_0067)
		IL2CPP_JUMP_TBL(0x60, IL_0060)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0060:
	{
		Il2CppObject* L_9 = V_0;
		Exception_t1967233988 * L_10 = ___error0;
		NullCheck((Il2CppObject*)L_9);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Int64>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_9, (Exception_t1967233988 *)L_10);
	}

IL_0067:
	{
		return;
	}
}
// System.Void UniRx.Subject`1<System.Int64>::OnNext(T)
extern "C"  void Subject_1_OnNext_m3292320070_gshared (Subject_1_t490482206 * __this, int64_t ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_outObserver_4();
		int64_t L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< int64_t >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Int64>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0, (int64_t)L_1);
		return;
	}
}
// System.IDisposable UniRx.Subject`1<System.Int64>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral348607190;
extern const uint32_t Subject_1_Subscribe_m2550760115_MetadataUsageId;
extern "C"  Il2CppObject * Subject_1_Subscribe_m2550760115_gshared (Subject_1_t490482206 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subject_1_Subscribe_m2550760115_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ListObserver_1_t3142032553 * V_2 = NULL;
	Il2CppObject* V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___observer0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral348607190, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		V_0 = (Exception_t1967233988 *)NULL;
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_observerLock_0();
		V_1 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
	}

IL_0020:
	try
	{ // begin try (depth: 1)
		{
			NullCheck((Subject_1_t490482206 *)__this);
			((  void (*) (Subject_1_t490482206 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Subject_1_t490482206 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			bool L_4 = (bool)__this->get_isStopped_1();
			if (L_4)
			{
				goto IL_009f;
			}
		}

IL_0031:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_outObserver_4();
			V_2 = (ListObserver_1_t3142032553 *)((ListObserver_1_t3142032553 *)IsInst(L_5, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)));
			ListObserver_1_t3142032553 * L_6 = V_2;
			if (!L_6)
			{
				goto IL_0055;
			}
		}

IL_0043:
		{
			ListObserver_1_t3142032553 * L_7 = V_2;
			Il2CppObject* L_8 = ___observer0;
			NullCheck((ListObserver_1_t3142032553 *)L_7);
			Il2CppObject* L_9 = ((  Il2CppObject* (*) (ListObserver_1_t3142032553 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((ListObserver_1_t3142032553 *)L_7, (Il2CppObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			__this->set_outObserver_4(L_9);
			goto IL_0091;
		}

IL_0055:
		{
			Il2CppObject* L_10 = (Il2CppObject*)__this->get_outObserver_4();
			V_3 = (Il2CppObject*)L_10;
			Il2CppObject* L_11 = V_3;
			if (!((EmptyObserver_1_t2256767872 *)IsInst(L_11, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1))))
			{
				goto IL_0073;
			}
		}

IL_0067:
		{
			Il2CppObject* L_12 = ___observer0;
			__this->set_outObserver_4(L_12);
			goto IL_0091;
		}

IL_0073:
		{
			IObserver_1U5BU5D_t445342820* L_13 = (IObserver_1U5BU5D_t445342820*)((IObserver_1U5BU5D_t445342820*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (uint32_t)2));
			Il2CppObject* L_14 = V_3;
			NullCheck(L_13);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 0);
			ArrayElementTypeCheck (L_13, L_14);
			(L_13)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject*)L_14);
			IObserver_1U5BU5D_t445342820* L_15 = (IObserver_1U5BU5D_t445342820*)L_13;
			Il2CppObject* L_16 = ___observer0;
			NullCheck(L_15);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 1);
			ArrayElementTypeCheck (L_15, L_16);
			(L_15)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject*)L_16);
			ImmutableList_1_t1824650988 * L_17 = (ImmutableList_1_t1824650988 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
			((  void (*) (ImmutableList_1_t1824650988 *, IObserver_1U5BU5D_t445342820*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_17, (IObserver_1U5BU5D_t445342820*)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
			ListObserver_1_t3142032553 * L_18 = (ListObserver_1_t3142032553 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			((  void (*) (ListObserver_1_t3142032553 *, ImmutableList_1_t1824650988 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(L_18, (ImmutableList_1_t1824650988 *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
			__this->set_outObserver_4(L_18);
		}

IL_0091:
		{
			Il2CppObject* L_19 = ___observer0;
			Subscription_t626661404 * L_20 = (Subscription_t626661404 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
			((  void (*) (Subscription_t626661404 *, Subject_1_t490482206 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_20, (Subject_1_t490482206 *)__this, (Il2CppObject*)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
			V_4 = (Il2CppObject *)L_20;
			IL2CPP_LEAVE(0xD0, FINALLY_00ab);
		}

IL_009f:
		{
			Exception_t1967233988 * L_21 = (Exception_t1967233988 *)__this->get_lastError_3();
			V_0 = (Exception_t1967233988 *)L_21;
			IL2CPP_LEAVE(0xB2, FINALLY_00ab);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00ab;
	}

FINALLY_00ab:
	{ // begin finally (depth: 1)
		Il2CppObject * L_22 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_22, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(171)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(171)
	{
		IL2CPP_JUMP_TBL(0xD0, IL_00d0)
		IL2CPP_JUMP_TBL(0xB2, IL_00b2)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00b2:
	{
		Exception_t1967233988 * L_23 = V_0;
		if (!L_23)
		{
			goto IL_00c4;
		}
	}
	{
		Il2CppObject* L_24 = ___observer0;
		Exception_t1967233988 * L_25 = V_0;
		NullCheck((Il2CppObject*)L_24);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Int64>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_24, (Exception_t1967233988 *)L_25);
		goto IL_00ca;
	}

IL_00c4:
	{
		Il2CppObject* L_26 = ___observer0;
		NullCheck((Il2CppObject*)L_26);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Int64>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_26);
	}

IL_00ca:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_27 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_27;
	}

IL_00d0:
	{
		Il2CppObject * L_28 = V_4;
		return L_28;
	}
}
// System.Void UniRx.Subject`1<System.Int64>::Dispose()
extern "C"  void Subject_1_Dispose_m359899867_gshared (Subject_1_t490482206 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_observerLock_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		__this->set_isDisposed_2((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		DisposedObserver_1_t2021872344 * L_2 = ((DisposedObserver_1_t2021872344_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->static_fields)->get_Instance_0();
		__this->set_outObserver_4(L_2);
		IL2CPP_LEAVE(0x2B, FINALLY_0024);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0024;
	}

FINALLY_0024:
	{ // begin finally (depth: 1)
		Il2CppObject * L_3 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(36)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(36)
	{
		IL2CPP_JUMP_TBL(0x2B, IL_002b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002b:
	{
		return;
	}
}
// System.Void UniRx.Subject`1<System.Int64>::ThrowIfDisposed()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t Subject_1_ThrowIfDisposed_m643449188_MetadataUsageId;
extern "C"  void Subject_1_ThrowIfDisposed_m643449188_gshared (Subject_1_t490482206 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subject_1_ThrowIfDisposed_m643449188_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_isDisposed_2();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_2 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0016:
	{
		return;
	}
}
// System.Boolean UniRx.Subject`1<System.Int64>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool Subject_1_IsRequiredSubscribeOnCurrentThread_m3393864557_gshared (Subject_1_t490482206 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void UniRx.Subject`1<System.Object>::.ctor()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Subject_1__ctor_m3752525464_MetadataUsageId;
extern "C"  void Subject_1__ctor_m3752525464_gshared (Subject_1_t2775141040 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subject_1__ctor_m3752525464_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_observerLock_0(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		EmptyObserver_1_t246459410 * L_1 = ((EmptyObserver_1_t246459410_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
		__this->set_outObserver_4(L_1);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UniRx.Subject`1<System.Object>::get_HasObservers()
extern "C"  bool Subject_1_get_HasObservers_m2673131508_gshared (Subject_1_t2775141040 * __this, const MethodInfo* method)
{
	int32_t G_B4_0 = 0;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_outObserver_4();
		if (((EmptyObserver_1_t246459410 *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1))))
		{
			goto IL_0026;
		}
	}
	{
		bool L_1 = (bool)__this->get_isStopped_1();
		if (L_1)
		{
			goto IL_0026;
		}
	}
	{
		bool L_2 = (bool)__this->get_isDisposed_2();
		G_B4_0 = ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_0027;
	}

IL_0026:
	{
		G_B4_0 = 0;
	}

IL_0027:
	{
		return (bool)G_B4_0;
	}
}
// System.Void UniRx.Subject`1<System.Object>::OnCompleted()
extern "C"  void Subject_1_OnCompleted_m1083367458_gshared (Subject_1_t2775141040 * __this, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_observerLock_0();
		V_1 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			NullCheck((Subject_1_t2775141040 *)__this);
			((  void (*) (Subject_1_t2775141040 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Subject_1_t2775141040 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			bool L_2 = (bool)__this->get_isStopped_1();
			if (!L_2)
			{
				goto IL_0023;
			}
		}

IL_001e:
		{
			IL2CPP_LEAVE(0x4E, FINALLY_0041);
		}

IL_0023:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_outObserver_4();
			V_0 = (Il2CppObject*)L_3;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
			EmptyObserver_1_t246459410 * L_4 = ((EmptyObserver_1_t246459410_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
			__this->set_outObserver_4(L_4);
			__this->set_isStopped_1((bool)1);
			IL2CPP_LEAVE(0x48, FINALLY_0041);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0041;
	}

FINALLY_0041:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(65)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(65)
	{
		IL2CPP_JUMP_TBL(0x4E, IL_004e)
		IL2CPP_JUMP_TBL(0x48, IL_0048)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0048:
	{
		Il2CppObject* L_6 = V_0;
		NullCheck((Il2CppObject*)L_6);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_6);
	}

IL_004e:
	{
		return;
	}
}
// System.Void UniRx.Subject`1<System.Object>::OnError(System.Exception)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral96784904;
extern const uint32_t Subject_1_OnError_m1700032079_MetadataUsageId;
extern "C"  void Subject_1_OnError_m1700032079_gshared (Subject_1_t2775141040 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subject_1_OnError_m1700032079_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Exception_t1967233988 * L_0 = ___error0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral96784904, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_observerLock_0();
		V_1 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
	}

IL_001e:
	try
	{ // begin try (depth: 1)
		{
			NullCheck((Subject_1_t2775141040 *)__this);
			((  void (*) (Subject_1_t2775141040 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Subject_1_t2775141040 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			bool L_4 = (bool)__this->get_isStopped_1();
			if (!L_4)
			{
				goto IL_0034;
			}
		}

IL_002f:
		{
			IL2CPP_LEAVE(0x67, FINALLY_0059);
		}

IL_0034:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_outObserver_4();
			V_0 = (Il2CppObject*)L_5;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
			EmptyObserver_1_t246459410 * L_6 = ((EmptyObserver_1_t246459410_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
			__this->set_outObserver_4(L_6);
			__this->set_isStopped_1((bool)1);
			Exception_t1967233988 * L_7 = ___error0;
			__this->set_lastError_3(L_7);
			IL2CPP_LEAVE(0x60, FINALLY_0059);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0059;
	}

FINALLY_0059:
	{ // begin finally (depth: 1)
		Il2CppObject * L_8 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(89)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(89)
	{
		IL2CPP_JUMP_TBL(0x67, IL_0067)
		IL2CPP_JUMP_TBL(0x60, IL_0060)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0060:
	{
		Il2CppObject* L_9 = V_0;
		Exception_t1967233988 * L_10 = ___error0;
		NullCheck((Il2CppObject*)L_9);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_9, (Exception_t1967233988 *)L_10);
	}

IL_0067:
	{
		return;
	}
}
// System.Void UniRx.Subject`1<System.Object>::OnNext(T)
extern "C"  void Subject_1_OnNext_m1235145536_gshared (Subject_1_t2775141040 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_outObserver_4();
		Il2CppObject * L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Object>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		return;
	}
}
// System.IDisposable UniRx.Subject`1<System.Object>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral348607190;
extern const uint32_t Subject_1_Subscribe_m2428743831_MetadataUsageId;
extern "C"  Il2CppObject * Subject_1_Subscribe_m2428743831_gshared (Subject_1_t2775141040 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subject_1_Subscribe_m2428743831_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ListObserver_1_t1131724091 * V_2 = NULL;
	Il2CppObject* V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___observer0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral348607190, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		V_0 = (Exception_t1967233988 *)NULL;
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_observerLock_0();
		V_1 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
	}

IL_0020:
	try
	{ // begin try (depth: 1)
		{
			NullCheck((Subject_1_t2775141040 *)__this);
			((  void (*) (Subject_1_t2775141040 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Subject_1_t2775141040 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			bool L_4 = (bool)__this->get_isStopped_1();
			if (L_4)
			{
				goto IL_009f;
			}
		}

IL_0031:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_outObserver_4();
			V_2 = (ListObserver_1_t1131724091 *)((ListObserver_1_t1131724091 *)IsInst(L_5, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)));
			ListObserver_1_t1131724091 * L_6 = V_2;
			if (!L_6)
			{
				goto IL_0055;
			}
		}

IL_0043:
		{
			ListObserver_1_t1131724091 * L_7 = V_2;
			Il2CppObject* L_8 = ___observer0;
			NullCheck((ListObserver_1_t1131724091 *)L_7);
			Il2CppObject* L_9 = ((  Il2CppObject* (*) (ListObserver_1_t1131724091 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((ListObserver_1_t1131724091 *)L_7, (Il2CppObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			__this->set_outObserver_4(L_9);
			goto IL_0091;
		}

IL_0055:
		{
			Il2CppObject* L_10 = (Il2CppObject*)__this->get_outObserver_4();
			V_3 = (Il2CppObject*)L_10;
			Il2CppObject* L_11 = V_3;
			if (!((EmptyObserver_1_t246459410 *)IsInst(L_11, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1))))
			{
				goto IL_0073;
			}
		}

IL_0067:
		{
			Il2CppObject* L_12 = ___observer0;
			__this->set_outObserver_4(L_12);
			goto IL_0091;
		}

IL_0073:
		{
			IObserver_1U5BU5D_t3998655818* L_13 = (IObserver_1U5BU5D_t3998655818*)((IObserver_1U5BU5D_t3998655818*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (uint32_t)2));
			Il2CppObject* L_14 = V_3;
			NullCheck(L_13);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 0);
			ArrayElementTypeCheck (L_13, L_14);
			(L_13)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject*)L_14);
			IObserver_1U5BU5D_t3998655818* L_15 = (IObserver_1U5BU5D_t3998655818*)L_13;
			Il2CppObject* L_16 = ___observer0;
			NullCheck(L_15);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 1);
			ArrayElementTypeCheck (L_15, L_16);
			(L_15)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject*)L_16);
			ImmutableList_1_t4109309822 * L_17 = (ImmutableList_1_t4109309822 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
			((  void (*) (ImmutableList_1_t4109309822 *, IObserver_1U5BU5D_t3998655818*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_17, (IObserver_1U5BU5D_t3998655818*)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
			ListObserver_1_t1131724091 * L_18 = (ListObserver_1_t1131724091 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			((  void (*) (ListObserver_1_t1131724091 *, ImmutableList_1_t4109309822 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(L_18, (ImmutableList_1_t4109309822 *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
			__this->set_outObserver_4(L_18);
		}

IL_0091:
		{
			Il2CppObject* L_19 = ___observer0;
			Subscription_t2911320239 * L_20 = (Subscription_t2911320239 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
			((  void (*) (Subscription_t2911320239 *, Subject_1_t2775141040 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_20, (Subject_1_t2775141040 *)__this, (Il2CppObject*)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
			V_4 = (Il2CppObject *)L_20;
			IL2CPP_LEAVE(0xD0, FINALLY_00ab);
		}

IL_009f:
		{
			Exception_t1967233988 * L_21 = (Exception_t1967233988 *)__this->get_lastError_3();
			V_0 = (Exception_t1967233988 *)L_21;
			IL2CPP_LEAVE(0xB2, FINALLY_00ab);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00ab;
	}

FINALLY_00ab:
	{ // begin finally (depth: 1)
		Il2CppObject * L_22 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_22, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(171)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(171)
	{
		IL2CPP_JUMP_TBL(0xD0, IL_00d0)
		IL2CPP_JUMP_TBL(0xB2, IL_00b2)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00b2:
	{
		Exception_t1967233988 * L_23 = V_0;
		if (!L_23)
		{
			goto IL_00c4;
		}
	}
	{
		Il2CppObject* L_24 = ___observer0;
		Exception_t1967233988 * L_25 = V_0;
		NullCheck((Il2CppObject*)L_24);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Object>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_24, (Exception_t1967233988 *)L_25);
		goto IL_00ca;
	}

IL_00c4:
	{
		Il2CppObject* L_26 = ___observer0;
		NullCheck((Il2CppObject*)L_26);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Object>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_26);
	}

IL_00ca:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_27 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_27;
	}

IL_00d0:
	{
		Il2CppObject * L_28 = V_4;
		return L_28;
	}
}
// System.Void UniRx.Subject`1<System.Object>::Dispose()
extern "C"  void Subject_1_Dispose_m2597692629_gshared (Subject_1_t2775141040 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_observerLock_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		__this->set_isDisposed_2((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		DisposedObserver_1_t11563882 * L_2 = ((DisposedObserver_1_t11563882_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->static_fields)->get_Instance_0();
		__this->set_outObserver_4(L_2);
		IL2CPP_LEAVE(0x2B, FINALLY_0024);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0024;
	}

FINALLY_0024:
	{ // begin finally (depth: 1)
		Il2CppObject * L_3 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(36)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(36)
	{
		IL2CPP_JUMP_TBL(0x2B, IL_002b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002b:
	{
		return;
	}
}
// System.Void UniRx.Subject`1<System.Object>::ThrowIfDisposed()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t Subject_1_ThrowIfDisposed_m956279134_MetadataUsageId;
extern "C"  void Subject_1_ThrowIfDisposed_m956279134_gshared (Subject_1_t2775141040 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subject_1_ThrowIfDisposed_m956279134_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_isDisposed_2();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_2 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0016:
	{
		return;
	}
}
// System.Boolean UniRx.Subject`1<System.Object>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool Subject_1_IsRequiredSubscribeOnCurrentThread_m3969161195_gshared (Subject_1_t2775141040 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void UniRx.Subject`1<System.Single>::.ctor()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Subject_1__ctor_m3410487393_MetadataUsageId;
extern "C"  void Subject_1__ctor_m3410487393_gshared (Subject_1_t2896243641 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subject_1__ctor_m3410487393_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_observerLock_0(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		EmptyObserver_1_t367562011 * L_1 = ((EmptyObserver_1_t367562011_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
		__this->set_outObserver_4(L_1);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UniRx.Subject`1<System.Single>::get_HasObservers()
extern "C"  bool Subject_1_get_HasObservers_m1412894347_gshared (Subject_1_t2896243641 * __this, const MethodInfo* method)
{
	int32_t G_B4_0 = 0;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_outObserver_4();
		if (((EmptyObserver_1_t367562011 *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1))))
		{
			goto IL_0026;
		}
	}
	{
		bool L_1 = (bool)__this->get_isStopped_1();
		if (L_1)
		{
			goto IL_0026;
		}
	}
	{
		bool L_2 = (bool)__this->get_isDisposed_2();
		G_B4_0 = ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_0027;
	}

IL_0026:
	{
		G_B4_0 = 0;
	}

IL_0027:
	{
		return (bool)G_B4_0;
	}
}
// System.Void UniRx.Subject`1<System.Single>::OnCompleted()
extern "C"  void Subject_1_OnCompleted_m467767083_gshared (Subject_1_t2896243641 * __this, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_observerLock_0();
		V_1 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			NullCheck((Subject_1_t2896243641 *)__this);
			((  void (*) (Subject_1_t2896243641 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Subject_1_t2896243641 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			bool L_2 = (bool)__this->get_isStopped_1();
			if (!L_2)
			{
				goto IL_0023;
			}
		}

IL_001e:
		{
			IL2CPP_LEAVE(0x4E, FINALLY_0041);
		}

IL_0023:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_outObserver_4();
			V_0 = (Il2CppObject*)L_3;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
			EmptyObserver_1_t367562011 * L_4 = ((EmptyObserver_1_t367562011_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
			__this->set_outObserver_4(L_4);
			__this->set_isStopped_1((bool)1);
			IL2CPP_LEAVE(0x48, FINALLY_0041);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0041;
	}

FINALLY_0041:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(65)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(65)
	{
		IL2CPP_JUMP_TBL(0x4E, IL_004e)
		IL2CPP_JUMP_TBL(0x48, IL_0048)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0048:
	{
		Il2CppObject* L_6 = V_0;
		NullCheck((Il2CppObject*)L_6);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Single>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_6);
	}

IL_004e:
	{
		return;
	}
}
// System.Void UniRx.Subject`1<System.Single>::OnError(System.Exception)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral96784904;
extern const uint32_t Subject_1_OnError_m2947536856_MetadataUsageId;
extern "C"  void Subject_1_OnError_m2947536856_gshared (Subject_1_t2896243641 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subject_1_OnError_m2947536856_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Exception_t1967233988 * L_0 = ___error0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral96784904, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_observerLock_0();
		V_1 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
	}

IL_001e:
	try
	{ // begin try (depth: 1)
		{
			NullCheck((Subject_1_t2896243641 *)__this);
			((  void (*) (Subject_1_t2896243641 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Subject_1_t2896243641 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			bool L_4 = (bool)__this->get_isStopped_1();
			if (!L_4)
			{
				goto IL_0034;
			}
		}

IL_002f:
		{
			IL2CPP_LEAVE(0x67, FINALLY_0059);
		}

IL_0034:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_outObserver_4();
			V_0 = (Il2CppObject*)L_5;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
			EmptyObserver_1_t367562011 * L_6 = ((EmptyObserver_1_t367562011_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Instance_0();
			__this->set_outObserver_4(L_6);
			__this->set_isStopped_1((bool)1);
			Exception_t1967233988 * L_7 = ___error0;
			__this->set_lastError_3(L_7);
			IL2CPP_LEAVE(0x60, FINALLY_0059);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0059;
	}

FINALLY_0059:
	{ // begin finally (depth: 1)
		Il2CppObject * L_8 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(89)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(89)
	{
		IL2CPP_JUMP_TBL(0x67, IL_0067)
		IL2CPP_JUMP_TBL(0x60, IL_0060)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0060:
	{
		Il2CppObject* L_9 = V_0;
		Exception_t1967233988 * L_10 = ___error0;
		NullCheck((Il2CppObject*)L_9);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Single>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_9, (Exception_t1967233988 *)L_10);
	}

IL_0067:
	{
		return;
	}
}
// System.Void UniRx.Subject`1<System.Single>::OnNext(T)
extern "C"  void Subject_1_OnNext_m3249041097_gshared (Subject_1_t2896243641 * __this, float ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_outObserver_4();
		float L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< float >::Invoke(2 /* System.Void UniRx.IObserver`1<System.Single>::OnNext(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0, (float)L_1);
		return;
	}
}
// System.IDisposable UniRx.Subject`1<System.Single>::Subscribe(UniRx.IObserver`1<T>)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppClass* Disposable_t3736388786_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral348607190;
extern const uint32_t Subject_1_Subscribe_m677515104_MetadataUsageId;
extern "C"  Il2CppObject * Subject_1_Subscribe_m677515104_gshared (Subject_1_t2896243641 * __this, Il2CppObject* ___observer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subject_1_Subscribe_m677515104_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ListObserver_1_t1252826692 * V_2 = NULL;
	Il2CppObject* V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___observer0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral348607190, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		V_0 = (Exception_t1967233988 *)NULL;
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_observerLock_0();
		V_1 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = V_1;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
	}

IL_0020:
	try
	{ // begin try (depth: 1)
		{
			NullCheck((Subject_1_t2896243641 *)__this);
			((  void (*) (Subject_1_t2896243641 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Subject_1_t2896243641 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
			bool L_4 = (bool)__this->get_isStopped_1();
			if (L_4)
			{
				goto IL_009f;
			}
		}

IL_0031:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_outObserver_4();
			V_2 = (ListObserver_1_t1252826692 *)((ListObserver_1_t1252826692 *)IsInst(L_5, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)));
			ListObserver_1_t1252826692 * L_6 = V_2;
			if (!L_6)
			{
				goto IL_0055;
			}
		}

IL_0043:
		{
			ListObserver_1_t1252826692 * L_7 = V_2;
			Il2CppObject* L_8 = ___observer0;
			NullCheck((ListObserver_1_t1252826692 *)L_7);
			Il2CppObject* L_9 = ((  Il2CppObject* (*) (ListObserver_1_t1252826692 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((ListObserver_1_t1252826692 *)L_7, (Il2CppObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			__this->set_outObserver_4(L_9);
			goto IL_0091;
		}

IL_0055:
		{
			Il2CppObject* L_10 = (Il2CppObject*)__this->get_outObserver_4();
			V_3 = (Il2CppObject*)L_10;
			Il2CppObject* L_11 = V_3;
			if (!((EmptyObserver_1_t367562011 *)IsInst(L_11, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1))))
			{
				goto IL_0073;
			}
		}

IL_0067:
		{
			Il2CppObject* L_12 = ___observer0;
			__this->set_outObserver_4(L_12);
			goto IL_0091;
		}

IL_0073:
		{
			IObserver_1U5BU5D_t911596029* L_13 = (IObserver_1U5BU5D_t911596029*)((IObserver_1U5BU5D_t911596029*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (uint32_t)2));
			Il2CppObject* L_14 = V_3;
			NullCheck(L_13);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 0);
			ArrayElementTypeCheck (L_13, L_14);
			(L_13)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject*)L_14);
			IObserver_1U5BU5D_t911596029* L_15 = (IObserver_1U5BU5D_t911596029*)L_13;
			Il2CppObject* L_16 = ___observer0;
			NullCheck(L_15);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 1);
			ArrayElementTypeCheck (L_15, L_16);
			(L_15)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject*)L_16);
			ImmutableList_1_t4230412423 * L_17 = (ImmutableList_1_t4230412423 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
			((  void (*) (ImmutableList_1_t4230412423 *, IObserver_1U5BU5D_t911596029*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_17, (IObserver_1U5BU5D_t911596029*)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
			ListObserver_1_t1252826692 * L_18 = (ListObserver_1_t1252826692 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			((  void (*) (ListObserver_1_t1252826692 *, ImmutableList_1_t4230412423 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(L_18, (ImmutableList_1_t4230412423 *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
			__this->set_outObserver_4(L_18);
		}

IL_0091:
		{
			Il2CppObject* L_19 = ___observer0;
			Subscription_t3032422839 * L_20 = (Subscription_t3032422839 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
			((  void (*) (Subscription_t3032422839 *, Subject_1_t2896243641 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(L_20, (Subject_1_t2896243641 *)__this, (Il2CppObject*)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
			V_4 = (Il2CppObject *)L_20;
			IL2CPP_LEAVE(0xD0, FINALLY_00ab);
		}

IL_009f:
		{
			Exception_t1967233988 * L_21 = (Exception_t1967233988 *)__this->get_lastError_3();
			V_0 = (Exception_t1967233988 *)L_21;
			IL2CPP_LEAVE(0xB2, FINALLY_00ab);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00ab;
	}

FINALLY_00ab:
	{ // begin finally (depth: 1)
		Il2CppObject * L_22 = V_1;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_22, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(171)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(171)
	{
		IL2CPP_JUMP_TBL(0xD0, IL_00d0)
		IL2CPP_JUMP_TBL(0xB2, IL_00b2)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00b2:
	{
		Exception_t1967233988 * L_23 = V_0;
		if (!L_23)
		{
			goto IL_00c4;
		}
	}
	{
		Il2CppObject* L_24 = ___observer0;
		Exception_t1967233988 * L_25 = V_0;
		NullCheck((Il2CppObject*)L_24);
		InterfaceActionInvoker1< Exception_t1967233988 * >::Invoke(1 /* System.Void UniRx.IObserver`1<System.Single>::OnError(System.Exception) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_24, (Exception_t1967233988 *)L_25);
		goto IL_00ca;
	}

IL_00c4:
	{
		Il2CppObject* L_26 = ___observer0;
		NullCheck((Il2CppObject*)L_26);
		InterfaceActionInvoker0::Invoke(0 /* System.Void UniRx.IObserver`1<System.Single>::OnCompleted() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_26);
	}

IL_00ca:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Disposable_t3736388786_il2cpp_TypeInfo_var);
		Il2CppObject * L_27 = ((Disposable_t3736388786_StaticFields*)Disposable_t3736388786_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
		return L_27;
	}

IL_00d0:
	{
		Il2CppObject * L_28 = V_4;
		return L_28;
	}
}
// System.Void UniRx.Subject`1<System.Single>::Dispose()
extern "C"  void Subject_1_Dispose_m316620894_gshared (Subject_1_t2896243641 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_observerLock_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		__this->set_isDisposed_2((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		DisposedObserver_1_t132666483 * L_2 = ((DisposedObserver_1_t132666483_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->static_fields)->get_Instance_0();
		__this->set_outObserver_4(L_2);
		IL2CPP_LEAVE(0x2B, FINALLY_0024);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0024;
	}

FINALLY_0024:
	{ // begin finally (depth: 1)
		Il2CppObject * L_3 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(36)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(36)
	{
		IL2CPP_JUMP_TBL(0x2B, IL_002b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_002b:
	{
		return;
	}
}
// System.Void UniRx.Subject`1<System.Single>::ThrowIfDisposed()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t Subject_1_ThrowIfDisposed_m1608362983_MetadataUsageId;
extern "C"  void Subject_1_ThrowIfDisposed_m1608362983_gshared (Subject_1_t2896243641 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Subject_1_ThrowIfDisposed_m1608362983_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_isDisposed_2();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ObjectDisposedException_t973246880 * L_2 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0016:
	{
		return;
	}
}
// System.Boolean UniRx.Subject`1<System.Single>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool Subject_1_IsRequiredSubscribeOnCurrentThread_m641666754_gshared (Subject_1_t2896243641 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
