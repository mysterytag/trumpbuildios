﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIHidder/<Move>c__Iterator28
struct U3CMoveU3Ec__Iterator28_t3571380998;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UIHidder/<Move>c__Iterator28::.ctor()
extern "C"  void U3CMoveU3Ec__Iterator28__ctor_m1910392802 (U3CMoveU3Ec__Iterator28_t3571380998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UIHidder/<Move>c__Iterator28::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CMoveU3Ec__Iterator28_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2111126970 (U3CMoveU3Ec__Iterator28_t3571380998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UIHidder/<Move>c__Iterator28::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CMoveU3Ec__Iterator28_System_Collections_IEnumerator_get_Current_m2342384974 (U3CMoveU3Ec__Iterator28_t3571380998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIHidder/<Move>c__Iterator28::MoveNext()
extern "C"  bool U3CMoveU3Ec__Iterator28_MoveNext_m373945530 (U3CMoveU3Ec__Iterator28_t3571380998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIHidder/<Move>c__Iterator28::Dispose()
extern "C"  void U3CMoveU3Ec__Iterator28_Dispose_m1834730399 (U3CMoveU3Ec__Iterator28_t3571380998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIHidder/<Move>c__Iterator28::Reset()
extern "C"  void U3CMoveU3Ec__Iterator28_Reset_m3851793039 (U3CMoveU3Ec__Iterator28_t3571380998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
