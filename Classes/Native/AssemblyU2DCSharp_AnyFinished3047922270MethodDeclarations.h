﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnyFinished
struct AnyFinished_t3047922270;
// IProcess
struct IProcess_t4026237414;

#include "codegen/il2cpp-codegen.h"

// System.Void AnyFinished::.ctor()
extern "C"  void AnyFinished__ctor_m2934221517 (AnyFinished_t3047922270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AnyFinished::get_finished()
extern "C"  bool AnyFinished_get_finished_m478991358 (AnyFinished_t3047922270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AnyFinished::<get_finished>m__1E9(IProcess)
extern "C"  bool AnyFinished_U3Cget_finishedU3Em__1E9_m406069340 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
