﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.CountNotifier
struct CountNotifier_t4027919079;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.CountNotifier/<Increment>c__AnonStorey57
struct  U3CIncrementU3Ec__AnonStorey57_t1898758910  : public Il2CppObject
{
public:
	// System.Int32 UniRx.CountNotifier/<Increment>c__AnonStorey57::incrementCount
	int32_t ___incrementCount_0;
	// UniRx.CountNotifier UniRx.CountNotifier/<Increment>c__AnonStorey57::<>f__this
	CountNotifier_t4027919079 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_incrementCount_0() { return static_cast<int32_t>(offsetof(U3CIncrementU3Ec__AnonStorey57_t1898758910, ___incrementCount_0)); }
	inline int32_t get_incrementCount_0() const { return ___incrementCount_0; }
	inline int32_t* get_address_of_incrementCount_0() { return &___incrementCount_0; }
	inline void set_incrementCount_0(int32_t value)
	{
		___incrementCount_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CIncrementU3Ec__AnonStorey57_t1898758910, ___U3CU3Ef__this_1)); }
	inline CountNotifier_t4027919079 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline CountNotifier_t4027919079 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(CountNotifier_t4027919079 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
