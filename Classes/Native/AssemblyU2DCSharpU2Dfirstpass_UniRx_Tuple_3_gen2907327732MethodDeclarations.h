﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.Collections.IComparer
struct IComparer_t2207526184;
// System.Collections.IEqualityComparer
struct IEqualityComparer_t1661793090;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_3_gen2907327732.h"
#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

// System.Void UniRx.Tuple`3<System.Object,UniRx.Unit,System.Object>::.ctor(T1,T2,T3)
extern "C"  void Tuple_3__ctor_m2303875472_gshared (Tuple_3_t2907327732 * __this, Il2CppObject * ___item10, Unit_t2558286038  ___item21, Il2CppObject * ___item32, const MethodInfo* method);
#define Tuple_3__ctor_m2303875472(__this, ___item10, ___item21, ___item32, method) ((  void (*) (Tuple_3_t2907327732 *, Il2CppObject *, Unit_t2558286038 , Il2CppObject *, const MethodInfo*))Tuple_3__ctor_m2303875472_gshared)(__this, ___item10, ___item21, ___item32, method)
// System.Int32 UniRx.Tuple`3<System.Object,UniRx.Unit,System.Object>::System.IComparable.CompareTo(System.Object)
extern "C"  int32_t Tuple_3_System_IComparable_CompareTo_m2583537726_gshared (Tuple_3_t2907327732 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define Tuple_3_System_IComparable_CompareTo_m2583537726(__this, ___obj0, method) ((  int32_t (*) (Tuple_3_t2907327732 *, Il2CppObject *, const MethodInfo*))Tuple_3_System_IComparable_CompareTo_m2583537726_gshared)(__this, ___obj0, method)
// System.Int32 UniRx.Tuple`3<System.Object,UniRx.Unit,System.Object>::UniRx.IStructuralComparable.CompareTo(System.Object,System.Collections.IComparer)
extern "C"  int32_t Tuple_3_UniRx_IStructuralComparable_CompareTo_m536773264_gshared (Tuple_3_t2907327732 * __this, Il2CppObject * ___other0, Il2CppObject * ___comparer1, const MethodInfo* method);
#define Tuple_3_UniRx_IStructuralComparable_CompareTo_m536773264(__this, ___other0, ___comparer1, method) ((  int32_t (*) (Tuple_3_t2907327732 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_3_UniRx_IStructuralComparable_CompareTo_m536773264_gshared)(__this, ___other0, ___comparer1, method)
// System.Boolean UniRx.Tuple`3<System.Object,UniRx.Unit,System.Object>::UniRx.IStructuralEquatable.Equals(System.Object,System.Collections.IEqualityComparer)
extern "C"  bool Tuple_3_UniRx_IStructuralEquatable_Equals_m366784903_gshared (Tuple_3_t2907327732 * __this, Il2CppObject * ___other0, Il2CppObject * ___comparer1, const MethodInfo* method);
#define Tuple_3_UniRx_IStructuralEquatable_Equals_m366784903(__this, ___other0, ___comparer1, method) ((  bool (*) (Tuple_3_t2907327732 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Tuple_3_UniRx_IStructuralEquatable_Equals_m366784903_gshared)(__this, ___other0, ___comparer1, method)
// System.Int32 UniRx.Tuple`3<System.Object,UniRx.Unit,System.Object>::UniRx.IStructuralEquatable.GetHashCode(System.Collections.IEqualityComparer)
extern "C"  int32_t Tuple_3_UniRx_IStructuralEquatable_GetHashCode_m1803176659_gshared (Tuple_3_t2907327732 * __this, Il2CppObject * ___comparer0, const MethodInfo* method);
#define Tuple_3_UniRx_IStructuralEquatable_GetHashCode_m1803176659(__this, ___comparer0, method) ((  int32_t (*) (Tuple_3_t2907327732 *, Il2CppObject *, const MethodInfo*))Tuple_3_UniRx_IStructuralEquatable_GetHashCode_m1803176659_gshared)(__this, ___comparer0, method)
// System.String UniRx.Tuple`3<System.Object,UniRx.Unit,System.Object>::UniRx.ITuple.ToString()
extern "C"  String_t* Tuple_3_UniRx_ITuple_ToString_m364752754_gshared (Tuple_3_t2907327732 * __this, const MethodInfo* method);
#define Tuple_3_UniRx_ITuple_ToString_m364752754(__this, method) ((  String_t* (*) (Tuple_3_t2907327732 *, const MethodInfo*))Tuple_3_UniRx_ITuple_ToString_m364752754_gshared)(__this, method)
// T1 UniRx.Tuple`3<System.Object,UniRx.Unit,System.Object>::get_Item1()
extern "C"  Il2CppObject * Tuple_3_get_Item1_m63303561_gshared (Tuple_3_t2907327732 * __this, const MethodInfo* method);
#define Tuple_3_get_Item1_m63303561(__this, method) ((  Il2CppObject * (*) (Tuple_3_t2907327732 *, const MethodInfo*))Tuple_3_get_Item1_m63303561_gshared)(__this, method)
// T2 UniRx.Tuple`3<System.Object,UniRx.Unit,System.Object>::get_Item2()
extern "C"  Unit_t2558286038  Tuple_3_get_Item2_m2687758249_gshared (Tuple_3_t2907327732 * __this, const MethodInfo* method);
#define Tuple_3_get_Item2_m2687758249(__this, method) ((  Unit_t2558286038  (*) (Tuple_3_t2907327732 *, const MethodInfo*))Tuple_3_get_Item2_m2687758249_gshared)(__this, method)
// T3 UniRx.Tuple`3<System.Object,UniRx.Unit,System.Object>::get_Item3()
extern "C"  Il2CppObject * Tuple_3_get_Item3_m1017245641_gshared (Tuple_3_t2907327732 * __this, const MethodInfo* method);
#define Tuple_3_get_Item3_m1017245641(__this, method) ((  Il2CppObject * (*) (Tuple_3_t2907327732 *, const MethodInfo*))Tuple_3_get_Item3_m1017245641_gshared)(__this, method)
// System.Boolean UniRx.Tuple`3<System.Object,UniRx.Unit,System.Object>::Equals(System.Object)
extern "C"  bool Tuple_3_Equals_m871068731_gshared (Tuple_3_t2907327732 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define Tuple_3_Equals_m871068731(__this, ___obj0, method) ((  bool (*) (Tuple_3_t2907327732 *, Il2CppObject *, const MethodInfo*))Tuple_3_Equals_m871068731_gshared)(__this, ___obj0, method)
// System.Int32 UniRx.Tuple`3<System.Object,UniRx.Unit,System.Object>::GetHashCode()
extern "C"  int32_t Tuple_3_GetHashCode_m3500510291_gshared (Tuple_3_t2907327732 * __this, const MethodInfo* method);
#define Tuple_3_GetHashCode_m3500510291(__this, method) ((  int32_t (*) (Tuple_3_t2907327732 *, const MethodInfo*))Tuple_3_GetHashCode_m3500510291_gshared)(__this, method)
// System.String UniRx.Tuple`3<System.Object,UniRx.Unit,System.Object>::ToString()
extern "C"  String_t* Tuple_3_ToString_m2811558527_gshared (Tuple_3_t2907327732 * __this, const MethodInfo* method);
#define Tuple_3_ToString_m2811558527(__this, method) ((  String_t* (*) (Tuple_3_t2907327732 *, const MethodInfo*))Tuple_3_ToString_m2811558527_gshared)(__this, method)
// System.Boolean UniRx.Tuple`3<System.Object,UniRx.Unit,System.Object>::Equals(UniRx.Tuple`3<T1,T2,T3>)
extern "C"  bool Tuple_3_Equals_m2139596626_gshared (Tuple_3_t2907327732 * __this, Tuple_3_t2907327732  ___other0, const MethodInfo* method);
#define Tuple_3_Equals_m2139596626(__this, ___other0, method) ((  bool (*) (Tuple_3_t2907327732 *, Tuple_3_t2907327732 , const MethodInfo*))Tuple_3_Equals_m2139596626_gshared)(__this, ___other0, method)
