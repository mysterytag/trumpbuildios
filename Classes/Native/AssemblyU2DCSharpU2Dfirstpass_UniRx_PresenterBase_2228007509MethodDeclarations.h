﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.PresenterBase`1<System.Object>
struct PresenterBase_1_t2228007509;
// UniRx.IPresenter
struct IPresenter_t2177085329;
// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;
// System.Object
struct Il2CppObject;
// UnityEngine.GameObject
struct GameObject_t4012695102;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.PresenterBase`1<System.Object>::.ctor()
extern "C"  void PresenterBase_1__ctor_m1017441733_gshared (PresenterBase_1_t2228007509 * __this, const MethodInfo* method);
#define PresenterBase_1__ctor_m1017441733(__this, method) ((  void (*) (PresenterBase_1_t2228007509 *, const MethodInfo*))PresenterBase_1__ctor_m1017441733_gshared)(__this, method)
// System.Void UniRx.PresenterBase`1<System.Object>::.cctor()
extern "C"  void PresenterBase_1__cctor_m993826440_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define PresenterBase_1__cctor_m993826440(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))PresenterBase_1__cctor_m993826440_gshared)(__this /* static, unused */, method)
// System.Void UniRx.PresenterBase`1<System.Object>::UniRx.IPresenter.Awake()
extern "C"  void PresenterBase_1_UniRx_IPresenter_Awake_m1721959053_gshared (PresenterBase_1_t2228007509 * __this, const MethodInfo* method);
#define PresenterBase_1_UniRx_IPresenter_Awake_m1721959053(__this, method) ((  void (*) (PresenterBase_1_t2228007509 *, const MethodInfo*))PresenterBase_1_UniRx_IPresenter_Awake_m1721959053_gshared)(__this, method)
// System.Void UniRx.PresenterBase`1<System.Object>::UniRx.IPresenter.StartCapturePhase()
extern "C"  void PresenterBase_1_UniRx_IPresenter_StartCapturePhase_m1085746527_gshared (PresenterBase_1_t2228007509 * __this, const MethodInfo* method);
#define PresenterBase_1_UniRx_IPresenter_StartCapturePhase_m1085746527(__this, method) ((  void (*) (PresenterBase_1_t2228007509 *, const MethodInfo*))PresenterBase_1_UniRx_IPresenter_StartCapturePhase_m1085746527_gshared)(__this, method)
// System.Void UniRx.PresenterBase`1<System.Object>::UniRx.IPresenter.RegisterParent(UniRx.IPresenter)
extern "C"  void PresenterBase_1_UniRx_IPresenter_RegisterParent_m1207557712_gshared (PresenterBase_1_t2228007509 * __this, Il2CppObject * ___parent0, const MethodInfo* method);
#define PresenterBase_1_UniRx_IPresenter_RegisterParent_m1207557712(__this, ___parent0, method) ((  void (*) (PresenterBase_1_t2228007509 *, Il2CppObject *, const MethodInfo*))PresenterBase_1_UniRx_IPresenter_RegisterParent_m1207557712_gshared)(__this, ___parent0, method)
// System.Void UniRx.PresenterBase`1<System.Object>::UniRx.IPresenter.InitializeCore()
extern "C"  void PresenterBase_1_UniRx_IPresenter_InitializeCore_m3007038249_gshared (PresenterBase_1_t2228007509 * __this, const MethodInfo* method);
#define PresenterBase_1_UniRx_IPresenter_InitializeCore_m3007038249(__this, method) ((  void (*) (PresenterBase_1_t2228007509 *, const MethodInfo*))PresenterBase_1_UniRx_IPresenter_InitializeCore_m3007038249_gshared)(__this, method)
// UniRx.IPresenter UniRx.PresenterBase`1<System.Object>::get_Parent()
extern "C"  Il2CppObject * PresenterBase_1_get_Parent_m3169181760_gshared (PresenterBase_1_t2228007509 * __this, const MethodInfo* method);
#define PresenterBase_1_get_Parent_m3169181760(__this, method) ((  Il2CppObject * (*) (PresenterBase_1_t2228007509 *, const MethodInfo*))PresenterBase_1_get_Parent_m3169181760_gshared)(__this, method)
// UniRx.IObservable`1<UniRx.Unit> UniRx.PresenterBase`1<System.Object>::InitializeAsObservable()
extern "C"  Il2CppObject* PresenterBase_1_InitializeAsObservable_m3919840940_gshared (PresenterBase_1_t2228007509 * __this, const MethodInfo* method);
#define PresenterBase_1_InitializeAsObservable_m3919840940(__this, method) ((  Il2CppObject* (*) (PresenterBase_1_t2228007509 *, const MethodInfo*))PresenterBase_1_InitializeAsObservable_m3919840940_gshared)(__this, method)
// System.Void UniRx.PresenterBase`1<System.Object>::PropagateArgument(T)
extern "C"  void PresenterBase_1_PropagateArgument_m1939852053_gshared (PresenterBase_1_t2228007509 * __this, Il2CppObject * ___argument0, const MethodInfo* method);
#define PresenterBase_1_PropagateArgument_m1939852053(__this, ___argument0, method) ((  void (*) (PresenterBase_1_t2228007509 *, Il2CppObject *, const MethodInfo*))PresenterBase_1_PropagateArgument_m1939852053_gshared)(__this, ___argument0, method)
// System.Void UniRx.PresenterBase`1<System.Object>::ForceInitialize(T)
extern "C"  void PresenterBase_1_ForceInitialize_m655226528_gshared (PresenterBase_1_t2228007509 * __this, Il2CppObject * ___argument0, const MethodInfo* method);
#define PresenterBase_1_ForceInitialize_m655226528(__this, ___argument0, method) ((  void (*) (PresenterBase_1_t2228007509 *, Il2CppObject *, const MethodInfo*))PresenterBase_1_ForceInitialize_m655226528_gshared)(__this, ___argument0, method)
// System.Void UniRx.PresenterBase`1<System.Object>::Awake()
extern "C"  void PresenterBase_1_Awake_m1255046952_gshared (PresenterBase_1_t2228007509 * __this, const MethodInfo* method);
#define PresenterBase_1_Awake_m1255046952(__this, method) ((  void (*) (PresenterBase_1_t2228007509 *, const MethodInfo*))PresenterBase_1_Awake_m1255046952_gshared)(__this, method)
// System.Void UniRx.PresenterBase`1<System.Object>::OnAwake()
extern "C"  void PresenterBase_1_OnAwake_m3180892105_gshared (PresenterBase_1_t2228007509 * __this, const MethodInfo* method);
#define PresenterBase_1_OnAwake_m3180892105(__this, method) ((  void (*) (PresenterBase_1_t2228007509 *, const MethodInfo*))PresenterBase_1_OnAwake_m3180892105_gshared)(__this, method)
// System.Void UniRx.PresenterBase`1<System.Object>::Start()
extern "C"  void PresenterBase_1_Start_m4259546821_gshared (PresenterBase_1_t2228007509 * __this, const MethodInfo* method);
#define PresenterBase_1_Start_m4259546821(__this, method) ((  void (*) (PresenterBase_1_t2228007509 *, const MethodInfo*))PresenterBase_1_Start_m4259546821_gshared)(__this, method)
// UnityEngine.GameObject UniRx.PresenterBase`1<System.Object>::UniRx.IPresenter.get_gameObject()
extern "C"  GameObject_t4012695102 * PresenterBase_1_UniRx_IPresenter_get_gameObject_m3480791831_gshared (PresenterBase_1_t2228007509 * __this, const MethodInfo* method);
#define PresenterBase_1_UniRx_IPresenter_get_gameObject_m3480791831(__this, method) ((  GameObject_t4012695102 * (*) (PresenterBase_1_t2228007509 *, const MethodInfo*))PresenterBase_1_UniRx_IPresenter_get_gameObject_m3480791831_gshared)(__this, method)
