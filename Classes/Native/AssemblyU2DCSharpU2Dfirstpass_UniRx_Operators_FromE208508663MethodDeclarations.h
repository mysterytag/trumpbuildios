﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.FromEventPatternObservable`2/FromEventPattern<System.Object,System.Object>
struct FromEventPattern_t208508663;
// UniRx.Operators.FromEventPatternObservable`2<System.Object,System.Object>
struct FromEventPatternObservable_2_t3019221245;
// UniRx.IObserver`1<UniRx.EventPattern`1<System.Object>>
struct IObserver_1_t820035585;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.Operators.FromEventPatternObservable`2/FromEventPattern<System.Object,System.Object>::.ctor(UniRx.Operators.FromEventPatternObservable`2<TDelegate,TEventArgs>,UniRx.IObserver`1<UniRx.EventPattern`1<TEventArgs>>)
extern "C"  void FromEventPattern__ctor_m1976577664_gshared (FromEventPattern_t208508663 * __this, FromEventPatternObservable_2_t3019221245 * ___parent0, Il2CppObject* ___observer1, const MethodInfo* method);
#define FromEventPattern__ctor_m1976577664(__this, ___parent0, ___observer1, method) ((  void (*) (FromEventPattern_t208508663 *, FromEventPatternObservable_2_t3019221245 *, Il2CppObject*, const MethodInfo*))FromEventPattern__ctor_m1976577664_gshared)(__this, ___parent0, ___observer1, method)
// System.Boolean UniRx.Operators.FromEventPatternObservable`2/FromEventPattern<System.Object,System.Object>::Register()
extern "C"  bool FromEventPattern_Register_m522344379_gshared (FromEventPattern_t208508663 * __this, const MethodInfo* method);
#define FromEventPattern_Register_m522344379(__this, method) ((  bool (*) (FromEventPattern_t208508663 *, const MethodInfo*))FromEventPattern_Register_m522344379_gshared)(__this, method)
// System.Void UniRx.Operators.FromEventPatternObservable`2/FromEventPattern<System.Object,System.Object>::OnNext(System.Object,TEventArgs)
extern "C"  void FromEventPattern_OnNext_m3769990365_gshared (FromEventPattern_t208508663 * __this, Il2CppObject * ___sender0, Il2CppObject * ___eventArgs1, const MethodInfo* method);
#define FromEventPattern_OnNext_m3769990365(__this, ___sender0, ___eventArgs1, method) ((  void (*) (FromEventPattern_t208508663 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))FromEventPattern_OnNext_m3769990365_gshared)(__this, ___sender0, ___eventArgs1, method)
// System.Void UniRx.Operators.FromEventPatternObservable`2/FromEventPattern<System.Object,System.Object>::Dispose()
extern "C"  void FromEventPattern_Dispose_m1389966277_gshared (FromEventPattern_t208508663 * __this, const MethodInfo* method);
#define FromEventPattern_Dispose_m1389966277(__this, method) ((  void (*) (FromEventPattern_t208508663 *, const MethodInfo*))FromEventPattern_Dispose_m1389966277_gshared)(__this, method)
