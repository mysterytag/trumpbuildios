﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// GameController
struct GameController_t2782302542;

#include "mscorlib_System_Object837106420.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameController/<ReleaseMoneyCoroutine>c__Iterator10
struct  U3CReleaseMoneyCoroutineU3Ec__Iterator10_t1779307829  : public Il2CppObject
{
public:
	// System.Single GameController/<ReleaseMoneyCoroutine>c__Iterator10::<startTime>__0
	float ___U3CstartTimeU3E__0_0;
	// System.Single GameController/<ReleaseMoneyCoroutine>c__Iterator10::<processTime>__1
	float ___U3CprocessTimeU3E__1_1;
	// UnityEngine.Vector3 GameController/<ReleaseMoneyCoroutine>c__Iterator10::<startMoneyPosition>__2
	Vector3_t3525329789  ___U3CstartMoneyPositionU3E__2_2;
	// System.Single GameController/<ReleaseMoneyCoroutine>c__Iterator10::<deltaTime>__3
	float ___U3CdeltaTimeU3E__3_3;
	// System.Int32 GameController/<ReleaseMoneyCoroutine>c__Iterator10::$PC
	int32_t ___U24PC_4;
	// System.Object GameController/<ReleaseMoneyCoroutine>c__Iterator10::$current
	Il2CppObject * ___U24current_5;
	// GameController GameController/<ReleaseMoneyCoroutine>c__Iterator10::<>f__this
	GameController_t2782302542 * ___U3CU3Ef__this_6;

public:
	inline static int32_t get_offset_of_U3CstartTimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CReleaseMoneyCoroutineU3Ec__Iterator10_t1779307829, ___U3CstartTimeU3E__0_0)); }
	inline float get_U3CstartTimeU3E__0_0() const { return ___U3CstartTimeU3E__0_0; }
	inline float* get_address_of_U3CstartTimeU3E__0_0() { return &___U3CstartTimeU3E__0_0; }
	inline void set_U3CstartTimeU3E__0_0(float value)
	{
		___U3CstartTimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CprocessTimeU3E__1_1() { return static_cast<int32_t>(offsetof(U3CReleaseMoneyCoroutineU3Ec__Iterator10_t1779307829, ___U3CprocessTimeU3E__1_1)); }
	inline float get_U3CprocessTimeU3E__1_1() const { return ___U3CprocessTimeU3E__1_1; }
	inline float* get_address_of_U3CprocessTimeU3E__1_1() { return &___U3CprocessTimeU3E__1_1; }
	inline void set_U3CprocessTimeU3E__1_1(float value)
	{
		___U3CprocessTimeU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CstartMoneyPositionU3E__2_2() { return static_cast<int32_t>(offsetof(U3CReleaseMoneyCoroutineU3Ec__Iterator10_t1779307829, ___U3CstartMoneyPositionU3E__2_2)); }
	inline Vector3_t3525329789  get_U3CstartMoneyPositionU3E__2_2() const { return ___U3CstartMoneyPositionU3E__2_2; }
	inline Vector3_t3525329789 * get_address_of_U3CstartMoneyPositionU3E__2_2() { return &___U3CstartMoneyPositionU3E__2_2; }
	inline void set_U3CstartMoneyPositionU3E__2_2(Vector3_t3525329789  value)
	{
		___U3CstartMoneyPositionU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U3CdeltaTimeU3E__3_3() { return static_cast<int32_t>(offsetof(U3CReleaseMoneyCoroutineU3Ec__Iterator10_t1779307829, ___U3CdeltaTimeU3E__3_3)); }
	inline float get_U3CdeltaTimeU3E__3_3() const { return ___U3CdeltaTimeU3E__3_3; }
	inline float* get_address_of_U3CdeltaTimeU3E__3_3() { return &___U3CdeltaTimeU3E__3_3; }
	inline void set_U3CdeltaTimeU3E__3_3(float value)
	{
		___U3CdeltaTimeU3E__3_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CReleaseMoneyCoroutineU3Ec__Iterator10_t1779307829, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CReleaseMoneyCoroutineU3Ec__Iterator10_t1779307829, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_6() { return static_cast<int32_t>(offsetof(U3CReleaseMoneyCoroutineU3Ec__Iterator10_t1779307829, ___U3CU3Ef__this_6)); }
	inline GameController_t2782302542 * get_U3CU3Ef__this_6() const { return ___U3CU3Ef__this_6; }
	inline GameController_t2782302542 ** get_address_of_U3CU3Ef__this_6() { return &___U3CU3Ef__this_6; }
	inline void set_U3CU3Ef__this_6(GameController_t2782302542 * value)
	{
		___U3CU3Ef__this_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
