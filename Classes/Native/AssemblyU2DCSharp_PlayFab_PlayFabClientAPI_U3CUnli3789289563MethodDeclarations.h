﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<UnlinkGameCenterAccount>c__AnonStorey83
struct U3CUnlinkGameCenterAccountU3Ec__AnonStorey83_t3789289563;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<UnlinkGameCenterAccount>c__AnonStorey83::.ctor()
extern "C"  void U3CUnlinkGameCenterAccountU3Ec__AnonStorey83__ctor_m2684095832 (U3CUnlinkGameCenterAccountU3Ec__AnonStorey83_t3789289563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<UnlinkGameCenterAccount>c__AnonStorey83::<>m__70(PlayFab.CallRequestContainer)
extern "C"  void U3CUnlinkGameCenterAccountU3Ec__AnonStorey83_U3CU3Em__70_m3295311695 (U3CUnlinkGameCenterAccountU3Ec__AnonStorey83_t3789289563 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
