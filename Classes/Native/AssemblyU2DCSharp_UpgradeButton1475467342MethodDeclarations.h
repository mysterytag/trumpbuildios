﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UpgradeButton
struct UpgradeButton_t1475467342;

#include "codegen/il2cpp-codegen.h"

// System.Void UpgradeButton::.ctor()
extern "C"  void UpgradeButton__ctor_m1404829917 (UpgradeButton_t1475467342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
