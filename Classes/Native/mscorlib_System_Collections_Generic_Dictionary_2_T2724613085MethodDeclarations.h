﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<SponsorPay.SPLogLevel,System.Int32,System.Int32>
struct Transform_1_t2724613085;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "AssemblyU2DCSharp_SponsorPay_SPLogLevel220722135.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<SponsorPay.SPLogLevel,System.Int32,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m896228104_gshared (Transform_1_t2724613085 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Transform_1__ctor_m896228104(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t2724613085 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m896228104_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<SponsorPay.SPLogLevel,System.Int32,System.Int32>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m2863556532_gshared (Transform_1_t2724613085 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
#define Transform_1_Invoke_m2863556532(__this, ___key0, ___value1, method) ((  int32_t (*) (Transform_1_t2724613085 *, int32_t, int32_t, const MethodInfo*))Transform_1_Invoke_m2863556532_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<SponsorPay.SPLogLevel,System.Int32,System.Int32>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m21515411_gshared (Transform_1_t2724613085 * __this, int32_t ___key0, int32_t ___value1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Transform_1_BeginInvoke_m21515411(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t2724613085 *, int32_t, int32_t, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m21515411_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<SponsorPay.SPLogLevel,System.Int32,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m1773019222_gshared (Transform_1_t2724613085 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Transform_1_EndInvoke_m1773019222(__this, ___result0, method) ((  int32_t (*) (Transform_1_t2724613085 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m1773019222_gshared)(__this, ___result0, method)
