﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetTitleDataRequestCallback
struct GetTitleDataRequestCallback_t3277276872;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetTitleDataRequest
struct GetTitleDataRequest_t535093619;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetTitleData535093619.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetTitleDataRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetTitleDataRequestCallback__ctor_m3838900055 (GetTitleDataRequestCallback_t3277276872 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetTitleDataRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetTitleDataRequest,System.Object)
extern "C"  void GetTitleDataRequestCallback_Invoke_m2104245897 (GetTitleDataRequestCallback_t3277276872 * __this, String_t* ___urlPath0, int32_t ___callId1, GetTitleDataRequest_t535093619 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetTitleDataRequestCallback_t3277276872(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetTitleDataRequest_t535093619 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetTitleDataRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetTitleDataRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetTitleDataRequestCallback_BeginInvoke_m834085342 (GetTitleDataRequestCallback_t3277276872 * __this, String_t* ___urlPath0, int32_t ___callId1, GetTitleDataRequest_t535093619 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetTitleDataRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetTitleDataRequestCallback_EndInvoke_m2751180007 (GetTitleDataRequestCallback_t3277276872 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
