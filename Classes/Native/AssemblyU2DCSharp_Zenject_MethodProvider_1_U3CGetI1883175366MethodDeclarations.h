﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.MethodProvider`1/<GetInstance>c__AnonStorey195<System.Object>
struct U3CGetInstanceU3Ec__AnonStorey195_t1883175366;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Zenject.MethodProvider`1/<GetInstance>c__AnonStorey195<System.Object>::.ctor()
extern "C"  void U3CGetInstanceU3Ec__AnonStorey195__ctor_m2793557463_gshared (U3CGetInstanceU3Ec__AnonStorey195_t1883175366 * __this, const MethodInfo* method);
#define U3CGetInstanceU3Ec__AnonStorey195__ctor_m2793557463(__this, method) ((  void (*) (U3CGetInstanceU3Ec__AnonStorey195_t1883175366 *, const MethodInfo*))U3CGetInstanceU3Ec__AnonStorey195__ctor_m2793557463_gshared)(__this, method)
// System.String Zenject.MethodProvider`1/<GetInstance>c__AnonStorey195<System.Object>::<>m__303()
extern "C"  String_t* U3CGetInstanceU3Ec__AnonStorey195_U3CU3Em__303_m42889013_gshared (U3CGetInstanceU3Ec__AnonStorey195_t1883175366 * __this, const MethodInfo* method);
#define U3CGetInstanceU3Ec__AnonStorey195_U3CU3Em__303_m42889013(__this, method) ((  String_t* (*) (U3CGetInstanceU3Ec__AnonStorey195_t1883175366 *, const MethodInfo*))U3CGetInstanceU3Ec__AnonStorey195_U3CU3Em__303_m42889013_gshared)(__this, method)
