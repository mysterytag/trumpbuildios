﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AdToApp.AndroidWrapper.ATAInterstitialAdListener
struct ATAInterstitialAdListener_t382012998;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void AdToApp.AndroidWrapper.ATAInterstitialAdListener::.ctor()
extern "C"  void ATAInterstitialAdListener__ctor_m979529469 (ATAInterstitialAdListener_t382012998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AndroidWrapper.ATAInterstitialAdListener::onInterstitialStarted(System.String,System.String)
extern "C"  void ATAInterstitialAdListener_onInterstitialStarted_m2636836621 (ATAInterstitialAdListener_t382012998 * __this, String_t* ___adType0, String_t* ___provider1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AndroidWrapper.ATAInterstitialAdListener::onInterstitialClosed(System.String,System.String)
extern "C"  void ATAInterstitialAdListener_onInterstitialClosed_m2707058752 (ATAInterstitialAdListener_t382012998 * __this, String_t* ___adType0, String_t* ___provider1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AndroidWrapper.ATAInterstitialAdListener::onInterstitialClicked(System.String,System.String)
extern "C"  void ATAInterstitialAdListener_onInterstitialClicked_m3793307943 (ATAInterstitialAdListener_t382012998 * __this, String_t* ___adType0, String_t* ___provider1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AndroidWrapper.ATAInterstitialAdListener::onFirstInterstitialLoad(System.String,System.String)
extern "C"  void ATAInterstitialAdListener_onFirstInterstitialLoad_m3388049312 (ATAInterstitialAdListener_t382012998 * __this, String_t* ___adType0, String_t* ___provider1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AdToApp.AndroidWrapper.ATAInterstitialAdListener::onInterstitialFailedToShow(System.String)
extern "C"  bool ATAInterstitialAdListener_onInterstitialFailedToShow_m3807340607 (ATAInterstitialAdListener_t382012998 * __this, String_t* ___adType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AndroidWrapper.ATAInterstitialAdListener::onRewardedCompleted(System.String,System.String,System.String)
extern "C"  void ATAInterstitialAdListener_onRewardedCompleted_m82407073 (ATAInterstitialAdListener_t382012998 * __this, String_t* ___adProvider0, String_t* ___currencyName1, String_t* ___currencyValue2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
