﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Stream_1_gen1107661009MethodDeclarations.h"

// System.Void Stream`1<UniRx.CollectionAddEvent`1<SettingsButton>>::.ctor()
#define Stream_1__ctor_m3308466741(__this, method) ((  void (*) (Stream_1_t1185811250 *, const MethodInfo*))Stream_1__ctor_m3235287902_gshared)(__this, method)
// System.Void Stream`1<UniRx.CollectionAddEvent`1<SettingsButton>>::Send(T)
#define Stream_1_Send_m1837684295(__this, ___value0, method) ((  void (*) (Stream_1_t1185811250 *, CollectionAddEvent_1_t2494672228 , const MethodInfo*))Stream_1_Send_m1764505456_gshared)(__this, ___value0, method)
// System.Void Stream`1<UniRx.CollectionAddEvent`1<SettingsButton>>::SendInTransaction(T,System.Int32)
#define Stream_1_SendInTransaction_m2397810461(__this, ___value0, ___p1, method) ((  void (*) (Stream_1_t1185811250 *, CollectionAddEvent_1_t2494672228 , int32_t, const MethodInfo*))Stream_1_SendInTransaction_m1512275718_gshared)(__this, ___value0, ___p1, method)
// System.IDisposable Stream`1<UniRx.CollectionAddEvent`1<SettingsButton>>::Listen(System.Action`1<T>,Priority)
#define Stream_1_Listen_m533917913(__this, ___action0, ___priority1, method) ((  Il2CppObject * (*) (Stream_1_t1185811250 *, Action_1_t2643124933 *, int32_t, const MethodInfo*))Stream_1_Listen_m1890769608_gshared)(__this, ___action0, ___priority1, method)
// System.IDisposable Stream`1<UniRx.CollectionAddEvent`1<SettingsButton>>::Listen(System.Action,Priority)
#define Stream_1_Listen_m3664719022(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (Stream_1_t1185811250 *, Action_t437523947 *, int32_t, const MethodInfo*))Stream_1_Listen_m3411183967_gshared)(__this, ___action0, ___p1, method)
// System.Void Stream`1<UniRx.CollectionAddEvent`1<SettingsButton>>::Dispose()
#define Stream_1_Dispose_m1059022130(__this, method) ((  void (*) (Stream_1_t1185811250 *, const MethodInfo*))Stream_1_Dispose_m3748601883_gshared)(__this, method)
// System.Void Stream`1<UniRx.CollectionAddEvent`1<SettingsButton>>::SetInputStream(IStream`1<T>)
#define Stream_1_SetInputStream_m733020395(__this, ___stream0, method) ((  void (*) (Stream_1_t1185811250 *, Il2CppObject*, const MethodInfo*))Stream_1_SetInputStream_m453976930_gshared)(__this, ___stream0, method)
// System.Void Stream`1<UniRx.CollectionAddEvent`1<SettingsButton>>::ClearInputStream()
#define Stream_1_ClearInputStream_m1684392844(__this, method) ((  void (*) (Stream_1_t1185811250 *, const MethodInfo*))Stream_1_ClearInputStream_m1610565315_gshared)(__this, method)
// System.Void Stream`1<UniRx.CollectionAddEvent`1<SettingsButton>>::TransactionIterationFinished()
#define Stream_1_TransactionIterationFinished_m2350422368(__this, method) ((  void (*) (Stream_1_t1185811250 *, const MethodInfo*))Stream_1_TransactionIterationFinished_m477624855_gshared)(__this, method)
// System.Void Stream`1<UniRx.CollectionAddEvent`1<SettingsButton>>::Unpack(System.Int32)
#define Stream_1_Unpack_m4075667570(__this, ___p0, method) ((  void (*) (Stream_1_t1185811250 *, int32_t, const MethodInfo*))Stream_1_Unpack_m1846888937_gshared)(__this, ___p0, method)
// System.Void Stream`1<UniRx.CollectionAddEvent`1<SettingsButton>>::<SetInputStream>m__1BA(T)
#define Stream_1_U3CSetInputStreamU3Em__1BA_m64908690(__this, ___val0, method) ((  void (*) (Stream_1_t1185811250 *, CollectionAddEvent_1_t2494672228 , const MethodInfo*))Stream_1_U3CSetInputStreamU3Em__1BA_m4229248635_gshared)(__this, ___val0, method)
