﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.PaymentOption
struct PaymentOption_t1289274891;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.PaymentOption::.ctor()
extern "C"  void PaymentOption__ctor_m3714308990 (PaymentOption_t1289274891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.PaymentOption::get_Currency()
extern "C"  String_t* PaymentOption_get_Currency_m439686339 (PaymentOption_t1289274891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.PaymentOption::set_Currency(System.String)
extern "C"  void PaymentOption_set_Currency_m506190510 (PaymentOption_t1289274891 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.PaymentOption::get_ProviderName()
extern "C"  String_t* PaymentOption_get_ProviderName_m2911863182 (PaymentOption_t1289274891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.PaymentOption::set_ProviderName(System.String)
extern "C"  void PaymentOption_set_ProviderName_m2034856003 (PaymentOption_t1289274891 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 PlayFab.ClientModels.PaymentOption::get_Price()
extern "C"  uint32_t PaymentOption_get_Price_m620295361 (PaymentOption_t1289274891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.PaymentOption::set_Price(System.UInt32)
extern "C"  void PaymentOption_set_Price_m2857973426 (PaymentOption_t1289274891 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 PlayFab.ClientModels.PaymentOption::get_StoreCredit()
extern "C"  uint32_t PaymentOption_get_StoreCredit_m1108183346 (PaymentOption_t1289274891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.PaymentOption::set_StoreCredit(System.UInt32)
extern "C"  void PaymentOption_set_StoreCredit_m4028193569 (PaymentOption_t1289274891 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
