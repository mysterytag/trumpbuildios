﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Diagnostics.UnityDebugSink
struct UnityDebugSink_t4077792013;
// System.Exception
struct Exception_t1967233988;
// UniRx.Diagnostics.LogEntry
struct LogEntry_t1891155722;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Diagnostics_Lo1891155722.h"

// System.Void UniRx.Diagnostics.UnityDebugSink::.ctor()
extern "C"  void UnityDebugSink__ctor_m1854900132 (UnityDebugSink_t4077792013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Diagnostics.UnityDebugSink::OnCompleted()
extern "C"  void UnityDebugSink_OnCompleted_m3304550446 (UnityDebugSink_t4077792013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Diagnostics.UnityDebugSink::OnError(System.Exception)
extern "C"  void UnityDebugSink_OnError_m1434799195 (UnityDebugSink_t4077792013 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Diagnostics.UnityDebugSink::OnNext(UniRx.Diagnostics.LogEntry)
extern "C"  void UnityDebugSink_OnNext_m3025534090 (UnityDebugSink_t4077792013 * __this, LogEntry_t1891155722 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
