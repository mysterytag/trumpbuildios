﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.SceneCompositionRoot
struct SceneCompositionRoot_t1259693845;
// Zenject.DiContainer
struct DiContainer_t2383114449;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// Zenject.IInstaller
struct IInstaller_t2455223476;
// System.Collections.Generic.List`1<Zenject.IInstaller>
struct List_1_t3252182445;
// UnityEngine.Transform
struct Transform_t284553113;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"
#include "AssemblyU2DCSharp_Zenject_DiContainer2383114449.h"
#include "UnityEngine_UnityEngine_Transform284553113.h"

// System.Void Zenject.SceneCompositionRoot::.ctor()
extern "C"  void SceneCompositionRoot__ctor_m3326687548 (SceneCompositionRoot_t1259693845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.SceneCompositionRoot::.cctor()
extern "C"  void SceneCompositionRoot__cctor_m3860969969 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.DiContainer Zenject.SceneCompositionRoot::get_Container()
extern "C"  DiContainer_t2383114449 * SceneCompositionRoot_get_Container_m2943470492 (SceneCompositionRoot_t1259693845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.SceneCompositionRoot Zenject.SceneCompositionRoot::AddComponent(UnityEngine.GameObject,Zenject.IInstaller)
extern "C"  SceneCompositionRoot_t1259693845 * SceneCompositionRoot_AddComponent_m15872878 (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * ___gameObject0, Il2CppObject * ___rootInstaller1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.SceneCompositionRoot Zenject.SceneCompositionRoot::AddComponent(UnityEngine.GameObject,System.Collections.Generic.List`1<Zenject.IInstaller>)
extern "C"  SceneCompositionRoot_t1259693845 * SceneCompositionRoot_AddComponent_m3339054340 (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * ___gameObject0, List_1_t3252182445 * ___installers1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.SceneCompositionRoot::Awake()
extern "C"  void SceneCompositionRoot_Awake_m3564292767 (SceneCompositionRoot_t1259693845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.SceneCompositionRoot::Start()
extern "C"  void SceneCompositionRoot_Start_m2273825340 (SceneCompositionRoot_t1259693845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Zenject.DiContainer Zenject.SceneCompositionRoot::CreateContainer(System.Boolean,Zenject.DiContainer,System.Collections.Generic.List`1<Zenject.IInstaller>)
extern "C"  DiContainer_t2383114449 * SceneCompositionRoot_CreateContainer_m3963119193 (SceneCompositionRoot_t1259693845 * __this, bool ___allowNullBindings0, DiContainer_t2383114449 * ___parentContainer1, List_1_t3252182445 * ___extraInstallers2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.SceneCompositionRoot::<Awake>m__2D2(UnityEngine.Transform)
extern "C"  bool SceneCompositionRoot_U3CAwakeU3Em__2D2_m2631449551 (SceneCompositionRoot_t1259693845 * __this, Transform_t284553113 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Zenject.SceneCompositionRoot::<Awake>m__2D3(UnityEngine.Transform)
extern "C"  GameObject_t4012695102 * SceneCompositionRoot_U3CAwakeU3Em__2D3_m1228698223 (Il2CppObject * __this /* static, unused */, Transform_t284553113 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zenject.SceneCompositionRoot::<CreateContainer>m__2D4(Zenject.IInstaller)
extern "C"  bool SceneCompositionRoot_U3CCreateContainerU3Em__2D4_m3655488626 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
