﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.ReactiveProperty`1<UnityEngine.Rect>
struct ReactiveProperty_1_t2133761855;
// UniRx.IObservable`1<UnityEngine.Rect>
struct IObservable_1_t1284227181;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Rect>
struct IEqualityComparer_1_t3849695468;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<UnityEngine.Rect>
struct IObserver_1_t3737427720;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Rect1525428817.h"

// System.Void UniRx.ReactiveProperty`1<UnityEngine.Rect>::.ctor()
extern "C"  void ReactiveProperty_1__ctor_m2303028849_gshared (ReactiveProperty_1_t2133761855 * __this, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m2303028849(__this, method) ((  void (*) (ReactiveProperty_1_t2133761855 *, const MethodInfo*))ReactiveProperty_1__ctor_m2303028849_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Rect>::.ctor(T)
extern "C"  void ReactiveProperty_1__ctor_m2674418957_gshared (ReactiveProperty_1_t2133761855 * __this, Rect_t1525428817  ___initialValue0, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m2674418957(__this, ___initialValue0, method) ((  void (*) (ReactiveProperty_1_t2133761855 *, Rect_t1525428817 , const MethodInfo*))ReactiveProperty_1__ctor_m2674418957_gshared)(__this, ___initialValue0, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Rect>::.ctor(UniRx.IObservable`1<T>)
extern "C"  void ReactiveProperty_1__ctor_m397310352_gshared (ReactiveProperty_1_t2133761855 * __this, Il2CppObject* ___source0, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m397310352(__this, ___source0, method) ((  void (*) (ReactiveProperty_1_t2133761855 *, Il2CppObject*, const MethodInfo*))ReactiveProperty_1__ctor_m397310352_gshared)(__this, ___source0, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Rect>::.ctor(UniRx.IObservable`1<T>,T)
extern "C"  void ReactiveProperty_1__ctor_m3858131752_gshared (ReactiveProperty_1_t2133761855 * __this, Il2CppObject* ___source0, Rect_t1525428817  ___initialValue1, const MethodInfo* method);
#define ReactiveProperty_1__ctor_m3858131752(__this, ___source0, ___initialValue1, method) ((  void (*) (ReactiveProperty_1_t2133761855 *, Il2CppObject*, Rect_t1525428817 , const MethodInfo*))ReactiveProperty_1__ctor_m3858131752_gshared)(__this, ___source0, ___initialValue1, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Rect>::.cctor()
extern "C"  void ReactiveProperty_1__cctor_m2192321372_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ReactiveProperty_1__cctor_m2192321372(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ReactiveProperty_1__cctor_m2192321372_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1<UnityEngine.Rect>::get_EqualityComparer()
extern "C"  Il2CppObject* ReactiveProperty_1_get_EqualityComparer_m1558262178_gshared (ReactiveProperty_1_t2133761855 * __this, const MethodInfo* method);
#define ReactiveProperty_1_get_EqualityComparer_m1558262178(__this, method) ((  Il2CppObject* (*) (ReactiveProperty_1_t2133761855 *, const MethodInfo*))ReactiveProperty_1_get_EqualityComparer_m1558262178_gshared)(__this, method)
// T UniRx.ReactiveProperty`1<UnityEngine.Rect>::get_Value()
extern "C"  Rect_t1525428817  ReactiveProperty_1_get_Value_m1076111480_gshared (ReactiveProperty_1_t2133761855 * __this, const MethodInfo* method);
#define ReactiveProperty_1_get_Value_m1076111480(__this, method) ((  Rect_t1525428817  (*) (ReactiveProperty_1_t2133761855 *, const MethodInfo*))ReactiveProperty_1_get_Value_m1076111480_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Rect>::set_Value(T)
extern "C"  void ReactiveProperty_1_set_Value_m3418176219_gshared (ReactiveProperty_1_t2133761855 * __this, Rect_t1525428817  ___value0, const MethodInfo* method);
#define ReactiveProperty_1_set_Value_m3418176219(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t2133761855 *, Rect_t1525428817 , const MethodInfo*))ReactiveProperty_1_set_Value_m3418176219_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Rect>::SetValue(T)
extern "C"  void ReactiveProperty_1_SetValue_m2966897532_gshared (ReactiveProperty_1_t2133761855 * __this, Rect_t1525428817  ___value0, const MethodInfo* method);
#define ReactiveProperty_1_SetValue_m2966897532(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t2133761855 *, Rect_t1525428817 , const MethodInfo*))ReactiveProperty_1_SetValue_m2966897532_gshared)(__this, ___value0, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Rect>::SetValueAndForceNotify(T)
extern "C"  void ReactiveProperty_1_SetValueAndForceNotify_m3614605375_gshared (ReactiveProperty_1_t2133761855 * __this, Rect_t1525428817  ___value0, const MethodInfo* method);
#define ReactiveProperty_1_SetValueAndForceNotify_m3614605375(__this, ___value0, method) ((  void (*) (ReactiveProperty_1_t2133761855 *, Rect_t1525428817 , const MethodInfo*))ReactiveProperty_1_SetValueAndForceNotify_m3614605375_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.ReactiveProperty`1<UnityEngine.Rect>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * ReactiveProperty_1_Subscribe_m1110078960_gshared (ReactiveProperty_1_t2133761855 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ReactiveProperty_1_Subscribe_m1110078960(__this, ___observer0, method) ((  Il2CppObject * (*) (ReactiveProperty_1_t2133761855 *, Il2CppObject*, const MethodInfo*))ReactiveProperty_1_Subscribe_m1110078960_gshared)(__this, ___observer0, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Rect>::Dispose()
extern "C"  void ReactiveProperty_1_Dispose_m1200849518_gshared (ReactiveProperty_1_t2133761855 * __this, const MethodInfo* method);
#define ReactiveProperty_1_Dispose_m1200849518(__this, method) ((  void (*) (ReactiveProperty_1_t2133761855 *, const MethodInfo*))ReactiveProperty_1_Dispose_m1200849518_gshared)(__this, method)
// System.Void UniRx.ReactiveProperty`1<UnityEngine.Rect>::Dispose(System.Boolean)
extern "C"  void ReactiveProperty_1_Dispose_m351690597_gshared (ReactiveProperty_1_t2133761855 * __this, bool ___disposing0, const MethodInfo* method);
#define ReactiveProperty_1_Dispose_m351690597(__this, ___disposing0, method) ((  void (*) (ReactiveProperty_1_t2133761855 *, bool, const MethodInfo*))ReactiveProperty_1_Dispose_m351690597_gshared)(__this, ___disposing0, method)
// System.String UniRx.ReactiveProperty`1<UnityEngine.Rect>::ToString()
extern "C"  String_t* ReactiveProperty_1_ToString_m3111913986_gshared (ReactiveProperty_1_t2133761855 * __this, const MethodInfo* method);
#define ReactiveProperty_1_ToString_m3111913986(__this, method) ((  String_t* (*) (ReactiveProperty_1_t2133761855 *, const MethodInfo*))ReactiveProperty_1_ToString_m3111913986_gshared)(__this, method)
// System.Boolean UniRx.ReactiveProperty`1<UnityEngine.Rect>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m4081809586_gshared (ReactiveProperty_1_t2133761855 * __this, const MethodInfo* method);
#define ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m4081809586(__this, method) ((  bool (*) (ReactiveProperty_1_t2133761855 *, const MethodInfo*))ReactiveProperty_1_IsRequiredSubscribeOnCurrentThread_m4081809586_gshared)(__this, method)
