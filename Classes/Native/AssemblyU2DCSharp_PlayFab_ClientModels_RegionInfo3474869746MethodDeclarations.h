﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.RegionInfo
struct RegionInfo_t3474869746;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen212373688.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.RegionInfo::.ctor()
extern "C"  void RegionInfo__ctor_m6961675 (RegionInfo_t3474869746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<PlayFab.ClientModels.Region> PlayFab.ClientModels.RegionInfo::get_Region()
extern "C"  Nullable_1_t212373688  RegionInfo_get_Region_m3381460784 (RegionInfo_t3474869746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.RegionInfo::set_Region(System.Nullable`1<PlayFab.ClientModels.Region>)
extern "C"  void RegionInfo_set_Region_m115377523 (RegionInfo_t3474869746 * __this, Nullable_1_t212373688  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.RegionInfo::get_Name()
extern "C"  String_t* RegionInfo_get_Name_m2482601130 (RegionInfo_t3474869746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.RegionInfo::set_Name(System.String)
extern "C"  void RegionInfo_set_Name_m3283075201 (RegionInfo_t3474869746 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayFab.ClientModels.RegionInfo::get_Available()
extern "C"  bool RegionInfo_get_Available_m3049764605 (RegionInfo_t3474869746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.RegionInfo::set_Available(System.Boolean)
extern "C"  void RegionInfo_set_Available_m3779082252 (RegionInfo_t3474869746 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.RegionInfo::get_PingUrl()
extern "C"  String_t* RegionInfo_get_PingUrl_m2559666112 (RegionInfo_t3474869746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.RegionInfo::set_PingUrl(System.String)
extern "C"  void RegionInfo_set_PingUrl_m818143417 (RegionInfo_t3474869746 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
