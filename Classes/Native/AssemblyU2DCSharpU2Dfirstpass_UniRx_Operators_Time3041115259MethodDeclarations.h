﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.TimeIntervalObservable`1/TimeInterval<System.Object>
struct TimeInterval_t3041115259;
// UniRx.Operators.TimeIntervalObservable`1<System.Object>
struct TimeIntervalObservable_1_t4159975444;
// UniRx.IObserver`1<UniRx.TimeInterval`1<System.Object>>
struct IObserver_1_t2920064445;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.TimeIntervalObservable`1/TimeInterval<System.Object>::.ctor(UniRx.Operators.TimeIntervalObservable`1<T>,UniRx.IObserver`1<UniRx.TimeInterval`1<T>>,System.IDisposable)
extern "C"  void TimeInterval__ctor_m2128041023_gshared (TimeInterval_t3041115259 * __this, TimeIntervalObservable_1_t4159975444 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define TimeInterval__ctor_m2128041023(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (TimeInterval_t3041115259 *, TimeIntervalObservable_1_t4159975444 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))TimeInterval__ctor_m2128041023_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.TimeIntervalObservable`1/TimeInterval<System.Object>::OnNext(T)
extern "C"  void TimeInterval_OnNext_m4003686687_gshared (TimeInterval_t3041115259 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define TimeInterval_OnNext_m4003686687(__this, ___value0, method) ((  void (*) (TimeInterval_t3041115259 *, Il2CppObject *, const MethodInfo*))TimeInterval_OnNext_m4003686687_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.TimeIntervalObservable`1/TimeInterval<System.Object>::OnError(System.Exception)
extern "C"  void TimeInterval_OnError_m2760336942_gshared (TimeInterval_t3041115259 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define TimeInterval_OnError_m2760336942(__this, ___error0, method) ((  void (*) (TimeInterval_t3041115259 *, Exception_t1967233988 *, const MethodInfo*))TimeInterval_OnError_m2760336942_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.TimeIntervalObservable`1/TimeInterval<System.Object>::OnCompleted()
extern "C"  void TimeInterval_OnCompleted_m59469441_gshared (TimeInterval_t3041115259 * __this, const MethodInfo* method);
#define TimeInterval_OnCompleted_m59469441(__this, method) ((  void (*) (TimeInterval_t3041115259 *, const MethodInfo*))TimeInterval_OnCompleted_m59469441_gshared)(__this, method)
