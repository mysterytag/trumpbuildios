﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LocalyticsUnity.Localytics
struct Localytics_t1579915497;
// LocalyticsUnity.Localytics/LocalyticsDidTagEvent
struct LocalyticsDidTagEvent_t1639420300;
// LocalyticsUnity.Localytics/LocalyticsSessionDidOpen
struct LocalyticsSessionDidOpen_t115241990;
// LocalyticsUnity.Localytics/LocalyticsSessionWillClose
struct LocalyticsSessionWillClose_t2690468515;
// LocalyticsUnity.Localytics/LocalyticsSessionWillOpen
struct LocalyticsSessionWillOpen_t2581002303;
// LocalyticsUnity.Localytics/LocalyticsDidDismissInAppMessage
struct LocalyticsDidDismissInAppMessage_t1872466793;
// LocalyticsUnity.Localytics/LocalyticsDidDisplayInAppMessage
struct LocalyticsDidDisplayInAppMessage_t3613222305;
// LocalyticsUnity.Localytics/LocalyticsWillDismissInAppMessage
struct LocalyticsWillDismissInAppMessage_t2580912496;
// LocalyticsUnity.Localytics/LocalyticsWillDisplayInAppMessage
struct LocalyticsWillDisplayInAppMessage_t26700712;
// System.String
struct String_t;
// System.Int64[]
struct Int64U5BU5D_t753178071;
// System.String[]
struct StringU5BU5D_t2956870243;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;
// LocalyticsUnity.Localytics/ReceiveAnalyticsDelegate
struct ReceiveAnalyticsDelegate_t2804656808;
// LocalyticsUnity.Localytics/ReceiveMessagingDelegate
struct ReceiveMessagingDelegate_t3408128038;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_LocalyticsUnity_Localytics_Local1639420300.h"
#include "AssemblyU2DCSharp_LocalyticsUnity_Localytics_Localy115241990.h"
#include "AssemblyU2DCSharp_LocalyticsUnity_Localytics_Local2690468515.h"
#include "AssemblyU2DCSharp_LocalyticsUnity_Localytics_Local2581002303.h"
#include "AssemblyU2DCSharp_LocalyticsUnity_Localytics_Local1872466793.h"
#include "AssemblyU2DCSharp_LocalyticsUnity_Localytics_Local3613222305.h"
#include "AssemblyU2DCSharp_LocalyticsUnity_Localytics_Local2580912496.h"
#include "AssemblyU2DCSharp_LocalyticsUnity_Localytics_Localyt26700712.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_LocalyticsUnity_Localytics_InApp1063031078.h"
#include "AssemblyU2DCSharp_LocalyticsUnity_Localytics_Profi1376631371.h"
#include "UnityEngine_UnityEngine_LocationInfo1628589680.h"
#include "AssemblyU2DCSharp_LocalyticsUnity_Localytics_Recei2804656808.h"
#include "AssemblyU2DCSharp_LocalyticsUnity_Localytics_Recei3408128038.h"

// System.Void LocalyticsUnity.Localytics::.ctor()
extern "C"  void Localytics__ctor_m604636892 (Localytics_t1579915497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::add_OnLocalyticsDidTagEvent(LocalyticsUnity.Localytics/LocalyticsDidTagEvent)
extern "C"  void Localytics_add_OnLocalyticsDidTagEvent_m2904114765 (Il2CppObject * __this /* static, unused */, LocalyticsDidTagEvent_t1639420300 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::remove_OnLocalyticsDidTagEvent(LocalyticsUnity.Localytics/LocalyticsDidTagEvent)
extern "C"  void Localytics_remove_OnLocalyticsDidTagEvent_m3611443998 (Il2CppObject * __this /* static, unused */, LocalyticsDidTagEvent_t1639420300 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::add_OnLocalyticsSessionDidOpen(LocalyticsUnity.Localytics/LocalyticsSessionDidOpen)
extern "C"  void Localytics_add_OnLocalyticsSessionDidOpen_m3672933809 (Il2CppObject * __this /* static, unused */, LocalyticsSessionDidOpen_t115241990 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::remove_OnLocalyticsSessionDidOpen(LocalyticsUnity.Localytics/LocalyticsSessionDidOpen)
extern "C"  void Localytics_remove_OnLocalyticsSessionDidOpen_m2573132226 (Il2CppObject * __this /* static, unused */, LocalyticsSessionDidOpen_t115241990 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::add_OnLocalyticsSessionWillClose(LocalyticsUnity.Localytics/LocalyticsSessionWillClose)
extern "C"  void Localytics_add_OnLocalyticsSessionWillClose_m419650807 (Il2CppObject * __this /* static, unused */, LocalyticsSessionWillClose_t2690468515 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::remove_OnLocalyticsSessionWillClose(LocalyticsUnity.Localytics/LocalyticsSessionWillClose)
extern "C"  void Localytics_remove_OnLocalyticsSessionWillClose_m1607944328 (Il2CppObject * __this /* static, unused */, LocalyticsSessionWillClose_t2690468515 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::add_OnLocalyticsSessionWillOpen(LocalyticsUnity.Localytics/LocalyticsSessionWillOpen)
extern "C"  void Localytics_add_OnLocalyticsSessionWillOpen_m1951830637 (Il2CppObject * __this /* static, unused */, LocalyticsSessionWillOpen_t2581002303 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::remove_OnLocalyticsSessionWillOpen(LocalyticsUnity.Localytics/LocalyticsSessionWillOpen)
extern "C"  void Localytics_remove_OnLocalyticsSessionWillOpen_m1604464190 (Il2CppObject * __this /* static, unused */, LocalyticsSessionWillOpen_t2581002303 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::add_OnLocalyticsDidDismissInAppMessage(LocalyticsUnity.Localytics/LocalyticsDidDismissInAppMessage)
extern "C"  void Localytics_add_OnLocalyticsDidDismissInAppMessage_m652407851 (Il2CppObject * __this /* static, unused */, LocalyticsDidDismissInAppMessage_t1872466793 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::remove_OnLocalyticsDidDismissInAppMessage(LocalyticsUnity.Localytics/LocalyticsDidDismissInAppMessage)
extern "C"  void Localytics_remove_OnLocalyticsDidDismissInAppMessage_m677711420 (Il2CppObject * __this /* static, unused */, LocalyticsDidDismissInAppMessage_t1872466793 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::add_OnLocalyticsDidDisplayInAppMessage(LocalyticsUnity.Localytics/LocalyticsDidDisplayInAppMessage)
extern "C"  void Localytics_add_OnLocalyticsDidDisplayInAppMessage_m2087928251 (Il2CppObject * __this /* static, unused */, LocalyticsDidDisplayInAppMessage_t3613222305 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::remove_OnLocalyticsDidDisplayInAppMessage(LocalyticsUnity.Localytics/LocalyticsDidDisplayInAppMessage)
extern "C"  void Localytics_remove_OnLocalyticsDidDisplayInAppMessage_m2113231820 (Il2CppObject * __this /* static, unused */, LocalyticsDidDisplayInAppMessage_t3613222305 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::add_OnLocalyticsWillDismissInAppMessage(LocalyticsUnity.Localytics/LocalyticsWillDismissInAppMessage)
extern "C"  void Localytics_add_OnLocalyticsWillDismissInAppMessage_m2235619789 (Il2CppObject * __this /* static, unused */, LocalyticsWillDismissInAppMessage_t2580912496 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::remove_OnLocalyticsWillDismissInAppMessage(LocalyticsUnity.Localytics/LocalyticsWillDismissInAppMessage)
extern "C"  void Localytics_remove_OnLocalyticsWillDismissInAppMessage_m782545822 (Il2CppObject * __this /* static, unused */, LocalyticsWillDismissInAppMessage_t2580912496 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::add_OnLocalyticsWillDisplayInAppMessage(LocalyticsUnity.Localytics/LocalyticsWillDisplayInAppMessage)
extern "C"  void Localytics_add_OnLocalyticsWillDisplayInAppMessage_m4087123661 (Il2CppObject * __this /* static, unused */, LocalyticsWillDisplayInAppMessage_t26700712 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::remove_OnLocalyticsWillDisplayInAppMessage(LocalyticsUnity.Localytics/LocalyticsWillDisplayInAppMessage)
extern "C"  void Localytics_remove_OnLocalyticsWillDisplayInAppMessage_m2634049694 (Il2CppObject * __this /* static, unused */, LocalyticsWillDisplayInAppMessage_t26700712 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LocalyticsUnity.Localytics::get_AnalyticsHost()
extern "C"  String_t* Localytics_get_AnalyticsHost_m1979565532 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::set_AnalyticsHost(System.String)
extern "C"  void Localytics_set_AnalyticsHost_m3145765239 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LocalyticsUnity.Localytics::get_AppKey()
extern "C"  String_t* Localytics_get_AppKey_m819823762 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LocalyticsUnity.Localytics::get_CustomerId()
extern "C"  String_t* Localytics_get_CustomerId_m1517405549 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::set_CustomerId(System.String)
extern "C"  void Localytics_set_CustomerId_m3487816068 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LocalyticsUnity.Localytics/InAppMessageDismissButtonLocation LocalyticsUnity.Localytics::get_InAppMessageDismissButtonLocationEnum()
extern "C"  int32_t Localytics_get_InAppMessageDismissButtonLocationEnum_m1519812625 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::set_InAppMessageDismissButtonLocationEnum(LocalyticsUnity.Localytics/InAppMessageDismissButtonLocation)
extern "C"  void Localytics_set_InAppMessageDismissButtonLocationEnum_m3542779598 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LocalyticsUnity.Localytics::get_InstallId()
extern "C"  String_t* Localytics_get_InstallId_m3132941348 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LocalyticsUnity.Localytics::get_LibraryVersion()
extern "C"  String_t* Localytics_get_LibraryVersion_m2467127537 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LocalyticsUnity.Localytics::get_LoggingEnabled()
extern "C"  bool Localytics_get_LoggingEnabled_m620943751 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::set_LoggingEnabled(System.Boolean)
extern "C"  void Localytics_set_LoggingEnabled_m257793150 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LocalyticsUnity.Localytics::get_MessagingHost()
extern "C"  String_t* Localytics_get_MessagingHost_m1430473434 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::set_MessagingHost(System.String)
extern "C"  void Localytics_set_MessagingHost_m174037305 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LocalyticsUnity.Localytics::get_OptedOut()
extern "C"  bool Localytics_get_OptedOut_m839117441 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::set_OptedOut(System.Boolean)
extern "C"  void Localytics_set_OptedOut_m1115668472 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LocalyticsUnity.Localytics::get_ProfilesHost()
extern "C"  String_t* Localytics_get_ProfilesHost_m3564794246 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::set_ProfilesHost(System.String)
extern "C"  void Localytics_set_ProfilesHost_m3117737547 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LocalyticsUnity.Localytics::get_TestModeEnabled()
extern "C"  bool Localytics_get_TestModeEnabled_m3712260233 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::set_TestModeEnabled(System.Boolean)
extern "C"  void Localytics_set_TestModeEnabled_m3092716672 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double LocalyticsUnity.Localytics::get_SessionTimeoutInterval()
extern "C"  double Localytics_get_SessionTimeoutInterval_m1713454500 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::set_SessionTimeoutInterval(System.Double)
extern "C"  void Localytics_set_SessionTimeoutInterval_m1562903725 (Il2CppObject * __this /* static, unused */, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::AddProfileAttributesToSet(System.String,System.Int64[],LocalyticsUnity.Localytics/ProfileScope)
extern "C"  void Localytics_AddProfileAttributesToSet_m2948220263 (Il2CppObject * __this /* static, unused */, String_t* ___attributeName0, Int64U5BU5D_t753178071* ___attributeValue1, int32_t ___scope2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::AddProfileAttributesToSet(System.String,System.String[],LocalyticsUnity.Localytics/ProfileScope)
extern "C"  void Localytics_AddProfileAttributesToSet_m2199160331 (Il2CppObject * __this /* static, unused */, String_t* ___attributeName0, StringU5BU5D_t2956870243* ___attributeValue1, int32_t ___scope2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::CloseSession()
extern "C"  void Localytics_CloseSession_m643641830 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::DecrementProfileAttribute(System.String,System.Int64,LocalyticsUnity.Localytics/ProfileScope)
extern "C"  void Localytics_DecrementProfileAttribute_m1540356871 (Il2CppObject * __this /* static, unused */, String_t* ___attributeName0, int64_t ___decrementValue1, int32_t ___scope2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::DeleteProfileAttribute(System.String,LocalyticsUnity.Localytics/ProfileScope)
extern "C"  void Localytics_DeleteProfileAttribute_m860759341 (Il2CppObject * __this /* static, unused */, String_t* ___attributeName0, int32_t ___scope1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::SetCustomerEmail(System.String)
extern "C"  void Localytics_SetCustomerEmail_m3350313726 (Il2CppObject * __this /* static, unused */, String_t* ___email0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::SetCustomerFirstName(System.String)
extern "C"  void Localytics_SetCustomerFirstName_m3155305183 (Il2CppObject * __this /* static, unused */, String_t* ___firstName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::SetCustomerLastName(System.String)
extern "C"  void Localytics_SetCustomerLastName_m2358470183 (Il2CppObject * __this /* static, unused */, String_t* ___lastName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::SetCustomerFullName(System.String)
extern "C"  void Localytics_SetCustomerFullName_m3164535182 (Il2CppObject * __this /* static, unused */, String_t* ___fullName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::DismissCurrentInAppMessage()
extern "C"  void Localytics_DismissCurrentInAppMessage_m3819815938 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LocalyticsUnity.Localytics::GetCustomDimension(System.Int32)
extern "C"  String_t* Localytics_GetCustomDimension_m6096923 (Il2CppObject * __this /* static, unused */, int32_t ___dimension0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LocalyticsUnity.Localytics::GetIdentifier(System.String)
extern "C"  String_t* Localytics_GetIdentifier_m3747102956 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::IncrementProfileAttribute(System.String,System.Int64,LocalyticsUnity.Localytics/ProfileScope)
extern "C"  void Localytics_IncrementProfileAttribute_m3498300387 (Il2CppObject * __this /* static, unused */, String_t* ___attributeName0, int64_t ___attributeValue1, int32_t ___scope2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::OpenSession()
extern "C"  void Localytics_OpenSession_m2787520294 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::RegisterForAnalyticsEvents()
extern "C"  void Localytics_RegisterForAnalyticsEvents_m3824577953 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::UnregisterForAnalyticsEvents()
extern "C"  void Localytics_UnregisterForAnalyticsEvents_m712507130 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::RegisterForMessagingEvents()
extern "C"  void Localytics_RegisterForMessagingEvents_m133081887 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::UnregisterForMessagingEvents()
extern "C"  void Localytics_UnregisterForMessagingEvents_m1315978360 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::RemoveProfileAttributesFromSet(System.String,System.Int64[],LocalyticsUnity.Localytics/ProfileScope)
extern "C"  void Localytics_RemoveProfileAttributesFromSet_m1131843881 (Il2CppObject * __this /* static, unused */, String_t* ___attributeName0, Int64U5BU5D_t753178071* ___attributeValue1, int32_t ___scope2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::RemoveProfileAttributesFromSet(System.String,System.String[],LocalyticsUnity.Localytics/ProfileScope)
extern "C"  void Localytics_RemoveProfileAttributesFromSet_m1726067337 (Il2CppObject * __this /* static, unused */, String_t* ___attributeName0, StringU5BU5D_t2956870243* ___attributeValue1, int32_t ___scope2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::SetCustomDimension(System.Int32,System.String)
extern "C"  void Localytics_SetCustomDimension_m2444359208 (Il2CppObject * __this /* static, unused */, int32_t ___dimension0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::SetIdentifier(System.String,System.String)
extern "C"  void Localytics_SetIdentifier_m3659144633 (Il2CppObject * __this /* static, unused */, String_t* ___key0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::SetLocation(UnityEngine.LocationInfo)
extern "C"  void Localytics_SetLocation_m288298039 (Il2CppObject * __this /* static, unused */, LocationInfo_t1628589680  ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::SetProfileAttribute(System.String,System.Int64,LocalyticsUnity.Localytics/ProfileScope)
extern "C"  void Localytics_SetProfileAttribute_m211571734 (Il2CppObject * __this /* static, unused */, String_t* ___attributeName0, int64_t ___attributeValue1, int32_t ___scope2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::SetProfileAttribute(System.String,System.Int64[],LocalyticsUnity.Localytics/ProfileScope)
extern "C"  void Localytics_SetProfileAttribute_m498919412 (Il2CppObject * __this /* static, unused */, String_t* ___attributeName0, Int64U5BU5D_t753178071* ___attributeValue1, int32_t ___scope2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::SetProfileAttribute(System.String,System.String,LocalyticsUnity.Localytics/ProfileScope)
extern "C"  void Localytics_SetProfileAttribute_m3450528704 (Il2CppObject * __this /* static, unused */, String_t* ___attributeName0, String_t* ___attributeValue1, int32_t ___scope2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::SetProfileAttribute(System.String,System.String[],LocalyticsUnity.Localytics/ProfileScope)
extern "C"  void Localytics_SetProfileAttribute_m3580245278 (Il2CppObject * __this /* static, unused */, String_t* ___attributeName0, StringU5BU5D_t2956870243* ___attributeValue1, int32_t ___scope2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::TagEvent(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>,System.Int64)
extern "C"  void Localytics_TagEvent_m1826336763 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, Dictionary_2_t2606186806 * ___attributes1, int64_t ___customerValueIncrease2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::TagScreen(System.String)
extern "C"  void Localytics_TagScreen_m3893025890 (Il2CppObject * __this /* static, unused */, String_t* ___screen0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::TriggerInAppMessage(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void Localytics_TriggerInAppMessage_m1408598194 (Il2CppObject * __this /* static, unused */, String_t* ___triggerName0, Dictionary_2_t2606186806 * ___attributes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::Upload()
extern "C"  void Localytics_Upload_m3532132073 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LocalyticsUnity.Localytics::get_PushToken()
extern "C"  String_t* Localytics_get_PushToken_m4057863373 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LocalyticsUnity.Localytics::get_IsCollectingAdvertisingIdentifier()
extern "C"  bool Localytics_get_IsCollectingAdvertisingIdentifier_m1756652136 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::set_IsCollectingAdvertisingIdentifier(System.Boolean)
extern "C"  void Localytics_set_IsCollectingAdvertisingIdentifier_m1154786335 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LocalyticsUnity.Localytics::get_InAppAdIdParameterEnabled()
extern "C"  bool Localytics_get_InAppAdIdParameterEnabled_m1846304975 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::set_InAppAdIdParameterEnabled(System.Boolean)
extern "C"  void Localytics_set_InAppAdIdParameterEnabled_m1757815366 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LocalyticsUnity.Localytics::_appKey()
extern "C"  String_t* Localytics__appKey_m443537396 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LocalyticsUnity.Localytics::_analyticsHost()
extern "C"  String_t* Localytics__analyticsHost_m3387637818 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::_setAnalyticsHost(System.String)
extern "C"  void Localytics__setAnalyticsHost_m2514892765 (Il2CppObject * __this /* static, unused */, String_t* ___analyticsHost0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LocalyticsUnity.Localytics::_customerId()
extern "C"  String_t* Localytics__customerId_m3255277519 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::_setCustomerId(System.String)
extern "C"  void Localytics__setCustomerId_m2351447262 (Il2CppObject * __this /* static, unused */, String_t* ___customerId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 LocalyticsUnity.Localytics::_inAppMessageDismissButtonLocation()
extern "C"  uint32_t Localytics__inAppMessageDismissButtonLocation_m3941093290 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::_setInAppMessageDismissButtonLocation(System.UInt32)
extern "C"  void Localytics__setInAppMessageDismissButtonLocation_m26342109 (Il2CppObject * __this /* static, unused */, uint32_t ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LocalyticsUnity.Localytics::_installId()
extern "C"  String_t* Localytics__installId_m3050454402 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LocalyticsUnity.Localytics::_isCollectingAdvertisingIdentifier()
extern "C"  bool Localytics__isCollectingAdvertisingIdentifier_m2529620104 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::_setCollectingAdvertisingIdentifier(System.Boolean)
extern "C"  void Localytics__setCollectingAdvertisingIdentifier_m3536717187 (Il2CppObject * __this /* static, unused */, bool ___collectingAdvertisingIdentifier0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LocalyticsUnity.Localytics::_libraryVersion()
extern "C"  String_t* Localytics__libraryVersion_m3167695443 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LocalyticsUnity.Localytics::_isLoggingEnabled()
extern "C"  bool Localytics__isLoggingEnabled_m1335200689 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::_setLoggingEnabled(System.Boolean)
extern "C"  void Localytics__setLoggingEnabled_m3874701668 (Il2CppObject * __this /* static, unused */, bool ___loggingEnabled0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LocalyticsUnity.Localytics::_messagingHost()
extern "C"  String_t* Localytics__messagingHost_m2838545720 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::_setMessagingHost(System.String)
extern "C"  void Localytics__setMessagingHost_m3838132127 (Il2CppObject * __this /* static, unused */, String_t* ___messagingHost0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LocalyticsUnity.Localytics::_isOptedOut()
extern "C"  bool Localytics__isOptedOut_m2758411307 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::_setOptedOut(System.Boolean)
extern "C"  void Localytics__setOptedOut_m1356106078 (Il2CppObject * __this /* static, unused */, bool ___optedOut0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LocalyticsUnity.Localytics::_profilesHost()
extern "C"  String_t* Localytics__profilesHost_m2917479272 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::_setProfilesHost(System.String)
extern "C"  void Localytics__setProfilesHost_m1989008165 (Il2CppObject * __this /* static, unused */, String_t* ___profilesHost0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LocalyticsUnity.Localytics::_pushToken()
extern "C"  String_t* Localytics__pushToken_m3975376427 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LocalyticsUnity.Localytics::_testModeEnabled()
extern "C"  bool Localytics__testModeEnabled_m3725881001 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::_setTestModeEnabled(System.Boolean)
extern "C"  void Localytics__setTestModeEnabled_m3547731034 (Il2CppObject * __this /* static, unused */, bool ___enabled0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double LocalyticsUnity.Localytics::_sessionTimeoutInterval()
extern "C"  double Localytics__sessionTimeoutInterval_m3711220870 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::_setSessionTimeoutInterval(System.Double)
extern "C"  void Localytics__setSessionTimeoutInterval_m1493184775 (Il2CppObject * __this /* static, unused */, double ___timeoutInterval0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::_addProfileAttributesToSet(System.String,System.String,System.Int32)
extern "C"  void Localytics__addProfileAttributesToSet_m3002844298 (Il2CppObject * __this /* static, unused */, String_t* ___attribute0, String_t* ___values1, int32_t ___scope2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::_closeSession()
extern "C"  void Localytics__closeSession_m3849701175 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::_decrementProfileAttribute(System.String,System.Int64,System.Int32)
extern "C"  void Localytics__decrementProfileAttribute_m2705617202 (Il2CppObject * __this /* static, unused */, String_t* ___attributeName0, int64_t ___value1, int32_t ___scope2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::_deleteProfileAttribute(System.String,System.Int32)
extern "C"  void Localytics__deleteProfileAttribute_m340220108 (Il2CppObject * __this /* static, unused */, String_t* ___attributeName0, int32_t ___scope1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::_setCustomerEmail(System.String)
extern "C"  void Localytics__setCustomerEmail_m912150989 (Il2CppObject * __this /* static, unused */, String_t* ___email0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::_setCustomerFirstName(System.String)
extern "C"  void Localytics__setCustomerFirstName_m1105771054 (Il2CppObject * __this /* static, unused */, String_t* ___firstName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::_setCustomerLastName(System.String)
extern "C"  void Localytics__setCustomerLastName_m3539282168 (Il2CppObject * __this /* static, unused */, String_t* ___lastName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::_setCustomerFullName(System.String)
extern "C"  void Localytics__setCustomerFullName_m50379871 (Il2CppObject * __this /* static, unused */, String_t* ___fullName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::_dismissCurrentInAppMessage()
extern "C"  void Localytics__dismissCurrentInAppMessage_m2081044371 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LocalyticsUnity.Localytics::_getCustomDimension(System.Int32)
extern "C"  String_t* Localytics__getCustomDimension_m640247878 (Il2CppObject * __this /* static, unused */, int32_t ___dimension0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LocalyticsUnity.Localytics::_getIdentifier(System.String)
extern "C"  String_t* Localytics__getIdentifier_m3390664343 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::_incrementProfileAttribute(System.String,System.Int64,System.Int32)
extern "C"  void Localytics__incrementProfileAttribute_m1877823958 (Il2CppObject * __this /* static, unused */, String_t* ___attributeName0, int64_t ___attributeValue1, int32_t ___scope2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::_openSession()
extern "C"  void Localytics__openSession_m535636917 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::_registerReceiveAnalyticsCallback(LocalyticsUnity.Localytics/ReceiveAnalyticsDelegate)
extern "C"  void Localytics__registerReceiveAnalyticsCallback_m3272957910 (Il2CppObject * __this /* static, unused */, ReceiveAnalyticsDelegate_t2804656808 * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::_removeAnalyticsCallback()
extern "C"  void Localytics__removeAnalyticsCallback_m3153879504 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LocalyticsUnity.Localytics::_isInAppAdIdParameterEnabled()
extern "C"  bool Localytics__isInAppAdIdParameterEnabled_m475762789 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::_setInAppAdIdParameterEnabled(System.Boolean)
extern "C"  void Localytics__setInAppAdIdParameterEnabled_m698126752 (Il2CppObject * __this /* static, unused */, bool ___appAdIdEnabled0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::_registerReceiveMessagingCallback(LocalyticsUnity.Localytics/ReceiveMessagingDelegate)
extern "C"  void Localytics__registerReceiveMessagingCallback_m3957393242 (Il2CppObject * __this /* static, unused */, ReceiveMessagingDelegate_t3408128038 * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::_removeMessagingCallback()
extern "C"  void Localytics__removeMessagingCallback_m3269146574 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::_removeProfileAttributesFromSet(System.String,System.String,System.Int32)
extern "C"  void Localytics__removeProfileAttributesFromSet_m391145998 (Il2CppObject * __this /* static, unused */, String_t* ___attributeName0, String_t* ___attributeValue1, int32_t ___scope2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::_setCustomDimension(System.Int32,System.String)
extern "C"  void Localytics__setCustomDimension_m1383663481 (Il2CppObject * __this /* static, unused */, int32_t ___dimension0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::_setIdentifier(System.String,System.String)
extern "C"  void Localytics__setIdentifier_m393772938 (Il2CppObject * __this /* static, unused */, String_t* ___key0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::_setLocation(System.Double,System.Double)
extern "C"  void Localytics__setLocation_m2200217086 (Il2CppObject * __this /* static, unused */, double ___latitude0, double ___longitude1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::_setProfileAttribute(System.String,System.String,System.Int32)
extern "C"  void Localytics__setProfileAttribute_m1689924567 (Il2CppObject * __this /* static, unused */, String_t* ___attributeName0, String_t* ___values1, int32_t ___scope2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::_tagEvent(System.String,System.String,System.Int64)
extern "C"  void Localytics__tagEvent_m2942919859 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, String_t* ___attributes1, int64_t ___customerValueIncrease2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::_tagScreen(System.String)
extern "C"  void Localytics__tagScreen_m3783901299 (Il2CppObject * __this /* static, unused */, String_t* ___screen0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::_triggerInAppMessage(System.String,System.String)
extern "C"  void Localytics__triggerInAppMessage_m318372786 (Il2CppObject * __this /* static, unused */, String_t* ___triggerName0, String_t* ___attributes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::_upload()
extern "C"  void Localytics__upload_m533411066 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::ReceiveAnalyticsMessage(System.String)
extern "C"  void Localytics_ReceiveAnalyticsMessage_m3745756772 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalyticsUnity.Localytics::ReceiveMessagingMessage(System.String)
extern "C"  void Localytics_ReceiveMessagingMessage_m864780130 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
