﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AbstractLayout
struct AbstractLayout_t909148684;
// System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>
struct Dictionary_2_t1267230538;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Range938821841.h"

// System.Void AbstractLayout::.ctor()
extern "C"  void AbstractLayout__ctor_m3838367759 (AbstractLayout_t909148684 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AbstractLayout::Repopulate(System.Collections.Generic.Dictionary`2<System.Int32,Tacticsoft.TableViewCell>,UnityEngine.SocialPlatforms.Range,System.Single)
extern "C"  void AbstractLayout_Repopulate_m3366526491 (AbstractLayout_t909148684 * __this, Dictionary_2_t1267230538 * ___cells0, Range_t938821841  ___cellRange1, float ___inTopPadding2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AbstractLayout::Reposition()
extern "C"  void AbstractLayout_Reposition_m1384326577 (AbstractLayout_t909148684 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AbstractLayout::ActivateCell(System.Int32)
extern "C"  void AbstractLayout_ActivateCell_m1876013403 (AbstractLayout_t909148684 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
