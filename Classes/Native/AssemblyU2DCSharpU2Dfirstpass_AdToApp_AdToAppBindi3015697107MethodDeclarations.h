﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AdToApp.AdToAppBinding
struct AdToAppBinding_t3015697107;
// System.String
struct String_t;
// AdToApp.AdToAppSDKDelegate
struct AdToAppSDKDelegate_t2853395885;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3012272455;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AdToApp_AdToAppLogLev289278932.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AdToApp_AdToAppSDKDe2853395885.h"
#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

// System.Void AdToApp.AdToAppBinding::.ctor()
extern "C"  void AdToAppBinding__ctor_m914889442 (AdToAppBinding_t3015697107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppBinding::.cctor()
extern "C"  void AdToAppBinding__cctor_m2109672715 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppBinding::start(System.String,System.String)
extern "C"  void AdToAppBinding_start_m1693568636 (Il2CppObject * __this /* static, unused */, String_t* ___adContentType0, String_t* ___appId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppBinding::setLogLevel(AdToApp.AdToAppLogLevel)
extern "C"  void AdToAppBinding_setLogLevel_m1570455448 (Il2CppObject * __this /* static, unused */, int32_t ___logLevel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AdToApp.AdToAppBinding::showInterstitial(System.String)
extern "C"  bool AdToAppBinding_showInterstitial_m1419824827 (Il2CppObject * __this /* static, unused */, String_t* ___adContentType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AdToApp.AdToAppBinding::hasInterstitial(System.String)
extern "C"  bool AdToAppBinding_hasInterstitial_m4192788120 (Il2CppObject * __this /* static, unused */, String_t* ___adContentType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AdToApp.AdToAppBinding::isInterstitialDisplayed()
extern "C"  bool AdToAppBinding_isInterstitialDisplayed_m1801670031 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppBinding::showBanner(System.String,System.Single,System.Single,System.Single,System.Single)
extern "C"  void AdToAppBinding_showBanner_m2188649323 (Il2CppObject * __this /* static, unused */, String_t* ___bannerSize0, float ___x1, float ___y2, float ___width3, float ___height4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppBinding::showBannerAtPosition(System.String,System.String,System.Single,System.Single,System.Single,System.Single)
extern "C"  void AdToAppBinding_showBannerAtPosition_m617533131 (Il2CppObject * __this /* static, unused */, String_t* ___position0, String_t* ___bannerSize1, float ___marginTop2, float ___marginLeft3, float ___marginBottom4, float ___marginRight5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppBinding::loadNextBanner()
extern "C"  void AdToAppBinding_loadNextBanner_m566023431 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppBinding::setBannerRefreshInterval(System.Double)
extern "C"  void AdToAppBinding_setBannerRefreshInterval_m3479436302 (Il2CppObject * __this /* static, unused */, double ___refreshInterval0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppBinding::removeAllBanners()
extern "C"  void AdToAppBinding_removeAllBanners_m1851435916 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppBinding::setTestMode(System.Boolean)
extern "C"  void AdToAppBinding_setTestMode_m2621369390 (Il2CppObject * __this /* static, unused */, bool ___isEnabled0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AdToApp.AdToAppBinding::getRewardedCurrency()
extern "C"  String_t* AdToAppBinding_getRewardedCurrency_m340757522 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AdToApp.AdToAppBinding::getRewardedValue()
extern "C"  String_t* AdToAppBinding_getRewardedValue_m3495948018 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppBinding::setTargetingParam(System.String,System.String)
extern "C"  void AdToAppBinding_setTargetingParam_m1383159680 (Il2CppObject * __this /* static, unused */, String_t* ___parameterName0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppBinding::setCallbacks(AdToApp.AdToAppSDKDelegate)
extern "C"  void AdToAppBinding_setCallbacks_m2190494663 (Il2CppObject * __this /* static, unused */, AdToAppSDKDelegate_t2853395885 * ___sdkDelegate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppBinding::onPause(System.Boolean)
extern "C"  void AdToAppBinding_onPause_m1474729774 (Il2CppObject * __this /* static, unused */, bool ___pauseStatus0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppBinding::setIOSCallback(UnityEngine.MonoBehaviour)
extern "C"  void AdToAppBinding_setIOSCallback_m795486493 (Il2CppObject * __this /* static, unused */, MonoBehaviour_t3012272455 * ___sdkDelegate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppBinding::AdToApp_start_platform(System.String,System.String)
extern "C"  void AdToAppBinding_AdToApp_start_platform_m2824761840 (Il2CppObject * __this /* static, unused */, String_t* ___appId0, String_t* ___adContentType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppBinding::AdToApp_setUnityCallbackTargetName_platform(System.String)
extern "C"  void AdToAppBinding_AdToApp_setUnityCallbackTargetName_platform_m3138860064 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AdToApp.AdToAppBinding::AdToApp_isInterstitialDisplayed_platform()
extern "C"  bool AdToAppBinding_AdToApp_isInterstitialDisplayed_platform_m2005541665 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AdToApp.AdToAppBinding::AdToApp_hasInterstitial_platform(System.String)
extern "C"  bool AdToAppBinding_AdToApp_hasInterstitial_platform_m451624540 (Il2CppObject * __this /* static, unused */, String_t* ___adContentType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppBinding::AdToApp_removeAllBanners_platform()
extern "C"  void AdToAppBinding_AdToApp_removeAllBanners_platform_m1600797196 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppBinding::AdToApp_showBannerAtPosition_platform(System.String,System.String,System.Single,System.Single,System.Single,System.Single)
extern "C"  void AdToAppBinding_AdToApp_showBannerAtPosition_platform_m3180678625 (Il2CppObject * __this /* static, unused */, String_t* ___position0, String_t* ___bannerSize1, float ___marginTop2, float ___marginLeft3, float ___marginBottom4, float ___marginRight5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppBinding::AdToApp_showBanner_platform(System.Single,System.Single,System.Single,System.Single,System.String)
extern "C"  void AdToAppBinding_AdToApp_showBanner_platform_m119572809 (Il2CppObject * __this /* static, unused */, float ___x0, float ___y1, float ___width2, float ___height3, String_t* ___bannerSize4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppBinding::AdToApp_setBannerRefreshInterval_platform(System.Double)
extern "C"  void AdToAppBinding_AdToApp_setBannerRefreshInterval_platform_m278725342 (Il2CppObject * __this /* static, unused */, double ___refreshInterval0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppBinding::AdToApp_setLogLevel_platform(System.Int32)
extern "C"  void AdToAppBinding_AdToApp_setLogLevel_platform_m660456159 (Il2CppObject * __this /* static, unused */, int32_t ___logLevel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AdToApp.AdToAppBinding::AdToApp_showInterstitial_platform(System.String)
extern "C"  bool AdToAppBinding_AdToApp_showInterstitial_platform_m1626468945 (Il2CppObject * __this /* static, unused */, String_t* ___adContentType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppBinding::AdToApp_loadNextBanner_platform()
extern "C"  void AdToAppBinding_AdToApp_loadNextBanner_platform_m2891887601 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdToApp.AdToAppBinding::AdToApp_setTargetingParam_platform(System.String,System.String)
extern "C"  void AdToAppBinding_AdToApp_setTargetingParam_platform_m1257445228 (Il2CppObject * __this /* static, unused */, String_t* ___parameterName0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AdToApp.AdToAppBinding::AdToApp_rewardedCurrency_platform()
extern "C"  String_t* AdToAppBinding_AdToApp_rewardedCurrency_platform_m2748434740 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AdToApp.AdToAppBinding::AdToApp_rewardedValue_platform()
extern "C"  String_t* AdToAppBinding_AdToApp_rewardedValue_platform_m991686128 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
