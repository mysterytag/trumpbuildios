﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey95
struct U3CAsObservableU3Ec__AnonStorey95_t3743595094;
// UnityEngine.Events.UnityAction
struct UnityAction_t909267611;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Events_UnityAction909267611.h"

// System.Void UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey95::.ctor()
extern "C"  void U3CAsObservableU3Ec__AnonStorey95__ctor_m1183975831 (U3CAsObservableU3Ec__AnonStorey95_t3743595094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey95::<>m__C7(UnityEngine.Events.UnityAction)
extern "C"  void U3CAsObservableU3Ec__AnonStorey95_U3CU3Em__C7_m1129494599 (U3CAsObservableU3Ec__AnonStorey95_t3743595094 * __this, UnityAction_t909267611 * ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey95::<>m__C8(UnityEngine.Events.UnityAction)
extern "C"  void U3CAsObservableU3Ec__AnonStorey95_U3CU3Em__C8_m3240784968 (U3CAsObservableU3Ec__AnonStorey95_t3743595094 * __this, UnityAction_t909267611 * ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
