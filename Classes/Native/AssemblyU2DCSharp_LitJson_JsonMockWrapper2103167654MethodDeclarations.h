﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LitJson.JsonMockWrapper
struct JsonMockWrapper_t2103167654;
// System.Object
struct Il2CppObject;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.ICollection
struct ICollection_t3761522009;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1541724277;
// System.String
struct String_t;
// LitJson.JsonWriter
struct JsonWriter_t3021344960;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharp_LitJson_JsonType2848171399.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_LitJson_JsonWriter3021344960.h"

// System.Void LitJson.JsonMockWrapper::.ctor()
extern "C"  void JsonMockWrapper__ctor_m2298562073 (JsonMockWrapper_t2103167654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonMockWrapper::System.Collections.IList.get_IsFixedSize()
extern "C"  bool JsonMockWrapper_System_Collections_IList_get_IsFixedSize_m2444906490 (JsonMockWrapper_t2103167654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonMockWrapper::System.Collections.IList.get_IsReadOnly()
extern "C"  bool JsonMockWrapper_System_Collections_IList_get_IsReadOnly_m3165276095 (JsonMockWrapper_t2103167654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LitJson.JsonMockWrapper::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * JsonMockWrapper_System_Collections_IList_get_Item_m308707896 (JsonMockWrapper_t2103167654 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMockWrapper::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void JsonMockWrapper_System_Collections_IList_set_Item_m513941637 (JsonMockWrapper_t2103167654 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LitJson.JsonMockWrapper::System.Collections.IList.Add(System.Object)
extern "C"  int32_t JsonMockWrapper_System_Collections_IList_Add_m4070550869 (JsonMockWrapper_t2103167654 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMockWrapper::System.Collections.IList.Clear()
extern "C"  void JsonMockWrapper_System_Collections_IList_Clear_m3144864857 (JsonMockWrapper_t2103167654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonMockWrapper::System.Collections.IList.Contains(System.Object)
extern "C"  bool JsonMockWrapper_System_Collections_IList_Contains_m2755447883 (JsonMockWrapper_t2103167654 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LitJson.JsonMockWrapper::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t JsonMockWrapper_System_Collections_IList_IndexOf_m3304010029 (JsonMockWrapper_t2103167654 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMockWrapper::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void JsonMockWrapper_System_Collections_IList_Insert_m4223354926 (JsonMockWrapper_t2103167654 * __this, int32_t ___i0, Il2CppObject * ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMockWrapper::System.Collections.IList.Remove(System.Object)
extern "C"  void JsonMockWrapper_System_Collections_IList_Remove_m180399674 (JsonMockWrapper_t2103167654 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMockWrapper::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void JsonMockWrapper_System_Collections_IList_RemoveAt_m2882778302 (JsonMockWrapper_t2103167654 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LitJson.JsonMockWrapper::System.Collections.ICollection.get_Count()
extern "C"  int32_t JsonMockWrapper_System_Collections_ICollection_get_Count_m2756782628 (JsonMockWrapper_t2103167654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonMockWrapper::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool JsonMockWrapper_System_Collections_ICollection_get_IsSynchronized_m2127897393 (JsonMockWrapper_t2103167654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LitJson.JsonMockWrapper::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * JsonMockWrapper_System_Collections_ICollection_get_SyncRoot_m2399209713 (JsonMockWrapper_t2103167654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMockWrapper::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void JsonMockWrapper_System_Collections_ICollection_CopyTo_m283498827 (JsonMockWrapper_t2103167654 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator LitJson.JsonMockWrapper::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * JsonMockWrapper_System_Collections_IEnumerable_GetEnumerator_m1261650728 (JsonMockWrapper_t2103167654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonMockWrapper::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool JsonMockWrapper_System_Collections_IDictionary_get_IsFixedSize_m3553300658 (JsonMockWrapper_t2103167654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonMockWrapper::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool JsonMockWrapper_System_Collections_IDictionary_get_IsReadOnly_m430084103 (JsonMockWrapper_t2103167654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ICollection LitJson.JsonMockWrapper::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * JsonMockWrapper_System_Collections_IDictionary_get_Keys_m3368643401 (JsonMockWrapper_t2103167654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ICollection LitJson.JsonMockWrapper::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * JsonMockWrapper_System_Collections_IDictionary_get_Values_m1228521975 (JsonMockWrapper_t2103167654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LitJson.JsonMockWrapper::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * JsonMockWrapper_System_Collections_IDictionary_get_Item_m2541212933 (JsonMockWrapper_t2103167654 * __this, Il2CppObject * ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMockWrapper::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void JsonMockWrapper_System_Collections_IDictionary_set_Item_m1141717428 (JsonMockWrapper_t2103167654 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMockWrapper::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void JsonMockWrapper_System_Collections_IDictionary_Add_m1923138589 (JsonMockWrapper_t2103167654 * __this, Il2CppObject * ___k0, Il2CppObject * ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMockWrapper::System.Collections.IDictionary.Clear()
extern "C"  void JsonMockWrapper_System_Collections_IDictionary_Clear_m1229300753 (JsonMockWrapper_t2103167654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonMockWrapper::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool JsonMockWrapper_System_Collections_IDictionary_Contains_m3426231043 (JsonMockWrapper_t2103167654 * __this, Il2CppObject * ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMockWrapper::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void JsonMockWrapper_System_Collections_IDictionary_Remove_m28417010 (JsonMockWrapper_t2103167654 * __this, Il2CppObject * ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionaryEnumerator LitJson.JsonMockWrapper::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * JsonMockWrapper_System_Collections_IDictionary_GetEnumerator_m25952300 (JsonMockWrapper_t2103167654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LitJson.JsonMockWrapper::System.Collections.Specialized.IOrderedDictionary.get_Item(System.Int32)
extern "C"  Il2CppObject * JsonMockWrapper_System_Collections_Specialized_IOrderedDictionary_get_Item_m2832629722 (JsonMockWrapper_t2103167654 * __this, int32_t ___idx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMockWrapper::System.Collections.Specialized.IOrderedDictionary.set_Item(System.Int32,System.Object)
extern "C"  void JsonMockWrapper_System_Collections_Specialized_IOrderedDictionary_set_Item_m877554417 (JsonMockWrapper_t2103167654 * __this, int32_t ___idx0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionaryEnumerator LitJson.JsonMockWrapper::System.Collections.Specialized.IOrderedDictionary.GetEnumerator()
extern "C"  Il2CppObject * JsonMockWrapper_System_Collections_Specialized_IOrderedDictionary_GetEnumerator_m1533740524 (JsonMockWrapper_t2103167654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMockWrapper::System.Collections.Specialized.IOrderedDictionary.Insert(System.Int32,System.Object,System.Object)
extern "C"  void JsonMockWrapper_System_Collections_Specialized_IOrderedDictionary_Insert_m3293885160 (JsonMockWrapper_t2103167654 * __this, int32_t ___i0, Il2CppObject * ___k1, Il2CppObject * ___v2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMockWrapper::System.Collections.Specialized.IOrderedDictionary.RemoveAt(System.Int32)
extern "C"  void JsonMockWrapper_System_Collections_Specialized_IOrderedDictionary_RemoveAt_m1077636138 (JsonMockWrapper_t2103167654 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonMockWrapper::get_IsArray()
extern "C"  bool JsonMockWrapper_get_IsArray_m4042997841 (JsonMockWrapper_t2103167654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonMockWrapper::get_IsBoolean()
extern "C"  bool JsonMockWrapper_get_IsBoolean_m1860243616 (JsonMockWrapper_t2103167654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonMockWrapper::get_IsDouble()
extern "C"  bool JsonMockWrapper_get_IsDouble_m3431283771 (JsonMockWrapper_t2103167654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonMockWrapper::get_IsInt()
extern "C"  bool JsonMockWrapper_get_IsInt_m3063986727 (JsonMockWrapper_t2103167654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonMockWrapper::get_IsLong()
extern "C"  bool JsonMockWrapper_get_IsLong_m581000806 (JsonMockWrapper_t2103167654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonMockWrapper::get_IsObject()
extern "C"  bool JsonMockWrapper_get_IsObject_m2162311209 (JsonMockWrapper_t2103167654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonMockWrapper::get_IsString()
extern "C"  bool JsonMockWrapper_get_IsString_m3866824827 (JsonMockWrapper_t2103167654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonMockWrapper::GetBoolean()
extern "C"  bool JsonMockWrapper_GetBoolean_m780678601 (JsonMockWrapper_t2103167654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double LitJson.JsonMockWrapper::GetDouble()
extern "C"  double JsonMockWrapper_GetDouble_m2331478561 (JsonMockWrapper_t2103167654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LitJson.JsonMockWrapper::GetInt()
extern "C"  int32_t JsonMockWrapper_GetInt_m2684287926 (JsonMockWrapper_t2103167654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LitJson.JsonType LitJson.JsonMockWrapper::GetJsonType()
extern "C"  int32_t JsonMockWrapper_GetJsonType_m2235548123 (JsonMockWrapper_t2103167654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 LitJson.JsonMockWrapper::GetLong()
extern "C"  int64_t JsonMockWrapper_GetLong_m2290170360 (JsonMockWrapper_t2103167654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LitJson.JsonMockWrapper::GetString()
extern "C"  String_t* JsonMockWrapper_GetString_m1230316065 (JsonMockWrapper_t2103167654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMockWrapper::SetBoolean(System.Boolean)
extern "C"  void JsonMockWrapper_SetBoolean_m1189388328 (JsonMockWrapper_t2103167654 * __this, bool ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMockWrapper::SetDouble(System.Double)
extern "C"  void JsonMockWrapper_SetDouble_m1299872056 (JsonMockWrapper_t2103167654 * __this, double ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMockWrapper::SetInt(System.Int32)
extern "C"  void JsonMockWrapper_SetInt_m1064501193 (JsonMockWrapper_t2103167654 * __this, int32_t ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMockWrapper::SetJsonType(LitJson.JsonType)
extern "C"  void JsonMockWrapper_SetJsonType_m2184611114 (JsonMockWrapper_t2103167654 * __this, int32_t ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMockWrapper::SetLong(System.Int64)
extern "C"  void JsonMockWrapper_SetLong_m3445849479 (JsonMockWrapper_t2103167654 * __this, int64_t ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMockWrapper::SetString(System.String)
extern "C"  void JsonMockWrapper_SetString_m233713848 (JsonMockWrapper_t2103167654 * __this, String_t* ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LitJson.JsonMockWrapper::ToJson()
extern "C"  String_t* JsonMockWrapper_ToJson_m3264004363 (JsonMockWrapper_t2103167654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMockWrapper::ToJson(LitJson.JsonWriter)
extern "C"  void JsonMockWrapper_ToJson_m3712121828 (JsonMockWrapper_t2103167654 * __this, JsonWriter_t3021344960 * ___writer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
