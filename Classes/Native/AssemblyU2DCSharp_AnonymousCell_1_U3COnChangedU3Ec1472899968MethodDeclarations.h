﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnonymousCell`1/<OnChanged>c__AnonStoreyFA<System.Double>
struct U3COnChangedU3Ec__AnonStoreyFA_t1472899968;

#include "codegen/il2cpp-codegen.h"

// System.Void AnonymousCell`1/<OnChanged>c__AnonStoreyFA<System.Double>::.ctor()
extern "C"  void U3COnChangedU3Ec__AnonStoreyFA__ctor_m44044589_gshared (U3COnChangedU3Ec__AnonStoreyFA_t1472899968 * __this, const MethodInfo* method);
#define U3COnChangedU3Ec__AnonStoreyFA__ctor_m44044589(__this, method) ((  void (*) (U3COnChangedU3Ec__AnonStoreyFA_t1472899968 *, const MethodInfo*))U3COnChangedU3Ec__AnonStoreyFA__ctor_m44044589_gshared)(__this, method)
// System.Void AnonymousCell`1/<OnChanged>c__AnonStoreyFA<System.Double>::<>m__170(T)
extern "C"  void U3COnChangedU3Ec__AnonStoreyFA_U3CU3Em__170_m297614312_gshared (U3COnChangedU3Ec__AnonStoreyFA_t1472899968 * __this, double ____0, const MethodInfo* method);
#define U3COnChangedU3Ec__AnonStoreyFA_U3CU3Em__170_m297614312(__this, ____0, method) ((  void (*) (U3COnChangedU3Ec__AnonStoreyFA_t1472899968 *, double, const MethodInfo*))U3COnChangedU3Ec__AnonStoreyFA_U3CU3Em__170_m297614312_gshared)(__this, ____0, method)
