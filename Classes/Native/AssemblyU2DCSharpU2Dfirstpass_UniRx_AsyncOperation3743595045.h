﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AsyncOperation
struct AsyncOperation_t3374395064;
struct AsyncOperation_t3374395064_marshaled_pinvoke;
// UniRx.IProgress`1<System.Single>
struct IProgress_1_t4173802291;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.AsyncOperationExtensions/<AsObservable>c__AnonStorey7B
struct  U3CAsObservableU3Ec__AnonStorey7B_t3743595045  : public Il2CppObject
{
public:
	// UnityEngine.AsyncOperation UniRx.AsyncOperationExtensions/<AsObservable>c__AnonStorey7B::asyncOperation
	AsyncOperation_t3374395064 * ___asyncOperation_0;
	// UniRx.IProgress`1<System.Single> UniRx.AsyncOperationExtensions/<AsObservable>c__AnonStorey7B::progress
	Il2CppObject* ___progress_1;

public:
	inline static int32_t get_offset_of_asyncOperation_0() { return static_cast<int32_t>(offsetof(U3CAsObservableU3Ec__AnonStorey7B_t3743595045, ___asyncOperation_0)); }
	inline AsyncOperation_t3374395064 * get_asyncOperation_0() const { return ___asyncOperation_0; }
	inline AsyncOperation_t3374395064 ** get_address_of_asyncOperation_0() { return &___asyncOperation_0; }
	inline void set_asyncOperation_0(AsyncOperation_t3374395064 * value)
	{
		___asyncOperation_0 = value;
		Il2CppCodeGenWriteBarrier(&___asyncOperation_0, value);
	}

	inline static int32_t get_offset_of_progress_1() { return static_cast<int32_t>(offsetof(U3CAsObservableU3Ec__AnonStorey7B_t3743595045, ___progress_1)); }
	inline Il2CppObject* get_progress_1() const { return ___progress_1; }
	inline Il2CppObject** get_address_of_progress_1() { return &___progress_1; }
	inline void set_progress_1(Il2CppObject* value)
	{
		___progress_1 = value;
		Il2CppCodeGenWriteBarrier(&___progress_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
