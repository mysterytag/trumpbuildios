﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayFab.ClientModels.PaymentOption
struct  PaymentOption_t1289274891  : public Il2CppObject
{
public:
	// System.String PlayFab.ClientModels.PaymentOption::<Currency>k__BackingField
	String_t* ___U3CCurrencyU3Ek__BackingField_0;
	// System.String PlayFab.ClientModels.PaymentOption::<ProviderName>k__BackingField
	String_t* ___U3CProviderNameU3Ek__BackingField_1;
	// System.UInt32 PlayFab.ClientModels.PaymentOption::<Price>k__BackingField
	uint32_t ___U3CPriceU3Ek__BackingField_2;
	// System.UInt32 PlayFab.ClientModels.PaymentOption::<StoreCredit>k__BackingField
	uint32_t ___U3CStoreCreditU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CCurrencyU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PaymentOption_t1289274891, ___U3CCurrencyU3Ek__BackingField_0)); }
	inline String_t* get_U3CCurrencyU3Ek__BackingField_0() const { return ___U3CCurrencyU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CCurrencyU3Ek__BackingField_0() { return &___U3CCurrencyU3Ek__BackingField_0; }
	inline void set_U3CCurrencyU3Ek__BackingField_0(String_t* value)
	{
		___U3CCurrencyU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCurrencyU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CProviderNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PaymentOption_t1289274891, ___U3CProviderNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CProviderNameU3Ek__BackingField_1() const { return ___U3CProviderNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CProviderNameU3Ek__BackingField_1() { return &___U3CProviderNameU3Ek__BackingField_1; }
	inline void set_U3CProviderNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CProviderNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CProviderNameU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CPriceU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PaymentOption_t1289274891, ___U3CPriceU3Ek__BackingField_2)); }
	inline uint32_t get_U3CPriceU3Ek__BackingField_2() const { return ___U3CPriceU3Ek__BackingField_2; }
	inline uint32_t* get_address_of_U3CPriceU3Ek__BackingField_2() { return &___U3CPriceU3Ek__BackingField_2; }
	inline void set_U3CPriceU3Ek__BackingField_2(uint32_t value)
	{
		___U3CPriceU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CStoreCreditU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(PaymentOption_t1289274891, ___U3CStoreCreditU3Ek__BackingField_3)); }
	inline uint32_t get_U3CStoreCreditU3Ek__BackingField_3() const { return ___U3CStoreCreditU3Ek__BackingField_3; }
	inline uint32_t* get_address_of_U3CStoreCreditU3Ek__BackingField_3() { return &___U3CStoreCreditU3Ek__BackingField_3; }
	inline void set_U3CStoreCreditU3Ek__BackingField_3(uint32_t value)
	{
		___U3CStoreCreditU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
