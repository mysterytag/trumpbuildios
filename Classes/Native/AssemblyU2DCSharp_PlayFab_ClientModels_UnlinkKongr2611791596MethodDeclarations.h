﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.UnlinkKongregateAccountResult
struct UnlinkKongregateAccountResult_t2611791596;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.UnlinkKongregateAccountResult::.ctor()
extern "C"  void UnlinkKongregateAccountResult__ctor_m1423989693 (UnlinkKongregateAccountResult_t2611791596 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
