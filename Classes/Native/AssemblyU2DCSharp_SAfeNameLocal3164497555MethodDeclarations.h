﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SAfeNameLocal
struct SAfeNameLocal_t3164497555;

#include "codegen/il2cpp-codegen.h"

// System.Void SAfeNameLocal::.ctor()
extern "C"  void SAfeNameLocal__ctor_m85635128 (SAfeNameLocal_t3164497555 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SAfeNameLocal::Start()
extern "C"  void SAfeNameLocal_Start_m3327740216 (SAfeNameLocal_t3164497555 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SAfeNameLocal::Update()
extern "C"  void SAfeNameLocal_Update_m86583765 (SAfeNameLocal_t3164497555 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
