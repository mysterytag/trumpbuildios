﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.DoOnCompletedObservable`1/DoOnCompleted<System.Object>
struct DoOnCompleted_t4100057170;
// UniRx.Operators.DoOnCompletedObservable`1<System.Object>
struct DoOnCompletedObservable_1_t3425980779;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.DoOnCompletedObservable`1/DoOnCompleted<System.Object>::.ctor(UniRx.Operators.DoOnCompletedObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void DoOnCompleted__ctor_m3030253215_gshared (DoOnCompleted_t4100057170 * __this, DoOnCompletedObservable_1_t3425980779 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define DoOnCompleted__ctor_m3030253215(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (DoOnCompleted_t4100057170 *, DoOnCompletedObservable_1_t3425980779 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))DoOnCompleted__ctor_m3030253215_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.DoOnCompletedObservable`1/DoOnCompleted<System.Object>::Run()
extern "C"  Il2CppObject * DoOnCompleted_Run_m3422597105_gshared (DoOnCompleted_t4100057170 * __this, const MethodInfo* method);
#define DoOnCompleted_Run_m3422597105(__this, method) ((  Il2CppObject * (*) (DoOnCompleted_t4100057170 *, const MethodInfo*))DoOnCompleted_Run_m3422597105_gshared)(__this, method)
// System.Void UniRx.Operators.DoOnCompletedObservable`1/DoOnCompleted<System.Object>::OnNext(T)
extern "C"  void DoOnCompleted_OnNext_m1431881803_gshared (DoOnCompleted_t4100057170 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define DoOnCompleted_OnNext_m1431881803(__this, ___value0, method) ((  void (*) (DoOnCompleted_t4100057170 *, Il2CppObject *, const MethodInfo*))DoOnCompleted_OnNext_m1431881803_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.DoOnCompletedObservable`1/DoOnCompleted<System.Object>::OnError(System.Exception)
extern "C"  void DoOnCompleted_OnError_m3889229146_gshared (DoOnCompleted_t4100057170 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define DoOnCompleted_OnError_m3889229146(__this, ___error0, method) ((  void (*) (DoOnCompleted_t4100057170 *, Exception_t1967233988 *, const MethodInfo*))DoOnCompleted_OnError_m3889229146_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.DoOnCompletedObservable`1/DoOnCompleted<System.Object>::OnCompleted()
extern "C"  void DoOnCompleted_OnCompleted_m1155880877_gshared (DoOnCompleted_t4100057170 * __this, const MethodInfo* method);
#define DoOnCompleted_OnCompleted_m1155880877(__this, method) ((  void (*) (DoOnCompleted_t4100057170 *, const MethodInfo*))DoOnCompleted_OnCompleted_m1155880877_gshared)(__this, method)
