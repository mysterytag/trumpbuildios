﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/LinkXboxAccountResponseCallback
struct LinkXboxAccountResponseCallback_t4121067430;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.LinkXboxAccountRequest
struct LinkXboxAccountRequest_t4157425311;
// PlayFab.ClientModels.LinkXboxAccountResult
struct LinkXboxAccountResult_t4187130925;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkXboxAcc4157425311.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkXboxAcc4187130925.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/LinkXboxAccountResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void LinkXboxAccountResponseCallback__ctor_m2042605877 (LinkXboxAccountResponseCallback_t4121067430 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LinkXboxAccountResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.LinkXboxAccountRequest,PlayFab.ClientModels.LinkXboxAccountResult,PlayFab.PlayFabError,System.Object)
extern "C"  void LinkXboxAccountResponseCallback_Invoke_m4258576416 (LinkXboxAccountResponseCallback_t4121067430 * __this, String_t* ___urlPath0, int32_t ___callId1, LinkXboxAccountRequest_t4157425311 * ___request2, LinkXboxAccountResult_t4187130925 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_LinkXboxAccountResponseCallback_t4121067430(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, LinkXboxAccountRequest_t4157425311 * ___request2, LinkXboxAccountResult_t4187130925 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/LinkXboxAccountResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.LinkXboxAccountRequest,PlayFab.ClientModels.LinkXboxAccountResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LinkXboxAccountResponseCallback_BeginInvoke_m2752390553 (LinkXboxAccountResponseCallback_t4121067430 * __this, String_t* ___urlPath0, int32_t ___callId1, LinkXboxAccountRequest_t4157425311 * ___request2, LinkXboxAccountResult_t4187130925 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LinkXboxAccountResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void LinkXboxAccountResponseCallback_EndInvoke_m4120774085 (LinkXboxAccountResponseCallback_t4121067430 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
