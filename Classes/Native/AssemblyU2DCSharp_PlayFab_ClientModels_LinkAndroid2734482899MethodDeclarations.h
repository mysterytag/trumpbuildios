﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.LinkAndroidDeviceIDResult
struct LinkAndroidDeviceIDResult_t2734482899;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.LinkAndroidDeviceIDResult::.ctor()
extern "C"  void LinkAndroidDeviceIDResult__ctor_m1034275766 (LinkAndroidDeviceIDResult_t2734482899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
