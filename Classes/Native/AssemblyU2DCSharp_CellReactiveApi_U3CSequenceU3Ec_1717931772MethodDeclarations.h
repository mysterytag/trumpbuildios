﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/<Sequence>c__AnonStorey110`1<System.Object>
struct U3CSequenceU3Ec__AnonStorey110_1_t1717931772;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t3709260776;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<System.Collections.Generic.IEnumerable`1<System.Object>>
struct Action_1_t3857713481;
// System.Object
struct Il2CppObject;
// ICell`1<System.Object>
struct ICell_1_t2388737397;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Priority3194150340.h"

// System.Void CellReactiveApi/<Sequence>c__AnonStorey110`1<System.Object>::.ctor()
extern "C"  void U3CSequenceU3Ec__AnonStorey110_1__ctor_m2464883546_gshared (U3CSequenceU3Ec__AnonStorey110_1_t1717931772 * __this, const MethodInfo* method);
#define U3CSequenceU3Ec__AnonStorey110_1__ctor_m2464883546(__this, method) ((  void (*) (U3CSequenceU3Ec__AnonStorey110_1_t1717931772 *, const MethodInfo*))U3CSequenceU3Ec__AnonStorey110_1__ctor_m2464883546_gshared)(__this, method)
// System.Collections.Generic.IEnumerable`1<T> CellReactiveApi/<Sequence>c__AnonStorey110`1<System.Object>::<>m__184()
extern "C"  Il2CppObject* U3CSequenceU3Ec__AnonStorey110_1_U3CU3Em__184_m2423780908_gshared (U3CSequenceU3Ec__AnonStorey110_1_t1717931772 * __this, const MethodInfo* method);
#define U3CSequenceU3Ec__AnonStorey110_1_U3CU3Em__184_m2423780908(__this, method) ((  Il2CppObject* (*) (U3CSequenceU3Ec__AnonStorey110_1_t1717931772 *, const MethodInfo*))U3CSequenceU3Ec__AnonStorey110_1_U3CU3Em__184_m2423780908_gshared)(__this, method)
// System.IDisposable CellReactiveApi/<Sequence>c__AnonStorey110`1<System.Object>::<>m__185(System.Action`1<System.Collections.Generic.IEnumerable`1<T>>,Priority)
extern "C"  Il2CppObject * U3CSequenceU3Ec__AnonStorey110_1_U3CU3Em__185_m3963883721_gshared (U3CSequenceU3Ec__AnonStorey110_1_t1717931772 * __this, Action_1_t3857713481 * ___reaction0, int32_t ___p1, const MethodInfo* method);
#define U3CSequenceU3Ec__AnonStorey110_1_U3CU3Em__185_m3963883721(__this, ___reaction0, ___p1, method) ((  Il2CppObject * (*) (U3CSequenceU3Ec__AnonStorey110_1_t1717931772 *, Action_1_t3857713481 *, int32_t, const MethodInfo*))U3CSequenceU3Ec__AnonStorey110_1_U3CU3Em__185_m3963883721_gshared)(__this, ___reaction0, ___p1, method)
// T CellReactiveApi/<Sequence>c__AnonStorey110`1<System.Object>::<>m__198(ICell`1<T>)
extern "C"  Il2CppObject * U3CSequenceU3Ec__AnonStorey110_1_U3CU3Em__198_m1110291974_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___cell0, const MethodInfo* method);
#define U3CSequenceU3Ec__AnonStorey110_1_U3CU3Em__198_m1110291974(__this /* static, unused */, ___cell0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))U3CSequenceU3Ec__AnonStorey110_1_U3CU3Em__198_m1110291974_gshared)(__this /* static, unused */, ___cell0, method)
