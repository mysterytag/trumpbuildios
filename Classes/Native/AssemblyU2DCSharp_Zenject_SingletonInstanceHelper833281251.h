﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Zenject.PrefabSingletonProviderMap
struct PrefabSingletonProviderMap_t2514131929;
// Zenject.SingletonProviderMap
struct SingletonProviderMap_t1557411893;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SingletonInstanceHelper
struct  SingletonInstanceHelper_t833281251  : public Il2CppObject
{
public:
	// Zenject.PrefabSingletonProviderMap Zenject.SingletonInstanceHelper::_prefabSingletonProviderMap
	PrefabSingletonProviderMap_t2514131929 * ____prefabSingletonProviderMap_0;
	// Zenject.SingletonProviderMap Zenject.SingletonInstanceHelper::_singletonProviderMap
	SingletonProviderMap_t1557411893 * ____singletonProviderMap_1;

public:
	inline static int32_t get_offset_of__prefabSingletonProviderMap_0() { return static_cast<int32_t>(offsetof(SingletonInstanceHelper_t833281251, ____prefabSingletonProviderMap_0)); }
	inline PrefabSingletonProviderMap_t2514131929 * get__prefabSingletonProviderMap_0() const { return ____prefabSingletonProviderMap_0; }
	inline PrefabSingletonProviderMap_t2514131929 ** get_address_of__prefabSingletonProviderMap_0() { return &____prefabSingletonProviderMap_0; }
	inline void set__prefabSingletonProviderMap_0(PrefabSingletonProviderMap_t2514131929 * value)
	{
		____prefabSingletonProviderMap_0 = value;
		Il2CppCodeGenWriteBarrier(&____prefabSingletonProviderMap_0, value);
	}

	inline static int32_t get_offset_of__singletonProviderMap_1() { return static_cast<int32_t>(offsetof(SingletonInstanceHelper_t833281251, ____singletonProviderMap_1)); }
	inline SingletonProviderMap_t1557411893 * get__singletonProviderMap_1() const { return ____singletonProviderMap_1; }
	inline SingletonProviderMap_t1557411893 ** get_address_of__singletonProviderMap_1() { return &____singletonProviderMap_1; }
	inline void set__singletonProviderMap_1(SingletonProviderMap_t1557411893 * value)
	{
		____singletonProviderMap_1 = value;
		Il2CppCodeGenWriteBarrier(&____singletonProviderMap_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
