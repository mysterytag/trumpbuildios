﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.CatchObservable`1<System.Object>
struct CatchObservable_1_t1624243829;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<UniRx.IObservable`1<System.Object>>
struct IEnumerator_1_t2079011232;
// UniRx.SerialDisposable
struct SerialDisposable_t2547852742;
// System.Exception
struct Exception_t1967233988;
// System.Action
struct Action_t437523947;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1187768149.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.CatchObservable`1/Catch<System.Object>
struct  Catch_t3617674332  : public OperatorObserverBase_2_t1187768149
{
public:
	// UniRx.Operators.CatchObservable`1<T> UniRx.Operators.CatchObservable`1/Catch::parent
	CatchObservable_1_t1624243829 * ___parent_2;
	// System.Object UniRx.Operators.CatchObservable`1/Catch::gate
	Il2CppObject * ___gate_3;
	// System.Boolean UniRx.Operators.CatchObservable`1/Catch::isDisposed
	bool ___isDisposed_4;
	// System.Collections.Generic.IEnumerator`1<UniRx.IObservable`1<T>> UniRx.Operators.CatchObservable`1/Catch::e
	Il2CppObject* ___e_5;
	// UniRx.SerialDisposable UniRx.Operators.CatchObservable`1/Catch::subscription
	SerialDisposable_t2547852742 * ___subscription_6;
	// System.Exception UniRx.Operators.CatchObservable`1/Catch::lastException
	Exception_t1967233988 * ___lastException_7;
	// System.Action UniRx.Operators.CatchObservable`1/Catch::nextSelf
	Action_t437523947 * ___nextSelf_8;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(Catch_t3617674332, ___parent_2)); }
	inline CatchObservable_1_t1624243829 * get_parent_2() const { return ___parent_2; }
	inline CatchObservable_1_t1624243829 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(CatchObservable_1_t1624243829 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_gate_3() { return static_cast<int32_t>(offsetof(Catch_t3617674332, ___gate_3)); }
	inline Il2CppObject * get_gate_3() const { return ___gate_3; }
	inline Il2CppObject ** get_address_of_gate_3() { return &___gate_3; }
	inline void set_gate_3(Il2CppObject * value)
	{
		___gate_3 = value;
		Il2CppCodeGenWriteBarrier(&___gate_3, value);
	}

	inline static int32_t get_offset_of_isDisposed_4() { return static_cast<int32_t>(offsetof(Catch_t3617674332, ___isDisposed_4)); }
	inline bool get_isDisposed_4() const { return ___isDisposed_4; }
	inline bool* get_address_of_isDisposed_4() { return &___isDisposed_4; }
	inline void set_isDisposed_4(bool value)
	{
		___isDisposed_4 = value;
	}

	inline static int32_t get_offset_of_e_5() { return static_cast<int32_t>(offsetof(Catch_t3617674332, ___e_5)); }
	inline Il2CppObject* get_e_5() const { return ___e_5; }
	inline Il2CppObject** get_address_of_e_5() { return &___e_5; }
	inline void set_e_5(Il2CppObject* value)
	{
		___e_5 = value;
		Il2CppCodeGenWriteBarrier(&___e_5, value);
	}

	inline static int32_t get_offset_of_subscription_6() { return static_cast<int32_t>(offsetof(Catch_t3617674332, ___subscription_6)); }
	inline SerialDisposable_t2547852742 * get_subscription_6() const { return ___subscription_6; }
	inline SerialDisposable_t2547852742 ** get_address_of_subscription_6() { return &___subscription_6; }
	inline void set_subscription_6(SerialDisposable_t2547852742 * value)
	{
		___subscription_6 = value;
		Il2CppCodeGenWriteBarrier(&___subscription_6, value);
	}

	inline static int32_t get_offset_of_lastException_7() { return static_cast<int32_t>(offsetof(Catch_t3617674332, ___lastException_7)); }
	inline Exception_t1967233988 * get_lastException_7() const { return ___lastException_7; }
	inline Exception_t1967233988 ** get_address_of_lastException_7() { return &___lastException_7; }
	inline void set_lastException_7(Exception_t1967233988 * value)
	{
		___lastException_7 = value;
		Il2CppCodeGenWriteBarrier(&___lastException_7, value);
	}

	inline static int32_t get_offset_of_nextSelf_8() { return static_cast<int32_t>(offsetof(Catch_t3617674332, ___nextSelf_8)); }
	inline Action_t437523947 * get_nextSelf_8() const { return ___nextSelf_8; }
	inline Action_t437523947 ** get_address_of_nextSelf_8() { return &___nextSelf_8; }
	inline void set_nextSelf_8(Action_t437523947 * value)
	{
		___nextSelf_8 = value;
		Il2CppCodeGenWriteBarrier(&___nextSelf_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
