﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.TimerObservable/Timer
struct Timer_t80811813;
// UniRx.Operators.TimerObservable
struct TimerObservable_t1697791765;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey6D
struct  U3CSubscribeCoreU3Ec__AnonStorey6D_t1995955652  : public Il2CppObject
{
public:
	// UniRx.Operators.TimerObservable/Timer UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey6D::timerObserver
	Timer_t80811813 * ___timerObserver_0;
	// UniRx.Operators.TimerObservable UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey6D::<>f__this
	TimerObservable_t1697791765 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_timerObserver_0() { return static_cast<int32_t>(offsetof(U3CSubscribeCoreU3Ec__AnonStorey6D_t1995955652, ___timerObserver_0)); }
	inline Timer_t80811813 * get_timerObserver_0() const { return ___timerObserver_0; }
	inline Timer_t80811813 ** get_address_of_timerObserver_0() { return &___timerObserver_0; }
	inline void set_timerObserver_0(Timer_t80811813 * value)
	{
		___timerObserver_0 = value;
		Il2CppCodeGenWriteBarrier(&___timerObserver_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CSubscribeCoreU3Ec__AnonStorey6D_t1995955652, ___U3CU3Ef__this_1)); }
	inline TimerObservable_t1697791765 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline TimerObservable_t1697791765 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(TimerObservable_t1697791765 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
