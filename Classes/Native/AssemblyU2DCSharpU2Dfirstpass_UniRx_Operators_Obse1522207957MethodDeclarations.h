﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ObserveOnObservable`1<System.Object>
struct ObserveOnObservable_1_t1522207957;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// UniRx.IScheduler
struct IScheduler_t2938318244;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.ObserveOnObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,UniRx.IScheduler)
extern "C"  void ObserveOnObservable_1__ctor_m3150553362_gshared (ObserveOnObservable_1_t1522207957 * __this, Il2CppObject* ___source0, Il2CppObject * ___scheduler1, const MethodInfo* method);
#define ObserveOnObservable_1__ctor_m3150553362(__this, ___source0, ___scheduler1, method) ((  void (*) (ObserveOnObservable_1_t1522207957 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))ObserveOnObservable_1__ctor_m3150553362_gshared)(__this, ___source0, ___scheduler1, method)
// System.IDisposable UniRx.Operators.ObserveOnObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * ObserveOnObservable_1_SubscribeCore_m2846530067_gshared (ObserveOnObservable_1_t1522207957 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define ObserveOnObservable_1_SubscribeCore_m2846530067(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (ObserveOnObservable_1_t1522207957 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))ObserveOnObservable_1_SubscribeCore_m2846530067_gshared)(__this, ___observer0, ___cancel1, method)
