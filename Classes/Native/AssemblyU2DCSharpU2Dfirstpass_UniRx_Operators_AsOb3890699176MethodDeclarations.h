﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.AsObservableObservable`1<System.Boolean>
struct AsObservableObservable_1_t3890699176;
// UniRx.IObservable`1<System.Boolean>
struct IObservable_1_t4264771001;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Boolean>
struct IObserver_1_t2423004244;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.AsObservableObservable`1<System.Boolean>::.ctor(UniRx.IObservable`1<T>)
extern "C"  void AsObservableObservable_1__ctor_m417038805_gshared (AsObservableObservable_1_t3890699176 * __this, Il2CppObject* ___source0, const MethodInfo* method);
#define AsObservableObservable_1__ctor_m417038805(__this, ___source0, method) ((  void (*) (AsObservableObservable_1_t3890699176 *, Il2CppObject*, const MethodInfo*))AsObservableObservable_1__ctor_m417038805_gshared)(__this, ___source0, method)
// System.IDisposable UniRx.Operators.AsObservableObservable`1<System.Boolean>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * AsObservableObservable_1_SubscribeCore_m286231784_gshared (AsObservableObservable_1_t3890699176 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define AsObservableObservable_1_SubscribeCore_m286231784(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (AsObservableObservable_1_t3890699176 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))AsObservableObservable_1_SubscribeCore_m286231784_gshared)(__this, ___observer0, ___cancel1, method)
