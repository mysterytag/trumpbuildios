﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.OperatorObserverBase`2<System.Object,UniRx.Unit>
struct OperatorObserverBase_2_t2908947767;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t475317645;
// System.IDisposable
struct IDisposable_t1628921374;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,UniRx.Unit>::.ctor(UniRx.IObserver`1<TResult>,System.IDisposable)
extern "C"  void OperatorObserverBase_2__ctor_m476501194_gshared (OperatorObserverBase_2_t2908947767 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define OperatorObserverBase_2__ctor_m476501194(__this, ___observer0, ___cancel1, method) ((  void (*) (OperatorObserverBase_2_t2908947767 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))OperatorObserverBase_2__ctor_m476501194_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.OperatorObserverBase`2<System.Object,UniRx.Unit>::Dispose()
extern "C"  void OperatorObserverBase_2_Dispose_m73684832_gshared (OperatorObserverBase_2_t2908947767 * __this, const MethodInfo* method);
#define OperatorObserverBase_2_Dispose_m73684832(__this, method) ((  void (*) (OperatorObserverBase_2_t2908947767 *, const MethodInfo*))OperatorObserverBase_2_Dispose_m73684832_gshared)(__this, method)
