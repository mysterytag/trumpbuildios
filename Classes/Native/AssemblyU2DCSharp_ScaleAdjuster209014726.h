﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScaleAdjuster
struct  ScaleAdjuster_t209014726  : public MonoBehaviour_t3012272455
{
public:
	// System.Single ScaleAdjuster::artHeight
	float ___artHeight_2;

public:
	inline static int32_t get_offset_of_artHeight_2() { return static_cast<int32_t>(offsetof(ScaleAdjuster_t209014726, ___artHeight_2)); }
	inline float get_artHeight_2() const { return ___artHeight_2; }
	inline float* get_address_of_artHeight_2() { return &___artHeight_2; }
	inline void set_artHeight_2(float value)
	{
		___artHeight_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
