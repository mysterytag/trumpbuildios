﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.StartWithObservable`1<System.Int32>
struct StartWithObservable_1_t338430729;
// UniRx.IObservable`1<System.Int32>
struct IObservable_1_t2606213151;
// System.Func`1<System.Int32>
struct Func_1_t3990196034;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Int32>
struct IObserver_1_t764446394;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.StartWithObservable`1<System.Int32>::.ctor(UniRx.IObservable`1<T>,T)
extern "C"  void StartWithObservable_1__ctor_m3411055606_gshared (StartWithObservable_1_t338430729 * __this, Il2CppObject* ___source0, int32_t ___value1, const MethodInfo* method);
#define StartWithObservable_1__ctor_m3411055606(__this, ___source0, ___value1, method) ((  void (*) (StartWithObservable_1_t338430729 *, Il2CppObject*, int32_t, const MethodInfo*))StartWithObservable_1__ctor_m3411055606_gshared)(__this, ___source0, ___value1, method)
// System.Void UniRx.Operators.StartWithObservable`1<System.Int32>::.ctor(UniRx.IObservable`1<T>,System.Func`1<T>)
extern "C"  void StartWithObservable_1__ctor_m24185270_gshared (StartWithObservable_1_t338430729 * __this, Il2CppObject* ___source0, Func_1_t3990196034 * ___valueFactory1, const MethodInfo* method);
#define StartWithObservable_1__ctor_m24185270(__this, ___source0, ___valueFactory1, method) ((  void (*) (StartWithObservable_1_t338430729 *, Il2CppObject*, Func_1_t3990196034 *, const MethodInfo*))StartWithObservable_1__ctor_m24185270_gshared)(__this, ___source0, ___valueFactory1, method)
// System.IDisposable UniRx.Operators.StartWithObservable`1<System.Int32>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * StartWithObservable_1_SubscribeCore_m3383464361_gshared (StartWithObservable_1_t338430729 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define StartWithObservable_1_SubscribeCore_m3383464361(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (StartWithObservable_1_t338430729 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))StartWithObservable_1_SubscribeCore_m3383464361_gshared)(__this, ___observer0, ___cancel1, method)
