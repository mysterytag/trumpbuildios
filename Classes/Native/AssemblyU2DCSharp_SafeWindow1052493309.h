﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Subject`1<UniRx.Unit>
struct Subject_1_t201353362;
// GlobalStat
struct GlobalStat_t1134678199;
// UnityEngine.UI.Text
struct Text_t3286458198;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// OligarchParameters
struct OligarchParameters_t821144443;
// UnityEngine.UI.Image
struct Image_t3354615620;
// GuiController
struct GuiController_t1495593751;
// System.Action`1<UniRx.Unit>
struct Action_1_t2706738743;
// System.Func`2<GlobalStat,System.Double>
struct Func_2_t2227950619;

#include "AssemblyU2DCSharp_WindowBase3855465217.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SafeWindow
struct  SafeWindow_t1052493309  : public WindowBase_t3855465217
{
public:
	// UniRx.Subject`1<UniRx.Unit> SafeWindow::OnClose
	Subject_1_t201353362 * ___OnClose_6;
	// UniRx.Subject`1<UniRx.Unit> SafeWindow::GetBigger
	Subject_1_t201353362 * ___GetBigger_7;
	// UniRx.Subject`1<UniRx.Unit> SafeWindow::Grab
	Subject_1_t201353362 * ___Grab_8;
	// GlobalStat SafeWindow::GlobalStat
	GlobalStat_t1134678199 * ___GlobalStat_9;
	// UnityEngine.UI.Text SafeWindow::SavingsText
	Text_t3286458198 * ___SavingsText_10;
	// UnityEngine.UI.Text SafeWindow::IncomeText
	Text_t3286458198 * ___IncomeText_11;
	// UnityEngine.GameObject SafeWindow::ProgressGreen
	GameObject_t4012695102 * ___ProgressGreen_12;
	// OligarchParameters SafeWindow::OligarchParameters
	OligarchParameters_t821144443 * ___OligarchParameters_13;
	// UnityEngine.UI.Image SafeWindow::SafeProgressGold
	Image_t3354615620 * ___SafeProgressGold_14;
	// GuiController SafeWindow::GuiController
	GuiController_t1495593751 * ___GuiController_15;

public:
	inline static int32_t get_offset_of_OnClose_6() { return static_cast<int32_t>(offsetof(SafeWindow_t1052493309, ___OnClose_6)); }
	inline Subject_1_t201353362 * get_OnClose_6() const { return ___OnClose_6; }
	inline Subject_1_t201353362 ** get_address_of_OnClose_6() { return &___OnClose_6; }
	inline void set_OnClose_6(Subject_1_t201353362 * value)
	{
		___OnClose_6 = value;
		Il2CppCodeGenWriteBarrier(&___OnClose_6, value);
	}

	inline static int32_t get_offset_of_GetBigger_7() { return static_cast<int32_t>(offsetof(SafeWindow_t1052493309, ___GetBigger_7)); }
	inline Subject_1_t201353362 * get_GetBigger_7() const { return ___GetBigger_7; }
	inline Subject_1_t201353362 ** get_address_of_GetBigger_7() { return &___GetBigger_7; }
	inline void set_GetBigger_7(Subject_1_t201353362 * value)
	{
		___GetBigger_7 = value;
		Il2CppCodeGenWriteBarrier(&___GetBigger_7, value);
	}

	inline static int32_t get_offset_of_Grab_8() { return static_cast<int32_t>(offsetof(SafeWindow_t1052493309, ___Grab_8)); }
	inline Subject_1_t201353362 * get_Grab_8() const { return ___Grab_8; }
	inline Subject_1_t201353362 ** get_address_of_Grab_8() { return &___Grab_8; }
	inline void set_Grab_8(Subject_1_t201353362 * value)
	{
		___Grab_8 = value;
		Il2CppCodeGenWriteBarrier(&___Grab_8, value);
	}

	inline static int32_t get_offset_of_GlobalStat_9() { return static_cast<int32_t>(offsetof(SafeWindow_t1052493309, ___GlobalStat_9)); }
	inline GlobalStat_t1134678199 * get_GlobalStat_9() const { return ___GlobalStat_9; }
	inline GlobalStat_t1134678199 ** get_address_of_GlobalStat_9() { return &___GlobalStat_9; }
	inline void set_GlobalStat_9(GlobalStat_t1134678199 * value)
	{
		___GlobalStat_9 = value;
		Il2CppCodeGenWriteBarrier(&___GlobalStat_9, value);
	}

	inline static int32_t get_offset_of_SavingsText_10() { return static_cast<int32_t>(offsetof(SafeWindow_t1052493309, ___SavingsText_10)); }
	inline Text_t3286458198 * get_SavingsText_10() const { return ___SavingsText_10; }
	inline Text_t3286458198 ** get_address_of_SavingsText_10() { return &___SavingsText_10; }
	inline void set_SavingsText_10(Text_t3286458198 * value)
	{
		___SavingsText_10 = value;
		Il2CppCodeGenWriteBarrier(&___SavingsText_10, value);
	}

	inline static int32_t get_offset_of_IncomeText_11() { return static_cast<int32_t>(offsetof(SafeWindow_t1052493309, ___IncomeText_11)); }
	inline Text_t3286458198 * get_IncomeText_11() const { return ___IncomeText_11; }
	inline Text_t3286458198 ** get_address_of_IncomeText_11() { return &___IncomeText_11; }
	inline void set_IncomeText_11(Text_t3286458198 * value)
	{
		___IncomeText_11 = value;
		Il2CppCodeGenWriteBarrier(&___IncomeText_11, value);
	}

	inline static int32_t get_offset_of_ProgressGreen_12() { return static_cast<int32_t>(offsetof(SafeWindow_t1052493309, ___ProgressGreen_12)); }
	inline GameObject_t4012695102 * get_ProgressGreen_12() const { return ___ProgressGreen_12; }
	inline GameObject_t4012695102 ** get_address_of_ProgressGreen_12() { return &___ProgressGreen_12; }
	inline void set_ProgressGreen_12(GameObject_t4012695102 * value)
	{
		___ProgressGreen_12 = value;
		Il2CppCodeGenWriteBarrier(&___ProgressGreen_12, value);
	}

	inline static int32_t get_offset_of_OligarchParameters_13() { return static_cast<int32_t>(offsetof(SafeWindow_t1052493309, ___OligarchParameters_13)); }
	inline OligarchParameters_t821144443 * get_OligarchParameters_13() const { return ___OligarchParameters_13; }
	inline OligarchParameters_t821144443 ** get_address_of_OligarchParameters_13() { return &___OligarchParameters_13; }
	inline void set_OligarchParameters_13(OligarchParameters_t821144443 * value)
	{
		___OligarchParameters_13 = value;
		Il2CppCodeGenWriteBarrier(&___OligarchParameters_13, value);
	}

	inline static int32_t get_offset_of_SafeProgressGold_14() { return static_cast<int32_t>(offsetof(SafeWindow_t1052493309, ___SafeProgressGold_14)); }
	inline Image_t3354615620 * get_SafeProgressGold_14() const { return ___SafeProgressGold_14; }
	inline Image_t3354615620 ** get_address_of_SafeProgressGold_14() { return &___SafeProgressGold_14; }
	inline void set_SafeProgressGold_14(Image_t3354615620 * value)
	{
		___SafeProgressGold_14 = value;
		Il2CppCodeGenWriteBarrier(&___SafeProgressGold_14, value);
	}

	inline static int32_t get_offset_of_GuiController_15() { return static_cast<int32_t>(offsetof(SafeWindow_t1052493309, ___GuiController_15)); }
	inline GuiController_t1495593751 * get_GuiController_15() const { return ___GuiController_15; }
	inline GuiController_t1495593751 ** get_address_of_GuiController_15() { return &___GuiController_15; }
	inline void set_GuiController_15(GuiController_t1495593751 * value)
	{
		___GuiController_15 = value;
		Il2CppCodeGenWriteBarrier(&___GuiController_15, value);
	}
};

struct SafeWindow_t1052493309_StaticFields
{
public:
	// System.Action`1<UniRx.Unit> SafeWindow::<>f__am$cacheA
	Action_1_t2706738743 * ___U3CU3Ef__amU24cacheA_16;
	// System.Func`2<GlobalStat,System.Double> SafeWindow::<>f__am$cacheB
	Func_2_t2227950619 * ___U3CU3Ef__amU24cacheB_17;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheA_16() { return static_cast<int32_t>(offsetof(SafeWindow_t1052493309_StaticFields, ___U3CU3Ef__amU24cacheA_16)); }
	inline Action_1_t2706738743 * get_U3CU3Ef__amU24cacheA_16() const { return ___U3CU3Ef__amU24cacheA_16; }
	inline Action_1_t2706738743 ** get_address_of_U3CU3Ef__amU24cacheA_16() { return &___U3CU3Ef__amU24cacheA_16; }
	inline void set_U3CU3Ef__amU24cacheA_16(Action_1_t2706738743 * value)
	{
		___U3CU3Ef__amU24cacheA_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheA_16, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_17() { return static_cast<int32_t>(offsetof(SafeWindow_t1052493309_StaticFields, ___U3CU3Ef__amU24cacheB_17)); }
	inline Func_2_t2227950619 * get_U3CU3Ef__amU24cacheB_17() const { return ___U3CU3Ef__amU24cacheB_17; }
	inline Func_2_t2227950619 ** get_address_of_U3CU3Ef__amU24cacheB_17() { return &___U3CU3Ef__amU24cacheB_17; }
	inline void set_U3CU3Ef__amU24cacheB_17(Func_2_t2227950619 * value)
	{
		___U3CU3Ef__amU24cacheB_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheB_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
