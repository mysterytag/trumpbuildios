﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// AnalyticsController
struct AnalyticsController_t2265609122;
// Zenject.InjectContext
struct InjectContext_t3456483891;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_MulticastDelegate2585444626.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`2<Zenject.InjectContext,AnalyticsController>
struct  Func_2_t4049748299  : public MulticastDelegate_t2585444626
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
