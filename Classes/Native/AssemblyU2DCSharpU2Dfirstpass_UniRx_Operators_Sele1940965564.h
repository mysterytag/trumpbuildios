﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.SelectObservable`2<System.Int32,UnityEngine.Color>
struct SelectObservable_2_t3125378242;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper3737296838.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.SelectObservable`2/Select<System.Int32,UnityEngine.Color>
struct  Select_t1940965564  : public OperatorObserverBase_2_t3737296838
{
public:
	// UniRx.Operators.SelectObservable`2<T,TR> UniRx.Operators.SelectObservable`2/Select::parent
	SelectObservable_2_t3125378242 * ___parent_2;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(Select_t1940965564, ___parent_2)); }
	inline SelectObservable_2_t3125378242 * get_parent_2() const { return ___parent_2; }
	inline SelectObservable_2_t3125378242 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(SelectObservable_2_t3125378242 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
