﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetLeaderboardResponseCallback
struct GetLeaderboardResponseCallback_t1387190317;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetLeaderboardRequest
struct GetLeaderboardRequest_t1150085688;
// PlayFab.ClientModels.GetLeaderboardResult
struct GetLeaderboardResult_t2566099316;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetLeaderbo1150085688.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetLeaderbo2566099316.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetLeaderboardResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetLeaderboardResponseCallback__ctor_m2107413148 (GetLeaderboardResponseCallback_t1387190317 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetLeaderboardResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetLeaderboardRequest,PlayFab.ClientModels.GetLeaderboardResult,PlayFab.PlayFabError,System.Object)
extern "C"  void GetLeaderboardResponseCallback_Invoke_m1395927433 (GetLeaderboardResponseCallback_t1387190317 * __this, String_t* ___urlPath0, int32_t ___callId1, GetLeaderboardRequest_t1150085688 * ___request2, GetLeaderboardResult_t2566099316 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetLeaderboardResponseCallback_t1387190317(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetLeaderboardRequest_t1150085688 * ___request2, GetLeaderboardResult_t2566099316 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetLeaderboardResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetLeaderboardRequest,PlayFab.ClientModels.GetLeaderboardResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetLeaderboardResponseCallback_BeginInvoke_m797693174 (GetLeaderboardResponseCallback_t1387190317 * __this, String_t* ___urlPath0, int32_t ___callId1, GetLeaderboardRequest_t1150085688 * ___request2, GetLeaderboardResult_t2566099316 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetLeaderboardResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetLeaderboardResponseCallback_EndInvoke_m1517925036 (GetLeaderboardResponseCallback_t1387190317 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
