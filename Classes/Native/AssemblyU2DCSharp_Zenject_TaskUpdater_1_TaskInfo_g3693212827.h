﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Zenject.ILateTickable
struct ILateTickable_t3465810843;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TaskUpdater`1/TaskInfo<Zenject.ILateTickable>
struct  TaskInfo_t3693212827  : public Il2CppObject
{
public:
	// TTask Zenject.TaskUpdater`1/TaskInfo::Task
	Il2CppObject * ___Task_0;
	// System.Int32 Zenject.TaskUpdater`1/TaskInfo::Priority
	int32_t ___Priority_1;
	// System.Boolean Zenject.TaskUpdater`1/TaskInfo::IsRemoved
	bool ___IsRemoved_2;

public:
	inline static int32_t get_offset_of_Task_0() { return static_cast<int32_t>(offsetof(TaskInfo_t3693212827, ___Task_0)); }
	inline Il2CppObject * get_Task_0() const { return ___Task_0; }
	inline Il2CppObject ** get_address_of_Task_0() { return &___Task_0; }
	inline void set_Task_0(Il2CppObject * value)
	{
		___Task_0 = value;
		Il2CppCodeGenWriteBarrier(&___Task_0, value);
	}

	inline static int32_t get_offset_of_Priority_1() { return static_cast<int32_t>(offsetof(TaskInfo_t3693212827, ___Priority_1)); }
	inline int32_t get_Priority_1() const { return ___Priority_1; }
	inline int32_t* get_address_of_Priority_1() { return &___Priority_1; }
	inline void set_Priority_1(int32_t value)
	{
		___Priority_1 = value;
	}

	inline static int32_t get_offset_of_IsRemoved_2() { return static_cast<int32_t>(offsetof(TaskInfo_t3693212827, ___IsRemoved_2)); }
	inline bool get_IsRemoved_2() const { return ___IsRemoved_2; }
	inline bool* get_address_of_IsRemoved_2() { return &___IsRemoved_2; }
	inline void set_IsRemoved_2(bool value)
	{
		___IsRemoved_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
