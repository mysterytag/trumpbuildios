﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1<System.Single>
struct Subject_1_t2896243641;
// System.Exception
struct Exception_t1967233988;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Single>
struct IObserver_1_t3170207924;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Subject`1<System.Single>::.ctor()
extern "C"  void Subject_1__ctor_m3410487393_gshared (Subject_1_t2896243641 * __this, const MethodInfo* method);
#define Subject_1__ctor_m3410487393(__this, method) ((  void (*) (Subject_1_t2896243641 *, const MethodInfo*))Subject_1__ctor_m3410487393_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<System.Single>::get_HasObservers()
extern "C"  bool Subject_1_get_HasObservers_m1412894347_gshared (Subject_1_t2896243641 * __this, const MethodInfo* method);
#define Subject_1_get_HasObservers_m1412894347(__this, method) ((  bool (*) (Subject_1_t2896243641 *, const MethodInfo*))Subject_1_get_HasObservers_m1412894347_gshared)(__this, method)
// System.Void UniRx.Subject`1<System.Single>::OnCompleted()
extern "C"  void Subject_1_OnCompleted_m467767083_gshared (Subject_1_t2896243641 * __this, const MethodInfo* method);
#define Subject_1_OnCompleted_m467767083(__this, method) ((  void (*) (Subject_1_t2896243641 *, const MethodInfo*))Subject_1_OnCompleted_m467767083_gshared)(__this, method)
// System.Void UniRx.Subject`1<System.Single>::OnError(System.Exception)
extern "C"  void Subject_1_OnError_m2947536856_gshared (Subject_1_t2896243641 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Subject_1_OnError_m2947536856(__this, ___error0, method) ((  void (*) (Subject_1_t2896243641 *, Exception_t1967233988 *, const MethodInfo*))Subject_1_OnError_m2947536856_gshared)(__this, ___error0, method)
// System.Void UniRx.Subject`1<System.Single>::OnNext(T)
extern "C"  void Subject_1_OnNext_m3249041097_gshared (Subject_1_t2896243641 * __this, float ___value0, const MethodInfo* method);
#define Subject_1_OnNext_m3249041097(__this, ___value0, method) ((  void (*) (Subject_1_t2896243641 *, float, const MethodInfo*))Subject_1_OnNext_m3249041097_gshared)(__this, ___value0, method)
// System.IDisposable UniRx.Subject`1<System.Single>::Subscribe(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject * Subject_1_Subscribe_m677515104_gshared (Subject_1_t2896243641 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define Subject_1_Subscribe_m677515104(__this, ___observer0, method) ((  Il2CppObject * (*) (Subject_1_t2896243641 *, Il2CppObject*, const MethodInfo*))Subject_1_Subscribe_m677515104_gshared)(__this, ___observer0, method)
// System.Void UniRx.Subject`1<System.Single>::Dispose()
extern "C"  void Subject_1_Dispose_m316620894_gshared (Subject_1_t2896243641 * __this, const MethodInfo* method);
#define Subject_1_Dispose_m316620894(__this, method) ((  void (*) (Subject_1_t2896243641 *, const MethodInfo*))Subject_1_Dispose_m316620894_gshared)(__this, method)
// System.Void UniRx.Subject`1<System.Single>::ThrowIfDisposed()
extern "C"  void Subject_1_ThrowIfDisposed_m1608362983_gshared (Subject_1_t2896243641 * __this, const MethodInfo* method);
#define Subject_1_ThrowIfDisposed_m1608362983(__this, method) ((  void (*) (Subject_1_t2896243641 *, const MethodInfo*))Subject_1_ThrowIfDisposed_m1608362983_gshared)(__this, method)
// System.Boolean UniRx.Subject`1<System.Single>::IsRequiredSubscribeOnCurrentThread()
extern "C"  bool Subject_1_IsRequiredSubscribeOnCurrentThread_m641666754_gshared (Subject_1_t2896243641 * __this, const MethodInfo* method);
#define Subject_1_IsRequiredSubscribeOnCurrentThread_m641666754(__this, method) ((  bool (*) (Subject_1_t2896243641 *, const MethodInfo*))Subject_1_IsRequiredSubscribeOnCurrentThread_m641666754_gshared)(__this, method)
