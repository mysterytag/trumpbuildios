﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CollectionReplaceEvent`1<System.Object>
struct CollectionReplaceEvent_1_t2497372070;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void CollectionReplaceEvent`1<System.Object>::.ctor(System.Int32,T,T)
extern "C"  void CollectionReplaceEvent_1__ctor_m1290910597_gshared (CollectionReplaceEvent_1_t2497372070 * __this, int32_t ___index0, Il2CppObject * ___oldValue1, Il2CppObject * ___newValue2, const MethodInfo* method);
#define CollectionReplaceEvent_1__ctor_m1290910597(__this, ___index0, ___oldValue1, ___newValue2, method) ((  void (*) (CollectionReplaceEvent_1_t2497372070 *, int32_t, Il2CppObject *, Il2CppObject *, const MethodInfo*))CollectionReplaceEvent_1__ctor_m1290910597_gshared)(__this, ___index0, ___oldValue1, ___newValue2, method)
// System.Int32 CollectionReplaceEvent`1<System.Object>::get_Index()
extern "C"  int32_t CollectionReplaceEvent_1_get_Index_m2241178009_gshared (CollectionReplaceEvent_1_t2497372070 * __this, const MethodInfo* method);
#define CollectionReplaceEvent_1_get_Index_m2241178009(__this, method) ((  int32_t (*) (CollectionReplaceEvent_1_t2497372070 *, const MethodInfo*))CollectionReplaceEvent_1_get_Index_m2241178009_gshared)(__this, method)
// System.Void CollectionReplaceEvent`1<System.Object>::set_Index(System.Int32)
extern "C"  void CollectionReplaceEvent_1_set_Index_m4096015976_gshared (CollectionReplaceEvent_1_t2497372070 * __this, int32_t ___value0, const MethodInfo* method);
#define CollectionReplaceEvent_1_set_Index_m4096015976(__this, ___value0, method) ((  void (*) (CollectionReplaceEvent_1_t2497372070 *, int32_t, const MethodInfo*))CollectionReplaceEvent_1_set_Index_m4096015976_gshared)(__this, ___value0, method)
// T CollectionReplaceEvent`1<System.Object>::get_OldValue()
extern "C"  Il2CppObject * CollectionReplaceEvent_1_get_OldValue_m3651064692_gshared (CollectionReplaceEvent_1_t2497372070 * __this, const MethodInfo* method);
#define CollectionReplaceEvent_1_get_OldValue_m3651064692(__this, method) ((  Il2CppObject * (*) (CollectionReplaceEvent_1_t2497372070 *, const MethodInfo*))CollectionReplaceEvent_1_get_OldValue_m3651064692_gshared)(__this, method)
// System.Void CollectionReplaceEvent`1<System.Object>::set_OldValue(T)
extern "C"  void CollectionReplaceEvent_1_set_OldValue_m1129314551_gshared (CollectionReplaceEvent_1_t2497372070 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define CollectionReplaceEvent_1_set_OldValue_m1129314551(__this, ___value0, method) ((  void (*) (CollectionReplaceEvent_1_t2497372070 *, Il2CppObject *, const MethodInfo*))CollectionReplaceEvent_1_set_OldValue_m1129314551_gshared)(__this, ___value0, method)
// T CollectionReplaceEvent`1<System.Object>::get_NewValue()
extern "C"  Il2CppObject * CollectionReplaceEvent_1_get_NewValue_m2368515547_gshared (CollectionReplaceEvent_1_t2497372070 * __this, const MethodInfo* method);
#define CollectionReplaceEvent_1_get_NewValue_m2368515547(__this, method) ((  Il2CppObject * (*) (CollectionReplaceEvent_1_t2497372070 *, const MethodInfo*))CollectionReplaceEvent_1_get_NewValue_m2368515547_gshared)(__this, method)
// System.Void CollectionReplaceEvent`1<System.Object>::set_NewValue(T)
extern "C"  void CollectionReplaceEvent_1_set_NewValue_m24996720_gshared (CollectionReplaceEvent_1_t2497372070 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define CollectionReplaceEvent_1_set_NewValue_m24996720(__this, ___value0, method) ((  void (*) (CollectionReplaceEvent_1_t2497372070 *, Il2CppObject *, const MethodInfo*))CollectionReplaceEvent_1_set_NewValue_m24996720_gshared)(__this, ___value0, method)
// System.String CollectionReplaceEvent`1<System.Object>::ToString()
extern "C"  String_t* CollectionReplaceEvent_1_ToString_m2524726313_gshared (CollectionReplaceEvent_1_t2497372070 * __this, const MethodInfo* method);
#define CollectionReplaceEvent_1_ToString_m2524726313(__this, method) ((  String_t* (*) (CollectionReplaceEvent_1_t2497372070 *, const MethodInfo*))CollectionReplaceEvent_1_ToString_m2524726313_gshared)(__this, method)
