﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.GetPlayerStatisticsRequest
struct GetPlayerStatisticsRequest_t3788214021;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.GetPlayerStatisticsRequest::.ctor()
extern "C"  void GetPlayerStatisticsRequest__ctor_m81652312 (GetPlayerStatisticsRequest_t3788214021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.GetPlayerStatisticsRequest::get_StatisticNames()
extern "C"  List_1_t1765447871 * GetPlayerStatisticsRequest_get_StatisticNames_m3522747644 (GetPlayerStatisticsRequest_t3788214021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.GetPlayerStatisticsRequest::set_StatisticNames(System.Collections.Generic.List`1<System.String>)
extern "C"  void GetPlayerStatisticsRequest_set_StatisticNames_m1979562741 (GetPlayerStatisticsRequest_t3788214021 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
