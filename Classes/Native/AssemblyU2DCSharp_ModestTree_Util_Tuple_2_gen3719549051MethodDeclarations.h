﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ModestTree_Util_Tuple_2_gen299352770MethodDeclarations.h"

// System.Void ModestTree.Util.Tuple`2<System.Type,System.Int32>::.ctor()
#define Tuple_2__ctor_m2391842737(__this, method) ((  void (*) (Tuple_2_t3719549051 *, const MethodInfo*))Tuple_2__ctor_m3946485580_gshared)(__this, method)
// System.Void ModestTree.Util.Tuple`2<System.Type,System.Int32>::.ctor(T1,T2)
#define Tuple_2__ctor_m3626806068(__this, ___first0, ___second1, method) ((  void (*) (Tuple_2_t3719549051 *, Type_t *, int32_t, const MethodInfo*))Tuple_2__ctor_m2191801465_gshared)(__this, ___first0, ___second1, method)
// System.Boolean ModestTree.Util.Tuple`2<System.Type,System.Int32>::Equals(System.Object)
#define Tuple_2_Equals_m1147562646(__this, ___obj0, method) ((  bool (*) (Tuple_2_t3719549051 *, Il2CppObject *, const MethodInfo*))Tuple_2_Equals_m897686001_gshared)(__this, ___obj0, method)
// System.Boolean ModestTree.Util.Tuple`2<System.Type,System.Int32>::Equals(ModestTree.Util.Tuple`2<T1,T2>)
#define Tuple_2_Equals_m1526048907(__this, ___that0, method) ((  bool (*) (Tuple_2_t3719549051 *, Tuple_2_t3719549051 *, const MethodInfo*))Tuple_2_Equals_m194624144_gshared)(__this, ___that0, method)
// System.Int32 ModestTree.Util.Tuple`2<System.Type,System.Int32>::GetHashCode()
#define Tuple_2_GetHashCode_m2712399278(__this, method) ((  int32_t (*) (Tuple_2_t3719549051 *, const MethodInfo*))Tuple_2_GetHashCode_m1510095241_gshared)(__this, method)
