﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4191540488MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
#define Transform_1__ctor_m676107769(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t333269717 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m2052388693_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m2070298911(__this, ___key0, ___value1, method) ((  DictionaryEntry_t130027246  (*) (Transform_1_t333269717 *, BindingId_t2965794261 *, List_1_t2424453360 *, const MethodInfo*))Transform_1_Invoke_m757436355_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m3181920330(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t333269717 *, BindingId_t2965794261 *, List_1_t2424453360 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m397518190_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.ProviderBase>,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m2634335755(__this, ___result0, method) ((  DictionaryEntry_t130027246  (*) (Transform_1_t333269717 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m3155601639_gshared)(__this, ___result0, method)
