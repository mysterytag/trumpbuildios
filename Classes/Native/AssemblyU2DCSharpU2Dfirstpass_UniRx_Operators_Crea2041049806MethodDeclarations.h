﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.CreateObservable`1<System.Object>
struct CreateObservable_1_t2041049806;
// System.Func`2<UniRx.IObserver`1<System.Object>,System.IDisposable>
struct Func_2_t2619763055;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.CreateObservable`1<System.Object>::.ctor(System.Func`2<UniRx.IObserver`1<T>,System.IDisposable>)
extern "C"  void CreateObservable_1__ctor_m3114897029_gshared (CreateObservable_1_t2041049806 * __this, Func_2_t2619763055 * ___subscribe0, const MethodInfo* method);
#define CreateObservable_1__ctor_m3114897029(__this, ___subscribe0, method) ((  void (*) (CreateObservable_1_t2041049806 *, Func_2_t2619763055 *, const MethodInfo*))CreateObservable_1__ctor_m3114897029_gshared)(__this, ___subscribe0, method)
// System.Void UniRx.Operators.CreateObservable`1<System.Object>::.ctor(System.Func`2<UniRx.IObserver`1<T>,System.IDisposable>,System.Boolean)
extern "C"  void CreateObservable_1__ctor_m3293661784_gshared (CreateObservable_1_t2041049806 * __this, Func_2_t2619763055 * ___subscribe0, bool ___isRequiredSubscribeOnCurrentThread1, const MethodInfo* method);
#define CreateObservable_1__ctor_m3293661784(__this, ___subscribe0, ___isRequiredSubscribeOnCurrentThread1, method) ((  void (*) (CreateObservable_1_t2041049806 *, Func_2_t2619763055 *, bool, const MethodInfo*))CreateObservable_1__ctor_m3293661784_gshared)(__this, ___subscribe0, ___isRequiredSubscribeOnCurrentThread1, method)
// System.IDisposable UniRx.Operators.CreateObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * CreateObservable_1_SubscribeCore_m2669489670_gshared (CreateObservable_1_t2041049806 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define CreateObservable_1_SubscribeCore_m2669489670(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (CreateObservable_1_t2041049806 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))CreateObservable_1_SubscribeCore_m2669489670_gshared)(__this, ___observer0, ___cancel1, method)
