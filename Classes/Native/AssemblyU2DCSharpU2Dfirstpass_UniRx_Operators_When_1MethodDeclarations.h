﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.WhenAllObservable`1/WhenAll_/WhenAllCollectionObserver<UniRx.Tuple`2<System.Object,System.Object>>
struct WhenAllCollectionObserver_t3269372324;
// UniRx.Operators.WhenAllObservable`1/WhenAll_<UniRx.Tuple`2<System.Object,System.Object>>
struct WhenAll__t2383258904;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Tuple_2_gen369261819.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.WhenAllObservable`1/WhenAll_/WhenAllCollectionObserver<UniRx.Tuple`2<System.Object,System.Object>>::.ctor(UniRx.Operators.WhenAllObservable`1/WhenAll_<T>,System.Int32)
extern "C"  void WhenAllCollectionObserver__ctor_m251965934_gshared (WhenAllCollectionObserver_t3269372324 * __this, WhenAll__t2383258904 * ___parent0, int32_t ___index1, const MethodInfo* method);
#define WhenAllCollectionObserver__ctor_m251965934(__this, ___parent0, ___index1, method) ((  void (*) (WhenAllCollectionObserver_t3269372324 *, WhenAll__t2383258904 *, int32_t, const MethodInfo*))WhenAllCollectionObserver__ctor_m251965934_gshared)(__this, ___parent0, ___index1, method)
// System.Void UniRx.Operators.WhenAllObservable`1/WhenAll_/WhenAllCollectionObserver<UniRx.Tuple`2<System.Object,System.Object>>::OnNext(T)
extern "C"  void WhenAllCollectionObserver_OnNext_m3597895694_gshared (WhenAllCollectionObserver_t3269372324 * __this, Tuple_2_t369261819  ___value0, const MethodInfo* method);
#define WhenAllCollectionObserver_OnNext_m3597895694(__this, ___value0, method) ((  void (*) (WhenAllCollectionObserver_t3269372324 *, Tuple_2_t369261819 , const MethodInfo*))WhenAllCollectionObserver_OnNext_m3597895694_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.WhenAllObservable`1/WhenAll_/WhenAllCollectionObserver<UniRx.Tuple`2<System.Object,System.Object>>::OnError(System.Exception)
extern "C"  void WhenAllCollectionObserver_OnError_m586270493_gshared (WhenAllCollectionObserver_t3269372324 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define WhenAllCollectionObserver_OnError_m586270493(__this, ___error0, method) ((  void (*) (WhenAllCollectionObserver_t3269372324 *, Exception_t1967233988 *, const MethodInfo*))WhenAllCollectionObserver_OnError_m586270493_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.WhenAllObservable`1/WhenAll_/WhenAllCollectionObserver<UniRx.Tuple`2<System.Object,System.Object>>::OnCompleted()
extern "C"  void WhenAllCollectionObserver_OnCompleted_m927235568_gshared (WhenAllCollectionObserver_t3269372324 * __this, const MethodInfo* method);
#define WhenAllCollectionObserver_OnCompleted_m927235568(__this, method) ((  void (*) (WhenAllCollectionObserver_t3269372324 *, const MethodInfo*))WhenAllCollectionObserver_OnCompleted_m927235568_gshared)(__this, method)
