﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.DoOnSubscribeObservable`1<System.Object>
struct DoOnSubscribeObservable_1_t1431598738;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Action
struct Action_t437523947;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"

// System.Void UniRx.Operators.DoOnSubscribeObservable`1<System.Object>::.ctor(UniRx.IObservable`1<T>,System.Action)
extern "C"  void DoOnSubscribeObservable_1__ctor_m1204279814_gshared (DoOnSubscribeObservable_1_t1431598738 * __this, Il2CppObject* ___source0, Action_t437523947 * ___onSubscribe1, const MethodInfo* method);
#define DoOnSubscribeObservable_1__ctor_m1204279814(__this, ___source0, ___onSubscribe1, method) ((  void (*) (DoOnSubscribeObservable_1_t1431598738 *, Il2CppObject*, Action_t437523947 *, const MethodInfo*))DoOnSubscribeObservable_1__ctor_m1204279814_gshared)(__this, ___source0, ___onSubscribe1, method)
// System.IDisposable UniRx.Operators.DoOnSubscribeObservable`1<System.Object>::SubscribeCore(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  Il2CppObject * DoOnSubscribeObservable_1_SubscribeCore_m3040731758_gshared (DoOnSubscribeObservable_1_t1431598738 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define DoOnSubscribeObservable_1_SubscribeCore_m3040731758(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (DoOnSubscribeObservable_1_t1431598738 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))DoOnSubscribeObservable_1_SubscribeCore_m3040731758_gshared)(__this, ___observer0, ___cancel1, method)
