﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type
struct Type_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InstantiateUtil/<PopValueWithType>c__AnonStorey180
struct  U3CPopValueWithTypeU3Ec__AnonStorey180_t578279556  : public Il2CppObject
{
public:
	// System.Type Zenject.InstantiateUtil/<PopValueWithType>c__AnonStorey180::injectedFieldType
	Type_t * ___injectedFieldType_0;

public:
	inline static int32_t get_offset_of_injectedFieldType_0() { return static_cast<int32_t>(offsetof(U3CPopValueWithTypeU3Ec__AnonStorey180_t578279556, ___injectedFieldType_0)); }
	inline Type_t * get_injectedFieldType_0() const { return ___injectedFieldType_0; }
	inline Type_t ** get_address_of_injectedFieldType_0() { return &___injectedFieldType_0; }
	inline void set_injectedFieldType_0(Type_t * value)
	{
		___injectedFieldType_0 = value;
		Il2CppCodeGenWriteBarrier(&___injectedFieldType_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
