﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.IDisposable
struct IDisposable_t1628921374;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CellUtils.SingleAssignmentDisposable
struct  SingleAssignmentDisposable_t1832432170  : public Il2CppObject
{
public:
	// System.IDisposable CellUtils.SingleAssignmentDisposable::current
	Il2CppObject * ___current_0;

public:
	inline static int32_t get_offset_of_current_0() { return static_cast<int32_t>(offsetof(SingleAssignmentDisposable_t1832432170, ___current_0)); }
	inline Il2CppObject * get_current_0() const { return ___current_0; }
	inline Il2CppObject ** get_address_of_current_0() { return &___current_0; }
	inline void set_current_0(Il2CppObject * value)
	{
		___current_0 = value;
		Il2CppCodeGenWriteBarrier(&___current_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
