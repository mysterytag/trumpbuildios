﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Examples.Sample10_MainThreadDispatcher
struct Sample10_MainThreadDispatcher_t1609111451;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"

// System.Void UniRx.Examples.Sample10_MainThreadDispatcher::.ctor()
extern "C"  void Sample10_MainThreadDispatcher__ctor_m699156572 (Sample10_MainThreadDispatcher_t1609111451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample10_MainThreadDispatcher::Run()
extern "C"  void Sample10_MainThreadDispatcher_Run_m3238968197 (Sample10_MainThreadDispatcher_t1609111451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UniRx.Examples.Sample10_MainThreadDispatcher::TestAsync()
extern "C"  Il2CppObject * Sample10_MainThreadDispatcher_TestAsync_m497817684 (Sample10_MainThreadDispatcher_t1609111451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample10_MainThreadDispatcher::<Run>m__30(System.Object)
extern "C"  void Sample10_MainThreadDispatcher_U3CRunU3Em__30_m1490611473 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample10_MainThreadDispatcher::<Run>m__31(System.Int64)
extern "C"  void Sample10_MainThreadDispatcher_U3CRunU3Em__31_m1672289270 (Il2CppObject * __this /* static, unused */, int64_t ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UniRx.Unit UniRx.Examples.Sample10_MainThreadDispatcher::<Run>m__32()
extern "C"  Unit_t2558286038  Sample10_MainThreadDispatcher_U3CRunU3Em__32_m426223150 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample10_MainThreadDispatcher::<Run>m__33(UniRx.Unit)
extern "C"  void Sample10_MainThreadDispatcher_U3CRunU3Em__33_m2418357898 (Il2CppObject * __this /* static, unused */, Unit_t2558286038  ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
