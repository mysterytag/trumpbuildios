﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// PlayFab.ClientModels.LoginWithCustomIDRequest
struct LoginWithCustomIDRequest_t3362330884;
// PlayFab.ClientModels.LoginResult
struct LoginResult_t2117559510;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginWithCu3362330884.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LoginResult2117559510.h"

// System.Void PlayFab.Internal.EventTest/EventStaticListener::.cctor()
extern "C"  void EventStaticListener__cctor_m2955654293 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.Internal.EventTest/EventStaticListener::Register()
extern "C"  void EventStaticListener_Register_m932852879 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.Internal.EventTest/EventStaticListener::Unregister()
extern "C"  void EventStaticListener_Unregister_m784259368 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.Internal.EventTest/EventStaticListener::OnRequest_StaticGl(System.String,System.Int32,System.Object,System.Object)
extern "C"  void EventStaticListener_OnRequest_StaticGl_m4076778079 (Il2CppObject * __this /* static, unused */, String_t* ___url0, int32_t ___callId1, Il2CppObject * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.Internal.EventTest/EventStaticListener::OnResponse_StaticGl(System.String,System.Int32,System.Object,System.Object,PlayFab.PlayFabError,System.Object)
extern "C"  void EventStaticListener_OnResponse_StaticGl_m4237512491 (Il2CppObject * __this /* static, unused */, String_t* ___url0, int32_t ___callId1, Il2CppObject * ___request2, Il2CppObject * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.Internal.EventTest/EventStaticListener::OnRequest_StaticLogin(System.String,System.Int32,PlayFab.ClientModels.LoginWithCustomIDRequest,System.Object)
extern "C"  void EventStaticListener_OnRequest_StaticLogin_m3961566783 (Il2CppObject * __this /* static, unused */, String_t* ___url0, int32_t ___callId1, LoginWithCustomIDRequest_t3362330884 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.Internal.EventTest/EventStaticListener::OnResponse_StaticLogin(System.String,System.Int32,PlayFab.ClientModels.LoginWithCustomIDRequest,PlayFab.ClientModels.LoginResult,PlayFab.PlayFabError,System.Object)
extern "C"  void EventStaticListener_OnResponse_StaticLogin_m68863197 (Il2CppObject * __this /* static, unused */, String_t* ___url0, int32_t ___callId1, LoginWithCustomIDRequest_t3362330884 * ___request2, LoginResult_t2117559510 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
