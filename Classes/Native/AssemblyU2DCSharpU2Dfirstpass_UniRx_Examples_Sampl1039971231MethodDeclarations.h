﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Examples.Sample04_ConvertFromUnityCallback/LogHelper/<LogCallbackAsObservable>c__AnonStorey2F
struct U3CLogCallbackAsObservableU3Ec__AnonStorey2F_t1039971231;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "UnityEngine_UnityEngine_LogType3529269451.h"

// System.Void UniRx.Examples.Sample04_ConvertFromUnityCallback/LogHelper/<LogCallbackAsObservable>c__AnonStorey2F::.ctor()
extern "C"  void U3CLogCallbackAsObservableU3Ec__AnonStorey2F__ctor_m549354189 (U3CLogCallbackAsObservableU3Ec__AnonStorey2F_t1039971231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Examples.Sample04_ConvertFromUnityCallback/LogHelper/<LogCallbackAsObservable>c__AnonStorey2F::<>m__1A(System.String,System.String,UnityEngine.LogType)
extern "C"  void U3CLogCallbackAsObservableU3Ec__AnonStorey2F_U3CU3Em__1A_m1551801725 (U3CLogCallbackAsObservableU3Ec__AnonStorey2F_t1039971231 * __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
