﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ModestTree.Util.Func`9<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct Func_9_t38423544;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void ModestTree.Util.Func`9<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_9__ctor_m555821126_gshared (Func_9_t38423544 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Func_9__ctor_m555821126(__this, ___object0, ___method1, method) ((  void (*) (Func_9_t38423544 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_9__ctor_m555821126_gshared)(__this, ___object0, ___method1, method)
// TResult ModestTree.Util.Func`9<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::Invoke(T1,T2,T3,T4,T5,T6,T7,T8)
extern "C"  Il2CppObject * Func_9_Invoke_m451447108_gshared (Func_9_t38423544 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, Il2CppObject * ___arg65, Il2CppObject * ___arg76, Il2CppObject * ___arg87, const MethodInfo* method);
#define Func_9_Invoke_m451447108(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76, ___arg87, method) ((  Il2CppObject * (*) (Func_9_t38423544 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Func_9_Invoke_m451447108_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76, ___arg87, method)
// System.IAsyncResult ModestTree.Util.Func`9<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,T3,T4,T5,T6,T7,T8,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_9_BeginInvoke_m4241629679_gshared (Func_9_t38423544 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, Il2CppObject * ___arg54, Il2CppObject * ___arg65, Il2CppObject * ___arg76, Il2CppObject * ___arg87, AsyncCallback_t1363551830 * ___callback8, Il2CppObject * ___object9, const MethodInfo* method);
#define Func_9_BeginInvoke_m4241629679(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76, ___arg87, ___callback8, ___object9, method) ((  Il2CppObject * (*) (Func_9_t38423544 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_9_BeginInvoke_m4241629679_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___arg54, ___arg65, ___arg76, ___arg87, ___callback8, ___object9, method)
// TResult ModestTree.Util.Func`9<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Func_9_EndInvoke_m3503956212_gshared (Func_9_t38423544 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Func_9_EndInvoke_m3503956212(__this, ___result0, method) ((  Il2CppObject * (*) (Func_9_t38423544 *, Il2CppObject *, const MethodInfo*))Func_9_EndInvoke_m3503956212_gshared)(__this, ___result0, method)
