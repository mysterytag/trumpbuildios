﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<LoginWithKongregate>c__AnonStorey66
struct U3CLoginWithKongregateU3Ec__AnonStorey66_t3884151735;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<LoginWithKongregate>c__AnonStorey66::.ctor()
extern "C"  void U3CLoginWithKongregateU3Ec__AnonStorey66__ctor_m902267260 (U3CLoginWithKongregateU3Ec__AnonStorey66_t3884151735 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<LoginWithKongregate>c__AnonStorey66::<>m__53(PlayFab.CallRequestContainer)
extern "C"  void U3CLoginWithKongregateU3Ec__AnonStorey66_U3CU3Em__53_m4274580856 (U3CLoginWithKongregateU3Ec__AnonStorey66_t3884151735 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
