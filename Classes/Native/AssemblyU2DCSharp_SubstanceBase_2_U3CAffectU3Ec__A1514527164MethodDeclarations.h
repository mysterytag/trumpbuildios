﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SubstanceBase`2/<Affect>c__AnonStorey144<System.Object,System.Object>
struct U3CAffectU3Ec__AnonStorey144_t1514527164;

#include "codegen/il2cpp-codegen.h"

// System.Void SubstanceBase`2/<Affect>c__AnonStorey144<System.Object,System.Object>::.ctor()
extern "C"  void U3CAffectU3Ec__AnonStorey144__ctor_m3486202036_gshared (U3CAffectU3Ec__AnonStorey144_t1514527164 * __this, const MethodInfo* method);
#define U3CAffectU3Ec__AnonStorey144__ctor_m3486202036(__this, method) ((  void (*) (U3CAffectU3Ec__AnonStorey144_t1514527164 *, const MethodInfo*))U3CAffectU3Ec__AnonStorey144__ctor_m3486202036_gshared)(__this, method)
// System.Void SubstanceBase`2/<Affect>c__AnonStorey144<System.Object,System.Object>::<>m__1DA()
extern "C"  void U3CAffectU3Ec__AnonStorey144_U3CU3Em__1DA_m3671052275_gshared (U3CAffectU3Ec__AnonStorey144_t1514527164 * __this, const MethodInfo* method);
#define U3CAffectU3Ec__AnonStorey144_U3CU3Em__1DA_m3671052275(__this, method) ((  void (*) (U3CAffectU3Ec__AnonStorey144_t1514527164 *, const MethodInfo*))U3CAffectU3Ec__AnonStorey144_U3CU3Em__1DA_m3671052275_gshared)(__this, method)
