﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey6D
struct U3CSubscribeCoreU3Ec__AnonStorey6D_t1995955652;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey6D::.ctor()
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey6D__ctor_m1186693325 (U3CSubscribeCoreU3Ec__AnonStorey6D_t1995955652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey6D::<>m__8B()
extern "C"  void U3CSubscribeCoreU3Ec__AnonStorey6D_U3CU3Em__8B_m259250400 (U3CSubscribeCoreU3Ec__AnonStorey6D_t1995955652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
