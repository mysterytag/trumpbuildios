﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ObserveOnObservable`1/ObserveOn/<OnCompleted>c__AnonStorey63<UniRx.Unit>
struct U3COnCompletedU3Ec__AnonStorey63_t1541436609;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn/<OnCompleted>c__AnonStorey63<UniRx.Unit>::.ctor()
extern "C"  void U3COnCompletedU3Ec__AnonStorey63__ctor_m3742015336_gshared (U3COnCompletedU3Ec__AnonStorey63_t1541436609 * __this, const MethodInfo* method);
#define U3COnCompletedU3Ec__AnonStorey63__ctor_m3742015336(__this, method) ((  void (*) (U3COnCompletedU3Ec__AnonStorey63_t1541436609 *, const MethodInfo*))U3COnCompletedU3Ec__AnonStorey63__ctor_m3742015336_gshared)(__this, method)
// System.Void UniRx.Operators.ObserveOnObservable`1/ObserveOn/<OnCompleted>c__AnonStorey63<UniRx.Unit>::<>m__80()
extern "C"  void U3COnCompletedU3Ec__AnonStorey63_U3CU3Em__80_m3497359657_gshared (U3COnCompletedU3Ec__AnonStorey63_t1541436609 * __this, const MethodInfo* method);
#define U3COnCompletedU3Ec__AnonStorey63_U3CU3Em__80_m3497359657(__this, method) ((  void (*) (U3COnCompletedU3Ec__AnonStorey63_t1541436609 *, const MethodInfo*))U3COnCompletedU3Ec__AnonStorey63_U3CU3Em__80_m3497359657_gshared)(__this, method)
