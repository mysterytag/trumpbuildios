﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.CombineLatestObserver`1<System.Boolean>
struct CombineLatestObserver_1_t3792433964;
// System.Object
struct Il2CppObject;
// UniRx.Operators.ICombineLatestObservable
struct ICombineLatestObservable_t195534285;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.CombineLatestObserver`1<System.Boolean>::.ctor(System.Object,UniRx.Operators.ICombineLatestObservable,System.Int32)
extern "C"  void CombineLatestObserver_1__ctor_m4220137716_gshared (CombineLatestObserver_1_t3792433964 * __this, Il2CppObject * ___gate0, Il2CppObject * ___parent1, int32_t ___index2, const MethodInfo* method);
#define CombineLatestObserver_1__ctor_m4220137716(__this, ___gate0, ___parent1, ___index2, method) ((  void (*) (CombineLatestObserver_1_t3792433964 *, Il2CppObject *, Il2CppObject *, int32_t, const MethodInfo*))CombineLatestObserver_1__ctor_m4220137716_gshared)(__this, ___gate0, ___parent1, ___index2, method)
// T UniRx.Operators.CombineLatestObserver`1<System.Boolean>::get_Value()
extern "C"  bool CombineLatestObserver_1_get_Value_m2167492077_gshared (CombineLatestObserver_1_t3792433964 * __this, const MethodInfo* method);
#define CombineLatestObserver_1_get_Value_m2167492077(__this, method) ((  bool (*) (CombineLatestObserver_1_t3792433964 *, const MethodInfo*))CombineLatestObserver_1_get_Value_m2167492077_gshared)(__this, method)
// System.Void UniRx.Operators.CombineLatestObserver`1<System.Boolean>::OnNext(T)
extern "C"  void CombineLatestObserver_1_OnNext_m3731548080_gshared (CombineLatestObserver_1_t3792433964 * __this, bool ___value0, const MethodInfo* method);
#define CombineLatestObserver_1_OnNext_m3731548080(__this, ___value0, method) ((  void (*) (CombineLatestObserver_1_t3792433964 *, bool, const MethodInfo*))CombineLatestObserver_1_OnNext_m3731548080_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.CombineLatestObserver`1<System.Boolean>::OnError(System.Exception)
extern "C"  void CombineLatestObserver_1_OnError_m1909039807_gshared (CombineLatestObserver_1_t3792433964 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define CombineLatestObserver_1_OnError_m1909039807(__this, ___error0, method) ((  void (*) (CombineLatestObserver_1_t3792433964 *, Exception_t1967233988 *, const MethodInfo*))CombineLatestObserver_1_OnError_m1909039807_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.CombineLatestObserver`1<System.Boolean>::OnCompleted()
extern "C"  void CombineLatestObserver_1_OnCompleted_m2942254226_gshared (CombineLatestObserver_1_t3792433964 * __this, const MethodInfo* method);
#define CombineLatestObserver_1_OnCompleted_m2942254226(__this, method) ((  void (*) (CombineLatestObserver_1_t3792433964 *, const MethodInfo*))CombineLatestObserver_1_OnCompleted_m2942254226_gshared)(__this, method)
