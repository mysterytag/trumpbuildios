﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XPath.XPathFunctionSubstringBefore
struct XPathFunctionSubstringBefore_t2778086872;
// System.Xml.XPath.FunctionArguments
struct FunctionArguments_t3402201403;
// System.Object
struct Il2CppObject;
// System.Xml.XPath.BaseIterator
struct BaseIterator_t3696600956;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XPath_FunctionArguments3402201403.h"
#include "System_Xml_System_Xml_XPath_XPathResultType370286193.h"
#include "System_Xml_System_Xml_XPath_BaseIterator3696600956.h"

// System.Void System.Xml.XPath.XPathFunctionSubstringBefore::.ctor(System.Xml.XPath.FunctionArguments)
extern "C"  void XPathFunctionSubstringBefore__ctor_m498616712 (XPathFunctionSubstringBefore_t2778086872 * __this, FunctionArguments_t3402201403 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XPath.XPathResultType System.Xml.XPath.XPathFunctionSubstringBefore::get_ReturnType()
extern "C"  int32_t XPathFunctionSubstringBefore_get_ReturnType_m529496916 (XPathFunctionSubstringBefore_t2778086872 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XPath.XPathFunctionSubstringBefore::get_Peer()
extern "C"  bool XPathFunctionSubstringBefore_get_Peer_m3859992328 (XPathFunctionSubstringBefore_t2778086872 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Xml.XPath.XPathFunctionSubstringBefore::Evaluate(System.Xml.XPath.BaseIterator)
extern "C"  Il2CppObject * XPathFunctionSubstringBefore_Evaluate_m2540736547 (XPathFunctionSubstringBefore_t2778086872 * __this, BaseIterator_t3696600956 * ___iter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XPath.XPathFunctionSubstringBefore::ToString()
extern "C"  String_t* XPathFunctionSubstringBefore_ToString_m1453021774 (XPathFunctionSubstringBefore_t2778086872 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
