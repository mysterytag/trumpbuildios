﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_Facebook_Unity_FacebookBase2319813814.h"
#include "AssemblyU2DCSharp_Facebook_Unity_ShareDialogMode698979849.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Mobile.MobileFacebook
struct  MobileFacebook_t974549177  : public FacebookBase_t2319813814
{
public:
	// Facebook.Unity.ShareDialogMode Facebook.Unity.Mobile.MobileFacebook::shareDialogMode
	int32_t ___shareDialogMode_5;

public:
	inline static int32_t get_offset_of_shareDialogMode_5() { return static_cast<int32_t>(offsetof(MobileFacebook_t974549177, ___shareDialogMode_5)); }
	inline int32_t get_shareDialogMode_5() const { return ___shareDialogMode_5; }
	inline int32_t* get_address_of_shareDialogMode_5() { return &___shareDialogMode_5; }
	inline void set_shareDialogMode_5(int32_t value)
	{
		___shareDialogMode_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
