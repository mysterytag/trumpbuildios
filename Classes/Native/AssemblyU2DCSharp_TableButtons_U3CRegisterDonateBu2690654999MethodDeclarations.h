﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TableButtons/<RegisterDonateButtons>c__AnonStorey168
struct U3CRegisterDonateButtonsU3Ec__AnonStorey168_t2690654999;
// DonateCell
struct DonateCell_t2798474385;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_DonateCell2798474385.h"

// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey168::.ctor()
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey168__ctor_m4196240384 (U3CRegisterDonateButtonsU3Ec__AnonStorey168_t2690654999 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey168::<>m__24D(DonateCell)
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey168_U3CU3Em__24D_m681026826 (U3CRegisterDonateButtonsU3Ec__AnonStorey168_t2690654999 * __this, DonateCell_t2798474385 * ___cell0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
