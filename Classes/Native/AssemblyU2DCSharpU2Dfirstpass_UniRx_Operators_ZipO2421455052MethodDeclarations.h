﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ZipObservable`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct ZipObservable_6_t2421455052;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// UniRx.Operators.ZipFunc`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>
struct ZipFunc_6_t3119944383;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.ZipObservable`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(UniRx.IObservable`1<T1>,UniRx.IObservable`1<T2>,UniRx.IObservable`1<T3>,UniRx.IObservable`1<T4>,UniRx.IObservable`1<T5>,UniRx.Operators.ZipFunc`6<T1,T2,T3,T4,T5,TR>)
extern "C"  void ZipObservable_6__ctor_m3671761457_gshared (ZipObservable_6_t2421455052 * __this, Il2CppObject* ___source10, Il2CppObject* ___source21, Il2CppObject* ___source32, Il2CppObject* ___source43, Il2CppObject* ___source54, ZipFunc_6_t3119944383 * ___resultSelector5, const MethodInfo* method);
#define ZipObservable_6__ctor_m3671761457(__this, ___source10, ___source21, ___source32, ___source43, ___source54, ___resultSelector5, method) ((  void (*) (ZipObservable_6_t2421455052 *, Il2CppObject*, Il2CppObject*, Il2CppObject*, Il2CppObject*, Il2CppObject*, ZipFunc_6_t3119944383 *, const MethodInfo*))ZipObservable_6__ctor_m3671761457_gshared)(__this, ___source10, ___source21, ___source32, ___source43, ___source54, ___resultSelector5, method)
// System.IDisposable UniRx.Operators.ZipObservable`6<System.Object,System.Object,System.Object,System.Object,System.Object,System.Object>::SubscribeCore(UniRx.IObserver`1<TR>,System.IDisposable)
extern "C"  Il2CppObject * ZipObservable_6_SubscribeCore_m3988752066_gshared (ZipObservable_6_t2421455052 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define ZipObservable_6_SubscribeCore_m3988752066(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (ZipObservable_6_t2421455052 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))ZipObservable_6_SubscribeCore_m3988752066_gshared)(__this, ___observer0, ___cancel1, method)
