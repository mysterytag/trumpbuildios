﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Action`2<System.Object,System.Object>
struct Action_2_t4105459918;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InjectableInfo
struct  InjectableInfo_t1147709774  : public Il2CppObject
{
public:
	// System.Boolean Zenject.InjectableInfo::Optional
	bool ___Optional_0;
	// System.String Zenject.InjectableInfo::Identifier
	String_t* ___Identifier_1;
	// System.String Zenject.InjectableInfo::MemberName
	String_t* ___MemberName_2;
	// System.Type Zenject.InjectableInfo::MemberType
	Type_t * ___MemberType_3;
	// System.Type Zenject.InjectableInfo::ObjectType
	Type_t * ___ObjectType_4;
	// System.Action`2<System.Object,System.Object> Zenject.InjectableInfo::Setter
	Action_2_t4105459918 * ___Setter_5;
	// System.Object Zenject.InjectableInfo::DefaultValue
	Il2CppObject * ___DefaultValue_6;

public:
	inline static int32_t get_offset_of_Optional_0() { return static_cast<int32_t>(offsetof(InjectableInfo_t1147709774, ___Optional_0)); }
	inline bool get_Optional_0() const { return ___Optional_0; }
	inline bool* get_address_of_Optional_0() { return &___Optional_0; }
	inline void set_Optional_0(bool value)
	{
		___Optional_0 = value;
	}

	inline static int32_t get_offset_of_Identifier_1() { return static_cast<int32_t>(offsetof(InjectableInfo_t1147709774, ___Identifier_1)); }
	inline String_t* get_Identifier_1() const { return ___Identifier_1; }
	inline String_t** get_address_of_Identifier_1() { return &___Identifier_1; }
	inline void set_Identifier_1(String_t* value)
	{
		___Identifier_1 = value;
		Il2CppCodeGenWriteBarrier(&___Identifier_1, value);
	}

	inline static int32_t get_offset_of_MemberName_2() { return static_cast<int32_t>(offsetof(InjectableInfo_t1147709774, ___MemberName_2)); }
	inline String_t* get_MemberName_2() const { return ___MemberName_2; }
	inline String_t** get_address_of_MemberName_2() { return &___MemberName_2; }
	inline void set_MemberName_2(String_t* value)
	{
		___MemberName_2 = value;
		Il2CppCodeGenWriteBarrier(&___MemberName_2, value);
	}

	inline static int32_t get_offset_of_MemberType_3() { return static_cast<int32_t>(offsetof(InjectableInfo_t1147709774, ___MemberType_3)); }
	inline Type_t * get_MemberType_3() const { return ___MemberType_3; }
	inline Type_t ** get_address_of_MemberType_3() { return &___MemberType_3; }
	inline void set_MemberType_3(Type_t * value)
	{
		___MemberType_3 = value;
		Il2CppCodeGenWriteBarrier(&___MemberType_3, value);
	}

	inline static int32_t get_offset_of_ObjectType_4() { return static_cast<int32_t>(offsetof(InjectableInfo_t1147709774, ___ObjectType_4)); }
	inline Type_t * get_ObjectType_4() const { return ___ObjectType_4; }
	inline Type_t ** get_address_of_ObjectType_4() { return &___ObjectType_4; }
	inline void set_ObjectType_4(Type_t * value)
	{
		___ObjectType_4 = value;
		Il2CppCodeGenWriteBarrier(&___ObjectType_4, value);
	}

	inline static int32_t get_offset_of_Setter_5() { return static_cast<int32_t>(offsetof(InjectableInfo_t1147709774, ___Setter_5)); }
	inline Action_2_t4105459918 * get_Setter_5() const { return ___Setter_5; }
	inline Action_2_t4105459918 ** get_address_of_Setter_5() { return &___Setter_5; }
	inline void set_Setter_5(Action_2_t4105459918 * value)
	{
		___Setter_5 = value;
		Il2CppCodeGenWriteBarrier(&___Setter_5, value);
	}

	inline static int32_t get_offset_of_DefaultValue_6() { return static_cast<int32_t>(offsetof(InjectableInfo_t1147709774, ___DefaultValue_6)); }
	inline Il2CppObject * get_DefaultValue_6() const { return ___DefaultValue_6; }
	inline Il2CppObject ** get_address_of_DefaultValue_6() { return &___DefaultValue_6; }
	inline void set_DefaultValue_6(Il2CppObject * value)
	{
		___DefaultValue_6 = value;
		Il2CppCodeGenWriteBarrier(&___DefaultValue_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
