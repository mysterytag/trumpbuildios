﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameController/<SendMoneyCoroutine>c__Iterator12
struct U3CSendMoneyCoroutineU3Ec__Iterator12_t392378224;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GameController/<SendMoneyCoroutine>c__Iterator12::.ctor()
extern "C"  void U3CSendMoneyCoroutineU3Ec__Iterator12__ctor_m2502372492 (U3CSendMoneyCoroutineU3Ec__Iterator12_t392378224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameController/<SendMoneyCoroutine>c__Iterator12::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSendMoneyCoroutineU3Ec__Iterator12_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1505698512 (U3CSendMoneyCoroutineU3Ec__Iterator12_t392378224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameController/<SendMoneyCoroutine>c__Iterator12::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSendMoneyCoroutineU3Ec__Iterator12_System_Collections_IEnumerator_get_Current_m783337060 (U3CSendMoneyCoroutineU3Ec__Iterator12_t392378224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameController/<SendMoneyCoroutine>c__Iterator12::MoveNext()
extern "C"  bool U3CSendMoneyCoroutineU3Ec__Iterator12_MoveNext_m2697426384 (U3CSendMoneyCoroutineU3Ec__Iterator12_t392378224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController/<SendMoneyCoroutine>c__Iterator12::Dispose()
extern "C"  void U3CSendMoneyCoroutineU3Ec__Iterator12_Dispose_m3791529417 (U3CSendMoneyCoroutineU3Ec__Iterator12_t392378224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController/<SendMoneyCoroutine>c__Iterator12::Reset()
extern "C"  void U3CSendMoneyCoroutineU3Ec__Iterator12_Reset_m148805433 (U3CSendMoneyCoroutineU3Ec__Iterator12_t392378224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
