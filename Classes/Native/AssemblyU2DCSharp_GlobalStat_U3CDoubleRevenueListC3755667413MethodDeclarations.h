﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GlobalStat/<DoubleRevenueListCheck>c__Iterator18
struct U3CDoubleRevenueListCheckU3Ec__Iterator18_t3755667413;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void GlobalStat/<DoubleRevenueListCheck>c__Iterator18::.ctor()
extern "C"  void U3CDoubleRevenueListCheckU3Ec__Iterator18__ctor_m2294203070 (U3CDoubleRevenueListCheckU3Ec__Iterator18_t3755667413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GlobalStat/<DoubleRevenueListCheck>c__Iterator18::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDoubleRevenueListCheckU3Ec__Iterator18_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m739772382 (U3CDoubleRevenueListCheckU3Ec__Iterator18_t3755667413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GlobalStat/<DoubleRevenueListCheck>c__Iterator18::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoubleRevenueListCheckU3Ec__Iterator18_System_Collections_IEnumerator_get_Current_m3881928562 (U3CDoubleRevenueListCheckU3Ec__Iterator18_t3755667413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GlobalStat/<DoubleRevenueListCheck>c__Iterator18::MoveNext()
extern "C"  bool U3CDoubleRevenueListCheckU3Ec__Iterator18_MoveNext_m3054951006 (U3CDoubleRevenueListCheckU3Ec__Iterator18_t3755667413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalStat/<DoubleRevenueListCheck>c__Iterator18::Dispose()
extern "C"  void U3CDoubleRevenueListCheckU3Ec__Iterator18_Dispose_m1309210491 (U3CDoubleRevenueListCheckU3Ec__Iterator18_t3755667413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalStat/<DoubleRevenueListCheck>c__Iterator18::Reset()
extern "C"  void U3CDoubleRevenueListCheckU3Ec__Iterator18_Reset_m4235603307 (U3CDoubleRevenueListCheckU3Ec__Iterator18_t3755667413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GlobalStat/<DoubleRevenueListCheck>c__Iterator18::<>m__110(System.Int64)
extern "C"  String_t* U3CDoubleRevenueListCheckU3Ec__Iterator18_U3CU3Em__110_m1801381440 (U3CDoubleRevenueListCheckU3Ec__Iterator18_t3755667413 * __this, int64_t ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GlobalStat/<DoubleRevenueListCheck>c__Iterator18::<>m__111(System.Int64)
extern "C"  bool U3CDoubleRevenueListCheckU3Ec__Iterator18_U3CU3Em__111_m3418381330 (Il2CppObject * __this /* static, unused */, int64_t ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GlobalStat/<DoubleRevenueListCheck>c__Iterator18::<>m__112(System.Int64)
extern "C"  bool U3CDoubleRevenueListCheckU3Ec__Iterator18_U3CU3Em__112_m630965843 (Il2CppObject * __this /* static, unused */, int64_t ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
