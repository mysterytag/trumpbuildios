﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GooglePlayGames.BasicApi.InvitationReceivedDelegate
struct InvitationReceivedDelegate_t716623425;
// GooglePlayGames.BasicApi.Multiplayer.MatchDelegate
struct MatchDelegate_t4292352560;

#include "mscorlib_System_ValueType4014882752.h"
#include "AssemblyU2DCSharp_GooglePlayGames_BasicApi_PlayGame962255808.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GooglePlayGames.BasicApi.PlayGamesClientConfiguration
struct  PlayGamesClientConfiguration_t962255808 
{
public:
	// System.Boolean GooglePlayGames.BasicApi.PlayGamesClientConfiguration::mEnableSavedGames
	bool ___mEnableSavedGames_1;
	// System.Boolean GooglePlayGames.BasicApi.PlayGamesClientConfiguration::mEnableDeprecatedCloudSave
	bool ___mEnableDeprecatedCloudSave_2;
	// GooglePlayGames.BasicApi.InvitationReceivedDelegate GooglePlayGames.BasicApi.PlayGamesClientConfiguration::mInvitationDelegate
	InvitationReceivedDelegate_t716623425 * ___mInvitationDelegate_3;
	// GooglePlayGames.BasicApi.Multiplayer.MatchDelegate GooglePlayGames.BasicApi.PlayGamesClientConfiguration::mMatchDelegate
	MatchDelegate_t4292352560 * ___mMatchDelegate_4;

public:
	inline static int32_t get_offset_of_mEnableSavedGames_1() { return static_cast<int32_t>(offsetof(PlayGamesClientConfiguration_t962255808, ___mEnableSavedGames_1)); }
	inline bool get_mEnableSavedGames_1() const { return ___mEnableSavedGames_1; }
	inline bool* get_address_of_mEnableSavedGames_1() { return &___mEnableSavedGames_1; }
	inline void set_mEnableSavedGames_1(bool value)
	{
		___mEnableSavedGames_1 = value;
	}

	inline static int32_t get_offset_of_mEnableDeprecatedCloudSave_2() { return static_cast<int32_t>(offsetof(PlayGamesClientConfiguration_t962255808, ___mEnableDeprecatedCloudSave_2)); }
	inline bool get_mEnableDeprecatedCloudSave_2() const { return ___mEnableDeprecatedCloudSave_2; }
	inline bool* get_address_of_mEnableDeprecatedCloudSave_2() { return &___mEnableDeprecatedCloudSave_2; }
	inline void set_mEnableDeprecatedCloudSave_2(bool value)
	{
		___mEnableDeprecatedCloudSave_2 = value;
	}

	inline static int32_t get_offset_of_mInvitationDelegate_3() { return static_cast<int32_t>(offsetof(PlayGamesClientConfiguration_t962255808, ___mInvitationDelegate_3)); }
	inline InvitationReceivedDelegate_t716623425 * get_mInvitationDelegate_3() const { return ___mInvitationDelegate_3; }
	inline InvitationReceivedDelegate_t716623425 ** get_address_of_mInvitationDelegate_3() { return &___mInvitationDelegate_3; }
	inline void set_mInvitationDelegate_3(InvitationReceivedDelegate_t716623425 * value)
	{
		___mInvitationDelegate_3 = value;
		Il2CppCodeGenWriteBarrier(&___mInvitationDelegate_3, value);
	}

	inline static int32_t get_offset_of_mMatchDelegate_4() { return static_cast<int32_t>(offsetof(PlayGamesClientConfiguration_t962255808, ___mMatchDelegate_4)); }
	inline MatchDelegate_t4292352560 * get_mMatchDelegate_4() const { return ___mMatchDelegate_4; }
	inline MatchDelegate_t4292352560 ** get_address_of_mMatchDelegate_4() { return &___mMatchDelegate_4; }
	inline void set_mMatchDelegate_4(MatchDelegate_t4292352560 * value)
	{
		___mMatchDelegate_4 = value;
		Il2CppCodeGenWriteBarrier(&___mMatchDelegate_4, value);
	}
};

struct PlayGamesClientConfiguration_t962255808_StaticFields
{
public:
	// GooglePlayGames.BasicApi.PlayGamesClientConfiguration GooglePlayGames.BasicApi.PlayGamesClientConfiguration::DefaultConfiguration
	PlayGamesClientConfiguration_t962255808  ___DefaultConfiguration_0;

public:
	inline static int32_t get_offset_of_DefaultConfiguration_0() { return static_cast<int32_t>(offsetof(PlayGamesClientConfiguration_t962255808_StaticFields, ___DefaultConfiguration_0)); }
	inline PlayGamesClientConfiguration_t962255808  get_DefaultConfiguration_0() const { return ___DefaultConfiguration_0; }
	inline PlayGamesClientConfiguration_t962255808 * get_address_of_DefaultConfiguration_0() { return &___DefaultConfiguration_0; }
	inline void set_DefaultConfiguration_0(PlayGamesClientConfiguration_t962255808  value)
	{
		___DefaultConfiguration_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
