﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Stream_1_gen3823212738MethodDeclarations.h"

// System.Void Stream`1<CollectionRemoveEvent`1<UpgradeButton>>::.ctor()
#define Stream_1__ctor_m2115980907(__this, method) ((  void (*) (Stream_1_t227759202 *, const MethodInfo*))Stream_1__ctor_m2034388992_gshared)(__this, method)
// System.Void Stream`1<CollectionRemoveEvent`1<UpgradeButton>>::Send(T)
#define Stream_1_Send_m645198461(__this, ___value0, method) ((  void (*) (Stream_1_t227759202 *, CollectionRemoveEvent_1_t1536620180 *, const MethodInfo*))Stream_1_Send_m563606546_gshared)(__this, ___value0, method)
// System.Void Stream`1<CollectionRemoveEvent`1<UpgradeButton>>::SendInTransaction(T,System.Int32)
#define Stream_1_SendInTransaction_m2915455955(__this, ___value0, ___p1, method) ((  void (*) (Stream_1_t227759202 *, CollectionRemoveEvent_1_t1536620180 *, int32_t, const MethodInfo*))Stream_1_SendInTransaction_m3764966696_gshared)(__this, ___value0, ___p1, method)
// System.IDisposable Stream`1<CollectionRemoveEvent`1<UpgradeButton>>::Listen(System.Action`1<T>,Priority)
#define Stream_1_Listen_m2606878351(__this, ___action0, ___priority1, method) ((  Il2CppObject * (*) (Stream_1_t227759202 *, Action_1_t1685072885 *, int32_t, const MethodInfo*))Stream_1_Listen_m889871082_gshared)(__this, ___action0, ___priority1, method)
// System.IDisposable Stream`1<CollectionRemoveEvent`1<UpgradeButton>>::Listen(System.Action,Priority)
#define Stream_1_Listen_m1567495480(__this, ___action0, ___p1, method) ((  Il2CppObject * (*) (Stream_1_t227759202 *, Action_t437523947 *, int32_t, const MethodInfo*))Stream_1_Listen_m2133851133_gshared)(__this, ___action0, ___p1, method)
// System.Void Stream`1<CollectionRemoveEvent`1<UpgradeButton>>::Dispose()
#define Stream_1_Dispose_m1836403688(__this, method) ((  void (*) (Stream_1_t227759202 *, const MethodInfo*))Stream_1_Dispose_m735984701_gshared)(__this, method)
// System.Void Stream`1<CollectionRemoveEvent`1<UpgradeButton>>::SetInputStream(IStream`1<T>)
#define Stream_1_SetInputStream_m2128855669(__this, ___stream0, method) ((  void (*) (Stream_1_t227759202 *, Il2CppObject*, const MethodInfo*))Stream_1_SetInputStream_m3106996224_gshared)(__this, ___stream0, method)
// System.Void Stream`1<CollectionRemoveEvent`1<UpgradeButton>>::ClearInputStream()
#define Stream_1_ClearInputStream_m763957654(__this, method) ((  void (*) (Stream_1_t227759202 *, const MethodInfo*))Stream_1_ClearInputStream_m445690081_gshared)(__this, method)
// System.Void Stream`1<CollectionRemoveEvent`1<UpgradeButton>>::TransactionIterationFinished()
#define Stream_1_TransactionIterationFinished_m3718324330(__this, method) ((  void (*) (Stream_1_t227759202 *, const MethodInfo*))Stream_1_TransactionIterationFinished_m3113559861_gshared)(__this, method)
// System.Void Stream`1<CollectionRemoveEvent`1<UpgradeButton>>::Unpack(System.Int32)
#define Stream_1_Unpack_m5745660(__this, ___p0, method) ((  void (*) (Stream_1_t227759202 *, int32_t, const MethodInfo*))Stream_1_Unpack_m3388253319_gshared)(__this, ___p0, method)
// System.Void Stream`1<CollectionRemoveEvent`1<UpgradeButton>>::<SetInputStream>m__1BA(T)
#define Stream_1_U3CSetInputStreamU3Em__1BA_m3731630152(__this, ___val0, method) ((  void (*) (Stream_1_t227759202 *, CollectionRemoveEvent_1_t1536620180 *, const MethodInfo*))Stream_1_U3CSetInputStreamU3Em__1BA_m3579563677_gshared)(__this, ___val0, method)
