﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/MapDisposable
struct MapDisposable_t304581564;

#include "codegen/il2cpp-codegen.h"

// System.Void CellReactiveApi/MapDisposable::.ctor()
extern "C"  void MapDisposable__ctor_m1056915391 (MapDisposable_t304581564 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
