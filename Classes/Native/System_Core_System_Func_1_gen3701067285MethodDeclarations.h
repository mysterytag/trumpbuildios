﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Func`1<UniRx.Unit>
struct Func_1_t3701067285;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Unit2558286038.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Func`1<UniRx.Unit>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_1__ctor_m2615142501_gshared (Func_1_t3701067285 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Func_1__ctor_m2615142501(__this, ___object0, ___method1, method) ((  void (*) (Func_1_t3701067285 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_1__ctor_m2615142501_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`1<UniRx.Unit>::Invoke()
extern "C"  Unit_t2558286038  Func_1_Invoke_m4267058009_gshared (Func_1_t3701067285 * __this, const MethodInfo* method);
#define Func_1_Invoke_m4267058009(__this, method) ((  Unit_t2558286038  (*) (Func_1_t3701067285 *, const MethodInfo*))Func_1_Invoke_m4267058009_gshared)(__this, method)
// System.IAsyncResult System.Func`1<UniRx.Unit>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_1_BeginInvoke_m3578428912_gshared (Func_1_t3701067285 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method);
#define Func_1_BeginInvoke_m3578428912(__this, ___callback0, ___object1, method) ((  Il2CppObject * (*) (Func_1_t3701067285 *, AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Func_1_BeginInvoke_m3578428912_gshared)(__this, ___callback0, ___object1, method)
// TResult System.Func`1<UniRx.Unit>::EndInvoke(System.IAsyncResult)
extern "C"  Unit_t2558286038  Func_1_EndInvoke_m3449556943_gshared (Func_1_t3701067285 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Func_1_EndInvoke_m3449556943(__this, ___result0, method) ((  Unit_t2558286038  (*) (Func_1_t3701067285 *, Il2CppObject *, const MethodInfo*))Func_1_EndInvoke_m3449556943_gshared)(__this, ___result0, method)
