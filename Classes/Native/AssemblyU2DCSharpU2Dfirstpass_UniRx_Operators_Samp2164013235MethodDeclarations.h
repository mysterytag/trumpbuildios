﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SampleObservable`1/Sample<System.Object>
struct Sample_t2164013235;
// UniRx.Operators.SampleObservable`1<System.Object>
struct SampleObservable_1_t2474383436;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Action`1<System.TimeSpan>
struct Action_1_t912315597;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.SampleObservable`1/Sample<System.Object>::.ctor(UniRx.Operators.SampleObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Sample__ctor_m2461186222_gshared (Sample_t2164013235 * __this, SampleObservable_1_t2474383436 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Sample__ctor_m2461186222(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Sample_t2164013235 *, SampleObservable_1_t2474383436 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Sample__ctor_m2461186222_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.SampleObservable`1/Sample<System.Object>::Run()
extern "C"  Il2CppObject * Sample_Run_m1032550085_gshared (Sample_t2164013235 * __this, const MethodInfo* method);
#define Sample_Run_m1032550085(__this, method) ((  Il2CppObject * (*) (Sample_t2164013235 *, const MethodInfo*))Sample_Run_m1032550085_gshared)(__this, method)
// System.Void UniRx.Operators.SampleObservable`1/Sample<System.Object>::OnNextTick()
extern "C"  void Sample_OnNextTick_m319470684_gshared (Sample_t2164013235 * __this, const MethodInfo* method);
#define Sample_OnNextTick_m319470684(__this, method) ((  void (*) (Sample_t2164013235 *, const MethodInfo*))Sample_OnNextTick_m319470684_gshared)(__this, method)
// System.Void UniRx.Operators.SampleObservable`1/Sample<System.Object>::OnNextRecursive(System.Action`1<System.TimeSpan>)
extern "C"  void Sample_OnNextRecursive_m2478447671_gshared (Sample_t2164013235 * __this, Action_1_t912315597 * ___self0, const MethodInfo* method);
#define Sample_OnNextRecursive_m2478447671(__this, ___self0, method) ((  void (*) (Sample_t2164013235 *, Action_1_t912315597 *, const MethodInfo*))Sample_OnNextRecursive_m2478447671_gshared)(__this, ___self0, method)
// System.Void UniRx.Operators.SampleObservable`1/Sample<System.Object>::OnNext(T)
extern "C"  void Sample_OnNext_m4237267039_gshared (Sample_t2164013235 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Sample_OnNext_m4237267039(__this, ___value0, method) ((  void (*) (Sample_t2164013235 *, Il2CppObject *, const MethodInfo*))Sample_OnNext_m4237267039_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.SampleObservable`1/Sample<System.Object>::OnError(System.Exception)
extern "C"  void Sample_OnError_m1706888558_gshared (Sample_t2164013235 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Sample_OnError_m1706888558(__this, ___error0, method) ((  void (*) (Sample_t2164013235 *, Exception_t1967233988 *, const MethodInfo*))Sample_OnError_m1706888558_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.SampleObservable`1/Sample<System.Object>::OnCompleted()
extern "C"  void Sample_OnCompleted_m1687287233_gshared (Sample_t2164013235 * __this, const MethodInfo* method);
#define Sample_OnCompleted_m1687287233(__this, method) ((  void (*) (Sample_t2164013235 *, const MethodInfo*))Sample_OnCompleted_m1687287233_gshared)(__this, method)
