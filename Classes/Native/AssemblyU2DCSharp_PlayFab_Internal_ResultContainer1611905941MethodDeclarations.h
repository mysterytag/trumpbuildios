﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PlayFab_Internal_ResultContainer1789381198MethodDeclarations.h"

// System.Void PlayFab.Internal.ResultContainer`1<PlayFab.ClientModels.GetFriendLeaderboardAroundCurrentUserResult>::.ctor()
#define ResultContainer_1__ctor_m3099468199(__this, method) ((  void (*) (ResultContainer_1_t1611905941 *, const MethodInfo*))ResultContainer_1__ctor_m944826126_gshared)(__this, method)
// System.Void PlayFab.Internal.ResultContainer`1<PlayFab.ClientModels.GetFriendLeaderboardAroundCurrentUserResult>::.cctor()
#define ResultContainer_1__cctor_m1112137446(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ResultContainer_1__cctor_m3037709919_gshared)(__this /* static, unused */, method)
// PlayFab.Internal.ResultContainer`1<TResultType> PlayFab.Internal.ResultContainer`1<PlayFab.ClientModels.GetFriendLeaderboardAroundCurrentUserResult>::KillWarnings()
#define ResultContainer_1_KillWarnings_m1661510805(__this /* static, unused */, method) ((  ResultContainer_1_t1611905941 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ResultContainer_1_KillWarnings_m2638698408_gshared)(__this /* static, unused */, method)
// TResultType PlayFab.Internal.ResultContainer`1<PlayFab.ClientModels.GetFriendLeaderboardAroundCurrentUserResult>::HandleResults(PlayFab.CallRequestContainer,System.Delegate,PlayFab.ErrorCallback,System.Action`2<TResultType,PlayFab.CallRequestContainer>)
#define ResultContainer_1_HandleResults_m3201875985(__this /* static, unused */, ___callRequest0, ___resultCallback1, ___errorCallback2, ___resultAction3, method) ((  GetFriendLeaderboardAroundCurrentUserResult_t659631163 * (*) (Il2CppObject * /* static, unused */, CallRequestContainer_t432318609 *, Delegate_t3660574010 *, ErrorCallback_t582108558 *, Action_2_t2179996712 *, const MethodInfo*))ResultContainer_1_HandleResults_m887745544_gshared)(__this /* static, unused */, ___callRequest0, ___resultCallback1, ___errorCallback2, ___resultAction3, method)
