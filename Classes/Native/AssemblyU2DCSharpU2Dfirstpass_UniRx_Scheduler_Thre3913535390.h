﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.BooleanDisposable
struct BooleanDisposable_t3065601722;
// System.Action
struct Action_t437523947;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Scheduler/ThreadPoolScheduler/<Schedule>c__AnonStorey79
struct  U3CScheduleU3Ec__AnonStorey79_t3913535390  : public Il2CppObject
{
public:
	// UniRx.BooleanDisposable UniRx.Scheduler/ThreadPoolScheduler/<Schedule>c__AnonStorey79::d
	BooleanDisposable_t3065601722 * ___d_0;
	// System.Action UniRx.Scheduler/ThreadPoolScheduler/<Schedule>c__AnonStorey79::action
	Action_t437523947 * ___action_1;

public:
	inline static int32_t get_offset_of_d_0() { return static_cast<int32_t>(offsetof(U3CScheduleU3Ec__AnonStorey79_t3913535390, ___d_0)); }
	inline BooleanDisposable_t3065601722 * get_d_0() const { return ___d_0; }
	inline BooleanDisposable_t3065601722 ** get_address_of_d_0() { return &___d_0; }
	inline void set_d_0(BooleanDisposable_t3065601722 * value)
	{
		___d_0 = value;
		Il2CppCodeGenWriteBarrier(&___d_0, value);
	}

	inline static int32_t get_offset_of_action_1() { return static_cast<int32_t>(offsetof(U3CScheduleU3Ec__AnonStorey79_t3913535390, ___action_1)); }
	inline Action_t437523947 * get_action_1() const { return ___action_1; }
	inline Action_t437523947 ** get_address_of_action_1() { return &___action_1; }
	inline void set_action_1(Action_t437523947 * value)
	{
		___action_1 = value;
		Il2CppCodeGenWriteBarrier(&___action_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
