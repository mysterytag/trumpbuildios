﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<ConsumeItem>c__AnonStoreyA1
struct U3CConsumeItemU3Ec__AnonStoreyA1_t990121260;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<ConsumeItem>c__AnonStoreyA1::.ctor()
extern "C"  void U3CConsumeItemU3Ec__AnonStoreyA1__ctor_m2427947943 (U3CConsumeItemU3Ec__AnonStoreyA1_t990121260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<ConsumeItem>c__AnonStoreyA1::<>m__8E(PlayFab.CallRequestContainer)
extern "C"  void U3CConsumeItemU3Ec__AnonStoreyA1_U3CU3Em__8E_m1100213906 (U3CConsumeItemU3Ec__AnonStoreyA1_t990121260 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
