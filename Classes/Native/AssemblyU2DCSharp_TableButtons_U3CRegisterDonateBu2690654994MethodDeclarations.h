﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey162/<RegisterDonateButtons>c__AnonStorey163
struct U3CRegisterDonateButtonsU3Ec__AnonStorey163_t2690654994;

#include "codegen/il2cpp-codegen.h"

// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey162/<RegisterDonateButtons>c__AnonStorey163::.ctor()
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey163__ctor_m4208676026 (U3CRegisterDonateButtonsU3Ec__AnonStorey163_t2690654994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TableButtons/<RegisterDonateButtons>c__AnonStorey169/<RegisterDonateButtons>c__AnonStorey162/<RegisterDonateButtons>c__AnonStorey163::<>m__277(System.Int64)
extern "C"  void U3CRegisterDonateButtonsU3Ec__AnonStorey163_U3CU3Em__277_m3873328803 (U3CRegisterDonateButtonsU3Ec__AnonStorey163_t2690654994 * __this, int64_t ___l0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
