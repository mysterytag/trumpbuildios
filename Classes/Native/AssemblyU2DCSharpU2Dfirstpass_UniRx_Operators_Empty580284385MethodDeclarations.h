﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionMoveEvent`1<System.Object>>
struct Empty_t580284385;
// UniRx.IObserver`1<UniRx.CollectionMoveEvent`1<System.Object>>
struct IObserver_1_t833465454;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionMove2916433847.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionMoveEvent`1<System.Object>>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Empty__ctor_m805200901_gshared (Empty_t580284385 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define Empty__ctor_m805200901(__this, ___observer0, ___cancel1, method) ((  void (*) (Empty_t580284385 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Empty__ctor_m805200901_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionMoveEvent`1<System.Object>>::OnNext(T)
extern "C"  void Empty_OnNext_m3327314029_gshared (Empty_t580284385 * __this, CollectionMoveEvent_1_t2916433847  ___value0, const MethodInfo* method);
#define Empty_OnNext_m3327314029(__this, ___value0, method) ((  void (*) (Empty_t580284385 *, CollectionMoveEvent_1_t2916433847 , const MethodInfo*))Empty_OnNext_m3327314029_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionMoveEvent`1<System.Object>>::OnError(System.Exception)
extern "C"  void Empty_OnError_m3680135548_gshared (Empty_t580284385 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Empty_OnError_m3680135548(__this, ___error0, method) ((  void (*) (Empty_t580284385 *, Exception_t1967233988 *, const MethodInfo*))Empty_OnError_m3680135548_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.EmptyObservable`1/Empty<UniRx.CollectionMoveEvent`1<System.Object>>::OnCompleted()
extern "C"  void Empty_OnCompleted_m2864608975_gshared (Empty_t580284385 * __this, const MethodInfo* method);
#define Empty_OnCompleted_m2864608975(__this, method) ((  void (*) (Empty_t580284385 *, const MethodInfo*))Empty_OnCompleted_m2864608975_gshared)(__this, method)
