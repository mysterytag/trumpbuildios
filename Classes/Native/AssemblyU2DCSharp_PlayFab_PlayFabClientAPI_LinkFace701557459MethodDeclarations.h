﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/LinkFacebookAccountResponseCallback
struct LinkFacebookAccountResponseCallback_t701557459;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.LinkFacebookAccountRequest
struct LinkFacebookAccountRequest_t3956942034;
// PlayFab.ClientModels.LinkFacebookAccountResult
struct LinkFacebookAccountResult_t3626474394;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkFaceboo3956942034.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_LinkFaceboo3626474394.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/LinkFacebookAccountResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void LinkFacebookAccountResponseCallback__ctor_m710861282 (LinkFacebookAccountResponseCallback_t701557459 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LinkFacebookAccountResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.LinkFacebookAccountRequest,PlayFab.ClientModels.LinkFacebookAccountResult,PlayFab.PlayFabError,System.Object)
extern "C"  void LinkFacebookAccountResponseCallback_Invoke_m2138791143 (LinkFacebookAccountResponseCallback_t701557459 * __this, String_t* ___urlPath0, int32_t ___callId1, LinkFacebookAccountRequest_t3956942034 * ___request2, LinkFacebookAccountResult_t3626474394 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_LinkFacebookAccountResponseCallback_t701557459(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, LinkFacebookAccountRequest_t3956942034 * ___request2, LinkFacebookAccountResult_t3626474394 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/LinkFacebookAccountResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.LinkFacebookAccountRequest,PlayFab.ClientModels.LinkFacebookAccountResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LinkFacebookAccountResponseCallback_BeginInvoke_m1248246636 (LinkFacebookAccountResponseCallback_t701557459 * __this, String_t* ___urlPath0, int32_t ___callId1, LinkFacebookAccountRequest_t3956942034 * ___request2, LinkFacebookAccountResult_t3626474394 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/LinkFacebookAccountResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void LinkFacebookAccountResponseCallback_EndInvoke_m3869414642 (LinkFacebookAccountResponseCallback_t701557459 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
