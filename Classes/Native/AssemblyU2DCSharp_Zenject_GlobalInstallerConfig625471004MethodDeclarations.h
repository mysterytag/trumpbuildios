﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.GlobalInstallerConfig
struct GlobalInstallerConfig_t625471004;

#include "codegen/il2cpp-codegen.h"

// System.Void Zenject.GlobalInstallerConfig::.ctor()
extern "C"  void GlobalInstallerConfig__ctor_m11203715 (GlobalInstallerConfig_t625471004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
