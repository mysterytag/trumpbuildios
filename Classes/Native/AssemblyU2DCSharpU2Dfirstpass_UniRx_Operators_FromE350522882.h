﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.Action`1<System.Int32>>
struct Action_1_t3144320197;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper1911559758.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.FromEventObservable_`1<System.Int32>
struct  FromEventObservable__1_t350522882  : public OperatorObservableBase_1_t1911559758
{
public:
	// System.Action`1<System.Action`1<T>> UniRx.Operators.FromEventObservable_`1::addHandler
	Action_1_t3144320197 * ___addHandler_1;
	// System.Action`1<System.Action`1<T>> UniRx.Operators.FromEventObservable_`1::removeHandler
	Action_1_t3144320197 * ___removeHandler_2;

public:
	inline static int32_t get_offset_of_addHandler_1() { return static_cast<int32_t>(offsetof(FromEventObservable__1_t350522882, ___addHandler_1)); }
	inline Action_1_t3144320197 * get_addHandler_1() const { return ___addHandler_1; }
	inline Action_1_t3144320197 ** get_address_of_addHandler_1() { return &___addHandler_1; }
	inline void set_addHandler_1(Action_1_t3144320197 * value)
	{
		___addHandler_1 = value;
		Il2CppCodeGenWriteBarrier(&___addHandler_1, value);
	}

	inline static int32_t get_offset_of_removeHandler_2() { return static_cast<int32_t>(offsetof(FromEventObservable__1_t350522882, ___removeHandler_2)); }
	inline Action_1_t3144320197 * get_removeHandler_2() const { return ___removeHandler_2; }
	inline Action_1_t3144320197 ** get_address_of_removeHandler_2() { return &___removeHandler_2; }
	inline void set_removeHandler_2(Action_1_t3144320197 * value)
	{
		___removeHandler_2 = value;
		Il2CppCodeGenWriteBarrier(&___removeHandler_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
