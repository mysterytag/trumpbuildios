﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/GetLeaderboardAroundCharacterRequestCallback
struct GetLeaderboardAroundCharacterRequestCallback_t1344227519;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.GetLeaderboardAroundCharacterRequest
struct GetLeaderboardAroundCharacterRequest_t1108200042;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_GetLeaderbo1108200042.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/GetLeaderboardAroundCharacterRequestCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetLeaderboardAroundCharacterRequestCallback__ctor_m920276526 (GetLeaderboardAroundCharacterRequestCallback_t1344227519 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetLeaderboardAroundCharacterRequestCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.GetLeaderboardAroundCharacterRequest,System.Object)
extern "C"  void GetLeaderboardAroundCharacterRequestCallback_Invoke_m672464639 (GetLeaderboardAroundCharacterRequestCallback_t1344227519 * __this, String_t* ___urlPath0, int32_t ___callId1, GetLeaderboardAroundCharacterRequest_t1108200042 * ___request2, Il2CppObject * ___customData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_GetLeaderboardAroundCharacterRequestCallback_t1344227519(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, GetLeaderboardAroundCharacterRequest_t1108200042 * ___request2, Il2CppObject * ___customData3);
// System.IAsyncResult PlayFab.PlayFabClientAPI/GetLeaderboardAroundCharacterRequestCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.GetLeaderboardAroundCharacterRequest,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetLeaderboardAroundCharacterRequestCallback_BeginInvoke_m1525748516 (GetLeaderboardAroundCharacterRequestCallback_t1344227519 * __this, String_t* ___urlPath0, int32_t ___callId1, GetLeaderboardAroundCharacterRequest_t1108200042 * ___request2, Il2CppObject * ___customData3, AsyncCallback_t1363551830 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/GetLeaderboardAroundCharacterRequestCallback::EndInvoke(System.IAsyncResult)
extern "C"  void GetLeaderboardAroundCharacterRequestCallback_EndInvoke_m3673687870 (GetLeaderboardAroundCharacterRequestCallback_t1344227519 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
