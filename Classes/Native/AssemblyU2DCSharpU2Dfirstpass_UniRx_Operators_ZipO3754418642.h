﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UniRx.Operators.ZipObservable`1<System.Object>
struct ZipObservable_1_t2062551787;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.Queue`1<System.Object>[]
struct Queue_1U5BU5D_t3718469497;
// System.Boolean[]
struct BooleanU5BU5D_t3804927312;

#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_Operators_Oper4067176365.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.ZipObservable`1/Zip<System.Object>
struct  Zip_t3754418642  : public OperatorObserverBase_2_t4067176365
{
public:
	// UniRx.Operators.ZipObservable`1<T> UniRx.Operators.ZipObservable`1/Zip::parent
	ZipObservable_1_t2062551787 * ___parent_2;
	// System.Object UniRx.Operators.ZipObservable`1/Zip::gate
	Il2CppObject * ___gate_3;
	// System.Collections.Generic.Queue`1<T>[] UniRx.Operators.ZipObservable`1/Zip::queues
	Queue_1U5BU5D_t3718469497* ___queues_4;
	// System.Boolean[] UniRx.Operators.ZipObservable`1/Zip::isDone
	BooleanU5BU5D_t3804927312* ___isDone_5;
	// System.Int32 UniRx.Operators.ZipObservable`1/Zip::length
	int32_t ___length_6;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(Zip_t3754418642, ___parent_2)); }
	inline ZipObservable_1_t2062551787 * get_parent_2() const { return ___parent_2; }
	inline ZipObservable_1_t2062551787 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(ZipObservable_1_t2062551787 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___parent_2, value);
	}

	inline static int32_t get_offset_of_gate_3() { return static_cast<int32_t>(offsetof(Zip_t3754418642, ___gate_3)); }
	inline Il2CppObject * get_gate_3() const { return ___gate_3; }
	inline Il2CppObject ** get_address_of_gate_3() { return &___gate_3; }
	inline void set_gate_3(Il2CppObject * value)
	{
		___gate_3 = value;
		Il2CppCodeGenWriteBarrier(&___gate_3, value);
	}

	inline static int32_t get_offset_of_queues_4() { return static_cast<int32_t>(offsetof(Zip_t3754418642, ___queues_4)); }
	inline Queue_1U5BU5D_t3718469497* get_queues_4() const { return ___queues_4; }
	inline Queue_1U5BU5D_t3718469497** get_address_of_queues_4() { return &___queues_4; }
	inline void set_queues_4(Queue_1U5BU5D_t3718469497* value)
	{
		___queues_4 = value;
		Il2CppCodeGenWriteBarrier(&___queues_4, value);
	}

	inline static int32_t get_offset_of_isDone_5() { return static_cast<int32_t>(offsetof(Zip_t3754418642, ___isDone_5)); }
	inline BooleanU5BU5D_t3804927312* get_isDone_5() const { return ___isDone_5; }
	inline BooleanU5BU5D_t3804927312** get_address_of_isDone_5() { return &___isDone_5; }
	inline void set_isDone_5(BooleanU5BU5D_t3804927312* value)
	{
		___isDone_5 = value;
		Il2CppCodeGenWriteBarrier(&___isDone_5, value);
	}

	inline static int32_t get_offset_of_length_6() { return static_cast<int32_t>(offsetof(Zip_t3754418642, ___length_6)); }
	inline int32_t get_length_6() const { return ___length_6; }
	inline int32_t* get_address_of_length_6() { return &___length_6; }
	inline void set_length_6(int32_t value)
	{
		___length_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
