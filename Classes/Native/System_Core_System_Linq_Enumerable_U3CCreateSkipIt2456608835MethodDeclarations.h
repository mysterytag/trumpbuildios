﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct U3CCreateSkipIteratorU3Ec__Iterator16_1_t2456608835;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t501095600;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23312956448.h"

// System.Void System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor()
extern "C"  void U3CCreateSkipIteratorU3Ec__Iterator16_1__ctor_m3112457808_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t2456608835 * __this, const MethodInfo* method);
#define U3CCreateSkipIteratorU3Ec__Iterator16_1__ctor_m3112457808(__this, method) ((  void (*) (U3CCreateSkipIteratorU3Ec__Iterator16_1_t2456608835 *, const MethodInfo*))U3CCreateSkipIteratorU3Ec__Iterator16_1__ctor_m3112457808_gshared)(__this, method)
// TSource System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern "C"  KeyValuePair_2_t3312956448  U3CCreateSkipIteratorU3Ec__Iterator16_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m2916775193_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t2456608835 * __this, const MethodInfo* method);
#define U3CCreateSkipIteratorU3Ec__Iterator16_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m2916775193(__this, method) ((  KeyValuePair_2_t3312956448  (*) (U3CCreateSkipIteratorU3Ec__Iterator16_1_t2456608835 *, const MethodInfo*))U3CCreateSkipIteratorU3Ec__Iterator16_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m2916775193_gshared)(__this, method)
// System.Object System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateSkipIteratorU3Ec__Iterator16_1_System_Collections_IEnumerator_get_Current_m3102820320_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t2456608835 * __this, const MethodInfo* method);
#define U3CCreateSkipIteratorU3Ec__Iterator16_1_System_Collections_IEnumerator_get_Current_m3102820320(__this, method) ((  Il2CppObject * (*) (U3CCreateSkipIteratorU3Ec__Iterator16_1_t2456608835 *, const MethodInfo*))U3CCreateSkipIteratorU3Ec__Iterator16_1_System_Collections_IEnumerator_get_Current_m3102820320_gshared)(__this, method)
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateSkipIteratorU3Ec__Iterator16_1_System_Collections_IEnumerable_GetEnumerator_m294036097_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t2456608835 * __this, const MethodInfo* method);
#define U3CCreateSkipIteratorU3Ec__Iterator16_1_System_Collections_IEnumerable_GetEnumerator_m294036097(__this, method) ((  Il2CppObject * (*) (U3CCreateSkipIteratorU3Ec__Iterator16_1_t2456608835 *, const MethodInfo*))U3CCreateSkipIteratorU3Ec__Iterator16_1_System_Collections_IEnumerable_GetEnumerator_m294036097_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateSkipIteratorU3Ec__Iterator16_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m1875105964_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t2456608835 * __this, const MethodInfo* method);
#define U3CCreateSkipIteratorU3Ec__Iterator16_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m1875105964(__this, method) ((  Il2CppObject* (*) (U3CCreateSkipIteratorU3Ec__Iterator16_1_t2456608835 *, const MethodInfo*))U3CCreateSkipIteratorU3Ec__Iterator16_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m1875105964_gshared)(__this, method)
// System.Boolean System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C"  bool U3CCreateSkipIteratorU3Ec__Iterator16_1_MoveNext_m762291308_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t2456608835 * __this, const MethodInfo* method);
#define U3CCreateSkipIteratorU3Ec__Iterator16_1_MoveNext_m762291308(__this, method) ((  bool (*) (U3CCreateSkipIteratorU3Ec__Iterator16_1_t2456608835 *, const MethodInfo*))U3CCreateSkipIteratorU3Ec__Iterator16_1_MoveNext_m762291308_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
extern "C"  void U3CCreateSkipIteratorU3Ec__Iterator16_1_Dispose_m1672998541_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t2456608835 * __this, const MethodInfo* method);
#define U3CCreateSkipIteratorU3Ec__Iterator16_1_Dispose_m1672998541(__this, method) ((  void (*) (U3CCreateSkipIteratorU3Ec__Iterator16_1_t2456608835 *, const MethodInfo*))U3CCreateSkipIteratorU3Ec__Iterator16_1_Dispose_m1672998541_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Reset()
extern "C"  void U3CCreateSkipIteratorU3Ec__Iterator16_1_Reset_m758890749_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t2456608835 * __this, const MethodInfo* method);
#define U3CCreateSkipIteratorU3Ec__Iterator16_1_Reset_m758890749(__this, method) ((  void (*) (U3CCreateSkipIteratorU3Ec__Iterator16_1_t2456608835 *, const MethodInfo*))U3CCreateSkipIteratorU3Ec__Iterator16_1_Reset_m758890749_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateSkipIterator>c__Iterator16`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::<>__Finally0()
extern "C"  void U3CCreateSkipIteratorU3Ec__Iterator16_1_U3CU3E__Finally0_m3370359331_gshared (U3CCreateSkipIteratorU3Ec__Iterator16_1_t2456608835 * __this, const MethodInfo* method);
#define U3CCreateSkipIteratorU3Ec__Iterator16_1_U3CU3E__Finally0_m3370359331(__this, method) ((  void (*) (U3CCreateSkipIteratorU3Ec__Iterator16_1_t2456608835 *, const MethodInfo*))U3CCreateSkipIteratorU3Ec__Iterator16_1_U3CU3E__Finally0_m3370359331_gshared)(__this, method)
