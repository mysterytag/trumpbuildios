﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Cell`1<System.DateTime>
struct Cell_1_t521131568;
// SpeedUpWindow
struct SpeedUpWindow_t4178678706;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpeedUpWindow/<Init>c__AnonStorey151
struct  U3CInitU3Ec__AnonStorey151_t1114672632  : public Il2CppObject
{
public:
	// Cell`1<System.DateTime> SpeedUpWindow/<Init>c__AnonStorey151::deliveryTime
	Cell_1_t521131568 * ___deliveryTime_0;
	// SpeedUpWindow SpeedUpWindow/<Init>c__AnonStorey151::<>f__this
	SpeedUpWindow_t4178678706 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_deliveryTime_0() { return static_cast<int32_t>(offsetof(U3CInitU3Ec__AnonStorey151_t1114672632, ___deliveryTime_0)); }
	inline Cell_1_t521131568 * get_deliveryTime_0() const { return ___deliveryTime_0; }
	inline Cell_1_t521131568 ** get_address_of_deliveryTime_0() { return &___deliveryTime_0; }
	inline void set_deliveryTime_0(Cell_1_t521131568 * value)
	{
		___deliveryTime_0 = value;
		Il2CppCodeGenWriteBarrier(&___deliveryTime_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CInitU3Ec__AnonStorey151_t1114672632, ___U3CU3Ef__this_1)); }
	inline SpeedUpWindow_t4178678706 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline SpeedUpWindow_t4178678706 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(SpeedUpWindow_t4178678706 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
