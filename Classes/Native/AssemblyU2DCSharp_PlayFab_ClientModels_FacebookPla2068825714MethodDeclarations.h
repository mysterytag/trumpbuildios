﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.FacebookPlayFabIdPair
struct FacebookPlayFabIdPair_t2068825714;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.FacebookPlayFabIdPair::.ctor()
extern "C"  void FacebookPlayFabIdPair__ctor_m1028679927 (FacebookPlayFabIdPair_t2068825714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.FacebookPlayFabIdPair::get_FacebookId()
extern "C"  String_t* FacebookPlayFabIdPair_get_FacebookId_m2668057242 (FacebookPlayFabIdPair_t2068825714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.FacebookPlayFabIdPair::set_FacebookId(System.String)
extern "C"  void FacebookPlayFabIdPair_set_FacebookId_m205256567 (FacebookPlayFabIdPair_t2068825714 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.FacebookPlayFabIdPair::get_PlayFabId()
extern "C"  String_t* FacebookPlayFabIdPair_get_PlayFabId_m3553163991 (FacebookPlayFabIdPair_t2068825714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.FacebookPlayFabIdPair::set_PlayFabId(System.String)
extern "C"  void FacebookPlayFabIdPair_set_PlayFabId_m3091415772 (FacebookPlayFabIdPair_t2068825714 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
