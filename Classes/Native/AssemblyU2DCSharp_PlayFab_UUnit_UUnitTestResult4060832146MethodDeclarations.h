﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.UUnit.UUnitTestResult
struct UUnitTestResult_t4060832146;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_UUnit_UUnitTestResult_Tes812759807.h"

// System.Void PlayFab.UUnit.UUnitTestResult::.ctor()
extern "C"  void UUnitTestResult__ctor_m3368620499 (UUnitTestResult_t4060832146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.UUnitTestResult::.cctor()
extern "C"  void UUnitTestResult__cctor_m865924154 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.UUnitTestResult::TestStarted()
extern "C"  void UUnitTestResult_TestStarted_m388187840 (UUnitTestResult_t4060832146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.UUnit.UUnitTestResult::TestComplete(System.String,PlayFab.UUnit.UUnitTestResult/TestState,System.Int64,System.String)
extern "C"  void UUnitTestResult_TestComplete_m1748945450 (UUnitTestResult_t4060832146 * __this, String_t* ___testName0, int32_t ___success1, int64_t ___stopwatchMS2, String_t* ___message3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.UUnit.UUnitTestResult::Summary()
extern "C"  String_t* UUnitTestResult_Summary_m2449255482 (UUnitTestResult_t4060832146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayFab.UUnit.UUnitTestResult::AllTestsPassed()
extern "C"  bool UUnitTestResult_AllTestsPassed_m2867551597 (UUnitTestResult_t4060832146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
