﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.UnlinkGameCenterAccountRequest
struct UnlinkGameCenterAccountRequest_t687920076;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.ClientModels.UnlinkGameCenterAccountRequest::.ctor()
extern "C"  void UnlinkGameCenterAccountRequest__ctor_m2334355569 (UnlinkGameCenterAccountRequest_t687920076 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
