﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen334555559.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_427065056.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3719968030_gshared (InternalEnumerator_1_t334555559 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3719968030(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t334555559 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3719968030_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2527920066_gshared (InternalEnumerator_1_t334555559 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2527920066(__this, method) ((  void (*) (InternalEnumerator_1_t334555559 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2527920066_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m506839480_gshared (InternalEnumerator_1_t334555559 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m506839480(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t334555559 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m506839480_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2776886389_gshared (InternalEnumerator_1_t334555559 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2776886389(__this, method) ((  void (*) (InternalEnumerator_1_t334555559 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2776886389_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m136899954_gshared (InternalEnumerator_1_t334555559 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m136899954(__this, method) ((  bool (*) (InternalEnumerator_1_t334555559 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m136899954_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<GameAnalyticsSDK.GA_ServerFieldTypes/FieldType,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t427065056  InternalEnumerator_1_get_Current_m2033003335_gshared (InternalEnumerator_1_t334555559 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2033003335(__this, method) ((  KeyValuePair_2_t427065056  (*) (InternalEnumerator_1_t334555559 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2033003335_gshared)(__this, method)
