﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CellReactiveApi/<WhenSatisfy>c__AnonStorey10C`1/<WhenSatisfy>c__AnonStorey10D`1<System.Object>
struct U3CWhenSatisfyU3Ec__AnonStorey10D_1_t3834446745;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void CellReactiveApi/<WhenSatisfy>c__AnonStorey10C`1/<WhenSatisfy>c__AnonStorey10D`1<System.Object>::.ctor()
extern "C"  void U3CWhenSatisfyU3Ec__AnonStorey10D_1__ctor_m3774480543_gshared (U3CWhenSatisfyU3Ec__AnonStorey10D_1_t3834446745 * __this, const MethodInfo* method);
#define U3CWhenSatisfyU3Ec__AnonStorey10D_1__ctor_m3774480543(__this, method) ((  void (*) (U3CWhenSatisfyU3Ec__AnonStorey10D_1_t3834446745 *, const MethodInfo*))U3CWhenSatisfyU3Ec__AnonStorey10D_1__ctor_m3774480543_gshared)(__this, method)
// System.Void CellReactiveApi/<WhenSatisfy>c__AnonStorey10C`1/<WhenSatisfy>c__AnonStorey10D`1<System.Object>::<>m__196(T)
extern "C"  void U3CWhenSatisfyU3Ec__AnonStorey10D_1_U3CU3Em__196_m1240271766_gshared (U3CWhenSatisfyU3Ec__AnonStorey10D_1_t3834446745 * __this, Il2CppObject * ___val0, const MethodInfo* method);
#define U3CWhenSatisfyU3Ec__AnonStorey10D_1_U3CU3Em__196_m1240271766(__this, ___val0, method) ((  void (*) (U3CWhenSatisfyU3Ec__AnonStorey10D_1_t3834446745 *, Il2CppObject *, const MethodInfo*))U3CWhenSatisfyU3Ec__AnonStorey10D_1_U3CU3Em__196_m1240271766_gshared)(__this, ___val0, method)
