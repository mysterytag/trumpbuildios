﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SubstanceBase_2_Intrusion_gen3044853612MethodDeclarations.h"

// System.Void SubstanceBase`2/Intrusion<UniRx.InternalUtil.ImmutableList`1<System.Object>,System.Object>::.ctor()
#define Intrusion__ctor_m1438375424(__this, method) ((  void (*) (Intrusion_t1436088045 *, const MethodInfo*))Intrusion__ctor_m2864964220_gshared)(__this, method)
