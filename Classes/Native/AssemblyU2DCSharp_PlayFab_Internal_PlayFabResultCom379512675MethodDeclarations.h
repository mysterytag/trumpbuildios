﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.Internal.PlayFabResultCommon
struct PlayFabResultCommon_t379512675;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayFab.Internal.PlayFabResultCommon::.ctor()
extern "C"  void PlayFabResultCommon__ctor_m4291338422 (PlayFabResultCommon_t379512675 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
