﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.KeyedFactory`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct KeyedFactory_5_t1813071951;
// System.Type[]
struct TypeU5BU5D_t3431720054;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void Zenject.KeyedFactory`5<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C"  void KeyedFactory_5__ctor_m2126176791_gshared (KeyedFactory_5_t1813071951 * __this, const MethodInfo* method);
#define KeyedFactory_5__ctor_m2126176791(__this, method) ((  void (*) (KeyedFactory_5_t1813071951 *, const MethodInfo*))KeyedFactory_5__ctor_m2126176791_gshared)(__this, method)
// System.Type[] Zenject.KeyedFactory`5<System.Object,System.Object,System.Object,System.Object,System.Object>::get_ProvidedTypes()
extern "C"  TypeU5BU5D_t3431720054* KeyedFactory_5_get_ProvidedTypes_m505427610_gshared (KeyedFactory_5_t1813071951 * __this, const MethodInfo* method);
#define KeyedFactory_5_get_ProvidedTypes_m505427610(__this, method) ((  TypeU5BU5D_t3431720054* (*) (KeyedFactory_5_t1813071951 *, const MethodInfo*))KeyedFactory_5_get_ProvidedTypes_m505427610_gshared)(__this, method)
// TBase Zenject.KeyedFactory`5<System.Object,System.Object,System.Object,System.Object,System.Object>::Create(TKey,TParam1,TParam2,TParam3)
extern "C"  Il2CppObject * KeyedFactory_5_Create_m2159402249_gshared (KeyedFactory_5_t1813071951 * __this, Il2CppObject * ___key0, Il2CppObject * ___param11, Il2CppObject * ___param22, Il2CppObject * ___param33, const MethodInfo* method);
#define KeyedFactory_5_Create_m2159402249(__this, ___key0, ___param11, ___param22, ___param33, method) ((  Il2CppObject * (*) (KeyedFactory_5_t1813071951 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))KeyedFactory_5_Create_m2159402249_gshared)(__this, ___key0, ___param11, ___param22, ___param33, method)
