﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Subject`1/Subscription<UnityEngine.Color>
struct Subscription_t3662389578;
// UniRx.Subject`1<UnityEngine.Color>
struct Subject_1_t3526210380;
// UniRx.IObserver`1<UnityEngine.Color>
struct IObserver_1_t3800174663;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Subject`1/Subscription<UnityEngine.Color>::.ctor(UniRx.Subject`1<T>,UniRx.IObserver`1<T>)
extern "C"  void Subscription__ctor_m2202885929_gshared (Subscription_t3662389578 * __this, Subject_1_t3526210380 * ___parent0, Il2CppObject* ___unsubscribeTarget1, const MethodInfo* method);
#define Subscription__ctor_m2202885929(__this, ___parent0, ___unsubscribeTarget1, method) ((  void (*) (Subscription_t3662389578 *, Subject_1_t3526210380 *, Il2CppObject*, const MethodInfo*))Subscription__ctor_m2202885929_gshared)(__this, ___parent0, ___unsubscribeTarget1, method)
// System.Void UniRx.Subject`1/Subscription<UnityEngine.Color>::Dispose()
extern "C"  void Subscription_Dispose_m2434813229_gshared (Subscription_t3662389578 * __this, const MethodInfo* method);
#define Subscription_Dispose_m2434813229(__this, method) ((  void (*) (Subscription_t3662389578 *, const MethodInfo*))Subscription_Dispose_m2434813229_gshared)(__this, method)
