﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<System.DateTime>
struct Collection_1_t2308021666;
// System.Collections.Generic.IList`1<System.DateTime>
struct IList_1_t2505526250;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// System.DateTime[]
struct DateTimeU5BU5D_t2411579761;
// System.Collections.Generic.IEnumerator`1<System.DateTime>
struct IEnumerator_1_t1822140384;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_DateTime339033936.h"

// System.Void System.Collections.ObjectModel.Collection`1<System.DateTime>::.ctor()
extern "C"  void Collection_1__ctor_m3421142973_gshared (Collection_1_t2308021666 * __this, const MethodInfo* method);
#define Collection_1__ctor_m3421142973(__this, method) ((  void (*) (Collection_1_t2308021666 *, const MethodInfo*))Collection_1__ctor_m3421142973_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.DateTime>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1__ctor_m2388601208_gshared (Collection_1_t2308021666 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1__ctor_m2388601208(__this, ___list0, method) ((  void (*) (Collection_1_t2308021666 *, Il2CppObject*, const MethodInfo*))Collection_1__ctor_m2388601208_gshared)(__this, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.DateTime>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1075042974_gshared (Collection_1_t2308021666 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1075042974(__this, method) ((  bool (*) (Collection_1_t2308021666 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1075042974_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.DateTime>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m740659239_gshared (Collection_1_t2308021666 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m740659239(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t2308021666 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m740659239_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.DateTime>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m948584418_gshared (Collection_1_t2308021666 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m948584418(__this, method) ((  Il2CppObject * (*) (Collection_1_t2308021666 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m948584418_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.DateTime>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m864255311_gshared (Collection_1_t2308021666 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m864255311(__this, ___value0, method) ((  int32_t (*) (Collection_1_t2308021666 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m864255311_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.DateTime>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m1532107485_gshared (Collection_1_t2308021666 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m1532107485(__this, ___value0, method) ((  bool (*) (Collection_1_t2308021666 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m1532107485_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.DateTime>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m1326871591_gshared (Collection_1_t2308021666 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m1326871591(__this, ___value0, method) ((  int32_t (*) (Collection_1_t2308021666 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m1326871591_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.DateTime>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m1464055762_gshared (Collection_1_t2308021666 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m1464055762(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2308021666 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m1464055762_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.DateTime>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m1032299542_gshared (Collection_1_t2308021666 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m1032299542(__this, ___value0, method) ((  void (*) (Collection_1_t2308021666 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m1032299542_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.DateTime>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m391607135_gshared (Collection_1_t2308021666 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m391607135(__this, method) ((  bool (*) (Collection_1_t2308021666 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m391607135_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.DateTime>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m3488399627_gshared (Collection_1_t2308021666 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m3488399627(__this, method) ((  Il2CppObject * (*) (Collection_1_t2308021666 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m3488399627_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.DateTime>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m1548383244_gshared (Collection_1_t2308021666 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m1548383244(__this, method) ((  bool (*) (Collection_1_t2308021666 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m1548383244_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.DateTime>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m1750882669_gshared (Collection_1_t2308021666 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m1750882669(__this, method) ((  bool (*) (Collection_1_t2308021666 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m1750882669_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.DateTime>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m3343162322_gshared (Collection_1_t2308021666 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m3343162322(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t2308021666 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m3343162322_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.DateTime>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m3117233961_gshared (Collection_1_t2308021666 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m3117233961(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2308021666 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m3117233961_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.DateTime>::Add(T)
extern "C"  void Collection_1_Add_m3425637666_gshared (Collection_1_t2308021666 * __this, DateTime_t339033936  ___item0, const MethodInfo* method);
#define Collection_1_Add_m3425637666(__this, ___item0, method) ((  void (*) (Collection_1_t2308021666 *, DateTime_t339033936 , const MethodInfo*))Collection_1_Add_m3425637666_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.DateTime>::Clear()
extern "C"  void Collection_1_Clear_m827276264_gshared (Collection_1_t2308021666 * __this, const MethodInfo* method);
#define Collection_1_Clear_m827276264(__this, method) ((  void (*) (Collection_1_t2308021666 *, const MethodInfo*))Collection_1_Clear_m827276264_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.DateTime>::ClearItems()
extern "C"  void Collection_1_ClearItems_m3645740570_gshared (Collection_1_t2308021666 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m3645740570(__this, method) ((  void (*) (Collection_1_t2308021666 *, const MethodInfo*))Collection_1_ClearItems_m3645740570_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.DateTime>::Contains(T)
extern "C"  bool Collection_1_Contains_m3863762966_gshared (Collection_1_t2308021666 * __this, DateTime_t339033936  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m3863762966(__this, ___item0, method) ((  bool (*) (Collection_1_t2308021666 *, DateTime_t339033936 , const MethodInfo*))Collection_1_Contains_m3863762966_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.DateTime>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m3594172562_gshared (Collection_1_t2308021666 * __this, DateTimeU5BU5D_t2411579761* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m3594172562(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t2308021666 *, DateTimeU5BU5D_t2411579761*, int32_t, const MethodInfo*))Collection_1_CopyTo_m3594172562_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.DateTime>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m2683679353_gshared (Collection_1_t2308021666 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m2683679353(__this, method) ((  Il2CppObject* (*) (Collection_1_t2308021666 *, const MethodInfo*))Collection_1_GetEnumerator_m2683679353_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.DateTime>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m2547155862_gshared (Collection_1_t2308021666 * __this, DateTime_t339033936  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m2547155862(__this, ___item0, method) ((  int32_t (*) (Collection_1_t2308021666 *, DateTime_t339033936 , const MethodInfo*))Collection_1_IndexOf_m2547155862_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.DateTime>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m1911671689_gshared (Collection_1_t2308021666 * __this, int32_t ___index0, DateTime_t339033936  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m1911671689(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2308021666 *, int32_t, DateTime_t339033936 , const MethodInfo*))Collection_1_Insert_m1911671689_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.DateTime>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m2807434556_gshared (Collection_1_t2308021666 * __this, int32_t ___index0, DateTime_t339033936  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m2807434556(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2308021666 *, int32_t, DateTime_t339033936 , const MethodInfo*))Collection_1_InsertItem_m2807434556_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.DateTime>::Remove(T)
extern "C"  bool Collection_1_Remove_m633611665_gshared (Collection_1_t2308021666 * __this, DateTime_t339033936  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m633611665(__this, ___item0, method) ((  bool (*) (Collection_1_t2308021666 *, DateTime_t339033936 , const MethodInfo*))Collection_1_Remove_m633611665_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.DateTime>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m4080491855_gshared (Collection_1_t2308021666 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m4080491855(__this, ___index0, method) ((  void (*) (Collection_1_t2308021666 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m4080491855_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.DateTime>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m2947156399_gshared (Collection_1_t2308021666 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m2947156399(__this, ___index0, method) ((  void (*) (Collection_1_t2308021666 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m2947156399_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.DateTime>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m1469727909_gshared (Collection_1_t2308021666 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m1469727909(__this, method) ((  int32_t (*) (Collection_1_t2308021666 *, const MethodInfo*))Collection_1_get_Count_m1469727909_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.DateTime>::get_Item(System.Int32)
extern "C"  DateTime_t339033936  Collection_1_get_Item_m955580499_gshared (Collection_1_t2308021666 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m955580499(__this, ___index0, method) ((  DateTime_t339033936  (*) (Collection_1_t2308021666 *, int32_t, const MethodInfo*))Collection_1_get_Item_m955580499_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.DateTime>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m680087072_gshared (Collection_1_t2308021666 * __this, int32_t ___index0, DateTime_t339033936  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m680087072(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t2308021666 *, int32_t, DateTime_t339033936 , const MethodInfo*))Collection_1_set_Item_m680087072_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.DateTime>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m448869977_gshared (Collection_1_t2308021666 * __this, int32_t ___index0, DateTime_t339033936  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m448869977(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t2308021666 *, int32_t, DateTime_t339033936 , const MethodInfo*))Collection_1_SetItem_m448869977_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.DateTime>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m1916939094_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m1916939094(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m1916939094_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<System.DateTime>::ConvertItem(System.Object)
extern "C"  DateTime_t339033936  Collection_1_ConvertItem_m239095218_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m239095218(__this /* static, unused */, ___item0, method) ((  DateTime_t339033936  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m239095218_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.DateTime>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m849548114_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m849548114(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m849548114_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.DateTime>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m3189387694_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m3189387694(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m3189387694_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.DateTime>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m3587384241_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m3587384241(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m3587384241_gshared)(__this /* static, unused */, ___list0, method)
