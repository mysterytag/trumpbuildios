﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ThrottleFirstFrameObservable`1/ThrottleFirstFrame<System.Object>
struct ThrottleFirstFrame_t517244520;
// UniRx.Operators.ThrottleFirstFrameObservable`1<System.Object>
struct ThrottleFirstFrameObservable_1_t955022977;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.ThrottleFirstFrameObservable`1/ThrottleFirstFrame<System.Object>::.ctor(UniRx.Operators.ThrottleFirstFrameObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void ThrottleFirstFrame__ctor_m2720474635_gshared (ThrottleFirstFrame_t517244520 * __this, ThrottleFirstFrameObservable_1_t955022977 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define ThrottleFirstFrame__ctor_m2720474635(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (ThrottleFirstFrame_t517244520 *, ThrottleFirstFrameObservable_1_t955022977 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))ThrottleFirstFrame__ctor_m2720474635_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.IDisposable UniRx.Operators.ThrottleFirstFrameObservable`1/ThrottleFirstFrame<System.Object>::Run()
extern "C"  Il2CppObject * ThrottleFirstFrame_Run_m1300998053_gshared (ThrottleFirstFrame_t517244520 * __this, const MethodInfo* method);
#define ThrottleFirstFrame_Run_m1300998053(__this, method) ((  Il2CppObject * (*) (ThrottleFirstFrame_t517244520 *, const MethodInfo*))ThrottleFirstFrame_Run_m1300998053_gshared)(__this, method)
// System.Void UniRx.Operators.ThrottleFirstFrameObservable`1/ThrottleFirstFrame<System.Object>::OnNext()
extern "C"  void ThrottleFirstFrame_OnNext_m3030201599_gshared (ThrottleFirstFrame_t517244520 * __this, const MethodInfo* method);
#define ThrottleFirstFrame_OnNext_m3030201599(__this, method) ((  void (*) (ThrottleFirstFrame_t517244520 *, const MethodInfo*))ThrottleFirstFrame_OnNext_m3030201599_gshared)(__this, method)
// System.Void UniRx.Operators.ThrottleFirstFrameObservable`1/ThrottleFirstFrame<System.Object>::OnNext(T)
extern "C"  void ThrottleFirstFrame_OnNext_m3741937727_gshared (ThrottleFirstFrame_t517244520 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ThrottleFirstFrame_OnNext_m3741937727(__this, ___value0, method) ((  void (*) (ThrottleFirstFrame_t517244520 *, Il2CppObject *, const MethodInfo*))ThrottleFirstFrame_OnNext_m3741937727_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.ThrottleFirstFrameObservable`1/ThrottleFirstFrame<System.Object>::OnError(System.Exception)
extern "C"  void ThrottleFirstFrame_OnError_m3275435342_gshared (ThrottleFirstFrame_t517244520 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ThrottleFirstFrame_OnError_m3275435342(__this, ___error0, method) ((  void (*) (ThrottleFirstFrame_t517244520 *, Exception_t1967233988 *, const MethodInfo*))ThrottleFirstFrame_OnError_m3275435342_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.ThrottleFirstFrameObservable`1/ThrottleFirstFrame<System.Object>::OnCompleted()
extern "C"  void ThrottleFirstFrame_OnCompleted_m3042502049_gshared (ThrottleFirstFrame_t517244520 * __this, const MethodInfo* method);
#define ThrottleFirstFrame_OnCompleted_m3042502049(__this, method) ((  void (*) (ThrottleFirstFrame_t517244520 *, const MethodInfo*))ThrottleFirstFrame_OnCompleted_m3042502049_gshared)(__this, method)
