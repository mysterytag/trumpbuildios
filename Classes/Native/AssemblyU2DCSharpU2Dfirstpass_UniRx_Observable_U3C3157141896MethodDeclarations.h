﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Observable/<FromAsyncPattern>c__AnonStorey43`3<System.Object,System.Object,UniRx.Unit>
struct U3CFromAsyncPatternU3Ec__AnonStorey43_3_t3157141896;
// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t2317084402;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.Observable/<FromAsyncPattern>c__AnonStorey43`3<System.Object,System.Object,UniRx.Unit>::.ctor()
extern "C"  void U3CFromAsyncPatternU3Ec__AnonStorey43_3__ctor_m2972212196_gshared (U3CFromAsyncPatternU3Ec__AnonStorey43_3_t3157141896 * __this, const MethodInfo* method);
#define U3CFromAsyncPatternU3Ec__AnonStorey43_3__ctor_m2972212196(__this, method) ((  void (*) (U3CFromAsyncPatternU3Ec__AnonStorey43_3_t3157141896 *, const MethodInfo*))U3CFromAsyncPatternU3Ec__AnonStorey43_3__ctor_m2972212196_gshared)(__this, method)
// UniRx.IObservable`1<TResult> UniRx.Observable/<FromAsyncPattern>c__AnonStorey43`3<System.Object,System.Object,UniRx.Unit>::<>m__42(T1,T2)
extern "C"  Il2CppObject* U3CFromAsyncPatternU3Ec__AnonStorey43_3_U3CU3Em__42_m4054626683_gshared (U3CFromAsyncPatternU3Ec__AnonStorey43_3_t3157141896 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define U3CFromAsyncPatternU3Ec__AnonStorey43_3_U3CU3Em__42_m4054626683(__this, ___x0, ___y1, method) ((  Il2CppObject* (*) (U3CFromAsyncPatternU3Ec__AnonStorey43_3_t3157141896 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))U3CFromAsyncPatternU3Ec__AnonStorey43_3_U3CU3Em__42_m4054626683_gshared)(__this, ___x0, ___y1, method)
