﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// <>__AnonType0`4<System.Boolean,System.Boolean,System.Boolean,System.Boolean>
struct U3CU3E__AnonType0_4_t733143067;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void <>__AnonType0`4<System.Boolean,System.Boolean,System.Boolean,System.Boolean>::.ctor(<show>__T,<tutor>__T,<bottom>__T,<hidden>__T)
extern "C"  void U3CU3E__AnonType0_4__ctor_m352903163_gshared (U3CU3E__AnonType0_4_t733143067 * __this, bool ___show0, bool ___tutor1, bool ___bottom2, bool ___hidden3, const MethodInfo* method);
#define U3CU3E__AnonType0_4__ctor_m352903163(__this, ___show0, ___tutor1, ___bottom2, ___hidden3, method) ((  void (*) (U3CU3E__AnonType0_4_t733143067 *, bool, bool, bool, bool, const MethodInfo*))U3CU3E__AnonType0_4__ctor_m352903163_gshared)(__this, ___show0, ___tutor1, ___bottom2, ___hidden3, method)
// <show>__T <>__AnonType0`4<System.Boolean,System.Boolean,System.Boolean,System.Boolean>::get_show()
extern "C"  bool U3CU3E__AnonType0_4_get_show_m1146603045_gshared (U3CU3E__AnonType0_4_t733143067 * __this, const MethodInfo* method);
#define U3CU3E__AnonType0_4_get_show_m1146603045(__this, method) ((  bool (*) (U3CU3E__AnonType0_4_t733143067 *, const MethodInfo*))U3CU3E__AnonType0_4_get_show_m1146603045_gshared)(__this, method)
// <tutor>__T <>__AnonType0`4<System.Boolean,System.Boolean,System.Boolean,System.Boolean>::get_tutor()
extern "C"  bool U3CU3E__AnonType0_4_get_tutor_m1418476487_gshared (U3CU3E__AnonType0_4_t733143067 * __this, const MethodInfo* method);
#define U3CU3E__AnonType0_4_get_tutor_m1418476487(__this, method) ((  bool (*) (U3CU3E__AnonType0_4_t733143067 *, const MethodInfo*))U3CU3E__AnonType0_4_get_tutor_m1418476487_gshared)(__this, method)
// <bottom>__T <>__AnonType0`4<System.Boolean,System.Boolean,System.Boolean,System.Boolean>::get_bottom()
extern "C"  bool U3CU3E__AnonType0_4_get_bottom_m3328873317_gshared (U3CU3E__AnonType0_4_t733143067 * __this, const MethodInfo* method);
#define U3CU3E__AnonType0_4_get_bottom_m3328873317(__this, method) ((  bool (*) (U3CU3E__AnonType0_4_t733143067 *, const MethodInfo*))U3CU3E__AnonType0_4_get_bottom_m3328873317_gshared)(__this, method)
// <hidden>__T <>__AnonType0`4<System.Boolean,System.Boolean,System.Boolean,System.Boolean>::get_hidden()
extern "C"  bool U3CU3E__AnonType0_4_get_hidden_m1137801157_gshared (U3CU3E__AnonType0_4_t733143067 * __this, const MethodInfo* method);
#define U3CU3E__AnonType0_4_get_hidden_m1137801157(__this, method) ((  bool (*) (U3CU3E__AnonType0_4_t733143067 *, const MethodInfo*))U3CU3E__AnonType0_4_get_hidden_m1137801157_gshared)(__this, method)
// System.Boolean <>__AnonType0`4<System.Boolean,System.Boolean,System.Boolean,System.Boolean>::Equals(System.Object)
extern "C"  bool U3CU3E__AnonType0_4_Equals_m2711421094_gshared (U3CU3E__AnonType0_4_t733143067 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define U3CU3E__AnonType0_4_Equals_m2711421094(__this, ___obj0, method) ((  bool (*) (U3CU3E__AnonType0_4_t733143067 *, Il2CppObject *, const MethodInfo*))U3CU3E__AnonType0_4_Equals_m2711421094_gshared)(__this, ___obj0, method)
// System.Int32 <>__AnonType0`4<System.Boolean,System.Boolean,System.Boolean,System.Boolean>::GetHashCode()
extern "C"  int32_t U3CU3E__AnonType0_4_GetHashCode_m1145406666_gshared (U3CU3E__AnonType0_4_t733143067 * __this, const MethodInfo* method);
#define U3CU3E__AnonType0_4_GetHashCode_m1145406666(__this, method) ((  int32_t (*) (U3CU3E__AnonType0_4_t733143067 *, const MethodInfo*))U3CU3E__AnonType0_4_GetHashCode_m1145406666_gshared)(__this, method)
// System.String <>__AnonType0`4<System.Boolean,System.Boolean,System.Boolean,System.Boolean>::ToString()
extern "C"  String_t* U3CU3E__AnonType0_4_ToString_m2444216842_gshared (U3CU3E__AnonType0_4_t733143067 * __this, const MethodInfo* method);
#define U3CU3E__AnonType0_4_ToString_m2444216842(__this, method) ((  String_t* (*) (U3CU3E__AnonType0_4_t733143067 *, const MethodInfo*))U3CU3E__AnonType0_4_ToString_m2444216842_gshared)(__this, method)
