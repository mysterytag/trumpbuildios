﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/UnlockContainerItemResponseCallback
struct UnlockContainerItemResponseCallback_t3474114038;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.UnlockContainerItemRequest
struct UnlockContainerItemRequest_t987080271;
// PlayFab.ClientModels.UnlockContainerItemResult
struct UnlockContainerItemResult_t2837935741;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlockContai987080271.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UnlockConta2837935741.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/UnlockContainerItemResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void UnlockContainerItemResponseCallback__ctor_m1157703557 (UnlockContainerItemResponseCallback_t3474114038 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UnlockContainerItemResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.UnlockContainerItemRequest,PlayFab.ClientModels.UnlockContainerItemResult,PlayFab.PlayFabError,System.Object)
extern "C"  void UnlockContainerItemResponseCallback_Invoke_m3007377040 (UnlockContainerItemResponseCallback_t3474114038 * __this, String_t* ___urlPath0, int32_t ___callId1, UnlockContainerItemRequest_t987080271 * ___request2, UnlockContainerItemResult_t2837935741 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_UnlockContainerItemResponseCallback_t3474114038(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, UnlockContainerItemRequest_t987080271 * ___request2, UnlockContainerItemResult_t2837935741 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/UnlockContainerItemResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.UnlockContainerItemRequest,PlayFab.ClientModels.UnlockContainerItemResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnlockContainerItemResponseCallback_BeginInvoke_m4225914569 (UnlockContainerItemResponseCallback_t3474114038 * __this, String_t* ___urlPath0, int32_t ___callId1, UnlockContainerItemRequest_t987080271 * ___request2, UnlockContainerItemResult_t2837935741 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UnlockContainerItemResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void UnlockContainerItemResponseCallback_EndInvoke_m171385877 (UnlockContainerItemResponseCallback_t3474114038 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
