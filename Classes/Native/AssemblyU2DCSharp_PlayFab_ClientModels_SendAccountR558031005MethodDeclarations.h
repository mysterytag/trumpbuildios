﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.SendAccountRecoveryEmailRequest
struct SendAccountRecoveryEmailRequest_t558031005;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.SendAccountRecoveryEmailRequest::.ctor()
extern "C"  void SendAccountRecoveryEmailRequest__ctor_m1001540524 (SendAccountRecoveryEmailRequest_t558031005 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.SendAccountRecoveryEmailRequest::get_Email()
extern "C"  String_t* SendAccountRecoveryEmailRequest_get_Email_m959843130 (SendAccountRecoveryEmailRequest_t558031005 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.SendAccountRecoveryEmailRequest::set_Email(System.String)
extern "C"  void SendAccountRecoveryEmailRequest_set_Email_m3906513817 (SendAccountRecoveryEmailRequest_t558031005 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.SendAccountRecoveryEmailRequest::get_TitleId()
extern "C"  String_t* SendAccountRecoveryEmailRequest_get_TitleId_m3326091313 (SendAccountRecoveryEmailRequest_t558031005 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.SendAccountRecoveryEmailRequest::set_TitleId(System.String)
extern "C"  void SendAccountRecoveryEmailRequest_set_TitleId_m2853208002 (SendAccountRecoveryEmailRequest_t558031005 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.SendAccountRecoveryEmailRequest::get_PublisherId()
extern "C"  String_t* SendAccountRecoveryEmailRequest_get_PublisherId_m837859157 (SendAccountRecoveryEmailRequest_t558031005 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.SendAccountRecoveryEmailRequest::set_PublisherId(System.String)
extern "C"  void SendAccountRecoveryEmailRequest_set_PublisherId_m571904670 (SendAccountRecoveryEmailRequest_t558031005 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
