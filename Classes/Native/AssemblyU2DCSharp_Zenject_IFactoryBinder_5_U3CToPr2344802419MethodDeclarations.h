﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.IFactoryBinder`5/<ToPrefab>c__AnonStorey17D<System.Object,System.Object,System.Object,System.Object,System.Object>
struct U3CToPrefabU3Ec__AnonStorey17D_t2344802419;
// Zenject.IFactory`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct IFactory_5_t363284204;
// Zenject.InjectContext
struct InjectContext_t3456483891;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_InjectContext3456483891.h"

// System.Void Zenject.IFactoryBinder`5/<ToPrefab>c__AnonStorey17D<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C"  void U3CToPrefabU3Ec__AnonStorey17D__ctor_m1582067505_gshared (U3CToPrefabU3Ec__AnonStorey17D_t2344802419 * __this, const MethodInfo* method);
#define U3CToPrefabU3Ec__AnonStorey17D__ctor_m1582067505(__this, method) ((  void (*) (U3CToPrefabU3Ec__AnonStorey17D_t2344802419 *, const MethodInfo*))U3CToPrefabU3Ec__AnonStorey17D__ctor_m1582067505_gshared)(__this, method)
// Zenject.IFactory`5<TParam1,TParam2,TParam3,TParam4,TContract> Zenject.IFactoryBinder`5/<ToPrefab>c__AnonStorey17D<System.Object,System.Object,System.Object,System.Object,System.Object>::<>m__2A7(Zenject.InjectContext)
extern "C"  Il2CppObject* U3CToPrefabU3Ec__AnonStorey17D_U3CU3Em__2A7_m3427982425_gshared (U3CToPrefabU3Ec__AnonStorey17D_t2344802419 * __this, InjectContext_t3456483891 * ___ctx0, const MethodInfo* method);
#define U3CToPrefabU3Ec__AnonStorey17D_U3CU3Em__2A7_m3427982425(__this, ___ctx0, method) ((  Il2CppObject* (*) (U3CToPrefabU3Ec__AnonStorey17D_t2344802419 *, InjectContext_t3456483891 *, const MethodInfo*))U3CToPrefabU3Ec__AnonStorey17D_U3CU3Em__2A7_m3427982425_gshared)(__this, ___ctx0, method)
