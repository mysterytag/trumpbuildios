﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.InjectOptionalAttribute
struct InjectOptionalAttribute_t899944672;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void Zenject.InjectOptionalAttribute::.ctor(System.String)
extern "C"  void InjectOptionalAttribute__ctor_m2673839331 (InjectOptionalAttribute_t899944672 * __this, String_t* ___identifier0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.InjectOptionalAttribute::.ctor()
extern "C"  void InjectOptionalAttribute__ctor_m2498762879 (InjectOptionalAttribute_t899944672 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Zenject.InjectOptionalAttribute::get_Identifier()
extern "C"  String_t* InjectOptionalAttribute_get_Identifier_m2651514068 (InjectOptionalAttribute_t899944672 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zenject.InjectOptionalAttribute::set_Identifier(System.String)
extern "C"  void InjectOptionalAttribute_set_Identifier_m1304926679 (InjectOptionalAttribute_t899944672 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
