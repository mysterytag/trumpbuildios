﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ZipObservable`4<System.Object,System.Object,System.Object,System.Object>
struct ZipObservable_4_t637587494;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// UniRx.Operators.ZipFunc`4<System.Object,System.Object,System.Object,System.Object>
struct ZipFunc_4_t799882833;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.ZipObservable`4<System.Object,System.Object,System.Object,System.Object>::.ctor(UniRx.IObservable`1<T1>,UniRx.IObservable`1<T2>,UniRx.IObservable`1<T3>,UniRx.Operators.ZipFunc`4<T1,T2,T3,TR>)
extern "C"  void ZipObservable_4__ctor_m4094438869_gshared (ZipObservable_4_t637587494 * __this, Il2CppObject* ___source10, Il2CppObject* ___source21, Il2CppObject* ___source32, ZipFunc_4_t799882833 * ___resultSelector3, const MethodInfo* method);
#define ZipObservable_4__ctor_m4094438869(__this, ___source10, ___source21, ___source32, ___resultSelector3, method) ((  void (*) (ZipObservable_4_t637587494 *, Il2CppObject*, Il2CppObject*, Il2CppObject*, ZipFunc_4_t799882833 *, const MethodInfo*))ZipObservable_4__ctor_m4094438869_gshared)(__this, ___source10, ___source21, ___source32, ___resultSelector3, method)
// System.IDisposable UniRx.Operators.ZipObservable`4<System.Object,System.Object,System.Object,System.Object>::SubscribeCore(UniRx.IObserver`1<TR>,System.IDisposable)
extern "C"  Il2CppObject * ZipObservable_4_SubscribeCore_m1083763548_gshared (ZipObservable_4_t637587494 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define ZipObservable_4_SubscribeCore_m1083763548(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (ZipObservable_4_t637587494 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))ZipObservable_4_SubscribeCore_m1083763548_gshared)(__this, ___observer0, ___cancel1, method)
