﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/UpdateCharacterStatisticsResponseCallback
struct UpdateCharacterStatisticsResponseCallback_t2257981257;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PlayFab.ClientModels.UpdateCharacterStatisticsRequest
struct UpdateCharacterStatisticsRequest_t1560413084;
// PlayFab.ClientModels.UpdateCharacterStatisticsResult
struct UpdateCharacterStatisticsResult_t2994977680;
// PlayFab.PlayFabError
struct PlayFabError_t750598646;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UpdateChara1560413084.h"
#include "AssemblyU2DCSharp_PlayFab_ClientModels_UpdateChara2994977680.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_PlayFabError750598646.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void PlayFab.PlayFabClientAPI/UpdateCharacterStatisticsResponseCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void UpdateCharacterStatisticsResponseCallback__ctor_m3266193240 (UpdateCharacterStatisticsResponseCallback_t2257981257 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UpdateCharacterStatisticsResponseCallback::Invoke(System.String,System.Int32,PlayFab.ClientModels.UpdateCharacterStatisticsRequest,PlayFab.ClientModels.UpdateCharacterStatisticsResult,PlayFab.PlayFabError,System.Object)
extern "C"  void UpdateCharacterStatisticsResponseCallback_Invoke_m1075318601 (UpdateCharacterStatisticsResponseCallback_t2257981257 * __this, String_t* ___urlPath0, int32_t ___callId1, UpdateCharacterStatisticsRequest_t1560413084 * ___request2, UpdateCharacterStatisticsResult_t2994977680 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_UpdateCharacterStatisticsResponseCallback_t2257981257(Il2CppObject* delegate, String_t* ___urlPath0, int32_t ___callId1, UpdateCharacterStatisticsRequest_t1560413084 * ___request2, UpdateCharacterStatisticsResult_t2994977680 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5);
// System.IAsyncResult PlayFab.PlayFabClientAPI/UpdateCharacterStatisticsResponseCallback::BeginInvoke(System.String,System.Int32,PlayFab.ClientModels.UpdateCharacterStatisticsRequest,PlayFab.ClientModels.UpdateCharacterStatisticsResult,PlayFab.PlayFabError,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UpdateCharacterStatisticsResponseCallback_BeginInvoke_m1623495926 (UpdateCharacterStatisticsResponseCallback_t2257981257 * __this, String_t* ___urlPath0, int32_t ___callId1, UpdateCharacterStatisticsRequest_t1560413084 * ___request2, UpdateCharacterStatisticsResult_t2994977680 * ___result3, PlayFabError_t750598646 * ___error4, Il2CppObject * ___customData5, AsyncCallback_t1363551830 * ___callback6, Il2CppObject * ___object7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/UpdateCharacterStatisticsResponseCallback::EndInvoke(System.IAsyncResult)
extern "C"  void UpdateCharacterStatisticsResponseCallback_EndInvoke_m1378551144 (UpdateCharacterStatisticsResponseCallback_t2257981257 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
