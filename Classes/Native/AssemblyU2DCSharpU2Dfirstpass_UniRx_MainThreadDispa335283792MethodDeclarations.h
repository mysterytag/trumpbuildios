﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.MainThreadDispatcher/<SendStartCoroutine>c__AnonStorey7D
struct U3CSendStartCoroutineU3Ec__AnonStorey7D_t335283792;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void UniRx.MainThreadDispatcher/<SendStartCoroutine>c__AnonStorey7D::.ctor()
extern "C"  void U3CSendStartCoroutineU3Ec__AnonStorey7D__ctor_m252643564 (U3CSendStartCoroutineU3Ec__AnonStorey7D_t335283792 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UniRx.MainThreadDispatcher/<SendStartCoroutine>c__AnonStorey7D::<>m__A9(System.Object)
extern "C"  void U3CSendStartCoroutineU3Ec__AnonStorey7D_U3CU3Em__A9_m1760013287 (U3CSendStartCoroutineU3Ec__AnonStorey7D_t335283792 * __this, Il2CppObject * ____0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
