﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.IDisposable
struct IDisposable_t1628921374;
// System.Action
struct Action_t437523947;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action437523947.h"

// System.Void UniRx.Disposable::.cctor()
extern "C"  void Disposable__cctor_m2169092470 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IDisposable UniRx.Disposable::Create(System.Action)
extern "C"  Il2CppObject * Disposable_Create_m2910846009 (Il2CppObject * __this /* static, unused */, Action_t437523947 * ___disposeAction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
