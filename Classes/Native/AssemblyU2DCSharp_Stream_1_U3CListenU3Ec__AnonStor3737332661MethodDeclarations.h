﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Stream`1/<Listen>c__AnonStorey128<System.Boolean>
struct U3CListenU3Ec__AnonStorey128_t3737332661;

#include "codegen/il2cpp-codegen.h"

// System.Void Stream`1/<Listen>c__AnonStorey128<System.Boolean>::.ctor()
extern "C"  void U3CListenU3Ec__AnonStorey128__ctor_m3851394505_gshared (U3CListenU3Ec__AnonStorey128_t3737332661 * __this, const MethodInfo* method);
#define U3CListenU3Ec__AnonStorey128__ctor_m3851394505(__this, method) ((  void (*) (U3CListenU3Ec__AnonStorey128_t3737332661 *, const MethodInfo*))U3CListenU3Ec__AnonStorey128__ctor_m3851394505_gshared)(__this, method)
// System.Void Stream`1/<Listen>c__AnonStorey128<System.Boolean>::<>m__1B9(T)
extern "C"  void U3CListenU3Ec__AnonStorey128_U3CU3Em__1B9_m2738631782_gshared (U3CListenU3Ec__AnonStorey128_t3737332661 * __this, bool ____0, const MethodInfo* method);
#define U3CListenU3Ec__AnonStorey128_U3CU3Em__1B9_m2738631782(__this, ____0, method) ((  void (*) (U3CListenU3Ec__AnonStorey128_t3737332661 *, bool, const MethodInfo*))U3CListenU3Ec__AnonStorey128_U3CU3Em__1B9_m2738631782_gshared)(__this, ____0, method)
