﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zenject.SingletonProviderMap/<AddCreatorFromMethod>c__AnonStorey197`1<System.Object>
struct U3CAddCreatorFromMethodU3Ec__AnonStorey197_1_t3364390262;
// System.Object
struct Il2CppObject;
// Zenject.InjectContext
struct InjectContext_t3456483891;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Zenject_InjectContext3456483891.h"

// System.Void Zenject.SingletonProviderMap/<AddCreatorFromMethod>c__AnonStorey197`1<System.Object>::.ctor()
extern "C"  void U3CAddCreatorFromMethodU3Ec__AnonStorey197_1__ctor_m2199910836_gshared (U3CAddCreatorFromMethodU3Ec__AnonStorey197_1_t3364390262 * __this, const MethodInfo* method);
#define U3CAddCreatorFromMethodU3Ec__AnonStorey197_1__ctor_m2199910836(__this, method) ((  void (*) (U3CAddCreatorFromMethodU3Ec__AnonStorey197_1_t3364390262 *, const MethodInfo*))U3CAddCreatorFromMethodU3Ec__AnonStorey197_1__ctor_m2199910836_gshared)(__this, method)
// System.Object Zenject.SingletonProviderMap/<AddCreatorFromMethod>c__AnonStorey197`1<System.Object>::<>m__307(Zenject.InjectContext)
extern "C"  Il2CppObject * U3CAddCreatorFromMethodU3Ec__AnonStorey197_1_U3CU3Em__307_m1180757087_gshared (U3CAddCreatorFromMethodU3Ec__AnonStorey197_1_t3364390262 * __this, InjectContext_t3456483891 * ___context0, const MethodInfo* method);
#define U3CAddCreatorFromMethodU3Ec__AnonStorey197_1_U3CU3Em__307_m1180757087(__this, ___context0, method) ((  Il2CppObject * (*) (U3CAddCreatorFromMethodU3Ec__AnonStorey197_1_t3364390262 *, InjectContext_t3456483891 *, const MethodInfo*))U3CAddCreatorFromMethodU3Ec__AnonStorey197_1_U3CU3Em__307_m1180757087_gshared)(__this, ___context0, method)
