﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Digital
struct Digital_t3325134328;

#include "codegen/il2cpp-codegen.h"

// System.Void Digital::.ctor()
extern "C"  void Digital__ctor_m570613875 (Digital_t3325134328 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Digital::Start()
extern "C"  void Digital_Start_m3812718963 (Digital_t3325134328 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Digital::Update()
extern "C"  void Digital_Update_m2236023034 (Digital_t3325134328 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
