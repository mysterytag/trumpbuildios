﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<UnlinkXboxAccount>c__AnonStorey89
struct U3CUnlinkXboxAccountU3Ec__AnonStorey89_t3629870133;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<UnlinkXboxAccount>c__AnonStorey89::.ctor()
extern "C"  void U3CUnlinkXboxAccountU3Ec__AnonStorey89__ctor_m690596542 (U3CUnlinkXboxAccountU3Ec__AnonStorey89_t3629870133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<UnlinkXboxAccount>c__AnonStorey89::<>m__76(PlayFab.CallRequestContainer)
extern "C"  void U3CUnlinkXboxAccountU3Ec__AnonStorey89_U3CU3Em__76_m1440468923 (U3CUnlinkXboxAccountU3Ec__AnonStorey89_t3629870133 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
