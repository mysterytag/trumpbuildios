﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.ReturnObservable`1/Return<System.Int32>
struct Return_t624058200;
// UniRx.IObserver`1<System.Int32>
struct IObserver_1_t764446394;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.ReturnObservable`1/Return<System.Int32>::.ctor(UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Return__ctor_m2470295850_gshared (Return_t624058200 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define Return__ctor_m2470295850(__this, ___observer0, ___cancel1, method) ((  void (*) (Return_t624058200 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Return__ctor_m2470295850_gshared)(__this, ___observer0, ___cancel1, method)
// System.Void UniRx.Operators.ReturnObservable`1/Return<System.Int32>::OnNext(T)
extern "C"  void Return_OnNext_m530037800_gshared (Return_t624058200 * __this, int32_t ___value0, const MethodInfo* method);
#define Return_OnNext_m530037800(__this, ___value0, method) ((  void (*) (Return_t624058200 *, int32_t, const MethodInfo*))Return_OnNext_m530037800_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.ReturnObservable`1/Return<System.Int32>::OnError(System.Exception)
extern "C"  void Return_OnError_m1450084151_gshared (Return_t624058200 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Return_OnError_m1450084151(__this, ___error0, method) ((  void (*) (Return_t624058200 *, Exception_t1967233988 *, const MethodInfo*))Return_OnError_m1450084151_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.ReturnObservable`1/Return<System.Int32>::OnCompleted()
extern "C"  void Return_OnCompleted_m748492042_gshared (Return_t624058200 * __this, const MethodInfo* method);
#define Return_OnCompleted_m748492042(__this, method) ((  void (*) (Return_t624058200 *, const MethodInfo*))Return_OnCompleted_m748492042_gshared)(__this, method)
