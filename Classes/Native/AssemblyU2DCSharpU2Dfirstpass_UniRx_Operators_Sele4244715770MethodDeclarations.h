﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.SelectObservable`2<System.Object,System.Boolean>
struct SelectObservable_2_t4244715770;
// UniRx.IObservable`1<System.Object>
struct IObservable_1_t595904784;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t1509682273;
// System.Func`3<System.Object,System.Int32,System.Boolean>
struct Func_3_t3064567499;
// System.IDisposable
struct IDisposable_t1628921374;
// UniRx.IObserver`1<System.Boolean>
struct IObserver_1_t2423004244;

#include "codegen/il2cpp-codegen.h"

// System.Void UniRx.Operators.SelectObservable`2<System.Object,System.Boolean>::.ctor(UniRx.IObservable`1<T>,System.Func`2<T,TR>)
extern "C"  void SelectObservable_2__ctor_m1636483622_gshared (SelectObservable_2_t4244715770 * __this, Il2CppObject* ___source0, Func_2_t1509682273 * ___selector1, const MethodInfo* method);
#define SelectObservable_2__ctor_m1636483622(__this, ___source0, ___selector1, method) ((  void (*) (SelectObservable_2_t4244715770 *, Il2CppObject*, Func_2_t1509682273 *, const MethodInfo*))SelectObservable_2__ctor_m1636483622_gshared)(__this, ___source0, ___selector1, method)
// System.Void UniRx.Operators.SelectObservable`2<System.Object,System.Boolean>::.ctor(UniRx.IObservable`1<T>,System.Func`3<T,System.Int32,TR>)
extern "C"  void SelectObservable_2__ctor_m3683472714_gshared (SelectObservable_2_t4244715770 * __this, Il2CppObject* ___source0, Func_3_t3064567499 * ___selector1, const MethodInfo* method);
#define SelectObservable_2__ctor_m3683472714(__this, ___source0, ___selector1, method) ((  void (*) (SelectObservable_2_t4244715770 *, Il2CppObject*, Func_3_t3064567499 *, const MethodInfo*))SelectObservable_2__ctor_m3683472714_gshared)(__this, ___source0, ___selector1, method)
// System.IDisposable UniRx.Operators.SelectObservable`2<System.Object,System.Boolean>::SubscribeCore(UniRx.IObserver`1<TR>,System.IDisposable)
extern "C"  Il2CppObject * SelectObservable_2_SubscribeCore_m1583615598_gshared (SelectObservable_2_t4244715770 * __this, Il2CppObject* ___observer0, Il2CppObject * ___cancel1, const MethodInfo* method);
#define SelectObservable_2_SubscribeCore_m1583615598(__this, ___observer0, ___cancel1, method) ((  Il2CppObject * (*) (SelectObservable_2_t4244715770 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))SelectObservable_2_SubscribeCore_m1583615598_gshared)(__this, ___observer0, ___cancel1, method)
