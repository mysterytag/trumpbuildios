﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.Operators.DistinctObservable`1/Distinct<System.Object>
struct Distinct_t3232980021;
// UniRx.Operators.DistinctObservable`1<System.Object>
struct DistinctObservable_1_t18876622;
// UniRx.IObserver`1<System.Object>
struct IObserver_1_t3049105323;
// System.IDisposable
struct IDisposable_t1628921374;
// System.Object
struct Il2CppObject;
// System.Exception
struct Exception_t1967233988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Exception1967233988.h"

// System.Void UniRx.Operators.DistinctObservable`1/Distinct<System.Object>::.ctor(UniRx.Operators.DistinctObservable`1<T>,UniRx.IObserver`1<T>,System.IDisposable)
extern "C"  void Distinct__ctor_m3356823872_gshared (Distinct_t3232980021 * __this, DistinctObservable_1_t18876622 * ___parent0, Il2CppObject* ___observer1, Il2CppObject * ___cancel2, const MethodInfo* method);
#define Distinct__ctor_m3356823872(__this, ___parent0, ___observer1, ___cancel2, method) ((  void (*) (Distinct_t3232980021 *, DistinctObservable_1_t18876622 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))Distinct__ctor_m3356823872_gshared)(__this, ___parent0, ___observer1, ___cancel2, method)
// System.Void UniRx.Operators.DistinctObservable`1/Distinct<System.Object>::OnNext(T)
extern "C"  void Distinct_OnNext_m2032766175_gshared (Distinct_t3232980021 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Distinct_OnNext_m2032766175(__this, ___value0, method) ((  void (*) (Distinct_t3232980021 *, Il2CppObject *, const MethodInfo*))Distinct_OnNext_m2032766175_gshared)(__this, ___value0, method)
// System.Void UniRx.Operators.DistinctObservable`1/Distinct<System.Object>::OnError(System.Exception)
extern "C"  void Distinct_OnError_m466487790_gshared (Distinct_t3232980021 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define Distinct_OnError_m466487790(__this, ___error0, method) ((  void (*) (Distinct_t3232980021 *, Exception_t1967233988 *, const MethodInfo*))Distinct_OnError_m466487790_gshared)(__this, ___error0, method)
// System.Void UniRx.Operators.DistinctObservable`1/Distinct<System.Object>::OnCompleted()
extern "C"  void Distinct_OnCompleted_m3537482305_gshared (Distinct_t3232980021 * __this, const MethodInfo* method);
#define Distinct_OnCompleted_m3537482305(__this, method) ((  void (*) (Distinct_t3232980021 *, const MethodInfo*))Distinct_OnCompleted_m3537482305_gshared)(__this, method)
