﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.ClientModels.SetFriendTagsRequest
struct SetFriendTagsRequest_t1987017382;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void PlayFab.ClientModels.SetFriendTagsRequest::.ctor()
extern "C"  void SetFriendTagsRequest__ctor_m367340375 (SetFriendTagsRequest_t1987017382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayFab.ClientModels.SetFriendTagsRequest::get_FriendPlayFabId()
extern "C"  String_t* SetFriendTagsRequest_get_FriendPlayFabId_m1238148511 (SetFriendTagsRequest_t1987017382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.SetFriendTagsRequest::set_FriendPlayFabId(System.String)
extern "C"  void SetFriendTagsRequest_set_FriendPlayFabId_m947902074 (SetFriendTagsRequest_t1987017382 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> PlayFab.ClientModels.SetFriendTagsRequest::get_Tags()
extern "C"  List_1_t1765447871 * SetFriendTagsRequest_get_Tags_m741977758 (SetFriendTagsRequest_t1987017382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.ClientModels.SetFriendTagsRequest::set_Tags(System.Collections.Generic.List`1<System.String>)
extern "C"  void SetFriendTagsRequest_set_Tags_m1105724311 (SetFriendTagsRequest_t1987017382 * __this, List_1_t1765447871 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
