﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UniRx.InternalUtil.ListObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct ListObserver_1_t2743206917;
// UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>>
struct ImmutableList_1_t1425825352;
// System.Exception
struct Exception_t1967233988;
// UniRx.IObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>
struct IObserver_1_t365620853;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1967233988.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UniRx_CollectionMove2448589246.h"

// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::.ctor(UniRx.InternalUtil.ImmutableList`1<UniRx.IObserver`1<T>>)
extern "C"  void ListObserver_1__ctor_m2128453921_gshared (ListObserver_1_t2743206917 * __this, ImmutableList_1_t1425825352 * ___observers0, const MethodInfo* method);
#define ListObserver_1__ctor_m2128453921(__this, ___observers0, method) ((  void (*) (ListObserver_1_t2743206917 *, ImmutableList_1_t1425825352 *, const MethodInfo*))ListObserver_1__ctor_m2128453921_gshared)(__this, ___observers0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnCompleted()
extern "C"  void ListObserver_1_OnCompleted_m845006233_gshared (ListObserver_1_t2743206917 * __this, const MethodInfo* method);
#define ListObserver_1_OnCompleted_m845006233(__this, method) ((  void (*) (ListObserver_1_t2743206917 *, const MethodInfo*))ListObserver_1_OnCompleted_m845006233_gshared)(__this, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnError(System.Exception)
extern "C"  void ListObserver_1_OnError_m198695238_gshared (ListObserver_1_t2743206917 * __this, Exception_t1967233988 * ___error0, const MethodInfo* method);
#define ListObserver_1_OnError_m198695238(__this, ___error0, method) ((  void (*) (ListObserver_1_t2743206917 *, Exception_t1967233988 *, const MethodInfo*))ListObserver_1_OnError_m198695238_gshared)(__this, ___error0, method)
// System.Void UniRx.InternalUtil.ListObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::OnNext(T)
extern "C"  void ListObserver_1_OnNext_m3484224567_gshared (ListObserver_1_t2743206917 * __this, CollectionMoveEvent_1_t2448589246  ___value0, const MethodInfo* method);
#define ListObserver_1_OnNext_m3484224567(__this, ___value0, method) ((  void (*) (ListObserver_1_t2743206917 *, CollectionMoveEvent_1_t2448589246 , const MethodInfo*))ListObserver_1_OnNext_m3484224567_gshared)(__this, ___value0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::Add(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Add_m3531350665_gshared (ListObserver_1_t2743206917 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Add_m3531350665(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t2743206917 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Add_m3531350665_gshared)(__this, ___observer0, method)
// UniRx.IObserver`1<T> UniRx.InternalUtil.ListObserver`1<UniRx.CollectionMoveEvent`1<UniRx.Tuple`2<System.Object,System.Object>>>::Remove(UniRx.IObserver`1<T>)
extern "C"  Il2CppObject* ListObserver_1_Remove_m1972158498_gshared (ListObserver_1_t2743206917 * __this, Il2CppObject* ___observer0, const MethodInfo* method);
#define ListObserver_1_Remove_m1972158498(__this, ___observer0, method) ((  Il2CppObject* (*) (ListObserver_1_t2743206917 *, Il2CppObject*, const MethodInfo*))ListObserver_1_Remove_m1972158498_gshared)(__this, ___observer0, method)
