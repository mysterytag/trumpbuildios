﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>
struct Dictionary_2_t2290665470;
// System.Collections.Generic.IEqualityComparer`1<SponsorPay.SPLogLevel>
struct IEqualityComparer_1_t2544988786;
// System.Collections.Generic.IDictionary`2<SponsorPay.SPLogLevel,System.Int32>
struct IDictionary_2_t3466331257;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2995724695;
// System.Collections.Generic.ICollection`1<SponsorPay.SPLogLevel>
struct ICollection_1_t686553521;
// System.Collections.Generic.ICollection`1<System.Int32>
struct ICollection_1_t3313246173;
// System.Collections.ICollection
struct ICollection_t3761522009;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<SponsorPay.SPLogLevel,System.Int32>[]
struct KeyValuePair_2U5BU5D_t456901409;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<SponsorPay.SPLogLevel,System.Int32>>
struct IEnumerator_1_t3262303216;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1541724277;
// System.Collections.Generic.Dictionary`2/KeyCollection<SponsorPay.SPLogLevel,System.Int32>
struct KeyCollection_t318973454;
// System.Collections.Generic.Dictionary`2/ValueCollection<SponsorPay.SPLogLevel,System.Int32>
struct ValueCollection_t4212802564;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2995724695.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCont986364934.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21779196768.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharp_SponsorPay_SPLogLevel220722135.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2057693411.h"
#include "mscorlib_System_Collections_DictionaryEntry130027246.h"

// System.Void System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::.ctor()
extern "C"  void Dictionary_2__ctor_m3761323824_gshared (Dictionary_2_t2290665470 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m3761323824(__this, method) ((  void (*) (Dictionary_2_t2290665470 *, const MethodInfo*))Dictionary_2__ctor_m3761323824_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m2036985904_gshared (Dictionary_2_t2290665470 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m2036985904(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t2290665470 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2036985904_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m3032746751_gshared (Dictionary_2_t2290665470 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m3032746751(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t2290665470 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3032746751_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m1388542986_gshared (Dictionary_2_t2290665470 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m1388542986(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t2290665470 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m1388542986_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m2482853470_gshared (Dictionary_2_t2290665470 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m2482853470(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t2290665470 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2482853470_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m2037446195_gshared (Dictionary_2_t2290665470 * __this, int32_t ___capacity0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m2037446195(__this, ___capacity0, ___comparer1, method) ((  void (*) (Dictionary_2_t2290665470 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2037446195_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m7384570_gshared (Dictionary_2_t2290665470 * __this, SerializationInfo_t2995724695 * ___info0, StreamingContext_t986364934  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m7384570(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t2290665470 *, SerializationInfo_t2995724695 *, StreamingContext_t986364934 , const MethodInfo*))Dictionary_2__ctor_m7384570_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3393814271_gshared (Dictionary_2_t2290665470 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3393814271(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t2290665470 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3393814271_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m813228443_gshared (Dictionary_2_t2290665470 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m813228443(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t2290665470 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m813228443_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m1034109495_gshared (Dictionary_2_t2290665470 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m1034109495(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2290665470 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m1034109495_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m4009334117_gshared (Dictionary_2_t2290665470 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m4009334117(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2290665470 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m4009334117_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m3470678788_gshared (Dictionary_2_t2290665470 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m3470678788(__this, method) ((  bool (*) (Dictionary_2_t2290665470 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m3470678788_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m11776885_gshared (Dictionary_2_t2290665470 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m11776885(__this, method) ((  bool (*) (Dictionary_2_t2290665470 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m11776885_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m2554608037_gshared (Dictionary_2_t2290665470 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m2554608037(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t2290665470 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m2554608037_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m3008356244_gshared (Dictionary_2_t2290665470 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m3008356244(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2290665470 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m3008356244_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m1342566973_gshared (Dictionary_2_t2290665470 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m1342566973(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2290665470 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m1342566973_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m1810556885_gshared (Dictionary_2_t2290665470 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m1810556885(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2290665470 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m1810556885_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m733741522_gshared (Dictionary_2_t2290665470 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m733741522(__this, ___key0, method) ((  void (*) (Dictionary_2_t2290665470 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m733741522_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1756028831_gshared (Dictionary_2_t2290665470 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1756028831(__this, method) ((  bool (*) (Dictionary_2_t2290665470 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1756028831_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2918379601_gshared (Dictionary_2_t2290665470 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2918379601(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2290665470 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2918379601_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3847504675_gshared (Dictionary_2_t2290665470 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3847504675(__this, method) ((  bool (*) (Dictionary_2_t2290665470 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3847504675_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1750539624_gshared (Dictionary_2_t2290665470 * __this, KeyValuePair_2_t1779196768  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1750539624(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t2290665470 *, KeyValuePair_2_t1779196768 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1750539624_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m604852638_gshared (Dictionary_2_t2290665470 * __this, KeyValuePair_2_t1779196768  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m604852638(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t2290665470 *, KeyValuePair_2_t1779196768 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m604852638_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1207862092_gshared (Dictionary_2_t2290665470 * __this, KeyValuePair_2U5BU5D_t456901409* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1207862092(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2290665470 *, KeyValuePair_2U5BU5D_t456901409*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1207862092_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3344910403_gshared (Dictionary_2_t2290665470 * __this, KeyValuePair_2_t1779196768  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3344910403(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t2290665470 *, KeyValuePair_2_t1779196768 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3344910403_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m3760615211_gshared (Dictionary_2_t2290665470 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m3760615211(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2290665470 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m3760615211_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m19098106_gshared (Dictionary_2_t2290665470 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m19098106(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2290665470 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m19098106_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1714710321_gshared (Dictionary_2_t2290665470 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1714710321(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t2290665470 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1714710321_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2875928766_gshared (Dictionary_2_t2290665470 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2875928766(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2290665470 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2875928766_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m974421977_gshared (Dictionary_2_t2290665470 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m974421977(__this, method) ((  int32_t (*) (Dictionary_2_t2290665470 *, const MethodInfo*))Dictionary_2_get_Count_m974421977_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::get_Item(TKey)
extern "C"  int32_t Dictionary_2_get_Item_m60323086_gshared (Dictionary_2_t2290665470 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m60323086(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t2290665470 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m60323086_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m3855823417_gshared (Dictionary_2_t2290665470 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m3855823417(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2290665470 *, int32_t, int32_t, const MethodInfo*))Dictionary_2_set_Item_m3855823417_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m3567117233_gshared (Dictionary_2_t2290665470 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m3567117233(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t2290665470 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m3567117233_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m3198197798_gshared (Dictionary_2_t2290665470 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m3198197798(__this, ___size0, method) ((  void (*) (Dictionary_2_t2290665470 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m3198197798_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m3944563106_gshared (Dictionary_2_t2290665470 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m3944563106(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2290665470 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m3944563106_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t1779196768  Dictionary_2_make_pair_m679973430_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m679973430(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t1779196768  (*) (Il2CppObject * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_make_pair_m679973430_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::pick_key(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_key_m594149832_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m594149832(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_pick_key_m594149832_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::pick_value(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_value_m2348106532_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m2348106532(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_pick_value_m2348106532_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m3006783469_gshared (Dictionary_2_t2290665470 * __this, KeyValuePair_2U5BU5D_t456901409* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m3006783469(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2290665470 *, KeyValuePair_2U5BU5D_t456901409*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m3006783469_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::Resize()
extern "C"  void Dictionary_2_Resize_m2868189983_gshared (Dictionary_2_t2290665470 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m2868189983(__this, method) ((  void (*) (Dictionary_2_t2290665470 *, const MethodInfo*))Dictionary_2_Resize_m2868189983_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m133730460_gshared (Dictionary_2_t2290665470 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m133730460(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2290665470 *, int32_t, int32_t, const MethodInfo*))Dictionary_2_Add_m133730460_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::Clear()
extern "C"  void Dictionary_2_Clear_m1791758436_gshared (Dictionary_2_t2290665470 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m1791758436(__this, method) ((  void (*) (Dictionary_2_t2290665470 *, const MethodInfo*))Dictionary_2_Clear_m1791758436_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m2021443854_gshared (Dictionary_2_t2290665470 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m2021443854(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2290665470 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m2021443854_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m3759203854_gshared (Dictionary_2_t2290665470 * __this, int32_t ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m3759203854(__this, ___value0, method) ((  bool (*) (Dictionary_2_t2290665470 *, int32_t, const MethodInfo*))Dictionary_2_ContainsValue_m3759203854_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m445209431_gshared (Dictionary_2_t2290665470 * __this, SerializationInfo_t2995724695 * ___info0, StreamingContext_t986364934  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m445209431(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t2290665470 *, SerializationInfo_t2995724695 *, StreamingContext_t986364934 , const MethodInfo*))Dictionary_2_GetObjectData_m445209431_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m1629917037_gshared (Dictionary_2_t2290665470 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m1629917037(__this, ___sender0, method) ((  void (*) (Dictionary_2_t2290665470 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m1629917037_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m823230242_gshared (Dictionary_2_t2290665470 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m823230242(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2290665470 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m823230242_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m586036071_gshared (Dictionary_2_t2290665470 * __this, int32_t ___key0, int32_t* ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m586036071(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t2290665470 *, int32_t, int32_t*, const MethodInfo*))Dictionary_2_TryGetValue_m586036071_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::get_Keys()
extern "C"  KeyCollection_t318973454 * Dictionary_2_get_Keys_m1227450644_gshared (Dictionary_2_t2290665470 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m1227450644(__this, method) ((  KeyCollection_t318973454 * (*) (Dictionary_2_t2290665470 *, const MethodInfo*))Dictionary_2_get_Keys_m1227450644_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::get_Values()
extern "C"  ValueCollection_t4212802564 * Dictionary_2_get_Values_m2499898672_gshared (Dictionary_2_t2290665470 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m2499898672(__this, method) ((  ValueCollection_t4212802564 * (*) (Dictionary_2_t2290665470 *, const MethodInfo*))Dictionary_2_get_Values_m2499898672_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::ToTKey(System.Object)
extern "C"  int32_t Dictionary_2_ToTKey_m44008739_gshared (Dictionary_2_t2290665470 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m44008739(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t2290665470 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m44008739_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::ToTValue(System.Object)
extern "C"  int32_t Dictionary_2_ToTValue_m660420671_gshared (Dictionary_2_t2290665470 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m660420671(__this, ___value0, method) ((  int32_t (*) (Dictionary_2_t2290665470 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m660420671_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m3140001029_gshared (Dictionary_2_t2290665470 * __this, KeyValuePair_2_t1779196768  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m3140001029(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t2290665470 *, KeyValuePair_2_t1779196768 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m3140001029_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::GetEnumerator()
extern "C"  Enumerator_t2057693412  Dictionary_2_GetEnumerator_m742811972_gshared (Dictionary_2_t2290665470 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m742811972(__this, method) ((  Enumerator_t2057693412  (*) (Dictionary_2_t2290665470 *, const MethodInfo*))Dictionary_2_GetEnumerator_m742811972_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<SponsorPay.SPLogLevel,System.Int32>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t130027246  Dictionary_2_U3CCopyToU3Em__0_m829055419_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m829055419(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t130027246  (*) (Il2CppObject * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m829055419_gshared)(__this /* static, unused */, ___key0, ___value1, method)
