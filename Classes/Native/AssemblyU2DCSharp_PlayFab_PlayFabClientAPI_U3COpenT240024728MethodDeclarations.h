﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayFab.PlayFabClientAPI/<OpenTrade>c__AnonStoreyD5
struct U3COpenTradeU3Ec__AnonStoreyD5_t240024728;
// PlayFab.CallRequestContainer
struct CallRequestContainer_t432318609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PlayFab_CallRequestCo432318609.h"

// System.Void PlayFab.PlayFabClientAPI/<OpenTrade>c__AnonStoreyD5::.ctor()
extern "C"  void U3COpenTradeU3Ec__AnonStoreyD5__ctor_m386345403 (U3COpenTradeU3Ec__AnonStoreyD5_t240024728 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayFab.PlayFabClientAPI/<OpenTrade>c__AnonStoreyD5::<>m__C2(PlayFab.CallRequestContainer)
extern "C"  void U3COpenTradeU3Ec__AnonStoreyD5_U3CU3Em__C2_m2922812264 (U3COpenTradeU3Ec__AnonStoreyD5_t240024728 * __this, CallRequestContainer_t432318609 * ___requestContainer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
